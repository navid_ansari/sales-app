cordova.define("plugin-handler.PluginHandler", function(require, exports, module) {
var PluginHandler = function(){
	
	PluginHandler.prototype.execPlugin = function(args, action, succCb, errCb){
		cordova.exec(
			function(fileData){
				succCb(fileData);
			},
			function(errMsg){
				errCb(errMsg)
			},
			"PluginHandler",action,args
		);
	};

}
});
