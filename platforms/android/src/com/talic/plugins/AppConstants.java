package com.talic.plugins;

import android.content.Context;
import com.talic.salesapp.SalesApp;

public class AppConstants {

	public static final String DOMAIN_URL = "https://lp.tataaia.com/"; // production
	//public static final String DOMAIN_URL = "https://lpu.tataaia.com/"; // UAT

	public static final int SA_DB_VER = 1;
	public static final String SA_MAIN_DB_NAME = "SA_DB_MAIN.db";
	public static final String SA_SIS_DB_NAME = "SA_DB_SIS.db";
	public static String SA_MAIN_DB_PATH = null;
	public static String SA_SIS_DB_PATH = null;
	public static String FILE_ABS_PATH = null;
	public static SalesApp SA_CLASS;
	public static EncryptDecryptFile EN_DE_OBJ;
	public static final String ROOT_FOLDER= ".Sales App";
	public static Context SA_APP_CONTEXT;
	public static final String TMP_DIR_NAME = "/.tmp";
	public static final String ENCRYPT_FILE_PREFIX = "_en";
}
