package com.talic.plugins.sisEngine.db;

import com.talic.plugins.sisEngine.BO.AAAFMCValues;
import com.talic.plugins.sisEngine.BO.ACMAdminChargesMaster;
import com.talic.plugins.sisEngine.BO.ANNUITYRate;
import com.talic.plugins.sisEngine.BO.AddressBO;
import com.talic.plugins.sisEngine.BO.AgentDetails;
import com.talic.plugins.sisEngine.BO.CCMCommissionChargeMaster;
import com.talic.plugins.sisEngine.BO.CommText;
import com.talic.plugins.sisEngine.BO.DataMapperBO;
import com.talic.plugins.sisEngine.BO.FCMFIBChargesMaster;
import com.talic.plugins.sisEngine.BO.FEDFormulaExtDesc;
import com.talic.plugins.sisEngine.BO.FOPRate;
import com.talic.plugins.sisEngine.BO.FundDetailsLK;
import com.talic.plugins.sisEngine.BO.GAIGuarAnnIncChMaster;
import com.talic.plugins.sisEngine.BO.GuaranteedMaturityBonusMaster;
import com.talic.plugins.sisEngine.BO.INCENTIVERate;
import com.talic.plugins.sisEngine.BO.IPCValues;
import com.talic.plugins.sisEngine.BO.LpbLargePremiumBoost;
import com.talic.plugins.sisEngine.BO.MCMMortilityChargesMaster;
import com.talic.plugins.sisEngine.BO.MDLModalFactorLk;
import com.talic.plugins.sisEngine.BO.MFMMaturityFactorMaster;
import com.talic.plugins.sisEngine.BO.OCLOccupationLk;
import com.talic.plugins.sisEngine.BO.PACPremiumAllocationCharges;
import com.talic.plugins.sisEngine.BO.PBAProductBand;
import com.talic.plugins.sisEngine.BO.PCRPlanChargesRef;
import com.talic.plugins.sisEngine.BO.PFRPlanFormulaRef;
import com.talic.plugins.sisEngine.BO.PGFPlanGroupFeature;
import com.talic.plugins.sisEngine.BO.PGLPlanGroupLk;
import com.talic.plugins.sisEngine.BO.PMLPremiumMultipleLk;
import com.talic.plugins.sisEngine.BO.PMVPlanMaturityValue;
import com.talic.plugins.sisEngine.BO.PNLMinPremLK;
import com.talic.plugins.sisEngine.BO.PNLPlanLk;
import com.talic.plugins.sisEngine.BO.PmdPlanPremiumMultDisc;
import com.talic.plugins.sisEngine.BO.RDLChargesValue;
import com.talic.plugins.sisEngine.BO.RMLRiderPremMultipleLK;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.BO.SPVSysParamValues;
import com.talic.plugins.sisEngine.BO.SURSurrenderChargeMaster;
import com.talic.plugins.sisEngine.BO.TempImagePath;
import com.talic.plugins.sisEngine.BO.WCMWOPChargesMaster;
import com.talic.plugins.sisEngine.bean.FormulaBean;
import com.talic.plugins.sisEngine.bean.SISRequestBean;
import com.talic.plugins.sisEngine.control.JCSCaching;
import com.talic.plugins.sisEngine.util.CommonConstant;
import com.talic.plugins.sisEngine.util.FormulaConstant;
import com.talic.plugins.sisEngine.util.FormulaHandler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author OSNPO272
 *
 */
@SuppressWarnings("unchecked")
public class DataAccessClass {

	public static DataMapperBO oDataMapperBO;


	static {
		oDataMapperBO = JCSCaching.getoDataMapperBO(null,null,null);
	}

	public static HashMap getformulaExp(String formulaID) {
		HashMap formulaExpMap = new HashMap();
		FEDFormulaExtDesc oFEDFormulaExtDesc = null;
		ArrayList FEDFormulaExtDescLst = oDataMapperBO.getFEDFormulaExtDesc();
		for (int lstCount = 0; lstCount < FEDFormulaExtDescLst.size(); lstCount++) {
			oFEDFormulaExtDesc = (FEDFormulaExtDesc) FEDFormulaExtDescLst
					.get(lstCount);
			if (oFEDFormulaExtDesc != null) {
				if (formulaID.equalsIgnoreCase(oFEDFormulaExtDesc.getFED_ID())) {
					formulaExpMap.put(oFEDFormulaExtDesc.getFED_OBJ_SEQ(), oFEDFormulaExtDesc);
				}
			}
		}

		return formulaExpMap;
	}

	public static PFRPlanFormulaRef getformula(String planCD, String type){
		PFRPlanFormulaRef oPFRPlanFormulaRef = null;
		ArrayList PFXPlanFormulaXrefLst = oDataMapperBO.getPFRPlanFormulaRef();
		for(int lstCount = 0; lstCount < PFXPlanFormulaXrefLst.size(); lstCount++){
			oPFRPlanFormulaRef = (PFRPlanFormulaRef)PFXPlanFormulaXrefLst.get(lstCount);
			if(oPFRPlanFormulaRef != null){
				if(planCD.equalsIgnoreCase(oPFRPlanFormulaRef.getPFR_PLAN_CD())
						&& type.equalsIgnoreCase(oPFRPlanFormulaRef.getPFR_FRM_TYPE())){
					return oPFRPlanFormulaRef;
				}
			}
		}

		return null;
	}

	public static HashMap getCashSurrenderValueFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "SV Factor for PLan ID::::: " + planId
				+ " term: " + term);
		HashMap SVFactorMap = new HashMap();
		boolean PVFlag = false;
		ArrayList PMVMaturityValueLst = oDataMapperBO.getPMVPlanMaturityValue();
		for(int count = 0; count < PMVMaturityValueLst.size(); count++){
			PMVPlanMaturityValue oPMVMaturityValue =  (PMVPlanMaturityValue)PMVMaturityValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_BAND() :  " +oPMVMaturityValue.getPMV_BAND());
			try {
				int band = Integer.parseInt(oPMVMaturityValue.getPMV_BAND());
				PVFlag = true;
			} catch (Exception e) {
				PVFlag = false;
			}

			if("D".equals(oPMVMaturityValue.getPMV_USE()) && PVFlag ){
				if(planId == oPMVMaturityValue.getPMV_PNL_ID()
						&& Integer.parseInt(oPMVMaturityValue.getPMV_BAND()) == term){
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR1(), oPMVMaturityValue.getPMV_CVAL1());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR2(), oPMVMaturityValue.getPMV_CVAL2());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR3(), oPMVMaturityValue.getPMV_CVAL3());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR4(), oPMVMaturityValue.getPMV_CVAL4());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR5(), oPMVMaturityValue.getPMV_CVAL5());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR6(), oPMVMaturityValue.getPMV_CVAL6());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR7(), oPMVMaturityValue.getPMV_CVAL7());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR8(), oPMVMaturityValue.getPMV_CVAL8());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR9(), oPMVMaturityValue.getPMV_CVAL9());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR10(), oPMVMaturityValue.getPMV_CVAL10());
					CommonConstant.printLog("d", "SV Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "SV Factor PMV_DUR1 " + oPMVMaturityValue.getPMV_DUR1() + " : " + oPMVMaturityValue.getPMV_CVAL1());
					CommonConstant.printLog("d", "SV Factor PMV_DUR2 " + oPMVMaturityValue.getPMV_DUR2() + " : " + oPMVMaturityValue.getPMV_CVAL2());
					CommonConstant.printLog("d", "SV Factor PMV_DUR3 " + oPMVMaturityValue.getPMV_DUR3() + " : " + oPMVMaturityValue.getPMV_CVAL3());
					CommonConstant.printLog("d", "SV Factor PMV_DUR4 " + oPMVMaturityValue.getPMV_DUR4() + " : " + oPMVMaturityValue.getPMV_CVAL4());
					CommonConstant.printLog("d", "SV Factor PMV_DUR5 " + oPMVMaturityValue.getPMV_DUR5() + " : " + oPMVMaturityValue.getPMV_CVAL5());
					CommonConstant.printLog("d", "SV Factor PMV_DUR6 " + oPMVMaturityValue.getPMV_DUR6() + " : " + oPMVMaturityValue.getPMV_CVAL6());
					CommonConstant.printLog("d", "SV Factor PMV_DUR7 " + oPMVMaturityValue.getPMV_DUR7() + " : " + oPMVMaturityValue.getPMV_CVAL7());
					CommonConstant.printLog("d", "SV Factor PMV_DUR8 " + oPMVMaturityValue.getPMV_DUR8() + " : " + oPMVMaturityValue.getPMV_CVAL8());
					CommonConstant.printLog("d", "SV Factor PMV_DUR9 " + oPMVMaturityValue.getPMV_DUR9() + " : " + oPMVMaturityValue.getPMV_CVAL9());
					CommonConstant.printLog("d", "SV Factor PMV_DUR10 " + oPMVMaturityValue.getPMV_DUR10() + " : " + oPMVMaturityValue.getPMV_CVAL10());
				}
			}
		}


		return SVFactorMap;
	}

	public static HashMap getGuranteedSurrenderValueFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "Guranteed Surrender Value Factor for PLan ID::::: " + planId + " term: " + term);
		HashMap SVFactorMap = new HashMap();
		ArrayList PMVMaturityValueLst = oDataMapperBO.getPMVPlanMaturityValue();
		for(int count = 0; count < PMVMaturityValueLst.size(); count++){
			PMVPlanMaturityValue oPMVMaturityValue =  (PMVPlanMaturityValue)PMVMaturityValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());

			if("GV".equals(oPMVMaturityValue.getPMV_BAND())){
				if(planId == oPMVMaturityValue.getPMV_PNL_ID()
						&& oPMVMaturityValue.getPMV_AGE() == term){
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR1(), oPMVMaturityValue.getPMV_CVAL1());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR2(), oPMVMaturityValue.getPMV_CVAL2());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR3(), oPMVMaturityValue.getPMV_CVAL3());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR4(), oPMVMaturityValue.getPMV_CVAL4());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR5(), oPMVMaturityValue.getPMV_CVAL5());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR6(), oPMVMaturityValue.getPMV_CVAL6());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR7(), oPMVMaturityValue.getPMV_CVAL7());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR8(), oPMVMaturityValue.getPMV_CVAL8());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR9(), oPMVMaturityValue.getPMV_CVAL9());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR10(), oPMVMaturityValue.getPMV_CVAL10());
					CommonConstant.printLog("d", "SV Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "SV Factor PMV_DUR1 " + oPMVMaturityValue.getPMV_DUR1() + " : " + oPMVMaturityValue.getPMV_CVAL1());
					CommonConstant.printLog("d", "SV Factor PMV_DUR2 " + oPMVMaturityValue.getPMV_DUR2() + " : " + oPMVMaturityValue.getPMV_CVAL2());
					CommonConstant.printLog("d", "SV Factor PMV_DUR3 " + oPMVMaturityValue.getPMV_DUR3() + " : " + oPMVMaturityValue.getPMV_CVAL3());
					CommonConstant.printLog("d", "SV Factor PMV_DUR4 " + oPMVMaturityValue.getPMV_DUR4() + " : " + oPMVMaturityValue.getPMV_CVAL4());
					CommonConstant.printLog("d", "SV Factor PMV_DUR5 " + oPMVMaturityValue.getPMV_DUR5() + " : " + oPMVMaturityValue.getPMV_CVAL5());
					CommonConstant.printLog("d", "SV Factor PMV_DUR6 " + oPMVMaturityValue.getPMV_DUR6() + " : " + oPMVMaturityValue.getPMV_CVAL6());
					CommonConstant.printLog("d", "SV Factor PMV_DUR7 " + oPMVMaturityValue.getPMV_DUR7() + " : " + oPMVMaturityValue.getPMV_CVAL7());
					CommonConstant.printLog("d", "SV Factor PMV_DUR8 " + oPMVMaturityValue.getPMV_DUR8() + " : " + oPMVMaturityValue.getPMV_CVAL8());
					CommonConstant.printLog("d", "SV Factor PMV_DUR9 " + oPMVMaturityValue.getPMV_DUR9() + " : " + oPMVMaturityValue.getPMV_CVAL9());
					CommonConstant.printLog("d", "SV Factor PMV_DUR10 " + oPMVMaturityValue.getPMV_DUR10() + " : " + oPMVMaturityValue.getPMV_CVAL10());
				}
			}
		}


		return SVFactorMap;
	}

	public static double getPremiumDiscount(String planCode, long sumAssured){
		double premiumDiscount = 0;

		ArrayList pmdPlanPremiumMultDiscList = oDataMapperBO.getPmdPlanPremiumMultDisc();
	//	CommonConstant.printLog("d","Primium Discount List Size:::: " + pmdPlanPremiumMultDiscList.size());
		for(int count = 0; count< pmdPlanPremiumMultDiscList.size(); count++)
		{
			PmdPlanPremiumMultDisc oPmdPremiumMultDisc =  (PmdPlanPremiumMultDisc)pmdPlanPremiumMultDiscList.get(count);
			if(planCode.equals(oPmdPremiumMultDisc.getPMD_PLAN_CODE())
					&& (sumAssured >= oPmdPremiumMultDisc.getPMD_MIN_SA() && sumAssured <= oPmdPremiumMultDisc.getPMD_MAX_SA())){
				premiumDiscount = oPmdPremiumMultDisc.getPMD_DISC_RATE();
				CommonConstant.printLog("d", "Premium Discount selected :: " + premiumDiscount);
				break;
			}
		}


		return premiumDiscount;
	}

	public static double getPremiumBoost(FormulaBean fBean){
		double premiumBoost = 0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(fBean.getRequestBean().getBaseplan());
		ArrayList lpbPremiumBoost = oDataMapperBO.getPremiumBoost();
		for(int count = 0; count< lpbPremiumBoost.size(); count++)
		{
			LpbLargePremiumBoost oLpbLargePremiumBoost =  (LpbLargePremiumBoost)lpbPremiumBoost.get(count);
			if((oPNLPlanLk.getPNL_CODE()).equals(oLpbLargePremiumBoost.getLPB_PLAN_CODE())
					&& (fBean.getP() >= oLpbLargePremiumBoost.getLPB_MIN_PREMIUM() && fBean.getP() <= oLpbLargePremiumBoost.getLPB_MAX_PREMIUM())){
				premiumBoost = oLpbLargePremiumBoost.getLPB_BOOST_RATE();
				CommonConstant.printLog("d", "Premium Boost selected :: " + premiumBoost);
				break;
			}
		}


		return premiumBoost;
	}

	public static ArrayList getPBABandList(String planCode) {
		ArrayList pbaBandList= new ArrayList();
		ArrayList pbaProductBandList = oDataMapperBO.getPBAProductBand();
		for(int count = 0; count< pbaProductBandList.size(); count++)
		{
			PBAProductBand oPBAProductBand =  (PBAProductBand)pbaProductBandList.get(count);
			if(planCode.equals(oPBAProductBand.getPBA_PLAN_CODE())){
				CommonConstant.printLog("d", "Product Band ::PLAN_CD:TYPE:BAND:SUM_A "
						+ oPBAProductBand.getPBA_PLAN_CODE() + " : "
						+ oPBAProductBand.getPBA_TYPE() + " : "
						+ oPBAProductBand.getPBA_BAND() + " : "
						+ oPBAProductBand.getPBA_SUM_ASSURED());
					pbaBandList.add(oPBAProductBand);
				}
		}
		return pbaBandList;
	}

	public static double getModelFactor(int pglID, String payMode){
		double modelFactor = 0;
		ArrayList modelFactorList = oDataMapperBO.getMDLModalFactorLk();
		MDLModalFactorLk oMDLModalFactorLk= new MDLModalFactorLk();
		CommonConstant.printLog("d", "Plan Group ID:::::" + pglID);
		for(int count = 0; count< modelFactorList.size(); count++){
			oMDLModalFactorLk = (MDLModalFactorLk)modelFactorList.get(count);
			if((pglID == oMDLModalFactorLk.getMDL_PGL_ID())
					&& payMode.equalsIgnoreCase(oMDLModalFactorLk.getMDL_PAY_FREQ())){
				modelFactor = oMDLModalFactorLk.getMDL_MODAL_FACTOR();
				break;
			}
		}
		CommonConstant.printLog("d", "Model Factor" + modelFactor);
		return modelFactor;
	}

	public static double getADMCharges(int pnlID,long premium){
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
		double dADMCharges=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
		ArrayList alACMAdminChargesMaster= oDataMapperBO.getACMAdminChargesMaster();
		ACMAdminChargesMaster oACMAdminChargesMaster;
		for(int count = 0; count< alACMAdminChargesMaster.size(); count++){
			oACMAdminChargesMaster=(ACMAdminChargesMaster)alACMAdminChargesMaster.get(count);

			if(oPCRPlanChargesRef.getPCR_ADMIN_ID()==oACMAdminChargesMaster.getACM_ID()
					&& (premium >= oACMAdminChargesMaster.getACM_MIN_PREM()&& premium<=oACMAdminChargesMaster.getACM_MAX_PREM())) {
				dADMCharges=oACMAdminChargesMaster.getACM_CHARGE();
				CommonConstant.printLog("d", " #premium : " + premium + " #ADM Charges : " + dADMCharges);
				break;
			}

		}
		return dADMCharges;
	}
///////////////////////////////////////////////////////////////////////////////
	public static double getPremiumMultiple(SISRequestBean reqBean) {
		double premiumMultiple = 0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		ArrayList pbaBandLst = getPBABandList(reqBean.getBaseplan());
		ArrayList pmlPremiumMultipleList = oDataMapperBO.getPMLPremiumMultipleLk();

		for (int count = 0; count < pmlPremiumMultipleList.size(); count++) {
			PMLPremiumMultipleLk oPMLPremiumMultipleLk = (PMLPremiumMultipleLk) pmlPremiumMultipleList
					.get(count);
			if (oPMLPremiumMultipleLk.getPML_PNL_ID() == oPNLPlanLk.getPNL_ID()) {

				String sex = "";
				int age = 0;
				int term = 0;
				String smokerFlg = "";
				if ("U".equals(oPMLPremiumMultipleLk.getPML_SEX())) {
					sex = "U";
				} else {
					sex = reqBean.getInsured_sex();
				}

				if (oPMLPremiumMultipleLk.getPML_AGE() == 999) {
					age = 999;
				} else {
					age = reqBean.getInsured_age();
				}
				if (oPNLPlanLk.getPNL_PREM_MULT_FORMULA() == 3) {
					term = reqBean.getPolicyterm();
				} else {
					term = oPMLPremiumMultipleLk.getPML_TERM();
				}
				CommonConstant.printLog("d", "Term :::::::::::" + term + " : " + oPMLPremiumMultipleLk.getPML_TERM());

				if ("0".equals(reqBean.getInsured_smokerstatus())) {
					smokerFlg = "Y";
				} else if ("1".equals(reqBean.getInsured_smokerstatus())) {
					smokerFlg = "N";
				} else {
					smokerFlg = oPMLPremiumMultipleLk.getSMOKER_FLAG();
				}

				if(oPMLPremiumMultipleLk.getSMOKER_FLAG()==null){
					oPMLPremiumMultipleLk.setSMOKER_FLAG("");
				}
				if (pbaBandLst.size() > 0) {
					int pbaBand = getPBABand(pbaBandLst, reqBean.getSumassured());
					CommonConstant.printLog("d", "################## pbaBand : " + pbaBand);

					if (oPMLPremiumMultipleLk.getPML_AGE() == age
							&& sex.equals(oPMLPremiumMultipleLk.getPML_SEX())
							&& oPMLPremiumMultipleLk.getPML_BAND() == pbaBand
							&& smokerFlg.equals(oPMLPremiumMultipleLk.getSMOKER_FLAG()))
					{
						premiumMultiple = oPMLPremiumMultipleLk.getPML_PREM_MULT();
						CommonConstant.printLog("d", "PML_PNL_ID:PML_SEX:PML_BAND:PML_AGE:PML_PREM_MULT:PML_BAND:PML_SMOKER"
								+ oPMLPremiumMultipleLk.getPML_PNL_ID()
								+ oPMLPremiumMultipleLk.getPML_SEX()
								+ oPMLPremiumMultipleLk.getPML_BAND()
								+ oPMLPremiumMultipleLk.getPML_AGE()
								+ oPMLPremiumMultipleLk.getPML_PREM_MULT()
								+ oPMLPremiumMultipleLk.getPML_BAND()
								+ oPMLPremiumMultipleLk.getSMOKER_FLAG());

						break;
					}
					} else if (oPMLPremiumMultipleLk.getPML_AGE() == age
							&& sex.equals(oPMLPremiumMultipleLk.getPML_SEX())
							&& oPMLPremiumMultipleLk.getPML_TERM() == term
							&& smokerFlg.equals(oPMLPremiumMultipleLk.getSMOKER_FLAG()))
					{
						premiumMultiple = oPMLPremiumMultipleLk.getPML_PREM_MULT();
						CommonConstant.printLog("d", "PML_PNL_ID:PML_SEX:PML_TERM:PML_AGE:PML_PREM_MULT:PML_BAND:PML_SMOKER"
								+ oPMLPremiumMultipleLk.getPML_PNL_ID()
								+ oPMLPremiumMultipleLk.getPML_SEX()
								+ oPMLPremiumMultipleLk.getPML_TERM()
								+ oPMLPremiumMultipleLk.getPML_AGE()
								+ oPMLPremiumMultipleLk.getPML_PREM_MULT()
								+ oPMLPremiumMultipleLk.getPML_BAND()
								+ oPMLPremiumMultipleLk.getSMOKER_FLAG());
						break;
					}
				}

			}
		// Check if any premium discount is there
		double premiumDiscount =  DataAccessClass.getPremiumDiscount(oPNLPlanLk.getPNL_CODE(), reqBean.getSumassured());
		double premMult= 0;
		BigDecimal premMultSR= BigDecimal.valueOf(0d);
		if (oPNLPlanLk.getPNL_CODE().startsWith("SR") || oPNLPlanLk.getPNL_CODE().startsWith("SRP")) {
			BigDecimal diffDisc = BigDecimal.valueOf(1 - premiumDiscount);

			premMultSR = BigDecimal.valueOf(premiumMultiple).multiply(diffDisc);
		} else
			premMult= premiumMultiple - premiumDiscount;
		//premiumDiscount =  DataAccessClass.getPremiumChannelDiscount(oPNLPlanLk.getPNL_CODE(), reqBean.getSumassured(),reqBean.getAgentType());
		//premMult = premMult - (premMult*(premiumDiscount));
		BigDecimal cut = new BigDecimal(0);
		BigDecimal fd = new BigDecimal(premMult);
		cut = fd.setScale(2, RoundingMode.HALF_UP);
		if (oPNLPlanLk.getPNL_CODE().startsWith("SR") || oPNLPlanLk.getPNL_CODE().startsWith("SRP")) {
			cut = premMultSR.setScale(2, RoundingMode.HALF_UP);
			premiumMultiple = cut.doubleValue();

		} else {
			premiumMultiple = cut.doubleValue();
		}
		/*double premMult = 1;
		if(oPNLPlanLk.getPNL_CODE().startsWith("SR") || oPNLPlanLk.getPNL_CODE().startsWith("SRP")) {
			premMult = premiumMultiple * (1 - premiumDiscount);
			BigDecimal fd = new BigDecimal(premMult);
			BigDecimal cut = fd.setScale(3, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
			premiumMultiple = cut.doubleValue();
		}
		else {
			premMult = premiumMultiple - premiumDiscount;
			BigDecimal fd = new BigDecimal(premMult);
			BigDecimal cut = fd.setScale(2, RoundingMode.HALF_UP);
			premiumMultiple = cut.doubleValue();
		}*/
		CommonConstant.printLog("d", "premiumMultiple from database::::::::::: " + premiumMultiple);
		return premiumMultiple;
	}

	public static PNLPlanLk getPlanLKObject(String planCode)
	{
		PNLPlanLk pnlPlanLk = null;
		HashMap pnlPlanLkHashMap = oDataMapperBO.getPNLPlanLk();
		pnlPlanLk = (PNLPlanLk)pnlPlanLkHashMap.get(planCode);

		return pnlPlanLk;
	}
	public static int getPBABand(ArrayList pbaBandLst, long sumAssured) {
		int pbaBand = 0;
		try {

			for (int count = 0; count < pbaBandLst.size(); count++) {
				PBAProductBand oProductBand = (PBAProductBand) pbaBandLst
						.get(count);
				if ("B".equals(oProductBand.getPBA_TYPE())
						&& oProductBand.getPBA_SUM_ASSURED() >= sumAssured) {
					count = pbaBandLst.size();
					int band = Integer.parseInt(oProductBand.getPBA_BAND());
					if (band > pbaBand) {
						pbaBand = band;
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		CommonConstant.printLog("d", "PBA Band selected :::: " + pbaBand);
		return pbaBand;
	}

	public static double getRiderPremiumMultiple(SISRequestBean reqBean, int cnt){
		double riderPremMultiple = 0;
		RiderBO oRiderBO = (RiderBO)reqBean.getRiderlist().get(cnt);
		if(oRiderBO.getCode().startsWith("PB")){
			//calculate PB rider premium here
			riderPremMultiple = getPBRiderPremiumMultiple(reqBean, cnt);
		}else{
			//calculate non PB rider premium here
			riderPremMultiple = getNonPBRiderPremiumMultiple(reqBean, cnt);
		}

		return riderPremMultiple;

	}

	public static double getPBRiderPremiumMultiple(SISRequestBean reqBean, int cnt)
	{
		int pbaBand = 0;
		double riderPremMultiple = 0;
		int min_band1 = 21 - reqBean.getInsured_age();
		int min_band2 = 60 - reqBean.getInsured_age();
		int min_band3 = reqBean.getPremiumpayingterm();
		RiderBO riderObj = (RiderBO)reqBean.getRiderlist().get(cnt);
		if((min_band1 <= min_band2) && (min_band1 <= min_band3))
        {
			pbaBand = min_band1;
        }else if((min_band2 <= min_band1)&& (min_band2 <= min_band3)){
        	pbaBand = min_band2;
		}else if((min_band3 <= min_band1)&& (min_band3 <= min_band2))
		{
			pbaBand = min_band3;
		}

		ArrayList rmlRiderPremMultipleList = oDataMapperBO.getRMLRiderPremMultipleLK();
		for (int count = 0; count < rmlRiderPremMultipleList.size(); count++) {
			RMLRiderPremMultipleLK oRMLRiderPremMultipleLK = (RMLRiderPremMultipleLK) rmlRiderPremMultipleList.get(count);
			if ( riderObj.getCode().equalsIgnoreCase(oRMLRiderPremMultipleLK.getRML_RIDER_CD())) {
				int age = 0;
				if (oRMLRiderPremMultipleLK.getRML_AGE() == 999) {
					age = 999;
				} else {
					age = reqBean.getInsured_age();
				}
				if (oRMLRiderPremMultipleLK.getRML_AGE() == age
							&& oRMLRiderPremMultipleLK.getRML_BAND() == pbaBand)
					{
						riderPremMultiple = oRMLRiderPremMultipleLK.getRML_PREM_MULT();
						break;
					}
			}
		}

		return riderPremMultiple;
	}

	public static double getNonPBRiderPremiumMultiple(SISRequestBean reqBean, int cnt)
	{
		int pbaBand = 0;
		int occClass = 0;
		double riderPremMultiple = 0;
		int pbaBandBySA = 0;
		int occBandCount = 0;
		RiderBO riderObj = (RiderBO)reqBean.getRiderlist().get(cnt);

		CommonConstant.printLog("d", "####getRiderPremium - Calculate rider premium for :::: " + riderObj.getCode());

		ArrayList pbaRiderList = getPBABandList(riderObj.getCode());

		HashMap oclOccupationLkMap =  oDataMapperBO.getOCLOccupationLk();
		OCLOccupationLk oOCLOccupationLk = (OCLOccupationLk)oclOccupationLkMap.get(reqBean.getInsured_occupation());
		occClass = oOCLOccupationLk.getOCL_CLASS();
		if(pbaRiderList.size()>0)
		{
			for(int count = 0; count < pbaRiderList.size(); count++){
				PBAProductBand oPBAProductBand = (PBAProductBand)pbaRiderList.get(count);
				if("R".equals(oPBAProductBand.getPBA_TYPE())
						&& oPBAProductBand.getPBA_SUM_ASSURED() >= riderObj.getSumAssured()){
					int pbaBand1 = Integer.parseInt(oPBAProductBand.getPBA_BAND());
					if(pbaBandBySA == 0){
						pbaBandBySA = pbaBand1;
					}else if(pbaBand1 < pbaBandBySA){
						pbaBandBySA = pbaBand1;
					}
					//CommonConstant.printLog("d","PBA Band By SA :::: " + pbaBandBySA);
				}
				if("R".equals(oPBAProductBand.getPBA_TYPE())
						&& Integer.parseInt(oPBAProductBand.getPBA_BAND()) == occClass){
					++occBandCount;
				}

			}
		}
		ArrayList rmlRiderPremMultipleList = oDataMapperBO.getRMLRiderPremMultipleLK();
		for (int count = 0; count < rmlRiderPremMultipleList.size(); count++) {
			RMLRiderPremMultipleLK oRMLRiderPremMultipleLK = (RMLRiderPremMultipleLK) rmlRiderPremMultipleList.get(count);
			if ( riderObj.getCode().equalsIgnoreCase(oRMLRiderPremMultipleLK.getRML_RIDER_CD())) {

				String sex = "";
				int age = 0;
				int term = 0;
				if("U".equals(oRMLRiderPremMultipleLK.getRML_SEX())){
					sex = "U";
				}else{
					sex = reqBean.getInsured_sex();
				}
				if (oRMLRiderPremMultipleLK.getRML_AGE() == 999) {
					age = 999;
				} else {
					age = reqBean.getInsured_age();
				}
				if(occBandCount > 0){
					pbaBand = occClass;
				}else{
					pbaBand = pbaBandBySA;
				}
				//get rml term
				if(oRMLRiderPremMultipleLK.getRML_TERM() == 0){
					term = 0;
				}else if(riderObj.getRtlID() == 13 || riderObj.getRtlID() == 8)
					{
						if( reqBean.getPolicyterm() > (65 - reqBean.getInsured_age())){
							term = 65 - reqBean.getInsured_age();
						}else{
							term = reqBean.getPolicyterm();
						}
				}
				//Start : addded by jayesh for Smart7 02-07-2014
				else if(riderObj.getRtlID() == 14){
						if( reqBean.getPremiumpayingterm() > (65 - reqBean.getInsured_age())){
							term = 65 - reqBean.getInsured_age();
				}else{
							term = reqBean.getPremiumpayingterm();
						}
				}
				else if(riderObj.getRtlID() == 15){
					if( reqBean.getPremiumpayingterm() > (65 - reqBean.getProposer_age())){
						term = 65 - reqBean.getProposer_age();
					}else{
						term = reqBean.getPremiumpayingterm();
					}
					sex = reqBean.getProposer_sex();
					age = reqBean.getProposer_age();
				}
				//End : addded by jayesh for Smart7 02-07-2014
				else{
					term = reqBean.getPolicyterm();
				}

				if(pbaRiderList.size()>0){
					if (oRMLRiderPremMultipleLK.getRML_AGE() == age
							&& oRMLRiderPremMultipleLK.getRML_BAND() == pbaBand
							&& sex.equalsIgnoreCase(oRMLRiderPremMultipleLK.getRML_SEX()))
					{
						CommonConstant.printLog("d", "#getRiderPremium  If Age :::: PBA Band ::: Sex ::: Term " + age + " : " + pbaBand + " : " + sex + " : " + term);
						riderPremMultiple = oRMLRiderPremMultipleLK.getRML_PREM_MULT();
						break;
					}
				}else{
					if (oRMLRiderPremMultipleLK.getRML_AGE() == age
							&& oRMLRiderPremMultipleLK.getRML_TERM() == term
							&& sex.equalsIgnoreCase(oRMLRiderPremMultipleLK.getRML_SEX()))
					{
						CommonConstant.printLog("d", "#getRiderPremium Else Age :::: PBA Band ::: Sex ::: Term " + age + " : " + pbaBand + " : " + sex + " : " + term);
						riderPremMultiple = oRMLRiderPremMultipleLK.getRML_PREM_MULT();
						break;
					}
				}
			}
		}

		return riderPremMultiple;
	}

	public static long getRiderSumAssured(SISRequestBean reqBean, int cnt){
		long riderSumAssured = 0;
		RiderBO riderObj = (RiderBO)reqBean.getRiderlist().get(cnt);
		riderSumAssured = riderObj.getSumAssured();
		return riderSumAssured;
	}

	public static double getWpLoadValue(SISRequestBean reqBean, int cnt){

		double wpLoadValue = 1;
		RiderBO riderObj = (RiderBO)reqBean.getRiderlist().get(cnt);
		if(riderObj.getRtlID() == 12)
		{
			HashMap oclOccupationLkMap =  oDataMapperBO.getOCLOccupationLk();
			OCLOccupationLk oOCLOccupationLk = (OCLOccupationLk)oclOccupationLkMap.get(reqBean.getInsured_occupation());
			wpLoadValue = oOCLOccupationLk.getOCL_WP();

		}
		return wpLoadValue;
	}
	public static double getInsLoadValue(SISRequestBean reqBean){

		double insLoadValue;
			HashMap oclOccupationLkMap =  oDataMapperBO.getOCLOccupationLk();
			OCLOccupationLk oOCLOccupationLk = (OCLOccupationLk)oclOccupationLkMap.get(reqBean.getInsured_occupation());
			//insLoadValue = oOCLOccupationLk.getOCL_WP();
			insLoadValue = oOCLOccupationLk.getOCL_BLIFE();
		return insLoadValue;
	}
	public static double getModelFactor(SISRequestBean reqBean){
		double modelFactor;
		PNLPlanLk oPlanObj =  getPlanLKObject(reqBean.getBaseplan());
		String payMode = "O".equals(reqBean.getFrequency())? "A" : reqBean.getFrequency();
		modelFactor = DataAccessClass.getModelFactor(oPlanObj.getPNL_PGL_ID(), payMode);

		return modelFactor;
	}
	public static double getRiderModelFactor(SISRequestBean reqBean){
		double modelFactor=1;
		String payMode = "O".equals(reqBean.getFrequency())? "A" : reqBean.getFrequency();
		if (payMode.equals("A")) {
			modelFactor = 1;
		} else if (payMode.equals("S")) {
			modelFactor = 0.51;
		} else if (payMode.equals("Q")) {
			modelFactor = 0.26;
		} else if (payMode.equals("M")) {
			modelFactor = 0.0883;
		}
		return modelFactor;
	}
	public static double getCSVFactor(SISRequestBean reqBean, int cnt){
		double SVFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap SVFactorMap = getCashSurrenderValueFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(SVFactorMap.get(cnt)!=null){
			 SVFactor = Double.parseDouble(SVFactorMap.get(cnt).toString());
		}else{
		  	 SVFactor=0.0;
		}
		return SVFactor;
	}
	public static double getGSVFactor(SISRequestBean reqBean, int cnt){
		double SVFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap SVFactorMap = getGuranteedSurrenderValueFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(SVFactorMap.get(cnt)!=null){
			SVFactor = Double.parseDouble(SVFactorMap.get(cnt).toString());
			CommonConstant.printLog("d", "GSVFactor selected :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}else{
		  	 SVFactor=0.0;
		  	CommonConstant.printLog("d", "GSVFactor in else :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}
		return SVFactor;
	}
	public static double getPAChargeValue(SISRequestBean reqBean,long premium,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnl_id=oPNLPlanLk.getPNL_ID();
		if (reqBean.getFrequency().equals("M"))
			premium = reqBean.getBasepremiumannual();
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnl_id);
		double iPACCharges=0;//Changed by Samina for getting decimal charge value
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnl_id);
		ArrayList PACPremiumAllocation= oDataMapperBO.getPACPremiumAllocationCharges();
		PACPremiumAllocationCharges oPACPremiumAllocationCharges;
		//PAC is 0 for first year for tata employee
		if("Y".equalsIgnoreCase(reqBean.getTata_employee()) && cnt == 1){
			iPACCharges = 0.0;
		}else{
			for(int count = 0; count< PACPremiumAllocation.size(); count++)
			{
				oPACPremiumAllocationCharges=(PACPremiumAllocationCharges)PACPremiumAllocation.get(count);

				if(oPCRPlanChargesRef.getPCR_PREM_ID()==oPACPremiumAllocationCharges.getPAC_ID()
					&&	(cnt >= oPACPremiumAllocationCharges.getPAC_MIN_BAND()&& cnt<=oPACPremiumAllocationCharges.getPAC_MAX_BAND())
					&& (premium >= oPACPremiumAllocationCharges.getPAC_MIN_PREM()&& premium<=oPACPremiumAllocationCharges.getPAC_MAX_PREM()))
				{
					iPACCharges=(double)oPACPremiumAllocationCharges.getPAC_CHARGE();
					CommonConstant.printLog("d", "#Policy Term : " + cnt + " #Premium : " + premium + " #Premium Allocation Charges : " + iPACCharges);
					break;
				}

			}
		}

		return iPACCharges;
	}
	public static double getWOPCharges(SISRequestBean reqBean,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnl_id=oPNLPlanLk.getPNL_ID();
		int band = reqBean.getPolicyterm()- cnt;
		int age = reqBean.getInsured_age() + (cnt -1);
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnl_id);
		double dWOPCharges=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnl_id);
		ArrayList alWCMWOPChargesMaster= oDataMapperBO.getWCMWOPChargesMaster();
		WCMWOPChargesMaster oWCMWOPChargesMaster;
		for(int count = 0; count< alWCMWOPChargesMaster.size(); count++)
		{
			oWCMWOPChargesMaster=(WCMWOPChargesMaster)alWCMWOPChargesMaster.get(count);

			if(oPCRPlanChargesRef.getPCR_WOP_ID()==oWCMWOPChargesMaster.getWCM_WOP_ID()
				&&	band == oWCMWOPChargesMaster.getWCM_BAND()
				&& age == oWCMWOPChargesMaster.getWCM_AGE())
			{
				dWOPCharges=oWCMWOPChargesMaster.getWCM_CHARGE();
				CommonConstant.printLog("d", "#Policy Term : " + band + " #age : " + age + " #WOP Charges : " + dWOPCharges);
				break;
			}

		}
		return dWOPCharges;
	}

	public static double getFIBCharges(SISRequestBean reqBean,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnl_id=oPNLPlanLk.getPNL_ID();
		int band = reqBean.getPolicyterm()- cnt;
		int age = reqBean.getInsured_age() + (cnt -1);
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnl_id);
		double dFIBCharges=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnl_id);
		ArrayList alFCMFIBChargesMaster= oDataMapperBO.getFCMFIBChargesMaster();
		FCMFIBChargesMaster oFCMFIBChargesMaster;
		if(band > 10){
			band = 10;
		}
		for(int count = 0; count< alFCMFIBChargesMaster.size(); count++)
		{
			oFCMFIBChargesMaster=(FCMFIBChargesMaster)alFCMFIBChargesMaster.get(count);

			if(oPCRPlanChargesRef.getPCR_FIB_ID()==oFCMFIBChargesMaster.getFCM_FIB_ID()
				&&	band == oFCMFIBChargesMaster.getFCM_BAND()
				&& age == oFCMFIBChargesMaster.getFCM_AGE())
			{
				dFIBCharges=oFCMFIBChargesMaster.getFCM_CHARGE();
				CommonConstant.printLog("d", "#Policy Term : " + band + " #age : " + age + " #FIB Charges : " + dFIBCharges);
				break;
			}

		}
		return dFIBCharges;
	}

    public static double getMCRCharges(SISRequestBean reqBean, int term)
    {
        HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
        PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
        int pnlID=oPNLPlanLk.getPNL_ID();
        int age = reqBean.getInsured_age() + (term -1);
        String sex= reqBean.getInsured_sex();
        CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
        double dMCRCharges=0.0;
        PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
        ArrayList alMCMMortilityChargesMaster= oDataMapperBO.getMCMMortilityChargesMaster();
        MCMMortilityChargesMaster oMCMMortilityChargesMaster;
        for(int count = 0; count< alMCMMortilityChargesMaster.size(); count++)
        {
            oMCMMortilityChargesMaster=(MCMMortilityChargesMaster)alMCMMortilityChargesMaster.get(count);
            if ("U".equals(oMCMMortilityChargesMaster.getMCM_SEX())) {
                sex = "U";
            } else {
                sex = reqBean.getInsured_sex();
            }
            if(oPCRPlanChargesRef.getPCR_MORT_ID()==oMCMMortilityChargesMaster.getMCM_ID()
                    && age == oMCMMortilityChargesMaster.getMCM_AGE() && sex.equals(oMCMMortilityChargesMaster.getMCM_SEX()))
            {
                dMCRCharges=oMCMMortilityChargesMaster.getMCM_MORT_CHARGE();
                CommonConstant.printLog("d", " #age : " + age + " #MCR Charges : " + dMCRCharges);
                break;
            }

        }
        return dMCRCharges;
    }

    //Added for InvestOne NSAP mortality charges
    public static double getNSAPMCRCharges(SISRequestBean reqBean, int term)
    {
        HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
        PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
        int pnlID=oPNLPlanLk.getPNL_ID();
        int age = reqBean.getInsured_age() + (term -1);
        CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
        double dMCRCharges=0.0;
        double nsap=0.0;
        PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
        ArrayList alMCMMortilityChargesMaster= oDataMapperBO.getMCMMortilityChargesMaster();
        MCMMortilityChargesMaster oMCMMortilityChargesMaster;
        for(int count = 0; count< alMCMMortilityChargesMaster.size(); count++)
        {
            oMCMMortilityChargesMaster=(MCMMortilityChargesMaster)alMCMMortilityChargesMaster.get(count);

            if(oPCRPlanChargesRef.getPCR_MORT_ID()==oMCMMortilityChargesMaster.getMCM_ID()
                    && age == oMCMMortilityChargesMaster.getMCM_AGE())
            {
                dMCRCharges=oMCMMortilityChargesMaster.getMCM_MORT_CHARGE();
                if("N".equals(reqBean.getAgeProofFlag())){
                    nsap =2.5;
                    dMCRCharges+=nsap;
                }
                CommonConstant.printLog("d", " #age : " + age + " #MCR Charges : " + dMCRCharges);
                break;
            }

        }
        return dMCRCharges;
    }
	public static double getSurrenderChargeValue(SISRequestBean reqBean,long premium,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnlID=oPNLPlanLk.getPNL_ID();
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
		double dSURCharges=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
		ArrayList alSurrenderChargeMaster= oDataMapperBO.getSURSurrenderChargeMaster();
		SURSurrenderChargeMaster oSURSurrenderChargeMaster;
		for(int count = 0; count< alSurrenderChargeMaster.size(); count++)
		{
			oSURSurrenderChargeMaster=(SURSurrenderChargeMaster)alSurrenderChargeMaster.get(count);

			if(oPCRPlanChargesRef.getPCR_SUR_ID()==oSURSurrenderChargeMaster.getSUR_ID()
					&& (premium >= oSURSurrenderChargeMaster.getSUR_MIN_PREM()&& premium<=oSURSurrenderChargeMaster.getSUR_MAX_PREM())
					&&	(cnt >= oSURSurrenderChargeMaster.getSUR_MIN_BAND()&& cnt<=oSURSurrenderChargeMaster.getSUR_MAX_BAND()))
			{
				dSURCharges=oSURSurrenderChargeMaster.getSUR_CHARGE();
				CommonConstant.printLog("d", "#PT : " + cnt + "  #premium : " + premium + " #Surrender Charges : " + dSURCharges);
				break;
			}

		}
		return dSURCharges;
	}
	public static double getSurrenderChargeAmount(SISRequestBean reqBean,long premium,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnlID=oPNLPlanLk.getPNL_ID();
		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
		double dSURAmount=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
		ArrayList alSurrenderChargeMaster= oDataMapperBO.getSURSurrenderChargeMaster();
		SURSurrenderChargeMaster oSURSurrenderChargeMaster;
		for(int count = 0; count< alSurrenderChargeMaster.size(); count++)
		{
			oSURSurrenderChargeMaster=(SURSurrenderChargeMaster)alSurrenderChargeMaster.get(count);

			if(oPCRPlanChargesRef.getPCR_SUR_ID()==oSURSurrenderChargeMaster.getSUR_ID()
					&& (premium >= oSURSurrenderChargeMaster.getSUR_MIN_PREM()&& premium<=oSURSurrenderChargeMaster.getSUR_MAX_PREM())
					&&	(cnt >= oSURSurrenderChargeMaster.getSUR_MIN_BAND()&& cnt<=oSURSurrenderChargeMaster.getSUR_MAX_BAND()))
			{
				dSURAmount=oSURSurrenderChargeMaster.getSUR_AMOUNT();
				CommonConstant.printLog("d", "#PT : " + cnt + " #premium : " + premium + " #Surrender Amount : " + dSURAmount);
				break;
			}

		}
		return dSURAmount;
	}
	public static double getGuaranteedMaturityBonus(SISRequestBean reqBean,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnlID=oPNLPlanLk.getPNL_ID();

		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
		double dGURMaturityBonus=0.0;
		PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
		ArrayList alGuaranteedMaturityBonusMaster= oDataMapperBO.getGuaranteedMaturityBonusMaster();
		GuaranteedMaturityBonusMaster oGuaranteedMaturityBonusMaster;
		for(int count = 0; count< alGuaranteedMaturityBonusMaster.size(); count++)
		{
			oGuaranteedMaturityBonusMaster=(GuaranteedMaturityBonusMaster)alGuaranteedMaturityBonusMaster.get(count);
			CommonConstant.printLog("d", "#PT : " + cnt
					+ " #Gaurantted Maturity ID : " + oGuaranteedMaturityBonusMaster.getGMM_ID()
					+ " #Gaurantted Maturity Min Band : " + oGuaranteedMaturityBonusMaster.getGMM_MIN_BAND()
					+ " #Gaurantted Maturity Max Band : " + oGuaranteedMaturityBonusMaster.getGMM_MAX_BAND()
					+ " #Gaurantted Maturity Bonus : " + oGuaranteedMaturityBonusMaster.getGMM_BONUS());
			if(oPCRPlanChargesRef.getPCR_GMM_ID()==oGuaranteedMaturityBonusMaster.getGMM_ID()
					&&	(cnt >= oGuaranteedMaturityBonusMaster.getGMM_MIN_BAND()&& cnt<=oGuaranteedMaturityBonusMaster.getGMM_MAX_BAND()))
			{
				dGURMaturityBonus=oGuaranteedMaturityBonusMaster.getGMM_BONUS();
				CommonConstant.printLog("d", "#PT : " + cnt + " #Gaurantted Maturity Bonus : " + dGURMaturityBonus);
				break;
			}

		}
		return dGURMaturityBonus;
	}

	public static double getFMC(SISRequestBean reqBean){
		 double fundChargeFactor = 0.0;
		 FundDetailsLK fundBO = null;
		 ArrayList fundList =  reqBean.getFundList();
		 for(int i=0; i< fundList.size(); i++){
			 fundBO = (FundDetailsLK)fundList.get(i);
			 fundChargeFactor = fundChargeFactor +  (fundBO.getPERCENTAGE() * fundBO.getFUND_MGMT_CHARGE());
		 }

		 return (fundChargeFactor/100);
	 }
	public static PGFPlanGroupFeature getPlanGroupFeature(String planCode)
	{
		PGFPlanGroupFeature oPGFPlanGroupFeature = null;
		HashMap oPlanFeatureHashmap = oDataMapperBO.getPGFPlanGroupFeature();
		oPGFPlanGroupFeature = (PGFPlanGroupFeature)oPlanFeatureHashmap.get(planCode);

		return oPGFPlanGroupFeature;
	}
	public static AddressBO getAddressBO(){

		ArrayList alist=oDataMapperBO.getAddress();
		AddressBO oAddressBO=(AddressBO)alist.get(0);

		return oAddressBO;

	}
	public static String getTempImagePath(String basePlan){
		PNLPlanLk oPlanObj =  getPlanLKObject(basePlan);
		String imgPath = "";
		TempImagePath oTempImagePath = null;
		HashMap oTempImagePathMap=oDataMapperBO.getTempImgPath();
		oTempImagePath=(TempImagePath)oTempImagePathMap.get(oPlanObj.getPNL_PGL_ID());
		if (oTempImagePath!=null) {
			imgPath = oTempImagePath.getTip_image_name()!=null?oTempImagePath.getTip_image_name():"";
		}
		return imgPath;
	}
	public static String getPlanType(String planCode) {

		PNLPlanLk oPlanObj = DataAccessClass.getPlanLKObject(planCode);
		HashMap pglPlanGrpHashMap = oDataMapperBO.getPGLPlanGroupLk();

		PGLPlanGroupLk oPGLPlanGroupLk = (PGLPlanGroupLk) pglPlanGrpHashMap.get(oPlanObj.getPNL_PGL_ID());
		return oPGLPlanGroupLk.getPGL_PRODUCT_TYPE();
	}
	public static double getCommissionPercentage(SISRequestBean reqBean,int cnt)
	{
		HashMap PCRPlanCharges = oDataMapperBO.getPCRPlanChargesRef();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int pnlID=oPNLPlanLk.getPNL_ID();

		CommonConstant.printLog("d", "Plan Group ID:: PNL_ID :::::" + pnlID);
		double dCommissionPercent=0.0;
		if("Y".equalsIgnoreCase(reqBean.getTata_employee()) || !"Payable".equalsIgnoreCase(reqBean.getCommission())){
			dCommissionPercent = 0;
		}else{

			PCRPlanChargesRef oPCRPlanChargesRef =(PCRPlanChargesRef)PCRPlanCharges.get(pnlID);
			ArrayList alCommissionPercentage= oDataMapperBO.getCommissionChargeMaster();
			CCMCommissionChargeMaster oCCMCommissionChargeMaster;

			for(int count = 0; count< alCommissionPercentage.size(); count++)
			{
				oCCMCommissionChargeMaster = (CCMCommissionChargeMaster)alCommissionPercentage.get(count);
				CommonConstant.printLog("d", "#PT : " + cnt
						+ " #oCCMCommissionChargeMaster ID : " + oCCMCommissionChargeMaster.getCCM_ID()
						+ " #oCCMCommissionChargeMaster Min Band : " + oCCMCommissionChargeMaster.getCCM_MIN_TERM()
						+ " #oCCMCommissionChargeMaster Max Band : " + oCCMCommissionChargeMaster.getCCM_MAX_TERM()
						+ " #oCCMCommissionChargeMaster Percent : " + oCCMCommissionChargeMaster.getCCM_COMMISSION());
				if(oPCRPlanChargesRef.getPCR_COMM_ID()==oCCMCommissionChargeMaster.getCCM_ID()
						&&	(cnt >= oCCMCommissionChargeMaster.getCCM_MIN_TERM() && cnt<=oCCMCommissionChargeMaster.getCCM_MAX_TERM()))
				{
					dCommissionPercent=oCCMCommissionChargeMaster.getCCM_COMMISSION();
					CommonConstant.printLog("d", "#PT : " + cnt + " #oCCMCommissionChargeMaster Percent : " + oCCMCommissionChargeMaster.getCCM_COMMISSION());
					break;
				}
			}
		}

		return dCommissionPercent;
	}
	//GET Maturity Value Factor
	public static double getMVFactor(SISRequestBean reqBean){

		CommonConstant.printLog("d", "Inside getMVFactor:::::");
		double MVFactor = 0.0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		ArrayList maturityLst = oDataMapperBO.getMFMMaturityFactorMaster();
		for(int i=0; i< maturityLst.size(); i++){
			MFMMaturityFactorMaster oMFMMaturityFactorMaster = (MFMMaturityFactorMaster)maturityLst.get(i) ;
			if(oPNLPlanLk.getPNL_ID() == oMFMMaturityFactorMaster.getMFF_PNL_ID()){
				if("A".equals(oMFMMaturityFactorMaster.getMFM_USE_FLG()) &&
						oMFMMaturityFactorMaster.getMFM_AGE() == reqBean.getInsured_age()){
					MVFactor = oMFMMaturityFactorMaster.getMFM_VALUE();
					CommonConstant.printLog("d", "Inside getMVFactor AGE:::::MVFactor : " + MVFactor);
					break;
				}else if("T".equals(oMFMMaturityFactorMaster.getMFM_USE_FLG()) &&
						oMFMMaturityFactorMaster.getMFM_BAND() == reqBean.getPolicyterm()){
					MVFactor = oMFMMaturityFactorMaster.getMFM_VALUE();
					CommonConstant.printLog("d", "Inside getMVFactor TERM:::::MVFactor : " + MVFactor);
					break;
				}
				else if("S".equals(oMFMMaturityFactorMaster.getMFM_USE_FLG()) &&
						oMFMMaturityFactorMaster.getMFM_BAND() == reqBean.getPolicyterm() &&
						oMFMMaturityFactorMaster.getMFM_AGE() == reqBean.getInsured_age() &&
						oMFMMaturityFactorMaster.getMFM_SEX().equals(reqBean.getInsured_sex())){
					MVFactor = oMFMMaturityFactorMaster.getMFM_VALUE();
					CommonConstant.printLog("d", "Inside getMVFactor Age, Term and Sex:::::MVFactor : " + MVFactor);
					break;
				}
			}
		}
		return MVFactor;
	}
	public static long getDiscountedPremium(FormulaBean fBean){
		long discountedPremium = 0;
		double discPrem = 0;
			FormulaHandler formulaHandler = new FormulaHandler();
			String discountedPremiumValue = formulaHandler.evaluateFormula(fBean.getRequestBean().getBaseplan(),
																FormulaConstant.CALCULATE_DISCOUNTED_PREMIUM, fBean)+ "";
			discPrem = Double.parseDouble(discountedPremiumValue);
			//discountedPremium = Long.parseLong(discountedPremiumValue);
			discountedPremium = Math.round(discPrem);
			CommonConstant.printLog("d", " discountedPremium " + discountedPremium);
		return discountedPremium;
	}

	public static long getTotalPremium(FormulaBean fBean){
		long totalPremium = 0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();
		int count = 0;
		if(fBean.getCNT() > ppt){
			count = ppt;
		}else{
			count = fBean.getCNT();
		}
		if(fBean.isCNT_MINUS()){
			count=count+1;
		}
		/*String strParamValue = DataAccessClass.getSPVSysParamValues(reqBean.getBaseplan());
		if("Y".equals(reqBean.getTata_employee()) && strParamValue.contains("CALDP")){
			//discountedPremium = getDiscountedPremium(fBean);
			discountedPremium = fBean.getDP();
			totalPremium = discountedPremium + (fBean.getQAP() * (count -1));
		}else{
			totalPremium = fBean.getQAP() * count;
		}*/
		//totalPremium = fBean.getP() * count;


		if(reqBean.getBaseplan().equalsIgnoreCase("IRSL10N1V2") ||reqBean.getBaseplan().equalsIgnoreCase("IRSL51N1V2") || reqBean.getBaseplan().equalsIgnoreCase("IRSRP1N1V2") || reqBean.getBaseplan().equalsIgnoreCase("IRSSP1N1V2")){

			totalPremium = fBean.getQAP() * count;
		}
		else{
			totalPremium = fBean.getP() * count;
		}

		return totalPremium;
	}

	public static long getTotalQAPPremium(FormulaBean fBean){
		long totalPremium = 0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();
		int count = 0;
		if(fBean.getCNT() > ppt){
			count = ppt;
		}else{
			count = fBean.getCNT();
		}
		if(fBean.isCNT_MINUS()){
			count=count+1;
		}
		/*String strParamValue = DataAccessClass.getSPVSysParamValues(reqBean.getBaseplan());
		if("Y".equals(reqBean.getTata_employee()) && strParamValue.contains("CALDP")){
			//discountedPremium = getDiscountedPremium(fBean);
			discountedPremium = fBean.getDP();
			totalPremium = discountedPremium + (fBean.getQAP() * (count -1));
		}else{
			totalPremium = fBean.getQAP() * count;
		}*/
		totalPremium = fBean.getQAP() * count;
		return totalPremium;
	}

	public static long getTotalAnnualPremium(FormulaBean fBean){
		long totalPremium = 0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();
		int count = 0;
		if(fBean.getCNT() > ppt){
			count = ppt;
		}else{
			count = fBean.getCNT();
		}
		if(fBean.isCNT_MINUS()){
			count=count+1;
		}
		/*String strParamValue = DataAccessClass.getSPVSysParamValues(reqBean.getBaseplan());
		if("Y".equals(reqBean.getTata_employee()) && strParamValue.contains("CALDP")){
			//discountedPremium = getDiscountedPremium(fBean);
			discountedPremium = fBean.getDP();
			totalPremium = discountedPremium + (fBean.getQAP() * (count -1));
		}else{
			totalPremium = fBean.getQAP() * count;
		}*/
		totalPremium = fBean.getAP() * count;
		return totalPremium;
	}

	public static long getALTTotalPremium(FormulaBean fBean){
		long totalPremium = 0;
		long finalTotalPremium=0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();
		int rpuy=Integer.parseInt(reqBean.getRpu_year());

			for(int i=1;i<=fBean.getCNT();i++)
			{
				if(i<=ppt && i<rpuy){
				totalPremium = fBean.getP();
				finalTotalPremium+=totalPremium;
           CommonConstant.printLog("d", "finalTotalPremium" + finalTotalPremium);
				}
}
			return finalTotalPremium;
	}
	public static long getEB_TotalPremium(FormulaBean fBean){
		long totalPremium = 0;
		long finalTotalPremium=0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();


			for(int i=1;i<=fBean.getCNT();i++)
			{
				if(i<=ppt){
				totalPremium = fBean.getP();
				finalTotalPremium+=totalPremium;
           CommonConstant.printLog("d", "finalTotalPremium" + finalTotalPremium);
				}
}
			return finalTotalPremium;
	}

	public static long getTotalPremiumULIP(FormulaBean fBean){
		long totalPremium = 0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int ppt = reqBean.getPremiumpayingterm();
		int count = 0;
		int monthCount = fBean.getMonthCount();
		if(fBean.getCNT() > ppt){
			count = ppt;
		}else{
			if (reqBean.getFrequency().equals("A") || reqBean.getFrequency().equals("O"))
				count = fBean.getCNT();
			else if (reqBean.getFrequency().equals("S") || reqBean.getFrequency().equals("Q") || reqBean.getFrequency().equals("M"))
				count = fBean.getCNT()-1;
		}
		totalPremium = fBean.getP() * count;
		if (reqBean.getFrequency().equals("S") || reqBean.getFrequency().equals("Q") || reqBean.getFrequency().equals("M")) {
			if (fBean.getCNT() <= ppt) {
				if (monthCount != 12) {
					if (reqBean.getFrequency().equals("S")) {
						monthCount = (monthCount-1) / 6 + 1;
						totalPremium = totalPremium + (fBean.getP() / 2 * monthCount);
					} else if (reqBean.getFrequency().equals("Q")) {
						monthCount = (monthCount-1) / 3 + 1;
						totalPremium = totalPremium + (fBean.getP() / 4 * monthCount);
					} else if (reqBean.getFrequency().equals("M")) {
						totalPremium = totalPremium + (fBean.getP() / 12 * monthCount);
					}
				} else {
					totalPremium += fBean.getP();
				}
			}
		}
		return totalPremium;
	}

	public static double getTotalRemainingPremium(FormulaBean fBean){//Added by Samina for total policy premium
		double totalRemainingPremium = 0.0;
		SISRequestBean reqBean = fBean.getRequestBean();
		int age = reqBean.getInsured_age();
		if (fBean.getA()==240)
			age = reqBean.getProposer_age();
		int ppt = reqBean.getPremiumpayingterm();
		if (age+ppt >= 65)
			ppt=65-age;
		int cnt = fBean.getCNT();
		int monthCount = fBean.getMonthCount();
		double pendingPremiumOtherMode = 0.0;
		totalRemainingPremium = fBean.getP() * (ppt-cnt);
		if (reqBean.getFrequency().equals("M")) {
			pendingPremiumOtherMode = ((12-monthCount) * Math.round(fBean.getP()/12));
		} else if (reqBean.getFrequency().equals("Q")) {
			int monthRemaining = (12-monthCount)/3;
			pendingPremiumOtherMode = monthRemaining * Math.round(fBean.getP()/4);
		}
		else if (reqBean.getFrequency().equals("S")) {
			int monthRemaining = (12-monthCount)/6;
			pendingPremiumOtherMode = monthRemaining * Math.round(fBean.getP()/2);
		}
		totalRemainingPremium+=pendingPremiumOtherMode;
		CommonConstant.printLog("d", "pendingPremiumOtherMode " + pendingPremiumOtherMode);
		CommonConstant.printLog("d", "Total Remaining Premium " + totalRemainingPremium);
		return totalRemainingPremium;
	}
	public static double getGAIFactor(FormulaBean fBean){
		double gaiFactor = 0.0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(fBean.getRequestBean().getBaseplan());
		int pt = fBean.getRequestBean().getPolicyterm();
		long premium = fBean.getP();
		long coverage = fBean.getRequestBean().getSumassured();
		ArrayList gaiLst = oDataMapperBO.getGAIGuarAnnIncChMaster();
		for(int i=0; i< gaiLst.size(); i++){
			GAIGuarAnnIncChMaster oGAIGuarAnnIncChMaster = (GAIGuarAnnIncChMaster)gaiLst.get(i) ;
			if("P".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG())){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& premium >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && premium <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "gaiFactor by premium ::: " + gaiFactor);
						break;
					}
			}else if("T".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG())){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  fBean.getCNT() >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && fBean.getCNT() <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "gaiFactor by policy Year ::: " + gaiFactor + "\t" + fBean.getCNT());
						break;
					}
			}else if("PT".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG())){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "gaiFactor by Term ::: " + gaiFactor);
						break;
					}
				//Added for Gold Plus Mohammad Rashid 06-Jan-2014
			} else if("CT".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG())){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  fBean.getCNT() >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && fBean.getCNT() <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&& coverage >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && coverage <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "gaiFactor by Policy Year and Sum assured for Year ::: " + fBean.getCNT() + " is " + gaiFactor);
						break;
				}
				// End
			} else if("TP".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG())){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  fBean.getCNT() >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && fBean.getCNT() <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&& premium >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && premium <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "gaiFactor by Policy Year and Premium for Year ::: " + fBean.getCNT() + " is " + gaiFactor);
					break;
				}
				// End
			}

		}
		CommonConstant.printLog("d", "gaiFactor ::: " + gaiFactor);
		return gaiFactor;

	}
	public static double getTotalGAI(FormulaBean fBean){
		double TotalGAIValue = 0;

			FormulaHandler formulaHandler = new FormulaHandler();
			int counter = fBean.getCNT();
			CommonConstant.printLog("d", "getTotalGAI ::::::::Counter " + fBean.getCNT());

			for(int i=1; i<=counter; i++){
				double GAIValue = 0;
				if(i> fBean.getRequestBean().getPremiumpayingterm()){
					fBean.setCNT(i);
					String GAIValueValueStr = formulaHandler.evaluateFormula(fBean.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME_F, fBean)+ "";
					GAIValue = Double.parseDouble(GAIValueValueStr);
					CommonConstant.printLog("d", "GAIValue :::::::: " + GAIValue);
				}
				TotalGAIValue = TotalGAIValue + GAIValue;
			}
			CommonConstant.printLog("d", "TotalGAIValue :::::::: " + TotalGAIValue);
		return TotalGAIValue;
	}

	/** Vikas - Added for Mahalife Gold & Gold Plus */
	public static double getTotalGAIMLG(FormulaBean fBean){
		double TotalGAIValue = 0;

			FormulaHandler formulaHandler = new FormulaHandler();
			int counter = fBean.getCNT();
			CommonConstant.printLog("d", "getTotalGAI ::::::::Counter " + fBean.getCNT());

			for(int i=1; i<counter; i++){
				double GAIValue = 0;
					fBean.setCNT(i);
					String GAIValueValueStr = formulaHandler.evaluateFormula(fBean.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME, fBean)+ "";
					GAIValue = Double.parseDouble(GAIValueValueStr);
					CommonConstant.printLog("d", "GAIValue :::::::: " + GAIValue);
				TotalGAIValue = TotalGAIValue + GAIValue;
			}
			fBean.setCNT(counter);
			CommonConstant.printLog("d", "TotalGAIValue :::::::: " + TotalGAIValue);
		return TotalGAIValue;
	}
	/** Vikas - Added for Mahalife Gold & Gold Plus */

	public static double getTotalGAIMBP(FormulaBean fBean){
		double TotalGAIValue = 0;
		FormulaHandler formulaHandler = new FormulaHandler();
		int counter = fBean.getRequestBean().getPolicyterm();
		CommonConstant.printLog("d", "getTotalGAI MBP ::::::::Counter " + fBean.getCNT());
		int iCounter = fBean.getCNT();
		if (iCounter<=counter) {
			for(int i=1; i<=counter; i++){
				double GAIValue = 0;
				fBean.setCNT(i);
				String GAIValueValueStr = formulaHandler.evaluateFormula(fBean.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME, fBean)+ "";
				GAIValue = Double.parseDouble(GAIValueValueStr);
				CommonConstant.printLog("d", "GAIValue MBP:::::::: " + GAIValue);
				TotalGAIValue = TotalGAIValue + GAIValue;
			}
		}
		fBean.setCNT(iCounter);
		CommonConstant.printLog("d", "TotalGAIValue MBP:::::::: " + TotalGAIValue);
		return TotalGAIValue;
	}

	public static String getSPVSysParamValues(String planCode)
	{
		String  strParamValue = "";
		HashMap hmSPVSysParamValues = oDataMapperBO.getSPVSysParamValues();
		CommonConstant.printLog("d", "planCode in getSPVSysParamValues:::::::::" + planCode + ":");
		SPVSysParamValues oSPVSysParamValues = (SPVSysParamValues)hmSPVSysParamValues.get(planCode.trim());
		strParamValue=oSPVSysParamValues.getSPV_VALUE();
		return strParamValue;
	}

	public static long getMinimumPremium(String planCode,String payMode)
	{

		long iAnual_min_prem=0;
		ArrayList alMinPremium = oDataMapperBO.getPNLMinPremLK();

		for(int i=0; i< alMinPremium.size(); i++){
			PNLMinPremLK oPNLMinPremLK = (PNLMinPremLK)alMinPremium.get(i) ;
			if(oPNLMinPremLK.getPML_PLAN_CD().equalsIgnoreCase(planCode)
				&& oPNLMinPremLK.getPML_PREMIUM_MODE().equalsIgnoreCase(payMode))
			{
				iAnual_min_prem = oPNLMinPremLK.getPML_MIN_PREM_AMT();
				break;
			}
		}
		return iAnual_min_prem;
	}

	public static double getRBTBBonus(FormulaBean fBean,String bonusType, int polYear){
		double gaiFactor = 0.0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(fBean.getRequestBean().getBaseplan());
		int pt = fBean.getRequestBean().getPolicyterm();
		int age=fBean.getRequestBean().getInsured_age();
		int count=fBean.getCNT();
		long coverage = fBean.getRequestBean().getSumassured();		//added by jayesh
		long premium=fBean.getRequestBean().getBasepremiumannual();
		int rpuy=fBean.getRequestBean().getRpu_year()!=null?Integer.parseInt(fBean.getRequestBean().getRpu_year()):0;
		CommonConstant.printLog("d", "coverage :  " + coverage);
		ArrayList gaiLst = oDataMapperBO.getGAIGuarAnnIncChMaster();
		for(int i=0; i< gaiLst.size(); i++){
			GAIGuarAnnIncChMaster oGAIGuarAnnIncChMaster = (GAIGuarAnnIncChMaster)gaiLst.get(i) ;
			if("RB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
				       CommonConstant.printLog("d", "RB4 value :  " + gaiFactor);
						break;
					}
			}else if("RB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RB8 value :  " + gaiFactor);
						break;
					}
			}
			if("RB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
				       CommonConstant.printLog("d", "RPRB4 value :  " + gaiFactor);
						break;
					}
			}else if("RB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RPRB8 value :  " + gaiFactor);
						break;
					}
			}
			if("TB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPTB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy && count>=12) {
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
				       CommonConstant.printLog("d", "RPTB4 value :  " + gaiFactor);
						break;
					}
			}else if("TB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPTB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy  && count>=12){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RPTB8 value :  " + gaiFactor);
						break;
					}
			}
			if("RB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB4_MM".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RPRB4_MM value :  " + gaiFactor);
						break;
					}
			}else if("RB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB8_MM".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RPRB8_MM value :  " + gaiFactor);
						break;
					}
			}
			else if("EBBONUS".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "EBBONUS".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "EBBONUS value :  " + gaiFactor);
						break;
					}
			}
			else if("TB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB4".equalsIgnoreCase(bonusType) ){
				CommonConstant.printLog("d", "getRBTBBonus TB4 policyterm : " + pt + " oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() : " + oGAIGuarAnnIncChMaster.getGAI_MIN_TERM()
						+ " oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	 : " + oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() + " Count : " + count + " polYear: " + polYear);
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	&& count>= polYear){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "TB4 value :  " + gaiFactor);
						break;
					}else if(count < polYear){
						CommonConstant.printLog("d", "else TB4 value :  " + 0.0);
						break;
					}
			}
			else if("TB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB8".equalsIgnoreCase(bonusType) ){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	&& count>= polYear){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "TB8 value :  " + gaiFactor);
						break;
					}else if(count < polYear){
						CommonConstant.printLog("d", "else TB8 value :  " + 0.0);
						break;
					}
			}	else if("LB".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "LB".equalsIgnoreCase(bonusType)){
					if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
							&& polYear >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && polYear <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
							gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					       CommonConstant.printLog("d", "LB value :  " + gaiFactor);
							break;
						}
				// For Mahalife Gold and Gold Plus Mohammad Rashid
			} else if("LB".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "LBIO".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& polYear >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && polYear <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						if (polYear == pt)
							gaiFactor +=0.046;
				       CommonConstant.printLog("d", "LB Invest One value :  " + gaiFactor);
						break;
					}
			}else if("LB".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "LBSA".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& pt == oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt == oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	&& polYear==pt){
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "LB value :  " + gaiFactor);
					break;
				}

			} else if("RBG4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
				       CommonConstant.printLog("d", "RBG4 value :  " + gaiFactor);
						break;
					}
			}else if("RBG8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()	){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RBG8 value :  " + gaiFactor);
						break;
					}
			}
			if("RBG4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB4_GP".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy ){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
				       CommonConstant.printLog("d", "RPRB4 value :  " + gaiFactor);
						break;
					}
			}else if("RBG8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RPRB8_GP".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM() && count<rpuy ){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RPRB8 value :  " + gaiFactor);
						break;
					}
			}
			// For Mahalife Gold and Gold Plus Mohammad Rashid End
			//start : added by jayesh for Smart 7 : 30-04-2014
			else if("RBS4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&& coverage >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && coverage <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RBS4 value : " + gaiFactor);
						break;
				}
			}
			else if("RBS8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&& coverage >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && coverage <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RBS8 value : " + gaiFactor);
						break;
				}
			} else if("RBIW8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB8".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& coverage >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && coverage <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RBIW8 value : " + gaiFactor);
						break;
				}
			}
			else if("RBIW4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "RB4".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&& coverage >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && coverage <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()){
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "RBIW4 value : " + gaiFactor);
						break;
				}
			}
			else if("EBTB".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "EBTB".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()) {
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "EBTB value : " + gaiFactor);
						break;
				}
			} else if("TB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB8SGP".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()) {
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "TB8SPG value : " + gaiFactor);
						break;
				}
			} else if("TB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB4SGP".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()) {
						gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
						CommonConstant.printLog("d", "TB4SPG value : " + gaiFactor);
						break;
				}
			} else if("MF_M".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "MFV_M".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  age >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && age <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&&  premium >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && premium <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()) {
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "MF_M value : " + gaiFactor);
					break;
				}
			} else if("MF_F".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "MFV_F".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  age >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && age <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()
						&&  premium >= oGAIGuarAnnIncChMaster.getGAI_MIN_PREM() && premium <= oGAIGuarAnnIncChMaster.getGAI_MAX_PREM()) {
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "MF_F value : " + gaiFactor);
					break;
				}
			} else if("TB8".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB8GK".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_PPT() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_PPT()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()) {
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "MF_M value : " + gaiFactor);
					break;
				}
			}
			else if("TB4".equalsIgnoreCase(oGAIGuarAnnIncChMaster.getGAI_FLAG()) && "TB4GK".equalsIgnoreCase(bonusType)){
				if(oPNLPlanLk.getPNL_ID() == oGAIGuarAnnIncChMaster.getGAI_PNL_ID()
						&&  count >= oGAIGuarAnnIncChMaster.getGAI_MIN_PPT() && count <= oGAIGuarAnnIncChMaster.getGAI_MAX_PPT()
						&& pt >= oGAIGuarAnnIncChMaster.getGAI_MIN_TERM() && pt <= oGAIGuarAnnIncChMaster.getGAI_MAX_TERM()) {
					gaiFactor = oGAIGuarAnnIncChMaster.getGAI_CHARGE();
					CommonConstant.printLog("d", "MF_M value : " + gaiFactor);
					break;
				}
			}
			//end : added by jayesh for Smart 7 : 30-04-2014

		}
		return gaiFactor;

	}

	public static double getBSVFactor(SISRequestBean reqBean, int cnt){
		double SVFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap SVFactorMap = getBSFVSurrenderValueFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(SVFactorMap.get(cnt)!=null){
			 SVFactor = Double.parseDouble(SVFactorMap.get(cnt).toString());
		}else{
		  	 SVFactor=0.0;
		}
		return SVFactor;

	}

	public static HashMap getBSFVSurrenderValueFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "SV Factor for PLan ID::::: " + planId + " term: " + term);
		HashMap RBFactorMap = new HashMap();
		ArrayList PMVMaturityValueLst = oDataMapperBO.getPMVPlanMaturityValue();
		for(int count = 0; count < PMVMaturityValueLst.size(); count++){
			PMVPlanMaturityValue oPMVMaturityValue =  (PMVPlanMaturityValue)PMVMaturityValueLst.get(count);

			if("RB".equals(oPMVMaturityValue.getPMV_BAND())){
				if(planId == oPMVMaturityValue.getPMV_PNL_ID()
						&& oPMVMaturityValue.getPMV_AGE() == term){
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR1(), oPMVMaturityValue.getPMV_CVAL1());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR2(), oPMVMaturityValue.getPMV_CVAL2());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR3(), oPMVMaturityValue.getPMV_CVAL3());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR4(), oPMVMaturityValue.getPMV_CVAL4());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR5(), oPMVMaturityValue.getPMV_CVAL5());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR6(), oPMVMaturityValue.getPMV_CVAL6());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR7(), oPMVMaturityValue.getPMV_CVAL7());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR8(), oPMVMaturityValue.getPMV_CVAL8());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR9(), oPMVMaturityValue.getPMV_CVAL9());
					RBFactorMap.put(oPMVMaturityValue.getPMV_DUR10(), oPMVMaturityValue.getPMV_CVAL10());
					CommonConstant.printLog("d", "RB Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "RB Factor PMV_DUR1 " + oPMVMaturityValue.getPMV_DUR1() + " : " + oPMVMaturityValue.getPMV_CVAL1());
					CommonConstant.printLog("d", "RB Factor PMV_DUR2 " + oPMVMaturityValue.getPMV_DUR2() + " : " + oPMVMaturityValue.getPMV_CVAL2());
					CommonConstant.printLog("d", "RB Factor PMV_DUR3 " + oPMVMaturityValue.getPMV_DUR3() + " : " + oPMVMaturityValue.getPMV_CVAL3());
					CommonConstant.printLog("d", "RB Factor PMV_DUR4 " + oPMVMaturityValue.getPMV_DUR4() + " : " + oPMVMaturityValue.getPMV_CVAL4());
					CommonConstant.printLog("d", "RB Factor PMV_DUR5 " + oPMVMaturityValue.getPMV_DUR5() + " : " + oPMVMaturityValue.getPMV_CVAL5());
					CommonConstant.printLog("d", "RB Factor PMV_DUR6 " + oPMVMaturityValue.getPMV_DUR6() + " : " + oPMVMaturityValue.getPMV_CVAL6());
					CommonConstant.printLog("d", "RB Factor PMV_DUR7 " + oPMVMaturityValue.getPMV_DUR7() + " : " + oPMVMaturityValue.getPMV_CVAL7());
					CommonConstant.printLog("d", "RB Factor PMV_DUR8 " + oPMVMaturityValue.getPMV_DUR8() + " : " + oPMVMaturityValue.getPMV_CVAL8());
					CommonConstant.printLog("d", "RB Factor PMV_DUR9 " + oPMVMaturityValue.getPMV_DUR9() + " : " + oPMVMaturityValue.getPMV_CVAL9());
					CommonConstant.printLog("d", "RB Factor PMV_DUR10 " + oPMVMaturityValue.getPMV_DUR10() + " : " + oPMVMaturityValue.getPMV_CVAL10());
				}
			}
		}


		return RBFactorMap;
	}


	// For Gold and Gold Plus Mohammad Rashid
	public static double getIPCFactor(SISRequestBean reqBean, int cnt){
		double IPCFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap IPCFactorMap = getInflationProtectionCoverFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(IPCFactorMap.get(cnt)!=null){
			IPCFactor = Double.parseDouble(IPCFactorMap.get(cnt).toString());
			 CommonConstant.printLog("d", "IPCFactor selected :::: " + IPCFactor + " For Policy Year :::: " + cnt);
		}else{
			IPCFactor=0.0;
		  	CommonConstant.printLog("d", "IPCFactor in else :::: " + IPCFactor + " For Policy Year :::: " + cnt);
		}
		return IPCFactor;
	}

	public static HashMap getInflationProtectionCoverFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "Inflation Protection Cover Value Factor for Plan ID::::: " + planId + " term: " + term);
		HashMap IPCFactorMap = new HashMap();
		ArrayList IPCValueLst = oDataMapperBO.getIPCValues();
		for(int count = 0; count < IPCValueLst.size(); count++){
			IPCValues oIPCValues =  (IPCValues)IPCValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());
				if(planId == oIPCValues.getIPC_PNL_ID() ){
					IPCFactorMap.put(oIPCValues.getIPC_AGE(), oIPCValues.getIPC_PREM_MULT());
					CommonConstant.printLog("d", "IPC Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "IPC Factor Policy Year " + oIPCValues.getIPC_AGE() + " : " + oIPCValues.getIPC_PREM_MULT());
				}
		}


		return IPCFactorMap;
	}
	// For Gold and Gold Plus Mohammad Rashid End

	public static double getRLCF(FormulaBean fBean,SISRequestBean reqBean,int cnt) {
		double RLCFactor;
		HashMap RLCFactorMap = new HashMap();
		RLCFactorMap = getRLCFactor(fBean,reqBean,cnt);
		int factorCount = cnt;
		CommonConstant.printLog("d", "FactorCount 1 " + factorCount);
		int age  = reqBean.getInsured_age()+cnt-1;
		CommonConstant.printLog("d", "age 1 " + age);
		if (fBean.getA()==240)
			age = reqBean.getProposer_age()+cnt-1;
		CommonConstant.printLog("d", "age 2 " + age);
		//CommonConstant.printLog("d","RDL is "+fBean.getA()+" condition checks fBean.getA()==239 "+(fBean.getA()==239)+" fBean.getA() == 240 "+(fBean.getA() == 240));
		if (fBean.getA()==240 || fBean.getA() == 239) {
			factorCount = reqBean.getPremiumpayingterm()+1-cnt<65-age?reqBean.getPremiumpayingterm()+1-cnt:65-age;
		}
		CommonConstant.printLog("d", "FactorCount 2 " + factorCount);
		if (fBean.getA()==242 || fBean.getA() == 241) {
			factorCount = 1;
		}
		CommonConstant.printLog("d", "FactorCount 3 " + factorCount);
		if(RLCFactorMap.get(factorCount)!=null){
			RLCFactor = Double.parseDouble(RLCFactorMap.get(factorCount).toString());
			 CommonConstant.printLog("d", "RLCFactor selected :::: " + RLCFactor + " For Factor :::: " + factorCount);
		}else{
			RLCFactor=0.0;
		  	CommonConstant.printLog("d", "RLCFactor in else :::: " + RLCFactor + " For Policy Year :::: " + cnt);
		}
		return RLCFactor;
	}

	public static HashMap getRLCFactor(FormulaBean fBean,SISRequestBean reqBean,int cnt) {

		HashMap RLCFactorMap = new HashMap();
		ArrayList RLCValueLst = oDataMapperBO.getRDLCharges();
		int age  = reqBean.getInsured_age();
		int tempAge=age;
		int page  = reqBean.getProposer_age();
		int ppt = reqBean.getPremiumpayingterm();
		String gender_insured = reqBean.getInsured_sex();
		String gender=gender_insured;
		String gender_proposer = reqBean.getProposer_sex();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int planId = oPNLPlanLk.getPNL_ID();
		double rdlId = fBean.getA();
		CommonConstant.printLog("d", "Rider Charges Factor for Plan ID::::: " + planId + " policy year: " + cnt);
		for(int count = 0; count < RLCValueLst.size(); count++){
			RDLChargesValue oRLCValue =  (RDLChargesValue)RLCValueLst.get(count);
			if (ppt<=10) {
				ppt = 0;
			}
			if (rdlId == 240) {
				tempAge = page;
				gender=gender_proposer;
			} else if (rdlId == 239) {
				tempAge = age;
			} else {
				tempAge = age;
				ppt=0;
			}
			if(((tempAge+cnt-1)==oRLCValue.getRCS_AGE() || oRLCValue.getRCS_AGE()==999) &&
					(gender.equals(oRLCValue.getRCS_GENDER()) || oRLCValue.getRCS_GENDER().equals("U"))
					&& planId==oRLCValue.getRCS_PNL_ID() && rdlId == oRLCValue.getRCS_RDL_ID()
					&& (Integer.parseInt(oRLCValue.getRCS_BAND())==ppt)){
				RLCFactorMap.put(oRLCValue.getRCS_DUR1(), oRLCValue.getRCS_CVAL1());
				RLCFactorMap.put(oRLCValue.getRCS_DUR2(), oRLCValue.getRCS_CVAL2());
				RLCFactorMap.put(oRLCValue.getRCS_DUR3(), oRLCValue.getRCS_CVAL3());
				RLCFactorMap.put(oRLCValue.getRCS_DUR4(), oRLCValue.getRCS_CVAL4());
				RLCFactorMap.put(oRLCValue.getRCS_DUR5(), oRLCValue.getRCS_CVAL5());
				RLCFactorMap.put(oRLCValue.getRCS_DUR6(), oRLCValue.getRCS_CVAL6());
				RLCFactorMap.put(oRLCValue.getRCS_DUR7(), oRLCValue.getRCS_CVAL7());
				RLCFactorMap.put(oRLCValue.getRCS_DUR8(), oRLCValue.getRCS_CVAL8());
				RLCFactorMap.put(oRLCValue.getRCS_DUR9(), oRLCValue.getRCS_CVAL9());
				RLCFactorMap.put(oRLCValue.getRCS_DUR10(), oRLCValue.getRCS_CVAL10());
					CommonConstant.printLog("d", "RLC Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "RLC Factor PMV_DUR1 " + oRLCValue.getRCS_DUR1() + " : " + oRLCValue.getRCS_CVAL1());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR2 " + oRLCValue.getRCS_DUR2() + " : " + oRLCValue.getRCS_CVAL2());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR3 " + oRLCValue.getRCS_DUR3() + " : " + oRLCValue.getRCS_CVAL3());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR4 " + oRLCValue.getRCS_DUR4() + " : " + oRLCValue.getRCS_CVAL4());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR5 " + oRLCValue.getRCS_DUR5() + " : " + oRLCValue.getRCS_CVAL5());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR6 " + oRLCValue.getRCS_DUR6() + " : " + oRLCValue.getRCS_CVAL6());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR7 " + oRLCValue.getRCS_DUR7() + " : " + oRLCValue.getRCS_CVAL7());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR8 " + oRLCValue.getRCS_DUR8() + " : " + oRLCValue.getRCS_CVAL8());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR9 " + oRLCValue.getRCS_DUR9() + " : " + oRLCValue.getRCS_CVAL9());
					CommonConstant.printLog("d", "RLC Factor PMV_DUR10 " + oRLCValue.getRCS_DUR10() + " : " + oRLCValue.getRCS_CVAL10());


			}
		}
		return RLCFactorMap;
	}
	public static double getAAAFMC(SISRequestBean reqBean, int cnt){//Addded by Samina for AAA3
		double AAAFMCFactor;
		int age = reqBean.getInsured_age();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap AAAFMCFactorMap = getAAAFMCFactor(oPNLPlanLk.getPNL_ID(), age + cnt - 1);
		if(AAAFMCFactorMap.get(age+cnt-1)!=null){
			AAAFMCFactor = Double.parseDouble(AAAFMCFactorMap.get(age+cnt-1).toString());
			 CommonConstant.printLog("d", "AAAFMCFactor selected :::: " + AAAFMCFactor + " For Policy Year :::: " + cnt);
		}else{
			AAAFMCFactor=0.0;
		  	CommonConstant.printLog("d", "AAAFMCFactor in else :::: " + AAAFMCFactor + " For Policy Year :::: " + cnt);
		}
		return AAAFMCFactor;
	 }


	public static HashMap getAAAFMCFactor(int planId, int age)
	{
		CommonConstant.printLog("d", "FMC AAA Value for Plan ID::::: " + planId + " age: " + age);
		HashMap AAAFMCFactorMap = new HashMap();
		ArrayList AAAFMCValueLst = oDataMapperBO.getAAAFMCValues();
		for(int count = 0; count < AAAFMCValueLst.size(); count++){
			AAAFMCValues oAAAFMCValues =  (AAAFMCValues)AAAFMCValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());
				if(planId == oAAAFMCValues.getAfc_plan_id() && age>= oAAAFMCValues.getAfc_min_age() && age<= oAAAFMCValues.getAfc_max_age()){
					AAAFMCFactorMap.put(age, oAAAFMCValues.getAfc_fmc());
				}
		}


		return AAAFMCFactorMap;
	}

	public static double getAAADFV(SISRequestBean reqBean, int cnt){//Addded by Samina for AAA3
		double AAADFVFactor;
		int age = reqBean.getInsured_age();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap AAADEBTFactorMap = getAAADFVFactor(oPNLPlanLk.getPNL_ID(), age + cnt - 1);
		if(AAADEBTFactorMap.get(age+cnt-1)!=null){
			AAADFVFactor = Double.parseDouble(AAADEBTFactorMap.get(age+cnt-1).toString());
			 CommonConstant.printLog("d", "AAADFVFactor selected :::: " + AAADFVFactor + " For Policy Year :::: " + cnt);
		}else{
			AAADFVFactor=0.0;
		  	CommonConstant.printLog("d", "AAADFVFactor in else :::: " + AAADFVFactor + " For Policy Year :::: " + cnt);
		}
		return AAADFVFactor;
	 }


	public static HashMap getAAADFVFactor(int planId, int age)
	{
		CommonConstant.printLog("d", "Debt Fund AAA Value for Plan ID::::: " + planId + " age: " + age);
		HashMap AAADEBTFactorMap = new HashMap();
		ArrayList AAAFMCValueLst = oDataMapperBO.getAAAFMCValues();
		for(int count = 0; count < AAAFMCValueLst.size(); count++){
			AAAFMCValues oAAAFMCValues =  (AAAFMCValues)AAAFMCValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());
				if(planId == oAAAFMCValues.getAfc_plan_id() && age>= oAAAFMCValues.getAfc_min_age() && age<= oAAAFMCValues.getAfc_max_age()){
					AAADEBTFactorMap.put(age, oAAAFMCValues.getAfc_whole_life_income());
				}
		}


		return AAADEBTFactorMap;
	}

	public static double getAAAEFV(SISRequestBean reqBean, int cnt){//Addded by Samina for AAA3
		double AAAEFVFactor;
		int age = reqBean.getInsured_age();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap AAADEBTFactorMap = getAAAEFVFactor(oPNLPlanLk.getPNL_ID(), age + cnt - 1);
		if(AAADEBTFactorMap.get(age+cnt-1)!=null){
			AAAEFVFactor = Double.parseDouble(AAADEBTFactorMap.get(age+cnt-1).toString());
			 CommonConstant.printLog("d", "AAADFVFactor selected :::: " + AAAEFVFactor + " For Policy Year :::: " + cnt);
		}else{
			AAAEFVFactor=0.0;
		  	CommonConstant.printLog("d", "AAADFVFactor in else :::: " + AAAEFVFactor + " For Policy Year :::: " + cnt);
		}
		return AAAEFVFactor;
	 }


	public static HashMap getAAAEFVFactor(int planId, int age)
	{
		CommonConstant.printLog("d", "Debt Fund AAA Value for Plan ID::::: " + planId + " age: " + age);
		HashMap AAAEQFactorMap = new HashMap();
		ArrayList AAAFMCValueLst = oDataMapperBO.getAAAFMCValues();
		for(int count = 0; count < AAAFMCValueLst.size(); count++){
			AAAFMCValues oAAAFMCValues =  (AAAFMCValues)AAAFMCValueLst.get(count);
				if(planId == oAAAFMCValues.getAfc_plan_id() && age>= oAAAFMCValues.getAfc_min_age() && age<= oAAAFMCValues.getAfc_max_age()){
					AAAEQFactorMap.put(age, oAAAFMCValues.getAfc_large_cap());
				}
		}


		return AAAEQFactorMap;
	}

	public static String getComissionText(String planCode){
		String comText="";
		ArrayList CommTextList = oDataMapperBO.getCommTextlist();
		for (int count=0;count<CommTextList.size();count++) {
			CommText CommTextValues = (CommText)CommTextList.get(count);
			if (planCode.equals(CommTextValues.getCmt_pgl_id())) {
				comText=CommTextValues.getCmt_comm_text();
			}
		}
		return comText;
	}
	public static double getANNUITYRate(SISRequestBean reqBean){
		double annuityRate=0;
		ArrayList AnnuityRateList = oDataMapperBO.getAnnuityRatelist();
		int age  = reqBean.getInsured_age();
		String mode=reqBean.getFrequency();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int planId = oPNLPlanLk.getPNL_ID();
		for (int count=0;count<AnnuityRateList.size();count++) {
			ANNUITYRate AnuityRateValues = (ANNUITYRate)AnnuityRateList.get(count);
			if (age==AnuityRateValues.getArl_age() && mode.equals(AnuityRateValues.getArl_annuity_mode()) && planId==Integer.parseInt(AnuityRateValues.getArl_pnl_id())) {
				annuityRate=AnuityRateValues.getArl_annuity_rate();
			}
		}
		return annuityRate;
	}
	public static double getFOPRate(SISRequestBean reqBean,int cnt){
		double fopRate=0;
		ArrayList fopRateList = oDataMapperBO.getFOPRatelist();
		String mode=reqBean.getFrequency();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		int planId = oPNLPlanLk.getPNL_ID();
		for (int count=0;count<fopRateList.size();count++) {
			FOPRate FOPRateValues = (FOPRate)fopRateList.get(count);
			if (cnt==FOPRateValues.getFop_term() && mode.equals(FOPRateValues.getFop_mode()) && planId==FOPRateValues.getFop_pnl_id()) {
				fopRate=FOPRateValues.getFop_rate();
			}

		}
		return fopRate;
	}
	public static double getINCENTIVERate(SISRequestBean reqBean){
		double incentiveRate=0;
		ArrayList IncentiveRateList = oDataMapperBO.getIncentiveRatelist();
		String mode=reqBean.getFrequency();
		String plancode = reqBean.getBaseplan();
		long coverage = reqBean.getSumassured();
		for (int count=0;count<IncentiveRateList.size();count++) {
			INCENTIVERate IncentiveRateValues = (INCENTIVERate)IncentiveRateList.get(count);
			if (mode.equals(IncentiveRateValues.getInc_mode()) && plancode.equals(IncentiveRateValues.getInc_plan_code()) && coverage>=IncentiveRateValues.getInc_min_sa() && coverage<=IncentiveRateValues.getInc_max_sa()) {
				incentiveRate=IncentiveRateValues.getInc_rate();
			}
		}
		return incentiveRate;
	}

	public static AgentDetails getAgentDetails(){
		AgentDetails detail = oDataMapperBO.getAgent_inermedDetails();
		return  detail;
	}

	public static double getServiceTaxMain() {
		double ServiceTaxMain=0;
		ArrayList ServiceTaxMaster = oDataMapperBO.getServiceTaxMaster();
		for (int count=0;count<ServiceTaxMaster.size();count++) {
			com.talic.plugins.sisEngine.BO.ServiceTaxMaster ServiceTaxValues = (com.talic.plugins.sisEngine.BO.ServiceTaxMaster) ServiceTaxMaster.get(count);
			if (CommonConstant.SERVICE_TAX_MAIN.equals(ServiceTaxValues.getStm_type())) {
				ServiceTaxMain = ServiceTaxValues.getStm_value();
			}
		}
		return ServiceTaxMain;
	}

	public static double getServiceTaxFY() {
		double ServiceTaxFY=0;
		ArrayList ServiceTaxMaster = oDataMapperBO.getServiceTaxMaster();
		for (int count=0;count<ServiceTaxMaster.size();count++) {
			com.talic.plugins.sisEngine.BO.ServiceTaxMaster ServiceTaxValues = (com.talic.plugins.sisEngine.BO.ServiceTaxMaster) ServiceTaxMaster.get(count);
			if (CommonConstant.SERVICE_TAX_FIRST_YEAR.equals(ServiceTaxValues.getStm_type())) {
				ServiceTaxFY = ServiceTaxValues.getStm_value();
			}
		}
		return ServiceTaxFY;
	}

	public static double getServiceTaxRenewal() {
		double ServiceTaxRenewal=0;
		ArrayList ServiceTaxMaster = oDataMapperBO.getServiceTaxMaster();
		for (int count=0;count<ServiceTaxMaster.size();count++) {
			com.talic.plugins.sisEngine.BO.ServiceTaxMaster ServiceTaxValues = (com.talic.plugins.sisEngine.BO.ServiceTaxMaster) ServiceTaxMaster.get(count);
			if (CommonConstant.SERVICE_TAX_RENEWAL.equals(ServiceTaxValues.getStm_type())) {
				ServiceTaxRenewal = ServiceTaxValues.getStm_value();
			}
		}
		return ServiceTaxRenewal;
	}
	public static double getTB8Factor(SISRequestBean reqBean, int cnt){
		double SVFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap SVFactorMap = getTB8PTFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(SVFactorMap.get(cnt)!=null){
			SVFactor = Double.parseDouble(SVFactorMap.get(cnt).toString());
			CommonConstant.printLog("d", "TB8 Factor selected :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}else{
			SVFactor=0.0;
			CommonConstant.printLog("d", "TB8 Factor in else :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}
		return SVFactor;
	}

	public static double getTB4Factor(SISRequestBean reqBean, int cnt){
		double SVFactor;
		int polTerm = reqBean.getPolicyterm();
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		HashMap SVFactorMap = getTB4PTFactor(oPNLPlanLk.getPNL_ID(), polTerm);
		if(SVFactorMap.get(cnt)!=null){
			SVFactor = Double.parseDouble(SVFactorMap.get(cnt).toString());
			CommonConstant.printLog("d", "TB4 Factor selected :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}else{
			SVFactor=0.0;
			CommonConstant.printLog("d", "TB4 Factor in else :::: " + SVFactor + " For Policy Year :::: " + cnt);
		}
		return SVFactor;
	}

	public static HashMap getTB8PTFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "Guranteed Surrender Value Factor for PLan ID::::: " + planId + " term: " + term);
		HashMap SVFactorMap = new HashMap();
		ArrayList PMVMaturityValueLst = oDataMapperBO.getPMVPlanMaturityValue();
		for(int count = 0; count < PMVMaturityValueLst.size(); count++){
			PMVPlanMaturityValue oPMVMaturityValue =  (PMVPlanMaturityValue)PMVMaturityValueLst.get(count);
			//logger.debug("oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());

			if("T8".equals(oPMVMaturityValue.getPMV_BAND())){
				if(planId == oPMVMaturityValue.getPMV_PNL_ID()
						&& oPMVMaturityValue.getPMV_AGE() == term){
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR1(), oPMVMaturityValue.getPMV_CVAL1());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR2(), oPMVMaturityValue.getPMV_CVAL2());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR3(), oPMVMaturityValue.getPMV_CVAL3());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR4(), oPMVMaturityValue.getPMV_CVAL4());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR5(), oPMVMaturityValue.getPMV_CVAL5());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR6(), oPMVMaturityValue.getPMV_CVAL6());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR7(), oPMVMaturityValue.getPMV_CVAL7());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR8(), oPMVMaturityValue.getPMV_CVAL8());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR9(), oPMVMaturityValue.getPMV_CVAL9());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR10(), oPMVMaturityValue.getPMV_CVAL10());
					CommonConstant.printLog("d", "T8 Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "T8 Factor PMV_DUR1 " + oPMVMaturityValue.getPMV_DUR1() + " : " + oPMVMaturityValue.getPMV_CVAL1());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR2 " + oPMVMaturityValue.getPMV_DUR2() + " : " + oPMVMaturityValue.getPMV_CVAL2());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR3 " + oPMVMaturityValue.getPMV_DUR3() + " : " + oPMVMaturityValue.getPMV_CVAL3());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR4 " + oPMVMaturityValue.getPMV_DUR4() + " : " + oPMVMaturityValue.getPMV_CVAL4());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR5 " + oPMVMaturityValue.getPMV_DUR5() + " : " + oPMVMaturityValue.getPMV_CVAL5());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR6 " + oPMVMaturityValue.getPMV_DUR6() + " : " + oPMVMaturityValue.getPMV_CVAL6());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR7 " + oPMVMaturityValue.getPMV_DUR7() + " : " + oPMVMaturityValue.getPMV_CVAL7());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR8 " + oPMVMaturityValue.getPMV_DUR8() + " : " + oPMVMaturityValue.getPMV_CVAL8());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR9 " + oPMVMaturityValue.getPMV_DUR9() + " : " + oPMVMaturityValue.getPMV_CVAL9());
					CommonConstant.printLog("d", "T8 Factor PMV_DUR10 " + oPMVMaturityValue.getPMV_DUR10() + " : " + oPMVMaturityValue.getPMV_CVAL10());
				}
			}
		}


		return SVFactorMap;
	}

	public static HashMap getTB4PTFactor(int planId, int term)
	{
		CommonConstant.printLog("d", "Guranteed Surrender Value Factor for PLan ID::::: " + planId + " term: " + term);
		HashMap SVFactorMap = new HashMap();
		ArrayList PMVMaturityValueLst = oDataMapperBO.getPMVPlanMaturityValue();
		for(int count = 0; count < PMVMaturityValueLst.size(); count++){
			PMVPlanMaturityValue oPMVMaturityValue =  (PMVPlanMaturityValue)PMVMaturityValueLst.get(count);
			//CommonConstant.printLog("d","oPMVMaturityValue.getPMV_PNL_ID() :  " +oPMVMaturityValue.getPMV_PNL_ID()
			//		+ "oPMVMaturityValue.getPMV_AGE() :  " +oPMVMaturityValue.getPMV_AGE());

			if("T4".equals(oPMVMaturityValue.getPMV_BAND())){
				if(planId == oPMVMaturityValue.getPMV_PNL_ID()
						&& oPMVMaturityValue.getPMV_AGE() == term){
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR1(), oPMVMaturityValue.getPMV_CVAL1());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR2(), oPMVMaturityValue.getPMV_CVAL2());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR3(), oPMVMaturityValue.getPMV_CVAL3());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR4(), oPMVMaturityValue.getPMV_CVAL4());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR5(), oPMVMaturityValue.getPMV_CVAL5());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR6(), oPMVMaturityValue.getPMV_CVAL6());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR7(), oPMVMaturityValue.getPMV_CVAL7());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR8(), oPMVMaturityValue.getPMV_CVAL8());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR9(), oPMVMaturityValue.getPMV_CVAL9());
					SVFactorMap.put(oPMVMaturityValue.getPMV_DUR10(), oPMVMaturityValue.getPMV_CVAL10());
					CommonConstant.printLog("d", "T4 Factor for PLan ID::::: " + planId);
					CommonConstant.printLog("d", "T4 Factor PMV_DUR1 " + oPMVMaturityValue.getPMV_DUR1() + " : " + oPMVMaturityValue.getPMV_CVAL1());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR2 " + oPMVMaturityValue.getPMV_DUR2() + " : " + oPMVMaturityValue.getPMV_CVAL2());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR3 " + oPMVMaturityValue.getPMV_DUR3() + " : " + oPMVMaturityValue.getPMV_CVAL3());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR4 " + oPMVMaturityValue.getPMV_DUR4() + " : " + oPMVMaturityValue.getPMV_CVAL4());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR5 " + oPMVMaturityValue.getPMV_DUR5() + " : " + oPMVMaturityValue.getPMV_CVAL5());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR6 " + oPMVMaturityValue.getPMV_DUR6() + " : " + oPMVMaturityValue.getPMV_CVAL6());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR7 " + oPMVMaturityValue.getPMV_DUR7() + " : " + oPMVMaturityValue.getPMV_CVAL7());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR8 " + oPMVMaturityValue.getPMV_DUR8() + " : " + oPMVMaturityValue.getPMV_CVAL8());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR9 " + oPMVMaturityValue.getPMV_DUR9() + " : " + oPMVMaturityValue.getPMV_CVAL9());
					CommonConstant.printLog("d", "T4 Factor PMV_DUR10 " + oPMVMaturityValue.getPMV_DUR10() + " : " + oPMVMaturityValue.getPMV_CVAL10());
				}
			}
		}


		return SVFactorMap;
	}

	public static double getPremiumMultiple_MIP(SISRequestBean reqBean)
	{
		double premiumMultiple = 0;
		PNLPlanLk oPNLPlanLk = getPlanLKObject(reqBean.getBaseplan());
		ArrayList pbaBandLst = getPBABandList(reqBean.getBaseplan());
		ArrayList pmlPremiumMultipleList = oDataMapperBO.getPMLPremiumMultipleLk();

		for (int count = 0; count < pmlPremiumMultipleList.size(); count++) {
			PMLPremiumMultipleLk oPMLPremiumMultipleLk = (PMLPremiumMultipleLk) pmlPremiumMultipleList
					.get(count);
			if (oPMLPremiumMultipleLk.getPML_PNL_ID() == oPNLPlanLk.getPNL_ID()) {

				String sex = "";
				int age = 0;
				int term = 0;
				String smokerFlg = "";
				if ("U".equals(oPMLPremiumMultipleLk.getPML_SEX())) {
					sex = "U";
				} else {
					sex = reqBean.getInsured_sex();
				}

				if (oPMLPremiumMultipleLk.getPML_AGE() == 999) {
					age = 999;
				} else {
					age = reqBean.getInsured_age();
				}
				if (oPNLPlanLk.getPNL_PREM_MULT_FORMULA() == 3) {
					term = reqBean.getPolicyterm();
				} else {
					term = oPMLPremiumMultipleLk.getPML_TERM();
				}
				//logger.debug("Term :::::::::::" + term +  " : " + oPMLPremiumMultipleLk.getPML_TERM());

				if ("0".equals(reqBean.getInsured_smokerstatus())) {
					smokerFlg = "Y";
				} else if ("1".equals(reqBean.getInsured_smokerstatus())) {
					smokerFlg = "N";
				} else {
					smokerFlg = oPMLPremiumMultipleLk.getSMOKER_FLAG();
				}

				if(oPMLPremiumMultipleLk.getSMOKER_FLAG()==null){
					oPMLPremiumMultipleLk.setSMOKER_FLAG("");
				}
				if (pbaBandLst.size() > 0) {
					int pbaBand = getPBABand(pbaBandLst, reqBean.getSumassured());
					CommonConstant.printLog("d", "################## pbaBand : " + pbaBand);

					if (oPMLPremiumMultipleLk.getPML_AGE() == age
							&& sex.equals(oPMLPremiumMultipleLk.getPML_SEX())
							&& oPMLPremiumMultipleLk.getPML_BAND() == pbaBand
							&& smokerFlg.equals(oPMLPremiumMultipleLk.getSMOKER_FLAG()))
					{
						premiumMultiple = oPMLPremiumMultipleLk.getPML_PREM_MULT();
						CommonConstant.printLog("d", "PML_PNL_ID:PML_SEX:PML_BAND:PML_AGE:PML_PREM_MULT:PML_BAND:PML_SMOKER"
								+ oPMLPremiumMultipleLk.getPML_PNL_ID()
								+ oPMLPremiumMultipleLk.getPML_SEX()
								+ oPMLPremiumMultipleLk.getPML_BAND()
								+ oPMLPremiumMultipleLk.getPML_AGE()
								+ oPMLPremiumMultipleLk.getPML_PREM_MULT()
								+ oPMLPremiumMultipleLk.getPML_BAND()
								+ oPMLPremiumMultipleLk.getSMOKER_FLAG());

						break;
					}
				} else if (oPMLPremiumMultipleLk.getPML_AGE() == age
						&& sex.equals(oPMLPremiumMultipleLk.getPML_SEX())
						&& oPMLPremiumMultipleLk.getPML_TERM() == term
						&& smokerFlg.equals(oPMLPremiumMultipleLk.getSMOKER_FLAG()))
				{
					premiumMultiple = oPMLPremiumMultipleLk.getPML_PREM_MULT();
					CommonConstant.printLog("d", "PML_PNL_ID:PML_SEX:PML_TERM:PML_AGE:PML_PREM_MULT:PML_BAND:PML_SMOKER"
							+ oPMLPremiumMultipleLk.getPML_PNL_ID()
							+ oPMLPremiumMultipleLk.getPML_SEX()
							+ oPMLPremiumMultipleLk.getPML_TERM()
							+ oPMLPremiumMultipleLk.getPML_AGE()
							+ oPMLPremiumMultipleLk.getPML_PREM_MULT()
							+ oPMLPremiumMultipleLk.getPML_BAND()
							+ oPMLPremiumMultipleLk.getSMOKER_FLAG());
					break;
				}
			}

		}
		// Check if any premium discount is there

		//Premium Discount on premium in MIP
		double premiumDiscount =  DataAccessClass.getPremiumDiscount(oPNLPlanLk.getPNL_CODE(), reqBean.getBasepremiumannual()*12);

		double premMult= premiumMultiple - premiumDiscount;
		BigDecimal fd = new BigDecimal(premMult);
		BigDecimal cut = fd.setScale(2, RoundingMode.HALF_UP);
		premiumMultiple = cut.doubleValue();
		CommonConstant.printLog("d", "premiumMultiple from database::::::::::: " + premiumMultiple);
		return premiumMultiple;
	}
}
