package com.talic.plugins.sisEngine.db;

import android.database.Cursor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;


import com.talic.plugins.sisEngine.BO.AAAFMCValues;
import com.talic.plugins.sisEngine.BO.ACMAdminChargesMaster;
import com.talic.plugins.sisEngine.BO.AddressBO;
import com.talic.plugins.sisEngine.BO.AgentDetails;
import com.talic.plugins.sisEngine.BO.CCMCommissionChargeMaster;
import com.talic.plugins.sisEngine.BO.CommText;
import com.talic.plugins.sisEngine.BO.FCMFIBChargesMaster;
import com.talic.plugins.sisEngine.BO.FEDFormulaExtDesc;
import com.talic.plugins.sisEngine.BO.FOPRate;
import com.talic.plugins.sisEngine.BO.FundDetailsLK;
import com.talic.plugins.sisEngine.BO.GAIGuarAnnIncChMaster;
import com.talic.plugins.sisEngine.BO.GuaranteedMaturityBonusMaster;
import com.talic.plugins.sisEngine.BO.IPCValues;
import com.talic.plugins.sisEngine.BO.IplIllussPagelist;
import com.talic.plugins.sisEngine.BO.LpbLargePremiumBoost;
import com.talic.plugins.sisEngine.BO.MCMMortilityChargesMaster;
import com.talic.plugins.sisEngine.BO.MDLModalFactorLk;
import com.talic.plugins.sisEngine.BO.MFMMaturityFactorMaster;
import com.talic.plugins.sisEngine.BO.OCLOccupationLk;
import com.talic.plugins.sisEngine.BO.PACPremiumAllocationCharges;
import com.talic.plugins.sisEngine.BO.PBAProductBand;
import com.talic.plugins.sisEngine.BO.PCRPlanChargesRef;
import com.talic.plugins.sisEngine.BO.PFRPlanFormulaRef;
import com.talic.plugins.sisEngine.BO.PGFPlanGroupFeature;
import com.talic.plugins.sisEngine.BO.PGLGrpMinPremLK;
import com.talic.plugins.sisEngine.BO.PGLPlanGroupLk;
import com.talic.plugins.sisEngine.BO.PMLPremiumMultipleLk;
import com.talic.plugins.sisEngine.BO.PMVPlanMaturityValue;
import com.talic.plugins.sisEngine.BO.PNLChkExcludesLk;
import com.talic.plugins.sisEngine.BO.PNLMinPremLK;
import com.talic.plugins.sisEngine.BO.PNLPlanLk;
import com.talic.plugins.sisEngine.BO.PNLTermPptLK;
import com.talic.plugins.sisEngine.BO.PmdPlanPremiumMultDisc;
import com.talic.plugins.sisEngine.BO.RDLChargesValue;
import com.talic.plugins.sisEngine.BO.RDLRiderLk;
import com.talic.plugins.sisEngine.BO.RMLRiderPremMultipleLK;
import com.talic.plugins.sisEngine.BO.SPVSysParamValues;
import com.talic.plugins.sisEngine.BO.STDTemplateValues;
import com.talic.plugins.sisEngine.BO.SURSurrenderChargeMaster;
import com.talic.plugins.sisEngine.BO.ServiceTaxMaster;
import com.talic.plugins.sisEngine.BO.TempImagePath;
import com.talic.plugins.sisEngine.BO.WCMWOPChargesMaster;
import com.talic.plugins.sisEngine.BO.XMMXmlMetadataMaster;
import com.talic.plugins.sisEngine.util.CommonConstant;
import net.sqlcipher.database.SQLiteDatabase;

@SuppressWarnings("unchecked")
public class FetchDataDAO {
	public static ArrayList getModalFactorDetails(SQLiteDatabase db, String pglId) {
		ArrayList PlanDetails=new ArrayList();

		Cursor rs;
		MDLModalFactorLk oModalFactor;
		try {

			rs=db.rawQuery("SELECT MDL_ID,MDL_PGL_ID,MDL_PAY_FREQ,MDL_MODAL_FACTOR FROM LP_MDL_MODAL_FACTOR_LK WHERE MDL_PGL_ID = ?",new String[]{pglId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oModalFactor=new MDLModalFactorLk();
					oModalFactor.setMDL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("MDL_ID"))));
					oModalFactor.setMDL_PGL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("MDL_PGL_ID"))));
					oModalFactor.setMDL_PAY_FREQ(rs.getString(rs.getColumnIndex("MDL_PAY_FREQ")));
					oModalFactor.setMDL_MODAL_FACTOR(Double.parseDouble(rs.getString(rs.getColumnIndex("MDL_MODAL_FACTOR"))));
					PlanDetails.add(oModalFactor);
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return PlanDetails;
	}



	public static HashMap getOCLOccupationLk(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		OCLOccupationLk oOccupation;
		try {

			rs = db.rawQuery("SELECT OCL_ID,OCL_CODE,OCL_DESCRIPTION,OCL_OCCUPATION,OCL_BUSINESS,OCL_BLIFE,OCL_WP,OCL_CLASS FROM LP_OCL_OCCUPATION_LK",new String[0]);
			if(rs!=null) {
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oOccupation=new OCLOccupationLk();
					oOccupation.setOCL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("OCL_ID"))));
					oOccupation.setOCL_CODE(rs.getString(rs.getColumnIndex("OCL_CODE")));
					oOccupation.setOCL_DESCRIPTION(rs.getString(rs.getColumnIndex("OCL_DESCRIPTION")));
					oOccupation.setOCL_OCCUPATION(rs.getString(rs.getColumnIndex("OCL_OCCUPATION")));
					oOccupation.setOCL_BUSINESS(rs.getString(rs.getColumnIndex("OCL_BUSINESS")));
					oOccupation.setOCL_BLIFE(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_BLIFE"))));
					oOccupation.setOCL_WP(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_WP"))));
					oOccupation.setOCL_CLASS(Integer.parseInt(rs.getString(rs.getColumnIndex("OCL_CLASS"))));
					/*

					commented by Akhil on 03-07-2015 10:44:00 as corresponding columns not available in sqlite db

					oOccupation.setOCL_IND(rs.getString(rs.getColumnIndex("OCL_IND")));
					oOccupation.setOCL_ADD(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_ADD"))));
					oOccupation.setOCL_PA(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_PA"))));

					oOccupation.setOCL_ADB(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_ADB"))));
					oOccupation.setOCL_AI(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_AI"))));
					oOccupation.setOCL_HS(Double.parseDouble(rs.getString(rs.getColumnIndex("OCL_HS"))));
					oOccupation.setOCL_RCC(rs.getString(rs.getColumnIndex("OCL_RCC")));
					oOccupation.setOCL_UW_CODE(rs.getString(rs.getColumnIndex("OCL_UW_CODE")));

					oOccupation.setOCL_TYPE(rs.getString(rs.getColumnIndex("OCL_TYPE")));
					oOccupation.setOCL_HB(Integer.parseInt(rs.getString(rs.getColumnIndex("OCL_HB"))));
					oOccupation.setOCL_SOCIAL(rs.getString(rs.getColumnIndex("OCL_SOCIAL")));
					*/

					oHashMap.put(rs.getString(rs.getColumnIndex("OCL_CODE")), oOccupation);



				}
				rs.close();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;
	}

	public static HashMap getPGLPlanGroupLk(SQLiteDatabase db, String pglId) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		PGLPlanGroupLk oPlanGroup;
		try {

			rs = db.rawQuery("SELECT PGL_ID,PGL_DESCRIPTION,PGL_JUVENILE_IND,PGL_ADULT_IND,PGL_PRODUCT_TYPE FROM LP_PGL_PLAN_GROUP_LK WHERE PGL_ID = ?", new String[]{pglId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPlanGroup=new PGLPlanGroupLk();
					oPlanGroup.setPGL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_ID"))));
					oPlanGroup.setPGL_DESCRIPTION(rs.getString(rs.getColumnIndex("PGL_DESCRIPTION")));
					oPlanGroup.setPGL_JUVENILE_IND(rs.getString(rs.getColumnIndex("PGL_JUVENILE_IND")));
					oPlanGroup.setPGL_ADULT_IND(rs.getString(rs.getColumnIndex("PGL_ADULT_IND")));
					oPlanGroup.setPGL_PRODUCT_TYPE(rs.getString(rs.getColumnIndex("PGL_PRODUCT_TYPE")));
					/*

					commented by Akhil on 03-07-2015 10:44:00 as corresponding columns not available in sqlite db

					oPlanGroup.setPGL_PRODUCT_SUBTYPE(rs.getString(rs.getColumnIndex("PGL_PRODUCT_SUBTYPE")));
					oPlanGroup.setPGL_TERM_CONFIGURED(rs.getString(rs.getColumnIndex("PGL_TERM_CONFIGURED")));
					oPlanGroup.setPGL_APPLICATION(rs.getString(rs.getColumnIndex("PGL_APPLICATION")));
					oPlanGroup.setPGL_TERM_FLAG(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_TERM_FLAG"))));
					oPlanGroup.setPGL_PREFIX(rs.getString(rs.getColumnIndex("PGL_PREFIX")));*/
					oHashMap.put(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_ID"))), oPlanGroup);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}


	public static ArrayList getPMLPremiumMultipleLk(SQLiteDatabase db, String pnlId) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PMLPremiumMultipleLk oPremiumMultiple;
		try {

			rs = db.rawQuery("SELECT PML_ID,PML_PNL_ID,PML_SEX,PML_TERM,PML_AGE,PML_PREM_MULT,PML_BAND,SMOKER_FLAGAG FROM LP_PML_PREMIUM_MULTIPLE_LK WHERE PML_PNL_ID = ?", new String[]{pnlId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPremiumMultiple=new PMLPremiumMultipleLk();
					oPremiumMultiple.setPML_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_ID"))));
					oPremiumMultiple.setPML_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_PNL_ID"))));
					oPremiumMultiple.setPML_SEX(rs.getString(rs.getColumnIndex("PML_SEX")));
					oPremiumMultiple.setPML_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_TERM"))));
					oPremiumMultiple.setPML_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_AGE"))));
					oPremiumMultiple.setPML_PREM_MULT(Double.parseDouble(rs.getString(rs.getColumnIndex("PML_PREM_MULT"))));
					oPremiumMultiple.setPML_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_BAND"))));
					oPremiumMultiple.setSMOKER_FLAG(rs.getString(rs.getColumnIndex("SMOKER_FLAGAG")));
					oList.add(oPremiumMultiple);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}


	public static HashMap getPNLChkExcludesLk(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		PNLChkExcludesLk oCheckExcludes;
		try {

			rs = db.rawQuery("SELECT CHK_ID,PNL_ID,PNL_CODE,SUM_ASSURED,UNITS,PURCHASE_PRICE,REGULAR_PREMIUM FROM LP_PNL_CHK_EXCLUDES_LK", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oCheckExcludes=new PNLChkExcludesLk();

					oCheckExcludes.setCHK_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("CHK_ID"))));
					oCheckExcludes.setPNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_ID"))));
					oCheckExcludes.setPNL_CODE(rs.getString(rs.getColumnIndex("PNL_CODE")));
					oCheckExcludes.setSUM_ASSURED(rs.getString(rs.getColumnIndex("SUM_ASSURED")));
					oCheckExcludes.setUNITS(rs.getString(rs.getColumnIndex("UNITS")));
					oCheckExcludes.setPURCHASE_PRICE(rs.getString(rs.getColumnIndex("PURCHASE_PRICE")));
					oCheckExcludes.setREGULAR_PREMIUM(rs.getString(rs.getColumnIndex("REGULAR_PREMIUM")));

					oHashMap.put(rs.getString(rs.getColumnIndex("PNL_CODE")), oCheckExcludes);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}


	public static HashMap getPNLPlanLk(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		PNLPlanLk oPlanLK;
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		try {

			rs = db.rawQuery("SELECT PNL_ID,PNL_CODE,PNL_DESCRIPTION,PNL_PGL_ID,PNL_TYPE,PNL_EFFECTIVE_DATE,PNL_END_DATE,PNL_MIN_AGE,PNL_MAX_AGE,PNL_MIN_SUM_ASSURED,PNL_MAX_SUM_ASSURED,PNL_MIN_PREMIUM,PNL_MIN_OCCUPATION_CLASS,PNL_MAX_OCCUPATION_CLASS,PNL_PREMIUM_MULTIPLE,PNL_MAX_PREMIUM_PAYING_TERM,PNL_PAMXUSE,PNL_FREQUENCY,PNL_SERVICE_TAX,PNL_PRREM_MULT_FORMULA,ISBACKDATE,NOOFMONTH,UIN,PNL_SA_MULTIPLEOF,PNL_AGENTTYPE,PNL_ANNUITY_FREQUENCY,URALLOW,URPERCENT,PNL_START_PT FROM LP_PNL_PLAN_LK", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPlanLK=new PNLPlanLk();
					if(rs.getString(rs.getColumnIndex("PNL_ID"))!=null && (!rs.getString(rs.getColumnIndex("PNL_ID")).equalsIgnoreCase("")))
						oPlanLK.setPNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_ID"))));
					else
						oPlanLK.setPNL_ID(0);
					if(rs.getString(rs.getColumnIndex("PNL_CODE"))!=null && (!rs.getString(rs.getColumnIndex("PNL_CODE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_CODE(rs.getString(rs.getColumnIndex("PNL_CODE")));
					else
						oPlanLK.setPNL_CODE(null);

					if(rs.getString(rs.getColumnIndex("PNL_DESCRIPTION"))!=null && (!rs.getString(rs.getColumnIndex("PNL_DESCRIPTION")).equalsIgnoreCase("")))
						oPlanLK.setPNL_DESCRIPTION(rs.getString(rs.getColumnIndex("PNL_DESCRIPTION")));
					else
						oPlanLK.setPNL_DESCRIPTION(null);
					if(rs.getString(rs.getColumnIndex("PNL_PGL_ID"))!=null && (!rs.getString(rs.getColumnIndex("PNL_PGL_ID")).equalsIgnoreCase("")))
						oPlanLK.setPNL_PGL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_PGL_ID"))));
					else
						oPlanLK.setPNL_PGL_ID(0);
					if(rs.getString(rs.getColumnIndex("PNL_TYPE"))!= null && (!rs.getString(rs.getColumnIndex("PNL_TYPE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_TYPE(rs.getString(rs.getColumnIndex("PNL_TYPE")));
					else
						oPlanLK.setPNL_TYPE(null);
					if(rs.getString(rs.getColumnIndex("PNL_EFFECTIVE_DATE"))!=null && (!rs.getString(rs.getColumnIndex("PNL_EFFECTIVE_DATE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_EFFECTIVE_DATE(dateFormat.parse(rs.getString(rs.getColumnIndex("PNL_EFFECTIVE_DATE"))));
					else
						oPlanLK.setPNL_EFFECTIVE_DATE(null);
					if(rs.getString(rs.getColumnIndex("PNL_END_DATE")) != null && (!rs.getString(rs.getColumnIndex("PNL_END_DATE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_END_DATE(dateFormat.parse(rs.getString(rs.getColumnIndex("PNL_END_DATE"))));
					else
						oPlanLK.setPNL_END_DATE(null);

					if(rs.getString(rs.getColumnIndex("PNL_MIN_AGE")) != null && (!rs.getString(rs.getColumnIndex("PNL_MIN_AGE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MIN_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MIN_AGE"))));
					else
						oPlanLK.setPNL_MIN_AGE(0);
					if(rs.getString(rs.getColumnIndex("PNL_MAX_AGE")) != null && (!rs.getString(rs.getColumnIndex("PNL_MAX_AGE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MAX_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MAX_AGE"))));
					else
						oPlanLK.setPNL_MAX_AGE(0);
					if(rs.getString(rs.getColumnIndex("PNL_MIN_SUM_ASSURED")) != null && (!rs.getString(rs.getColumnIndex("PNL_MIN_SUM_ASSURED")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MIN_SUM_ASSURED(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MIN_SUM_ASSURED"))));
					else
						oPlanLK.setPNL_MIN_SUM_ASSURED(0);
					if(rs.getString(rs.getColumnIndex("PNL_MAX_SUM_ASSURED")) != null && (!rs.getString(rs.getColumnIndex("PNL_MAX_SUM_ASSURED")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MAX_SUM_ASSURED(Long.parseLong(rs.getString(rs.getColumnIndex("PNL_MAX_SUM_ASSURED"))));
					else
						oPlanLK.setPNL_MAX_SUM_ASSURED(0);
					if(rs.getString(rs.getColumnIndex("PNL_MIN_PREMIUM")) != null && (!rs.getString(rs.getColumnIndex("PNL_MIN_PREMIUM")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MIN_PREMIUM(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MIN_PREMIUM"))));
					else
						oPlanLK.setPNL_MIN_PREMIUM(0);
					if(rs.getString(rs.getColumnIndex("PNL_MIN_OCCUPATION_CLASS")) != null && (!rs.getString(rs.getColumnIndex("PNL_MIN_OCCUPATION_CLASS")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MIN_OCCUPATION_CLASS(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MIN_OCCUPATION_CLASS"))));
					else
						oPlanLK.setPNL_MIN_OCCUPATION_CLASS(0);
					if(rs.getString(rs.getColumnIndex("PNL_MAX_OCCUPATION_CLASS")) != null && (!rs.getString(rs.getColumnIndex("PNL_MAX_OCCUPATION_CLASS")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MAX_OCCUPATION_CLASS(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MAX_OCCUPATION_CLASS"))));
					else
						oPlanLK.setPNL_MAX_OCCUPATION_CLASS(0);
					if(rs.getString(rs.getColumnIndex("PNL_PREMIUM_MULTIPLE")) != null && (!rs.getString(rs.getColumnIndex("PNL_PREMIUM_MULTIPLE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_PREMIUM_MULTIPLE(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_PREMIUM_MULTIPLE"))));
					else
						oPlanLK.setPNL_PREMIUM_MULTIPLE(0);
					if(rs.getString(rs.getColumnIndex("PNL_MAX_PREMIUM_PAYING_TERM")) != null && (!rs.getString(rs.getColumnIndex("PNL_MAX_PREMIUM_PAYING_TERM")).equalsIgnoreCase("")))
						oPlanLK.setPNL_MAX_PREMIUM_PAYING_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MAX_PREMIUM_PAYING_TERM"))));
					else
						oPlanLK.setPNL_MAX_PREMIUM_PAYING_TERM(0);
					if(rs.getString(rs.getColumnIndex("PNL_PAMXUSE")) != null && (!rs.getString(rs.getColumnIndex("PNL_PAMXUSE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_PAMXUSE(rs.getString(rs.getColumnIndex("PNL_PAMXUSE")));
					else
						oPlanLK.setPNL_PAMXUSE(null);
					if(rs.getString(rs.getColumnIndex("PNL_FREQUENCY")) != null && (!rs.getString(rs.getColumnIndex("PNL_FREQUENCY")).equalsIgnoreCase("")))
						oPlanLK.setPNL_FREQUENCY(rs.getString(rs.getColumnIndex("PNL_FREQUENCY")));
					else
						oPlanLK.setPNL_FREQUENCY(null);
					if(rs.getString(rs.getColumnIndex("PNL_SERVICE_TAX")) != null && (!rs.getString(rs.getColumnIndex("PNL_SERVICE_TAX")).equalsIgnoreCase("")))
						oPlanLK.setPNL_SERVICE_TAX(Double.parseDouble(rs.getString(rs.getColumnIndex("PNL_SERVICE_TAX"))));
					else
						oPlanLK.setPNL_SERVICE_TAX(0);
					if(rs.getString(rs.getColumnIndex("PNL_PRREM_MULT_FORMULA")) != null && (!rs.getString(rs.getColumnIndex("PNL_PRREM_MULT_FORMULA")).equalsIgnoreCase("")))
						oPlanLK.setPNL_PREM_MULT_FORMULA(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_PRREM_MULT_FORMULA"))));
					else
						oPlanLK.setPNL_PREM_MULT_FORMULA(0);
					/*

					commented by Akhil on 03-07-2015 10:30:00 as corresponding columns not available in sqlite db

					oPlanLK.setPNL_MIN_SINGLE_TOPUP_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("PNL_MIN_SINGLE_TOPUP_PREM"))));

					oPlanLK.setPNL_LOCK_IN_TOPUP_WITHDRAWAL(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_LOCK_IN_TOPUP_WITHDRAWAL"))));

					oPlanLK.setPNL_MIN_PURCHASE_PRICE(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MIN_PURCHASE_PRICE"))));
					oPlanLK.setPNL_MAX_PURCHASE_PRICE(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_MAX_PURCHASE_PRICE"))));
					oPlanLK.setISSIMPLIFIED(rs.getString(rs.getColumnIndex("ISSIMPLIFIED")));
					oPlanLK.setISANNUITYPRODUCT(rs.getString(rs.getColumnIndex("ISANNUITYPRODUCT")));

					oPlanLK.setPNL_APPLICATION(rs.getString(rs.getColumnIndex("PNL_APPLICATION")));
					oPlanLK.setFIXCVGID(Integer.parseInt(rs.getString(rs.getColumnIndex("FIXCVGID"))));

					oPlanLK.setFREE_SWITCHE_CNT(Integer.parseInt(rs.getString(rs.getColumnIndex("FREE_SWITCHE_CNT"))));
					oPlanLK.setSWITCH_CHARGE_AMT(Double.parseDouble(rs.getString(rs.getColumnIndex("SWITCH_CHARGE_AMT"))));
					oPlanLK.setFREE_PREM_REDIR_CNT(Integer.parseInt(rs.getString(rs.getColumnIndex("FREE_PREM_REDIR_CNT"))));


					oPlanLK.setUVRMINATOP(Double.parseDouble(rs.getString(rs.getColumnIndex("UVRMINATOP"))));
					oPlanLK.setUVRMAXATOP(Double.parseDouble(rs.getString(rs.getColumnIndex("UVRMAXATOP"))));
					oPlanLK.setUVRMAXATM(Integer.parseInt(rs.getString(rs.getColumnIndex("UVRMAXATM"))));
					oPlanLK.setUVRMINRTOP(Integer.parseInt(rs.getString(rs.getColumnIndex("UVRMINRTOP"))));
					oPlanLK.setUVRMAXRTOP(Double.parseDouble(rs.getString(rs.getColumnIndex("UVRMAXRTOP"))));
					oPlanLK.setUVRFILLER0(rs.getString(rs.getColumnIndex("UVRFILLER0")));
					oPlanLK.setUVRFILLER1(rs.getString(rs.getColumnIndex("UVRFILLER1")));
					oPlanLK.setUVRFILLER2(rs.getString(rs.getColumnIndex("UVRFILLER2")));
					oPlanLK.setUVRFILLER3(rs.getString(rs.getColumnIndex("UVRFILLER3")));
					oPlanLK.setUVRFILLER4(rs.getString(rs.getColumnIndex("UVRFILLER4")));

					oPlanLK.setREN_MTHLY(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_MTHLY"))));
					oPlanLK.setREN_QTRLY(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_QTRLY"))));
					oPlanLK.setREN_SANAL(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_SANAL"))));
					oPlanLK.setREN_ANNUAL(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_ANNUAL"))));
					oPlanLK.setREN_MTHLY_INT(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_MTHLY_INT"))));
					oPlanLK.setREN_QTRLY_INT(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_QTRLY_INT"))));
					oPlanLK.setREN_SANAL_INT(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_SANAL_INT"))));
					oPlanLK.setREN_ANNUAL_INT(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_ANNUAL_INT"))));
					oPlanLK.setREN_PTD_NO(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_PTD_NO"))));
					oPlanLK.setREN_ISSUE_NO(Integer.parseInt(rs.getString(rs.getColumnIndex("REN_ISSUE_NO"))));

					oPlanLK.setPREM_REDIR_CHARGE_AMT(Double.parseDouble(rs.getString(rs.getColumnIndex("PREM_REDIR_CHARGE_AMT"))));


					oPlanLK.setPNL_MIN_INCOME(Double.parseDouble(rs.getString(rs.getColumnIndex("PNL_MIN_INCOME"))));
					oPlanLK.setPNL_MAX_INCOME(Double.parseDouble(rs.getString(rs.getColumnIndex("PNL_MAX_INCOME"))));

					*/
					if(rs.getString(rs.getColumnIndex("ISBACKDATE")) != null && (!rs.getString(rs.getColumnIndex("ISBACKDATE")).equalsIgnoreCase("")))
						oPlanLK.setISBACKDATE(rs.getString(rs.getColumnIndex("ISBACKDATE")));
					else
						oPlanLK.setISBACKDATE(null);

					if(rs.getString(rs.getColumnIndex("NOOFMONTH")) != null && (!rs.getString(rs.getColumnIndex("NOOFMONTH")).equalsIgnoreCase("")))
						oPlanLK.setNOOFMONTH(Integer.parseInt(rs.getString(rs.getColumnIndex("NOOFMONTH"))));
					else
						oPlanLK.setNOOFMONTH(0);
					if(rs.getString(rs.getColumnIndex("UIN")) != null && (!rs.getString(rs.getColumnIndex("UIN")).equalsIgnoreCase("")))
						oPlanLK.setUIN(rs.getString(rs.getColumnIndex("UIN")));
					else
						oPlanLK.setUIN(null);
					if(rs.getString(rs.getColumnIndex("PNL_SA_MULTIPLEOF")) != null && (!rs.getString(rs.getColumnIndex("PNL_SA_MULTIPLEOF")).equalsIgnoreCase("")))
						oPlanLK.setPNL_SA_MULTIPLEOF(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_SA_MULTIPLEOF"))));
					else
						oPlanLK.setPNL_SA_MULTIPLEOF(0);
					if(rs.getString(rs.getColumnIndex("PNL_AGENTTYPE")) != null && (!rs.getString(rs.getColumnIndex("PNL_AGENTTYPE")).equalsIgnoreCase("")))
						oPlanLK.setPNL_AGENTTYPE(rs.getString(rs.getColumnIndex("PNL_AGENTTYPE")));
					else
						oPlanLK.setPNL_AGENTTYPE(null);
					if(rs.getString(rs.getColumnIndex("PNL_ANNUITY_FREQUENCY")) != null && (!rs.getString(rs.getColumnIndex("PNL_ANNUITY_FREQUENCY")).equalsIgnoreCase("")))
						oPlanLK.setPNL_ANNUITY_FREQUENCY(rs.getString(rs.getColumnIndex("PNL_ANNUITY_FREQUENCY")));
					else
						oPlanLK.setPNL_ANNUITY_FREQUENCY(null);

					if(rs.getString(rs.getColumnIndex("URALLOW")) != null && (!rs.getString(rs.getColumnIndex("URALLOW")).equalsIgnoreCase("")))
						oPlanLK.setURALLOW(rs.getString(rs.getColumnIndex("URALLOW")));
					else
						oPlanLK.setURALLOW(null);
					if(rs.getString(rs.getColumnIndex("URPERCENT")) != null && (!rs.getString(rs.getColumnIndex("URPERCENT")).equalsIgnoreCase("")))
						oPlanLK.setURPERCENT(Double.parseDouble(rs.getString(rs.getColumnIndex("URPERCENT"))));
					else
						oPlanLK.setURPERCENT(0);

					if(rs.getString(rs.getColumnIndex("PNL_START_PT")) != null && (!rs.getString(rs.getColumnIndex("PNL_START_PT")).equalsIgnoreCase("")))
						oPlanLK.setPNL_START_PT(rs.getString(rs.getColumnIndex("PNL_START_PT")));
					else
						oPlanLK.setPNL_START_PT(null);
					//oPlanLK.setHQGRP(rs.getString(rs.getColumnIndex("HQGRP")));
					oHashMap.put(rs.getString(rs.getColumnIndex("PNL_CODE")), oPlanLK);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}


	public static HashMap getFundDeatils(SQLiteDatabase db) {
		HashMap oMap= new HashMap();

		Cursor rs;
		FundDetailsLK oFundDetails;
		try {

			rs = db.rawQuery("SELECT distinct pnl.pnl_pgl_id, fdl.fdl_code as code,fdl.fdl_description||' ('||fdl.sfin_number||')' as description,fdl.fdl_fund_mgmt_chrg as charge,fdl.FDL_PERFORMANCE_3M as PERF_3M,fdl.FDL_PERFORMANCE_6M as PERF_6M,fdl.FDL_PERFORMANCE_1Y as PERF_1Y,fdl.FDL_PERFORMANCE_2Y as PERF_2Y, fdl.FDL_PERFORMANCE_3Y as PERF_3Y,fdl.FDL_PERFORMANCE_4Y as PERF_4Y,fdl.FDL_PERFORMANCE_5Y as PERF_5Y,fdl.FDL_PERFORMANCE_INCEPTION as PERF_INCEPTION FROM lp_pfx_plan_fund_xref pfx,lp_pnl_plan_lk pnl,lp_fdl_fund_lk fdl WHERE  pfx.pfx_pnl_id=pnl.pnl_id  AND pfx.pfx_fdl_id=fdl.fdl_id AND  (pfx.end_date >= strftime('%d-%m-%Y', 'now') OR  pfx.end_date IS NULL ) order by  pnl.pnl_pgl_id", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oFundDetails=new FundDetailsLK();
					oFundDetails.setPNL_PGL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PNL_PGL_ID"))));
					oFundDetails.setCODE(rs.getString(rs.getColumnIndex("code")));
					oFundDetails.setDESCRIPTION(rs.getString(rs.getColumnIndex("description")));
					oFundDetails.setFUND_MGMT_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("charge"))));
					oFundDetails.setPERF_3M(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_3M"))));
					oFundDetails.setPERF_6M(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_6M"))));
					oFundDetails.setPERF_1Y(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_1Y"))));
					oFundDetails.setPERF_2Y(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_2Y"))));
					oFundDetails.setPERF_3Y(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_3Y"))));
					oFundDetails.setPERF_4Y(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_4Y"))));
					oFundDetails.setPERF_5Y(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_5Y"))));
					oFundDetails.setPERF_INCEPTION(Double.parseDouble(rs.getString(rs.getColumnIndex("PERF_INCEPTION"))));
					oMap.put(rs.getString(rs.getColumnIndex("code")), oFundDetails);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oMap;

	}

	public static ArrayList getPGLGrpMinPremLK(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PGLGrpMinPremLK oMinPrem;
		try {

			rs = db.rawQuery("select PGL_GRP_ID,PGL_PREMIUM_MODE,PGL_MIN_PREM_AMT,PGL_MAX_PREM_AMT," +
					"PGL_MIN_PPT,PGL_MAX_PPT,PGL_MIN_SUM_ASSURED,PGL_MAX_SUM_ASSURED from LP_PGL_GRP_MIN_PREM_LK", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oMinPrem=new PGLGrpMinPremLK();
					oMinPrem.setPGL_GRP_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_GRP_ID"))));
					oMinPrem.setPGL_PREMIUM_MODE(rs.getString(rs.getColumnIndex("PGL_PREMIUM_MODE")));
					oMinPrem.setPGL_MIN_PREM_AMT(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_MIN_PREM_AMT"))));
					oMinPrem.setPGL_MAX_PREM_AMT(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_MAX_PREM_AMT"))));
					oMinPrem.setPGL_MIN_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_MIN_PPT"))));
					oMinPrem.setPGL_MAX_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_MAX_PPT"))));
					oMinPrem.setPGL_MIN_SUM_ASSURED(Integer.parseInt(rs.getString(rs.getColumnIndex("PGL_MIN_SUM_ASSURED"))));
					oMinPrem.setPGL_MAX_SUM_ASSURED(Long.parseLong(rs.getString(rs.getColumnIndex("PGL_MAX_SUM_ASSURED"))));
					oList.add(oMinPrem);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getPNLMinPremLK(SQLiteDatabase db,String planCode) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PNLMinPremLK oMinPrem;
		try {

			rs = db.rawQuery("select PMML_ID,PML_PLAN_CD,PML_PREMIUM_MODE,PML_MIN_PREM_AMT,PML_MAX_PREM_AMT from LP_PNL_MIN_PREM_LK WHERE PML_PLAN_CD = ?", new String[]{planCode});

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oMinPrem=new PNLMinPremLK();
					oMinPrem.setPMML_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PMML_ID"))));
					oMinPrem.setPML_PLAN_CD(rs.getString(rs.getColumnIndex("PML_PLAN_CD")));
					oMinPrem.setPML_PREMIUM_MODE(rs.getString(rs.getColumnIndex("PML_PREMIUM_MODE")));
					oMinPrem.setPML_MIN_PREM_AMT(((Double) Double.parseDouble(rs.getString(rs.getColumnIndex("PML_MIN_PREM_AMT")))).intValue());
					oMinPrem.setPML_MAX_PREM_AMT(((Double) Double.parseDouble(rs.getString(rs.getColumnIndex("PML_MAX_PREM_AMT")))).intValue());
					/*

					// commented by Akhil on 03-07-2015 12:40:00 as the corresponding columns not available in sqlite table

					oMinPrem.setPML_MIN_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_MIN_PPT"))));
					oMinPrem.setPML_MAX_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("PML_MAX_PPT"))));
					oMinPrem.setPML_ANL_PREM_AMT(Long.parseLong(rs.getString(rs.getColumnIndex("PML_ANL_PREM_AMT"))));*/

					oList.add(oMinPrem);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}


	public static ArrayList getPNLTermPptLK(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PNLTermPptLK oMinPrem;
		try {

			rs = db.rawQuery("select PNL_CODE,TERM_FLAG,MIN_TERM,MAX_TERM,TERM_FORMULA,TERM,PPT_FLAG," +
					"PPT_FORMULA,PPT,MATURITY_AGE,MIN_VESTING_AGE " +
					"from LP_PNL_TERM_PPT_LK", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oMinPrem=new PNLTermPptLK();
					oMinPrem.setPNL_CODE(rs.getString(rs.getColumnIndex("PNL_CODE")));
					oMinPrem.setTERM_FLAG(rs.getString(rs.getColumnIndex("TERM_FLAG")));
					oMinPrem.setMIN_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("MIN_TERM"))));
					oMinPrem.setMAX_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("MAX_TERM"))));
					oMinPrem.setTERM_FORMULA(rs.getString(rs.getColumnIndex("TERM_FORMULA")));
					oMinPrem.setTERM(Integer.parseInt(rs.getString(rs.getColumnIndex("TERM"))));
					oMinPrem.setPPT_FLAG(rs.getString(rs.getColumnIndex("PPT_FLAG")));
					oMinPrem.setPPT_FORMULA(rs.getString(rs.getColumnIndex("PPT_FORMULA")));
					oMinPrem.setPPT(Integer.parseInt(rs.getString(rs.getColumnIndex("PPT"))));
					oMinPrem.setMATURITY_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("MATURITY_AGE"))));
					oMinPrem.setMIN_VESTING_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("MIN_VESTING_AGE"))));

					oList.add(oMinPrem);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}


	public static ArrayList getRMLRiderPremMultipleLK(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		RMLRiderPremMultipleLK oRiderPrem;
		try {

			rs = db.rawQuery("select RML_ID,RML_RIDER_CD,RML_SEX,RML_TERM,RML_AGE,RML_PREM_MULT,RML_BAND from LP_RML_RIDER_PREM_MULTIPLE_LK", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oRiderPrem=new RMLRiderPremMultipleLK();
					oRiderPrem.setRML_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RML_ID"))));
					oRiderPrem.setRML_RIDER_CD(rs.getString(rs.getColumnIndex("RML_RIDER_CD")));
					oRiderPrem.setRML_SEX(rs.getString(rs.getColumnIndex("RML_SEX")));
					oRiderPrem.setRML_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("RML_TERM"))));
					oRiderPrem.setRML_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("RML_AGE"))));
					oRiderPrem.setRML_PREM_MULT(Double.parseDouble(rs.getString(rs.getColumnIndex("RML_PREM_MULT"))));
					oRiderPrem.setRML_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("RML_BAND"))));

					oList.add(oRiderPrem);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getPmdPlanPremiumMultDisc(SQLiteDatabase db, String planCode) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PmdPlanPremiumMultDisc oPremMulti;
		try {

			rs = db.rawQuery("SELECT PMD_PLAN_CODE,PMD_MIN_SA,PMD_MAX_SA,PMD_DISC_RATE FROM LP_PMD_PLAN_PREMIUM_MULT_DISC WHERE PMD_PLAN_CODE = ?", new String[]{planCode});

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPremMulti=new PmdPlanPremiumMultDisc();
					oPremMulti.setPMD_PLAN_CODE(rs.getString(rs.getColumnIndex("PMD_PLAN_CODE")));
					/*

					commented by Akhil on 03-07-2015 12:44:00 as the corresponding columns not available in sqlite table

					oPremMulti.setPMD_RIDER_CODE(rs.getString(rs.getColumnIndex("PMD_RIDER_CODE")));
					 */
					oPremMulti.setPMD_MIN_SA(Long.parseLong(rs.getString(rs.getColumnIndex("PMD_MIN_SA"))));
					oPremMulti.setPMD_MAX_SA(Long.parseLong(rs.getString(rs.getColumnIndex("PMD_MAX_SA"))));
					oPremMulti.setPMD_DISC_RATE(Double.parseDouble(rs.getString(rs.getColumnIndex("PMD_DISC_RATE"))));
					oList.add(oPremMulti);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getPBAProductBand(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PBAProductBand oPBAProductBand;
		try {

			rs = db.rawQuery("SELECT PBA_PLAN_CODE,PBA_RIDER_CODE,PBA_TYPE,PBA_BAND,PBA_SUM_ASSURED,PBA_SEQ FROM LP_PBA_PRODUCT_BAND ORDER BY PBA_PLAN_CODE,PBA_SUM_ASSURED", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPBAProductBand = new PBAProductBand();
					oPBAProductBand.setPBA_PLAN_CODE(rs.getString(rs.getColumnIndex("PBA_PLAN_CODE")));
					oPBAProductBand.setPBA_RIDER_CODE(rs.getString(rs.getColumnIndex("PBA_RIDER_CODE")));
					oPBAProductBand.setPBA_TYPE(rs.getString(rs.getColumnIndex("PBA_TYPE")));
					oPBAProductBand.setPBA_BAND(rs.getString(rs.getColumnIndex("PBA_BAND")));
					oPBAProductBand.setPBA_SUM_ASSURED(Long.parseLong(rs.getString(rs.getColumnIndex("PBA_SUM_ASSURED"))));
					oPBAProductBand.setPBA_SEQ(Integer.parseInt(rs.getString(rs.getColumnIndex("PBA_SEQ"))));
					oList.add(oPBAProductBand);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static HashMap getImagePath(SQLiteDatabase db) {
		HashMap<Integer,TempImagePath> oHashMap=new HashMap<Integer,TempImagePath>();

		Cursor rs;
		TempImagePath oTempImagePath=null;
		try
		{

			rs = db.rawQuery("SELECT tip_pgl_id,tip_image_name FROM lp_template_image_path", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oTempImagePath=new TempImagePath();
					oTempImagePath.setTip_pgl_id(Integer.parseInt(rs.getString(rs.getColumnIndex("TIP_PGL_ID"))));
					oTempImagePath.setTip_image_name(rs.getString(rs.getColumnIndex("TIP_IMAGE_NAME")));
					oHashMap.put(Integer.parseInt(rs.getString(rs.getColumnIndex("TIP_PGL_ID"))), oTempImagePath);
				}
			}
			rs.close();
		} catch (Exception e) {
			CommonConstant.printLog("d", "Error occured in getImagePath()");
		}
		return oHashMap;
	}

	public static HashMap getRDLRiderLk(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		RDLRiderLk ORiderLk;
		try {

			rs = db.rawQuery("SELECT RDL_ID,RDL_CODE,RDL_DESCRIPTION,RDL_RTL_ID,RDL_SERVICE_TAX,ISSIMPLIFIED," +
									"ISANNUITYPRODUCT,ISBACKDATE,NOOFMONTH,NBFC,FIXCVGID,MINOCC,MAXOCC,RDL_UIN FROM LP_RDL_RIDER_LK", new String[0]); //modified by jayesh for Rider UIN
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					ORiderLk=new RDLRiderLk();

					ORiderLk.setRDL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RDL_ID"))));
					ORiderLk.setRDL_CODE(rs.getString(rs.getColumnIndex("RDL_CODE")));
					ORiderLk.setRDL_DESCRIPTION(rs.getString(rs.getColumnIndex("RDL_DESCRIPTION")));
					ORiderLk.setRDL_RTL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RDL_RTL_ID"))));
					ORiderLk.setRDL_SERVICE_TAX(Double.parseDouble(rs.getString(rs.getColumnIndex("RDL_SERVICE_TAX"))));
					ORiderLk.setISSIMPLIFIED(rs.getString(rs.getColumnIndex("ISSIMPLIFIED")));
					ORiderLk.setISANNUITYPRODUCT(rs.getString(rs.getColumnIndex("ISANNUITYPRODUCT")));
					ORiderLk.setISBACKDATE(rs.getString(rs.getColumnIndex("ISBACKDATE")));
					ORiderLk.setNOOFMONTH(Integer.parseInt(rs.getString(rs.getColumnIndex("NOOFMONTH"))));
					ORiderLk.setNBFC(rs.getString(rs.getColumnIndex("NBFC")));
					if(rs.getString(rs.getColumnIndex("FIXCVGID"))==null)
						ORiderLk.setFIXCVGID(0);
					else
						ORiderLk.setFIXCVGID(Integer.parseInt(rs.getString(rs.getColumnIndex("FIXCVGID"))));
					ORiderLk.setMINOCC(Integer.parseInt(rs.getString(rs.getColumnIndex("MINOCC"))));
					ORiderLk.setMAXOCC(Integer.parseInt(rs.getString(rs.getColumnIndex("MAXOCC"))));
					ORiderLk.setRDL_UIN(rs.getString(rs.getColumnIndex("RDL_UIN")));	//added by jayesh for Rider UIN
					oHashMap.put(rs.getString(rs.getColumnIndex("RDL_CODE")), ORiderLk);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}
	/*public static void closeResources(Connection conn,Statement stmt,ResultSet rs){
		try{
			if(rs!= null)
				rs.close();

			if(stmt!= null)
				stmt.close();

			if(conn!= null)
				conn.close();

		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static ArrayList getPMVPlanMaturityValue(SQLiteDatabase db, String pnlId) {
		ArrayList planMaturityValueLst =new ArrayList();

		Cursor rs;
		PMVPlanMaturityValue oPMVPlanMaturityValue;
		try {

			rs = db.rawQuery("SELECT PMV_ID,PMV_PNL_ID,PMV_GENDER,PMV_BAND,PMV_AGE,PMV_OCCUR,PMV_NSP,PMV_USE,PMV_NUM,PMV_DUR1,"
									+"PMV_CVAL1,PMV_DUR2,PMV_CVAL2,PMV_DUR3,PMV_CVAL3,PMV_DUR4,PMV_CVAL4,PMV_DUR5,PMV_CVAL5,PMV_DUR6,"
									+"PMV_CVAL6,PMV_DUR7,PMV_CVAL7,PMV_DUR8,PMV_CVAL8,PMV_DUR9,PMV_CVAL9,PMV_DUR10,PMV_CVAL10 FROM "
									+"LP_PMV_PLAN_MATURITY_VALUE WHERE PMV_PNL_ID = ?", new String[]{pnlId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPMVPlanMaturityValue = new PMVPlanMaturityValue();
					oPMVPlanMaturityValue.setPMV_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_ID"))));
					oPMVPlanMaturityValue.setPMV_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_PNL_ID"))));
					oPMVPlanMaturityValue.setPMV_GENDER(rs.getString(rs.getColumnIndex("PMV_GENDER")));
					oPMVPlanMaturityValue.setPMV_BAND(rs.getString(rs.getColumnIndex("PMV_BAND")));
					oPMVPlanMaturityValue.setPMV_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_AGE"))));
					oPMVPlanMaturityValue.setPMV_OCCUR(rs.getString(rs.getColumnIndex("PMV_OCCUR")));
					oPMVPlanMaturityValue.setPMV_NSP(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_NSP"))));
					oPMVPlanMaturityValue.setPMV_USE(rs.getString(rs.getColumnIndex("PMV_USE")));
					oPMVPlanMaturityValue.setPMV_NUM(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_NUM"))));
					oPMVPlanMaturityValue.setPMV_DUR1(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR1"))));
					oPMVPlanMaturityValue.setPMV_CVAL1(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL1"))));
					oPMVPlanMaturityValue.setPMV_DUR2(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR2"))));
					oPMVPlanMaturityValue.setPMV_CVAL2(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL2"))));
					oPMVPlanMaturityValue.setPMV_DUR3(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR3"))));
					oPMVPlanMaturityValue.setPMV_CVAL3(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL3"))));
					oPMVPlanMaturityValue.setPMV_DUR4(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR4"))));
					oPMVPlanMaturityValue.setPMV_CVAL4(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL4"))));
					oPMVPlanMaturityValue.setPMV_DUR5(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR5"))));
					oPMVPlanMaturityValue.setPMV_CVAL5(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL5"))));
					oPMVPlanMaturityValue.setPMV_DUR6(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR6"))));
					oPMVPlanMaturityValue.setPMV_CVAL6(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL6"))));
					oPMVPlanMaturityValue.setPMV_DUR7(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR7"))));
					oPMVPlanMaturityValue.setPMV_CVAL7(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL7"))));
					oPMVPlanMaturityValue.setPMV_DUR8(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR8"))));
					oPMVPlanMaturityValue.setPMV_CVAL8(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL8"))));
					oPMVPlanMaturityValue.setPMV_DUR9(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR9"))));
					oPMVPlanMaturityValue.setPMV_CVAL9(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL9"))));
					oPMVPlanMaturityValue.setPMV_DUR10(Integer.parseInt(rs.getString(rs.getColumnIndex("PMV_DUR10"))));
					oPMVPlanMaturityValue.setPMV_CVAL10(Double.parseDouble(rs.getString(rs.getColumnIndex("PMV_CVAL10"))));

					planMaturityValueLst.add( oPMVPlanMaturityValue);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return planMaturityValueLst;
	}

	public static ArrayList getPFRPlanFormulaRef(SQLiteDatabase db) {
		ArrayList planFormulaXrefLst =new ArrayList();

		Cursor rs;
		PFRPlanFormulaRef oPFRPlanFormulaRef;
		try
		{

			rs = db.rawQuery("SELECT PER_PLAN_CD,PER_FRM_CD,PER_FRM_TYPE,PER_FRM_EXP "
									+" FROM LP_PER_PLAN_FORMULA_REF", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPFRPlanFormulaRef = new PFRPlanFormulaRef();
					oPFRPlanFormulaRef.setPFR_PLAN_CD(rs.getString(rs.getColumnIndex("PER_PLAN_CD")));
					oPFRPlanFormulaRef.setPFR_FRM_CD(rs.getString(rs.getColumnIndex("PER_FRM_CD")));
					oPFRPlanFormulaRef.setPFR_FRM_TYPE(rs.getString(rs.getColumnIndex("PER_FRM_TYPE")));
					oPFRPlanFormulaRef.setPFR_FRM_EXP(rs.getString(rs.getColumnIndex("PER_FRM_EXP")));

					planFormulaXrefLst.add(oPFRPlanFormulaRef);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return planFormulaXrefLst;
	}

	public static ArrayList getFEDFormulaExtDesc(SQLiteDatabase db) {
		ArrayList formulaExtDescLst =new ArrayList();

		Cursor rs;
		FEDFormulaExtDesc oFEDFormulaExtDesc;
		try {

			rs = db.rawQuery("SELECT FED_ID,FED_OBJ_SEQ,FED_OBJ_OPND,FED_OBJ_OPND_TYPE,FED_OBJ_OPTR, "
									+ "FED_OBJ_TYPE FROM LP_FED_FORMULA_EXT_DESC", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oFEDFormulaExtDesc = new FEDFormulaExtDesc();
					oFEDFormulaExtDesc.setFED_ID(rs.getString(rs.getColumnIndex("FED_ID")));
					oFEDFormulaExtDesc.setFED_OBJ_SEQ(Integer.parseInt(rs.getString(rs.getColumnIndex("FED_OBJ_SEQ"))));
					oFEDFormulaExtDesc.setFED_OBJ_OPND(rs.getString(rs.getColumnIndex("FED_OBJ_OPND")));
					oFEDFormulaExtDesc.setFED_OBJ_OPND_TYPE(rs.getString(rs.getColumnIndex("FED_OBJ_OPND_TYPE")));
					oFEDFormulaExtDesc.setFED_OBJ_OPTR(rs.getString(rs.getColumnIndex("FED_OBJ_OPTR")));
					oFEDFormulaExtDesc.setFED_OBJ_TYPE(rs.getString(rs.getColumnIndex("FED_OBJ_TYPE")));

					formulaExtDescLst.add(oFEDFormulaExtDesc);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return formulaExtDescLst;
	}

	public static ArrayList getAddress(SQLiteDatabase db) {
		ArrayList alistAdd =new ArrayList();

		Cursor rs;
		AddressBO oAddressBO;
		try {

			rs = db.rawQuery("SELECT CO_NAME,CO_ADDRESS,CO_HELPLINE,SIS_VERSION,FILLER1 FROM LP_COMPANY_MASTER", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oAddressBO = new AddressBO();
					oAddressBO.setCO_NAME(rs.getString(rs.getColumnIndex("CO_NAME")));
					oAddressBO.setCO_ADDRESS(rs.getString(rs.getColumnIndex("CO_ADDRESS")));
					oAddressBO.setCO_HELP_LINE(rs.getString(rs.getColumnIndex("CO_HELPLINE")));
					oAddressBO.setSIS_VERSION(rs.getString(rs.getColumnIndex("SIS_VERSION")));
					oAddressBO.setFILLER1(rs.getString(rs.getColumnIndex("FILLER1")));
					alistAdd.add(oAddressBO);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return alistAdd;
	}

	public static HashMap getPGFPlanGroupFeature(SQLiteDatabase db, String pglId) {
		HashMap PlanDeatails=new HashMap();

		Cursor rs;
		PGFPlanGroupFeature oPlanFeature;
		try {

			rs = db.rawQuery("select PGF_PNL_CODE,PGF_FEATURE FROM LP_PGF_PLAN_GROUP_FEATURE WHERE PGF_PGL_ID = ?", new String[]{pglId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPlanFeature=new PGFPlanGroupFeature();
					oPlanFeature.setPGF_PNL_CODE(rs.getString(rs.getColumnIndex("PGF_PNL_CODE")));
					oPlanFeature.setPGF_FEATURE(rs.getString(rs.getColumnIndex("PGF_FEATURE")));

					PlanDeatails.put(rs.getString(rs.getColumnIndex("PGF_PNL_CODE")), oPlanFeature);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return PlanDeatails;
	}


	public static ArrayList getPACPremiumAllocationCharges(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		PACPremiumAllocationCharges oPAC;
		try {

			rs = db.rawQuery("select PAC_ID,PAC_MIN_BAND,PAC_MAX_BAND,PAC_MIN_PREM,PAC_MAX_PREM,PAC_CHARGE FROM LP_PAC_Premium_Allocation_Charges", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPAC=new PACPremiumAllocationCharges();
					oPAC.setPAC_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PAC_ID"))));
					oPAC.setPAC_MIN_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("PAC_MIN_BAND"))));
					oPAC.setPAC_MAX_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("PAC_MAX_BAND"))));
					oPAC.setPAC_MIN_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("PAC_MIN_PREM"))));
					oPAC.setPAC_MAX_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("PAC_MAX_PREM"))));
					oPAC.setPAC_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("PAC_CHARGE"))));
					oList.add(oPAC);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getACMAdminChargesMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		ACMAdminChargesMaster oACM;
		try {

			rs = db.rawQuery("SELECT ACM_ID,ACM_MIN_PREM,ACM_MAX_PREM,ACM_CHARGE FROM LP_ACM_Admin_Charges_Master", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oACM=new ACMAdminChargesMaster();
					oACM.setACM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("ACM_ID"))));
					oACM.setACM_MIN_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("ACM_MIN_PREM"))));
					oACM.setACM_MAX_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("ACM_MAX_PREM"))));
					oACM.setACM_CHARGE(Long.parseLong(rs.getString(rs.getColumnIndex("ACM_CHARGE"))));

					oList.add(oACM);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}
	public static ArrayList getMCMMortilityChargesMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		MCMMortilityChargesMaster oMCM;
		try {

			rs = db.rawQuery("select MCM_ID,MCM_AGE,MCM_SEX,MCM_MORT_CHARGE from LP_MCM_Mortility_Charges_Master", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oMCM=new MCMMortilityChargesMaster();
					oMCM.setMCM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("MCM_ID"))));
					oMCM.setMCM_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("MCM_AGE"))));
                    oMCM.setMCM_SEX(rs.getString(rs.getColumnIndex("MCM_SEX")));
					oMCM.setMCM_MORT_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("MCM_MORT_CHARGE"))));

					oList.add(oMCM);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static HashMap getPCRPlanChargesRef(SQLiteDatabase db, String pnlId) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		PCRPlanChargesRef oPCR;
		try {

			rs = db.rawQuery("select PCR_PNL_ID,PCR_ADMIN_ID,PCR_PREM_ID,PCR_MORT_ID,PCR_WOP_ID,PCR_FIB_ID,PCR_SUR_ID,PCR_GMM_ID,PCR_COMM_ID from LP_PCR_Plan_Charges_Ref WHERE PCR_PNL_ID = ?", new String[]{pnlId});

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oPCR=new PCRPlanChargesRef();
					oPCR.setPCR_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_PNL_ID"))));
					oPCR.setPCR_ADMIN_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_ADMIN_ID"))));
					oPCR.setPCR_PREM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_PREM_ID"))));
					oPCR.setPCR_MORT_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_MORT_ID"))));
					oPCR.setPCR_WOP_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_WOP_ID"))));
					oPCR.setPCR_FIB_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_FIB_ID"))));
					oPCR.setPCR_SUR_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_SUR_ID"))));
					oPCR.setPCR_GMM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_GMM_ID"))));
					oPCR.setPCR_COMM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_COMM_ID"))));
					//oList.add(oPCR);
					oHashMap.put(Integer.parseInt(rs.getString(rs.getColumnIndex("PCR_PNL_ID"))), oPCR);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}

	public static ArrayList getWCMWOPChargesMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		WCMWOPChargesMaster oWCM;
		try {

			rs = db.rawQuery("select WCM_WOP_ID,WCM_AGE,WCM_BAND,WCM_CHARGE from LP_WCM_WOP_Charges_Master", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oWCM=new WCMWOPChargesMaster();
					oWCM.setWCM_WOP_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("WCM_WOP_ID"))));
					oWCM.setWCM_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("WCM_AGE"))));
					oWCM.setWCM_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("WCM_BAND"))));
					oWCM.setWCM_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("WCM_CHARGE"))));

					oList.add(oWCM);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getFCMFIBChargesMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		FCMFIBChargesMaster oFCMFIB;
		try {

			rs = db.rawQuery("select FCM_FIB_ID,FCM_AGE,FCM_BAND,FCM_CHARGE from LP_FCM_FIB_Charges_Master", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oFCMFIB=new FCMFIBChargesMaster();
					oFCMFIB.setFCM_FIB_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("FCM_FIB_ID"))));
					oFCMFIB.setFCM_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("FCM_AGE"))));
					oFCMFIB.setFCM_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("FCM_BAND"))));
					oFCMFIB.setFCM_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("FCM_CHARGE"))));
					oList.add(oFCMFIB);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getSURSurrenderChargeMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		SURSurrenderChargeMaster oSUR;
		try {

			rs = db.rawQuery("select SUR_ID,SUR_MIN_BAND,SUR_MAX_BAND,SUR_MIN_PREM,SUR_MAX_PREM,SUR_CHARGE,SUR_AMOUNT from LP_SUR_SURRENDER_CHARGE_MASTER", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oSUR=new SURSurrenderChargeMaster();
					oSUR.setSUR_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("SUR_ID"))));
					oSUR.setSUR_MIN_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("SUR_MIN_BAND"))));
					oSUR.setSUR_MAX_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("SUR_MAX_BAND"))));
					oSUR.setSUR_MIN_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("SUR_MIN_PREM"))));
					oSUR.setSUR_MAX_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("SUR_MAX_PREM"))));
					oSUR.setSUR_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("SUR_CHARGE"))));
					oSUR.setSUR_AMOUNT(Long.parseLong(rs.getString(rs.getColumnIndex("SUR_AMOUNT"))));
					oList.add(oSUR);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

	public static ArrayList getGuaranteedMaturityBonusMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		GuaranteedMaturityBonusMaster oGMB;
		try {

			rs = db.rawQuery("select GMM_ID,GMM_MIN_BAND,GMM_MAX_BAND,GMM_BONUS from LP_GMM_GUARANTEED_MATURITY_MASTER", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oGMB=new GuaranteedMaturityBonusMaster();
					oGMB.setGMM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("GMM_ID"))));
					oGMB.setGMM_MIN_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("GMM_MIN_BAND"))));
					oGMB.setGMM_MAX_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("GMM_MAX_BAND"))));
					oGMB.setGMM_BONUS(Double.parseDouble(rs.getString(rs.getColumnIndex("GMM_BONUS"))));
					oList.add(oGMB);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}
	public static ArrayList getXmlMetadataMaster(SQLiteDatabase db) {

		Cursor rs;
		XMMXmlMetadataMaster oxml;
		ArrayList oArrayList=new ArrayList();
		try {

			rs = db.rawQuery("select PNL_CODE,SRNO,ELEMENT_NAME from lp_xmm_xml_metadata_master", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oxml=new XMMXmlMetadataMaster();
					oxml.setPNL_CODE(rs.getString(rs.getColumnIndex("PNL_CODE")));
					oxml.setSRNO(Integer.parseInt(rs.getString(rs.getColumnIndex("SRNO"))));
					oxml.setELEMENT_NAME(rs.getString(rs.getColumnIndex("ELEMENT_NAME")));
					oArrayList.add(oxml);
				}
				rs.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oArrayList;

	}
	public static ArrayList getCCMCommissionChargeMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		CCMCommissionChargeMaster oCCM;
		try {

			rs = db.rawQuery("select CCM_ID,CCM_MIN_TERM,CCM_MAX_TERM,CCM_COMMISSION from lp_ccm_commission_charge_master", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oCCM=new CCMCommissionChargeMaster();
					oCCM.setCCM_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("CCM_ID"))));
					oCCM.setCCM_MIN_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("CCM_MIN_TERM"))));
					oCCM.setCCM_MAX_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("CCM_MAX_TERM"))));
					oCCM.setCCM_COMMISSION(Double.parseDouble(rs.getString(rs.getColumnIndex("CCM_COMMISSION"))));

					oList.add(oCCM);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}
	public static ArrayList getMFMMaturityFactorMaster(SQLiteDatabase db, String pnlId) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		MFMMaturityFactorMaster oMFM;
		try {

			rs = db.rawQuery("select MFM_PNL_ID, MFM_BAND, MFM_AGE, MFM_USE_FLG, MFM_VALUE, MFM_SEX from lp_mfm_maturity_factor_master where MFM_PNL_ID = ?", new String[]{pnlId});

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oMFM=new MFMMaturityFactorMaster();
					oMFM.setMFF_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("MFM_PNL_ID"))));
					oMFM.setMFM_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("MFM_BAND"))));
					oMFM.setMFM_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("MFM_AGE"))));
					oMFM.setMFM_USE_FLG(rs.getString(rs.getColumnIndex("MFM_USE_FLG")));
					oMFM.setMFM_VALUE(Double.parseDouble(rs.getString(rs.getColumnIndex("MFM_VALUE"))));
					oMFM.setMFM_SEX((rs.getString(rs.getColumnIndex("MFM_SEX"))));
					oList.add(oMFM);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}
	public static ArrayList getGAIGuarAnnIncChMaster(SQLiteDatabase db, String pnlId) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		GAIGuarAnnIncChMaster oGAI;
		try {

			rs = db.rawQuery("select gai_id,gai_pnl_id,gai_min_term,gai_max_term,gai_min_prem,gai_max_prem,gai_flag,gai_charge,gai_min_ppt,gai_max_ppt from LP_GAI_Guar_Ann_Inc_Ch_Master where GAI_PNL_ID = ?", new String[]{pnlId});

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oGAI=new GAIGuarAnnIncChMaster();
					oGAI.setGAI_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_ID"))));
					oGAI.setGAI_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_PNL_ID"))));
					oGAI.setGAI_MIN_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_MIN_TERM"))));
					oGAI.setGAI_MAX_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_MAX_TERM"))));
					oGAI.setGAI_MIN_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("GAI_MIN_PREM"))));
					oGAI.setGAI_MAX_PREM(Long.parseLong(rs.getString(rs.getColumnIndex("GAI_MAX_PREM"))));
					oGAI.setGAI_FLAG(rs.getString(rs.getColumnIndex("GAI_FLAG")));
					oGAI.setGAI_CHARGE(Double.parseDouble(rs.getString(rs.getColumnIndex("GAI_CHARGE"))));
                    if(rs.getString(rs.getColumnIndex("GAI_MIN_PPT"))!=null)
					oGAI.setGAI_MIN_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_MIN_PPT"))));
                    if(rs.getString(rs.getColumnIndex("GAI_MAX_PPT"))!=null)
                    oGAI.setGAI_MAX_PPT(Integer.parseInt(rs.getString(rs.getColumnIndex("GAI_MAX_PPT"))));
					oList.add(oGAI);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;
	}

	public static HashMap getSPVSysParamValues(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();

		Cursor rs;
		SPVSysParamValues oSPVSysParamValues;
		try {

			rs = db.rawQuery("select SPV_SYS_PARAM,SPV_VALUE from LP_SPV_SYS_PARAM_VALUES", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oSPVSysParamValues = new SPVSysParamValues();

					oSPVSysParamValues.setSPV_SYS_PARAM(rs.getString(rs.getColumnIndex("SPV_SYS_PARAM")));
					oSPVSysParamValues.setSPV_VALUE(rs.getString(rs.getColumnIndex("SPV_VALUE")));
					oHashMap.put(rs.getString(rs.getColumnIndex("SPV_SYS_PARAM")), oSPVSysParamValues);

				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oHashMap;

	}

	// Added By Mohammad Rashid 06-Jan-2013 For Mahalife Gold Plus - Inflation Protection Cover Values
	public static ArrayList getIPCValues(SQLiteDatabase db, String pnlId) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		IPCValues oIPCValues;
		try {

			rs = db.rawQuery("SELECT IPC_ID,IPC_PNL_ID,IPC_SEX,IPC_TERM,IPC_AGE,IPC_PREM_MULT,IPC_BAND,SMOKER_FLAG FROM LP_IPC_INF_PROT_COV_MASTER WHERE IPC_PNL_ID = ?", new String[]{pnlId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);

					oIPCValues=new IPCValues();
					oIPCValues.setIPC_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("IPC_ID"))));
					oIPCValues.setIPC_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("IPC_PNL_ID"))));
					oIPCValues.setIPC_SEX(rs.getString(rs.getColumnIndex("IPC_SEX")));
					oIPCValues.setIPC_TERM(Integer.parseInt(rs.getString(rs.getColumnIndex("IPC_TERM"))));
					oIPCValues.setIPC_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("IPC_AGE"))));
					oIPCValues.setIPC_PREM_MULT(Double.parseDouble(rs.getString(rs.getColumnIndex("IPC_PREM_MULT"))));
					oIPCValues.setIPC_BAND(Integer.parseInt(rs.getString(rs.getColumnIndex("IPC_BAND"))));
					oIPCValues.setSMOKER_FLAG(rs.getString(rs.getColumnIndex("SMOKER_FLAG")));

					oList.add(oIPCValues);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}
	//End

	// Added By Mohammad Rashid 06-Jan-2013 For Mahalife Gold Plus - Inflation Protection Cover Values
	public static ArrayList getRDLCharges(SQLiteDatabase db, String pnlId) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		RDLChargesValue oRDLCharges;
		try {
			CommonConstant.printLog("d", "Inside getRDLCharges");

			rs = db.rawQuery("SELECT RCS_ID,RCS_PNL_ID,RCS_RDL_ID,RCS_GENDER,RCS_BAND,RCS_AGE,RCS_OCCUR,RCS_NSP,RCS_USE,RCS_NUM,RCS_CVAL1,RCS_CVAL2,RCS_CVAL3,RCS_CVAL4,RCS_CVAL5,RCS_CVAL6,RCS_CVAL7,RCS_CVAL8,RCS_CVAL9,RCS_CVAL10,RCS_DUR1,RCS_DUR2,RCS_DUR3,RCS_DUR4,RCS_DUR5,RCS_DUR6,RCS_DUR7,RCS_DUR8,RCS_DUR9,RCS_DUR10 FROM lp_rdl_charges where rcs_pnl_id = ?", new String[]{pnlId});
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);

					oRDLCharges=new RDLChargesValue();
					oRDLCharges.setRCS_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_ID"))));
					oRDLCharges.setRCS_PNL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_PNL_ID"))));
					oRDLCharges.setRCS_RDL_ID(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_RDL_ID"))));
					oRDLCharges.setRCS_GENDER(rs.getString(rs.getColumnIndex("RCS_GENDER")));
					oRDLCharges.setRCS_AGE(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_AGE"))));
					oRDLCharges.setRCS_BAND(rs.getString(rs.getColumnIndex("RCS_BAND")));
					oRDLCharges.setRCS_OCCUR(rs.getString(rs.getColumnIndex("RCS_OCCUR")));
					oRDLCharges.setRCS_NSP(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_NSP"))));
					oRDLCharges.setRCS_USE(rs.getString(rs.getColumnIndex("RCS_USE")));
					oRDLCharges.setRCS_NUM(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_NUM"))));
					oRDLCharges.setRCS_CVAL1(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL1"))));
					oRDLCharges.setRCS_CVAL2(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL2"))));
					oRDLCharges.setRCS_CVAL3(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL3"))));
					oRDLCharges.setRCS_CVAL4(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL4"))));
					oRDLCharges.setRCS_CVAL5(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL5"))));
					oRDLCharges.setRCS_CVAL6(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL6"))));
					oRDLCharges.setRCS_CVAL7(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL7"))));
					oRDLCharges.setRCS_CVAL8(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL8"))));
					oRDLCharges.setRCS_CVAL9(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL9"))));
					oRDLCharges.setRCS_CVAL10(Double.parseDouble(rs.getString(rs.getColumnIndex("RCS_CVAL10"))));
					oRDLCharges.setRCS_DUR1(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR1"))));
					oRDLCharges.setRCS_DUR2(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR2"))));
					oRDLCharges.setRCS_DUR3(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR3"))));
					oRDLCharges.setRCS_DUR4(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR4"))));
					oRDLCharges.setRCS_DUR5(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR5"))));
					oRDLCharges.setRCS_DUR6(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR6"))));
					oRDLCharges.setRCS_DUR7(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR7"))));
					oRDLCharges.setRCS_DUR8(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR8"))));
					oRDLCharges.setRCS_DUR9(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR9"))));
					oRDLCharges.setRCS_DUR10(Integer.parseInt(rs.getString(rs.getColumnIndex("RCS_DUR10"))));

					oList.add(oRDLCharges);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;

	}

public static ArrayList getAAAFMCValues(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		AAAFMCValues oAAAFMCValues;
		try {

			rs = db.rawQuery("SELECT afc_plan_ID,afc_min_age,afc_max_age,afc_large_cap,afc_whole_life_income,afc_fmc FROM LP_AFC_AAA_FundCharges", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oAAAFMCValues=new AAAFMCValues();
					oAAAFMCValues.setAfc_plan_id(Integer.parseInt(rs.getString(rs.getColumnIndex("afc_plan_ID"))));
					oAAAFMCValues.setAfc_min_age(Integer.parseInt(rs.getString(rs.getColumnIndex("afc_min_age"))));
					oAAAFMCValues.setAfc_max_age(Integer.parseInt(rs.getString(rs.getColumnIndex("afc_max_age"))));
					oAAAFMCValues.setAfc_large_cap(Integer.parseInt(rs.getString(rs.getColumnIndex("afc_large_cap"))));
					oAAAFMCValues.setAfc_whole_life_income(Integer.parseInt(rs.getString(rs.getColumnIndex("afc_whole_life_income"))));
					oAAAFMCValues.setAfc_fmc(Float.parseFloat(rs.getString(rs.getColumnIndex("afc_fmc"))));
					oList.add(oAAAFMCValues);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;
	}
	public static ArrayList getStdTemplateValues(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		STDTemplateValues oStdTemplateValues;
		try {

			rs = db.rawQuery("SELECT STD_PGL_ID,STD_TEMP_DESC,STD_SEQUENCE,STD_MIN_STARTP,STD_MAX_STARTP,STD_MIN_PT,STD_MAX_PT,STD_NO_PAGES,STD_MANDATORY,STD_SHOW,STD_UPDATE_BY,STD_UPDATE_DT,STD_IS_RIDER,STD_RDL_CODE,STD_MAX_NO_PAGES FROM lp_std_sis_template_data ORDER BY STD_PGL_ID,STD_SEQUENCE", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oStdTemplateValues=new STDTemplateValues();
					oStdTemplateValues.setStd_pgl_id(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_PGL_ID"))));
					oStdTemplateValues.setStd_temp_desc(rs.getString(rs.getColumnIndex("STD_TEMP_DESC")));
					oStdTemplateValues.setStd_sequence(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_SEQUENCE"))));
					oStdTemplateValues.setStd_min_startp(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_MIN_STARTP"))));
					oStdTemplateValues.setStd_max_startp(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_MAX_STARTP"))));
					oStdTemplateValues.setStd_min_pt(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_MIN_PT"))));
					oStdTemplateValues.setStd_max_pt(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_MAX_PT"))));
					oStdTemplateValues.setStd_no_pages(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_NO_PAGES"))));
					oStdTemplateValues.setStd_mandatory(rs.getString(rs.getColumnIndex("STD_MANDATORY")));
					oStdTemplateValues.setStd_show(rs.getString(rs.getColumnIndex("STD_SHOW")));
					oStdTemplateValues.setStd_show(rs.getString(rs.getColumnIndex("STD_UPDATE_BY")));
					oStdTemplateValues.setStd_show(rs.getString(rs.getColumnIndex("STD_UPDATE_DT")));
					oStdTemplateValues.setStd_is_rider(rs.getString(rs.getColumnIndex("STD_IS_RIDER")));
					oStdTemplateValues.setStd_rdl_code(rs.getString(rs.getColumnIndex("STD_RDL_CODE")));
					oStdTemplateValues.setStd_max_no_pages(Integer.parseInt(rs.getString(rs.getColumnIndex("STD_MAX_NO_PAGES"))));
					oList.add(oStdTemplateValues);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;
	}

	public static ArrayList getIplIllussPagelist(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		IplIllussPagelist oIplIllussPagelist;
		try {

			rs = db.rawQuery("SELECT IPL_PGL_ID,IPL_MIN_PT,IPL_MAX_PT,IPL_MIN_PAGE,IPL_MAX_PAGE FROM LP_IPL_ILLUSS_PAGELIST", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oIplIllussPagelist=new IplIllussPagelist();
					oIplIllussPagelist.setIpl_pgl_id(Integer.parseInt(rs.getString(rs.getColumnIndex("IPL_PGL_ID"))));
					oIplIllussPagelist.setIpl_min_pt(Integer.parseInt(rs.getString(rs.getColumnIndex("IPL_MIN_PT"))));
					oIplIllussPagelist.setIpl_max_pt(Integer.parseInt(rs.getString(rs.getColumnIndex("IPL_MAX_PT"))));
					oIplIllussPagelist.setIpl_min_page(rs.getString(rs.getColumnIndex("IPL_MIN_PAGE")));
					oIplIllussPagelist.setIpl_max_page(rs.getString(rs.getColumnIndex("IPL_MAX_PAGE")));
					oList.add(oIplIllussPagelist);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;
	}

	public static ArrayList getCommText(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		CommText oCommText;
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		try {

			rs = db.rawQuery("SELECT CMT_PGL_ID,CMT_COMM_TEXT,CMT_UPDATE_BY,CMT_UPDATE_DT FROM LP_COMM_TEXT", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oCommText=new CommText();
					oCommText.setCmt_pgl_id(rs.getString(rs.getColumnIndex("CMT_PGL_ID")));
					oCommText.setCmt_comm_text(rs.getString(rs.getColumnIndex("CMT_COMM_TEXT")));
					oCommText.setCmt_update_by(rs.getString(rs.getColumnIndex("CMT_UPDATE_BY")));
					oCommText.setCmt_update_dt(dateFormat.parse(rs.getString(rs.getColumnIndex("CMT_UPDATE_DT"))));
					oList.add(oCommText);
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oList;
	}

	public static AgentDetails getAgentDetails(SQLiteDatabase db) {
		AgentDetails oAgentDetails = new AgentDetails();;

		Cursor rs;
		//CommText oCommText;
		//DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		try {

			rs = db.rawQuery("Select AGENT_NAME,MOBILENO,AGENT_CD from LP_USERINFO", new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oAgentDetails.setAGENT_NAME(rs.getString(rs.getColumnIndex("AGENT_NAME")));
					oAgentDetails.setAGENT_NUMBER(rs.getString(rs.getColumnIndex("AGENT_CD")));
					oAgentDetails.setAGENT_CONTACTNUMBER(rs.getString(rs.getColumnIndex("MOBILENO")));
				}
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally{
			closeResources(con,cstmt,rs);
		}*/
		return oAgentDetails;
	}

	/*public static HashMap getImagePath(SQLiteDatabase db) {
		HashMap oHashMap=new HashMap();
		Connection con=null;
		Statement cstmt = null;
		ResultSet rs=null;
		TempImagePath oTempImagePath=null;
		try
		{
			con = DBConnection.getConnection();
			cstmt = con.createStatement();
			rs=cstmt.executeQuery("SELECT tip_pgl_id,tip_image_name FROM template_image_path");
			if(rs!=null){
				while(rs.next())
				{
					oTempImagePath=new TempImagePath();
					oTempImagePath.setTip_pgl_id(rs.getInt("tip_pgl_id"));
					oTempImagePath.setTip_image_name(rs.getString("tip_image_name"));
					oHashMap.put(rs.getInt("tip_pgl_id"),oTempImagePath);
				}
			}
		} catch (Exception e) {
			logger.debug("Error occured in getImagePath()");
		}finally{
			closeResources(con,cstmt,rs);
		}
		return oHashMap;
	}

	public static ArrayList getPNLComboLk(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();
		Connection con=null;
		Statement cstmt = null;
		ResultSet rs=null;
		PNLComboLk oPlanComboLK=null;
		try {
			con = DBConnection.getConnection();
			cstmt = con.createStatement();
			rs=cstmt.executeQuery("SELECT PCL_GRP_ID,PCL_PGL_ID,PCL_PROD_TYPE,PCL_SUMASS_PER,PCL_UPDATE_BY,PCL_UPDATE_DATE FROM PCL_COMBO_LK");
			if(rs!=null){
				while(rs.next())
				{
					oPlanComboLK=new PNLComboLk();

					oPlanComboLK.setPcl_grp_id(rs.getInt("PCL_GRP_ID"));
					oPlanComboLK.setPcl_pgl_id(rs.getInt("PCL_PGL_ID"));
					oPlanComboLK.setPcl_prod_type(rs.getString("PCL_PROD_TYPE"));
					oPlanComboLK.setPcl_sumass_per(rs.getInt("PCL_SUMASS_PER"));
					oPlanComboLK.setPcl_update_by(rs.getString("PCL_UPDATE_BY"));
					oPlanComboLK.setPcl_update_date(rs.getDate("PCL_UPDATE_DATE"));
					oList.add(oPlanComboLK);
				}
			}
		} catch (Exception e) {
			logger.debug("Error occured in getPNLComboLk()");
		} finally {
			closeResources(con,cstmt,rs);
		}
		return oList;
	}
	*/
	public static ArrayList getServiceTaxMaster(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		ServiceTaxMaster oServiceTaxMaster=null;
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		try {

			rs = db.rawQuery("SELECT stm_type,stm_value,stm_update_by,stm_update_dt FROM lp_stm_servicetax_master",new String[0]);
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					rs.moveToPosition(i);
					oServiceTaxMaster=new ServiceTaxMaster();

					oServiceTaxMaster.setStm_type(rs.getString(rs.getColumnIndex("STM_TYPE")));
					oServiceTaxMaster.setStm_value(Double.parseDouble(rs.getString(rs.getColumnIndex("STM_VALUE"))));
					oServiceTaxMaster.setStm_update_by(rs.getString(rs.getColumnIndex("STM_UPDATE_BY")));
					oServiceTaxMaster.setStm_update_dt(dateFormat.parse(rs.getString(rs.getColumnIndex("STM_UPDATE_DT"))));
					oList.add(oServiceTaxMaster);
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			CommonConstant.printLog("d", "Error occured in getServiceTaxMaster()");
		}
		return oList;
	}

	public static ArrayList getLpbLargePremiumBoost(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		LpbLargePremiumBoost oPremBoost=null;
		try
		{

			rs = db.rawQuery("SELECT LPB_PLAN_CODE,LPB_RIDER_CODE,LPB_MIN_PREMIUM,LPB_MAX_PREMIUM,LPB_BOOST_RATE FROM LP_LPB_PREMIUM_BOOST", new String[0]);

			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
				{	rs.moveToPosition(i);
					oPremBoost=new LpbLargePremiumBoost();
					oPremBoost.setLPB_PLAN_CODE(rs.getString(rs.getColumnIndex("LPB_PLAN_CODE")));
					oPremBoost.setLPB_RIDER_CODE(rs.getString(rs.getColumnIndex("LPB_RIDER_CODE")));
					oPremBoost.setLPB_MIN_PREMIUM(Long.parseLong(rs.getString(rs.getColumnIndex("LPB_MIN_PREMIUM"))));
					oPremBoost.setLPB_MAX_PREMIUM(Long.parseLong(rs.getString(rs.getColumnIndex("LPB_MAX_PREMIUM"))));
					oPremBoost.setLPB_BOOST_RATE(Double.parseDouble(rs.getString(rs.getColumnIndex("LPB_BOOST_RATE"))));
					oList.add(oPremBoost);
				}
			}
			rs.close();
		} } catch (Exception e) {
			e.printStackTrace();
			CommonConstant.printLog("d", "Error occured in getLpbLargePremiumBoost()");
		}
		return oList;

	}
	public static ArrayList getFOPRate(SQLiteDatabase db,String pnlId) {
		ArrayList oList=new ArrayList();

        Cursor rs;
		FOPRate oFopRate=null;
		try
		{

            rs = db.rawQuery("select fop_pnl_id,fop_term,fop_mode,fop_charge from LP_FOP_CHARGES_MASTER where FOP_PNL_ID = ?", new String[]{pnlId});
            if(rs!=null){
                for(int i=0;i<rs.getCount();i++) {
                    rs.moveToPosition(i);
                    oFopRate=new FOPRate();
                    oFopRate.setFop_pnl_id(Integer.parseInt(rs.getString(rs.getColumnIndex("FOP_PNL_ID"))));
                    oFopRate.setFop_term(Integer.parseInt(rs.getString(rs.getColumnIndex("FOP_TERM"))));
                    oFopRate.setFop_mode(rs.getString(rs.getColumnIndex("FOP_MODE")));
                    oFopRate.setFop_rate(Double.parseDouble(rs.getString(rs.getColumnIndex("FOP_CHARGE"))));
                    oList.add(oFopRate);
				}
			}
			rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            CommonConstant.printLog("d", "Error occured in getFOPRate()");
        }
		return oList;
	}
	/*public static ArrayList getARLANUUITYRate(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		ANNUITYRate oAnnuityRate;
		try
		{

			rs = db.rawQuery("select arl_pnl_id,arl_age,arl_annuity_mode,arl_annuity_rate from arl_annuity_rate_lk");
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					oAnnuityRate=new ANNUITYRate();
					oAnnuityRate.setArl_pnl_id(rs.getString(rs.getColumnIndex("arl_pnl_id"));
					oAnnuityRate.setArl_age(Integer.parseInt(rs.getString(rs.getColumnIndex("arl_age"));
					oAnnuityRate.setArl_annuity_mode(rs.getString(rs.getColumnIndex("arl_annuity_mode"));
					oAnnuityRate.setArl_annuity_rate(Double.parseDouble(rs.getString(rs.getColumnIndex("arl_annuity_rate"));
					oList.add(oAnnuityRate);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			closeResources(con,cstmt,rs);
		}
		return oList;
	}
	public static ArrayList getINCENTIVERate(SQLiteDatabase db) {
		ArrayList oList=new ArrayList();

		Cursor rs;
		INCENTIVERate oIncentiveRate;
		try
		{

			rs = db.rawQuery("select inc_plan_code,inc_mode,inc_min_sa,inc_max_sa,inc_rate FROM inc_incentive_rate");
			if(rs!=null){
				for(int i=0;i<rs.getCount();i++) {
					oIncentiveRate=new INCENTIVERate();
					oIncentiveRate.setInc_plan_code(rs.getString(rs.getColumnIndex("inc_plan_code"));
					oIncentiveRate.setInc_mode(rs.getString(rs.getColumnIndex("inc_mode"));
					oIncentiveRate.setInc_min_sa(Long.parseLong(rs.getString(rs.getColumnIndex("inc_min_sa"));
					oIncentiveRate.setInc_max_sa(Long.parseLong(rs.getString(rs.getColumnIndex("inc_max_sa"));
					oIncentiveRate.setInc_rate(Double.parseDouble(rs.getString(rs.getColumnIndex("inc_rate"));
					oList.add(oIncentiveRate);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			closeResources(con,cstmt,rs);
		}
		return oList;
	}*/
}
