package com.talic.plugins.sisEngine.view;

import android.os.Environment;
import com.talic.plugins.sisEngine.util.CommonConstant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class PDFGenerator {
	OutputStream pdfOutputStream;
	static String strPDF;


	public  byte[] generatePDF(StringBuffer StrBuf,String outputPdfPath, String outputTempPath/*, String pageList*/, String altYesNo) {
		strPDF = StrBuf.toString();
		CommonConstant.printLog("d", "HTML:: " + strPDF);
		CommonConstant.printLog("d", "Start pdf conversion");
		OutputStream outStrmPdf = null;//\Android\data\com.tataaia.goodSolutions\
		outputTempPath = "storage\\sdcard\\Android\\data\\com.tataaia.goodSolutions\\cache\\FromClient\\SIS\\PDF\\MahaLife Supreme V2.pdf";
		byte[] fileData =null;
		try {
			//if (altYesNo.equals("No")) {
			String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PDF";
			File dir = new File(path);
			if(!dir.exists())
				dir.mkdirs();
			File file1 = new File(dir, "demo.pdf");
			outStrmPdf = new FileOutputStream(file1);


			/*ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(strPDF);
			renderer.layout();
			renderer.createPDF(outStrmPdf);
			outStrmPdf.close();*/
			CommonConstant.printLog("d", "PDF Generated Successfully");
			//PdfReader pdfReader = new PdfReader(outputTempPath);
			/*if (!pageList.equals("")) {
				pdfReader.selectPages(pageList);
			}*/
			// Code to generate byteStream
			/*PdfStamper pdfStamper = new PdfStamper(pdfReader,
	                new FileOutputStream(outputPdfPath));
			if (altYesNo.equals("Y")) {
				pdfStamper.setEncryption(null,null, PdfWriter.HideWindowUI|PdfWriter.HideMenubar, PdfWriter.STANDARD_ENCRYPTION_128);
				pdfStamper.setViewerPreferences(PdfWriter.HideToolbar|PdfWriter.HideMenubar);
			}
			if (altYesNo.equals("P")) {
				pdfStamper.setEncryption(null,null,PdfWriter.ALLOW_PRINTING|PdfWriter.ALLOW_COPY|PdfWriter.HideWindowUI|PdfWriter.HideMenubar, PdfWriter.STANDARD_ENCRYPTION_128);
				pdfStamper.setViewerPreferences(PdfWriter.HideToolbar|PdfWriter.HideMenubar);
			}
			pdfStamper.close();
			pdfReader.close();
		//	uncomment for html
			/*} else {
			byte[] bt=strPDF.getBytes();
  		    FileOutputStream fos = new FileOutputStream(outputPdfPath);

  		    fos.write(bt);
			fos.close();
			}	*/
			File file = new File(outputPdfPath);
			FileInputStream fis = new FileInputStream(file);
		//	uncomment for html
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
		        byte[] buf = new byte[(int)file.length()];

		        try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		                //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.

		            }
		        } catch (IOException ex) {
		          System.out.println("error");
		        }
		       fileData = bos.toByteArray();


/*code commented to stop delete file on server
 * 		       fis.read(fileData);
		    	if(file.exists())
		    	{    fis.close();
		    		file.delete();
		    	}

*/

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}finally{

			try {
				if (altYesNo.equals("No")) {
					outStrmPdf.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return fileData;

	}

}
