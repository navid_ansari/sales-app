package com.talic.plugins.sisEngine.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.talic.plugins.sisEngine.BO.FundDetailsLK;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.BO.XMMXmlMetadataMaster;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.bean.SISRequestBean;
import com.talic.plugins.sisEngine.db.DataAccessClass;
import com.talic.plugins.sisEngine.util.XMLTagConstants;

@SuppressWarnings("unchecked")
public class XmlBuilder {
	private ModelMaster master;
	public XmlBuilder(ModelMaster master){
		this.master=master;
	}

	public String buildXML() throws IOException
	{
		StringBuffer sb=new StringBuffer();
		HashMap metaDataMap = new HashMap();
		ArrayList  alXmlMetadata=DataAccessClass.oDataMapperBO.getXmlMetadataMaster();
		String planCode=master.getRequestBean().getBaseplan();
		SISRequestBean reqBean=master.getRequestBean();
		sb.append("<SIS>");
		for(int i=0;i<alXmlMetadata.size();i++){
			XMMXmlMetadataMaster oXMMXmlMetadataMaster=(XMMXmlMetadataMaster)alXmlMetadata.get(i);
			if(oXMMXmlMetadataMaster.getPNL_CODE().equalsIgnoreCase(planCode))
			{
				metaDataMap.put(oXMMXmlMetadataMaster.getSRNO(), oXMMXmlMetadataMaster);
			}
		}
		if(metaDataMap.size()>0){
		for(int count=1; count<=metaDataMap.size(); count++){
			XMMXmlMetadataMaster oXMMXmlMetadata = (XMMXmlMetadataMaster)metaDataMap.get(count);
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposalNo)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposal_no());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposalDate)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposal_date());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.InsuredName)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getInsured_name());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.InsuredAge)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getInsured_age());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.InsuredGender)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getInsured_sex());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.InsuredOccupation)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getInsured_occupation());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposerName)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposer_name());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposerAge)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposer_age());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposerGender)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposer_sex());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposerOccupation)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposer_occupation());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.BasePlan)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getBaseplan());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.SumAssured)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getSumassured());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PolicyTerm)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getPolicyterm());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PremiumPayingTerm)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getPremiumpayingterm());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.Mode)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getFrequency());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PremiumMultiple)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getPremmultiple());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.BasePremium)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getQuotedAnnualizedPremium());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ModelPremium)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getModalPremium());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.Riders)){
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				ArrayList riderLst = master.getRiderData();
				 for(int i=0; i <riderLst.size(); i++){
					 RiderBO oRiderBO = (RiderBO)riderLst.get(i);
					 sb.append("<Rider>");
					 sb.append("<RiderCode>");
					 sb.append(oRiderBO.getCode());
					 sb.append("</RiderCode>");
					 sb.append("<RiderTerm>");
					 sb.append(oRiderBO.getPolTerm());
					 sb.append("</RiderTerm>");
					 sb.append("<RiderSumAssured>");
					 sb.append(oRiderBO.getSumAssured());
					 sb.append("</RiderSumAssured>");
					 sb.append("<RiderPremium>");
					 sb.append(oRiderBO.getPrimium());
					 sb.append("</RiderPremium>");
					 sb.append("</Rider>");
				 }
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			/*if(oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase("TotalRiderPremium")){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.get);
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}*/

			/*
			if(oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase("RiderPremium")){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(reqBean.getProposal_no());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}*/
			/*if(oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase("TotalRiderPremiumWithST")){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.);
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}*/
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.Funds)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
					ArrayList fundLst = reqBean.getFundList();
					 for(int i=0; i <fundLst.size(); i++){
						 FundDetailsLK oFundDetailsLK = (FundDetailsLK)fundLst.get(i);
						 sb.append("<Fund>");
						 sb.append("<FundCode>");
						 sb.append(oFundDetailsLK.getCODE());
						 sb.append("</FundCode>");
						 sb.append("<FundPercentage>");
						 sb.append(oFundDetailsLK.getPERCENTAGE());
						 sb.append("</FundPercentage>");
						 sb.append("</Fund>");
					 }
					 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PolicyYear)){
				int pt=master.getRequestBean().getPolicyterm();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					 sb.append("<Values>");
					 sb.append(i);
					 sb.append("</Values>");
				}
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PolicyInsuredAge)){
				int pt=master.getRequestBean().getPolicyterm();
				int age=(int)(master.getRequestBean().getInsured_age());
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					 sb.append("<Values>");
					 sb.append(age);
					 sb.append("</Values>");
					 age++;
				}
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.AnnualizedBasicPremium)){
                int pt=master.getRequestBean().getPolicyterm();
                String mode=master.getRequestBean().getFrequency();
                long prem=master.getQuotedAnnualizedPremium();
                int ppt = master.getRequestBean().getPremiumpayingterm();   //added by jayesh :24-07-2014
                sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
                for(int i=1;i<=pt;i++){
                       sb.append("<Values>");
                       //if("O".equals(mode) && i>1)                  //commented by jayesh :24-07-2014
                       if("O".equals(mode) && i>1 || (i>ppt))   //added by jayesh :24-07-2014
                            {
                                  sb.append("0");
                            }else{
                                  sb.append(prem);
                            }
                       sb.append("</Values>");
                      }
                 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			/*if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.AnnualizedBasicPremium)){
				int pt=master.getRequestBean().getPolicyterm();
				String mode=master.getRequestBean().getFrequency();
				long prem=master.getQuotedAnnualizedPremium();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					 sb.append("<Values>");
					 if("O".equals(mode) && i>1)
						{
							sb.append("0");
						}else{
							sb.append(prem);
						}
					 sb.append("</Values>");
					}
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}*/
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PremiumAllocationCharge)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmAllocCharg=master.getPremiumAllocationCharges();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmAllocCharg.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.AmountAvailableForIInvestment)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmAmtInv=master.getAmountForInvestment();
			    sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmAmtInv.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.OtherInbuiltBenefitCharge)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmOBIC=master.getOtherBenefitCharges();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmOBIC.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");

			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.PolicyAdminCharge)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmAdmChrg=master.getPolicyAdminCharges();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmAdmChrg.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.MortalityCharges)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmMortChrg=master.getMortalityCharges();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmMortChrg.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.GuaranteeCharges)){
				int pt=master.getRequestBean().getPolicyterm();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append("0");
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.OtherCharges)){
				int pt=master.getRequestBean().getPolicyterm();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append("0");
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ApplicableServiceTax)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmAppTax=master.getApplicableServiceTaxPH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmAppTax.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.RegularFundValuebeforeFMC)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmRegFundVal.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.FundManagementCharge)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmFMC=master.getFundManagmentChargePH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmFMC.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.RegularFundValueAtEOPY)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmFundVal=master.getRegularFundValuePH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmFundVal.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.GuaranteedMaturityAddition)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmGMA=master.getGuaranteedMaturityAdditionPH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmGMA.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.TotalFundValueAtEOPY)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmFundVal=master.getTotalFundValueAtEndPolicyYearPH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmFundVal.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.SurrenderValueAtEOPY)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmSurrVal=master.getSurrenderValuePH10();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmSurrVal.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.DeathBenefitAtEOPY)){

				int pt=master.getRequestBean().getPolicyterm();
				HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYear();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=pt;i++){
					sb.append("<Values>");
					sb.append(hmDB.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			// added for iRakshaTrop

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.TotalModalPlusRiderPrem)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getTotalModalPremium());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ServiceTax)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getServiceTax());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.TotalPremWtServiceTax)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getTotalModalPremiumPayble());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.Currency)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append("INR");
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.MaturityValue)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap matValue=master.getMaturityVal();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=0;i<pt;i++){
					sb.append("<Values>");
					if(i == (pt-1)){
						sb.append(matValue.get(i));
					}
					else{
						sb.append("0");
					}
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.SurrenderBenefit)){
				int pt=master.getRequestBean().getPolicyterm();
				HashMap matValue=master.getMaturityVal();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=0;i<pt;i++){
					sb.append("<Values>");
					if(i == (pt-1)){
					    sb.append("0");
					}
					else{
						sb.append(matValue.get(i));
					}
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.LifeCoverage)){

				HashMap hLCoverage=master.getLifeCoverage();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=hLCoverage.size();i++){
					sb.append("<Values>");
					sb.append(hLCoverage.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.InsuredDOB)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getRequestBean().getInsured_dob());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
			if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.ProposerDOB)){
				 sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				 sb.append(master.getRequestBean().getProposer_dob());
				 sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

        if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.GAI)){
				HashMap hGuranteedAnnualIncome=master.getGuranteedAnnualIncome();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=hGuranteedAnnualIncome.size();i++){
					sb.append("<Values>");
					sb.append(hGuranteedAnnualIncome.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}

        if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.GSV)){
				HashMap hGSV=master.getGuranteedSurrenderValue();
				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
				for(int i=1;i<=hGSV.size();i++){
					sb.append("<Values>");
					sb.append(hGSV.get(i));
					sb.append("</Values>");
				}
				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
			}
        if(oXMMXmlMetadata.getELEMENT_NAME()!=null && oXMMXmlMetadata.getELEMENT_NAME().equalsIgnoreCase(XMLTagConstants.SSV)){
 				HashMap hSSV=master.getNonGuaranteedSurrBenefit();
 				sb.append("<"+oXMMXmlMetadata.getELEMENT_NAME()+">");
 				for(int i=1;i<=hSSV.size();i++){
 					sb.append("<Values>");
 					sb.append(hSSV.get(i));
 					sb.append("</Values>");
 				}
 				sb.append("</"+oXMMXmlMetadata.getELEMENT_NAME()+">");
 			}

		}


	}else{
		sb.append("<ERRORCODE>TALIC001</ERRORCODE><ERRORCODE>Deatails for the provided plan does not available</ERRORCODE>");
	}

		sb.append("</SIS>");
	  return sb.toString();

	}


public String getTableRegularFundValueBeforeFMC_PH6(){

		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td>");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
}
