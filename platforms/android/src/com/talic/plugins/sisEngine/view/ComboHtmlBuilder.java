package com.talic.plugins.sisEngine.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.talic.plugins.AppConstants;
import com.talic.plugins.sisEngine.BO.AddressBO;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.db.DataAccessClass;
import com.talic.plugins.sisEngine.util.CommonConstant;
import com.talic.plugins.sisEngine.util.FileCounter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unchecked")
public class ComboHtmlBuilder {
	private ModelMaster master;
	public ComboHtmlBuilder(ModelMaster master){
		this.master=master;
	}
	DecimalFormat df = new DecimalFormat("#,###");

	public String buildHtml(String outputFile) throws IOException {
		String filedata=null;
		//byte[] pdfdata =null;
		try{
			String altYesNo = "N";
			String fileNameOptional = ".html";

			if (outputFile==null) {
				outputFile = AppConstants.SA_APP_CONTEXT + "\\" + AppConstants.ROOT_FOLDER + "\\FromClient\\SIS\\PDF\\";
			}

			InputStream templateFile = AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/"+master.getCombotemplateName()+".html");// + master.getPlanDescription() + fileNameOptional);
			byte[] inputBuffer =new byte[templateFile.available()];

			int noOfBytesRead = templateFile.read(inputBuffer);
			String inputString=new String(inputBuffer);

			Pattern p = Pattern.compile("\\|\\|[a-zA-Z_0-9\\p{Punct}]+\\|\\|");
			StringBuffer sb = new StringBuffer();
			long COMBOSAP=0;
			long COMBOARP=0;
			long COMBORAP=0;
			long COMBOMRP=0;
			// Create a matcher with an input string
			while (noOfBytesRead > 0) {
				Matcher m = p.matcher(inputString);
				boolean result = m.find();
				// Loop through and create a new String
				// with the replacements
				int iStart = 0;
				int iEnd = 0;
				while(result) {
					iStart = m.start();
					iEnd = m.end();
					String pattern = inputString.substring(iStart, iEnd);
					pattern=pattern.replace('|',' ');
					pattern = pattern.trim();

					if(pattern.startsWith("BPLAN_OPTION"))
					{
						String replaceString = "";
						/*if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("MBPV1N1")
								|| master.getRequestBean().getBaseplan().equals("FR7V1P1") || master.getRequestBean().getBaseplan().equals("FR10V1P1")
								|| master.getRequestBean().getBaseplan().equals("FR15V1P1"))
							replaceString = "OPTION 1";
						else if (master.getRequestBean().getBaseplan().equals("SGPV1N2") || master.getRequestBean().getBaseplan().equals("MBPV1N2")
								|| master.getRequestBean().getBaseplan().equals("FR7V1P2") || master.getRequestBean().getBaseplan().equals("FR10V1P2")
								|| master.getRequestBean().getBaseplan().equals("FR15V1P2"))
							replaceString = "OPTION 2";
						else if (master.getRequestBean().getBaseplan().equals("MBPV1N3"))
							replaceString = "OPTION 3";*/
						String BplanName = master.getComboBasePlanCode();
						if (BplanName.equals("SIP7IV1N1")
								|| BplanName.equals("SIP10IV1N1")
								|| BplanName.equals("SIP12IV1N1"))
							replaceString = "Regular Income Benefit";
						else if (BplanName.equals("SIP7EV1N1")
								|| BplanName.equals("SIP10EV1N1")
								|| BplanName.equals("SIP12EV1N1"))
							replaceString = "Endowment Benefit";
						/*else if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV"))
							replaceString = "Pro Care";
						else if (master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
							replaceString = "Pro Care Plus";
						else if (master.getRequestBean().getBaseplan().contains("VCPSDV") || master.getRequestBean().getBaseplan().contains("VCPFDV"))
							replaceString = "Duo Care";
						else if (master.getRequestBean().getBaseplan().contains("VCPSDPV") || master.getRequestBean().getBaseplan().contains("VCPFDPV"))
							replaceString = "Duo Care Plus";
						else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N1"))
							replaceString = "Option 1: \"Sum Assured on Death\" payable on Death";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N2"))
							replaceString = "Option 2: \"Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N3"))
							replaceString = "Option 3: \"Enhanced Sum Assured on Death\" payable on Death";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N4"))
							replaceString = "Option 4: \"Enhanced Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";*/
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TPLAN_OPTION")){
						String TPlanName = master.getComboTermPlanCode();

						String replaceString = "";
						if (TPlanName.startsWith("SR") && TPlanName.contains("V1N1"))
							replaceString = "Option 1: \"Sum Assured on Death\" payable on Death";
						else if (TPlanName.startsWith("SR") && TPlanName.contains("V1N2"))
							replaceString = "Option 2: \"Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";
						else if (TPlanName.startsWith("SR") && TPlanName.contains("V1N3"))
							replaceString = "Option 3: \"Enhanced Sum Assured on Death\" payable on Death";
						else if (TPlanName.startsWith("SR") && TPlanName.contains("V1N4"))
							replaceString = "Option 4: \"Enhanced Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";

						m.appendReplacement(sb, replaceString);
					}


					if(pattern.startsWith("PROPNO")) {
						String replaceString = master.getComboPropNo();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("PROPDATE")) {
                        SimpleDateFormat sdfDate = null;
						//if(master.getRequestBean().getBaseplan().startsWith("MRS") || master.getRequestBean().getBaseplan().startsWith("IRS") || master.getRequestBean().getBaseplan().startsWith("IRT")){
                           // sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");//dd/MM/yyyy
                        //}else {
                            sdfDate = new SimpleDateFormat("MM-dd-yyyy,hh:mm a");//dd/MM/yyyy
                        //-}
						Date now = new Date();
						String replaceString = sdfDate.format(now);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("SISUIN")){
						String replaceString = "";
						if(master.getUINNumber() != null)
							replaceString = master.getUINNumber();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("INSUREDNAME")){
						String replaceString = master.getComboInsName();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("PROPNAME")){
						String replaceString = master.getComboPropName();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("IMale")){
						String replaceString = master.getComboInsGender();
						if(replaceString.equals("M")){
							replaceString = "Male";
						}
						if(replaceString.equals("F")){
							replaceString = "Female";
						}
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("PMale")){
						String replaceString = master.getComboPropGender();
						if(replaceString.equals("M")){
							replaceString = "Male";
						}
						if(replaceString.equals("F")){
							replaceString = "Female";
						}
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("IAGE")){
						String replaceString = master.getComboInsAge() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("PAGE")){
						String replaceString = master.getComboPropAge() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("IMode")){
						String mode = "";
						if(master.getComboInsPayMode().equals("A")){
							mode = "Annual";
						}else if(master.getComboInsPayMode().equals("S")){
							mode = "Semi Annual";
						}else if(master.getComboInsPayMode().equals("M")){
							mode = "Monthly";
						}else if(master.getComboInsPayMode().equals("Q")) {
							mode = "Quarterly";
						}
						else
							mode = "Single Pay";

						String replaceString = mode;
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("IOCC")){
						String replaceString = master.getComboInsOccDesc() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("POCC")){
						String replaceString = master.getComboPropOccDesc() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("INonSmoker")){
						System.out.println("Smoke ::" + master.getComboInsSmoker());
						String smok = "Smoker";
						if(master.getComboInsSmoker().equals("1")){
							smok = "Non Smoker";
						}
						String replaceString = smok + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BPLAN_NAME")){
						String replaceString = master.getComboBasePlanName() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TPLAN_NAME")){
						String replaceString = master.getComboTermPlanName() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BPOLICY_TERM")){
						String replaceString = master.getComboBasePolTerm() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TPOLICY_TERM")){
						String replaceString = master.getComboTermPolTerm() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BPREMIUM_PAYMENT_TERM")){
						String replaceString = master.getComboBasePPT() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TPREMIUM_PAYMENT_TERM")){
						String replaceString = master.getComboTermPPT() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BANNUAL_PREMIUM")){
						String replaceString = master.getComboBaseAnnPrem();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TANNUAL_PREMIUM")){
						String replaceString = master.getComboTermAP() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TOTAL_ANN_PREM")){
						long comboBaseAp = Long.parseLong(master.getComboBaseAnnPrem());
						long comboTermAp = master.getComboTermAP() ;
						long totalAp=comboTermAp+comboBaseAp;
						m.appendReplacement(sb, totalAp+"");
					}

					if(pattern.startsWith("BSERVICE_TAX")){
						String replaceString = master.getComboBaseST() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TOTAL_AMOUNT")){
						String totalAmt = master.getComboBaseTotaAmt()+"";
						m.appendReplacement(sb,totalAmt);
					}

					if(pattern.startsWith("TSERVICE_TAX")){
						String replaceString = master.getComboTermST() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BTOTAL_PREMIUM")){
						String replaceString = master.getComboBaseTP() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TTOTAL_PREMIUM")){
						String replaceString = master.getComboTermTP() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("BSUM_ASSURED")){
						String replaceString = master.getComboBaseSA() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TSUM_ASSURED")){
						String replaceString = master.getComboTermSA() + "";
						m.appendReplacement(sb, replaceString);
					}

					//renewal ann prem.
					if(pattern.startsWith("COMBORAP")){
						long termPrem = master.getComboAnnRenewalPrem();
						long BasePrem = 0;
						if (master.getComboBaseProdType()!=null && master.getComboBaseProdType().equals("U")) {
							if (master.getComboBaseAnnRenPrem() != null && !master.getComboBaseAnnRenPrem().equals("NA"))
								BasePrem = Long.parseLong(master.getComboBaseAnnRenPrem());
//							else
//								BasePrem = master.getRequestBean().getBasepremiumannual();
						}
						else
							BasePrem =  Long.parseLong(master.getComboBaseAnnRenPrem());

						COMBORAP=BasePrem+termPrem;
						String replaceString = String.valueOf(BasePrem + termPrem);

						m.appendReplacement(sb, replaceString);
					}

					//renewal ST.
					if(pattern.startsWith("COMBOAST")){
						long serviceTaxbase = 0;
						if(!master.getComboBaseProdType().equals("U"))
							serviceTaxbase = master.getComboBaseAnnST();

						String replaceString = (master.getComboAnnServiceTax() + serviceTaxbase) + "";
						m.appendReplacement(sb, replaceString);
					}

					//Total Renewal ST
					if(pattern.startsWith("COMBOATRP")){
						long tax = 0;
						if(!master.getComboBaseProdType().equals("U"))
							tax = master.getComboAnnServiceTax() + master.getComboBaseAnnST();
						else
							tax = master.getComboAnnServiceTax();

						String replaceString = ""+ (tax +COMBORAP);
						m.appendReplacement(sb, replaceString);
					}

					//renewal semi annual
					if(pattern.startsWith("COMBOSAP")){
						long termPrem = master.getComboAnnRenewalPrem();
						long basePrem = 0;

						String baseplan=master.getComboBasePlanName();
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							basePrem = Long.parseLong(master.getComboBaseAnnRenPrem());
						else
						if (master.getComboBaseProdType()!=null && master.getComboBaseProdType().equals("U")) {
							if (!master.getComboBaseAnnRenPrem().equals("NA"))
								basePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem()) + termPrem) * 0.51);
							else
								basePrem = Math.round(termPrem * 0.51);
						}
						else
								basePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem()) + termPrem) * 0.51);

						Log.d("Base SA: ",basePrem+"");
						Log.d("Term SA: ",termPrem+"");
						COMBOSAP = basePrem;
						long total=basePrem;
						String replaceString = String.valueOf(total);

						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSAST")){
						long serviceTaxbase = 0;
						if(!master.getComboBaseProdType().equals("U"))
							serviceTaxbase = master.getComboBaseSemAnnST();

						String replaceString = (master.getComboSemServiceTax() + serviceTaxbase) + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSTRP")){
						long tax = 0;
						if(!master.getComboBaseProdType().equals("U"))
							tax = master.getComboSemServiceTax() + master.getComboBaseSemAnnST();
						else
							tax = master.getComboSemServiceTax();

						String replaceString = (tax + COMBOSAP) + "";
						m.appendReplacement(sb, replaceString);
					}

					//Quarter renewal
					if(pattern.startsWith("COMBOARP")){
						long termPrem = master.getComboAnnRenewalPrem();
						long BasePrem = 0;

						String baseplan=master.getComboBasePlanName();
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							BasePrem = Long.parseLong(master.getComboBaseAnnRenPrem());
						else
						if (master.getComboBaseProdType()!=null && master.getComboBaseProdType().equals("U")) {
							if (!master.getComboBaseAnnRenPrem().equals("NA"))
								BasePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem()) + termPrem) * 0.26);
							else
								BasePrem = Math.round(termPrem * 0.26);
						}
						else
							BasePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem())+termPrem) * 0.26);

						COMBOARP = BasePrem;
						String replaceString = String.valueOf(BasePrem );
						m.appendReplacement(sb, replaceString);
					}

					//service tax = 0 for ulip
					if(pattern.startsWith("COMBOQST")){
						long serviceTaxbase = 0;
						if(!master.getComboBaseProdType().equals("U"))
							serviceTaxbase = master.getComboBaseQuaST();

						String replaceString = (master.getComboQuaServiceTax() + serviceTaxbase) + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOQTRP")){
						long tax = 0;
						if(!master.getComboBaseProdType().equals("U"))
							tax = master.getComboQuaServiceTax() + master.getComboBaseQuaST();
						else
							tax = master.getComboQuaServiceTax();

						String replaceString = ""+(tax + COMBOARP);
						m.appendReplacement(sb, replaceString);
					}

					//Month renewal
					if(pattern.startsWith("COMBOMRP")){
						long termPrem = master.getComboAnnRenewalPrem();
						long BasePrem = 0;

						String baseplan=master.getComboBasePlanName();
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							BasePrem = Long.parseLong(master.getComboBaseAnnRenPrem());
						else
						if (master.getComboBaseProdType()!=null && master.getComboBaseProdType().equals("U")) {
							if (!master.getComboBaseAnnRenPrem().equals("NA"))
								BasePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem())
										+ termPrem) * 0.0883);
							else
								BasePrem = Math.round(termPrem * 0.0883);

						}
						else
							BasePrem = Math.round((Long.parseLong(master.getComboBaseAnnRenPrem())+termPrem) * 0.0883);
						COMBOMRP = BasePrem;
						String replaceString = String.valueOf(BasePrem);

						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOMST")){
						long serviceTaxbase = 0;
						if(!master.getComboBaseProdType().equals("U"))
							serviceTaxbase = master.getComboBaseMonthST();

						String replaceString = (master.getComboMonthServiceTax() + serviceTaxbase) + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOMTRP")){
						long tax = 0;
						if(!master.getComboBaseProdType().equals("U"))
							tax = master.getComboMonthServiceTax() + master.getComboBaseMonthST();
						else
							tax = master.getComboMonthServiceTax();

						String replaceString = (tax + COMBOMRP) + "";
						m.appendReplacement(sb, replaceString);
					}

					//
					if(pattern.startsWith("TLC")){
						String replaceString = master.getTLC() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("WLE")){
						String replaceString = master.getWLE() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("WLA")){
						String replaceString = master.getWLA() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("WLS")){
						String replaceString = master.getWLS() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("WLI")){
						String replaceString = master.getWLI() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("WLF")){
						String replaceString = master.getWLF() + "";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOPOLYEAR")){
						int polTerm = master.getComboPolTerm();
						String polyear = createPolicyYear(1,polTerm);
						System.out.println("Pol polyear ::"+polyear);
						m.appendReplacement(sb, polyear);
					}

					if(pattern.startsWith("COMBOINSAGE")){
						int pt=master.getComboPolTerm();
						int count = pt;
						int age=(int)(master.getComboInsAge());
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOQAP")){
						int pt=master.getComboPolTerm();
						int count = pt;
						String replaceString = getComboTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBODB8")){
						String replaceString = getComboDeathBenfit8();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBODB4")){
						String replaceString = getComboDeathBenfit4();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSV8")){
						String replaceString = getComboSurrVal8();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSV4")){
						String replaceString = getComboSurrVal4();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOMV8")){
						String replaceString = getFundValueMatVal8();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOMV4")){
						String replaceString = getFundValueMatVal4();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOGUARPAY")){
						String replaceString = getComboGuarPayout();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOGUARMATPAY")){
						String replaceString = getComboGuarMatPayout();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSIPDB")){
						String replaceString = getComboSIPDB();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("COMBOSIPSURRVAL")){
						String replaceString = getComboSIPSurrVal();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith("TOTAL_SERVICE_TAX")){
						long comboBaseST = master.getComboBaseST() ;
						long comboTermST = master.getComboTermST() ;
						long totalST=comboTermST+comboBaseST;
						m.appendReplacement(sb, totalST+"");
					}

					if(pattern.startsWith("TOTAL_PREMIUM")){
						long comboBaseTP = master.getComboBaseTP() ;
						long comboTermSP = master.getComboTermTP() ;
						long totalTP=comboTermSP+comboBaseTP;
						m.appendReplacement(sb, totalTP+"");
					}

					if(pattern.startsWith("TOTAL_SUM_ASSURED")){
						long comboBaseSA = master.getComboBaseSA() ;
						long comboTermSA = master.getComboTermSA() ;
						long totalTSA=comboTermSA+comboBaseSA;
						m.appendReplacement(sb, totalTSA+"");
					}


					/*if(pattern.startsWith("COMBODB4")){
						String replaceString = master.getWLF() + "";
						m.appendReplacement(sb, replaceString);
					}

					*/


					//End : added by jayesh for Smart 7 05-05-2014
					result = m.find();
				}
				m.appendTail(sb);
				noOfBytesRead = templateFile.read(inputBuffer);
				inputString = new String(inputBuffer);
			}

			filedata = sb.toString();

			//File myFile = new File(outputFilePath);
			//Desktop.getDesktop().open(myFile);

			templateFile.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return filedata;
	}

	//Combo
	//no term value to assign
	public String getComboGuarPayout(){
		int pt=master.getComboPolTerm();
		HashMap bGuarPay = master.getComboBaseGuarPayout();
		HashMap tGuarPay = null;

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bGuarPay != null && bGuarPay.get(i) != null){
				if(tGuarPay != null && bGuarPay.get(i) != null){
					long b = (Long)bGuarPay.get(i);
					long t = (Long)tGuarPay.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bGuarPay.get(i)));
				}
			}else if(tGuarPay != null && tGuarPay.get(i) != null){
				sb.append(df.format(tGuarPay.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	//no term value to assign
	public String getComboGuarMatPayout(){
		int pt=master.getComboPolTerm();
		HashMap bGuarMatPay = master.getComboBaseGuarMatPayout();
		HashMap tGuarMatPay = null;

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bGuarMatPay != null && bGuarMatPay.get(i) != null){
				if(tGuarMatPay != null && bGuarMatPay.get(i) != null){
					long b = (Long)bGuarMatPay.get(i);
					long t = (Long)tGuarMatPay.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bGuarMatPay.get(i)));
				}
			}else if(tGuarMatPay != null && tGuarMatPay.get(i) != null){
				sb.append(df.format(tGuarMatPay.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboSIPDB(){
		int pt=master.getComboPolTerm();
		HashMap bCSIPDB = master.getComboBaseDeathBenefit();
		HashMap tCSIPDB = master.getComboTermDeathBenefit8();

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bCSIPDB != null && bCSIPDB.get(i) != null){
				if(tCSIPDB != null && tCSIPDB.get(i) != null){
					long b = (Long)bCSIPDB.get(i);
					long t = (Long)tCSIPDB.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bCSIPDB.get(i)));
				}
			}else if(tCSIPDB != null && tCSIPDB.get(i) != null){
				sb.append(df.format(tCSIPDB.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboSIPSurrVal(){
		int pt=master.getComboPolTerm();
		HashMap bCSIPSV = master.getComboBaseSurrVal();
		HashMap bCSIPSV1 = master.getComboBaseGuarSurrVal();
		HashMap maxVal = new HashMap();

		Iterator it = bCSIPSV.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			long val1 = ((Number)pair.getValue()).longValue();
			long val2 = ((Number)bCSIPSV1.get(pair.getKey())).longValue();
			int key = ((Number)pair.getKey()).intValue();

			if(val1 > val2)
				maxVal.put(key,val1);
			else
				maxVal.put(key,val2);
			System.out.println(pair.getKey() + " = " + pair.getValue());
		}

		HashMap tCSIPSV = null;
		//SR
		if(master.ComboTermPGLID.equals("224"))
			tCSIPSV = master.getComboTermSurrVal();
		else if(master.ComboTermPGLID.equals("225")) {
			//SR+
			tCSIPSV = new HashMap();
			HashMap firtMap = master.getComboTermSurrVal();
			HashMap secondMap = master.getComboTermSurrVal1();

			Iterator it1 = firtMap.entrySet().iterator();
			while (it1.hasNext()) {
				Map.Entry pair = (Map.Entry)it1.next();
				long val1 = ((Number)pair.getValue()).longValue();
				long val2 = ((Number)secondMap.get(pair.getKey())).longValue();
				int key = ((Number)pair.getKey()).intValue();

				if(val1 > val2)
					tCSIPSV.put(key,val1);
				else
					tCSIPSV.put(key,val2);
				System.out.println(pair.getKey() + " = " + pair.getValue());
			}
		}

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(maxVal != null && maxVal.get(i) != null){
				if(tCSIPSV != null && maxVal.get(i) != null){
					long b = (Long)maxVal.get(i);
					long t = (tCSIPSV.get(i) != null)?(Long)tCSIPSV.get(i):0;
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(maxVal.get(i)));
				}
			}else if(tCSIPSV != null && tCSIPSV.get(i) != null){
				sb.append(df.format(tCSIPSV.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	//no term value to assign
	public String getFundValueMatVal8(){
		int pt=master.getComboPolTerm();
		HashMap bFundVal = master.getComboBaseFundValue8();
		HashMap tFundVal = master.getComboTermMatVal();

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bFundVal != null && bFundVal.get(i) != null){
				if(tFundVal != null && tFundVal.get(i) != null){
					long b = (Long)bFundVal.get(i);
					long t = (Long)tFundVal.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bFundVal.get(i)));
				}
			}else if(tFundVal != null && tFundVal.get(i) != null){
				sb.append(df.format(tFundVal.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	//No term value to assign
	public String getFundValueMatVal4(){
		int pt=master.getComboPolTerm();
		HashMap bFundVal = master.getComboBaseFundValue4();
		HashMap tFundVal = master.getComboTermMatVal();

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bFundVal != null && bFundVal.get(i) != null){
				if(tFundVal != null && tFundVal.get(i) != null){
					long b = (Long)bFundVal.get(i);
					long t = (Long)tFundVal.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bFundVal.get(i)));
				}
			}else if(tFundVal != null && tFundVal.get(i) != null){
				sb.append(df.format(tFundVal.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboSurrVal8(){
		int pt=master.getComboPolTerm();
		HashMap bSurVal = master.getComboBaseSurrVal8();
		HashMap TSurVal = null;

		//SR
		if(master.getComboTermPGLID().equals("224")){
			TSurVal = master.getComboTermSurrVal();
		}else if(master.getComboTermPGLID().equals("225")){
			//SR+
			TSurVal = new HashMap();
			HashMap firtMap = master.getComboTermSurrVal();
			HashMap secondMap = master.getComboTermSurrVal1();

			Iterator it = firtMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();
				long val1 = ((Number)pair.getValue()).longValue();
				long val2 = ((Number)secondMap.get(pair.getKey())).longValue();
				int key = ((Number)pair.getKey()).intValue();

				if(val1 > val2)
					TSurVal.put(key,val1);
				else
					TSurVal.put(key,val2);
				System.out.println(pair.getKey() + " = " + pair.getValue());
			}
		}

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bSurVal.get(i) != null){
				if(TSurVal.get(i) != null){
					long b = (Long)bSurVal.get(i);
					long t = (Long)TSurVal.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bSurVal.get(i)));
				}
			}else if(TSurVal.get(i) != null){
				sb.append(df.format(TSurVal.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboSurrVal4(){
		int pt=master.getComboPolTerm();
		HashMap bSurVal = master.getComboBaseSurrVal4();
		HashMap TSurVal = null;

		//SR
		if(master.getComboTermPGLID().equals("224")){
			TSurVal = master.getComboTermSurrVal();
		}else if(master.getComboTermPGLID().equals("225")){
			//SR+
			TSurVal = new HashMap();
			HashMap firtMap = master.getComboTermSurrVal();
			HashMap secondMap = master.getComboTermSurrVal1();

			Iterator it = firtMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();
				long val1 = ((Number)pair.getValue()).longValue();
				long val2 = ((Number)secondMap.get(pair.getKey())).longValue();
				int key = ((Number)pair.getKey()).intValue();

				if(val1 > val2)
					TSurVal.put(key,val1);
				else
					TSurVal.put(key,val2);
				System.out.println(pair.getKey() + " = " + pair.getValue());
			}
		}

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(bSurVal.get(i) != null){
				if(TSurVal.get(i) != null){
					long b = (Long)bSurVal.get(i);
					long t = (Long)TSurVal.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(bSurVal.get(i)));
				}
			}else if(TSurVal.get(i) != null){
				sb.append(df.format(TSurVal.get(i)));
			}else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboDeathBenfit8(){
		int pt=master.getComboPolTerm();

		HashMap baseDB8 =master.getComboBaseDeathBenefit8();
		HashMap termDB =master.getComboTermDeathBenefit8();

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(baseDB8.get(i) != null){
				if(termDB.get(i) != null){
					long b = (Long)baseDB8.get(i);
					long t = (Long)termDB.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(baseDB8.get(i)));
				}
			}else if(termDB.get(i) != null){
				sb.append(df.format(termDB.get(i)));
			}else
				sb.append("0");

			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getComboDeathBenfit4(){
		int pt=master.getComboPolTerm();

		HashMap baseDB4 =master.getComboBaseDeathBenefit4();
		HashMap termDB =master.getComboTermDeathBenefit8();

		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			if(baseDB4.get(i) != null){
				if(termDB.get(i) != null){
					long b = (Long)baseDB4.get(i);
					long t = (Long)termDB.get(i);
					sb.append(df.format(b + t));
				}else{
					sb.append(df.format(baseDB4.get(i)));
				}
			}else if(termDB.get(i) != null){
				sb.append(df.format(termDB.get(i)));
			}else
				sb.append("0");

			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String createPolicyYear(int startPt, int endPt){
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
				sb.append(i);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getComboTablePremium(int startPt, int endPt){
		String mode=master.getComboInsPayMode();//frequecy will be same for insured and proposer..
		int Bppt = master.ComboBasePPT;
		int Tppt = master.ComboTermPPT;

		int maxPPT = Math.max(Bppt,Tppt);
		int minPPt = Math.min(Bppt, Tppt);

		long Baseprem=0;
		long TermPrem = 0;
		//Base
		if (master.getComboBaseProdType()!=null && master.getComboBaseProdType().equals("U")) {
			if (!(master.getComboBaseAnnPrem().equals("NA"))) {
				Baseprem = Long.parseLong(master.getComboBaseAnnPrem());
			}
			if(master.getComboInsPayMode().equals("O")) {
				Baseprem = master.getComboBaseAP();
			}
		} else
			Baseprem=master.getComboBaseQAP();

		//Term
		if (master.getComboTermProdType()!=null && master.getComboTermProdType().equals("U")) {
			if (!(master.getComboTermAnnPrem().equals("NA"))) {
				//annual prem..
				TermPrem = Long.parseLong(master.getComboTermAnnPrem());
			}
			if(master.getComboInsPayMode().equals("O")) {
				//base annualized..
				TermPrem = master.getComboTermAP();
			}
		} else
			TermPrem=master.getComboTermQAP();//Quoted annual prem..




		//String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");



		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
				if((i>maxPPT) )
				{
					sb.append("0");
				}else{
					//no tata discount for term..
					//if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
					//	sb.append(master.getDiscountedBasePrimium());
					//}else{
					if(i<=Bppt){
						//if ("U".equals(master.getComboBaseProdType()))
							if(i<=Tppt)
								sb.append(df.format(Baseprem + TermPrem));
							else
								sb.append(df.format(Baseprem));
						//else
							//sb.append(Baseprem + TermPrem);
					}else{
						sb.append(TermPrem);
					}
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	//Combo

    public String getWPPNote(){
        StringBuffer sb=new StringBuffer();

        sb.append("<tr><td colspan=\"14\" align=\"left\"><div class=\"boxnote\"><em>**The benefit equals all future premiums payable under the base policy from the date of claim for a period as specified in the policy contract. Please refer the policy contract for more details.</em></div></td></tr>");

        return sb.toString();
    }
	public String getRiderDisclaimer(){

		ArrayList aList=master.getRiderData();
		AddressBO oAddressBO=master.getOAddressBO();
		String versionString = "";
		//String imagePath = "";
		if ((master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y")) || master.getRequestBean().getBaseplan().contains("IONEV1N") || master.getRequestBean().getBaseplan().contains("FGV1N1"))
			if (master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y"))
				versionString = "CITI_";
			else
				versionString="";
		versionString += oAddressBO.getSIS_VERSION();
		//imagePath = master.getTempImagePath();
		InputStream imagePath = null;
		byte[] imgBuffer = null;
		StringBuilder stringBuilder = new StringBuilder();
		String disclaimString = "";

		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			String inputFile="";
			try {
				/*if(master.getRequestBean().getBaseplan().contains("IONEV1N"))
					inputFile = oRiderBO.getCode() + "_IO.html";
				else if(master.getRequestBean().getBaseplan().contains("FGV1N1"))
					inputFile = oRiderBO.getCode() + "_FG.html";
				else*/
					inputFile = oRiderBO.getCode() + ".html";
				BufferedReader reader = new BufferedReader(/* new FileReader(inputFile)*/new InputStreamReader(AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/" + inputFile),"UTF-8"));

				String line = null;
				String ls = System.getProperty("line.separator");
				while((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		aList=null;
		disclaimString = stringBuilder.toString();
		StringBuffer sb = new StringBuffer(disclaimString);
		disclaimString = disclaimString.replace("||VERSION||", versionString);

		try{
			imagePath = AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/Header_" + master.getPlanDescription() + ".jpg");
			imgBuffer =new byte[imagePath.available()];
		}catch(IOException e){
			e.printStackTrace();
		}
		String imgString=new String(imgBuffer);
		Bitmap bm = BitmapFactory.decodeStream(imagePath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100 , baos);
		byte[] b = baos.toByteArray();
		String replaceString = Base64.encodeToString(b, Base64.DEFAULT);
		disclaimString = disclaimString.replace("||IMGNAME||", "\"data:image/jpg;base64," + replaceString + "\"");


		return disclaimString;
	}

	public String getTablePA(int startPt, int endPt){
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
				sb.append(i);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableERPA(int startPt, int endPt){//Added for Easy Retire
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
				if(i==endPt)
					sb.append("Till survival");
				else
					sb.append(i);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
    public String getTableAge(int startPt, int endPt, int age){
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
                if(age==0)
                    sb.append("");
                else
                    sb.append(age);
                sb.append("</td></tr>");
                if(age>0)
                    age++;
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");

        return sb.toString();
    }

	public String getTablePremium(int startPt, int endPt){
		String mode=master.getRequestBean().getFrequency();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long prem=0;
		if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U")) {
			if (!(master.getAnnualPremium().equals("NA"))) {
				prem = Long.parseLong(master.getAnnualPremium());
			}
			if(master.getRequestBean().getFrequency().equals("O")) {
				prem = master.getBaseAnnualizedPremium();
			}
		} else
			prem=master.getQuotedAnnualizedPremium();
		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;border-left:0;border-right:0;border-top:0;padding-left:5px\">");
				if(("O".equals(mode) && i>1) || (i>ppt) )
				{
					sb.append("0");
				}else{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						if ("U".equals(planType))
							sb.append(df.format(prem));
						else
							sb.append(prem);
					}
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableTotalPremium(int startPt, int endPt){
		HashMap hmTotalPremAnnual = master.getTotalPremiumAnnual();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalPremAnnual.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremAnnual=null;
		return sb.toString();

	}
	public String getTableTotalPremium_MIP(int startPt, int endPt){
		HashMap hmTotalPremAnnual = master.getTotalPremiumAnnual();
		long riderPremTotal = 0;
		int pt = master.getRequestBean().getPolicyterm();
		String mode = master.getRequestBean().getFrequency();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		String planType = DataAccessClass.getPlanType(master.getRequestBean()
				.getBaseplan());
		ArrayList aList = master.getRiderData();

		for (int i = 1; i <= pt; i++) {
			for (int j = 0; j < aList.size(); j++) {
				RiderBO oRiderBO = (RiderBO) aList.get(j);
				riderPremTotal = riderPremTotal + master.getTotAnnRiderModalPremium();
			}
			if (("O".equals(mode) && i > 1) || (i > ppt)) {
				hmTotalPremAnnual.put(i, 0);
			} else {
				if (("C".equals(planType))
						&& ("Y".equals(master.getRequestBean()
						.getTata_employee())) && (i == 1)
					//&& !"O".equals(mode)) {
						) {
					// discounted premium
					hmTotalPremAnnual.put(i, master.getDiscountedBasePrimium()
							+ riderPremTotal);
				} else {
					hmTotalPremAnnual.put(i, master.getQuotedAnnualizedPremium()
							+ riderPremTotal);
				}
			}
			riderPremTotal=0;	//added by jayesh
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (hmTotalPremAnnual.get(i)!=null) {
					if (master.isCombo())
						sb.append(df.format(hmTotalPremAnnual.get(i)));
					else
						sb.append(hmTotalPremAnnual.get(i));
				} else
					sb.append("0");
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremAnnual=null;
		return sb.toString();

	}

	public String getTableCoverage(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverage();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}
    public String getTableCoverage4_Gk(int startPt, int endPt){
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(master.getRequestBean().getSumassured());
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }
	public String getTableSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	public String getTableFRSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i<endPt)
					sb.append(surrValue.get(i));
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableALTSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getALTSCESurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableMaturityValue(int startPt, int endPt) {
        HashMap matValue=master.getMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}
	public String getTableBasicSumMIP(int startPt, int endPt){
        HashMap matValue=master.getBasicSumAssured();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i==endPt)
					sb.append(matValue.get(i)!=null?matValue.get(i):0);
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}
	public String getTableMilestone8(int startPt, int endPt){
        HashMap matValue=master.getMilestoneAdd8Dict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
	}
    public String getTableMilestone4(int startPt, int endPt){
        HashMap matValue=master.getMilestoneAdd8Dict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
    }
    public String getTableMBB(int startPt, int endPt){
        HashMap matValue=master.getMoneyBackBenefitDict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
    }
	public String getTableMaturityValueALTSCE(int startPt, int endPt){
		HashMap matValue=master.getMaturityValALTSCE();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableTotalMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getTotalMaturityValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableRider(int startPt, int endPt){
		long totalRiderPrem=master.getTotalRiderModalPremium();
        if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)) {
            totalRiderPrem = totalRiderPrem * 2;
        } else if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.QUATERLY_MODE)) {
            totalRiderPrem = totalRiderPrem * 4;
        } else if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.MONTHLY_MODE)) {
            totalRiderPrem = totalRiderPrem * 12;
        }
		int ppt=master.getRequestBean().getPremiumpayingterm();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i<=ppt)
					sb.append(totalRiderPrem);
				else
					sb.append("0");
				sb.append("</td></tr>");

			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	public String getTableRider_MIP(int startPt, int endPt){
		long totalRiderPrem=master.getTotAnnRiderModalPremium();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		StringBuffer sb= new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i<=ppt)
					sb.append(totalRiderPrem);
				else
					sb.append("0");
				sb.append("</td></tr>");

			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	public String getUniquePDFFileName(String productName){
		String fileName = "";
		Date date=new Date();
		long currentTimeMillS=date.getTime();

		FileCounter ofileCounter=new FileCounter();
		//ofileCounter.countIncrementer();

		fileName=Long.toString(currentTimeMillS);
		fileName = productName + fileName+"_"+ofileCounter.countIncrementer()+ ".pdf";

		return fileName;
	}

	public String getUniqueJSPFileName(String productName){
		String fileName = "";
		Date date=new Date();
		long currentTimeMillS=date.getTime();

		FileCounter ofileCounter=new FileCounter();
		//ofileCounter.countIncrementer();

		fileName=Long.toString(currentTimeMillS);
		fileName = productName + fileName+"_"+ofileCounter.countIncrementer()+ ".html";

		return fileName;
	}

	public String getAddress(){
		AddressBO oAddressBO=master.getOAddressBO();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		sb.append("<tr><td align=\"center\">");
		sb.append(oAddressBO.getCO_NAME());
		sb.append("</td></tr>");

		sb.append("<tr><td align=\"center\" >");
		sb.append(oAddressBO.getCO_ADDRESS());
		sb.append("</td></tr>");

		sb.append("<tr><td align=\"center\" >");
		sb.append(oAddressBO.getCO_HELP_LINE());
		sb.append("</td></tr>");

		sb.append("<tr><td>");
        sb.append(oAddressBO.getFILLER1());
		sb.append("</td></tr>");
		sb.append("</table>");
		oAddressBO=null;
		return sb.toString();
	}

	public String getRiderNames(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(getFormattedString(oRiderBO.getDescription(), "&", "&amp;"));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderUIN(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			if (oRiderBO.getRdlUIN()!=null) {
				sb.append(oRiderBO.getRdlUIN());
			} else {
				sb.append("");
			}
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}
	public String getRiderSA(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			if (oRiderBO.getSumAssured()==0) {
				sb.append("**");
			} else {
				sb.append(oRiderBO.getSumAssured());
			}
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderModalPremium(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getModalpremium());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderPolTerm(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPolTerm());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderPremPayTerm(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPremPayTerm());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getFormattedString(String strString,String strToReplace, String strWithReplace){
		String strReplaced="";
		strReplaced=strString.replace(strToReplace, strWithReplace);
		return strReplaced;
	}

	public String getRiderBasePrem(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPrimium());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getTablePremiumAllocationCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAllocCharg=master.getPremiumAllocationCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAllocCharg.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAllocCharg=null;
		return sb.toString();
	}

	public String getTableAmountForInvestment(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAmtInv=master.getAmountForInvestment();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAmtInv.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAmtInv = null;
        return sb.toString();
	}

	public String getTableOtherBenifitInbuiltCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmOBIC=master.getOtherBenefitCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmOBIC.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmOBIC=null;
        return sb.toString();
	}

	public String getTablePolicyAdminCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAdmChrg=master.getPolicyAdminCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAdmChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmAdmChrg = null;
        return sb.toString();
	}

	public String getTableMortalityCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortChrg=master.getMortalityCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmMortChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortChrg=null;
		return sb.toString();
	}

	public String getTableMortalityChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortChrg=master.getMortalityChargesPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmMortChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortChrg=null;
		return sb.toString();
	}

	public String getTableLoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmLoyaltyChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTablePH6LoyaltyCharges(){//Added for Loyalty bonus PH6
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmLoyaltyChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTableGuaranteeCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableOtherCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmOtherChrg=master.getOtherBenefitCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmOtherChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableApplicableServiceTax(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppTax=master.getApplicableServiceTaxPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAppTax.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppTax=null;
		return sb.toString();
	}

	public String getTableTotalCharge(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotC=master.getTotalchargeforPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmTotC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotC=null;
		return sb.toString();
	}

	public String getTablePH6TotalCharge(){//Added for PH6 total charge
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotC=master.getTotalchargeforPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmTotC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotC=null;
		return sb.toString();
	}

	public String getTableRegularFundValueBeforeFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValuePostFMC(){// Added by Samina for Regular value ost FMC
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValuePostFMCPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValuePostFMC6(){// Added by Samina for Regular value post FMC for PH6
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValuePostFMCPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableFundManagementCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFMC=master.getFundManagmentChargePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFMC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFMC = null;
        return sb.toString();
	}

	public String getTableRegularFundValAtEndPolicyYearBeforeGMA(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
        return sb.toString();
	}


	public String getTableGuaranteedMaturityAddition(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmGMA=master.getGuaranteedMaturityAdditionPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmGMA.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmGMA=null;
		return sb.toString();
	}


	public String getTableTotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalFundValueAtEndPolicyYearPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmFundVal = null;
        return sb.toString();
	}

	public String getTableSurrenderValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValuePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmSurrVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableDeathBenifitAtEndOfPolicyYear(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYear();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmDB.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}

	public String getTableDeathBenifitAtEndOfPolicyYearPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYearPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmDB.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}

	public String getTableApplicableServiceTax_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppTax=master.getApplicableServiceTaxPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAppTax.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppTax=null;
		return sb.toString();
	}

	public String getTableFundManagementCharges_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFMC=master.getFundManagmentChargePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFMC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFMC=null;
		return sb.toString();
	}

	public String getTableRegularFundValAtEndPolicyYearBeforeGMA_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
        return sb.toString();
	}

	public String getTableGuaranteedMaturityAddition_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmGMA=master.getGuaranteedMaturityAdditionPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmGMA.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmGMA=null;
		return sb.toString();
	}

	public String getTableTotalFundValue_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalFundValueAtEndPolicyYearPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmFundVal = null;
        return sb.toString();
	}

	public String getTableSurrenderValue_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValuePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmSurrVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValueBeforeFMC_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableCommissionPayble(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmCommPaybleVal=master.getCommissionPayble();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmCommPaybleVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmCommPaybleVal = null;
		return sb.toString();
	}

	public String getTableGAI(int startPt, int endPt){
		HashMap GAIMap = master.getGuranteedAnnualIncome();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableGAIMBP(int startPt, int endPt){
		HashMap GAIMap = master.getGuranteedAnnualIncomeAll();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getAccTableGAI(int startPt, int endPt){
		HashMap GAIMap = master.getAccGuranteedAnnualIncome();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}


	public String getTableRPGAI(int startPt, int endPt){
		HashMap GAIMap = master.getRPGAI();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableALTSCEGAI(int startPt, int endPt){
		HashMap GAIMap = master.getALTSCEGAI();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableGuranteedSuurValue(int startPt, int endPt){
		HashMap gurSurrValMap = master.getGuranteedSurrenderValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(gurSurrValMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		gurSurrValMap=null;
		return sb.toString();
	}

	public String getTableLTGuranteedSuurValue(int startPt, int endPt){
		HashMap gurSurrValMap = master.getALTguranteedSurrenderValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(gurSurrValMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		gurSurrValMap=null;
		return sb.toString();
	}


	public String getTable4VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVSRBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableAlt4VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getAltVSRBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableEBDURING(int startPt, int endPt){
		HashMap hmEBDURING=master.getAltEBDURING();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmEBDURING.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmEBDURING=null;
		return sb.toString();
	}
	public String getTableTotGuarBenefit(int startPt, int endPt){
		HashMap totGuarBenefit=master.getTotalGuaranteedBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(totGuarBenefit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		totGuarBenefit=null;
		return sb.toString();
	}
	public String getTable4TerminalBonus(int startPt, int endPt){
		HashMap hmTerminalBonus=master.getTerminalBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTerminalBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTerminalBonus=null;
		return sb.toString();
	}

	public String getTable4TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	/** Vikas - Added for Mahalife Gold*/
	public String getTableTotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}
	/** Vikas - Added for Mahalife Gold*/
	// for 8% money maxima

	public String getTable8VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVSRBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableAlt8VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getAltVSRBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTable8TerminalBonus(int startPt, int endPt){
		HashMap hmTerminalBonus=master.getTerminalBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTerminalBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTerminalBonus=null;
		return sb.toString();
	}

	public String getTable8TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getTable8SurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit_8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTable8FRSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit_8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i<endPt)
					sb.append(surrValue.get(i));
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	public static String getFormattedDate(String strDate ){
		try {
			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"MM/dd/yyyy");
			Date date = sdfSource.parse(strDate);
			SimpleDateFormat sdfDestination = new SimpleDateFormat(
					"dd-MMM-yyyy");
			strDate = sdfDestination.format(date);

		} catch (Exception pe) {
		}

		return strDate;
	}
    public static String getFormattedDateRaksha(String strDate ){
        try {
			//formater 1 changed for ibl by ganesh on 05-07-2016
            SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = sdfDate1.parse(strDate);
            SimpleDateFormat sdfDate2 = new SimpleDateFormat("dd-MMM-yyyy");
            strDate = sdfDate2.format(date);
        } catch (Exception pe) {

        }

        return strDate;
    }

//For Gold and Gold Plus Mohammad Rashid

	public String getTableIPC(int startPt, int endPt){
		HashMap IPCMap = master.getGuranteedAnnualInflationCover();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(IPCMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		IPCMap=null;
		return sb.toString();
	}
//For Gold and Gold Plus Mohammad Rashid

	public String getTableRiderOnPPT(int startPt, int endPt, int showPt){
		long totalRiderPrem = Math.round(master.getTotalRiderModalPremium());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				if(showPt>=i){
					sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
					sb.append(totalRiderPrem);
					sb.append("</td></tr>");
				} else {
					sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
					sb.append("0");
					sb.append("</td></tr>");
				}
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getTablePremiumPaidDuringYear(int startPt, int endPt){
		int rpuy = Integer.parseInt(master.getRequestBean().getRpu_year());
		long prem=master.getQuotedAnnualizedPremium();
		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(rpuy>i)
				{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						sb.append(prem);
					}
				}else{
					sb.append("0");
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getTableTotalPremiumPaidCumulative(int startPt, int endPt){
		long premPaidcumulativeTotal=0;
		int pt = master.getRequestBean().getPolicyterm();
		int rpuy = Integer.parseInt(master.getRequestBean().getRpu_year());
		long qap = master.getQuotedAnnualizedPremium();
		HashMap hmPremPaidCumulative = new HashMap();
		for (int i = 1; i <= pt; i++) {
			if(rpuy>i){

				premPaidcumulativeTotal=premPaidcumulativeTotal+qap;
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
			else{
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmPremPaidCumulative=null;
		return sb.toString();
	}

	public String getTableCoverageAltSce(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverageAltSce();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}

	public String getTableCoverageAlternateSce(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverageAlternateSce();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}

	public String getTableAltSceMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableTOTAltSceMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getTOTaltScenarioMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltSceTable4TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getAltSceTable8TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getAltSceTableMaturityValue4(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltSceTableMaturityValue8(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltScePremiumPaidDuringYear(int startPt, int endPt){
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long prem=master.getQuotedAnnualizedPremium();
		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(ppt>=i)
				{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						sb.append(prem);
					}
				}else{
					sb.append("0");
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getAltSceTotalPremiumPaidCumulative(int startPt, int endPt){
		long premPaidcumulativeTotal=0;
		int pt = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long qap = master.getQuotedAnnualizedPremium();
		HashMap hmPremPaidCumulative = new HashMap();
		for (int i = 1; i <= pt; i++) {
			if(ppt>=i){
				premPaidcumulativeTotal=premPaidcumulativeTotal+qap;
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
			else{
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmPremPaidCumulative=null;
		return sb.toString();
	}

	public String getAltSceTotalMaturityBenefitCurrentRate(int startPt, int endPt){
		HashMap surrValue=master.getAltScenarioTotMatBenCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getAltSceSurrenderBenefitCurrentRate(int startPt, int endPt){
		HashMap surrValue=master.getAltScenarioSurrBenefitCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableTotalPremiumPaidCumulativeCurrentRate(int startPt, int endPt){
		HashMap hmTotalPremPaidCumulative = master.getAltScenarioPremPaidCumulativeCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremPaidCumulative=null;
		return sb.toString();
	}

	public double getTaxMMXText(int endPt) {
		HashMap hmTotalPremPaidCumulative = master.getAltScenarioPremPaidCumulativeCurrentRate();
		return Double.parseDouble(hmTotalPremPaidCumulative.get(endPt).toString());
	}

	public double getTaxALTText(int endPt) {
		HashMap surrValue=master.getTotalMaturityBenefit8();
		return Double.parseDouble(surrValue.get(endPt).toString());
	}

	public double getTax23Text(int endPt) {
		HashMap surrValue=master.getAltScenarioTotMatBenCurrentRate();
		return Double.parseDouble(surrValue.get(endPt).toString());
	}

	public String getTableFPSurrenderBenefit(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValueFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurrVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableFPRegularFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableTotalFPLoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyChrg.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTableFPDeathBenefit(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYearFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDB.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}
	//added on 06-06-2014

	public String getAltSceTableTaxSaving(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenarioTaxSaving();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSceTableAnnualEffectivePremium(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenarioannualizedeffectivepremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSceTableCummulativeEffectivePremium(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenariocummulativeeffectivepremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSce4MaturityBenefitTax(int startPt, int endPt){
		HashMap surrValue=master.getAltScenario4MaturityBenefitTax();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//End : added by jayesh for SMART7 : 07-05-2014

	public String getTableTopUpPrem(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap topUpPremium=master.getTopUpPremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(topUpPremium.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		topUpPremium=null;
		return sb.toString();
	}

	public String getTablePartialW() {
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getWithdrawalAmout();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableRPFVTUFVPFMC() {
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getRegularFundValuePostFMC();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpLoyalty(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpLoyalty();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableROFVTUFVEOY(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpRegularFundValueEndOfYear();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpSURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpSurrValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpDBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpDeathBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	//Added by Samina for AAA3
	public String getS_AAASurrenderValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurVal=master.getSurrenderValuePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurVal=null;
		return sb.toString();
	}

	public String getD_AAADeathBenifitAtEndOfPolYear(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDeathVal=master.getTotalDeathBenefitPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDeathVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDeathVal=null;
		return sb.toString();
	}

	public String getM_AAAMortalityCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortVal=master.getMortalityCharges_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmMortVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortVal=null;
		return sb.toString();
	}

	public String getT_AAATotalCharge(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotVal=master.getTotalchargeforPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmTotVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotVal=null;
		return sb.toString();
	}

	public String getAAA_ApplicableServiceTax(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppSerVal=master.getApplicableServiceTaxPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAppSerVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppSerVal=null;
		return sb.toString();
	}

	public String getR_AAARegularFundValueBeforeFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getF_AAAFundManagementCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getP_AAARegularFundValuePostFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getL_AAALoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyVal=master.getLoyaltyChargeforPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyVal=null;
		return sb.toString();
	}

	public String getTF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_LCEF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueAAA_LCEF_PH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_WLEF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueAAA_WLEF_PH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}
	//Added by Samina for AAA3

	public String getS_AAASurrenderValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurVal=master.getSurrenderValuePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurVal=null;
		return sb.toString();
	}

	public String getD_AAADeathBenifitAtEndOfPolYearPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDeathVal=master.getTotalDeathBenefitPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDeathVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDeathVal=null;
		return sb.toString();
	}

	public String getM_AAAMortalityChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortVal=master.getMortalityChargesPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmMortVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortVal=null;
		return sb.toString();
	}

	public String getT_AAATotalChargePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotVal=master.getTotalchargeforPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmTotVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotVal=null;
		return sb.toString();
	}

	public String getAAA_ApplicableServiceTaxPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppSerVal=master.getApplicableServiceTaxPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAppSerVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppSerVal=null;
		return sb.toString();
	}

	public String getR_AAARegularFundValueBeforeFMCPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getF_AAAFundManagementChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getP_AAARegularFundValuePostFMCPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getL_AAALoyaltyChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyVal=master.getLoyaltyChargeforPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyVal=null;
		return sb.toString();
	}

	public String getTF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_LCEF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_LCEF_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_WLEF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_WLEF_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8RPFVE(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8SURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getSurrenderValuePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8DBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalDeathBenefitPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4RPFVE(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4SURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getSurrenderValuePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4DBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalDeathBenefitPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10MORTCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getMortalityCharges_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10TOTC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalchargeforPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10APPSERTAX(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getApplicableServiceTaxPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10RFVBFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10FMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10RFVPFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10LOYALTYCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getLoyaltyChargeforPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6MORTCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getMortalityCharges_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6TOTC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalchargeforPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6APPSERTAX(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getApplicableServiceTaxPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6RFVBFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6FMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6RFVPFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMARTLOYALTYCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getLoyaltyChargeforPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTable4SurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteed4SurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//added on 25-06-2014
	public String getTableANNUITYRate(int startPt, int endPt){//Added by Samina for Easy Retire
		HashMap annuityrate=master.getAnnuityRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(annuityrate.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		annuityrate=null;
		return sb.toString();
	}

	public String getAltSceVariableBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVariableBonus();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}
	//added on 26-06-2014

	public String getAltSceTotalDeathBenefitCR(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefitCR();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getTotalMaturityBenefit8(int startPt, int endPt){
		HashMap surrValue=master.getTotalMaturityBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTotalMaturityBenefit4(int startPt, int endPt){
		HashMap surrValue=master.getTotalMaturityBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getSurrenderBenefit8(int startPt, int endPt){
		HashMap surrValue=master.getSurrBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//End : added by jayesh for SMART7 : 07-05-2014

	public String getTablePremiumAllocationChargesPPT(){
		int ppt=master.getRequestBean().getPremiumpayingterm();
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAllocCharg=master.getPremiumAllocationCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i<=ppt)
				sb.append(df.format(hmAllocCharg.get(i)));
			else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAllocCharg=null;
		return sb.toString();
	}
    public String getTableRiderOnPPTMRS(int startPt, int endPt, int showPt){
        ArrayList aList=master.getRiderData();
		int ppt = 0;
		if(aList!=null && aList.size()>0) {
			RiderBO oRiderBO = (RiderBO) aList.get(0);
			ppt = oRiderBO.getPremPayTerm();//master.getRequestBean().getPremiumpayingterm();
		}
			//int pt=master.getRequestBean().getPolicyterm();
			HashMap totalRiderPrem = master.getTotalRiderAnnualPremium();
			StringBuffer sb = new StringBuffer();
			sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
			for (int i = startPt; i <= endPt; i++) {
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i <= ppt)
					sb.append(totalRiderPrem.get(i));
				else
					sb.append("0");
				sb.append("</td></tr>");
			}
			sb.append("</table>");
			totalRiderPrem=null;
			return sb.toString();
    }
	public String getTableRiderOnPPTSmart7(int startPt, int endPt, int showPt){
		int ppt=master.getRequestBean().getPremiumpayingterm();
		int pt=master.getRequestBean().getPolicyterm();
		HashMap totalRiderPrem=master.getTotalRiderAnnualPremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i<=ppt)
				sb.append(totalRiderPrem.get(i));
			else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		totalRiderPrem=null;
		return sb.toString();
	}
}
