package com.talic.plugins.sisEngine.view;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import com.talic.plugins.sisEngine.BO.DataMapperBO;
import com.talic.plugins.sisEngine.BO.IplIllussPagelist;
import com.talic.plugins.sisEngine.BO.PNLPlanLk;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.BO.STDTemplateValues;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.control.JCSCaching;
import com.talic.plugins.sisEngine.util.CommonConstant;


@SuppressWarnings("unchecked")
public class PageListGenerator {

	private ModelMaster master;
	public static DataMapperBO oDataMapperBO;

	static {
		oDataMapperBO = JCSCaching.getoDataMapperBO(null,null,null);
	}

	public PageListGenerator(ModelMaster mm) {
		this.master = mm;
	}


	public String generatePageList() {
		String pageList = "";
		String pageNos="";
		PNLPlanLk oPNLPlanLk = null;
		HashMap pnlPlanLkHashMap = oDataMapperBO.getPNLPlanLk();
		oPNLPlanLk = (PNLPlanLk)pnlPlanLkHashMap.get(master.getRequestBean().getBaseplan());
		int planId = oPNLPlanLk.getPNL_PGL_ID();
		int pt = master.getRequestBean().getPolicyterm();
		if (planId==198 || planId==199 || planId==200 || planId==201 || planId==202) {
			int additionalPages = 0;
			ArrayList oStdTemplateValues = oDataMapperBO.getStdTemplateValues();
			ArrayList riderData = master.getRiderData();
			for(int count = 0; count < oStdTemplateValues.size(); count++){
				STDTemplateValues oSTDTemplateValues =  (STDTemplateValues)oStdTemplateValues.get(count);
				if(planId == oSTDTemplateValues.getStd_pgl_id()){
					int NOP = pt>oSTDTemplateValues.getStd_min_pt()?oSTDTemplateValues.getStd_max_no_pages():oSTDTemplateValues.getStd_no_pages();
					int startPoint = oSTDTemplateValues.getStd_min_startp() + additionalPages;
					if (oSTDTemplateValues.getStd_mandatory().equals("Y")) {
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.Illustration)) {
							if(planId==200 || planId==201)
								pageNos = "4 to 7";
							else
								pageNos = "4 &amp; 5";
							if (pt>oSTDTemplateValues.getStd_min_pt()) {
								if(planId==200 || planId==201)
									pageNos = "4 to 9";
								else
									pageNos = "4 to 7";
							}
							master.getRequestBean().setIllusPageNo(pageNos);
						}
						for (int i=0;i<NOP;i++)
							pageList=pageList + (startPoint+i+",");
					} else {
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.ADDLRider)) {
							for(int i=0;i<riderData.size();i++){
								RiderBO oRiderBO=(RiderBO)riderData.get(i);
								if (oRiderBO.getCode().equals(oSTDTemplateValues.getStd_rdl_code())) {
									CommonConstant.printLog("d","ADDL Writing from "+startPoint+" for "+NOP+" no of pages");
									for (i=0;i<NOP;i++)
										pageList=pageList + (startPoint+i+",");
								}
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.WOPRider)) {
							for(int i=0;i<riderData.size();i++){
								RiderBO oRiderBO=(RiderBO)riderData.get(i);
								if (oRiderBO.getCode().equals(oSTDTemplateValues.getStd_rdl_code())) {
									CommonConstant.printLog("d","WOP Writing from "+startPoint+" for "+NOP+" no of pages");
									for (i=0;i<NOP;i++)
										pageList=pageList + (startPoint+i+",");
								}
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.WOPPRider)) {
							for(int i=0;i<riderData.size();i++){
								RiderBO oRiderBO=(RiderBO)riderData.get(i);
								if (oRiderBO.getCode().equals(oSTDTemplateValues.getStd_rdl_code())) {
									CommonConstant.printLog("d","WOPP Writing from "+startPoint+" for "+NOP+" no of pages");
									for (i=0;i<NOP;i++)
										pageList=pageList + (startPoint+i+",");
								}
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.ReducedPaidUP)) {
							if (master.getRequestBean().getRpu_year()!=null && !master.getRequestBean().getRpu_year().equals("0")) {
								CommonConstant.printLog("d","RPU Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.TaxBenefit)) {
							if (!master.getRequestBean().getTaxslab().equals("0")) {
								CommonConstant.printLog("d","TAX Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.ExperienceBasedRate)) {
							if (master.getRequestBean().getExpbonusrate()!=null && !master.getRequestBean().getExpbonusrate().equals("0")) {
								CommonConstant.printLog("d","EBR Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.ExperiencebasedrateandTaxbenefit)) {
							if (!master.getRequestBean().getExpbonusrate().equals("0") && !master.getRequestBean().getTaxslab().equals("0")
									&& master.getRequestBean().getTaxslab()!=null) {
								CommonConstant.printLog("d","EBR and Tax Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.TOPUPPWD)) {
							if (master.getRequestBean().getTopUpWDSelected().equals("YES") || master.getRequestBean().getFpSelected().equals("YES")) {
								CommonConstant.printLog("d","TopUp Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.SMART)) {
							if (master.getRequestBean().getSmartSelected().equals("YES")) {
								CommonConstant.printLog("d","SMART Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
						if (oSTDTemplateValues.getStd_temp_desc().equals(CommonConstant.AAA)) {
							if (master.getRequestBean().getAAASelected().equals("YES")) {
								CommonConstant.printLog("d","AAA Writing from "+startPoint+" for "+NOP+" no of pages");
								for (int i=0;i<NOP;i++)
									pageList=pageList + (startPoint+i+",");
							}
						}
					}
					if (pt>oSTDTemplateValues.getStd_min_pt()) {
						additionalPages+=oSTDTemplateValues.getStd_max_no_pages()-oSTDTemplateValues.getStd_no_pages();
						CommonConstant.printLog("d","Current no of additional Pages "+additionalPages);
					}
				}
			}
			CommonConstant.printLog("d","PageList is "+pageList);
			oPNLPlanLk = (PNLPlanLk)pnlPlanLkHashMap.get(master.getRequestBean().getBaseplan());
			if (planId==198 || planId==199 || planId==200 || planId==201 || planId==202) {
				ArrayList oIplIllussPagelist = oDataMapperBO.getIplIllussPagelist();
				for(int count = 0; count < oIplIllussPagelist.size(); count++){
					IplIllussPagelist oIplIllussPagelistValues =  (IplIllussPagelist)oIplIllussPagelist.get(count);
					if(planId == oIplIllussPagelistValues.getIpl_pgl_id()){
						if (pt>oIplIllussPagelistValues.getIpl_min_pt()) {
							pageNos = oIplIllussPagelistValues.getIpl_max_page();
						} else {
							pageNos = oIplIllussPagelistValues.getIpl_min_page();
						}

					}
				}
			}
		}
		CommonConstant.printLog("d","Illustration Page Nos for Plan ID " + planId + " are " + pageNos);
		master.getRequestBean().setIllusPageNo(pageNos);
		return pageList;
	}
}
