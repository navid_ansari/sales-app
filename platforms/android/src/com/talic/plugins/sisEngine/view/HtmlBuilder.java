package com.talic.plugins.sisEngine.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.talic.plugins.AppConstants;
import com.talic.plugins.sisEngine.BO.AddressBO;
import com.talic.plugins.sisEngine.BO.FundDetailsLK;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.bean.SISRequestBean;
import com.talic.plugins.sisEngine.db.DataAccessClass;
import com.talic.plugins.sisEngine.util.CommonConstant;
import com.talic.plugins.sisEngine.util.FileCounter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unchecked")
public class HtmlBuilder {
	private ModelMaster master;
	public HtmlBuilder(ModelMaster master){
		this.master=master;
	}
	DecimalFormat df = new DecimalFormat("#,###");

	public String buildHtml(String outputFile) throws IOException {
		String filedata=null;
		//byte[] pdfdata =null;
		try{
			String altYesNo = "N";
			if ((master.getRequestBean().getTopUpWDSelected() != null && master.getRequestBean().getTopUpWDSelected().equals("YES"))
					|| (master.getRequestBean().getFpSelected() != null && master.getRequestBean().getFpSelected().equals("YES"))
					|| (master.getRequestBean().getSmartSelected() != null && master.getRequestBean().getSmartSelected().equals("YES"))
					|| (master.getRequestBean().getRpuSelected() != null && master.getRequestBean().getRpuSelected().equals("YES"))
					|| (master.getRequestBean().getTaxslabSelected() != null && master.getRequestBean().getTaxslabSelected().equals("YES"))
					|| (master.getRequestBean().getExpbonusSelected() != null && master.getRequestBean().getExpbonusSelected().equals("YES"))) {
				altYesNo = "P";
			}
			String fileNameOptional = ".html";
			if (outputFile==null) {
				outputFile = AppConstants.SA_APP_CONTEXT + "\\" + AppConstants.ROOT_FOLDER + "\\FromClient\\SIS\\PDF\\";
			}
			if (master.getRequestBean().getNegativeError()!=null) {
				fileNameOptional = "_Error.html";
			}
			else if((master.getRequestBean().getRpuSelected()!=null && master.getRequestBean().getRpuSelected().equals("YES"))
					&& (master.getRequestBean().getTaxslabSelected()!=null && master.getRequestBean().getTaxslabSelected().equals("YES"))
					&& master.getRequestBean().getExpbonusSelected()!=null && master.getRequestBean().getExpbonusSelected().equals("YES")){
				fileNameOptional = "_all.html";
			}
			else if((master.getRequestBean().getRpuSelected()!=null && master.getRequestBean().getRpuSelected().equals("YES"))
					&& (master.getRequestBean().getTaxslabSelected()!=null && master.getRequestBean().getTaxslabSelected().equals("YES"))){
				fileNameOptional = "_alt12.html";
			}
			else if((master.getRequestBean().getRpuSelected()!=null && master.getRequestBean().getRpuSelected().equals("YES"))
					&& (master.getRequestBean().getExpbonusSelected()!=null && master.getRequestBean().getExpbonusSelected().equals("YES"))){
				fileNameOptional = "_alt13.html";
			}
			else if((master.getRequestBean().getTaxslabSelected()!=null && master.getRequestBean().getTaxslabSelected().equals("YES"))
					&& (master.getRequestBean().getExpbonusSelected()!=null && master.getRequestBean().getExpbonusSelected().equals("YES"))){
				fileNameOptional = "_alt23.html";
			}
			else if (((master.getRequestBean().getTopUpWDSelected() != null && master.getRequestBean().getTopUpWDSelected().equals("YES"))
					|| (master.getRequestBean().getFpSelected() != null && master.getRequestBean().getFpSelected().equals("YES")))
					&& (master.getRequestBean().getSmartSelected() != null && master.getRequestBean().getSmartSelected().equals("YES"))) {
				fileNameOptional = "_all.html";
			} else if ((master.getRequestBean().getTopUpWDSelected() != null && master.getRequestBean().getTopUpWDSelected().equals("YES"))
					|| (master.getRequestBean().getFpSelected() != null && master.getRequestBean().getFpSelected().equals("YES"))
					|| (master.getRequestBean().getRpuSelected()!=null && master.getRequestBean().getRpuSelected().equals("YES"))) {
				fileNameOptional = "_alt1.html";
			} else if ((master.getRequestBean().getSmartSelected() != null && master.getRequestBean().getSmartSelected().equals("YES"))
					/*|| (master.getRequestBean().getTaxslabSelected()!=null && master.getRequestBean().getTaxslabSelected().equals("YES"))*/) {
				fileNameOptional = "_alt2.html";
			}else if(master.getRequestBean().getExpbonusSelected()!=null && master.getRequestBean().getExpbonusSelected().equals("YES")){
				fileNameOptional = "_alt3.html";
			}

			InputStream templateFile = AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/" + master.getPlanDescription() + fileNameOptional);


			byte[] inputBuffer =new byte[templateFile.available()];

			int noOfBytesRead = templateFile.read(inputBuffer);
			String inputString=new String(inputBuffer);

			Pattern p = Pattern.compile("\\|\\|[a-zA-Z_0-9\\p{Punct}]+\\|\\|");
			StringBuffer sb = new StringBuffer();
			// Create a matcher with an input string
//			PageListGenerator oPLgen = new PageListGenerator(master);
//			String pageList = oPLgen.generatePageList();
			while (noOfBytesRead > 0) {
				Matcher m = p.matcher(inputString);
				//StringBuffer sb = new StringBuffer();
				boolean result = m.find();
				// Loop through and create a new String
				// with the replacements
				int iStart = 0;
				int iEnd = 0;
				SISRequestBean reqBean=master.getRequestBean();
				while(result) {
					iStart = m.start();
					iEnd = m.end();
					String pattern = inputString.substring(iStart, iEnd);
					pattern=pattern.replace('|',' ');
					pattern = pattern.trim();
					if(pattern.startsWith(CommonConstant.POLICYOPTION))
					{
						String replaceString = "OPTION 1";
						if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("MBPV1N1")
								|| master.getRequestBean().getBaseplan().equals("FR7V1P1") || master.getRequestBean().getBaseplan().equals("FR10V1P1")
								|| master.getRequestBean().getBaseplan().equals("FR15V1P1"))
							replaceString = "OPTION 1";
						else if (master.getRequestBean().getBaseplan().equals("SGPV1N2") || master.getRequestBean().getBaseplan().equals("MBPV1N2")
								|| master.getRequestBean().getBaseplan().equals("FR7V1P2") || master.getRequestBean().getBaseplan().equals("FR10V1P2")
								|| master.getRequestBean().getBaseplan().equals("FR15V1P2"))
							replaceString = "OPTION 2";
						else if (master.getRequestBean().getBaseplan().equals("MBPV1N3"))
							replaceString = "OPTION 3";
						else if (master.getRequestBean().getBaseplan().equals("SIP7IV1N1")
								|| master.getRequestBean().getBaseplan().equals("SIP10IV1N1")
								|| master.getRequestBean().getBaseplan().equals("SIP12IV1N1"))
							replaceString = "Regular Income Benefit";
						else if (master.getRequestBean().getBaseplan().equals("SIP7EV1N1")
								|| master.getRequestBean().getBaseplan().equals("SIP10EV1N1")
								|| master.getRequestBean().getBaseplan().equals("SIP12EV1N1"))
							replaceString = "Endowment Benefit";
						else if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV"))
							replaceString = "Pro Care";
						else if (master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
							replaceString = "Pro Care Plus";
						else if (master.getRequestBean().getBaseplan().contains("VCPSDV") || master.getRequestBean().getBaseplan().contains("VCPFDV"))
							replaceString = "Duo Care";
						else if (master.getRequestBean().getBaseplan().contains("VCPSDPV") || master.getRequestBean().getBaseplan().contains("VCPFDPV"))
							replaceString = "Duo Care Plus";
						else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N1"))
							replaceString = "Option 1: \"Sum Assured on Death\" payable on Death";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N2"))
							replaceString = "Option 2: \"Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N3"))
							replaceString = "Option 3: \"Enhanced Sum Assured on Death\" payable on Death";
                        else if (master.getRequestBean().getBaseplan().startsWith("SR") && master.getRequestBean().getBaseplan().contains("V1N4"))
							replaceString = "Option 4: \"Enhanced Sum Assured on Death\" payable on Death &amp; Monthly Income thereafter for 10 years";
						else if (master.getRequestBean().getBaseplan().startsWith("GIP5I10") || master.getRequestBean().getBaseplan().startsWith("GIP12I10"))
							replaceString = "Income Term 10";
						else if (master.getRequestBean().getBaseplan().startsWith("GIP5I15") || master.getRequestBean().getBaseplan().startsWith("GIP12I15"))
							replaceString = "Income Term 15";
						else if (master.getRequestBean().getBaseplan().startsWith("MLS2NA"))
							replaceString = "Option A";
						else if (master.getRequestBean().getBaseplan().startsWith("MLS2NB"))
							replaceString = "Option B";
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.COVERPAGE)) {
						String replaceString = "";
						if("IndusSolution".equalsIgnoreCase(reqBean.getBusinessType())){
							replaceString = "''";
						}else {
							replaceString = "none";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.FIRSTPAGE)) {
						String replaceString = "";
						if("IndusSolution".equalsIgnoreCase(reqBean.getBusinessType())){
							replaceString = "always";
						}else {
							replaceString = "none";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ONLYVERSION)) {
						AddressBO oAddressBO=master.getOAddressBO();
						String replaceString = oAddressBO.getSIS_VERSION();
						replaceString = replaceString.substring(0,5);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.AGEPROOF)) {
						String replaceString = reqBean.getAgeProof();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PROPNO)) {
						String replaceString = reqBean.getProposal_no();
						m.appendReplacement(sb, replaceString);
					}
                    //Changed by Ganesh for iRaksha 06-05-2016
					if(pattern.startsWith(CommonConstant.PROPDATE)) {
                        SimpleDateFormat sdfDate = null;
						if(master.getRequestBean().getBaseplan().startsWith("MRS") || master.getRequestBean().getBaseplan().startsWith("IRS") || master.getRequestBean().getBaseplan().startsWith("IRT")){
                            sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");//dd/MM/yyyy
                        }else {
                            sdfDate = new SimpleDateFormat("MM-dd-yyyy,hh:mm a");//dd/MM/yyyy
                        }
						Date now = new Date();
						String replaceString = sdfDate.format(now);
						m.appendReplacement(sb, replaceString);
					}
					//SISUIN
					if(pattern.startsWith(CommonConstant.SISUIN)) {
						String replaceString = master.getUINNumber();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INSNAME)) {
						String replaceString = reqBean.getInsured_name();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.COVPINSNAME)) {
						String replaceString="";
						if(reqBean.getInsured_sex().equals("M")) {
							replaceString = ("Mr. "+reqBean.getInsured_name());
						}
						else{
							replaceString = ("Ms. "+reqBean.getInsured_name());
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PROPNAME)) {
						String replaceString = reqBean.getProposer_name();
                        if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV") || master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
                            replaceString = "";
						m.appendReplacement(sb, replaceString);
					}
                    //Changed by Ganesh for iRaksha 06-05-2016
					if(pattern.startsWith(CommonConstant.INSDOB)) {
                        String replaceString = "";
                        if(master.getRequestBean().getBaseplan().startsWith("MRS") || master.getRequestBean().getBaseplan().startsWith("MLS") || master.getRequestBean().getBaseplan().startsWith("IRS") || master.getRequestBean().getBaseplan().startsWith("IRT")){
                            replaceString = getFormattedDateRaksha(reqBean.getInsured_dob());
                        }else {
                            replaceString = getFormattedDate(reqBean.getInsured_dob());
                        }
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INSOCC)) {
						String replaceString = reqBean.getInsured_occupation();
						m.appendReplacement(sb, replaceString);
					}
					/*if(pattern.startsWith(CommonConstant.INSDESCOCC)) {
						String replaceString = "";
						if (reqBean.getInsured_occupation_desc()!=null) {
							replaceString = reqBean.getInsured_occupation_desc();
						}
						else {
							replaceString = reqBean.getInsured_occupation();
						}
						replaceString = replaceString.replace("&","&amp;");
						replaceString = replaceString.replace("<","&lt;");
						replaceString = replaceString.replace(">","&gt;");
						replaceString = replaceString.replace("\"","&quot;");
						m.appendReplacement(sb, replaceString);
					}*/
					if(pattern.startsWith(CommonConstant.INSDESCOCC)) {
						String replaceString = "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INSAGE)) {
						String replaceString = reqBean.getInsured_age()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INSGENDER)) {
						String replaceString = reqBean.getInsured_sex().equals("M")?"Male":"Female";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ER_INSGENDER)) {//For Easy Retire
						String replaceString = reqBean.getInsured_sex().equals("M")?"Male":"Female";
						m.appendReplacement(sb, replaceString);
					}
                    //Changed by Ganesh for iRaksha 06-05-2016
					if(pattern.startsWith(CommonConstant.PROPDOB)) {
                        String replaceString = "";
                        if (master.getRequestBean().getBaseplan().startsWith("MRS") || master.getRequestBean().getBaseplan().startsWith("MLS") || master.getRequestBean().getBaseplan().startsWith("IRS") || master.getRequestBean().getBaseplan().startsWith("IRT")) {
                            replaceString = getFormattedDateRaksha(reqBean.getProposer_dob());
                        } else {
                            replaceString = getFormattedDate(reqBean.getProposer_dob());
                        }
                        m.appendReplacement(sb, replaceString);
                    }
					if(pattern.startsWith(CommonConstant.PROPOCC)) {
						String replaceString = reqBean.getProposer_occupation();
						m.appendReplacement(sb, replaceString);
					}
					/*if(pattern.startsWith(CommonConstant.PROPDESCOCC)) {
						String replaceString = "";
						if (reqBean.getProposer_occupation_desc()!=null)
							replaceString = reqBean.getProposer_occupation_desc();
						else
							replaceString = reqBean.getProposer_occupation();
						replaceString = replaceString.replace("&","&amp;");
						replaceString = replaceString.replace("<","&lt;");
						replaceString = replaceString.replace(">","&gt;");
						replaceString = replaceString.replace("\"","&quot;");
                        if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV") || master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
                            replaceString = "";
						m.appendReplacement(sb, replaceString);
					}*/
					if(pattern.startsWith(CommonConstant.PROPDESCOCC)) {
						String replaceString = "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PROPAGE)) {
						String replaceString = reqBean.getProposer_age()+"";
                        if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV") || master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
                            replaceString = "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PROPGENDER)) {
						String replaceString = reqBean.getProposer_sex().equals("M")?"Male":"Female";;
                        if (master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV") || master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
                            replaceString = "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ER_PROPGENDER)) {//Added for Easy Retire
						String replaceString = reqBean.getProposer_sex().equals("M")?"Male":"Female";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLTERM)) {
						String replaceString = reqBean.getPolicyterm()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLPM)) {
						String replaceString = reqBean.getPremiummul()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLPPT)) {
						String replaceString = reqBean.getPremiumpayingterm()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INSSMOKER)) {
						String replaceString="";
						if(reqBean.getInsured_smokerstatus().equalsIgnoreCase("1"))
							replaceString = "Non Smoker";
						else
							replaceString = "Smoker";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLMODE)) {
						String replaceString="";

						if(reqBean.getFrequency()!=null && !reqBean.getFrequency().equalsIgnoreCase("")){
							if(reqBean.getFrequency().equalsIgnoreCase("A"))
								replaceString="Annual";
							if(reqBean.getFrequency().equalsIgnoreCase("S"))
								replaceString="Semi Annual";
							if(reqBean.getFrequency().equalsIgnoreCase("Q"))
								replaceString="Quarterly";
							if(reqBean.getFrequency().equalsIgnoreCase("M"))
								replaceString="Monthly";
							if(reqBean.getFrequency().equalsIgnoreCase("O"))
								replaceString="Single";
						}
						//replaceString = reqBean.getFrequency();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSA)) {
						String replaceString = reqBean.getSumassured()+"";
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							replaceString = df.format(reqBean.getSumassured())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNSGPPLSA))
					{
						long sumAssured = reqBean.getSumassured();
						sumAssured = sumAssured - 5000000;
						String replaceString = df.format(sumAssured)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLTOTCOMBOSA))
					{
						long sumAssured = reqBean.getSumassured();
						sumAssured = sumAssured + 5000000;
						String replaceString = df.format(sumAssured)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLMODPREM))
					{
						String replaceString = master.getModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSGP1MODPREM))
					{
						String replaceString = df.format(master.getModalPremiumSGP1())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSGP2MODPREM))
					{
						String replaceString = df.format(master.getModalPremiumSGP2())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLCOMBOCMODPREM))
					{
						long modPremium = master.getRequestBean().getBasepremiumannual();
						if (master.getRequestBean().getFrequency().equals("M")) {
							modPremium = (long) (Math.round(modPremium * 0.0883));
						}
						if (master.getRequestBean().getFrequency().equals("S")) {
							modPremium = (long) (modPremium * 0.51);
						}
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLFPSEC7_COMBOCMODPREM))
					{
						long modPremium = master.getComboprem1();
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSGPMRS_COMBOCMODPREM))
					{
						long modPremium = master.getCombo1AnnualizedPremium();
						modPremium = modPremium + (long) (Math.round(modPremium * master.getServiceTaxFY()/100));
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSGP1PREM))
					{
						long modPremium = master.getCombo1AnnualizedPremium();
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSGP2PREM))
					{
						long modPremium = master.getCombo2AnnualizedPremium();
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLCOMBO2BP))
					{
						long modPremium = master.getCombo2AnnualizedPremium();
						modPremium = modPremium + (long) (Math.round(modPremium * master.getServiceTaxMain()/100));
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLCOMBOUMODPREM))
					{
						long modPremium = master.getRequestBean().getBasepremiumannual();
						String replaceString = df.format(modPremium)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTPREM)) {
						String replaceString =master.getTotalModalPremium()+"";

						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTSERVTAX)) {
						String replaceString =master.getServiceTax()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTSTPREM)) {
						//String replaceString =master.getServiceTax()+"";
						String replaceString = (master.getTotalModalPremiumPayble())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTER_STPREM)) {
						String replaceString = (master.getTotalERModalPremPayble())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePA)) {
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTablePA(1,count);
						m.appendReplacement(sb, replaceString);
					}
                    //MRS new template 30-05-2016 Start
                    if(pattern.startsWith(CommonConstant.ANNUAL_QUOTEDMODPREM))
                    {
                        String replaceString =master.getQuotedAnnualizedPremium()+"";
                        m.appendReplacement(sb, replaceString);
                    }
					if(pattern.startsWith(CommonConstant.ANNUAL_TOTALRIDPREM))
					{
						long annRidPrem = master.getTotalRiderModalPremium();
						if(master.getRequestBean().getFrequency().equals("S")) {
							annRidPrem = annRidPrem * 2;
						} else if (master.getRequestBean().getFrequency().equals("Q")) {
							annRidPrem = annRidPrem * 4;
						} else if (master.getRequestBean().getFrequency().equals("M")) {
							annRidPrem = annRidPrem * 12;
						}
						String replaceString =annRidPrem+"";
						m.appendReplacement(sb, replaceString);
					}
                    if(pattern.startsWith(CommonConstant.TableMRSPA))
                    {
                        int count=0;
                        int pt=master.getRequestBean().getPolicyterm();
                        count = pt<15 ? pt : 15;
                        String replaceString = getTablePA(1,count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSAGE))
                    {
                        int count=0;
                        int pt=master.getRequestBean().getPolicyterm();
                        count = pt<15 ? pt : 15;
                        int age=(int)(master.getRequestBean().getInsured_age());
                        String replaceString = getTableAge(1, count, age);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSPEM))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt<15 ? pt : 15;
                        String replaceString = getTablePremium(1, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSRIDPEM))
                    {

                        int pt=master.getRequestBean().getPolicyterm();
                        int ppt=master.getRequestBean().getPremiumpayingterm();

                        String replaceString = getTableRiderOnPPTMRS(1, pt, ppt);
                        m.appendReplacement(sb, (replaceString==null)?"":replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSTOTPEM))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt<15 ? pt : 15;
                        String replaceString = getTableTotalPremium(1, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSCOV))
                    {
                        int count = 0;
                        int pt=master.getRequestBean().getPolicyterm();
                        count = pt<15 ? pt : 15;
                        String replaceString = getTableCoverage(1, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.TableMRSSURRBEN))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt<15 ? pt : 15;
                        String replaceString = getTableSurrenderBenefit(1, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSPA))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTablePA(16,count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSAGE))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        int age=(int)(master.getRequestBean().getInsured_age())+ 15;
                        String replaceString = getTableAge(16, count,age);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSPEM))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTablePremium(16, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSRIDPEM))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int ppt=master.getRequestBean().getPremiumpayingterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTableRiderOnPPTMRS(16, count, ppt);
                        m.appendReplacement(sb, (replaceString==null)?"":replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSTOTPEM))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTableTotalPremium(16, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSCOV))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTableCoverage(16, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSMATVAL))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTableMaturityValue(16, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    if(pattern.startsWith(CommonConstant.Table2MRSSURRBEN))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt>15 ? pt : 0;
                        String replaceString = getTableSurrenderBenefit(16, count);
                        m.appendReplacement(sb, replaceString);
                    }
                    //MRS new template 30-05-2016 End
					if(pattern.startsWith(CommonConstant.MRSTablePA)) {
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						count= pt<85 ? pt : 85;
						String replaceString = getTablePA(1,count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSGPPA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTablePA(1,pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableERPA))//Added for Easy Retire
					{
						int count=36;
						String replaceString = getTableERPA(0, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableULIPPA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt<30 ? pt : 30;
						String replaceString = getTablePA(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					/*if(pattern.startsWith(CommonConstant.TableULIPIOPA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt<30 ? pt : 30;
						String replaceString = getTableIOPA(1, pt);
						m.appendReplacement(sb, replaceString);
					}*/
					if(pattern.startsWith(CommonConstant.TableFPSEC7_ULIPPA))
					{
						int pt=14;
						String replaceString = getTablePA(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table15ULIPPA))
					{
						String replaceString = getTablePA(1, 15);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMaximaPA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						String replaceString = getTablePA(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAGE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						count = pt<30 ? pt : 30;
						int age=(int)(master.getRequestBean().getInsured_age());
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}
                    if(pattern.startsWith(CommonConstant.TablePropAGE))
                    {
                        int count=0;
                        int pt=master.getRequestBean().getPolicyterm();
                        int age = 0;
                        //int count = pt;
                        count = pt<30 ? pt : 30;
                        if(master.getRequestBean().getBaseplan().contains("VCPSPV") || master.getRequestBean().getBaseplan().contains("VCPFPV") || master.getRequestBean().getBaseplan().contains("VCPSPPV") || master.getRequestBean().getBaseplan().contains("VCPFPPV"))
                            age=0;
                        else
                            age=(int)(master.getRequestBean().getProposer_age());
                        if (master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
                            count= pt<85 ? pt : 85;
                        }
                        String replaceString = getTableAgeProp(1, count, age);
                        m.appendReplacement(sb, replaceString);
                    }
					if(pattern.startsWith(CommonConstant.MRSTableAGE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						int age=(int)(master.getRequestBean().getInsured_age());
						count= pt<85 ? pt : 85;
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSGPAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1, pt, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableERAGE))
					{
						int count=36;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableULIPAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt;
						//int count = pt<30 ? pt : 30;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}
					/*if(pattern.startsWith(CommonConstant.TableULIPIOAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt;
						//int count = pt<30 ? pt : 30;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableIOAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}*/
					if(pattern.startsWith(CommonConstant.TableFPSEC7_ULIPAGE))
					{
						int count = 14;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1,count, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table15ULIPAGE))
					{
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1,15, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMaximaAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1,count, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMaximaPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt;
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MRSTablePEM))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						//int count = pt;
						count = pt<85 ? pt : 85;
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableULIPPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt;
						//int count = pt<30 ? pt : 30;
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableRider(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MRSTableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = 0;
						count= pt<85 ? pt : 85;
						String replaceString = getTableRider(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableTotalPremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MIP_TableTOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTableTotalPremium_MIP(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MRSTableTOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = 0;
						count= pt<85 ? pt : 85;
						String replaceString = getTableTotalPremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableCOV))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;

						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableCoverage(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MRSTableCOV))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						count= pt<85 ? pt : 85;
						String replaceString = getTableCoverage(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableERCOV))//Added for Easy Retire
					{
						int count = 37;
						String replaceString = getTableCoverage(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMATVAL))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableMaturityValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableSurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSIPSSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>15 ? pt : 15;
						String replaceString = getTableSurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableGIPSSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableSurrenderBenefitGIP(1, pt, incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MRSTableSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = 0;
						count= pt<85 ? pt : 85;
						String replaceString = getTableSurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSGPSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					/*if(pattern.startsWith(CommonConstant.TableCombo1SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableComboSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.TableComboSGPSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableComboSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFPSEC7_Combo1SURRBEN))
					{
						int pt=14;
						String replaceString = getTableComboSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}*/
					if(pattern.startsWith(CommonConstant.TableALTSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTableALTSurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					//Added for page break PIR
					if(pattern.startsWith(CommonConstant.Table2PA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTablePA(31,count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2AGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						int age=(int)(master.getRequestBean().getInsured_age())+ 30;
						String replaceString = getTableAge(31, count,age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2PEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTablePremium(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2RIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableRider(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2TOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableTotalPremium(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2COV))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableCoverage(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2MATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableMaturityValue(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableSurrenderBenefit(31, count);
						m.appendReplacement(sb, replaceString);
					}
					//End
					if(pattern.startsWith(CommonConstant.TOTGUARBENEFIT)) {
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString =getTableTotGuarBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTGIPGUARBENEFIT)) {
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString =getTableTotGuarBenefit(1, pt+incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					/** Vikas - Added for Mahalife Gold  & Gold Plus*/
					if(pattern.startsWith(CommonConstant.MLG_TablePA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTablePA(1,count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TablePA))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTablePA(1,pt+incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1, count, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableAGE))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						int age=(int)(master.getRequestBean().getInsured_age());
						String replaceString = getTableAge(1,pt+incomeTerm, age);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TablePEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TablePEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTablePremium(1, pt+incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableERPURP))
					{
						int count = 37;
						String replaceString = getTablePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableRider(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MIP_TableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableRider_MIP(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableTOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableTotalPremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableTOTPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableTotalPremium(1, pt+incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableCOV))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableCoverage(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableCOV))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableCoverageGIP(1, pt, incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
                    /*if(pattern.startsWith(CommonConstant.Table4COV_GK))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        int count = pt<85 ? pt : 85;
                        String replaceString = getTableCoverage4_Gk(1, count);
                        m.appendReplacement(sb, replaceString);
                    }*/
					if(pattern.startsWith(CommonConstant.MLG_TableMATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableMaturityValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableMATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableMaturityValueGIP(1, pt, incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableBasicSum))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableBasicSumMIP(1, count);
						m.appendReplacement(sb, replaceString);
					}
					//Good Kid
					if(pattern.startsWith(CommonConstant.GK_TableMILESTONEADD8))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableMilestone8(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GK_TableMILESTONEADD4))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableMilestone4(1, pt);
						m.appendReplacement(sb, replaceString);
					}
                    if(pattern.startsWith(CommonConstant.GK_TableMBB))
                    {
                        int pt=master.getRequestBean().getPolicyterm();
                        String replaceString = getTableMBB(1, pt);
                        m.appendReplacement(sb, replaceString);
                    }
					//Good Kid
					if(pattern.startsWith(CommonConstant.TableALTSCEMATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableMaturityValueALTSCE(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableGSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableGuranteedSuurValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableGSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableGuranteedSuurValueGIP(1, pt, incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TABLEALTGSURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableLTGuranteedSuurValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TABLEGAI))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableGAI(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TABLEGAI))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int incomeTerm=master.getRequestBean().getIncomeBooster();
						String replaceString = getTableGAI(1, pt+incomeTerm);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MBP_TABLEGAI))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableGAIMBP(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.SGP_TABLEGAI))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getAccTableGAI(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TABLEALTGAI))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableRPGAI(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TABLEALTSCEGAI))//Added for GAI for old products ALTSCENARIO
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableALTSCEGAI(1, count);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.MLG_TABLE4ANNCASH))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTable4VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TABLE8ANNCASH))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTable8VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}


					if(pattern.startsWith(CommonConstant.MLG_Table4TOTALDEATHBENEFIT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableTotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_Table8TOTALDEATHBENEFIT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableTotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}


					if(pattern.startsWith(CommonConstant.MLG_TableNG4SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTableSurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFRNG4SURRBEN)){
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTableFRSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableNG8SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString = getTable8SurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFRNG8SURRBEN)) {
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable8FRSurrenderBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TOT_CASH_DIV_4))
					{
						//if(reqBean.getPolicyterm().equals(obj))
						String replaceString = String.valueOf(master.getTotalcashdividend4());
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TOT_CASH_DIV_8))
					{
						String replaceString = String.valueOf(master.getTotalcashdividend8());
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TOT_G_ANNCOUP))
					{
						String replaceString = String.valueOf(master.getTotalGAnnualCoupon());
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.MLG_TableGuranteedInflationCover))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTableIPC(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_TableDeathBenefit))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTableTotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_Table4VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTable4VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.MLG_Table8VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTable8VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAlt8VSRBONUS))//Added by Samina for reduced paid on 20/11/2014
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTableAlt8VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAlt4VSRBONUS))//Added by Samina for reduced paid on 20/11/2014
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTableAlt4VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_CURRATEPREMPAIDDURING))//Added by Samina for reduced paid on 20/11/2014
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<85 ? pt : 85;
						String replaceString =getTableEBDURING(1, count);
						m.appendReplacement(sb, replaceString);
					}

					/** Vikas - Added for Mahalife Gold & Gold Plus*/


					if(pattern.startsWith(CommonConstant.ADDR))
					{
						String replaceString = getAddress();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDNAME))
					{
						String replaceString = getRiderNames();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDUIN))
					{
						String replaceString = getRiderUIN();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDSA))
					{
						String replaceString = getRiderSA();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDMODPREM))
					{
						String replaceString = getRiderModalPremium();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLFEATURE) || pattern.startsWith(CommonConstant.PNLBENTYPE))
					{
						String replaceString = master.getPlanFeature();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.RIDTERM))
					{
						String replaceString = getRiderPolTerm();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDPPT))
					{
						String replaceString = getRiderPremPayTerm();
						m.appendReplacement(sb, replaceString);
					}
					if(master.getRequestBean().getFundList()!=null && master.getRequestBean().getFundList().size()!=0){
						ArrayList aFundList=master.getRequestBean().getFundList();

						for(int i=0;i<aFundList.size();i++){
							FundDetailsLK oFundDetailsLK=(FundDetailsLK)aFundList.get(i);
							if(pattern.startsWith(oFundDetailsLK.getCODE())){

								String replaceString = oFundDetailsLK.getPERCENTAGE()+"";
								m.appendReplacement(sb, replaceString);
							}
						}
						if(pattern.startsWith(CommonConstant.FMC))
						{
							String replaceString = master.getFmc()+"";
							m.appendReplacement(sb, replaceString);
						}

					}

					if(pattern.startsWith(CommonConstant.STRIDERPREM))
					{
						String replaceString =master.getSTRiderModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALRIDPREM))
					{
						String replaceString =master.getTotalRiderModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALPREMRIDST))
					{
						String replaceString =master.getModalPremium()+master.getTotalRiderModalPremium()+master.getSTRiderModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLBASEPREM))
					{
						String replaceString =master.getBaseAnnualizedPremium()+"";
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							replaceString = df.format(master.getRequestBean().getBasepremiumannual())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDBASEPREM))
					{
						String replaceString =getRiderBasePrem();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PREMIUMMULTI))
					{
						String replaceString =master.getPremiumMultiple()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePACVAL))
					{
						String replaceString =getTablePremiumAllocationCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PACPPTVAL))
					{
						String replaceString =getTablePremiumAllocationChargesPPT();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAMTINV))
					{
						String replaceString =getTableAmountForInvestment();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableOBIC))
					{
						String replaceString =getTableOtherBenifitInbuiltCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableADMCHRG))
					{
						String replaceString =getTablePolicyAdminCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMORTCHRG))
					{
						String replaceString =getTableMortalityCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6MORTCHRG))
					{
						String replaceString =getTableMortalityChargesPH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableGUARCHR))
					{
						String replaceString =getTableGuaranteeCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableOTHCHRG))
					{
						String replaceString =getTableOtherCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAPPSERTAX))
					{
						String replaceString =getTableApplicableServiceTax();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTOTC))
					{
						String replaceString =getTableTotalCharge();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableCTOTAL6))//Added for PH6 total charge
					{
						String replaceString =getTablePH6TotalCharge();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableRFVBFMC))
					{

						String replaceString =getTableRegularFundValueBeforeFMC();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableRFVPFMC)) // added by samina for regular fund value post FMC
					{
						String replaceString =getTableRegularFundValuePostFMC();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table6RFVPFMC)) // added by samina for regular fund value post FMC for PH6
					{
						String replaceString =getTableRegularFundValuePostFMC6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFMC))
					{
						String replaceString =getTableFundManagementCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableRFVAEPYBGMA))
					{
						String replaceString =getTableRegularFundValAtEndPolicyYearBeforeGMA();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableGMA))
					{
						String replaceString =getTableGuaranteedMaturityAddition();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTOTFUNDVAL))
					{
						String replaceString =getTableTotalFundValue();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSURRENDERVAL))
					{
						String replaceString =getTableSurrenderValue();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableDBEP))
					{
						String replaceString =getTableDeathBenifitAtEndOfPolicyYear();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6DBEP))
					{
						String replaceString =getTableDeathBenifitAtEndOfPolicyYearPH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6APPSERTAX))
					{
						String replaceString =getTableApplicableServiceTax_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6FMC))
					{
						String replaceString =getTableFundManagementCharges_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6RFVAEPYBGMA))
					{
						String replaceString =getTableRegularFundValAtEndPolicyYearBeforeGMA_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6GMA))
					{
						String replaceString =getTableGuaranteedMaturityAddition_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6TOTFUNDVAL))
					{
						String replaceString =getTableTotalFundValue_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6SURRENDERVAL))
					{
						String replaceString =getTableSurrenderValue_PH6();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePH6RFVBFMC))
					{
						String replaceString =getTableRegularFundValueBeforeFMC_PH6();
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.TableLoyaltyCharges))
					{
						String replaceString =getTableLoyaltyCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableMLOYALTY6))//Added for Loyalty bonus PH6
					{
						String replaceString =getTablePH6LoyaltyCharges();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableGuranteedAnnualIncome))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString =getTableGAI(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2GuranteedAnnualIncome))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString =getTableGAI(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableGuranteedSuurValue))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString =getTableGuranteedSuurValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2GuranteedSuurValue))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString =getTableGuranteedSuurValue(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLNSAP))
					{
						String replaceString =master.getModalPremiumForNSAP()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALRB8))
					{
						//String replaceString = master.getTOTALRB8()+""; //commented by Akhil and below code was added acc to iOS app.
						String replaceString = master.getTotalcashdividend8() + "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALRB4))
					{
						//String replaceString = master.getTOTALRB4()+""; //commented by Akhil and below code was added acc to iOS app.
						String replaceString = master.getTotalcashdividend4() + "";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALGAI))
					{
						//String replaceString = master.getTOTALGAI()+""; //commented by Akhil and below code was added acc to iOS app.

						String replaceString = master.getTotalGAnnualCoupon()+"";

						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TAXTEXT))
					{
						String replaceString = master.getTAXtext()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TAXMMTEXT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						long matVal = Math.round(getTaxMMXText(pt) * Double.parseDouble(master.getRequestBean().getTaxslab()) * 1.03);
						String replaceString = matVal+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TAXALTTEXT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						long matVal = Math.round(getTaxALTText(pt) * Double.parseDouble(master.getRequestBean().getTaxslab()) * 1.03);
						String replaceString = matVal+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TAX23TEXT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						long matVal = Math.round(getTax23Text(pt) * Double.parseDouble(master.getRequestBean().getTaxslab()) * 1.03);
						String replaceString = matVal+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Exp2Rate))
					{
						String rate=null;
						if(master.getRequestBean().getExpbonusrate().equals("110"))
							rate="40";
						else
							rate="110";
						String replaceString = Double.parseDouble(rate)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ExpRate))
					{
						String replaceString = Double.parseDouble( master.getRequestBean().getExpbonusrate())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.QUOTEDMODPREM))
					{
						String replaceString =master.getQuotedModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RENEWALMODPREM))//Added by Samina for Annual renewl premium on 18/11/2014
					{
						String replaceString =master.getRenewalModalPremium()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.S_AAASURRENDERVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getS_AAASurrenderValue();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.D_AAADBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getD_AAADeathBenifitAtEndOfPolYear();
						}
						m.appendReplacement(sb, replaceString);
					}
					//Rider Details display logic MRS.

					//Rider 1
					if(pattern.startsWith(CommonConstant.RDL1))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							replaceString =getFormattedString(oRiderBO.getDescription(), "&", "&amp;");
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLSA1))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							replaceString =oRiderBO.getSumAssured()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLTERM1))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							replaceString =oRiderBO.getPolTerm()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLMODPREM1))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							replaceString =oRiderBO.getModalpremium()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLUIN1))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							replaceString =oRiderBO.getRdlUIN();
						}
						if(replaceString!=null){
							m.appendReplacement(sb, replaceString);
						}else{
							m.appendReplacement(sb, "");
						}
					}
					// Rider 2
					if(pattern.startsWith(CommonConstant.RDL2))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							replaceString =getFormattedString(oRiderBO.getDescription(), "&", "&amp;");
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLSA2))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							replaceString =oRiderBO.getSumAssured()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLTERM2))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							replaceString =oRiderBO.getPolTerm()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLMODPREM2))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							replaceString =oRiderBO.getModalpremium()+"";
						}
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.RDLUIN2))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							replaceString =oRiderBO.getRdlUIN();
						}
						if(replaceString!=null){
							m.appendReplacement(sb, replaceString);
						}else{
							m.appendReplacement(sb, "");
						}
					}

					// Rider 3
					if(pattern.startsWith(CommonConstant.RDL3))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>2){
							RiderBO oRiderBO=(RiderBO)alist.get(2);
							replaceString =getFormattedString(oRiderBO.getDescription(), "&", "&amp;");
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLSA3))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>2){
							RiderBO oRiderBO=(RiderBO)alist.get(2);
							replaceString =oRiderBO.getSumAssured()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLTERM3))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>2){
							RiderBO oRiderBO=(RiderBO)alist.get(2);
							replaceString =oRiderBO.getPolTerm()+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLMODPREM3))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						if(alist.size()>2){
							RiderBO oRiderBO=(RiderBO)alist.get(2);
							replaceString =oRiderBO.getModalpremium()+"";
						}
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.TableTOTMATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableTotalMaturityValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table2TOTMATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt>30 ? pt : 0;
						String replaceString = getTableTotalMaturityValue(31, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table4VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable4VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR4VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable4VSRBonus(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table4TERMINALBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable4TerminalBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR4TERMINALBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable4TerminalBonus(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table4TOTALDEATHBENEFIT) || pattern.startsWith(CommonConstant.Table4TDB))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTable4TotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.TableFR4TOTALDEATHBENEFIT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable4TotalDeathBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}

					// for 8% maturity Bonus

					if(pattern.startsWith(CommonConstant.Table8VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable8VSRBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR8VSRBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable8VSRBonus(1, pt);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.Table8TERMINALBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable8TerminalBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR8TERMINALBONUS))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable8TerminalBonus(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table8TOTALDEATHBENEFIT) || pattern.startsWith(CommonConstant.Table8TDB))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTable8TotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR8TOTALDEATHBENEFIT))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTable8TotalDeathBenefit(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table8SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable8SurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.VERSION))
					{
						AddressBO oAddressBO=master.getOAddressBO();
						String replaceString = "";
						if (master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y"))
							replaceString = "CITI_";
						replaceString += oAddressBO.getSIS_VERSION();
						m.appendReplacement(sb, replaceString);
					}

					//Added by Mohammad Rashid for UL Products - 31-Mar-2014

					if(pattern.startsWith(CommonConstant.TableCOMMISSION))
					{
						String replaceString = getTableCommissionPayble();
						m.appendReplacement(sb, replaceString);
					}
					//End Added by Mohammad Rashid for UL Products - 31-Mar-2014
					// Start : added by jayesh for Smart 7 05-05-2014
					if(pattern.startsWith(CommonConstant.SMART7_TableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int ppt=master.getRequestBean().getPremiumpayingterm();
						String replaceString = getTableRiderOnPPTSmart7(1, pt, ppt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.GIP_TableRIDPEM))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int ppt=master.getRequestBean().getPremiumpayingterm();
						int incomeTerm = master.getRequestBean().getIncomeBooster();
						String replaceString = getTableRiderOnPPTSmart7(1, pt+incomeTerm, ppt);
						m.appendReplacement(sb, replaceString);
					}
					//START: Added for Maha Raksha Supreme rider premium
					if(pattern.startsWith(CommonConstant.MRS_TableRIDPEM))
					{

						int pt=master.getRequestBean().getPolicyterm();
						int ppt=master.getRequestBean().getPremiumpayingterm();
						int count = pt<30 ? pt : 30;

						String replaceString = getTableRiderOnPPTSmart7(1, count, ppt);
						m.appendReplacement(sb, replaceString);
					}
					//END
					if(pattern.startsWith(CommonConstant.RPANNUAL))
					{
						String replaceString =master.getRenewalPremium_A()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPSEMIANNUAL))
					{
						String replaceString =master.getRenewalPremium_SA()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPQUARTERLY))
					{
						String replaceString =master.getRenewalPremium_Q()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPMONTHLY))
					{
						String replaceString =master.getRenewalPremium_M()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPUYEAR))
					{
						String replaceString =reqBean.getRpu_year()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePREMPAIDDURINGYR))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTablePremiumPaidDuringYear(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePREMPAIDCUMULATIVE))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableTotalPremiumPaidCumulative(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCELIFECOV))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableCoverageAltSce(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.SCENARIOLIFECOV))//Added by Samina on 11/11/2014
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTableCoverageAlternateSce(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCEMATVAL))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableAltSceMaturityValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTALTSCEMATVAL))//Added by Samina on 13/11/2014
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTableTOTAltSceMaturityValue(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_Table4TOTALDEATHBENEFIT))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTable4TotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_Table8TOTALDEATHBENEFIT))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTable8TotalDeathBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_MATVAL4))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTableMaturityValue4(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_MATVAL8))
					{
						int count =0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTableMaturityValue8(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_PREMPAIDDURINGYR))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltScePremiumPaidDuringYear(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_PREMPAIDCUMULATIVE))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTotalPremiumPaidCumulative(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_TOTMATBEN_CURRATE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTotalMaturityBenefitCurrentRate(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_SURRBEN_CURRATE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceSurrenderBenefitCurrentRate(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_CURRATEPREMPAIDCUMULATIVE_CURRATE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTableTotalPremiumPaidCumulativeCurrentRate(1, count);
						m.appendReplacement(sb, replaceString);
					}
					//Added for Fund Performance sheet - Mohammad Rashid 10-Jun-2014
					if(pattern.startsWith(CommonConstant.TableFPRPFVEP))
					{
						String replaceString="";
						if (master.getRequestBean().getFpSelected().equals("YES")) {
							replaceString = getTableFPRegularFundValue();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFPLOYALTY))
					{
						String replaceString="";
						if (master.getRequestBean().getFpSelected().equals("YES")) {
							replaceString = getTableTotalFPLoyaltyCharges();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFPSURRVEP))
					{
						String replaceString="";
						if (master.getRequestBean().getFpSelected().equals("YES")) {
							replaceString = getTableFPSurrenderBenefit();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFPDBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getFpSelected().equals("YES")) {
							replaceString = getTableFPDeathBenefit();
						}
						m.appendReplacement(sb, replaceString);
					}
					//EndFund Performance sheet
					if(pattern.startsWith(CommonConstant.Table_ALTSCE_TAXSAVING))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTableTaxSaving(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_ANN_EFF_PREM))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTableAnnualEffectivePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_CUMMULATIVE_EFF_PREM))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTableCummulativeEffectivePremium(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_TOT4MATBEN_TAX))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSce4MaturityBenefitTax(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.EXPBONUSRATE))
					{
						String replaceString = master.getRequestBean().getExpbonusrate();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TAXSLAB))
					{
						String replaceString = String.valueOf((Double.parseDouble(master.getRequestBean().getTaxslab())*100));
						m.appendReplacement(sb, replaceString);
					}
					//End : added by jayesh for Smart 7 05-05-2014
					if(pattern.startsWith(CommonConstant.TableTopUPPrem))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableTopUpPrem();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TablePartialW))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTablePartialW();
						}
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.TableRPFVTUFVPFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableRPFVTUFVPFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTopUpLoyalty))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableTopUpLoyalty();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableROFVTUFVEOY))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableROFVTUFVEOY();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTopUpSURRENDERVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableTopUpSURRENDERVAL();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableTopUpDBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getTopUpWDSelected().equals("YES")) {
							replaceString = getTableTopUpDBEP();
						}
						m.appendReplacement(sb, replaceString);
					}
					//Added by Samina for Alternate Scenrio AAA
					if(pattern.startsWith(CommonConstant.M_AAAMORTCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getM_AAAMortalityCharges();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.O_AAATOTC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getT_AAATotalCharge();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.AAA_APPSERTAX))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getAAA_ApplicableServiceTax();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.R_AAARFVBFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getR_AAARegularFundValueBeforeFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.F_AAAFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getF_AAAFundManagementCharges();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.P_AAARFVPFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getP_AAARegularFundValuePostFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.L_AAALOYALTYCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getL_AAALoyaltyCharges();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TF_AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_AAATotalFundValue();
						}
						m.appendReplacement(sb, replaceString);
					}
					//Added by samina on 24/066/2014
					if(pattern.startsWith(CommonConstant.TF_LCEF_AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_LCEF_AAATotalFundValue();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TF_WLEF_AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_WLEF_AAATotalFundValue();
						}
						m.appendReplacement(sb, replaceString);
					}
					//Added by Samina for Alternate Scenrio AAA
					if(pattern.startsWith(CommonConstant.M_PH6AAAMORTCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getM_AAAMortalityChargesPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.O_PH6AAATOTC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getT_AAATotalChargePH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.AAA_PH6APPSERTAX))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getAAA_ApplicableServiceTaxPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.R_PH6AAARFVBFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getR_AAARegularFundValueBeforeFMCPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.F_PH6AAAFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getF_AAAFundManagementChargesPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.P_PH6AAARFVPFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getP_AAARegularFundValuePostFMCPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.L_PH6AAALOYALTYCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getL_AAALoyaltyChargesPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TF_PH6AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_AAATotalFundValuePH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TF_LCEF_PH6AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_LCEF_AAATotalFundValuePH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TF_WLEF_PH6AAATOTFUNDVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getTF_WLEF_AAATotalFundValuePH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.S_PH6AAASURRENDERVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getS_AAASurrenderValuePH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.D_PH6AAADBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getAAASelected().equals("YES")) {
							replaceString =getD_AAADeathBenifitAtEndOfPolYearPH6();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART8RPFVE))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART8RPFVE();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART8SURRENDERVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART8SURRENDERVAL();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART8DBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART8DBEP();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART4RPFVE))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART4RPFVE();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART4SURRENDERVAL))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART4SURRENDERVAL();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART4DBEP))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART4DBEP();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10MORTCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10MORTCHRG();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10TOTC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10TOTC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10APPSERTAX))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10APPSERTAX();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10RFVBFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10RFVBFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10FMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10FMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10RFVPFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10RFVPFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART10LOYALTYCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART10LOYALTYCHRG();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6MORTCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6MORTCHRG();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6TOTC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6TOTC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6APPSERTAX))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6APPSERTAX();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6RFVBFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6RFVBFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6FMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6FMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMART6RFVPFMC))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMART6RFVPFMC();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableSMARTLOYALTYCHRG))
					{
						String replaceString="";
						if (master.getRequestBean().getSmartSelected().equals("YES")) {
							replaceString =getTableSMARTLOYALTYCHRG();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.BASEPLANTOTSERVTAX))
					{
						String replaceString =master.getServiceTaxBasePlan()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTMODALPREMPAYABLE))
					{
						String replaceString = (master.getTotModalPremiumPayble())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPSTANNUAL))
					{
						String replaceString="";

						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")  ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getRenewalPremiumST_RS_A()+"";
						else
							replaceString =master.getRenewalPremiumST_A()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPSTSEMIANNUAL))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")  ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getRenewalPremiumST_RS_SA()+"";
						else
							replaceString =master.getRenewalPremiumST_SA()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPSTQUARTERLY))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2") ||  master.getRequestBean().getBaseplan().equals("MLGV2N1") ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getRenewalPremiumST_RS_Q()+"";
						else
							replaceString =master.getRenewalPremiumST_Q()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RPSTMONTHLY))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")  ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getRenewalPremiumST_RS_M()+"";
						else
							replaceString =master.getRenewalPremiumST_M()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTRPSEMIANNUAL))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")  ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getTotalRenewalPremium_RS_SA()+"";
						else
							replaceString =master.getTotalRenewalPremium_SA()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTRPQUARTERLY))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2") ||  master.getRequestBean().getBaseplan().equals("MLGV2N1") ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getTotalRenewalPremium_RS_Q()+"";
						else
							replaceString =master.getTotalRenewalPremium_Q()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTRPMONTHLY))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2") ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getTotalRenewalPremium_RS_M()+"";
						else
							replaceString =master.getTotalRenewalPremium_M()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TOTRPANNUAL))
					{
						String replaceString="";
						if(master.getRequestBean().getBaseplan().equals("MRSP1N1V2") || master.getRequestBean().getBaseplan().equals("MRSP1N2V2") || master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")  ||  master.getRequestBean().getBaseplan().equals("MLS2NA") ||  master.getRequestBean().getBaseplan().equals("MLS2NB") )
							replaceString =master.getTotalRenewalPremium_RS_A()+"";
						else
							replaceString =master.getTotalRenewalPremium_A()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table4SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getTable4SurrenderBenefit(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_VBONUS))
					{
						int count = 0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceVariableBonus(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ALTSCE_TOTDB_CURRATE))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getAltSceTotalDeathBenefitCR(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table8MATVAL) || pattern.startsWith(CommonConstant.MLG_Table8MATVAL))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGV2N1") || master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLS2NA") || master.getRequestBean().getBaseplan().equals("MLS2NB")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTotalMaturityBenefit8(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR8MATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTotalMaturityBenefit8(1, pt);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.Table4MATVAL) || pattern.startsWith(CommonConstant.MLG_Table4MATVAL))
					{
						int count=0;
						int pt=master.getRequestBean().getPolicyterm();
						count = pt<30 ? pt : 30;
						if(master.getRequestBean().getBaseplan().equals("MLGPV1N1")  || master.getRequestBean().getBaseplan().equals("MLGV2N1")){
							count= pt<85 ? pt : 85;
						}
						String replaceString = getTotalMaturityBenefit4(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableFR4MATVAL))
					{
						int pt=master.getRequestBean().getPolicyterm();
						String replaceString = getTotalMaturityBenefit4(1, pt);
						m.appendReplacement(sb, replaceString);
					}

					if(pattern.startsWith(CommonConstant.SMART7_8SURRBEN))
					{
						int pt=master.getRequestBean().getPolicyterm();
						int count = pt<30 ? pt : 30;
						String replaceString = getSurrenderBenefit8(1, count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLANNUALPREM))
					{
						String replaceString =master.getAnnualPremium()+"";
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							if(master.getAnnualPremium().equals("NA"))
								replaceString=master.getAnnualPremium()+"";
							else
								replaceString=df.format(Long.parseLong(master.getAnnualPremium()))+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLSEMIPREM))
					{
						String baseplan=master.getRequestBean().getBaseplan();
						String replaceString=null;
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							replaceString =master.getAnnualPremium()+"";
						else
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							replaceString =df.format(Math.round(Long.parseLong(master.getAnnualPremium()) * .5))+"";
						else
							replaceString =Math.round(Long.parseLong(master.getAnnualPremium()) * .5)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INITDFF))
					{
						String replaceString =Math.round(master.getInitDebtFundFactor())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.INITEFF))
					{
						String replaceString =Math.round(master.getInitEquityFundFactor())+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.HEADERTOPUP))
					{
						String replaceString =master.getAltHeader()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.SMDB1))
					{
						String replaceString = "0";
						if (master.getRequestBean().getSmartDebtFund().equals("WLI")) {
							replaceString = "100";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.SMDB2))
					{
						String replaceString = "0";
						if (master.getRequestBean().getSmartDebtFund().equals("WLF")) {
							replaceString = "100";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ILLUSPAGE))
					{
						String replaceString = master.getRequestBean().getIllusPageNo()+"";
						m.appendReplacement(sb, replaceString);
					}
                    // added by Ganesh for WPP rider 03-03-2016
					if(pattern.startsWith(CommonConstant.WPPNOTE))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						for(int i=0;i<alist.size();i++){
							RiderBO oRiderBO=(RiderBO)alist.get(i);
							if(oRiderBO.getCode().equals("WPPN1V1"))
							{
								replaceString = getWPPNote();
							}else
                            {
                                replaceString = "";
                            }

						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RIDERTEXT))
					{
						String replaceString="";
						ArrayList alist=master.getRiderData();
						for(int i=0;i<alist.size();i++){
							RiderBO oRiderBO=(RiderBO)alist.get(i);
							if(oRiderBO.getCode().equals("WOPULV1") || (oRiderBO.getCode().equals("WOPPULV1"))
									|| (oRiderBO.getCode().equals("WPPN1V1")))
							{
								replaceString =CommonConstant.RIDERText ;
							}

						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.COMISSIONTEXT))
					{
						String replaceString="";
						//if( master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y")) {
						replaceString = master.getCommissionText() ;
						//}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ACTUALFP))
					{
						String replaceString =master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals("")?master.getRequestBean().getFundPerform()+"":"8";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.NETYIELD8P))
					{
						String replaceString =master.getNetYield8P()+"%";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.NETYIELD8TW))
					{
						String replaceString =master.getNetYield8TW()+"%";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.NETYIELD8SMART))
					{
						String replaceString =master.getNetYield8SMART()+"%";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.NEGERROR))
					{
						String replaceString =master.getRequestBean().getNegativeError()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableANNUITYRATE))//Added by Samine for Easy retire
					{
						int count=37;
						String replaceString = getTableANNUITYRate(1,count);
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.TableAMT_ANNUITY))//Added by Samine for Easy retire
					{
						String replaceString = master.getRequestBean().getAnnuity_amt()+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLQUARTERPREM))
					{
						String baseplan=master.getRequestBean().getBaseplan();
						String replaceString=null;
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							replaceString =master.getAnnualPremium()+"";
						else
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							replaceString=df.format(Math.round(Long.parseLong(master.getAnnualPremium())*0.25))+"";
						else
							replaceString =Math.round(Long.parseLong(master.getAnnualPremium())*0.25)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PNLMONTHLYRPREM))
					{
						String baseplan=master.getRequestBean().getBaseplan();
						String replaceString=null;
						if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan))
							replaceString =master.getAnnualPremium()+"";
						else
						if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
							replaceString=df.format(Math.round(Long.parseLong(master.getAnnualPremium())*0.0833))+"";
						else
							replaceString =Math.round(Long.parseLong(master.getAnnualPremium())*0.0833)+"";
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLDYNTERM1))
					{
						String replaceString="";
						int term = 0;
						ArrayList alist=master.getRiderData();
						if(alist.size()>0){
							RiderBO oRiderBO=(RiderBO)alist.get(0);
							if(oRiderBO.getRtlID() == 14){
								if( reqBean.getPremiumpayingterm() > (65 - reqBean.getInsured_age())){
									term = 65 - reqBean.getInsured_age();
								}else{
									term = reqBean.getPremiumpayingterm();
								}
							}
							else if(oRiderBO.getRtlID() == 15){
								if( reqBean.getPremiumpayingterm() > (65 - reqBean.getProposer_age())){
									term = 65 - reqBean.getProposer_age();
								}else{
									term = reqBean.getPremiumpayingterm();
								}
							}
							else{
								term = reqBean.getPremiumpayingterm();
							}
							replaceString =term+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.RDLDYNTERM2))
					{
						String replaceString="";
						int term = 0;
						ArrayList alist=master.getRiderData();
						if(alist.size()>1){
							RiderBO oRiderBO=(RiderBO)alist.get(1);
							if(oRiderBO.getRtlID() == 14){
								if( reqBean.getPremiumpayingterm() > (65 - reqBean.getInsured_age())){
									term = 65 - reqBean.getInsured_age();
								}else{
									term = reqBean.getPremiumpayingterm();
								}
							}
							else if(oRiderBO.getRtlID() == 15){
								if( reqBean.getPremiumpayingterm() > (65 - reqBean.getProposer_age())){
									term = 65 - reqBean.getProposer_age();
								}else{
									term = reqBean.getPremiumpayingterm();
								}
							}
							else{
								term = reqBean.getPremiumpayingterm();
							}
							replaceString =term+"";
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.ADDLRID))
					{
						String replaceString =getRiderDisclaimer();
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.AGENTNAME)){
						/*String replaceString="";
						replaceString = reqBean.getAgentName();
						m.appendReplacement(sb, replaceString);*/
					}
					if(pattern.startsWith(CommonConstant.AGENTNUMBER)){
						/*String replaceString="";
						replaceString = reqBean.getAgentNumber();
						m.appendReplacement(sb, replaceString);*/
					}
					if(pattern.startsWith(CommonConstant.AGENTCONTACTNUMBER)){
						/*String replaceString="";
						replaceString = reqBean.getAgentContactNumber();
						m.appendReplacement(sb, replaceString);*/
					}
					if(pattern.startsWith(CommonConstant.DATE)){
						String replaceString="";
						DateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
						replaceString = formater.format(new Date());
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.PROPOSERNAME)){
						String replaceString="";
						if(master.getRequestBean().getProposer_name() != null){
							replaceString = master.getRequestBean().getProposer_name();
						}
						m.appendReplacement(sb, replaceString);
					}
					if(pattern.startsWith(CommonConstant.IMAGE) || pattern.startsWith(CommonConstant.WITHOUT_SALES) /*|| pattern.startsWith(CommonConstant.IMGNAME)*/){
						String replaceString="";
						String typeOfImg = ".jpg";
						if(pattern.startsWith(CommonConstant.IMAGE))
							typeOfImg = ".png";
						InputStream imagePath = AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/Header_" + master.getPlanDescription() + typeOfImg);
						byte[] imgBuffer =new byte[imagePath.available()];
						String imgString=new String(imgBuffer);
						Bitmap bm = BitmapFactory.decodeStream(imagePath);
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						bm.compress(Bitmap.CompressFormat.PNG, 100 , baos);
						byte[] b = baos.toByteArray();
						replaceString = Base64.encodeToString(b, Base64.DEFAULT);
						m.appendReplacement(sb, "\"data:image/png;base64," + replaceString + "\"");
					}

					//End : added by jayesh for Smart 7 05-05-2014
					result = m.find();
				}
				m.appendTail(sb);
				noOfBytesRead = templateFile.read(inputBuffer);
				inputString = new String(inputBuffer);
			}
			//String altYesNo = "No";
			if ((master.getRequestBean().getTopUpWDSelected() != null && master.getRequestBean().getTopUpWDSelected().equals("YES")) || (master.getRequestBean().getFpSelected() != null && master.getRequestBean().getFpSelected().equals("YES"))
					|| (master.getRequestBean().getSmartSelected() != null && master.getRequestBean().getSmartSelected().equals("YES"))) {
				altYesNo = "P";
			}
			if ((master.getRequestBean().getFpSelected() != null && master.getRequestBean().getFpSelected().equals("YES")) ||
					(master.getRequestBean().getExpbonusSelected() != null && master.getRequestBean().getExpbonusSelected().equals("YES")))
				altYesNo = "Y";
			//PDFGenerator oPDFgen = new PDFGenerator();
			//pdfdata = oPDFgen.generatePDF(sb,outputFilePath,outputTempFile,altYesNo);



			filedata = sb.toString();

			//File myFile = new File(outputFilePath);
			//Desktop.getDesktop().open(myFile);

			templateFile.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return filedata;
	}
    public String getWPPNote(){
        StringBuffer sb=new StringBuffer();

        sb.append("<tr><td colspan=\"14\" align=\"left\"><div class=\"boxnote\"><em>**The benefit equals all future premiums payable under the base policy from the date of claim for a period as specified in the policy contract. Please refer the policy contract for more details.</em></div></td></tr>");

        return sb.toString();
    }
	public String getRiderDisclaimer(){

		ArrayList aList=master.getRiderData();
		AddressBO oAddressBO=master.getOAddressBO();
		String versionString = "";
		//String imagePath = "";
		if ((master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y")) || master.getRequestBean().getBaseplan().contains("IONEV1N") || master.getRequestBean().getBaseplan().contains("FGV1N1"))
			if (master.getRequestBean().getAgentCITI()!=null && master.getRequestBean().getAgentCITI().equals("Y"))
				versionString = "CITI_";
			else
				versionString="";
		versionString += oAddressBO.getSIS_VERSION();
		//imagePath = master.getTempImagePath();
		InputStream imagePath = null;
		byte[] imgBuffer = null;
		StringBuilder stringBuilder = new StringBuilder();
		String disclaimString = "";

		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			String inputFile="";
			try {
				/*if(master.getRequestBean().getBaseplan().contains("IONEV1N"))
					inputFile = oRiderBO.getCode() + "_IO.html";
				else if(master.getRequestBean().getBaseplan().contains("FGV1N1"))
					inputFile = oRiderBO.getCode() + "_FG.html";
				else*/
					inputFile = oRiderBO.getCode() + ".html";
				BufferedReader reader = new BufferedReader(/* new FileReader(inputFile)*/new InputStreamReader(AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/" + inputFile),"UTF-8"));

				String line = null;
				String ls = System.getProperty("line.separator");
				while((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		aList=null;
		disclaimString = stringBuilder.toString();
		StringBuffer sb = new StringBuffer(disclaimString);
		disclaimString = disclaimString.replace("||VERSION||", versionString);

		try{
			imagePath = AppConstants.SA_APP_CONTEXT.getAssets().open("www/templates/SIS/Header_" + master.getPlanDescription() + ".jpg");
			imgBuffer =new byte[imagePath.available()];
		}catch(IOException e){
			e.printStackTrace();
		}
		String imgString=new String(imgBuffer);
		Bitmap bm = BitmapFactory.decodeStream(imagePath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100 , baos);
		byte[] b = baos.toByteArray();
		String replaceString = Base64.encodeToString(b, Base64.DEFAULT);
		disclaimString = disclaimString.replace("||IMGNAME||", "\"data:image/jpg;base64," + replaceString + "\"");


		return disclaimString;
	}
	public String getTablePA(int startPt, int endPt){
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(i);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	public String getTableERPA(int startPt, int endPt){//Added for Easy Retire
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i==endPt)
					sb.append("Till survival");
				else
					sb.append(i);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableAge(int startPt, int endPt, int age){
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(age);
				sb.append("</td></tr>");
				age++;
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableAgeProp(int startPt, int endPt, int age){
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(age==0)
					sb.append("");
				else
					sb.append(age);
				sb.append("</td></tr>");
				if(age>0)
					age++;
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTablePremium(int startPt, int endPt){
		String mode=master.getRequestBean().getFrequency();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long prem=0;
		if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U")) {
			if (!(master.getAnnualPremium().equals("NA"))) {
				prem = Long.parseLong(master.getAnnualPremium());
			}
			if(master.getRequestBean().getFrequency().equals("O")) {
				prem = master.getBaseAnnualizedPremium();
			}
		} else
			prem=master.getQuotedAnnualizedPremium();

		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(("O".equals(mode) && i>1) || (i>ppt) )
				{
					sb.append("0");
				}else{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						if ("U".equals(planType))
							sb.append(df.format(prem));
						else
							sb.append(prem);
					}
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	/*public String getTableTotalPremium(int startPt, int endPt){
		HashMap hmTotalPremAnnual = master.getTotalPremiumAnnual();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalPremAnnual.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremAnnual=null;
		return sb.toString();

	}*/
	public String getTableTotalPremium(int startPt, int endPt){
		HashMap hmTotalPremAnnual = master.getTotalPremiumAnnual();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (hmTotalPremAnnual.get(i)!=null) {
					if (master.isCombo())
						sb.append(df.format(hmTotalPremAnnual.get(i)));
					else
						sb.append(hmTotalPremAnnual.get(i));
				} else
					sb.append("0");
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremAnnual=null;
		return sb.toString();

	}
	public String getTableTotalPremium_MIP(int startPt, int endPt){
		HashMap hmTotalPremAnnual = master.getTotalPremiumAnnual();
		long riderPremTotal = 0;
		int pt = master.getRequestBean().getPolicyterm();
		String mode = master.getRequestBean().getFrequency();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		String planType = DataAccessClass.getPlanType(master.getRequestBean()
				.getBaseplan());
		ArrayList aList = master.getRiderData();

		for (int i = 1; i <= pt; i++) {
			for (int j = 0; j < aList.size(); j++) {
				RiderBO oRiderBO = (RiderBO) aList.get(j);
				riderPremTotal = riderPremTotal + master.getTotAnnRiderModalPremium();
			}
			if (("O".equals(mode) && i > 1) || (i > ppt)) {
				hmTotalPremAnnual.put(i, 0);
			} else {
				if (("C".equals(planType))
						&& ("Y".equals(master.getRequestBean()
						.getTata_employee())) && (i == 1)
					//&& !"O".equals(mode)) {
						) {
					// discounted premium
					hmTotalPremAnnual.put(i, master.getDiscountedBasePrimium()
							+ riderPremTotal);
				} else {
					hmTotalPremAnnual.put(i, master.getQuotedAnnualizedPremium()
							+ riderPremTotal);
				}
			}
			riderPremTotal=0;	//added by jayesh
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (hmTotalPremAnnual.get(i)!=null) {
					if (master.isCombo())
						sb.append(df.format(hmTotalPremAnnual.get(i)));
					else
						sb.append(hmTotalPremAnnual.get(i));
				} else
					sb.append("0");
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremAnnual=null;
		return sb.toString();

	}

	public String getTableCoverage(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverage();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}
	public String getTableCoverageGIP(int startPt, int endPt, int incomeTerm){
		HashMap lifeCoverageMap = master.getLifeCoverage();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt+incomeTerm;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i>endPt)
					sb.append("0");
				else
					sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}
    public String getTableCoverage4_Gk(int startPt, int endPt){
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(master.getRequestBean().getSumassured());
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }
	public String getTableSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	public String getTableSurrenderBenefitGIP(int startPt, int endPt, int incomeTerm){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=startPt;i<=endPt+incomeTerm;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i>endPt)
				sb.append("0");
			else
				sb.append(surrValue.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	public String getTableFRSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i<endPt)
					sb.append(surrValue.get(i));
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableALTSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getALTSCESurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableMaturityValue(int startPt, int endPt) {
        HashMap matValue=master.getMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}
	public String getTableMaturityValueGIP(int startPt, int endPt, int incomeTerm){
		HashMap matValue=master.getMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt+incomeTerm;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i)!=null?matValue.get(i):0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}
	public String getTableBasicSumMIP(int startPt, int endPt){
        HashMap matValue=master.getBasicSumAssured();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i==endPt)
					sb.append(matValue.get(i)!=null?matValue.get(i):0);
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}
	public String getTableMilestone8(int startPt, int endPt){
        HashMap matValue=master.getMilestoneAdd8Dict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
	}
    public String getTableMilestone4(int startPt, int endPt){
        HashMap matValue=master.getMilestoneAdd8Dict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
    }
    public String getTableMBB(int startPt, int endPt){
        HashMap matValue=master.getMoneyBackBenefitDict();
        StringBuffer sb=new StringBuffer();
        sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
        if(startPt <= endPt){
            for(int i=startPt;i<=endPt;i++){
                sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
                sb.append(matValue.get(i)!=null?matValue.get(i):0);
                sb.append("</td></tr>");
            }
        }else{
            sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
            sb.append("</td></tr>");
        }
        sb.append("</table>");
        matValue=null;
        return sb.toString();
    }
	public String getTableMaturityValueALTSCE(int startPt, int endPt){
		HashMap matValue=master.getMaturityValALTSCE();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableTotalMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getTotalMaturityValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableRider(int startPt, int endPt){
		long totalRiderPrem=master.getTotalRiderModalPremium();
        if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)) {
            totalRiderPrem = totalRiderPrem * 2;
        } else if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.QUATERLY_MODE)) {
            totalRiderPrem = totalRiderPrem * 4;
        } else if (master.getRequestBean().getFrequency()
                .equalsIgnoreCase(CommonConstant.MONTHLY_MODE)) {
            totalRiderPrem = totalRiderPrem * 12;
        }
		int ppt=master.getRequestBean().getPremiumpayingterm();
		ArrayList alist=master.getRiderData();
		if(alist.size()>0){
			RiderBO oRiderBO=(RiderBO)alist.get(0);
			ppt = oRiderBO.getPremPayTerm();
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i<=ppt)
					sb.append(totalRiderPrem);
				else
					sb.append("0");
				sb.append("</td></tr>");

			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	public String getTableRider_MIP(int startPt, int endPt){
		long totalRiderPrem=master.getTotAnnRiderModalPremium();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		StringBuffer sb= new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(i<=ppt)
					sb.append(totalRiderPrem);
				else
					sb.append("0");
				sb.append("</td></tr>");

			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}
	public String getUniquePDFFileName(String productName){
		String fileName = "";
		Date date=new Date();
		long currentTimeMillS=date.getTime();

		FileCounter ofileCounter=new FileCounter();
		//ofileCounter.countIncrementer();

		fileName=Long.toString(currentTimeMillS);
		fileName = productName + fileName+"_"+ofileCounter.countIncrementer()+ ".pdf";

		return fileName;
	}

	public String getUniqueJSPFileName(String productName){
		String fileName = "";
		Date date=new Date();
		long currentTimeMillS=date.getTime();

		FileCounter ofileCounter=new FileCounter();
		//ofileCounter.countIncrementer();

		fileName=Long.toString(currentTimeMillS);
		fileName = productName + fileName+"_"+ofileCounter.countIncrementer()+ ".html";

		return fileName;
	}

	public String getAddress(){
		AddressBO oAddressBO=master.getOAddressBO();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		sb.append("<tr><td align=\"center\">");
		sb.append(oAddressBO.getCO_NAME());
		sb.append("</td></tr>");

		sb.append("<tr><td align=\"center\" >");
		sb.append(oAddressBO.getCO_ADDRESS());
		sb.append("</td></tr>");

		sb.append("<tr><td align=\"center\" >");
		sb.append(oAddressBO.getCO_HELP_LINE());
		sb.append("</td></tr>");

		sb.append("<tr><td>");
        sb.append(oAddressBO.getFILLER1());
		sb.append("</td></tr>");
		sb.append("</table>");
		oAddressBO=null;
		return sb.toString();
	}

	public String getRiderNames(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(getFormattedString(oRiderBO.getDescription(), "&", "&amp;"));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderUIN(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			if (oRiderBO.getRdlUIN()!=null) {
				sb.append(oRiderBO.getRdlUIN());
			} else {
				sb.append("");
			}
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}
	public String getRiderSA(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			if (oRiderBO.getSumAssured()==0) {
				sb.append("**");
			} else {
				sb.append(oRiderBO.getSumAssured());
			}
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderModalPremium(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getModalpremium());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderPolTerm(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPolTerm());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getRiderPremPayTerm(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPremPayTerm());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getFormattedString(String strString,String strToReplace, String strWithReplace){
		String strReplaced="";
		strReplaced=strString.replace(strToReplace, strWithReplace);
		return strReplaced;
	}

	public String getRiderBasePrem(){
		ArrayList aList=master.getRiderData();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=0;i<aList.size();i++){
			RiderBO oRiderBO=(RiderBO)aList.get(i);
			//sb.append("<br></br>");
			sb.append("<tr style=\"height:40px\"><td>");
			sb.append(oRiderBO.getPrimium());
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		aList=null;
		return sb.toString();
	}

	public String getTablePremiumAllocationCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAllocCharg=master.getPremiumAllocationCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAllocCharg.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAllocCharg=null;
		return sb.toString();
	}

	public String getTableAmountForInvestment(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAmtInv=master.getAmountForInvestment();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAmtInv.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAmtInv = null;
        return sb.toString();
	}

	public String getTableOtherBenifitInbuiltCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmOBIC=master.getOtherBenefitCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmOBIC.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmOBIC=null;
        return sb.toString();
	}

	public String getTablePolicyAdminCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAdmChrg=master.getPolicyAdminCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAdmChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmAdmChrg = null;
        return sb.toString();
	}

	public String getTableMortalityCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortChrg=master.getMortalityCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmMortChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortChrg=null;
		return sb.toString();
	}

	public String getTableMortalityChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortChrg=master.getMortalityChargesPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmMortChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortChrg=null;
		return sb.toString();
	}

	public String getTableLoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmLoyaltyChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTablePH6LoyaltyCharges(){//Added for Loyalty bonus PH6
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmLoyaltyChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTableGuaranteeCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableOtherCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmOtherChrg=master.getOtherBenefitCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmOtherChrg.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");

		return sb.toString();
	}

	public String getTableApplicableServiceTax(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppTax=master.getApplicableServiceTaxPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAppTax.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppTax=null;
		return sb.toString();
	}

	public String getTableTotalCharge(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotC=master.getTotalchargeforPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmTotC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotC=null;
		return sb.toString();
	}

	public String getTablePH6TotalCharge(){//Added for PH6 total charge
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotC=master.getTotalchargeforPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmTotC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotC=null;
		return sb.toString();
	}

	public String getTableRegularFundValueBeforeFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValuePostFMC(){// Added by Samina for Regular value ost FMC
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValuePostFMCPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValuePostFMC6(){// Added by Samina for Regular value post FMC for PH6
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValuePostFMCPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableFundManagementCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFMC=master.getFundManagmentChargePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFMC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFMC = null;
        return sb.toString();
	}

	public String getTableRegularFundValAtEndPolicyYearBeforeGMA(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
        return sb.toString();
	}


	public String getTableGuaranteedMaturityAddition(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmGMA=master.getGuaranteedMaturityAdditionPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmGMA.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmGMA=null;
		return sb.toString();
	}


	public String getTableTotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalFundValueAtEndPolicyYearPH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmFundVal = null;
        return sb.toString();
	}

	public String getTableSurrenderValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValuePH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmSurrVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableDeathBenifitAtEndOfPolicyYear(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYear();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmDB.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}

	public String getTableDeathBenifitAtEndOfPolicyYearPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYearPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmDB.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}

	public String getTableApplicableServiceTax_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppTax=master.getApplicableServiceTaxPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmAppTax.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppTax=null;
		return sb.toString();
	}

	public String getTableFundManagementCharges_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFMC=master.getFundManagmentChargePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFMC.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFMC=null;
		return sb.toString();
	}

	public String getTableRegularFundValAtEndPolicyYearBeforeGMA_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
        return sb.toString();
	}

	public String getTableGuaranteedMaturityAddition_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmGMA=master.getGuaranteedMaturityAdditionPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmGMA.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmGMA=null;
		return sb.toString();
	}

	public String getTableTotalFundValue_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalFundValueAtEndPolicyYearPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmFundVal = null;
        return sb.toString();
	}

	public String getTableSurrenderValue_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValuePH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmSurrVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableRegularFundValueBeforeFMC_PH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmRegFundVal=master.getRegularFundValueBeforeFMCPH6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmRegFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmRegFundVal=null;
		return sb.toString();
	}

	public String getTableCommissionPayble(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmCommPaybleVal=master.getCommissionPayble();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmCommPaybleVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
        hmCommPaybleVal = null;
		return sb.toString();
	}

	public String getTableGAI(int startPt, int endPt){
		HashMap GAIMap = master.getGuranteedAnnualIncome();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableGAIMBP(int startPt, int endPt){
		HashMap GAIMap = master.getGuranteedAnnualIncomeAll();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getAccTableGAI(int startPt, int endPt){
		HashMap GAIMap = master.getAccGuranteedAnnualIncome();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}


	public String getTableRPGAI(int startPt, int endPt){
		HashMap GAIMap = master.getRPGAI();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableALTSCEGAI(int startPt, int endPt){
		HashMap GAIMap = master.getALTSCEGAI();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(GAIMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		GAIMap=null;
		return sb.toString();
	}

	public String getTableGuranteedSuurValue(int startPt, int endPt){
		HashMap gurSurrValMap = master.getGuranteedSurrenderValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(gurSurrValMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		gurSurrValMap=null;
		return sb.toString();
	}

	public String getTableGuranteedSuurValueGIP(int startPt, int endPt, int incomeTerm){
		HashMap gurSurrValMap = master.getGuranteedSurrenderValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt+incomeTerm;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i>endPt)
					sb.append("0");
				else
					sb.append(gurSurrValMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		gurSurrValMap=null;
		return sb.toString();
	}

	public String getTableLTGuranteedSuurValue(int startPt, int endPt){
		HashMap gurSurrValMap = master.getALTguranteedSurrenderValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(gurSurrValMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		gurSurrValMap=null;
		return sb.toString();
	}


	public String getTable4VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVSRBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableAlt4VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getAltVSRBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableEBDURING(int startPt, int endPt){
		HashMap hmEBDURING=master.getAltEBDURING();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmEBDURING.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmEBDURING=null;
		return sb.toString();
	}
	public String getTableTotGuarBenefit(int startPt, int endPt){
		HashMap totGuarBenefit=master.getTotalGuaranteedBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(totGuarBenefit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		totGuarBenefit=null;
		return sb.toString();
	}
	public String getTable4TerminalBonus(int startPt, int endPt){
		HashMap hmTerminalBonus=master.getTerminalBonus4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTerminalBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTerminalBonus=null;
		return sb.toString();
	}

	public String getTable4TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	/** Vikas - Added for Mahalife Gold*/
	public String getTableTotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}
	/** Vikas - Added for Mahalife Gold*/
	// for 8% money maxima

	public String getTable8VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVSRBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTableAlt8VSRBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getAltVSRBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}

	public String getTable8TerminalBonus(int startPt, int endPt){
		HashMap hmTerminalBonus=master.getTerminalBonus8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTerminalBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTerminalBonus=null;
		return sb.toString();
	}

	public String getTable8TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getTotalDeathBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getTable8SurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit_8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTable8FRSurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteedSurrBenefit_8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i<endPt)
					sb.append(surrValue.get(i));
				else
					sb.append(0);
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	public static String getFormattedDate(String strDate ){
		try {
			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"MM/dd/yyyy");
			Date date = sdfSource.parse(strDate);
			SimpleDateFormat sdfDestination = new SimpleDateFormat(
					"dd-MMM-yyyy");
			strDate = sdfDestination.format(date);

		} catch (Exception pe) {
		}

		return strDate;
	}
    public static String getFormattedDateRaksha(String strDate ){
        try {
			//formater 1 changed for ibl by ganesh on 05-07-2016
            SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = sdfDate1.parse(strDate);
            SimpleDateFormat sdfDate2 = new SimpleDateFormat("dd-MMM-yyyy");
            strDate = sdfDate2.format(date);
        } catch (Exception pe) {

        }

        return strDate;
    }

//For Gold and Gold Plus Mohammad Rashid

	public String getTableIPC(int startPt, int endPt){
		HashMap IPCMap = master.getGuranteedAnnualInflationCover();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(IPCMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		IPCMap=null;
		return sb.toString();
	}
//For Gold and Gold Plus Mohammad Rashid

	public String getTableRiderOnPPT(int startPt, int endPt, int showPt){
		long totalRiderPrem = Math.round(master.getTotalRiderModalPremium());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				if(showPt>=i){
					sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
					sb.append(totalRiderPrem);
					sb.append("</td></tr>");
				} else {
					sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
					sb.append("0");
					sb.append("</td></tr>");
				}
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getTablePremiumPaidDuringYear(int startPt, int endPt){
		int rpuy = Integer.parseInt(master.getRequestBean().getRpu_year());
		long prem=master.getQuotedAnnualizedPremium();
		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(rpuy>i)
				{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						sb.append(prem);
					}
				}else{
					sb.append("0");
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getTableTotalPremiumPaidCumulative(int startPt, int endPt){
		long premPaidcumulativeTotal=0;
		int pt = master.getRequestBean().getPolicyterm();
		int rpuy = Integer.parseInt(master.getRequestBean().getRpu_year());
		long qap = master.getQuotedAnnualizedPremium();
		HashMap hmPremPaidCumulative = new HashMap();
		for (int i = 1; i <= pt; i++) {
			if(rpuy>i){

				premPaidcumulativeTotal=premPaidcumulativeTotal+qap;
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
			else{
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmPremPaidCumulative=null;
		return sb.toString();
	}

	public String getTableCoverageAltSce(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverageAltSce();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}

	public String getTableCoverageAlternateSce(int startPt, int endPt){
		HashMap lifeCoverageMap = master.getLifeCoverageAlternateSce();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(lifeCoverageMap.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		lifeCoverageMap=null;
		return sb.toString();
	}

	public String getTableAltSceMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getTableTOTAltSceMaturityValue(int startPt, int endPt){
		HashMap matValue=master.getTOTaltScenarioMaturityVal();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltSceTable4TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getAltSceTable8TotalDeathBenefit(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getAltSceTableMaturityValue4(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltSceTableMaturityValue8(int startPt, int endPt){
		HashMap matValue=master.getAltScenarioMaturityVal8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(matValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		matValue=null;
		return sb.toString();
	}

	public String getAltScePremiumPaidDuringYear(int startPt, int endPt){
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long prem=master.getQuotedAnnualizedPremium();
		String planType = DataAccessClass.getPlanType(master.getRequestBean().getBaseplan());
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if(ppt>=i)
				{
					if(("C".equals(planType)) && ("Y".equals(master.getRequestBean().getTata_employee())) && (i == 1)){
						sb.append(master.getDiscountedBasePrimium());
					}else{
						sb.append(prem);
					}
				}else{
					sb.append("0");
				}
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public String getAltSceTotalPremiumPaidCumulative(int startPt, int endPt){
		long premPaidcumulativeTotal=0;
		int pt = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		long qap = master.getQuotedAnnualizedPremium();
		HashMap hmPremPaidCumulative = new HashMap();
		for (int i = 1; i <= pt; i++) {
			if(ppt>=i){
				premPaidcumulativeTotal=premPaidcumulativeTotal+qap;
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
			else{
				hmPremPaidCumulative.put(i, premPaidcumulativeTotal);
			}
		}
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmPremPaidCumulative=null;
		return sb.toString();
	}

	public String getAltSceTotalMaturityBenefitCurrentRate(int startPt, int endPt){
		HashMap surrValue=master.getAltScenarioTotMatBenCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getAltSceSurrenderBenefitCurrentRate(int startPt, int endPt){
		HashMap surrValue=master.getAltScenarioSurrBenefitCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTableTotalPremiumPaidCumulativeCurrentRate(int startPt, int endPt){
		HashMap hmTotalPremPaidCumulative = master.getAltScenarioPremPaidCumulativeCurrentRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalPremPaidCumulative.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalPremPaidCumulative=null;
		return sb.toString();
	}

	public double getTaxMMXText(int endPt) {
		HashMap hmTotalPremPaidCumulative = master.getAltScenarioPremPaidCumulativeCurrentRate();
		return Double.parseDouble(hmTotalPremPaidCumulative.get(endPt).toString());
	}

	public double getTaxALTText(int endPt) {
		HashMap surrValue=master.getTotalMaturityBenefit8();
		return Double.parseDouble(surrValue.get(endPt).toString());
	}

	public double getTax23Text(int endPt) {
		HashMap surrValue=master.getAltScenarioTotMatBenCurrentRate();
		return Double.parseDouble(surrValue.get(endPt).toString());
	}

	public String getTableFPSurrenderBenefit(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurrVal=master.getSurrenderValueFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurrVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurrVal=null;
		return sb.toString();
	}

	public String getTableFPRegularFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableTotalFPLoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyChrg=master.getLoyaltyChargeforFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyChrg.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyChrg=null;
		return sb.toString();
	}

	public String getTableFPDeathBenefit(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDB=master.getDeathBenifitAtEndOfPolicyYearFP();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDB.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDB=null;
		return sb.toString();
	}
	//added on 06-06-2014

	public String getAltSceTableTaxSaving(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenarioTaxSaving();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSceTableAnnualEffectivePremium(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenarioannualizedeffectivepremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSceTableCummulativeEffectivePremium(int startPt, int endPt){
		HashMap hmTaxSaving = master.getAltScenariocummulativeeffectivepremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTaxSaving.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTaxSaving=null;
		return sb.toString();
	}

	public String getAltSce4MaturityBenefitTax(int startPt, int endPt){
		HashMap surrValue=master.getAltScenario4MaturityBenefitTax();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//End : added by jayesh for SMART7 : 07-05-2014

	public String getTableTopUpPrem(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap topUpPremium=master.getTopUpPremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(topUpPremium.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		topUpPremium=null;
		return sb.toString();
	}

	public String getTablePartialW() {
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getWithdrawalAmout();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableRPFVTUFVPFMC() {
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getRegularFundValuePostFMC();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpLoyalty(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpLoyalty();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableROFVTUFVEOY(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpRegularFundValueEndOfYear();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpSURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpSurrValue();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	public String getTableTopUpDBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap withDAmt=master.getTopUpDeathBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(withDAmt.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		withDAmt=null;
		return sb.toString();
	}

	//Added by Samina for AAA3
	public String getS_AAASurrenderValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurVal=master.getSurrenderValuePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurVal=null;
		return sb.toString();
	}

	public String getD_AAADeathBenifitAtEndOfPolYear(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDeathVal=master.getTotalDeathBenefitPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDeathVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDeathVal=null;
		return sb.toString();
	}

	public String getM_AAAMortalityCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortVal=master.getMortalityCharges_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmMortVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortVal=null;
		return sb.toString();
	}

	public String getT_AAATotalCharge(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotVal=master.getTotalchargeforPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmTotVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotVal=null;
		return sb.toString();
	}

	public String getAAA_ApplicableServiceTax(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppSerVal=master.getApplicableServiceTaxPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAppSerVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppSerVal=null;
		return sb.toString();
	}

	public String getR_AAARegularFundValueBeforeFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getF_AAAFundManagementCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getP_AAARegularFundValuePostFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getL_AAALoyaltyCharges(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyVal=master.getLoyaltyChargeforPH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyVal=null;
		return sb.toString();
	}

	public String getTF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_LCEF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueAAA_LCEF_PH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_WLEF_AAATotalFundValue(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueAAA_WLEF_PH10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}
	//Added by Samina for AAA3

	public String getS_AAASurrenderValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmSurVal=master.getSurrenderValuePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmSurVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmSurVal=null;
		return sb.toString();
	}

	public String getD_AAADeathBenifitAtEndOfPolYearPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmDeathVal=master.getTotalDeathBenefitPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmDeathVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmDeathVal=null;
		return sb.toString();
	}

	public String getM_AAAMortalityChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmMortVal=master.getMortalityChargesPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmMortVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmMortVal=null;
		return sb.toString();
	}

	public String getT_AAATotalChargePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmTotVal=master.getTotalchargeforPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmTotVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotVal=null;
		return sb.toString();
	}

	public String getAAA_ApplicableServiceTaxPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAppSerVal=master.getApplicableServiceTaxPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmAppSerVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAppSerVal=null;
		return sb.toString();
	}

	public String getR_AAARegularFundValueBeforeFMCPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getF_AAAFundManagementChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getP_AAARegularFundValuePostFMCPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getL_AAALoyaltyChargesPH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmLoyaltyVal=master.getLoyaltyChargeforPH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmLoyaltyVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmLoyaltyVal=null;
		return sb.toString();
	}

	public String getTF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_LCEF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_LCEF_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTF_WLEF_AAATotalFundValuePH6(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH6_WLEF_AS3();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8RPFVE(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8SURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getSurrenderValuePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART8DBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalDeathBenefitPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4RPFVE(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4SURRENDERVAL(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getSurrenderValuePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART4DBEP(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalDeathBenefitPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(df.format(hmFundVal.get(i)));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10MORTCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getMortalityCharges_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10TOTC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalchargeforPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10APPSERTAX(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getApplicableServiceTaxPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10RFVBFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10FMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10RFVPFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART10LOYALTYCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getLoyaltyChargeforPH10_SMART10();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6MORTCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getMortalityCharges_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6TOTC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getTotalchargeforPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6APPSERTAX(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getApplicableServiceTaxPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6RFVBFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValueBeforeFMCPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6FMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getFundManagmentChargePH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMART6RFVPFMC(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getRegularFundValuePostFMCPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTableSMARTLOYALTYCHRG(){
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmFundVal=master.getLoyaltyChargeforPH10_SMART6();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append(hmFundVal.get(i));
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmFundVal=null;
		return sb.toString();
	}

	public String getTable4SurrenderBenefit(int startPt, int endPt){
		HashMap surrValue=master.getNonGuaranteed4SurrBenefit();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//added on 25-06-2014
	public String getTableANNUITYRate(int startPt, int endPt){//Added by Samina for Easy Retire
		HashMap annuityrate=master.getAnnuityRate();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(annuityrate.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		annuityrate=null;
		return sb.toString();
	}

	public String getAltSceVariableBonus(int startPt, int endPt){
		HashMap hmVSRBonus=master.getVariableBonus();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmVSRBonus.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmVSRBonus=null;
		return sb.toString();
	}
	//added on 26-06-2014

	public String getAltSceTotalDeathBenefitCR(int startPt, int endPt){
		HashMap hmTotalDeathBenifit=master.getAltScenarioTotalDeathBenefitCR();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(hmTotalDeathBenifit.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmTotalDeathBenifit=null;
		return sb.toString();
	}

	public String getTotalMaturityBenefit8(int startPt, int endPt){
		HashMap surrValue=master.getTotalMaturityBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getTotalMaturityBenefit4(int startPt, int endPt){
		HashMap surrValue=master.getTotalMaturityBenefit4();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				sb.append(surrValue.get(i));
				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}

	public String getSurrenderBenefit8(int startPt, int endPt){
		HashMap surrValue=master.getSurrBenefit8();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		if(startPt <= endPt){
			for(int i=startPt;i<=endPt;i++){
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");

				sb.append(surrValue.get(i));

				sb.append("</td></tr>");
			}
		}else{
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		surrValue=null;
		return sb.toString();
	}
	//End : added by jayesh for SMART7 : 07-05-2014

	public String getTablePremiumAllocationChargesPPT(){
		int ppt=master.getRequestBean().getPremiumpayingterm();
		int pt=master.getRequestBean().getPolicyterm();
		HashMap hmAllocCharg=master.getPremiumAllocationCharges();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i<=ppt)
				sb.append(df.format(hmAllocCharg.get(i)));
			else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		hmAllocCharg=null;
		return sb.toString();
	}
    public String getTableRiderOnPPTMRS(int startPt, int endPt, int showPt){
        ArrayList aList=master.getRiderData();
		int ppt = 0;
		if(aList!=null && aList.size()>0) {
			RiderBO oRiderBO = (RiderBO) aList.get(0);
			ppt = oRiderBO.getPremPayTerm();//master.getRequestBean().getPremiumpayingterm();
		}
			//int pt=master.getRequestBean().getPolicyterm();
			HashMap totalRiderPrem = master.getTotalRiderAnnualPremium();
			StringBuffer sb = new StringBuffer();
			sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
			for (int i = startPt; i <= endPt; i++) {
				sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
				if (i <= ppt)
					sb.append(totalRiderPrem.get(i));
				else
					sb.append("0");
				sb.append("</td></tr>");
			}
			sb.append("</table>");
			totalRiderPrem=null;
			return sb.toString();
    }
	/*public String getTableRiderOnPPTSmart7(int startPt, int endPt, int showPt){
		int ppt=master.getRequestBean().getPremiumpayingterm();
		int pt=master.getRequestBean().getPolicyterm();
		HashMap totalRiderPrem=master.getTotalRiderAnnualPremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=1;i<=pt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i<=ppt)
				sb.append(totalRiderPrem.get(i));
			else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		totalRiderPrem=null;
		return sb.toString();
	}*/
	public String getTableRiderOnPPTSmart7(int startPt, int endPt, int showPt){
		int ppt=master.getRequestBean().getPremiumpayingterm();
		//int pt=master.getRequestBean().getPolicyterm();
		HashMap totalRiderPrem=master.getTotalRiderAnnualPremium();
		StringBuffer sb=new StringBuffer();
		sb.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		for(int i=startPt;i<=endPt;i++){
			sb.append("<tr><td style=\"font-size:7.0pt;font-weight:400;font-style:normal;font-family:Tahoma, sans-serif;\">");
			if (i<=ppt)
				sb.append(totalRiderPrem.get(i));
			else
				sb.append("0");
			sb.append("</td></tr>");
		}
		sb.append("</table>");
		totalRiderPrem=null;
		return sb.toString();
	}
}
