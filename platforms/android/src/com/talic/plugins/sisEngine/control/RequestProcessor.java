package com.talic.plugins.sisEngine.control;

import android.util.Log;

import com.talic.plugins.sisEngine.BO.AgentDetails;
import com.talic.plugins.sisEngine.BO.PFRPlanFormulaRef;
import com.talic.plugins.sisEngine.BO.PGFPlanGroupFeature;
import com.talic.plugins.sisEngine.BO.PNLPlanLk;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.bean.FormulaBean;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.db.DataAccessClass;
import com.talic.plugins.sisEngine.util.CommonConstant;
import com.talic.plugins.sisEngine.util.FormulaConstant;
import com.talic.plugins.sisEngine.util.FormulaHandler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import static com.talic.plugins.sisEngine.db.DataAccessClass.getPlanLKObject;

@SuppressWarnings("unchecked")
public class RequestProcessor {
	private ModelMaster master;

	private FormulaBean oFormulaBean = new FormulaBean();

	public RequestProcessor(ModelMaster mm) {
		this.master = mm;
	}

	public ModelMaster getSISData() {

		ArrayList alList = new ArrayList();
		String strParamValue = DataAccessClass.getSPVSysParamValues(master.getRequestBean().getBaseplan());

		if (strParamValue.contains("CAL_BAP")) {
			//if(master.getRequestBean().getProduct_type().equals("U")){
				master.setBaseAnnualizedPremium(getBasePremium());
			/*}else {
				if (master.getRequestBean().getBasepremium() == 0)
					master.setBaseAnnualizedPremium(getBasePremium());
				else
					master.setBaseAnnualizedPremium(master.getRequestBean().getBasepremium());
			}*/
			master.setAnnualPremium(getAnnualPremium());
		}
		if (strParamValue.contains("CAL_APWOL")) {
			master.setAnnualizedPremiumWithOccLoading(getAnnualizedPremiumWithOccLoading());
		}
		if (strParamValue.contains("CALC_MP")) {
			master.setModalPremium(getModalPremium());
			// master.setBasePrimium(getAnnualizedPremium(master.getModalPremium()));
		}
		if (strParamValue.contains("CALC_RENEWAL_MP")) {
			master.setModalPremiumRenewal(getModalPremiumRenewal());
		}
		if (strParamValue.contains("CALC_NSAP")) {
			master.setModalPremiumForNSAP(getNSAPLoadPremium());
			//added by Ganesh for Fortune Gaurantee 07/09/2015
			master.setModalPremiumForAnnNSAP(getNSAPAnnualLoadPremium());
		}
		master.setQuotedAnnualizedPremium(getQuotedAnnualizedPremium());

		if (strParamValue.contains("CALDP")) {
			if ("Y".equalsIgnoreCase(master.getRequestBean().getTata_employee())) {
				master.setDiscountedBasePrimium(getDiscountedPremium());
			}
		}
		if (strParamValue.contains("RP")) {
			master.setRiderData(getRiderWithPremium());
		} else {
			// initializing with empty list if rider premium not applicable
			master.setRiderData(alList);
		}
		if(strParamValue.contains("CALC_RENEWAL_MP")){
			master.setTotalRenewalModalPremium(getTotalRenewalModalPremium());

		}
		//master.setTotalRiderModalPremium(getTotalRiderModePremium());
		master.setTotalModalPremium(getTotalModalPremium());

		master.setQuotedModalPremium(master.getTotalModalPremium() + master.getModalPremiumForNSAP());


		if (strParamValue.contains("PHACC10")) {
			calculateChargesForPH10();
			calculateChargesForPH10withoutST();
			//master.setNetYield8P(6.83);
		}
        if (strParamValue.contains("PHACC6")) {
            calculateChargesForPH6();
        }
        // Good Kid
        if (strParamValue.contains("CAL_TOT_MILE_ADD8")) {
            master.setMilestoneAdd8Dict(getMilestoneAdditions("CAL_TOT_MILE_ADD8"));
        }
        if (strParamValue.contains("CAL_TOT_MILE_ADD4")) {
            master.setMilestoneAdd4Dict(getMilestoneAdditions("CAL_TOT_MILE_ADD4"));
        }
        if (strParamValue.contains("CAL_MBB")) {
            master.setMoneyBackBenefitDict(getMoneyBackBenfit());
        }
        // Good Kid

		//if (strParamValue.contains("PHACC10_AAA")) {//Added for Alternate Scanario AAA -- Also commented by Akhil
			/*if (!master.getRequestBean().getAAA().equals("null") && !master.getRequestBean().getAAA().equals("")) {
				calculateAlternateChargesAAAPH10();
				master.getRequestBean().setAAASelected("YES");
			} else
				master.getRequestBean().setAAASelected("NO");*/
		//}
		//if (strParamValue.contains("PHACC6_AAA")) {//Added for Alternate Scanario AAA -- Also commented by Akhil
			/*if (!master.getRequestBean().getAAA().equals("null") && !master.getRequestBean().getAAA().equals("")) {
				calculateAlternateChargesAAAPH6();
				master.getRequestBean().setAAASelected("YES");
			} else
				master.getRequestBean().setAAASelected("NO");*/
		//}
		/*

		commented by Akhil

		if (strParamValue.contains("PHACC10_SMART")) {//Added for Alternate Scanario SMART8
			if (!master.getRequestBean().getSmartEquFund().equals("null") && !master.getRequestBean().getSmartEquFund().equals("")) {
				calculateAlternateChargesSMARTPH10();
				calculateAlternateChargesSMARTPH10ST();
				//master.setNetYield8SMART(7.17);
				master.getRequestBean().setSmartSelected("YES");
			} else
				master.getRequestBean().setSmartSelected("NO");
		}
		if (strParamValue.contains("PHACC6_SMART")) {//Added for Alternate Scanario SMART6
			if (!master.getRequestBean().getSmartEquFund().equals("null") && !master.getRequestBean().getSmartEquFund().equals("")) {
				calculateAlternateChargesSMARTPH6();
				master.getRequestBean().setSmartSelected("YES");
			} else
				master.getRequestBean().setSmartSelected("NO");
		}
		if (strParamValue.contains("PHACCFP")) {//Added for Alternate Scanario Fund Performance
			if (master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals("")) {
				//calculateChargesForFPPH10();
				master.getRequestBean().setFpSelected("YES");
			} else
				master.getRequestBean().setFpSelected("NO");
		}
		if (strParamValue.contains("PHACCTUWD")) {//Added for Alternate Scanario TopUp Withdrawal
			if ((master.getRequestBean().getFixWithDAmt()!=null && !master.getRequestBean().getFixWithDAmt().equals("")) || (master.getRequestBean().getVarWithDAmtYr()!=null &&  !master.getRequestBean().getVarWithDAmtYr().equals("")) || (master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals("")) || (master.getRequestBean().getFixTopupAmt()!=null &&  !master.getRequestBean().getFixTopupAmt().equals("")) || (master.getRequestBean().getVarTopupAmtYr()!=null &&  !master.getRequestBean().getVarTopupAmtYr().equals(""))) {
				String header = "";
				int noHeader = 0;
				if ((master.getRequestBean().getFixTopupAmt()!=null &&  !master.getRequestBean().getFixTopupAmt().equals("")) || (master.getRequestBean().getVarTopupAmtYr()!=null &&  !master.getRequestBean().getVarTopupAmtYr().equals(""))) {
					noHeader = 1;
					header = "Top-ups";
				}
				if ((master.getRequestBean().getFixWithDAmt()!=null && !master.getRequestBean().getFixWithDAmt().equals(""))
						|| (master.getRequestBean().getVarWithDAmtYr()!=null &&  !master.getRequestBean().getVarWithDAmtYr().equals(""))) {
					if (noHeader == 1) {
						header += " and ";
					}
					header += "Partial Withdrawals";
					noHeader = 1;
				}
				if (master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals("")) {
					if (noHeader == 1) {
						header += " and ";
					}
					header += "Actual Fund Performance";
				}
				calculateTopUpPartialWithDForPH10();
				calculateTopUpPartialWithDForPH10ST();
				//master.setNetYield8TW(4.87);
				master.getRequestBean().setTopUpWDSelected("YES");
				master.setAltHeader(header);
			} else
				master.getRequestBean().setTopUpWDSelected("NO");
		}*/
		if (strParamValue.contains("CALCFMC")) {
			master.setFmc(DataAccessClass.getFMC(master.getRequestBean()));
		}
		if (strParamValue.contains("LIFE_COV")) {
			master.setLifeCoverage(getLifeCoverage());
		}
		if (strParamValue.contains("LIFE_SIP_COV")) {
			master.setLifeCoverage(getSIPLifeCoverage());
		}
		if (strParamValue.contains("LIFE_GIP_COV")) {
			master.setLifeCoverage(getGIPLifeCoverage());
		}
		if (strParamValue.contains("CALCMV")) {
			master.setMaturityVal(getMaturityValue());
		}
		 //added by ganesh 07/01/2015
		if (strParamValue.contains("CALCMV_MIP")) {
			master.setBasicSumAssured(getBasicSumAssured());
		}
		/*

		commented by Akhil

		if (strParamValue.contains("CALCMVALTSCE")) {//Added by samina on 13/11/2014
			master.setMaturityValALTSCE(getMaturityValueALTSCE());
		}*/
		if (strParamValue.contains("CALGAI")) {
			master.setGuranteedAnnualIncome(getGuranteedAnnualIncome());
		}
        if (strParamValue.contains("CALGIPGAI")) {
            master.setGuranteedAnnualIncome(getGuranteedAnnualIncome_GIP());
        }
		if (strParamValue.contains("CALEPGAI")) {
			master.setGuranteedAnnualIncome(getGuranteedEPAnnualIncome());
		}
		if (strParamValue.contains("CALTOTGB")) {
			master.setTotalGuaranteedBenefit(getTotalGuaranteeeBenefit());
		}
        if (strParamValue.contains("CALGIPTOTGB")) {
            master.setTotalGuaranteedBenefit(getTotalGuaranteeeBenefit_GIP());
        }
		if (strParamValue.contains("CALETOTGB")) {
			master.setTotalGuaranteedBenefit(getTotalEGuaranteeeBenefit());
		}
		if (strParamValue.contains("CALALLGAI")) {
			master.setGuranteedAnnualIncomeAll(getGuranteedAnnualIncomeAll());
		}
		if (strParamValue.contains("CALACCGAI")) {
			master.setAccGuranteedAnnualIncome(getAccGuranteedAnnualIncome());
		}
		if (strParamValue.contains("CALACCPTGAI")) {
			master.setAccGuranteedAnnualIncome(getAccPTGuranteedAnnualIncome());
		}
		if (strParamValue.contains("CALRPGAI")) {
			master.setRPGAI(getRPGAI());
		}
		/*
		commented by Akhil
		if (strParamValue.contains("CALALTSCEGAI")) {
			master.setALTSCEGAI(getALTSCEGuranteedAnnualIncome());
		}*/
		if (strParamValue.contains("SB")) {
			master.setNonGuaranteedSurrBenefit(getSurrenderBenefit());
		}
        if (strParamValue.contains("LTGIPSB")) {
            master.setNonGuaranteedSurrBenefit(getSurrenderBenefit_GIP());
        }
		if (strParamValue.contains("SIPEB")) {
			master.setNonGuaranteedSurrBenefit(getSurrenderBenefit_SIP());
		}
		/*
		commented by Akhil
		if (strParamValue.contains("ALTSB")) {//Added by Samina on 18/11/2014
			master.setALTSCESurrBenefit(getALTSurrenderBenefit());
		}*/
		if (strParamValue.contains("CALGSV")) {
			master.setGuranteedSurrenderValue(getGuranteedSurrenderValue());
		}
		if (strParamValue.contains("CALGSV_MIP")) {
			master.setGuranteedSurrenderValue(getGuranteedSurrenderValue_MIP());
		}
        if (strParamValue.contains("CALLTGSV")) {
            master.setGuranteedSurrenderValue(getGuranteedSurrenderValue_LastYear());
        }
		if (strParamValue.contains("CALGSV_SIP")) {
			master.setGuranteedSurrenderValue(getGuranteedSurrenderValue_SIP());
		}
		if (strParamValue.contains("CALGSV_ESIP")) {
			master.setGuranteedSurrenderValue(getGuranteedSurrenderValue_ESIP());
		}
		/*

		commented by Akhil

		if (strParamValue.contains("ALTCALGSV")) {
			master.setALTguranteedSurrenderValue(getALTGuranteedSurrenderValue());
		}*/
		if (strParamValue.contains("TOTMATVAL")) {
		  master.setTotalMaturityValue(getTotalMaturityValue());
		}
		if (strParamValue.contains("CAL_VSRB4")) {
			  master.setVSRBonus4(getVSRBonus4());
		}
		if (strParamValue.contains("CAL_VSRB4_MIP")) {
			master.setVSRBonus4(getVSRBonus4_MIP());
		}
		/*
		commented by Akhil
		if (strParamValue.contains("CAL_ALT_VSRB4")) {//Added by Samina for reduced paid up on 20/11/2014
			  master.setAltVSRBonus4(getAltVSRBonus4());
		}
		if (strParamValue.contains("CAL_ALT_EBDURING")) {//Added by Samina for reduced paid up on 20/11/2014
			  //master.setAltEBDURING(getAltEBONUSDURING());
		}*/
		if (strParamValue.contains("CAL_TB4")) {
			  master.setTerminalBonus4(getTerminalBonus4());
		}
		if (strParamValue.contains("CAL_TB4_MIP")) {
			master.setTerminalBonus4(getTerminalBonus4_MIP());
		}
		if (strParamValue.contains("CAL_TDB4")) {
			  master.setTotalDeathBenefit4(getTotalDeathBenefit4());
		}
		if (strParamValue.contains("CAL_TDB4_MIP")) {
			master.setTotalDeathBenefit4(getTotalDeathBenefit4_MIP());
		}

		if (strParamValue.contains("CALFR_TDB4")) {
			master.setTotalDeathBenefit4(getTotalDeathBenefitFR4());
		}
		/** Vikas - Added for Mahalife Gold*/

		if (strParamValue.contains("CAL_TDBG")) {
			  master.setTotalDeathBenefit(getTotalDeathBenefit());
		}
		if (strParamValue.contains("TOT_CASHDIV4")) {
			  master.setTotalcashdividend4(getTotalCashDividend4());
		}
		if (strParamValue.contains("TOT_CASHDIV8")) {
			  master.setTotalcashdividend8(getTotalCashDividend8());
		}
		if (strParamValue.contains("TOT_ANNCOUP")) {
			  master.setTotalGAnnualCoupon(getTotalAnnualCoupon());
		}
		/** Vikas - Added for Mahalife Gold*/


		// Added for Inflation Protection Cover Gold Plus - Mohammad Rashid - 7-Jan-2014
		if (strParamValue.contains("CALIPC")) {
			master.setGuranteedAnnualInflationCover(getGuranteedAnnualInflationCover());
		}
		if (strParamValue.contains("CAL_TDBGP")) {
			master.setTotalDeathBenefit(getTotalDeathBenefit());
		}
		//End

		if (strParamValue.contains("SB4")) {
			master.setNonGuaranteedSurrBenefit(getSurrenderBenefit_4());
			master.setNonGuaranteed4SurrBenefit(getSurrenderBenefit_4_Smart7());
		}
		if (strParamValue.contains("SBMIP4")) {
			master.setNonGuaranteedSurrBenefit(getSurrenderBenefitMIP_4());
		}
		if (strParamValue.contains("SBFR4")) {
			master.setNonGuaranteedSurrBenefit(getSurrenderBenefitFR_4());
		}
		// for 8 % Money Maxima

		if (strParamValue.contains("CAL_VSRB8")) {
			  master.setVSRBonus8(getVSRBonus8());
		}
		if (strParamValue.contains("CAL_VSRB8_MIP")) {
			master.setVSRBonus8(getVSRBonus8_MIP());
		}
		/*
		commented by Akhil
		if (strParamValue.contains("CAL_ALT_VSRB8")) {
			  master.setAltVSRBonus8(getAltVSRBonus8());
		}*/
		if (strParamValue.contains("CAL_TB8")) {
			  master.setTerminalBonus8(getTerminalBonus8());
		}
		if (strParamValue.contains("CAL_TB8_MIP")) {
			master.setTerminalBonus8(getTerminalBonus8_MIP());
		}
		if (strParamValue.contains("CALFR_TDB8")) {
			master.setTotalDeathBenefit8(getTotalDeathBenefitFR8());
		}
		if (strParamValue.contains("CAL_TDB8")) {
			  master.setTotalDeathBenefit8(getTotalDeathBenefit8());
		}
		if (strParamValue.contains("CAL_TDB8_MIP")) {
			master.setTotalDeathBenefit8(getTotalDeathBenefit8_MIP());
		}
		if (strParamValue.contains("CALFR_TDB8")) {
			master.setTotalDeathBenefit8(getTotalDeathBenefitFR8());
		}
		if (strParamValue.contains("SB8")) {
			master.setNonGuaranteedSurrBenefit_8(getSurrenderBenefit_8());
		}
		if (strParamValue.contains("SBMIP8")) {
			master.setNonGuaranteedSurrBenefit_8(getSurrenderBenefitMIP_8());
		}
		if (strParamValue.contains("SBFR8")) {
			master.setNonGuaranteedSurrBenefit_8(getSurrenderBenefitFR_8());
		}
		if (strParamValue.contains("CAL_RP_A")) {
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremium_A(0);
			else
				master.setRenewalPremium_A(getRenewalPremium("CAL_RP_A"));
		}
		if (strParamValue.contains("CAL_RP_SA")) {
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremium_SA(0);
			else
				master.setRenewalPremium_SA(getRenewalPremium("CAL_RP_SA"));
		}
		if (strParamValue.contains("CAL_RP_Q")) {
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremium_Q(0);
			else
				master.setRenewalPremium_Q(getRenewalPremium("CAL_RP_Q"));
		}
		if (strParamValue.contains("CAL_RP_M")) {
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremium_M(0);
			else
				master.setRenewalPremium_M(getRenewalPremium("CAL_RP_M"));
		}
		//Added by Ganesh for Fortune Garantee 07/09/2015
		if (strParamValue.contains("CAL_RP_FG_M")) {
			master.setRenewalPremium_M(getRenewalPremiumMonthly());
		}
		if (strParamValue.contains("CAL_RP_FG_SA")) {
			master.setRenewalPremium_SA(getRenewalPremiumSemi());
		}
		if (strParamValue.contains("CAL_RP_FG_Q")) {
			master.setRenewalPremium_Q(getRenewalPremiumQuaterly());
		}
        /************************* Smart 7 ****************************/
        //added by Ganesh for Smart 7 on 11-04-2016
        if(strParamValue.contains("ALTSCE_SBCR")){
            master.setSurrBenefit8(getSurrenderBenefit8());
            //master.setAltScenarioSurrBenefitCurrentRate(getAltScenarioSurrenderBenefit_CurrentRate());
        }
        if (strParamValue.contains("ALTSCE_MAT4BEN_TAX")) {
            master.setAltScenario4MaturityBenefitTax(getAltScenario4MaturityBenefitTax());
        }
        /************************* Smart 7 end ****************************/
		/*

		commented by Akhil

		if (strParamValue.contains("LIFE_COV_RPU")) {
			master.setLifeCoverageAltSce(getLifeCoverageAltSce());
		}
		if (strParamValue.contains("LIFE_COV_RPUY")) {
			master.setLifeCoverageAlternateSce(getLifeCoverageAlternateSce());
		}
		if (strParamValue.contains("CALCALTSCEMV")) {
			master.setAltScenarioMaturityVal(getAltScenarioMaturityValue());
		}
		if (strParamValue.contains("CALCTOTALTSCEMV")) {
			master.setTOTaltScenarioMaturityVal(getAltScenarioTOTMaturityValue());
		}
		if (strParamValue.contains("CAL_ALTSCE_TDB4")) {
			  master.setAltScenarioTotalDeathBenefit4(getAltSceTotalDeathBenefit4());
		}
		if (strParamValue.contains("CAL_ALTSCE_TDB8")) {
			  master.setAltScenarioTotalDeathBenefit8(getAltSceTotalDeathBenefit8());
		}
		if (strParamValue.contains("ALTSCE_MAT4VAL")) {
			master.setAltScenarioMaturityVal4(getAltScenarioMaturityValue4());
		}
		if (strParamValue.contains("ALTSCE_MAT8VAL")) {
			master.setAltScenarioMaturityVal8(getAltScenarioMaturityValue8());
		}
		if(strParamValue.contains("ALTSCE_TOTMATBENCR")){
			master.setAltScenarioTotMatBenCurrentRate(getAltScenarioTotMatBenCurrentRate());
		}
		if(strParamValue.contains("ALTSCE_SBCR")){
			master.setSurrBenefit8(getSurrenderBenefit8());
			master.setAltScenarioSurrBenefitCurrentRate(getAltScenarioSurrenderBenefit_CurrentRate());
		}
		if(strParamValue.contains("ALTSCE_VBCUMMULATIVECR")){
			master.setAltScenarioPremPaidCumulativeCurrentRate(getAltScenarioPremiumPaid_Cumulative_CurrentRate());
			master.setAltEBDURING(getAltEBONUSDURING());
		}
		if(strParamValue.contains("ALTSCE_TAXSAVING")){
			master.setAltScenarioTaxSaving(getAltScenarioTaxSaving());
		}
		if(strParamValue.contains("ALTSCE_AEP")){
			master.setAltScenarioannualizedeffectivepremium(getAltScenarioAnnEffPrem());
		}
		if(strParamValue.contains("ALTSCE_CEP")){
			master.setAltScenariocummulativeeffectivepremium(getAltScenarioCumulativeEffPrem());
		}
		*/
		if(strParamValue.contains("STAXB")){
			master.setServiceTaxBasePlan(getServiceTaxForBasePlanPremium());
		}
		if(strParamValue.contains("TOTMODAL_PREM_PAYBLE")){
			master.setTotModalPremiumPayble(getTotalModalPremiumPayble());
		}
		if (strParamValue.contains("CAL_RPST_A") || strParamValue.contains("CAL_RPST_RS_A")) {
			master.setRenewalPremiumST_A(getRenewalPremiumServiceTax("CAL_RPST_A"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremiumST_RS_A(0);
			else
				master.setRenewalPremiumST_RS_A(getRenewalPremiumServiceTax("CAL_RPST_RS_A"));
		}
		if (strParamValue.contains("CAL_RPST_SA") || strParamValue.contains("CAL_RPST_RS_SA")) {
			master.setRenewalPremiumST_SA(getRenewalPremiumServiceTax("CAL_RPST_SA"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremiumST_RS_SA(0);
			else
				master.setRenewalPremiumST_RS_SA(getRenewalPremiumServiceTax("CAL_RPST_RS_SA"));
		}
		if (strParamValue.contains("CAL_RPST_Q") || strParamValue.contains("CAL_RPST_RS_Q")) {
			master.setRenewalPremiumST_Q(getRenewalPremiumServiceTax("CAL_RPST_Q"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremiumST_RS_Q(0);
			else
				master.setRenewalPremiumST_RS_Q(getRenewalPremiumServiceTax("CAL_RPST_RS_Q"));
		}
		if (strParamValue.contains("CAL_RPST_M") || strParamValue.contains("CAL_RPST_RS_M")) {
			master.setRenewalPremiumST_M(getRenewalPremiumServiceTax("CAL_RPST_M"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setRenewalPremiumST_RS_M(0);
			else
				master.setRenewalPremiumST_RS_M(getRenewalPremiumServiceTax("CAL_RPST_RS_M"));
        }
        //Added by Ganesh for FG 07/09/2015
        if (strParamValue.contains("CAL_RPST_FG_A")) {
            master.setRenewalPremiumST_A(getRenewalPremiumServiceTaxMonthly(getRenewalPremiumAnn()));
        }

        if (strParamValue.contains("CAL_RPST_FG_M")) {
            master.setRenewalPremiumST_M(getRenewalPremiumServiceTaxMonthly(getRenewalPremiumMonthly()));
        }
        if (strParamValue.contains("CAL_RPST_FG_SA")) {
            master.setRenewalPremiumST_SA(getRenewalPremiumServiceTaxMonthly(getRenewalPremiumSemi()));
        }
		if (strParamValue.contains("CAL_RPST_FG_Q")) {
			master.setRenewalPremiumST_Q(getRenewalPremiumServiceTaxMonthly(getRenewalPremiumQuaterly()));
		}
		if (strParamValue.contains("CAL_TOTRP_SA") || strParamValue.contains("CAL_TOTRP_RS_SA")) {
			master.setTotalRenewalPremium_SA(getTotalRenewalPremium("CAL_TOTRP_SA"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setTotalRenewalPremium_RS_SA(0);
			else
				master.setTotalRenewalPremium_RS_SA(getTotalRenewalPremium("CAL_TOTRP_RS_SA"));
		}
		if (strParamValue.contains("CAL_TOTRP_Q") || strParamValue.contains("CAL_TOTRP_RS_Q")) {
			master.setTotalRenewalPremium_Q(getTotalRenewalPremium("CAL_TOTRP_Q"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setTotalRenewalPremium_RS_Q(0);
			else
				master.setTotalRenewalPremium_RS_Q(getTotalRenewalPremium("CAL_TOTRP_RS_Q"));
		}
		if (strParamValue.contains("CAL_TOTRP_M") || strParamValue.contains("CAL_TOTRP_RS_M")) {
			master.setTotalRenewalPremium_M(getTotalRenewalPremium("CAL_TOTRP_M"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setTotalRenewalPremium_RS_M(0);
			else
				master.setTotalRenewalPremium_RS_M(getTotalRenewalPremium("CAL_TOTRP_RS_M"));
		}
		//added by Ganesh for fg 07/09/2015
        if (strParamValue.contains("CAL_TOTRP_FG_A")) {
            master.setTotalRenewalPremium_A(getTotalRenewalPremiumMonthly(getRenewalPremiumAnn()));
        }
		if (strParamValue.contains("CAL_TOTRP_FG_M")) {
			master.setTotalRenewalPremium_M(getTotalRenewalPremiumMonthly(getRenewalPremiumMonthly()));
		}
		if (strParamValue.contains("CAL_TOTRP_FG_SA")) {
			master.setTotalRenewalPremium_SA(getTotalRenewalPremiumMonthly(getRenewalPremiumSemi()));
		}
		if (strParamValue.contains("CAL_TOTRP_FG_Q")) {
			master.setTotalRenewalPremium_Q(getTotalRenewalPremiumMonthly(getRenewalPremiumQuaterly()));
		}
		if (strParamValue.contains("CAL_TOTRP_A") || strParamValue.contains("CAL_TOTRP_RS_A")) {
			master.setTotalRenewalPremium_A(getTotalRenewalPremium("CAL_TOTRP_A"));
			if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
				master.setTotalRenewalPremium_RS_A(0);
			else
				master.setTotalRenewalPremium_RS_A(getTotalRenewalPremium("CAL_TOTRP_RS_A"));
		}




		// Added by Ganesh for MLS

		if (strParamValue.contains("CAL_RPST_LS_A")){
			master.setRenewalPremiumST_RS_A(getRenewalPremiumServiceTax("CAL_RPST_LS_A"));
		}
		if (strParamValue.contains("CAL_RPST_LS_SA")){
			master.setRenewalPremiumST_RS_SA(getRenewalPremiumServiceTax("CAL_RPST_LS_SA"));
		}
		if (strParamValue.contains("CAL_RPST_LS_Q")){
			master.setRenewalPremiumST_RS_Q(getRenewalPremiumServiceTax("CAL_RPST_LS_Q"));
		}
		if(strParamValue.contains("CAL_RPST_LS_M")){
			//master.setRenewalPremiumST_RS_M(getRenewalPremiumServiceTax("CAL_RPST_LS_M"));
            master.setRenewalPremiumST_RS_M(Math.round(master.getRenewalPremium_M() * 0.0181));
		}



		if(strParamValue.contains("CAL_TOTRP_LS_A")){
			master.setTotalRenewalPremium_RS_A(getTotalRenewalPremium("CAL_TOTRP_LS_A"));
		}

		if(strParamValue.contains("CAL_TOTRP_LS_SA")){
			//master.setTotalRenewalPremium_RS_SA(getTotalRenewalPremium("CAL_TOTRP_LS_SA"));
            master.setTotalRenewalPremium_RS_SA(master.getRenewalPremium_SA() + master.getRenewalPremiumST_RS_SA());
            Log.d("PremSA", String.valueOf(master.getRenewalPremium_SA()));
            Log.d("PremSAST", String.valueOf(master.getRenewalPremiumST_RS_SA()));
        }

		if(strParamValue.contains("CAL_TOTRP_LS_Q")){
            //master.setTotalRenewalPremium_RS_Q(getTotalRenewalPremium("CAL_TOTRP_LS_Q"));
            master.setTotalRenewalPremium_RS_Q(master.getRenewalPremium_Q() + master.getRenewalPremiumST_RS_Q());
		}

		if(strParamValue.contains("CAL_TOTRP_LS_M")) {
            //master.setTotalRenewalPremium_RS_M(getTotalRenewalPremium("CAL_TOTRP_LS_M"));
            master.setTotalRenewalPremium_RS_M(master.getRenewalPremium_M() + master.getRenewalPremiumST_M());
		}


		/*

		commented by Akhil

		if (strParamValue.contains("ALTSCE_VBCR")) {
			  master.setVariableBonus(getVariableBonus());
		}
		if (strParamValue.contains("ALTSCE_TDBCR")) {
			  master.setAltScenarioTotalDeathBenefitCR(getAltScenarioTotalDeathBenefitCR());
		}*/
		if(strParamValue.contains("ALTSCE_TOTMATBENCR")){
			master.setTotalMaturityBenefit8(getTotMaturityBenefit8());
			//master.setAltScenarioTotMatBenCurrentRate(getAltScenarioTotMatBenCurrentRate());
		}
		if(strParamValue.contains("TOT_MATVAL8_FR")){
			master.setTotalMaturityBenefit8(getTotMaturityBenefitFR8());
		}
		if(strParamValue.contains("ALTSCE_TOTMATBENCR_MIP")){
			master.setTotalMaturityBenefit8(getTotMaturityBenefit8_MIP());
		}
		if(strParamValue.contains("TOT_MATVAL4") || strParamValue.contains("MAT4VAL")){
			master.setTotalMaturityBenefit4(getTotMaturityBenefit4());
		}
		if(strParamValue.contains("TOT_MATVAL4_FR")){
			master.setTotalMaturityBenefit4(getTotMaturityBenefitFR4());
		}
		if(strParamValue.contains("TOT_MATVAL4_MIP")){
			master.setTotalMaturityBenefit4(getTotMaturityBenefit4_MIP());
		}
		if(strParamValue.contains("TOT_MATVAL8_MBP")){
			master.setTotalMaturityBenefit8(getTotMaturityBenefit8_MBP());
		}
		if(strParamValue.contains("TOT_MATVAL4_MBP")){
			master.setTotalMaturityBenefit4(getTotMaturityBenefit4_MBP());
		}
		if(strParamValue.contains("TOT_RB8")){
			master.setTOTALRB8(getTOTALVSRBonus8());
			master.setTOTALRB4(getTOTALVSRBonus4());
			master.setTOTALGAI(getTOTALGuranteedAnnualIncome());
			master.setTAXtext(getEffectivePremium8());
		}
		if(strParamValue.contains("TOT_RB8_MIP")){

			master.setTOTALRB4(getTOTALVSRBonus4_MIP());
			master.setTOTALRB8(getTOTALVSRBonus8_MIP());
			master.setTOTALGAI(getTOTALGuranteedAnnualIncome());

		}
        //Good Kid
        if(strParamValue.contains("TOT_MATVAL8_GK")){
            master.setTotalMaturityBenefit8(getTotBen8Payable());
        }
        if(strParamValue.contains("TOT_MATVAL4_GK")){
            master.setTotalMaturityBenefit4(getTotBen4Payable());
        }
        if(strParamValue.contains("GKSB8")){
            master.setNonGuaranteedSurrBenefit_8(getNonGuarSurrBen8_GK());
        }
        if(strParamValue.contains("GKSB4")){
            master.setNonGuaranteedSurrBenefit(getNonGuarSurrBen4_GK());
        }

        //Good Kid
		if(strParamValue.contains("MAT8VAL")){
			//added by Akhil on 06-07-2015 11:25:00 for Mahalife Gold
			master.setTotalMaturityBenefit8(getTotMaturityBenefit8());
        }


		master.setTotalRiderAnnualPremium(getTotalRiderAnnualPremium());

		master.setServiceTax(getServiceTaxOnPremium());

		/*if(master.getRequestBean().getBaseplan().equals("FGV1N1"))
			master.setTotalModalPremiumPayble((master.getQuotedModalPremium() + master.getServiceTax()+(Math.round(getServiceTax_Rider(master.getTotalRiderModalPremium())))));
		else
			master.setTotalModalPremiumPayble(master.getQuotedModalPremium() + master.getServiceTax());*/

		//if(master.getRequestBean().getBaseplan().equals("FGV1N1"))
			master.setTotalModalPremiumPayble((master.getQuotedModalPremium()+ master.getServiceTax()+(Math.round(getServiceTax(master.getTotalRiderModalPremium())))));
		//else
		//master.setTotalModalPremiumPayble(master.getQuotedModalPremium()
		//	+ master.getServiceTax());
		master.setTempImagePath(DataAccessClass.getTempImagePath(master.getRequestBean().getBaseplan()));
		master.setOAddressBO(DataAccessClass.getAddressBO());
		master.setPlanFeature(getPlanFeature());
		master.setTotalPremiumAnnual(getTotalPremium_Annual());
		master.setUINNumber(getUINNumber());
		if(master.getRequestBean().getBaseplan().equals("MRSP2N1V2") || master.getRequestBean().getBaseplan().equals("MRSP2N2V2"))
			master.setRenewalModalPremium(0);
		else
			master.setRenewalModalPremium(getRenewalModalPremium_A());
		master.setPlanDescription(getPlanDescription());
		//master.setAgentDeatils(getAgentDetails());
		master.setCommissionText(getComissionText());
		if(strParamValue.contains("CAL_TOTMATGAI")){
			master.setTotalMaturityValuePlusGAI(getTotalMaturityValuePlusGAI());
		}
		if (strParamValue.contains("ANNUITY")) {
			master.setAnnuityRate(getANNUITY_Val());
			master.getRequestBean().setAnnuity_amt(getAMT_ANNUITYRate());
			master.setTotalERModalPremPayble(master.getBaseAnnualizedPremium()
					+ master.getServiceTax());
		}

		master.setErrorMessage(ValidateMinimumPremium());

		return master;
	}
	public AgentDetails getAgentDetails(){
		return DataAccessClass.getAgentDetails();
	}

	public long getQuotedAnnualizedPremium() {
		long annualizedPremium = 0;
		if (master.getRequestBean().getFrequency().equalsIgnoreCase("A")) {
			annualizedPremium = master.getModalPremium()
					+ master.getModalPremiumForNSAP();
		} else if (master.getRequestBean().getFrequency().equalsIgnoreCase("S")) {

			annualizedPremium = (master.getModalPremium() + master
					.getModalPremiumForNSAP()) * 2;
		} else if (master.getRequestBean().getFrequency().equalsIgnoreCase("Q")) {
			annualizedPremium = (master.getModalPremium() + master
					.getModalPremiumForNSAP()) * 4;

		} else if (master.getRequestBean().getFrequency().equalsIgnoreCase("M")) {
			annualizedPremium = (master.getModalPremium() + master
					.getModalPremiumForNSAP()) * 12;

		} else if (master.getRequestBean().getFrequency().equalsIgnoreCase("O")) {

			annualizedPremium = (master.getModalPremium() + master
					.getModalPremiumForNSAP());
		}

		return annualizedPremium;
	}

	public long getModalPremium() {
		long modalPremium = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			if (master.getAnnualizedPremiumWithOccLoading() == 0) {
				if (master.getRequestBean().getProduct_type() != null && master.getRequestBean().getProduct_type().equals("U"))
					oFormulaBean.setP(master.getRequestBean().getBasepremiumannual()); //Changed by Ganesh 28/09/2015 3:10PM changed for ULIP Modal Premium
				else
				oFormulaBean.setP(master.getBaseAnnualizedPremium());
			} else {
				oFormulaBean.setP(master.getAnnualizedPremiumWithOccLoading());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String modalPremiumStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_MODE_PREMIUM, oFormulaBean)
					+ "";
			modalPremium = Long.parseLong(modalPremiumStr);
			CommonConstant.printLog("d", "Modal Premium Calculated :::: " + modalPremium);

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Modal Premium" + e + " " + e.getMessage());
		}
		oFormulaBean.setP(master.getRequestBean().getBasepremium()); //Changed by Ganesh 28/09/2015 3:10PM changed for ULIP Modal Premium

		return modalPremium;
	}

	public long getModalPremiumRenewal() {
		long renewalModalPremium = 0;
		double NSAPModalPremium=0;
		try {
			oFormulaBean = getFormulaBeanObj();
			if (master.getAnnualizedPremiumWithOccLoading() == 0) {
				oFormulaBean.setP(master.getBaseAnnualizedPremium());
			} else {
				oFormulaBean.setP(master.getAnnualizedPremiumWithOccLoading());
			}
			NSAPModalPremium=getNSAPLoadPremium();
			oFormulaBean.setA(NSAPModalPremium);
			FormulaHandler formulaHandler = new FormulaHandler();
			String modalPremiumStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_RENEWAL_MODEL_PREMIUM, oFormulaBean)
					+ "";
			renewalModalPremium = Long.parseLong(modalPremiumStr);
			CommonConstant.printLog("d", "Renewal Modal Premium Calculated :::: " + renewalModalPremium);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Modal Premium" + e + " " + e.getMessage());
		}
		return renewalModalPremium;
	}

	public long getAnnualizedPremiumWithOccLoading() {
		long annualPremiumOccLoading = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setP(master.getBaseAnnualizedPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String annualPremiumOccLoadingStr = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ANNUAL_PREMIUM_WITH_OCCUP_LOADING,
							oFormulaBean)
					+ "";
			annualPremiumOccLoading = Long
					.parseLong(annualPremiumOccLoadingStr);
			CommonConstant.printLog("d", "annualPremiumOccLoading Calculated :::: "
					+ annualPremiumOccLoading);

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating annualPremiumOccLoading"
					+ e + " " + e.getMessage());
		}

		return annualPremiumOccLoading;
	}

	public long getBasePremium() {
		long basePrimum = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			FormulaHandler formulaHandler = new FormulaHandler();
			String basePremium = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_BASE_ANNUALIZED_PREMIUM,
					oFormulaBean)
					+ "";
			basePrimum = Long.parseLong(basePremium);

			CommonConstant.printLog("d", "Base Premium Calculated :::: " + basePrimum);
			// master.setBasePrimium(basePrimum);
		if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U"))
				master.setPremiumMultiple(Double.parseDouble(master.getRequestBean().getPremiummul()));
			else
				master.setPremiumMultiple(DataAccessClass.getPremiumMultiple(master.getRequestBean()));
		master.setPremiumDiscount(DataAccessClass.getPremiumDiscount(master.getRequestBean().getBaseplan(), master.getRequestBean().getSumassured()));
			//master.setPremiumMultiple(Double.parseDouble(master.getRequestBean().getPremiummul()));
		} catch (ClassCastException e) {
			CommonConstant.printLog("e", "Error occured in calculating base premium" + e + " " + e.getMessage());
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating base premium" + e + " " + e.getMessage());
		}
		if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U")) {
			if (master.getRequestBean().getFrequency().equals("S"))
				return master.getRequestBean().getBasepremium()* 2;
			else if (master.getRequestBean().getFrequency().equals("Q"))
				return master.getRequestBean().getBasepremium()* 4;
			else if (master.getRequestBean().getFrequency().equals("M"))
				return master.getRequestBean().getBasepremium()* 12;
			else
				return master.getRequestBean().getBasepremium();
		} else
			return basePrimum;
	}
	public String getAnnualPremium() {
		String basePrimum = null;
		String baseplan=master.getRequestBean().getBaseplan();
		if (master.getRequestBean().getProduct_type()!=null && master.getRequestBean().getProduct_type().equals("U")) {
			if("IPR1ULN1".equals(baseplan) || "IMX1ULN1".equals(baseplan) || "WPR1ULN1".equals(baseplan) || "WMX1ULN1".equals(baseplan)) {
				if (master.getRequestBean().getFrequency().equals("S"))
					return "NA";
				else if (master.getRequestBean().getFrequency().equals("Q"))
					return "NA";
				else if (master.getRequestBean().getFrequency().equals("M"))
					return "NA";
				else
					return "NA";
			} else {
				return Long.toString(master.getRequestBean().getBasepremiumannual());
			}
		} else
			return basePrimum;
	}
    public ArrayList getRiderWithPremium() {
        ArrayList riderList = master.getRequestBean().getRiderlist();
        ArrayList riderPremiumLst = new ArrayList();
        long lTotalRiderModalPrem = 0;
        long lTotalAnnualRiderModalPrem = 0;
        if (riderList.size() > 0) {

            for (int count = 0; count < riderList.size(); count++) {
                RiderBO oRiderBO = (RiderBO) riderList.get(count);
                long riderPremium = 0;
                long riderAnnualPremium = 0;
                oFormulaBean = getFormulaBeanObj();
                oFormulaBean.setCNT(count);
                if (oRiderBO.getCode().startsWith("PB")) {
                    // calculate PB rider premium here
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.FORALL,
                            FormulaConstant.CALCULATE_PB_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderPremium);
                }
				/*else if(oRiderBO.getCode().startsWith("ADDL") && master.getRequestBean().getBaseplan().equals("FGV1N1"))
				{
					FormulaHandler formulaHandler = new FormulaHandler();
					String riderPremiumObj = formulaHandler.evaluateFormula(
							FormulaConstant.ADDL_FG,
							FormulaConstant.CALCULATE_RIDER_PREMIUM,
							oFormulaBean)
							+ "";
					riderPremium = Long.parseLong(riderPremiumObj);
					logger.debug("Rider Premium of ::::  "
							+ oRiderBO.getDescription() + " : " + riderPremium);
				}*/
                //Start : added by jayesh for SMART7 : 07-04-2014
                else if(oRiderBO.getCode().startsWith("ADDL"))
                {
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.ADDL,
                            FormulaConstant.CALCULATE_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderPremium);
                    riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.ADDL,
                            FormulaConstant.CALCULATE_ANNUAL_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderAnnualPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Annual Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderAnnualPremium);
                }
                else if(oRiderBO.getCode().startsWith("WOP"))
                {
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.WOP,
                            FormulaConstant.CALCULATE_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderPremium);
                }
                else if(oRiderBO.getCode().startsWith("WPP"))
                {
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.WPP,
                            FormulaConstant.CALCULATE_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderPremium);
                    riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.WPP,
                            FormulaConstant.CALCULATE_ANNUAL_RIDER_PREMIUM_WPP,
                            oFormulaBean)
                            + "";
                    riderAnnualPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Annual Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderAnnualPremium);
                }
                //End : added by jayesh for SMART7 : 07-04-2014
                else {
                    // calculate non PB rider premium here
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String riderPremiumObj = formulaHandler.evaluateFormula(
                            FormulaConstant.FORALL,
                            FormulaConstant.CALCULATE_RIDER_PREMIUM,
                            oFormulaBean)
                            + "";
                    riderPremium = Long.parseLong(riderPremiumObj);
                    CommonConstant.printLog("d", "Rider Premium of ::::  "
                            + oRiderBO.getDescription() + " : " + riderPremium);
                }

                // modal rider premium calculation
                //long modalPremium = riderPremium;
                long modalPremium = 0;
				/*if (oRiderBO.getCode().equals("ADDLULN2")) {
					if (master.getRequestBean().getFrequency().equalsIgnoreCase(
							CommonConstant.ANNUAL_MODE)) {
						modalPremium = riderPremium;
					} else if (master.getRequestBean().getFrequency()
							.equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)) {

						modalPremium = (long) (riderPremium * 0.51);
					} else if (master.getRequestBean().getFrequency()
							.equalsIgnoreCase(CommonConstant.QUATERLY_MODE)) {
						modalPremium = (long) (riderPremium * 0.26);

					} else if (master.getRequestBean().getFrequency()
							.equalsIgnoreCase(CommonConstant.MONTHLY_MODE)) {
						modalPremium = (long) (riderPremium * 0.0883);

					} else if (master.getRequestBean().getFrequency()
							.equalsIgnoreCase(CommonConstant.SINGLE_MODE)) {

						modalPremium = riderPremium;
					}
				} else {*/
                if (oRiderBO.getCode().startsWith("ADDL") || oRiderBO.getCode().startsWith("WOP")
                        || oRiderBO.getCode().startsWith("WPP")) {
                    modalPremium = riderPremium;
                    if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)) {
                        riderPremium = riderPremium * 2;
                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.QUATERLY_MODE)) {
                        riderPremium = riderPremium * 4;
                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.MONTHLY_MODE)) {
                        riderPremium = riderPremium * 12;
                    }
                }else{
                    if (master.getRequestBean().getFrequency().equalsIgnoreCase(
                            CommonConstant.ANNUAL_MODE)) {
                        modalPremium = riderPremium;
                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)) {

                        modalPremium = riderPremium / 2;
                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.QUATERLY_MODE)) {
                        modalPremium = riderPremium / 4;

                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.MONTHLY_MODE)) {
                        modalPremium = riderPremium / 12;

                    } else if (master.getRequestBean().getFrequency()
                            .equalsIgnoreCase(CommonConstant.SINGLE_MODE)) {

                        modalPremium = riderPremium;
                    }
                }
                //}
                oRiderBO.setModalpremium(modalPremium);
                oRiderBO.setPrimium(riderPremium);
                oRiderBO.setPolTerm(getRiderPolTerm(oRiderBO));
                oRiderBO.setPremPayTerm(getRiderPremPayTerm(oRiderBO));
                riderPremiumLst.add(oRiderBO);
                lTotalRiderModalPrem = lTotalRiderModalPrem + modalPremium;
                lTotalAnnualRiderModalPrem = lTotalAnnualRiderModalPrem + riderAnnualPremium;
            }
            double dServiceTaxRiderPerm = 0;
			/*if(master.getRequestBean().getBaseplan().equals("FGV1N1"))
				dServiceTaxRiderPerm = getServiceTax_Rider(lTotalRiderModalPrem);
			else*/
            dServiceTaxRiderPerm = getServiceTax(lTotalRiderModalPrem);
            //master.setAnnualRiderPremium(riderPremium);
            master.setTotAnnRiderModalPremium(lTotalAnnualRiderModalPrem);
            master.setTotalRiderModalPremium(lTotalRiderModalPrem);
            master.setSTRiderModalPremium(Math.round(dServiceTaxRiderPerm));
        }
        return riderPremiumLst;
    }
	/*
	 * public double getServiceTaxForTraditional(double amount){ double
	 * serviceTax=0; oFormulaBean = getFormulaBeanObj();
	 * oFormulaBean.setA(amount); FormulaHandler formulaHandler = new
	 * FormulaHandler(); String serviceTaxValue =
	 * formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(),
	 * FormulaConstant.CALCULATE_SERVICE_TAX, oFormulaBean)+ ""; serviceTax =
	 * Double.parseDouble(serviceTaxValue);
	 *
	 * return serviceTax; }
	 */
	public double getServiceTax(double amount) {
		double serviceTax = 0;
		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setA(amount);
		oFormulaBean.setP(master.getBaseAnnualizedPremium());
		FormulaHandler formulaHandler = new FormulaHandler();
		String serviceTaxValue = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SERVICE_TAX, oFormulaBean)
				+ "";
		serviceTax = Double.parseDouble(serviceTaxValue);

		return serviceTax;
	}
	public double getServiceTax_Rider(double amount) {
		double serviceTax = 0;
		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setA(amount);
		oFormulaBean.setP(master.getBaseAnnualizedPremium());
		FormulaHandler formulaHandler = new FormulaHandler();
		String serviceTaxValue = formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SERVICE_TAX_RIDER, oFormulaBean)
				+ "";
		serviceTax = Double.parseDouble(serviceTaxValue);

		return serviceTax;
	}
	public long getServiceTaxOnPremium() {
		long serviceTax = 0;
		serviceTax = Math.round(getServiceTax(master.getQuotedModalPremium()-master.getTotalRiderModalPremium()));
		return serviceTax;
	}

	//Start : added by jayesh for Smart7 : 21-06-2014
	public double getServiceTaxForBasePlan(double amount) {
		double serviceTax = 0;
		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setA(amount);
		FormulaHandler formulaHandler = new FormulaHandler();
		String serviceTaxValue = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SERVICE_TAX_BASE_PLAN, oFormulaBean)
				+ "";
		serviceTax = Double.parseDouble(serviceTaxValue);

		return serviceTax;
	}
	public long getServiceTaxForBasePlanPremium() {
		long serviceTax = 0;
		serviceTax = Math.round(getServiceTaxForBasePlan(master.getQuotedModalPremium()-master.getTotalRiderModalPremium()));
		return serviceTax;
	}
	//End : added by jayesh for Smart7 : 21-06-2014


	/*
	 * public double getServiceTaxForUnitLink(double amount){ double
	 * serviceTax=0;
	 *
	 * oFormulaBean = getFormulaBeanObj(); oFormulaBean.setA(amount);
	 * FormulaHandler formulaHandler = new FormulaHandler(); String
	 * serviceTaxValue =
	 * formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(),
	 * FormulaConstant.CALCULATE_SERVICE_TAX, oFormulaBean)+ ""; serviceTax =
	 * Double.parseDouble(serviceTaxValue);
	 *
	 * return serviceTax; }
	 */

	public String getUINNumber() {
		PNLPlanLk oPNLPlanLk = getPlanLKObject(master
				.getRequestBean().getBaseplan());

		if (oPNLPlanLk != null && oPNLPlanLk.getUIN() != null)
			return oPNLPlanLk.getUIN();
		else
			return "";

	}

	public String getPlanFeature() {
		PGFPlanGroupFeature oPGFPlanGroupFeature = DataAccessClass
				.getPlanGroupFeature(master.getRequestBean().getBaseplan());

		if (oPGFPlanGroupFeature != null
				&& oPGFPlanGroupFeature.getPGF_FEATURE() != null)
			return oPGFPlanGroupFeature.getPGF_FEATURE();
		else
			return "";
	}

	private HashMap getSurrenderBenefit() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double TotalGAISum = getTotalGAISum();
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(TotalGAISum);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			if(count==polTerm && !(master.getRequestBean().getBaseplan().startsWith("SR") || master.getRequestBean().getBaseplan().startsWith("SRP"))){
				surrenderBenefit=0;
			}
			SBMap.put(count, surrenderBenefit);
			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
					+ surrenderBenefit);
		}
		return SBMap;
	}
    private HashMap getSurrenderBenefit_GIP() {
        HashMap SBMap = new HashMap();
        long surrenderBenefit;
        int polTerm = master.getRequestBean().getPolicyterm();
        double SumAssuredMaturity = getSumAssuredMatGIP();
        for (int count = 1; count <= polTerm; count++) {
            oFormulaBean = getFormulaBeanObj();
            oFormulaBean.setCNT(count);
            oFormulaBean.setA(SumAssuredMaturity);

            FormulaHandler formulaHandler = new FormulaHandler();
            String surrBenStr = formulaHandler.evaluateFormula(master
                            .getRequestBean().getBaseplan(),
                    FormulaConstant.CALCULATE_SURRENDER_BENEFIT, oFormulaBean)
                    + "";
            surrenderBenefit = Long.parseLong(surrBenStr);
            SBMap.put(count, surrenderBenefit);
            CommonConstant.printLog("d", "Pol Term :::  Surr Benefit getSurrenderBenefit " + count + " : "
                    + surrenderBenefit);
        }
        return SBMap;
    }
	private HashMap getSurrenderBenefit_SIP() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		long maturityValue = 0;
		long totalMaturityValue = 0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			if (count ==polTerm-1) {
				String maturityValueStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_VALUE,
						oFormulaBean)
						+ "";
				maturityValue = Long.parseLong(maturityValueStr);
				totalMaturityValue += maturityValue;
			}
			oFormulaBean.setA(totalMaturityValue);
			String surrBenStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			if(count==polTerm){
				surrenderBenefit=0;
			}
            SBMap.put(count, surrenderBenefit);
			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit getSurrenderBenefit_SIP " + count + " : "
					+ surrenderBenefit);
		}
		return SBMap;
	}
	private HashMap getANNUITY_Val() {
		HashMap AVMap = new HashMap();
		long Annuityval;
		int polTerm = master.getRequestBean().getPolicyterm();
		int mode=0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			if(master.getRequestBean().getFrequency().equals("A"))
				mode = 1;
			else if (master.getRequestBean().getFrequency().equals("S"))
				mode=2;
			else if(master.getRequestBean().getFrequency().equals("Q"))
				mode=4;
			else
				mode=12;
			oFormulaBean.setCNT(mode);
			oFormulaBean.setP(master.getBaseAnnualizedPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String AnnuityvalStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_ANNUITY_VAL, oFormulaBean)
					+ "";
			Annuityval = Long.parseLong(AnnuityvalStr);
            if (count == 1) {
                Annuityval=0;
			}
			AVMap.put(count, Annuityval);
			CommonConstant.printLog("d", "Pol Term :::  Annuityval " + count + " : "
					+ Annuityval);
		}
		return AVMap;
	}
	private HashMap getALTSurrenderBenefit() {
		HashMap ALTSBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double TotalValue=0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			double totAllGAI=getALTTOTALGAIValue(polTerm);
			double totGAI=getALTTOTALGAIValue(count);
			double totMatVal=getALTTOTALMatVal(polTerm);
			TotalValue=totAllGAI+totMatVal-totGAI;
			oFormulaBean.setA(TotalValue);
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_ALTSCE_SURRENDER_BENEFIT, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			if(count==polTerm){
				surrenderBenefit=0;
			}
			ALTSBMap.put(count, surrenderBenefit);
			CommonConstant.printLog("d", "Pol Term ::: ALTSCENARIO CASH Surr Benefit " + count + " : "
					+ surrenderBenefit);
		}
		return ALTSBMap;
	}

	private HashMap getMaturityValue() {
		HashMap MVMap = new HashMap();
		long maturityValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		long GAIVal=0;
	   long GAIValue=0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				GAIVal=calculateGAI(count);
				GAIValue+=GAIVal;
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					oFormulaBean.setA(GAIValue); //Added for Insta Wealth
					FormulaHandler formulaHandler = new FormulaHandler();
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
					MVMap.put(count, maturityValue);
				} else {
					MVMap.put(count, maturityValue);
				}
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating maturity value" + e + " " + e.getMessage());
			// TODO: handle exception
		}
		return MVMap;
	}
	private HashMap getBasicSumAssured() {
		HashMap MVMap = new HashMap();
		long maturityValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		long GAIVal=0;
		long GAIValue=0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				GAIVal=calculateGAI(count);
				GAIValue+=GAIVal;

				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(GAIValue); //Added for Insta Wealth
				FormulaHandler formulaHandler = new FormulaHandler();
				String maturityValueStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_VALUE,
						oFormulaBean)
						+ "";
				maturityValue = Long.parseLong(maturityValueStr);
				MVMap.put(count, maturityValue);

			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating basic sum assured value" + e + " " + e.getMessage());
			// TODO: handle exception
		}
		return MVMap;
	}
	private HashMap getMaturityValueALTSCE() {
		HashMap MVMap = new HashMap();
        long maturityValue = 0;
		double maturityVal = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					if(count<rpuyear){
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALTSCE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityVal = Double.parseDouble((maturityValueStr));
					maturityValue=Math.round(maturityVal);
					MVMap.put(count, maturityValue);
					}else{
						String maturityValueStr = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_ALTSCENARIO_MATURITY_VALUE,
								oFormulaBean)
								+ "";
						maturityVal = Double.parseDouble((maturityValueStr));
						maturityValue=Math.round(maturityVal);
						MVMap.put(count, maturityValue);
					}
				} else {
					MVMap.put(count, 0);
				}
				CommonConstant.printLog("d", "Pol Term ::: AltSCE MATVAL " + count + "" + maturityValue);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating alternate scenario maturity value" + e + " " + e.getMessage());
		}
		return MVMap;
	}
	private FormulaBean getFormulaBeanObj() {
		oFormulaBean.setRequestBean(master.getRequestBean());
		// oFormulaBean.setP(master.getQuotedAnnualizedPremium());
		oFormulaBean.setP(master.getBaseAnnualizedPremium());
		oFormulaBean.setDP(master.getDiscountedBasePrimium());
		oFormulaBean.setQAP(master.getQuotedAnnualizedPremium());
		oFormulaBean.setQMP(master.getTotalModalPremium() + master.getModalPremiumForNSAP());	//added by jayesh for SMART 7 21-05-2014
		oFormulaBean.setSTAXR(master.getSTRiderModalPremium());	//added by jayesh for SMART 7 23-06-2014
		oFormulaBean.setRiderPremium(master.getTotalRiderModalPremium());	//added by jayesh for SMART 7 23-06-2014
		oFormulaBean.setAP(master.getBaseAnnualizedPremium());
		oFormulaBean.setANSAP(master.getBaseAnnualizedPremium()+master.getModalPremiumForAnnNSAP());
		oFormulaBean.setULIPAP(master.getRequestBean().getBasepremiumannual());
		oFormulaBean.setPD(master.getPremiumDiscount());
		oFormulaBean.setSTMain(DataAccessClass.getServiceTaxMain());
		oFormulaBean.setSTFY(DataAccessClass.getServiceTaxFY());
		oFormulaBean.setSTRenewal(DataAccessClass.getServiceTaxRenewal());
		return oFormulaBean;
	}

	public String getPlanDescription() {
		PNLPlanLk oPNLPlanLk = getPlanLKObject(master
				.getRequestBean().getBaseplan());

		if (oPNLPlanLk != null && oPNLPlanLk.getPNL_DESCRIPTION() != null)
			return oPNLPlanLk.getPNL_DESCRIPTION();
		else
			return "";

	}

	public int getRiderPolTerm(RiderBO oRiderBO) {
		int pt = 0;
		PFRPlanFormulaRef formulaObj = DataAccessClass.getformula(master
                .getRequestBean().getBaseplan(), oRiderBO.getCode() + "_PT");
		if (formulaObj != null) {
			oFormulaBean = getFormulaBeanObj();
			FormulaHandler formulaHandler = new FormulaHandler();
			HashMap formulaExpMap = formulaHandler.getFormulaExp(formulaObj
					.getPFR_FRM_CD());
			if (oRiderBO.getCode().equals("WOPPULV1") || oRiderBO.getCode().equals("WPPN1V1")) {
				oFormulaBean.setA(master.getRequestBean().getProposer_age());
			} else {
				oFormulaBean.setA(master.getRequestBean().getInsured_age());
			}
			String PT = formulaHandler.executeFormula(formulaExpMap,
					oFormulaBean)
					+ "";
			pt = Integer.parseInt(PT);
		}
		return pt;
	}

	public int getRiderPremPayTerm(RiderBO oRiderBO) {
		int pt = 0;
		int ridTerm = 0;
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int insuredAge  = master.getRequestBean().getInsured_age();
		int planMatVal = insuredAge + ppt;

		PFRPlanFormulaRef formulaObj = DataAccessClass.getformula(master
				.getRequestBean().getBaseplan(), oRiderBO.getCode() + "_PPT");
		if (formulaObj != null) {
			oFormulaBean = getFormulaBeanObj();
			FormulaHandler formulaHandler = new FormulaHandler();
			HashMap formulaExpMap = formulaHandler.getFormulaExp(formulaObj
					.getPFR_FRM_CD());
			String PT = formulaHandler.executeFormula(formulaExpMap,
                    oFormulaBean)
                    + "";
			ridTerm = Integer.parseInt(PT);
			if(oRiderBO.getCode().equals("ADDLN1V1") && (planMatVal > 70))
			{
				int riderCovTerm = planMatVal - 70;
				pt = ppt - riderCovTerm;
			}
			else{
				pt = ridTerm;

			}
		}

		return pt;
	}

	public long getTotalModalPremium() {
		long riderPremTotal = 0;
		ArrayList aList = master.getRiderData();
		if(aList != null) { // condition added by Akhil on 03-07-2015 14:08:00 as riders currently not available also corresponding tables not available.
			for (int i = 0; i < aList.size(); i++) {
				RiderBO oRiderBO = (RiderBO) aList.get(i);
				riderPremTotal = riderPremTotal + oRiderBO.getModalpremium();
			}
		}
		return master.getModalPremium() + riderPremTotal;
	}
	public long getTotalRenewalModalPremium() {
		long riderPremTotal = 0;
		ArrayList aList = master.getRiderData();
		for (int i = 0; i < aList.size(); i++) {
			RiderBO oRiderBO = (RiderBO) aList.get(i);
			riderPremTotal = riderPremTotal + oRiderBO.getModalpremium();
		}
		return master.getModalPremiumRenewal() + riderPremTotal;
	}
	public long getRenewalModalPremium_A() {
		long renewalAnnual=0;
		long renewalNSAP = 0;
		renewalAnnual = master.getBaseAnnualizedPremium();
		if (master.getModalPremiumForNSAP()!=0) {
			//if (master.getRequestBean().getFrequency().equals("A")) {
			renewalNSAP = master.getModalPremiumForNSAP();
			/*}
			if (master.getRequestBean().getFrequency().equals("S")) {
				renewalNSAP = Math.round(master.getModalPremiumForNSAP() * 0.51);
			}
			if (master.getRequestBean().getFrequency().equals("Q")) {
				renewalNSAP = Math.round(master.getModalPremiumForNSAP() * 0.26);
			}
			if (master.getRequestBean().getFrequency().equals("M")) {
				renewalNSAP = Math.round(master.getModalPremiumForNSAP() * 0.0883);
			}*/
		}
		//if(master.getRequestBean().getBaseplan().equals("FGV1N1"))
		renewalNSAP = master.getModalPremiumForAnnNSAP()!=0?master.getModalPremiumForAnnNSAP():0;
		if (master.getTotAnnRiderModalPremium()!=0) {
			CommonConstant.printLog("d", "RIDER PREM ANNUAL " + master.getTotAnnRiderModalPremium());
			renewalAnnual += master.getTotAnnRiderModalPremium();
		}
		renewalAnnual += renewalNSAP;
		CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : RENEWAL PREM ANNUAL " + renewalAnnual);
		return renewalAnnual;
	}

	public long getTotalRiderModePremium() {
		long riderPremTotal = 0;
		ArrayList aList = master.getRiderData();
		if(aList!=null) { // condition added by Akhil on 03-07-2015 14:05:00 as riders currently not available also corresponding tables not available.
			for (int i = 0; i < aList.size(); i++) {
				RiderBO oRiderBO = (RiderBO) aList.get(i);
				riderPremTotal = riderPremTotal + oRiderBO.getModalpremium();
			}
		}

		return riderPremTotal;
	}

	public HashMap getTotalRiderAnnualPremium() {
		HashMap hmRiderPrem = new HashMap();
		int pt = master.getRequestBean().getPolicyterm();
		int insuredAge = master.getRequestBean().getInsured_age();
		long riderPremTotal = 0;
		ArrayList aList = master.getRiderData();
		for (int i = 1; i <= pt; i++) {
			if(aList!=null){
				for (int j = 0; j < aList.size(); j++) {
					RiderBO oRiderBO = (RiderBO) aList.get(j);
					if(oRiderBO.getCode().equalsIgnoreCase("WOPSM7V1") && i > (65 - master.getRequestBean().getInsured_age())){
					}else if(oRiderBO.getCode().equalsIgnoreCase("WOPPSM7V1") && i > (65 - master.getRequestBean().getProposer_age())){
					}else if(oRiderBO.getCode().equalsIgnoreCase("ADDLN1V1") && (insuredAge > 69)){
					}
					else{
						riderPremTotal = riderPremTotal + oRiderBO.getPrimium();
					}
				}
			}
			hmRiderPrem.put(i, riderPremTotal);
			riderPremTotal=0;
		}
		return hmRiderPrem;
	}

	public HashMap getTotalPremium_Annual() {
		long riderPremTotal = 0;
		int pt = master.getRequestBean().getPolicyterm();
		String mode = master.getRequestBean().getFrequency();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int insuredAge = master.getRequestBean().getInsured_age();
		String planType = DataAccessClass.getPlanType(master.getRequestBean()
				.getBaseplan());
		HashMap hmTotalPrem = new HashMap();
		ArrayList aList = master.getRiderData();

		for (int i = 1; i <= pt; i++) {
			if(aList != null){
				for (int j = 0; j < aList.size(); j++) {
					RiderBO oRiderBO = (RiderBO) aList.get(j);
					if(oRiderBO.getCode().equalsIgnoreCase("WOPSM7V1") && i > (65 - master.getRequestBean().getInsured_age())){
					}else if(oRiderBO.getCode().equalsIgnoreCase("WOPPSM7V1") && i > (65 - master.getRequestBean().getProposer_age())){
					}else if(oRiderBO.getCode().equalsIgnoreCase("ADDLN1V1") && (insuredAge > 69)){
					}else{
							riderPremTotal = riderPremTotal + oRiderBO.getPrimium();
					}
					insuredAge++;
				}
			}
			if (("O".equals(mode) && i > 1) || (i > ppt)) {
				hmTotalPrem.put(i, 0);
			} else {
				if (("C".equals(planType))
						&& ("Y".equals(master.getRequestBean()
								.getTata_employee())) && (i == 1)
						//&& !"O".equals(mode)) {
				        ) {
					// discounted premium
					hmTotalPrem.put(i, master.getDiscountedBasePrimium()
							+ riderPremTotal);
				} else {
					hmTotalPrem.put(i, master.getQuotedAnnualizedPremium()
							+ riderPremTotal);
				}
			}
			riderPremTotal=0;	//added by jayesh
		}

		return hmTotalPrem;
	}
	public void calculateAlternateChargesAAAPH10() {//Added by Samina for Alternate Scenrio AAA
		CommonConstant.printLog("d", "Inside calculateChargesAAA PH10");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		HashMap hmTotalFundValueAtEndPolicyYearLCEF_PH10 = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYearWLEF_PH10 = new HashMap();
		HashMap hmSurrenderValuePH10_AS3 = new HashMap();
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		} else if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}		double tempAdminChargeSTax = 0.0;
		long lMortalityChargesAS3 = 0;
		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;
		double mortChargesSTaxAS3 = 0.0;
		double tempNetHolderAccAS3=0.0;
		double templMortalityChargesAS3 = 0.0;
		double tempFulllMortalityChargesAS3 = 0.0;
		double tempNetHolderInvestmentAS3 = 0.0;
		double tempPHAccBeforeGrowthAS3 = 0.0;
		double tempPHAccInvestGrowthAS3=0.0;
		double tempPHAccHolderAS3=0.0;
		double tempPhInvestmentFeeAS3=0.0;
		double phInvestFeeSTaxAS3=0.0;
		double tempPhNetHolderAccAS3=0.0;
		double tempFinalPhNetHolderAccAS3=0.0;
		double fundGrowthPM = 0.00643403011;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyChargesAS3=0.0;
		double RiderCharges=0.0;
		double finalLoyaltyAS3 = 0.0;
		double finalDebtLoyaltyAS3 = 0.0;
		double finalEquityLoyaltyAS3 = 0.0;
		double quarterForAAA = 10.0;
		double FMCFactor = 0.0;
		double beforeSwitchEquity = 0.0;
		double beforeSwitchDebt = 0.0;
		double afterSwitchEquity = 0.0;
		double afterSwitchDebt = 0.0;
		double mainPartialWithdrawal = 0.0;
		double initDebtFundFactor = 0.0;
		double initEquityFundFactor = 0.0;
		HashMap hmWithdrawalAmountAAA = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDoubleAAA = new HashMap<Integer, Double>();
		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTaxAS3 = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			PAC = Double.parseDouble(premAllocCharge);
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTaxAS3 = totalServiceTaxAS3 + premiumAllocChargesSTax;
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String strCommissionPayble = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
							+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTaxAS3 = totalServiceTaxAS3 + (otherBenefitSTax * 12);
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTaxAS3 = totalServiceTaxAS3 + (adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			double serviceTaxOnInvestmentFee = 0.0;
			tempFulllMortalityChargesAS3=0.0;
			double fullPartialWithdrawal = 0.0;
			double completePartialWithdrawal = 0.0;
			double partialWithdrawal=0.0;
			for(int i=1;i<=12;i++) {
				finalLoyaltyAS3=0.0;
				finalDebtLoyaltyAS3=0.0;
				finalEquityLoyaltyAS3=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
							|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && (i!=1))){
					partialWithdrawal = 0.0;
				} else {
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
									/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;*/
							fullPartialWithdrawal+=partialWithdrawal;
							completePartialWithdrawal+=partialWithdrawal;
						} else {
							partialWithdrawal = 0.0;
						}
					} else if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							//CommonConstant.printLog("d","withdrawVar of "+l+" is "+withdrawVar[l]);
							if (withdrawVar[l].contains(count+",")) {
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							} else {
								partialWithdrawal = 0.0;
							}
						}
					}
			}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);
				oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAccAS3);
				oFormulaBean.setCNT(count);
				oFormulaBean.setWithdrawalAmount(mainPartialWithdrawal);
				oFormulaBean.setMonthCount(i);
				String MortalityChargesAS3 = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityChargesAS3 = Double.parseDouble(MortalityChargesAS3);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges AS3: "
						+ templMortalityChargesAS3);
				tempFulllMortalityChargesAS3 += templMortalityChargesAS3;
				RiderCharges = getRiderCharges(count,age, ppt, i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);
			mortChargesSTaxAS3 = getServiceTax(templMortalityChargesAS3+RiderCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax AS3: "
					+ mortChargesSTaxAS3 + " and full Mortc AS3 " + tempFulllMortalityChargesAS3);
			lMortalityChargesAS3 = Math.round(tempFulllMortalityChargesAS3);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges AS3: "
					+ lMortalityChargesAS3);
			hmMortalityCharges.put(count, lMortalityChargesAS3);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
			totalServiceTaxAS3 = totalServiceTaxAS3 + mortChargesSTaxAS3;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
			tempNetHolderInvestmentAS3 = tempHoldetNettoInvest - templMortalityChargesAS3 - mortChargesSTaxAS3 - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges AS3: "
					+ tempNetHolderInvestmentAS3);
			tempPHAccBeforeGrowthAS3 = tempNetHolderAccAS3 + tempNetHolderInvestmentAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges AS3: "
					+ tempPHAccBeforeGrowthAS3);
			oFormulaBean.setA(0.1);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges AS3: "
					+ fundGrowthPM);
			tempPHAccInvestGrowthAS3=(tempPHAccBeforeGrowthAS3*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges AS3: "
					+ tempPHAccInvestGrowthAS3);
			tempPHAccHolderAS3=tempPHAccBeforeGrowthAS3+tempPHAccInvestGrowthAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges AS3: "
					+ tempPHAccHolderAS3);
			double DebtFundBeforeFMC=0.0;
			double EquityFundBeforeFMC=0.0;
			if (count*12+i-12 > polTerm*12-30) {
				if (quarterForAAA==10.0) {
					beforeSwitchEquity = DataAccessClass.getAAAEFV(master.getRequestBean(), polTerm - 2);
					beforeSwitchDebt = 100-beforeSwitchEquity;
					//CommonConstant.printLog("d",beforeSwitchEquity * (1.0 / quarterForAAA));
					afterSwitchEquity = (beforeSwitchEquity - (beforeSwitchEquity * (1.0 / quarterForAAA)));
					afterSwitchDebt = (beforeSwitchDebt + beforeSwitchEquity -  afterSwitchEquity);
				} else if (i==1 || i == 4 || i == 7 || i == 10) {
					beforeSwitchEquity = afterSwitchEquity;
					beforeSwitchDebt = afterSwitchDebt;
					afterSwitchEquity = beforeSwitchEquity - (beforeSwitchEquity * (1.0 / quarterForAAA));
					afterSwitchDebt = beforeSwitchDebt + (beforeSwitchEquity -  afterSwitchEquity);
				}
				FMCFactor = (afterSwitchEquity * 0.012) + (afterSwitchDebt * 0.008);
				tempPhInvestmentFeeAS3 = tempPHAccHolderAS3 * FMCFactor /1200;
				DebtFundBeforeFMC = tempPHAccHolderAS3 * afterSwitchDebt / 100;
				EquityFundBeforeFMC = tempPHAccHolderAS3 * afterSwitchEquity / 100;
				if (i==3 || i == 6 || i == 9 || i == 12)
					quarterForAAA--;
			} else {
				oFormulaBean.setA(tempPHAccHolderAS3);
				String tempPhInvestmentFeeValueAS3 = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_PH_INVESTMENT_FEE_AAA,
						oFormulaBean)
						+ "";
				tempPhInvestmentFeeAS3 = Double.parseDouble(tempPhInvestmentFeeValueAS3);
				oFormulaBean.setA(tempPHAccHolderAS3);
				String DBFMC = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.DEBT_FUND_BEFORE_FMC,
						oFormulaBean)
						+ "";
				DebtFundBeforeFMC=Double.parseDouble(DBFMC);
				String EBFMC = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.EQUITY_FUND_BEFORE_FMC,
						oFormulaBean)
						+ "";
				EquityFundBeforeFMC =  Double.parseDouble(EBFMC);
			}
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #beforeSwitchEquity: " + beforeSwitchEquity);

			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #beforeSwitchDebt: " + beforeSwitchDebt);

			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #afterSwitchEquity: " + afterSwitchEquity);

			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #afterSwitchDebt: " + afterSwitchDebt);


			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges AS3: "
					+ tempPhInvestmentFeeAS3);
			phInvestFeeSTaxAS3 = getServiceTax(tempPHAccHolderAS3*0.0135/12);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges AS3: "
					+ phInvestFeeSTaxAS3);
			totalServiceTaxAS3 = totalServiceTaxAS3 + phInvestFeeSTaxAS3;
			tempPhNetHolderAccAS3=tempPHAccHolderAS3-tempPhInvestmentFeeAS3-phInvestFeeSTaxAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges AS3: "
					+ tempPhNetHolderAccAS3);
			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolderAS3;
			phInvestmentFee = tempPhInvestmentFeeAS3;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);
			InvestmentFeeSTax = phInvestFeeSTaxAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);
			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;

			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #DebtFundBeforeFMC: " + DebtFundBeforeFMC);
			double  phInvestmentFeeLCEF=DebtFundBeforeFMC*0.8/1200;
			double InvestmentFeeSTaxLCEF=getServiceTax(DebtFundBeforeFMC*0.0135/12);
			double DebtFundAfterFMC=DebtFundBeforeFMC - phInvestmentFeeLCEF - InvestmentFeeSTaxLCEF;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #DebtFundAfterFMC: " + DebtFundAfterFMC);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #EquityFundBeforeFMC: " + EquityFundBeforeFMC);
			double NetPHHolderDebtAccount=DebtFundAfterFMC- (partialWithdrawal* DataAccessClass.getAAADFV(master.getRequestBean(), count));
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #NetPHHolderDebtAccount: " + NetPHHolderDebtAccount);
			double  phInvestmentFeeWLEF=EquityFundBeforeFMC*1.2/1200;
			double InvestmentFeeSTaxWLEF=getServiceTax(EquityFundBeforeFMC*0.0135/12);
			double EquityFundAfterFMC=EquityFundBeforeFMC - phInvestmentFeeWLEF - InvestmentFeeSTaxWLEF;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #EquityFundAfterFMC: " + EquityFundAfterFMC);
			double NetPHHolderEquityAccount=EquityFundAfterFMC-(partialWithdrawal* DataAccessClass.getAAAEFV(master.getRequestBean(), count));
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #NetPHHolderEquityAccount: " + NetPHHolderEquityAccount);


			if (i == 12) {
						hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					RiderCharges=0;
					 oFormulaBean.setA(tempPhNetHolderAccAS3);
						String tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
					 finalLoyaltyAS3=loyaltyChargesAS3;
					 oFormulaBean.setA(DebtFundAfterFMC);
						tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
						finalDebtLoyaltyAS3=loyaltyChargesAS3;
						NetPHHolderDebtAccount=DebtFundAfterFMC+finalDebtLoyaltyAS3- (partialWithdrawal* DataAccessClass.getAAADFV(master.getRequestBean(), count));

						oFormulaBean.setA(EquityFundAfterFMC);
						tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
						finalEquityLoyaltyAS3=loyaltyChargesAS3;
						NetPHHolderEquityAccount=EquityFundAfterFMC+finalEquityLoyaltyAS3-(partialWithdrawal* DataAccessClass.getAAAEFV(master.getRequestBean(), count));
					hmLoyaltyCharge.put(count,Math.round(finalLoyaltyAS3));
				hmRegularFundValue.put(count, Math.round(netPHAccount));
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));
				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);
				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);
				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAccAS3);
            	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges AS3: "
						+ tempFinalPhNetHolderAccAS3);
            	hmTotalFundValueAtEndPolicyYearLCEF_PH10.put(count, Math.round(NetPHHolderDebtAccount));
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetPHHolderDebtAccount Charges AS3: "
						+ NetPHHolderDebtAccount);
            	hmTotalFundValueAtEndPolicyYearWLEF_PH10.put(count, Math.round(NetPHHolderEquityAccount));
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetPHHolderEquityAccount Charges AS3: "
						+ NetPHHolderEquityAccount);
				}
			if (i==1) {
				CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
				hmWithdrawalAmountAAA.put(count, Math.round(fullPartialWithdrawal));
				hmWithdrawalAmountDoubleAAA.put(count,fullPartialWithdrawal);
				mainPartialWithdrawal=0.0;
				if (master.getRequestBean().getInsured_age()+count-1 >=60) {
					for (int k=58-master.getRequestBean().getInsured_age()+1;k<=master.getRequestBean().getInsured_age()+count-1;k++) {
						if (hmWithdrawalAmountAAA.get(k)!=null) {
							//CommonConstant.printLog("d","For Year "+k+" Withdrawal is "+hmWithdrawalAmountDoubleAAA.get(k));
							mainPartialWithdrawal+=hmWithdrawalAmountDoubleAAA.get(k);
						}
					}
				} else {
					for (int k=count-1;k<=count;k++) {
						if (hmWithdrawalAmountAAA.get(k)!=null && k>=1) {
							//CommonConstant.printLog("d","For Year "+k+" Withdrawal is "+hmWithdrawalAmountDoubleAAA.get(k));
							mainPartialWithdrawal += hmWithdrawalAmountDoubleAAA.get(k);
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
						+ mainPartialWithdrawal);
			}
                tempFinalPhNetHolderAccAS3=tempPhNetHolderAccAS3+finalLoyaltyAS3-partialWithdrawal;
                CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges AS3: "
						+ tempFinalPhNetHolderAccAS3);
				tempNetHolderAccAS3=tempFinalPhNetHolderAccAS3;
				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAccAS3);
			double surrendercharges=getSurrenderValuePH10_AS3(count, tempFinalPhNetHolderAccAS3);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmSurrenderValueAS3" + surrendercharges);
			double totsurcharge_AS3=tempFinalPhNetHolderAccAS3-surrendercharges;
			hmSurrenderValuePH10_AS3.put(count,Math.round(totsurcharge_AS3));
			}
			age++;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityChargesAS3);
			CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
					+ totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityChargesAS3);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));
			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			ApplicableServiceTax = Math.round(totalServiceTaxAS3);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);
			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(partialWithdrawal);
				String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AAA_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		       double lDBEP = Double.parseDouble(strDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benefit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benefit At EndOf Policy Year: " + lDBEP);
			if (count==1) {
				initDebtFundFactor = DataAccessClass.getAAADFV(master.getRequestBean(), 1);
				initEquityFundFactor = DataAccessClass.getAAAEFV(master.getRequestBean(), 1);
			}
		}
		master.setMortalityCharges_AS3(hmMortalityCharges);
		master.setApplicableServiceTaxPH10_AS3(hmApplicableServiceTax);
		master.setTotalchargeforPH10_AS3(hmtotalcharge);
		master.setLoyaltyChargeforPH10_AS3(hmLoyaltyCharge);
		master.setFundManagmentChargePH10_AS3(hmFundManagementCharge);
		master.setRegularFundValuePH10_AS3(hmTotalFundValueAtEndPolicyYear);
		master.setRegularFundValueBeforeFMCPH10_AS3(hmRegularFundValueBeforeFMC);
		master.setRegularFundValuePostFMCPH10_AS3(hmRegularFundValuePostFMC);
		master.setGuaranteedMaturityAdditionPH10_AS3(hmGuaranteedMaturityAmount);
		master.setTotalDeathBenefitPH10_AS3(hmDeathBenefitAtEndofPolicyYear);
		master.setSurrenderValuePH10_AS3(hmSurrenderValuePH10_AS3);
		master.setRegularFundValueAAA_LCEF_PH10(hmTotalFundValueAtEndPolicyYearLCEF_PH10);
		master.setRegularFundValueAAA_WLEF_PH10(hmTotalFundValueAtEndPolicyYearWLEF_PH10);
		master.setInitDebtFundFactor(initDebtFundFactor);
		master.setInitEquityFundFactor(initEquityFundFactor);
	}

	public void calculateChargesForPH10() {

		CommonConstant.printLog("d", "Inside calculateCharges");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitCharges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmSurrenderValue = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge

		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		long lMortalityCharges = 0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;

		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double mortChargesSTax = 0.0;
		double tempNetHolderAcc=0.0;
		double templMortalityCharges = 0.0;
		double tempFulllMortalityCharges = 0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;

		double tempPhInvestmentFee=0.0;
		double phInvestFeeSTax=0.0;
		double tempPhNetHolderAcc=0.0;
		double tempFinalPhNetHolderAcc=0.0;
		double tempAdminChargeSTax = 0.0;
		double fundGrowthPM = 0.00643403011;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTax = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;
			double RiderCharges=0.0;
			double loyaltyCharges=0.0;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble(premAllocCharge);
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			// Calculate Service Tax on Premium Allocation Charges
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTax = totalServiceTax + premiumAllocChargesSTax; // Yearly
																			// Tax
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
					+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			CommonConstant.printLog("d", "For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			// Premium Allocation / Investment Amount Charges Calculation end
			// Other Inbuilt Benefit charges start
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);


			// Other Inbuilt Benefit Monthly charges
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			// Calculate Service Tax on Other Benefit Charges
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTax = totalServiceTax + (otherBenefitSTax * 12); // Yearly
																			// Tax
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			// END
			// Other Inbuilt Benefit charges End
			// Mortility Charges calculation Start
			// Policy Admin Charges calculation Start
			//if (count == 1) {
				//dAdminCharge = DataAccessClass.getADMCharges(pnl_id, master
				//		.getQuotedAnnualizedPremium());
				//lAdminCharge = Math.round(dAdminCharge * 12);
				//hmPolicyAdminCharges.put(1, lAdminCharge);
			//} else {
				//oFormulaBean.setADM(dAdminCharge);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			//}

			// Calculate Service Tax on Admin Charges
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTax = totalServiceTax + (adminChargeSTax * 12); // Yearly
																		// Tax

			CommonConstant.printLog("d", "#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			// END

			double serviceTaxOnInvestmentFee = 0.0;
			lMortalityCharges = 0;
			tempFulllMortalityCharges=0.0;
			//Addedd by Samina Mohammad iFlex
			for(int i=1;i<=12;i++) {
				//double tempHoldetNettoInvest = investmentAmount - premiumAllocChargesSTax;
				double finalLoyalty=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}
			      oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAcc);
			oFormulaBean.setCNT(count);
			oFormulaBean.setMonthCount(i);
				oFormulaBean.setAP(master.getRequestBean().getBasepremiumannual());
			String MortalityCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
					+ "";
			templMortalityCharges = Double.parseDouble(MortalityCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges: "
					+ templMortalityCharges);
			tempFulllMortalityCharges += templMortalityCharges;
				RiderCharges = getRiderCharges(count,age,ppt,i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);


			mortChargesSTax = getServiceTax(templMortalityCharges+RiderCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax: "
					+ mortChargesSTax + " and full Mortc " + tempFulllMortalityCharges);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
				lMortalityCharges = Math.round(tempFulllMortalityCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges: "
						+ lMortalityCharges);
				hmMortalityCharges.put(count, lMortalityCharges);
			totalServiceTax = totalServiceTax + mortChargesSTax;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
				tempNetHolderInvestment = tempHoldetNettoInvest - templMortalityCharges - mortChargesSTax - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: "
					+ tempNetHolderInvestment);
			tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
			//totalNetHolderInvestment+=tempNetHolderInvestment;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: "
					+ tempPHAccBeforeGrowth);
			oFormulaBean.setA(0.1);

			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
					+ fundGrowthPM);

				tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: "
					+ tempPHAccInvestGrowth);
			tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: "
					+ tempPHAccHolder);
			oFormulaBean.setA(tempPHAccHolder);
			String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
					oFormulaBean)
					+ "";
			tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
					+ tempPhInvestmentFee);
			phInvestFeeSTax = getServiceTax(tempPhInvestmentFee);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges: "
					+ phInvestFeeSTax);

			totalServiceTax = totalServiceTax + phInvestFeeSTax;
			tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee-phInvestFeeSTax;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
					+ tempPhNetHolderAcc);




			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolder;
			phInvestmentFee = tempPhInvestmentFee;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);

			// Calculate Service Tax on phInvestmentFee
			// InvestmentFeeSTax =
			// getServiceTaxForUnitLink(phInvestmentFee);

			InvestmentFeeSTax = phInvestFeeSTax;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);

			// CommonConstant.printLog("d","#Policy term : "+count+" #serviceTaxOnInvestmentFee : "
			// + serviceTaxOnInvestmentFee);

			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;

			// Storing regular fund values
			if (i == 12) {
						hmOtherBenifitCharges.put(count, Math.round(totRiderCharges));
										oFormulaBean.setA(tempPhNetHolderAcc);
						String tempLoyaltyCharges = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
					 finalLoyalty=loyaltyCharges;
					 hmLoyaltyCharge.put(count,Math.round(finalLoyalty));
				hmRegularFundValue.put(count, Math.round(netPHAccount));
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));

				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);

				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);

				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);

				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);

				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAcc);
            	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);

				}
                tempFinalPhNetHolderAcc=tempPhNetHolderAcc+finalLoyalty;

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);
				tempNetHolderAcc=tempFinalPhNetHolderAcc;
				totalFundValueAtEndtPolicyYear = Math
				.round(tempNetHolderAcc);


				long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmSurrenderValue" + surrenderValue);
			// END

				hmSurrenderValue.put(count, surrenderValue);





			}

			//End iFlex

			// Mortility Charges Monthly start
			age++;

			//Added by Samina Mohammad 02-04-2014 for iFlexi
			//netPHInvestment = totalNetHolderInvestment;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
					+ totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));

			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			//Commented by Mohammad Rashid 02-04-2014 for iFlexi

			//totalServiceTax = totalServiceTax + serviceTaxOnInvestmentFee;
			ApplicableServiceTax = Math.round(totalServiceTax);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);

			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			// Calculate Death benefit at the end of policy year
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);


			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setAP(master.getRequestBean().getBasepremiumannual());
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
			String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		double lDBEP = Double.parseDouble(strDBEP);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benefit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			// END
			// Calculate Commission payble


			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benefit At EndOf Policy Year: " + lDBEP);

			// END

		}
		master.setAmountForInvestment(hmInvestmentAmount);
		master.setPremiumAllocationCharges(hmPremiumAllocation);
		master.setOtherBenefitCharges(hmOtherBenifitCharges);
		master.setMortalityCharges(hmMortalityCharges);
		master.setPolicyAdminCharges(hmPolicyAdminCharges);
		master.setApplicableServiceTaxPH10(hmApplicableServiceTax);
		master.setTotalchargeforPH10(hmtotalcharge);
		master.setLoyaltyChargeforPH10(hmLoyaltyCharge);
		master.setFundManagmentChargePH10(hmFundManagementCharge);
		master.setRegularFundValuePH10(hmRegularFundValue);
		master.setRegularFundValueBeforeFMCPH10(hmRegularFundValueBeforeFMC);
		master.setRegularFundValuePostFMCPH10(hmRegularFundValuePostFMC);
		master.setGuaranteedMaturityAdditionPH10(hmGuaranteedMaturityAmount);

		master.setTotalFundValueAtEndPolicyYearPH10(hmTotalFundValueAtEndPolicyYear);
		master.setSurrenderValuePH10(hmSurrenderValue);
		master.setDeathBenifitAtEndOfPolicyYear(hmDeathBenefitAtEndofPolicyYear);
		master.setCommissionPayble(hmCommissionPayble);

	}

	public long getSurrenderValue(int term, double netPHAccount) {
		long lSurrenderValue = 0;
		double dSurrenderValue = 0;
		double dSerTax_SurCharge = 0;
		double dCalculatedSurrValue = 0;

		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setNPHA(netPHAccount);
		oFormulaBean.setCNT(term);
		FormulaHandler formulaHandler = new FormulaHandler();
		String strSurVal = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SURRENDER_CHARGE, oFormulaBean)
				+ "";
		dSurrenderValue = Double.parseDouble(strSurVal);

		CommonConstant.printLog("d", "getSurrenderValue #Policy term : " + term
				+ " #Surrrender Value	: " + dSurrenderValue);
		dSerTax_SurCharge = getServiceTax(dSurrenderValue);

		// Calculate Service Tax on phAccountValue
		CommonConstant.printLog("d", "getSurrenderValue #Policy term : " + term
				+ " #serviceTaxOnSurrcharge : " + dSerTax_SurCharge);
		// }//END

		// calculate surrender value
		/*dCalculatedSurrValue = netPHAccount - dSurrenderValue
				- dSerTax_SurCharge;*/
		dCalculatedSurrValue = netPHAccount - dSurrenderValue;
		lSurrenderValue = Math.round(dCalculatedSurrValue);

		return lSurrenderValue;
	}
	/*public long getSurrenderValueWithoutST(int term, double netPHAccount) {
		long lSurrenderValue = 0;
		double dSurrenderValue = 0;
		double dSerTax_SurCharge = 0;
		double dCalculatedSurrValue = 0;

		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setNPHA(netPHAccount);
		oFormulaBean.setCNT(term);
		FormulaHandler formulaHandler = new FormulaHandler();
		String strSurVal = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SURRENDER_CHARGE_ST, oFormulaBean)
				+ "";
		dSurrenderValue = Double.parseDouble(strSurVal);

		logger.debug("getSurrenderValue #Policy term : " + term
				+ " #Surrrender Value	: " + dSurrenderValue);
		dSerTax_SurCharge = getServiceTax(dSurrenderValue);

		// Calculate Service Tax on phAccountValue
		logger.debug("getSurrenderValue #Policy term : " + term
				+ " #serviceTaxOnSurrcharge : " + dSerTax_SurCharge);
		// }//END

		// calculate surrender value
		dCalculatedSurrValue = netPHAccount - dSurrenderValue;
		lSurrenderValue = Math.round(dCalculatedSurrValue);

		return lSurrenderValue;
	}*/
	public double getSurrenderValuePH10_AS3(int term, double netPHAccount) {
		double dSurrenderValue = 0;

		oFormulaBean = getFormulaBeanObj();
		oFormulaBean.setNPHA(netPHAccount);
		oFormulaBean.setCNT(term);
		FormulaHandler formulaHandler = new FormulaHandler();
		String strSurVal = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.CALCULATE_SURRENDER_CHARGE_AS3, oFormulaBean)
				+ "";
		dSurrenderValue = Double.parseDouble(strSurVal);
		return dSurrenderValue;
	}
	public long getGuaranteedMaturityAddition(int term, double netPHAccount) {

		long lGurMatAdd = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		double dGurMaturityAddition = 0;

		if (term == polTerm) {

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(netPHAccount);// NetPH Account value
			oFormulaBean.setCNT(term);
			FormulaHandler formulaHandler = new FormulaHandler();
			String strGurMaturityAddition = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_GUARANTEED_MATURITY_ADDITION,
					oFormulaBean)
					+ "";
			lGurMatAdd = Long.parseLong(strGurMaturityAddition);
			CommonConstant.printLog("d", "getGuaranteedMaturityAddition If #Policy term : "
					+ term + " #Guaranteed Maturity Addition : "
					+ dGurMaturityAddition);

		} else {
			lGurMatAdd = 0;
			CommonConstant.printLog("d", "getGuaranteedMaturityAddition Else #Policy term : "
					+ term + " #Guaranteed Maturity Addition : " + lGurMatAdd);
		}

		return lGurMatAdd;
	}

	public void calculateChargesForPH6() {

		CommonConstant.printLog("d", "Inside calculateCharges for PH6");
			double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmSurrenderValue = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		//String Baseplan=master.getRequestBean().getBaseplan();
		//CommonConstant.printLog("d","----Policy Option"+Baseplan);
		long lMortalityCharges = 0;
		double lAdminChargetot = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;

		String frequency=master.getRequestBean().getFrequency();
		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;

		double mortChargesSTax = 0.0;
		double tempNetHolderAcc=0.0;
		double templMortalityCharges = 0.0;
		double tempFulllMortalityCharges = 0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;
		double tempPhInvestmentFee=0.0;
		double phInvestFeeSTax=0.0;
		double tempPhNetHolderAcc=0.0;
		double tempAdminChargeSTax = 0.0;
		double fundGrowthPM = 0.00327373978220;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyCharges=0.0;
		double tempFinalPhNetHolderAcc=0.0;


	double totRiderCharges=0.0;
		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTax = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
				long regularFundValuePostFMC = 0;
				 double RiderCharges=0.0;
				 String TotalInvestment=null;

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH6  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
				hmPremiumAllocation.put(count, Math.round(PAC));
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setA(PAC);
			//hmInvestmentAmount.put(count, investmentAmount);
				if(count<=ppt)
				{
				 TotalInvestment=formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
						oFormulaBean)
						+ "";
				}
				else{
					 TotalInvestment="0";
				}
			// Calculate Service Tax on Premium Allocation Charges
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTax = totalServiceTax + premiumAllocChargesSTax; // Yearly
																			// Tax
				CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
						+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
				double investmentAmount=Double.parseDouble(TotalInvestment);
			// Premium Allocation / Investment Amount Charges Calculation end
			// Other Inbuilt Benefit charges start
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH6 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);
			hmOtherBenifitChrges.put(count, iOtherBenifitChrges);

			// Other Inbuilt Benefit Monthly charges
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH6 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);

			// Calculate Service Tax on Other Benefit Charges
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTax = totalServiceTax + (otherBenefitSTax * 12); // Yearly
																			// Tax
			CommonConstant.printLog("d", "#PH6 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			// END

			// Policy Admin Charges calculation Start
				/*if (count == 1) {
					dAdminCharge = DataAccessClass.getADMCharges(pnl_id, master
							.getQuotedAnnualizedPremium());
					lAdminCharge = Math.round(dAdminCharge * 12);
					hmPolicyAdminCharges.put(1, lAdminCharge);
				} else {
					oFormulaBean.setADM(dAdminCharge);
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = Math.round(dAdminCharge * 12);
				CommonConstant.printLog("d","#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d","#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(1, lAdminCharge);
			}

			// Calculate Service Tax on Admin Charges
				adminChargeSTax = getServiceTax(dAdminCharge);
				totalServiceTax = totalServiceTax + (adminChargeSTax * 12); // Yearly
																		// Tax

			CommonConstant.printLog("d","#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax*12);
			CommonConstant.printLog("d","#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);*/
			// END
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminChargetot=dAdminCharge * 12;
				//hmPolicyAdminCharges.put(1, lAdminCharge);
				adminChargeSTax = getServiceTax(dAdminCharge);
				totalServiceTax = totalServiceTax + (adminChargeSTax * 12); // Yearly
				double serviceTaxOnInvestmentFee = 0.0;
				lMortalityCharges = 0;
				tempFulllMortalityCharges=0.0;
				RiderCharges=0;
				totRiderCharges=0;
				for(int i=1;i<=12;i++) {
					//double tempHoldetNettoInvest = investmentAmount ;
					double finalLoyalty=0.0;
					double tempHoldetNettoInvest = investmentAmount ;
					if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
						tempHoldetNettoInvest = investmentAmount;
					} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
					} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
					} else if (frequency.equalsIgnoreCase("M")) {
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
					} else {
						tempHoldetNettoInvest = 0.0;
					}
					if (count>ppt) {
						tempHoldetNettoInvest = 0.0;
					}

					oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAcc);
				oFormulaBean.setCNT(count);
				oFormulaBean.setMonthCount(i);
				oFormulaBean.setAP(master.getRequestBean().getBasepremiumannual());
				String MortalityCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityCharges = Double.parseDouble(MortalityCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges: "
						+ templMortalityCharges);
				tempFulllMortalityCharges +=templMortalityCharges;
				RiderCharges = getRiderCharges(count,age, ppt, i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);
				mortChargesSTax = getServiceTax(templMortalityCharges+RiderCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax: "
						+ mortChargesSTax + " and full Mortc " + tempFulllMortalityCharges);
			totRiderCharges=totRiderCharges+RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
					+ totRiderCharges);
				lMortalityCharges = Math.round(tempFulllMortalityCharges);// Mortility Charges Monthly Start
			CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges: "
					+ lMortalityCharges);
				hmMortalityCharges.put(count, lMortalityCharges);
				totalServiceTax = totalServiceTax + mortChargesSTax;
				tempAdminChargeSTax = getServiceTax(dAdminCharge);
			tempNetHolderInvestment = tempHoldetNettoInvest - templMortalityCharges - mortChargesSTax - dAdminCharge - tempAdminChargeSTax-RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: "
						+ tempNetHolderInvestment);
				tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
			//totalNetHolderInvestment+=tempNetHolderInvestment;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: "
						+ tempPHAccBeforeGrowth);
				oFormulaBean.setA(0.1);

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
						+ fundGrowthPM);

				tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: "
						+ tempPHAccInvestGrowth);
				tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: "
						+ tempPHAccHolder);
				oFormulaBean.setA(tempPHAccHolder);
				String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
						oFormulaBean)
						+ "";
				tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
						+ tempPhInvestmentFee);
				phInvestFeeSTax = getServiceTax(tempPhInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges: "
						+ phInvestFeeSTax);

				totalServiceTax = totalServiceTax + phInvestFeeSTax;
				tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee-phInvestFeeSTax;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
						+ tempPhNetHolderAcc);








				double InvestmentFeeSTax = 0.0;
				phAccount = tempPHAccHolder;
				phInvestmentFee = tempPhInvestmentFee;
				phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
				CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
						+ " #phInvestmentFeeValue: " + phInvestmentFee);

			// Calculate Service Tax on phInvestmentFee
			// InvestmentFeeSTax =
			// getServiceTaxForUnitLink(phInvestmentFee);

				InvestmentFeeSTax = phInvestFeeSTax;
				CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
						+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);

			// CommonConstant.printLog("d","#Policy term : "+count+" #serviceTaxOnInvestmentFee : "
			// + serviceTaxOnInvestmentFee);

				serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
						+ InvestmentFeeSTax;
				netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;
				regularFundValuePostFMC=Math.round(tempPhNetHolderAcc);
			// Storing regular fund values
				if (i == 12) {
					hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					oFormulaBean.setA(tempPhNetHolderAcc);
	          String tempLoyaltyCharges = formulaHandler.evaluateFormula(
			master.getRequestBean().getBaseplan(),
			FormulaConstant.CALCULATE_LC,
			oFormulaBean)
			+ "";
	         loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
            finalLoyalty=loyaltyCharges;
             hmLoyaltyCharge.put(count,Math.round(finalLoyalty));
					hmRegularFundValue.put(count, Math.round(netPHAccount));

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #RegularFundValue:  "
							+ Math.round(netPHAccount));

					regularFundValueBeforeFMC = Math.round(netPHAccount
							+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);
					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmRegularFundValueBeforeFMC: "
							+ regularFundValueBeforeFMC + " #netPHAccount:  "
							+ netPHAccount + " #phInvestmentFeeTotal: "
							+ phInvestmentFeeTotal
							+ " #serviceTaxOnInvestmentFee: "
							+ serviceTaxOnInvestmentFee);

					hmRegularFundValueBeforeFMC.put(count,
							regularFundValueBeforeFMC);
					long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
							count, netPHAccount);
					hmGuaranteedMaturityAmount.put(count,
							gurananteedMaturityAmount);

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmGuaranteedMaturityAmount: "
							+ gurananteedMaturityAmount);

	            	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
	            	CommonConstant.printLog("d", "#Policy term : " + count
							+ " #regularFundValuePostFMC: "
							+ regularFundValuePostFMC);
					/*totalFundValueAtEndtPolicyYear = Math
							.round(netPHAccount)
							+ gurananteedMaturityAmount;
					hmTotalFundValueAtEndPolicyYear.put(count,
							totalFundValueAtEndtPolicyYear);

				CommonConstant.printLog("d","#Policy term : " + count
						+ " #hmTotalFundValueAtEndPolicyYear:  "
						+ totalFundValueAtEndtPolicyYear);

				long surrenderValue = getSurrenderValue(count, netPHAccount)
						+ gurananteedMaturityAmount;
				hmSurrenderValue.put(count, surrenderValue);
				master.setMaturityBenefit10(surrenderValue);*/
				}
				 tempFinalPhNetHolderAcc=tempPhNetHolderAcc+finalLoyalty;

					CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
							+ tempFinalPhNetHolderAcc);
					tempNetHolderAcc=tempFinalPhNetHolderAcc;
					totalFundValueAtEndtPolicyYear = Math
					.round(tempNetHolderAcc);
					long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc);
					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmSurrenderValue" + surrenderValue);
			// END

					hmSurrenderValue.put(count, surrenderValue);

			// Other Inbuilt Benefit charges End
			// Mortility Charges calculation Start
			// Mortility Charges Monthly Start
				}
			// Calculate Service Tax on Mortility Charges
																		// Tax
			// END
			// End
			// Policy Admin Charges calculation Start

			// Calculate Service Tax on Admin Charges
																		// Tax
			// END
				age++;

                  	if(count<=ppt)
		{
		totalCharges=Math.round(PAC+totRiderCharges+lAdminChargetot+tempFulllMortalityCharges);
	 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
			 + totalCharges + "PAC" + PAC + "totRiderCharges" + totRiderCharges + "lAdminChargetot" + lAdminChargetot + "tempFulllMortalityCharges" + tempFulllMortalityCharges);
		}else{
			totalCharges=Math.round(totRiderCharges+lAdminChargetot+tempFulllMortalityCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					+ totalCharges + "PAC" + PAC + "lAdminChargetot" + lAdminChargetot + "tempFulllMortalityCharges" + tempFulllMortalityCharges);
		}
		 hmtotalcharge.put(count,Math.round(totalCharges));
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
				 + totalCharges);

			//totalServiceTax = totalServiceTax + serviceTaxOnInvestmentFee;
				ApplicableServiceTax = Math.round(totalServiceTax);
				hmApplicableServiceTax.put(count, ApplicableServiceTax);
				CommonConstant.printLog("d", "#PH6 #Policy term : " + count
						+ " #Total Applicable Service Tax ::: "
						+ ApplicableServiceTax);

				hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));

				CommonConstant.printLog("d", "#PH6 #Policy term : " + count
						+ " #ThmFundManagementCharge ::: "
						+ Math.round(phInvestmentFeeTotal));
				hmTotalFundValueAtEndPolicyYear.put(count,
						totalFundValueAtEndtPolicyYear);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmTotalFundValueAtEndPolicyYear:  "
						+ totalFundValueAtEndtPolicyYear);

			// Calculate Death benefit at the end of policy year
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setAP(master.getRequestBean().getBasepremiumannual());
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
				String strDBEP = formulaHandler
						.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
								oFormulaBean)
						+ "";
				double lDBEP = Double.parseDouble(strDBEP);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Death Benefit At EndOf Policy Year: " + lDBEP);
				hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			/*oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String strCommissionPayble = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
					+ "";
			long lCommision = Long.parseLong(strCommissionPayble);
				CommonConstant.printLog("d","#Policy term : " + count
						+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
				hmCommissionPayble.put(count, lCommision);
			*/
		}
        //master.setAmountForInvestment(hmInvestmentAmount);
		master.setPremiumAllocationCharges(hmPremiumAllocation);
		//master.setOtherBenefitCharges(hmOtherBenifitChrges);
			master.setTotalchargeforPH6(hmtotalcharge);
			//master.setPolicyAdminCharges(hmPolicyAdminCharges);
		master.setApplicableServiceTaxPH6(hmApplicableServiceTax);
		master.setFundManagmentChargePH6(hmFundManagementCharge);
		master.setRegularFundValuePH6(hmRegularFundValue);
			master.setRegularFundValuePostFMCPH6(hmRegularFundValuePostFMC);
		master.setRegularFundValueBeforeFMCPH6(hmRegularFundValueBeforeFMC);
		master.setGuaranteedMaturityAdditionPH6(hmGuaranteedMaturityAmount);
		master.setTotalFundValueAtEndPolicyYearPH6(hmTotalFundValueAtEndPolicyYear);
			master.setLoyaltyChargeforPH6(hmLoyaltyCharge);
		master.setSurrenderValuePH6(hmSurrenderValue);
		master.setMortalityChargesPH6(hmMortalityCharges);
		master.setDeathBenifitAtEndOfPolicyYearPH6(hmDeathBenefitAtEndofPolicyYear);
			//master.setCommissionPayble(hmCommissionPayble);

	}

	private long getDiscountedPremium() {
		long discountedPremium = 0;
		double discountedBasePremium = 0;
		long NSAPPremium = 0;
		double occuLoadValueOnPremium = 0;
		try {
			String strParamValue = DataAccessClass.getSPVSysParamValues(master
					.getRequestBean().getBaseplan());
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setP(master.getBaseAnnualizedPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String discountedBasePremiumValue = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_DISCOUNTED_PREMIUM, oFormulaBean)
					+ "";
			discountedBasePremium = Double
					.parseDouble(discountedBasePremiumValue);
			CommonConstant.printLog("d", " discountedBasePremium " + discountedBasePremium);
			if (strParamValue.contains("CAL_APWOL")) {
				CommonConstant.printLog("d", " inside CAL_APWOL 1");
				String occuLoadValueOnPremiumValue = formulaHandler
						.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_OCCUPATION_LOAD_VALUE_ON_PREMIUM,
								oFormulaBean)
						+ "";
				CommonConstant.printLog("d", " inside CAL_APWOL 2 " + occuLoadValueOnPremiumValue.toString());
				occuLoadValueOnPremium = Double
						.parseDouble(occuLoadValueOnPremiumValue);
				CommonConstant.printLog("d", " occuLoadValueOnPremium " + occuLoadValueOnPremium);
			}
			if (strParamValue.contains("CALC_NSAP")) {
				NSAPPremium = master.getModalPremiumForNSAP();
			}
			CommonConstant.printLog("d", " NSAPPremium " + NSAPPremium);
			master.setDiscountedModalPrimium(Math.round(discountedBasePremium
					+ occuLoadValueOnPremium + NSAPPremium));
			if (master.getRequestBean().getFrequency().equalsIgnoreCase("A")) {
				discountedPremium = Math.round(discountedBasePremium
						+ occuLoadValueOnPremium + NSAPPremium);
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					"S")) {
				discountedPremium = Math.round((discountedBasePremium
						+ occuLoadValueOnPremium + NSAPPremium) )* 2;
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					"Q")) {
				discountedPremium = Math.round((discountedBasePremium
						+ occuLoadValueOnPremium + NSAPPremium) )* 4;
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					"M")) {
				discountedPremium = Math.round((discountedBasePremium
						+ occuLoadValueOnPremium + NSAPPremium) )* 12;
			}else if (master.getRequestBean().getFrequency().equalsIgnoreCase("O")) {
				discountedPremium = Math.round(discountedBasePremium
						+ occuLoadValueOnPremium + NSAPPremium);
			}

			CommonConstant.printLog("d", " Final discountedPremium " + discountedPremium);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating DiscountedPremium for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return discountedPremium;
	}

	private HashMap getLifeCoverage() {
		HashMap lifeCoverageMap = new HashMap();
		try {

			long lifeCoverage = 0;
			double lifeCoverage_IW = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			long GAIValue = 0;
			long GAIVal = 0;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
			    GAIVal = calculateGAI(count);//Added for Insta Wealth
				oFormulaBean.setCNT(count);
			    GAIValue+=GAIVal;
				oFormulaBean.setA(GAIValue);
				FormulaHandler formulaHandler = new FormulaHandler();
				if(master.getRequestBean().getBaseplan().equals("SPAIN1") && count==1){
					lifeCoverageMap.put(count, 0);
				}
				else {
					String lifeCoverageValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LIFE_COVERAGE, oFormulaBean)
							+ "";
					if (master.getRequestBean().getBaseplan().contains("IWV1N")) {
						lifeCoverage_IW = Double.parseDouble(lifeCoverageValueStr);
						lifeCoverage = Math.round(lifeCoverage_IW);
					} else {
						lifeCoverage = Long.parseLong(lifeCoverageValueStr);
					}
					lifeCoverageMap.put(count, lifeCoverage);
				}
				CommonConstant.printLog("d", "Pol Term :::  lifeCoverage " + count + " : "
                        + lifeCoverage);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating LifeCoverage for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
        }

        return lifeCoverageMap;
	}
	private HashMap getSIPLifeCoverage() {
		HashMap lifeCoverageMap = new HashMap();
		try {
			long lifeCoverage = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			long GAIVal = 0;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				GAIVal = calculateGAI(polTerm);
				oFormulaBean.setA(GAIVal);
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				String lifeCoverageValueStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_LIFE_COVERAGE, oFormulaBean)
						+ "";
				lifeCoverage = Long.parseLong(lifeCoverageValueStr);
				lifeCoverageMap.put(count, lifeCoverage);
				CommonConstant.printLog("d", "Pol Term :::  lifeCoverage SIP " + count + " : "
                        + lifeCoverage);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating LifeCoverage SIP for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}

		return lifeCoverageMap;
	}
    private HashMap getGIPLifeCoverage() {
        HashMap lifeCoverageMap = new HashMap();
        try {
            long lifeCoverage = 0;
            int polTerm = master.getRequestBean().getPolicyterm();
            double NPRVal = 0;
            NPRVal = getSumAssuredMatGIP();
            for (int count = 1; count <= polTerm; count++) {
                oFormulaBean = getFormulaBeanObj();
                //NPRVal = calculateNPR(polTerm);
                oFormulaBean.setCNT(count);
                oFormulaBean.setA(NPRVal);
                FormulaHandler formulaHandler = new FormulaHandler();
                String lifeCoverageValueStr = formulaHandler.evaluateFormula(
                        master.getRequestBean().getBaseplan(),
                        FormulaConstant.CALCULATE_LIFE_COVERAGE, oFormulaBean)
                        + "";
                lifeCoverage = Long.parseLong(lifeCoverageValueStr);
                lifeCoverageMap.put(count, lifeCoverage);
                CommonConstant.printLog("d", "Pol Term :::  lifeCoverage GIP " + count + " : "
                        + lifeCoverage);
            }
        } catch (Exception e) {
            CommonConstant.printLog("e", "Error occured in calculating LifeCoverage GIP for planCode:  "
                    + master.getRequestBean().getBaseplan());
        }

        return lifeCoverageMap;
    }
	private HashMap getALTSCEGuranteedAnnualIncome() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		try {

			for (int count = 1; count <= polTerm; count++) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler=new FormulaHandler();
					long basePrimum=0;
					String basePremium = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_BASE_ANNUALIZED_PREMIUM,
							oFormulaBean)
							+ "";
					basePrimum = Long.parseLong(basePremium);
					oFormulaBean.setA(basePrimum);
					if(count<rpuyear){
						double gaivalue=0;
						long gaival=0;
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCE,
								oFormulaBean)
								+ "";
						gaivalue = Double.parseDouble((GAIVALUE));
						gaival=Math.round(gaivalue);
						gaiMap.put(count, gaival);
						CommonConstant.printLog("d", "Pol Term :::ALTSCE GAI " + count + " :" + gaival);
					}else{
						double gaivalue=0;
						long gaival=0;
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCENARIO,
								oFormulaBean)
								+ "";
						gaivalue = Double.parseDouble((GAIVALUE));
						gaival=Math.round(gaivalue);
						gaiMap.put(count, gaival);
						CommonConstant.printLog("d", "Pol Term :::  ALTSCE GAI  " + count + " :" + gaival);
					}
			}
		}catch (Exception e) {
				CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncomeALTCSE for planCode:  "
						+ master.getRequestBean().getBaseplan() + " " + e);
	  }
	return gaiMap;
  }
	private HashMap getGuranteedAnnualIncome() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		//int rpuyear = Integer.parseInt(master.getRequestBean().getRpu_year());
		try {

			for (int count = 1; count <= polTerm; count++) {
				/*if (count > master.getRequestBean().getPremiumpayingterm()) {
					long GAIValue = calculateGAI(count);
					gaiMap.put(count, GAIValue);
				} else {
					gaiMap.put(count, 0);
				}*/
				if (master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLGV2N1")) {
                    long GAIValue = calculateGAI(count);
                    gaiMap.put(count, GAIValue);
	              } else if (count > master.getRequestBean().getPremiumpayingterm()) {
	                    long GAIValue = calculateGAI(count);
	                    gaiMap.put(count, GAIValue);
	              } else {
	                    gaiMap.put(count, 0);
	              }

			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}
    private HashMap getGuranteedAnnualIncome_GIP() {
        HashMap gaiMap = new HashMap();
        int polTerm = master.getRequestBean().getPolicyterm();
        int incomeBooster = master.getRequestBean().getIncomeBooster();
        double GAIValue = 0;
        try {
            for (int count = 1; count <= polTerm+incomeBooster; count++) {
                GAIValue = calculateGAIGIP(count,GAIValue);
                gaiMap.put(count, Math.round(GAIValue));
            }
        } catch (Exception e) {
            CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome GIP for planCode:  "
                    + master.getRequestBean().getBaseplan() + e);
        }
        return gaiMap;
    }
	private HashMap getGuranteedEPAnnualIncome() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		long GAIValue = 0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm-1) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					oFormulaBean.setA(GAIValue); //Added for Insta Wealth
					FormulaHandler formulaHandler = new FormulaHandler();
					String GAIValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					GAIValue = Long.parseLong(GAIValueStr);
					gaiMap.put(count, GAIValue);
					CommonConstant.printLog("d", "getGuranteedEPAnnualIncome GAIValue " + GAIValue);
				} else {
					gaiMap.put(count, 0);
				}
			}
		} catch (Exception e) {
			CommonConstant.printLog("d", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}
	private HashMap getGuranteedAnnualIncomeAll() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		try {
			for (int count = 1; count <= polTerm; count++) {
				long GAIValue = calculateGAI(count);
				if (count==polTerm)
					gaiMap.put(count, 0);
				else
					gaiMap.put(count, GAIValue);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}

	private HashMap getAccGuranteedAnnualIncome() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		long GAIValue=0;
		//int rpuyear = Integer.parseInt(master.getRequestBean().getRpu_year());
		try {

			for (int count = 1; count <= polTerm; count++) {
				GAIValue += calculateGAI(count);
                gaiMap.put(count, GAIValue);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}

	private HashMap getAccPTGuranteedAnnualIncome() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		double GAIValue=0;
		//int rpuyear = Integer.parseInt(master.getRequestBean().getRpu_year());
		try {

			for (int count = 1; count <= polTerm; count++) {
				if (count<=ppt)
					GAIValue += calculateGAIDouble(count);
                /*if (count == polTerm)
                	gaiMap.put(count, 0);
                else*/
				gaiMap.put(count, Math.round(GAIValue));
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}

	private long getTOTALGuranteedAnnualIncome() {
		long gaiTOTAL = 0;
		 long GAIValue=0;
		int polTerm = master.getRequestBean().getPolicyterm();
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLGV2N1")) {
                   GAIValue = calculateGAI(count);
	              } else if (count > master.getRequestBean().getPremiumpayingterm()) {
	                   GAIValue = calculateGAI(count);
	              } else {
	            	  GAIValue=0;
	              }
				gaiTOTAL+=GAIValue;
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiTOTAL;
	}
	private HashMap getRPGAI() {
		HashMap gaiMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		long GAIValue=0;
		try {

			for (int count = 1; count <= polTerm; count++) {
	   if (master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLGV2N1")) {
		   if(rpuyear==0){
			   gaiMap.put(count, 0);
			}
			else if(count<rpuyear){
				GAIValue = calculateGAI(count);
                gaiMap.put(count, GAIValue);
			}else{
				oFormulaBean = getFormulaBeanObj();
				FormulaHandler formulaHandler=new FormulaHandler();
				oFormulaBean.setCNT(count);
				String GAIVALUE=formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GAI_RP,
						oFormulaBean)
						+ "";
				GAIValue = Long.parseLong(GAIVALUE);
				gaiMap.put(count, GAIValue);
			}
                    gaiMap.put(count, GAIValue);
                    CommonConstant.printLog("d", "Pol Term ::: RP GAI  " + count + " :" + GAIValue);
	    } else if (count > master.getRequestBean().getPremiumpayingterm()) {
	                    GAIValue = calculateGAI(count);
	                    gaiMap.put(count, GAIValue);
	    } else {
	                    gaiMap.put(count, 0);
	              }
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualIncome for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return gaiMap;
	}
	private HashMap getTotalGuaranteeeBenefit() {
		HashMap TGBMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		long GAIValue=0;
		long maturityValue = 0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				GAIValue = calculateGAI(count);
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
				} else {
					maturityValue=0;
				}
				TGBMap.put(count, (GAIValue+maturityValue));
			}
		} catch (Exception e) {
			CommonConstant.printLog("d", "Error occured in calculating calculateGAI for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TGBMap;
	}
    private HashMap getTotalGuaranteeeBenefit_GIP() {
        HashMap TGBMap = new HashMap();
        int polTerm = master.getRequestBean().getPolicyterm();
        int incomeBooster = master.getRequestBean().getIncomeBooster();
        double GAIValue=0;
        long maturityValue = 0;
        try {
            for (int count = 1; count <= polTerm+incomeBooster; count++) {
                GAIValue = calculateGAIGIP(count, GAIValue);
                if (count == polTerm) {
                    oFormulaBean = getFormulaBeanObj();
                    oFormulaBean.setCNT(count);
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String maturityValueStr = formulaHandler.evaluateFormula(
                            master.getRequestBean().getBaseplan(),
                            FormulaConstant.CALCULATE_MATURITY_VALUE,
                            oFormulaBean)
                            + "";
                    maturityValue = Long.parseLong(maturityValueStr);
                } else {
                    maturityValue=0;
                }
                TGBMap.put(count, (Math.round(GAIValue)+maturityValue));
            }
        } catch (Exception e) {
            CommonConstant.printLog("d", "Error occured in calculating getTotalGuaranteeeBenefit for planCode:  "
                    + master.getRequestBean().getBaseplan() + " " + e);
        }
        return TGBMap;
    }
	private HashMap getTotalEGuaranteeeBenefit() {
		HashMap TGBMap = new HashMap();
        int polTerm = master.getRequestBean().getPolicyterm();
		long GAIValue=0;
		long maturityValue = 0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				GAIValue = calculateGAI(count);
				if (count >= polTerm-1) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
				} else {
					maturityValue=0;
				}
				TGBMap.put(count, (GAIValue+maturityValue));
			}
		} catch (Exception e) {
			CommonConstant.printLog("d","Error occured in calculating calculateGAI for planCode:  "
							+ master.getRequestBean().getBaseplan()+" "+e);
		}
		return TGBMap;
	}
	private long calculateGAI(int polYear) {
		long GAIValue = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(polYear);
			FormulaHandler formulaHandler = new FormulaHandler();
			String GAIValueValueStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME,
					oFormulaBean)
					+ "";
			GAIValue = Long.parseLong(GAIValueValueStr);
			CommonConstant.printLog("d", "GAIValue :::::::: " + GAIValue);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating calculateGAI for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}

		return GAIValue;
	}
    private double calculateGAIGIP(int polYear, double GAIVal) {
        double GAIValue = 0;
        try {
            oFormulaBean = getFormulaBeanObj();
            oFormulaBean.setCNT(polYear);
            FormulaHandler formulaHandler = new FormulaHandler();
            if (GAIVal==0) {
                oFormulaBean.setA(master.getRequestBean().getSumassured());
            } else {
                oFormulaBean.setA(GAIVal);
            }
            String GAIValueValueStr = formulaHandler.evaluateFormula(master
                            .getRequestBean().getBaseplan(),
                    FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME_F,
                    oFormulaBean)
                    + "";
            GAIValue = Double.parseDouble(GAIValueValueStr);
            CommonConstant.printLog("d", "GAIValue GIP :::::::: " + GAIValue);
        } catch (Exception e) {
            CommonConstant.printLog("e", "Error occured in calculating calculateGAI for planCode:  "
                    + master.getRequestBean().getBaseplan() + " " + e);
        }

        return GAIValue;
    }
	private double calculateGAIDouble(int polYear) {
		double GAIValue = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(polYear);
			FormulaHandler formulaHandler = new FormulaHandler();
			String GAIValueValueStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME_D,
					oFormulaBean)
					+ "";
			GAIValue = Double.parseDouble(GAIValueValueStr);
			CommonConstant.printLog("d", "GAIValue :::::::: " + GAIValue);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating calculateGAI for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}

		return GAIValue;
	}

	private HashMap getGuranteedSurrenderValue() {
		HashMap GSBMap = new HashMap();
		try {
			long guranteedSurrenderValue;
			double guranteedSurrenderval=0;
			long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				if (master.getRequestBean().getBaseplan().startsWith("MBPV1N") && count!=polTerm) {
					long GAIValue = calculateGAI(count);
					totalGAIVal = totalGAIVal + GAIValue;
				}
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(totalGAIVal);
				oFormulaBean.setAGAI(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				String guaSurrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GURANTEED_SURRENDER_VALUE,
						oFormulaBean)
						+ "";
				guranteedSurrenderval=Double.parseDouble(guaSurrBenStr);
				if((master.getRequestBean().getBaseplan().equals("FGV1N1")
						|| master.getRequestBean().getBaseplan().equals("FR7V1P1")
						|| master.getRequestBean().getBaseplan().equals("FR7V1P2")
						|| master.getRequestBean().getBaseplan().equals("FR10V1P1")
						|| master.getRequestBean().getBaseplan().equals("FR10V1P2")
						|| master.getRequestBean().getBaseplan().equals("FR15V1P1")
						|| master.getRequestBean().getBaseplan().equals("FR15V1P2")
						|| master.getRequestBean().getBaseplan().equals("GKIDV1P1")) && count==polTerm)
					guranteedSurrenderValue=0;
				else
					guranteedSurrenderValue = Math.round(guranteedSurrenderval);
				GSBMap.put(count, guranteedSurrenderValue);
				CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
						+ guranteedSurrenderValue);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getGuranteedSurrenderValue "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return GSBMap;
	}
	private HashMap getALTGuranteedSurrenderValue() {
		HashMap ALTGSBMap = new HashMap();
		try {
			long guranteedSurrenderValue;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(getALTTOTALGAIValue(count));
				FormulaHandler formulaHandler = new FormulaHandler();
				String guaSurrBenStr = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ALTSCE_GURANTEED_SURRENDER_VALUE,
						oFormulaBean)
						+ "";
				guranteedSurrenderValue = Long.parseLong(guaSurrBenStr);
				if(master.getRequestBean().getBaseplan().equals("FGV1N1") && count==polTerm){
					guranteedSurrenderValue = 0;
				}
				ALTGSBMap.put(count, guranteedSurrenderValue);
				CommonConstant.printLog("d", "Pol Term :::  altsce Surr Benefit " + count + " : "
						+ guranteedSurrenderValue);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getGuranteedSurrenderValue "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return ALTGSBMap;
	}
	private double getALTTOTALGAIValue(int count) {
		double TotalGAIValue = 0;
		double gaival=0;
		int rpuyear = master.getRequestBean().getRpu_year() != null ? Integer.parseInt(master.getRequestBean().getRpu_year()) : 0;
        try {
            if (rpuyear != 0) {
                for (int term = 1; term <= count; term++) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(term);
					FormulaHandler formulaHandler=new FormulaHandler();
					long basePrimum=0;
					String basePremium = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_BASE_ANNUALIZED_PREMIUM,
							oFormulaBean)
							+ "";
					basePrimum = Long.parseLong(basePremium);
					oFormulaBean.setA(basePrimum);
					if(count<rpuyear){
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCE,
								oFormulaBean)
								+ "";
						gaival = Double.parseDouble(GAIVALUE);
						CommonConstant.printLog("d", "Pol Term :::ALTSCE GAI " + count + " :" + gaival);
					}else{
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCENARIO,
								oFormulaBean)
								+ "";
						gaival = Double.parseDouble(GAIVALUE);
						CommonConstant.printLog("d", "Pol Term :::  ALTSCE GAI in else " + count + " :" + gaival);
					}
					TotalGAIValue+=gaival;
					CommonConstant.printLog("d", "Pol Term :::  ALTSCE TOTALGAI  " + count + " :" + TotalGAIValue);
			}
			}
			}catch (Exception e) {
			//System.out
				//	.println("Exception occured in getALTGuranteedSurrenderValue "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating ALTGuranteedSurrenderValue for planCode:  "
                    + master.getRequestBean().getBaseplan() + " " + e);
		}
		return TotalGAIValue;
	}
	private double getALTTOTALMatVal(int val) {
		double maturityValue = 0;
		double totMatVal=0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		try {
			for (int count = 1; count <= val; count++) {
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					if(count<rpuyear){
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALTSCE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Double.parseDouble(maturityValueStr);
					}else{
						String maturityValueStr = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_ALTSCENARIO_MATURITY_VALUE,
								oFormulaBean)
								+ "";
						maturityValue = Double.parseDouble(maturityValueStr);
					}
				}
				totMatVal+=maturityValue;
				CommonConstant.printLog("d", "Pol Term ::: AltSCE MATVAL " + count + "" + maturityValue);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating alternate scenario total maturity value" + e + " " + e.getMessage());
		}
		return totMatVal;
	}

	public long getNSAPLoadPremium() {
		long NSAPPremium = 0;
		try {
			if ("N".equalsIgnoreCase(master.getRequestBean().getAgeProofFlag())) {
				oFormulaBean = getFormulaBeanObj();
				FormulaHandler formulaHandler = new FormulaHandler();
				String basePremium = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_NON_STANDARD_AGE_PROOF,
						oFormulaBean)
						+ "";
				NSAPPremium = Long.parseLong(basePremium);
			} else {
				NSAPPremium = 0;
			}
			CommonConstant.printLog("d", "NSAPPremium Premium Calculated :::: " + NSAPPremium);

		} catch (Exception e) {
			NSAPPremium = 0;
			CommonConstant.printLog("e", "Error occured in calculating Modal Premium" + e + " " + e.getMessage());
		}

		return NSAPPremium;
	}

	//added by Ganesh for Fortune Gaurantee 07/09/2015
	public long getNSAPAnnualLoadPremium() {
		long NSAPPremium = 0;
		try {
			if ("N".equalsIgnoreCase(master.getRequestBean().getAgeProofFlag())) {
				oFormulaBean = getFormulaBeanObj();
				FormulaHandler formulaHandler = new FormulaHandler();
				String basePremium = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ANNUAL_NON_STANDARD_AGE_PROOF,
						oFormulaBean)
						+ "";
				NSAPPremium = Long.parseLong(basePremium);
			} else {
				NSAPPremium = 0;
			}
			CommonConstant.printLog("d", "NSAPPremium Annual Premium Calculated :::: " + NSAPPremium);

		} catch (Exception e) {
			NSAPPremium = 0;
			CommonConstant.printLog("d", "Error occured in calculating Modal Premium NSAP Annual Load" + e + " " + e.getMessage());
		}

		return NSAPPremium;
	}

	public String ValidateMinimumPremium() {
		String strErrorMessage = "";
		String strFrequency = "";
		try {

			if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					CommonConstant.ANNUAL_MODE)) {
				strFrequency = "Annual";
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					CommonConstant.SEMI_ANNUAL_MODE)) {
				strFrequency = "Semi Annual";
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					CommonConstant.QUATERLY_MODE)) {
				strFrequency = "Quaterly";
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					CommonConstant.MONTHLY_MODE)) {
				strFrequency = "Monthly";
			} else if (master.getRequestBean().getFrequency().equalsIgnoreCase(
					CommonConstant.SINGLE_MODE)) {
				strFrequency = "Single";
			}

			long iAnnualMinPremium = DataAccessClass.getMinimumPremium(master
					.getRequestBean().getBaseplan(), master.getRequestBean()
					.getFrequency());
			if (master.getQuotedAnnualizedPremium() < iAnnualMinPremium) {
				strErrorMessage = "Premium is less than minimum premium for "
						+ strFrequency;
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "inside ValidateMinimumPremium method " + e + " " + e.getMessage());
			e.printStackTrace();
		}
		return strErrorMessage;
	}

    /*	private long getTotalGAISum() {
            HashMap gaiMap = new HashMap();
            long TotalGAISum = 0;
            gaiMap = master.getGuranteedAnnualIncome();
            try {
                if (gaiMap.size() > 0) {
                    for (int i = 1; i <= master.getRequestBean().getPolicyterm(); i++) {

                        TotalGAISum = TotalGAISum
                                + Long.parseLong(gaiMap.get(i) + "");
                    }
                }
            } catch (Exception e) {
                Logger
                        .error("Error occured in calculating getTotalGAISum for planCode:  "
                                + master.getRequestBean().getBaseplan());
            }
            return TotalGAISum;
        }*/
	private double getTotalGAISum() {
		double TotalGAISum = 0;
		double GAIValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (count > master.getRequestBean().getPremiumpayingterm()) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String GAIValueValueStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME_D,
							oFormulaBean) + "";
					GAIValue = Double.parseDouble(GAIValueValueStr);
					TotalGAISum = TotalGAISum + GAIValue;
					CommonConstant.printLog("d", "TotalGAISum :::::::: " + TotalGAISum);
				}
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating getTotalGAISum for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TotalGAISum;
	}

    private double getSumAssuredMatGIP() {
        double TotalGAISum = 0;
        double GAIValue = 0;
        int polTerm = master.getRequestBean().getPolicyterm();
        int incomeBooster = master.getRequestBean().getIncomeBooster();
        try {
            for (int count = polTerm+1; count <= polTerm+incomeBooster; count++) {
                if (count > master.getRequestBean().getPremiumpayingterm()) {
                    oFormulaBean = getFormulaBeanObj();
                    oFormulaBean.setCNT(count);
                    if (GAIValue==0) {
                        oFormulaBean.setA(master.getRequestBean().getSumassured());
                    } else {
                        oFormulaBean.setA(GAIValue);
                    }
                    FormulaHandler formulaHandler = new FormulaHandler();
                    String GAIValueValueStr = formulaHandler.evaluateFormula(master
                                    .getRequestBean().getBaseplan(),
                            FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME_F,
                            oFormulaBean) + "";
                    GAIValue = Double.parseDouble(GAIValueValueStr);
                    TotalGAISum = TotalGAISum + (GAIValue / (Math.pow((1+(7.5/100.0)), count-polTerm)));
                    CommonConstant.printLog("d", "TotalGAISum :::::::: " + TotalGAISum);
                }
            }
        } catch (Exception e) {
            CommonConstant.printLog("e", "Error occured in calculating getTotalGAISum for planCode:  "
                    + master.getRequestBean().getBaseplan() + e);
        }
        return TotalGAISum;
    }

	private HashMap getTotalMaturityValue() {
        HashMap hmTotalMatVal = new HashMap();
		HashMap hmGAI = master.getGuranteedAnnualIncome();
		HashMap hmMatVal = master.getMaturityVal();
		int polTerm = master.getRequestBean().getPolicyterm();
		long lTotMatVal=0;
		try {
			lTotMatVal=Long.parseLong(hmGAI.get(polTerm)+"")+Long.parseLong(hmMatVal.get(polTerm)+"");

			 for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					hmTotalMatVal.put(count, lTotMatVal);
					CommonConstant.printLog("d", "Pol Term :::  TotalMaturityValue " + count + " : "
							+ "");
				} else {
					hmTotalMatVal.put(count, 0);
				}
			}

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating getTotalGAISum for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return hmTotalMatVal;
	}


	private long getTotalMaturityValuePlusGAI() {
		HashMap hmGAI = master.getGuranteedAnnualIncome();
		HashMap hmMatVal = master.getMaturityVal();
		int polTerm = master.getRequestBean().getPolicyterm();
		long lTotMatVal=0;
		try {
			  if(hmMatVal.size()>0 && hmGAI.size()>0){
					 for (int count = 1; count <= polTerm; count++)
					 {
						 lTotMatVal=lTotMatVal + Long.parseLong(hmGAI.get(count)+"" ) +Long.parseLong(hmMatVal.get(polTerm)+"");
					 }
			  }
		 } catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating getTotalGAISum for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return lTotMatVal;
	}

	private HashMap getVSRBonus4() {
		HashMap RBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			double onePlusRBD = 0;
			for (int count = 1; count <= polTerm; count++) {
				/*
				 * if(count <= master.getRequestBean().getPremiumpayingterm()){
				 * totalGAIVal = 0; }else{ long GAIValue = calculateGAI(count);
				 * totalGAIVal = totalGAIVal + GAIValue; }
				 */
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				// oFormulaBean.setA(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) ||  master.getRequestBean().getBaseplan().startsWith("IWV1N")
						|| (master.getRequestBean().getBaseplan().startsWith("FR7V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR7V1P2"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR10V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR10V1P2"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR15V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR15V1P2"))
                        || (master.getRequestBean().getBaseplan().startsWith("GKIDV1P1"))){
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getVSRBonus4");
				String strVsrbonus = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_4,
					oFormulaBean)
					+ "";
				//vsrbonus = Long.parseLong(strVsrbonus);
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus = Math.round(dvsrbonus);
				RBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus4 " + count + " : "
						+ vsrbonus);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVSRBonus4 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getVSRBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return RBMap;
	}
	private long getTOTALVSRBonus4() {
		long TOTALRB4 = 0;
		double onePlusRBD = 0;
		try {
			long vsrbonus;
			double dvsrbonus;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getTOTALVSRBonus4");
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				TOTALRB4 += vsrbonus;
                CommonConstant.printLog("d", "Pol Term :::  TOTALVSRBonus4 " + count + " : "
						+ vsrbonus);
            }
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVSRBonus4 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTOTALVSRBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TOTALRB4;
	}
	private HashMap getAltVSRBonus4() {
		HashMap AltRBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				if(count>=6 ){
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ALTVRSBONUS_4,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				AltRBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::AltRBMap  VSRBonus4 " + count + " : "
						+ vsrbonus);
				}else{
					AltRBMap.put(count, 0);
				}
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVSRBonus4 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltVSRBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return AltRBMap;
	}
	private HashMap getAltEBONUSDURING() {
		HashMap AltEBMap = new HashMap();
		try {
			long ebbonus;
			long vsrbonus;
			long termBonus=0;
			double dvsrbonus=0;
			long premPaidcumulativeTotal=0;
			long prevPremPaidcumulativeTotal=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <=polTerm; count++) {
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1")  || master.getRequestBean().getBaseplan().equals("MMXV1N1")){
					prevPremPaidcumulativeTotal = premPaidcumulativeTotal+termBonus;
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strEbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_EBONUSCUMULATIVE,
							oFormulaBean)
							+ "";
					vsrbonus=Long.parseLong(strEbonus);
					String strETerbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_EBONUSTB,
							oFormulaBean)
							+ "";
					termBonus=Long.parseLong(strETerbonus);
					premPaidcumulativeTotal+=vsrbonus;
					CommonConstant.printLog("d", "Pol Term :::  GP EBDURING EBONUS " + count + " : "
							+ premPaidcumulativeTotal);
					ebbonus = premPaidcumulativeTotal+termBonus-prevPremPaidcumulativeTotal;
					AltEBMap.put(count, ebbonus);
					CommonConstant.printLog("d", "Pol Term :::AltRBMap  EBDURING " + count + " : "
							+ ebbonus);
				}else{
					prevPremPaidcumulativeTotal=premPaidcumulativeTotal;
					premPaidcumulativeTotal=0;
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					oFormulaBean.setCNT_MINUS(false);

					FormulaHandler formulaHandler = new FormulaHandler();
					String strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_VBCUMMULATIVECR,
							oFormulaBean)
							+ "";
					dvsrbonus = Double.parseDouble(strVsrbonus);
					vsrbonus=Math.round(dvsrbonus);
					CommonConstant.printLog("d", "Pol Term :::  GP EBDURING EBONUS " + count + " : "
							+ vsrbonus);
					premPaidcumulativeTotal=premPaidcumulativeTotal+vsrbonus;
					CommonConstant.printLog("d", "Pol Term :::  GP EBDURING EBONUS " + count + " : "
							+ premPaidcumulativeTotal);

					ebbonus = premPaidcumulativeTotal-prevPremPaidcumulativeTotal;
					AltEBMap.put(count, ebbonus);
					CommonConstant.printLog("d", "Pol Term :::AltRBMap  EBDURING " + count + " : "
							+ ebbonus);
				}
				//ebbonus = Long.parseLong(altScPremPaidCumulCurrRt.get(count).toString()) - Long.parseLong(altScPremPaidCumulCurrRt.get(count-1).toString());
				//ebbonus=Math.round(dvsrbonus+termBonus);
				//AltEBMap.put(count, ebbonus);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getAltEBDURING "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltEBDURING for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return AltEBMap;
	}

	private HashMap getTerminalBonus4() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			double dterminalBonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}

			double onePlusRBD = 0;
			for (int count = 1; count <= polTerm; count++) {
				/*
				 * if(count <= master.getRequestBean().getPremiumpayingterm()){
				 * totalGAIVal = 0; }else{ long GAIValue = calculateGAI(count);
				 * totalGAIVal = totalGAIVal + GAIValue; }
				 */
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				// oFormulaBean.setA(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) || master.getRequestBean().getBaseplan().startsWith("IWV1N")) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TERMINAL_BONUS_4,
						oFormulaBean)
						+ "";
				//terminalBonus = Long.parseLong(strTerminalbonus);

				dterminalBonus=Double.parseDouble(strTerminalbonus);
				terminalBonus=Math.round(dterminalBonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getTerminalBonus4 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefit4() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			long totalGAIVal = 0;
			double onePlusRBD = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt= master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 6;
			}

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
				FormulaHandler formulaHandler = new FormulaHandler();
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2"))
						|| (master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						|| (master.getRequestBean().getBaseplan().startsWith("IWV1N"))
                        || (master.getRequestBean().getBaseplan().startsWith("GKIDV1P1"))) {
					long GAIValue = calculateGAI(count);
					totalGAIVal = totalGAIVal + GAIValue;
					if (count==polTerm && master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						totalGAIVal = totalGAIVal - GAIValue;
					if (count>startPt) {
						String onePlusRB = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.ONEPLUSRB4,
								oFormulaBean)
								+ "";
						onePlusRBD = Double.parseDouble(onePlusRB);
						onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					} else {
						onePlusRBD = 0;
					}
					oFormulaBean.setA(onePlusRBD);
					if (count>ppt)
						oFormulaBean.setCNT_MINUS(false);
				} else
					oFormulaBean.setA(0);
				oFormulaBean.setCNT(count - 1);
				oFormulaBean.setAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_4,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  total death benifit 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getTerminalBonus4 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefitFR4() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			double totalGAIVal = 0;
			double onePlusRBD = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt= master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
				FormulaHandler formulaHandler = new FormulaHandler();
				double GAIValue = calculateGAIDouble(count);
				if (count<=ppt)
					totalGAIVal = totalGAIVal + GAIValue;
				if (count==polTerm)
					totalGAIVal=0;
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
				} else {
					onePlusRBD = 0;
				}
				oFormulaBean.setA(onePlusRBD);
				if (count>ppt)
					oFormulaBean.setCNT_MINUS(false);
				oFormulaBean.setCNT(count - 1);
				oFormulaBean.setDoubleAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_4,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  total death benifit 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//System.out.println("Exception occured in getTerminalBonus4 "+ e);
			CommonConstant.printLog("d", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}
	private HashMap getTotalDeathBenefit() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				/*
				 * if(count <= master.getRequestBean().getPremiumpayingterm()){
				 * totalGAIVal = 0; }else{ long GAIValue = calculateGAI(count);
				 * totalGAIVal = totalGAIVal + GAIValue; }
				 */
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				// oFormulaBean.setA(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_4,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  total death benifit 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTerminalBonus4 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TBMap;
	}
	/** Vikas - Added for Mahalife Gold - GAI*/

	private HashMap getSurrenderBenefit_4() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 1;
		if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
			startPt = 6;
		}
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		long finalTotalGAIVal = 0;
		long finalTotalGAIValSSV = 0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			if(count == 1){
				oFormulaBean.setA(0);
				oFormulaBean.setAGAI(totalGAIVal);
				oFormulaBean.setAGAISSV(finalTotalGAIValSSV);
			}else{
				FormulaHandler formulaHandler = new FormulaHandler();
				long GAIValue = calculateGAI(count);
				oFormulaBean.setCNT(count-1);
				totalGAIVal = totalGAIVal + GAIValue;
				finalTotalGAIVal=totalGAIVal;
				finalTotalGAIValSSV=finalTotalGAIVal;
				if (count==polTerm && master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
					finalTotalGAIVal = finalTotalGAIVal - GAIValue;
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) || master.getRequestBean().getBaseplan().startsWith("IWV1N")) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				oFormulaBean.setAGAI(finalTotalGAIVal);
				oFormulaBean.setAGAISSV(finalTotalGAIValSSV);
				CommonConstant.printLog("d", "getSurrenderBenefit_4");
				String strVsrbonus = "0";
				if (master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N")
						|| master.getRequestBean().getBaseplan().startsWith("IWV1N")) {
					strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_VRSBONUS_4_D,
							oFormulaBean)
							+ "";
				} else {
				 strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";
				}//vsrbonus = Long.parseLong(strVsrbonus);
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));
				//oFormulaBean.setA(Double.parseDouble(VSRB4Map.get(count-1)+""));
			}
			oFormulaBean.setCNT(count);

			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_4, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);

		// for policy term maturity value should be there
			/*if(count==polTerm){
				 surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
			}*/

			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
					+ surrenderBenefit);

		}

		return SBMap;
	}

	private HashMap getSurrenderBenefitFR_4() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		double finalTotalGAIVal = 0;
		double finalTotalGAIValSSV = 0;
		oFormulaBean.setRBV(0);
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			//if(count == 1){
			FormulaHandler formulaHandler = new FormulaHandler();
			double GAIValue = calculateGAIDouble(count);
				/*totalGAIVal = totalGAIVal + GAIValue;
				String onePlusRB = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ONEPLUSRB4,
						oFormulaBean)
						+ "";
				onePlusRBD = Double.parseDouble(onePlusRB);
				onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
				oFormulaBean.setA(onePlusRBD);
				String strVsrbonus = "0";
				strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4_D,
						oFormulaBean)
						+ "";

				oFormulaBean.setA(Double.parseDouble(strVsrbonus));*/
			oFormulaBean.setDoubleAGAI(totalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
			//}else{
			//FormulaHandler formulaHandler = new FormulaHandler();
			//double GAIValue = calculateGAI(count);
			oFormulaBean.setCNT(count-1);
			GAIValue = calculateGAIDouble(count);
			if (count<=ppt)
				totalGAIVal = totalGAIVal + GAIValue;
			finalTotalGAIVal=totalGAIVal;
			finalTotalGAIValSSV=finalTotalGAIVal;
			String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.ONEPLUSRB4,
					oFormulaBean)
					+ "";
			onePlusRBD = Double.parseDouble(onePlusRB);
			onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
			oFormulaBean.setA(onePlusRBD);
			oFormulaBean.setDoubleAGAI(finalTotalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
			CommonConstant.printLog("d", "getSurrenderBenefit_4");
			String strVsrbonus = "0";
			strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_4_D,
					oFormulaBean)
					+ "";
			oFormulaBean.setA(Double.parseDouble(strVsrbonus));


			//oFormulaBean.setA(Double.parseDouble(VSRB4Map.get(count-1)+""));
			//}

			oFormulaBean.setCNT(count);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			//FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_4, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			oFormulaBean.setRBV(Double.parseDouble(strVsrbonus));
			// for policy term maturity value should be there
			/*if(count==polTerm){
				 surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
			}*/

			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
					+ surrenderBenefit);

		}

		return SBMap;
	}


	// for 8% table Money Maxima

	private HashMap getVSRBonus8() {
		HashMap RBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			double onePlusRBD=0;
			// long totalGAIVal = 0;
			int startPt = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			for (int count = 1; count <= polTerm; count++) {
				/*
				 * if(count <= master.getRequestBean().getPremiumpayingterm()){
				 * totalGAIVal = 0; }else{ long GAIValue = calculateGAI(count);
				 * totalGAIVal = totalGAIVal + GAIValue; }
				 */
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				// oFormulaBean.setA(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) || (master.getRequestBean().getBaseplan().startsWith("IWV1N"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR7V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR7V1P2"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR10V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR10V1P2"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR15V1P1"))
						|| (master.getRequestBean().getBaseplan().startsWith("FR15V1P2"))
						|| (master.getRequestBean().getBaseplan().startsWith("GKIDV1P1"))) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getVSRBonus8");
				String strVsrbonus = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_8,
					oFormulaBean)
					+ "";
			//	vsrbonus = Long.parseLong(strVsrbonus);
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				RBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus8 " + count + " : "
						+ vsrbonus);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVSRBonus8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getVSRBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return RBMap;
	}
	private long getTOTALVSRBonus8() {
		long TOTALRB =0;
		try {
			long vsrbonus;
			double dvsrbonus;
			double onePlusRBD = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getTOTALVSRBonus8");
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				TOTALRB+=vsrbonus;
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus8 " + count + " : "
						+ TOTALRB);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTOTALVSRBonus8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getVSRBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TOTALRB;
	}
	private HashMap getAltVSRBonus8() {
		HashMap AltRBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				if(count>=6){
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ALTVRSBONUS_8,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				AltRBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::AltRBMap  VSRBonus8 " + count + " : "
						+ vsrbonus);
				}else{
					AltRBMap.put(count,0);
				}
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVSRBonus8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltVSRBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return AltRBMap;
	}

	private HashMap getTerminalBonus8() {
		HashMap TBMap = new HashMap();
		double dterminalBonus;
		double onePlusRBD = 0;
		try {
			long terminalBonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			for (int count = 1; count <= polTerm; count++) {
				/*
				 * if(count <= master.getRequestBean().getPremiumpayingterm()){
				 * totalGAIVal = 0; }else{ long GAIValue = calculateGAI(count);
				 * totalGAIVal = totalGAIVal + GAIValue; }
				 */
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				// oFormulaBean.setA(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) || master.getRequestBean().getBaseplan().startsWith("IWV1N")) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TERMINAL_BONUS_8,
						oFormulaBean)
						+ "";
			//	terminalBonus = Long.parseLong(strTerminalbonus);
				dterminalBonus=Double.parseDouble(strTerminalbonus);
				terminalBonus=Math.round(dterminalBonus);

				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 8 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getTerminalBonus8 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefit8() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			long totalGAIVal = 0;
			double onePlusRBD=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt = master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 6;
			}

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
			FormulaHandler formulaHandler = new FormulaHandler();
			if ((master.getRequestBean().getBaseplan().equals("SGPV1N1")
					|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
					|| master.getRequestBean().getBaseplan().startsWith("MBPV1N")
					|| master.getRequestBean().getBaseplan().startsWith("IWV1N"))
                    || master.getRequestBean().getBaseplan().startsWith("GKIDV1P1")) {
					long GAIValue = calculateGAI(count);
					totalGAIVal = totalGAIVal + GAIValue;
					if (count==polTerm && master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						totalGAIVal = totalGAIVal - GAIValue;
					if (count>startPt) {
						String onePlusRB = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.ONEPLUSRB8,
								oFormulaBean)
								+ "";
						onePlusRBD = Double.parseDouble(onePlusRB);
						onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					} else {
						onePlusRBD = 0;
					}
					oFormulaBean.setA(onePlusRBD);
					if (count>ppt)
						oFormulaBean.setCNT_MINUS(false);
				} else
					oFormulaBean.setA(0);
				oFormulaBean.setCNT(count - 1);
				oFormulaBean.setAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_8,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 8 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getTerminalBonus8 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefitFR8() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			double totalGAIVal = 0;
			double onePlusRBD=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt = master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
				FormulaHandler formulaHandler = new FormulaHandler();
				double GAIValue = calculateGAIDouble(count);
				if (count<=ppt)
					totalGAIVal = totalGAIVal + GAIValue;
				if (count==polTerm)
					totalGAIVal=0;
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
				} else {
					onePlusRBD = 0;
				}
				oFormulaBean.setA(onePlusRBD);
				if (count>ppt)
					oFormulaBean.setCNT_MINUS(false);
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setDoubleAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_8,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 8 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {
			//System.out.println("Exception occured in getTerminalBonus8 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTerminalBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getSurrenderBenefit_8() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 1;
		if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
			startPt = 6;
		}
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		long finalTotalGAIVal = 0;
		long finalTotalGAIValSSV = 0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			if(count == 1){
				oFormulaBean.setA(0);
				oFormulaBean.setAGAI(totalGAIVal);
				oFormulaBean.setAGAISSV(finalTotalGAIValSSV);
			}else{
				FormulaHandler formulaHandler = new FormulaHandler();
				long GAIValue = calculateGAI(count);
				oFormulaBean.setCNT(count-1);
				totalGAIVal = totalGAIVal + GAIValue;
				finalTotalGAIVal=totalGAIVal;
				finalTotalGAIValSSV=finalTotalGAIVal;
				if (count==polTerm && master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
					finalTotalGAIVal = finalTotalGAIVal - GAIValue;
				if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) || master.getRequestBean().getBaseplan().startsWith("IWV1N")) {
					String onePlusRB = formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(), FormulaConstant.ONEPLUSRB8, oFormulaBean) + "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				oFormulaBean.setAGAI(finalTotalGAIVal);
				oFormulaBean.setAGAISSV(finalTotalGAIValSSV);
				CommonConstant.printLog("d", "getSurrenderBenefit_8");
				String strVsrbonus = "0";
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N")
						|| master.getRequestBean().getBaseplan().startsWith("IWV1N"))) {
					strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_VRSBONUS_8_D,
							oFormulaBean)
							+ "";
				} else {
					strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";
				}
				oFormulaBean.setCNT(count);
				//vsrbonus = Long.parseLong(strVsrbonus);
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));
			//oFormulaBean.setA(Double.parseDouble(VSRB4Map.get(count-1)+""));

			}
			oFormulaBean.setCNT(count);

			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_8, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);

		// for policy term maturity value should be there
			/*if(count==polTerm){
				 surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
			}*/

			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit 8" + count + " : "
					+ surrenderBenefit);

		}

		return SBMap;
	}

	private HashMap getSurrenderBenefitFR_8() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		double finalTotalGAIVal = 0;
		double finalTotalGAIValSSV = 0;
		oFormulaBean.setRBV(0);
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
//			if(count == 1){
			//FormulaHandler formulaHandler = new FormulaHandler();
			//oFormulaBean.setA(0);
			double GAIValue = calculateGAIDouble(count);
			//totalGAIVal = totalGAIVal + GAIValue;
				/*String strVsrbonus = "0";
				String onePlusRB = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ONEPLUSRB8,
						oFormulaBean)
						+ "";
				onePlusRBD = Double.parseDouble(onePlusRB);
				onePlusRBD = Math.pow(onePlusRBD,(count-startPt));

				oFormulaBean.setA(onePlusRBD);
				strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8_D,
						oFormulaBean)
						+ "";
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));*/
			oFormulaBean.setDoubleAGAI(totalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
//			} else {
			FormulaHandler formulaHandler = new FormulaHandler();
			//double GAIValue = calculateGAIDouble(count);
			oFormulaBean.setCNT(count-1);
			if (count<=ppt)
				totalGAIVal = totalGAIVal + GAIValue;
			finalTotalGAIVal=totalGAIVal;
			finalTotalGAIValSSV=finalTotalGAIVal;
			String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.ONEPLUSRB8,
					oFormulaBean)
					+ "";
			onePlusRBD = Double.parseDouble(onePlusRB);
			onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
			oFormulaBean.setA(onePlusRBD);
			oFormulaBean.setDoubleAGAI(finalTotalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
			CommonConstant.printLog("d", "getSurrenderBenefit_8");
			String strVsrbonus = "0";
			//oFormulaBean.setCNT(count);
			strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_8_D,
					oFormulaBean)
					+ "";
			oFormulaBean.setA(Double.parseDouble(strVsrbonus));
			//		}

			oFormulaBean.setCNT(count);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			//FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_8, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			oFormulaBean.setRBV(Double.parseDouble(strVsrbonus));
			// for policy term maturity value should be there
			/*if(count==polTerm){
				 surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
			}*/

			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit 8" + count + " : "
					+ surrenderBenefit);

		}

		return SBMap;
	}

	/** Vikas - Added for Mahalife Gold & Gold Plus*/
	private long getTotalCashDividend4(){

		long totalCashDiv=0;
		long cashDiv=0;
		int polTerm = master.getRequestBean().getPolicyterm();

		for (int count = 1; count <= polTerm; count++)
		{
			FormulaHandler formulaHandler = new FormulaHandler();
			oFormulaBean.setCNT(count);
			String strCashDiv = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_4,
					oFormulaBean)
					+ "";
			cashDiv = Long.parseLong(strCashDiv);
			totalCashDiv = totalCashDiv + cashDiv;
			CommonConstant.printLog("d", "Get Total Cash Dividend :::  : " + cashDiv);

		}


		return totalCashDiv;
	}

	private long getTotalCashDividend8(){

		long totalCashDiv=0;
		long cashDiv=0;
		int polTerm = master.getRequestBean().getPolicyterm();

		for (int count = 1; count <= polTerm; count++)
		{
			FormulaHandler formulaHandler = new FormulaHandler();
			oFormulaBean.setCNT(count);
			String strCashDiv = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_8,
					oFormulaBean)
					+ "";
			cashDiv = Long.parseLong(strCashDiv);
			totalCashDiv = totalCashDiv + cashDiv;
			CommonConstant.printLog("d", "Get Total Cash Dividend :::  : " + cashDiv);

		}


		return totalCashDiv;
	}

	private long getTotalAnnualCoupon(){
		long totalAnnCoupon=0;
		long annCoupon;
		int polTerm = master.getRequestBean().getPolicyterm();

		for (int count = 1; count <= polTerm; count++)
		{
			FormulaHandler formulaHandler = new FormulaHandler();
			oFormulaBean.setCNT(count);
			String strCashDiv = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INCOME,
					oFormulaBean)
					+ "";
			annCoupon = Long.parseLong(strCashDiv);
			totalAnnCoupon = totalAnnCoupon + annCoupon;
			CommonConstant.printLog("d", "Get Total Annual Coupon :::  : " + annCoupon);

		}
		return totalAnnCoupon;
	}

	private HashMap getGuranteedAnnualInflationCover() {
		HashMap ipcMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		try {

			for (int count = 1; count <= polTerm; count++) {
				long IPCValue = calculateIPC(count);
					ipcMap.put(count, IPCValue);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating GuranteedAnnualInflationCover for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return ipcMap;
	}

	private long calculateIPC(int polYear) {
		long IPCValue = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(polYear);
			FormulaHandler formulaHandler = new FormulaHandler();
			String IPCValueValueStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_GURANTEED_ANNUAL_INFLATION_COVER,
					oFormulaBean)
					+ "";
			IPCValue = Long.parseLong(IPCValueValueStr);
			CommonConstant.printLog("d", "IPCValue :::::::: " + IPCValue);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating calculateIPC for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}

		return IPCValue;
	}

	private double getRiderCharges(int count, int age, int ppt, int monthCount) {
		double riderCharges = 0.0;
		ArrayList aList = master.getRiderData();
		String rtlCode;
		long rtlID;
		long rtlCoverage;
		long rtlTerm;
		CommonConstant.printLog("d", "Rider list size is " + aList.size());
		for (int i = 0; i < aList.size(); i++) {
			RiderBO oRiderBO = (RiderBO) aList.get(i);
			rtlID = oRiderBO.getRdlID();
			rtlCode = oRiderBO.getCode();
			rtlCoverage = oRiderBO.getSumAssured();
			rtlTerm = oRiderBO.getPolTerm();
			CommonConstant.printLog("d", "Rider detils -  ID " + rtlID + " Code " + rtlCode);
			riderCharges = riderCharges + getRiderCharge(rtlCode,rtlID,count,age, ppt, rtlCoverage, rtlTerm, monthCount);
		}

		return riderCharges;
	}

	private double getRiderCharge(String riderCode, long riderID,int count,int age, int ppt, long rtlCoverage, long rtlTerm, int monthCount) {
		double riderCharge=0.0;
		oFormulaBean.setMonthCount(monthCount);
		if (riderCode.startsWith("ADDL")) {
			CommonConstant.printLog("d", "Getting Rider Charges for ADDL");
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(riderID);
			oFormulaBean.setRC(rtlCoverage);
			if (rtlTerm>=count) {
				FormulaHandler formulaHandler = new FormulaHandler();
				String ADDLRiderCharge = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADDL_RIDER_CHARGE,
						oFormulaBean)
						+ "";
				riderCharge=Double.parseDouble(ADDLRiderCharge);
			} else {
				riderCharge=0.0;
			}
		} else if (riderCode.startsWith("WOPP") && count<=ppt) {
			CommonConstant.printLog("d", "Getting Rider Charges for WOPP");
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(riderID);
			oFormulaBean.setRC(rtlCoverage);
			if (rtlTerm>=count) {
				FormulaHandler formulaHandler = new FormulaHandler();
				String WOPPRiderCharge = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_WOP_RIDER_CHARGE,
						oFormulaBean)
						+ "";
				riderCharge=Double.parseDouble(WOPPRiderCharge);
			} else {
				riderCharge=0.0;
			}
		} else if (riderCode.startsWith("WOP") && count<=ppt) {
			CommonConstant.printLog("d", "Getting Rider Charges for WOP");
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(riderID);
			oFormulaBean.setRC(rtlCoverage);
			if (rtlTerm>=count) {
				FormulaHandler formulaHandler = new FormulaHandler();
				String WOPRiderCharge = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_WOP_RIDER_CHARGE,
						oFormulaBean)
						+ "";
				riderCharge=Double.parseDouble(WOPRiderCharge);
			} else {
				riderCharge=0.0;
			}
		}
		CommonConstant.printLog("d", "getRiderCharge rider charge is " + riderCharge);
		return riderCharge;
	}

	/** Vikas - Added for Mahalife Gold & Gold Plus*/

	public void calculateTopUpPartialWithDForPH10() {

		CommonConstant.printLog("d", "Inside calculateTopUpPartialWithDForPH10 calculateCharges");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmSurrenderValue = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		HashMap hmTopUpPremium = new HashMap();
		HashMap hmWithdrawalAmount = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDouble = new HashMap<Integer, Double>();
		String topUpAmount = "";
		String topUpStartYear = "";
		String topUpEndYear = "";
		String topUpAmountYear = "";
		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		boolean negativeCheck = false;
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		double minFundVal = 0.0;
		String fundValErr = "";
		double tempPremium = master.getBaseAnnualizedPremium();
		if (frequency.equals("O")) {
			fundValErr = "The Total Fund Value (Single + Top Up Fund) post your withdrawal is less than an amount equivalent to 5% of the Single Premium paid. Please reduce the amount of withdrawal";
			minFundVal = 0.05 * tempPremium;
		} else {
			fundValErr = "The Total Fund Value (Regular + Top Up Fund) post your withdrawal is less than an amount equivalent to one year�s Annualised Regular Premium. Please reduce the amount of withdrawal";
			minFundVal = tempPremium;
		}
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		if (master.getRequestBean().getFixTopupAmt()!= null && !master.getRequestBean().getFixTopupAmt().equals("")) {
			topUpAmount = master.getRequestBean().getFixTopupAmt();
			topUpStartYear = master.getRequestBean().getFixTopupStYr();
			topUpEndYear = master.getRequestBean().getFixTopupEndYr();
		}
		if (master.getRequestBean().getVarTopupAmtYr()!= null && !master.getRequestBean().getVarTopupAmtYr().equals("")) {
			topUpAmountYear = master.getRequestBean().getVarTopupAmtYr();
			topUpAmountYear = "-" + topUpAmountYear.substring(0, topUpAmountYear.length()-1);
		}
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			negativeCheck = true;
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		}
		if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			negativeCheck = true;
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		long lMortalityCharges = 0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;

		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double mortChargesSTax = 0.0;
		double tempNetHolderAcc=0.0;
		double templMortalityCharges = 0.0;
		double tempFulllMortalityCharges = 0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;
		double topUpPremium=0.0;
		double partialWithdrawal=0.0;
		double topUpInsuredAmount = 0.0;

		double tempPhInvestmentFee=0.0;
		double phInvestFeeSTax=0.0;
		double tempPhNetHolderAcc=0.0;
		double tempFinalPhNetHolderAcc=0.0;
		double tempAdminChargeSTax = 0.0;
		String fundPerStr = (master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals(""))?master.getRequestBean().getFundPerform():"8";
		double fundPerD = (Double.parseDouble(fundPerStr))/100;
		double fundGrowthPM = Math.pow(1+fundPerD,0.0833333333333333333333333)-1;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyCharges=0.0;
		double RiderCharges=0.0;
		double totRiderCharges=0.0;
		double fullTopUpPremium=0.0;
		double fullPartialWithdrawal = 0.0;
		double completeTopUpPremium=0.0;
		double completePartialWithdrawal = 0.0;
		double mainPartialWithdrawal = 0.0;
		double yearlyPartialWithdrawal = 0.0;
		double yearlyTopUpPremium=0.0;
		double serviceTax = master.getServiceTaxMain()/100;

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTax = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			//double FMC = DataAccessClass.getFMC(oFormulaBean.getRequestBean())/100;
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);

			String TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";

			// Calculate Service Tax on Premium Allocation Charges
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTax = totalServiceTax + premiumAllocChargesSTax; // Yearly
																			// Tax
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=0.0;
			if (count<=master.getRequestBean().getPremiumpayingterm()) {
				investmentAmount=Double.parseDouble(TotalInvestment);
			}
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
					+ "";
			long lCommision = Long.parseLong(strCommissionPayble);
			CommonConstant.printLog("d", "For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count, lCommision);
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			// Premium Allocation / Investment Amount Charges Calculation end
			// Other Inbuilt Benefit charges start
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);


			// Other Inbuilt Benefit Monthly charges
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			// Calculate Service Tax on Other Benefit Charges
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTax = totalServiceTax + (otherBenefitSTax * 12); // Yearly
																			// Tax
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			// END
			// Other Inbuilt Benefit charges End
			// Mortility Charges calculation Start
			// Policy Admin Charges calculation Start
			//if (count == 1) {
				//dAdminCharge = DataAccessClass.getADMCharges(pnl_id, master
				//		.getQuotedAnnualizedPremium());
				//lAdminCharge = Math.round(dAdminCharge * 12);
				//hmPolicyAdminCharges.put(1, lAdminCharge);
			//} else {
				//oFormulaBean.setADM(dAdminCharge);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			//}

			// Calculate Service Tax on Admin Charges
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTax = totalServiceTax + (adminChargeSTax * 12); // Yearly
																		// Tax

			CommonConstant.printLog("d", "#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			// END

			double serviceTaxOnInvestmentFee = 0.0;
			lMortalityCharges = 0;
			tempFulllMortalityCharges=0.0;
			fullPartialWithdrawal=0.0;
			fullTopUpPremium=0.0;
			yearlyTopUpPremium=0.0;
			yearlyPartialWithdrawal = 0.0;
			//Addedd by Samina Mohammad iFlex
			for(int i=1;i<=12;i++) {
				//double tempHoldetNettoInvest = investmentAmount - premiumAllocChargesSTax;
				double finalLoyalty=0.0;
				//fullTopUpPremium=0.0;
				//double topupmount=0.0;
				double topupfinaltax=0.0;
				double tempHoldetNettoInvest = investmentAmount ;

				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}

				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					//|| count!=1)
					topUpPremium = 0.0;
					partialWithdrawal = 0.0;
				} else {
					topUpPremium = 0.0;
					if (!topUpAmount.equals("")) {
						if (count>=Integer.parseInt(topUpStartYear) && count<=Integer.parseInt(topUpEndYear)) {
							topUpPremium = Double.parseDouble(topUpAmount);
							yearlyTopUpPremium=topUpPremium;
							/*if (frequency.equalsIgnoreCase("S"))
								topUpPremium/=2;
							if (frequency.equalsIgnoreCase("Q"))
								topUpPremium/=4;*/
							fullTopUpPremium+=topUpPremium;
							if (frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")){
								//topUpPremium=(topUpPremium);
								topupfinaltax=(topUpPremium*.015*(1+serviceTax));
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium;
							} else if (frequency.equalsIgnoreCase("M")){
								topUpPremium=(topUpPremium/12);
								topupfinaltax=(topUpPremium*.015*(1+serviceTax));
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium;
							} else if (frequency.equalsIgnoreCase("S")){
								topUpPremium=(topUpPremium/2);
								topupfinaltax=(topUpPremium*.015*(1+serviceTax));
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium;
							}
							else{
								topUpPremium=(topUpPremium/4);
								topupfinaltax=(topUpPremium*.015*(1+serviceTax));
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium;
							}
						}
						CommonConstant.printLog("d", "tempHoldetNettoInvestTopup" + tempHoldetNettoInvest);
					} if (!topUpAmountYear.equals("")) {
						String[] topUpVar = topUpAmountYear.split("-");
						for (int l=0;l<topUpVar.length;l++) {
							if (topUpVar[l].equals(count+","+topUpVar[l].substring(topUpVar[l].indexOf(",")+1,topUpVar[l].length()))) {
								topUpPremium = Double.parseDouble(topUpVar[l].substring(topUpVar[l].indexOf(",")+1,topUpVar[l].length()));
								yearlyTopUpPremium=topUpPremium;
								/*if (frequency.equalsIgnoreCase("S"))
									topUpPremium/=2;
								if (frequency.equalsIgnoreCase("Q"))
									topUpPremium/=4;
								if (frequency.equalsIgnoreCase("M"))
									topUpPremium/=12;*/
								CommonConstant.printLog("d", "topUpPremium----" + topUpPremium);
								fullTopUpPremium+=topUpPremium;
								if (frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")){
									//topupmount=(topUpPremium);
									topupfinaltax=(topUpPremium*.015*(1+serviceTax));
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium;
								} else if (frequency.equalsIgnoreCase("M")){
									topUpPremium=(topUpPremium/12);
									topupfinaltax=(topUpPremium*.015*(1+serviceTax));
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium;
								} else if (frequency.equalsIgnoreCase("S")){
									topUpPremium=(topUpPremium/2);
									topupfinaltax=(topUpPremium*.015*(1+serviceTax));
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium;
								}
								else{
									topUpPremium=(topUpPremium/4);
									topupfinaltax=(topUpPremium*.015*(1+serviceTax));
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium;
								}
								l=topUpVar.length;
							}
						}
					}
					partialWithdrawal = 0.0;
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
							yearlyPartialWithdrawal = partialWithdrawal;
							/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;
							fullPartialWithdrawal+=partialWithdrawal;*/
							completePartialWithdrawal+=partialWithdrawal;
						}
					} if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							if (withdrawVar[l].equals(count+","+withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()))) {
								//withdrawVar[l].contains(count+",");
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								yearlyPartialWithdrawal = partialWithdrawal;
								/*if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;*/
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							}
						}
					}


				}
				//Samina Changes on 4/08/2014

					/*if(partialWithdrawal!=0 )
					{
							if(frequency.equalsIgnoreCase("O") || frequency.equalsIgnoreCase("M") ){
								if((count-1)%1==0){
									partialWithdrawal=(partialWithdrawal*0.0833333333333333333);
			                       }
							}
							else if(frequency.equalsIgnoreCase("S")){
									if((count-1)%6==0){
										partialWithdrawal=(partialWithdrawal*0.5);
									}
							}
							else if(frequency.equalsIgnoreCase("Q")){
								if((count-1)%3==0){
									partialWithdrawal=(partialWithdrawal*0.25);
								}
						}
							else if(frequency.equalsIgnoreCase("A")){
							if((count-1)%12==0){
								partialWithdrawal=(partialWithdrawal*1);
							}
					}
					}*/

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fullPartialWithdrawal Charges: "
						+ fullPartialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #topUpPremium Charges: "
						+ topUpPremium);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);

				if (topUpPremium!=0.0) {
					if (master.getRequestBean().getInsured_age()+count-1 <45) {
						topUpInsuredAmount += (topUpPremium*1.25);
						//topUpInsuredAmount = completeTopUpPremium*1.25;
					} else if (master.getRequestBean().getInsured_age()+count-1 >=45) {
						topUpInsuredAmount += (topUpPremium*1.1);
						//topUpInsuredAmount = completeTopUpPremium*1.1;
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #topUpInsuredAmount Charges: "
						+ topUpInsuredAmount);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempHoldetNettoInvest Charges: "
						+ tempHoldetNettoInvest);
				oFormulaBean.setA(tempHoldetNettoInvest+tempNetHolderAcc-topUpInsuredAmount);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + "tempPhNetHolderAcc" + tempPhNetHolderAcc + "topUpInsuredAmount"
						+ topUpInsuredAmount);
				oFormulaBean.setCNT(count);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #WDA: "
						+ mainPartialWithdrawal);
				oFormulaBean.setWithdrawalAmount(mainPartialWithdrawal);
				oFormulaBean.setMonthCount(i);
				String MortalityCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityCharges = Double.parseDouble(MortalityCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges: "
						+ templMortalityCharges);
				tempFulllMortalityCharges += templMortalityCharges;
					RiderCharges = getRiderCharges(count,age,ppt,i);
					CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
							+ RiderCharges);


				mortChargesSTax = getServiceTax(templMortalityCharges+RiderCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax: "
						+ mortChargesSTax + " and full Mortc " + tempFulllMortalityCharges);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
				lMortalityCharges = Math.round(tempFulllMortalityCharges);
				CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges: "
						+ lMortalityCharges);
				hmMortalityCharges.put(count, lMortalityCharges);
				totalServiceTax = totalServiceTax + mortChargesSTax;
				tempAdminChargeSTax = getServiceTax(dAdminCharge);
					tempNetHolderInvestment = tempHoldetNettoInvest - templMortalityCharges - mortChargesSTax - dAdminCharge - tempAdminChargeSTax-RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: "
						+ tempNetHolderInvestment);
				tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
				//totalNetHolderInvestment+=tempNetHolderInvestment;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: "
						+ tempPHAccBeforeGrowth);
				oFormulaBean.setA(0.1);

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
						+ fundGrowthPM);

					tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: "
						+ tempPHAccInvestGrowth);
				tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: "
						+ tempPHAccHolder);
				oFormulaBean.setA(tempPHAccHolder);
				String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
						oFormulaBean)
						+ "";
				tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
						+ tempPhInvestmentFee);
				phInvestFeeSTax = getServiceTax(tempPhInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges: "
						+ phInvestFeeSTax);

				totalServiceTax = totalServiceTax + phInvestFeeSTax;
				tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee-phInvestFeeSTax;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
						+ tempPhNetHolderAcc);




				double InvestmentFeeSTax = 0.0;
				phAccount = tempPHAccHolder;
				phInvestmentFee = tempPhInvestmentFee;
				phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
				CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
						+ " #phInvestmentFeeValue: " + phInvestmentFee);

				// Calculate Service Tax on phInvestmentFee
				// InvestmentFeeSTax =
				// getServiceTaxForUnitLink(phInvestmentFee);

				InvestmentFeeSTax = phInvestFeeSTax;
				CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
						+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);

				// CommonConstant.printLog("d","#Policy term : "+count+" #serviceTaxOnInvestmentFee : "
				// + serviceTaxOnInvestmentFee);

				serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
						+ InvestmentFeeSTax;
				netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;

				// Storing regular fund values
				if (i == 12) {
					hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
									oFormulaBean.setA(tempPhNetHolderAcc);
					String tempLoyaltyCharges = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LC,
							oFormulaBean)
							+ "";
					loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
					finalLoyalty=loyaltyCharges;
					hmLoyaltyCharge.put(count,Math.round(finalLoyalty));
					hmRegularFundValue.put(count, Math.round(netPHAccount));
					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #RegularFundValue:  "
							+ Math.round(netPHAccount));

					regularFundValueBeforeFMC = Math.round(netPHAccount
							+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmRegularFundValueBeforeFMC: "
							+ regularFundValueBeforeFMC + " #netPHAccount:  "
							+ netPHAccount + " #phInvestmentFeeTotal: "
							+ phInvestmentFeeTotal
							+ " #serviceTaxOnInvestmentFee: "
							+ serviceTaxOnInvestmentFee);

					hmRegularFundValueBeforeFMC.put(count,
							regularFundValueBeforeFMC);

					long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
							count, netPHAccount);
					hmGuaranteedMaturityAmount.put(count,
							gurananteedMaturityAmount);

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmGuaranteedMaturityAmount: "
							+ gurananteedMaturityAmount);
					CommonConstant.printLog("d", "#Policy Term : " + count + " #TopUpPremium " + fullTopUpPremium);
					hmTopUpPremium.put(count, Math.round(yearlyTopUpPremium));

				}
				if (frequency.equals("M"))
					partialWithdrawal= (partialWithdrawal/12);
				else if (frequency.equals("S"))
					partialWithdrawal= (partialWithdrawal/2);
				else if (frequency.equals("Q"))
					partialWithdrawal= (partialWithdrawal/4);
				fullPartialWithdrawal+=partialWithdrawal;
				if (i==1) {
					CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
					hmWithdrawalAmount.put(count, Math.round(yearlyPartialWithdrawal));
				}
				int currentMonth = (count-1)*12+i;
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					hmWithdrawalAmountDouble.put(currentMonth,0.0);
				} else {
					hmWithdrawalAmountDouble.put(currentMonth,partialWithdrawal);
					mainPartialWithdrawal=0.0;
					if (master.getRequestBean().getInsured_age()+count-1 >=60) {
						for (int k=currentMonth;k>0;k--) {
							int inAge = k%12!=0?k/12+1:k/12;
							if ((hmWithdrawalAmountDouble.get(k)!=null) && (master.getRequestBean().getInsured_age()+inAge-1>=58)){
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDouble.get(k));
								mainPartialWithdrawal+=hmWithdrawalAmountDouble.get(k);
							}
						}
					} else {
						for (int k=currentMonth;k>=currentMonth-23;k--) {
							if (hmWithdrawalAmountDouble.get(k)!=null && k>=1) {
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDouble.get(k));
								mainPartialWithdrawal += hmWithdrawalAmountDouble.get(k);
							}
						}
					}
					CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
							+ mainPartialWithdrawal);
				}
				tempFinalPhNetHolderAcc=tempPhNetHolderAcc+finalLoyalty-partialWithdrawal;
            	regularFundValuePostFMC=Math.round(tempPhNetHolderAcc);
            	if (i==12 && count>5 && negativeCheck) {
	            	CommonConstant.printLog("d", "regularFundValuePostFMC negative or not " + regularFundValuePostFMC);
	            	if (regularFundValuePostFMC < minFundVal && master.getRequestBean().getNegativeError()==null) {
	            		//CommonConstant.printLog("d","regularFundValuePostFMC negative or not "+regularFundValuePostFMC+" Condition "+(regularFundValuePostFMC < minFundVal));
	            		master.getRequestBean().setNegativeError(fundValErr);
	            	}
            	}
            	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);
				tempNetHolderAcc=tempFinalPhNetHolderAcc;

				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAcc);
				if (i==12 && count>5 && negativeCheck) {
					CommonConstant.printLog("d", "totalFundValueAtEndtPolicyYear negative or not " + totalFundValueAtEndtPolicyYear);
					if (totalFundValueAtEndtPolicyYear < minFundVal && master.getRequestBean().getNegativeError()==null) {
						//CommonConstant.printLog("d","regularFundValuePostFMC negative or not "+regularFundValuePostFMC+" Condition "+(regularFundValuePostFMC < minFundVal));
	            		master.getRequestBean().setNegativeError(fundValErr);
	            	}
				}
				long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmSurrenderValue" + surrenderValue);
			// END
				hmSurrenderValue.put(count, surrenderValue);
			}

			//End iFlex

			// Mortility Charges Monthly start
			age++;

			//Added by Samina Mohammad 02-04-2014 for iFlexi
			//netPHInvestment = totalNetHolderInvestment;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));

			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			//Commented by Mohammad Rashid 02-04-2014 for iFlexi

			//totalServiceTax = totalServiceTax + serviceTaxOnInvestmentFee;
			ApplicableServiceTax = Math.round(totalServiceTax);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);

			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			// Calculate Death benefit at the end of policy year
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);


			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
			String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		double lDBEP = Double.parseDouble(strDBEP);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			// END
			// Calculate Commission payble


			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);

			// END

		}
		master.setTopUpLoyalty(hmLoyaltyCharge);
		//master.setTopUpRegularFundValueEndOfYear(hmRegularFundValue);
		master.setRegularFundValuePostFMC(hmRegularFundValuePostFMC);
		master.setTopUpRegularFundValueEndOfYear(hmTotalFundValueAtEndPolicyYear);
		master.setTopUpSurrValue(hmSurrenderValue);
		master.setTopUpDeathBenefit(hmDeathBenefitAtEndofPolicyYear);
		master.setTopUpPremium(hmTopUpPremium);
		master.setWithdrawalAmout(hmWithdrawalAmount);
	}
	//Start : added by jayesh for SMART 7 : 21-05-2014
	public long getRenewalPremium(String formula) {
		long renewalPremium = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(getRenewalModalPremium_A());
			if(master.getRequestBean().getBaseplan().startsWith("MRS"))
				oFormulaBean.setRiderPremium(master.getTotAnnRiderModalPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String renewalPremiumStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					formula, oFormulaBean)
					+ "";
			renewalPremium = Long.parseLong(renewalPremiumStr);
			CommonConstant.printLog("d", "renewal Premium for " + formula + " Calculated :::: " + renewalPremium);

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating renewalPremium Premium of " + formula + "::::" + e + " " + e.getMessage());
		}

		return renewalPremium;
	}
	//added by Ganesh for Fortune Guarantee 07/09/2015
	public long getRenewalPremiumMonthly() {
		long renewalMonthly=0;
		long renewalNSAP = 0;
		renewalMonthly = master.getBaseAnnualizedPremium();
		renewalMonthly = Math.round(renewalMonthly * 0.0883);
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 1" + renewalMonthly);
		if (master.getModalPremiumForNSAP()!=0) {
			renewalNSAP = Math.round(master.getModalPremiumForAnnNSAP() * 0.0883);
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 2" + renewalNSAP);
		}
		if (master.getTotAnnRiderModalPremium()!=0) {
			renewalMonthly = (long) (renewalMonthly + Math.round((master.getTotAnnRiderModalPremium() * 0.0883)));
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 3" + renewalMonthly);
		}
		renewalMonthly += renewalNSAP;
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG" + renewalMonthly);
		return renewalMonthly;
	}
	//quaterly
	public long getRenewalPremiumQuaterly() {
		long renewalQuaterly=0;
		long renewalNSAP = 0;
		renewalQuaterly = master.getBaseAnnualizedPremium();
		renewalQuaterly = Math.round(renewalQuaterly * 0.26);
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 1" + renewalQuaterly);
		if (master.getModalPremiumForNSAP()!=0) {
			renewalNSAP = Math.round(master.getModalPremiumForAnnNSAP() * 0.26);
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 2" + renewalNSAP);
		}
		if (master.getTotAnnRiderModalPremium()!=0) {
			renewalQuaterly = (long) (renewalQuaterly + Math.round((master.getTotAnnRiderModalPremium() * 0.26)));
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 3" + renewalQuaterly);
		}
		renewalQuaterly += renewalNSAP;
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG" + renewalQuaterly);
		return renewalQuaterly;
	}
    //Anuual
    public long getRenewalPremiumAnn() {
        long renewalAnn=0;
        long renewalNSAP = 0;
        renewalAnn = master.getBaseAnnualizedPremium();
        renewalAnn = Math.round(renewalAnn * 1);
        CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 1" + renewalAnn);
        if (master.getModalPremiumForNSAP()!=0) {
            renewalNSAP = Math.round(master.getModalPremiumForAnnNSAP() * 1);
            CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 2" + renewalNSAP);
        }
        if (master.getTotAnnRiderModalPremium()!=0) {
            renewalAnn = (long) (renewalAnn + Math.round((master.getTotAnnRiderModalPremium() * 1)));
            CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 3" + renewalAnn);
        }
        renewalAnn += renewalNSAP;
        CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG" + renewalAnn);
        return renewalAnn;
    }
	//Semi Anuual
	public long getRenewalPremiumSemi() {
		long renewalSemi=0;
		long renewalNSAP = 0;
		renewalSemi = master.getBaseAnnualizedPremium();
		renewalSemi = Math.round(renewalSemi * 0.51);
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 1" + renewalSemi);
		if (master.getModalPremiumForNSAP()!=0) {
			renewalNSAP = Math.round(master.getModalPremiumForAnnNSAP() * 0.51);
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 2" + renewalNSAP);
		}
		if (master.getTotAnnRiderModalPremium()!=0) {
			renewalSemi = (long) (renewalSemi + Math.round((master.getTotAnnRiderModalPremium() * 0.51)));
			CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG 3" + renewalSemi);
		}
		renewalSemi += renewalNSAP;
		CommonConstant.printLog("d", "RENEWAL PREM ANNUAL FG" + renewalSemi);
		return renewalSemi;
	}

	//added by jayesh on 23-05-2014
	private HashMap getLifeCoverageAltSce() {
		HashMap lifeCoverageMap = new HashMap();
		try {

			long lifeCoverage = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				if(rpuyear==0){
					lifeCoverageMap.put(count, 0);
				}
				else if(count<rpuyear){
					String lifeCoverageValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LIFE_COVERAGE, oFormulaBean)
							+ "";
					lifeCoverage = Long.parseLong(lifeCoverageValueStr);
					lifeCoverageMap.put(count, lifeCoverage);
				}
				else
				{
					String lifeCoverageValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LIFE_COVERAGE_RPU, oFormulaBean)
							+ "";
					lifeCoverage = Long.parseLong(lifeCoverageValueStr);
					lifeCoverageMap.put(count, lifeCoverage);
				}
				CommonConstant.printLog("d", "Pol Term :::  lifeCoverage " + count + " : "
						+ lifeCoverage);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating LifeCoverage for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}

		return lifeCoverageMap;
	}
	private HashMap getLifeCoverageAlternateSce() {
		HashMap lifeCoverageMapAltSce = new HashMap();
		try {
			long lifeCoverage = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(getMinimumGaurnteedSAMaturity());
				FormulaHandler formulaHandler = new FormulaHandler();
				if(rpuyear==0){
					lifeCoverageMapAltSce.put(count, 0);
				}
				else if(count<rpuyear){
					String lifeCoverageValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LIFE_COVERAGE_ALTERNATESCE, oFormulaBean)
							+ "";
					lifeCoverage = Long.parseLong(lifeCoverageValueStr);
					lifeCoverageMapAltSce.put(count, lifeCoverage);
				}
				else
				{
					String lifeCoverageValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LIFE_COVERAGE_ALTERNATESCENARIO, oFormulaBean)
							+ "";
					lifeCoverage = Long.parseLong(lifeCoverageValueStr);
					lifeCoverageMapAltSce.put(count, lifeCoverage);
				}
				CommonConstant.printLog("d", "Pol Term :::  lifeCoverageAlternateScenario " + count + " : "
						+ lifeCoverage);
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating LifeCoverage for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return lifeCoverageMapAltSce;
	}
	private double getMinimumGaurnteedSAMaturity(){
		  double minMaturity = 0.0;
		try {
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		//int ppt=master.getRequestBean().getPremiumpayingterm();
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			long basePrimum=0;
			String basePremium = formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(), FormulaConstant.CALCULATE_BASE_ANNUALIZED_PREMIUM, oFormulaBean) + "";
			basePrimum = Long.parseLong(basePremium);
			oFormulaBean.setA(basePrimum);
			if(count<rpuyear){
				String minMaturityStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_SA_ON_MATURITY_ALTSCE, oFormulaBean)
						+ "";
				minMaturity = Double.parseDouble(minMaturityStr);
			}else{
				String minMaturityStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_SA_ON_MATURITY_ALTSCENARIO, oFormulaBean)
						+ "";
				minMaturity = Double.parseDouble(minMaturityStr);
			}
			CommonConstant.printLog("d", "getMinimumGaurnteedSAMaturity vlue::::" + minMaturity);
		}
	} catch (Exception e) {
		CommonConstant.printLog("e", "Error occured in calculating getMinimumGaurnteedSAMaturity for planCode:  "
				+ master.getRequestBean().getBaseplan() + " " + e);
	}
		return minMaturity;
	}
	//added on 23-05-2014
	//Start : added on 23-05-2014
	private HashMap getAltScenarioMaturityValue() {
		HashMap MVMap = new HashMap();
		long maturityValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALT_SCE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Math.round(Double.parseDouble(maturityValueStr));
					MVMap.put(count, maturityValue);
					CommonConstant.printLog("d", "Pol Term :::  ALTSCEMVMap " + count + " : "
							+ maturityValue);
				} else {
					MVMap.put(count, 0);
				}
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating alternate scenario maturity value ULIP" + e + " " + e.getMessage());
			// TODO: handle exception
		}
		return MVMap;
	}
	//End : added on 23-05-2014
	private HashMap getAltScenarioTOTMaturityValue() {
		HashMap MVMap = new HashMap();
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		long TOTmaturityval=0;
		try {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					long maturityValue = 0;
					double maturityVal = 0;
					double gaivalue=0;
					long gaival=0;
					long TotalMatVal=0;
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					long basePrimum=0;
					String basePremium = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_BASE_ANNUALIZED_PREMIUM,
							oFormulaBean)
							+ "";
					basePrimum = Long.parseLong(basePremium);
					oFormulaBean.setA(basePrimum);
					if(count<rpuyear){
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCE,
								oFormulaBean)
								+ "";
						gaival = Long.parseLong(GAIVALUE);
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALTSCE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
					TotalMatVal=gaival+maturityValue;
					MVMap.put(count, maturityValue);
					}else{
						String GAIVALUE=formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_GAI_ALTSCENARIO,
								oFormulaBean)
								+ "";
						gaivalue = Double.parseDouble((GAIVALUE));
						gaival=Math.round(gaivalue);
						String maturityValueStr = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_ALTSCENARIO_MATURITY_VALUE,
								oFormulaBean)
								+ "";
						maturityVal=Double.parseDouble(maturityValueStr);
						TotalMatVal=Math.round(gaivalue+maturityVal);
					}

					MVMap.put(count, TotalMatVal);
					CommonConstant.printLog("d", "Pol Term :::  TOTmaturityval " + count + " : " + TOTmaturityval);
				} else {
					MVMap.put(count, 0);
				}
			}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating alternate scenario total maturity value ULIP" + e + " " + e.getMessage());
		}
		return MVMap;
	}
	//Start : added on 27-05-2014
	private HashMap getAltSceTotalDeathBenefit4() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
			if (rpuyear!=0) {
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				FormulaHandler formulaHandler = new FormulaHandler();
				if(rpuyear>=count){
				oFormulaBean.setCNT(count-1);
				} else {
					oFormulaBean.setCNT(rpuyear-1);
				}
				oFormulaBean.setCNT_MINUS(true);
				String strRBonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";
				//rBonus = Long.parseLong(strRBonus);
				oFormulaBean.setCNT(count-1);
				//oFormulaBean.setA(rBonus);
				oFormulaBean.setA(Double.parseDouble(strRBonus));
				if(rpuyear==0){
					TBMap.put(count, 0);
				}
				else if(count<rpuyear){

					String strTerminalbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_TOT_DEATH_BENF_4,
							oFormulaBean)
							+ "";
						terminalBonus = Long.parseLong(strTerminalbonus);
						TBMap.put(count, terminalBonus);
				}
				else{

					String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ALTSCE_TOT_DEATH_BENF_4,
						oFormulaBean)
						+ "";
					terminalBonus = Long.parseLong(strTerminalbonus);
					TBMap.put(count, terminalBonus);
				}

				CommonConstant.printLog("d", "Pol Term :::  total death benifit 4 (alternate scenario) " + count + " : "
						+ terminalBonus);
			}
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getAltSceTotalDeathBenefit4 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltSceTotalDeathBenefit4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}
	private HashMap getAltSceTotalDeathBenefit8() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus=0;
			//long rBonus=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
			if (rpuyear!=0) {
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				FormulaHandler formulaHandler = new FormulaHandler();
				if(rpuyear>=count){
				oFormulaBean.setCNT(count-1);
				} else {
					oFormulaBean.setCNT(rpuyear-1);
				}
				oFormulaBean.setCNT_MINUS(true);
				String strRBonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";
				//rBonus = Long.parseLong(strRBonus);
				oFormulaBean.setCNT(count-1);
				//oFormulaBean.setA(rBonus);
				oFormulaBean.setA(Double.parseDouble(strRBonus));
				if(rpuyear==0){
					TBMap.put(count, 0);
				}
				else if(count<rpuyear){
					//oFormulaBean.setCNT(count);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_8,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				}
				else{
						String strTerminalbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ALTSCE_TOT_DEATH_BENF_8,
						oFormulaBean)
						+ "";
					terminalBonus = Long.parseLong(strTerminalbonus);
					TBMap.put(count, terminalBonus);
				}
				CommonConstant.printLog("d", "Pol Term :::  Total Death Benefit 8 (alternate scenario)" + count + " : "
						+ terminalBonus);
			}
			}
		} catch (Exception e) {
			//CommonConstant.printLog("d","Exception occured in getAltSceTotalDeathBenefit8 "+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltSceTotalDeathBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}
	//End : added on 27-05-2014
	//added on 28-05-2014
	private HashMap getAltScenarioMaturityValue4() {
		HashMap MVMap = new HashMap();
		//long rBonus=0;
		long maturityValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		try {
			if (rpuyear!=0) {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					if(rpuyear==0){
						oFormulaBean.setCNT(0);
					}
					else if(rpuyear>=count){
						oFormulaBean.setCNT(count);
						}
						else{
						oFormulaBean.setCNT(rpuyear-1);
						}
						FormulaHandler formulaHandler = new FormulaHandler();
					String strRBonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_VRSBONUS_4,
								oFormulaBean)
								+ "";
					//rBonus = Long.parseLong(strRBonus);
					oFormulaBean.setCNT(count-1);
					//oFormulaBean.setA(rBonus);
					oFormulaBean.setA(Double.parseDouble(strRBonus));

					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALTSCE_MATURITY_BENEFIT_4,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
					MVMap.put(count, maturityValue);
					CommonConstant.printLog("d", "Pol Term :::  ALTSCEMVMap " + count + " : "
							+ maturityValue);

				} else {
					MVMap.put(count, 0);
				}
				CommonConstant.printLog("d", "Pol Term :::  Maturity Value 4 (alternate scenario)" + count + " : " + maturityValue);
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//System.out
			//.println("Exception occured in getAltScenarioMaturityValue4 "
				//	+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltScenarioMaturityValue4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return MVMap;
	}

	private HashMap getAltScenarioMaturityValue8() {
		HashMap MVMap = new HashMap();
		//long rBonus=0;
		long maturityValue = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int rpuyear = master.getRequestBean().getRpu_year()!=null?Integer.parseInt(master.getRequestBean().getRpu_year()):0;
		try {
			if (rpuyear!=0) {
			for (int count = 1; count <= polTerm; count++) {
				if (count == polTerm) {
					oFormulaBean = getFormulaBeanObj();
					if(rpuyear==0){
						oFormulaBean.setCNT(0);
					}
					else if(rpuyear>=count){
						oFormulaBean.setCNT(count);
						}
						else{
						oFormulaBean.setCNT(rpuyear-1);
						}
					oFormulaBean.setCNT_MINUS(true);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strRBonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_VRSBONUS_8,
							oFormulaBean)
							+ "";
					//rBonus = Long.parseLong(strRBonus);
					oFormulaBean.setCNT(count-1);
					//oFormulaBean.setA(rBonus);
					oFormulaBean.setA(Double.parseDouble(strRBonus));

					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_ALTSCE_MATURITY_BENEFIT_8,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
					MVMap.put(count, maturityValue);
					CommonConstant.printLog("d", "Pol Term :::  ALTSCEMVMap " + count + " : "
							+ maturityValue);

				} else {
					MVMap.put(count, 0);
				}
				CommonConstant.printLog("d", "Pol Term :::  Maturity Value 8 (alternate scenario)" + count + " : " + maturityValue);
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//System.out
			//.println("Exception occured in getAltScenarioMaturityValue8 "
				//	+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltScenarioMaturityValue8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return MVMap;
	}
	//added on 04-06-2014
	private HashMap getAltScenarioTotMatBenCurrentRate() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);

			FormulaHandler formulaHandler = new FormulaHandler();
			if(count==polTerm){
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLMV1N1") || master.getRequestBean().getBaseplan().equals("MMXV1N1") || master.getRequestBean().getBaseplan().equals("SMART7V1N1")){
					oFormulaBean.setCNT(count);
				    double GAIVAL=calculateGAI(count);
					oFormulaBean.setA(GAIVAL);
					String strEbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_EBTOTMATVAL,
							oFormulaBean)
							+ "";
					surrenderBenefit=Long.parseLong(strEbonus);
					CommonConstant.printLog("d", "Pol Term :::  GP EB MATVAL " + count + " : "
							+ surrenderBenefit);
					SBMap.put(count, surrenderBenefit);
				}
			}else{
				SBMap.put(count, 0);
			}

		}
		} catch (Exception e) {
			//System.out
			//.println("Exception occured in getAltScenarioTotMatBenCurrentRate "
				//	+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltScenarioTotMatBenCurrentRate for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	private HashMap getAltScenarioSurrenderBenefit_CurrentRate() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		double accumulatedGAI=0;
		try{
		int polTerm = master.getRequestBean().getPolicyterm();
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count-1);
			oFormulaBean.setCNT_MINUS(true);
			if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MMXV1N1"))
			{
			if(master.getRequestBean().getBaseplan().equals("MLGPV1N1")) {
				double GAIValue = calculateGAI(count-1);
				accumulatedGAI+=GAIValue;
			}
			oFormulaBean.setA(accumulatedGAI);
			FormulaHandler formulaHandler = new FormulaHandler();
			String strVsrbonus = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.ALTSCE_EBONUSCUMULATIVE_SUM,
					oFormulaBean)
					+ "";
			//vsrbonus = Long.parseLong(strVsrbonus);
			if (master.getRequestBean().getBaseplan().equals("MMXV1N1"))
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.ALTSCE_EB_SBCR, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			SBMap.put(count, surrenderBenefit);
			CommonConstant.printLog("d", "Pol Term :::  Alternate Scenario Surr Benefit 8_GP" + count + " : "
                    + surrenderBenefit);
			}
			else {

			oFormulaBean.setCNT(count-1);
			oFormulaBean.setCNT_MINUS(true);
			FormulaHandler formulaHandler = new FormulaHandler();
			String strVsrbonus = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.ALTSCE_EBONUSCUMULATIVE_SUM,
					oFormulaBean)
					+ "";
			//vsrbonus = Long.parseLong(strVsrbonus);
			if (master.getRequestBean().getBaseplan().equals("MLMV1N1"))
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
						FormulaConstant.ALTSCE_SBCR, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);

			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Alternate Scenario Surr Benefit 8" + count + " : "
                    + surrenderBenefit);

		}}
		}
		 catch (Exception e) {
			//System.out
				//	.println("Exception occured in getAltScenarioSurrenderBenefit_CurrentRate "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating alternate scenario getAltScenarioSurrenderBenefit_CurrentRate for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	//added on 05-06-2014
	public HashMap getAltScenarioPremiumPaid_Cumulative_CurrentRate() {
		HashMap hmPremPaidCumulative = new HashMap();
		try {
			long vsrbonus;
			long termBonus=0;
			double dvsrbonus;
			long premPaidcumulativeTotal=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1")  || master.getRequestBean().getBaseplan().equals("MMXV1N1") || master.getRequestBean().getBaseplan().equals("SMART7V1N1")){
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strEbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_EBONUSCUMULATIVE,
							oFormulaBean)
							+ "";
					vsrbonus=Long.parseLong(strEbonus);
					if (master.getRequestBean().getBaseplan().equals("MMXV1N1")) {
						String strETerbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.ALTSCE_EBONUSTB,
								oFormulaBean)
								+ "";
						termBonus=Long.parseLong(strETerbonus);
					}
					premPaidcumulativeTotal+=vsrbonus;
					CommonConstant.printLog("d", "Pol Term :::  GP EBONUS " + count + " : "
							+ premPaidcumulativeTotal);
					hmPremPaidCumulative.put(count, premPaidcumulativeTotal+termBonus);
				}

				else{
					premPaidcumulativeTotal=0;
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count);
					oFormulaBean.setCNT_MINUS(false);

					FormulaHandler formulaHandler = new FormulaHandler();
					String strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_VBCUMMULATIVECR,
							oFormulaBean)
							+ "";
					dvsrbonus = Double.parseDouble(strVsrbonus);
					vsrbonus=Math.round(dvsrbonus);
					CommonConstant.printLog("d", "Pol Term :::  VSRBonus8 " + count + " : "
							+ vsrbonus);
					premPaidcumulativeTotal=premPaidcumulativeTotal+vsrbonus;

					CommonConstant.printLog("d", "Pol Term :::  premPaidcumulativeTotal " + count + " : "
							+ premPaidcumulativeTotal);
					hmPremPaidCumulative.put(count, premPaidcumulativeTotal);
				}

			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in premPaidcumulativeTotal "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating premPaidcumulativeTotal for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return hmPremPaidCumulative;
	}



	public void calculateChargesForFPPH10() {
		CommonConstant.printLog("d", "Inside calculateCharges for Fund Performance");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmSurrenderValue = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge

		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		long lMortalityCharges = 0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;

		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double mortChargesSTax = 0.0;
		double tempNetHolderAcc=0.0;
		double templMortalityCharges = 0.0;
		double tempFulllMortalityCharges = 0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;

		double tempPhInvestmentFee=0.0;
		double phInvestFeeSTax=0.0;
		double tempPhNetHolderAcc=0.0;
		double tempFinalPhNetHolderAcc=0.0;
		double tempAdminChargeSTax = 0.0;
		String fundPerStr = master.getRequestBean().getFundPerform();
		double fundPerD = (Double.parseDouble(fundPerStr))/100;

		double fundGrowthPM = Math.pow(1+fundPerD,0.0833333333333333333333)-1;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyCharges=0.0;
		double RiderCharges=0.0;



		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTax = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#ForFundPerformance #calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			// Calculate Service Tax on Premium Allocation Charges
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTax = totalServiceTax + premiumAllocChargesSTax; // Yearly
																			// Tax
			CommonConstant.printLog("d", "#ForFundPerformance #calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
					+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			CommonConstant.printLog("d", "#ForFundPerformance For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			// Premium Allocation / Investment Amount Charges Calculation end
			// Other Inbuilt Benefit charges start
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#ForFundPerformance #calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);


			// Other Inbuilt Benefit Monthly charges
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#ForFundPerformance #calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			// Calculate Service Tax on Other Benefit Charges
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTax = totalServiceTax + (otherBenefitSTax * 12); // Yearly
																			// Tax
			CommonConstant.printLog("d", "#ForFundPerformance #calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			// END
			// Other Inbuilt Benefit charges End
			// Mortility Charges calculation Start
			// Policy Admin Charges calculation Start
			//if (count == 1) {
				//dAdminCharge = DataAccessClass.getADMCharges(pnl_id, master
				//		.getQuotedAnnualizedPremium());
				//lAdminCharge = Math.round(dAdminCharge * 12);
				//hmPolicyAdminCharges.put(1, lAdminCharge);
			//} else {
				//oFormulaBean.setADM(dAdminCharge);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			//}

			// Calculate Service Tax on Admin Charges
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTax = totalServiceTax + (adminChargeSTax * 12); // Yearly
																		// Tax

			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			// END

			double serviceTaxOnInvestmentFee = 0.0;
			lMortalityCharges = 0;
			tempFulllMortalityCharges=0.0;
			//Addedd by Samina Mohammad iFlex
			for(int i=1;i<=12;i++) {
				//double tempHoldetNettoInvest = investmentAmount - premiumAllocChargesSTax;
				double finalLoyalty=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}

			      oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAcc);
			oFormulaBean.setCNT(count);
			oFormulaBean.setMonthCount(i);
			String MortalityCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
					+ "";
			templMortalityCharges = Double.parseDouble(MortalityCharges);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #Mortality Charges: "
					+ templMortalityCharges);
			tempFulllMortalityCharges += templMortalityCharges;
				RiderCharges = getRiderCharges(count,age,ppt,i);
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);


			mortChargesSTax = getServiceTax(templMortalityCharges+RiderCharges);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax: "
					+ mortChargesSTax + " and full Mortc " + tempFulllMortalityCharges);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
				lMortalityCharges = Math.round(tempFulllMortalityCharges);
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " #Final lMortalityCharges: "
						+ lMortalityCharges);
				hmMortalityCharges.put(count, lMortalityCharges);
			totalServiceTax = totalServiceTax + mortChargesSTax;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
				tempNetHolderInvestment = tempHoldetNettoInvest - templMortalityCharges - mortChargesSTax - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: "
					+ tempNetHolderInvestment);
			tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
			//totalNetHolderInvestment+=tempNetHolderInvestment;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: "
					+ tempPHAccBeforeGrowth);
			oFormulaBean.setA(0.1);

			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
					+ fundGrowthPM);

				tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: "
					+ tempPHAccInvestGrowth);
			tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: "
					+ tempPHAccHolder);
			oFormulaBean.setA(tempPHAccHolder);
			String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
					oFormulaBean)
					+ "";
			tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
					+ tempPhInvestmentFee);
			phInvestFeeSTax = getServiceTax(tempPhInvestmentFee);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges: "
					+ phInvestFeeSTax);

			totalServiceTax = totalServiceTax + phInvestFeeSTax;
			tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee-phInvestFeeSTax;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
					+ tempPhNetHolderAcc);




			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolder;
			phInvestmentFee = tempPhInvestmentFee;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);

			// Calculate Service Tax on phInvestmentFee
			// InvestmentFeeSTax =
			// getServiceTaxForUnitLink(phInvestmentFee);

			InvestmentFeeSTax = phInvestFeeSTax;
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);

			// CommonConstant.printLog("d","#Policy term : "+count+" #serviceTaxOnInvestmentFee : "
			// + serviceTaxOnInvestmentFee);

			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;

			// Storing regular fund values
			if (i == 12) {
					hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					RiderCharges=0;
										oFormulaBean.setA(tempPhNetHolderAcc);
						String tempLoyaltyCharges = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
					 finalLoyalty=loyaltyCharges;
					 hmLoyaltyCharge.put(count,Math.round(finalLoyalty));
				hmRegularFundValue.put(count, Math.round(netPHAccount+finalLoyalty));
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));

				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);

				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);

				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);

				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);

				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAcc);
	        	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
	        	CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);

				}
	            tempFinalPhNetHolderAcc=tempPhNetHolderAcc+finalLoyalty;

				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);
				tempNetHolderAcc=tempFinalPhNetHolderAcc;
				totalFundValueAtEndtPolicyYear = Math
				.round(tempNetHolderAcc);


				long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc);
				CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
						+ " #hmSurrenderValue" + surrenderValue);
			// END

				hmSurrenderValue.put(count, surrenderValue);





			}

			//End iFlex

			// Mortility Charges Monthly start
			age++;

			//Added by Samina Mohammad 02-04-2014 for iFlexi
			//netPHInvestment = totalNetHolderInvestment;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
		 CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));

			 CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			//Commented by Mohammad Rashid 02-04-2014 for iFlexi

			//totalServiceTax = totalServiceTax + serviceTaxOnInvestmentFee;
			ApplicableServiceTax = Math.round(totalServiceTax);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);

			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));

			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			// Calculate Death benefit at the end of policy year
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);

			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);


			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
			String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		double lDBEP = Double.parseDouble(strDBEP);

			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			// END
			// Calculate Commission payble


			CommonConstant.printLog("d", "#ForFundPerformance #Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);

			// END

		}
		master.setLoyaltyChargeforFP(hmLoyaltyCharge);
		master.setRegularFundValueFP(hmRegularFundValue);
		master.setSurrenderValueFP(hmSurrenderValue);
		master.setDeathBenifitAtEndOfPolicyYearFP(hmDeathBenefitAtEndofPolicyYear);
	}
		public HashMap getAltScenarioTaxSaving() {
		HashMap TSMap = new HashMap();
		try {
		double finalTax=0;
		long taxsave;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);

			FormulaHandler formulaHandler = new FormulaHandler();
			if ("Y".equalsIgnoreCase(master.getRequestBean().getTata_employee()) && (count==1)) {
				oFormulaBean.setQAP(getDiscountedPremium());
			}
			if(ppt>=count){
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("SEC7V1N1") || master.getRequestBean().getBaseplan().equals("FGV1N1") || master.getRequestBean().getBaseplan().equals("MLMV1N1") || master.getRequestBean().getBaseplan().equals("MMXV1N1") || master.getRequestBean().getBaseplan().equals("SMART7V1N1")|| master.getRequestBean().getBaseplan().equals("MRSP1N1V2")  || master.getRequestBean().getBaseplan().equals("MRSP1N2V2")  || master.getRequestBean().getBaseplan().equals("MRSP2N1V2")  || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")){
					String taxSaveStr = formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(), FormulaConstant.ALTSCE_TAXSAVING_GP, oFormulaBean) + "";
					finalTax = Double.parseDouble(taxSaveStr);
					taxsave=Math.round(finalTax);
					CommonConstant.printLog("d", "Pol Term :::  Tax Saving (alternate scenario) GP" + count + " : "
							+ taxsave);
				}
				else
				{
				String taxSaveStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ALTSCE_TAXSAVING, oFormulaBean)
						+ "";
				finalTax = Double.parseDouble(taxSaveStr);
				taxsave=Math.round(finalTax);

				CommonConstant.printLog("d", "Pol Term :::  Tax Saving (alternate scenario)" + count + " : "
						+ taxsave);
				}
				TSMap.put(count, taxsave);
			}
			else{
				TSMap.put(count, 0);
			}
		}
		}
		catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Tax Saving (alternate scenario) for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TSMap;
	}
	public HashMap getAltScenarioAnnEffPrem() {
		HashMap AEPMap = new HashMap();
		try {
		long aep;
		double finalAep=0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);

			FormulaHandler formulaHandler = new FormulaHandler();
			if ("Y".equalsIgnoreCase(master.getRequestBean().getTata_employee()) && (count==1)) {
				oFormulaBean.setQAP(getDiscountedPremium());
			}else{
				oFormulaBean.setQAP(getBasePremium());
			}
			if(ppt>=count){
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("SEC7V1N1") || master.getRequestBean().getBaseplan().equals("FGV1N1") || master.getRequestBean().getBaseplan().equals("MLMV1N1") || master.getRequestBean().getBaseplan().equals("MMXV1N1") || master.getRequestBean().getBaseplan().equals("MRSP1N1V2")  || master.getRequestBean().getBaseplan().equals("MRSP1N2V2")  || master.getRequestBean().getBaseplan().equals("MRSP2N1V2")  || master.getRequestBean().getBaseplan().equals("MRSP2N2V2")){
					String AEPStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_AEP_GP, oFormulaBean)
							+ "";
					finalAep=Double.parseDouble(AEPStr);
					aep = Math.round(finalAep);
					CommonConstant.printLog("d", "Pol Term :::  Annualized Effective Premium (alternate scenario)" + count + " : " + aep);
				}
				else {
				String AEPStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ALTSCE_AEP, oFormulaBean)
						+ "";
				finalAep=Double.parseDouble(AEPStr);
				aep = Math.round(finalAep);

				CommonConstant.printLog("d", "Pol Term :::  Annualized Effective Premium (alternate scenario)" + count + " : "
						+ aep);
				}
				AEPMap.put(count, aep);
			}
			else{
				AEPMap.put(count, 0);
			}
		}
		}
		catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Annualized Effective Premium (alternate scenario) for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return AEPMap;
	}
	public HashMap getAltScenarioCumulativeEffPrem() {
		HashMap CEPMap = new HashMap();
		try {
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		double tempcep=0;
		double finaltempCep=0;
		long finalCep=0;
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);

			FormulaHandler formulaHandler = new FormulaHandler();
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee()) && (count==1)) {
				oFormulaBean.setQAP(getDiscountedPremium());
			}else{
				oFormulaBean.setQAP(getBasePremium());
			}
			String CEPStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ALTSCE_CEP, oFormulaBean)
						+ "";
			tempcep = Double.parseDouble(CEPStr);
			if(count>ppt){
				tempcep=0;
			}
			finaltempCep+=tempcep;
			finalCep=Math.round(finaltempCep);
			CEPMap.put(count, finalCep);

			CommonConstant.printLog("d", "Pol Term :::  Cumulative Effective Premium (alternate scenario)" + count + " : " + finalCep);
		}
		}
		catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Cumulative Effective Premium (alternate scenario) for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return CEPMap;
	}
	//added on 09-06-2014
	private HashMap getAltScenario4MaturityBenefitTax() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double TotalGAISum = getTotalGAISum();
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(TotalGAISum);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			if(count==polTerm){
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ALTSCE_MAT4BEN_TAX, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);

			}
			else{
				surrenderBenefit=0;
			}

			SBMap.put(count, surrenderBenefit);
			CommonConstant.printLog("d", "Pol Term :::  Total Maturity Benefit tax (alternate scenario)" + count + " : "
					+ surrenderBenefit);
			}
		}
		catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating getAltScenario4MaturityBenefitTax (alternate scenario) for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}

	//Start : added by jayesh for SMART 7 : 21-05-2014

	public void calculateAlternateChargesAAAPH6() {//Added by Samina for Alternate Scenrio AAA
		CommonConstant.printLog("d", "Inside calculateChargesAAA PH6");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		HashMap hmTotalFundValueAtEndPolicyYearLCEF_PH10 = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYearWLEF_PH10 = new HashMap();
		HashMap hmSurrenderValuePH10_AS3 = new HashMap();
		double mainPartialWithdrawal = 0.0;
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		} else if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		double tempAdminChargeSTax = 0.0;
		long lMortalityChargesAS3 = 0;
		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;
		double mortChargesSTaxAS3 = 0.0;
		double tempNetHolderAccAS3=0.0;
		double templMortalityChargesAS3 = 0.0;
		double tempFulllMortalityChargesAS3 = 0.0;
		double tempNetHolderInvestmentAS3 = 0.0;
		double tempPHAccBeforeGrowthAS3 = 0.0;
		double tempPHAccInvestGrowthAS3=0.0;
		double tempPHAccHolderAS3=0.0;
		double tempPhInvestmentFeeAS3=0.0;
		double phInvestFeeSTaxAS3=0.0;
		double tempPhNetHolderAccAS3=0.0;
		double tempFinalPhNetHolderAccAS3=0.0;
		double fundGrowthPM = 0.00327373978220;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyChargesAS3=0.0;
		double finalLoyaltyAS3=0.0;
		double finalDebtLoyaltyAS3=0.0;
		double finalEquityLoyaltyAS3 = 0.0;
		double RiderCharges=0.0;
		double quarterForAAA = 10.0;
		double beforeSwitchEquity = 0.0;
		double beforeSwitchDebt = 0.0;
		double afterSwitchEquity = 0.0;
		double afterSwitchDebt = 0.0;
		double FMCFactor = 0.0 ;
		HashMap hmWithdrawalAmountAAA = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDoubleAAA = new HashMap<Integer, Double>();
		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTaxAS3 = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTaxAS3 = totalServiceTaxAS3 + premiumAllocChargesSTax;
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
							+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			CommonConstant.printLog("d", "For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTaxAS3 = totalServiceTaxAS3 + (otherBenefitSTax * 12);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTaxAS3 = totalServiceTaxAS3 + (adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			double serviceTaxOnInvestmentFee = 0.0;
			tempFulllMortalityChargesAS3=0.0;
			double fullPartialWithdrawal = 0.0;
			double completePartialWithdrawal = 0.0;
			double topUpPremium=0.0;
			double partialWithdrawal=0.0;
			for(int i=1;i<=12;i++) {
				finalLoyaltyAS3=0.0;
				finalDebtLoyaltyAS3=0.0;
				finalEquityLoyaltyAS3=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}

				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
							|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && (i!=1))){
					topUpPremium = 0.0;
					partialWithdrawal = 0.0;
				} else {
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
									/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;*/
							fullPartialWithdrawal+=partialWithdrawal;
							completePartialWithdrawal+=partialWithdrawal;
						} else {
							partialWithdrawal = 0.0;
						}
					} else if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							if (withdrawVar[l].contains(count+",")) {
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							} else {
								partialWithdrawal = 0.0;
							}
						}
					}
			}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #topUpPremium Charges: "
                        + topUpPremium);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
                        + partialWithdrawal);
			 oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAccAS3);
				oFormulaBean.setCNT(count);
				oFormulaBean.setWithdrawalAmount(mainPartialWithdrawal);
				oFormulaBean.setMonthCount(i);
				String MortalityChargesAS3 = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityChargesAS3 = Double.parseDouble(MortalityChargesAS3);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges AS3: "
						+ templMortalityChargesAS3);
				tempFulllMortalityChargesAS3 += templMortalityChargesAS3;
				RiderCharges = getRiderCharges(count, age, ppt, i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);
			mortChargesSTaxAS3 = getServiceTax(templMortalityChargesAS3+RiderCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax AS3: "
					+ mortChargesSTaxAS3 + " and full Mortc AS3 " + tempFulllMortalityChargesAS3);
			lMortalityChargesAS3 = Math.round(tempFulllMortalityChargesAS3);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges AS3: "
					+ lMortalityChargesAS3);
			hmMortalityCharges.put(count, lMortalityChargesAS3);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
			totalServiceTaxAS3 = totalServiceTaxAS3 + mortChargesSTaxAS3;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
			tempNetHolderInvestmentAS3 = tempHoldetNettoInvest - templMortalityChargesAS3 - mortChargesSTaxAS3 - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges AS3: "
					+ tempNetHolderInvestmentAS3);
			tempPHAccBeforeGrowthAS3 = tempNetHolderAccAS3 + tempNetHolderInvestmentAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges AS3: "
					+ tempPHAccBeforeGrowthAS3);
			oFormulaBean.setA(0.1);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges AS3: "
					+ fundGrowthPM);
			tempPHAccInvestGrowthAS3=(tempPHAccBeforeGrowthAS3*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges AS3: "
					+ tempPHAccInvestGrowthAS3);
			tempPHAccHolderAS3=tempPHAccBeforeGrowthAS3+tempPHAccInvestGrowthAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges AS3: "
					+ tempPHAccHolderAS3);
			double DebtFundBeforeFMC=0.0;
			double EquityFundBeforeFMC=0.0;
			if (count*12+i-12 > polTerm*12-30) {
				if (quarterForAAA==10.0) {
					beforeSwitchEquity = DataAccessClass.getAAAEFV(master.getRequestBean(), polTerm - 2);
					beforeSwitchDebt = 100-beforeSwitchEquity;
					afterSwitchEquity = beforeSwitchEquity - (beforeSwitchEquity * (1.0 / quarterForAAA));
					afterSwitchDebt = beforeSwitchDebt + (beforeSwitchEquity -  afterSwitchEquity);
				} else if (i==1 || i == 4 || i == 7 || i == 10) {
					beforeSwitchEquity = afterSwitchEquity;
					beforeSwitchDebt = afterSwitchDebt;
					afterSwitchEquity = beforeSwitchEquity - (beforeSwitchEquity * (1.0 / quarterForAAA));
					afterSwitchDebt = beforeSwitchDebt + (beforeSwitchEquity -  afterSwitchEquity);
				}
				FMCFactor = (afterSwitchEquity * 0.012) + (afterSwitchDebt * 0.008);
				tempPhInvestmentFeeAS3 = tempPHAccHolderAS3 * FMCFactor / 1200;
				DebtFundBeforeFMC = tempPHAccHolderAS3 * afterSwitchDebt / 100;
				EquityFundBeforeFMC = tempPHAccHolderAS3 * afterSwitchEquity / 100;
				if (i==3 || i == 6 || i == 9 || i == 12)
					quarterForAAA--;
			} else {
				oFormulaBean.setA(tempPHAccHolderAS3);
				String tempPhInvestmentFeeValueAS3 = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_PH_INVESTMENT_FEE_AAA,
						oFormulaBean)
						+ "";
				tempPhInvestmentFeeAS3 = Double.parseDouble(tempPhInvestmentFeeValueAS3);
				oFormulaBean.setA(tempPHAccHolderAS3);
				String DBFMC = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.DEBT_FUND_BEFORE_FMC,
						oFormulaBean)
						+ "";
				DebtFundBeforeFMC=Double.parseDouble(DBFMC);
				String EBFMC = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.EQUITY_FUND_BEFORE_FMC,
						oFormulaBean)
						+ "";
				EquityFundBeforeFMC =  Double.parseDouble(EBFMC);
			}
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #beforeSwitchEquity: " + beforeSwitchEquity);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #beforeSwitchDebt: " + beforeSwitchDebt);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #afterSwitchEquity: " + afterSwitchEquity);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #afterSwitchDebt: " + afterSwitchDebt);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #DebtFundBeforeFMC: " + DebtFundBeforeFMC);

			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges AS3: "
					+ tempPhInvestmentFeeAS3);
			phInvestFeeSTaxAS3 = getServiceTax(tempPHAccHolderAS3*0.0135/12);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges AS3: "
					+ phInvestFeeSTaxAS3);
			totalServiceTaxAS3 = totalServiceTaxAS3 + phInvestFeeSTaxAS3;
			tempPhNetHolderAccAS3=tempPHAccHolderAS3-tempPhInvestmentFeeAS3-phInvestFeeSTaxAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges AS3: "
					+ tempPhNetHolderAccAS3);
			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolderAS3;
			phInvestmentFee = tempPhInvestmentFeeAS3;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);
			InvestmentFeeSTax = phInvestFeeSTaxAS3;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);
			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;

			double  phInvestmentFeeLCEF=DebtFundBeforeFMC*0.8/1200;
			double InvestmentFeeSTaxLCEF=getServiceTax(DebtFundBeforeFMC*0.0135/12);
			double DebtFundAfterFMC=DebtFundBeforeFMC - phInvestmentFeeLCEF - InvestmentFeeSTaxLCEF;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #DebtFundAfterFMC: " + DebtFundAfterFMC);
			double NetPHHolderDebtAccount=DebtFundAfterFMC- (partialWithdrawal* DataAccessClass.getAAADFV(master.getRequestBean(), count));
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #NetPHHolderDebtAccount: " + NetPHHolderDebtAccount);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #EquityFundBeforeFMC: " + EquityFundBeforeFMC);
			double  phInvestmentFeeWLEF=EquityFundBeforeFMC*1.2/1200;
			double InvestmentFeeSTaxWLEF=getServiceTax(EquityFundBeforeFMC*0.0135/12);
			double EquityFundAfterFMC=EquityFundBeforeFMC - phInvestmentFeeWLEF - InvestmentFeeSTaxWLEF;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #EquityFundAfterFMC: " + EquityFundAfterFMC);
			double NetPHHolderEquityAccount=EquityFundAfterFMC-(partialWithdrawal* DataAccessClass.getAAAEFV(master.getRequestBean(), count));
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #NetPHHolderEquityAccount: " + NetPHHolderEquityAccount);
			if (i == 12) {
					hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					RiderCharges=0;
					oFormulaBean.setA(tempPhNetHolderAccAS3);
					String tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LC,
							oFormulaBean)
							+ "";
					loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
				 finalLoyaltyAS3=loyaltyChargesAS3;
				 oFormulaBean.setA(DebtFundAfterFMC);
					tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LC,
							oFormulaBean)
							+ "";
					loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
					finalDebtLoyaltyAS3=loyaltyChargesAS3;
					NetPHHolderDebtAccount=DebtFundAfterFMC+finalDebtLoyaltyAS3- (partialWithdrawal* DataAccessClass.getAAADFV(master.getRequestBean(), count));

					oFormulaBean.setA(EquityFundAfterFMC);
					tempLoyaltyChargesAS3 = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LC,
							oFormulaBean)
							+ "";
					loyaltyChargesAS3=Double.parseDouble(tempLoyaltyChargesAS3);
					finalEquityLoyaltyAS3=loyaltyChargesAS3;
					NetPHHolderEquityAccount=EquityFundAfterFMC+finalEquityLoyaltyAS3-(partialWithdrawal* DataAccessClass.getAAAEFV(master.getRequestBean(), count));
				hmLoyaltyCharge.put(count,Math.round(finalLoyaltyAS3));
				hmRegularFundValue.put(count, Math.round(netPHAccount));
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));
				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);
				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);
				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAccAS3);
	        	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
	        	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges AS3: "
						+ tempFinalPhNetHolderAccAS3);
	        	hmTotalFundValueAtEndPolicyYearLCEF_PH10.put(count, Math.round(NetPHHolderDebtAccount));
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetPHHolderDebtAccount Charges AS3: "
						+ NetPHHolderDebtAccount);
            	hmTotalFundValueAtEndPolicyYearWLEF_PH10.put(count, Math.round(NetPHHolderEquityAccount));
            	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetPHHolderEquityAccount Charges AS3: "
						+ NetPHHolderEquityAccount);
				}
			if (i==1) {
				CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
				hmWithdrawalAmountAAA.put(count, Math.round(fullPartialWithdrawal));
				hmWithdrawalAmountDoubleAAA.put(count,fullPartialWithdrawal);
				mainPartialWithdrawal=0.0;
				if (master.getRequestBean().getInsured_age()+count-1 >=60) {
					for (int k=58-master.getRequestBean().getInsured_age()+1;k<=master.getRequestBean().getInsured_age()+count-1;k++) {
						if (hmWithdrawalAmountAAA.get(k)!=null) {
							//CommonConstant.printLog("d","For Year "+k+" Withdrawal is "+hmWithdrawalAmountDoubleAAA.get(k));
							mainPartialWithdrawal+=hmWithdrawalAmountDoubleAAA.get(k);
						}
					}
				} else {
					for (int k=count-1;k<=count;k++) {
						if (hmWithdrawalAmountAAA.get(k)!=null && k>=1) {
							//CommonConstant.printLog("d","For Year "+k+" Withdrawal is "+hmWithdrawalAmountDoubleAAA.get(k));
							mainPartialWithdrawal += hmWithdrawalAmountDoubleAAA.get(k);
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
						+ mainPartialWithdrawal);
			}
                tempFinalPhNetHolderAccAS3=tempPhNetHolderAccAS3+finalLoyaltyAS3-partialWithdrawal;
	            CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges AS3: "
						+ tempFinalPhNetHolderAccAS3);
				tempNetHolderAccAS3=tempFinalPhNetHolderAccAS3;
				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAccAS3);
			double surrendercharges=getSurrenderValuePH10_AS3(count, tempFinalPhNetHolderAccAS3);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmSurrenderValueAS3" + surrendercharges);
			double totsurcharge_AS3=tempFinalPhNetHolderAccAS3-surrendercharges;
			hmSurrenderValuePH10_AS3.put(count,Math.round(totsurcharge_AS3));
			}
			age++;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityChargesAS3);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityChargesAS3);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));
			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			ApplicableServiceTax = Math.round(totalServiceTaxAS3);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);
			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
				String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AAA_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		       double lDBEP = Double.parseDouble(strDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
		}
		//master.setAmountForInvestment(hmInvestmentAmount);
		//master.setPremiumAllocationCharges(hmPremiumAllocation);
		//master.setOtherBenefitCharges(hmOtherBenifitChrges);
		master.setMortalityChargesPH6_AS3(hmMortalityCharges);
		//master.setPolicyAdminCharges(hmPolicyAdminCharges);
		master.setApplicableServiceTaxPH6_AS3(hmApplicableServiceTax);
		master.setTotalchargeforPH6_AS3(hmtotalcharge);
		master.setLoyaltyChargeforPH6_AS3(hmLoyaltyCharge);
		master.setFundManagmentChargePH6_AS3(hmFundManagementCharge);
		master.setRegularFundValuePH6_AS3(hmTotalFundValueAtEndPolicyYear);
		master.setRegularFundValueBeforeFMCPH6_AS3(hmRegularFundValueBeforeFMC);
		master.setRegularFundValuePostFMCPH6_AS3(hmRegularFundValuePostFMC);
		master.setGuaranteedMaturityAdditionPH6_AS3(hmGuaranteedMaturityAmount);
		master.setTotalDeathBenefitPH6_AS3(hmDeathBenefitAtEndofPolicyYear);
		//master.setTotalFundValueAtEndPolicyYearPH10_AS3(hmTotalFundValueAtEndPolicyYear);
		master.setSurrenderValuePH6_AS3(hmSurrenderValuePH10_AS3);
		//master.setCommissionPayble(hmCommissionPayble);
		master.setRegularFundValuePH6_LCEF_AS3(hmTotalFundValueAtEndPolicyYearLCEF_PH10);
		master.setRegularFundValuePH6_WLEF_AS3(hmTotalFundValueAtEndPolicyYearWLEF_PH10);
	}

	public void calculateAlternateChargesSMARTPH10() {//Added by Samina for Alternate Scenrio SMART
		CommonConstant.printLog("d", "Inside calculateChargesSMART10");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		HashMap hmSurrenderValuePH10_SMART10 = new HashMap();
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		double minFundVal = 0.0;
		String fundValErr = "";
		double tempPremium = master.getBaseAnnualizedPremium();
		if (frequency.equals("O")) {
			fundValErr = "The Total Fund Value (Single + Top Up Fund) post your withdrawal is less than an amount equivalent to 5% of the Single Premium paid. Please reduce the amount of withdrawal";
			minFundVal = 0.05 * tempPremium;
		} else {
			fundValErr = "The Total Fund Value (Regular + Top Up Fund) post your withdrawal is less than an amount equivalent to one year�s Annualised Regular Premium. Please reduce the amount of withdrawal";
			minFundVal = tempPremium;
		}
		boolean negativeCheck = false;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		double tempAdminChargeSTax = 0.0;
		long lMortalityChargesSMART10 = 0;
		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;
		double mortChargesSTaxSMART10 = 0.0;
		double tempNetHolderAccSMART10=0.0;
		double templMortalityChargesSMART10 = 0.0;
		double tempFulllMortalityChargesSMART10 = 0.0;
		double tempNetHolderInvestmentSMART10 = 0.0;
		double tempPHAccBeforeGrowthSMART10 = 0.0;
		double tempPHAccInvestGrowthSMART10=0.0;
		double tempPHAccHolderSMART10=0.0;
		double tempPhInvestmentFeeSMART10=0.0;
		double phInvestFeeSTaxSMART10=0.0;
		double tempPhNetHolderAccSMART10=0.0;
		double tempFinalPhNetHolderAccSMART10=0.0;
		double fundGrowthPM = 0.00643403011;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyChargesSMART10=0.0;
		double RiderCharges=0.0;
		double amountInDebtFundEnd = 0.0;
		double debtFundProp = 0.0;
		double equityFMC = 0.0;
		double debtFMC = 0.0;
		double amtDebtFundBeforeTransfer=0.0;
		double FMCDebtFund = 0.0;
		double ServiceTaxMain = master.getServiceTaxMain()/100;

		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			negativeCheck = true;
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		}
		if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			negativeCheck = true;
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		double partialWithdrawal=0.0;
		double fullPartialWithdrawal = 0.0;
		double completePartialWithdrawal = 0.0;
		double mainPartialWithdrawal = 0.0;
		HashMap hmWithdrawalAmountSM10 = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDoubleSM10 = new HashMap<Integer, Double>();
		double yearlyPartialWithdrawal = 0.0;
		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTaxSMART10 = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			double FMC = DataAccessClass.getFMC(oFormulaBean.getRequestBean())/100;
			CommonConstant.printLog("d", "FMC is " + FMC);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTaxSMART10 = totalServiceTaxSMART10 + premiumAllocChargesSTax;
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
							+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			CommonConstant.printLog("d", "For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTaxSMART10 = totalServiceTaxSMART10 + (otherBenefitSTax * 12);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTaxSMART10 = totalServiceTaxSMART10 + (adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			double serviceTaxOnInvestmentFee = 0.0;
			tempFulllMortalityChargesSMART10=0.0;
			fullPartialWithdrawal=0.0;
			completePartialWithdrawal = 0.0;
			yearlyPartialWithdrawal = 0.0;
			for(int i=1;i<=12;i++) {
				double finalLoyaltySMART10=0.0;//Added by Samina for Alternate Scenario 3
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}

				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					//|| count!=1)
					//topUpPremium = 0.0;
					partialWithdrawal = 0.0;
				} else {
					partialWithdrawal = 0.0;
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
							yearlyPartialWithdrawal = partialWithdrawal;
							/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;
							fullPartialWithdrawal+=partialWithdrawal;*/
							completePartialWithdrawal+=partialWithdrawal;
						}
					}
					if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							if (withdrawVar[l].equals(count+","+withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()))) {
								//withdrawVar[l].contains(count+",");
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								yearlyPartialWithdrawal = partialWithdrawal;
								/*if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;*/
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							}
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fullPartialWithdrawal Charges: "
						+ fullPartialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);
			oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAccSMART10);
			oFormulaBean.setCNT(count);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #WDA: "
					+ mainPartialWithdrawal);
			oFormulaBean.setWithdrawalAmount(mainPartialWithdrawal);
			oFormulaBean.setMonthCount(i);
				String MortalityChargesSMART10 = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityChargesSMART10 = Double.parseDouble(MortalityChargesSMART10);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges SMART10: "
						+ templMortalityChargesSMART10);
				tempFulllMortalityChargesSMART10 += templMortalityChargesSMART10;
				RiderCharges = getRiderCharges(count,age, ppt,i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);
			mortChargesSTaxSMART10 = getServiceTax(templMortalityChargesSMART10 + RiderCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax SMART10: "
					+ mortChargesSTaxSMART10 + " and full Mortc SMART10 " + tempFulllMortalityChargesSMART10);
			lMortalityChargesSMART10 = Math.round(tempFulllMortalityChargesSMART10);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges SMART10: "
					+ lMortalityChargesSMART10);
			hmMortalityCharges.put(count, lMortalityChargesSMART10);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
			totalServiceTaxSMART10 = totalServiceTaxSMART10 + mortChargesSTaxSMART10;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
			tempNetHolderInvestmentSMART10 = tempHoldetNettoInvest - templMortalityChargesSMART10 - mortChargesSTaxSMART10 - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges SMART10: "
					+ tempNetHolderInvestmentSMART10);
			tempPHAccBeforeGrowthSMART10 = tempNetHolderAccSMART10 + tempNetHolderInvestmentSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges SMART10: "
					+ tempPHAccBeforeGrowthSMART10);
			oFormulaBean.setA(0.1);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges SMART10: "
					+ fundGrowthPM);
			tempPHAccInvestGrowthSMART10=(tempPHAccBeforeGrowthSMART10*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges SMART10: "
					+ tempPHAccInvestGrowthSMART10);
			tempPHAccHolderSMART10=tempPHAccBeforeGrowthSMART10+tempPHAccInvestGrowthSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges SMART10: "
					+ tempPHAccHolderSMART10);
			double propTransferDtoE = (i==12?1.0:(1.0/(13.0-(i%12.0))));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #propTransferDtoE Charges SMART10: "
					+ propTransferDtoE);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amountInDebtFundEnd "
					+ amountInDebtFundEnd + "debtFundProp" + debtFundProp + "investmentAmount" + investmentAmount);
			if(count==1 && i==1){
			 amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund + tempHoldetNettoInvest;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundBeforeTransfer Charges SMART10: "
					+ amtDebtFundBeforeTransfer);
			}
			else
			{
			amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund - (amountInDebtFundEnd* FMC / 12*0.1236) + tempHoldetNettoInvest;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundBeforeTransfer Charges SMART10: "
						+ amtDebtFundBeforeTransfer);
			}
			double transferFromDebtFund = amtDebtFundBeforeTransfer * propTransferDtoE;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #transferFromDebtFund Charges SMART10: "
					+ transferFromDebtFund);
			double amtDebtFundStartPrem = i==12?0.0:(transferFromDebtFund*(12.0-(i%12.0)));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundStartPrem Charges SMART10: "
					+ amtDebtFundStartPrem);
			amountInDebtFundEnd=tempPHAccHolderSMART10/(tempFinalPhNetHolderAccSMART10+tempHoldetNettoInvest)*amtDebtFundStartPrem;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amountInDebtFundEnd Charges SMART10: "
					+ amountInDebtFundEnd);
			debtFundProp=amountInDebtFundEnd/tempPHAccHolderSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #debtFundProp Charges SMART10: "
					+ debtFundProp);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				debtFMC = master.getRequestBean().getSmartDebtFundFMC();
			FMCDebtFund = amountInDebtFundEnd*debtFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCDebtFund Charges SMART10: "
					+ FMCDebtFund);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				equityFMC = master.getRequestBean().getSmartEquityFundFMC();
			double FMCEquityFund = (tempPHAccHolderSMART10-amountInDebtFundEnd)*equityFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCEquityFund Charges SMART10: "
					+ FMCEquityFund);
			/*oFormulaBean.setA(tempPHAccHolderSMART10);
			String tempPhInvestmentFeeValueSMART10 = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE_AAA,
					oFormulaBean)
					+ "";
			tempPhInvestmentFeeSMART10 = Double.parseDouble(tempPhInvestmentFeeValueSMART10);*/
			tempPhInvestmentFeeSMART10 = FMCEquityFund+FMCDebtFund;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges SMART10: "
					+ tempPhInvestmentFeeSMART10);
			phInvestFeeSTaxSMART10 = getServiceTax(tempPHAccHolderSMART10*FMC/12);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges SMART10: "
					+ phInvestFeeSTaxSMART10);
			totalServiceTaxSMART10 = totalServiceTaxSMART10 + phInvestFeeSTaxSMART10;
			tempPhNetHolderAccSMART10=tempPHAccHolderSMART10-tempPhInvestmentFeeSMART10-phInvestFeeSTaxSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges SMART10: "
					+ tempPhNetHolderAccSMART10);
			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolderSMART10;
			phInvestmentFee = tempPhInvestmentFeeSMART10;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);
			InvestmentFeeSTax = phInvestFeeSTaxSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);
			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;
			if (i == 12) {
						hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					RiderCharges=0;
					 oFormulaBean.setA(tempPhNetHolderAccSMART10);
						String tempLoyaltyChargesSMART10 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesSMART10=Double.parseDouble(tempLoyaltyChargesSMART10);
					 finalLoyaltySMART10=loyaltyChargesSMART10;
					hmLoyaltyCharge.put(count,Math.round(finalLoyaltySMART10));
				hmRegularFundValue.put(count, Math.round(netPHAccount));
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));
				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);
				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);
				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAccSMART10);
	        	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
	        	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges SMART10: "
						+ tempFinalPhNetHolderAccSMART10);
				}
				if (frequency.equals("M"))
					partialWithdrawal= (partialWithdrawal/12);
				else if (frequency.equals("S"))
					partialWithdrawal= (partialWithdrawal/2);
				else if (frequency.equals("Q"))
					partialWithdrawal= (partialWithdrawal/4);
				fullPartialWithdrawal+=partialWithdrawal;
			if (i==1) {
				CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
					hmWithdrawalAmountSM10.put(count, Math.round(yearlyPartialWithdrawal));
				}
				int currentMonth = (count-1)*12+i;
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					hmWithdrawalAmountDoubleSM10.put(currentMonth,0.0);
				} else {
					hmWithdrawalAmountDoubleSM10.put(currentMonth,partialWithdrawal);
				mainPartialWithdrawal=0.0;
				if (master.getRequestBean().getInsured_age()+count-1 >=60) {
						for (int k=currentMonth;k>0;k--) {
							int inAge = k%12!=0?k/12+1:k/12;
							if ((hmWithdrawalAmountDoubleSM10.get(k)!=null) && (master.getRequestBean().getInsured_age()+inAge-1>=58)){
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM10.get(k));
								mainPartialWithdrawal+=hmWithdrawalAmountDoubleSM10.get(k);
						}
					}
				} else {
						for (int k=currentMonth;k>=currentMonth-23;k--) {
							if (hmWithdrawalAmountDoubleSM10.get(k)!=null && k>=1) {
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM10.get(k));
								mainPartialWithdrawal += hmWithdrawalAmountDoubleSM10.get(k);
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
                        + mainPartialWithdrawal);
			}
				tempFinalPhNetHolderAccSMART10=tempPhNetHolderAccSMART10+finalLoyaltySMART10-partialWithdrawal;
            	regularFundValuePostFMC=Math.round(tempPhNetHolderAccSMART10);
            	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAccSMART10);
				tempNetHolderAccSMART10=tempFinalPhNetHolderAccSMART10;
				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAccSMART10);
				if (i==12 && count>5 && negativeCheck) {
					if (totalFundValueAtEndtPolicyYear < minFundVal && master.getRequestBean().getNegativeError()==null) {
	            		master.getRequestBean().setNegativeError(fundValErr);
	            	}
				}
			double surrendercharges=getSurrenderValuePH10_AS3(count, tempFinalPhNetHolderAccSMART10);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmSurrenderValueSMART10" + surrendercharges);
			double totsurcharge_SMART10=tempFinalPhNetHolderAccSMART10-surrendercharges;
			hmSurrenderValuePH10_SMART10.put(count,Math.round(totsurcharge_SMART10));
			}
			age++;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART10);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART10);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));
			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			ApplicableServiceTax = Math.round(totalServiceTaxSMART10);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);
			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
				String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		       double lDBEP = Double.parseDouble(strDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
		}
		//master.setAmountForInvestment(hmInvestmentAmount);
		//master.setPremiumAllocationCharges(hmPremiumAllocation);
		//master.setOtherBenefitCharges(hmOtherBenifitChrges);
		master.setMortalityCharges_SMART10(hmMortalityCharges);
		//master.setPolicyAdminCharges(hmPolicyAdminCharges);
		master.setApplicableServiceTaxPH10_SMART10(hmApplicableServiceTax);
		master.setTotalchargeforPH10_SMART10(hmtotalcharge);
		master.setLoyaltyChargeforPH10_SMART10(hmLoyaltyCharge);
		master.setFundManagmentChargePH10_SMART10(hmFundManagementCharge);
		master.setRegularFundValuePH10_SMART10(hmTotalFundValueAtEndPolicyYear);
		master.setRegularFundValueBeforeFMCPH10_SMART10(hmRegularFundValueBeforeFMC);
		master.setRegularFundValuePostFMCPH10_SMART10(hmRegularFundValuePostFMC);
		master.setGuaranteedMaturityAdditionPH10_SMART10(hmGuaranteedMaturityAmount);
		master.setTotalDeathBenefitPH10_SMART10(hmDeathBenefitAtEndofPolicyYear);
		//master.setTotalFundValueAtEndPolicyYearPH10_SMART10(hmTotalFundValueAtEndPolicyYear);
		master.setSurrenderValuePH10_SMART10(hmSurrenderValuePH10_SMART10);
		//master.setCommissionPayble(hmCommissionPayble);

}

	public void calculateAlternateChargesSMARTPH6() {//Added by Samina for Alternate Scenrio SMART
		CommonConstant.printLog("d", "Inside calculateChargesSMART6");
		double PAC = 0.0;
		HashMap hmPremiumAllocation = new HashMap();
		HashMap hmInvestmentAmount = new HashMap();
		HashMap hmOtherBenifitChrges = new HashMap();
		HashMap hmMortalityCharges = new HashMap();
		HashMap hmPolicyAdminCharges = new HashMap();
		HashMap hmApplicableServiceTax = new HashMap();
		HashMap hmtotalcharge = new HashMap();
		HashMap hmFundManagementCharge = new HashMap();
		HashMap hmRegularFundValue = new HashMap();
		HashMap hmRegularFundValueBeforeFMC = new HashMap();
		HashMap hmRegularFundValuePostFMC = new HashMap();
		HashMap hmGuaranteedMaturityAmount = new HashMap();
		HashMap hmTotalFundValueAtEndPolicyYear = new HashMap();
		HashMap hmDeathBenefitAtEndofPolicyYear = new HashMap();
		HashMap hmCommissionPayble = new HashMap();
		HashMap hmLoyaltyCharge= new HashMap();//loyalty charge
		HashMap hmSurrenderValuePH10_SMART6 = new HashMap();
		long iOtherBenifitChrges = 0;
		double dOtherBenifitChrges = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		double minFundVal = 0.0;
		String fundValErr = "";
		double tempPremium = master.getBaseAnnualizedPremium();
		if (frequency.equals("O")) {
			fundValErr = "The Total Fund Value (Single + Top Up Fund) post your withdrawal is less than an amount equivalent to 5% of the Single Premium paid. Please reduce the amount of withdrawal";
			minFundVal = 0.05 * tempPremium;
		} else {
			fundValErr = "The Total Fund Value (Regular + Top Up Fund) post your withdrawal is less than an amount equivalent to one year�s Annualised Regular Premium. Please reduce the amount of withdrawal";
			minFundVal = tempPremium;
		}
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		double tempAdminChargeSTax = 0.0;
		long lMortalityChargesSMART6 = 0;
		double adminChargeSTax = 0.0;
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double premiumAllocChargesSTax = 0.0;
		double otherBenefitSTax = 0.0;
		double mortChargesSTaxSMART6 = 0.0;
		double tempNetHolderAccSMART6=0.0;
		double templMortalityChargesSMART6 = 0.0;
		double tempFulllMortalityChargesSMART6 = 0.0;
		double tempNetHolderInvestmentSMART6 = 0.0;
		double tempPHAccBeforeGrowthSMART6 = 0.0;
		double tempPHAccInvestGrowthSMART6=0.0;
		double tempPHAccHolderSMART6=0.0;
		double tempPhInvestmentFeeSMART6=0.0;
		double phInvestFeeSTaxSMART6=0.0;
		double tempPhNetHolderAccSMART6=0.0;
		double tempFinalPhNetHolderAccSMART6=0.0;
		double fundGrowthPM = 0.00327373978220;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyChargesSMART6=0.0;
		double RiderCharges=0.0;
		double amountInDebtFundEnd = 0.0;
		double debtFundProp = 0.0;
		double equityFMC = 0.0;
		double debtFMC = 0.0;
		double FMCDebtFund = 0.0;
		double ServiceTaxMain = master.getServiceTaxMain();
		double amtDebtFundBeforeTransfer = 0.0;
		boolean negativeCheck = false;
		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			negativeCheck = true;
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		}
		if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			negativeCheck = true;
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		double partialWithdrawal=0.0;
		double fullPartialWithdrawal = 0.0;
		double completePartialWithdrawal = 0.0;
		double mainPartialWithdrawal = 0.0;
		double yearlyPartialWithdrawal = 0.0;
		HashMap hmWithdrawalAmountSM6 = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDoubleSM6 = new HashMap<Integer, Double>();

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			long ApplicableServiceTax = 0;
			double totalServiceTaxSMART6 = 0.0;
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			long regularFundValuePostFMC = 0;
			double totRiderCharges=0.0;
			String TotalInvestment=null;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			double FMC = DataAccessClass.getFMC(oFormulaBean.getRequestBean())/100;
			CommonConstant.printLog("d", "FMC is " + FMC);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			hmPremiumAllocation.put(count, Math.round(PAC));
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setA(PAC);
			if(count<=ppt)
			{
			 TotalInvestment=formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_TOTAL_INVESTMENT_CHARGES,
					oFormulaBean)
					+ "";
			}
			else{
				 TotalInvestment="0";
			}
			premiumAllocChargesSTax = getServiceTax(PAC);
			totalServiceTaxSMART6 = totalServiceTaxSMART6 + premiumAllocChargesSTax;
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #premiumAllocChargesSTax : " + premiumAllocChargesSTax);
			double investmentAmount=Double.parseDouble(TotalInvestment);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String strCommissionPayble = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_COMMISSION_PAYBLE, oFormulaBean)
							+ "";
			double lCommision = Double.parseDouble(strCommissionPayble);
			CommonConstant.printLog("d", "For Count " + count + " Investment amount is " + investmentAmount);
			if(count<=ppt)
			{
			hmInvestmentAmount.put(count, Math.round(investmentAmount));
			hmCommissionPayble.put(count,  Math.round(lCommision));
			}
			else{
				hmInvestmentAmount.put(count, 0);
				hmCommissionPayble.put(count, 0);
			}
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			String otherBefinteCharges = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES,
					oFormulaBean)
					+ "";
			iOtherBenifitChrges = Long.parseLong(otherBefinteCharges);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges: "
					+ iOtherBenifitChrges);
			String otherBefinteChargesMonthly = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY,
							oFormulaBean)
					+ "";
			dOtherBenifitChrges = Double
					.parseDouble(otherBefinteChargesMonthly);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #getOtherBenifitInbuiltCharges Monthly: "
					+ dOtherBenifitChrges);
			otherBenefitSTax = getServiceTax(dOtherBenifitChrges);
			totalServiceTaxSMART6 = totalServiceTaxSMART6 + (otherBenefitSTax * 12);
			CommonConstant.printLog("d", "#calculateChargesForPH10 #Policy term : " + count
					+ " #otherBenefitSTax : " + otherBenefitSTax);
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
				hmPolicyAdminCharges.put(count,Math.round(lAdminCharge));
			adminChargeSTax = getServiceTax(dAdminCharge);
			totalServiceTaxSMART6 = totalServiceTaxSMART6 + (adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #service tax on admin fee : "
					+ adminChargeSTax * 12);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			double serviceTaxOnInvestmentFee = 0.0;
			tempFulllMortalityChargesSMART6=0.0;
			fullPartialWithdrawal=0.0;
			completePartialWithdrawal=0.0;
			yearlyPartialWithdrawal = 0.0;
			for(int i=1;i<=12;i++) {
				double finalLoyaltySMART6=0.0;//Added by Samina for Alternate Scenario 3
				double tempHoldetNettoInvest = investmentAmount ;
				if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
					tempHoldetNettoInvest = investmentAmount;
				} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2 - getServiceTax(PAC / 2);
				} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4 - getServiceTax(PAC / 4);
				} else if (frequency.equalsIgnoreCase("M")) {
					tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12 - getServiceTax(PAC / 12);
				} else {
					tempHoldetNettoInvest = 0.0;
				}
				if (count>ppt) {
					tempHoldetNettoInvest = 0.0;
				}

				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					//|| count!=1)
					//topUpPremium = 0.0;
					partialWithdrawal = 0.0;
				} else {
					partialWithdrawal = 0.0;
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
							yearlyPartialWithdrawal = partialWithdrawal;
							/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;
							if (frequency.equalsIgnoreCase("M"))
								partialWithdrawal/=12;
							fullPartialWithdrawal+=partialWithdrawal;*/
							completePartialWithdrawal+=partialWithdrawal;
						}
					}
					if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							if (withdrawVar[l].equals(count+","+withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()))) {
								//withdrawVar[l].contains(count+",");
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								yearlyPartialWithdrawal = partialWithdrawal;
								/*if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								if (frequency.equalsIgnoreCase("M"))
									partialWithdrawal/=12;
								fullPartialWithdrawal+=partialWithdrawal;*/
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							}
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fullPartialWithdrawal Charges: "
						+ fullPartialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);
			 oFormulaBean.setA(tempHoldetNettoInvest+tempFinalPhNetHolderAccSMART6);
				oFormulaBean.setCNT(count);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #WDA: "
					+ mainPartialWithdrawal);
				oFormulaBean.setWithdrawalAmount(mainPartialWithdrawal);
				oFormulaBean.setMonthCount(i);
				String MortalityChargesSMART6 = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MORTALITY_CHARGES_MONTHLY, oFormulaBean)
						+ "";
				templMortalityChargesSMART6 = Double.parseDouble(MortalityChargesSMART6);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges SMART6: "
						+ templMortalityChargesSMART6);
				tempFulllMortalityChargesSMART6 += templMortalityChargesSMART6;
				RiderCharges = getRiderCharges(count,age,ppt,i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);
			mortChargesSTaxSMART6 = getServiceTax(templMortalityChargesSMART6+RiderCharges);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #Mortality Charges Service Tax SMART6: "
					+ mortChargesSTaxSMART6 + " and full Mortc SMART6 " + tempFulllMortalityChargesSMART6);
			lMortalityChargesSMART6 = Math.round(tempFulllMortalityChargesSMART6);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Final lMortalityCharges SMART6: "
					+ lMortalityChargesSMART6);
			hmMortalityCharges.put(count, lMortalityChargesSMART6);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
			totalServiceTaxSMART6 = totalServiceTaxSMART6 + mortChargesSTaxSMART6;
			tempAdminChargeSTax = getServiceTax(dAdminCharge);
			tempNetHolderInvestmentSMART6 = tempHoldetNettoInvest - templMortalityChargesSMART6 - mortChargesSTaxSMART6 - dAdminCharge - tempAdminChargeSTax-RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges SMART6: "
					+ tempNetHolderInvestmentSMART6);
			tempPHAccBeforeGrowthSMART6 = tempNetHolderAccSMART6 + tempNetHolderInvestmentSMART6;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges SMART6: "
					+ tempPHAccBeforeGrowthSMART6);
			oFormulaBean.setA(0.1);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges SMART6: "
					+ fundGrowthPM);
			tempPHAccInvestGrowthSMART6=(tempPHAccBeforeGrowthSMART6*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges SMART6: "
					+ tempPHAccInvestGrowthSMART6);
			tempPHAccHolderSMART6=tempPHAccBeforeGrowthSMART6+tempPHAccInvestGrowthSMART6;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges SMART6: "
					+ tempPHAccHolderSMART6);
			double propTransferDtoE = (i==12?1.0:(1.0/(13.0-(i%12.0))));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #propTransferDtoE Charges SMART6: "
					+ propTransferDtoE);
			if(count==1 && i==1){
				amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund + tempHoldetNettoInvest;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundBeforeTransfer Charges SMART10: "
						+ amtDebtFundBeforeTransfer);
			} else {
				amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund - (amountInDebtFundEnd* FMC / 12*0.1236) + tempHoldetNettoInvest;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundBeforeTransfer Charges SMART10: "
						+ amtDebtFundBeforeTransfer);
			}
			double transferFromDebtFund = amtDebtFundBeforeTransfer * propTransferDtoE;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #transferFromDebtFund Charges SMART6: "
					+ transferFromDebtFund);
			double amtDebtFundStartPrem = i==12?0.0:(transferFromDebtFund*(12.0-(i%12.0)));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundStartPrem Charges SMART6: "
					+ amtDebtFundStartPrem);
			amountInDebtFundEnd=tempPHAccHolderSMART6/(tempFinalPhNetHolderAccSMART6+tempHoldetNettoInvest)*amtDebtFundStartPrem;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amountInDebtFundEnd Charges SMART6: "
					+ amountInDebtFundEnd);
			debtFundProp=amountInDebtFundEnd/tempPHAccHolderSMART6;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #debtFundProp Charges SMART6: "
					+ debtFundProp);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				debtFMC = master.getRequestBean().getSmartDebtFundFMC();
			FMCDebtFund = amountInDebtFundEnd*debtFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCDebtFund Charges SMART6: "
					+ FMCDebtFund);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				equityFMC = master.getRequestBean().getSmartEquityFundFMC();
			double FMCEquityFund = (tempPHAccHolderSMART6-amountInDebtFundEnd)*equityFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCEquityFund Charges SMART6: "
					+ FMCEquityFund);
			/*oFormulaBean.setA(tempPHAccHolderSMART6);
			String tempPhInvestmentFeeValueSMART6 = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE_AAA,
					oFormulaBean)
					+ "";
			tempPhInvestmentFeeSMART6 = Double.parseDouble(tempPhInvestmentFeeValueSMART6);*/
			tempPhInvestmentFeeSMART6 = FMCEquityFund+FMCDebtFund;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges SMART6: "
					+ tempPhInvestmentFeeSMART6);
			phInvestFeeSTaxSMART6 = getServiceTax(tempPHAccHolderSMART6*FMC/12);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #phInvestFeeSTax Charges SMART6: "
					+ phInvestFeeSTaxSMART6);
			totalServiceTaxSMART6 = totalServiceTaxSMART6 + phInvestFeeSTaxSMART6;
			tempPhNetHolderAccSMART6=tempPHAccHolderSMART6-tempPhInvestmentFeeSMART6-phInvestFeeSTaxSMART6;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges SMART6: "
					+ tempPhNetHolderAccSMART6);
			double InvestmentFeeSTax = 0.0;
			phAccount = tempPHAccHolderSMART6;
			phInvestmentFee = tempPhInvestmentFeeSMART6;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);
			InvestmentFeeSTax = phInvestFeeSTaxSMART6;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #InvestmentFeeSTax: " + InvestmentFeeSTax);
			serviceTaxOnInvestmentFee = serviceTaxOnInvestmentFee
					+ InvestmentFeeSTax;
			netPHAccount = phAccount - phInvestmentFee - InvestmentFeeSTax;
			if (i == 12) {
					hmOtherBenifitChrges.put(count, Math.round(totRiderCharges));
					RiderCharges=0;
					 oFormulaBean.setA(tempPhNetHolderAccSMART6);
						String tempLoyaltyChargesSMART6 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesSMART6=Double.parseDouble(tempLoyaltyChargesSMART6);
					 finalLoyaltySMART6=loyaltyChargesSMART6;
					hmLoyaltyCharge.put(count,Math.round(finalLoyaltySMART6));
				hmRegularFundValue.put(count, Math.round(netPHAccount));
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));
				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal + serviceTaxOnInvestmentFee);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal
						+ " #serviceTaxOnInvestmentFee: "
						+ serviceTaxOnInvestmentFee);
				hmRegularFundValueBeforeFMC.put(count,
						regularFundValueBeforeFMC);
				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				hmGuaranteedMaturityAmount.put(count,
						gurananteedMaturityAmount);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				regularFundValuePostFMC=Math.round(tempPhNetHolderAccSMART6);
	        	hmRegularFundValuePostFMC.put(count, regularFundValuePostFMC);
	        	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges SMART6: "
						+ tempFinalPhNetHolderAccSMART6);
				}
			if (frequency.equals("M"))
				partialWithdrawal= (partialWithdrawal/12);
			else if (frequency.equals("S"))
				partialWithdrawal= (partialWithdrawal/2);
			else if (frequency.equals("Q"))
				partialWithdrawal= (partialWithdrawal/4);

			if (i==1) {
				CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
				hmWithdrawalAmountSM6.put(count, Math.round(yearlyPartialWithdrawal));
			}
			int currentMonth = (count-1)*12+i;
			if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
					|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
				hmWithdrawalAmountDoubleSM6.put(currentMonth,0.0);
			} else {
				hmWithdrawalAmountDoubleSM6.put(currentMonth,partialWithdrawal);
				mainPartialWithdrawal=0.0;
				if (master.getRequestBean().getInsured_age()+count-1 >=60) {
					CommonConstant.printLog("d", "Age > 60");
					for (int k=currentMonth;k>0;k--) {
						int inAge = k%12!=0?k/12+1:k/12;
						if ((hmWithdrawalAmountDoubleSM6.get(k)!=null) && (master.getRequestBean().getInsured_age()+inAge-1>=58)){
							//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM6.get(k));
							mainPartialWithdrawal+=hmWithdrawalAmountDoubleSM6.get(k);
						}
					}
				} else {
					for (int k=currentMonth;k>=currentMonth-23;k--) {
						if (hmWithdrawalAmountDoubleSM6.get(k)!=null && k>=1) {
							//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM6.get(k));
							mainPartialWithdrawal += hmWithdrawalAmountDoubleSM6.get(k);
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
						+ mainPartialWithdrawal);
			}
	            tempFinalPhNetHolderAccSMART6=tempPhNetHolderAccSMART6+finalLoyaltySMART6-partialWithdrawal;
	            CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges SMART6: "
						+ tempFinalPhNetHolderAccSMART6);
				tempNetHolderAccSMART6=tempFinalPhNetHolderAccSMART6;
				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAccSMART6);
				if (i==12 && count>5 && negativeCheck) {
					if (totalFundValueAtEndtPolicyYear < minFundVal && master.getRequestBean().getNegativeError()==null) {
	            		master.getRequestBean().setNegativeError(fundValErr);
	            	}
				}
			double surrendercharges=getSurrenderValuePH10_AS3(count, tempFinalPhNetHolderAccSMART6);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmSurrenderValueSMART6" + surrendercharges);
			double totsurcharge_SMART6=tempFinalPhNetHolderAccSMART6-surrendercharges;
			hmSurrenderValuePH10_SMART6.put(count,Math.round(totsurcharge_SMART6));
			}
			age++;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART6);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART6);
			}
			 hmtotalcharge.put(count,Math.round(totalCharges));
			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			ApplicableServiceTax = Math.round(totalServiceTaxSMART6);
			hmApplicableServiceTax.put(count, ApplicableServiceTax);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Total Applicable Service Tax ::: "
					+ ApplicableServiceTax);
			hmFundManagementCharge.put(count, Math.round(phInvestmentFeeTotal));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			hmTotalFundValueAtEndPolicyYear.put(count,
					totalFundValueAtEndtPolicyYear);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
				String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		       double lDBEP = Double.parseDouble(strDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			hmDeathBenefitAtEndofPolicyYear.put(count, Math.round(lDBEP));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
		}
		//master.setAmountForInvestment(hmInvestmentAmount);
		//master.setPremiumAllocationCharges(hmPremiumAllocation);
		//master.setOtherBenefitCharges(hmOtherBenifitChrges);
		master.setMortalityCharges_SMART6(hmMortalityCharges);
		//master.setPolicyAdminCharges(hmPolicyAdminCharges);
		master.setApplicableServiceTaxPH10_SMART6(hmApplicableServiceTax);
		master.setTotalchargeforPH10_SMART6(hmtotalcharge);
		master.setLoyaltyChargeforPH10_SMART6(hmLoyaltyCharge);
		master.setFundManagmentChargePH10_SMART6(hmFundManagementCharge);
		master.setRegularFundValuePH10_SMART6(hmTotalFundValueAtEndPolicyYear);
		master.setRegularFundValueBeforeFMCPH10_SMART6(hmRegularFundValueBeforeFMC);
		master.setRegularFundValuePostFMCPH10_SMART6(hmRegularFundValuePostFMC);
		master.setGuaranteedMaturityAdditionPH10_SMART6(hmGuaranteedMaturityAmount);
		master.setTotalDeathBenefitPH10_SMART6(hmDeathBenefitAtEndofPolicyYear);
		//master.setTotalFundValueAtEndPolicyYearPH10_SMART6(hmTotalFundValueAtEndPolicyYear);
		master.setSurrenderValuePH10_SMART6(hmSurrenderValuePH10_SMART6);
		//master.setCommissionPayble(hmCommissionPayble);

}
	public long getTotalModalPremiumPayble() {
		long totMPP=0;
		try {
		FormulaHandler formulaHandler = new FormulaHandler();
		String totMPPStr = formulaHandler.evaluateFormula(master
				.getRequestBean().getBaseplan(),
				FormulaConstant.TOTMODAL_PREM_PAYBLE, oFormulaBean)
				+ "";
		totMPP = Long.parseLong(totMPPStr);
		CommonConstant.printLog("d", "Total Modal Premium Payble :::" + totMPP);
		}
		catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating Total Modal Premium Payble:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return totMPP;
	}
	//added on 23-06-2014
	public long getRenewalPremiumServiceTax(String formula) {
		long renewalPremiumST = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
            if(master.getRequestBean().getBaseplan().startsWith("MLS"))
                oFormulaBean.setAP(getRenewalModalPremium_A());
            oFormulaBean.setA(getRenewalModalPremium_A());
            oFormulaBean.setMRSPRP(getRenewalModalPremium_A());
			if(master.getRequestBean().getBaseplan().startsWith("MRS"))
				oFormulaBean.setRiderPremium(master.getTotAnnRiderModalPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String renewalPremiumSTStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					formula, oFormulaBean)
					+ "";
			renewalPremiumST = Long.parseLong(renewalPremiumSTStr);
			CommonConstant.printLog("d", "renewal Premium Service Tax for " + formula + " Calculated :::: " + renewalPremiumST);

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating renewalPremium Service Tax of " + formula + "::::" + e + " " + e.getMessage());
		}

		return renewalPremiumST;
	}
	public long getRenewalPremiumServiceTaxMonthly (long renewalPremiumM) {
		long renewalPremiumST = 0;
		if (renewalPremiumM!=0)
			renewalPremiumST = Math.round(renewalPremiumM * master.getServiceTaxRenewal() / 100);
		return renewalPremiumST;
	}
	public long getTotalRenewalPremium(String formula) {
		long totalrenewalPremium = 0;
		try {
			oFormulaBean = getFormulaBeanObj();
            if(master.getRequestBean().getBaseplan().startsWith("MLS"))
                oFormulaBean.setAP(getRenewalModalPremium_A());
			oFormulaBean.setA(getRenewalModalPremium_A());
            oFormulaBean.setMRSPRP(getRenewalModalPremium_A());
			if(master.getRequestBean().getBaseplan().startsWith("MRS"))
				oFormulaBean.setRiderPremium(master.getTotAnnRiderModalPremium());
			FormulaHandler formulaHandler = new FormulaHandler();
			String totalrenewalPremiumStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					formula, oFormulaBean)
					+ "";
			totalrenewalPremium = Long.parseLong(totalrenewalPremiumStr);
			CommonConstant.printLog("d", "total renewal Premium for " + formula + " Calculated :::: " + totalrenewalPremium);

		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating total renewalPremium of " + formula + "::::" + e + " " + e.getMessage());
		}

		return totalrenewalPremium;
	}
	public long getTotalRenewalPremiumMonthly(long renewalPremiumM) {
		long renewalPremiumST = 0;
		if (renewalPremiumM!=0)
			renewalPremiumST = Math.round(renewalPremiumM * master.getServiceTaxRenewal() / 100);
		return (renewalPremiumM + renewalPremiumST);
	}
	//added on 25-06-2014
	private HashMap getSurrenderBenefit_4_Smart7() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			if(count == 1){
				oFormulaBean.setA(0);
			}else{
				FormulaHandler formulaHandler = new FormulaHandler();
				oFormulaBean.setCNT(count-1);
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";
				//vsrbonus = Long.parseLong(strVsrbonus);
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));

				//oFormulaBean.setA(Double.parseDouble(VSRB4Map.get(count-1)+""));

			}
			oFormulaBean.setCNT(count);

			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_4, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			if(master.getRequestBean().getBaseplan().equals("SMART7V1N1") && count==polTerm)
					SBMap.put(count, 0);
				else
			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
					+ surrenderBenefit);

		}
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating getSurrenderBenefit_4_Smart7 (alternate scenario) for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	//added on 25-06-2014
	private HashMap getVariableBonus() {
		HashMap VBMap = new HashMap();
		try {
			long vbonus;
			double dvsrbonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				if(count>=8){
					oFormulaBean.setA(0.25);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_VARIABLE_BONUS,
							oFormulaBean)
							+ "";
					dvsrbonus = Double.parseDouble(strVsrbonus);
					vbonus=Math.round(dvsrbonus);

					VBMap.put(count, vbonus);
				}
				else{
					oFormulaBean.setA(0);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_VARIABLE_BONUS,
							oFormulaBean)
							+ "";
					dvsrbonus = Double.parseDouble(strVsrbonus);
					vbonus=Math.round(dvsrbonus);

					VBMap.put(count, vbonus);
				}
				CommonConstant.printLog("d", "Pol Term :::  Variable Bonus " + count + " : "
						+ vbonus);
			}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getVariableBonus "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getVariableBonus for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return VBMap;
	}
	//added on 26-06-2014
	private HashMap getAltScenarioTotalDeathBenefitCR() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MMXV1N1")){
					oFormulaBean.setCNT(count - 1);
					FormulaHandler formulaHandler = new FormulaHandler();
					String strEbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_EBDEATHBENEFIT,
							oFormulaBean)
							+ "";
					terminalBonus=Long.parseLong(strEbonus);
					CommonConstant.printLog("d", "Pol Term :::  GP deathBenefitCR " + count + " : "
							+ terminalBonus);
					TBMap.put(count, terminalBonus);
				}


				else{
					oFormulaBean = getFormulaBeanObj();
					oFormulaBean.setCNT(count-1);
					oFormulaBean.setCNT_MINUS(true);

					FormulaHandler formulaHandler = new FormulaHandler();
					String strTerminalbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ALTSCE_TOTDB_CR,
							oFormulaBean)
							+ "";
					terminalBonus = Long.parseLong(strTerminalbonus);
					TBMap.put(count, terminalBonus);
				}
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus CR " + count + " : "
						+ terminalBonus);
				}

		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getAltScenarioTotalDeathBenefitCR "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getAltScenarioTotalDeathBenefitCR for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TBMap;
	}
	private HashMap getTotMaturityBenefit8() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;
		if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2") ) {
			startPt = 5;
		}
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1")
					|| master.getRequestBean().getBaseplan().equals("SGPV1N2") ||  master.getRequestBean().getBaseplan().startsWith("IWV1N")	) {
				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;
			}
			if(count==polTerm){
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("IWV1N"))) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setAGAI(totalGAIVal);
				}
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
				SBMap.put(count, surrenderBenefit);
				CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
						+ surrenderBenefit);
			}
			else{
				SBMap.put(count, 0);
			}
		}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTotMaturityBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}

	private HashMap getTotMaturityBenefitFR8() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		try{
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				double GAIValue = calculateGAIDouble(count);
				if (count <= ppt){
					totalGAIVal = totalGAIVal + GAIValue;
				}
				if(count==polTerm){
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setDoubleAGAI(totalGAIVal);
					String surrBenStr = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
							+ "";
					surrenderBenefit = Long.parseLong(surrBenStr);
					SBMap.put(count, surrenderBenefit);
					CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit FR" + count + " : "
							+ surrenderBenefit);
				}
				else{
					SBMap.put(count, 0);
				}
			/*if (count==polTerm-1)
				SBMap.put(count, Math.round(totalGAIVal));*/

			}
		} catch (Exception e) {
			//System.out
			//	.println("Exception occured in getTotMaturityBenefit8 "
			//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8FR for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}

	private HashMap getTotMaturityBenefit8_MBP() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			long GAIValue = calculateGAI(count);
			totalGAIVal = GAIValue;
			if(count==polTerm){
				String onePlusRB = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ONEPLUSRB8,
						oFormulaBean)
						+ "";
				onePlusRBD = Double.parseDouble(onePlusRB);
				onePlusRBD = Math.pow(onePlusRBD,(count));
				oFormulaBean.setA(onePlusRBD);
				oFormulaBean.setAGAI(totalGAIVal);
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
				CommonConstant.printLog("d", "TEST totalGAIVal " + totalGAIVal);
				SBMap.put(count, surrenderBenefit+totalGAIVal);
				CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
						+ surrenderBenefit);
			} else{
				SBMap.put(count, totalGAIVal);
			}
		}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTotMaturityBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}

	private HashMap getTotMaturityBenefit4_MBP() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			long GAIValue = calculateGAI(count);
			totalGAIVal = GAIValue;
			if(count==polTerm){
				String onePlusRB = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.ONEPLUSRB4,
						oFormulaBean)
						+ "";
				onePlusRBD = Double.parseDouble(onePlusRB);
				onePlusRBD = Math.pow(onePlusRBD,(count));
				oFormulaBean.setA(onePlusRBD);
				oFormulaBean.setAGAI(totalGAIVal);
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
				CommonConstant.printLog("d", "TEST 4 totalGAIVal " + totalGAIVal);
				SBMap.put(count, surrenderBenefit+totalGAIVal);
				CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit" + count + " : "
						+ surrenderBenefit);
			} else{
				SBMap.put(count, totalGAIVal);
			}
		}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTotMaturityBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	/*private long getEffectivePremium8() {
		double EffectivePrem = 0;
		long TotalEffectivePrem=0;
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		double tax_slab=Double.parseDouble(master.getRequestBean().getTaxslab());
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			if(count==polTerm){
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
				EffectivePrem=surrenderBenefit*tax_slab*1.03;
				TotalEffectivePrem=Math.round(EffectivePrem);
				CommonConstant.printLog("d","Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
						+ TotalEffectivePrem);
			}
		}
		} catch (Exception e) {
			System.out
					.println("Exception occured in getTotMaturityBenefit8 "
							+ e);
			Logger
					.info("Error occured in calculating getTotMaturityBenefit8 for planCode:  "
							+ master.getRequestBean().getBaseplan());
		}
		return TotalEffectivePrem;
	}*/
	private long getEffectivePremium8() {
		double EffectivePrem = 0;
		long TotalEffectivePrem=0;

		long maturityValue=0;
		long lTotMatVal=0;
		long GAIValue=0;
		int polTerm = master.getRequestBean().getPolicyterm();
		double tax_slab=master.getRequestBean().getTaxslab()!=null?Double.parseDouble(master.getRequestBean().getTaxslab()):0;
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			if(count==polTerm){

				if(master.getRequestBean().getBaseplan().equals("MLGPV1N1") || master.getRequestBean().getBaseplan().equals("MLGV2N1")) {


						String surrBenStr = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
								+ "";
						lTotMatVal = Long.parseLong(surrBenStr);
						EffectivePrem=lTotMatVal*tax_slab*1.03;
						TotalEffectivePrem=Math.round(EffectivePrem);

				}
				else{


				String maturityValueStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_VALUE,
						oFormulaBean)
						+ "";
				maturityValue = Long.parseLong(maturityValueStr);


				if (count > master.getRequestBean().getPremiumpayingterm()) {
	                   GAIValue = calculateGAI(count);

	              }
				lTotMatVal=GAIValue+maturityValue;
				EffectivePrem=lTotMatVal*tax_slab*1.03;
				TotalEffectivePrem=Math.round(EffectivePrem);
				CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
						+ TotalEffectivePrem);
			}

				}
			}
		}
		 catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTotMaturityBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TotalEffectivePrem;
	}
	private HashMap getTotMaturityBenefit4() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;
		if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
			startPt = 5;
		}
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			if ((master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2"))
					|| (master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
					|| (master.getRequestBean().getBaseplan().startsWith("IWV1N"))) {
				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;
				if (count==polTerm && master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
					totalGAIVal = totalGAIVal - GAIValue;
			}
			if(count==polTerm){
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2"))
						|| (master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						|| (master.getRequestBean().getBaseplan().startsWith("IWV1N"))) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setAGAI(totalGAIVal);
				}
				String surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
				SBMap.put(count, surrenderBenefit);

				CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
						+ surrenderBenefit);
			}
			else{
				SBMap.put(count, 0);
			}

		}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getTotMaturityBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}

	private HashMap getTotMaturityBenefitFR4() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt = master.getRequestBean().getPremiumpayingterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		try{
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				double GAIValue = calculateGAIDouble(count);
				if (count<=ppt) {
					totalGAIVal = totalGAIVal + GAIValue;
				}
				if(count==polTerm){
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setDoubleAGAI(totalGAIVal);
					String surrBenStr = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
							+ "";
					surrenderBenefit = Long.parseLong(surrBenStr);
					SBMap.put(count, surrenderBenefit);

					CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit FR4" + count + " : "
							+ surrenderBenefit);
				}
				else{
					SBMap.put(count, 0);
				}
			/*if (count==polTerm-1) {
				SBMap.put(count, Math.round(totalGAIVal));
			}*/

			}
		} catch (Exception e) {
			//System.out
			//	.println("Exception occured in getTotMaturityBenefit8 "
			//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getTotMaturityBenefitFR4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	private HashMap getSurrenderBenefit8() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		double onePlusRBD = 0;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 1;
		if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
			startPt = 6;
		}
		try{
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			if(count == 1){
				oFormulaBean.setA(0);
			}else{
				FormulaHandler formulaHandler = new FormulaHandler();
				oFormulaBean.setCNT(count-1);
				if ((master.getRequestBean().getBaseplan().equals("SGPV1N1")
						|| master.getRequestBean().getBaseplan().equals("SGPV1N2")
						|| master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
						&& count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Long.parseLong(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getSurrenderBenefit8");
				String strVsrbonus = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";
				//vsrbonus = Long.parseLong(strVsrbonus);
				oFormulaBean.setA(Double.parseDouble(strVsrbonus));

				//oFormulaBean.setA(Double.parseDouble(VSRB4Map.get(count-1)+""));

			}
			oFormulaBean.setCNT(count);

			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_8, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			if(master.getRequestBean().getBaseplan().equals("SMART7V1N1") && count==polTerm)
				SBMap.put(count, 0);
			else
			SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit 8" + count + " : "
					+ surrenderBenefit);

		}
		} catch (Exception e) {
			//System.out
				//	.println("Exception occured in getSurrenderBenefit8 "
					//		+ e);
			CommonConstant.printLog("e", "Error occured in calculating getSurrenderBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	//Start : added by jayesh for SMART 7 : 21-05-2014

	public void calculateChargesForPH10withoutST() {
		CommonConstant.printLog("d", "Inside calculateCharges without ST");
		double PAC = 0.0;

		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;

		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double tempNetHolderAcc=0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;

		double tempPhInvestmentFee=0.0;
		double tempPhNetHolderAcc=0.0;
		BigDecimal tempFinalPhNetHolderAcc=new BigDecimal(0.0);
		double fundGrowthPM = 0.00643403011;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		HashMap<Integer,BigDecimal> hmPHIRRTerm = new HashMap();
		double tempPremium = master.getRequestBean().getBasepremiumannual();

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			//String TotalInvestment=null;
			double loyaltyCharges=0.0;

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble(premAllocCharge);
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			double investmentAmount=0.0;
			if(count<=ppt) {
				investmentAmount = tempPremium - PAC;
			}
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
			String AdminCharges = formulaHandler.evaluateFormula(master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
			dAdminCharge = Double.parseDouble(AdminCharges);
			lAdminCharge = dAdminCharge * 12;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Policy Admin Charges: " + lAdminCharge);
			CommonConstant.printLog("d", "#Policy term : " + count + " #Policy Admin Charges: " + dAdminCharge);

			for(int i=1;i<=12;i++) {
				//double tempHoldetNettoInvest = investmentAmount - premiumAllocChargesSTax;
				double finalLoyalty=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if (count<=ppt) {
					if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
						tempHoldetNettoInvest = investmentAmount;
						hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(tempPremium));
					} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
						hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.5));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2;
					} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
						hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.25));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4;
					} else if (frequency.equalsIgnoreCase("M")) {
						double tempMPremium = Math.round(tempPremium*0.0833);
						hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(tempMPremium));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12;
					} else {
						hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(0.0));
						tempHoldetNettoInvest = 0.0;
					}
				} else {
					hmPHIRRTerm.put((count-1)*12+i,BigDecimal.valueOf(0.0));
					tempHoldetNettoInvest = 0.0;
				}
				tempNetHolderInvestment = tempHoldetNettoInvest - dAdminCharge;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: " + tempNetHolderInvestment);
				tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: " + tempPHAccBeforeGrowth);
				oFormulaBean.setA(0.1);

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
						+ fundGrowthPM);

				tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: " + tempPHAccInvestGrowth);
				tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: " + tempPHAccHolder);
				oFormulaBean.setA(tempPHAccHolder);
				String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
						oFormulaBean)
						+ "";
				tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
						+ tempPhInvestmentFee);
				tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
						+ tempPhNetHolderAcc);
				phAccount = tempPHAccHolder;
				phInvestmentFee = tempPhInvestmentFee;
				phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
				CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
						+ " #phInvestmentFeeValue: " + phInvestmentFee);

				// CommonConstant.printLog("d","#Policy term : "+count+" #serviceTaxOnInvestmentFee : "
				// + serviceTaxOnInvestmentFee);

				netPHAccount = phAccount - phInvestmentFee;

				// Storing regular fund values
				if (i == 12) {
					oFormulaBean.setA(tempPhNetHolderAcc);
					String tempLoyaltyCharges = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_LC,
							oFormulaBean)
							+ "";
					loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
					finalLoyalty=loyaltyCharges;
					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #RegularFundValue:  "
							+ Math.round(netPHAccount));

					regularFundValueBeforeFMC = Math.round(netPHAccount
							+ phInvestmentFeeTotal);

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmRegularFundValueBeforeFMC: "
							+ regularFundValueBeforeFMC + " #netPHAccount:  "
							+ netPHAccount + " #phInvestmentFeeTotal: "
							+ phInvestmentFeeTotal);

					long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
							count, netPHAccount);

					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmGuaranteedMaturityAmount: "
							+ gurananteedMaturityAmount);
		        	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
							+ tempFinalPhNetHolderAcc);

					}
		            tempFinalPhNetHolderAcc=BigDecimal.valueOf(tempPhNetHolderAcc+finalLoyalty);

					CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
							+ tempFinalPhNetHolderAcc);
					tempNetHolderAcc=tempFinalPhNetHolderAcc.doubleValue();
					totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAcc);


					long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc.doubleValue());
					CommonConstant.printLog("d", "#Policy term : " + count
							+ " #hmSurrenderValue" + surrenderValue);
				// END
			}

			//End iFlex

			// Mortility Charges Monthly start
			age++;

			//Added by Samina Mohammad 02-04-2014 for iFlexi
			//netPHInvestment = totalNetHolderInvestment;
			if(count<=ppt) {
				totalCharges=Math.round(PAC+lAdminCharge);
			} else {
				totalCharges=Math.round(lAdminCharge);
			}

			CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					+ totalCharges);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);


			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
			String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
			double lDBEP = Double.parseDouble(strDBEP);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
		}
		hmPHIRRTerm.put(polTerm*12+1,tempFinalPhNetHolderAcc);
		CommonConstant.printLog("d", "tempFinalPhNetHolderAcc: " + tempFinalPhNetHolderAcc);
		double netYield = getIRRAlternate(hmPHIRRTerm , polTerm*12);
		netYield = netYield * 100;
		DecimalFormat df = new DecimalFormat("#.##");
		netYield = Double.parseDouble(df.format(netYield));
		master.setNetYield8P(netYield);
		CommonConstant.printLog("d", "#NetYield: " + netYield);

	}



	public void calculateTopUpPartialWithDForPH10ST() {
		CommonConstant.printLog("d", "Inside calculateTopUpPartialWithDForPH10 calculateCharges without ST");
		double PAC = 0.0;
		HashMap hmTopUpPremium = new HashMap();
		HashMap hmWithdrawalAmount = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDouble = new HashMap<Integer, Double>();
		HashMap<Integer,BigDecimal> hmPHIRRTermTW = new HashMap();
		double tempPremium = master.getBaseAnnualizedPremium();
		String topUpAmount = "";
		String topUpStartYear = "";
		String topUpEndYear = "";
		String topUpAmountYear = "";
		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";

		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		if (master.getRequestBean().getFixTopupAmt()!= null && !master.getRequestBean().getFixTopupAmt().equals("")) {
			topUpAmount = master.getRequestBean().getFixTopupAmt();
			topUpStartYear = master.getRequestBean().getFixTopupStYr();
			topUpEndYear = master.getRequestBean().getFixTopupEndYr();
		}
		if (master.getRequestBean().getVarTopupAmtYr()!= null && !master.getRequestBean().getVarTopupAmtYr().equals("")) {
			topUpAmountYear = master.getRequestBean().getVarTopupAmtYr();
			topUpAmountYear = "-" + topUpAmountYear.substring(0, topUpAmountYear.length()-1);
		}
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		}
		if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;

		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double tempNetHolderAcc=0.0;
		double templMortalityCharges = 0.0;
		double tempFulllMortalityCharges = 0.0;
		double tempNetHolderInvestment = 0.0;
		double tempPHAccBeforeGrowth = 0.0;
		double tempPHAccInvestGrowth=0.0;
		double tempPHAccHolder=0.0;
		double topUpPremium=0.0;
		double partialWithdrawal=0.0;
		double topUpInsuredAmount = 0.0;

		double tempPhInvestmentFee=0.0;
		double tempPhNetHolderAcc=0.0;
		BigDecimal tempFinalPhNetHolderAcc=new BigDecimal(0.0);
		String fundPerStr = (master.getRequestBean().getFundPerform()!=null && !master.getRequestBean().getFundPerform().equals(""))?master.getRequestBean().getFundPerform():"8";
		double fundPerD = (Double.parseDouble(fundPerStr))/100;
		double fundGrowthPM = Math.pow(1+fundPerD,0.0833333333333333333333333)-1;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyCharges=0.0;
		double RiderCharges=0.0;
		double totRiderCharges=0.0;
		double fullTopUpPremium=0.0;
		double fullPartialWithdrawal = 0.0;
		double completeTopUpPremium=0.0;
		double completePartialWithdrawal = 0.0;
		double mainPartialWithdrawal = 0.0;
		double yearlyPartialWithdrawal = 0.0;
		double yearlyTopUpPremium=0.0;

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;

			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			double investmentAmount=0.0;
			if (count<=master.getRequestBean().getPremiumpayingterm()) {
				investmentAmount = tempPremium - PAC;
			}
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
			//}

			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			// END

			tempFulllMortalityCharges=0.0;
			fullPartialWithdrawal=0.0;
			fullTopUpPremium=0.0;
			yearlyTopUpPremium=0.0;
			yearlyPartialWithdrawal = 0.0;
			BigDecimal investValue = new BigDecimal(0.0);
			//Addedd by Samina Mohammad iFlex
			for(int i=1;i<=12;i++) {
				double finalLoyalty=0.0;
				//double topupmount=0.0;
				double topupfinaltax=0.0;
				double tempHoldetNettoInvest = investmentAmount ;
				if (count<=ppt) {
					if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
						tempHoldetNettoInvest = investmentAmount;
						investValue=BigDecimal.valueOf(tempPremium);
						//hmPHIRRTermTW.put((count-1)*12+i,BigDecimal.valueOf(tempPremium-partialWithdrawal));
					} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2;
						investValue=BigDecimal.valueOf(tempPremium*0.5);
						//hmPHIRRTermTW.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.5-partialWithdrawal));
					} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4;
						investValue=BigDecimal.valueOf(tempPremium*0.25);
						//hmPHIRRTermTW.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.25-partialWithdrawal));
					} else if (frequency.equalsIgnoreCase("M")) {
						double tempMPremium = Math.round(tempPremium*0.0833);
						investValue=BigDecimal.valueOf(tempMPremium);
						//hmPHIRRTermTW.put((count-1)*12+i,BigDecimal.valueOf(tempMPremium-partialWithdrawal));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12;
					} else {
						investValue=BigDecimal.valueOf(0.0);
						//hmPHIRRTermTW.put((count-1)*12+i,new BigDecimal(0.0-partialWithdrawal));
						tempHoldetNettoInvest = 0.0;
					}
				} else {
					investValue=BigDecimal.valueOf(0.0);
					//hmPHIRRTermTW.put((count-1)*12+i,new BigDecimal(0.0-partialWithdrawal));
					tempHoldetNettoInvest = 0.0;
				}
				investValue = BigDecimal.valueOf(investValue.doubleValue() - partialWithdrawal);
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					//|| count!=1)
					topUpPremium = 0.0;
					partialWithdrawal = 0.0;
				} else {
					topUpPremium = 0.0;
					if (!topUpAmount.equals("")) {
						if (count>=Integer.parseInt(topUpStartYear) && count<=Integer.parseInt(topUpEndYear)) {
							topUpPremium = Double.parseDouble(topUpAmount);
							yearlyTopUpPremium=topUpPremium;
							/*if (frequency.equalsIgnoreCase("S"))
								topUpPremium/=2;
							if (frequency.equalsIgnoreCase("Q"))
								topUpPremium/=4;*/
							fullTopUpPremium+=topUpPremium;
							if (frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")){
								//topupmount=(topUpPremium);
								topupfinaltax=(topUpPremium*.015);
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium;
							} else if (frequency.equalsIgnoreCase("M")){
								topUpPremium=(topUpPremium/12);
								topupfinaltax=(topUpPremium*.015);
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium/12;
							} else if (frequency.equalsIgnoreCase("S")){
								topUpPremium=(topUpPremium/2);
								topupfinaltax=(topUpPremium*.015);
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium/2;
							}
							else{
								topUpPremium=(topUpPremium/4);
								topupfinaltax=(topUpPremium*.015);
								tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
								completeTopUpPremium+=topUpPremium/4;
							}
						}
						CommonConstant.printLog("d", "tempHoldetNettoInvestTopup" + tempHoldetNettoInvest);
					} if (!topUpAmountYear.equals("")) {

						String[] topUpVar = topUpAmountYear.split("-");
						for (int l=0;l<topUpVar.length;l++) {
							if (topUpVar[l].equals(count+","+topUpVar[l].substring(topUpVar[l].indexOf(",")+1,topUpVar[l].length()))) {
								topUpPremium = Double.parseDouble(topUpVar[l].substring(topUpVar[l].indexOf(",")+1,topUpVar[l].length()));
								yearlyTopUpPremium=topUpPremium;
								/*if (frequency.equalsIgnoreCase("S"))
									topUpPremium/=2;
								if (frequency.equalsIgnoreCase("Q"))
									topUpPremium/=4;*/
								fullTopUpPremium+=topUpPremium;
								if (frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")){
									//topUpPremium=(topUpPremium);
									topupfinaltax=(topUpPremium*.015);
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium;
								} else if (frequency.equalsIgnoreCase("M")){
									topUpPremium=(topUpPremium/12);
									topupfinaltax=(topUpPremium*.015);
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium/12;
								} else if (frequency.equalsIgnoreCase("S")){
									topUpPremium=(topUpPremium/2);
									topupfinaltax=(topUpPremium*.015);
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium/2;
								}
								else{
									topUpPremium=(topUpPremium/4);
									topupfinaltax=(topUpPremium*.015);
									tempHoldetNettoInvest+=(topUpPremium-topupfinaltax);
									completeTopUpPremium+=topUpPremium/4;
								}
								l=topUpVar.length;
							}
						}
					}
					partialWithdrawal = 0.0;
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
							yearlyPartialWithdrawal = partialWithdrawal;
							/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;
							fullPartialWithdrawal+=partialWithdrawal;*/
							completePartialWithdrawal+=partialWithdrawal;
						}
					} if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							if (withdrawVar[l].equals(count+","+withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()))) {
								//withdrawVar[l].contains(count+",");
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								yearlyPartialWithdrawal = partialWithdrawal;
								/*if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;*/
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							}
						}
					}
				}
				//Samina Changes on 4/08/2014

					/*if(partialWithdrawal!=0 )
					{
							if(frequency.equalsIgnoreCase("O") || frequency.equalsIgnoreCase("M") ){
								if((count-1)%1==0){
									partialWithdrawal=(partialWithdrawal*0.0833333333333333333);
			                       }
							}
							else if(frequency.equalsIgnoreCase("S")){
									if((count-1)%6==0){
										partialWithdrawal=(partialWithdrawal*0.5);
									}
							}
							else if(frequency.equalsIgnoreCase("Q")){
								if((count-1)%3==0){
									partialWithdrawal=(partialWithdrawal*0.25);
								}
						}
							else if(frequency.equalsIgnoreCase("A")){
							if((count-1)%12==0){
								partialWithdrawal=(partialWithdrawal*1);
							}
					}
					}*/

				investValue = BigDecimal.valueOf(investValue.doubleValue() + topUpPremium);
				hmPHIRRTermTW.put((count-1)*12+i,investValue);

				if (frequency.equals("M"))
					partialWithdrawal= (partialWithdrawal/12);
				else if (frequency.equals("S"))
					partialWithdrawal= (partialWithdrawal/2);
				else if (frequency.equals("Q"))
					partialWithdrawal= (partialWithdrawal/4);
				fullPartialWithdrawal+=partialWithdrawal;


				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #investValue Charges: "
						+ investValue);

				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fullPartialWithdrawal Charges: "
						+ fullPartialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #topUpPremium Charges: "
						+ topUpPremium);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);
				if (topUpPremium!=0.0) {
					if (master.getRequestBean().getInsured_age()+count-1 <45) {
						topUpInsuredAmount += (topUpPremium*1.25);
						//topUpInsuredAmount = completeTopUpPremium*1.25;
					} else if (master.getRequestBean().getInsured_age()+count-1 >=45) {
						topUpInsuredAmount += (topUpPremium*1.1);
						//topUpInsuredAmount = completeTopUpPremium*1.1;
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #topUpInsuredAmount Charges: "
						+ topUpInsuredAmount);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempHoldetNettoInvest Charges: "
						+ tempHoldetNettoInvest);
			oFormulaBean.setA(tempHoldetNettoInvest+tempNetHolderAcc-topUpInsuredAmount);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + "tempPhNetHolderAcc" + tempPhNetHolderAcc + "topUpInsuredAmount"
					+ topUpInsuredAmount);
			oFormulaBean.setCNT(count);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #WDA: "
					+ mainPartialWithdrawal);
				RiderCharges = getRiderCharges(count,age,ppt,i);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #RiderCharges Charges: "
						+ RiderCharges);


			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " and full Mortc " + tempFulllMortalityCharges);
				totRiderCharges=totRiderCharges+RiderCharges;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #totRiderCharges: "
						+ totRiderCharges);
				tempNetHolderInvestment = tempHoldetNettoInvest - templMortalityCharges -  dAdminCharge - RiderCharges;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges: "
					+ tempNetHolderInvestment);
			tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvestment;
			//totalNetHolderInvestment+=tempNetHolderInvestment;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges: "
					+ tempPHAccBeforeGrowth);
			oFormulaBean.setA(0.1);

			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges: "
					+ fundGrowthPM);

				tempPHAccInvestGrowth=(tempPHAccBeforeGrowth*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges: "
					+ tempPHAccInvestGrowth);
			tempPHAccHolder=tempPHAccBeforeGrowth+tempPHAccInvestGrowth;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges: "
					+ tempPHAccHolder);
			oFormulaBean.setA(tempPHAccHolder);
			String tempPhInvestmentFeeValue = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE,
					oFormulaBean)
					+ "";
			tempPhInvestmentFee = Double.parseDouble(tempPhInvestmentFeeValue);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges: "
					+ tempPhInvestmentFee);

			tempPhNetHolderAcc=tempPHAccHolder-tempPhInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges: "
					+ tempPhNetHolderAcc);




			phAccount = tempPHAccHolder;
			phInvestmentFee = tempPhInvestmentFee;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);


			netPHAccount = phAccount - phInvestmentFee;

			// Storing regular fund values
			if (i == 12) {
										oFormulaBean.setA(tempPhNetHolderAcc);
						String tempLoyaltyCharges = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyCharges=Double.parseDouble(tempLoyaltyCharges);
					 finalLoyalty=loyaltyCharges;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));

				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal);

				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal);


				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);

				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
				CommonConstant.printLog("d", "#Policy Term : " + count + " #TopUpPremium " + fullTopUpPremium);
				hmTopUpPremium.put(count, Math.round(yearlyTopUpPremium));

			}
				if (i==1) {
					CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
					hmWithdrawalAmount.put(count, Math.round(yearlyPartialWithdrawal));
				}
				int currentMonth = (count-1)*12+i;
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					hmWithdrawalAmountDouble.put(currentMonth,0.0);
				} else {
					hmWithdrawalAmountDouble.put(currentMonth,partialWithdrawal);
					mainPartialWithdrawal=0.0;
					if (master.getRequestBean().getInsured_age()+count-1 >=60) {
						for (int k=currentMonth;k>0;k--) {
							int inAge = k%12!=0?k/12+1:k/12;
							if ((hmWithdrawalAmountDouble.get(k)!=null) && (master.getRequestBean().getInsured_age()+inAge-1>=58)){
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDouble.get(k));
								mainPartialWithdrawal+=hmWithdrawalAmountDouble.get(k);
							}
						}
					} else {
						for (int k=currentMonth;k>=currentMonth-23;k--) {
							if (hmWithdrawalAmountDouble.get(k)!=null && k>=1) {
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDouble.get(k));
								mainPartialWithdrawal += hmWithdrawalAmountDouble.get(k);
							}
						}
					}
					CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
							+ mainPartialWithdrawal);
				}
				tempFinalPhNetHolderAcc=BigDecimal.valueOf(tempPhNetHolderAcc+finalLoyalty-partialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges: "
						+ tempFinalPhNetHolderAcc);
				tempNetHolderAcc=tempFinalPhNetHolderAcc.doubleValue();
				totalFundValueAtEndtPolicyYear = Math
				.round(tempNetHolderAcc);


				long surrenderValue = getSurrenderValue(count, tempFinalPhNetHolderAcc.doubleValue());
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmSurrenderValue" + surrenderValue);
			// END
			}

			//End iFlex

			// Mortility Charges Monthly start
			age++;

			//Added by Samina Mohammad 02-04-2014 for iFlexi
			//netPHInvestment = totalNetHolderInvestment;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityCharges);
			}

			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			// Calculate Death benefit at the end of policy year

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);


			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
			String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		double lDBEP = Double.parseDouble(strDBEP);

			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			// END
			// Calculate Commission payble


			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);

			// END

		}
		hmPHIRRTermTW.put(polTerm*12+1,tempFinalPhNetHolderAcc);
		double netYield = getIRRAlternate(hmPHIRRTermTW , polTerm*12);
		//double netYield = 0.5;
		netYield = netYield * 100;
		DecimalFormat df = new DecimalFormat("#.##");
		CommonConstant.printLog("d", "#NetYield before Parse: " + netYield);
		netYield = Double.parseDouble(df.format(netYield));
		//master.setNetYield8P(6.19);
		master.setNetYield8TW(netYield);
		CommonConstant.printLog("d", "#NetYield Top UP Withdrawal: " + netYield);

	}

	public void calculateAlternateChargesSMARTPH10ST() {//Added by Samina for Alternate Scenrio SMART
		CommonConstant.printLog("d", "Inside calculateChargesSMART10 without Service Tax");
		double PAC = 0.0;
		int age = master.getRequestBean().getInsured_age();
		String frequency=master.getRequestBean().getFrequency();
		int polTerm = master.getRequestBean().getPolicyterm();
		int ppt=master.getRequestBean().getPremiumpayingterm();
		double phAccount = 0.0;
		double phInvestmentFee = 0.0;
		double netPHAccount = 0.0;
		double lAdminCharge = 0.0;
		double dAdminCharge = 0.0;
		double tempNetHolderAccSMART10=0.0;
		double tempFulllMortalityChargesSMART10 = 0.0;
		double tempNetHolderInvestmentSMART10 = 0.0;
		double tempPHAccBeforeGrowthSMART10 = 0.0;
		double tempPHAccInvestGrowthSMART10=0.0;
		double tempPHAccHolderSMART10=0.0;
		double tempPhInvestmentFeeSMART10=0.0;
		double tempPhNetHolderAccSMART10=0.0;
		BigDecimal tempFinalPhNetHolderAccSMART10=new BigDecimal(0.0);
		double fundGrowthPM = 0.00643403011;
		long totalFundValueAtEndtPolicyYear=0;
		double totalCharges=0.0;
		double loyaltyChargesSMART10=0.0;
		double amountInDebtFundEnd = 0.0;
		double debtFundProp = 0.0;
		double equityFMC = 0.0;
		double debtFMC = 0.0;
		double amtDebtFundBeforeTransfer=0.0;
		double FMCDebtFund = 0.0;

		String withdrawalAmount = "";
		String withdrawalStartYear = "";
		String withdrawalEndYear = "";
		String withdrawalAmountYear = "";
		if (master.getRequestBean().getFixWithDAmt()!= null && !master.getRequestBean().getFixWithDAmt().equals("")) {
			withdrawalAmount = master.getRequestBean().getFixWithDAmt();
			withdrawalStartYear = master.getRequestBean().getFixWithDStYr();
			withdrawalEndYear = master.getRequestBean().getFixWithDEndYr();
		} if (master.getRequestBean().getVarWithDAmtYr()!= null && !master.getRequestBean().getVarWithDAmtYr().equals("")) {
			withdrawalAmountYear = master.getRequestBean().getVarWithDAmtYr();
			withdrawalAmountYear = "-" + withdrawalAmountYear.substring(0, withdrawalAmountYear.length()-1);
		}
		double partialWithdrawal=0.0;
		double fullPartialWithdrawal = 0.0;
		double completePartialWithdrawal = 0.0;
		double mainPartialWithdrawal = 0.0;
		HashMap hmWithdrawalAmountSM10 = new HashMap();
		HashMap<Integer, Double> hmWithdrawalAmountDoubleSM10 = new HashMap<Integer, Double>();
		HashMap<Integer,BigDecimal> hmPHIRRTermSMART = new HashMap();
		double yearlyPartialWithdrawal = 0.0;
		double tempPremium = master.getBaseAnnualizedPremium();

		oFormulaBean = getFormulaBeanObj();
		for (int count = 1; count <= polTerm; count++) {
			double phInvestmentFeeTotal = 0.0;
			long regularFundValueBeforeFMC = 0;
			double totRiderCharges=0.0;
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			double FMC = DataAccessClass.getFMC(oFormulaBean.getRequestBean())/100;
			FormulaHandler formulaHandler = new FormulaHandler();
			String premAllocCharge = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PRIMIUM_ALLOCATION_CHARGES,
					oFormulaBean)
					+ "";
			if (count<=ppt)
				PAC = Double.parseDouble((premAllocCharge));
			else
				PAC = 0.0;
			CommonConstant.printLog("d", "#calculateChargesForPH10  #Policy term : " + count
					+ " #Premium Allocation Charges : " + PAC);
			double investmentAmount=0.0;
			if (count<=ppt) {
				investmentAmount = tempPremium - PAC;
			}
			oFormulaBean.setP(master.getRequestBean().getBasepremiumannual());
				String AdminCharges = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_ADMIN_CHARGES, oFormulaBean)
						+ "";
				dAdminCharge = Double.parseDouble(AdminCharges);
				lAdminCharge = dAdminCharge * 12;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + lAdminCharge);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #Policy Admin Charges: " + dAdminCharge);
			CommonConstant.printLog("d", "#Policy term : " + count + " #adminCharge monthly : "
					+ dAdminCharge);
			tempFulllMortalityChargesSMART10=0.0;
			fullPartialWithdrawal=0.0;
			completePartialWithdrawal = 0.0;
			yearlyPartialWithdrawal = 0.0;
			for(int i=1;i<=12;i++) {
				double finalLoyaltySMART10=0.0;//Added by Samina for Alternate Scenario 3
				double tempHoldetNettoInvest = investmentAmount ;
				if (count<=ppt) {
					if ((frequency.equalsIgnoreCase("A") || frequency.equalsIgnoreCase("O")) && i==1) {
						tempHoldetNettoInvest = investmentAmount;
						hmPHIRRTermSMART.put((count-1)*12+i,BigDecimal.valueOf(tempPremium-partialWithdrawal));
					} else if (frequency.equalsIgnoreCase("S") && (i==1 || i == 7)) {
						hmPHIRRTermSMART.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.5-partialWithdrawal));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 2 - PAC / 2;
					} else if (frequency.equalsIgnoreCase("Q") && (i==1 || i == 7 || i==4 || i == 10)) {
						hmPHIRRTermSMART.put((count-1)*12+i,BigDecimal.valueOf(tempPremium*0.25-partialWithdrawal));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 4 - PAC / 4;
					} else if (frequency.equalsIgnoreCase("M")) {
						double tempMPremium = Math.round(tempPremium*0.0833);
						hmPHIRRTermSMART.put((count-1)*12+i,BigDecimal.valueOf(tempMPremium-partialWithdrawal));
						tempHoldetNettoInvest = master.getBaseAnnualizedPremium() / 12 - PAC / 12;
					} else {
						hmPHIRRTermSMART.put((count-1)*12+i,new BigDecimal(0.0-partialWithdrawal));
						tempHoldetNettoInvest = 0.0;
					}
				} else {
					hmPHIRRTermSMART.put((count-1)*12+i,new BigDecimal(0.0-partialWithdrawal));
					tempHoldetNettoInvest = 0.0;
				}
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
							|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && (i!=1))){
					partialWithdrawal = 0.0;
				} else {
					partialWithdrawal = 0.0;
					if (!withdrawalAmount.equals("")) {
						if (count>=Integer.parseInt(withdrawalStartYear) && count<=Integer.parseInt(withdrawalEndYear)) {
							partialWithdrawal = Double.parseDouble(withdrawalAmount);
							yearlyPartialWithdrawal = partialWithdrawal;
							/*if (frequency.equalsIgnoreCase("S"))
								partialWithdrawal/=2;
							if (frequency.equalsIgnoreCase("Q"))
								partialWithdrawal/=4;
							fullPartialWithdrawal+=partialWithdrawal;*/
							completePartialWithdrawal+=partialWithdrawal;
						}
					} if (!withdrawalAmountYear.equals("")) {
						String[] withdrawVar = withdrawalAmountYear.split("-");
						for (int l=0;l<withdrawVar.length;l++) {
							CommonConstant.printLog("d", "withdrawVar of " + l + " is " + withdrawVar[l]);
							if (withdrawVar[l].equals(count+","+withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()))) {
								//withdrawVar[l].contains(count+",");
								partialWithdrawal = Double.parseDouble(withdrawVar[l].substring(withdrawVar[l].indexOf(",")+1,withdrawVar[l].length()));
								yearlyPartialWithdrawal = partialWithdrawal;
								/*if (frequency.equalsIgnoreCase("S"))
									partialWithdrawal/=2;
								if (frequency.equalsIgnoreCase("Q"))
									partialWithdrawal/=4;
								fullPartialWithdrawal+=partialWithdrawal;*/
								completePartialWithdrawal+=partialWithdrawal;
								l=withdrawVar.length;
							}
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fullPartialWithdrawal Charges: "
						+ fullPartialWithdrawal);
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #partialWithdrawal Charges: "
						+ partialWithdrawal);
			tempNetHolderInvestmentSMART10 = tempHoldetNettoInvest - dAdminCharge;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #NetHolderInvestment Charges SMART10: "
					+ tempNetHolderInvestmentSMART10);
			tempPHAccBeforeGrowthSMART10 = tempNetHolderAccSMART10 + tempNetHolderInvestmentSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccBeforeGrowth Charges SMART10: "
					+ tempPHAccBeforeGrowthSMART10);
			oFormulaBean.setA(0.1);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #fundGrowthPM Charges SMART10: "
					+ fundGrowthPM);
			tempPHAccInvestGrowthSMART10=(tempPHAccBeforeGrowthSMART10*fundGrowthPM);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccInvestGrowth Charges SMART10: "
					+ tempPHAccInvestGrowthSMART10);
			tempPHAccHolderSMART10=tempPHAccBeforeGrowthSMART10+tempPHAccInvestGrowthSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PHAccHolder Charges SMART10: "
					+ tempPHAccHolderSMART10);
			double propTransferDtoE = (i==12?1.0:(1.0/(13.0-(i%12.0))));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #propTransferDtoE Charges SMART10: "
					+ propTransferDtoE);
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amountInDebtFundEnd "
					+ amountInDebtFundEnd + "debtFundProp" + debtFundProp + "investmentAmount" + investmentAmount);
			if(count==1 && i==1){
			 amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund + tempHoldetNettoInvest;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundBeforeTransfer Charges SMART10: "
					+ amtDebtFundBeforeTransfer);
			}
			else
			{
			amtDebtFundBeforeTransfer = amountInDebtFundEnd - FMCDebtFund + tempHoldetNettoInvest;
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " for FMC " + FMC + " #amtDebtFundBeforeTransfer Charges SMART10: "
						+ amtDebtFundBeforeTransfer);
			}
			double transferFromDebtFund = amtDebtFundBeforeTransfer * propTransferDtoE;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #transferFromDebtFund Charges SMART10: "
					+ transferFromDebtFund);
			double amtDebtFundStartPrem = i==12?0.0:(transferFromDebtFund*(12.0-(i%12.0)));
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amtDebtFundStartPrem Charges SMART10: "
					+ amtDebtFundStartPrem);
			amountInDebtFundEnd=tempPHAccHolderSMART10/(tempFinalPhNetHolderAccSMART10.doubleValue()+tempHoldetNettoInvest)*amtDebtFundStartPrem;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #amountInDebtFundEnd Charges SMART10: "
					+ amountInDebtFundEnd);
			debtFundProp=amountInDebtFundEnd/tempPHAccHolderSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #debtFundProp Charges SMART10: "
					+ debtFundProp);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				debtFMC = master.getRequestBean().getSmartDebtFundFMC();
			FMCDebtFund = amountInDebtFundEnd*debtFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCDebtFund Charges SMART10: "
					+ FMCDebtFund);
			if (master.getRequestBean().getSmartDebtFund()!=null)
				equityFMC = master.getRequestBean().getSmartEquityFundFMC();
			double FMCEquityFund = (tempPHAccHolderSMART10-amountInDebtFundEnd)*equityFMC/1200;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #FMCEquityFund Charges SMART10: "
					+ FMCEquityFund);
			/*oFormulaBean.setA(tempPHAccHolderSMART10);
			String tempPhInvestmentFeeValueSMART10 = formulaHandler.evaluateFormula(
					master.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_PH_INVESTMENT_FEE_AAA,
					oFormulaBean)
					+ "";
			tempPhInvestmentFeeSMART10 = Double.parseDouble(tempPhInvestmentFeeValueSMART10);*/
			tempPhInvestmentFeeSMART10 = FMCEquityFund+FMCDebtFund;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhInvestmentFe Charges SMART10: "
					+ tempPhInvestmentFeeSMART10);
			tempPhNetHolderAccSMART10=tempPHAccHolderSMART10-tempPhInvestmentFeeSMART10;
			CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #PhNetHolderAcc Charges SMART10: "
					+ tempPhNetHolderAccSMART10);
			phAccount = tempPHAccHolderSMART10;
			phInvestmentFee = tempPhInvestmentFeeSMART10;
			phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestmentFee;
			CommonConstant.printLog("d", "#Policy term : " + count + " #Month:: " + i
					+ " #phInvestmentFeeValue: " + phInvestmentFee);
			netPHAccount = phAccount - phInvestmentFee;
			if (i == 12) {
					 oFormulaBean.setA(tempPhNetHolderAccSMART10);
						String tempLoyaltyChargesSMART10 = formulaHandler.evaluateFormula(
								master.getRequestBean().getBaseplan(),
								FormulaConstant.CALCULATE_LC,
								oFormulaBean)
								+ "";
						loyaltyChargesSMART10=Double.parseDouble(tempLoyaltyChargesSMART10);
					 finalLoyaltySMART10=loyaltyChargesSMART10;
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #RegularFundValue:  "
						+ Math.round(netPHAccount));
				regularFundValueBeforeFMC = Math.round(netPHAccount
						+ phInvestmentFeeTotal);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmRegularFundValueBeforeFMC: "
						+ regularFundValueBeforeFMC + " #netPHAccount:  "
						+ netPHAccount + " #phInvestmentFeeTotal: "
						+ phInvestmentFeeTotal);
				long gurananteedMaturityAmount = getGuaranteedMaturityAddition(
						count, netPHAccount);
				CommonConstant.printLog("d", "#Policy term : " + count
						+ " #hmGuaranteedMaturityAmount: "
						+ gurananteedMaturityAmount);
	        	CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges SMART10: "
						+ tempFinalPhNetHolderAccSMART10);
				}
				if (frequency.equals("M"))
					partialWithdrawal= (partialWithdrawal/12);
				else if (frequency.equals("S"))
					partialWithdrawal= (partialWithdrawal/2);
				else if (frequency.equals("Q"))
					partialWithdrawal= (partialWithdrawal/4);
				fullPartialWithdrawal+=partialWithdrawal;
			if (i==1) {
				CommonConstant.printLog("d", "#Policy Term : " + count + " #Partial Withdrawal " + fullPartialWithdrawal);
					hmWithdrawalAmountSM10.put(count, Math.round(yearlyPartialWithdrawal));
				}
				int currentMonth = (count-1)*12+i;
				if((frequency.equalsIgnoreCase("A") && i!=1) || (frequency.equalsIgnoreCase("S") && (i!=1 && i != 7))
						|| (frequency.equalsIgnoreCase("Q") && (i!=1 && i != 7 && i!=4 && i != 10)) || (frequency.equalsIgnoreCase("O") && i!=1)){
					hmWithdrawalAmountDoubleSM10.put(currentMonth,0.0);
				} else {
					hmWithdrawalAmountDoubleSM10.put(currentMonth,partialWithdrawal);
				mainPartialWithdrawal=0.0;
				if (master.getRequestBean().getInsured_age()+count-1 >=60) {
						for (int k=currentMonth;k>0;k--) {
							int inAge = k%12!=0?k/12+1:k/12;
							if ((hmWithdrawalAmountDoubleSM10.get(k)!=null) && (master.getRequestBean().getInsured_age()+inAge-1>=58)){
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM10.get(k));
							mainPartialWithdrawal+=hmWithdrawalAmountDoubleSM10.get(k);
						}
					}
				} else {
						for (int k=currentMonth;k>=currentMonth-23;k--) {
							if (hmWithdrawalAmountDoubleSM10.get(k)!=null && k>=1) {
								//CommonConstant.printLog("d","For Month "+k+" Withdrawal is "+hmWithdrawalAmountDoubleSM10.get(k));
							mainPartialWithdrawal += hmWithdrawalAmountDoubleSM10.get(k);
						}
					}
				}
				CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #mainPartialWithdrawal Charges: "
						+ mainPartialWithdrawal);
			}
	            tempFinalPhNetHolderAccSMART10=BigDecimal.valueOf(tempPhNetHolderAccSMART10+finalLoyaltySMART10-partialWithdrawal);
	            CommonConstant.printLog("d", "#Policy term : " + count + " after Month " + i + " #tempFinalPhNetHolderAcc Charges SMART10: "
						+ tempFinalPhNetHolderAccSMART10);
				tempNetHolderAccSMART10=tempFinalPhNetHolderAccSMART10.doubleValue();
				totalFundValueAtEndtPolicyYear = Math.round(tempNetHolderAccSMART10);
			double surrendercharges=getSurrenderValuePH10_AS3(count, tempFinalPhNetHolderAccSMART10.doubleValue());
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmSurrenderValueSMART10" + surrendercharges);
			}
			age++;
			if(count<=ppt)
			{
			totalCharges=Math.round(PAC+totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART10);
		 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totRiderCharges "
				 + totRiderCharges);
			}else{
				totalCharges=Math.round(totRiderCharges+lAdminCharge+tempFulllMortalityChargesSMART10);
			}
			 CommonConstant.printLog("d", "#Policy term : " + count + " after year " + count + " #totalCharges "
					 + totalCharges);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #ThmFundManagementCharge ::: "
					+ Math.round(phInvestmentFeeTotal));
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #hmTotalFundValueAtEndPolicyYear:  "
					+ totalFundValueAtEndtPolicyYear);
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setA(totalFundValueAtEndtPolicyYear);
				String strDBEP = formulaHandler
					.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR,
							oFormulaBean)
					+ "";
		       double lDBEP = Double.parseDouble(strDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
			CommonConstant.printLog("d", "#Policy term : " + count
					+ " #Death Benifit At EndOf Policy Year: " + lDBEP);
		}
		hmPHIRRTermSMART.put(polTerm*12+1,tempFinalPhNetHolderAccSMART10);
		double netYield = getIRRAlternate(hmPHIRRTermSMART , polTerm*12);
		//double netYield = 0.5;
		netYield = netYield * 100;
		DecimalFormat df = new DecimalFormat("#.##");
		CommonConstant.printLog("d", "#NetYield before Parse: " + netYield);
		netYield = Double.parseDouble(df.format(netYield));
		//master.setNetYield8P(6.19);
		master.setNetYield8SMART(netYield);
		CommonConstant.printLog("d", "#NetYield: " + netYield);

	}

	/*public double getIRR(HashMap<Integer,BigDecimal> hmPHIRRTerm , int polTerm) {
	    double guessLower = 0.0050;
	    double guessUpper = 0.0050;
	    double previousUpper = 0.0050;
	    double finalGuess = 0.0050;
	    double inc   = 0.0010;
	    BigDecimal npv = new BigDecimal(0.0);
	    boolean checkFlag = true;
	    boolean minusLast = false;
	    do {
	    	//guess += inc;
	    	npv = new BigDecimal(0.0); //net present value
	        for (int i=0; i<=polTerm; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + guessUpper, i));
	        	CommonConstant.printLog("d","Cashflow "+hmPHIRRTerm.get(i));
	            npv = npv.add((hmPHIRRTerm.get(i).divide(powerNPV,6,RoundingMode.HALF_UP)));
	        }
	        if (npv.compareTo(new BigDecimal(0.0))==1 && !minusLast) {
	        	guessLower = guessUpper;
	        	guessUpper+=inc;
	        } else {
	        	if (guessUpper==0.0050) {
	        		guessLower = guessUpper;
    				guessUpper = previousUpper;
	        		//finalGuess = guessUpper;
	        		//checkFlag=false;
	        	} else {
	        		if (npv.compareTo(new BigDecimal(0.0))==-1)
	        			if (previousUpper-guessUpper<=0.0001) {
	        				finalGuess = previousUpper;
	        				checkFlag=false;
	        			} else {
	        				guessLower = guessUpper;
	        				guessUpper = previousUpper;
	        			}
	        	}
	        	minusLast=true;
	        	previousUpper = guessUpper;
	        	guessUpper=guessLower - (guessLower-guessUpper)/2;




	        	/*if (minusLast) {
		        	guessLower=guessUpper;
		        	guessUpper=guessLower - (guessLower-guessUpper)/2;
		        	minusLast = false;
	        	} else {
	        		guessLower=guessUpper;
	        		guessUpper=guessUpper - (guessUpper-guessLower)/2;
	        		minusLast = true;
	        	}*/
	        	/*if (guessUpper%0.001>0)
	        		guessUpper+=0.0005;
	        }
	        CommonConstant.printLog("d","NPV for "+guessUpper+" is "+npv);
	    } while (checkFlag);

	    finalGuess =  finalGuess * 100;
	    CommonConstant.printLog("d","#guess: " + finalGuess);
	    double netYield = Math.pow((1 + finalGuess/100),12) - 1;
	    return netYield;
	}*/


	/*public double getIRR(HashMap<Integer,BigDecimal> hmPHIRRTerm , int polTerm) {
    double guess = 0.00499;
    double inc   = 0.00001;
    BigDecimal zeroVal = new BigDecimal(0.0);
    BigDecimal npv = new BigDecimal(0.0);
    BigDecimal cashFlow = new BigDecimal(0.0);
    do {
    	guess += inc;
    	npv = new BigDecimal(0.0); //net present value
        for (int i=1; i<=polTerm+1; i++)  {
        	//CommonConstant.printLog("d","i is "+i);
        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + guess, i));
        	//CommonConstant.printLog("d","Cashflow "+hmPHIRRTerm.get(i));
        	if (i==polTerm+1)
        		cashFlow = hmPHIRRTerm.get(i);
        	else
        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
        	//CommonConstant.printLog("d","Cashflow "+cashFlow);
            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
        }
        CommonConstant.printLog("d","NPV for "+guess+" is "+npv);
    } while (npv.compareTo(new BigDecimal(0.0))==1);

    guess =  guess * 100;
    CommonConstant.printLog("d","#guess: " + guess);
    double netYield = Math.pow((1 + guess/100),12) - 1;
    return netYield;
	}*/

	public double getIRR(HashMap<Integer,BigDecimal> hmPHIRRTerm , int polTerm) {
	    double prevGuess = 0.004;
	    double currGuess = 0.004;
	    float inc   = 0.0001f;
	    boolean notFound = true;
	    double diffGuess = 0.0;
	    DecimalFormat df = new DecimalFormat("#.######");
	    BigDecimal zeroVal = new BigDecimal(0.0);
	    BigDecimal npv = new BigDecimal(0.0);
	    BigDecimal cashFlow = new BigDecimal(0.0);
	    //CommonConstant.printLog("d","prevGuess is "+prevGuess);
	    //CommonConstant.printLog("d","currGuess is "+currGuess);
	    do {
	    	//guess += inc;
	    	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + currGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        //CommonConstant.printLog("d","NPV for currGuess "+currGuess+" is "+npv);
	        if (npv.compareTo(new BigDecimal(0.0))==1) {
	        	//CommonConstant.printLog("d","NPV 1");
        		prevGuess = currGuess;
        		currGuess = (currGuess + inc);
            } else {
	            if (npv.compareTo(new BigDecimal(0.0))==-1) {
	            	diffGuess = Math.round((currGuess-prevGuess) * 1000000.0);
	            	//CommonConstant.printLog("d","NPV -1 "+diffGuess);
	            	//CommonConstant.printLog("d","diffGuess == 1 "+(diffGuess == 1));
	            	if ((currGuess == 0.004) || (diffGuess == 1) || (prevGuess==currGuess && currGuess!=0.004)) {
	            		//CommonConstant.printLog("d","NPV not found");
	            		notFound = false;
	            	} else {
	            		//CommonConstant.printLog("d","NPV found");
	            		double minusPoint = (currGuess - prevGuess) / 2;
	            		minusPoint = Double.parseDouble(df.format(minusPoint));
	            		currGuess = currGuess - minusPoint;
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		//CommonConstant.printLog("d","currGuess "+currGuess+" divide is "+(currGuess*1000000)%1);
	            		if ((currGuess*1000000)%1>0)
	            			currGuess+=0.000005;
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		inc = ((float)(currGuess - prevGuess)) / 2;
	            		//CommonConstant.printLog("d","inc is "+inc);
	            	}
	            }
            }
            currGuess = Double.parseDouble(df.format(currGuess));
            prevGuess = Double.parseDouble(df.format(prevGuess));
            if (currGuess==prevGuess) {
            	notFound = false;
            	if (npv.compareTo(new BigDecimal(0.0))==1) {
            		currGuess = currGuess - 0.000001;
            		currGuess = Double.parseDouble(df.format(currGuess));
            	}
            }
            //CommonConstant.printLog("d","NPV for new prevGuess"+prevGuess);
	        //CommonConstant.printLog("d","NPV for new currGuess"+currGuess);
	    } while (notFound);
	    if (prevGuess > currGuess) {
	    	currGuess = currGuess + 0.000002;
	    	currGuess = Double.parseDouble(df.format(currGuess));
	    }
	    if (diffGuess == 1 && currGuess != 0.004) {
        	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + prevGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        //CommonConstant.printLog("d","NPV for prevGuess "+prevGuess+" is "+npv);
        }
	    if (npv.compareTo(new BigDecimal(0.0))==-1)
	    	currGuess = prevGuess;
	    currGuess =  currGuess * 100;
	    CommonConstant.printLog("d", "#guess: " + currGuess);
	    double netYield = Math.pow((1 + currGuess/100),12) - 1;
	    return netYield;
	}


	/*public double getIRRAlternate(HashMap<Integer,BigDecimal> hmPHIRRTerm , int polTerm) {
		double prevGuess = 0.001;
	    double currGuess = 0.001;
	    float inc   = 0.0001f;
	    boolean notFound = true;
	    double diffGuess = 0.0;
	    DecimalFormat df = new DecimalFormat("#.######");
	    DecimalFormat df7 = new DecimalFormat("#.#######");
	    BigDecimal zeroVal = new BigDecimal(0.0);
	    BigDecimal npv = new BigDecimal(0.0);
	    BigDecimal cashFlow = new BigDecimal(0.0);
	    CommonConstant.printLog("d","prevGuess is "+prevGuess);
	    CommonConstant.printLog("d","currGuess is "+currGuess);
	    do {
	    	//guess += inc;
	    	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + currGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        CommonConstant.printLog("d","NPV for currGuess "+currGuess+" is "+npv);
	        if (npv.compareTo(new BigDecimal(0.0))==1) {
	        	CommonConstant.printLog("d","NPV 1");
        		prevGuess = currGuess;
        		currGuess = (currGuess + inc);
            } else {
	            if (npv.compareTo(new BigDecimal(0.0))==-1) {
	            	diffGuess = Math.round((currGuess-prevGuess) * 1000000.0);
	            	CommonConstant.printLog("d","NPV -1 "+diffGuess);
	            	CommonConstant.printLog("d","diffGuess == 1 "+(diffGuess == 1));
	            	if ((currGuess == 0.001) || (diffGuess == 1) || (prevGuess==currGuess && currGuess!=0.001)) {
	            		CommonConstant.printLog("d","NPV not found");
	            		notFound = false;
	            	} else {
	            		CommonConstant.printLog("d","NPV found");
	            		double minusPoint = (currGuess - prevGuess) / 2;
	            		minusPoint = Double.parseDouble(df.format(minusPoint));
	            		currGuess = currGuess - minusPoint;
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		CommonConstant.printLog("d","currGuess "+currGuess+" divide is "+(currGuess*1000000)%1);
	            		if ((currGuess*1000000)%1>0)
	            			currGuess+=0.000005;
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		inc = ((float)(currGuess - prevGuess)) / 2;
	            		CommonConstant.printLog("d","inc is "+inc);
	            	}
	            }
            }
            currGuess = Double.parseDouble(df.format(currGuess));
            prevGuess = Double.parseDouble(df.format(prevGuess));
            if (currGuess==prevGuess) {
            	notFound = false;
            	if (npv.compareTo(new BigDecimal(0.0))==1) {
            		currGuess = currGuess - 0.000001;
            		currGuess = Double.parseDouble(df.format(currGuess));
            	}
            }
            CommonConstant.printLog("d","NPV for new prevGuess"+prevGuess);
	        CommonConstant.printLog("d","NPV for new currGuess"+currGuess);
	    } while (notFound);
	    if (prevGuess > currGuess) {
	    	currGuess = currGuess + 0.000002;
	    	currGuess = Double.parseDouble(df.format(currGuess));
	    }
	    if (diffGuess == 1 && currGuess != 0.001) {
        	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + prevGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        CommonConstant.printLog("d","NPV for prevGuess "+prevGuess+" is "+npv);
        }
    	double prevGuess7p = Double.parseDouble(df7.format(currGuess));
	    double currGuess7p = Double.parseDouble(df7.format(prevGuess+0.0000001));
	    if (npv.compareTo(new BigDecimal(0.0))==-1) {
	    	CommonConstant.printLog("d","Inside changing curr to prev");
	    	currGuess = prevGuess;
	    	currGuess = Double.parseDouble(df.format(currGuess));
	    } else {
	    	do {
	    		npv = new BigDecimal(0.0); //net present value
		        for (int i=1; i<=polTerm+1; i++)  {
		        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + currGuess7p, i));
		        	if (i==polTerm+1)
		        		cashFlow = hmPHIRRTerm.get(i);
		        	else
		        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
		            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
		        }
		        CommonConstant.printLog("d","NPV for currGuess7p "+currGuess7p+" is "+npv);
		        if (npv.compareTo(new BigDecimal(0.0))==-1) {
		        	prevGuess7p = currGuess7p;
		        	prevGuess7p = Double.parseDouble(df7.format(prevGuess7p));
		        } else {
		        	currGuess7p = currGuess7p+0.0000001;
		        	currGuess7p = Double.parseDouble(df7.format(currGuess7p));
		        }
	    	} while (currGuess7p!=prevGuess7p);
	    }
	    if (currGuess7p!=prevGuess7p) {
	    	currGuess = currGuess * 100;
	    	currGuess = Double.parseDouble(df7.format(currGuess));
	    } else {
		    currGuess = currGuess7p * 100;
		    currGuess = Double.parseDouble(df.format(currGuess));
	    }
	    CommonConstant.printLog("d","#guess: " + currGuess);
	    double netYield = Math.pow((1 + currGuess/100),12) - 1;
	    return netYield;
	}*/

	public double getIRRAlternate(HashMap<Integer,BigDecimal> hmPHIRRTerm , int polTerm) {
		double prevGuess = 0.0005;
	    double currGuess = 0.0005;
	    float inc   = 0.0001f;
	    boolean notFound = true;
	    double diffGuess = 0.0;
	    DecimalFormat df = new DecimalFormat("#.######");
	    DecimalFormat df7 = new DecimalFormat("#.#######");
	    DecimalFormat df2 = new DecimalFormat("#.##");
	    BigDecimal zeroVal = new BigDecimal(0.0);
	    BigDecimal npv = new BigDecimal(0.0);
	    BigDecimal cashFlow = new BigDecimal(0.0);
	    CommonConstant.printLog("d", "prevGuess is " + prevGuess);
	    CommonConstant.printLog("d", "currGuess is " + currGuess);
	    do {
	    	//guess += inc;
	    	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + currGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        CommonConstant.printLog("d", "NPV for currGuess " + currGuess + " is " + npv);
	        if (npv.compareTo(new BigDecimal(0.0))==1) {
	        	CommonConstant.printLog("d", "NPV 1");
        		prevGuess = currGuess;
        		currGuess = (currGuess + inc);
            } else {
	            if (npv.compareTo(new BigDecimal(0.0))==-1) {
	            	diffGuess = Math.round((currGuess-prevGuess) * 1000000.0);
	            	CommonConstant.printLog("d", "NPV -1 " + diffGuess);
	            	CommonConstant.printLog("d", "diffGuess == 1 " + (diffGuess == 1));
	            	if ((currGuess == 0.0005) || (diffGuess == 1) || (prevGuess==currGuess && currGuess!=0.0005)) {
	            		CommonConstant.printLog("d", "NPV not found");
	            		notFound = false;
	            	} else {
	            		CommonConstant.printLog("d", "NPV found");
	            		double minusPoint = (currGuess - prevGuess) / 2;
	            		minusPoint = Double.parseDouble(df.format(minusPoint));
	            		currGuess = currGuess - minusPoint;
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		CommonConstant.printLog("d", "currGuess " + currGuess + " divide is " + (currGuess * 1000000) % 1);
	            		double curby100 = Double.parseDouble(df2.format((currGuess*1000000)));
	            		if (curby100%1>0) {
	            			CommonConstant.printLog("d", (currGuess * 1000000) + "");
	            			currGuess+=0.000005;
	            		}
	            		currGuess = Double.parseDouble(df.format(currGuess));
	            		inc = ((float)(currGuess - prevGuess)) / 2;
	            		CommonConstant.printLog("d", "inc is " + inc);
	            	}
	            }
            }
            currGuess = Double.parseDouble(df.format(currGuess));
            prevGuess = Double.parseDouble(df.format(prevGuess));
            if (currGuess==prevGuess) {
            	notFound = false;
            	if (npv.compareTo(new BigDecimal(0.0))==1) {
            		currGuess = currGuess - 0.000001;
            		currGuess = Double.parseDouble(df.format(currGuess));
            	}
            }
            CommonConstant.printLog("d", "NPV for new prevGuess" + prevGuess);
	        CommonConstant.printLog("d", "NPV for new currGuess" + currGuess);
	    } while (notFound);
	    if (prevGuess > currGuess) {
	    	currGuess = currGuess + 0.000002;
	    	currGuess = Double.parseDouble(df.format(currGuess));
	    }
	    if (diffGuess == 1 && currGuess != 0.0005) {
        	npv = new BigDecimal(0.0); //net present value
	        for (int i=1; i<=polTerm+1; i++)  {
	        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + prevGuess, i));
	        	if (i==polTerm+1)
	        		cashFlow = hmPHIRRTerm.get(i);
	        	else
	        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
	            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
	        }
	        CommonConstant.printLog("d", "NPV for prevGuess " + prevGuess + " is " + npv);
        }
    	double prevGuess7p = Double.parseDouble(df7.format(currGuess));
	    double currGuess7p = Double.parseDouble(df7.format(prevGuess+0.0000001));

		CommonConstant.printLog("d", "prevGuess7p: " + prevGuess7p);
		CommonConstant.printLog("d", "currGuess7p: " + currGuess7p);

	    if (npv.compareTo(new BigDecimal(0.0))==-1) {
	    	CommonConstant.printLog("d", "Inside changing curr to prev");
	    	currGuess = prevGuess;
	    	currGuess = Double.parseDouble(df.format(currGuess));
	    } else {
	    	do {
	    		npv = new BigDecimal(0.0); //net present value
		        for (int i=1; i<=polTerm+1; i++)  {
		        	BigDecimal powerNPV = new BigDecimal(Math.pow(1 + currGuess7p, i));
		        	if (i==polTerm+1)
		        		cashFlow = hmPHIRRTerm.get(i);
		        	else
		        		cashFlow = zeroVal.subtract(hmPHIRRTerm.get(i));
		            npv = npv.add(cashFlow.divide(powerNPV,6,RoundingMode.HALF_UP));
		        }
		        CommonConstant.printLog("d", "NPV for currGuess7p " + currGuess7p + " is " + npv);
		        if (npv.compareTo(new BigDecimal(0.0))==-1) {
		        	prevGuess7p = currGuess7p;
		        	prevGuess7p = Double.parseDouble(df7.format(prevGuess7p));
		        } else {
		        	currGuess7p = currGuess7p+0.0000001;
		        	currGuess7p = Double.parseDouble(df7.format(currGuess7p));
		        }
	    	} while (currGuess7p!=prevGuess7p);
	    }
	    if (currGuess7p!=prevGuess7p) {
	    	currGuess = currGuess * 100;
	    	currGuess = Double.parseDouble(df7.format(currGuess));
	    } else {
		    currGuess = currGuess7p * 100;
		    currGuess = Double.parseDouble(df.format(currGuess));
	    }
	    CommonConstant.printLog("d", "#guess: " + currGuess);
	    double netYield = Math.pow((1 + currGuess/100),12) - 1;
	    return netYield;
	}
	/*public double getIRR(HashMap<Integer,Double> hmPHIRRTerm , int polTerm) {
	    double guess = 0.0049;
	    double inc   = 0.0001;
	    double npv = 0.0;
	    do {
	    	guess += inc;
	    	npv = 0; //net present value
	        for (int i=0; i<=polTerm; i++)  {
	        	CommonConstant.printLog("d","Cashflow "+hmPHIRRTerm.get(i));
	            npv += (hmPHIRRTerm.get(i) / (Math.pow(1 + guess, i)));
	        }
	        CommonConstant.printLog("d","NPV for "+guess+" is "+npv);
	    } while ( npv > 0 );

	    guess =  guess * 100;
	    CommonConstant.printLog("d","#guess: " + guess);
	    double netYield = Math.pow((1 + guess/100),12) - 1;
	    return netYield;
	}*/

	/*public double getIRR(HashMap<Integer,Double> hmPHIRRTerm , int polTerm) {
	    double lowRate = 0.0050;
	    double highRate   = 0.5;
	    double maxIteration = 1000;
	    double precisionReq = 0.00000001;
	    int i = 0;
	    int j = 0;
	    double m = 0.0;
	    double oldRate = 0.00;
	    double newRate = 0.00;
	    double oldguessRate = lowRate;
	    double newguessRate = lowRate;
	    double guessRate = lowRate;
	    double lowGuessRate = lowRate;
	    double highGuessRate = highRate;
	    double npv = 0.0;
	    double denom = 0.0;
	    for(i=0; i<maxIteration; i++)
	    {
	     npv = 0.00;
	     for(j=0; j<polTerm; j++)
	     {
	      denom = Math.pow((1 + guessRate),j);
	      npv = npv + (hmPHIRRTerm.get(j)/denom);
	     }
	      /* Stop checking once the required precision is achieved
	     if((npv > 0) && (npv < precisionReq))
	      break;
	     if(oldRate == 0)
	      oldRate = npv;
	     else
	      oldRate = newRate;
	     newRate = npv;
	     if(i > 0)
	     {
	      if(oldRate < newRate)
	      {
	       if(oldRate < 0 && newRate < 0)
	        highGuessRate = newguessRate;
	       else
	        lowGuessRate = newguessRate;
	      }
	      else
	      {
	       if(oldRate > 0 && newRate > 0)
	        lowGuessRate = newguessRate;
	       else
	        highGuessRate = newguessRate;
	      }
	     }
	     oldguessRate = guessRate;
	     guessRate = (lowGuessRate + highGuessRate) / 2;
	     newguessRate = guessRate;
	    }
	    //return guessRate;
		guessRate =  guessRate * 100;
	    CommonConstant.printLog("d","#guess: " + guessRate);
	    double netYield = Math.pow((1 + guessRate/100),12) - 1;
	    return netYield;
	}*/

	public String getComissionText(){
		String comText="";
		String planCode = master.getRequestBean().getBaseplan();
		comText = DataAccessClass.getComissionText(planCode);
		return comText;
	}
	public long getAMT_ANNUITYRate() {
		long annuity = 0;
		int mode=0;
		try {
			oFormulaBean = getFormulaBeanObj();
			if(master.getRequestBean().getFrequency().equals("A"))
				mode=1;
			else if(master.getRequestBean().getFrequency().equals("S"))
				mode=2;
			else if(master.getRequestBean().getFrequency().equals("Q"))
				mode=4;
			else
				mode=12;
			oFormulaBean.setCNT(mode);
			FormulaHandler formulaHandler = new FormulaHandler();
			String annuityStr = formulaHandler.evaluateFormula(master
					.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_ANNUITY_AMMOUNT, oFormulaBean)
					+ "";
			annuity = Long.parseLong(annuityStr);
			CommonConstant.printLog("d", "Annuity Calculated :::: " + annuity);
		} catch (Exception e) {
			CommonConstant.printLog("e", "Error occured in calculating annuity" + e + " " + e.getMessage());
		}
		return annuity;
	}
	//Added by Samina for MIP
	private HashMap getSurrenderBenefitMIP_8() {

		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		//int ppt = master.getRequestBean().getPremiumpayingterm();
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		double finalTotalGAIVal = 0;
		double finalTotalGAIValSSV = 0;
		oFormulaBean.setRBV(0);
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
//
			double GAIValue = calculateGAI(count);

			oFormulaBean.setDoubleAGAI(totalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
//
			FormulaHandler formulaHandler = new FormulaHandler();
			//double GAIValue = calculateGAIDouble(count);
			oFormulaBean.setCNT(count-1);

			totalGAIVal = totalGAIVal + GAIValue;
			finalTotalGAIVal=totalGAIVal;
			finalTotalGAIValSSV=finalTotalGAIVal;
			String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.ONEPLUSRB8,
					oFormulaBean)
					+ "";
			onePlusRBD = Double.parseDouble(onePlusRB);
			onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
			oFormulaBean.setA(onePlusRBD);
			oFormulaBean.setDoubleAGAI(finalTotalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
			CommonConstant.printLog("d","getSurrenderBenefit_8");
			String strVsrbonus = "0";
			//oFormulaBean.setCNT(count);
			strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_8_D,
					oFormulaBean)
					+ "";
			oFormulaBean.setA(Double.parseDouble(strVsrbonus));
			//		}

			oFormulaBean.setCNT(count);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			//FormulaHandler formulaHandler = new FormulaHandler();
			String surrBenStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_8, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			oFormulaBean.setRBV(Double.parseDouble(strVsrbonus));
			// for policy term maturity value should be there
			/*if(count==polTerm){
				 surrBenStr = formulaHandler.evaluateFormula(master
						.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
						+ "";
				surrenderBenefit = Long.parseLong(surrBenStr);
			}*/
			if(count==polTerm)
				SBMap.put(count, 0);
			else
				SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d","Pol Term :::  Surr Benefit 8" + count + " : "
					+ surrenderBenefit);

		}
		return SBMap;
	}
	private HashMap getGuranteedSurrenderValue_MIP() {
		HashMap GSBMap = new HashMap();
		try {
			long guranteedSurrenderValue;
			double guranteedSurrenderval=0;
			long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setA(totalGAIVal);
				oFormulaBean.setAGAI(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				String guaSurrBenStr = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GURANTEED_SURRENDER_VALUE,
						oFormulaBean)
						+ "";
				guranteedSurrenderval=Double.parseDouble(guaSurrBenStr);

				if(count==polTerm)
					guranteedSurrenderValue=0;
				else
					guranteedSurrenderValue = Math.round(guranteedSurrenderval);
				GSBMap.put(count, guranteedSurrenderValue);
				CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
						+ guranteedSurrenderValue);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return GSBMap;
	}

    private HashMap getGuranteedSurrenderValue_LastYear() {
        HashMap GSBMap = new HashMap();
        try {
            long guranteedSurrenderValue;
            double guranteedSurrenderval=0;
            //long totalGAIVal = 0;
            int polTerm = master.getRequestBean().getPolicyterm();
            for (int count = 1; count <= polTerm; count++) {
                oFormulaBean = getFormulaBeanObj();
                oFormulaBean.setCNT(count);
                //oFormulaBean.setA(totalGAIVal);
                //oFormulaBean.setAGAI(totalGAIVal);
                FormulaHandler formulaHandler = new FormulaHandler();
                String guaSurrBenStr = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
                        FormulaConstant.CALCULATE_GURANTEED_SURRENDER_VALUE,
                        oFormulaBean)
                        + "";
                guranteedSurrenderval=Double.parseDouble(guaSurrBenStr);

                //if(count==polTerm)
                //guranteedSurrenderValue=0;
                //else
                guranteedSurrenderValue = Math.round(guranteedSurrenderval);
                GSBMap.put(count, guranteedSurrenderValue);
                CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
                        + guranteedSurrenderValue);
            }
        } catch (Exception e) {
            CommonConstant.printLog("e", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
                    + master.getRequestBean().getBaseplan() + " " + e);
        }
        return GSBMap;
    }

	private HashMap getGuranteedSurrenderValue_SIP() {
		HashMap GSBMap = new HashMap();
		try {
			long guranteedSurrenderValue;
			double guranteedSurrenderval=0;
			long maturityValue = 0;
			long totalMaturityValue = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (count ==polTerm-1) {
					String maturityValueStr = formulaHandler.evaluateFormula(
							master.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_VALUE,
							oFormulaBean)
							+ "";
					maturityValue = Long.parseLong(maturityValueStr);
					totalMaturityValue += maturityValue;
				}
				oFormulaBean.setA(totalMaturityValue);
				String guaSurrBenStr = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GURANTEED_SURRENDER_VALUE,
						oFormulaBean)
						+ "";
				guranteedSurrenderval=Double.parseDouble(guaSurrBenStr);
				if(count==polTerm)
					guranteedSurrenderValue=0;
				else
					guranteedSurrenderValue = Math.round(guranteedSurrenderval);
				GSBMap.put(count, guranteedSurrenderValue);
				CommonConstant.printLog("d", "Pol Term :::  Surr Benefit CALGSV_SIP " + count + " : "
						+ guranteedSurrenderValue);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
					+ master.getRequestBean().getBaseplan()+" "+e);
		}
		return GSBMap;
	}

	private HashMap getGuranteedSurrenderValue_ESIP() {
		HashMap GSBMap = new HashMap();
		try {
			long guranteedSurrenderValue;
			double guranteedSurrenderval=0;
			long totalGAIVal = 0;
			long maturityValue = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			for (int count = 1; count <= polTerm; count++) {
				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setAGAI(totalGAIVal);
				FormulaHandler formulaHandler = new FormulaHandler();
				String maturityValueStr = formulaHandler.evaluateFormula(
						master.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_MATURITY_VALUE,
						oFormulaBean)
						+ "";
				maturityValue = Long.parseLong(maturityValueStr);
				oFormulaBean.setA(maturityValue);
				String guaSurrBenStr = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_GURANTEED_SURRENDER_VALUE,
						oFormulaBean)
						+ "";
				guranteedSurrenderval=Double.parseDouble(guaSurrBenStr);
				if(count==polTerm)
                    guranteedSurrenderValue=0;
				else
					guranteedSurrenderValue = Math.round(guranteedSurrenderval);
				GSBMap.put(count, guranteedSurrenderValue);
				CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
						+ guranteedSurrenderValue);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating GuranteedSurrenderValue for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return GSBMap;
	}


	private HashMap getVSRBonus4_MIP() {
		HashMap RBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			// long totalGAIVal = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;
			if (master.getRequestBean().getBaseplan().equals("SGPV1N1") || master.getRequestBean().getBaseplan().equals("SGPV1N2")) {
				startPt = 5;
			}
			double onePlusRBD = 0;
			for (int count = 1; count <= polTerm; count++) {

				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);

				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getVSRBonus4");
				String strVsrbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";

				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				RBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus4 " + count + " : "
						+ vsrbonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getVSRBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return RBMap;
	}
	private long getTOTALVSRBonus4_MIP() {
		long TOTALRB4 = 0;
		double onePlusRBD = 0;
		try {
			long vsrbonus;
			double dvsrbonus;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getTOTALVSRBonus4");
				String strVsrbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_4,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				TOTALRB4+=vsrbonus;
				CommonConstant.printLog("d", "Pol Term :::  TOTALVSRBonus4 " + count + " : "
						+ vsrbonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTOTALVSRBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TOTALRB4;
	}

	private HashMap getTerminalBonus4_MIP() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			double dterminalBonus;

			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;


			double onePlusRBD = 0;
			for (int count = 1; count <= polTerm; count++) {

				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);

				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt)  {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TERMINAL_BONUS_4,
						oFormulaBean)
						+ "";
				//t

				dterminalBonus=Double.parseDouble(strTerminalbonus);
				terminalBonus=Math.round(dterminalBonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefit4_MIP() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			long totalGAIVal = 0;
			double onePlusRBD = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt= master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
				FormulaHandler formulaHandler = new FormulaHandler();

				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;

				if(count==polTerm )
					totalGAIVal=0;
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
				} else {
					onePlusRBD = 0;
				}
				oFormulaBean.setA(onePlusRBD);
				if (count>ppt)
					oFormulaBean.setCNT_MINUS(false);

				oFormulaBean.setCNT(count - 1);
				oFormulaBean.setAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_4,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  total death benifit 4 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTerminalBonus4 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}
	private HashMap getVSRBonus8_MIP() {
		HashMap RBMap = new HashMap();
		try {
			long vsrbonus;
			double dvsrbonus;
			double onePlusRBD=0;

			int startPt = 0;
			int polTerm = master.getRequestBean().getPolicyterm();

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);

				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getVSRBonus8");
				String strVsrbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";

				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				RBMap.put(count, vsrbonus);
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus8 " + count + " : "
						+ vsrbonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getVSRBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return RBMap;
	}
	private long getTOTALVSRBonus8_MIP() {
		long TOTALRB =0;
		try {
			long vsrbonus;
			double dvsrbonus;
			double onePlusRBD = 0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				CommonConstant.printLog("d", "getTOTALVSRBonus8");
				String strVsrbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_VRSBONUS_8,
						oFormulaBean)
						+ "";
				dvsrbonus = Double.parseDouble(strVsrbonus);
				vsrbonus=Math.round(dvsrbonus);
				TOTALRB+=vsrbonus;
				CommonConstant.printLog("d", "Pol Term :::  VSRBonus8 " + count + " : "
						+ TOTALRB);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getVSRBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return TOTALRB;
	}
	private HashMap getTerminalBonus8_MIP() {
		HashMap TBMap = new HashMap();
		double dterminalBonus;
		double onePlusRBD = 0;
		try {
			long terminalBonus;

			int polTerm = master.getRequestBean().getPolicyterm();
			int startPt = 0;

			for (int count = 1; count <= polTerm; count++) {

				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);

				FormulaHandler formulaHandler = new FormulaHandler();
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
				} else
					oFormulaBean.setA(0);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TERMINAL_BONUS_8,
						oFormulaBean)
						+ "";

				dterminalBonus=Double.parseDouble(strTerminalbonus);
				terminalBonus=Math.round(dterminalBonus);

				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 8 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTerminalBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotalDeathBenefit8_MIP() {
		HashMap TBMap = new HashMap();
		try {
			long terminalBonus;
			long totalGAIVal = 0;
			double onePlusRBD=0;
			int polTerm = master.getRequestBean().getPolicyterm();
			int ppt = master.getRequestBean().getPremiumpayingterm();
			int startPt = 1;

			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count-1);
				oFormulaBean.setCNT_MINUS(true);
				FormulaHandler formulaHandler = new FormulaHandler();

				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;

				if(count==polTerm)
					totalGAIVal=0;
				if (count>startPt) {
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
				} else {
					onePlusRBD = 0;
				}
				oFormulaBean.setA(onePlusRBD);
				if (count>ppt)
					oFormulaBean.setCNT_MINUS(false);

				oFormulaBean.setCNT(count-1);
				oFormulaBean.setAGAI(totalGAIVal);
				String strTerminalbonus = formulaHandler.evaluateFormula(master
								.getRequestBean().getBaseplan(),
						FormulaConstant.CALCULATE_TOT_DEATH_BENF_8,
						oFormulaBean)
						+ "";
				terminalBonus = Long.parseLong(strTerminalbonus);
				TBMap.put(count, terminalBonus);
				CommonConstant.printLog("d", "Pol Term :::  Terminal Bonus 8 " + count + " : "
						+ terminalBonus);
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTerminalBonus8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e + " " + e.getMessage());
		}
		return TBMap;
	}

	private HashMap getTotMaturityBenefit8_MIP() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;
		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();
				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;

				if(count==polTerm){
					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB8,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setAGAI(totalGAIVal);

					String surrBenStr = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_BENEFIT_8, oFormulaBean)
							+ "";
					surrenderBenefit = Long.parseLong(surrBenStr);
					SBMap.put(count, surrenderBenefit);
					CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
							+ surrenderBenefit);
				}
				else{
					SBMap.put(count, 0);
				}
			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	private HashMap getTotMaturityBenefit4_MIP() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		int startPt = 0;

		double onePlusRBD = 0;
		long totalGAIVal = 0;
		try{
			for (int count = 1; count <= polTerm; count++) {
				oFormulaBean = getFormulaBeanObj();
				oFormulaBean.setCNT(count);
				oFormulaBean.setCNT_MINUS(false);
				FormulaHandler formulaHandler = new FormulaHandler();

				long GAIValue = calculateGAI(count);
				totalGAIVal = totalGAIVal + GAIValue;
				if (count==polTerm)
					totalGAIVal = totalGAIVal - GAIValue;

				if(count==polTerm){

					String onePlusRB = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.ONEPLUSRB4,
							oFormulaBean)
							+ "";
					onePlusRBD = Double.parseDouble(onePlusRB);
					onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
					oFormulaBean.setA(onePlusRBD);
					oFormulaBean.setAGAI(totalGAIVal);

					String surrBenStr = formulaHandler.evaluateFormula(master
									.getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MATURITY_BENEFIT_4, oFormulaBean)
							+ "";
					surrenderBenefit = Long.parseLong(surrBenStr);
					SBMap.put(count, surrenderBenefit);

					CommonConstant.printLog("d", "Pol Term :::  Maturity Benefit (alternate scenario-Current rate)" + count + " : "
							+ surrenderBenefit);
				}
				else{
					SBMap.put(count, 0);
				}

			}
		} catch (Exception e) {

			CommonConstant.printLog("d", "Error occured in calculating getTotMaturityBenefit8 for planCode:  "
					+ master.getRequestBean().getBaseplan() + " " + e);
		}
		return SBMap;
	}
	private HashMap getSurrenderBenefitMIP_4() {
		HashMap SBMap = new HashMap();
		long surrenderBenefit;
		int polTerm = master.getRequestBean().getPolicyterm();
		//int ppt = master.getRequestBean().getPremiumpayingterm();
		int startPt = 0;
		double onePlusRBD = 0;
		double totalGAIVal = 0;
		double finalTotalGAIVal = 0;
		double finalTotalGAIValSSV = 0;
		oFormulaBean.setRBV(0);
		for (int count = 1; count <= polTerm; count++) {
			oFormulaBean = getFormulaBeanObj();
			oFormulaBean.setCNT(count);
			oFormulaBean.setCNT_MINUS(false);
			FormulaHandler formulaHandler = new FormulaHandler();
			double GAIValue = calculateGAI(count);

			oFormulaBean.setDoubleAGAI(totalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));

			oFormulaBean.setCNT(count-1);
			GAIValue = calculateGAI(count);

			totalGAIVal = totalGAIVal + GAIValue;
			finalTotalGAIVal=totalGAIVal;
			finalTotalGAIValSSV=finalTotalGAIVal;
			String onePlusRB = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.ONEPLUSRB4,
					oFormulaBean)
					+ "";
			onePlusRBD = Double.parseDouble(onePlusRB);
			onePlusRBD = Math.pow(onePlusRBD,(count-startPt));
			oFormulaBean.setA(onePlusRBD);
			oFormulaBean.setDoubleAGAI(finalTotalGAIVal);
			oFormulaBean.setAGAISSV(Math.round(finalTotalGAIValSSV));
			CommonConstant.printLog("d", "getSurrenderBenefit_4");
			String strVsrbonus = "0";
			strVsrbonus = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_VRSBONUS_4_D,
					oFormulaBean)
					+ "";
			oFormulaBean.setA(Double.parseDouble(strVsrbonus));




			oFormulaBean.setCNT(count);
			if ("Y"
					.equalsIgnoreCase(master.getRequestBean()
							.getTata_employee())
					&& count == 1) {
				oFormulaBean.setP(master.getDiscountedBasePrimium());
			}
			String surrBenStr = formulaHandler.evaluateFormula(master
							.getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SURRENDER_BENEFIT_4, oFormulaBean)
					+ "";
			surrenderBenefit = Long.parseLong(surrBenStr);
			oFormulaBean.setRBV(Double.parseDouble(strVsrbonus));
			if(count==polTerm)
				SBMap.put(count, 0);
			else
				SBMap.put(count, surrenderBenefit);

			CommonConstant.printLog("d", "Pol Term :::  Surr Benefit " + count + " : "
					+ surrenderBenefit);

		}

		return SBMap;
	}
    private HashMap getMilestoneAdditions(String parm){
        int pt = master.getRequestBean().getPolicyterm();
        HashMap milestoneDict = new HashMap();
        long milestoneValue = 0;
        for(int cnt = 1 ;cnt<=pt ; cnt++){
            if (cnt != pt -4) {
                milestoneDict.put(cnt, 0);
            }else {
                FormulaHandler formulaHandler = new FormulaHandler();
                String MBBStr = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
                        parm, oFormulaBean)
                        + "";
                milestoneValue = Long.parseLong(MBBStr);
                milestoneDict.put(cnt, milestoneValue);
            }
        }
        return milestoneDict;
    }

    private HashMap getMoneyBackBenfit(){
        int pt = master.getRequestBean().getPolicyterm();
        HashMap MBBDict = new HashMap();
        long MBBValue = 0;
        for(int cnt = 1 ;cnt<=pt ; cnt++){
            if ((pt - cnt) <= 3 && (pt - cnt) > 0) {
                FormulaHandler formulaHandler = new FormulaHandler();
                String MBBStr = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
                        FormulaConstant.CALCULATE_MONEYBACK_BENEFIT, oFormulaBean)
                        + "";
                MBBValue = Long.parseLong(MBBStr);
                MBBDict.put(cnt, MBBValue);
            }else {
                MBBDict.put(cnt, 0);
            }
        }
        return MBBDict;
    }
    private  HashMap getTotBen8Payable(){
        HashMap SBDict = new HashMap();
        long surrenderBenefit;
        int polterm = master.getRequestBean().getPolicyterm();
        HashMap mileStoneDict = master.getMilestoneAdd8Dict();
        HashMap moneyBackBenDict = master.getMoneyBackBenefitDict();
        HashMap vsrbonusDict = master.getVSRBonus8();
        HashMap termBonusDict = master.getTerminalBonus8();
        HashMap GSurrValDict = master.getMaturityVal();
        for (int cnt = 1; cnt <= polterm; cnt++) {
            surrenderBenefit = Long.parseLong(moneyBackBenDict.get(cnt).toString()) + Long.parseLong(mileStoneDict.get(cnt).toString());
            if (cnt == polterm) {
                long val = Long.parseLong(vsrbonusDict.get(cnt).toString()) + Long.parseLong(termBonusDict.get(cnt).toString()) + Long.parseLong(GSurrValDict.get(cnt).toString());
                surrenderBenefit += val;
            }
            SBDict.put(cnt,surrenderBenefit);
        }
        return SBDict;
    }
    private  HashMap getTotBen4Payable(){
        HashMap SBDict = new HashMap();
        long surrenderBenefit;
        int polterm = master.getRequestBean().getPolicyterm();
        HashMap mileStoneDict = master.getMilestoneAdd4Dict();
        HashMap moneyBackBenDict = master.getMoneyBackBenefitDict();
        HashMap vsrbonusDict = master.getVSRBonus4();
        HashMap termBonusDict = master.getTerminalBonus4();
        HashMap GSurrValDict = master.getMaturityVal();
        for (int cnt = 1; cnt <= polterm; cnt++) {
            surrenderBenefit = Long.parseLong(moneyBackBenDict.get(cnt).toString()) + Long.parseLong(mileStoneDict.get(cnt).toString());
            if (cnt == polterm) {
                long val = Long.parseLong(vsrbonusDict.get(cnt).toString()) + Long.parseLong(termBonusDict.get(cnt).toString()) + Long.parseLong(GSurrValDict.get(cnt).toString());
                surrenderBenefit += val;
            }
            SBDict.put(cnt,surrenderBenefit);
        }
        return SBDict;
    }
    private HashMap getNonGuarSurrBen8_GK() {
        int pt = master.getRequestBean().getPolicyterm();
        HashMap NonGuarSurrBenDict = new HashMap();
        HashMap gsvDict = master.getGuranteedSurrenderValue();
        HashMap vsrBonusDict = master.getVSRBonus8();
        HashMap totBenPayableDict = master.getTotalMaturityBenefit8();
        HashMap moneyBackBenDict = master.getMoneyBackBenefitDict();
        HashMap GSurrValDict = master.getMaturityVal();
        long totBenPayable = 0;
        long RBVal = 0;
        long totMilStones = 0;

        double surrBen = 0;
        long ssvVal = 0;
        long nonGuarSurrBen = 0;

        int startPt = 0;
        double onePlusRBD = 0;
        long sumOfMBBAndMat = 0;
        long benPayable = 0;

        for (int cnt = 1; cnt <= pt; cnt++) {
            FormulaHandler formulaHandler = new FormulaHandler();
            surrBen = Long.parseLong(gsvDict.get(cnt).toString());
            //double GSVFactor = DataAccessClass.getCSVFactor(oFormulaBean.getRequestBean(), cnt - 1);
            double GSVFactor = DataAccessClass.getBSVFactor(oFormulaBean.getRequestBean(),cnt);
            if (cnt < (pt - 4)) {
                long mileStone = 0;
                if (cnt <= 5) {
                    String milestonestr = formulaHandler.evaluateFormula(master
                                    .getRequestBean().getBaseplan(),
							FormulaConstant.CALCULATE_MILESTONE_ADD8, oFormulaBean)
                            + "";
                    mileStone = Long.parseLong(milestonestr);
                } else {
                    mileStone = 0;
                }
                totMilStones += mileStone;

                surrBen += GSVFactor * (RBVal + totMilStones);
            } else {
                totMilStones = 0;
                surrBen += GSVFactor * RBVal;
            }
            if(vsrBonusDict.get(cnt)!=null)
            RBVal = Long.parseLong(vsrBonusDict.get(cnt).toString());
            if(totBenPayableDict.get(cnt)!=null)
                benPayable = Long.parseLong(totBenPayableDict.get(cnt).toString());
            totBenPayable += benPayable;

            surrBen -= totBenPayable;
            oFormulaBean.setCNT(cnt);
            oFormulaBean.setCNT_MINUS(false);
            if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
                    || master.getRequestBean().getBaseplan().equals("SGPV1N2")
                    || master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
                    && cnt>startPt) ||  master.getRequestBean().getBaseplan().startsWith("IWV1N")
                    || (master.getRequestBean().getBaseplan().startsWith("FR7V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR7V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR10V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR10V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR15V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR15V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("GKIDV1P1"))){
                String onePlusRB = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
                        FormulaConstant.ONEPLUSRB8,
                        oFormulaBean)
                        + "";
                onePlusRBD = Double.parseDouble(onePlusRB);
                onePlusRBD = Math.pow(onePlusRBD,(cnt-startPt));
                oFormulaBean.setA(onePlusRBD);
            } else
                oFormulaBean.setA(0);
            String calSSVVal = formulaHandler.evaluateFormula(master
                            .getRequestBean().getBaseplan(),
                    FormulaConstant.CALCULATE_SSV8_GK, oFormulaBean)
                    + "";
            long mbbMatAddition = Long.parseLong(moneyBackBenDict.get(cnt).toString()) + Long.parseLong(GSurrValDict.get(cnt).toString());
            sumOfMBBAndMat += mbbMatAddition;

            double csvFactor = DataAccessClass.getCSVFactor(oFormulaBean.getRequestBean(),cnt);
            ssvVal = Math.round((Double.parseDouble(calSSVVal) +totMilStones - sumOfMBBAndMat) * csvFactor);
            nonGuarSurrBen = Math.max(Math.round(surrBen),ssvVal);
            if (cnt == pt) {
                nonGuarSurrBen = 0;
            }
            NonGuarSurrBenDict.put(cnt,nonGuarSurrBen);
        }
        return NonGuarSurrBenDict;
    }
    private HashMap getNonGuarSurrBen4_GK(){
        int pt = master.getRequestBean().getPolicyterm();
        HashMap NonGuarSurrBenDict = new HashMap();
        HashMap gsvDict = master.getGuranteedSurrenderValue();
        HashMap vsrBonusDict = master.getVSRBonus4();
        HashMap totBenPayableDict = master.getTotalMaturityBenefit4();
        HashMap moneyBackBenDict = master.getMoneyBackBenefitDict();
        HashMap GSurrValDict = master.getMaturityVal();
        long totBenPayable = 0;
        long RBVal = 0;
        long totMilStones = 0;

        double surrBen = 0;
        long ssvVal = 0;
        long nonGuarSurrBen = 0;

        int startPt = 0;
        double onePlusRBD = 0;
        long sumOfMBBAndMat = 0;

        for (int cnt = 1; cnt <= pt; cnt++) {
            FormulaHandler formulaHandler = new FormulaHandler();
            surrBen = Long.parseLong(gsvDict.get(cnt).toString());
            //double GSVFactor = DataAccessClass.getCSVFactor(oFormulaBean.getRequestBean(), cnt - 1);
			double GSVFactor = DataAccessClass.getBSVFactor(oFormulaBean.getRequestBean(),cnt);
            if (cnt < (pt - 4)) {
                long mileStone = 0;
                if (cnt <= 5) {
                    String milestonestr = formulaHandler.evaluateFormula(master
                                    .getRequestBean().getBaseplan(),
                            FormulaConstant.CALCULATE_MILESTONE_ADD4, oFormulaBean)
                            + "";
                    mileStone = Long.parseLong(milestonestr);
                } else {
                    mileStone = 0;
                }
                totMilStones += mileStone;

                surrBen += GSVFactor * (RBVal + totMilStones);
            } else {
                totMilStones = 0;
                surrBen += GSVFactor * RBVal;
            }
            RBVal = Long.parseLong(vsrBonusDict.get(cnt).toString());
            long benPayable = Long.parseLong(totBenPayableDict.get(cnt).toString());
            totBenPayable += benPayable;

            surrBen -= totBenPayable;
            oFormulaBean.setCNT(cnt);
            oFormulaBean.setCNT_MINUS(false);
            if (((master.getRequestBean().getBaseplan().equals("SGPV1N1")
                    || master.getRequestBean().getBaseplan().equals("SGPV1N2")
                    || master.getRequestBean().getBaseplan().startsWith("MBPV1N"))
                    && cnt>startPt) ||  master.getRequestBean().getBaseplan().startsWith("IWV1N")
                    || (master.getRequestBean().getBaseplan().startsWith("FR7V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR7V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR10V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR10V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR15V1P1"))
                    || (master.getRequestBean().getBaseplan().startsWith("FR15V1P2"))
                    || (master.getRequestBean().getBaseplan().startsWith("GKIDV1P1"))){
                String onePlusRB = formulaHandler.evaluateFormula(master
                                .getRequestBean().getBaseplan(),
                        FormulaConstant.ONEPLUSRB4,
                        oFormulaBean)
                        + "";
                onePlusRBD = Double.parseDouble(onePlusRB);
                onePlusRBD = Math.pow(onePlusRBD,(cnt-startPt));
                oFormulaBean.setA(onePlusRBD);
            } else

                oFormulaBean.setA(0);
            String calSSVVal = formulaHandler.evaluateFormula(master
                            .getRequestBean().getBaseplan(),
					FormulaConstant.CALCULATE_SSV4_GK, oFormulaBean)
                    + "";
            long mbbMatAddition = Long.parseLong(moneyBackBenDict.get(cnt).toString()) + Long.parseLong(GSurrValDict.get(cnt).toString());
            sumOfMBBAndMat += mbbMatAddition;

            double csvFactor = DataAccessClass.getCSVFactor(oFormulaBean.getRequestBean(),cnt);
            ssvVal = Math.round((Double.parseDouble(calSSVVal) +totMilStones - sumOfMBBAndMat) * csvFactor);
            nonGuarSurrBen = Math.max(Math.round(surrBen),ssvVal);
            if (cnt == pt) {
                nonGuarSurrBen = 0;
            }
            NonGuarSurrBenDict.put(cnt,nonGuarSurrBen);
        }
        return NonGuarSurrBenDict;
    }
}
