package com.talic.plugins.sisEngine.control;

import com.talic.plugins.sisEngine.BO.DataMapperBO;
import com.talic.plugins.sisEngine.BO.FundDetailsLK;
import com.talic.plugins.sisEngine.BO.RDLRiderLk;
import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.plugins.sisEngine.bean.SISRequestBean;
import com.talic.plugins.sisEngine.db.DataAccessClass;
import com.talic.plugins.sisEngine.util.CommonConstant;
import com.talic.plugins.sisEngine.util.FileCounter;
import com.talic.plugins.sisEngine.view.HtmlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class ActionController {

    private static final long serialVersionUID = 1L;

	public JSONObject handleSisCall(JSONArray request) throws JSONException,IOException, Exception {
		CommonConstant.printLog("d", " inside do post method...: ");

		 try {

			 ModelMaster master = new ModelMaster();
			 CommonConstant.printLog("d", "before JCSCaching");
			 System.out.println("2222  request" + request);

			 DataMapperBO oDataMapperBO=JCSCaching.getoDataMapperBO((String)request.getJSONObject(0).get("PnlId"), (String)request.getJSONObject(0).get("PglId"), (String)request.getJSONObject(0).get("BasePlan"));

			 SISRequestBean requestBean = getRequest(request,oDataMapperBO);

			 requestBean.setRiderlist(getRiders(request, oDataMapperBO));
			 requestBean.setFundList(getFund(request, oDataMapperBO));

			 master.setRequestBean(requestBean);
			 master.setServiceTaxRenewal(DataAccessClass.getServiceTaxRenewal());
			 CommonConstant.printLog("d", "before getBasePremium");
			 RequestProcessor reqestProcessor = new RequestProcessor(master);
			 JSONObject json = new JSONObject();
			 /*ValidationUtil vu = new ValidationUtil(master);
			 boolean isridPremvalid = vu.validateRiderPrem();
             if (isridPremvalid){
                 json.put("code", 2);
                 json.put("message", "Total Rider Premium should not be greater than 30% of base premium");
                 return json;
             }*/

				 master = reqestProcessor.getSISData();

				 /** Changes done for HSW - No html flag added for returning required data only **/
			 	 if(request.getJSONObject(0).has("NoHtmlFlag")){

					 CommonConstant.printLog("d", " Option values: " +request.getJSONObject(0).get("Option"));
					 // For Fortune Maxima - 200 and Wealth Pro - 201
					 if(request.getJSONObject(0).get("PglId").equals("200") || request.getJSONObject(0).get("PglId").equals("201")){

						// json.put("BasicSumAssured", master.getBasicSumAssured());
						 /*json.put("AnnualizedPremium", master.getAnnualizedPremium());*/

						 if(request.getJSONObject(0).get("Option").equals("4")) {
							 CommonConstant.printLog("d", " Option values CHECK 4 : ");
							 json.put("LoyaltyChargeDetails", master.getLoyaltyChargeforPH6().get(request.getJSONObject(0).get("PolicyTerm")));
							 json.put("TotalFundValueEndPolicyYear", master.getTotalFundValueAtEndPolicyYearPH6().get(request.getJSONObject(0).get("PolicyTerm")));
//							 json.put("MaturityBenefit", master.getTotalMaturityBenefit4());

						 }else if(request.getJSONObject(0).get("Option").equals("8")) {
							 CommonConstant.printLog("d", " Option values CHECK 8 : ");
							 json.put("LoyaltyChargeDetails", master.getLoyaltyChargeforPH10().get(request.getJSONObject(0).get("PolicyTerm")));
							 json.put("TotalFundValueEndPolicyYear", master.getTotalFundValueAtEndPolicyYearPH10().get(request.getJSONObject(0).get("PolicyTerm")));
//							 json.put("MaturityBenefit", master.getTotalMaturityBenefit8());

						 }
						 json.put("code", 1);
						 json.put("message", "SIS DATA GENERATED SUCCESSFULLY");
					 }
					 return json;
				 }

				 CommonConstant.printLog("d", "Html generation start:::  ");
				 HtmlBuilder htmlBuilder;
				 String fileData;
				 htmlBuilder = new HtmlBuilder(master);
				 fileData = htmlBuilder.buildHtml(null);
				 if (fileData != null && fileData.length() > 0) {
					 json.put("SIS", fileData);

					 JSONArray jsonArray = new JSONArray();
					 ArrayList<RiderBO> riderList = master.getRiderData();
					 for (int i = 0; i < riderList.size(); i++) {
						 JSONObject tempJSONObject = new JSONObject();
						 tempJSONObject.put("PolicyTerm", riderList.get(i).getPolTerm());
						 tempJSONObject.put("PremiumPayTerm", riderList.get(i).getPremPayTerm());
						 tempJSONObject.put("ModalPremium", riderList.get(i).getModalpremium());
						 tempJSONObject.put("RiderCode", riderList.get(i).getCode());
						 tempJSONObject.put("RiderDescription", riderList.get(i).getDescription());
						 tempJSONObject.put("RiderServiceTax", master.getSTRiderModalPremium());
						 jsonArray.put(i, tempJSONObject);
					 }
					 json.put("riderData", jsonArray);
					 /**** Changes made on 18th Sept to set ST = 0 for ULIP Product on Vivek's req. ****/
					 if(requestBean.getProduct_type().equalsIgnoreCase("U")) {
						 json.put("AnnualPremiumST", master.getBaseAnnualizedPremium());
						 json.put("ServiceTax", 0);
					 }
					 else{
						 json.put("AnnualPremiumST", master.getBaseAnnualizedPremium());
						 json.put("ServiceTax", master.getServiceTax());
					 }
					 /**** Changes end ****/
					 json.put("ModalPremium", master.getModalPremium() + master.getModalPremiumForNSAP());

					 /*Staff Discount Changes*/
					 if("Y".equalsIgnoreCase(master.getRequestBean().getTata_employee())){
						 json.put("AnnualPremiumST", master.getDiscountedBasePrimium());
						 json.put("DiscountedModalPremium", master.getDiscountedModalPrimium());
						 json.put("DiscountedServiceTax", Math.round(reqestProcessor.getServiceTax(master.getDiscountedModalPrimium())));
					 }
					 /*Sum At Risk for GIP*/
					 if(master.getRequestBean().getBaseplan().startsWith("GIP")){
						 HashMap lifeCoverageMap = master.getLifeCoverage();
						 json.put("Sar",lifeCoverageMap.get(1));
					 }else {
						 json.put("Sar",null);
					 }

					 json.put("code", 1);
					 json.put("message", "SIS REPORT GENERATED SUCCESSFULLY");
				 } else {
					 json.put("code", 0);
					 json.put("message", "SIS REPORT NOT GENERATED");
				 }
				 return json;
		}catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	private ArrayList getRiders(JSONArray req,DataMapperBO oDataMapper) {
		try {
			JSONObject request = req.getJSONObject(0);
			ArrayList riderLst = new ArrayList();
			RiderBO riderBO = null;
			HashMap rdlRiderLkHashMap = oDataMapper.getRDLRiderLk(); //COMMENTED BY AKHIL AS RIDER TABLES ARE NOT AVAILABLE*/ new HashMap();
			Iterator riderListIte = rdlRiderLkHashMap.keySet().iterator();
			while (riderListIte.hasNext()) {
				String keyHashMap = (String) riderListIte.next();
				if(request.has(keyHashMap)) {
					String riderSA = request.getString(keyHashMap);
					RDLRiderLk rdlRiderLk = (RDLRiderLk) rdlRiderLkHashMap.get(keyHashMap);
					if ((riderSA != null && !"0".equals(riderSA) && !"".equals(riderSA)) || ((rdlRiderLk.getRDL_CODE().equals("WOPULV1") || rdlRiderLk.getRDL_CODE().equals("WOPPULV1") || rdlRiderLk.getRDL_CODE().equals("WPPN1V1")) && "0".equals(riderSA))) {
						riderBO = new RiderBO();
						riderBO.setCode(keyHashMap);
						riderBO.setDescription(rdlRiderLk.getRDL_DESCRIPTION());
						riderBO.setSumAssured(Long.parseLong(riderSA));
						riderBO.setRtlID(rdlRiderLk.getRDL_RTL_ID());
						riderBO.setRdlID(rdlRiderLk.getRDL_ID());
						riderBO.setRdlUIN(rdlRiderLk.getRDL_UIN());        //added by jayesh for Rider UIN
						riderLst.add(riderBO);
						CommonConstant.printLog("d", "Rider selected: rider code: " + riderBO.getCode() + " rider description: " + riderBO.getDescription() + " rider SA: " + riderBO.getSumAssured());
					}
				}
			}
			return riderLst;
		}
		catch (Exception e){
			CommonConstant.printLog("d","Exception in getRiders(): " + e.getMessage());
			e.printStackTrace();

		}
		return null;
	}

	private ArrayList getFund(JSONArray req,DataMapperBO oDataMapper){
		try {
			JSONObject request = req.getJSONObject(0);
			ArrayList FundLst = new ArrayList();
			FundDetailsLK fundBO = null;
			HashMap fdlFundLkHashMap = oDataMapper.getFDLFundLk();
			Iterator fundListIte = fdlFundLkHashMap.keySet().iterator();
			while (fundListIte.hasNext()) {
				String keyHashMap = (String) fundListIte.next();
				String fundPercent = null;
				if (request.has(keyHashMap) && request.getString(keyHashMap) != null) {
					fundPercent = request.getString(keyHashMap);
				}
				FundDetailsLK rdlRiderLk = (FundDetailsLK) fdlFundLkHashMap.get(keyHashMap);

				fundBO = new FundDetailsLK();
				fundBO.setCODE(keyHashMap);
				fundBO.setDESCRIPTION(rdlRiderLk.getDESCRIPTION());
				fundBO.setFUND_MGMT_CHARGE(rdlRiderLk.getFUND_MGMT_CHARGE());
				if (fundPercent != null && !"0".equals(fundPercent) && !"".equals(fundPercent)) {
					fundBO.setPERCENTAGE(Integer.parseInt(fundPercent));
				} else {
					fundBO.setPERCENTAGE(0);
				}
				FundLst.add(fundBO);
				CommonConstant.printLog("d", "Rider selected: fund code: " + fundBO.getCODE() + " fund description: " + fundBO.getDESCRIPTION() + " Fund charge: " + fundBO.getFUND_MGMT_CHARGE());
			}
			return FundLst;
		}catch (Exception e){
			e.printStackTrace();
			CommonConstant.printLog("d", "Exception in getFund(): " + e.getMessage());
			return null;
		}
	}
	private double getFMCforFund(String FundName, DataMapperBO oDataMapper)
	{
		HashMap fdlFundLkHashMap = oDataMapper.getFDLFundLk();
		Iterator fundListIte =  fdlFundLkHashMap.keySet().iterator();
		while(fundListIte.hasNext()){
			String keyHashMap =  (String)fundListIte.next();
			FundDetailsLK fdlRiderLk = (FundDetailsLK)fdlFundLkHashMap.get(keyHashMap);
			if(fdlRiderLk.getCODE().equals(FundName)) {
				return fdlRiderLk.getFUND_MGMT_CHARGE();
			}
		}

		return 0.0;
	}


	private long getBasePremium(SISRequestBean sisRequest){
		//try{
			double premiumAmt = 0;
			double premMult = 0;
			if(sisRequest.getPremiummul()!=null && !sisRequest.getPremiummul().equalsIgnoreCase(""))
				premMult = Double.parseDouble(sisRequest.getPremiummul());
			double modFactor = DataAccessClass.getModelFactor(sisRequest);
			double sumAssured = sisRequest.getSumassured();
			if(sisRequest.getPremiummul() !=null && !sisRequest.getPremiummul().equalsIgnoreCase(""))
				premiumAmt = (sisRequest.getSumassured() * modFactor)/premMult;
			else{
				premMult = DataAccessClass.getPremiumMultiple(sisRequest);
				premiumAmt = Math.round(Math.round((sumAssured * premMult)/1000) * modFactor);
			}

			return (long)premiumAmt;
		/*}catch(Exception e){
			e.printStackTrace();
			return (long)0;
		}*/
		//return new Long("0");
	}

	private SISRequestBean getRequest(JSONArray req,DataMapperBO oDataMapper) {
		try {
			SISRequestBean requestBean = new SISRequestBean();
			JSONObject request = req.getJSONObject(0);
			CommonConstant.printLog("d", "Request:: " + request.toString());
			requestBean.setInsured_name((String) request.get("InsName"));
			requestBean.setInsured_age(Integer.parseInt(request.getString("InsAge")));
			requestBean.setInsured_sex(request.getString("InsSex"));
			requestBean.setInsured_smokerstatus(request.getString("InsSmoker"));
			requestBean.setInsured_occupation(request.getString("InsOcc"));

			requestBean.setProposer_name(request.getString("OwnName"));
			if (request.has("OwnAge") && request.getString("OwnAge") != null) {
				requestBean.setProposer_age(Integer.parseInt(request.getString("OwnAge")));
			}
			requestBean.setProposer_sex(request.getString("OwnSex"));
			//requestBean.setInsured_smokerstatus(request.getString("InsSmoker"));
			requestBean.setProposer_occupation(request.getString("OwnOcc"));

			//requestBean.setIrequest.getString("InsOccClass"));
			//request.getString("InsLoadType"));
			//request.getString("InsLoadValue"));
			requestBean.setFrequency(request.getString("Frequency"));
//added by Ganesh for MIP 29/01/2016
			if(request.getString("BasePlan").startsWith("MI7"))
				requestBean.setBaseplan("MI7MV1P1");
			else if(request.getString("BasePlan").startsWith("MI10"))
				requestBean.setBaseplan("MI10MV1P1");
			else
				requestBean.setBaseplan(request.getString("BasePlan"));
			requestBean.setSumassured(Long.parseLong(request.getString("Sumassured")));
			requestBean.setPolicyterm(Integer.parseInt(request.getString("PolicyTerm")));
			if (request.has("ProdTye") && request.getString("ProdTye") != null) {
				requestBean.setProduct_type(request.getString("ProdTye"));
			}
			requestBean.setPremiumpayingterm(Integer.parseInt(request.getString("PPTerm")));
			//request.getString("OwnerName"));
			requestBean.setOwnerAge(Integer.parseInt(request.getString("OwnAge")));
			if (request.has("InsOccDesc") && request.getString("InsOccDesc") != null) {
				requestBean.setInsured_occupation_desc(request.getString("InsOccDesc"));
			}
			if (request.has("OwnOccDesc") && request.getString("OwnOccDesc") != null) {
				requestBean.setProposer_occupation_desc(request.getString("OwnOccDesc"));
			}
			//request.getString("OwnerSex"));
			//request.getString("OwnerOcc"));
			//request.getString("OwnerOccClass"));
			//request.getString("OwnerLoadType"));
			//request.getString("OwnerLoadValue"));
			//PropNo , PropDt, insDOB, tataDis
			requestBean.setProposal_no(request.getString("PropNo"));
			//requestBean.setProposal_date(request.getString("PropDt")); // Commented by akhil on 02-07-2015 19:27:00 as it was not available in iOS project (js file)
			requestBean.setTata_employee(request.getString("TataEmployee")); // akhil changed the key on 02-07-2015 19:30:00 from tataDis to TataEmployee as the later was used in iOS project (js file)
			requestBean.setInsured_dob(request.getString("InsDOB")); // akhil changed the key on 02-07-2015 19:31:00 from insDOB to InsDOB as the later was used in iOS project (js file)
			requestBean.setProposer_dob(request.getString("PropDOB"));
			if (request.has("PremiumMul") && request.getString("PremiumMul") != null) {
				requestBean.setPremiummul(request.getString("PremiumMul"));
			}
			//if (request.has("BasePremium") && request.getString("BasePremium")!=null) {
			//requestBean.setBasepremium(Long.parseLong(request.getString("BasePremium")));
			requestBean.setBasepremium(getBasePremium(requestBean));
			CommonConstant.printLog("d", "base premium  in : requestBean " + requestBean.getBasepremium());
			//}
			if (request.has("BasePremiumAnnual") && request.getString("BasePremiumAnnual") != null) {
				requestBean.setBasepremiumannual(Long.parseLong(request.getString("BasePremiumAnnual")));
				CommonConstant.printLog("d", "base premium Annual in : requestBean " + requestBean.getBasepremiumannual());
			}

			//requestBean.setOperationType(request.getString("opType")); // Commented by akhil on 02-07-2015 19:33:00 as it was not available in iOS project (js file)

			if (request.has("SAgeProofFlg") && request.getString("SAgeProofFlg") != null) {
				requestBean.setAgeProofFlag(request.getString("SAgeProofFlg"));
				if ("N".equalsIgnoreCase(request.getString("SAgeProofFlg"))) {
					requestBean.setAgeProof("Non-Standard");
				} else {
					requestBean.setAgeProof("Standard");
				}
			} else
				requestBean.setAgeProofFlag("");
			//Start : added by jayesh for SMART7
			if (request.has("commision") && request.getString("commision") != null) {
				requestBean.setCommission(request.getString("commision")); // Commented by akhil on 02-07-2015 19:46:00 as it was not available in iOS project (js file)
			}

			if (request.has("RpuYear") && request.getString("RpuYear") != null) {
				requestBean.setRpu_year(request.getString("RpuYear"));    //added by jayesh for SMART 7
			}
			if (request.has("TaxSlab") && request.getString("TaxSlab") != null) {
				requestBean.setTaxslab(request.getString("TaxSlab"));    //added by jayesh for SMART 7
			}
			if (request.has("TaxSlab") && request.getString("TaxSlab") != null && !requestBean.getTaxslab().equals("0")) {
				requestBean.setTaxslabSelected("YES");////Samina Changes 26112014
			} else {
				requestBean.setTaxslabSelected("NO");////Samina Changes 26112014
			}
			if (request.has("RpuYear") && request.getString("RpuYear") != null && !requestBean.getRpu_year().equals("0")) {
				requestBean.setRpuSelected("YES");////Samina Changes 26112014
			} else {
				requestBean.setRpuSelected("NO");////Samina Changes 26112014
			}
			if (request.has("agentCITI") && request.getString("agentCITI") != null) {
				requestBean.setAgentCITI(request.getString("agentCITI"));
			} else
				requestBean.setAgentCITI("N");
			if (request.has("FixWithAmt") && request.getString("FixWithAmt") != null)
				requestBean.setFixWithDAmt(request.getString("FixWithAmt"));
			if (request.has("FixWithStYr") && request.getString("FixWithStYr") != null)
				requestBean.setFixWithDStYr(request.getString("FixWithStYr"));
			if (request.has("FixWithEndYr") && request.getString("FixWithEndYr") != null)
				requestBean.setFixWithDEndYr(request.getString("FixWithEndYr"));
			if (request.has("FixTopupAmt") && request.getString("FixTopupAmt") != null)
				requestBean.setFixTopupAmt(request.getString("FixTopupAmt"));
			if (request.has("FixTopupStYr") && request.getString("FixTopupStYr") != null)
				requestBean.setFixTopupStYr(request.getString("FixTopupStYr"));
			if (request.has("FixTopupEndYr") && request.getString("FixTopupEndYr") != null)
				requestBean.setFixTopupEndYr(request.getString("FixTopupEndYr"));
			if (request.has("VariWithYrAmt") && request.getString("VariWithYrAmt") != null)
				requestBean.setVarWithDAmtYr(request.getString("VariWithYrAmt"));
			if (request.has("VariTopupYrAmt") && request.getString("VariTopupYrAmt") != null)
				requestBean.setVarTopupAmtYr(request.getString("VariTopupYrAmt"));
			if (request.has("FundPerform") && request.getString("FundPerform") != null)
				requestBean.setFundPerform(request.getString("FundPerform"));
			if (request.has("SmartDebtFund") && request.getString("SmartDebtFund") != null) {
				requestBean.setSmartDebtFund(request.getString("SmartDebtFund"));
				requestBean.setSmartDebtFundFMC(getFMCforFund(request.getString("SmartDebtFund"), oDataMapper));
			}
			if (request.has("SmartEquFund") && request.getString("SmartEquFund") != null) {
				requestBean.setSmartEquFund(request.getString("SmartEquFund"));
				requestBean.setSmartEquityFundFMC(getFMCforFund(request.getString("SmartEquFund"), oDataMapper));
			}
			if (request.has("AAA") && request.getString("AAA") != null) {
				requestBean.setAAA(request.getString("AAA"));
			}
			if (request.has("ExpBonusRate") && request.getString("ExpBonusRate") != null) {
				requestBean.setExpbonusrate(request.getString("ExpBonusRate"));
			}
			if (request.has("ExpBonusRate") && request.getString("ExpBonusRate") != null && !requestBean.getExpbonusrate().equals("0")) {
				requestBean.setExpbonusSelected("YES");//Samina Changes 26112014
			} else {
				requestBean.setExpbonusSelected("NO");//Samina Changes 26112014
			}

			if (request.has("AgentName") && request.getString("AgentName") != null) {
				requestBean.setAgentName(request.getString("AgentName"));
			}


			if (request.has("AgentNumber") && request.getString("AgentNumber") != null) {
				requestBean.setAgentNumber(request.getString("AgentNumber"));
			}


			if (request.has("AgentContactNumber") && request.getString("AgentContactNumber") != null) {
				requestBean.setAgentContactNumber(request.getString("AgentContactNumber"));
			}

			if(request.getString("BasePlan").startsWith("GIP5I10") || request.getString("BasePlan").startsWith("GIP12I10")){
				requestBean.setIncomeBooster(Integer.parseInt("10"));
			}else if(request.getString("BasePlan").startsWith("GIP5I15") || request.getString("BasePlan").startsWith("GIP12I15")){
				requestBean.setIncomeBooster(Integer.parseInt("15"));
			}

			if (request.has("businessType") && request.getString("businessType") != null) {
				requestBean.setBusinessType(request.getString("businessType"));
			}
			CommonConstant.printLog("d", "Request param : ");
			CommonConstant.printLog("d", "Insured Name : " + requestBean.getInsured_name());
			CommonConstant.printLog("d", "AGE : " + requestBean.getInsured_age());
			CommonConstant.printLog("d", "Sex : " + requestBean.getInsured_sex());
			CommonConstant.printLog("d", "SmokerStatus : " + requestBean.getInsured_smokerstatus());
			CommonConstant.printLog("d", "Occupation : " + requestBean.getInsured_occupation());
			CommonConstant.printLog("d", "Base Plan : " + requestBean.getBaseplan());
			CommonConstant.printLog("d", "Payment Mode::: " + requestBean.getFrequency());
			CommonConstant.printLog("d", "SA : " + requestBean.getSumassured());
			//CommonConstant.printLog("d","PT : " + requestBean.getPolicyterm());
			//CommonConstant.printLog("d","PPT : " + requestBean.getPremiumpayingterm());
			//CommonConstant.printLog("d","Premium multiple : " + request.getString("PremiumMultiple"));
			CommonConstant.printLog("d", "Proposal no : " + request.getString("PropNo"));
			//CommonConstant.printLog("d","Proposal no : " + request.getString("PropDt"));
			CommonConstant.printLog("d", "Tata Discount : " + request.getString("TataEmployee"));
			CommonConstant.printLog("d", "Insured DOB : " + request.getString("InsDOB"));
			//CommonConstant.printLog("d","operation Type : " + request.getString("opType"));
			//CommonConstant.printLog("d","Age Proof Flag : " + request.getString("SAgeProofFlg"));
			//CommonConstant.printLog("d","Commision : " + request.getString("commision"));
			//CommonConstant.printLog("d","RPU Year : " +request.getString("RpuYear"));	//added by jayesh for SMART 7
			//CommonConstant.printLog("d","TaxSlab : " +request.getString("TaxSlab"));	//added by jayesh for SMART 7
			//CommonConstant.printLog("d","ExpBonusRate : " +request.getString("ExpBonusRate"));	//added by jayesh for SMART 7
			//CommonConstant.printLog("d","AAA : "+request.getString("AAA")+" and "+requestBean.getAAA());
			//CommonConstant.printLog("d","SMART : "+request.getString("SmartEquFund")+" and "+requestBean.getSmartEquFund());
			//CommonConstant.printLog("d","FP : "+request.getString("FundPerform")+" and "+requestBean.getFundPerform());
			//CommonConstant.printLog("d","Var Withdrawal "+request.getString("VariWithYrAmt")+" and "+requestBean.getVarWithDAmtYr());
			return requestBean;
		}catch (Exception e){
			CommonConstant.printLog("d", "Exception in getRequest(): " + e.getMessage());
			e.printStackTrace();

		}
		return null;

	}
	public String getUniqueJSPFileName(String productName){
		String fileName = "";
		Date date=new Date();
		 long currentTimeMillS=date.getTime();

		 FileCounter ofileCounter=new FileCounter();
		 //ofileCounter.countIncrementer();

		 fileName=Long.toString(currentTimeMillS);
		 fileName = productName + fileName+"_"+ofileCounter.countIncrementer()+ ".pdf";

		 return fileName;
	}
}
