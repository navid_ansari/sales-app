package com.talic.plugins.sisEngine.control;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import com.talic.plugins.sisEngine.BO.DataMapperBO;
import com.talic.plugins.sisEngine.db.FetchDataDAO;
import com.talic.salesapp.DBHelper;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Main class to test JCS Cache.
 *
 * @author Bhavesh Thaker
 */
@SuppressWarnings("unchecked")
public class JCSCaching {

	/**
	 * Main method.
	 */
	private static final String TAG = "SA";
	public static DataMapperBO oDataMapperBO = null;

	//private static String pnlId;

	public static DataMapperBO getoDataMapperBO(String pnlId, String pglId, String planCode){
		if(oDataMapperBO==null){
			oDataMapperBO = loadCacheForDemo(pnlId, pglId, planCode);
		}
		else{
            SQLiteDatabase db = DBHelper.getDatabaseConnection();
			if(pnlId!=null){
				/*MIP Check*/
				/*MIP Check*/
				if(pglId.equals("215") && planCode.startsWith("MI7")) {
					pnlId = "696";
					planCode = "MI7MV1P1";
				}
				else if(pglId.equals("215") && planCode.startsWith("MI10")) {
					pnlId = "697";
					planCode = "MI10MV1P1";
				}

				final ArrayList PMLPremiumMultipleLk = FetchDataDAO.getPMLPremiumMultipleLk(db, pnlId);
				oDataMapperBO.setPMLPremiumMultipleLk(PMLPremiumMultipleLk);

				final ArrayList RDLCharges = FetchDataDAO.getRDLCharges(db, pnlId);
				oDataMapperBO.setRDLCharges(RDLCharges);

                /*final HashMap PNLPlanLk = FetchDataDAO.getPNLPlanLk(db, pnlId);
                oDataMapperBO.setPNLPlanLk(PNLPlanLk);
                Log.d(TAG, "Done table: PNLPlanLK");*/

                final HashMap PCRPlanChargesRef = FetchDataDAO.getPCRPlanChargesRef(db, pnlId);
                oDataMapperBO.setPCRPlanChargesRef(PCRPlanChargesRef);
                Log.d(TAG, "Done table: LP_PCR_Plan_Charges_Ref");

                final ArrayList PMVPlanMaturityValue = FetchDataDAO.getPMVPlanMaturityValue(db, pnlId);
                oDataMapperBO.setPMVPlanMaturityValue(PMVPlanMaturityValue);
                Log.d(TAG, "Done table: LP_PMV_PLAN_MATURITY_VALUE");

                final ArrayList MFMMaturityFactorMaster =FetchDataDAO.getMFMMaturityFactorMaster(db, pnlId);
                oDataMapperBO.setMFMMaturityFactorMaster(MFMMaturityFactorMaster);
                Log.d(TAG, "Done table: LP_MFM_MATURITY_FACTOR_MASTER");

                final ArrayList GAIGuarAnnIncChMaster =FetchDataDAO.getGAIGuarAnnIncChMaster(db, pnlId);
                oDataMapperBO.setGAIGuarAnnIncChMaster(GAIGuarAnnIncChMaster);
                Log.d(TAG, "Done table: LP_GAI_GUAR_ANN_INC_CH_MASTER");

                final ArrayList IPCValues = FetchDataDAO.getIPCValues(db,pnlId);
                oDataMapperBO.setIPCValues(IPCValues);
                Log.d(TAG, "Done table: LP_IPC_INF_PROT_COV_MASTER");

                final ArrayList fopList = FetchDataDAO.getFOPRate(db, pnlId);
                oDataMapperBO.setFOPRatelist(fopList);
                Log.d(TAG, "Done table: LP_FOP_CHARGES_MASTER");
            }
            if(pglId!=null){
                final ArrayList mapPlanDetails = FetchDataDAO.getModalFactorDetails(db, pglId);
                oDataMapperBO.setMDLModalFactorLk(mapPlanDetails);
                Log.d(TAG, "Done table: MDLModalFactorLk");

                final HashMap PGLPlanGroupLk = FetchDataDAO.getPGLPlanGroupLk(db, pglId);
                oDataMapperBO.setPGLPlanGroupLk(PGLPlanGroupLk);
                Log.d(TAG, "Done table: PGLPlanGroupLK");

                final HashMap PGFPlanGroupFeature = FetchDataDAO.getPGFPlanGroupFeature(db,pglId);
                oDataMapperBO.setPGFPlanGroupFeature(PGFPlanGroupFeature);
                Log.d(TAG, "Done table: LP_PGF_PLAN_GROUP_FEATURE");
            }
            if(planCode!=null){
                /*final ArrayList RMLRiderPremMultipleLK = FetchDataDAO.getRMLRiderPremMultipleLK(db);
                oDataMapperBO.setRMLRiderPremMultipleLK(RMLRiderPremMultipleLK);
                Log.d(TAG, "Done table: LP_RML_RIDER_PREM_MULTIPLE_LK");*/

                final ArrayList PNLMinPremLK = FetchDataDAO.getPNLMinPremLK(db,planCode);
                oDataMapperBO.setPNLMinPremLK(PNLMinPremLK);
                Log.d(TAG, "Done table: LP_PNL_MIN_PREM_LK");

                final ArrayList PmdPlanPremiumMultDisc=FetchDataDAO.getPmdPlanPremiumMultDisc(db,planCode);
                oDataMapperBO.setPmdPlanPremiumMultDisc(PmdPlanPremiumMultDisc);
                Log.d(TAG, "Done table: LP_PMD_PLAN_PREMIUM_MULT_DISC");

                /*final ArrayList PFRPlanFormulaRef = FetchDataDAO.getPFRPlanFormulaRef(db, planCode);
                oDataMapperBO.setPFRPlanFormulaRef(PFRPlanFormulaRef);
                Log.d(TAG, "Done table: LP_PER_PLAN_FORMULA_REF");*/

                /*final HashMap SPVSysParamValues = FetchDataDAO.getSPVSysParamValues(db,planCode);
                oDataMapperBO.setSPVSysParamValues(SPVSysParamValues);
                Log.d(TAG, "Done table: LP_SPV_SYS_PARAM_VALUES");*/
            }
		}
		return oDataMapperBO;
	}

	public static DataMapperBO loadCacheForDemo(String pnlId, String pglId, String planCode) {
		DataMapperBO oDataMapperBO = new DataMapperBO();

		SQLiteDatabase db = DBHelper.getDatabaseConnection();

		/*MIP Check*/
		if(pglId.equals("215") && planCode.startsWith("MI7")) {
			pnlId = "696";
			planCode = "MI7MV1P1";
		}
		else if(pglId.equals("215") && planCode.startsWith("MI10")) {
			pnlId = "697";
			planCode = "MI10MV1P1";
		}

		final ArrayList mapPlanDetails = FetchDataDAO.getModalFactorDetails(db, pglId);
		oDataMapperBO.setMDLModalFactorLk(mapPlanDetails);
		Log.d(TAG, "Done table: MDLModalFactorLk");

		final HashMap OCLOccupationLk = FetchDataDAO.getOCLOccupationLk(db);
		oDataMapperBO.setOCLOccupationLk(OCLOccupationLk);
		Log.d(TAG, "Done table: OCLOccupationLk");

		final HashMap PGLPlanGroupLk = FetchDataDAO.getPGLPlanGroupLk(db, pglId);
		oDataMapperBO.setPGLPlanGroupLk(PGLPlanGroupLk);
		Log.d(TAG, "Done table: PGLPlanGroupLK");

		final ArrayList PMLPremiumMultipleLk = FetchDataDAO.getPMLPremiumMultipleLk(db, pnlId);
		oDataMapperBO.setPMLPremiumMultipleLk(PMLPremiumMultipleLk);
		Log.d(TAG, "Done table: PMLPremiumMultipleLK");

		final HashMap PNLPlanLk = FetchDataDAO.getPNLPlanLk(db);
		oDataMapperBO.setPNLPlanLk(PNLPlanLk);
		Log.d(TAG, "Done table: PNLPlanLK");

		final HashMap FundDetails = FetchDataDAO.getFundDeatils(db);
		oDataMapperBO.setFDLFundLk(FundDetails);
		Log.d(TAG, "Done table: lp_pfx_plan_fund_xref");

		final ArrayList PACPremiumAllocationCharges = FetchDataDAO.getPACPremiumAllocationCharges(db);
		oDataMapperBO.setPACPremiumAllocationCharges(PACPremiumAllocationCharges);
        Log.d(TAG, "Done table: LP_PAC_Premium_Allocation_Charges");

		final ArrayList ACMAdminChargesMaster = FetchDataDAO.getACMAdminChargesMaster(db);
		oDataMapperBO.setACMAdminChargesMaster(ACMAdminChargesMaster);
        Log.d(TAG, "Done table: LP_ACM_Admin_Charges_Master");

		final ArrayList MCMMortilityChargesMaster = FetchDataDAO.getMCMMortilityChargesMaster(db);
		oDataMapperBO.setMCMMortilityChargesMaster(MCMMortilityChargesMaster);
        Log.d(TAG, "Done table: LP_MCM_Mortility_Charges_Master");

		final HashMap PCRPlanChargesRef = FetchDataDAO.getPCRPlanChargesRef(db,pnlId);
		oDataMapperBO.setPCRPlanChargesRef(PCRPlanChargesRef);
        Log.d(TAG, "Done table: LP_PCR_Plan_Charges_Ref");

		final ArrayList WCMWOPChargesMaster = FetchDataDAO.getWCMWOPChargesMaster(db);
		oDataMapperBO.setWCMWOPChargesMaster(WCMWOPChargesMaster);
        Log.d(TAG, "Done table: LP_WCM_WOP_Charges_Master");

		final ArrayList FCMFIBChargesMaster = FetchDataDAO.getFCMFIBChargesMaster(db);
		oDataMapperBO.setFCMFIBChargesMaster(FCMFIBChargesMaster);
        Log.d(TAG, "Done table: LP_FCM_FIB_Charges_Master");

		final ArrayList SURSurrenderChargeMaster=FetchDataDAO.getSURSurrenderChargeMaster(db);
		oDataMapperBO.setSURSurrenderChargeMaster(SURSurrenderChargeMaster);
        Log.d(TAG, "Done table: LP_SUR_SURRENDER_CHARGE_MASTER");

		final ArrayList CommissionChargeMaster =FetchDataDAO.getCCMCommissionChargeMaster(db);
		oDataMapperBO.setCommissionChargeMaster(CommissionChargeMaster);
        Log.d(TAG, "Done table: lp_ccm_commission_charge_master");


		/*
		//commented by Akhil on 03-07-2015 12:26:00 as the table not available in sqlite db

		final ArrayList PGLGrpMinPremLK = FetchDataDAO.getPGLGrpMinPremLK(db);
		oDataMapperBO.setPGLGrpMinPremLK(PGLGrpMinPremLK);

		final ArrayList AAAFMCValues = FetchDataDAO.getAAAFMCValues(db);
		oDataMapperBO.setAAAFMCValues(AAAFMCValues);

		*/

		final ArrayList commTextList = FetchDataDAO.getCommText(db);
		oDataMapperBO.setCommTextlist(commTextList);
        Log.d(TAG, "Done table: LP_COMM_TEXT");

		final ArrayList RMLRiderPremMultipleLK = FetchDataDAO.getRMLRiderPremMultipleLK(db);
		oDataMapperBO.setRMLRiderPremMultipleLK(RMLRiderPremMultipleLK);
        Log.d(TAG, "Done table: LP_RML_RIDER_PREM_MULTIPLE_LK");

		final HashMap RDLRiderLk = FetchDataDAO.getRDLRiderLk(db);
		oDataMapperBO.setRDLRiderLk(RDLRiderLk);
        Log.d(TAG, "Done table: LP_RDL_RIDER_LK");

		final ArrayList RDLCharges =FetchDataDAO.getRDLCharges(db, pnlId);
		oDataMapperBO.setRDLCharges(RDLCharges);
        Log.d(TAG, "Done table: lp_rdl_charges");

		final ArrayList stdTemplateValues = FetchDataDAO.getStdTemplateValues(db);
		oDataMapperBO.setStdTemplateValues(stdTemplateValues);
        Log.d(TAG, "Done table: lp_std_sis_template_data");


		final ArrayList iplIllussPagelist = FetchDataDAO.getIplIllussPagelist(db);
		oDataMapperBO.setIplIllussPagelist(iplIllussPagelist);
        Log.d(TAG, "Done table: LP_IPL_ILLUSS_PAGELIST");

		final HashMap tempImgPath = FetchDataDAO.getImagePath(db);
		oDataMapperBO.setTempImgPath(tempImgPath);
        Log.d(TAG, "Done table: lp_template_image_path");


		final ArrayList PNLMinPremLK = FetchDataDAO.getPNLMinPremLK(db,planCode);
		oDataMapperBO.setPNLMinPremLK(PNLMinPremLK);
        Log.d(TAG, "Done table: LP_PNL_MIN_PREM_LK");


		final ArrayList PmdPlanPremiumMultDisc=FetchDataDAO.getPmdPlanPremiumMultDisc(db,planCode);
		oDataMapperBO.setPmdPlanPremiumMultDisc(PmdPlanPremiumMultDisc);
        Log.d(TAG, "Done table: LP_PMD_PLAN_PREMIUM_MULT_DISC");

		final ArrayList PBAProductBand=FetchDataDAO.getPBAProductBand(db);
		oDataMapperBO.setPBAProductBand(PBAProductBand);
        Log.d(TAG, "Done table: LP_PBA_PRODUCT_BAND");

		final ArrayList PMVPlanMaturityValue = FetchDataDAO.getPMVPlanMaturityValue(db,pnlId);
		oDataMapperBO.setPMVPlanMaturityValue(PMVPlanMaturityValue);
        Log.d(TAG, "Done table: LP_PMV_PLAN_MATURITY_VALUE");

		final ArrayList PFRPlanFormulaRef = FetchDataDAO.getPFRPlanFormulaRef(db);
		oDataMapperBO.setPFRPlanFormulaRef(PFRPlanFormulaRef);
        Log.d(TAG, "Done table: LP_PER_PLAN_FORMULA_REF");

		final ArrayList FEDFormulaExtDesc = FetchDataDAO.getFEDFormulaExtDesc(db);
		oDataMapperBO.setFEDFormulaExtDesc(FEDFormulaExtDesc);
        Log.d(TAG, "Done table: LP_FED_FORMULA_EXT_DESC");

		final ArrayList Address = FetchDataDAO.getAddress(db);
		oDataMapperBO.setAddress(Address);
        Log.d(TAG, "Done table: LP_COMPANY_MASTER");

		final HashMap PGFPlanGroupFeature = FetchDataDAO.getPGFPlanGroupFeature(db,pglId);
		oDataMapperBO.setPGFPlanGroupFeature(PGFPlanGroupFeature);
        Log.d(TAG, "Done table: LP_PGF_PLAN_GROUP_FEATURE");

		final ArrayList GuaranteedMaturityBonusMaster=FetchDataDAO.getGuaranteedMaturityBonusMaster(db);
		oDataMapperBO.setGuaranteedMaturityBonusMaster(GuaranteedMaturityBonusMaster);
        Log.d(TAG, "Done table: LP_GAI_GUAR_ANN_INC_CH_MASTER");

		final ArrayList XmlMetadataMaster = FetchDataDAO.getXmlMetadataMaster(db);
		oDataMapperBO.setXmlMetadataMaster(XmlMetadataMaster);
        Log.d(TAG, "Done table: lp_xmm_xml_metadata_master");

		final ArrayList MFMMaturityFactorMaster =FetchDataDAO.getMFMMaturityFactorMaster(db,pnlId);
		oDataMapperBO.setMFMMaturityFactorMaster(MFMMaturityFactorMaster);
        Log.d(TAG, "Done table: LP_MFM_MATURITY_FACTOR_MASTER");

		final ArrayList GAIGuarAnnIncChMaster =FetchDataDAO.getGAIGuarAnnIncChMaster(db,pnlId);
		oDataMapperBO.setGAIGuarAnnIncChMaster(GAIGuarAnnIncChMaster);
        Log.d(TAG, "Done table: LP_GAI_GUAR_ANN_INC_CH_MASTER");

		final HashMap SPVSysParamValues = FetchDataDAO.getSPVSysParamValues(db);
		oDataMapperBO.setSPVSysParamValues(SPVSysParamValues);
        Log.d(TAG, "Done table: LP_SPV_SYS_PARAM_VALUES");

		final ArrayList IPCValues = FetchDataDAO.getIPCValues(db,pnlId);
		oDataMapperBO.setIPCValues(IPCValues);
        Log.d(TAG, "Done table: LP_IPC_INF_PROT_COV_MASTER");

		/*final AgentDetails AgentDetails = FetchDataDAO.getAgentDetails(db);
		oDataMapperBO.setAgent_inermedDetails(AgentDetails);*/

		final ArrayList ServiceTaxMaster = FetchDataDAO.getServiceTaxMaster(db);
		oDataMapperBO.setServiceTaxMaster(ServiceTaxMaster);
        Log.d(TAG, "Done table: LP_STM_SERVICETAX_MASTER");

		final ArrayList premiumBoost = FetchDataDAO.getLpbLargePremiumBoost(db);
		oDataMapperBO.setPremiumBoost(premiumBoost);
        Log.d(TAG, "Done table: LP_LPB_PREMIUM_BOOST");

		final ArrayList fopList = FetchDataDAO.getFOPRate(db, pnlId);
		oDataMapperBO.setFOPRatelist(fopList);
        Log.d(TAG, "Done table: LP_FOP_CHARGES_MASTER");

		Log.d(TAG, "Done with all tables");

		return oDataMapperBO;
	}
}
