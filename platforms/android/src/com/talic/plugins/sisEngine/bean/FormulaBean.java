package com.talic.plugins.sisEngine.bean;

	import java.io.Serializable;

	/**
	 * @author OSNPO271
	 *
	 */
	@SuppressWarnings("serial")
	public class FormulaBean implements Serializable {

		private SISRequestBean requestBean;
		private long P;//Premium
		private long AP;//Annualized Premium
		private long DP;//Discounted Premium
		private long QAP;//Quoted Annualized Premium
		private int CNT; // Counter variable
		private double ADM;//Admin charges
		private double A;//Amount for service tax
		private double NPHA;    //  Net P/holder Account
		private double NPHAMTLB; //NetPHAmmountforLoyaltyBonus
		private boolean CNT_MINUS=false;
		private long QMP; //Quoted Modal Premium added by jayesh for SMART 7 21-05-2014
		private long STAXR; //Service Tax on Riders added by jayesh for SMART 7 23-06-2014
		private long RiderPremium;	//Rider Premium added by jayesh for SMART 7 23-06-2014
		private double withdrawalAmount;
		private long RC;
		private int monthCount; // Mounthly Counter variable
		private long AGAI; // Mounthly Counter variable
		private double DoubleAGAI; // Mounthly Counter variable
		private double PD;
		private long AGAISSV; // Mounthly Counter variable for SSV
		private double STMain;
		private double STFY;
		private double STRenewal;
		private long ULIPAP;
		private double RBV;
		private long MRSPRP;//Maharaksha Renewal Premium added by Ganesh 04-03-2016
		private long ANSAP;//Annualized Premium

		public boolean isCNT_MINUS() {
			return CNT_MINUS;
		}
		public void setCNT_MINUS(boolean cnt_minus) {
			CNT_MINUS = cnt_minus;
		}

		public SISRequestBean getRequestBean() {
			return requestBean;
		}
		public void setRequestBean(SISRequestBean requestBean) {
			this.requestBean = requestBean;
		}
		public long getP() {
			return P;
		}
		public void setP(long p) {
			P = p;
		}
		public long getDP() {
			return DP;
		}
		public void setDP(long dp) {
			DP = dp;
		}

		public long getQAP() {
			return QAP;
		}
		public void setQAP(long qap) {
			QAP = qap;
		}
		public int getCNT() {
			return CNT;
		}
		public void setCNT(int cnt) {
			CNT = cnt;
		}
		public double getADM() {
			return ADM;
		}
		public void setADM(double adm) {
			ADM = adm;
		}
		public double getA() {
			return A;
		}
		public void setA(double a) {
			A = a;
		}
		public double getNPHA() {
			return NPHA;
		}
		public void setNPHA(double npha) {
			NPHA = npha;
		}
		public double getNPHAMTLB() {
			return NPHAMTLB;
		}
		public void setNPHAMTLB(double nphamtlb) {
			NPHAMTLB = nphamtlb;
		}
		public long getQMP() {
			return QMP;
		}
		public void setQMP(long qmp) {
			QMP = qmp;
		}
		public long getSTAXR() {
			return STAXR;
		}
		public void setSTAXR(long staxr) {
			STAXR = staxr;
		}
		public long getRiderPremium() {
			return RiderPremium;
		}
		public void setRiderPremium(long riderPremium) {
			RiderPremium = riderPremium;
		}

		public double getWithdrawalAmount() {
			return withdrawalAmount;
		}
		public void setWithdrawalAmount(double withdrawalAmount) {
			this.withdrawalAmount = withdrawalAmount;
		}
		public long getRC() {
			return RC;
		}
		public void setRC(long rc) {
			RC = rc;
		}
		public int getMonthCount() {
			return monthCount;
		}
		public void setMonthCount(int monthCount) {
			this.monthCount = monthCount;
		}
		public long getAP() {
			return AP;
		}
		public void setAP(long ap) {
			AP = ap;
		}
		public long getAGAI() {
			return AGAI;
		}
		public void setAGAI(long agai) {
			AGAI = agai;
		}
		public double getPD() {
			return PD;
		}
		public void setPD(double pd) {
			PD = pd;
		}
		public long getAGAISSV() {
			return AGAISSV;
		}
		public void setAGAISSV(long agaissv) {
			AGAISSV = agaissv;
		}
		public double getSTMain() {
			return STMain;
		}
		public void setSTMain(double main) {
			STMain = main;
		}
		public double getSTFY() {
			return STFY;
		}
		public void setSTFY(double stfy) {
			STFY = stfy;
		}
		public double getSTRenewal() {
			return STRenewal;
		}
		public void setSTRenewal(double renewal) {
			STRenewal = renewal;
		}
		public long getULIPAP() {
			return ULIPAP;
		}
		public void setULIPAP(long ulipap) {
			ULIPAP = ulipap;
		}
		public double getDoubleAGAI() {
			return DoubleAGAI;
		}
		public void setDoubleAGAI(double doubleAGAI) {
			DoubleAGAI = doubleAGAI;
		}
		public double getRBV() {
			return RBV;
		}
		public void setRBV(double rbv) {
			RBV = rbv;
		}
        public long getMRSPRP() {
            return MRSPRP;
        }
        public void setMRSPRP(long mrsprp) {MRSPRP = mrsprp;}
		public long getANSAP() {
			return ANSAP;
		}
		public void setANSAP(long ansap) {
			ANSAP = ansap;
		}
}
