package com.talic.plugins.sisEngine.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings({ "unchecked", "serial" })
	public class SISRequestBean implements Serializable {
	private String insured_name;
    private int insured_age;
    private String insured_dob;
    private String insured_sex;
    private String insured_occupation;
    private String insured_occupation_desc;
    private String insured_smokerstatus;
    private String proposer_name;
    private Integer proposer_age;
    private String proposer_dob;
    private String proposer_occupation;
    private String proposer_occupation_desc;
    private String proposer_sex;
    private Integer planGroupID;
    private String baseplan;
    private long sumassured;
    private Integer units;
    private long basepremium;
    private long basepremiumannual;
    private String frequency;
    private Integer policyterm;
    private Integer premiumpayingterm;
    private String premmultiple;
    private String policyoption;
    private String product_type;
    private String proposal_date;
    private String proposal_no;
    private String start_point;
    private String tata_employee;
    private String commission;
    private String agentcode;
    private String agentname;
    private ArrayList riderlist;
    private ArrayList fundList;
    private String operationType;
    private String ageProofFlag;
    private String premiummul;
    private String rpu_year;	//added by jayesh for SMART 7
    private String taxslab;	//added by jayesh for SMART 7
    private String fixWithDAmt;
    private String fixWithDStYr;
    private String fixWithDEndYr;
    private String fixTopupAmt;
    private String fixTopupStYr;
    private String fixTopupEndYr;
    private String varWithDAmtYr;
    private String varTopupAmtYr;
    private String fundPerform;
    private String smartDebtFund;
    private String smartEquFund;
    private double smartDebtFundFMC;
    private double smartEquityFundFMC;
    private String AAA;
	private String topUpWDSelected;
	private String AAASelected;
	private String smartSelected;
	private String fpSelected;
    private String expbonusrate;	//added by jayesh for SMART 7
    private String ageProof;
    private Integer ownerAge;
    private String illusPageNo;
    private String negativeError;
    private String rpuSelected;
    private String taxslabSelected;
    private String expbonusSelected;
    private String citiproduct;
    private String AgentCITI;

	private String agentName;

	private String agentNumber;

	private String agentContactNumber;


	private long annuity_amt;
	private int incomeBooster;
	private  String businessType;



	//Combo

	public int TLC;
	public int WLE;
	public int WLA;
	public int WLS;
	public int WLI;
	public int WLF;

    public String getPremiummul() {
		return premiummul;
	}
	public void setPremiummul(String premiummul) {
		this.premiummul = premiummul;
	}
	public String getInsured_name(){
		return insured_name;
	}
	public void setInsured_name(String insured_name) {
		this.insured_name = insured_name;
	}
	public int getInsured_age() {
		return insured_age;
	}
	public void setInsured_age(int insured_age) {
		this.insured_age = insured_age;
	}
	public String getInsured_sex() {
		return insured_sex;
	}
	public void setInsured_sex(String insured_sex) {
		this.insured_sex = insured_sex;
	}
	public String getInsured_occupation() {
		return insured_occupation;
	}
	public void setInsured_occupation(String insured_occupation) {
		this.insured_occupation = insured_occupation;
	}
	public String getInsured_smokerstatus() {
		return insured_smokerstatus;
	}
	public void setInsured_smokerstatus(String insured_smokerstatus) {
		this.insured_smokerstatus = insured_smokerstatus;
	}
	public String getProposer_name() {
		return proposer_name;
	}
	public void setProposer_name(String proposer_name) {
		this.proposer_name = proposer_name;
	}
	public Integer getProposer_age() {
		return proposer_age;
	}
	public void setProposer_age(Integer proposer_age) {
		this.proposer_age = proposer_age;
	}
	public String getProposer_occupation() {
		return proposer_occupation;
	}
	public void setProposer_occupation(String proposer_occupation) {
		this.proposer_occupation = proposer_occupation;
	}
	public String getProposer_sex() {
		return proposer_sex;
	}
	public void setProposer_sex(String proposer_sex) {
		this.proposer_sex = proposer_sex;
	}

	public Integer getPlanGroupID() {
		return planGroupID;
	}
	public void setPlanGroupID(Integer planGroupID) {
		this.planGroupID = planGroupID;
	}
	public String getBaseplan() {
		return baseplan;
	}
	public void setBaseplan(String baseplan) {
		this.baseplan = baseplan;
	}
	public long getSumassured() {
		return sumassured;
	}
	public void setSumassured(long sumassured) {
		this.sumassured = sumassured;
	}
	public Integer getUnits() {
		return units;
	}
	public void setUnits(Integer units) {
		this.units = units;
	}
	public long getBasepremium() {
		return basepremium;
	}
	public void setBasepremium(long basepremium) {
		this.basepremium = basepremium;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public Integer getPolicyterm() {
		return policyterm;
	}
	public void setPolicyterm(Integer policyterm) {
		this.policyterm = policyterm;
	}
	public Integer getPremiumpayingterm() {
		return premiumpayingterm;
	}
	public void setPremiumpayingterm(Integer premiumpayingterm) {
		this.premiumpayingterm = premiumpayingterm;
	}
	public String getPremmultiple() {
		return premmultiple;
	}
	public void setPremmultiple(String premmultiple) {
		this.premmultiple = premmultiple;
	}
	public String getPolicyoption() {
		return policyoption;
	}
	public void setPolicyoption(String policyoption) {
		this.policyoption = policyoption;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public String getProposal_date() {
		return proposal_date;
	}
	public void setProposal_date(String proposal_date) {
		this.proposal_date = proposal_date;
	}
	public String getProposal_no() {
		return proposal_no;
	}
	public void setProposal_no(String proposal_no) {
		this.proposal_no = proposal_no;
	}
	public String getStart_point() {
		return start_point;
	}
	public void setStart_point(String start_point) {
		this.start_point = start_point;
	}
	public String getTata_employee() {
		return tata_employee;
	}
	public void setTata_employee(String tata_employee) {
		this.tata_employee = tata_employee;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getAgentcode() {
		return agentcode;
	}
	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}
	public String getAgentname() {
		return agentname;
	}
	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}
	public ArrayList getRiderlist() {
		return riderlist;
	}
	public void setRiderlist(ArrayList riderlist) {
		this.riderlist = riderlist;
	}
	public String getInsured_dob() {
		return insured_dob;
	}
	public void setInsured_dob(String insured_dob) {
		this.insured_dob = insured_dob;
	}
	public String getProposer_dob() {
		return proposer_dob;
	}
	public void setProposer_dob(String proposer_dob) {
		this.proposer_dob = proposer_dob;
	}
	public ArrayList getFundList() {
		return fundList;
	}
	public void setFundList(ArrayList fundList) {
		this.fundList = fundList;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getAgeProofFlag() {
		return ageProofFlag;
	}
	public void setAgeProofFlag(String ageProofFlag) {
		this.ageProofFlag = ageProofFlag;
	}
	public String getRpu_year() {
		return rpu_year;
	}
	public void setRpu_year(String rpu_year) {
		this.rpu_year = rpu_year;
	}
	public String getTaxslab() {
		return taxslab;
	}
	public void setTaxslab(String taxslab) {
		this.taxslab = taxslab;
	}
	public String getFixWithDAmt() {
		return fixWithDAmt;
	}
	public void setFixWithDAmt(String fixWithDAmt) {
		this.fixWithDAmt = fixWithDAmt;
	}
	public String getFixWithDStYr() {
		return fixWithDStYr;
	}
	public void setFixWithDStYr(String fixWithDStYr) {
		this.fixWithDStYr = fixWithDStYr;
	}
	public String getFixWithDEndYr() {
		return fixWithDEndYr;
	}
	public void setFixWithDEndYr(String fixWithDEndYr) {
		this.fixWithDEndYr = fixWithDEndYr;
	}
	public String getFixTopupAmt() {
		return fixTopupAmt;
	}
	public void setFixTopupAmt(String fixTopupAmt) {
		this.fixTopupAmt = fixTopupAmt;
	}
	public String getFixTopupStYr() {
		return fixTopupStYr;
	}
	public void setFixTopupStYr(String fixTopupStYr) {
		this.fixTopupStYr = fixTopupStYr;
	}
	public String getFixTopupEndYr() {
		return fixTopupEndYr;
	}
	public void setFixTopupEndYr(String fixTopupEndYr) {
		this.fixTopupEndYr = fixTopupEndYr;
	}
	public String getVarWithDAmtYr() {
		return varWithDAmtYr;
	}
	public void setVarWithDAmtYr(String varWithDAmtYr) {
		this.varWithDAmtYr = varWithDAmtYr;
	}
	public String getVarTopupAmtYr() {
		return varTopupAmtYr;
	}
	public void setVarTopupAmtYr(String varTopupAmtYr) {
		this.varTopupAmtYr = varTopupAmtYr;
	}
	public String getFundPerform() {
		return fundPerform;
	}
	public void setFundPerform(String fundPerform) {
		this.fundPerform = fundPerform;
	}
	public String getSmartDebtFund() {
		return smartDebtFund;
	}
	public void setSmartDebtFund(String smartDebtFund) {
		this.smartDebtFund = smartDebtFund;
	}
	public String getSmartEquFund() {
		return smartEquFund;
	}
	public void setSmartEquFund(String smartEquFund) {
		this.smartEquFund = smartEquFund;
	}
	public double getSmartDebtFundFMC() {
		return smartDebtFundFMC;
	}
	public void setSmartDebtFundFMC(double smartDebtFundFMC) {
		this.smartDebtFundFMC = smartDebtFundFMC;
	}
	public double getSmartEquityFundFMC() {
		return smartEquityFundFMC;
	}
	public void setSmartEquityFundFMC(double smartEquityFundFMC) {
		this.smartEquityFundFMC = smartEquityFundFMC;
	}
	public String getAAA() {
		return AAA;
	}
	public void setAAA(String aaa) {
		AAA = aaa;
	}
	public String getTopUpWDSelected() {
		return topUpWDSelected;
	}
	public void setTopUpWDSelected(String topUpWDSelected) {
		this.topUpWDSelected = topUpWDSelected;
	}
	public String getAAASelected() {
		return AAASelected;
	}
	public void setAAASelected(String selected) {
		AAASelected = selected;
	}
	public String getSmartSelected() {
		return smartSelected;
	}
	public void setSmartSelected(String smartSelected) {
		this.smartSelected = smartSelected;
	}
	public String getFpSelected() {
		return fpSelected;
	}
	public void setFpSelected(String fpSelected) {
		this.fpSelected = fpSelected;
	}
	public String getExpbonusrate() {
		return expbonusrate;
	}
	public void setExpbonusrate(String expbonusrate) {
		this.expbonusrate = expbonusrate;
	}
	public String getAgeProof() {
		return ageProof;
	}
	public void setAgeProof(String ageProof) {
		this.ageProof = ageProof;
	}
	public Integer getOwnerAge() {
		return ownerAge;
	}
	public void setOwnerAge(Integer ownerAge) {
		this.ownerAge = ownerAge;
	}
	public String getInsured_occupation_desc() {
		return insured_occupation_desc;
	}
	public void setInsured_occupation_desc(String insured_occupation_desc) {
		this.insured_occupation_desc = insured_occupation_desc;
	}
	public String getProposer_occupation_desc() {
		return proposer_occupation_desc;
	}
	public void setProposer_occupation_desc(String proposer_occupation_desc) {
		this.proposer_occupation_desc = proposer_occupation_desc;
	}
	public String getIllusPageNo() {
		return illusPageNo;
	}
	public void setIllusPageNo(String illusPageNo) {
		this.illusPageNo = illusPageNo;
	}
	public long getBasepremiumannual() {
		return basepremiumannual;
	}
	public void setBasepremiumannual(long basepremiumannual) {
		this.basepremiumannual = basepremiumannual;
	}
	public String getNegativeError() {
		return negativeError;
	}
	public void setNegativeError(String negativeError) {
		this.negativeError = negativeError;
	}
	public String getRpuSelected() {
		return rpuSelected;
	}
	public void setRpuSelected(String rpuSelected) {
		this.rpuSelected = rpuSelected;
	}
	public String getTaxslabSelected() {
		return taxslabSelected;
	}
	public void setTaxslabSelected(String taxslabSelected) {
		this.taxslabSelected = taxslabSelected;
	}
	public String getExpbonusSelected() {
		return expbonusSelected;
	}
	public void setExpbonusSelected(String expbonusSelected) {
		this.expbonusSelected = expbonusSelected;
	}
	public String getCitiproduct() {
		return citiproduct;
	}
	public void setCitiproduct(String citiproduct) {
		this.citiproduct = citiproduct;
	}
	public String getAgentCITI() {
		return AgentCITI;
	}
	public void setAgentCITI(String agentCITI) {
		this.AgentCITI = agentCITI;
	}
	public long getAnnuity_amt() {
		return annuity_amt;
	}
	public void setAnnuity_amt(long annuity_amt) {
		this.annuity_amt = annuity_amt;
	}

	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentNumber() {
		return agentNumber;
	}
	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getAgentContactNumber() {
		return agentContactNumber;
	}
	public void setAgentContactNumber(String agentContactNumber) {
		this.agentContactNumber = agentContactNumber;
	}
	public int getIncomeBooster() {
		return incomeBooster;
	}
	public void setIncomeBooster(int incomeBooster) {
		this.incomeBooster = incomeBooster;
	}
	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
 }
