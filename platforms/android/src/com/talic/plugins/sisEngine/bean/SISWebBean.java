package com.talic.plugins.sisEngine.bean;

	import java.io.Serializable;

	@SuppressWarnings("serial")
	public class SISWebBean implements Serializable {
		private String firstName;
		private String lastName;
		private String dateOfBirth;
		private String gender;
		private String purchaseFor;
		private String buyingFor;
		private String payorRelation;
		private String title;
		private String occupation;
		private String productName;
		private String planId;
		private String premiumPaymentMode;
		private String premium;
		private String policyTerm;
		private String premiumTerm;
		private String sumAssured;
		private String fundStrategy;
		private String wholeLifeMidCapEquityFund;
		private String wholeLifeIncomeFund;
		private String wholeLifeShortTermFixedIncomeFund;
		private String wholeLifeStableGrowthFund;
		private String wholeLifeAggressiveGrowthFund;
		private String largeCapEquityFund;
		private String superSelectEquityFund;
		private String total;
		private String portfolioStrategy;
		private String accumulationFund;
		private String allocationPercentageAccumulationFund;
		private String targetFund;
		private String allocationPercentageTargetFund;
		private String largeCapEquityFundAAA;
		private String wholeLifeIncomeAAA;
		private String ADBRider;
		private String ADDRiderLongScale;
		private String CIBRiderLumpsumBenefit;
		private String RiderPayorBenefit;
		private String strGenderId;
		private int PremiumPaymentMode = 0;
		public String getRiderPayorBenefit() {
			return RiderPayorBenefit;
		}
		public void setRiderPayorBenefit(String riderPayorBenefit) {
			RiderPayorBenefit = riderPayorBenefit;
		}
		public String getRiderFamilyIncomeBenefit() {
			return RiderFamilyIncomeBenefit;
		}
		public void setRiderFamilyIncomeBenefit(String riderFamilyIncomeBenefit) {
			RiderFamilyIncomeBenefit = riderFamilyIncomeBenefit;
		}
		public String getRiderSurgicalBenefit() {
			return RiderSurgicalBenefit;
		}
		public void setRiderSurgicalBenefit(String riderSurgicalBenefit) {
			RiderSurgicalBenefit = riderSurgicalBenefit;
		}
		public String getRiderADBLUBenefit() {
			return RiderADBLUBenefit;
		}
		public void setRiderADBLUBenefit(String riderADBLUBenefit) {
			RiderADBLUBenefit = riderADBLUBenefit;
		}
		public String getRiderADDSSBenefit() {
			return RiderADDSSBenefit;
		}
		public void setRiderADDSSBenefit(String riderADDSSBenefit) {
			RiderADDSSBenefit = riderADDSSBenefit;
		}
		public String getRiderCIABenefit() {
			return RiderCIABenefit;
		}
		public void setRiderCIABenefit(String riderCIBenefit) {
			RiderCIABenefit = riderCIBenefit;
		}
		public String getRiderWaiverofPremiunBenefit() {
			return RiderWaiverofPremiunBenefit;
		}
		public void setRiderWaiverofPremiunBenefit(String riderWaiverofPremiunBenefit) {
			RiderWaiverofPremiunBenefit = riderWaiverofPremiunBenefit;
		}
		public String getRider5YearRenewableBenefit() {
			return Rider5YearRenewableBenefit;
		}
		public void setRider5YearRenewableBenefit(String rider5YearRenewableBenefit) {
			Rider5YearRenewableBenefit = rider5YearRenewableBenefit;
		}
		public String getRider10YearTermBenefit() {
			return Rider10YearTermBenefit;
		}
		public void setRider10YearTermBenefit(String rider10YearTermBenefit) {
			Rider10YearTermBenefit = rider10YearTermBenefit;
		}
		public String getRider15YearTermBenefit() {
			return Rider15YearTermBenefit;
		}
		public void setRider15YearTermBenefit(String rider15YearTermBenefit) {
			Rider15YearTermBenefit = rider15YearTermBenefit;
		}
		public String getRider20YearTermBenefit() {
			return Rider20YearTermBenefit;
		}
		public void setRider20YearTermBenefit(String rider20YearTermBenefit) {
			Rider20YearTermBenefit = rider20YearTermBenefit;
		}
		public String getRider25YearTermBenefit() {
			return Rider25YearTermBenefit;
		}
		public void setRider25YearTermBenefit(String rider25YearTermBenefit) {
			Rider25YearTermBenefit = rider25YearTermBenefit;
		}
		public String getRiderTermtoAge60Benefit() {
			return RiderTermtoAge60Benefit;
		}
		public void setRiderTermtoAge60Benefit(String riderTermtoAge60Benefit) {
			RiderTermtoAge60Benefit = riderTermtoAge60Benefit;
		}
		private String RiderFamilyIncomeBenefit;
		private String RiderSurgicalBenefit;
		private String RiderADBLUBenefit;
		private String RiderADDSSBenefit;
		private String RiderCIABenefit;
		private String RiderWaiverofPremiunBenefit;
		private String Rider5YearRenewableBenefit;
		private String Rider10YearTermBenefit;
		private String Rider15YearTermBenefit;
		private String Rider20YearTermBenefit;
		private String Rider25YearTermBenefit;
		private String RiderTermtoAge60Benefit;

		private String age;

		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(String dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getPurchaseFor() {
			return purchaseFor;
		}
		public void setPurchaseFor(String purchaseFor) {
			this.purchaseFor = purchaseFor;
		}
		public String getBuyingFor() {
			return buyingFor;
		}
		public void setBuyingFor(String buyingFor) {
			this.buyingFor = buyingFor;
		}
		public String getPayorRelation() {
			return payorRelation;
		}
		public void setPayorRelation(String payorRelation) {
			this.payorRelation = payorRelation;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getOccupation() {
			return occupation;
		}
		public void setOccupation(String occupation) {
			this.occupation = occupation;
		}
		public String getProductName() {
			return productName;
		}
		public void setProductName(String productName) {
			this.productName = productName;
		}
		public String getPlanId() {
			return planId;
		}
		public void setPlanId(String planId) {
			this.planId = planId;
		}
		public String getPremiumPaymentMode() {
			return premiumPaymentMode;
		}
		public void setPremiumPaymentMode(String premiumPaymentMode) {
			this.premiumPaymentMode = premiumPaymentMode;
		}
		public String getPremium() {
			return premium;
		}
		public void setPremium(String premium) {
			this.premium = premium;
		}
		public String getPolicyTerm() {
			return policyTerm;
		}
		public void setPolicyTerm(String policyTerm) {
			this.policyTerm = policyTerm;
		}
		public String getPremiumTerm() {
			return premiumTerm;
		}
		public void setPremiumTerm(String premiumTerm) {
			this.premiumTerm = premiumTerm;
		}
		public String getSumAssured() {
			return sumAssured;
		}
		public void setSumAssured(String sumAssured) {
			this.sumAssured = sumAssured;
		}
		public String getFundStrategy() {
			return fundStrategy;
		}
		public void setFundStrategy(String fundStrategy) {
			this.fundStrategy = fundStrategy;
		}
		public String getWholeLifeMidCapEquityFund() {
			return wholeLifeMidCapEquityFund;
		}
		public void setWholeLifeMidCapEquityFund(String wholeLifeMidCapEquityFund) {
			this.wholeLifeMidCapEquityFund = wholeLifeMidCapEquityFund;
		}
		public String getWholeLifeIncomeFund() {
			return wholeLifeIncomeFund;
		}
		public void setWholeLifeIncomeFund(String wholeLifeIncomeFund) {
			this.wholeLifeIncomeFund = wholeLifeIncomeFund;
		}
		public String getWholeLifeShortTermFixedIncomeFund() {
			return wholeLifeShortTermFixedIncomeFund;
		}
		public void setWholeLifeShortTermFixedIncomeFund(
				String wholeLifeShortTermFixedIncomeFund) {
			this.wholeLifeShortTermFixedIncomeFund = wholeLifeShortTermFixedIncomeFund;
		}
		public String getWholeLifeStableGrowthFund() {
			return wholeLifeStableGrowthFund;
		}
		public void setWholeLifeStableGrowthFund(String wholeLifeStableGrowthFund) {
			this.wholeLifeStableGrowthFund = wholeLifeStableGrowthFund;
		}
		public String getWholeLifeAggressiveGrowthFund() {
			return wholeLifeAggressiveGrowthFund;
		}
		public void setWholeLifeAggressiveGrowthFund(
				String wholeLifeAggressiveGrowthFund) {
			this.wholeLifeAggressiveGrowthFund = wholeLifeAggressiveGrowthFund;
		}
		public String getLargeCapEquityFund() {
			return largeCapEquityFund;
		}
		public void setLargeCapEquityFund(String largeCapEquityFund) {
			this.largeCapEquityFund = largeCapEquityFund;
		}
		public String getSuperSelectEquityFund() {
			return superSelectEquityFund;
		}
		public void setSuperSelectEquityFund(String superSelectEquityFund) {
			this.superSelectEquityFund = superSelectEquityFund;
		}
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		public String getPortfolioStrategy() {
			return portfolioStrategy;
		}
		public void setPortfolioStrategy(String portfolioStrategy) {
			this.portfolioStrategy = portfolioStrategy;
		}
		public String getAccumulationFund() {
			return accumulationFund;
		}
		public void setAccumulationFund(String accumulationFund) {
			this.accumulationFund = accumulationFund;
		}
		public String getAllocationPercentageAccumulationFund() {
			return allocationPercentageAccumulationFund;
		}
		public void setAllocationPercentageAccumulationFund(
				String allocationPercentageAccumulationFund) {
			this.allocationPercentageAccumulationFund = allocationPercentageAccumulationFund;
		}
		public String getTargetFund() {
			return targetFund;
		}
		public void setTargetFund(String targetFund) {
			this.targetFund = targetFund;
		}
		public String getAllocationPercentageTargetFund() {
			return allocationPercentageTargetFund;
		}
		public void setAllocationPercentageTargetFund(
				String allocationPercentageTargetFund) {
			this.allocationPercentageTargetFund = allocationPercentageTargetFund;
		}
		public String getLargeCapEquityFundAAA() {
			return largeCapEquityFundAAA;
		}
		public void setLargeCapEquityFundAAA(String largeCapEquityFundAAA) {
			this.largeCapEquityFundAAA = largeCapEquityFundAAA;
		}
		public String getWholeLifeIncomeAAA() {
			return wholeLifeIncomeAAA;
		}
		public void setWholeLifeIncomeAAA(String wholeLifeIncomeAAA) {
			this.wholeLifeIncomeAAA = wholeLifeIncomeAAA;
		}
		public String getADBRider() {
			return ADBRider;
		}
		public void setADBRider(String aDBRider) {
			ADBRider = aDBRider;
		}
		public String getADDRiderLongScale() {
			return ADDRiderLongScale;
		}
		public void setADDRiderLongScale(String aDDRiderLongScale) {
			ADDRiderLongScale = aDDRiderLongScale;
		}
		public String getCIBRiderLumpsumBenefit() {
			return CIBRiderLumpsumBenefit;
		}
		public void setCIBRiderLumpsumBenefit(String cIBRiderLumpsumBenefit) {
			CIBRiderLumpsumBenefit = cIBRiderLumpsumBenefit;
		}

		private String modalPremiumTraditional;
		private String totalPremiumTraditional;
		private String totalServiceTaxTradional;
		private String totalPremiumWithServiceTax;

		public String getModalPremiumTraditional() {
			return modalPremiumTraditional;
		}
		public void setModalPremiumTraditional(String modalPremiumTraditional) {
			this.modalPremiumTraditional = modalPremiumTraditional;
		}
		public String getTotalPremiumTraditional() {
			return totalPremiumTraditional;
		}
		public void setTotalPremiumTraditional(String totalPremiumTraditional) {
			this.totalPremiumTraditional = totalPremiumTraditional;
		}
		public String getTotalServiceTaxTradional() {
			return totalServiceTaxTradional;
		}
		public void setTotalServiceTaxTradional(String totalServiceTaxTradional) {
			this.totalServiceTaxTradional = totalServiceTaxTradional;
		}
		public String getTotalPremiumWithServiceTax() {
			return totalPremiumWithServiceTax;
		}
		public void setTotalPremiumWithServiceTax(String totalPremiumWithServiceTax) {
			this.totalPremiumWithServiceTax = totalPremiumWithServiceTax;
		}

		public String OwnerFirstName;
		public String OwnerLastName;
		public String OwnerGender;
		public String OwnerAge;

		public String getOwnerFirstName() {
			return OwnerFirstName;
		}
		public void setOwnerFirstName(String ownerFirstName) {
			OwnerFirstName = ownerFirstName;
		}
		public String getOwnerLastName() {
			return OwnerLastName;
		}
		public void setOwnerLastName(String ownerLastName) {
			OwnerLastName = ownerLastName;
		}
		public String getOwnerGender() {
			return OwnerGender;
		}
		public void setOwnerGender(String ownerGender) {
			OwnerGender = ownerGender;
		}
		public String getOwnerAge() {
			return OwnerAge;
		}
		public void setOwnerAge(String ownerAge) {
			OwnerAge = ownerAge;
		}


		private String modalPremiumMahaLifeSupreme;
		private String totalPremiumMahaLifeSupreme;
		private String totalServiceTaxMahaLifeSupreme;
		private String TotalPremiumWithServiceTaxMahaLifeSupreme;


		public String getModalPremiumMahaLifeSupreme() {
			return modalPremiumMahaLifeSupreme;
		}
		public void setModalPremiumMahaLifeSupreme(String modalPremiumMahaLifeSupreme) {
			this.modalPremiumMahaLifeSupreme = modalPremiumMahaLifeSupreme;
		}
		public String getTotalPremiumMahaLifeSupreme() {
			return totalPremiumMahaLifeSupreme;
		}
		public void setTotalPremiumMahaLifeSupreme(String totalPremiumMahaLifeSupreme) {
			this.totalPremiumMahaLifeSupreme = totalPremiumMahaLifeSupreme;
		}
		public String getTotalServiceTaxMahaLifeSupreme() {
			return totalServiceTaxMahaLifeSupreme;
		}
		public void setTotalServiceTaxMahaLifeSupreme(
				String totalServiceTaxMahaLifeSupreme) {
			this.totalServiceTaxMahaLifeSupreme = totalServiceTaxMahaLifeSupreme;
		}
		public String getTotalPremiumWithServiceTaxMahaLifeSupreme() {
			return TotalPremiumWithServiceTaxMahaLifeSupreme;
		}
		public void setTotalPremiumWithServiceTaxMahaLifeSupreme(
				String TotalPremiumWithServiceTaxMahaLifeSupreme) {
			this.TotalPremiumWithServiceTaxMahaLifeSupreme = TotalPremiumWithServiceTaxMahaLifeSupreme;
		}
		public String getStrGenderId() {
			return strGenderId;
		}
		public void setStrGenderId(String strGenderId) {
			this.strGenderId = strGenderId;
		}
		public void setPremiumPaymentMode(int premiumPaymentMode) {
			PremiumPaymentMode = premiumPaymentMode;
		}
}
