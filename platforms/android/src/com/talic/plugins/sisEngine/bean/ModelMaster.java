package com.talic.plugins.sisEngine.bean;

import com.talic.plugins.sisEngine.BO.AddressBO;
import com.talic.plugins.sisEngine.BO.AgentDetails;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings({ "unchecked", "serial" })
public class ModelMaster implements java.io.Serializable{

	private SISRequestBean requestBean;
	//private DataMapperBO dataMapperBO;
	private long baseAnnualizedPremium=0;
	private long annualizedPremiumWithOccLoading=0;
	private long modalPremium=0;
	private long modalPremiumForNSAP=0;
	private long modalPremiumForAnnNSAP=0;
	private long AnnualizedPremium=0;
	private long quotedModalPremium=0;
	private long renewalModalPremium=0;//Added by Samina on 18/11/2014
	private long modalPremiumForRiders=0;
	private long serviceTax=0;
	private long serviceTaxCombo1=0;
	private long serviceTaxCombo2=0;
	private long totalModalPremium=0;
	private long totalRenewalModalPremium=0;//Added by Samina on 18/11/2014
	private long totalModalPremiumPayble=0;
	private long totalModalPremiumPaybleCombo1=0;
	private long totalModalPremiumPaybleCombo2=0;
	private long quotedAnnualizedPremium=0;
	private long totalPremiumWithServiceTax=0;
	private long modalPrem=0;

	private long discountedBasePrimium=0;
	private long discountedModalPrimium = 0;
	private ArrayList riderData;
	private String UINNumber;
	private HashMap nonGuaranteedSurrBenefit;
	private HashMap guaranteedSurrBenefit;
	private HashMap maturityVal;
	private HashMap maturityValSGP1;
	private HashMap maturityValSGP2;
	private String planDescription;
	private double premiumDiscount;

	private AddressBO oAddressBO;
	private String tempImagePath;

	private String PlanFeature;

	private HashMap PremiumAllocationCharges;
	private HashMap amountForInvestment;
	private HashMap otherBenefitCharges;
	private HashMap mortalityCharges;
	private HashMap policyAdminCharges;
	private HashMap ApplicableServiceTaxPH10;
	private HashMap DeathBenifitAtEndOfPolicyYear;
	private HashMap DeathBenifitAtEndOfPolicyYearPH6;
	private HashMap netPHAmmountforLoyaltyBonus;
	private HashMap GuaranteedMaturityAdditionPH10;
	private HashMap SurrenderValuePH10;
	private HashMap fundManagmentChargePH10;
	private HashMap regularFundValuePH10;
	private HashMap regularFundValueBeforeFMCPH10;
	private HashMap regularFundValuePostFMCPH10;
	private HashMap regularFundValuePostFMCPH6;//Added by Samina for PH6
	private HashMap totalFundValueAtEndPolicyYearPH10;

	private HashMap ApplicableServiceTaxPH6;
	private HashMap GuaranteedMaturityAdditionPH6;
	private HashMap SurrenderValuePH6;
	private HashMap fundManagmentChargePH6;
	private HashMap regularFundValuePH6;
	private HashMap regularFundValueBeforeFMCPH6;
	private HashMap totalFundValueAtEndPolicyYearPH6;
	private HashMap mortalityChargesPH6;

	private double fmc; // average fund management charges
	private long STRiderModalPremium;
	private long totalRiderModalPremium;
	private long totAnnRiderModalPremium;
	private double premiumMultiple;
	private long maturityBenefit10;
	private long maturityBenefit6;
	private HashMap lifeCoverage;
	private HashMap lifeCoverageCombo1;
	private HashMap lifeCoverageCombo2;
	private HashMap guranteedAnnualIncome;
	private HashMap guranteedAnnualIncomeAll;
	private HashMap RPGAI;
	private HashMap guranteedSurrenderValue;
	private HashMap totalMaturityValue;

	private long TotalMaturityValuePlusGAI;

	//private long modalPremiumNSAP;
	private String errorMessage;

	private String commissionText;
	private HashMap commissionPayble;
	private HashMap TotalPremiumAnnual;
	private HashMap	VSRBonus4;
	private HashMap	AltVSRBonus4;//Added by Samina forn reduced paid up on 20/11/2014
	private HashMap	AltEBDURING;//Added by Samina forn reduced paid up on 20/11/2014
	private HashMap TerminalBonus4;

	private HashMap	VSRBonus8;
	private HashMap	AltVSRBonus8;
	private HashMap TerminalBonus8;

	private HashMap TotalDeathBenefit4;
	private HashMap TotalDeathBenefit8;

	private HashMap nonGuaranteedSurrBenefit_8; //money MAxima

	/** Vikas - Added for Mahalife Gold*/
	private HashMap guranteedAnnualInflationCover;
	private HashMap TotalDeathBenefit;
	private long totalcashdividend4;
	private long totalcashdividend8;
	private long totalGAnnualCoupon;
	/** Vikas - Added for Mahalife Gold*/
	private long renewalPremium_A=0;	//added by jayesh for SMART 7 21-05-2014
	private long renewalPremium_SA=0;	//added by jayesh for SMART 7 21-05-2014
	private long renewalPremium_Q=0;	//added by jayesh for SMART 7 21-05-2014
	private long renewalPremium_M=0;	//added by jayesh for SMART 7 21-05-2014
	private HashMap lifeCoverageAltSce;	//added by jayesh for SMART 7 23-05-2014
	private HashMap LifeCoverageAlternateSce; //Added by Samina on 11/11/2014
	private HashMap altScenarioMaturityVal;		//added by jayesh for SMART 7 26-05-2014
	private HashMap TOTaltScenarioMaturityVal;
	private HashMap altScenarioTotalDeathBenefit4;	//added by jayesh for SMART 7 27-05-2014
	private HashMap loyaltyChargeforPH10;
	private HashMap loyaltyChargeforPH6;                                     //Added by samina for loyalty charges
	private HashMap totalchargeforPH10;
	private HashMap TotalchargeforPH6;//Added by samina for total charges for PH6
	private HashMap totalFundValueAtEndPolicyYearPH10_AS3;
	private HashMap SurrenderValuePH10_AS3;
	private HashMap TotalDeathBenefitPH10_AS3;
	private HashMap MortalityCharges_AS3;
	private HashMap ApplicableServiceTaxPH10_AS3;
	private HashMap TotalchargeforPH10_AS3;
	private HashMap LoyaltyChargeforPH10_AS3;
	private HashMap FundManagmentChargePH10_AS3;
	private HashMap RegularFundValuePH10_AS3;
	private HashMap RegularFundValueBeforeFMCPH10_AS3;
	private HashMap RegularFundValuePostFMCPH10_AS3;
	private HashMap GuaranteedMaturityAdditionPH10_AS3;
	private HashMap loyaltyChargeforFP;
	private HashMap SurrenderValueFP;
	private HashMap DeathBenifitAtEndOfPolicyYearFP;
	private HashMap regularFundValueFP;
	private HashMap topUpPremium;
	private HashMap withdrawalAmout;
	private HashMap regularFundValuePostFMC;
	private HashMap topUpLoyalty;
	private HashMap topUpRegularFundValueEndOfYear;
	private HashMap topUpSurrValue;
	private HashMap topUpDeathBenefit;
	private HashMap totalFundValueAtEndPolicyYearPH6_AS3;
	private HashMap SurrenderValuePH6_AS3;
	private HashMap TotalDeathBenefitPH6_AS3;
	private HashMap MortalityChargesPH6_AS3;
	private HashMap ApplicableServiceTaxPH6_AS3;
	private HashMap TotalchargeforPH6_AS3;
	private HashMap LoyaltyChargeforPH6_AS3;
	private HashMap FundManagmentChargePH6_AS3;
	private HashMap RegularFundValuePH6_AS3;
	private HashMap RegularFundValueBeforeFMCPH6_AS3;
	private HashMap RegularFundValuePostFMCPH6_AS3;
	private HashMap GuaranteedMaturityAdditionPH6_AS3;
	private HashMap mortalityCharges_SMART10;
	private HashMap applicableServiceTaxPH10_SMART10;
	private HashMap totalchargeforPH10_SMART10;
	private HashMap loyaltyChargeforPH10_SMART10;
	private HashMap fundManagmentChargePH10_SMART10;
	private HashMap regularFundValuePH10_SMART10;
	private HashMap regularFundValueBeforeFMCPH10_SMART10;
	private HashMap regularFundValuePostFMCPH10_SMART10;
	private HashMap guaranteedMaturityAdditionPH10_SMART10;
	private HashMap totalDeathBenefitPH10_SMART10;
	private HashMap surrenderValuePH10_SMART10;
	private HashMap mortalityCharges_SMART6;
	private HashMap applicableServiceTaxPH10_SMART6;
	private HashMap totalchargeforPH10_SMART6;
	private HashMap loyaltyChargeforPH10_SMART6;
	private HashMap fundManagmentChargePH10_SMART6;
	private HashMap regularFundValuePH10_SMART6;
	private HashMap regularFundValueBeforeFMCPH10_SMART6;
	private HashMap regularFundValuePostFMCPH10_SMART6;
	private HashMap guaranteedMaturityAdditionPH10_SMART6;
	private HashMap totalDeathBenefitPH10_SMART6;
	private HashMap surrenderValuePH10_SMART6;
	private HashMap RegularFundValueAAA_LCEF_PH10;
	private HashMap RegularFundValueAAA_WLEF_PH10;
	private HashMap RegularFundValuePH6_LCEF_AS3;
	private HashMap RegularFundValuePH6_WLEF_AS3;
	private HashMap altScenarioTotalDeathBenefit8;
	private HashMap altScenarioMaturityVal4;
	private HashMap altScenarioMaturityVal8;
	private HashMap altScenarioTotMatBenCurrentRate;
	private HashMap altScenarioSurrBenefitCurrentRate;
	private HashMap altScenarioPremPaidCumulativeCurrentRate;
	private HashMap altScenarioTaxSaving;
	private HashMap altScenarioannualizedeffectivepremium;
	private HashMap altScenariocummulativeeffectivepremium;
	private HashMap altScenario4MaturityBenefitTax;
	private long serviceTaxBasePlan=0;						//added by jayesh for SMART 7 23-06-2014
	private long totModalPremiumPayble=0;
	private long renewalPremiumST_A=0;	//added by jayesh for SMART 7 23-06-2014
	private long renewalPremiumST_RS_A=0;
	private long renewalPremiumST_RS_SA=0;
	private long renewalPremiumST_RS_Q=0;
	private long renewalPremiumST_RS_M=0;
	private long renewalPremiumST_RS_A_Combo1=0;
	private long renewalPremiumST_RS_SA_Combo1=0;
	private long renewalPremiumST_RS_Q_Combo1=0;
	private long renewalPremiumST_RS_M_Combo1=0;
	private long renewalPremiumST_RS_A_Combo2=0;
	private long renewalPremiumST_RS_SA_Combo2=0;
	private long renewalPremiumST_RS_Q_Combo2=0;
	private long renewalPremiumST_RS_M_Combo2=0;
	private long renewalPremiumST_SA=0;	//added by jayesh for SMART 7 23-06-2014
	private long renewalPremiumST_Q=0;	//added by jayesh for SMART 7 23-06-2014
	private long renewalPremiumST_M=0;	//added by jayesh for SMART 7 23-06-2014
	private long totalRenewalPremium_SA=0;	//added by jayesh for SMART 7 23-06-2014
	private long totalRenewalPremium_Q=0;	//added by jayesh for SMART 7 23-06-2014
	private long totalRenewalPremium_M=0;	//added by jayesh for SMART 7 23-06-2014
	private long totalRenewalPremium_A=0;	//added by jayesh for SMART 7 23-06-2014
	private long totalRenewalPremium_RS_SA=0;
	private long totalRenewalPremium_RS_Q=0;
	private long totalRenewalPremium_RS_M=0;
	private long totalRenewalPremium_RS_A=0;
	private long totalRenewalPremium_RS_SA_Combo1=0;
	private long totalRenewalPremium_RS_Q_Combo1=0;
	private long totalRenewalPremium_RS_M_Combo1=0;
	private long totalRenewalPremium_RS_A_Combo1=0;
	private long totalRenewalPremium_RS_SA_Combo2=0;
	private long totalRenewalPremium_RS_Q_Combo2=0;
	private long totalRenewalPremium_RS_M_Combo2=0;
	private long totalRenewalPremium_RS_A_Combo2=0;
	private HashMap nonGuaranteed4SurrBenefit;	//added by jayesh for SMART 7 25-06-2014
	private HashMap	variableBonus;					//added by jayesh for SMART 7 25-06-2014
	private HashMap altScenarioTotalDeathBenefitCR;	//added by jayesh for SMART 7 26-06-2014
	private HashMap totalMaturityBenefit8;	//added by jayesh for SMART 7 04-06-2014
	private HashMap SurrBenefit8;	//added by jayesh for SMART 7 04-06-2014
	private HashMap totalRiderAnnualPremium;
	private HashMap accGuranteedAnnualIncome;
	private double initDebtFundFactor;
	private double initEquityFundFactor;
	private String annualPremium;
	private String altHeader;
	private String illusPageNo;
	private double netYield8P;
	private double netYield8TW;
	private double netYield8SMART;
	private HashMap ALTSCEGAI;//Added by samina on 13/11/2014
	private HashMap MaturityValALTSCE;//Added by samina on 13/11/2014
	private HashMap ALTguranteedSurrenderValue;//Added by Samina on 13/11/2014
	private HashMap ALTSCESurrBenefit;////Added by Samina on 18/11/2014
	private long ModalPremiumRenewal=0;//Added by Samina on 18/11/2014
	private HashMap totalMaturityBenefit4;
	private long TOTALRB8;
	private long TOTALRB4;
	private long TOTALGAI;
	private long TAXtext;
	private HashMap AnnuityRate;//Added by Samina for Easy Retire
	private long totalERModalPremPayble=0;
	private AgentDetails AgentDeatils;
	private ArrayList masterCombo;
	private HashMap nonGuaranteedSurrBenefitCombo1;
	private HashMap nonGuaranteedSurrBenefitCombo2;
	private boolean isCombo;
	private long comboAnnualizedPremium;
	private long Combo2AnnualizedPremium;
	private long Combo1AnnualizedPremium;
	private long Comboprem1;
	private long Comboprem2;
	private double sec7premmonthly1;
	private long sec7Ann;
	private double ServiceTaxMain;
	private double ServiceTaxFY;
	private double ServiceTaxRenewal;
	private long modalPremiumSGP1;
	private long modalPremiumSGP2;
	private HashMap basicSumAssured;
	private HashMap totalGuaranteedBenefit;
    private HashMap MilestoneAdd8Dict;
    private HashMap MilestoneAdd4Dict;
    private HashMap MoneyBackBenefitDict;
    public HashMap getMilestoneAdd8Dict() {
        return MilestoneAdd8Dict;
    }


	//Combo Details
	public  String ComboInsName;
	public  String ComboPropName;
	public  int ComboInsAge;
	public  Integer ComboPropAge;
	public  String ComboInsGender;
	public  String ComboPropGender;
	public  String ComboInsPayMode;
	public  String ComboInsOcc;
	public  String ComboProOcc;
	public  String ComboInsSmoker;
	public  String ComboPropSmoker;
	public  String ComboUIN;
	public  String ComboPropNo;
	public  String ComboPropDate;

	public String ComboBasePlanName;
	public String ComboTermPlanName;

	public Integer ComboBasePolTerm;
	public Integer ComboTermPolTerm;

	public Integer ComboBasePPT;
	public Integer ComboTermPPT;

	public long getComboBaseTotaAmt() {
		return ComboBaseTotaAmt;
	}

	public void setComboBaseTotaAmt(long comboBaseTotaAmt) {
		ComboBaseTotaAmt = comboBaseTotaAmt;
	}

	public long ComboBaseTotaAmt;

	public long ComboBaseAP;
	public long ComboTermAP;

	public String getComboInsOccDesc() {
		return ComboInsOccDesc;
	}

	public void setComboInsOccDesc(String comboInsOccDesc) {
		ComboInsOccDesc = comboInsOccDesc;
	}

	public String ComboInsOccDesc;

	public String getComboPropOccDesc() {
		return ComboPropOccDesc;
	}

	public void setComboPropOccDesc(String comboPropOccDesc) {
		ComboPropOccDesc = comboPropOccDesc;
	}

	public String ComboPropOccDesc;

	public String getComboBaseAnnPrem() {
		return ComboBaseAnnPrem;
	}

	public void setComboBaseAnnPrem(String comboBaseAnnPrem) {
		ComboBaseAnnPrem = comboBaseAnnPrem;
	}

	public String getComboTermAnnPrem() {
		return ComboTermAnnPrem;
	}

	public void setComboTermAnnPrem(String comboTermAnnPrem) {
		ComboTermAnnPrem = comboTermAnnPrem;
	}

	public long getComboBaseQAP() {
		return ComboBaseQAP;
	}

	public void setComboBaseQAP(long comboBaseQAP) {
		ComboBaseQAP = comboBaseQAP;
	}

	public long getComboTermQAP() {
		return ComboTermQAP;
	}

	public void setComboTermQAP(long comboTermQAP) {
		ComboTermQAP = comboTermQAP;
	}

	public long ComboBaseQAP;
	public long ComboTermQAP;

	public String ComboBaseAnnPrem;
	public String ComboTermAnnPrem;

	public long ComboBaseST;
	public long ComboTermST;

	public long ComboBaseTP;
	public long ComboTermTP;

	public long ComboBaseSA;
	public long ComboTermSA;

	public String getComboBaseAnnRenPrem() {
		return ComboBaseAnnRenPrem;
	}

	public void setComboBaseAnnRenPrem(String comboBaseAnnRenPrem) {
		ComboBaseAnnRenPrem = comboBaseAnnRenPrem;
	}

	public long getComboBaseSemAnnRenPrem() {
		return ComboBaseSemAnnRenPrem;
	}

	public void setComboBaseSemAnnRenPrem(long comboBaseSemAnnRenPrem) {
		ComboBaseSemAnnRenPrem = comboBaseSemAnnRenPrem;
	}

	public long getComboBaseQuaAnnRenPrem() {
		return ComboBaseQuaAnnRenPrem;
	}

	public void setComboBaseQuaAnnRenPrem(long comboBaseQuaAnnRenPrem) {
		ComboBaseQuaAnnRenPrem = comboBaseQuaAnnRenPrem;
	}

	public long getComboBaseMonthAnnRenPrem() {
		return ComboBaseMonthAnnRenPrem;
	}

	public void setComboBaseMonthAnnRenPrem(long comboBaseMonthAnnRenPrem) {
		ComboBaseMonthAnnRenPrem = comboBaseMonthAnnRenPrem;
	}

	public long getComboBaseAnnST() {
		return ComboBaseAnnST;
	}

	public void setComboBaseAnnST(long comboBaseAnnST) {
		ComboBaseAnnST = comboBaseAnnST;
	}

	public long getComboBaseSemAnnST() {
		return ComboBaseSemAnnST;
	}

	public void setComboBaseSemAnnST(long comboBaseSemAnnST) {
		ComboBaseSemAnnST = comboBaseSemAnnST;
	}

	public long getComboBaseQuaST() {
		return ComboBaseQuaST;
	}

	public void setComboBaseQuaST(long comboBaseQuaST) {
		ComboBaseQuaST = comboBaseQuaST;
	}

	public long getComboBaseMonthST() {
		return ComboBaseMonthST;
	}

	public void setComboBaseMonthST(long comboBaseMonthST) {
		ComboBaseMonthST = comboBaseMonthST;
	}

	public long getComboBaseAnnTotPrem() {
		return ComboBaseAnnTotPrem;
	}

	public void setComboBaseAnnTotPrem(long comboBaseAnnTotPrem) {
		ComboBaseAnnTotPrem = comboBaseAnnTotPrem;
	}

	public long getComboBaseSemAnnTotPrem() {
		return ComboBaseSemAnnTotPrem;
	}

	public void setComboBaseSemAnnTotPrem(long comboBaseSemAnnTotPrem) {
		ComboBaseSemAnnTotPrem = comboBaseSemAnnTotPrem;
	}

	public long getComboBaseQuaTotPrem() {
		return ComboBaseQuaTotPrem;
	}

	public void setComboBaseQuaTotPrem(long comboBaseQuaTotPrem) {
		ComboBaseQuaTotPrem = comboBaseQuaTotPrem;
	}

	public long getComboBaseMonthTotPrem() {
		return ComboBaseMonthTotPrem;
	}

	public void setComboBaseMonthTotPrem(long comboBaseMonthTotPrem) {
		ComboBaseMonthTotPrem = comboBaseMonthTotPrem;
	}

	public String ComboBaseAnnRenPrem;
	public long ComboBaseSemAnnRenPrem;
	public long ComboBaseQuaAnnRenPrem;
	public long ComboBaseMonthAnnRenPrem;

	public long ComboBaseAnnST;
	public long ComboBaseSemAnnST;
	public long ComboBaseQuaST;
	public long ComboBaseMonthST;

	public long ComboBaseAnnTotPrem;
	public long ComboBaseSemAnnTotPrem;
	public long ComboBaseQuaTotPrem;
	public long ComboBaseMonthTotPrem;



	public long ComboAnnRenewalPrem;
	public long ComboSemAnnRenewalPrem;
	public long ComboQuaAnnRenewalPrem;
	public long ComboMonthAnnRenewalPrem;

	public long ComboAnnServiceTax;
	public long ComboSemServiceTax;
	public long ComboQuaServiceTax;
	public long ComboMonthServiceTax;

	public long ComboAnnTotPrem;
	public long ComboSemTotPrem;
	public long ComboQuaTotPrem;
	public long ComboMonthTotPrem;

	public int TLC;
	public int WLE;
	public int WLA;
	public int WLS;
	public int WLI;
	public int WLF;

	public String ComboBaseProdType;
	public HashMap ComboBaseDeathBenefit8;
	public HashMap ComboBaseSurrVal8;
	public HashMap ComboBaseSurrVal4;
	public HashMap ComboTermSurrVal;

	public HashMap getComboTermSurrVal1() {
		return ComboTermSurrVal1;
	}

	public void setComboTermSurrVal1(HashMap comboTermSurrVal1) {
		ComboTermSurrVal1 = comboTermSurrVal1;
	}

	public HashMap ComboTermSurrVal1;
	public HashMap ComboBaseDeathBenefit4;
	public HashMap ComboTermDeathBenefit8;
	public String ComboTermProdType;
	public String ComboPayMode;
	public String ComboUINNumber;

	public String getComboBasePlanCode() {
		return ComboBasePlanCode;
	}

	public void setComboBasePlanCode(String comboBasePlanCode) {
		ComboBasePlanCode = comboBasePlanCode;
	}

	public String getComboTermPlanCode() {
		return ComboTermPlanCode;
	}

	public void setComboTermPlanCode(String comboTermPlanCode) {
		ComboTermPlanCode = comboTermPlanCode;
	}

	public String ComboBasePlanCode;
	public String ComboTermPlanCode;

	public HashMap getComboBaseSurrVal() {
		return ComboBaseSurrVal;
	}

	public void setComboBaseSurrVal(HashMap comboBaseSurrVal) {
		ComboBaseSurrVal = comboBaseSurrVal;
	}

	public HashMap ComboBaseSurrVal;

	public HashMap getComboBaseGuarSurrVal() {
		return ComboBaseGuarSurrVal;
	}

	public void setComboBaseGuarSurrVal(HashMap comboBaseGuarSurrVal) {
		ComboBaseGuarSurrVal = comboBaseGuarSurrVal;
	}

	public HashMap ComboBaseGuarSurrVal;

	public HashMap getComboBaseDeathBenefit() {
		return ComboBaseDeathBenefit;
	}

	public void setComboBaseDeathBenefit(HashMap comboBaseDeathBenefit) {
		ComboBaseDeathBenefit = comboBaseDeathBenefit;
	}

	public HashMap ComboBaseDeathBenefit;

	public HashMap getComboBaseGuarPayout() {
		return ComboBaseGuarPayout;
	}

	public void setComboBaseGuarPayout(HashMap comboBaseGuarPayout) {
		ComboBaseGuarPayout = comboBaseGuarPayout;
	}

	public HashMap getComboBaseGuarMatPayout() {
		return ComboBaseGuarMatPayout;
	}

	public void setComboBaseGuarMatPayout(HashMap comboBaseGuarMatPayout) {
		ComboBaseGuarMatPayout = comboBaseGuarMatPayout;
	}

	public HashMap ComboBaseGuarPayout;
	public HashMap ComboBaseGuarMatPayout;

	public HashMap getComboTermMatVal() {
		return ComboTermMatVal;
	}

	public void setComboTermMatVal(HashMap comboTermMatVal) {
		ComboTermMatVal = comboTermMatVal;
	}

	public HashMap ComboTermMatVal;

	public String getCombotemplateName() {
		return CombotemplateName;
	}

	public void setCombotemplateName(String combotemplateName) {
		CombotemplateName = combotemplateName;
	}

	public String CombotemplateName;

	public String getComboBasePGLID() {
		return ComboBasePGLID;
	}

	public void setComboBasePGLID(String comboBasePGLID) {
		ComboBasePGLID = comboBasePGLID;
	}

	public String getComboTermPGLID() {
		return ComboTermPGLID;
	}

	public void setComboTermPGLID(String comboTermPGLID) {
		ComboTermPGLID = comboTermPGLID;
	}

	public String ComboBasePGLID;
	public String ComboTermPGLID;


	public HashMap getComboBaseFundValue8() {
		return ComboBaseFundValue8;
	}

	public void setComboBaseFundValue8(HashMap comboBaseFundValue8) {
		ComboBaseFundValue8 = comboBaseFundValue8;
	}

	public HashMap getComboBaseFundValue4() {
		return ComboBaseFundValue4;
	}

	public void setComboBaseFundValue4(HashMap comboBaseFundValue4) {
		ComboBaseFundValue4 = comboBaseFundValue4;
	}

	public HashMap ComboBaseFundValue8;
	public HashMap ComboBaseFundValue4;


	public HashMap getComboBaseDeathBenefit8() {
		return ComboBaseDeathBenefit8;
	}

	public void setComboBaseDeathBenefit8(HashMap comboBaseDeathBenefit8) {
		ComboBaseDeathBenefit8 = comboBaseDeathBenefit8;
	}

	public HashMap getComboTermDeathBenefit8() {
		return ComboTermDeathBenefit8;
	}

	public void setComboTermDeathBenefit8(HashMap comboTermDeathBenefit8) {
		ComboTermDeathBenefit8 = comboTermDeathBenefit8;
	}

	public HashMap getComboBaseSurrVal8() {
		return ComboBaseSurrVal8;
	}

	public void setComboBaseSurrVal8(HashMap comboBaseSurrVal8) {
		ComboBaseSurrVal8 = comboBaseSurrVal8;
	}

	public HashMap getComboBaseSurrVal4() {
		return ComboBaseSurrVal4;
	}

	public void setComboBaseSurrVal4(HashMap comboBaseSurrVal4) {
		ComboBaseSurrVal4 = comboBaseSurrVal4;
	}

	public HashMap getComboTermSurrVal() {
		return ComboTermSurrVal;
	}

	public void setComboTermSurrVal(HashMap comboTermSurrVal) {
		ComboTermSurrVal = comboTermSurrVal;
	}

	public HashMap getComboBaseDeathBenefit4() {
		return ComboBaseDeathBenefit4;
	}

	public void setComboBaseDeathBenefit4(HashMap comboBaseDeathBenefit4) {
		ComboBaseDeathBenefit4 = comboBaseDeathBenefit4;
	}

	public String getComboTermProdType() {
		return ComboTermProdType;
	}

	public void setComboTermProdType(String comboTermProdType) {
		ComboTermProdType = comboTermProdType;
	}

	public String getComboBaseProdType() {
		return ComboBaseProdType;
	}

	public void setComboBaseProdType(String comboBaseProdType) {
		ComboBaseProdType = comboBaseProdType;
	}

	public String getComboPayMode() {
		return ComboPayMode;
	}

	public void setComboPayMode(String comboPayMode) {
		ComboPayMode = comboPayMode;
	}

	public int getComboPolTerm() {
		return ComboPolTerm;
	}

	public void setComboPolTerm(int comboPolTerm) {
		ComboPolTerm = comboPolTerm;
	}

	public int ComboPolTerm;

	public String getComboUINNumber() {
		return ComboUINNumber;
	}

	public void setComboUINNumber(String comboUINNumber) {
		ComboUINNumber = comboUINNumber;
	}

	public AddressBO getoAddressBO() {
		return oAddressBO;
	}

	public void setoAddressBO(AddressBO oAddressBO) {
		this.oAddressBO = oAddressBO;
	}

	public void setIsCombo(boolean isCombo) {
		this.isCombo = isCombo;
	}

	public String getComboInsName() {
		return ComboInsName;
	}

	public void setComboInsName(String comboInsName) {
		ComboInsName = comboInsName;
	}

	public String getComboPropName() {
		return ComboPropName;
	}

	public void setComboPropName(String comboPropName) {
		ComboPropName = comboPropName;
	}

	public int getComboInsAge() {
		return ComboInsAge;
	}

	public void setComboInsAge(int comboInsAge) {
		ComboInsAge = comboInsAge;
	}

	public Integer getComboPropAge() {
		return ComboPropAge;
	}

	public void setComboPropAge(Integer comboPropAge) {
		ComboPropAge = comboPropAge;
	}

	public String getComboInsGender() {
		return ComboInsGender;
	}

	public void setComboInsGender(String comboInsGender) {
		ComboInsGender = comboInsGender;
	}

	public String getComboPropGender() {
		return ComboPropGender;
	}

	public void setComboPropGender(String comboPropGender) {
		ComboPropGender = comboPropGender;
	}

	public String getComboInsPayMode() {
		return ComboInsPayMode;
	}

	public void setComboInsPayMode(String comboInsPayMode) {
		ComboInsPayMode = comboInsPayMode;
	}

	public String getComboInsOcc() {
		return ComboInsOcc;
	}

	public void setComboInsOcc(String comboInsOcc) {
		ComboInsOcc = comboInsOcc;
	}

	public String getComboProOcc() {
		return ComboProOcc;
	}

	public void setComboProOcc(String comboProOcc) {
		ComboProOcc = comboProOcc;
	}

	public String getComboInsSmoker() {
		return ComboInsSmoker;
	}

	public void setComboInsSmoker(String comboInsSmoker) {
		ComboInsSmoker = comboInsSmoker;
	}

	public String getComboPropSmoker() {
		return ComboPropSmoker;
	}

	public void setComboPropSmoker(String comboPropSmoker) {
		ComboPropSmoker = comboPropSmoker;
	}

	public String getComboUIN() {
		return ComboUIN;
	}

	public void setComboUIN(String comboUIN) {
		ComboUIN = comboUIN;
	}

	public String getComboPropNo() {
		return ComboPropNo;
	}

	public void setComboPropNo(String comboPropNo) {
		ComboPropNo = comboPropNo;
	}

	public String getComboPropDate() {
		return ComboPropDate;
	}

	public void setComboPropDate(String comboPropDate) {
		ComboPropDate = comboPropDate;
	}

	public String getComboBasePlanName() {
		return ComboBasePlanName;
	}

	public void setComboBasePlanName(String comboBasePlanName) {
		ComboBasePlanName = comboBasePlanName;
	}

	public String getComboTermPlanName() {
		return ComboTermPlanName;
	}

	public void setComboTermPlanName(String comboTermPlanName) {
		ComboTermPlanName = comboTermPlanName;
	}

	public Integer getComboBasePolTerm() {
		return ComboBasePolTerm;
	}

	public void setComboBasePolTerm(Integer comboBasePolTerm) {
		ComboBasePolTerm = comboBasePolTerm;
	}

	public Integer getComboTermPolTerm() {
		return ComboTermPolTerm;
	}

	public void setComboTermPolTerm(Integer comboTermPolTerm) {
		ComboTermPolTerm = comboTermPolTerm;
	}

	public Integer getComboBasePPT() {
		return ComboBasePPT;
	}

	public void setComboBasePPT(Integer comboBasePPT) {
		ComboBasePPT = comboBasePPT;
	}

	public Integer getComboTermPPT() {
		return ComboTermPPT;
	}

	public void setComboTermPPT(Integer comboTermPPT) {
		ComboTermPPT = comboTermPPT;
	}

	public long getComboBaseAP() {
		return ComboBaseAP;
	}

	public void setComboBaseAP(long comboBaseAP) {
		ComboBaseAP = comboBaseAP;
	}

	public long getComboTermAP() {
		return ComboTermAP;
	}

	public void setComboTermAP(long comboTermAP) {
		ComboTermAP = comboTermAP;
	}

	public long getComboBaseST() {
		return ComboBaseST;
	}

	public void setComboBaseST(long comboBaseST) {
		ComboBaseST = comboBaseST;
	}

	public long getComboTermST() {
		return ComboTermST;
	}

	public void setComboTermST(long comboTermST) {
		ComboTermST = comboTermST;
	}

	public long getComboBaseTP() {
		return ComboBaseTP;
	}

	public void setComboBaseTP(long comboBaseTP) {
		ComboBaseTP = comboBaseTP;
	}

	public long getComboTermTP() {
		return ComboTermTP;
	}

	public void setComboTermTP(long comboTermTP) {
		ComboTermTP = comboTermTP;
	}

	public long getComboBaseSA() {
		return ComboBaseSA;
	}

	public void setComboBaseSA(long comboBaseSA) {
		ComboBaseSA = comboBaseSA;
	}

	public long getComboTermSA() {
		return ComboTermSA;
	}

	public void setComboTermSA(long comboTermSA) {
		ComboTermSA = comboTermSA;
	}

	public long getComboAnnRenewalPrem() {
		return ComboAnnRenewalPrem;
	}

	public void setComboAnnRenewalPrem(long comboAnnRenewalPrem) {
		ComboAnnRenewalPrem = comboAnnRenewalPrem;
	}

	public long getComboSemAnnRenewalPrem() {
		return ComboSemAnnRenewalPrem;
	}

	public void setComboSemAnnRenewalPrem(long comboSemAnnRenewalPrem) {
		ComboSemAnnRenewalPrem = comboSemAnnRenewalPrem;
	}

	public long getComboQuaAnnRenewalPrem() {
		return ComboQuaAnnRenewalPrem;
	}

	public void setComboQuaAnnRenewalPrem(long comboQuaAnnRenewalPrem) {
		ComboQuaAnnRenewalPrem = comboQuaAnnRenewalPrem;
	}

	public long getComboMonthAnnRenewalPrem() {
		return ComboMonthAnnRenewalPrem;
	}

	public void setComboMonthAnnRenewalPrem(long comboMonthAnnRenewalPrem) {
		ComboMonthAnnRenewalPrem = comboMonthAnnRenewalPrem;
	}

	public long getComboAnnServiceTax() {
		return ComboAnnServiceTax;
	}

	public void setComboAnnServiceTax(long comboAnnServiceTax) {
		ComboAnnServiceTax = comboAnnServiceTax;
	}

	public long getComboSemServiceTax() {
		return ComboSemServiceTax;
	}

	public void setComboSemServiceTax(long comboSemServiceTax) {
		ComboSemServiceTax = comboSemServiceTax;
	}

	public long getComboQuaServiceTax() {
		return ComboQuaServiceTax;
	}

	public void setComboQuaServiceTax(long comboQuaServiceTax) {
		ComboQuaServiceTax = comboQuaServiceTax;
	}

	public long getComboMonthServiceTax() {
		return ComboMonthServiceTax;
	}

	public void setComboMonthServiceTax(long comboMonthServiceTax) {
		ComboMonthServiceTax = comboMonthServiceTax;
	}

	public long getComboAnnTotPrem() {
		return ComboAnnTotPrem;
	}

	public void setComboAnnTotPrem(long comboAnnTotPrem) {
		ComboAnnTotPrem = comboAnnTotPrem;
	}

	public long getComboSemTotPrem() {
		return ComboSemTotPrem;
	}

	public void setComboSemTotPrem(long comboSemTotPrem) {
		ComboSemTotPrem = comboSemTotPrem;
	}

	public long getComboQuaTotPrem() {
		return ComboQuaTotPrem;
	}

	public void setComboQuaTotPrem(long comboQuaTotPrem) {
		ComboQuaTotPrem = comboQuaTotPrem;
	}

	public long getComboMonthTotPrem() {
		return ComboMonthTotPrem;
	}

	public void setComboMonthTotPrem(long comboMonthTotPrem) {
		ComboMonthTotPrem = comboMonthTotPrem;
	}

	public int getTLC() {
		return TLC;
	}

	public void setTLC(int TLC) {
		this.TLC = TLC;
	}

	public int getWLE() {
		return WLE;
	}

	public void setWLE(int WLE) {
		this.WLE = WLE;
	}

	public int getWLA() {
		return WLA;
	}

	public void setWLA(int WLA) {
		this.WLA = WLA;
	}

	public int getWLS() {
		return WLS;
	}

	public void setWLS(int WLS) {
		this.WLS = WLS;
	}

	public int getWLI() {
		return WLI;
	}

	public void setWLI(int WLI) {
		this.WLI = WLI;
	}

	public int getWLF() {
		return WLF;
	}

	public void setWLF(int WLF) {
		this.WLF = WLF;
	}




    public void setMilestoneAdd8Dict(HashMap milestoneAdd8Dict) {
        MilestoneAdd8Dict = milestoneAdd8Dict;
    }

    public HashMap getMilestoneAdd4Dict() {
        return MilestoneAdd4Dict;
    }

    public void setMilestoneAdd4Dict(HashMap milestoneAdd4Dict) {
        MilestoneAdd4Dict = milestoneAdd4Dict;
    }

    public HashMap getMoneyBackBenefitDict() {
        return MoneyBackBenefitDict;
    }

    public void setMoneyBackBenefitDict(HashMap moneyBackBenefitDict) {
        MoneyBackBenefitDict = moneyBackBenefitDict;
    }
	public AgentDetails getAgentDeatils() {
		return AgentDeatils;
	}

	public void setAgentDeatils(AgentDetails agentDeatils) {
		AgentDeatils = agentDeatils;
	}



	public long getServiceTaxBasePlan() {
		return serviceTaxBasePlan;
	}
	public void setServiceTaxBasePlan(long serviceTaxBasePlan) {
		this.serviceTaxBasePlan = serviceTaxBasePlan;
	}
	public long getTotModalPremiumPayble() {
		return totModalPremiumPayble;
	}
	public void setTotModalPremiumPayble(long totModalPremiumPayble) {
		this.totModalPremiumPayble = totModalPremiumPayble;
	}
	public long getRenewalPremiumST_A() {
		return renewalPremiumST_A;
	}
	public void setRenewalPremiumST_A(long renewalPremiumST_A) {
		this.renewalPremiumST_A = renewalPremiumST_A;
	}
	public long getRenewalPremiumST_SA() {
		return renewalPremiumST_SA;
	}
	public void setRenewalPremiumST_SA(long renewalPremiumST_SA) {
		this.renewalPremiumST_SA = renewalPremiumST_SA;
	}
	public long getRenewalPremiumST_Q() {
		return renewalPremiumST_Q;
	}
	public void setRenewalPremiumST_Q(long renewalPremiumST_Q) {
		this.renewalPremiumST_Q = renewalPremiumST_Q;
	}
	public long getRenewalPremiumST_M() {
		return renewalPremiumST_M;
	}
	public void setRenewalPremiumST_M(long renewalPremiumST_M) {
		this.renewalPremiumST_M = renewalPremiumST_M;
	}
	public long getTotalRenewalPremium_SA() {
		return totalRenewalPremium_SA;
	}
	public void setTotalRenewalPremium_SA(long totalRenewalPremium_SA) {
		this.totalRenewalPremium_SA = totalRenewalPremium_SA;
	}
	public long getTotalRenewalPremium_Q() {
		return totalRenewalPremium_Q;
	}
	public void setTotalRenewalPremium_Q(long totalRenewalPremium_Q) {
		this.totalRenewalPremium_Q = totalRenewalPremium_Q;
	}
	public long getTotalRenewalPremium_M() {
		return totalRenewalPremium_M;
	}
	public void setTotalRenewalPremium_M(long totalRenewalPremium_M) {
		this.totalRenewalPremium_M = totalRenewalPremium_M;
	}
	public long getTotalRenewalPremium_A() {
		return totalRenewalPremium_A;
	}
	public void setTotalRenewalPremium_A(long totalRenewalPremium_A) {
		this.totalRenewalPremium_A = totalRenewalPremium_A;
	}
	public HashMap getNonGuaranteed4SurrBenefit() {
		return nonGuaranteed4SurrBenefit;
	}
	public void setNonGuaranteed4SurrBenefit(HashMap nonGuaranteed4SurrBenefit) {
		this.nonGuaranteed4SurrBenefit = nonGuaranteed4SurrBenefit;
	}
	public HashMap getVariableBonus() {
		return variableBonus;
	}
	public void setVariableBonus(HashMap variableBonus) {
		this.variableBonus = variableBonus;
	}
	public HashMap getAltScenarioTotalDeathBenefitCR() {
		return altScenarioTotalDeathBenefitCR;
	}
	public void setAltScenarioTotalDeathBenefitCR(
			HashMap altScenarioTotalDeathBenefitCR) {
		this.altScenarioTotalDeathBenefitCR = altScenarioTotalDeathBenefitCR;
	}
	public HashMap getTotalMaturityBenefit8() {
		return totalMaturityBenefit8;
	}
	public void setTotalMaturityBenefit8(HashMap totalMaturityBenefit8) {
		this.totalMaturityBenefit8 = totalMaturityBenefit8;
	}
	public HashMap getSurrBenefit8() {
		return SurrBenefit8;
	}
	public void setSurrBenefit8(HashMap surrBenefit8) {
		SurrBenefit8 = surrBenefit8;
	}
	public SISRequestBean getRequestBean() {
		return requestBean;
	}
	public void setRequestBean(SISRequestBean requestBean) {
		this.requestBean = requestBean;
	}
	public long getBaseAnnualizedPremium() {
		return baseAnnualizedPremium;
	}
	public void setBaseAnnualizedPremium(long baseAnnualizedPremium) {
		this.baseAnnualizedPremium = baseAnnualizedPremium;
	}
	public long getAnnualizedPremiumWithOccLoading() {
		return annualizedPremiumWithOccLoading;
	}
	public void setAnnualizedPremiumWithOccLoading(
			long annualizedPremiumWithOccLoading) {
		this.annualizedPremiumWithOccLoading = annualizedPremiumWithOccLoading;
	}
	public long getModalPremium() {
		return modalPremium;
	}
	public void setModalPremium(long modalPremium) {
		this.modalPremium = modalPremium;
	}
	public long getModalPremiumForNSAP() {
		return modalPremiumForNSAP;
	}
	public void setModalPremiumForNSAP(long modalPremiumForNSAP) {
		this.modalPremiumForNSAP = modalPremiumForNSAP;
	}
	public long getAnnualizedPremium() {
		return AnnualizedPremium;
	}
	public void setAnnualizedPremium(long annualizedPremium) {
		AnnualizedPremium = annualizedPremium;
	}
	public long getQuotedModalPremium() {
		return quotedModalPremium;
	}
	public void setQuotedModalPremium(long quotedModalPremium) {
		this.quotedModalPremium = quotedModalPremium;
	}
	public long getModalPremiumForRiders() {
		return modalPremiumForRiders;
	}
	public void setModalPremiumForRiders(long modalPremiumForRiders) {
		this.modalPremiumForRiders = modalPremiumForRiders;
	}
	public long getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(long serviceTax) {
		this.serviceTax = serviceTax;
	}
	public long getTotalModalPremium() {
		return totalModalPremium;
	}
	public void setTotalModalPremium(long totalModalPremium) {
		this.totalModalPremium = totalModalPremium;
	}
	public long getTotalModalPremiumPayble() {
		return totalModalPremiumPayble;
	}
	public void setTotalModalPremiumPayble(long totalModalPremiumPayble) {
		this.totalModalPremiumPayble = totalModalPremiumPayble;
	}
	public long getQuotedAnnualizedPremium() {
		return quotedAnnualizedPremium;
	}
	public void setQuotedAnnualizedPremium(long quotedAnnualizedPremium) {
		this.quotedAnnualizedPremium = quotedAnnualizedPremium;
	}
	public long getTotalPremiumWithServiceTax() {
		return totalPremiumWithServiceTax;
	}
	public void setTotalPremiumWithServiceTax(long totalPremiumWithServiceTax) {
		this.totalPremiumWithServiceTax = totalPremiumWithServiceTax;
	}
	public long getDiscountedBasePrimium() {
		return discountedBasePrimium;
	}
	public void setDiscountedBasePrimium(long discountedBasePrimium) {
		this.discountedBasePrimium = discountedBasePrimium;
	}
	public ArrayList getRiderData() {
		return riderData;
	}
	public void setRiderData(ArrayList riderData) {
		this.riderData = riderData;
	}
	public String getUINNumber() {
		return UINNumber;
	}
	public void setUINNumber(String number) {
		UINNumber = number;
	}
	public HashMap getNonGuaranteedSurrBenefit() {
		return nonGuaranteedSurrBenefit;
	}
	public void setNonGuaranteedSurrBenefit(HashMap nonGuaranteedSurrBenefit) {
		this.nonGuaranteedSurrBenefit = nonGuaranteedSurrBenefit;
	}
	public HashMap getGuaranteedSurrBenefit() {
		return guaranteedSurrBenefit;
	}
	public void setGuaranteedSurrBenefit(HashMap guaranteedSurrBenefit) {
		this.guaranteedSurrBenefit = guaranteedSurrBenefit;
	}
	public HashMap getMaturityVal() {
		return maturityVal;
	}
	public void setMaturityVal(HashMap maturityVal) {
		this.maturityVal = maturityVal;
	}
	public String getPlanDescription() {
		return planDescription;
	}
	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}
	public AddressBO getOAddressBO() {
		return oAddressBO;
	}
	public void setOAddressBO(AddressBO addressBO) {
		oAddressBO = addressBO;
	}
	public String getPlanFeature() {
		return PlanFeature;
	}
	public void setPlanFeature(String planFeature) {
		PlanFeature = planFeature;
	}
	public HashMap getPremiumAllocationCharges() {
		return PremiumAllocationCharges;
	}
	public void setPremiumAllocationCharges(HashMap premiumAllocationCharges) {
		PremiumAllocationCharges = premiumAllocationCharges;
	}
	public HashMap getAmountForInvestment() {
		return amountForInvestment;
	}
	public void setAmountForInvestment(HashMap amountForInvestment) {
		this.amountForInvestment = amountForInvestment;
	}
	public HashMap getOtherBenefitCharges() {
		return otherBenefitCharges;
	}
	public void setOtherBenefitCharges(HashMap otherBenefitCharges) {
		this.otherBenefitCharges = otherBenefitCharges;
	}
	public HashMap getMortalityCharges() {
		return mortalityCharges;
	}
	public void setMortalityCharges(HashMap mortalityCharges) {
		this.mortalityCharges = mortalityCharges;
	}
	public HashMap getPolicyAdminCharges() {
		return policyAdminCharges;
	}
	public void setPolicyAdminCharges(HashMap policyAdminCharges) {
		this.policyAdminCharges = policyAdminCharges;
	}
	public HashMap getApplicableServiceTaxPH10() {
		return ApplicableServiceTaxPH10;
	}
	public void setApplicableServiceTaxPH10(HashMap applicableServiceTaxPH10) {
		ApplicableServiceTaxPH10 = applicableServiceTaxPH10;
	}
	public HashMap getDeathBenifitAtEndOfPolicyYear() {
		return DeathBenifitAtEndOfPolicyYear;
	}
	public void setDeathBenifitAtEndOfPolicyYear(
			HashMap deathBenifitAtEndOfPolicyYear) {
		DeathBenifitAtEndOfPolicyYear = deathBenifitAtEndOfPolicyYear;
	}
	public HashMap getNetPHAmmountforLoyaltyBonus() {
		return netPHAmmountforLoyaltyBonus;
	}
	public void setNetPHAmmountforLoyaltyBonus(HashMap netPHAmmountforLoyaltyBonus) {
		this.netPHAmmountforLoyaltyBonus = netPHAmmountforLoyaltyBonus;
	}
	public HashMap getGuaranteedMaturityAdditionPH10() {
		return GuaranteedMaturityAdditionPH10;
	}
	public void setGuaranteedMaturityAdditionPH10(
			HashMap guaranteedMaturityAdditionPH10) {
		GuaranteedMaturityAdditionPH10 = guaranteedMaturityAdditionPH10;
	}
	public HashMap getSurrenderValuePH10() {
		return SurrenderValuePH10;
	}
	public void setSurrenderValuePH10(HashMap surrenderValuePH10) {
		SurrenderValuePH10 = surrenderValuePH10;
	}
	public HashMap getFundManagmentChargePH10() {
		return fundManagmentChargePH10;
	}
	public void setFundManagmentChargePH10(HashMap fundManagmentChargePH10) {
		this.fundManagmentChargePH10 = fundManagmentChargePH10;
	}
	public HashMap getRegularFundValuePH10() {
		return regularFundValuePH10;
	}
	public void setRegularFundValuePH10(HashMap regularFundValuePH10) {
		this.regularFundValuePH10 = regularFundValuePH10;
	}
	public HashMap getRegularFundValueBeforeFMCPH10() {
		return regularFundValueBeforeFMCPH10;
	}
	public void setRegularFundValueBeforeFMCPH10(
			HashMap regularFundValueBeforeFMCPH10) {
		this.regularFundValueBeforeFMCPH10 = regularFundValueBeforeFMCPH10;
	}
	public HashMap getRegularFundValuePostFMCPH10() {
		return regularFundValuePostFMCPH10;
	}
	public void setRegularFundValuePostFMCPH10(HashMap regularFundValuePostFMCPH10) {
		this.regularFundValuePostFMCPH10 = regularFundValuePostFMCPH10;
	}
	public HashMap getTotalFundValueAtEndPolicyYearPH10() {
		return totalFundValueAtEndPolicyYearPH10;
	}
	public void setTotalFundValueAtEndPolicyYearPH10(
			HashMap totalFundValueAtEndPolicyYearPH10) {
		this.totalFundValueAtEndPolicyYearPH10 = totalFundValueAtEndPolicyYearPH10;
	}
	public HashMap getApplicableServiceTaxPH6() {
		return ApplicableServiceTaxPH6;
	}
	public void setApplicableServiceTaxPH6(HashMap applicableServiceTaxPH6) {
		ApplicableServiceTaxPH6 = applicableServiceTaxPH6;
	}
	public HashMap getGuaranteedMaturityAdditionPH6() {
		return GuaranteedMaturityAdditionPH6;
	}
	public void setGuaranteedMaturityAdditionPH6(
			HashMap guaranteedMaturityAdditionPH6) {
		GuaranteedMaturityAdditionPH6 = guaranteedMaturityAdditionPH6;
	}
	public HashMap getSurrenderValuePH6() {
		return SurrenderValuePH6;
	}
	public void setSurrenderValuePH6(HashMap surrenderValuePH6) {
		SurrenderValuePH6 = surrenderValuePH6;
	}
	public HashMap getFundManagmentChargePH6() {
		return fundManagmentChargePH6;
	}
	public void setFundManagmentChargePH6(HashMap fundManagmentChargePH6) {
		this.fundManagmentChargePH6 = fundManagmentChargePH6;
	}
	public HashMap getRegularFundValuePH6() {
		return regularFundValuePH6;
	}
	public void setRegularFundValuePH6(HashMap regularFundValuePH6) {
		this.regularFundValuePH6 = regularFundValuePH6;
	}
	public HashMap getRegularFundValueBeforeFMCPH6() {
		return regularFundValueBeforeFMCPH6;
	}
	public void setRegularFundValueBeforeFMCPH6(HashMap regularFundValueBeforeFMCPH6) {
		this.regularFundValueBeforeFMCPH6 = regularFundValueBeforeFMCPH6;
	}
	public HashMap getTotalFundValueAtEndPolicyYearPH6() {
		return totalFundValueAtEndPolicyYearPH6;
	}
	public void setTotalFundValueAtEndPolicyYearPH6(HashMap totalFundValueAtEndPolicyYearPH6) {
		this.totalFundValueAtEndPolicyYearPH6 = totalFundValueAtEndPolicyYearPH6;
	}
	public double getFmc() {
		return fmc;
	}
	public void setFmc(double fmc) {
		this.fmc = fmc;
	}
	public long getSTRiderModalPremium() {
		return STRiderModalPremium;
	}
	public void setSTRiderModalPremium(long riderModalPremium) {
		STRiderModalPremium = riderModalPremium;
	}
	public long getTotalRiderModalPremium() {
		return totalRiderModalPremium;
	}
	public void setTotalRiderModalPremium(long totalRiderModalPremium) {
		this.totalRiderModalPremium = totalRiderModalPremium;
	}
	public double getPremiumMultiple() {
		return premiumMultiple;
	}
	public void setPremiumMultiple(double premiumMultiple) {
		this.premiumMultiple = premiumMultiple;
	}
	public long getMaturityBenefit10() {
		return maturityBenefit10;
	}
	public void setMaturityBenefit10(long maturityBenefit10) {
		this.maturityBenefit10 = maturityBenefit10;
	}
	public long getMaturityBenefit6() {
		return maturityBenefit6;
	}
	public void setMaturityBenefit6(long maturityBenefit6) {
		this.maturityBenefit6 = maturityBenefit6;
	}
	public HashMap getLifeCoverage() {
		return lifeCoverage;
	}
	public void setLifeCoverage(HashMap lifeCoverage) {
		this.lifeCoverage = lifeCoverage;
	}
	public HashMap getGuranteedAnnualIncome() {
		return guranteedAnnualIncome;
	}
	public void setGuranteedAnnualIncome(HashMap guranteedAnnualIncome) {
		this.guranteedAnnualIncome = guranteedAnnualIncome;
	}
	public HashMap getGuranteedSurrenderValue() {
		return guranteedSurrenderValue;
	}
	public void setGuranteedSurrenderValue(HashMap guranteedSurrenderValue) {
		this.guranteedSurrenderValue = guranteedSurrenderValue;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public HashMap getCommissionPayble() {
		return commissionPayble;
	}
	public void setCommissionPayble(HashMap commissionPayble) {
		this.commissionPayble = commissionPayble;
	}
	public HashMap getTotalPremiumAnnual() {
		return TotalPremiumAnnual;
	}
	public void setTotalPremiumAnnual(HashMap totalPremiumAnnual) {
		TotalPremiumAnnual = totalPremiumAnnual;
	}
	public HashMap getTotalMaturityValue() {
		return totalMaturityValue;
	}
	public void setTotalMaturityValue(HashMap totalMaturityValue) {
		this.totalMaturityValue = totalMaturityValue;
	}
	public long getTotalMaturityValuePlusGAI() {
		return TotalMaturityValuePlusGAI;
	}
	public void setTotalMaturityValuePlusGAI(long totalMaturityValuePlusGAI) {
		TotalMaturityValuePlusGAI = totalMaturityValuePlusGAI;
	}
	public HashMap getVSRBonus4() {
		return VSRBonus4;
	}
	public void setVSRBonus4(HashMap bonus4) {
		VSRBonus4 = bonus4;
	}
	public HashMap getTerminalBonus4() {
		return TerminalBonus4;
	}
	public void setTerminalBonus4(HashMap terminalBonus4) {
		TerminalBonus4 = terminalBonus4;
	}
	public HashMap getVSRBonus8() {
		return VSRBonus8;
	}
	public void setVSRBonus8(HashMap bonus8) {
		VSRBonus8 = bonus8;
	}
	public HashMap getTerminalBonus8() {
		return TerminalBonus8;
	}
	public void setTerminalBonus8(HashMap terminalBonus8) {
		TerminalBonus8 = terminalBonus8;
	}
	public HashMap getTotalDeathBenefit4() {
		return TotalDeathBenefit4;
	}
	public void setTotalDeathBenefit4(HashMap totalDeathBenefit4) {
		TotalDeathBenefit4 = totalDeathBenefit4;
	}
	/** Vikas - Added for Mahalife Gold */
	public HashMap getTotalDeathBenefit() {
		return TotalDeathBenefit;
	}
	public void setTotalDeathBenefit(HashMap totalDeathBenefit) {
		TotalDeathBenefit = totalDeathBenefit;
	}
	public long getTotalcashdividend4() {
		return totalcashdividend4;
	}
	public void setTotalcashdividend4(long totalcashdividend4) {
		this.totalcashdividend4 = totalcashdividend4;
	}
	public long getTotalcashdividend8() {
		return totalcashdividend8;
	}
	public void setTotalcashdividend8(long totalcashdividend8) {
		this.totalcashdividend8 = totalcashdividend8;
	}
	public long getTotalGAnnualCoupon() {
		return totalGAnnualCoupon;
	}
	public void setTotalGAnnualCoupon(long totalGAnnualCoupon) {
		this.totalGAnnualCoupon = totalGAnnualCoupon;
	}
	/** Vikas - Added for Mahalife Gold*/
	public HashMap getTotalDeathBenefit8() {
		return TotalDeathBenefit8;
	}
	public void setTotalDeathBenefit8(HashMap totalDeathBenefit8) {
		TotalDeathBenefit8 = totalDeathBenefit8;
	}
	public HashMap getNonGuaranteedSurrBenefit_8() {
		return nonGuaranteedSurrBenefit_8;
	}
	public void setNonGuaranteedSurrBenefit_8(HashMap nonGuaranteedSurrBenefit_8) {
		this.nonGuaranteedSurrBenefit_8 = nonGuaranteedSurrBenefit_8;
	}
	public HashMap getGuranteedAnnualInflationCover() {
		return guranteedAnnualInflationCover;
	}
	public void setGuranteedAnnualInflationCover(
			HashMap guranteedAnnualInflationCover) {
		this.guranteedAnnualInflationCover = guranteedAnnualInflationCover;
	}

	public HashMap getMortalityChargesPH6() {
		return mortalityChargesPH6;
	}
	public void setMortalityChargesPH6(HashMap mortalityChargesPH6) {
		this.mortalityChargesPH6 = mortalityChargesPH6;
	}
	public HashMap getDeathBenifitAtEndOfPolicyYearPH6() {
		return DeathBenifitAtEndOfPolicyYearPH6;
	}
	public void setDeathBenifitAtEndOfPolicyYearPH6(
			HashMap deathBenifitAtEndOfPolicyYearPH6) {
		DeathBenifitAtEndOfPolicyYearPH6 = deathBenifitAtEndOfPolicyYearPH6;
	}
	public long getModalPrem() {
		return modalPrem;
	}
	public void setModalPrem(long modalPrem) {
		this.modalPrem = modalPrem;
	}
	public HashMap getTotalchargeforPH10() {
		return totalchargeforPH10;
	}
	public void setTotalchargeforPH10(HashMap totalchargeforPH10) {
		this.totalchargeforPH10 = totalchargeforPH10;
	}

	public HashMap getLoyaltyChargeforPH10() {
		return loyaltyChargeforPH10;
	}
	public void setLoyaltyChargeforPH10(HashMap loyaltyChargeforPH10) {
		this.loyaltyChargeforPH10 = loyaltyChargeforPH10;
	}
	public HashMap getTotalchargeforPH6() {
		return TotalchargeforPH6;
	}
	public void setTotalchargeforPH6(HashMap totalchargeforPH6) {
		TotalchargeforPH6 = totalchargeforPH6;
	}
	public HashMap getRegularFundValuePostFMCPH6() {
		return regularFundValuePostFMCPH6;
	}
	public void setRegularFundValuePostFMCPH6(HashMap regularFundValuePostFMCPH6) {
		this.regularFundValuePostFMCPH6 = regularFundValuePostFMCPH6;
	}
	public HashMap getLoyaltyChargeforPH6() {
		return loyaltyChargeforPH6;
	}
	public void setLoyaltyChargeforPH6(HashMap loyaltyChargeforPH6) {
		this.loyaltyChargeforPH6 = loyaltyChargeforPH6;
	}
	public HashMap getTotalFundValueAtEndPolicyYearPH10_AS3() {
		return totalFundValueAtEndPolicyYearPH10_AS3;
	}
	public void setTotalFundValueAtEndPolicyYearPH10_AS3(
			HashMap totalFundValueAtEndPolicyYearPH10_AS3) {
		this.totalFundValueAtEndPolicyYearPH10_AS3 = totalFundValueAtEndPolicyYearPH10_AS3;
	}
	public HashMap getSurrenderValuePH10_AS3() {
		return SurrenderValuePH10_AS3;
	}
	public void setSurrenderValuePH10_AS3(HashMap surrenderValuePH10_AS3) {
		SurrenderValuePH10_AS3 = surrenderValuePH10_AS3;
	}
	public HashMap getTotalDeathBenefitPH10_AS3() {
		return TotalDeathBenefitPH10_AS3;
	}
	public void setTotalDeathBenefitPH10_AS3(HashMap totalDeathBenefitPH10_AS3) {
		TotalDeathBenefitPH10_AS3 = totalDeathBenefitPH10_AS3;
	}
	public HashMap getMortalityCharges_AS3() {
		return MortalityCharges_AS3;
	}
	public void setMortalityCharges_AS3(HashMap mortalityCharges_AS3) {
		MortalityCharges_AS3 = mortalityCharges_AS3;
	}
	public HashMap getApplicableServiceTaxPH10_AS3() {
		return ApplicableServiceTaxPH10_AS3;
	}
	public void setApplicableServiceTaxPH10_AS3(HashMap applicableServiceTaxPH10_AS3) {
		ApplicableServiceTaxPH10_AS3 = applicableServiceTaxPH10_AS3;
	}
	public HashMap getTotalchargeforPH10_AS3() {
		return TotalchargeforPH10_AS3;
	}
	public void setTotalchargeforPH10_AS3(HashMap totalchargeforPH10_AS3) {
		TotalchargeforPH10_AS3 = totalchargeforPH10_AS3;
	}
	public HashMap getLoyaltyChargeforPH10_AS3() {
		return LoyaltyChargeforPH10_AS3;
	}
	public void setLoyaltyChargeforPH10_AS3(HashMap loyaltyChargeforPH10_AS3) {
		LoyaltyChargeforPH10_AS3 = loyaltyChargeforPH10_AS3;
	}
	public HashMap getFundManagmentChargePH10_AS3() {
		return FundManagmentChargePH10_AS3;
	}
	public void setFundManagmentChargePH10_AS3(HashMap fundManagmentChargePH10_AS3) {
		FundManagmentChargePH10_AS3 = fundManagmentChargePH10_AS3;
	}
	public HashMap getRegularFundValuePH10_AS3() {
		return RegularFundValuePH10_AS3;
	}
	public void setRegularFundValuePH10_AS3(HashMap regularFundValuePH10_AS3) {
		RegularFundValuePH10_AS3 = regularFundValuePH10_AS3;
	}
	public HashMap getRegularFundValueBeforeFMCPH10_AS3() {
		return RegularFundValueBeforeFMCPH10_AS3;
	}
	public void setRegularFundValueBeforeFMCPH10_AS3(
			HashMap regularFundValueBeforeFMCPH10_AS3) {
		RegularFundValueBeforeFMCPH10_AS3 = regularFundValueBeforeFMCPH10_AS3;
	}
	public HashMap getRegularFundValuePostFMCPH10_AS3() {
		return RegularFundValuePostFMCPH10_AS3;
	}
	public void setRegularFundValuePostFMCPH10_AS3(
			HashMap regularFundValuePostFMCPH10_AS3) {
		RegularFundValuePostFMCPH10_AS3 = regularFundValuePostFMCPH10_AS3;
	}
	public HashMap getGuaranteedMaturityAdditionPH10_AS3() {
		return GuaranteedMaturityAdditionPH10_AS3;
	}
	public void setGuaranteedMaturityAdditionPH10_AS3(
			HashMap guaranteedMaturityAdditionPH10_AS3) {
		GuaranteedMaturityAdditionPH10_AS3 = guaranteedMaturityAdditionPH10_AS3;
	}
	public long getRenewalPremium_A() {
		return renewalPremium_A;
	}
	public void setRenewalPremium_A(long renewalPremium_A) {
		this.renewalPremium_A = renewalPremium_A;
	}
	public long getRenewalPremium_SA() {
		return renewalPremium_SA;
	}
	public void setRenewalPremium_SA(long renewalPremium_SA) {
		this.renewalPremium_SA = renewalPremium_SA;
	}
	public long getRenewalPremium_Q() {
		return renewalPremium_Q;
	}
	public void setRenewalPremium_Q(long renewalPremium_Q) {
		this.renewalPremium_Q = renewalPremium_Q;
	}
	public long getRenewalPremium_M() {
		return renewalPremium_M;
	}
	public void setRenewalPremium_M(long renewalPremium_M) {
		this.renewalPremium_M = renewalPremium_M;
	}
	public HashMap getLifeCoverageAltSce() {
		return lifeCoverageAltSce;
	}
	public void setLifeCoverageAltSce(HashMap lifeCoverageAltSce) {
		this.lifeCoverageAltSce = lifeCoverageAltSce;
	}
	public HashMap getAltScenarioMaturityVal() {
		return altScenarioMaturityVal;
	}
	public void setAltScenarioMaturityVal(HashMap altScenarioMaturityVal) {
		this.altScenarioMaturityVal = altScenarioMaturityVal;
	}
	public HashMap getAltScenarioTotalDeathBenefit4() {
		return altScenarioTotalDeathBenefit4;
	}
	public void setAltScenarioTotalDeathBenefit4(
			HashMap altScenarioTotalDeathBenefit4) {
		this.altScenarioTotalDeathBenefit4 = altScenarioTotalDeathBenefit4;
	}
	public HashMap getAltScenarioTotalDeathBenefit8() {
		return altScenarioTotalDeathBenefit8;
	}
	public void setAltScenarioTotalDeathBenefit8(
			HashMap altScenarioTotalDeathBenefit8) {
		this.altScenarioTotalDeathBenefit8 = altScenarioTotalDeathBenefit8;
	}
	public HashMap getAltScenarioMaturityVal4() {
		return altScenarioMaturityVal4;
	}
	public void setAltScenarioMaturityVal4(HashMap altScenarioMaturityVal4) {
		this.altScenarioMaturityVal4 = altScenarioMaturityVal4;
	}
	public HashMap getAltScenarioMaturityVal8() {
		return altScenarioMaturityVal8;
	}
	public void setAltScenarioMaturityVal8(HashMap altScenarioMaturityVal8) {
		this.altScenarioMaturityVal8 = altScenarioMaturityVal8;
	}
	public HashMap getAltScenarioSurrBenefitCurrentRate() {
		return altScenarioSurrBenefitCurrentRate;
	}
	public void setAltScenarioSurrBenefitCurrentRate(
			HashMap altScenarioSurrBenefitCurrentRate) {
		this.altScenarioSurrBenefitCurrentRate = altScenarioSurrBenefitCurrentRate;
	}
	public HashMap getAltScenarioPremPaidCumulativeCurrentRate() {
		return altScenarioPremPaidCumulativeCurrentRate;
	}
	public void setAltScenarioPremPaidCumulativeCurrentRate(
			HashMap altScenarioPremPaidCumulativeCurrentRate) {
		this.altScenarioPremPaidCumulativeCurrentRate = altScenarioPremPaidCumulativeCurrentRate;
	}
	public HashMap getAltScenarioTotMatBenCurrentRate() {
		return altScenarioTotMatBenCurrentRate;
	}
	public void setAltScenarioTotMatBenCurrentRate(
			HashMap altScenarioTotMatBenCurrentRate) {
		this.altScenarioTotMatBenCurrentRate = altScenarioTotMatBenCurrentRate;
	}
	public HashMap getLoyaltyChargeforFP() {
		return loyaltyChargeforFP;
	}
	public void setLoyaltyChargeforFP(HashMap loyaltyChargeforFP) {
		this.loyaltyChargeforFP = loyaltyChargeforFP;
	}
	public HashMap getSurrenderValueFP() {
		return SurrenderValueFP;
	}
	public void setSurrenderValueFP(HashMap surrenderValueFP) {
		SurrenderValueFP = surrenderValueFP;
	}
	public HashMap getDeathBenifitAtEndOfPolicyYearFP() {
		return DeathBenifitAtEndOfPolicyYearFP;
	}
	public void setDeathBenifitAtEndOfPolicyYearFP(
			HashMap deathBenifitAtEndOfPolicyYearFP) {
		DeathBenifitAtEndOfPolicyYearFP = deathBenifitAtEndOfPolicyYearFP;
	}
	public HashMap getRegularFundValueFP() {
		return regularFundValueFP;
	}
	public void setRegularFundValueFP(HashMap regularFundValueFP) {
		this.regularFundValueFP = regularFundValueFP;
	}
	public HashMap getAltScenarioTaxSaving() {
		return altScenarioTaxSaving;
	}
	public void setAltScenarioTaxSaving(HashMap altScenarioTaxSaving) {
		this.altScenarioTaxSaving = altScenarioTaxSaving;
	}
	public HashMap getAltScenarioannualizedeffectivepremium() {
		return altScenarioannualizedeffectivepremium;
	}
	public void setAltScenarioannualizedeffectivepremium(
			HashMap altScenarioannualizedeffectivepremium) {
		this.altScenarioannualizedeffectivepremium = altScenarioannualizedeffectivepremium;
	}
	public HashMap getAltScenariocummulativeeffectivepremium() {
		return altScenariocummulativeeffectivepremium;
	}
	public void setAltScenariocummulativeeffectivepremium(
			HashMap altScenariocummulativeeffectivepremium) {
		this.altScenariocummulativeeffectivepremium = altScenariocummulativeeffectivepremium;
	}
	public HashMap getAltScenario4MaturityBenefitTax() {
		return altScenario4MaturityBenefitTax;
	}
	public void setAltScenario4MaturityBenefitTax(
			HashMap altScenario4MaturityBenefitTax) {
		this.altScenario4MaturityBenefitTax = altScenario4MaturityBenefitTax;
	}
	public HashMap getTopUpPremium() {
		return topUpPremium;
	}
	public void setTopUpPremium(HashMap topUpPremium) {
		this.topUpPremium = topUpPremium;
	}
	public HashMap getWithdrawalAmout() {
		return withdrawalAmout;
	}
	public void setWithdrawalAmout(HashMap withdrawalAmout) {
		this.withdrawalAmout = withdrawalAmout;
	}
	public HashMap getRegularFundValuePostFMC() {
		return regularFundValuePostFMC;
	}
	public void setRegularFundValuePostFMC(HashMap regularFundValuePostFMC) {
		this.regularFundValuePostFMC = regularFundValuePostFMC;
	}
	public HashMap getTopUpLoyalty() {
		return topUpLoyalty;
	}
	public void setTopUpLoyalty(HashMap topUpLoyalty) {
		this.topUpLoyalty = topUpLoyalty;
	}
	public HashMap getTopUpRegularFundValueEndOfYear() {
		return topUpRegularFundValueEndOfYear;
	}
	public void setTopUpRegularFundValueEndOfYear(
			HashMap topUpRegularFundValueEndOfYear) {
		this.topUpRegularFundValueEndOfYear = topUpRegularFundValueEndOfYear;
	}
	public HashMap getTopUpSurrValue() {
		return topUpSurrValue;
	}
	public void setTopUpSurrValue(HashMap topUpSurrValue) {
		this.topUpSurrValue = topUpSurrValue;
	}
	public HashMap getTopUpDeathBenefit() {
		return topUpDeathBenefit;
	}
	public void setTopUpDeathBenefit(HashMap topUpDeathBenefit) {
		this.topUpDeathBenefit = topUpDeathBenefit;
	}
	public HashMap getTotalFundValueAtEndPolicyYearPH6_AS3() {
		return totalFundValueAtEndPolicyYearPH6_AS3;
	}
	public void setTotalFundValueAtEndPolicyYearPH6_AS3(
			HashMap totalFundValueAtEndPolicyYearPH6_AS3) {
		this.totalFundValueAtEndPolicyYearPH6_AS3 = totalFundValueAtEndPolicyYearPH6_AS3;
	}
	public HashMap getSurrenderValuePH6_AS3() {
		return SurrenderValuePH6_AS3;
	}
	public void setSurrenderValuePH6_AS3(HashMap surrenderValuePH6_AS3) {
		SurrenderValuePH6_AS3 = surrenderValuePH6_AS3;
	}
	public HashMap getTotalDeathBenefitPH6_AS3() {
		return TotalDeathBenefitPH6_AS3;
	}
	public void setTotalDeathBenefitPH6_AS3(HashMap totalDeathBenefitPH6_AS3) {
		TotalDeathBenefitPH6_AS3 = totalDeathBenefitPH6_AS3;
	}
	public HashMap getMortalityChargesPH6_AS3() {
		return MortalityChargesPH6_AS3;
	}
	public void setMortalityChargesPH6_AS3(HashMap mortalityChargesPH6_AS3) {
		MortalityChargesPH6_AS3 = mortalityChargesPH6_AS3;
	}
	public HashMap getApplicableServiceTaxPH6_AS3() {
		return ApplicableServiceTaxPH6_AS3;
	}
	public void setApplicableServiceTaxPH6_AS3(HashMap applicableServiceTaxPH6_AS3) {
		ApplicableServiceTaxPH6_AS3 = applicableServiceTaxPH6_AS3;
	}
	public HashMap getTotalchargeforPH6_AS3() {
		return TotalchargeforPH6_AS3;
	}
	public void setTotalchargeforPH6_AS3(HashMap totalchargeforPH6_AS3) {
		TotalchargeforPH6_AS3 = totalchargeforPH6_AS3;
	}
	public HashMap getLoyaltyChargeforPH6_AS3() {
		return LoyaltyChargeforPH6_AS3;
	}
	public void setLoyaltyChargeforPH6_AS3(HashMap loyaltyChargeforPH6_AS3) {
		LoyaltyChargeforPH6_AS3 = loyaltyChargeforPH6_AS3;
	}
	public HashMap getFundManagmentChargePH6_AS3() {
		return FundManagmentChargePH6_AS3;
	}
	public void setFundManagmentChargePH6_AS3(HashMap fundManagmentChargePH6_AS3) {
		FundManagmentChargePH6_AS3 = fundManagmentChargePH6_AS3;
	}
	public HashMap getRegularFundValuePH6_AS3() {
		return RegularFundValuePH6_AS3;
	}
	public void setRegularFundValuePH6_AS3(HashMap regularFundValuePH6_AS3) {
		RegularFundValuePH6_AS3 = regularFundValuePH6_AS3;
	}
	public HashMap getRegularFundValueBeforeFMCPH6_AS3() {
		return RegularFundValueBeforeFMCPH6_AS3;
	}
	public void setRegularFundValueBeforeFMCPH6_AS3(
			HashMap regularFundValueBeforeFMCPH6_AS3) {
		RegularFundValueBeforeFMCPH6_AS3 = regularFundValueBeforeFMCPH6_AS3;
	}
	public HashMap getRegularFundValuePostFMCPH6_AS3() {
		return RegularFundValuePostFMCPH6_AS3;
	}
	public void setRegularFundValuePostFMCPH6_AS3(
			HashMap regularFundValuePostFMCPH6_AS3) {
		RegularFundValuePostFMCPH6_AS3 = regularFundValuePostFMCPH6_AS3;
	}
	public HashMap getGuaranteedMaturityAdditionPH6_AS3() {
		return GuaranteedMaturityAdditionPH6_AS3;
	}
	public void setGuaranteedMaturityAdditionPH6_AS3(
			HashMap guaranteedMaturityAdditionPH6_AS3) {
		GuaranteedMaturityAdditionPH6_AS3 = guaranteedMaturityAdditionPH6_AS3;
	}
	public HashMap getMortalityCharges_SMART10() {
		return mortalityCharges_SMART10;
	}
	public void setMortalityCharges_SMART10(HashMap mortalityCharges_SMART10) {
		this.mortalityCharges_SMART10 = mortalityCharges_SMART10;
	}
	public HashMap getApplicableServiceTaxPH10_SMART10() {
		return applicableServiceTaxPH10_SMART10;
	}
	public void setApplicableServiceTaxPH10_SMART10(
			HashMap applicableServiceTaxPH10_SMART10) {
		this.applicableServiceTaxPH10_SMART10 = applicableServiceTaxPH10_SMART10;
	}
	public HashMap getTotalchargeforPH10_SMART10() {
		return totalchargeforPH10_SMART10;
	}
	public void setTotalchargeforPH10_SMART10(HashMap totalchargeforPH10_SMART10) {
		this.totalchargeforPH10_SMART10 = totalchargeforPH10_SMART10;
	}
	public HashMap getLoyaltyChargeforPH10_SMART10() {
		return loyaltyChargeforPH10_SMART10;
	}
	public void setLoyaltyChargeforPH10_SMART10(HashMap loyaltyChargeforPH10_SMART10) {
		this.loyaltyChargeforPH10_SMART10 = loyaltyChargeforPH10_SMART10;
	}
	public HashMap getFundManagmentChargePH10_SMART10() {
		return fundManagmentChargePH10_SMART10;
	}
	public void setFundManagmentChargePH10_SMART10(
			HashMap fundManagmentChargePH10_SMART10) {
		this.fundManagmentChargePH10_SMART10 = fundManagmentChargePH10_SMART10;
	}
	public HashMap getRegularFundValuePH10_SMART10() {
		return regularFundValuePH10_SMART10;
	}
	public void setRegularFundValuePH10_SMART10(HashMap regularFundValuePH10_SMART10) {
		this.regularFundValuePH10_SMART10 = regularFundValuePH10_SMART10;
	}
	public HashMap getRegularFundValueBeforeFMCPH10_SMART10() {
		return regularFundValueBeforeFMCPH10_SMART10;
	}
	public void setRegularFundValueBeforeFMCPH10_SMART10(
			HashMap regularFundValueBeforeFMCPH10_SMART10) {
		this.regularFundValueBeforeFMCPH10_SMART10 = regularFundValueBeforeFMCPH10_SMART10;
	}
	public HashMap getRegularFundValuePostFMCPH10_SMART10() {
		return regularFundValuePostFMCPH10_SMART10;
	}
	public void setRegularFundValuePostFMCPH10_SMART10(
			HashMap regularFundValuePostFMCPH10_SMART10) {
		this.regularFundValuePostFMCPH10_SMART10 = regularFundValuePostFMCPH10_SMART10;
	}
	public HashMap getGuaranteedMaturityAdditionPH10_SMART10() {
		return guaranteedMaturityAdditionPH10_SMART10;
	}
	public void setGuaranteedMaturityAdditionPH10_SMART10(
			HashMap guaranteedMaturityAdditionPH10_SMART10) {
		this.guaranteedMaturityAdditionPH10_SMART10 = guaranteedMaturityAdditionPH10_SMART10;
	}
	public HashMap getTotalDeathBenefitPH10_SMART10() {
		return totalDeathBenefitPH10_SMART10;
	}
	public void setTotalDeathBenefitPH10_SMART10(
			HashMap totalDeathBenefitPH10_SMART10) {
		this.totalDeathBenefitPH10_SMART10 = totalDeathBenefitPH10_SMART10;
	}
	public HashMap getSurrenderValuePH10_SMART10() {
		return surrenderValuePH10_SMART10;
	}
	public void setSurrenderValuePH10_SMART10(HashMap surrenderValuePH10_SMART10) {
		this.surrenderValuePH10_SMART10 = surrenderValuePH10_SMART10;
	}
	public HashMap getMortalityCharges_SMART6() {
		return mortalityCharges_SMART6;
	}
	public void setMortalityCharges_SMART6(HashMap mortalityCharges_SMART6) {
		this.mortalityCharges_SMART6 = mortalityCharges_SMART6;
	}
	public HashMap getApplicableServiceTaxPH10_SMART6() {
		return applicableServiceTaxPH10_SMART6;
	}
	public void setApplicableServiceTaxPH10_SMART6(
			HashMap applicableServiceTaxPH10_SMART6) {
		this.applicableServiceTaxPH10_SMART6 = applicableServiceTaxPH10_SMART6;
	}
	public HashMap getTotalchargeforPH10_SMART6() {
		return totalchargeforPH10_SMART6;
	}
	public void setTotalchargeforPH10_SMART6(HashMap totalchargeforPH10_SMART6) {
		this.totalchargeforPH10_SMART6 = totalchargeforPH10_SMART6;
	}
	public HashMap getLoyaltyChargeforPH10_SMART6() {
		return loyaltyChargeforPH10_SMART6;
	}
	public void setLoyaltyChargeforPH10_SMART6(HashMap loyaltyChargeforPH10_SMART6) {
		this.loyaltyChargeforPH10_SMART6 = loyaltyChargeforPH10_SMART6;
	}
	public HashMap getFundManagmentChargePH10_SMART6() {
		return fundManagmentChargePH10_SMART6;
	}
	public void setFundManagmentChargePH10_SMART6(
			HashMap fundManagmentChargePH10_SMART6) {
		this.fundManagmentChargePH10_SMART6 = fundManagmentChargePH10_SMART6;
	}
	public HashMap getRegularFundValuePH10_SMART6() {
		return regularFundValuePH10_SMART6;
	}
	public void setRegularFundValuePH10_SMART6(HashMap regularFundValuePH10_SMART6) {
		this.regularFundValuePH10_SMART6 = regularFundValuePH10_SMART6;
	}
	public HashMap getRegularFundValueBeforeFMCPH10_SMART6() {
		return regularFundValueBeforeFMCPH10_SMART6;
	}
	public void setRegularFundValueBeforeFMCPH10_SMART6(
			HashMap regularFundValueBeforeFMCPH10_SMART6) {
		this.regularFundValueBeforeFMCPH10_SMART6 = regularFundValueBeforeFMCPH10_SMART6;
	}
	public HashMap getRegularFundValuePostFMCPH10_SMART6() {
		return regularFundValuePostFMCPH10_SMART6;
	}
	public void setRegularFundValuePostFMCPH10_SMART6(
			HashMap regularFundValuePostFMCPH10_SMART6) {
		this.regularFundValuePostFMCPH10_SMART6 = regularFundValuePostFMCPH10_SMART6;
	}
	public HashMap getGuaranteedMaturityAdditionPH10_SMART6() {
		return guaranteedMaturityAdditionPH10_SMART6;
	}
	public void setGuaranteedMaturityAdditionPH10_SMART6(
			HashMap guaranteedMaturityAdditionPH10_SMART6) {
		this.guaranteedMaturityAdditionPH10_SMART6 = guaranteedMaturityAdditionPH10_SMART6;
	}
	public HashMap getTotalDeathBenefitPH10_SMART6() {
		return totalDeathBenefitPH10_SMART6;
	}
	public void setTotalDeathBenefitPH10_SMART6(HashMap totalDeathBenefitPH10_SMART6) {
		this.totalDeathBenefitPH10_SMART6 = totalDeathBenefitPH10_SMART6;
	}
	public HashMap getSurrenderValuePH10_SMART6() {
		return surrenderValuePH10_SMART6;
	}
	public void setSurrenderValuePH10_SMART6(HashMap surrenderValuePH10_SMART6) {
		this.surrenderValuePH10_SMART6 = surrenderValuePH10_SMART6;
	}
	public HashMap getRegularFundValueAAA_LCEF_PH10() {
		return RegularFundValueAAA_LCEF_PH10;
	}
	public void setRegularFundValueAAA_LCEF_PH10(
			HashMap regularFundValueAAA_LCEF_PH10) {
		RegularFundValueAAA_LCEF_PH10 = regularFundValueAAA_LCEF_PH10;
	}
	public HashMap getRegularFundValueAAA_WLEF_PH10() {
		return RegularFundValueAAA_WLEF_PH10;
	}
	public void setRegularFundValueAAA_WLEF_PH10(
			HashMap regularFundValueAAA_WLEF_PH10) {
		RegularFundValueAAA_WLEF_PH10 = regularFundValueAAA_WLEF_PH10;
	}
	public HashMap getRegularFundValuePH6_LCEF_AS3() {
		return RegularFundValuePH6_LCEF_AS3;
	}
	public void setRegularFundValuePH6_LCEF_AS3(HashMap regularFundValuePH6_LCEF_AS3) {
		RegularFundValuePH6_LCEF_AS3 = regularFundValuePH6_LCEF_AS3;
	}
	public HashMap getRegularFundValuePH6_WLEF_AS3() {
		return RegularFundValuePH6_WLEF_AS3;
	}
	public void setRegularFundValuePH6_WLEF_AS3(HashMap regularFundValuePH6_WLEF_AS3) {
		RegularFundValuePH6_WLEF_AS3 = regularFundValuePH6_WLEF_AS3;
	}
	public HashMap getTotalRiderAnnualPremium() {
		return totalRiderAnnualPremium;
	}
	public void setTotalRiderAnnualPremium(HashMap totalRiderAnnualPremium) {
		this.totalRiderAnnualPremium = totalRiderAnnualPremium;
	}
	public double getInitDebtFundFactor() {
		return initDebtFundFactor;
	}
	public void setInitDebtFundFactor(double initDebtFundFactor) {
		this.initDebtFundFactor = initDebtFundFactor;
	}
	public double getInitEquityFundFactor() {
		return initEquityFundFactor;
	}
	public void setInitEquityFundFactor(double initEquityFundFactor) {
		this.initEquityFundFactor = initEquityFundFactor;
	}
	public String getAnnualPremium() {
		return annualPremium;
	}
	public void setAnnualPremium(String annualPremium) {
		this.annualPremium = annualPremium;
	}
	public String getAltHeader() {
		return altHeader;
	}
	public void setAltHeader(String altHeader) {
		this.altHeader = altHeader;
	}
	public String getIllusPageNo() {
		return illusPageNo;
	}
	public void setIllusPageNo(String illusPageNo) {
		this.illusPageNo = illusPageNo;
	}
	public double getNetYield8P() {
		return netYield8P;
	}
	public void setNetYield8P(double netYield8P) {
		this.netYield8P = netYield8P;
	}
	public double getNetYield8TW() {
		return netYield8TW;
	}
	public void setNetYield8TW(double netYield8TW) {
		this.netYield8TW = netYield8TW;
	}
	public double getNetYield8SMART() {
		return netYield8SMART;
	}
	public void setNetYield8SMART(double netYield8SMART) {
		this.netYield8SMART = netYield8SMART;
	}
	public HashMap getLifeCoverageAlternateSce() {
		return LifeCoverageAlternateSce;
	}
	public void setLifeCoverageAlternateSce(HashMap lifeCoverageAlternateSce) {
		LifeCoverageAlternateSce = lifeCoverageAlternateSce;
	}
	public HashMap getALTSCEGAI() {
		return ALTSCEGAI;
	}
	public void setALTSCEGAI(HashMap altscegai) {
		ALTSCEGAI = altscegai;
	}
	public HashMap getMaturityValALTSCE() {
		return MaturityValALTSCE;
	}
	public void setMaturityValALTSCE(HashMap maturityValALTSCE) {
		MaturityValALTSCE = maturityValALTSCE;
	}
	public HashMap getTOTaltScenarioMaturityVal() {
		return TOTaltScenarioMaturityVal;
	}
	public void setTOTaltScenarioMaturityVal(HashMap taltScenarioMaturityVal) {
		TOTaltScenarioMaturityVal = taltScenarioMaturityVal;
	}
	public HashMap getALTguranteedSurrenderValue() {
		return ALTguranteedSurrenderValue;
	}
	public void setALTguranteedSurrenderValue(HashMap tguranteedSurrenderValue) {
		ALTguranteedSurrenderValue = tguranteedSurrenderValue;
	}
	public HashMap getALTSCESurrBenefit() {
		return ALTSCESurrBenefit;
	}
	public void setALTSCESurrBenefit(HashMap surrBenefit) {
		ALTSCESurrBenefit = surrBenefit;
	}
	public long getRenewalModalPremium() {
		return renewalModalPremium;
	}
	public void setRenewalModalPremium(long renewalModalPremium) {
		this.renewalModalPremium = renewalModalPremium;
	}
	public long getTotalRenewalModalPremium() {
		return totalRenewalModalPremium;
	}
	public void setTotalRenewalModalPremium(long totalRenewalModalPremium) {
		this.totalRenewalModalPremium = totalRenewalModalPremium;
	}
	public long getModalPremiumRenewal() {
		return ModalPremiumRenewal;
	}
	public void setModalPremiumRenewal(long modalPremiumRenewal) {
		ModalPremiumRenewal = modalPremiumRenewal;
	}
	public HashMap getAltVSRBonus4() {
		return AltVSRBonus4;
	}
	public void setAltVSRBonus4(HashMap altVSRBonus4) {
		AltVSRBonus4 = altVSRBonus4;
	}
	public HashMap getAltVSRBonus8() {
		return AltVSRBonus8;
	}
	public void setAltVSRBonus8(HashMap altVSRBonus8) {
		AltVSRBonus8 = altVSRBonus8;
	}
	public HashMap getRPGAI() {
		return RPGAI;
	}
	public void setRPGAI(HashMap rpgai) {
		RPGAI = rpgai;
	}
	public HashMap getAltEBDURING() {
		return AltEBDURING;
	}
	public void setAltEBDURING(HashMap altEBDURING) {
		AltEBDURING = altEBDURING;
	}
	public HashMap getTotalMaturityBenefit4() {
		return totalMaturityBenefit4;
	}
	public void setTotalMaturityBenefit4(HashMap totalMaturityBenefit4) {
		this.totalMaturityBenefit4 = totalMaturityBenefit4;
	}
	public long getRenewalPremiumST_RS_A() {
		return renewalPremiumST_RS_A;
	}
	public void setRenewalPremiumST_RS_A(long renewalPremiumST_RS_A) {
		this.renewalPremiumST_RS_A = renewalPremiumST_RS_A;
	}
	public long getRenewalPremiumST_RS_SA() {
		return renewalPremiumST_RS_SA;
	}
	public void setRenewalPremiumST_RS_SA(long renewalPremiumST_RS_SA) {
		this.renewalPremiumST_RS_SA = renewalPremiumST_RS_SA;
	}
	public long getRenewalPremiumST_RS_Q() {
		return renewalPremiumST_RS_Q;
	}
	public void setRenewalPremiumST_RS_Q(long renewalPremiumST_RS_Q) {
		this.renewalPremiumST_RS_Q = renewalPremiumST_RS_Q;
	}
	public long getRenewalPremiumST_RS_M() {
		return renewalPremiumST_RS_M;
	}
	public void setRenewalPremiumST_RS_M(long renewalPremiumST_RS_M) {
		this.renewalPremiumST_RS_M = renewalPremiumST_RS_M;
	}
	public long getTotalRenewalPremium_RS_SA() {
		return totalRenewalPremium_RS_SA;
	}
	public void setTotalRenewalPremium_RS_SA(long totalRenewalPremium_RS_SA) {
		this.totalRenewalPremium_RS_SA = totalRenewalPremium_RS_SA;
	}
	public long getTotalRenewalPremium_RS_Q() {
		return totalRenewalPremium_RS_Q;
	}
	public void setTotalRenewalPremium_RS_Q(long totalRenewalPremium_RS_Q) {
		this.totalRenewalPremium_RS_Q = totalRenewalPremium_RS_Q;
	}
	public long getTotalRenewalPremium_RS_M() {
		return totalRenewalPremium_RS_M;
	}
	public void setTotalRenewalPremium_RS_M(long totalRenewalPremium_RS_M) {
		this.totalRenewalPremium_RS_M = totalRenewalPremium_RS_M;
	}
	public long getTotalRenewalPremium_RS_A() {
		return totalRenewalPremium_RS_A;
	}
	public void setTotalRenewalPremium_RS_A(long totalRenewalPremium_RS_A) {
		this.totalRenewalPremium_RS_A = totalRenewalPremium_RS_A;
	}
	public String getCommissionText() {
		return commissionText;
	}
	public void setCommissionText(String commissionText) {
		this.commissionText = commissionText;
	}
	public long getTOTALRB8() {
		return TOTALRB8;
	}
	public void setTOTALRB8(long totalrb8) {
		TOTALRB8 = totalrb8;
	}
	public long getTOTALRB4() {
		return TOTALRB4;
	}
	public void setTOTALRB4(long totalrb4) {
		TOTALRB4 = totalrb4;
	}
	public long getTOTALGAI() {
		return TOTALGAI;
	}
	public void setTOTALGAI(long totalgai) {
		TOTALGAI = totalgai;
	}
	public long getTAXtext() {
		return TAXtext;
	}
	public void setTAXtext(long xtext) {
		TAXtext = xtext;
	}
	public HashMap getAccGuranteedAnnualIncome() {
		return accGuranteedAnnualIncome;
	}
	public void setAccGuranteedAnnualIncome(HashMap accGuranteedAnnualIncome) {
		this.accGuranteedAnnualIncome = accGuranteedAnnualIncome;
	}
	public double getPremiumDiscount() {
		return premiumDiscount;
	}
	public void setPremiumDiscount(double premiumDiscount) {
		this.premiumDiscount = premiumDiscount;
	}
	public HashMap getAnnuityRate() {
		return AnnuityRate;
	}
	public void setAnnuityRate(HashMap annuityRate) {
		AnnuityRate = annuityRate;
	}
	public long getTotalERModalPremPayble() {
		return totalERModalPremPayble;
	}
	public void setTotalERModalPremPayble(long totalERModalPremPayble) {
		this.totalERModalPremPayble = totalERModalPremPayble;
	}
	public HashMap getGuranteedAnnualIncomeAll() {
		return guranteedAnnualIncomeAll;
	}
	public void setGuranteedAnnualIncomeAll(HashMap guranteedAnnualIncomeAll) {
		this.guranteedAnnualIncomeAll = guranteedAnnualIncomeAll;
	}
	public String getTempImagePath() {
		return tempImagePath;
	}
	public void setTempImagePath(String tempImagePath) {
		this.tempImagePath = tempImagePath;
	}
	public ArrayList getMasterCombo() {
		return masterCombo;
	}
	public void setMasterCombo(ArrayList masterCombo) {
		this.masterCombo = masterCombo;
	}
	public HashMap getLifeCoverageCombo1() {
		return lifeCoverageCombo1;
	}
	public void setLifeCoverageCombo1(HashMap lifeCoverageCombo1) {
		this.lifeCoverageCombo1 = lifeCoverageCombo1;
	}
	public HashMap getLifeCoverageCombo2() {
		return lifeCoverageCombo2;
	}
	public void setLifeCoverageCombo2(HashMap lifeCoverageCombo2) {
		this.lifeCoverageCombo2 = lifeCoverageCombo2;
	}
	public HashMap getNonGuaranteedSurrBenefitCombo1() {
		return nonGuaranteedSurrBenefitCombo1;
	}
	public void setNonGuaranteedSurrBenefitCombo1(
			HashMap nonGuaranteedSurrBenefitCombo1) {
		this.nonGuaranteedSurrBenefitCombo1 = nonGuaranteedSurrBenefitCombo1;
	}
	public HashMap getNonGuaranteedSurrBenefitCombo2() {
		return nonGuaranteedSurrBenefitCombo2;
	}
	public void setNonGuaranteedSurrBenefitCombo2(
			HashMap nonGuaranteedSurrBenefitCombo2) {
		this.nonGuaranteedSurrBenefitCombo2 = nonGuaranteedSurrBenefitCombo2;
	}
	public boolean isCombo() {
		return isCombo;
	}
	public void setCombo(boolean isCombo) {
		this.isCombo = isCombo;
	}
	public long getServiceTaxCombo1() {
		return serviceTaxCombo1;
	}
	public void setServiceTaxCombo1(long serviceTaxCombo1) {
		this.serviceTaxCombo1 = serviceTaxCombo1;
	}
	public long getServiceTaxCombo2() {
		return serviceTaxCombo2;
	}
	public void setServiceTaxCombo2(long serviceTaxCombo2) {
		this.serviceTaxCombo2 = serviceTaxCombo2;
	}
	public long getTotalModalPremiumPaybleCombo1() {
		return totalModalPremiumPaybleCombo1;
	}
	public void setTotalModalPremiumPaybleCombo1(long totalModalPremiumPaybleCombo1) {
		this.totalModalPremiumPaybleCombo1 = totalModalPremiumPaybleCombo1;
	}
	public long getTotalModalPremiumPaybleCombo2() {
		return totalModalPremiumPaybleCombo2;
	}
	public void setTotalModalPremiumPaybleCombo2(long totalModalPremiumPaybleCombo2) {
		this.totalModalPremiumPaybleCombo2 = totalModalPremiumPaybleCombo2;
	}
	public long getRenewalPremiumST_RS_A_Combo1() {
		return renewalPremiumST_RS_A_Combo1;
	}
	public void setRenewalPremiumST_RS_A_Combo1(long renewalPremiumST_RS_A_Combo1) {
		this.renewalPremiumST_RS_A_Combo1 = renewalPremiumST_RS_A_Combo1;
	}
	public long getRenewalPremiumST_RS_SA_Combo1() {
		return renewalPremiumST_RS_SA_Combo1;
	}
	public void setRenewalPremiumST_RS_SA_Combo1(long renewalPremiumST_RS_SA_Combo1) {
		this.renewalPremiumST_RS_SA_Combo1 = renewalPremiumST_RS_SA_Combo1;
	}
	public long getRenewalPremiumST_RS_Q_Combo1() {
		return renewalPremiumST_RS_Q_Combo1;
	}
	public void setRenewalPremiumST_RS_Q_Combo1(long renewalPremiumST_RS_Q_Combo1) {
		this.renewalPremiumST_RS_Q_Combo1 = renewalPremiumST_RS_Q_Combo1;
	}
	public long getRenewalPremiumST_RS_M_Combo1() {
		return renewalPremiumST_RS_M_Combo1;
	}
	public void setRenewalPremiumST_RS_M_Combo1(long renewalPremiumST_RS_M_Combo1) {
		this.renewalPremiumST_RS_M_Combo1 = renewalPremiumST_RS_M_Combo1;
	}
	public long getTotalRenewalPremium_RS_SA_Combo1() {
		return totalRenewalPremium_RS_SA_Combo1;
	}
	public void setTotalRenewalPremium_RS_SA_Combo1(
			long totalRenewalPremium_RS_SA_Combo1) {
		this.totalRenewalPremium_RS_SA_Combo1 = totalRenewalPremium_RS_SA_Combo1;
	}
	public long getTotalRenewalPremium_RS_Q_Combo1() {
		return totalRenewalPremium_RS_Q_Combo1;
	}
	public void setTotalRenewalPremium_RS_Q_Combo1(
			long totalRenewalPremium_RS_Q_Combo1) {
		this.totalRenewalPremium_RS_Q_Combo1 = totalRenewalPremium_RS_Q_Combo1;
	}
	public long getTotalRenewalPremium_RS_M_Combo1() {
		return totalRenewalPremium_RS_M_Combo1;
	}
	public void setTotalRenewalPremium_RS_M_Combo1(
			long totalRenewalPremium_RS_M_Combo1) {
		this.totalRenewalPremium_RS_M_Combo1 = totalRenewalPremium_RS_M_Combo1;
	}
	public long getTotalRenewalPremium_RS_A_Combo1() {
		return totalRenewalPremium_RS_A_Combo1;
	}
	public void setTotalRenewalPremium_RS_A_Combo1(
			long totalRenewalPremium_RS_A_Combo1) {
		this.totalRenewalPremium_RS_A_Combo1 = totalRenewalPremium_RS_A_Combo1;
	}
	public long getRenewalPremiumST_RS_A_Combo2() {
		return renewalPremiumST_RS_A_Combo2;
	}
	public void setRenewalPremiumST_RS_A_Combo2(long renewalPremiumST_RS_A_Combo2) {
		this.renewalPremiumST_RS_A_Combo2 = renewalPremiumST_RS_A_Combo2;
	}
	public long getRenewalPremiumST_RS_SA_Combo2() {
		return renewalPremiumST_RS_SA_Combo2;
	}
	public void setRenewalPremiumST_RS_SA_Combo2(long renewalPremiumST_RS_SA_Combo2) {
		this.renewalPremiumST_RS_SA_Combo2 = renewalPremiumST_RS_SA_Combo2;
	}
	public long getRenewalPremiumST_RS_Q_Combo2() {
		return renewalPremiumST_RS_Q_Combo2;
	}
	public void setRenewalPremiumST_RS_Q_Combo2(long renewalPremiumST_RS_Q_Combo2) {
		this.renewalPremiumST_RS_Q_Combo2 = renewalPremiumST_RS_Q_Combo2;
	}
	public long getRenewalPremiumST_RS_M_Combo2() {
		return renewalPremiumST_RS_M_Combo2;
	}
	public void setRenewalPremiumST_RS_M_Combo2(long renewalPremiumST_RS_M_Combo2) {
		this.renewalPremiumST_RS_M_Combo2 = renewalPremiumST_RS_M_Combo2;
	}
	public long getTotalRenewalPremium_RS_SA_Combo2() {
		return totalRenewalPremium_RS_SA_Combo2;
	}
	public void setTotalRenewalPremium_RS_SA_Combo2(
			long totalRenewalPremium_RS_SA_Combo2) {
		this.totalRenewalPremium_RS_SA_Combo2 = totalRenewalPremium_RS_SA_Combo2;
	}
	public long getTotalRenewalPremium_RS_Q_Combo2() {
		return totalRenewalPremium_RS_Q_Combo2;
	}
	public void setTotalRenewalPremium_RS_Q_Combo2(
			long totalRenewalPremium_RS_Q_Combo2) {
		this.totalRenewalPremium_RS_Q_Combo2 = totalRenewalPremium_RS_Q_Combo2;
	}
	public long getTotalRenewalPremium_RS_M_Combo2() {
		return totalRenewalPremium_RS_M_Combo2;
	}
	public void setTotalRenewalPremium_RS_M_Combo2(
			long totalRenewalPremium_RS_M_Combo2) {
		this.totalRenewalPremium_RS_M_Combo2 = totalRenewalPremium_RS_M_Combo2;
	}
	public long getTotalRenewalPremium_RS_A_Combo2() {
		return totalRenewalPremium_RS_A_Combo2;
	}
	public void setTotalRenewalPremium_RS_A_Combo2(
			long totalRenewalPremium_RS_A_Combo2) {
		this.totalRenewalPremium_RS_A_Combo2 = totalRenewalPremium_RS_A_Combo2;
	}
	public long getComboAnnualizedPremium() {
		return comboAnnualizedPremium;
	}
	public void setComboAnnualizedPremium(long comboAnnualizedPremium) {
		this.comboAnnualizedPremium = comboAnnualizedPremium;
	}
	public long getCombo2AnnualizedPremium() {
		return Combo2AnnualizedPremium;
	}
	public void setCombo2AnnualizedPremium(long combo2AnnualizedPremium) {
		Combo2AnnualizedPremium = combo2AnnualizedPremium;
	}
	public long getComboprem1() {
		return Comboprem1;
	}
	public void setComboprem1(long comboprem1) {
		Comboprem1 = comboprem1;
	}
	public long getComboprem2() {
		return Comboprem2;
	}
	public void setComboprem2(long comboprem2) {
		Comboprem2 = comboprem2;
	}
	public double getSec7premmonthly1() {
		return sec7premmonthly1;
	}
	public void setSec7premmonthly1(double sec7premmonthly1) {
		this.sec7premmonthly1 = sec7premmonthly1;
	}
	public long getSec7Ann() {
		return sec7Ann;
	}
	public void setSec7Ann(long sec7Ann) {
		this.sec7Ann = sec7Ann;
	}
	public double getServiceTaxMain() {
		return ServiceTaxMain;
	}
	public void setServiceTaxMain(double serviceTaxMain) {
		ServiceTaxMain = serviceTaxMain;
	}
	public double getServiceTaxFY() {
		return ServiceTaxFY;
	}
	public void setServiceTaxFY(double serviceTaxFY) {
		ServiceTaxFY = serviceTaxFY;
	}
	public double getServiceTaxRenewal() {
		return ServiceTaxRenewal;
	}
	public void setServiceTaxRenewal(double serviceTaxRenewal) {
		ServiceTaxRenewal = serviceTaxRenewal;
	}
	public long getCombo1AnnualizedPremium() {
		return Combo1AnnualizedPremium;
	}
	public void setCombo1AnnualizedPremium(long combo1AnnualizedPremium) {
		Combo1AnnualizedPremium = combo1AnnualizedPremium;
	}
	public long getModalPremiumSGP1() {
		return modalPremiumSGP1;
	}
	public void setModalPremiumSGP1(long modalPremiumSGP1) {
		this.modalPremiumSGP1 = modalPremiumSGP1;
	}
	public long getModalPremiumSGP2() {
		return modalPremiumSGP2;
	}
	public void setModalPremiumSGP2(long modalPremiumSGP2) {
		this.modalPremiumSGP2 = modalPremiumSGP2;
	}
	public HashMap getMaturityValSGP1() {
		return maturityValSGP1;
	}
	public void setMaturityValSGP1(HashMap maturityValSGP1) {
		this.maturityValSGP1 = maturityValSGP1;
	}
	public HashMap getMaturityValSGP2() {
		return maturityValSGP2;
	}
	public void setMaturityValSGP2(HashMap maturityValSGP2) {
		this.maturityValSGP2 = maturityValSGP2;
	}
	public long getModalPremiumForAnnNSAP() {
		return modalPremiumForAnnNSAP;
	}
	public void setModalPremiumForAnnNSAP(long modalPremiumForAnnNSAP) {
		this.modalPremiumForAnnNSAP = modalPremiumForAnnNSAP;
	}
	public long getTotAnnRiderModalPremium() {
		return totAnnRiderModalPremium;
	}
	public void setTotAnnRiderModalPremium(long totAnnRiderModalPremium) {
		this.totAnnRiderModalPremium = totAnnRiderModalPremium;
	}
	public HashMap getBasicSumAssured() {
		return basicSumAssured;
	}
	public void setBasicSumAssured(HashMap basicSumAssured) {
		this.basicSumAssured = basicSumAssured;
	}
	public HashMap getTotalGuaranteedBenefit() {
		return totalGuaranteedBenefit;
	}
	public void setTotalGuaranteedBenefit(HashMap totalGuaranteedBenefit) {
		this.totalGuaranteedBenefit = totalGuaranteedBenefit;
	}
	public long getDiscountedModalPrimium() {
		return discountedModalPrimium;
	}

	public void setDiscountedModalPrimium(long discountedModalPrimium) {
		this.discountedModalPrimium = discountedModalPrimium;
	}
}
