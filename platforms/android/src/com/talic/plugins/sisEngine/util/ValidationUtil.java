package com.talic.plugins.sisEngine.util;

import android.database.Cursor;

import com.talic.plugins.sisEngine.BO.RiderBO;
import com.talic.plugins.sisEngine.bean.ModelMaster;
import com.talic.salesapp.DBHelper;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 3/2/16.
 */
public class ValidationUtil {
    private ModelMaster master;
    public ValidationUtil(ModelMaster master){
        this.master=master;
    }
    public boolean validateRiderPrem(){
        long riderPrem = 0;
        long baseAnnPrem = 0;
        JSONObject json = new JSONObject();
        String planCode = master.getRequestBean().getBaseplan();
        ArrayList aList=master.getRequestBean().getRiderlist();
        if(aList != null && aList.size() > 0) {
            RiderBO oRiderBO = (RiderBO) aList.get(0);
            if ((planCode.startsWith("MRSP") || planCode.startsWith("SR")) && (aList != null) && (oRiderBO.getCode().equalsIgnoreCase("ADDLN1V1"))) {
                SQLiteDatabase db;
                Cursor rs;
                try {
                    db = DBHelper.getDatabaseConnection();
                    String[] args = {master.getRequestBean().getInsured_occupation(), oRiderBO.getCode()};
                    rs = db.rawQuery("select RML_PREM_MULT from LP_RML_RIDER_PREM_MULTIPLE_LK where (RML_BAND = (select OCL_CLASS from LP_OCL_OCCUPATION_LK where OCL_CODE = ?)) and (RML_RIDER_CD = ?)", args);
                    if (rs != null) {
                        for (int i = 0; i < rs.getCount(); i++) {
                            rs.moveToPosition(i);
                            double PM = rs.getDouble(rs.getColumnIndex("RML_PREM_MULT"));
                            riderPrem = Math.round((oRiderBO.getSumAssured() * PM) / 1000);
                            baseAnnPrem = Math.round(master.getRequestBean().getSumassured() * Double.parseDouble(master.getRequestBean().getPremiummul()) / 1000);
                            if (riderPrem > (baseAnnPrem * 0.30)) {
                                rs.close();
                                return true;
                            }
                        }
                        rs.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
