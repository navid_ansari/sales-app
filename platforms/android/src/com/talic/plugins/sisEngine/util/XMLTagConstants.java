package com.talic.plugins.sisEngine.util;

import java.io.Serializable;

@SuppressWarnings("serial")
public class XMLTagConstants implements Serializable{

			public static final String ProposalNo="ProposalNo";
			public static final String ProposalDate="ProposalDate ";
			public static final String InsuredName ="InsuredName";
			public static final String InsuredAge ="InsuredAge";
			public static final String InsuredGender="InsuredGender";
			public static final String InsuredOccupation="InsuredOccupation";
			public static final String ProposerName="ProposerName";
			public static final String ProposerAge="ProposerAge";
			public static final String ProposerGender ="ProposerGender";
			public static final String ProposerOccupation="ProposerOccupation";
			public static final String BasePlan="BasePlan";
			public static final String SumAssured="SumAssured";
			public static final String PolicyTerm="PolicyTerm";
			public static final String PremiumPayingTerm="PremiumPayingTerm";
			public static final String Mode="Mode";
			public static final String PremiumMultiple="PremiumMultiple";
			public static final String BasePremium ="BasePremium";
			public static final String ModelPremium ="ModelPremium";
			public static final String Riders ="Riders";
			public static final String Funds="Funds";
			public static final String PolicyYear ="PolicyYear";
			public static final String PolicyInsuredAge ="PolicyInsuredAge";
			public static final String AnnualizedBasicPremium ="AnnualizedBasicPremium";
			public static final String PremiumAllocationCharge="PremiumAllocationCharge";
			public static final String AmountAvailableForIInvestment ="AmountAvailableForIInvestment";
			public static final String OtherInbuiltBenefitCharge="OtherInbuiltBenefitCharge";
			public static final String PolicyAdminCharge ="PolicyAdminCharge";
			public static final String MortalityCharges="MortalityCharges";
			public static final String GuaranteeCharges="GuaranteeCharges";
			public static final String OtherCharges ="OtherCharges";
			public static final String ApplicableServiceTax ="ApplicableServiceTax";
			public static final String RegularFundValuebeforeFMC="RegularFundValuebeforeFMC";
			public static final String FundManagementCharge ="FundManagementCharge";
			public static final String RegularFundValueAtEOPY ="RegularFundValueAtEOPY";
			public static final String GuaranteedMaturityAddition="GuaranteedMaturityAddition";
			public static final String TotalFundValueAtEOPY ="TotalFundValueAtEOPY";;
			public static final String SurrenderValueAtEOPY ="SurrenderValueAtEOPY";
			public static final String DeathBenefitAtEOPY="DeathBenefitAtEOPY";
			public static final String TotalModalPlusRiderPrem="TotalModalPlusRiderPrem";
			public static final String ServiceTax="ServiceTax";
			public static final String TotalPremWtServiceTax="TotalPremWtServiceTax";
			public static final String Currency="Currency";
			public static final String MaturityValue="MaturityValue";
			public static final String SurrenderBenefit="SurrenderBenefit";
			public static final String LifeCoverage="LifeCoverage";
			public static final String InsuredDOB="InsuredDOB";
			public static final String ProposerDOB="ProposerDOB";
			public static final String GAI="GauranteedAnnualIncome";
			public static final String GSV="GauranteedSurrenderValue";
			public static final String SSV="SpecialSurrenderValue";

}
