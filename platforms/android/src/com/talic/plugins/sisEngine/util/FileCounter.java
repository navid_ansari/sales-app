package com.talic.plugins.sisEngine.util;

public class FileCounter {

	private static long count = 0;
	private static final Object lock = new Object();

	public synchronized long countIncrementer() {
		synchronized (lock) {
			return ++count;
		}
	}

}
