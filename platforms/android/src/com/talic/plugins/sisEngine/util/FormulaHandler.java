package com.talic.plugins.sisEngine.util;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;


import com.talic.plugins.sisEngine.BO.FEDFormulaExtDesc;
import com.talic.plugins.sisEngine.BO.PFRPlanFormulaRef;
import com.talic.plugins.sisEngine.bean.FormulaBean;
import com.talic.plugins.sisEngine.db.DataAccessClass;

@SuppressWarnings("unchecked")
public class FormulaHandler {


	public Object evaluateFormula(String formulaKey, String formulaName, FormulaBean fBean){
		Object obj = new Object();
		PFRPlanFormulaRef formulaObj =  DataAccessClass.getformula(formulaKey,formulaName);
		obj = 0; // Added by Akhil on 03-07-2015 14:52:00 to avoid conversion exceptions in request processor
		if(formulaObj != null){
			CommonConstant.printLog("d", "Formula execution start : " + formulaName);
			HashMap formulaExpMap =  getFormulaExp(formulaObj.getPFR_FRM_CD());
			obj = executeFormula(formulaExpMap, fBean);
		}
	 return obj;

	}

	public Object executeFormula(HashMap frmExpMap, FormulaBean fBean)
	{
		Object obj = new Object();
		Object obj1 = new Object();
		Object obj2 = new Object();
		String oprType = "";

		FEDFormulaExtDesc oFormulaExp1 = (FEDFormulaExtDesc)frmExpMap.get(1);
		CommonConstant.printLog("d", " oFormulaExp1::: " + oFormulaExp1.getFED_ID() + " : "
				+ oFormulaExp1.getFED_OBJ_OPND() + " : "
				+ oFormulaExp1.getFED_OBJ_OPND_TYPE() + " : "
				+ oFormulaExp1.getFED_OBJ_OPTR() + " : "
				+ oFormulaExp1.getFED_OBJ_SEQ() + " : "
				+ oFormulaExp1.getFED_OBJ_TYPE());
		//Oparand1
		if(FormulaConstant.TYPE_FORMULA.equals(oFormulaExp1.getFED_OBJ_OPND_TYPE())){
			// evaluate formula here
			//CommonConstant.printLog("d","Inside Obj1 type formula " + oFormulaExp1.getFED_OBJ_OPND());
			HashMap newFormulaMap = new HashMap();
			newFormulaMap = getFormulaExp(oFormulaExp1.getFED_OBJ_OPND());
			obj1 = executeFormula(newFormulaMap, fBean);
			CommonConstant.printLog("d", "Object1 from formula::::::::::::" + obj1);
		}else if(FormulaConstant.TYPE_MATH_FORMULA.equals(oFormulaExp1.getFED_OBJ_OPND_TYPE())){

		}else if(FormulaConstant.TYPE_NUMBER.equals(oFormulaExp1.getFED_OBJ_OPND_TYPE())){
			//getNumber
			obj1 = getNumberValue(oFormulaExp1.getFED_OBJ_TYPE(), oFormulaExp1.getFED_OBJ_OPND());
			CommonConstant.printLog("d", "Object1 from Number::::::::::::" + obj1);
		}else{
			obj1 =  getParameterVal(oFormulaExp1.getFED_OBJ_OPND(), fBean);
			CommonConstant.printLog("d", "Object1 from Variable ::::::::::::" + obj1);
		}
		oprType = oFormulaExp1.getFED_OBJ_TYPE();
		for(int mapCount=2; mapCount<=frmExpMap.size(); mapCount++){
			FEDFormulaExtDesc oFormulaExp2 = (FEDFormulaExtDesc)frmExpMap.get(mapCount);
			CommonConstant.printLog("d", " oFormulaExp2::: " + oFormulaExp2.getFED_ID() + " : "
					+ oFormulaExp2.getFED_OBJ_OPND() + " : "
					+ oFormulaExp2.getFED_OBJ_OPND_TYPE() + " : "
					+ oFormulaExp2.getFED_OBJ_OPTR() + " : "
					+ oFormulaExp2.getFED_OBJ_SEQ() + " : "
					+ oFormulaExp2.getFED_OBJ_TYPE());
			//Oparand2
			if(FormulaConstant.TYPE_FORMULA.equals(oFormulaExp2.getFED_OBJ_OPND_TYPE())){
				// evaluate formula here
				//CommonConstant.printLog("d","Inside Obj2 type formula " + oFormulaExp2.getFED_OBJ_OPND());
				HashMap newFormulaMap = new HashMap();
				newFormulaMap = getFormulaExp(oFormulaExp2.getFED_OBJ_OPND());
				obj2 = executeFormula(newFormulaMap, fBean);
				CommonConstant.printLog("d", "Object2 from formula ::::::::::::" + obj2);
			}else if(FormulaConstant.TYPE_MATH_FORMULA.equals(oFormulaExp2.getFED_OBJ_OPND_TYPE())){
				obj1 = evalMathFormula(obj1, oprType, oFormulaExp2.getFED_OBJ_OPND(), oFormulaExp2.getFED_OBJ_TYPE());
				oprType = oFormulaExp2.getFED_OBJ_TYPE();
				CommonConstant.printLog("d", "Object1 from math formula inside for ::::::::::::" + obj1);
				continue;
			}else if(FormulaConstant.TYPE_NUMBER.equals(oFormulaExp2.getFED_OBJ_OPND_TYPE())){
				 obj2 = getNumberValue(oFormulaExp2.getFED_OBJ_TYPE(), oFormulaExp2.getFED_OBJ_OPND());
				//obj2 = Integer.parseInt(oFormulaExp2.getFED_OBJ_OPND());
				 CommonConstant.printLog("d", "Object2 from Number::::::::::::" + obj2);
			}else{
				obj2 = getParameterVal(oFormulaExp2.getFED_OBJ_OPND(), fBean);
				CommonConstant.printLog("d", "Object2 from variable ::::::::::::" + obj2);
			}

			obj1 = evalFormula(obj1, oprType, obj2,oFormulaExp2.getFED_OBJ_TYPE(),oFormulaExp2.getFED_OBJ_OPTR());
			oprType = getOprType(oprType, oFormulaExp2.getFED_OBJ_TYPE());
			CommonConstant.printLog("d", "Object1 after calculation ::::::::::::" + obj1 + " : " + oprType);
		}
		obj = obj1;
		return obj;
	}

	private Object getParameterVal(String param, FormulaBean fBean){

		Object obj = new Object();
		if(FormulaConstant.PARM_PREMIUM.equals(param)){
			obj = fBean.getP();
		}
		if(FormulaConstant.PARM_ANNUALIZED_PREMIUM.equals(param)){
			obj = fBean.getAP();
		}
		if(FormulaConstant.PARM_ULIP_ANNUALIZED_PREMIUM.equals(param)){
			obj = fBean.getULIPAP();
		}
		if(FormulaConstant.PARM_PREMIUM_MULTIPLE.equals(param)){
			obj = DataAccessClass.getPremiumMultiple(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_PREMIUM_DISCOUNT.equals(param)){
			obj = fBean.getPD();
		}
		if(FormulaConstant.PARM_TERM.equals(param)){
			obj = fBean.getRequestBean().getPolicyterm();
		}
		if(FormulaConstant.PARM_COVERAGE.equals(param)){
			obj = fBean.getRequestBean().getSumassured();
		}
		if(FormulaConstant.PARM_CASH_SURR_VAL_FACTOR.equals(param)){
			obj = DataAccessClass.getCSVFactor(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_GURANTEED_SURR_VAL_FACTOR.equals(param)){
			obj = DataAccessClass.getGSVFactor(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_TBPT_8_FACTOR.equals(param)){
			obj = DataAccessClass.getTB8Factor(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_TBPT_4_FACTOR.equals(param)){
			obj = DataAccessClass.getTB4Factor(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_RIDER_PREMIUM_MULTIPLE.equals(param)){
			obj = DataAccessClass.getRiderPremiumMultiple(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_PREMIUM_MULTIPLE_PM_MIP.equals(param)){
			obj = DataAccessClass.getPremiumMultiple_MIP(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_LARGE_PREMIUM_BOOST.equals(param)){
			obj = DataAccessClass.getPremiumBoost(fBean);
		}
		if(FormulaConstant.PARM_RIDER_SUM_ASSURED.equals(param)){
			obj = DataAccessClass.getRiderSumAssured(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_WP_LOAD_VALUE.equals(param)){
			obj = DataAccessClass.getWpLoadValue(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_INS_LOAD_VALUE.equals(param)){
			obj = DataAccessClass.getInsLoadValue(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_MODEL_FACTOR.equals(param)){
			obj = DataAccessClass.getModelFactor(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_RIDER_MODEL_FACTOR.equals(param)){
			obj = DataAccessClass.getRiderModelFactor(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_PREMIUM_PAYING_TERM.equals(param)){
			obj = fBean.getRequestBean().getPremiumpayingterm();
		}
		if(FormulaConstant.PARM_PREMIUM_ALLOCATION_CHARGES.equals(param)){
			obj = DataAccessClass.getPAChargeValue(fBean.getRequestBean(), fBean.getP(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_WOP_CHARGES.equals(param)){
			obj = DataAccessClass.getWOPCharges(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_FIB_CHARGES.equals(param)){
			obj = DataAccessClass.getFIBCharges(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_COUNTER.equals(param)){
			obj = fBean.getCNT();
		}
		if(FormulaConstant.PARM_MORTALITY_CHARGES_RATE.equals(param)){
			obj = DataAccessClass.getMCRCharges(fBean.getRequestBean(), fBean.getCNT());
		}
        if(FormulaConstant.PARM_NSAP_MORTALITY_CHARGES_RATE.equals(param)){
            obj = DataAccessClass.getNSAPMCRCharges(fBean.getRequestBean(), fBean.getCNT());
        }
		if(FormulaConstant.PARM_POLICY_ADMIN_CHARGES.equals(param)){
			obj = fBean.getADM();
		}
		if(FormulaConstant.PARM_NET_P_HOLDER_ACCOUNT.equals(param)){
			obj = fBean.getNPHA();
		}
		if(FormulaConstant.PARM_SURCHARGE.equals(param)){
			obj = DataAccessClass.getSurrenderChargeValue(fBean.getRequestBean(), fBean.getP(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_SURCHARGE_AMOUNT.equals(param)){
			obj = DataAccessClass.getSurrenderChargeAmount(fBean.getRequestBean(), fBean.getP(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_NETPH_AMOUNT_FOR_LOYALTY_BONUS.equals(param)){
			obj = fBean.getNPHAMTLB();
		}
		if(FormulaConstant.PARM_GUARANTEED_MATURITY_BONUS.equals(param)){
			obj = DataAccessClass.getGuaranteedMaturityBonus(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_AMOUNT.equals(param)){
			obj = fBean.getA();
		}
		if(FormulaConstant.PARM_RBV.equals(param)){
			obj = fBean.getRBV();
		}
		if(FormulaConstant.PARM_AGAI.equals(param)){
			obj = fBean.getAGAI();
		}
		if(FormulaConstant.PARM_DAGAI.equals(param)){
			obj = fBean.getDoubleAGAI();
		}
		if(FormulaConstant.PARM_AGAISSV.equals(param)){
			obj = fBean.getAGAISSV();
		}
		if(FormulaConstant.PARM_RC.equals(param)){
			obj = fBean.getRC();
		}
		if(FormulaConstant.PARM_FUND_MANAGMENT_CHARGE.equals(param)){
			obj = DataAccessClass.getFMC(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_COMMISSION_PERCENTAGE.equals(param)){
			obj = DataAccessClass.getCommissionPercentage(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_MATURITY_FACTOR_VALUE.equals(param)){
			obj = DataAccessClass.getMVFactor(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_MATURITY_FACTOR_VALUE_FG.equals(param) && fBean.getRequestBean().getInsured_sex().equals("M")){
			obj =DataAccessClass.getRBTBBonus(fBean, "MFV_M", 0);
		}
		else if(FormulaConstant.PARM_MATURITY_FACTOR_VALUE_FG.equals(param) && fBean.getRequestBean().getInsured_sex().equals("F")){
			obj =DataAccessClass.getRBTBBonus(fBean, "MFV_F", 0);
		}
		if(FormulaConstant.PARM_TOTAL_PREMIUM.equals(param)){
			obj = DataAccessClass.getTotalPremium(fBean);
		}
		if(FormulaConstant.PARM_TOTAL_ANNUAL_PREMIUM.equals(param)){
			obj = DataAccessClass.getTotalAnnualPremium(fBean);
		}
		if(FormulaConstant.PARM_ALT_TOTAL_PREMIUM.equals(param)){
			obj = DataAccessClass.getALTTotalPremium(fBean);
		}
		if(FormulaConstant.PARM_ALT_EB_TOTAL_PREMIUM.equals(param)){
			obj = DataAccessClass.getEB_TotalPremium(fBean);
		}

		if(FormulaConstant.PARM_TOTAL_PREMIUM_ULIP.equals(param)){
			obj = DataAccessClass.getTotalPremiumULIP(fBean);
		}
		if(FormulaConstant.PARM_TOTAL_REMAINING_PREMIUM.equals(param)){//Added by Samina for total policy premium
			obj = DataAccessClass.getTotalRemainingPremium(fBean);
		}
		if(FormulaConstant.PARM_GURANTEED_ANNUAL_INCOME.equals(param)){
			obj = DataAccessClass.getGAIFactor(fBean);
		}
		if(FormulaConstant.PARM_TOTAL_GURANTEED_ANNUAL_INCOME.equals(param)){
			obj = DataAccessClass.getTotalGAI(fBean);
		}
		if(FormulaConstant.PARM_ANNUITY_RATE.equals(param)){//Added by Samina for easy retire
			obj = DataAccessClass.getANNUITYRate(fBean.getRequestBean());
		}
		if(FormulaConstant.PARM_FOP_RATE.equals(param)){//Added by Samina for Super Achiever
			//obj = 0.0;
			obj = DataAccessClass.getFOPRate(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_INCENTIVE_RATE.equals(param)){////Added by Samina for easy retire
			obj = DataAccessClass.getINCENTIVERate(fBean.getRequestBean());
		}
		/** Vikas - Added for Mahalife Gold - GAI*/
		if(FormulaConstant.PARM_TOTAL_GURANTEED_ANNUAL_INCOME_MLG.equals(param)){
			obj = DataAccessClass.getTotalGAIMLG(fBean);
		}
		/** Vikas - Added for Mahalife Gold - GAI*/
		// For Mahalife Gold Plus Mohammad Rashid
		if(FormulaConstant.PARM_TOTAL_GURANTEED_ANNUAL_INCOME_MBP.equals(param)){
            obj = DataAccessClass.getTotalGAIMBP(fBean);
		}
		if(FormulaConstant.PARM_INFLATION_PROTECTION_COVER.equals(param)){
			obj = DataAccessClass.getIPCFactor(fBean.getRequestBean(), fBean.getCNT());
		}
		if(FormulaConstant.PARM_INFLATION_PROTECTION_COVER_MINUS1.equals(param)){
			obj = DataAccessClass.getIPCFactor(fBean.getRequestBean(), fBean.getCNT() - 1);
		}
		if(FormulaConstant.PARAM_LOYALTY_BONUS.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean, "LB", fBean.getCNT());
		}
		if(FormulaConstant.PARAM_LOYALTY_BONUS_IO.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean, "LBIO", fBean.getCNT());
		}
        if(FormulaConstant.PARAM_LOYALTY_BONUS_SA.equals(param)){
            obj = DataAccessClass.getRBTBBonus(fBean,"LBSA", fBean.getCNT());
        }
		// For Mahalife Gold Plus Mohammad Rashid
		if(FormulaConstant.PARAM_REVERSIONARY_BONUS_4.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean, "RB4", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_BONUS_8.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RB8", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPBONUS_4.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean, "RPRB4", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPBONUS_8.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RPRB8", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPBONUS_GP_8.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RPRB8_GP", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPBONUS_GP_4.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RPRB4_GP", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_EB_BONUS.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"EBBONUS", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_EBBONUS.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"EBTB", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPTB_8.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RPTB8", 0);
		}
		if(FormulaConstant.PARAM_REVERSIONARY_RPTB_4.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"RPTB4", 0);
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_4.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB4", 12);
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_8.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB8", 12);
		}
        //Good Kid
        if(FormulaConstant.PARAM_TBPT_8_FACTOR_GK.equals(param)){
            obj = DataAccessClass.getRBTBBonus(fBean,"TB8GK", fBean.getCNT());
        }
        if(FormulaConstant.PARAM_TBPT_4_FACTOR_GK.equals(param)){
            obj = DataAccessClass.getRBTBBonus(fBean,"TB4GK", fBean.getCNT());
        }

        //Good Kid
		if(FormulaConstant.PARM_TERMINAL_BONUS_4_MLM.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB4", 10);
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_8_MLM.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB8", 10);
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_4_SGP.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB4SGP", fBean.getCNT());
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_8_SGP.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB8SGP", fBean.getCNT());
		}
		if(FormulaConstant.PARM_BONUS_SURRENDER_FACTOR.equals(param)){
			obj = DataAccessClass.getBSVFactor(fBean.getRequestBean(),fBean.getCNT());
		}
		//start : added by jayesh for Smart 7 : 05-05-2014
		if(FormulaConstant.PARM_TERMINAL_BONUS_4_SMART7.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB4", 8); 	//modified by jayesh
		}
		if(FormulaConstant.PARM_TERMINAL_BONUS_8_SMART7.equals(param)){
			obj = DataAccessClass.getRBTBBonus(fBean,"TB8", 8); 	//modified by jayesh
		}
		//end : added by jayesh for Smart 7 : 05-05-2014
		if(FormulaConstant.PARM_PREMIUM_MULTIPLE_ULS.equals(param)){
			obj = fBean.getRequestBean().getPremiummul();
		}
		if(FormulaConstant.PARM_RIDER_CHARGE_FACTOR.equals(param)){
			//obj = 0.0;
			obj = DataAccessClass.getRLCF(fBean,fBean.getRequestBean(),fBean.getCNT());
		}
		if(FormulaConstant.PARM_FUND_MANAGMENT_CHARGE_AAA.equals(param)){//Added by Samina for AAA3
			obj = DataAccessClass.getAAAFMC(fBean.getRequestBean(),fBean.getCNT());
		}
		if(FormulaConstant.PARM_FUND_MANAGMENT_CHARGE_DEBT.equals(param)){//Added by Samina for AAA3
			obj = DataAccessClass.getAAADFV(fBean.getRequestBean(),fBean.getCNT());
		}
		if(FormulaConstant.PARM_FUND_MANAGMENT_CHARGE_EQUITY.equals(param)){//Added by Samina for AAA3
			obj = DataAccessClass.getAAAEFV(fBean.getRequestBean(),fBean.getCNT());
		}
		if(FormulaConstant.PARAM_QUOTED_ANNUAL_PREMIUM.equals(param)){
			obj = fBean.getQAP();
		}
		if(FormulaConstant.PARAM_TOTAL_QUOTED_ANNUAL_PREMIUM.equals(param)){
			obj = DataAccessClass.getTotalQAPPremium(fBean);
		}
		if(FormulaConstant.PARM_QUOTED_MODAL_PREMIUM.equals(param)){
			obj = fBean.getQMP();
		}
		if(FormulaConstant.PARM_REDUCED_PREMIUM_PAID_UP_YEAR.equals(param)){
			obj = fBean.getRequestBean().getRpu_year();
		}
		if(FormulaConstant.PARM_QAP.equals(param)){
			obj = fBean.getQAP();
		}
		if(FormulaConstant.TAXSLAB.equals(param)){
			obj = fBean.getRequestBean().getTaxslab();
		}
		if(FormulaConstant.WITHDRAWAL_AMOUNT.equals(param)){
			obj = fBean.getWithdrawalAmount();
		}
		if(FormulaConstant.PARM_SERVICE_TAX_RIDERS.equals(param)){
			obj = fBean.getSTAXR();
		}
		if(FormulaConstant.RIDER_PREMIUM.equals(param)){
			obj = fBean.getRiderPremium();
		}
		if(FormulaConstant.PARM_EXPERIENCED_BONUS_RATE.equals(param)){
			obj = fBean.getRequestBean().getExpbonusrate();
		}
		if(FormulaConstant.MODAL_FREQUENCY.equals(param)){
			if(fBean.getRequestBean().getFrequency().equalsIgnoreCase(CommonConstant.ANNUAL_MODE)){
				obj=1;
			}else if(fBean.getRequestBean().getFrequency().equalsIgnoreCase(CommonConstant.SEMI_ANNUAL_MODE)){
				obj=2;
			}else if(fBean.getRequestBean().getFrequency().equalsIgnoreCase(CommonConstant.QUATERLY_MODE)){
				obj=4;
			}else if(fBean.getRequestBean().getFrequency().equalsIgnoreCase(CommonConstant.MONTHLY_MODE)){
				obj=12;
			}
		}
		//end : added by jayesh for Smart 7 : 05-05-2014
		if(FormulaConstant.PARM_MRSP_RENEWAL_PREM.equals(param)){
			obj = fBean.getMRSPRP();
		}
		if(FormulaConstant.PARM_ANNUALIZED_NSAP_PREMIUM.equals(param)){
			obj = fBean.getANSAP();
		}

        /***** Krishi Kalyan 18-05-2016 *****/
        if(FormulaConstant.PARAM_RPST_H.equals(param)){
            obj = CommonConstant.RPST_H;
        }
        if(FormulaConstant.PARAM_RPST_N.equals(param)){
            obj = CommonConstant.RPST_N;
        }
        if(FormulaConstant.PARAM_BPST_H.equals(param)){
            obj = CommonConstant.BPST_H;
        }
        if(FormulaConstant.PARAM_BPST_N.equals(param)){
            obj = CommonConstant.BPST_N;
        }
        if(FormulaConstant.PARAM_RIDST_H.equals(param)){
            obj = CommonConstant.RIDST_H;
        }
        if(FormulaConstant.PARAM_RIDST_N.equals(param)){
            obj = CommonConstant.RIDST_N;
        }
        /***** Krishi Kalyan 18-05-2016 *****/

		return obj;
	}

	private Object evalFormula(Object op1, String optyp1, Object op2, String optyp2, String oprt){
		Object obj = new Object();
		int i_obj1 =-1;
		long l_obj1 =-1;
		double d_obj1=-1;
		int i_obj2=-1;
		long l_obj2=-1;
		double d_obj2=-1;
		//CommonConstant.printLog("d","Inside evalFormula ::: " + op1+"" + " : " + optyp1 + " : " + op2+"" + " : " + optyp2 + " : " + oprt );

		if(FormulaConstant.DATA_TYPE_INT.equals(optyp1)){
			i_obj1 = Integer.parseInt(op1+"");
		}
		if(FormulaConstant.DATA_TYPE_LONG.equals(optyp1)){
			l_obj1 = Long.parseLong(op1+"");
		}
		if(FormulaConstant.DATA_TYPE_DOUBLE.equals(optyp1)){
			d_obj1 = Double.parseDouble(op1+"");
		}
		if(FormulaConstant.DATA_TYPE_INT.equals(optyp2)){
			i_obj2 = Integer.parseInt(op2+"");
		}
		if(FormulaConstant.DATA_TYPE_LONG.equals(optyp2)){
			l_obj2 = Long.parseLong(op2+"");
		}
		if(FormulaConstant.DATA_TYPE_DOUBLE.equals(optyp2)){
			d_obj2 = Double.parseDouble(op2+"");
		}

		if(i_obj1 != -1){

			if(i_obj2 != -1){
				obj = getValue(i_obj1, i_obj2, oprt);
			}else if(l_obj2 != -1){
				obj = getValue(i_obj1, l_obj2, oprt);
			}else if(d_obj2 != -1){
				obj = getValue(i_obj1, d_obj2, oprt);
			}
		}else if(l_obj1 != -1){

			if(i_obj2 != -1){
				obj = getValue(l_obj1, i_obj2, oprt);
			}else if(l_obj2 != -1){
				obj = getValue(l_obj1, l_obj2, oprt);
			}else if(d_obj2 != -1){
				obj = getValue(l_obj1, d_obj2, oprt);
			}
		}else if(d_obj1 != -1){

			if(i_obj2 != -1){
				obj = getValue(d_obj1, i_obj2, oprt);
			}else if(l_obj2 != -1){
				obj = getValue(d_obj1, l_obj2, oprt);
			}else if(d_obj2 != -1){
				obj = getValue(d_obj1, d_obj2, oprt);
			}
		}


		return obj;
	}

	private Object getValue(int obj1, int obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside in int method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MOD.equals(optr)){
			//obj = obj1 % obj2;
			obj =((obj1 % obj2)==0)?1:0;

		}else if(FormulaConstant.OPERATOR_POWER.equals(optr)){
			int val = 1;
			for(int i=1; i<=obj2 ; i++){
				val = val * obj1;
			}
			obj = val;
		}

		return obj;
	}

	private Object getValue(int obj1, long obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside in long method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}

	private Object getValue(int obj1, double obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside in double method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}
	private Object getValue(long obj1, int obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside long int method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_POWER.equals(optr)){
			long val = 1;
			for(int i=1; i<=obj2 ; i++){
				val = val * obj1;
			}
			obj = val;
		}

		return obj;
	}
	private Object getValue(long obj1, long obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside long long method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}
	private Object getValue(long obj1, double obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside long double method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}

	private Object getValue(double obj1, int obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside double int method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_POWER.equals(optr)){
			double val = 1;
			for(int i=1; i<=obj2 ; i++){
				val = val * obj1;
			}
			obj = val;
		}

		return obj;
	}
	private Object getValue(double obj1, long obj2, String optr)
	{
		//CommonConstant.printLog("d","::::::::::::::::::::::::Inside double long method:::::::::::::::::::::::");
		Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}
 private Object getValue(double obj1, double obj2, String optr)
	{
	 //CommonConstant.printLog("d","::::::::::::::::::::::::Inside double double method:::::::::::::::::::::::");
	    Object obj = new Object();
		if(FormulaConstant.OPERATOR_PLUSE.equals(optr)){
			obj = obj1 + obj2;
		}else if(FormulaConstant.OPERATOR_MINUS.equals(optr)){
			obj = obj1 - obj2;
		}else if(FormulaConstant.OPERATOR_MUL.equals(optr)){
			obj = obj1 * obj2;
		}else if(FormulaConstant.OPERATOR_DIVIDE.equals(optr)){
			obj = obj1 / obj2;
		}else if(FormulaConstant.OPERATOR_MAX.equals(optr)){
			if(obj1 > obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}else if(FormulaConstant.OPERATOR_MIN.equals(optr)){
			if(obj1 < obj2){
				obj = obj1;
			}else{
				obj = obj2;
			}
		}

		return obj;
	}
 private String getOprType(String type1, String type2){
	 String type="";

	 if(FormulaConstant.DATA_TYPE_INT.equals(type1) && FormulaConstant.DATA_TYPE_INT.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_INT;
	 }
	 if(FormulaConstant.DATA_TYPE_INT.equals(type1) && FormulaConstant.DATA_TYPE_LONG.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_LONG;
	 }
	 if(FormulaConstant.DATA_TYPE_INT.equals(type1) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_DOUBLE;
	 }
	 if(FormulaConstant.DATA_TYPE_DOUBLE.equals(type1) && FormulaConstant.DATA_TYPE_INT.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_DOUBLE;
	 }
	 if(FormulaConstant.DATA_TYPE_DOUBLE.equals(type1) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_DOUBLE;
	 }
	 if(FormulaConstant.DATA_TYPE_DOUBLE.equals(type1) && FormulaConstant.DATA_TYPE_LONG.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_DOUBLE;
	 }
	 if(FormulaConstant.DATA_TYPE_LONG.equals(type1) && FormulaConstant.DATA_TYPE_INT.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_LONG;
	 }
	 if(FormulaConstant.DATA_TYPE_LONG.equals(type1) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_DOUBLE;
	 }
	 if(FormulaConstant.DATA_TYPE_LONG.equals(type1) && FormulaConstant.DATA_TYPE_LONG.equals(type2)){
		 type = FormulaConstant.DATA_TYPE_LONG;
	 }

	 return type;
 }

 private Object evalMathFormula(Object obj1, String type1, String formula, String type2){
	 Object obj = new Object();
	 double d_obj1=0;
	 float f_obj1=0;

	 if(FormulaConstant.DATA_TYPE_DOUBLE.equals(type1)){
			d_obj1 = Double.parseDouble(obj1+"");
	 }
	 if(FormulaConstant.DATA_TYPE_FLOAT.equals(type1)){
			f_obj1 = Float.parseFloat(obj1 + "");
	 }

	 if("ROUND".equals(formula) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type1)){
		 obj = Math.round(d_obj1);
	 }
	 if("ROUND".equals(formula) && FormulaConstant.DATA_TYPE_FLOAT.equals(type1)){
		 obj = Math.round(f_obj1);
	 }

	 if("ROUND2".equals(formula) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type1)){
		 //obj = Math.round(d_obj1);
		     DecimalFormat df = new DecimalFormat("###.##");
			 String dbval=df.format(d_obj1);
			 obj=Double.parseDouble(dbval);
	 }
	 if("ROUND2".equals(formula) && FormulaConstant.DATA_TYPE_FLOAT.equals(type1)){
		// obj = Math.round(f_obj1);
		 DecimalFormat df = new DecimalFormat("###.##");
		 String dbval=df.format(f_obj1);
		 obj=Float.parseFloat(dbval);
	 }
	 if("ROUNDDOWN2".equals(formula) && FormulaConstant.DATA_TYPE_DOUBLE.equals(type1)){
		 //obj = Math.round(d_obj1);

		 BigDecimal fd = new BigDecimal(d_obj1);
		 BigDecimal cut = fd.setScale(2, RoundingMode.FLOOR);
		 obj = cut.doubleValue();
	 }

	 return obj;

 }

 public HashMap getFormulaExp(String frmID){

	 	HashMap formulaExpMap =  DataAccessClass.getformulaExp(frmID);

		return formulaExpMap;
 }

 private Object getNumberValue(String type, String operand){

	 Object obj=0;
	 if(FormulaConstant.DATA_TYPE_INT.equals(type)){
		 obj = Integer.parseInt(operand);
	 }else if(FormulaConstant.DATA_TYPE_DOUBLE.equals(type)){
		 obj = Double.parseDouble(operand);
	 }else if(FormulaConstant.DATA_TYPE_LONG.equals(type)){
		 obj = Long.parseLong(operand);
	 }

	 return obj;
 }

}
