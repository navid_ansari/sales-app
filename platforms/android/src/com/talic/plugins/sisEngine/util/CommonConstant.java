package com.talic.plugins.sisEngine.util;

import android.content.Context;
import java.io.Serializable;

@SuppressWarnings("serial")
public class CommonConstant implements Serializable{

	public static final String SISUIN = "SISUIN";
	public static final String INSNAME = "INSNAME";
	public static final String COVPINSNAME = "COVPINSNAME";//Added by Samin for Cover Page
	public static final String PROPNAME = "PROPNAME";
	public static final String INSDOB = "INSDOB";
	public static final String INSOCC = "INSOCC";
	public static final String INSDESCOCC = "INSDESCOCC";
	public static final String INSAGE = "INSAGE";
	public static final String INSGENDER = "INSGENDER";
	public static final String ER_INSGENDER = "ER_INSGENDER";
	public static final String PROPDOB = "PROPDOB";
	public static final String PROPOCC = "PROPOCC";
	public static final String PROPDESCOCC = "PROPDESCOCC";
	public static final String PROPAGE = "PROPAGE";
	public static final String PROPGENDER = "PROPGENDER";
	public static final String ER_PROPGENDER = "ER_PROPGENDER";
	public static final String PNLTERM = "PNLTERM";
	public static final String PNLPPT = "PNLPPT";
	public static final String INSSMOKER = "INSSMOKER";
	public static final String PNLMODE = "PNLMODE";
	public static final String PNLSA = "PNLSA";
	public static final String PNLMODPREM = "PNLMODPREM";
	public static final String TOTPREM = "TOTPREM";
	public static final String TOTSERVTAX = "TOTSERVTAX";
	public static final String TOTSTPREM = "TOTSTPREM";
	public static final String TablePA = "TablePA";
	public static final String TableULIPPA = "TableULIPPA";
	public static final String TableMaximaPA = "TableMaximaPA";//Added by Samina for Invest Maxima
	public static final String TableMaximaAGE = "TableMaximaAGE";//Added by Samina for Invest Maxima
	public static final String TableMaximaPEM = "TableMaximaPEM";//Added by Samina for Invest Maxima
	public static final String TableAS3_PH10MORTCHRG="TableAS3_PH10MORTCHRG"; //Added by Samina for Alternate Scenario3
	public static final String TableAGE = "TableAGE";
	public static final String TablePropAGE = "TablePropAGE";
	public static final String TableULIPAGE = "TableULIPAGE";
	public static final String TablePEM = "TablePEM";
	public static final String TableULIPPEM = "TableULIPPEM";
	public static final String TableRIDPEM = "TableRIDPEM";
	public static final String TableTOTPEM = "TableTOTPEM";
	public static final String TableCOV = "TableCOV";
	public static final String TableMATVAL = "TableMATVAL";
	public static final String TableSURRBEN = "TableSURRBEN";
	public static final String TableALTSURRBEN = "TableALTSURRBEN";//Added by Samina on 18/11/2014
	public static final String ADDR = "TABADDR";
	public static final String PROPNO = "PROPNO";
	public static final String PROPDATE = "PROPDATE";
	public static final String VERSION = "VERSION";
	public static final String ONLYVERSION = "ONLYVERSION";
	public static final String POLICYOPTION = "POLICYOPTION";

	public static final String RIDNAME = "RIDNAME";
	public static final String RIDUIN = "RIDUIN";
	public static final String RIDTERM = "RIDTERM";
	public static final String RIDPPT = "RIDPPT";
	public static final String RIDSA = "RIDSA";
	public static final String RIDMODPREM = "RIDMODPREM";


	public static final String PNLFEATURE = "PNLFEATURE";
	public static final String FMC = "FMC";
	public static final String STRIDERPREM="STRIDERPREM";
	public static final String TOTALRIDPREM="TOTALRIDPREM";
	public static final String TOTALPREMRIDST="TOTALPREMRIDST";

	public static final String PNLBASEPREM="PNLBASEPREM";
	public static final String RIDBASEPREM="RIDBASEPREM";
	public static final String PREMIUMMULTI="PREMIUMMULTI";
	public static final String PNLPM="PNLPM";
	public static final String PNLMP="PNLMP";
	public static final String TablePACVAL="PACVAL";
	public static final String TableAMTINV="TableAMTINV"; // Amount for Investment (out of premium)
	public static final String TableOBIC="TableOBIC";   // Other benifit inbuilt charges
	public static final String TableADMCHRG="TableADMCHRG";   // Policy Admin charges
	public static final String TableMORTCHRG="TableMORTCHRG";   // Mortality Charges
	public static final String TablePH6MORTCHRG="TablePH6MORTCHRG";
	public static final String TableGUARCHR="TableGUARCHR";   // Guarantee charges
	public static final String TableOTHCHRG="TableOTHCHRG";   // Other charges
	public static final String TableAPPSERTAX="TableAPPSERTAX";   // Applicable Service Tax
	public static final String TableTOTC="TableTOTC";   // Total charge
	public static final String TableCTOTAL6="TableCTOTAL6";   // Total charge for PH6
	public static final String TableRFVBFMC="TableRFVBFMC";   // Regular Fund Value Before FMC
	public static final String TableRFVPFMC="TableRFVPFMC"; // Regular Fund Value post FMC
	public static final String Table6RFVPFMC="Table6RFVPFMC";
	public static final String TableFMC="TableFMC";   // Fund Management Charges
	public static final String TableRFVAEPYBGMA="TableRFVAEPYBGMA"; // Regular Fund Value at the end of Policy year before Guaranteed Maturity Addition
	public static final String TableGMA="TableGMA";  // Guaranteed Maturity Addition
	public static final String TableTOTFUNDVAL="TableTOTFUNDVAL";  // Total Fund Value at the end of policy year
	public static final String TableSURRENDERVAL="TableSURRENDERVAL";  //  Surrender Value (net of discontinuance charge & Service Tax thereupon) at the end of the policy year**
	public static final String TableDBEP="TableDBEP";  // Death Benefit* at the end of the policy year
	public static final String TablePH6DBEP="TablePH6DBEP";  // Death Benefit* at the end of the policy year
	public static final String BASEPREM="BASEPREM"; //Added for IA flexi

	public static final String TablePH6APPSERTAX="TablePH6APPSERTAX";
	public static final String TablePH6FMC="TablePH6FMC";
	public static final String TablePH6RFVAEPYBGMA="TablePH6RFVAEPYBGMA";
	public static final String TablePH6GMA="TablePH6GMA";
	public static final String TablePH6TOTFUNDVAL="TablePH6TOTFUNDVAL";
	public static final String TablePH6SURRENDERVAL="TablePH6SURRENDERVAL";
	public static final String TablePH6RFVBFMC="TablePH6RFVBFMC";
	public static final String TableLoyaltyCharges="TableLOYALTYCHRG";
	public static final String TableMLOYALTY6="TableMLOYALTY6";//Added for loyalty bonus PH6
	public static final String TableCOMMISSION="TableCOMMISSION";
	public static final String ANNUAL_MODE = "A";
	public static final String SEMI_ANNUAL_MODE = "S";
	public static final String QUATERLY_MODE = "Q";
	public static final String MONTHLY_MODE = "M";
	public static final String SINGLE_MODE = "O";
	public static final String TableGuranteedAnnualIncome="TableGAI";
	public static final String TableGuranteedSuurValue="TableGSURRBEN";

	public static final String PNLNSAP="PNLNSAP";
	public static final String QUOTEDMODPREM="QUOTEDMODPREM";
	public static final String RENEWALMODPREM="RENEWALMODPREM";//Added by Samina on 18/11/2014

	// rider details
	public static final String RDL1="RDL1";
	public static final String RDLSA1="RDLSA1";
	public static final String RDLTERM1="RDLTERM1";
	public static final String RDLMODPREM1="RDLMODPREM1";

	public static final String RDL2="RDL2";
	public static final String RDLSA2="RDLSA2";
	public static final String RDLTERM2="RDLTERM2";
	public static final String RDLMODPREM2="RDLMODPREM2";

	public static final String RDL3="RDL3";
	public static final String RDLSA3="RDLSA3";
	public static final String RDLTERM3="RDLTERM3";
	public static final String RDLMODPREM3="RDLMODPREM3";

	public static final String Table2PA = "Table2PA";
	public static final String Table2AGE = "Table2AGE";
	public static final String Table2PEM = "Table2PEM";
	public static final String Table2RIDPEM = "Table2RIDPEM";
	public static final String Table2TOTPEM = "Table2TOTPEM";
	public static final String Table2COV = "Table2COV";
	public static final String Table2MATVAL = "Table2MATVAL";
	public static final String Table2SURRBEN = "Table2SURRBEN";
	public static final String Table2GuranteedAnnualIncome="Table2GAI";
	public static final String Table2GuranteedSuurValue="Table2GSURRBEN";

	public static final String TableTOTMATVAL = "TableTOTMATVAL";
	public static final String Table2TOTMATVAL = "Table2TOTMATVAL";
	public static final String Table4VSRBONUS="Table4VSRBONUS";
	public static final String Table8VSRBONUS="Table8VSRBONUS";

	public static final String  Table4TERMINALBONUS="Table4TERMINALBONUS";
	public static final String  Table8TERMINALBONUS="Table8TERMINALBONUS";

	public static final String Table4TOTALDEATHBENEFIT="Table4TDB";
	public static final String Table8TOTALDEATHBENEFIT="Table8TDB";

	public static final String Table8SURRBEN = "Table8SURRBEN";


	/** Vikas - Added for Mahalife Gold & Gold Plus */
	public static final String MLG_TablePA = "MLG_TablePA";
	public static final String MLG_TableAGE = "MLG_TableAGE";
	public static final String MLG_TablePEM = "MLG_TablePEM";
	public static final String MLG_TableRIDPEM = "MLG_TableRIDPEM";
	public static final String MLG_TableTOTPEM = "MLG_TableTOTPEM";
	public static final String MLG_TableCOV = "MLG_TableCOV";
	public static final String MLG_TableMATVAL = "MLG_TableMATVAL";
	public static final String TableALTSCEMATVAL = "TableALTSCEMATVAL";//Added by Samina on 13/11/2014
	public static final String MLG_TableGSURRBEN = "MLG_TableGSURRBEN";
	public static final String TABLEALTGSURRBEN = "TABLEALTGSURRBEN";//Added by Samina on 13/11/2014
	public static final String MLG_TABLEGAI = "MLG_TABLEGAI";
	public static final String SGP_TABLEGAI = "SGP_TABLEGAI";
	public static final String TABLEALTGAI = "TABLEALTGAI";
	public static final String TABLEALTSCEGAI = "TABLEALTSCEGAI";
	public static final String MLG_TABLE4ANNCASH = "MLG_TABLE4ANNCASH";
	public static final String MLG_TABLE8ANNCASH = "MLG_TABLE8ANNCASH";
	public static final String MLG_TableNG4SURRBEN = "MLG_TableNG4SURRBEN";
	public static final String MLG_TableNG8SURRBEN = "MLG_TableNG8SURRBEN";
	public static final String MLG_Table4TOTALDEATHBENEFIT = "MLG_Table4TOTALDEATHBENEFIT";
	public static final String MLG_Table8TOTALDEATHBENEFIT = "MLG_Table8TOTALDEATHBENEFIT";
	public static final String MLG_TOT_CASH_DIV_4 = "MLG_TOT_CASH_DIV_4";
	public static final String MLG_TOT_CASH_DIV_8 = "MLG_TOT_CASH_DIV_8";
	public static final String MLG_TOT_G_ANNCOUP = "MLG_TOT_G_ANNCOUP";

	public static final String MBP_TABLEGAI = "MBP_TABLEGAI";

	//public static final String MLG_TableSURRBEN = "MLG_TableSURRBEN";
	//public static final String MLG_TableGuranteedSuurValue="MLG_TableGSURRBEN";
	//public static final String MLG_TableGuranteedAnnualIncome="MLG_TableGAI";
	public static final String MLG_TableGuranteedInflationCover="MLG_TableIPC";
	public static final String MLG_TableDeathBenefit="MLG_TableTDB";
	public static final String MLG_Table4VSRBONUS="MLG_Table4VSRBONUS";
	public static final String MLG_Table8VSRBONUS="MLG_Table8VSRBONUS";
	public static final String TableAlt8VSRBONUS="TableAlt8VSRBONUS";//Added by Samina for reduced paid on 20/11/2014
	public static final String TableAlt4VSRBONUS="TableAlt4VSRBONUS";//Added by Samina for reduced paid on 20/11/2014
	public static final String ALTSCE_CURRATEPREMPAIDDURING="ALTSCE_CURRATEPREMPAIDDURING";//Added by Samina for reduced paid on 20/11/2014
	/** Vikas - Added for Mahalife Gold & Gold Plus */

	public static final String TF_AAATOTFUNDVAL="TF_AAATOTFUNDVAL";
	public static final String TF_LCEF_AAATOTFUNDVAL="TF_LCEF_AAATOTFUNDVAL";//Added by Samina on 24/06/2014
	public static final String TF_WLEF_AAATOTFUNDVAL="TF_WLEF_AAATOTFUNDVAL";//Added by Samina on 24/06/2014
	public static final String S_AAASURRENDERVAL="S_AAASURRENDERVAL";
	public static final String D_AAADBEP="D_AAADBEP";
	public static final String M_AAAMORTCHRG="M_AAAMORTCHRG";
	public static final String O_AAATOTC="O_AAATOTC";
	public static final String AAA_APPSERTAX="AAA_APPSERTAX";
	public static final String R_AAARFVBFMC="R_AAARFVBFMC";
	public static final String F_AAAFMC="F_AAAFMC";
	public static final String P_AAARFVPFMC="P_AAARFVPFMC";
	public static final String L_AAALOYALTYCHRG="L_AAALOYALTYCHRG";
	public static final String TF_PH6AAATOTFUNDVAL="TF_PH6AAATOTFUNDVAL";
	public static final String TF_LCEF_PH6AAATOTFUNDVAL="TF_LCEF_PH6AAATOTFUNDVAL";//Added by Samina on 24/06/2014
	public static final String TF_WLEF_PH6AAATOTFUNDVAL="TF_WLEF_PH6AAATOTFUNDVAL";//Added by Samina on 24/06/2014
	public static final String S_PH6AAASURRENDERVAL="S_PH6AAASURRENDERVAL";
	public static final String D_PH6AAADBEP="D_PH6AAADBEP";
	public static final String M_PH6AAAMORTCHRG="M_PH6AAAMORTCHRG";
	public static final String O_PH6AAATOTC="O_PH6AAATOTC";
	public static final String AAA_PH6APPSERTAX="AAA_PH6APPSERTAX";
	public static final String R_PH6AAARFVBFMC="R_PH6AAARFVBFMC";
	public static final String F_PH6AAAFMC="F_PH6AAAFMC";
	public static final String P_PH6AAARFVPFMC="P_PH6AAARFVPFMC";
	public static final String L_PH6AAALOYALTYCHRG="L_PH6AAALOYALTYCHRG";
	public static final String RPANNUAL="RPANNUAL";							//added by jayesh for Smart 7 21-05-2014

	//Added for Fund Performance sheet - Mohammad Rashid 10-Jun-2014
	public static final String TableFPRPFVEP="TableFPRPFVEP";
	public static final String TableFPLOYALTY="TableFPLOYALTY";
	public static final String TableFPSURRVEP="TableFPSURRVEP";
	public static final String TableFPDBEP="TableFPDBEP";
	//EndFund Performance sheet


	public static final String TableTopUPPrem = "TableTopUPPrem";
	public static final String TablePartialW = "TablePartialW";
	public static final String TableRPFVTUFVPFMC = "TableRPFVTUFVPFMC";
	public static final String TableTopUpLoyalty = "TableTopUpLoyalty";
	public static final String TableROFVTUFVEOY = "TableROFVTUFVEOY";
	public static final String TableTopUpSURRENDERVAL = "TableTopUpSURRENDERVAL";
	public static final String TableTopUpDBEP = "TableTopUpDBEP";

	public static final String TableSMART8RPFVE = "TableSMART8RPFVE";
	public static final String TableSMART8SURRENDERVAL = "TableSMART8SURRENDERVAL";
	public static final String TableSMART8DBEP = "TableSMART8DBEP";
	public static final String TableSMART4RPFVE = "TableSMART4RPFVE";
	public static final String TableSMART4SURRENDERVAL = "TableSMART4SURRENDERVAL";
	public static final String TableSMART4DBEP = "TableSMART4DBEP";
	public static final String TableSMART10MORTCHRG = "TableSMART10MORTCHRG";

	public static final String TableSMART10TOTC = "TableSMART10TOTC";
	public static final String TableSMART10APPSERTAX = "TableSMART10APPSERTAX";
	public static final String TableSMART10RFVBFMC = "TableSMART10RFVBFMC";
	public static final String TableSMART10FMC = "TableSMART10FMC";
	public static final String TableSMART10RFVPFMC = "TableSMART10RFVPFMC";
	public static final String TableSMART10LOYALTYCHRG = "TableSMART10LOYALTYCHRG";

	public static final String TableSMART6MORTCHRG = "TableSMART6MORTCHRG";

	public static final String TableSMART6TOTC = "TableSMART6TOTC";
	public static final String TableSMART6APPSERTAX = "TableSMART6APPSERTAX";
	public static final String TableSMART6RFVBFMC = "TableSMART6RFVBFMC";
	public static final String TableSMART6FMC = "TableSMART6FMC";
	public static final String TableSMART6RFVPFMC = "TableSMART6RFVPFMC";
	public static final String TableSMARTLOYALTYCHRG = "TableSMART6LOYALTYCHRG";
	public static final String PACPPTVAL="PACPPTVAL";
	public static final String PNLSEMIPREM="PNLSEMIPREM";
	public static final String PNLANNUALPREM="PNLANNUALPREM";
	public static final String PNLQUARTERPREM="PNLQUARTERPREM";
	public static final String PNLMONTHLYRPREM="PNLMONTHLYRPREM";
	public static final String HEADERTOPUP="HEADERTOPUP";
    //Start : added by jayesh for Smart7 : 05-05-2014
	public static final String SMART7_TableRIDPEM = "SMART7_TableRIDPEM";
	public static final String RPSEMIANNUAL="RPSEMIANNUAL";
	public static final String RPQUARTERLY="RPQUARTERLY";
	public static final String RPMONTHLY="RPMONTHLY";
	public static final String RPUYEAR="RPUYEAR";
	public static final String TablePREMPAIDDURINGYR = "TablePREMPAIDDURINGYR";
	public static final String TablePREMPAIDCUMULATIVE = "TablePREMPAIDCUMULATIVE";
	public static final String ALTSCELIFECOV = "ALTSCELIFECOV";
	public static final String SCENARIOLIFECOV = "SCENARIOLIFECOV";	//added by Samina on 11/11/2014
	public static final String ALTSCEMATVAL = "ALTSCEMATVAL";
	public static final String TOTALTSCEMATVAL = "TOTALTSCEMATVAL";//Added by Samina on 13/11/2014
	public static final String TableTAXMATVAL = "TableTAXMATVAL";//Added by Samina on 18/11/2014
	public static final String ALTSCE_Table4TOTALDEATHBENEFIT="ALTSCE_Table4TDB";
	public static final String ALTSCE_Table8TOTALDEATHBENEFIT="ALTSCE_Table8TDB";
	public static final String ALTSCE_MATVAL4 = "ALTSCE_MATVAL4";
	public static final String ALTSCE_MATVAL8 = "ALTSCE_MATVAL8";
	public static final String ALTSCE_PREMPAIDDURINGYR = "ALTSCE_PREMPAIDDURINGYR";
	public static final String ALTSCE_PREMPAIDCUMULATIVE = "ALTSCE_PREMPAIDCUMULATIVE";
	public static final String ALTSCE_TOTMATBEN_CURRATE = "ALTSCE_TOTMATBEN_CURRATE";
	public static final String ALTSCE_SURRBEN_CURRATE = "ALTSCE_SURRBEN_CURRATE";
	public static final String ALTSCE_CURRATEPREMPAIDCUMULATIVE_CURRATE = "ALTSCE_CURRATEPREMPAIDCUMULATIVE_CURRATE";
	public static final String Table_ALTSCE_TAXSAVING = "Table_ALTSCE_TAXSAVING";
	public static final String ALTSCE_ANN_EFF_PREM = "ALTSCE_ANN_EFF_PREM";
	public static final String ALTSCE_CUMMULATIVE_EFF_PREM = "ALTSCE_CUMMULATIVE_EFF_PREM";
	public static final String ALTSCE_TOT4MATBEN_TAX = "ALTSCE_TOT4MATBEN_TAX";
	public static final String BASEPLANTOTSERVTAX = "BASEPLANTOTSERVTAX";
	public static final String TOTMODALPREMPAYABLE = "TOTMODALPREMPAYABLE";
	public static final String RPSTANNUAL="RPSTANNUAL";
	public static final String RPSTSEMIANNUAL="RPSTSEMIANNUAL";
	public static final String RPSTQUARTERLY="RPSTQUARTERLY";
	public static final String RPSTMONTHLY="RPSTMONTHLY";
	public static final String TOTRPSEMIANNUAL="TOTRPSEMIANNUAL";
	public static final String TOTRPQUARTERLY="TOTRPQUARTERLY";
	public static final String TOTRPMONTHLY="TOTRPMONTHLY";
	public static final String TOTRPANNUAL="TOTRPANNUAL";
	public static final String Table4SURRBEN = "Table4SURRBEN";
	public static final String ALTSCE_VBONUS="ALTSCE_VBONUS";
	public static final String ALTSCE_TOTDB_CURRATE="ALTSCE_TOTDB_CURRATE";
	public static final String Table8MATVAL="Table8MATVAL";
	public static final String MLG_Table8MATVAL = "MLG_Table8MATVAL"; //added by Ganesh for MLG 24-Nov-2015
	public static final String Table4MATVAL="Table4MATVAL";
	public static final String MLG_Table4MATVAL="MLG_Table4MATVAL"; //added by Ganesh for MLG 24-Nov-2015
	public static final String SMART7_8SURRBEN = "SMART7_8SURRBEN";
	public static final String RDLDYNTERM1="RDLDYNTERM1";
	public static final String RDLDYNTERM2="RDLDYNTERM2";
	public static final String EXPBONUSRATE="EXPBONUSRATE";
	public static final String TAXSLAB="TAXSLAB";
	public static final String RDLUIN1 = "RDLUIN1";
	public static final String RDLUIN2 = "RDLUIN2";
	//End : added by jayesh for Smart7 : 05-05-2014
	public static final String AGEPROOF="AGEPROOF";
	public static final String Illustration="Illustration";
	public static final String ADDLRider="ADDL Rider";
	public static final String WOPRider="WOP Rider";
	public static final String WOPPRider="WOPP Rider";
	public static final String ReducedPaidUP="Reduced Paid UP";
	public static final String ExperienceBasedRate="Experience Based Rate";
	public static final String TaxBenefit="Tax Benefit";
	public static final String ExperiencebasedrateandTaxbenefit="Experience based rate and Tax benefit";
	public static final String TOPUPPWD="TOPUP AND PWD";
	public static final String SMART="SMART";
	public static final String AAA="AAA";
	public static final String INITDFF="INITDFF";
	public static final String INITEFF="INITEFF";
	public static final String SMDB1="SMDB1";
	public static final String SMDB2="SMDB2";
	public static final String ILLUSPAGE="ILLUSPAGE";
	public static final String ACTUALFP="ACTUALFP";
	public static final String NETYIELD8P="NETYIELD8P";
	public static final String NETYIELD8TW="NETYIELD8TW";
	public static final String NETYIELD8SMART="NETYIELD8SMART";
	public static final String NEGERROR="NEGERROR";
	public static final String RIDERTEXT="RIDERTEXT";
	public static final String RIDERText="** The benefit equals all future premiums payable under the base policy from the date of claim for a period as specified in the policy contract. Please refer the policy contract for more details.";
	public static final String COMISSIONTEXT="COMISSIONTEXT";
	public static final String COMISSIONLITEXT="COMISSIONLITEXT";
	public static final String TOTALRB8="TOTALRB8";
	public static final String TOTALRB4="TOTALRB4";
	public static final String TOTALGAI="TOTALGAI";
	public static final String TAXTEXT="TAXTEXT";
	public static final String TAXMMTEXT="TAXMMTEXT";
	public static final String TAXALTTEXT="TAXALTTEXT";
	public static final String TAX23TEXT="TAX23TEXT";
	public static final String Exp2Rate="Exp2Rate";
	public static final String ExpRate="ExpRate";
	public static final String TableANNUITYRATE="TableANNUITYRATE";
	public static final String TableAMT_ANNUITY="TableAMT_ANNUITY";
	public static final String TableERPA = "TableERPA";//Added by Samina for Easy Retire on 28/01/2015
	public static final String TableERCOV = "TableERCOV";
	public static final String TableERAGE = "TableERAGE";
	public static final String MLG_TableERPURP = "MLG_TableERPURP";
	public static final String TOTER_STPREM = "TOTER_STPREM";
	public static Context APP_CONTEXT;

	public static final String ADDLRID = "ADDLRID"; //Adding Rider Disclaimer Dynamically
	public static final double ServiceTax = 0.14;
	public static final String PNLCUCOMBOBASEPREM="PNLCUCOMBOBASEPREM";
	public static final String PNLUCOMBOPREM = "PNLUCOMBOPREM";
	public static final String TableComboGuranteedAnnualIncome = "TableComboGAI";
	public static final String Table15GAI = "Table15GAI";
	public static final String TableComboTOTMATVAL = "TableComboTOTMATVAL";
	public static final String TableComboTOTFUNDVAL = "TableComboTOTFUNDVAL";
	public static final String TableComboPH6TOTFUNDVAL = "TableComboPH6TOTFUNDVAL";
	public static final String TableCOMBODBEP = "TableCOMBODBEP";
	public static final String TableCOMBOPH6DBEP = "TableCOMBOPH6DBEP";
	public static final String TableFinalTOTMATVAL = "TableFinalTOTMATVAL";
	public static final String  TableFPSEC7_FinalTOTMATVAL="TableFPSEC7_FinalTOTMATVAL";
	public static final String Table15TOTFUNDVAL = "Table15TOTFUNDVAL";
	public static final String PNLCOMBOSA = "PNLCOMBOSA";
	public static final String TableCombo1COV = "TableCombo1COV";
	public static final String TableCombo2COV = "TableCombo2COV";
	public static final String TableCOMBOPEM = "TableCOMBOPEM";
	public static final String TableComboSURRENDERVAL = "TableComboSURRENDERVAL";
	public static final String TableComboPH6SURRENDERVAL = "TableComboPH6SURRENDERVAL";
	public static final String TableCombo1SURRBEN = "TableCombo1SURRBEN";
	public static final String Table15ULIPPA = "Table15ULIPPA";
	public static final String Table15ULIPAGE = "Table15ULIPAGE";
	public static final String Table15ULIPPEM = "Table15ULIPPEM";
	public static final String PAC15PPTVAL = "PAC15PPTVAL";
	public static final String Table15AMTINV = "Table15AMTINV";
	public static final String Table15ADMCHRG = "Table15ADMCHRG";
	public static final String Table15MORTCHRG = "Table15MORTCHRG";
	public static final String Table15PH6MORTCHRG = "Table15PH6MORTCHRG";
	public static final String Table15OTHCHRG = "Table15OTHCHRG";
	public static final String Table15APPSERTAX = "Table15APPSERTAX";
	public static final String Table15TOTC = "Table15TOTC";
	public static final String Table15CTOTAL6 = "Table15CTOTAL6";
	public static final String Table15FMC = "Table15FMC";
	public static final String Table15SURRENDERVAL = "Table15SURRENDERVAL";
	public static final String Table15DBEP = "Table15DBEP";
	public static final String Table15PH6DBEP = "Table15PH6DBEP";
	public static final String Table15PH6APPSERTAX = "Table15PH6APPSERTAX";
	public static final String Table15PH6FMC = "Table15PH6FMC";
	public static final String Table15PH6TOTFUNDVAL = "Table15PH6TOTFUNDVAL";
	public static final String Table15PH6SURRENDERVAL = "Table15PH6SURRENDERVAL";
	public static final String Table15MLOYALTY6 = "Table15MLOYALTY6";
	public static final String Table15COMMISSION = "Table15COMMISSION";
	public static final String Table15PTTOTFUNDVAL = "Table15PTTOTFUNDVAL";
	public static final String Table15LOYALTYCHRG = "Table15LOYALTYCHRG";
	public static final String TOTCOMBO1SERVTAX = "TOTCOMBO1SERVTAX";
	public static final String TOTCOMBO2SERVTAX = "TOTCOMBO2SERVTAX";
	public static final String TOTCOMBO1STPREM = "TOTCOMBO1STPREM";
	public static final String TOTCOMBO2STPREM = "TOTCOMBO2STPREM";
	public static final String RPCOMBO1STANNUAL = "RPCOMBO1STANNUAL";
	public static final String RPCOMBO1STSEMIANNUAL = "RPCOMBO1STSEMIANNUAL";
	public static final String RPCOMBO1STQUARTERLY = "RPCOMBO1STQUARTERLY";
	public static final String RPCOMBO1STMONTHLY = "RPCOMBO1STMONTHLY";
	public static final String TOTCOMBO1RPSEMIANNUAL = "TOTCOMBO1RPSEMIANNUAL";
	public static final String TOTCOMBO1RPQUARTERLY = "TOTCOMBO1RPQUARTERLY";
	public static final String TOTCOMBO1RPMONTHLY = "TOTCOMBO1RPMONTHLY";
	public static final String TOTCOMBO1RPANNUAL = "TOTCOMBO1RPANNUAL";
	public static final String PNLCOMBOBASEPREM = "PNLCOMBOBASEPREM";
	public static final String PNLCOMBOMLSBASEPREM = "PNLCOMBOMLSBASEPREM";
	public static final String PNLCOMBOCMODPREM = "PNLCOMBOCMODPREM";
	public static final String PNLCOMBOUMODPREM = "PNLCOMBOUMODPREM";
	public static final String TableComboCPEM = "TableComboCPEM";
	public static final String RENEWALCOMBOCMODPREM = "RENEWALCOMBOCMODPREM";
	public static final String RPCOMBOCMONTHLY = "RPCOMBOCMONTHLY";
	public static final String RPCOMBOCSEMIANNUAL = "RPCOMBOCSEMIANNUAL";
	public static final String PNLCOMBO2BASEPREM = "PNLCOMBO2BASEPREM";
	public static final String PNLFPSEC_CUCOMBOBASEPREM="PNLFPSEC_CUCOMBOBASEPREM";
	public static final String TableCOMBOFPSec7PEM = "TableCOMBOFPSec7PEM";
	public static final String Table10GAI = "Table10GAI";
	public static final String TableFPSEC7_ULIPPA = "TableFPSEC7_ULIPPA";
	public static final String TableFPSEC7_ULIPAGE = "TableFPSEC7_ULIPAGE";
	public static final String TableFPSEC7_ComboCPEM = "TableFPSEC7_ComboCPEM";
	public static final String TableFPSEC7_Combo1COV = "TableFPSEC7_Combo1COV";
	public static final String MLGFPSEC7_TABLEGAI = "MLGFPSEC7_TABLEGAI";
	public static final String MLGFPSEC7_TableMATVAL = "MLGFPSEC7_TableMATVAL";
	public static final String TableFPSEC7_ComboTOTMATVAL = "TableFPSEC7_ComboTOTMATVAL";
	public static final String MLGFPSEC7_TableGSURRBEN = "MLGFPSEC7_TableGSURRBEN";
	public static final String TableFPSEC7_Combo1SURRBEN = "TableFPSEC7_Combo1SURRBEN";
	public static final String PNLFPSEC7_TERM = "PNLFPSEC7_TERM";
	public static final String PNLFPSEC7_UCOMBOPREM = "PNLFPSEC7_UCOMBOPREM";
	public static final String PNLFPSEC7_COMBOCMODPREM = "PNLFPSEC7_COMBOCMODPREM";
	public static final String MLGFPSEC7_TableRIDPEM = "MLGFPSEC7_TableRIDPEM";
	public static final String RENEWALFPSEC7_COMBOCMODPREM = "RENEWALFPSEC7_COMBOCMODPREM";
	public static final String RPFPSEC7_COMBO1STANNUAL = "RPFPSEC7_COMBO1STANNUAL";
	public static final String TOTFPSEC7_COMBO1RPANNUAL = "TOTFPSEC7_COMBO1RPANNUAL";
	public static final String RPFPSEC7_COMBOCMONTHLY = "RPFPSEC7_COMBOCMONTHLY";
	public static final String RPFPSEC7_COMBO1STMONTHLY = "RPFPSEC7_COMBO1STMONTHLY";
	public static final String TOTFPSEC7_COMBO1RPMONTHLY = "TOTFPSEC7_COMBO1RPMONTHLY";
	public static final String TOTFPSEC7_COMBO1RPSEMIANNUAL = "TOTFPSEC7_COMBO1RPSEMIANNUAL";
	public static final String RPFPSEC7_COMBO1STSEMIANNUAL = "RPFPSEC7_COMBO1STSEMIANNUAL";
	public static final String RPFPSEC7_COMBOCSEMIANNUAL = "RPFPSEC7_COMBOCSEMIANNUAL";
	public static final String SERVICE_TAX_MAIN = "SERVICE_TAX";
	public static final String SERVICE_TAX_FIRST_YEAR = "FIRST_YEAR_ST";
	public static final String SERVICE_TAX_RENEWAL = "RENEWAL_ST";
	public static final String PNLCOMBO2BP = "PNLCOMBO2BP";
	public static final String PNLTOTCOMBOBASEPREM = "PNLTOTCOMBOBASEPREM";
	public static final String PNLTOTCOMBOSA = "PNLTOTCOMBOSA";
	public static final String TableCOMBOSGPPEM = "TableCOMBOSGPPEM";
	public static final String TableSGPDBEP = "TableSGPDBEP";
	public static final String TableSGPPH6DBEP = "TableSGPPH6DBEP";
	public static final String TableSGPPA = "TableSGPPA";
	public static final String TableSGP8MATVAL = "TableSGP8MATVAL";
	public static final String TableSGP4MATVAL = "TableSGP4MATVAL";
	public static final String TableSGPAGE = "TableSGPAGE";
	public static final String TableSGPNG8SURRBEN = "TableSGPNG8SURRBEN";
	public static final String TableComboSGPSURRBEN = "TableComboSGPSURRBEN";
	public static final String TableSGP8FinalTOTMATVAL = "TableSGP8FinalTOTMATVAL";
	public static final String PNLSGP1PREM = "PNLSGP1PREM";
	public static final String PNLSGP2PREM = "PNLSGP2PREM";
	public static final String PNLSGP1MODPREM = "PNLSGP1MODPREM";
	public static final String PNLSGP2MODPREM = "PNLSGP2MODPREM";
	public static final String TableSGPRIDPEM = "TableSGPRIDPEM";
	public static final String TableSGPSURRBEN = "TableSGPSURRBEN";
	public static final String TableSGPTOTPEM = "TableSGPTOTPEM";
	public static final String TableSGPCOV = "TableSGPCOV";
	public static final String RENEWALSGPMRS_COMBOCMODPREM = "RENEWALSGPMRS_COMBOCMODPREM";
	public static final String RPSGPMRS_COMBO1STANNUAL = "RPSGPMRS_COMBO1STANNUAL";
	public static final String TOTSGPMRS_COMBO1RPANNUAL = "TOTSGPMRS_COMBO1RPANNUAL";
	public static final String RPSGPMRS_COMBOCSEMIANNUAL = "RPSGPMRS_COMBOCSEMIANNUAL";
	public static final String RPSGPMRS_COMBO1STSEMIANNUAL = "RPSGPMRS_COMBO1STSEMIANNUAL";
	public static final String TOTSGPMRS_COMBO1RPSEMIANNUAL = "TOTSGPMRS_COMBO1RPSEMIANNUAL";
	public static final String TableSGPCOMBO1_ComboCPEM = "TableSGPCOMBO1_ComboCPEM";
	public static final String RENEWALMRSCOMBO2_COMBOCMODPREM = "RENEWALMRSCOMBO2_COMBOCMODPREM";
	public static final String RPMRSCOMBO2_COMBO2STANNUAL = "RPMRSCOMBO2_COMBO2STANNUAL";
	public static final String TOTMRSCOMBO2_COMBO2RPANNUAL = "TOTMRSCOMBO2_COMBO2RPANNUAL";
	public static final String RPMRSCOMBO2_COMBOCSEMIANNUAL = "RPMRSCOMBO2_COMBOCSEMIANNUAL";
	public static final String RPMRSCOMBO2_COMBO2STSEMIANNUAL = "RPMRSCOMBO2_COMBO2STSEMIANNUAL";
	public static final String TOTMRSCOMBO2_COMBO2RPSEMIANNUAL = "TOTMRSCOMBO2_COMBO2RPSEMIANNUAL";
	public static final String RPMRSCOMBO2_COMBOCMONTHLY = "RPMRSCOMBO2_COMBOCMONTHLY";
	public static final String RPMRSCOMBO2_COMBO2STMONTHLY = "RPMRSCOMBO2_COMBO2STMONTHLY";
	public static final String TOTMRSCOMBO2_COMBO2RPMONTHLY = "TOTMRSCOMBO2_COMBO2RPMONTHLY";
	public static final String PNLSGPMRS_COMBOCMODPREM = "PNLSGPMRS_COMBOCMODPREM";
	public static final String SGP_TableMATVAL = "SGP_TableMATVAL";
	public static final String PNSGPPLSA = "PNSGPPLSA";
	public static final String TableULIPIOPA = "TableULIPIOPA";
	public static final String TableIOCOMMISSION = "TableIOCOMMISSION";
	public static final String TableIOMLOYALTY6 = "TableIOMLOYALTY6";
	public static final String TableIOLOYALTYCHRG = "TableIOLOYALTYCHRG";
	public static final String TableIOPH6RFVBFMC = "TableIOPH6RFVBFMC";
	public static final String TableIOPH6SURRENDERVAL = "TableIOPH6SURRENDERVAL";
	public static final String TableIOPH6TOTFUNDVAL = "TableIOPH6TOTFUNDVAL";
	public static final String TableIOPH6FMC = "TableIOPH6FMC";
	public static final String TableIOPH6APPSERTAX = "TableIOPH6APPSERTAX";
	public static final String TableIOPH6DBEP = "TableIOPH6DBEP";
	public static final String TableIODBEP = "TableIODBEP";
	public static final String TableIOSURRENDERVAL = "TableIOSURRENDERVAL";
	public static final String TableIOTOTFUNDVAL = "TableIOTOTFUNDVAL";
	public static final String TableIOFMC = "TableIOFMC";
	public static final String TableIORFVPFMC = "TableIORFVPFMC";
	public static final String TableIO6RFVPFMC = "TableIO6RFVPFMC";
	public static final String TableIORFVBFMC = "TableIORFVBFMC";
	public static final String TableIOCTOTAL6 = "TableIOCTOTAL6";
	public static final String TableIOTOTC = "TableIOTOTC";
	public static final String TableIOAPPSERTAX = "TableIOAPPSERTAX";
	public static final String TableIOOTHCHRG = "TableIOOTHCHRG";
	public static final String TableIOPH6MORTCHRG = "TableIOPH6MORTCHRG";
	public static final String TableIOMORTCHRG = "TableIOMORTCHRG";
	public static final String TableIOADMCHRG = "TableIOADMCHRG";
	public static final String TableIOAMTINV = "TableIOAMTINV";
	public static final String PACIOPPTVAL = "PACIOPPTVAL";
	public static final String TableULIPIOAGE = "TableULIPIOAGE";
	public static final String TableULIPIOPEM = "TableULIPIOPEM";
	public static final String TableCUMULATIVE_RPFG = "TableCUMULATIVE_RPFG";
	public static final String MRS_TableRIDPEM = "MRS_TableRIDPEM";


	public static final String ANNUAL_TOTALRIDPREM="ANNUAL_TOTALRIDPREM";
	public static final String ANNUAL_QUOTEDMODPREM="ANNUAL_QUOTEDMODPREM";
	public static final String Table2MRSPA = "Table2MRSPA";
	public static final String Table2MRSAGE = "Table2MRSAGE";
	public static final String Table2MRSPEM = "Table2MRSPEM";
	public static final String Table2MRSRIDPEM = "Table2MRSRIDPEM";
	public static final String Table2MRSTOTPEM = "Table2MRSTOTPEM";
	public static final String Table2MRSCOV = "Table2MRSCOV";
	public static final String Table2MRSMATVAL = "Table2MRSMATVAL";
	public static final String Table2MRSSURRBEN = "Table2MRSSURRBEN";
	public static final String TableMRSPA = "TableMRSPA";
	public static final String TableMRSAGE = "TableMRSAGE";
	public static final String TableMRSPEM = "TableMRSPEM";
	public static final String TableMRSTOTPEM = "TableMRSTOTPEM";
	public static final String TableMRSCOV = "TableMRSCOV";
	public static final String TableMRSSURRBEN = "TableMRSSURRBEN";
    public static final String TableMRSRIDPEM = "TableMRSRIDPEM";

	public static final String MRSTablePA = "MRSTablePA";
	public static final String MRSTableAGE = "MRSTableAGE";
	public static final String MRSTablePEM = "MRSTablePEM";
	public static final String MRSTableRIDPEM = "MRSTableRIDPEM";
	public static final String MRSTableTOTPEM = "MRSTableTOTPEM";
	public static final String MRSTableCOV = "MRSTableCOV";
	public static final String MRSTableSURRBEN = "MRSTableSURRBEN";
	public static final String PNLBENTYPE = "PNLBENTYPE";
	public static final String Table8TDB = "Table8TDB";
	public static final String Table4TDB = "Table4TDB";
	public static final String AGENTNAME = "AGENTNAME";
	public static final String AGENTNUMBER = "AGENTNUMBER";
	public static final String AGENTCONTACTNUMBER = "AGENTCONTACTNUMBER";
	public static final String DATE = "DATE";
	public static final String PROPOSERNAME = "PROPOSERNAME";
	public static final String IMAGE = "IMAGE";
	public static final String WITHOUT_SALES = "WITHOUT_SALES";
	public static final String IMGNAME = "IMGNAME";
	public static final String TableFRNG8SURRBEN = "TableFRNG8SURRBEN";
	public static final String TableFRNG4SURRBEN = "TableFRNG4SURRBEN";
	public static final String TableFRGSURRBEN = "TableFRGSURRBEN";
	public static final String TableFR8VSRBONUS = "TableFR8VSRBONUS";
	public static final String TableFR8TOTALDEATHBENEFIT = "TableFR8TOTALDEATHBENEFIT";
	public static final String TableFR4TOTALDEATHBENEFIT = "TableFR4TOTALDEATHBENEFIT";
	public static final String TableFR4VSRBONUS = "TableFR4VSRBONUS";
	public static final String TableFR4MATVAL = "TableFR4MATVAL";
	public static final String TableFR4TERMINALBONUS = "TableFR4TERMINALBONUS";
	public static final String TableFR8TERMINALBONUS = "TableFR8TERMINALBONUS";
	public static final String TableFR8MATVAL = "TableFR8MATVAL";
	public static final String MIP_TableRIDPEM = "MIP_TableRIDPEM";
	public static final String MIP_TableTOTPEM = "MIP_TableTOTPEM";
	public static final String MLG_TableBasicSum = "MLG_TableBasicSum";
	public static final String TableSIPSSURRBEN = "TableSIPSSURRBEN";
	public static final String TOTGUARBENEFIT = "TOTGUARBENEFIT";
	public static final String WPPNOTE = "WPPNOTE";
	public static final String GK_TableMILESTONEADD8 = "GK_TableMILESTONEADD8";
	public static final String GK_TableMILESTONEADD4 = "GK_TableMILESTONEADD4";
	public static final String GK_TableMBB = "GK_TableMBB";
	//public static final String Table4COV_GK = "Table4COV_GK";

	/*GIP*/
	public static final String GIP_TablePA = "GIP_TablePA";
	public static final String GIP_TableAGE = "GIP_TableAGE";
	public static final String GIP_TablePEM = "GIP_TablePEM";
	public static final String GIP_TableRIDPEM = "GIP_TableRIDPEM";
	public static final String GIP_TableTOTPEM = "GIP_TableTOTPEM";
	public static final String GIP_TableCOV = "GIP_TableCOV";
	public static final String GIP_TableMATVAL = "GIP_TableMATVAL";
	public static final String GIP_TABLEGAI = "GIP_TABLEGAI";
	public static final String TOTGIPGUARBENEFIT = "TOTGIPGUARBENEFIT";
	public static final String GIP_TableGSURRBEN = "GIP_TableGSURRBEN";
	public static final String TableGIPSSURRBEN = "TableGIPSSURRBEN";
	public static final String COVERPAGE = "COVERPAGE";
	public static final String FIRSTPAGE = "FIRSTPAGE";

    /***** Krishi Kalyan 18-05-2016 *****/
    public static final double RPST_H = 0.0188;
    public static final double RPST_N = 1.88;
    public static final double BPST_H = 0.15;
    public static final double BPST_N = 15;
    public static final double RIDST_H = 0.0375;
    public static final double RIDST_N = 3.75;
    //***** Krishi Kalyan 18-05-2016 *****/

    /***** Krishi Kalyan 25-05-2016 *****
    public static final double RPST_H = 0.0231;
    public static final double RPST_N = 2.31;
    public static final double BPST_H = 0.15;
    public static final double BPST_N = 15;
    public static final double RIDST_H = 0.0413;
    public static final double RIDST_N = 4.13;
    /***** Krishi Kalyan 25-05-2016 *****/



	//Added by Ganesh
	//Log Function

	public static void printLog(String level,String message) {
		try{
			/*switch (level){
				case "d": 	//Log.d("LP",message);
					break;
				case "e": 	Log.e("LP", message);
					break;
				case "i": 	Log.i("LP", message);
					break;
				default: 	Log.v("LP",message);
					break;
			}*/
		}
		catch (Exception e){
			CommonConstant.printLog("e", "Exception in printLog: " + e.getMessage());
		}
	}

}
