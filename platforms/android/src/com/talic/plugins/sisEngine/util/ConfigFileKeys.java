package com.talic.plugins.sisEngine.util;

/**
 * Insert the type's description here.
 * Creation date: (09/11/2000 08:53:51 PM)
 * @author: Administrator
 */
public interface ConfigFileKeys {

	String CONFIG_FILE = "SISEngineConfig";
	String DEBUG = "Debug";
	String LOG_DIR = "LogDir";
	String WEB_APP_NAME = "WebAppName";
	String INPUT_DIR = "www\\templates\\SIS\\"/*"InputDir"*/;
	String OUTPUT_DIR = "OutputDir\\";
	String TEMP_OUTPUT_DIR = "TempOutputDir";

}
