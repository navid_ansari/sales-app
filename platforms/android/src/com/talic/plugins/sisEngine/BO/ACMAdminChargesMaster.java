package com.talic.plugins.sisEngine.BO;

public class ACMAdminChargesMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int ACM_ID;	//NUMBER(10,0)
	private long ACM_MIN_PREM;	//NUMBER(10,0)
	private long ACM_MAX_PREM;	//NUMBER(10,0)
	private long ACM_CHARGE;	//NUMBER(10,0)
	public int getACM_ID() {
		return ACM_ID;
	}
	public void setACM_ID(int acm_id) {
		ACM_ID = acm_id;
	}
	public long getACM_MIN_PREM() {
		return ACM_MIN_PREM;
	}
	public void setACM_MIN_PREM(long acm_min_prem) {
		ACM_MIN_PREM = acm_min_prem;
	}
	public long getACM_MAX_PREM() {
		return ACM_MAX_PREM;
	}
	public void setACM_MAX_PREM(long acm_max_prem) {
		ACM_MAX_PREM = acm_max_prem;
	}
	public long getACM_CHARGE() {
		return ACM_CHARGE;
	}
	public void setACM_CHARGE(long acm_charge) {
		ACM_CHARGE = acm_charge;
	}



}
