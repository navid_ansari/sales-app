package com.talic.plugins.sisEngine.BO;

import java.util.Date;

public class STDTemplateValues implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private	int	std_pgl_id;
	private	String	std_temp_desc;
	private	int	std_sequence;
	private	int	std_min_startp;
	private	int	std_max_startp;
	private	int std_min_pt;
	private	int std_max_pt;
	private	int std_no_pages;
	private	String std_mandatory;
	private	String std_show;
	private	String std_update_by;
	private	Date std_update_dt;
	private	String std_is_rider;
	private String std_rdl_code;
	private int std_max_no_pages;

	public int getStd_pgl_id() {
		return std_pgl_id;
	}
	public void setStd_pgl_id(int std_pgl_id) {
		this.std_pgl_id = std_pgl_id;
	}
	public String getStd_temp_desc() {
		return std_temp_desc;
	}
	public void setStd_temp_desc(String std_temp_desc) {
		this.std_temp_desc = std_temp_desc;
	}
	public int getStd_sequence() {
		return std_sequence;
	}
	public void setStd_sequence(int std_sequence) {
		this.std_sequence = std_sequence;
	}
	public int getStd_min_startp() {
		return std_min_startp;
	}
	public void setStd_min_startp(int std_min_startp) {
		this.std_min_startp = std_min_startp;
	}
	public int getStd_max_startp() {
		return std_max_startp;
	}
	public void setStd_max_startp(int std_max_startp) {
		this.std_max_startp = std_max_startp;
	}
	public int getStd_min_pt() {
		return std_min_pt;
	}
	public void setStd_min_pt(int std_min_pt) {
		this.std_min_pt = std_min_pt;
	}
	public int getStd_max_pt() {
		return std_max_pt;
	}
	public void setStd_max_pt(int std_max_pt) {
		this.std_max_pt = std_max_pt;
	}
	public int getStd_no_pages() {
		return std_no_pages;
	}
	public void setStd_no_pages(int std_no_pages) {
		this.std_no_pages = std_no_pages;
	}
	public String getStd_mandatory() {
		return std_mandatory;
	}
	public void setStd_mandatory(String std_mandatory) {
		this.std_mandatory = std_mandatory;
	}
	public String getStd_show() {
		return std_show;
	}
	public void setStd_show(String std_show) {
		this.std_show = std_show;
	}
	public String getStd_update_by() {
		return std_update_by;
	}
	public void setStd_update_by(String std_update_by) {
		this.std_update_by = std_update_by;
	}
	public Date getStd_update_dt() {
		return std_update_dt;
	}
	public void setStd_update_dt(Date std_update_dt) {
		this.std_update_dt = std_update_dt;
	}
	public String getStd_is_rider() {
		return std_is_rider;
	}
	public void setStd_is_rider(String std_is_rider) {
		this.std_is_rider = std_is_rider;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public String getStd_rdl_code() {
		return std_rdl_code;
	}
	public void setStd_rdl_code(String std_rdl_code) {
		this.std_rdl_code = std_rdl_code;
	}
	public int getStd_max_no_pages() {
		return std_max_no_pages;
	}
	public void setStd_max_no_pages(int std_max_no_pages) {
		this.std_max_no_pages = std_max_no_pages;
	}


}
