package com.talic.plugins.sisEngine.BO;

public class GuaranteedMaturityBonusMaster implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int GMM_ID;	//NUMBER(10,0)
	private int GMM_MIN_BAND;	//NUMBER(10,0)
	private int GMM_MAX_BAND;	//NUMBER(10,0)
	private double GMM_BONUS;	//NUMBER(10,2) // Guaranteed maturity Bonus
	public int getGMM_ID() {
		return GMM_ID;
	}
	public void setGMM_ID(int gmm_id) {
		GMM_ID = gmm_id;
	}
	public int getGMM_MIN_BAND() {
		return GMM_MIN_BAND;
	}
	public void setGMM_MIN_BAND(int gmm_min_band) {
		GMM_MIN_BAND = gmm_min_band;
	}
	public int getGMM_MAX_BAND() {
		return GMM_MAX_BAND;
	}
	public void setGMM_MAX_BAND(int gmm_max_band) {
		GMM_MAX_BAND = gmm_max_band;
	}
	public double getGMM_BONUS() {
		return GMM_BONUS;
	}
	public void setGMM_BONUS(double gmm_bonus) {
		GMM_BONUS = gmm_bonus;
	}


}
