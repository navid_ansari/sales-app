package com.talic.plugins.sisEngine.BO;

public class GAIGuarAnnIncChMaster implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int GAI_ID ;
	private int GAI_PNL_ID;
	private int GAI_MIN_TERM;
	private int GAI_MAX_TERM;
	private int GAI_MIN_PPT;
	private int GAI_MAX_PPT;
	private long GAI_MIN_PREM;
	private long GAI_MAX_PREM;
	private String GAI_FLAG;
	private double GAI_CHARGE;
	public int getGAI_ID() {
		return GAI_ID;
	}
	public void setGAI_ID(int gai_id) {
		GAI_ID = gai_id;
	}
	public int getGAI_PNL_ID() {
		return GAI_PNL_ID;
	}
	public void setGAI_PNL_ID(int gai_pnl_id) {
		GAI_PNL_ID = gai_pnl_id;
	}
	public int getGAI_MIN_TERM() {
		return GAI_MIN_TERM;
	}
	public void setGAI_MIN_TERM(int gai_min_term) {
		GAI_MIN_TERM = gai_min_term;
	}
	public int getGAI_MAX_TERM() {
		return GAI_MAX_TERM;
	}
	public void setGAI_MAX_TERM(int gai_max_term) {
		GAI_MAX_TERM = gai_max_term;
	}
	public long getGAI_MIN_PREM() {
		return GAI_MIN_PREM;
	}
	public void setGAI_MIN_PREM(long gai_min_prem) {
		GAI_MIN_PREM = gai_min_prem;
	}
	public long getGAI_MAX_PREM() {
		return GAI_MAX_PREM;
	}
	public void setGAI_MAX_PREM(long gai_max_prem) {
		GAI_MAX_PREM = gai_max_prem;
	}
	public String getGAI_FLAG() {
		return GAI_FLAG;
	}
	public void setGAI_FLAG(String gai_flag) {
		GAI_FLAG = gai_flag;
	}
	public double getGAI_CHARGE() {
		return GAI_CHARGE;
	}
	public void setGAI_CHARGE(double gai_charge) {
		GAI_CHARGE = gai_charge;
	}
	public int getGAI_MIN_PPT() {
		return GAI_MIN_PPT;
	}

	public void setGAI_MIN_PPT(int GAI_MIN_PPT) {
		this.GAI_MIN_PPT = GAI_MIN_PPT;
	}

	public int getGAI_MAX_PPT() {
		return GAI_MAX_PPT;
	}

	public void setGAI_MAX_PPT(int GAI_MAX_PPT) {
		this.GAI_MAX_PPT = GAI_MAX_PPT;
	}

}
