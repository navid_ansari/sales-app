package com.talic.plugins.sisEngine.BO;

public class AAAFMCValues implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private	int	afc_plan_id;
	private	int	afc_min_age;
	private	int	afc_max_age;
	private	int	afc_large_cap;
	private	int	afc_whole_life_income;
	private	float afc_fmc	;
	public int getAfc_plan_id() {
		return afc_plan_id;
	}
	public void setAfc_plan_id(int afc_plan_id) {
		this.afc_plan_id = afc_plan_id;
	}
	public int getAfc_min_age() {
		return afc_min_age;
	}
	public void setAfc_min_age(int afc_min_age) {
		this.afc_min_age = afc_min_age;
	}
	public int getAfc_max_age() {
		return afc_max_age;
	}
	public void setAfc_max_age(int afc_max_age) {
		this.afc_max_age = afc_max_age;
	}
	public int getAfc_large_cap() {
		return afc_large_cap;
	}
	public void setAfc_large_cap(int afc_large_cap) {
		this.afc_large_cap = afc_large_cap;
	}
	public int getAfc_whole_life_income() {
		return afc_whole_life_income;
	}
	public void setAfc_whole_life_income(int afc_whole_life_income) {
		this.afc_whole_life_income = afc_whole_life_income;
	}
	public float getAfc_fmc() {
		return afc_fmc;
	}
	public void setAfc_fmc(float afc_fmc) {
		this.afc_fmc = afc_fmc;
	}


}
