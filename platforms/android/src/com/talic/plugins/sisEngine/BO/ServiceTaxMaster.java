package com.talic.plugins.sisEngine.BO;

import java.util.Date;

public class ServiceTaxMaster {
	private	String	stm_type;
	private	double	stm_value;
	private	String	stm_update_by;
	private Date stm_update_dt;

	public String getStm_type() {
		return stm_type;
	}
	public void setStm_type(String stm_type) {
		this.stm_type = stm_type;
	}
	public double getStm_value() {
		return stm_value;
	}
	public void setStm_value(double stm_value) {
		this.stm_value = stm_value;
	}
	public String getStm_update_by() {
		return stm_update_by;
	}
	public void setStm_update_by(String stm_update_by) {
		this.stm_update_by = stm_update_by;
	}
	public Date getStm_update_dt() {
		return stm_update_dt;
	}
	public void setStm_update_dt(Date stm_update_dt) {
		this.stm_update_dt = stm_update_dt;
	}
}
