package com.talic.plugins.sisEngine.BO;

public class PFRPlanFormulaRef implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String	PFR_PLAN_CD;
	private String	PFR_FRM_CD;
	private String	PFR_FRM_TYPE;
	private String	PFR_FRM_EXP;

	public String getPFR_PLAN_CD() {
		return PFR_PLAN_CD;
	}
	public void setPFR_PLAN_CD(String PFR_plan_cd) {
		PFR_PLAN_CD = PFR_plan_cd;
	}
	public String getPFR_FRM_CD() {
		return PFR_FRM_CD;
	}
	public void setPFR_FRM_CD(String PFR_frm_cd) {
		PFR_FRM_CD = PFR_frm_cd;
	}
	public String getPFR_FRM_TYPE() {
		return PFR_FRM_TYPE;
	}
	public void setPFR_FRM_TYPE(String PFR_frm_type) {
		PFR_FRM_TYPE = PFR_frm_type;
	}
	public String getPFR_FRM_EXP() {
		return PFR_FRM_EXP;
	}
	public void setPFR_FRM_EXP(String PFR_frm_exp) {
		PFR_FRM_EXP = PFR_frm_exp;
	}




}
