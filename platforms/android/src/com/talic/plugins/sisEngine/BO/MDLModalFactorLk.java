package com.talic.plugins.sisEngine.BO;

public class MDLModalFactorLk implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int MDL_ID;
	private int MDL_PGL_ID;
	private String MDL_PAY_FREQ;
	private double MDL_MODAL_FACTOR;


	public int getMDL_ID() {
		return MDL_ID;
	}
	public void setMDL_ID(int mdl_id) {
		MDL_ID = mdl_id;
	}
	public int getMDL_PGL_ID() {
		return MDL_PGL_ID;
	}
	public void setMDL_PGL_ID(int mdl_pgl_id) {
		MDL_PGL_ID = mdl_pgl_id;
	}
	public String getMDL_PAY_FREQ() {
		return MDL_PAY_FREQ;
	}
	public void setMDL_PAY_FREQ(String mdl_pay_freq) {
		MDL_PAY_FREQ = mdl_pay_freq;
	}
	public double getMDL_MODAL_FACTOR() {
		return MDL_MODAL_FACTOR;
	}
	public void setMDL_MODAL_FACTOR(double mdl_modal_factor) {
		MDL_MODAL_FACTOR = mdl_modal_factor;
	}



}
