package com.talic.plugins.sisEngine.BO;

public class RDLRiderLk implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private	int	RDL_ID;
	private	String	RDL_CODE;
	private	String	RDL_DESCRIPTION	;
	private	int	RDL_RTL_ID	;
	private	double	RDL_SERVICE_TAX	;
	private	String	ISSIMPLIFIED	;
	private	String	ISANNUITYPRODUCT;
	private	String	ISBACKDATE	;
	private	int	NOOFMONTH	;
	private	String	NBFC	;
	private	int	FIXCVGID	;
	private	int	MINOCC	;
	private	int	MAXOCC	;
	private String RDL_UIN;		//added by jayesh
	public int getRDL_ID() {
		return RDL_ID;
	}
	public void setRDL_ID(int rdl_id) {
		RDL_ID = rdl_id;
	}
	public String getRDL_CODE() {
		return RDL_CODE;
	}
	public void setRDL_CODE(String rdl_code) {
		RDL_CODE = rdl_code;
	}
	public String getRDL_DESCRIPTION() {
		return RDL_DESCRIPTION;
	}
	public void setRDL_DESCRIPTION(String rdl_description) {
		RDL_DESCRIPTION = rdl_description;
	}
	public int getRDL_RTL_ID() {
		return RDL_RTL_ID;
	}
	public void setRDL_RTL_ID(int rdl_rtl_id) {
		RDL_RTL_ID = rdl_rtl_id;
	}
	public double getRDL_SERVICE_TAX() {
		return RDL_SERVICE_TAX;
	}
	public void setRDL_SERVICE_TAX(double rdl_service_tax) {
		RDL_SERVICE_TAX = rdl_service_tax;
	}
	public String getISSIMPLIFIED() {
		return ISSIMPLIFIED;
	}
	public void setISSIMPLIFIED(String issimplified) {
		ISSIMPLIFIED = issimplified;
	}
	public String getISANNUITYPRODUCT() {
		return ISANNUITYPRODUCT;
	}
	public void setISANNUITYPRODUCT(String isannuityproduct) {
		ISANNUITYPRODUCT = isannuityproduct;
	}
	public String getISBACKDATE() {
		return ISBACKDATE;
	}
	public void setISBACKDATE(String isbackdate) {
		ISBACKDATE = isbackdate;
	}
	public int getNOOFMONTH() {
		return NOOFMONTH;
	}
	public void setNOOFMONTH(int noofmonth) {
		NOOFMONTH = noofmonth;
	}
	public String getNBFC() {
		return NBFC;
	}
	public void setNBFC(String nbfc) {
		NBFC = nbfc;
	}
	public int getFIXCVGID() {
		return FIXCVGID;
	}
	public void setFIXCVGID(int fixcvgid) {
		FIXCVGID = fixcvgid;
	}
	public int getMINOCC() {
		return MINOCC;
	}
	public void setMINOCC(int minocc) {
		MINOCC = minocc;
	}
	public int getMAXOCC() {
		return MAXOCC;
	}
	public void setMAXOCC(int maxocc) {
		MAXOCC = maxocc;
	}
	public String getRDL_UIN() {
		return RDL_UIN;
	}
	public void setRDL_UIN(String rdl_uin) {
		RDL_UIN = rdl_uin;
	}


}
