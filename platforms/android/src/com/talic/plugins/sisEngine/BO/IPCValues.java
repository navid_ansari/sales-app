package com.talic.plugins.sisEngine.BO;

public class IPCValues implements java.io.Serializable{

private static final long serialVersionUID = 1L;

	private int IPC_ID;	//NUMBER
	private int IPC_PNL_ID;	//NUMBER
	private String IPC_SEX;	//CHAR(1 Bytes)
	private int IPC_TERM;	//NUMBER
	private int IPC_AGE;	//NUMBER
	private double IPC_PREM_MULT;	//NUMBER
	private int IPC_BAND;	//NUMBER
	private String SMOKER_FLAG;	//VARCHAR2(1 Bytes)


	public int getIPC_ID() {
		return IPC_ID;
	}
	public void setIPC_ID(int IPC_id) {
		IPC_ID = IPC_id;
	}
	public int getIPC_PNL_ID() {
		return IPC_PNL_ID;
	}
	public void setIPC_PNL_ID(int IPC_pnl_id) {
		IPC_PNL_ID = IPC_pnl_id;
	}
	public String getIPC_SEX() {
		return IPC_SEX;
	}
	public void setIPC_SEX(String IPC_sex) {
		IPC_SEX = IPC_sex;
	}
	public int getIPC_TERM() {
		return IPC_TERM;
	}
	public void setIPC_TERM(int IPC_term) {
		IPC_TERM = IPC_term;
	}
	public int getIPC_AGE() {
		return IPC_AGE;
	}
	public void setIPC_AGE(int IPC_age) {
		IPC_AGE = IPC_age;
	}
	public double getIPC_PREM_MULT() {
		return IPC_PREM_MULT;
	}
	public void setIPC_PREM_MULT(double IPC_prem_mult) {
		IPC_PREM_MULT = IPC_prem_mult;
	}
	public int getIPC_BAND() {
		return IPC_BAND;
	}
	public void setIPC_BAND(int IPC_band) {
		IPC_BAND = IPC_band;
	}
	public String getSMOKER_FLAG() {
		return SMOKER_FLAG;
	}
	public void setSMOKER_FLAG(String smoker_flag) {
		SMOKER_FLAG = smoker_flag;
	}



}
