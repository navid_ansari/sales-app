package com.talic.plugins.sisEngine.BO;

public class WCMWOPChargesMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int WCM_WOP_ID;	//NUMBER(10,0)
	private int WCM_AGE;	//NUMBER(10,0)
	private int WCM_BAND;	//NUMBER(10,0)
	private double WCM_CHARGE;	//NUMBER(10,2)
	public int getWCM_WOP_ID() {
		return WCM_WOP_ID;
	}
	public void setWCM_WOP_ID(int wcm_wop_id) {
		WCM_WOP_ID = wcm_wop_id;
	}
	public int getWCM_AGE() {
		return WCM_AGE;
	}
	public void setWCM_AGE(int wcm_age) {
		WCM_AGE = wcm_age;
	}
	public int getWCM_BAND() {
		return WCM_BAND;
	}
	public void setWCM_BAND(int wcm_band) {
		WCM_BAND = wcm_band;
	}
	public double getWCM_CHARGE() {
		return WCM_CHARGE;
	}
	public void setWCM_CHARGE(double wcm_charge) {
		WCM_CHARGE = wcm_charge;
	}




}
