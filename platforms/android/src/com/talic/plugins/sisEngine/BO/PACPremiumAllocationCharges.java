package com.talic.plugins.sisEngine.BO;

public class PACPremiumAllocationCharges implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int	PAC_ID;	//NUMBER(10,0)
	private int PAC_MIN_BAND;	//NUMBER(10,0)
	private int PAC_MAX_BAND;	//NUMBER(10,0)
	private long PAC_MIN_PREM;	//NUMBER(10,0)
	private long PAC_MAX_PREM;	//NUMBER(10,0)
	private double PAC_CHARGE;	//NUMBER(10,2)
	public int getPAC_ID() {
		return PAC_ID;
	}
	public void setPAC_ID(int pac_id) {
		PAC_ID = pac_id;
	}
	public int getPAC_MIN_BAND() {
		return PAC_MIN_BAND;
	}
	public void setPAC_MIN_BAND(int pac_min_band) {
		PAC_MIN_BAND = pac_min_band;
	}
	public int getPAC_MAX_BAND() {
		return PAC_MAX_BAND;
	}
	public void setPAC_MAX_BAND(int pac_max_band) {
		PAC_MAX_BAND = pac_max_band;
	}
	public long getPAC_MIN_PREM() {
		return PAC_MIN_PREM;
	}
	public void setPAC_MIN_PREM(long pac_min_prem) {
		PAC_MIN_PREM = pac_min_prem;
	}
	public long getPAC_MAX_PREM() {
		return PAC_MAX_PREM;
	}
	public void setPAC_MAX_PREM(long pac_max_prem) {
		PAC_MAX_PREM = pac_max_prem;
	}
	public double getPAC_CHARGE() {
		return PAC_CHARGE;
	}
	public void setPAC_CHARGE(double pac_charge) {
		PAC_CHARGE = pac_charge;
	}


}
