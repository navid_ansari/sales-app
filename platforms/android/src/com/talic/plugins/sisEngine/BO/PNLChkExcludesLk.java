package com.talic.plugins.sisEngine.BO;

public class PNLChkExcludesLk implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private int CHK_ID;
	private int PNL_ID;
	private String PNL_CODE;
	private String SUM_ASSURED;
	private String UNITS;
	private String PURCHASE_PRICE;
	private String REGULAR_PREMIUM;

	public int getCHK_ID() {
		return CHK_ID;
	}
	public void setCHK_ID(int chk_id) {
		CHK_ID = chk_id;
	}
	public int getPNL_ID() {
		return PNL_ID;
	}
	public void setPNL_ID(int pnl_id) {
		PNL_ID = pnl_id;
	}
	public String getPNL_CODE() {
		return PNL_CODE;
	}
	public void setPNL_CODE(String pnl_code) {
		PNL_CODE = pnl_code;
	}
	public String getSUM_ASSURED() {
		return SUM_ASSURED;
	}
	public void setSUM_ASSURED(String sum_assured) {
		SUM_ASSURED = sum_assured;
	}
	public String getUNITS() {
		return UNITS;
	}
	public void setUNITS(String units) {
		UNITS = units;
	}
	public String getPURCHASE_PRICE() {
		return PURCHASE_PRICE;
	}
	public void setPURCHASE_PRICE(String purchase_price) {
		PURCHASE_PRICE = purchase_price;
	}
	public String getREGULAR_PREMIUM() {
		return REGULAR_PREMIUM;
	}
	public void setREGULAR_PREMIUM(String regular_premium) {
		REGULAR_PREMIUM = regular_premium;
	}


}
