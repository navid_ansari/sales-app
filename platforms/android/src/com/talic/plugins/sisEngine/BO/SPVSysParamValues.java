package com.talic.plugins.sisEngine.BO;

public class SPVSysParamValues  implements java.io.Serializable {

  private String SPV_SYS_PARAM	;
  private String SPV_VALUE	;
  private String SPV_FILLER	;


   public String getSPV_SYS_PARAM() {
		return SPV_SYS_PARAM;
	}
	public void setSPV_SYS_PARAM(String spv_sys_param) {
		SPV_SYS_PARAM = spv_sys_param;
	}
	public String getSPV_VALUE() {
		return SPV_VALUE;
	}
	public void setSPV_VALUE(String spv_value) {
		SPV_VALUE = spv_value;
	}
	public String getSPV_FILLER() {
		return SPV_FILLER;
	}
	public void setSPV_FILLER(String spv_filler) {
		SPV_FILLER = spv_filler;
	}


}
