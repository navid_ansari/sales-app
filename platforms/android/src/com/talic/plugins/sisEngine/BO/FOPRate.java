package com.talic.plugins.sisEngine.BO;

public class FOPRate {
	private	int	fop_pnl_id;
	private	String	fop_mode;
	private	int	fop_term;
	private	double fop_rate;
	public int getFop_pnl_id() {
		return fop_pnl_id;
	}
	public void setFop_pnl_id(int fop_pnl_id) {
		this.fop_pnl_id = fop_pnl_id;
	}
	public String getFop_mode() {
		return fop_mode;
	}
	public void setFop_mode(String fop_mode) {
		this.fop_mode = fop_mode;
	}
	public int getFop_term() {
		return fop_term;
	}
	public void setFop_term(int fop_term) {
		this.fop_term = fop_term;
	}
	public double getFop_rate() {
		return fop_rate;
	}
	public void setFop_rate(double fop_rate) {
		this.fop_rate = fop_rate;
	}


}
