package com.talic.plugins.sisEngine.BO;

public class MCMMortilityChargesMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int MCM_ID;	//NUMBER(10,0)
	private int MCM_AGE;	//NUMBER(10,0)
	private double MCM_MORT_CHARGE;	//NUMBER(10,3)
	private String MCM_SEX;
	public int getMCM_ID() {
		return MCM_ID;
	}
	public void setMCM_ID(int mcm_id) {
		MCM_ID = mcm_id;
	}
	public int getMCM_AGE() {
		return MCM_AGE;
	}
	public void setMCM_AGE(int mcm_age) {
		MCM_AGE = mcm_age;
	}
	public double getMCM_MORT_CHARGE() {
		return MCM_MORT_CHARGE;
	}
	public void setMCM_MORT_CHARGE(double mcm_mort_charge) {
		MCM_MORT_CHARGE = mcm_mort_charge;
	}

	public String getMCM_SEX() {
		return MCM_SEX;
	}
	public void setMCM_SEX(String mcm_sex) {
		MCM_SEX = mcm_sex;
	}

}
