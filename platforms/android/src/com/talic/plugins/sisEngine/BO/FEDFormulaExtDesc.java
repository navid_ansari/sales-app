package com.talic.plugins.sisEngine.BO;

public class FEDFormulaExtDesc implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String	FED_ID;
	private int	FED_OBJ_SEQ;
	private String	FED_OBJ_OPND;
	private String FED_OBJ_OPND_TYPE;
	private String	FED_OBJ_OPTR;
	private String	FED_OBJ_TYPE;

	public String getFED_ID() {
		return FED_ID;
	}
	public void setFED_ID(String fed_id) {
		FED_ID = fed_id;
	}
	public int getFED_OBJ_SEQ() {
		return FED_OBJ_SEQ;
	}
	public void setFED_OBJ_SEQ(int fed_obj_seq) {
		FED_OBJ_SEQ = fed_obj_seq;
	}
	public String getFED_OBJ_OPND() {
		return FED_OBJ_OPND;
	}

	public String getFED_OBJ_OPND_TYPE() {
		return FED_OBJ_OPND_TYPE;
	}
	public void setFED_OBJ_OPND_TYPE(String fed_obj_opnd_type) {
		FED_OBJ_OPND_TYPE = fed_obj_opnd_type;
	}
	public void setFED_OBJ_OPND(String fed_obj_opnd) {
		FED_OBJ_OPND = fed_obj_opnd;
	}
	public String getFED_OBJ_OPTR() {
		return FED_OBJ_OPTR;
	}
	public void setFED_OBJ_OPTR(String fed_obj_optr) {
		FED_OBJ_OPTR = fed_obj_optr;
	}
	public String getFED_OBJ_TYPE() {
		return FED_OBJ_TYPE;
	}
	public void setFED_OBJ_TYPE(String fed_obj_type) {
		FED_OBJ_TYPE = fed_obj_type;
	}




}
