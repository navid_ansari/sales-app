package com.talic.plugins.sisEngine.BO;

public class PmdPlanPremiumMultDisc implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	 private String PMD_PLAN_CODE;
	 private String PMD_RIDER_CODE;
	 private long PMD_MIN_SA;
	 private long PMD_MAX_SA;
     private double PMD_DISC_RATE;

	 public String getPMD_PLAN_CODE() {
		return PMD_PLAN_CODE;
	}
	public void setPMD_PLAN_CODE(String pmd_plan_code) {
		PMD_PLAN_CODE = pmd_plan_code;
	}
	public String getPMD_RIDER_CODE() {
		return PMD_RIDER_CODE;
	}
	public void setPMD_RIDER_CODE(String pmd_rider_code) {
		PMD_RIDER_CODE = pmd_rider_code;
	}
	public long getPMD_MIN_SA() {
		return PMD_MIN_SA;
	}
	public void setPMD_MIN_SA(long pmd_min_sa) {
		PMD_MIN_SA = pmd_min_sa;
	}
	public long getPMD_MAX_SA() {
		return PMD_MAX_SA;
	}
	public void setPMD_MAX_SA(long pmd_max_sa) {
		PMD_MAX_SA = pmd_max_sa;
	}
	public double getPMD_DISC_RATE() {
		return PMD_DISC_RATE;
	}
	public void setPMD_DISC_RATE(double pmd_disc_rate) {
		PMD_DISC_RATE = pmd_disc_rate;
	}


}
