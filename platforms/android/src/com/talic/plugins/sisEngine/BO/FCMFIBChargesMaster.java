package com.talic.plugins.sisEngine.BO;

public class FCMFIBChargesMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int FCM_FIB_ID;	//NUMBER(10,0)
	private int FCM_AGE;	//NUMBER(10,0)
	private int FCM_BAND;	//NUMBER(10,0)
	private double FCM_CHARGE;	//NUMBER(10,2)
	public int getFCM_FIB_ID() {
		return FCM_FIB_ID;
	}
	public void setFCM_FIB_ID(int fcm_fib_id) {
		FCM_FIB_ID = fcm_fib_id;
	}
	public int getFCM_AGE() {
		return FCM_AGE;
	}
	public void setFCM_AGE(int fcm_age) {
		FCM_AGE = fcm_age;
	}
	public int getFCM_BAND() {
		return FCM_BAND;
	}
	public void setFCM_BAND(int fcm_band) {
		FCM_BAND = fcm_band;
	}
	public double getFCM_CHARGE() {
		return FCM_CHARGE;
	}
	public void setFCM_CHARGE(double fcm_charge) {
		FCM_CHARGE = fcm_charge;
	}



}
