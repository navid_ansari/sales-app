package com.talic.plugins.sisEngine.BO;

public class ANNUITYRate {
	private	String	arl_pnl_id;
	private	String	arl_annuity_mode;
	private	int	arl_age;
	private	double arl_annuity_rate;
	public String getArl_pnl_id() {
		return arl_pnl_id;
	}
	public void setArl_pnl_id(String arl_pnl_id) {
		this.arl_pnl_id = arl_pnl_id;
	}
	public String getArl_annuity_mode() {
		return arl_annuity_mode;
	}
	public void setArl_annuity_mode(String arl_annuity_mode) {
		this.arl_annuity_mode = arl_annuity_mode;
	}
	public int getArl_age() {
		return arl_age;
	}
	public void setArl_age(int arl_age) {
		this.arl_age = arl_age;
	}
	public double getArl_annuity_rate() {
		return arl_annuity_rate;
	}
	public void setArl_annuity_rate(double arl_annuity_rate) {
		this.arl_annuity_rate = arl_annuity_rate;
	}



}
