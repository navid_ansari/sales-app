package com.talic.plugins.sisEngine.BO;

public class SURSurrenderChargeMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;


	private int  SUR_ID;	  //NUMBER(10,0)
	private int  SUR_MIN_BAND;	//NUMBER(10,0)
	private int  SUR_MAX_BAND;	//NUMBER(10,0)
	private long SUR_MIN_PREM;	//NUMBER(10,0)
	private long SUR_MAX_PREM;	//NUMBER(10,0)
	private double SUR_CHARGE;	//NUMBER(10,2)
	private long SUR_AMOUNT;  //NUMBER(10,2)
	public int getSUR_ID() {
		return SUR_ID;
	}
	public void setSUR_ID(int sur_id) {
		SUR_ID = sur_id;
	}
	public int getSUR_MIN_BAND() {
		return SUR_MIN_BAND;
	}
	public void setSUR_MIN_BAND(int sur_min_band) {
		SUR_MIN_BAND = sur_min_band;
	}
	public int getSUR_MAX_BAND() {
		return SUR_MAX_BAND;
	}
	public void setSUR_MAX_BAND(int sur_max_band) {
		SUR_MAX_BAND = sur_max_band;
	}
	public long getSUR_MIN_PREM() {
		return SUR_MIN_PREM;
	}
	public void setSUR_MIN_PREM(long sur_min_prem) {
		SUR_MIN_PREM = sur_min_prem;
	}
	public long getSUR_MAX_PREM() {
		return SUR_MAX_PREM;
	}
	public void setSUR_MAX_PREM(long sur_max_prem) {
		SUR_MAX_PREM = sur_max_prem;
	}
	public double getSUR_CHARGE() {
		return SUR_CHARGE;
	}
	public void setSUR_CHARGE(double sur_charge) {
		SUR_CHARGE = sur_charge;
	}
	public long getSUR_AMOUNT() {
		return SUR_AMOUNT;
	}
	public void setSUR_AMOUNT(long sur_amount) {
		SUR_AMOUNT = sur_amount;
	}



}
