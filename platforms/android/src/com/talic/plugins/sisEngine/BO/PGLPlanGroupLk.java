package com.talic.plugins.sisEngine.BO;

public class PGLPlanGroupLk implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PGL_ID;	//NUMBER
	private String PGL_DESCRIPTION;	//VARCHAR2(100 Bytes)
	private String PGL_JUVENILE_IND;	//CHAR(1 Bytes)
	private String PGL_ADULT_IND;	//CHAR(1 Bytes)
	private String PGL_PRODUCT_TYPE;	//CHAR(1 Bytes)
	private String PGL_PRODUCT_SUBTYPE;	//VARCHAR2(2 Bytes)
	private String PGL_TERM_CONFIGURED;	//CHAR(1 Bytes)
	private String PGL_APPLICATION;	//VARCHAR2(5 Bytes)
	private int PGL_TERM_FLAG;	//NUMBER
	private String PGL_PREFIX;	//VARCHAR2(30 Char)


	public int getPGL_ID() {
		return PGL_ID;
	}
	public void setPGL_ID(int pgl_id) {
		PGL_ID = pgl_id;
	}
	public String getPGL_DESCRIPTION() {
		return PGL_DESCRIPTION;
	}
	public void setPGL_DESCRIPTION(String pgl_description) {
		PGL_DESCRIPTION = pgl_description;
	}
	public String getPGL_JUVENILE_IND() {
		return PGL_JUVENILE_IND;
	}
	public void setPGL_JUVENILE_IND(String pgl_juvenile_ind) {
		PGL_JUVENILE_IND = pgl_juvenile_ind;
	}
	public String getPGL_ADULT_IND() {
		return PGL_ADULT_IND;
	}
	public void setPGL_ADULT_IND(String pgl_adult_ind) {
		PGL_ADULT_IND = pgl_adult_ind;
	}
	public String getPGL_PRODUCT_TYPE() {
		return PGL_PRODUCT_TYPE;
	}
	public void setPGL_PRODUCT_TYPE(String pgl_product_type) {
		PGL_PRODUCT_TYPE = pgl_product_type;
	}
	public String getPGL_PRODUCT_SUBTYPE() {
		return PGL_PRODUCT_SUBTYPE;
	}
	public void setPGL_PRODUCT_SUBTYPE(String pgl_product_subtype) {
		PGL_PRODUCT_SUBTYPE = pgl_product_subtype;
	}
	public String getPGL_TERM_CONFIGURED() {
		return PGL_TERM_CONFIGURED;
	}
	public void setPGL_TERM_CONFIGURED(String pgl_term_configured) {
		PGL_TERM_CONFIGURED = pgl_term_configured;
	}
	public String getPGL_APPLICATION() {
		return PGL_APPLICATION;
	}
	public void setPGL_APPLICATION(String pgl_application) {
		PGL_APPLICATION = pgl_application;
	}
	public int getPGL_TERM_FLAG() {
		return PGL_TERM_FLAG;
	}
	public void setPGL_TERM_FLAG(int pgl_term_flag) {
		PGL_TERM_FLAG = pgl_term_flag;
	}
	public String getPGL_PREFIX() {
		return PGL_PREFIX;
	}
	public void setPGL_PREFIX(String pgl_prefix) {
		PGL_PREFIX = pgl_prefix;
	}


}
