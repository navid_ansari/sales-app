package com.talic.plugins.sisEngine.BO;

public class CCMCommissionChargeMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int CCM_ID;	//NUMBER(10,0)
	private int CCM_MIN_TERM;	//NUMBER(10,0)
	private int CCM_MAX_TERM;	//NUMBER(10,0)
	private double CCM_COMMISSION;	//NUMBER(10,2)
	public int getCCM_ID() {
		return CCM_ID;
	}
	public void setCCM_ID(int ccm_id) {
		CCM_ID = ccm_id;
	}
	public int getCCM_MIN_TERM() {
		return CCM_MIN_TERM;
	}
	public void setCCM_MIN_TERM(int ccm_min_term) {
		CCM_MIN_TERM = ccm_min_term;
	}
	public int getCCM_MAX_TERM() {
		return CCM_MAX_TERM;
	}
	public void setCCM_MAX_TERM(int ccm_max_term) {
		CCM_MAX_TERM = ccm_max_term;
	}
	public double getCCM_COMMISSION() {
		return CCM_COMMISSION;
	}
	public void setCCM_COMMISSION(double ccm_commission) {
		CCM_COMMISSION = ccm_commission;
	}




}
