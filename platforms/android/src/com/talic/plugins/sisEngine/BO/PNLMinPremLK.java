package com.talic.plugins.sisEngine.BO;

public class PNLMinPremLK implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PMML_ID;	//NUMBER
	private String PML_PLAN_CD;	//VARCHAR2(25 Bytes)
	private String PML_PREMIUM_MODE;	//VARCHAR2(3 Bytes)
	private int PML_MIN_PREM_AMT;	//NUMBER
	private int PML_MAX_PREM_AMT;	//NUMBER
	private int PML_MIN_PPT;	//NUMBER
	private int PML_MAX_PPT;	//NUMBER
	private long PML_ANL_PREM_AMT;	//NUMBER


	public int getPMML_ID() {
		return PMML_ID;
	}
	public void setPMML_ID(int pmml_id) {
		PMML_ID = pmml_id;
	}
	public String getPML_PLAN_CD() {
		return PML_PLAN_CD;
	}
	public void setPML_PLAN_CD(String pml_plan_cd) {
		PML_PLAN_CD = pml_plan_cd;
	}
	public String getPML_PREMIUM_MODE() {
		return PML_PREMIUM_MODE;
	}
	public void setPML_PREMIUM_MODE(String pml_premium_mode) {
		PML_PREMIUM_MODE = pml_premium_mode;
	}
	public int getPML_MIN_PREM_AMT() {
		return PML_MIN_PREM_AMT;
	}
	public void setPML_MIN_PREM_AMT(int pml_min_prem_amt) {
		PML_MIN_PREM_AMT = pml_min_prem_amt;
	}
	public int getPML_MAX_PREM_AMT() {
		return PML_MAX_PREM_AMT;
	}
	public void setPML_MAX_PREM_AMT(int pml_max_prem_amt) {
		PML_MAX_PREM_AMT = pml_max_prem_amt;
	}
	public int getPML_MIN_PPT() {
		return PML_MIN_PPT;
	}
	public void setPML_MIN_PPT(int pml_min_ppt) {
		PML_MIN_PPT = pml_min_ppt;
	}
	public int getPML_MAX_PPT() {
		return PML_MAX_PPT;
	}
	public void setPML_MAX_PPT(int pml_max_ppt) {
		PML_MAX_PPT = pml_max_ppt;
	}
	public long getPML_ANL_PREM_AMT() {
		return PML_ANL_PREM_AMT;
	}
	public void setPML_ANL_PREM_AMT(long pml_anl_prem_amt) {
		PML_ANL_PREM_AMT = pml_anl_prem_amt;
	}




}
