package com.talic.plugins.sisEngine.BO;

public class AddressBO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String CO_NAME;
	private String CO_ADDRESS;
	private String CO_HELP_LINE;
	private String SIS_VERSION;
	private String FILLER1;
	public String getCO_NAME() {
		return CO_NAME;
	}
	public void setCO_NAME(String co_name) {
		CO_NAME = co_name;
	}
	public String getCO_ADDRESS() {
		return CO_ADDRESS;
	}
	public void setCO_ADDRESS(String co_address) {
		CO_ADDRESS = co_address;
	}
	public String getCO_HELP_LINE() {
		return CO_HELP_LINE;
	}
	public void setCO_HELP_LINE(String co_help_line) {
		CO_HELP_LINE = co_help_line;
	}
	public String getSIS_VERSION() {
		return SIS_VERSION;
	}
	public void setSIS_VERSION(String sis_version) {
		SIS_VERSION = sis_version;
	}
	public String getFILLER1() {
		return FILLER1;
	}
	public void setFILLER1(String filler1) {
		FILLER1 = filler1;
	}




}
