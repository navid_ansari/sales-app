package com.talic.plugins.sisEngine.BO;

public class PMLPremiumMultipleLk implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PML_ID;	//NUMBER
	private int PML_PNL_ID;	//NUMBER
	private String PML_SEX;	//CHAR(1 Bytes)
	private int PML_TERM;	//NUMBER
	private int PML_AGE;	//NUMBER
	private double PML_PREM_MULT;	//NUMBER
	private int PML_BAND;	//NUMBER
	private String SMOKER_FLAG;	//VARCHAR2(1 Bytes)


	public int getPML_ID() {
		return PML_ID;
	}
	public void setPML_ID(int pml_id) {
		PML_ID = pml_id;
	}
	public int getPML_PNL_ID() {
		return PML_PNL_ID;
	}
	public void setPML_PNL_ID(int pml_pnl_id) {
		PML_PNL_ID = pml_pnl_id;
	}
	public String getPML_SEX() {
		return PML_SEX;
	}
	public void setPML_SEX(String pml_sex) {
		PML_SEX = pml_sex;
	}
	public int getPML_TERM() {
		return PML_TERM;
	}
	public void setPML_TERM(int pml_term) {
		PML_TERM = pml_term;
	}
	public int getPML_AGE() {
		return PML_AGE;
	}
	public void setPML_AGE(int pml_age) {
		PML_AGE = pml_age;
	}
	public double getPML_PREM_MULT() {
		return PML_PREM_MULT;
	}
	public void setPML_PREM_MULT(double pml_prem_mult) {
		PML_PREM_MULT = pml_prem_mult;
	}
	public int getPML_BAND() {
		return PML_BAND;
	}
	public void setPML_BAND(int pml_band) {
		PML_BAND = pml_band;
	}
	public String getSMOKER_FLAG() {
		return SMOKER_FLAG;
	}
	public void setSMOKER_FLAG(String smoker_flag) {
		SMOKER_FLAG = smoker_flag;
	}


}
