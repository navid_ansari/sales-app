package com.talic.plugins.sisEngine.BO;

import java.util.Date;

public class CommText implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private	String	cmt_pgl_id;
	private	String cmt_comm_text;
	private	String cmt_update_by;
	private Date cmt_update_dt;

	public String getCmt_pgl_id() {
		return cmt_pgl_id;
	}
	public void setCmt_pgl_id(String cmt_pgl_id) {
		this.cmt_pgl_id = cmt_pgl_id;
	}
	public String getCmt_comm_text() {
		return cmt_comm_text;
	}
	public void setCmt_comm_text(String cmt_comm_text) {
		this.cmt_comm_text = cmt_comm_text;
	}
	public String getCmt_update_by() {
		return cmt_update_by;
	}
	public void setCmt_update_by(String cmt_update_by) {
		this.cmt_update_by = cmt_update_by;
	}
	public Date getCmt_update_dt() {
		return cmt_update_dt;
	}
	public void setCmt_update_dt(Date cmt_update_dt) {
		this.cmt_update_dt = cmt_update_dt;
	}



}
