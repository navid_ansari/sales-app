package com.talic.plugins.sisEngine.BO;

public class IplIllussPagelist implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private	int	ipl_pgl_id;
	private	int	ipl_min_pt;
	private	int	ipl_max_pt;
	private	String ipl_min_page;
	private	String ipl_max_page;
	public int getIpl_pgl_id() {
		return ipl_pgl_id;
	}
	public void setIpl_pgl_id(int ipl_pgl_id) {
		this.ipl_pgl_id = ipl_pgl_id;
	}
	public int getIpl_min_pt() {
		return ipl_min_pt;
	}
	public void setIpl_min_pt(int ipl_min_pt) {
		this.ipl_min_pt = ipl_min_pt;
	}
	public int getIpl_max_pt() {
		return ipl_max_pt;
	}
	public void setIpl_max_pt(int ipl_max_pt) {
		this.ipl_max_pt = ipl_max_pt;
	}
	public String getIpl_min_page() {
		return ipl_min_page;
	}
	public void setIpl_min_page(String ipl_min_page) {
		this.ipl_min_page = ipl_min_page;
	}
	public String getIpl_max_page() {
		return ipl_max_page;
	}
	public void setIpl_max_page(String ipl_max_page) {
		this.ipl_max_page = ipl_max_page;
	}


}
