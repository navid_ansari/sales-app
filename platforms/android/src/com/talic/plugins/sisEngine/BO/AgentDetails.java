package com.talic.plugins.sisEngine.BO;

/**
 * Created by admin on 7/8/15.
 */
public class AgentDetails implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    private Object AgentDetails;
    private String AGENT_NAME;
    private String AGENT_NUMBER;
    private String AGENT_CONTACTNUMBER;

    public String getAGENT_NAME() {
        return AGENT_NAME;
    }

    public void setAGENT_NAME(String AGENT_NAME) {
        this.AGENT_NAME = AGENT_NAME;
    }

    public String getAGENT_NUMBER() {
        return AGENT_NUMBER;
    }

    public void setAGENT_NUMBER(String AGENT_NUMBER) {
        this.AGENT_NUMBER = AGENT_NUMBER;
    }

    public String getAGENT_CONTACTNUMBER() {
        return AGENT_CONTACTNUMBER;
    }

    public void setAGENT_CONTACTNUMBER(String AGENT_CONTACTNUMBER) {
        this.AGENT_CONTACTNUMBER = AGENT_CONTACTNUMBER;
    }



}
