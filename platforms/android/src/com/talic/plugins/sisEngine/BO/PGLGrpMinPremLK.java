package com.talic.plugins.sisEngine.BO;

public class PGLGrpMinPremLK implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PGL_GRP_ID;	//NUMBER
	private String PGL_PREMIUM_MODE;	//VARCHAR2(3 Bytes)
	private int PGL_MIN_PREM_AMT;	//NUMBER
	private int PGL_MAX_PREM_AMT;	//NUMBER
	private int PGL_MIN_PPT;	//NUMBER
	private int PGL_MAX_PPT;	//NUMBER
	private int PGL_MIN_SUM_ASSURED;	//NUMBER
	private long PGL_MAX_SUM_ASSURED;	//NUMBER



	public int getPGL_GRP_ID() {
		return PGL_GRP_ID;
	}
	public void setPGL_GRP_ID(int pgl_grp_id) {
		PGL_GRP_ID = pgl_grp_id;
	}
	public String getPGL_PREMIUM_MODE() {
		return PGL_PREMIUM_MODE;
	}
	public void setPGL_PREMIUM_MODE(String pgl_premium_mode) {
		PGL_PREMIUM_MODE = pgl_premium_mode;
	}
	public int getPGL_MIN_PREM_AMT() {
		return PGL_MIN_PREM_AMT;
	}
	public void setPGL_MIN_PREM_AMT(int pgl_min_prem_amt) {
		PGL_MIN_PREM_AMT = pgl_min_prem_amt;
	}
	public int getPGL_MAX_PREM_AMT() {
		return PGL_MAX_PREM_AMT;
	}
	public void setPGL_MAX_PREM_AMT(int pgl_max_prem_amt) {
		PGL_MAX_PREM_AMT = pgl_max_prem_amt;
	}
	public int getPGL_MIN_PPT() {
		return PGL_MIN_PPT;
	}
	public void setPGL_MIN_PPT(int pgl_min_ppt) {
		PGL_MIN_PPT = pgl_min_ppt;
	}
	public int getPGL_MAX_PPT() {
		return PGL_MAX_PPT;
	}
	public void setPGL_MAX_PPT(int pgl_max_ppt) {
		PGL_MAX_PPT = pgl_max_ppt;
	}
	public int getPGL_MIN_SUM_ASSURED() {
		return PGL_MIN_SUM_ASSURED;
	}
	public void setPGL_MIN_SUM_ASSURED(int pgl_min_sum_assured) {
		PGL_MIN_SUM_ASSURED = pgl_min_sum_assured;
	}
	public long getPGL_MAX_SUM_ASSURED() {
		return PGL_MAX_SUM_ASSURED;
	}
	public void setPGL_MAX_SUM_ASSURED(long pgl_max_sum_assured) {
		PGL_MAX_SUM_ASSURED = pgl_max_sum_assured;
	}

}
