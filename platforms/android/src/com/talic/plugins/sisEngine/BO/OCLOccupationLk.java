package com.talic.plugins.sisEngine.BO;

public class OCLOccupationLk implements java.io.Serializable {

	private int	OCL_ID;//	NUMBER
	   private String OCL_CODE; //	VARCHAR2(10 Bytes)
	   private String OCL_DESCRIPTION;	//VARCHAR2(180 Bytes)
	   private String OCL_IND;	//CHAR(1 Bytes)
	   private String	OCL_OCCUPATION; //	VARCHAR2(50 Bytes)
	   private String OCL_BUSINESS;	//VARCHAR2(50 Bytes)
	   private double OCL_BLIFE;	//NUMBER
	   private double OCL_ADD;	//NUMBER
	   private double OCL_PA;	//NUMBER
	   private double OCL_WP;	//NUMBER
	   private double OCL_ADB;	//NUMBER
	   private double OCL_AI;	//NUMBER
	   private double OCL_HS;	//NUMBER
	   private String  OCL_RCC;	//VARCHAR2(5 Bytes)
	   private String  OCL_UW_CODE;	//VARCHAR2(5 Bytes)
	   private int OCL_CLASS;	//NUMBER
	   private String  OCL_TYPE;	//VARCHAR2(2 Bytes)
	   private int  OCL_HB;	//NUMBER
	   private String OCL_SOCIAL;//	CHAR(1 Bytes)


	public int getOCL_ID() {
		return OCL_ID;
	}
	public void setOCL_ID(int ocl_id) {
		OCL_ID = ocl_id;
	}
	public String getOCL_CODE() {
		return OCL_CODE;
	}
	public void setOCL_CODE(String ocl_code) {
		OCL_CODE = ocl_code;
	}
	public String getOCL_DESCRIPTION() {
		return OCL_DESCRIPTION;
	}
	public void setOCL_DESCRIPTION(String ocl_description) {
		OCL_DESCRIPTION = ocl_description;
	}
	public String getOCL_IND() {
		return OCL_IND;
	}
	public void setOCL_IND(String ocl_ind) {
		OCL_IND = ocl_ind;
	}
	public String getOCL_OCCUPATION() {
		return OCL_OCCUPATION;
	}
	public void setOCL_OCCUPATION(String ocl_occupation) {
		OCL_OCCUPATION = ocl_occupation;
	}
	public String getOCL_BUSINESS() {
		return OCL_BUSINESS;
	}
	public void setOCL_BUSINESS(String ocl_business) {
		OCL_BUSINESS = ocl_business;
	}
	public double getOCL_BLIFE() {
		return OCL_BLIFE;
	}
	public void setOCL_BLIFE(double ocl_blife) {
		OCL_BLIFE = ocl_blife;
	}
	public double getOCL_ADD() {
		return OCL_ADD;
	}
	public void setOCL_ADD(double ocl_add) {
		OCL_ADD = ocl_add;
	}
	public double getOCL_PA() {
		return OCL_PA;
	}
	public void setOCL_PA(double ocl_pa) {
		OCL_PA = ocl_pa;
	}
	public double getOCL_WP() {
		return OCL_WP;
	}
	public void setOCL_WP(double ocl_wp) {
		OCL_WP = ocl_wp;
	}
	public double getOCL_ADB() {
		return OCL_ADB;
	}
	public void setOCL_ADB(double ocl_adb) {
		OCL_ADB = ocl_adb;
	}
	public double getOCL_AI() {
		return OCL_AI;
	}
	public void setOCL_AI(double ocl_ai) {
		OCL_AI = ocl_ai;
	}
	public double getOCL_HS() {
		return OCL_HS;
	}
	public void setOCL_HS(double ocl_hs) {
		OCL_HS = ocl_hs;
	}
	public String getOCL_RCC() {
		return OCL_RCC;
	}
	public void setOCL_RCC(String ocl_rcc) {
		OCL_RCC = ocl_rcc;
	}
	public String getOCL_UW_CODE() {
		return OCL_UW_CODE;
	}
	public void setOCL_UW_CODE(String ocl_uw_code) {
		OCL_UW_CODE = ocl_uw_code;
	}
	public int getOCL_CLASS() {
		return OCL_CLASS;
	}
	public void setOCL_CLASS(int ocl_class) {
		OCL_CLASS = ocl_class;
	}
	public String getOCL_TYPE() {
		return OCL_TYPE;
	}
	public void setOCL_TYPE(String ocl_type) {
		OCL_TYPE = ocl_type;
	}
	public int getOCL_HB() {
		return OCL_HB;
	}
	public void setOCL_HB(int ocl_hb) {
		OCL_HB = ocl_hb;
	}
	public String getOCL_SOCIAL() {
		return OCL_SOCIAL;
	}
	public void setOCL_SOCIAL(String ocl_social) {
		OCL_SOCIAL = ocl_social;
	}
	private static final long serialVersionUID = 1L;


}
