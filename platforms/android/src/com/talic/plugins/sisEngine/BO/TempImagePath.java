package com.talic.plugins.sisEngine.BO;

public class TempImagePath implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private	int	tip_pgl_id;
	private	String tip_image_name;
	public int getTip_pgl_id() {
		return tip_pgl_id;
	}
	public void setTip_pgl_id(int tip_pgl_id) {
		this.tip_pgl_id = tip_pgl_id;
	}
	public String getTip_image_name() {
		return tip_image_name;
	}
	public void setTip_image_name(String tip_image_name) {
		this.tip_image_name = tip_image_name;
	}


}
