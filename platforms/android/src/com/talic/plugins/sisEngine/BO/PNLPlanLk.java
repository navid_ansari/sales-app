package com.talic.plugins.sisEngine.BO;

import java.util.Date;

public class PNLPlanLk implements java.io.Serializable{


	private int PNL_ID;	//NUMBER
	private String PNL_CODE;	//VARCHAR2(16 Bytes)
	private String PNL_DESCRIPTION;	//VARCHAR2(100 Bytes)
	private int PNL_PGL_ID;	//NUMBER
	private String PNL_TYPE;	//VARCHAR2(2 Bytes)
	private Date PNL_EFFECTIVE_DATE;	//DATE
	private Date PNL_END_DATE;	//DATE
	private int PNL_MIN_AGE;	//NUMBER
	private int PNL_MAX_AGE;	//NUMBER
	private int PNL_MIN_SUM_ASSURED;	//NUMBER
	private long PNL_MAX_SUM_ASSURED;	//NUMBER
	private int PNL_MIN_PREMIUM;	//NUMBER
	private int PNL_MIN_OCCUPATION_CLASS;	//NUMBER
	private int PNL_MAX_OCCUPATION_CLASS;	//NUMBER
	private int PNL_PREMIUM_MULTIPLE;	//NUMBER
	private int PNL_MAX_PREMIUM_PAYING_TERM;	//NUMBER
	private String PNL_PAMXUSE;	//CHAR(1 Bytes)
	private String PNL_FREQUENCY;	//VARCHAR2(20 Bytes)
	private String  PNL_UPDATED_BY;	//VARCHAR2(50 Bytes)
	private Date  PNL_UPDATED_DATE;	//DATE
	private double PNL_SERVICE_TAX;	//NUMBER(5,2)
	private int PNL_PREM_MULT_FORMULA;	//NUMBER
	private long PNL_MIN_SINGLE_TOPUP_PREM;	//NUMBER
	private int PNL_LOCK_IN_TOPUP_WITHDRAWAL;	//NUMBER
	private int PNL_MIN_PURCHASE_PRICE;	//NUMBER
	private int PNL_MAX_PURCHASE_PRICE;	//NUMBER
	private String ISSIMPLIFIED;	//VARCHAR2(1 Bytes)
	private String ISANNUITYPRODUCT;	//VARCHAR2(1 Bytes)
	private String ISBACKDATE;	//VARCHAR2(1 Bytes)
	private int NOOFMONTH;	//NUMBER(1,0)
	private String PNL_APPLICATION;	//VARCHAR2(6 Bytes)
	private int FIXCVGID;	//NUMBER
	private String UIN;	//VARCHAR2(20 Bytes)
	private int PNL_SA_MULTIPLEOF;	//NUMBER
	private String PNL_AGENTTYPE;	//VARCHAR2(500 Bytes)
	private String PNL_ANNUITY_FREQUENCY;	//VARCHAR2(20 Bytes)
	private int FREE_SWITCHE_CNT;	//NUMBER
	private double SWITCH_CHARGE_AMT;	//NUMBER(10,2)
	private int FREE_PREM_REDIR_CNT;	//NUMBER
	private double PREM_REDIR_CHARGE_AMT;	//NUMBER(10,2)
	private double UVRMINATOP;	//NUMBER(15,2)
	private double UVRMAXATOP;	//NUMBER(15,2)
	private int UVRMAXATM;	//NUMBER(5,0)
	private int UVRMINRTOP;	//NUMBER(5,0)
	private double UVRMAXRTOP;	//NUMBER(15,2)
	private String UVRFILLER0;	//VARCHAR2(5 Bytes)
	private String UVRFILLER1;	//VARCHAR2(5 Bytes)
	private String UVRFILLER2;	//VARCHAR2(10 Bytes)
	private String UVRFILLER3;	//VARCHAR2(10 Bytes)
	private String UVRFILLER4;	//VARCHAR2(10 Bytes)
	private String URALLOW;	//VARCHAR2(1 Bytes)
	private double URPERCENT;	//NUMBER(5,2)
	private int REN_MTHLY;	//NUMBER(3,0)
	private int REN_QTRLY;	//NUMBER(3,0)
	private int REN_SANAL;	//NUMBER(3,0)
	private int REN_ANNUAL;	//NUMBER(3,0)
	private int REN_MTHLY_INT;	//NUMBER(3,0)
	private int REN_QTRLY_INT;	//NUMBER(3,0)
	private int REN_SANAL_INT;	//NUMBER(3,0)
	private int REN_ANNUAL_INT;	//NUMBER(3,0)
	private int REN_PTD_NO;	//NUMBER(3,0)
	private int REN_ISSUE_NO;	//NUMBER(3,0)
	private String HQGRP;	//VARCHAR2(5 Bytes)
	private String	PNL_START_PT;	//VARCHAR2(1 Char)
	private double PNL_MIN_INCOME;	//NUMBER(13,2)
	private double PNL_MAX_INCOME;	//NUMBER(13,2)

	public int getPNL_ID() {
		return PNL_ID;
	}
	public void setPNL_ID(int pnl_id) {
		PNL_ID = pnl_id;
	}
	public String getPNL_CODE() {
		return PNL_CODE;
	}
	public void setPNL_CODE(String pnl_code) {
		PNL_CODE = pnl_code;
	}
	public String getPNL_DESCRIPTION() {
		return PNL_DESCRIPTION;
	}
	public void setPNL_DESCRIPTION(String pnl_description) {
		PNL_DESCRIPTION = pnl_description;
	}
	public int getPNL_PGL_ID() {
		return PNL_PGL_ID;
	}
	public void setPNL_PGL_ID(int pnl_pgl_id) {
		PNL_PGL_ID = pnl_pgl_id;
	}
	public String getPNL_TYPE() {
		return PNL_TYPE;
	}
	public void setPNL_TYPE(String pnl_type) {
		PNL_TYPE = pnl_type;
	}
	public Date getPNL_EFFECTIVE_DATE() {
		return PNL_EFFECTIVE_DATE;
	}
	public void setPNL_EFFECTIVE_DATE(Date pnl_effective_date) {
		PNL_EFFECTIVE_DATE = pnl_effective_date;
	}
	public Date getPNL_END_DATE() {
		return PNL_END_DATE;
	}
	public void setPNL_END_DATE(Date pnl_end_date) {
		PNL_END_DATE = pnl_end_date;
	}
	public int getPNL_MIN_AGE() {
		return PNL_MIN_AGE;
	}
	public void setPNL_MIN_AGE(int pnl_min_age) {
		PNL_MIN_AGE = pnl_min_age;
	}
	public int getPNL_MAX_AGE() {
		return PNL_MAX_AGE;
	}
	public void setPNL_MAX_AGE(int pnl_max_age) {
		PNL_MAX_AGE = pnl_max_age;
	}
	public int getPNL_MIN_SUM_ASSURED() {
		return PNL_MIN_SUM_ASSURED;
	}
	public void setPNL_MIN_SUM_ASSURED(int pnl_min_sum_assured) {
		PNL_MIN_SUM_ASSURED = pnl_min_sum_assured;
	}
	public long getPNL_MAX_SUM_ASSURED() {
		return PNL_MAX_SUM_ASSURED;
	}
	public void setPNL_MAX_SUM_ASSURED(long pnl_max_sum_assured) {
		PNL_MAX_SUM_ASSURED = pnl_max_sum_assured;
	}
	public int getPNL_MIN_PREMIUM() {
		return PNL_MIN_PREMIUM;
	}
	public void setPNL_MIN_PREMIUM(int pnl_min_premium) {
		PNL_MIN_PREMIUM = pnl_min_premium;
	}
	public int getPNL_MIN_OCCUPATION_CLASS() {
		return PNL_MIN_OCCUPATION_CLASS;
	}
	public void setPNL_MIN_OCCUPATION_CLASS(int pnl_min_occupation_class) {
		PNL_MIN_OCCUPATION_CLASS = pnl_min_occupation_class;
	}
	public int getPNL_MAX_OCCUPATION_CLASS() {
		return PNL_MAX_OCCUPATION_CLASS;
	}
	public void setPNL_MAX_OCCUPATION_CLASS(int pnl_max_occupation_class) {
		PNL_MAX_OCCUPATION_CLASS = pnl_max_occupation_class;
	}
	public int getPNL_PREMIUM_MULTIPLE() {
		return PNL_PREMIUM_MULTIPLE;
	}
	public void setPNL_PREMIUM_MULTIPLE(int pnl_premium_multiple) {
		PNL_PREMIUM_MULTIPLE = pnl_premium_multiple;
	}
	public int getPNL_MAX_PREMIUM_PAYING_TERM() {
		return PNL_MAX_PREMIUM_PAYING_TERM;
	}
	public void setPNL_MAX_PREMIUM_PAYING_TERM(int pnl_max_premium_paying_term) {
		PNL_MAX_PREMIUM_PAYING_TERM = pnl_max_premium_paying_term;
	}
	public String getPNL_PAMXUSE() {
		return PNL_PAMXUSE;
	}
	public void setPNL_PAMXUSE(String pnl_pamxuse) {
		PNL_PAMXUSE = pnl_pamxuse;
	}
	public String getPNL_FREQUENCY() {
		return PNL_FREQUENCY;
	}
	public void setPNL_FREQUENCY(String pnl_frequency) {
		PNL_FREQUENCY = pnl_frequency;
	}
	public String getPNL_UPDATED_BY() {
		return PNL_UPDATED_BY;
	}
	public void setPNL_UPDATED_BY(String pnl_updated_by) {
		PNL_UPDATED_BY = pnl_updated_by;
	}
	public Date getPNL_UPDATED_DATE() {
		return PNL_UPDATED_DATE;
	}
	public void setPNL_UPDATED_DATE(Date pnl_updated_date) {
		PNL_UPDATED_DATE = pnl_updated_date;
	}
	public double getPNL_SERVICE_TAX() {
		return PNL_SERVICE_TAX;
	}
	public void setPNL_SERVICE_TAX(double pnl_service_tax) {
		PNL_SERVICE_TAX = pnl_service_tax;
	}
	public int getPNL_PREM_MULT_FORMULA() {
		return PNL_PREM_MULT_FORMULA;
	}
	public void setPNL_PREM_MULT_FORMULA(int pnl_prem_mult_formula) {
		PNL_PREM_MULT_FORMULA = pnl_prem_mult_formula;
	}
	public long getPNL_MIN_SINGLE_TOPUP_PREM() {
		return PNL_MIN_SINGLE_TOPUP_PREM;
	}
	public void setPNL_MIN_SINGLE_TOPUP_PREM(long pnl_min_single_topup_prem) {
		PNL_MIN_SINGLE_TOPUP_PREM = pnl_min_single_topup_prem;
	}
	public int getPNL_LOCK_IN_TOPUP_WITHDRAWAL() {
		return PNL_LOCK_IN_TOPUP_WITHDRAWAL;
	}
	public void setPNL_LOCK_IN_TOPUP_WITHDRAWAL(int pnl_lock_in_topup_withdrawal) {
		PNL_LOCK_IN_TOPUP_WITHDRAWAL = pnl_lock_in_topup_withdrawal;
	}
	public int getPNL_MIN_PURCHASE_PRICE() {
		return PNL_MIN_PURCHASE_PRICE;
	}
	public void setPNL_MIN_PURCHASE_PRICE(int pnl_min_purchase_price) {
		PNL_MIN_PURCHASE_PRICE = pnl_min_purchase_price;
	}
	public int getPNL_MAX_PURCHASE_PRICE() {
		return PNL_MAX_PURCHASE_PRICE;
	}
	public void setPNL_MAX_PURCHASE_PRICE(int pnl_max_purchase_price) {
		PNL_MAX_PURCHASE_PRICE = pnl_max_purchase_price;
	}
	public String getISSIMPLIFIED() {
		return ISSIMPLIFIED;
	}
	public void setISSIMPLIFIED(String issimplified) {
		ISSIMPLIFIED = issimplified;
	}
	public String getISANNUITYPRODUCT() {
		return ISANNUITYPRODUCT;
	}
	public void setISANNUITYPRODUCT(String isannuityproduct) {
		ISANNUITYPRODUCT = isannuityproduct;
	}
	public String getISBACKDATE() {
		return ISBACKDATE;
	}
	public void setISBACKDATE(String isbackdate) {
		ISBACKDATE = isbackdate;
	}
	public int getNOOFMONTH() {
		return NOOFMONTH;
	}
	public void setNOOFMONTH(int noofmonth) {
		NOOFMONTH = noofmonth;
	}
	public String getPNL_APPLICATION() {
		return PNL_APPLICATION;
	}
	public void setPNL_APPLICATION(String pnl_application) {
		PNL_APPLICATION = pnl_application;
	}
	public int getFIXCVGID() {
		return FIXCVGID;
	}
	public void setFIXCVGID(int fixcvgid) {
		FIXCVGID = fixcvgid;
	}
	public String getUIN() {
		return UIN;
	}
	public void setUIN(String uin) {
		UIN = uin;
	}
	public int getPNL_SA_MULTIPLEOF() {
		return PNL_SA_MULTIPLEOF;
	}
	public void setPNL_SA_MULTIPLEOF(int pnl_sa_multipleof) {
		PNL_SA_MULTIPLEOF = pnl_sa_multipleof;
	}
	public String getPNL_AGENTTYPE() {
		return PNL_AGENTTYPE;
	}
	public void setPNL_AGENTTYPE(String pnl_agenttype) {
		PNL_AGENTTYPE = pnl_agenttype;
	}
	public String getPNL_ANNUITY_FREQUENCY() {
		return PNL_ANNUITY_FREQUENCY;
	}
	public void setPNL_ANNUITY_FREQUENCY(String pnl_annuity_frequency) {
		PNL_ANNUITY_FREQUENCY = pnl_annuity_frequency;
	}
	public int getFREE_SWITCHE_CNT() {
		return FREE_SWITCHE_CNT;
	}
	public void setFREE_SWITCHE_CNT(int free_switche_cnt) {
		FREE_SWITCHE_CNT = free_switche_cnt;
	}
	public double getSWITCH_CHARGE_AMT() {
		return SWITCH_CHARGE_AMT;
	}
	public void setSWITCH_CHARGE_AMT(double switch_charge_amt) {
		SWITCH_CHARGE_AMT = switch_charge_amt;
	}
	public int getFREE_PREM_REDIR_CNT() {
		return FREE_PREM_REDIR_CNT;
	}
	public void setFREE_PREM_REDIR_CNT(int free_prem_redir_cnt) {
		FREE_PREM_REDIR_CNT = free_prem_redir_cnt;
	}
	public double getPREM_REDIR_CHARGE_AMT() {
		return PREM_REDIR_CHARGE_AMT;
	}
	public void setPREM_REDIR_CHARGE_AMT(double prem_redir_charge_amt) {
		PREM_REDIR_CHARGE_AMT = prem_redir_charge_amt;
	}
	public double getUVRMINATOP() {
		return UVRMINATOP;
	}
	public void setUVRMINATOP(double uvrminatop) {
		UVRMINATOP = uvrminatop;
	}
	public double getUVRMAXATOP() {
		return UVRMAXATOP;
	}
	public void setUVRMAXATOP(double uvrmaxatop) {
		UVRMAXATOP = uvrmaxatop;
	}
	public int getUVRMAXATM() {
		return UVRMAXATM;
	}
	public void setUVRMAXATM(int uvrmaxatm) {
		UVRMAXATM = uvrmaxatm;
	}
	public int getUVRMINRTOP() {
		return UVRMINRTOP;
	}
	public void setUVRMINRTOP(int uvrminrtop) {
		UVRMINRTOP = uvrminrtop;
	}
	public double getUVRMAXRTOP() {
		return UVRMAXRTOP;
	}
	public void setUVRMAXRTOP(double uvrmaxrtop) {
		UVRMAXRTOP = uvrmaxrtop;
	}
	public String getUVRFILLER0() {
		return UVRFILLER0;
	}
	public void setUVRFILLER0(String uvrfiller0) {
		UVRFILLER0 = uvrfiller0;
	}
	public String getUVRFILLER1() {
		return UVRFILLER1;
	}
	public void setUVRFILLER1(String uvrfiller1) {
		UVRFILLER1 = uvrfiller1;
	}
	public String getUVRFILLER2() {
		return UVRFILLER2;
	}
	public void setUVRFILLER2(String uvrfiller2) {
		UVRFILLER2 = uvrfiller2;
	}
	public String getUVRFILLER3() {
		return UVRFILLER3;
	}
	public void setUVRFILLER3(String uvrfiller3) {
		UVRFILLER3 = uvrfiller3;
	}
	public String getUVRFILLER4() {
		return UVRFILLER4;
	}
	public void setUVRFILLER4(String uvrfiller4) {
		UVRFILLER4 = uvrfiller4;
	}
	public String getURALLOW() {
		return URALLOW;
	}
	public void setURALLOW(String urallow) {
		URALLOW = urallow;
	}
	public double getURPERCENT() {
		return URPERCENT;
	}
	public void setURPERCENT(double urpercent) {
		URPERCENT = urpercent;
	}
	public int getREN_MTHLY() {
		return REN_MTHLY;
	}
	public void setREN_MTHLY(int ren_mthly) {
		REN_MTHLY = ren_mthly;
	}
	public int getREN_QTRLY() {
		return REN_QTRLY;
	}
	public void setREN_QTRLY(int ren_qtrly) {
		REN_QTRLY = ren_qtrly;
	}
	public int getREN_SANAL() {
		return REN_SANAL;
	}
	public void setREN_SANAL(int ren_sanal) {
		REN_SANAL = ren_sanal;
	}
	public int getREN_ANNUAL() {
		return REN_ANNUAL;
	}
	public void setREN_ANNUAL(int ren_annual) {
		REN_ANNUAL = ren_annual;
	}
	public int getREN_MTHLY_INT() {
		return REN_MTHLY_INT;
	}
	public void setREN_MTHLY_INT(int ren_mthly_int) {
		REN_MTHLY_INT = ren_mthly_int;
	}
	public int getREN_QTRLY_INT() {
		return REN_QTRLY_INT;
	}
	public void setREN_QTRLY_INT(int ren_qtrly_int) {
		REN_QTRLY_INT = ren_qtrly_int;
	}
	public int getREN_SANAL_INT() {
		return REN_SANAL_INT;
	}
	public void setREN_SANAL_INT(int ren_sanal_int) {
		REN_SANAL_INT = ren_sanal_int;
	}
	public int getREN_ANNUAL_INT() {
		return REN_ANNUAL_INT;
	}
	public void setREN_ANNUAL_INT(int ren_annual_int) {
		REN_ANNUAL_INT = ren_annual_int;
	}
	public int getREN_PTD_NO() {
		return REN_PTD_NO;
	}
	public void setREN_PTD_NO(int ren_ptd_no) {
		REN_PTD_NO = ren_ptd_no;
	}
	public int getREN_ISSUE_NO() {
		return REN_ISSUE_NO;
	}
	public void setREN_ISSUE_NO(int ren_issue_no) {
		REN_ISSUE_NO = ren_issue_no;
	}
	public String getHQGRP() {
		return HQGRP;
	}
	public void setHQGRP(String hqgrp) {
		HQGRP = hqgrp;
	}
	public String getPNL_START_PT() {
		return PNL_START_PT;
	}
	public void setPNL_START_PT(String pnl_start_pt) {
		PNL_START_PT = pnl_start_pt;
	}
	public double getPNL_MIN_INCOME() {
		return PNL_MIN_INCOME;
	}
	public void setPNL_MIN_INCOME(double pnl_min_income) {
		PNL_MIN_INCOME = pnl_min_income;
	}
	public double getPNL_MAX_INCOME() {
		return PNL_MAX_INCOME;
	}
	public void setPNL_MAX_INCOME(double pnl_max_income) {
		PNL_MAX_INCOME = pnl_max_income;
	}
	private static final long serialVersionUID = 1L;



}
