package com.talic.plugins.sisEngine.BO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {


	/**
	 * @param args
	 */
	public static void main(String[] args) {

        String strDate = "10/18/1983";

        try {

            System.out.println("Converted date is : " + getFormattedDate(strDate));

        } catch (Exception pe) {
            System.out.println("Parse Exception : " + pe);
        }
    }

	public static String getFormattedDate(String strDate ){
		try {
            SimpleDateFormat sdfSource = new SimpleDateFormat(
                    "MM/dd/yyyy");
            Date date = sdfSource.parse(strDate);
            SimpleDateFormat sdfDestination = new SimpleDateFormat(
                    "dd-MMM-yyyy");
            strDate = sdfDestination.format(date);

        } catch (Exception pe) {
            System.out.println(" Exception in getFormattedDate : " + pe);
        }

		return strDate;
	}
}
