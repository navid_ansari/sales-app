package com.talic.plugins.sisEngine.BO;

public class RDLChargesValue implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private	int	RCS_ID;
	private	int	RCS_PNL_ID;
	private	int	RCS_RDL_ID;
	private	String	RCS_GENDER;
	private	String	RCS_BAND;
	private	int	RCS_AGE	;
	private	String	RCS_OCCUR;
	private	double	RCS_NSP;
	private	String	RCS_USE;
	private	int	RCS_NUM;
	private	int	RCS_DUR1;
	private	double	RCS_CVAL1;
	private	int	RCS_DUR2;
	private	double	RCS_CVAL2;
	private	int	RCS_DUR3;
	private	double	RCS_CVAL3;
	private	int	RCS_DUR4;
	private	double	RCS_CVAL4;
	private	int	RCS_DUR5;
	private	double	RCS_CVAL5;
	private	int	RCS_DUR6;
	private	double	RCS_CVAL6;
	private	int	RCS_DUR7;
	private	double	RCS_CVAL7;
	private	int	RCS_DUR8;
	private	double	RCS_CVAL8;
	private	int	RCS_DUR9;
	private	double	RCS_CVAL9;
	private	int	RCS_DUR10;
	private	double	RCS_CVAL10;

	public int getRCS_ID() {
		return RCS_ID;
	}
	public void setRCS_ID(int RCS_id) {
		RCS_ID = RCS_id;
	}
	public int getRCS_PNL_ID() {
		return RCS_PNL_ID;
	}
	public void setRCS_PNL_ID(int RCS_pnl_id) {
		RCS_PNL_ID = RCS_pnl_id;
	}
	public String getRCS_GENDER() {
		return RCS_GENDER;
	}
	public void setRCS_GENDER(String RCS_gender) {
		RCS_GENDER = RCS_gender;
	}
	public String getRCS_BAND() {
		return RCS_BAND;
	}
	public void setRCS_BAND(String RCS_band) {
		RCS_BAND = RCS_band;
	}
	public int getRCS_AGE() {
		return RCS_AGE;
	}
	public void setRCS_AGE(int RCS_age) {
		RCS_AGE = RCS_age;
	}
	public String getRCS_OCCUR() {
		return RCS_OCCUR;
	}
	public void setRCS_OCCUR(String RCS_occur) {
		RCS_OCCUR = RCS_occur;
	}
	public double getRCS_NSP() {
		return RCS_NSP;
	}
	public void setRCS_NSP(double RCS_nsp) {
		RCS_NSP = RCS_nsp;
	}
	public String getRCS_USE() {
		return RCS_USE;
	}
	public void setRCS_USE(String RCS_use) {
		RCS_USE = RCS_use;
	}
	public int getRCS_NUM() {
		return RCS_NUM;
	}
	public void setRCS_NUM(int RCS_num) {
		RCS_NUM = RCS_num;
	}
	public int getRCS_DUR1() {
		return RCS_DUR1;
	}
	public void setRCS_DUR1(int RCS_dur1) {
		RCS_DUR1 = RCS_dur1;
	}
	public double getRCS_CVAL1() {
		return RCS_CVAL1;
	}
	public void setRCS_CVAL1(double RCS_cval1) {
		RCS_CVAL1 = RCS_cval1;
	}
	public int getRCS_DUR2() {
		return RCS_DUR2;
	}
	public void setRCS_DUR2(int RCS_dur2) {
		RCS_DUR2 = RCS_dur2;
	}
	public double getRCS_CVAL2() {
		return RCS_CVAL2;
	}
	public void setRCS_CVAL2(double RCS_cval2) {
		RCS_CVAL2 = RCS_cval2;
	}
	public int getRCS_DUR3() {
		return RCS_DUR3;
	}
	public void setRCS_DUR3(int RCS_dur3) {
		RCS_DUR3 = RCS_dur3;
	}
	public double getRCS_CVAL3() {
		return RCS_CVAL3;
	}
	public void setRCS_CVAL3(double RCS_cval3) {
		RCS_CVAL3 = RCS_cval3;
	}
	public int getRCS_DUR4() {
		return RCS_DUR4;
	}
	public void setRCS_DUR4(int RCS_dur4) {
		RCS_DUR4 = RCS_dur4;
	}
	public double getRCS_CVAL4() {
		return RCS_CVAL4;
	}
	public void setRCS_CVAL4(double RCS_cval4) {
		RCS_CVAL4 = RCS_cval4;
	}
	public int getRCS_DUR5() {
		return RCS_DUR5;
	}
	public void setRCS_DUR5(int RCS_dur5) {
		RCS_DUR5 = RCS_dur5;
	}
	public double getRCS_CVAL5() {
		return RCS_CVAL5;
	}
	public void setRCS_CVAL5(double RCS_cval5) {
		RCS_CVAL5 = RCS_cval5;
	}
	public int getRCS_DUR6() {
		return RCS_DUR6;
	}
	public void setRCS_DUR6(int RCS_dur6) {
		RCS_DUR6 = RCS_dur6;
	}
	public double getRCS_CVAL6() {
		return RCS_CVAL6;
	}
	public void setRCS_CVAL6(double RCS_cval6) {
		RCS_CVAL6 = RCS_cval6;
	}
	public int getRCS_DUR7() {
		return RCS_DUR7;
	}
	public void setRCS_DUR7(int RCS_dur7) {
		RCS_DUR7 = RCS_dur7;
	}
	public double getRCS_CVAL7() {
		return RCS_CVAL7;
	}
	public void setRCS_CVAL7(double RCS_cval7) {
		RCS_CVAL7 = RCS_cval7;
	}
	public int getRCS_DUR8() {
		return RCS_DUR8;
	}
	public void setRCS_DUR8(int RCS_dur8) {
		RCS_DUR8 = RCS_dur8;
	}
	public double getRCS_CVAL8() {
		return RCS_CVAL8;
	}
	public void setRCS_CVAL8(double RCS_cval8) {
		RCS_CVAL8 = RCS_cval8;
	}
	public int getRCS_DUR9() {
		return RCS_DUR9;
	}
	public void setRCS_DUR9(int RCS_dur9) {
		RCS_DUR9 = RCS_dur9;
	}
	public double getRCS_CVAL9() {
		return RCS_CVAL9;
	}
	public void setRCS_CVAL9(double RCS_cval9) {
		RCS_CVAL9 = RCS_cval9;
	}
	public int getRCS_DUR10() {
		return RCS_DUR10;
	}
	public void setRCS_DUR10(int RCS_dur10) {
		RCS_DUR10 = RCS_dur10;
	}
	public double getRCS_CVAL10() {
		return RCS_CVAL10;
	}
	public void setRCS_CVAL10(double RCS_cvl10) {
		RCS_CVAL10 = RCS_cvl10;
	}
	public int getRCS_RDL_ID() {
		return RCS_RDL_ID;
	}
	public void setRCS_RDL_ID(int rcs_rdl_id) {
		RCS_RDL_ID = rcs_rdl_id;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

}
