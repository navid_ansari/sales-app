package com.talic.plugins.sisEngine.BO;

public class MFMMaturityFactorMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;


	private int  MFF_PNL_ID;
	private int  MFM_BAND;
	private int  MFM_AGE;
	private String MFM_USE_FLG;
	private double MFM_VALUE;
	private String MFM_SEX;
	public int getMFF_PNL_ID() {
		return MFF_PNL_ID;
	}
	public void setMFF_PNL_ID(int mff_pnl_id) {
		MFF_PNL_ID = mff_pnl_id;
	}
	public int getMFM_BAND() {
		return MFM_BAND;
	}
	public void setMFM_BAND(int mfm_band) {
		MFM_BAND = mfm_band;
	}
	public int getMFM_AGE() {
		return MFM_AGE;
	}
	public void setMFM_AGE(int mfm_age) {
		MFM_AGE = mfm_age;
	}
	public String getMFM_USE_FLG() {
		return MFM_USE_FLG;
	}
	public void setMFM_USE_FLG(String mfm_use_flg) {
		MFM_USE_FLG = mfm_use_flg;
	}
	public double getMFM_VALUE() {
		return MFM_VALUE;
	}
	public void setMFM_VALUE(double mfm_value) {
		MFM_VALUE = mfm_value;
	}
	public String getMFM_SEX() {
		return MFM_SEX;
	}
	public void setMFM_SEX(String mfm_sex) {
		MFM_SEX = mfm_sex;
	}
}
