package com.talic.plugins.sisEngine.BO;

public class PMVPlanMaturityValue implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private	int	PMV_ID;
	private	int	PMV_PNL_ID;
	private	String	PMV_GENDER;
	private	String	PMV_BAND;
	private	int	PMV_AGE	;
	private	String	PMV_OCCUR;
	private	double	PMV_NSP;
	private	String	PMV_USE;
	private	int	PMV_NUM;
	private	int	PMV_DUR1;
	private	double	PMV_CVAL1;
	private	int	PMV_DUR2;
	private	double	PMV_CVAL2;
	private	int	PMV_DUR3;
	private	double	PMV_CVAL3;
	private	int	PMV_DUR4;
	private	double	PMV_CVAL4;
	private	int	PMV_DUR5;
	private	double	PMV_CVAL5;
	private	int	PMV_DUR6;
	private	double	PMV_CVAL6;
	private	int	PMV_DUR7;
	private	double	PMV_CVAL7;
	private	int	PMV_DUR8;
	private	double	PMV_CVAL8;
	private	int	PMV_DUR9;
	private	double	PMV_CVAL9;
	private	int	PMV_DUR10;
	private	double	PMV_CVAL10;

	public int getPMV_ID() {
		return PMV_ID;
	}
	public void setPMV_ID(int pmv_id) {
		PMV_ID = pmv_id;
	}
	public int getPMV_PNL_ID() {
		return PMV_PNL_ID;
	}
	public void setPMV_PNL_ID(int pmv_pnl_id) {
		PMV_PNL_ID = pmv_pnl_id;
	}
	public String getPMV_GENDER() {
		return PMV_GENDER;
	}
	public void setPMV_GENDER(String pmv_gender) {
		PMV_GENDER = pmv_gender;
	}
	public String getPMV_BAND() {
		return PMV_BAND;
	}
	public void setPMV_BAND(String pmv_band) {
		PMV_BAND = pmv_band;
	}
	public int getPMV_AGE() {
		return PMV_AGE;
	}
	public void setPMV_AGE(int pmv_age) {
		PMV_AGE = pmv_age;
	}
	public String getPMV_OCCUR() {
		return PMV_OCCUR;
	}
	public void setPMV_OCCUR(String pmv_occur) {
		PMV_OCCUR = pmv_occur;
	}
	public double getPMV_NSP() {
		return PMV_NSP;
	}
	public void setPMV_NSP(double pmv_nsp) {
		PMV_NSP = pmv_nsp;
	}
	public String getPMV_USE() {
		return PMV_USE;
	}
	public void setPMV_USE(String pmv_use) {
		PMV_USE = pmv_use;
	}
	public int getPMV_NUM() {
		return PMV_NUM;
	}
	public void setPMV_NUM(int pmv_num) {
		PMV_NUM = pmv_num;
	}
	public int getPMV_DUR1() {
		return PMV_DUR1;
	}
	public void setPMV_DUR1(int pmv_dur1) {
		PMV_DUR1 = pmv_dur1;
	}
	public double getPMV_CVAL1() {
		return PMV_CVAL1;
	}
	public void setPMV_CVAL1(double pmv_cval1) {
		PMV_CVAL1 = pmv_cval1;
	}
	public int getPMV_DUR2() {
		return PMV_DUR2;
	}
	public void setPMV_DUR2(int pmv_dur2) {
		PMV_DUR2 = pmv_dur2;
	}
	public double getPMV_CVAL2() {
		return PMV_CVAL2;
	}
	public void setPMV_CVAL2(double pmv_cval2) {
		PMV_CVAL2 = pmv_cval2;
	}
	public int getPMV_DUR3() {
		return PMV_DUR3;
	}
	public void setPMV_DUR3(int pmv_dur3) {
		PMV_DUR3 = pmv_dur3;
	}
	public double getPMV_CVAL3() {
		return PMV_CVAL3;
	}
	public void setPMV_CVAL3(double pmv_cval3) {
		PMV_CVAL3 = pmv_cval3;
	}
	public int getPMV_DUR4() {
		return PMV_DUR4;
	}
	public void setPMV_DUR4(int pmv_dur4) {
		PMV_DUR4 = pmv_dur4;
	}
	public double getPMV_CVAL4() {
		return PMV_CVAL4;
	}
	public void setPMV_CVAL4(double pmv_cval4) {
		PMV_CVAL4 = pmv_cval4;
	}
	public int getPMV_DUR5() {
		return PMV_DUR5;
	}
	public void setPMV_DUR5(int pmv_dur5) {
		PMV_DUR5 = pmv_dur5;
	}
	public double getPMV_CVAL5() {
		return PMV_CVAL5;
	}
	public void setPMV_CVAL5(double pmv_cval5) {
		PMV_CVAL5 = pmv_cval5;
	}
	public int getPMV_DUR6() {
		return PMV_DUR6;
	}
	public void setPMV_DUR6(int pmv_dur6) {
		PMV_DUR6 = pmv_dur6;
	}
	public double getPMV_CVAL6() {
		return PMV_CVAL6;
	}
	public void setPMV_CVAL6(double pmv_cval6) {
		PMV_CVAL6 = pmv_cval6;
	}
	public int getPMV_DUR7() {
		return PMV_DUR7;
	}
	public void setPMV_DUR7(int pmv_dur7) {
		PMV_DUR7 = pmv_dur7;
	}
	public double getPMV_CVAL7() {
		return PMV_CVAL7;
	}
	public void setPMV_CVAL7(double pmv_cval7) {
		PMV_CVAL7 = pmv_cval7;
	}
	public int getPMV_DUR8() {
		return PMV_DUR8;
	}
	public void setPMV_DUR8(int pmv_dur8) {
		PMV_DUR8 = pmv_dur8;
	}
	public double getPMV_CVAL8() {
		return PMV_CVAL8;
	}
	public void setPMV_CVAL8(double pmv_cval8) {
		PMV_CVAL8 = pmv_cval8;
	}
	public int getPMV_DUR9() {
		return PMV_DUR9;
	}
	public void setPMV_DUR9(int pmv_dur9) {
		PMV_DUR9 = pmv_dur9;
	}
	public double getPMV_CVAL9() {
		return PMV_CVAL9;
	}
	public void setPMV_CVAL9(double pmv_cval9) {
		PMV_CVAL9 = pmv_cval9;
	}
	public int getPMV_DUR10() {
		return PMV_DUR10;
	}
	public void setPMV_DUR10(int pmv_dur10) {
		PMV_DUR10 = pmv_dur10;
	}
	public double getPMV_CVAL10() {
		return PMV_CVAL10;
	}
	public void setPMV_CVAL10(double pmv_cvl10) {
		PMV_CVAL10 = pmv_cvl10;
	}


}
