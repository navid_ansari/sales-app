package com.talic.plugins.sisEngine.BO;

public class PCRPlanChargesRef implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int PCR_PNL_ID;	//NUMBER(10,0)
	private int PCR_ADMIN_ID;	//NUMBER(10,0)
	private int PCR_PREM_ID;	//NUMBER(10,0)
	private int PCR_MORT_ID;	//NUMBER(10,0)
	private int PCR_WOP_ID;	//NUMBER(10,0)
	private int PCR_FIB_ID;	//NUMBER(10,0)
	private int PCR_SUR_ID;
	private int PCR_GMM_ID;
	private int PCR_COMM_ID;

	public int getPCR_PNL_ID() {
		return PCR_PNL_ID;
	}
	public void setPCR_PNL_ID(int pcr_pnl_id) {
		PCR_PNL_ID = pcr_pnl_id;
	}
	public int getPCR_ADMIN_ID() {
		return PCR_ADMIN_ID;
	}
	public void setPCR_ADMIN_ID(int pcr_admin_id) {
		PCR_ADMIN_ID = pcr_admin_id;
	}
	public int getPCR_PREM_ID() {
		return PCR_PREM_ID;
	}
	public void setPCR_PREM_ID(int pcr_prem_id) {
		PCR_PREM_ID = pcr_prem_id;
	}
	public int getPCR_MORT_ID() {
		return PCR_MORT_ID;
	}
	public void setPCR_MORT_ID(int pcr_mort_id) {
		PCR_MORT_ID = pcr_mort_id;
	}
	public int getPCR_WOP_ID() {
		return PCR_WOP_ID;
	}
	public void setPCR_WOP_ID(int pcr_wop_id) {
		PCR_WOP_ID = pcr_wop_id;
	}
	public int getPCR_FIB_ID() {
		return PCR_FIB_ID;
	}
	public void setPCR_FIB_ID(int pcr_fib_id) {
		PCR_FIB_ID = pcr_fib_id;
	}
	public int getPCR_SUR_ID() {
		return PCR_SUR_ID;
	}
	public void setPCR_SUR_ID(int pcr_sur_id) {
		PCR_SUR_ID = pcr_sur_id;
	}
	public int getPCR_GMM_ID() {
		return PCR_GMM_ID;
	}
	public void setPCR_GMM_ID(int pcr_gmm_id) {
		PCR_GMM_ID = pcr_gmm_id;
	}
	public int getPCR_COMM_ID() {
		return PCR_COMM_ID;
	}
	public void setPCR_COMM_ID(int pcr_comm_id) {
		PCR_COMM_ID = pcr_comm_id;
	}



}
