package com.talic.plugins.sisEngine.BO;

public class PBAProductBand implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String PBA_PLAN_CODE;
	private String PBA_RIDER_CODE;
	private String PBA_TYPE;
	private String PBA_BAND;
	private long PBA_SUM_ASSURED;
	private int PBA_SEQ;
	public String getPBA_PLAN_CODE() {
		return PBA_PLAN_CODE;
	}
	public void setPBA_PLAN_CODE(String pba_plan_code) {
		PBA_PLAN_CODE = pba_plan_code;
	}
	public String getPBA_RIDER_CODE() {
		return PBA_RIDER_CODE;
	}
	public void setPBA_RIDER_CODE(String pba_rider_code) {
		PBA_RIDER_CODE = pba_rider_code;
	}
	public String getPBA_TYPE() {
		return PBA_TYPE;
	}
	public void setPBA_TYPE(String pba_type) {
		PBA_TYPE = pba_type;
	}
	public String getPBA_BAND() {
		return PBA_BAND;
	}
	public void setPBA_BAND(String pba_band) {
		PBA_BAND = pba_band;
	}
	public long getPBA_SUM_ASSURED() {
		return PBA_SUM_ASSURED;
	}
	public void setPBA_SUM_ASSURED(long pba_sum_assured) {
		PBA_SUM_ASSURED = pba_sum_assured;
	}
	public int getPBA_SEQ() {
		return PBA_SEQ;
	}
	public void setPBA_SEQ(int pba_seq) {
		PBA_SEQ = pba_seq;
	}




}
