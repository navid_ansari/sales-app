package com.talic.plugins.sisEngine.BO;

public class PNLTermPptLK implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String PNL_CODE;	//VARCHAR2(15 Bytes)
	private String TERM_FLAG;	//VARCHAR2(1 Bytes)
	private int MIN_TERM;	//NUMBER(3,0)
	private int MAX_TERM;	//NUMBER(3,0)
	private String TERM_FORMULA;	//VARCHAR2(100 Bytes)
	private int TERM;	//NUMBER(3,0)
	private String PPT_FLAG;	//VARCHAR2(1 Bytes)
	private String PPT_FORMULA;	//VARCHAR2(100 Bytes)
	private int PPT;	//NUMBER(3,0)
	private int MATURITY_AGE;	//NUMBER
	private int MIN_VESTING_AGE;	//NUMBER



	public String getPNL_CODE() {
		return PNL_CODE;
	}
	public void setPNL_CODE(String pnl_code) {
		PNL_CODE = pnl_code;
	}
	public String getTERM_FLAG() {
		return TERM_FLAG;
	}
	public void setTERM_FLAG(String term_flag) {
		TERM_FLAG = term_flag;
	}
	public int getMIN_TERM() {
		return MIN_TERM;
	}
	public void setMIN_TERM(int min_term) {
		MIN_TERM = min_term;
	}
	public int getMAX_TERM() {
		return MAX_TERM;
	}
	public void setMAX_TERM(int max_term) {
		MAX_TERM = max_term;
	}
	public String getTERM_FORMULA() {
		return TERM_FORMULA;
	}
	public void setTERM_FORMULA(String term_formula) {
		TERM_FORMULA = term_formula;
	}
	public int getTERM() {
		return TERM;
	}
	public void setTERM(int term) {
		TERM = term;
	}
	public String getPPT_FLAG() {
		return PPT_FLAG;
	}
	public void setPPT_FLAG(String ppt_flag) {
		PPT_FLAG = ppt_flag;
	}
	public String getPPT_FORMULA() {
		return PPT_FORMULA;
	}
	public void setPPT_FORMULA(String ppt_formula) {
		PPT_FORMULA = ppt_formula;
	}
	public int getPPT() {
		return PPT;
	}
	public void setPPT(int ppt) {
		PPT = ppt;
	}
	public int getMATURITY_AGE() {
		return MATURITY_AGE;
	}
	public void setMATURITY_AGE(int maturity_age) {
		MATURITY_AGE = maturity_age;
	}
	public int getMIN_VESTING_AGE() {
		return MIN_VESTING_AGE;
	}
	public void setMIN_VESTING_AGE(int min_vesting_age) {
		MIN_VESTING_AGE = min_vesting_age;
	}




}
