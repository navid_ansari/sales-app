package com.talic.plugins.sisEngine.BO;

public class FundDetailsLK implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PNL_PGL_ID;
	private String CODE;
	private String DESCRIPTION;
	private double FUND_MGMT_CHARGE;
	private int PERCENTAGE;
	private double PERF_3M;
	private double PERF_6M;
	private double PERF_1Y;
	private double PERF_2Y;
	private double PERF_3Y;
	private double PERF_4Y;
	private double PERF_5Y;
	private double PERF_INCEPTION;

	public int getPNL_PGL_ID() {
		return PNL_PGL_ID;
	}
	public void setPNL_PGL_ID(int pnl_pgl_id) {
		PNL_PGL_ID = pnl_pgl_id;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String code) {
		CODE = code;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String description) {
		DESCRIPTION = description;
	}
	public double getFUND_MGMT_CHARGE() {
		return FUND_MGMT_CHARGE;
	}
	public void setFUND_MGMT_CHARGE(double fund_mgmt_charge) {
		FUND_MGMT_CHARGE = fund_mgmt_charge;
	}
	public int getPERCENTAGE() {
		return PERCENTAGE;
	}
	public void setPERCENTAGE(int percentage) {
		PERCENTAGE = percentage;
	}
	public double getPERF_3M() {
		return PERF_3M;
	}
	public void setPERF_3M(double perf_3m) {
		PERF_3M = perf_3m;
	}
	public double getPERF_6M() {
		return PERF_6M;
	}
	public void setPERF_6M(double perf_6m) {
		PERF_6M = perf_6m;
	}
	public double getPERF_1Y() {
		return PERF_1Y;
	}
	public void setPERF_1Y(double perf_1y) {
		PERF_1Y = perf_1y;
	}
	public double getPERF_2Y() {
		return PERF_2Y;
	}
	public void setPERF_2Y(double perf_2y) {
		PERF_2Y = perf_2y;
	}
	public double getPERF_3Y() {
		return PERF_3Y;
	}
	public void setPERF_3Y(double perf_3y) {
		PERF_3Y = perf_3y;
	}
	public double getPERF_4Y() {
		return PERF_4Y;
	}
	public void setPERF_4Y(double perf_4y) {
		PERF_4Y = perf_4y;
	}
	public double getPERF_5Y() {
		return PERF_5Y;
	}
	public void setPERF_5Y(double perf_5y) {
		PERF_5Y = perf_5y;
	}
	public double getPERF_INCEPTION() {
		return PERF_INCEPTION;
	}
	public void setPERF_INCEPTION(double perf_inception) {
		PERF_INCEPTION = perf_inception;
	}
}
