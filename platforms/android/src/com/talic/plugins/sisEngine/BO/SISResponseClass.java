package com.talic.plugins.sisEngine.BO;

import java.io.Serializable;

public class SISResponseClass implements Serializable {

	private static final long serialVersionUID = 1L;
	private byte[] fileData ;
	private String Model_Premium;
	private String Maturity_Benefit6;
	private String Maturity_Benefit10;
	private String GAI;
	private String GAI_EOY;
	private String Maturity_Benefit;
	private String BusinessError;
	private String BusinessErrorCode;
	private String XMLString;
	private String TotalMatGAI;
	private String fileName;

	public byte[] getFileData() {
		return fileData;
	}
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
	public String getModel_Premium() {
		return Model_Premium;
	}
	public void setModel_Premium(String model_Premium) {
		Model_Premium = model_Premium;
	}
	public String getMaturity_Benefit6() {
		return Maturity_Benefit6;
	}
	public void setMaturity_Benefit6(String maturity_Benefit6) {
		Maturity_Benefit6 = maturity_Benefit6;
	}
	public String getMaturity_Benefit10() {
		return Maturity_Benefit10;
	}
	public void setMaturity_Benefit10(String maturity_Benefit10) {
		Maturity_Benefit10 = maturity_Benefit10;
	}
	public String getGAI() {
		return GAI;
	}
	public void setGAI(String gai) {
		GAI = gai;
	}
	public String getGAI_EOY() {
		return GAI_EOY;
	}
	public void setGAI_EOY(String gai_eoy) {
		GAI_EOY = gai_eoy;
	}
	public String getMaturity_Benefit() {
		return Maturity_Benefit;
	}
	public void setMaturity_Benefit(String maturity_Benefit) {
		Maturity_Benefit = maturity_Benefit;
	}
	public String getBusinessError() {
		return BusinessError;
	}
	public void setBusinessError(String businessError) {
		BusinessError = businessError;
	}
	public String getBusinessErrorCode() {
		return BusinessErrorCode;
	}
	public void setBusinessErrorCode(String businessErrorCode) {
		BusinessErrorCode = businessErrorCode;
	}
	public String getXMLString() {
		return XMLString;
	}
	public void setXMLString(String string) {
		XMLString = string;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTotalMatGAI() {
		return TotalMatGAI;
	}
	public void setTotalMatGAI(String totalMatGAI) {
		TotalMatGAI = totalMatGAI;
	}


}
