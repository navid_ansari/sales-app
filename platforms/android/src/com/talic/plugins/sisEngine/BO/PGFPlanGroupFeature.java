package com.talic.plugins.sisEngine.BO;

public class PGFPlanGroupFeature implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String PGF_PNL_CODE;
	private String PGF_FEATURE;
	public String getPGF_PNL_CODE() {
		return PGF_PNL_CODE;
	}
	public void setPGF_PNL_CODE(String pgf_pnl_code) {
		PGF_PNL_CODE = pgf_pnl_code;
	}
	public String getPGF_FEATURE() {
		return PGF_FEATURE;
	}
	public void setPGF_FEATURE(String pgf_feature) {
		PGF_FEATURE = pgf_feature;
	}



}
