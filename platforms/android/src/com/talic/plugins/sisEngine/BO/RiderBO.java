package com.talic.plugins.sisEngine.BO;

import java.io.Serializable;

public class RiderBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;
	private String description;
	private long sumAssured;
	private long primium;
	private int rtlID;
	private int polTerm;
	private int premPayTerm;
	private long Modalpremium;
	private long rdlID;
	private String rdlUIN;	//added by jayesh for Rider UIN

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getSumAssured() {
		return sumAssured;
	}
	public void setSumAssured(long sumAssured) {
		this.sumAssured = sumAssured;
	}
	public long getPrimium() {
		return primium;
	}
	public void setPrimium(long primium) {
		this.primium = primium;
	}
	public int getRtlID() {
		return rtlID;
	}
	public void setRtlID(int rtlID) {
		this.rtlID = rtlID;
	}
	public int getPolTerm() {
		return polTerm;
	}
	public void setPolTerm(int polTerm) {
		this.polTerm = polTerm;
	}
	public int getPremPayTerm() {
		return premPayTerm;
	}
	public void setPremPayTerm(int premPayTerm) {
		this.premPayTerm = premPayTerm;
	}
	public long getModalpremium() {
		return Modalpremium;
	}
	public void setModalpremium(long modalpremium) {
		Modalpremium = modalpremium;
	}
	public long getRdlID() {
		return rdlID;
	}
	public void setRdlID(long rdlID) {
		this.rdlID = rdlID;
	}
	public String getRdlUIN() {
		return rdlUIN;
	}
	public void setRdlUIN(String rdlUIN) {
		this.rdlUIN = rdlUIN;
	}


}
