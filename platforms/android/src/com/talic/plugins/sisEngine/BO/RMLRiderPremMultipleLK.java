package com.talic.plugins.sisEngine.BO;

public class RMLRiderPremMultipleLK implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int RML_ID;	//NUMBER
	private String RML_RIDER_CD;	//VARCHAR2(16 Bytes)
	private String RML_SEX;	//CHAR(1 Bytes)
	private int RML_TERM;	//NUMBER
	private int RML_AGE;	//NUMBER
	private double RML_PREM_MULT;	//NUMBER changed by Manish Bambhaniya from int to double
	private int RML_BAND;	//NUMBER



	public int getRML_ID() {
		return RML_ID;
	}
	public void setRML_ID(int rml_id) {
		RML_ID = rml_id;
	}
	public String getRML_RIDER_CD() {
		return RML_RIDER_CD;
	}
	public void setRML_RIDER_CD(String rml_rider_cd) {
		RML_RIDER_CD = rml_rider_cd;
	}
	public String getRML_SEX() {
		return RML_SEX;
	}
	public void setRML_SEX(String rml_sex) {
		RML_SEX = rml_sex;
	}
	public int getRML_TERM() {
		return RML_TERM;
	}
	public void setRML_TERM(int rml_term) {
		RML_TERM = rml_term;
	}
	public int getRML_AGE() {
		return RML_AGE;
	}
	public void setRML_AGE(int rml_age) {
		RML_AGE = rml_age;
	}
	public double getRML_PREM_MULT() {
		return RML_PREM_MULT;
	}
	public void setRML_PREM_MULT(double rml_prem_mult) {
		RML_PREM_MULT = rml_prem_mult;
	}
	public int getRML_BAND() {
		return RML_BAND;
	}
	public void setRML_BAND(int rml_band) {
		RML_BAND = rml_band;
	}




}
