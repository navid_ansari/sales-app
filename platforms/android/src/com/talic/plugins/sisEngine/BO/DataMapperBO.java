package com.talic.plugins.sisEngine.BO;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("unchecked")
public class DataMapperBO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;


	private ArrayList MDLModalFactorLk;
	private HashMap OCLOccupationLk;
	private HashMap PGLPlanGroupLk;
	private ArrayList PMLPremiumMultipleLk;
	private HashMap PNLChkExcludesLk;
	private HashMap PNLPlanLk;
	private ArrayList PGLGrpMinPremLK;
	private ArrayList PNLMinPremLK;
	private ArrayList PNLTermPptLK;
	private ArrayList RMLRiderPremMultipleLK;
	private ArrayList  PmdPlanPremiumMultDisc;
	private ArrayList PBAProductBand;
	private HashMap RDLRiderLk;
	private HashMap FDLFundLk;
	private ArrayList PMVPlanMaturityValue;
	private ArrayList PFRPlanFormulaRef;
	private ArrayList FEDFormulaExtDesc;
	private ArrayList Address;
	private HashMap PGFPlanGroupFeature;

	private ArrayList PACPremiumAllocationCharges;
	private ArrayList ACMAdminChargesMaster;
	private ArrayList MCMMortilityChargesMaster;
	private HashMap PCRPlanChargesRef;
	private ArrayList WCMWOPChargesMaster;
	private ArrayList FCMFIBChargesMaster;
	private ArrayList SURSurrenderChargeMaster;
	private ArrayList GuaranteedMaturityBonusMaster;
	private ArrayList CommissionChargeMaster;
	private ArrayList MFMMaturityFactorMaster;
	private ArrayList GAIGuarAnnIncChMaster;

	private ArrayList XmlMetadataMaster;
	private HashMap SPVSysParamValues;
	private ArrayList RDLCharges;

	private ArrayList IPCValues; // For Gold and Gold Plus Mohammad Rashid
	private ArrayList AAAFMCValues; // Samina Mohammad

	private ArrayList StdTemplateValues;
	private ArrayList iplIllussPagelist;
	private ArrayList commTextlist;
	private ArrayList annuityRatelist;
	private ArrayList FOPRatelist;
	private ArrayList incentiveRatelist;
	private ArrayList PremiumBoost;
	private AgentDetails Agent_inermedDetails;
	private HashMap tempImgPath;

	private ArrayList PNLComboLk; //Mohammad Rashid For Combo Plan
	private ArrayList serviceTaxMaster;

	public AgentDetails getAgent_inermedDetails() {
		return Agent_inermedDetails;
	}
	public void setAgent_inermedDetails(AgentDetails agent_inermedDetails) {
		Agent_inermedDetails = agent_inermedDetails;
	}



	public ArrayList getPmdPlanPremiumMultDisc() {
		return PmdPlanPremiumMultDisc;
	}
	public void setPmdPlanPremiumMultDisc(ArrayList pmdPlanPremiumMultDisc) {
		PmdPlanPremiumMultDisc = pmdPlanPremiumMultDisc;
	}


	public ArrayList getPGLGrpMinPremLK() {
		return PGLGrpMinPremLK;
	}
	public void setPGLGrpMinPremLK(ArrayList grpMinPremLK) {
		PGLGrpMinPremLK = grpMinPremLK;
	}
	public ArrayList getPNLMinPremLK() {
		return PNLMinPremLK;
	}
	public void setPNLMinPremLK(ArrayList minPremLK) {
		PNLMinPremLK = minPremLK;
	}
	public ArrayList getPNLTermPptLK() {
		return PNLTermPptLK;
	}
	public void setPNLTermPptLK(ArrayList termPptLK) {
		PNLTermPptLK = termPptLK;
	}
	public ArrayList getRMLRiderPremMultipleLK() {
		return RMLRiderPremMultipleLK;
	}
	public void setRMLRiderPremMultipleLK(ArrayList riderPremMultipleLK) {
		RMLRiderPremMultipleLK = riderPremMultipleLK;
	}
	public ArrayList getMDLModalFactorLk() {
		return MDLModalFactorLk;
	}
	public void setMDLModalFactorLk(ArrayList modalFactorLk) {
		MDLModalFactorLk = modalFactorLk;
	}
	public HashMap getOCLOccupationLk() {
		return OCLOccupationLk;
	}
	public void setOCLOccupationLk(HashMap occupationLk) {
		OCLOccupationLk = occupationLk;
	}
	public HashMap getPGLPlanGroupLk() {
		return PGLPlanGroupLk;
	}
	public void setPGLPlanGroupLk(HashMap planGroupLk) {
		PGLPlanGroupLk = planGroupLk;
	}
	public ArrayList getPMLPremiumMultipleLk() {
		return PMLPremiumMultipleLk;
	}
	public void setPMLPremiumMultipleLk(ArrayList premiumMultipleLk) {
		PMLPremiumMultipleLk = premiumMultipleLk;
	}
	public HashMap getPNLChkExcludesLk() {
		return PNLChkExcludesLk;
	}
	public void setPNLChkExcludesLk(HashMap chkExcludesLk) {
		PNLChkExcludesLk = chkExcludesLk;
	}
	public HashMap getPNLPlanLk() {
		return PNLPlanLk;
	}
	public void setPNLPlanLk(HashMap planLk) {
		PNLPlanLk = planLk;
	}
	public ArrayList getPBAProductBand() {
		return PBAProductBand;
	}
	public void setPBAProductBand(ArrayList productBand) {
		PBAProductBand = productBand;
	}
	public HashMap getRDLRiderLk() {
		return RDLRiderLk;
	}
	public void setRDLRiderLk(HashMap riderLk) {
		RDLRiderLk = riderLk;
	}
	public HashMap getFDLFundLk() {
		return FDLFundLk;
	}
	public void setFDLFundLk(HashMap fundLk) {
		FDLFundLk = fundLk;
	}
	public ArrayList getPMVPlanMaturityValue() {
		return PMVPlanMaturityValue;
	}
	public void setPMVPlanMaturityValue(ArrayList planMaturityValue) {
		PMVPlanMaturityValue = planMaturityValue;
	}
	public ArrayList getPFRPlanFormulaRef() {
		return PFRPlanFormulaRef;
	}
	public void setPFRPlanFormulaRef(ArrayList planFormulaRef) {
		PFRPlanFormulaRef = planFormulaRef;
	}
	public ArrayList getFEDFormulaExtDesc() {
		return FEDFormulaExtDesc;
	}
	public void setFEDFormulaExtDesc(ArrayList formulaExtDesc) {
		FEDFormulaExtDesc = formulaExtDesc;
	}
	public ArrayList getAddress() {
		return Address;
	}
	public void setAddress(ArrayList address) {
		Address = address;
	}
	public HashMap getPGFPlanGroupFeature() {
		return PGFPlanGroupFeature;
	}
	public void setPGFPlanGroupFeature(HashMap planGroupFeature) {
		PGFPlanGroupFeature = planGroupFeature;
	}
	public ArrayList getPACPremiumAllocationCharges() {
		return PACPremiumAllocationCharges;
	}
	public void setPACPremiumAllocationCharges(ArrayList premiumAllocationCharges) {
		PACPremiumAllocationCharges = premiumAllocationCharges;
	}
	public ArrayList getACMAdminChargesMaster() {
		return ACMAdminChargesMaster;
	}
	public void setACMAdminChargesMaster(ArrayList adminChargesMaster) {
		ACMAdminChargesMaster = adminChargesMaster;
	}
	public ArrayList getMCMMortilityChargesMaster() {
		return MCMMortilityChargesMaster;
	}
	public void setMCMMortilityChargesMaster(ArrayList mortilityChargesMaster) {
		MCMMortilityChargesMaster = mortilityChargesMaster;
	}
	public HashMap getPCRPlanChargesRef() {
		return PCRPlanChargesRef;
	}
	public void setPCRPlanChargesRef(HashMap planChargesRef) {
		PCRPlanChargesRef = planChargesRef;
	}
	public ArrayList getWCMWOPChargesMaster() {
		return WCMWOPChargesMaster;
	}
	public void setWCMWOPChargesMaster(ArrayList charges_Master) {
		WCMWOPChargesMaster = charges_Master;
	}
	public ArrayList getFCMFIBChargesMaster() {
		return FCMFIBChargesMaster;
	}
	public void setFCMFIBChargesMaster(ArrayList chargesMaster) {
		FCMFIBChargesMaster = chargesMaster;
	}
	public ArrayList getSURSurrenderChargeMaster() {
		return SURSurrenderChargeMaster;
	}
	public void setSURSurrenderChargeMaster(ArrayList surrenderChargeMaster) {
		SURSurrenderChargeMaster = surrenderChargeMaster;
	}
	public ArrayList getGuaranteedMaturityBonusMaster() {
		return GuaranteedMaturityBonusMaster;
	}
	public void setGuaranteedMaturityBonusMaster(
			ArrayList guaranteedMaturityBonusMaster) {
		GuaranteedMaturityBonusMaster = guaranteedMaturityBonusMaster;
	}
	public ArrayList getXmlMetadataMaster() {
		return XmlMetadataMaster;
	}
	public void setXmlMetadataMaster(ArrayList xmlMetadataMaster) {
		XmlMetadataMaster = xmlMetadataMaster;
	}
	public ArrayList getCommissionChargeMaster() {
		return CommissionChargeMaster;
	}
	public void setCommissionChargeMaster(ArrayList commissionChargeMaster) {
		CommissionChargeMaster = commissionChargeMaster;
	}
	public ArrayList getMFMMaturityFactorMaster() {
		return MFMMaturityFactorMaster;
	}
	public void setMFMMaturityFactorMaster(ArrayList maturityFactorMaster) {
		MFMMaturityFactorMaster = maturityFactorMaster;
	}
	public ArrayList getGAIGuarAnnIncChMaster() {
		return GAIGuarAnnIncChMaster;
	}
	public void setGAIGuarAnnIncChMaster(ArrayList guarAnnIncChMaster) {
		GAIGuarAnnIncChMaster = guarAnnIncChMaster;
	}
	public HashMap getSPVSysParamValues() {
		return SPVSysParamValues;
	}
	public void setSPVSysParamValues(HashMap sysParamValues) {
		SPVSysParamValues = sysParamValues;
	}
	public ArrayList getIPCValues() {
		return IPCValues;
	}
	public void setIPCValues(ArrayList values) {
		IPCValues = values;
	}
	public ArrayList getRDLCharges() {
		return RDLCharges;
	}
	public void setRDLCharges(ArrayList rdlCharges) {
		RDLCharges = rdlCharges;
	}
	public ArrayList getAAAFMCValues() {
		return AAAFMCValues;
	}
	public void setAAAFMCValues(ArrayList values) {
		AAAFMCValues = values;
	}
	public ArrayList getStdTemplateValues() {
		return StdTemplateValues;
	}
	public void setStdTemplateValues(ArrayList stdTemplateValues) {
		StdTemplateValues = stdTemplateValues;
	}
	public ArrayList getIplIllussPagelist() {
		return iplIllussPagelist;
	}
	public void setIplIllussPagelist(ArrayList iplIllussPagelist) {
		this.iplIllussPagelist = iplIllussPagelist;
	}
	public ArrayList getCommTextlist() {
		return commTextlist;
	}
	public void setCommTextlist(ArrayList commTextlist) {
		this.commTextlist = commTextlist;
	}
	public ArrayList getAnnuityRatelist() {
		return annuityRatelist;
	}
	public void setAnnuityRatelist(ArrayList annuityRatelist) {
		this.annuityRatelist = annuityRatelist;
	}
	public ArrayList getFOPRatelist() {
		return FOPRatelist;
	}
	public void setFOPRatelist(ArrayList ratelist) {
		FOPRatelist = ratelist;
	}
	public ArrayList getIncentiveRatelist() {
		return incentiveRatelist;
	}
	public void setIncentiveRatelist(ArrayList incentiveRatelist) {
		this.incentiveRatelist = incentiveRatelist;
	}
	public HashMap getTempImgPath() {
		return tempImgPath;
	}
	public void setTempImgPath(HashMap tempImgPath) {
		this.tempImgPath = tempImgPath;
	}
	public ArrayList getPNLComboLk() {
		return PNLComboLk;
	}
	public void setPNLComboLk(ArrayList comboLk) {
		PNLComboLk = comboLk;
	}
	public ArrayList getServiceTaxMaster() {
		return serviceTaxMaster;
	}
	public void setServiceTaxMaster(ArrayList serviceTaxMaster) {
		this.serviceTaxMaster = serviceTaxMaster;
	}
	public ArrayList getPremiumBoost() {
		return PremiumBoost;
	}
	public void setPremiumBoost(ArrayList premiumBoost) {
		PremiumBoost = premiumBoost;
	}
}
