package com.talic.plugins.sisEngine.BO;

public class LpbLargePremiumBoost {

	private String LPB_PLAN_CODE;
	private String LPB_RIDER_CODE;
	private long LPB_MIN_PREMIUM;
	private long LPB_MAX_PREMIUM;
    private double LPB_BOOST_RATE;
	public String getLPB_PLAN_CODE() {
		return LPB_PLAN_CODE;
	}
	public void setLPB_PLAN_CODE(String lpb_plan_code) {
		LPB_PLAN_CODE = lpb_plan_code;
	}
	public String getLPB_RIDER_CODE() {
		return LPB_RIDER_CODE;
	}
	public void setLPB_RIDER_CODE(String lpb_rider_code) {
		LPB_RIDER_CODE = lpb_rider_code;
	}
	public long getLPB_MIN_PREMIUM() {
		return LPB_MIN_PREMIUM;
	}
	public void setLPB_MIN_PREMIUM(long lpb_min_premium) {
		LPB_MIN_PREMIUM = lpb_min_premium;
	}
	public long getLPB_MAX_PREMIUM() {
		return LPB_MAX_PREMIUM;
	}
	public void setLPB_MAX_PREMIUM(long lpb_max_premium) {
		LPB_MAX_PREMIUM = lpb_max_premium;
	}
	public double getLPB_BOOST_RATE() {
		return LPB_BOOST_RATE;
	}
	public void setLPB_BOOST_RATE(double lpb_boost_rate) {
		LPB_BOOST_RATE = lpb_boost_rate;
	}


}
