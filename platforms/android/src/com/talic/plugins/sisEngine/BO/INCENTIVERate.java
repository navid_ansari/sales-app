package com.talic.plugins.sisEngine.BO;

public class INCENTIVERate {
	private	String	inc_plan_code;
	private	String	inc_mode;
	private long inc_min_sa;
	private long inc_max_sa;
	private	double inc_rate;
	public String getInc_plan_code() {
		return inc_plan_code;
	}
	public void setInc_plan_code(String inc_plan_code) {
		this.inc_plan_code = inc_plan_code;
	}
	public String getInc_mode() {
		return inc_mode;
	}
	public void setInc_mode(String inc_mode) {
		this.inc_mode = inc_mode;
	}
	public long getInc_min_sa() {
		return inc_min_sa;
	}
	public void setInc_min_sa(long inc_min_sa) {
		this.inc_min_sa = inc_min_sa;
	}
	public long getInc_max_sa() {
		return inc_max_sa;
	}
	public void setInc_max_sa(long inc_max_sa) {
		this.inc_max_sa = inc_max_sa;
	}
	public double getInc_rate() {
		return inc_rate;
	}
	public void setInc_rate(double inc_rate) {
		this.inc_rate = inc_rate;
	}


}
