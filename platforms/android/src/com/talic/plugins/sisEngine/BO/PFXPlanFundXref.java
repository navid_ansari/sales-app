package com.talic.plugins.sisEngine.BO;

import java.util.Date;

public class PFXPlanFundXref implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private int PFX_PNL_ID;//	NUMBER
	private int PFX_FDL_ID;//	NUMBER
	private int PFX_ID;	//NUMBER
	private String PFX_UPDATED_BY;	//VARCHAR2(50 Bytes)
	private Date PFX_UPDATED_DATE;	//DATE
	private int PFX_PPT_ALLOWED;	//NUMBER
	private String  APEX;	//CHAR(1 Bytes)
	private String GUARANTEED;	//CHAR(1 Bytes)
	private Date START_DATE;	//DATE
	private Date END_DATE;	//DATE
	private String SMART_FROM_FUND;	//CHAR(1 Bytes)
	private String SMART_TO_FUND;	//CHAR(1 Bytes)
	private String AAA_FUND_FLAG;	//CHAR(1 Bytes)
	private double VARIABLE_FUND_SWITCH_CHARGE;	//NUMBER(10,2)

	public int getPFX_PNL_ID() {
		return PFX_PNL_ID;
	}
	public void setPFX_PNL_ID(int pfx_pnl_id) {
		PFX_PNL_ID = pfx_pnl_id;
	}
	public int getPFX_FDL_ID() {
		return PFX_FDL_ID;
	}
	public void setPFX_FDL_ID(int pfx_fdl_id) {
		PFX_FDL_ID = pfx_fdl_id;
	}
	public int getPFX_ID() {
		return PFX_ID;
	}
	public void setPFX_ID(int pfx_id) {
		PFX_ID = pfx_id;
	}
	public String getPFX_UPDATED_BY() {
		return PFX_UPDATED_BY;
	}
	public void setPFX_UPDATED_BY(String pfx_updated_by) {
		PFX_UPDATED_BY = pfx_updated_by;
	}
	public Date getPFX_UPDATED_DATE() {
		return PFX_UPDATED_DATE;
	}
	public void setPFX_UPDATED_DATE(Date pfx_updated_date) {
		PFX_UPDATED_DATE = pfx_updated_date;
	}
	public int getPFX_PPT_ALLOWED() {
		return PFX_PPT_ALLOWED;
	}
	public void setPFX_PPT_ALLOWED(int pfx_ppt_allowed) {
		PFX_PPT_ALLOWED = pfx_ppt_allowed;
	}
	public String getAPEX() {
		return APEX;
	}
	public void setAPEX(String apex) {
		APEX = apex;
	}
	public String getGUARANTEED() {
		return GUARANTEED;
	}
	public void setGUARANTEED(String guaranteed) {
		GUARANTEED = guaranteed;
	}
	public Date getSTART_DATE() {
		return START_DATE;
	}
	public void setSTART_DATE(Date start_date) {
		START_DATE = start_date;
	}
	public Date getEND_DATE() {
		return END_DATE;
	}
	public void setEND_DATE(Date end_date) {
		END_DATE = end_date;
	}
	public String getSMART_FROM_FUND() {
		return SMART_FROM_FUND;
	}
	public void setSMART_FROM_FUND(String smart_from_fund) {
		SMART_FROM_FUND = smart_from_fund;
	}
	public String getSMART_TO_FUND() {
		return SMART_TO_FUND;
	}
	public void setSMART_TO_FUND(String smart_to_fund) {
		SMART_TO_FUND = smart_to_fund;
	}
	public String getAAA_FUND_FLAG() {
		return AAA_FUND_FLAG;
	}
	public void setAAA_FUND_FLAG(String aaa_fund_flag) {
		AAA_FUND_FLAG = aaa_fund_flag;
	}
	public double getVARIABLE_FUND_SWITCH_CHARGE() {
		return VARIABLE_FUND_SWITCH_CHARGE;
	}
	public void setVARIABLE_FUND_SWITCH_CHARGE(double variable_fund_switch_charge) {
		VARIABLE_FUND_SWITCH_CHARGE = variable_fund_switch_charge;
	}

}
