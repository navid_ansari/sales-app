package com.talic.plugins;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecryptFile {
    private static final String TAG = "SA";
	SecretKey secretKey = null;
    Cipher cipher = null;
	public EncryptDecryptFile() {
		try {
			/**
			 * Create a AES key
			 */
			SharedPreferences saSharedPreferences = AppConstants.SA_CLASS.getSharedPreferences("SA_SHARED_PREFERENCES", AppConstants.SA_CLASS.MODE_PRIVATE);
			SharedPreferences.Editor saSharedPreferencesEdit = saSharedPreferences.edit();

			if(saSharedPreferences.contains("secretkey")) {
				byte[] decodedKey = Base64.decode(saSharedPreferences.getString("secretkey",null),Base64.DEFAULT);
				secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
			}
			else{
				secretKey = KeyGenerator.getInstance("AES").generateKey();
				String encodedKey = Base64.encodeToString(secretKey.getEncoded(),Base64.DEFAULT);
				saSharedPreferencesEdit.putString("secretkey",encodedKey);
				saSharedPreferencesEdit.commit();
			}
			cipher = Cipher.getInstance("AES");
			/**
			 * Create an instance of cipher mentioning the name of algorithm
			 *     - AES
			 */
		} catch (NoSuchAlgorithmException ex) {
			Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
		} catch (NoSuchPaddingException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        }
	}

    /**
     *
     * @param srcPath
     * @param destPath
     *
     * Encrypts the file in srcPath and creates a file in destPath
     */
    public void encrypt(String srcPath, String destPath) {
        File rawFile = new File(srcPath);
        File encryptedFile = new File(destPath);
        InputStream inStream;
        OutputStream outStream;
        Log.d(TAG,"srcpath: " + srcPath);
        Log.d(TAG,"destpath: " + destPath);
        try {
            /**
             * Initialize the cipher for encryption
             */
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            /**
             * Initialize input and output streams
             */
            inStream = new FileInputStream(rawFile);
            outStream = new FileOutputStream(encryptedFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inStream.read(buffer)) > 0) {
                outStream.write(cipher.update(buffer, 0, len));
                outStream.flush();
            }
            outStream.write(cipher.doFinal());
            inStream.close();
            outStream.close();
            rawFile.delete();
            rawFile = new File(srcPath);
            encryptedFile.renameTo(rawFile);

        } catch (IllegalBlockSizeException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param srcPath
     * @param destPath
     *
     * Decrypts the file in srcPath and creates a file in destPath
     */
    public void decrypt(String srcPath, String destPath) {
		Log.d(TAG,"srcpath: " + srcPath);
		Log.d(TAG,"destpath: " + destPath);
        File encryptedFile = new File(srcPath);
        File decryptedFile = new File(destPath);
        InputStream inStream;
        OutputStream outStream;
        try {
			if(encryptedFile.exists()) {
				/**
				 * Initialize the cipher for decryption
				 * */
				cipher.init(Cipher.DECRYPT_MODE, secretKey);
				/**
				 * Initialize input and output streams
				 */
				inStream = new FileInputStream(encryptedFile);
				outStream = new FileOutputStream(decryptedFile);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = inStream.read(buffer)) > 0) {
					outStream.write(cipher.update(buffer, 0, len));
					outStream.flush();
				}
				outStream.write(cipher.doFinal());
				inStream.close();
				outStream.close();
			}

        } catch (IllegalBlockSizeException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
            ex.printStackTrace();
        }
    }
}
