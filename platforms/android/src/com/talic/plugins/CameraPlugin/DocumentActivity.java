package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;

import io.fabric.sdk.android.Fabric;

public class DocumentActivity extends Activity {

	String filePath="",fileName="";
	private static String realFilename = "";
	private static final int FILE_SELECT_CODE = 0;
	String pdfpath = "";
	File finalfile,newFile;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		Intent intent=getIntent();
		filePath=intent.getStringExtra("path");
		fileName=intent.getStringExtra("name");
		showFileChooser();
	}
	private void showFileChooser() {

		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("application/pdf");
		String[] mimetypes = {"application/pdf"};
		intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);


		try {
			startActivityForResult(
					Intent.createChooser(intent, "Select a File to Upload"),
					FILE_SELECT_CODE);
		} catch (ActivityNotFoundException ex) {
			// Potentially direct the user to the Market with a Dialog
			Toast.makeText(this, "Please install a File Manager.",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {

			case FILE_SELECT_CODE:
				if (resultCode == RESULT_OK) {
					Uri uri = data.getData();
					Log.d("File", "File Uri: " + uri.toString());

					try {
						realFilename = getFileName(this, uri);
						IsValidFile(uri, data, getPath(DocumentActivity.this, uri, data));
						finalfile=new File(pdfpath);
						Log.d("File Path: ",pdfpath);
						if(finalfile.exists())
						{
							//Toast.makeText(getApplicationContext(),"File Exists",Toast.LENGTH_LONG).show();
							int size=Integer.parseInt(String.valueOf(finalfile.length()/1024));
							if(size<=250)
							{
								//Toast.makeText(getApplicationContext(),"File Appropriate",Toast.LENGTH_LONG).show();
								newFile=copyFile(finalfile);
								try {
									JSONObject result = new JSONObject();
									result.put("code", "1");
									result.put("message", "success");
									result.put("count", "1");
									Intent i = new Intent();
									i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									i.putExtra("values", result.toString());
									setResult(0, i);
									finish();
								}
								catch (JSONException Ex){

								}
							}
							else
							{
								Toast.makeText(getApplicationContext(),"Please, select a file size less than 250 kb",Toast.LENGTH_LONG).show();
								showFileChooser();
							}

						}
						else
						{
							Toast.makeText(getApplicationContext(),"Invalid File Path",Toast.LENGTH_LONG).show();
						}
//						newFile=copyFile(finalfile);
//				File tempdir = new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+ AppConstants.ROOT_FOLDER + imgPath+ AppConstants.TMP_DIR_NAME + imgName);
//				AppConstants.EN_DE_OBJ.encrypt(newFile.getAbsolutePath(),tempdir.getAbsolutePath());
//						myFile.delete();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Log.d("File path", "File Path: " + realFilename);

				}
				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	public void IsValidFile(Uri uri,Intent data,String path) {
		Log.d("uploadpath", realFilename);
		if ( realFilename.contains(".pdf") ) {


			if(path==null) {


				String realpath = uriToFilename(uri, "external", data);
				path = realpath;
			}
			if(path==null){

				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case DialogInterface.BUTTON_POSITIVE:
								// Yes button clicked
								dialog.dismiss();

								break;


						}
					}
				};
				//
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DocumentActivity.this);
				builder.setMessage("Please Put Your File in Internal Root Directory")
						.setPositiveButton("Ok", dialogClickListener)
						.show();



			}else {
				File file = new File(path);
				if (file.exists()) {
					pdfpath = file.getAbsolutePath();
					Log.d("pdfpath", pdfpath);
				} else {
					String realpath1 = uriToFilename(uri, "internal", data);
					Log.d("Realpath1", realpath1);

					File file1 = new File(realpath1);
					Log.d("pdfpathinternal", file1.getAbsolutePath());

					if (file1.exists()) {
//
						finalfile = file1;
//

					} else {
						DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								switch (which) {
									case DialogInterface.BUTTON_POSITIVE:
										// Yes button clicked
										dialog.dismiss();

										break;


								}
							}
						};
						//
						AlertDialog.Builder builder = new AlertDialog.Builder(
								DocumentActivity.this);
						builder.setMessage("Please Put Your File in Internal Root Directory")
								.setPositiveButton("Ok", dialogClickListener)
								.show();

					}
				}

			}

		} else {
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
						case DialogInterface.BUTTON_POSITIVE:
							// Yes button clicked
							dialog.dismiss();

							break;


					}
				}
			};
			//
			AlertDialog.Builder builder = new AlertDialog.Builder(
					DocumentActivity.this);
			builder.setMessage("You have Selected wrong file")
					.setPositiveButton("Ok", dialogClickListener)
					.show();
		}

	}
	public String uriToFilename(Uri uri ,String type,Intent data){
		String path = null;


		Log.d("case20", "grater 19");
		if (uri.toString().contains("file:")) {

			path=uri.toString();
			path = path.substring(path.indexOf(":") + 3, path.length());

			Log.d("case20path",path );
		} else if (uri.toString().contains(" content:")) {
			path=uri.toString();
			path = path.substring(path.indexOf(":") + 2, path.length());


			Log.d("case20path",path );
		}else {

			path = uriToFile(uri, type);
			path = path.substring(path.indexOf(":") + 1, path.length());
			if (type.equals("external")) {
				String ext = System.getenv("SECONDARY_STORAGE");
				path = ext + File.separator + path;
			} else {
				String ext = "";
				if (!path.contains("emulated")) {
					ext = Environment.getExternalStorageDirectory().getAbsolutePath();
					path = ext + File.separator + path;
				}


			}

		}

		return path;
	}
	public String uriToFile(Uri uri,String type) {
		String filePath = null;
		ContentResolver cr = DocumentActivity.this.getContentResolver();
		Uri uri1 = MediaStore.Files.getContentUri(type);
		String[] projection = null;;
		String sortOrder = null; // unordered
		String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=?";
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf");
		String[] selectionArgsPdf = new String[]{ mimeType };
		Cursor allPdfFiles = cr.query(uri, projection, selectionMimeType, selectionArgsPdf, sortOrder);
		if (allPdfFiles.moveToFirst()) {
			filePath = allPdfFiles.getString(0);
			Log.d("filepath",allPdfFiles.getString(0));
			Log.d("filePath1",allPdfFiles.getString(1));
			Log.d("filePath2",allPdfFiles.getString(2));
			Log.d("filePath3",allPdfFiles.getString(3));
			Log.d("filePath4",allPdfFiles.getString(4));

		}
		allPdfFiles.close();
		return filePath;
	}

	public static String getPath(final Context context, final Uri uri,Intent data) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/" + split[1];
				}

				// TODO handle non-primary volumes
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] {
						split[1]
				};

				return getDataColumn(context, contentUri, selection, selectionArgs);
			}
		}
		// MediaStore (and general)
		else if ("content".equalsIgnoreCase(uri.getScheme())) {

			// Return the remote address
			if (isGooglePhotosUri(uri))
				return uri.getLastPathSegment();

			return getDataColumn(context, uri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is Google Photos.
	 */
	public static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri.getAuthority());
	}

	public static String getDataColumn(Context context, Uri uri, String selection,
									   String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {
				column
		};

		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
					null);
			if (cursor != null && cursor.moveToFirst()) {
				final int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}
	public static String getFileName(Context context, Uri uri)
			throws URISyntaxException {

		Log.d("scheme", uri.getScheme());
		Log.d(" Whole path", uri.toString());
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = {MediaStore.MediaColumns.DATA};
			Cursor cursor = null;

			try {

				cursor = context.getContentResolver().query(uri, null, null,
						null, null);
				Log.d("type", context.getContentResolver().getType(uri));

				cursor.moveToFirst();
				int nameIndex = cursor
						.getColumnIndex(OpenableColumns.DISPLAY_NAME);

				return cursor.getString(nameIndex);


			} catch (Exception e) {

			}
		} else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getLastPathSegment();
		}

		return null;

	}
	public File copyFile(File src) throws IOException {
		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + filePath);
//		File path=new File(	 Environment
//				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				"TALIC");
		path.mkdirs();
		File mypath=new File(path,fileName+"_1"+".pdf");
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(mypath);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
		Log.d("encrypted file path ",""+mypath.getAbsolutePath());
		return mypath;

	}
	public void forceCrash(View view) {
		throw new RuntimeException("This is a crash");
	}


}
