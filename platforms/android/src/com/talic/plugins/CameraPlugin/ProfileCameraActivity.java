package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class ProfileCameraActivity extends Activity {

	// Activity request codes
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	String imgPath="",imgName="";
	private Uri fileUri,cropedFileUri;; // file url to store image
	File myFile,newFile,cropedFile;
	JSONObject result=new JSONObject();
	final int PIC_CROP = 2;
	File mediaStorageDir;
	Bitmap btmp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		Intent intent=getIntent();
		imgPath=intent.getStringExtra("path");
		imgName=intent.getStringExtra("name");
		Log.d("Path",imgPath);
		Log.d("imgName",imgName);
		captureImage();
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(),
					"Sorry! Your device doesn't support camera",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}

	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri();

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	/*
	 * Here we store the file url as it will be null after returning from camera
	 * app
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}


	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				try {
					btmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);//extras.getParcelable("data");
				}
				catch (Exception e){
					e.printStackTrace();
				}
				performCrop();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
		//user is returning from cropping the image
		else if(requestCode == PIC_CROP) {
			if (resultCode == RESULT_CANCELED) {
				try {
					ImageCompression ic = new ImageCompression();
					ic.compressImage(myFile);
					newFile=copyFile(myFile);
					previewCapturedImage();
				}
				catch (Exception e){
					Log.d("Exception",e.getMessage());
				}
			}
			else{
				try {
					ImageCompression ic = new ImageCompression();
					ic.compressImage(cropedFile);
					newFile=copyFile(cropedFile);
					previewCapturedImage();
				}
				catch (Exception e){
					Log.d("Exception",e.getMessage());
				}
			}
		}

	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for larger
				// images
				options.inSampleSize = 8;

				final Bitmap bitmap = BitmapFactory.decodeFile(newFile.getAbsolutePath(),
						options);
				Bitmap resizedBitmap = Bitmap.createScaledBitmap(
						bitmap, 110,110, false);
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

				//you can create a new file name "test.jpg" in sdcard folder.
				File f = new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath+imgName+"_thumb.jpg");
				Log.d("Thumb Path :",f.getAbsolutePath());
				//write the bytes in file
				FileOutputStream fo;
				try {
					f.createNewFile();
					fo = new FileOutputStream(f);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				myFile.delete();
				cropedFile.delete();
				try {
					result.put("code", "1");
					result.put("message", "success");
					Intent i = new Intent();
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.putExtra("values", result.toString());
					setResult(0, i);
					finish();
				}
				catch (JSONException e)
				{
					Log.d("Exception",e.getMessage());

				}
			}
			catch (Exception e)
			{
				Log.d("Exception",e.getMessage());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	/**
	 * ------------ Helper Methods ----------------------
	 * */

	/*
	 * Creating file uri to store image
	 */
	public Uri getOutputMediaFileUri() {
		myFile=getOutputMediaFile();
		return Uri.fromFile(myFile);
	}

	/*
	 * returning image / video
	 */
	private  File getOutputMediaFile() {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"TALIC");
		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("TALIC", "Oops! Failed create "
						+ "TALIC" + " directory");
				return null;
			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		return mediaFile;
	}
	 public File copyFile(File src) throws IOException {
       File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
       path.mkdirs();
       File mypath=new File(path,imgName+".jpg");
	   mypath.delete();
	   mypath=new File(path,imgName+".jpg");
       FileInputStream inStream = new FileInputStream(src);
       FileOutputStream outStream = new FileOutputStream(mypath);
       FileChannel inChannel = inStream.getChannel();
       FileChannel outChannel = outStream.getChannel();
       inChannel.transferTo(0, inChannel.size(), outChannel);
       inStream.close();
       outStream.close();
		 Log.d("encrypted file path ",""+mypath.getAbsolutePath());
       return mypath;

   }
	private void performCrop(){
		//take care of exceptions

		try {
			long heightRatio= btmp.getHeight()/ btmp.getWidth();
			long widthtRatio=  btmp.getWidth()/btmp.getHeight();
			//call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.setDataAndType(fileUri, "image/*");
			//set crop properties
			cropIntent.putExtra("crop", "true");
			//indicate aspect of desired crop
			cropIntent.putExtra("aspectX", widthtRatio);
			cropIntent.putExtra("aspectY", heightRatio);
			//indicate output X and Y
			cropIntent.putExtra("outputX", 320);
			cropIntent.putExtra("outputY", 250);
			//retrieve data on return
			cropIntent.putExtra("return-data", true);
			try {
				cropedFile=getOutputMediaFile();
				cropedFileUri= Uri.fromFile(cropedFile);
			} catch (Exception ex) {

			}
			cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropedFileUri);
			//start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		//respond to users whose devices do not support the crop action
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}
	public void forceCrash(View view) {
		throw new RuntimeException("This is a crash");
	}

}
