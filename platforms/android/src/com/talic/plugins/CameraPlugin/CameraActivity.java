package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class CameraActivity extends Activity {

	// Activity request codes
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int DISPLAY_IMAGE_REQUEST_CODE = 1000;

	String imgPath="",imgName="",type="";
	private Uri fileUri,cropedFileUri; // file url to store image
	File myFile,newFile,cropedFile;
	JSONObject result=new JSONObject();
	int count=1;
	final int PIC_CROP = 2;
	File mediaStorageDir;
	Bitmap btmp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		Intent intent=getIntent();
		imgPath=intent.getStringExtra("path");
		imgName=intent.getStringExtra("name");
		type=intent.getStringExtra("type");
//		File file=new File(getFilesDir()+ AppConstants.ROOT_FOLDER+imgPath);
//		if (file.isDirectory())
//		{
//			String[] children = file.list();
//			for (int i = 0; i < children.length; i++)
//			{
//				new File(file, children[i]).delete();
//			}
//
//		}
//		clearData();
		captureImage();
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(),
					"Sorry! Your device doesn't support camera",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}
	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri();

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	/*
	 * Here we store the file url as it will be null after returning from camera
	 * app
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}


	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
//				Bundle extras = data.getExtras();
				try {
					btmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);//extras.getParcelable("data");
				} catch (Exception e) {
					e.printStackTrace();
				}
				performCrop();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
		//user is returning from cropping the image
		else if (requestCode == PIC_CROP) {
			if (resultCode == RESULT_CANCELED) {
				try {
					ImageCompression ic = new ImageCompression();
					ic.compressImage(myFile);
					newFile = copyFile(myFile);
					myFile.delete();
					cropedFile.delete();
				} catch (Exception e) {
					Log.d("Exception", e.getMessage());
				}
				previewCapturedImage();
			}
		 else {
				try {
					ImageCompression ic = new ImageCompression();
					ic.compressImage(cropedFile);
					newFile = copyFile(cropedFile);
					myFile.delete();
					cropedFile.delete();
				} catch (Exception e) {
					Log.d("Exception", e.getMessage());
				}
				previewCapturedImage();
			}
		}
		else if(requestCode==DISPLAY_IMAGE_REQUEST_CODE){
			try {
				result.put("code", "1");
				result.put("message", "success");
				result.put("count", count);
				Intent i = new Intent();
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.putExtra("values", result.toString());
				setResult(0, i);
				finish();
			} catch (JSONException e) {
				Log.d("Exception", e.getMessage());

			}
		}

	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			try {
				if(count==1){
					clearData();
				}
				Log.d("Native Encrypted file",""+newFile.getAbsolutePath());
//				AppConstants.EN_DE_OBJ.encrypt(newFile.getAbsolutePath(),myFile.getAbsolutePath());
				if(count<5) {
					AlertDialog.Builder builder1 = new AlertDialog.Builder(CameraActivity.this);
					builder1.setMessage("Do you want to click another image?");
					builder1.setCancelable(false);
					builder1.setPositiveButton(
							"Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
									count++;
									captureImage();
								}
							});

					builder1.setNegativeButton(
							"No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
//									try {
									if (mediaStorageDir.isDirectory()) {
										String[] children = mediaStorageDir.list();
										for (int i = 0; i < children.length; i++) {
											new File(mediaStorageDir, children[i]).delete();
										}
										mediaStorageDir.delete();
									}
									Intent intent=new Intent(CameraActivity.this,DisplayImageActivity.class);
									intent.putExtra("name", imgName);
									intent.putExtra("path", imgPath);
									intent.putExtra("from","camera");
									intent.putExtra("type",type);
									startActivityForResult(intent, DISPLAY_IMAGE_REQUEST_CODE);
//										result.put("code", "1");
//										result.put("message", "success");
//										result.put("count", count);
//										Intent i = new Intent();
//										i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//										i.putExtra("values", result.toString());
//										setResult(0, i);
//										finish();
//									} catch (JSONException e) {
//										Log.d("Exception", e.getMessage());
//
//									}
								}
							});

					AlertDialog alert11 = builder1.create();
					alert11.show();
				}
				else{
//					try {
					if (mediaStorageDir.isDirectory()) {
						String[] children = mediaStorageDir.list();
						for (int i = 0; i < children.length; i++) {
							new File(mediaStorageDir, children[i]).delete();
						}
						mediaStorageDir.delete();
					}
					Intent intent=new Intent(CameraActivity.this,DisplayImageActivity.class);
					intent.putExtra("name", imgName);
					intent.putExtra("path", imgPath);
					intent.putExtra("from","camera");
					intent.putExtra("type",type);
					startActivityForResult(intent,DISPLAY_IMAGE_REQUEST_CODE);
//						result.put("code", "1");
//						result.put("message", "success");
//						result.put("count", count);
//						Intent i = new Intent();
//						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						i.putExtra("values", result.toString());
//						setResult(0, i);
//						finish();
//					} catch (JSONException e) {
//						Log.d("Exception", e.getMessage());
//
//					}
				}
			}
			catch (Exception e) {
				Log.d("Exception", e.getMessage());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	/**
	 * ------------ Helper Methods ----------------------
	 * */

	/*
	 * Creating file uri to store image
	 */
	public Uri getOutputMediaFileUri() {
		myFile=getOutputMediaFile();
		return Uri.fromFile(myFile);
	}

	/*
	 * returning image / video
	 */
	private  File getOutputMediaFile() {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"TALIC");
		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("TALIC", "Oops! Failed create "
						+ "TALIC" + " directory");
				return null;
			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		//myFile=mediaFile;
		return mediaFile;
	}
	public File copyFile(File src) throws IOException {
		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
		path.mkdirs();
		File mypath=new File(path,imgName+"_"+count+".jpg");
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(mypath);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
		return mypath;

	}
	private void performCrop(){
		//take care of exceptions

		try {
			long heightRatio= btmp.getHeight()/ btmp.getWidth();
			long widthtRatio=  btmp.getWidth()/btmp.getHeight();
			//call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.setDataAndType(fileUri, "image/*");
			//set crop properties
			cropIntent.putExtra("crop", "true");
			//indicate aspect of desired crop
			cropIntent.putExtra("aspectX", widthtRatio);
			cropIntent.putExtra("aspectY", heightRatio);
			//indicate output X and Y
			cropIntent.putExtra("outputX", widthtRatio);
			cropIntent.putExtra("outputY", heightRatio);
			//retrieve data on return
			cropIntent.putExtra("return-data", true);

			try {
				cropedFile=getOutputMediaFile();
				cropedFileUri= Uri.fromFile(cropedFile);
			} catch (Exception ex) {

			}
			cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropedFileUri);
			//start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		//respond to users whose devices do not support the crop action
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}
	private void clearData() {
		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
		path.mkdirs();
		for (int i=2;i<6;i++) {
			File myfile=new File(path,imgName+"_"+i+".jpg");
			Log.d("File Path", myfile.getAbsolutePath());
			if(myfile.exists())
			{
				myfile.delete();

			}
		}

	}
	private File createNewFile(String prefix){
		if(prefix==null || "".equalsIgnoreCase(prefix)){
			prefix="IMG_";
		}
		File newDirectory = new File(Environment.getExternalStorageDirectory()+"/mypics/");
		if(!newDirectory.exists()){
			if(newDirectory.mkdir()){
				Log.d("Dir Created", newDirectory.getAbsolutePath()+" directory created");
			}
		}
		File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
		if(file.exists()){
			//this wont be executed
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return file;
	}
	public void forceCrash(View view) {
		throw new RuntimeException("This is a crash");
	}

}
