package com.talic.plugins.CameraPlugin;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by akhil on 12/02/16.
 */

public class ImageCompression {

	public void compressImage(File file){
		try {
			(new CompressImageTask()).execute(new File[]{file}).get();
		} catch (InterruptedException var3) {
			var3.printStackTrace();
		} catch (ExecutionException var4) {
			var4.printStackTrace();
		}

	}

	private Bitmap avoidOutOfMemoryError(File f) {
		Bitmap b = null;

		try {
			BitmapFactory.Options e = new BitmapFactory.Options();
			e.inJustDecodeBounds = true;
			FileInputStream fis = new FileInputStream(f);
			BitmapFactory.decodeStream(fis, (Rect)null, e);
			fis.close();
			int scale = 1;
			if(e.outHeight > 1000 || e.outWidth > 1000) {
				scale = (int)Math.pow(2.0D, (double)((int)Math.ceil(Math.log((double)1000 / (double)Math.max(e.outHeight, e.outWidth)) / Math.log(0.2D))));
			}

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inDither = false;
			o2.inScaled = true;
			o2.outWidth = 1024;
			o2.outHeight = 768;
			o2.inPreferQualityOverSpeed = true;
			o2.inPreferredConfig = Bitmap.Config.ARGB_8888;
			fis = new FileInputStream(f);
			b = BitmapFactory.decodeStream(fis, (Rect)null, o2);
			fis.close();
		} catch (FileNotFoundException var7) {
			var7.printStackTrace();
		} catch (Exception var8) {
			var8.printStackTrace();
		}

		return b;
	}

}


class CompressImageTask extends AsyncTask<File, Integer, Integer> {
	public CompressImageTask() {
	}

	protected void onPreExecute() {
		super.onPreExecute();
	}

	protected Integer doInBackground(File... params) {
		byte[] bmpPicByteArray = null;
		ByteArrayOutputStream bao = null;
		ByteArrayOutputStream bao1 = null;

		try {
			Bitmap bit = BitmapFactory.decodeFile(params[0].getAbsolutePath());
			bao = new ByteArrayOutputStream();
			bao.reset();
			bao1 = new ByteArrayOutputStream();
			bao1.reset();


			if(bit != null) {
				bit.setDensity(300);
				bit.compress(Bitmap.CompressFormat.JPEG, 50, bao);
				bmpPicByteArray = bao.toByteArray();
				bit.recycle();
				bit = null;
			}

			try {

				File file = new File(params[0].getPath());
				if(file.exists()) {
					file.delete();
					file.createNewFile();
				}

				FileOutputStream e1 = new FileOutputStream(file);
				e1.write(bmpPicByteArray);
				e1.flush();
				e1.close();

			} catch (FileNotFoundException var12) {
				var12.printStackTrace();
			} catch (IOException var13) {
				var13.printStackTrace();
			}
		} catch (Exception var14) {
			var14.printStackTrace();
		}

		return 1;
	}

	protected void onPostExecute(Integer count) {
		super.onPostExecute(count);
	}
}
