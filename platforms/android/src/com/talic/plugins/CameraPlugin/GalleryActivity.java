package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.fabric.sdk.android.Fabric;

public class GalleryActivity extends Activity {

	// Activity request codes
	public static final int REQ_CODE_PICK_IMAGE=200;
	private static final int DISPLAY_IMAGE_REQUEST_CODE = 1000;
	String imgPath="",imgName="",type="";
	private Uri fileUri; // file url to store image
	File myFile,newFile;
	JSONObject result=new JSONObject();
	int count=1;
	int fileCount=0;
	long fileSize=0;
	Bitmap btmp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		Intent intent=getIntent();
		imgPath=intent.getStringExtra("path");
		imgName=intent.getStringExtra("name");
		type=intent.getStringExtra("type");
		if(type.equals("profile")){
			fileCount=1;
			fileSize=2;
		}else{
			fileCount=5;
			fileSize=4;
		}

		captureImage();
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(),
					"Sorry! Your device doesn't support camera",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}
	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent photoPickerIntent = new Intent(
				Intent.ACTION_PICK,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		photoPickerIntent.setType("image/*");
		startActivityForResult(
				Intent.createChooser(photoPickerIntent, "Select File"),
				REQ_CODE_PICK_IMAGE);
	}

	/*
	 * Here we store the file url as it will be null after returning from camera
	 * app
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}


	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == REQ_CODE_PICK_IMAGE) {
			if (resultCode == RESULT_OK && null != data) {
				// successfully captured the image
				// display it in image view
				try {
					fileUri= data.getData();
					String[] projection = {MediaStore.MediaColumns.DATA};
					Cursor cursor = getContentResolver().query(fileUri, projection, null, null,
							null);
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
					cursor.moveToFirst();
					String selectedImagePath = cursor.getString(column_index);
					myFile=new File(selectedImagePath);
					long currentFileSize=(myFile.length()/(1024*1024));
					if(currentFileSize<=fileSize) {
						btmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
						previewCapturedImage();
					}
					else{

						Toast toast = Toast.makeText(GalleryActivity.this,"File Size should be less than "+fileSize+" MB", Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						captureImage();
					}
				}
				catch (Exception e){
					e.printStackTrace();
					alertForLargeImage();
				}
//				catch (OutOfMemoryError e){
//					alertForLargeImage();
//					//captureImage();
//				}
				catch (OutOfMemoryError e){
					e.printStackTrace();
					Toast toast = Toast.makeText(GalleryActivity.this,"File Size should be less than "+fileSize+" MB", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					captureImage();
				}

			} else if (resultCode == RESULT_CANCELED) {
				finish();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
				captureImage();
			}
		}

		else if(requestCode==DISPLAY_IMAGE_REQUEST_CODE){
			try {
				result.put("code", "1");
				result.put("message", "success");
				result.put("count", count);
				Intent i = new Intent();
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.putExtra("values", result.toString());
				setResult(0, i);
				finish();
			} catch (JSONException e) {
				Log.d("Exception", e.getMessage());

			}
		}
	}
	/*
		Alert Dailog for File Size greater than 2MB
	 */
	public void alertForLargeImage(){
		Toast toast = Toast.makeText(GalleryActivity.this,"File size should be less than 2MB!", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
//		Context appContext = this.getApplicationContext();
//		AlertDialog.Builder builder1 = new AlertDialog.Builder(appContext);
//		builder1.setMessage("File size should be less than 2MB!");
//		builder1.setCancelable(true);
//
//		builder1.setPositiveButton(
//				"Yes",
//				new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						dialog.cancel();
//						captureImage();
//					}
//				});
//		AlertDialog alert11 = builder1.create();
//		alert11.show();
//		Toast.makeText(getApplicationContext(),
//				"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
//				.show();
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//				GalleryActivity.this);
//
//		// set title
//		alertDialogBuilder.setTitle("Image Size error");
//
//		// set dialog message
//		alertDialogBuilder
//				.setMessage("File size should be less than 2MB!")
//				.setCancelable(false)
//				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int id) {
//						// if this button is clicked, close
//						// current activity
						captureImage();
//					}
//				})
//				.setNegativeButton("No",new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int id) {
//						// if this button is clicked, just close
//						// the dialog box and do nothing
//						dialog.cancel();
//					}
//				});
//
//		// create alert dialog
//		AlertDialog alertDialog = alertDialogBuilder.create();
//
//		// show it
//		alertDialog.show();
	}
	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			try {
				File tempFile=copyFileTemp(myFile);
				ImageCompression ic = new ImageCompression();
				ic.compressImage(tempFile);
				newFile=copyFile(tempFile);
				tempFile.delete();
				if(count==1){
					clearData();
				}
				Log.d("Native Encrypted file",""+newFile.getAbsolutePath());
				if(count<fileCount) {
					AlertDialog.Builder builder1 = new AlertDialog.Builder(GalleryActivity.this);
					builder1.setMessage("Do you want to click another image?");
					builder1.setCancelable(false);
					builder1.setPositiveButton(
							"Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
									count++;
									captureImage();
								}
							});

					builder1.setNegativeButton(
							"No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
									Intent intent=new Intent(GalleryActivity.this,DisplayImageActivity.class);
									intent.putExtra("name", imgName);
									intent.putExtra("path", imgPath);
									intent.putExtra("from","gallery");
									intent.putExtra("type",type);
									startActivityForResult(intent,DISPLAY_IMAGE_REQUEST_CODE);
								}
							});

					AlertDialog alert11 = builder1.create();
					alert11.show();
				}
				else{
					Intent intent=new Intent(GalleryActivity.this,DisplayImageActivity.class);
					intent.putExtra("name", imgName);
					intent.putExtra("path", imgPath);
					intent.putExtra("from","gallery");
					intent.putExtra("type",type);
					startActivityForResult(intent,DISPLAY_IMAGE_REQUEST_CODE);
				}
			}
			catch (Exception e) {
				Log.d("Exception", e.getMessage());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	public File copyFile(File src) throws IOException {
		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
		path.mkdirs();
		String fileName="";
		if(type.equals("profile")){
			fileName=   imgName+".jpg";
		}
		else{

			fileName=   imgName+"_"+count+".jpg";
		}
		File mypath=new File(path,fileName);
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(mypath);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
		return mypath;

	}
	public File copyFileTemp(File src) throws IOException {
		File path=new File(Environment.getExternalStorageDirectory()+"/mypics/" + imgPath);
		path.mkdirs();
		String fileName="";
		if(type.equals("profile")){
			fileName=   imgName+".jpg";
		}
		else{

			fileName=   imgName+"_"+count+".jpg";
		}
		File mypath=new File(path,fileName);
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(mypath);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
		return mypath;

	}

	private void clearData() {
		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
		path.mkdirs();
		for (int i=1;i<fileCount;i++) {
			int j=i+1;
			File myfile=new File(path,imgName+"_"+j+".jpg");
			Log.d("File Delete Path", myfile.getAbsolutePath());
			if(myfile.exists())
			{
				myfile.delete();

			}
		}

	}
	public void forceCrash(View view) {
		throw new RuntimeException("This is a crash");
	}

}
