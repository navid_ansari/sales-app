package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;
import com.talic.salesapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class DisplayImageActivity extends Activity {
    private GridView gridView;
    private GridViewAdapter gridAdapter;
    String imgPath="",imgName="",from="",type="";
    Button btnBack;
    int count=0;
    boolean editTable=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Intent intent=getIntent();
        imgPath=intent.getStringExtra("path");
        imgName=intent.getStringExtra("name");
        from=intent.getStringExtra("from");
        type=intent.getStringExtra("type");
        gridView = (GridView) findViewById(R.id.gridView);
        btnBack=(Button)findViewById(R.id.btnBack);
        if(from.equals("view")) {
            btnBack.setText("Back");
            editTable=false;
        }
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from.equals("view"))
                {
                    try {
                        JSONObject result=new JSONObject();
                        result.put("code", "1");
                        result.put("message", "success");
                        result.put("count", count);
                        Intent i = new Intent();
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("values", result.toString());
                        setResult(0, i);
                        finish();
                    } catch (JSONException e) {
                        Log.d("Exception", e.getMessage());

                    }
                }
                else{
                    Intent intent=new Intent();
                    setResult(1000,intent);
                    finish();
                }
            }
        });

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if(editTable==true) {
                    ImageItem item = (ImageItem) parent.getItemAtPosition(position);
                    Log.d("Selected Image Path", "" + item.getImgPath());
                    File file = new File(item.getImgPath());
                    String name = file.getName();
                    int pos = name.lastIndexOf(".");
                    if (pos > 0) {
                        name = name.substring(0, pos);
                    }
                    Log.d("Image Name", file.getName());
                    Intent intent;
                    if (from.equals("gallery")) {
                        intent = new Intent(DisplayImageActivity.this, GalleryEditingActivity.class);
                    } else {
                        intent = new Intent(DisplayImageActivity.this, CameraEditingActivity.class);
                    }
                    intent.putExtra("name", file.getName());
                    intent.putExtra("path", imgPath);
                    intent.putExtra("title", item.getImgPath());
                     startActivity(intent);
                }
            }
        });
    }

    /**
     * Prepare some dummy data for gridview
     */
    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();
        File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
        path.mkdirs();
        if(type.equals("profile")) {
            File myfile = new File(path, imgName + ".jpg");
            Log.d("File Path", myfile.getAbsolutePath());
            if (myfile.exists()) {
                imageItems.add(new ImageItem(myfile.getAbsolutePath()));
                count=1;

            }
        }
        else{
            for (int i = 1; i < 6; i++) {
                File myfile = new File(path, imgName + "_" + i + ".jpg");
                Log.d("File Path", myfile.getAbsolutePath());
                if (myfile.exists()) {
                    imageItems.add(new ImageItem(myfile.getAbsolutePath()));
                    count=count+1;

                }
            }
        }
        return imageItems;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);
    }

    @Override
    public void onBackPressed() {
        if(from.equals("view"))
        {
            try {
                JSONObject result=new JSONObject();
                result.put("code", "1");
                result.put("message", "success");
                result.put("count", count);
                Intent i = new Intent();
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("values", result.toString());
                setResult(0, i);
                finish();
            } catch (JSONException e) {
                Log.d("Exception", e.getMessage());

            }
        }
        else{
            Intent intent=new Intent();
            setResult(1000,intent);
            finish();
        }
    }
    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

}
