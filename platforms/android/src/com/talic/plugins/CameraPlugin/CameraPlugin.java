package com.talic.plugins.CameraPlugin;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CameraPlugin extends CordovaPlugin {

    String imgname;
    String values;
    static CallbackContext currenContext;
    public CameraPlugin()
    {

    }

    public boolean execute(String action,JSONArray args, CallbackContext callbackContext) throws JSONException {
        //String i=m.getdata();
        //String file=m.captureImage();

        this.currenContext = callbackContext;
        String fullPath = args.getString(0);
        String imageName = args.getString(1);
        String flag = args.getString(2);

        Intent intent = null;

        Context context=this.cordova.getActivity().getApplicationContext();
        if(flag.equals("Edit")){
            intent=new Intent(context,CameraEditingActivity.class);
        }else if(flag.equals("Camera")){
            intent=new Intent(context,CameraActivity.class);
        }else if(flag.equals("Gallery")){
            intent = new Intent(context,GalleryActivity.class);
        }

        intent.putExtra("name", imageName);
        intent.putExtra("path", fullPath);

        Log.d("full path",fullPath);
        Log.d("img name",imageName);

        this.cordova.startActivityForResult(this, intent, 0);
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 0) {

                values=data.getStringExtra("values");
                JSONObject path=new JSONObject(values);
                this.currenContext.success(path);
                System.out.println(path.toString());
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
