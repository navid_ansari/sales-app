package com.talic.plugins.CameraPlugin;

public class ImageItem {
    private String imagePath;

    public ImageItem(String imagepath) {
        super();
        this.imagePath = imagepath;
    }
    public String getImgPath() {
        return imagePath;
    }

}
