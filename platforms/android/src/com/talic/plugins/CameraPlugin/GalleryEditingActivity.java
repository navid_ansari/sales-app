package com.talic.plugins.CameraPlugin;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.fabric.sdk.android.Fabric;

public class GalleryEditingActivity extends Activity {

	// Activity request codes
//	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int REQ_CODE_PICK_IMAGE=200;
	private static final int DISPLAY_IMAGE_REQUEST_CODE = 1000;
	String imgPath="",imgName="";
	private Uri fileUri; // file url to store image
	File myFile,newFile;
	JSONObject result=new JSONObject();
	int count=1;
	final int PIC_CROP = 2;
	Bitmap btmp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		Intent intent=getIntent();
		imgPath=intent.getStringExtra("path");
		imgName=intent.getStringExtra("name");
//		clearData();
		File file=new File(getFilesDir()+ AppConstants.ROOT_FOLDER+imgPath);
		if (file.isDirectory())
		{
			String[] children = file.list();
			for (int i = 0; i < children.length; i++)
			{
				new File(file, children[i]).delete();
			}

		}
		captureImage();
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(),
					"Sorry! Your device doesn't support camera",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}
	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent photoPickerIntent = new Intent(
				Intent.ACTION_PICK,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		photoPickerIntent.setType("image/*");
		startActivityForResult(
				Intent.createChooser(photoPickerIntent, "Select File"),
				REQ_CODE_PICK_IMAGE);
	}

	/*
	 * Here we store the file url as it will be null after returning from camera
	 * app
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}


	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == REQ_CODE_PICK_IMAGE) {
			if (resultCode == RESULT_OK && null != data) {
				// successfully captured the image
				// display it in image view
//				Bundle extras = data.getExtras();
				try {
					fileUri= data.getData();
					String[] projection = {MediaStore.MediaColumns.DATA};
					Cursor cursor = getContentResolver().query(fileUri, projection, null, null,
							null);
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
					cursor.moveToFirst();
					String selectedImagePath = cursor.getString(column_index);
					myFile=new File(selectedImagePath);
					btmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());;//MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);//extras.getParcelable("data");
					previewCapturedImage();
				}

				catch (Exception e){
					e.printStackTrace();
					captureImage();
				}

			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
//				Toast.makeText(getApplicationContext(),
//						"Media Not found", Toast.LENGTH_SHORT)
//						.show();
//				captureImage();
				finish();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
				captureImage();
			}
		}
		//user is returning from cropping the image
		else if(requestCode == PIC_CROP) {
			previewCapturedImage();
		}
		else if(requestCode==DISPLAY_IMAGE_REQUEST_CODE){
			try {
					result.put("code", "1");
					result.put("message", "success");
					result.put("count", count);
					Intent i = new Intent();
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.putExtra("values", result.toString());
					setResult(0, i);
					finish();
				} catch (JSONException e) {
					Log.d("Exception", e.getMessage());

				}
			}
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			try {

				ImageCompression ic = new ImageCompression();
				ic.compressImage(myFile);
				newFile=copyFile(myFile);

//				BitmapFactory.Options options = new BitmapFactory.Options();
//
//				// downsizing image as it throws OutOfMemory Exception for larger
//				// images
//				options.inSampleSize = 8;
//
//				final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
//						options);
//				Bitmap resizedBitmap = Bitmap.createScaledBitmap(
//						bitmap, 110,110, false);
//				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//				resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//				//you can create a new file name "test.jpg" in sdcard folder.
//				File f = new File(Environment.getExternalStorageDirectory()
//						+ File.separator + "test.jpg");
//
//				//write the bytes in file
//				FileOutputStream fo;
//				try {
//					f.createNewFile();
//					fo = new FileOutputStream(f);
//					fo.write(bytes.toByteArray());
//					fo.close();
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				File temprootdir = new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+ AppConstants.ROOT_FOLDER +AppConstants.TMP_DIR_NAME+  imgName+"_"+count+".jpg");
//				myFile.delete();
				Log.d("Native Encrypted file",""+newFile.getAbsolutePath());
//				AppConstants.EN_DE_OBJ.encrypt(newFile.getAbsolutePath(),myFile.getAbsolutePath());
//				if(count<5) {
//					AlertDialog.Builder builder1 = new AlertDialog.Builder(GalleryEditingActivity.this);
//					builder1.setMessage("Do you want to click another image?");
//					builder1.setCancelable(false);
//					builder1.setPositiveButton(
//							"Yes",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int id) {
//									dialog.cancel();
//									count++;
//									captureImage();
//								}
//							});
//
//					builder1.setNegativeButton(
//							"No",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int id) {
//									dialog.cancel();
////									try {
////										result.put("code", "1");
////										result.put("message", "success");
////										result.put("count", count);
////										Intent i = new Intent();
////										i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////										i.putExtra("values", result.toString());
////										setResult(0, i);
////										finish();
////									} catch (JSONException e) {
////										Log.d("Exception", e.getMessage());
////
////									}
//									Intent intent=new Intent(GalleryEditingActivity.this,DisplayImageActivity.class);
//									intent.putExtra("name", imgName);
//									intent.putExtra("path", imgPath);
//									startActivityForResult(intent,DISPLAY_IMAGE_REQUEST_CODE);
//								}
//							});
//
//					AlertDialog alert11 = builder1.create();
//					alert11.show();
//				}
//				else{
////					try {
////
////						result.put("code", "1");
////						result.put("message", "success");
////						result.put("count", count);
////						Intent i = new Intent();
////						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////						i.putExtra("values", result.toString());
////						setResult(0, i);
////						finish();
////					} catch (JSONException e) {
////						Log.d("Exception", e.getMessage());
////
////					}
//					Intent intent=new Intent(GalleryEditingActivity.this,DisplayImageActivity.class);
//					intent.putExtra("name", imgName);
//					intent.putExtra("path", imgPath);
//					startActivityForResult(intent,DISPLAY_IMAGE_REQUEST_CODE);
//				}
				finish();
			}
			catch (Exception e)
			{
				Log.d("Exception", e.getMessage());

			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	/**
	 * ------------ Helper Methods ----------------------
	 * */

	/*
	 * Creating file uri to store image
	 */
	//public Uri getOutputMediaFileUri(int type) {
//		return Uri.fromFile(getOutputMediaFile(type));
//	}

	/*
	 * returning image / video
	 */
//	private  File getOutputMediaFile(int type) {
//
//		// External sdcard location
//		mediaStorageDir = new File(
//				Environment
//						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				"TALIC");
//		// Create the storage directory if it does not exist
//		if (!mediaStorageDir.exists()) {
//			if (!mediaStorageDir.mkdirs()) {
//				Log.d("TALIC", "Oops! Failed create "
//						+ "TALIC" + " directory");
//				return null;
//			}
//		}
//		// Create a media file name
//		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
//				Locale.getDefault()).format(new Date());
//		File mediaFile;
//		if (type == MEDIA_TYPE_IMAGE) {
//			mediaFile = new File(mediaStorageDir.getPath() + File.separator
//					+ "IMG_" + timeStamp + ".jpg");
//		}  else {
//			return null;
//		}
//		myFile=mediaFile;
//		return mediaFile;
//	}
	 public File copyFile(File src) throws IOException {
       File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
//		 File path=new File(	 Environment
//				 .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				 "TALIC");
		 path.mkdirs();
       File mypath=new File(path,imgName);
       FileInputStream inStream = new FileInputStream(src);
       FileOutputStream outStream = new FileOutputStream(mypath);
       FileChannel inChannel = inStream.getChannel();
       FileChannel outChannel = outStream.getChannel();
       inChannel.transferTo(0, inChannel.size(), outChannel);
       inStream.close();
       outStream.close();
       return mypath;

   }
	private void performCrop(){
		//take care of exceptions

		try {
			long heightRatio= btmp.getHeight()/ btmp.getWidth();
			long widthtRatio=  btmp.getWidth()/btmp.getHeight();
			//call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.setDataAndType(fileUri, "image/*");
			//set crop properties
			cropIntent.putExtra("crop", "true");
			//indicate aspect of desired crop
			cropIntent.putExtra("aspectX", widthtRatio);
			cropIntent.putExtra("aspectY", heightRatio);
			//indicate output X and Y
			cropIntent.putExtra("outputX", 320);
			cropIntent.putExtra("outputY", 250);
			//retrieve data on return
			cropIntent.putExtra("return-data", true);
			//start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		//respond to users whose devices do not support the crop action
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}
//	private void clearData() {
//		File path=new File(AppConstants.SA_APP_CONTEXT.getFilesDir()+"/"+ AppConstants.ROOT_FOLDER + imgPath);
//		path.mkdirs();
//		for (int i=1;i<6;i++) {
//			File myfile=new File(path,imgName+"_"+i+".jpg");
//			Log.d("File Path", myfile.getAbsolutePath());
//			if(myfile.exists())
//			{
//				myfile.delete();
//
//			}
//		}
//
//	}

	public void forceCrash(View view) {
		throw new RuntimeException("This is a crash");
	}

}
