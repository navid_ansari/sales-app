package com.talic.plugins;

import android.content.res.AssetManager;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;

public class AppUtils {
    public static final String TAG = "SA";

    public void copyFiles(AssetManager assetManager,String assetPath,String path,String[] fileList){
        try {
            Log.d(TAG, "AM LIST: " + Arrays.toString(fileList));
            for (String fileName : fileList) {
                InputStream in;
                OutputStream out;
                String newFileName;
                in = assetManager.open(assetPath + fileName);
                Log.d(TAG, "newFileName: " + AppConstants.FILE_ABS_PATH + path + fileName);
                if (fileName.endsWith(".jpg")) // extension was added to avoid compression on APK file
                    newFileName = AppConstants.FILE_ABS_PATH + path + fileName.substring(0, fileName.length() - 4);
                else
                    newFileName = AppConstants.FILE_ABS_PATH + path + fileName;
                out = new FileOutputStream(newFileName);
                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                out.flush();
                out.close();
            }
        } catch (FileNotFoundException ex) {
            Log.d(TAG, "Exception in copyFile() " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            Log.d(TAG, "Exception in copyFile() " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    //camera
    public String readOriginalFile(String fileName, String fileRelPath){
        Log.d(TAG, "readDecryptedFile: " + AppConstants.FILE_ABS_PATH + fileRelPath + fileName);
		AppUtils nf = new AppUtils();
		nf.deleteTmp();
        File folder = new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME);
        if (!folder.exists()) {
            folder.mkdir();
        }
        File srcFile = new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileName);
        //Log.d(TAG, "readEncryptedFile fileData.length: " + srcFile.length());
        if(srcFile.exists()) {
            //AppConstants.EN_DE_OBJ.decrypt(AppConstants.FILE_ABS_PATH + fileRelPath + fileName, AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME + "/" + fileName);
            String type = fileName.substring(fileName.length() - 4, fileName.length());
            String decryptedFileData;
            if (".jpg".equals(type))
                decryptedFileData = Base64.encodeToString(readFile(fileName, fileRelPath ), Base64.DEFAULT);
            else
                decryptedFileData = new String(readFile(fileName, AppConstants.FILE_ABS_PATH + fileRelPath ));
            Log.d(TAG, "readDecryptedFile fileData.length: " + decryptedFileData.length());

            return decryptedFileData;
        }
        return null;
    }

    public void deleteGSInternalStorageFolder(File fileOrDirectory){
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteGSInternalStorageFolder(child);
        fileOrDirectory.delete();
    }

    public void createGSInternalStorageFolder(String[] dirList, int i){
		try {
			Log.d(TAG, "createGSInternalStorageFolder " + i);
			File fileOrDirectory = new File(AppConstants.FILE_ABS_PATH + dirList[i]);
			if (!fileOrDirectory.isDirectory()) {
				fileOrDirectory.mkdir();
				Log.d(TAG, "createGSInternalStorageFolder Making dir: " + dirList[i]);
			}
			i++;
			if (i < dirList.length)
				createGSInternalStorageFolder(dirList, i);
		}catch (Exception ex){
			Log.d(TAG, "Exception ex: " + ex.getMessage());
		}
    }

    public byte[] readFile(String fileName, String fileRelPath){
        FileInputStream in;
        try {
            Log.d(TAG, "readFilePath: " + AppConstants.FILE_ABS_PATH + fileRelPath + "/" + fileName);
            File file = new File(AppConstants.FILE_ABS_PATH + fileRelPath + "/" + fileName);
            if(file.exists()) {
                int length = (int) file.length();
                byte[] bytes = new byte[length];
                in = new FileInputStream(file);
                in.read(bytes);
                in.close();
                return bytes;
            }
            return null;
        } catch (FileNotFoundException ex){
            Log.d(TAG, "Exception in readFile(): " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex){
            Log.d(TAG, "Exception in readFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }

    public String readDecryptedFile(String fileName, String fileRelPath){
        Log.d(TAG, "readDecryptedFile: " + AppConstants.FILE_ABS_PATH + fileRelPath + fileName);
        AppUtils nf = new AppUtils();
		nf.deleteTmp();
		File folder = new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME);
		if (!folder.exists()) {
            folder.mkdir();
        }
		File srcFile = new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileName);
		if(srcFile.exists()) {
			AppConstants.EN_DE_OBJ.decrypt(AppConstants.FILE_ABS_PATH + fileRelPath + fileName, AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME + "/" + fileName);
			String type = fileName.substring(fileName.length() - 4, fileName.length());
			String decryptedFileData;
			if (".jpg".equals(type))
				decryptedFileData = Base64.encodeToString(readFile(fileName, AppConstants.TMP_DIR_NAME), Base64.DEFAULT);
			else
				decryptedFileData = new String(readFile(fileName, AppConstants.TMP_DIR_NAME));
			Log.d(TAG, "readDecryptedFile fileData.length: " + decryptedFileData.length());

			return decryptedFileData;
		}
		return null;
    }

    public String getDecryptedFileName(String fileName, String fileRelPath){
        Log.d(TAG, "readDecryptedFilePath: " + AppConstants.FILE_ABS_PATH + fileRelPath + fileName);
		AppUtils nf = new AppUtils();
		nf.deleteTmp();
        File folder = new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME);
        if (!folder.exists()) {
            folder.mkdir();
        }
        AppConstants.EN_DE_OBJ.decrypt(AppConstants.FILE_ABS_PATH + fileRelPath + fileName, AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME + "/" + fileName);
		/*try {
			copyFileUsingStream(new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME + "/" + fileName), new File(AppConstants.SA_APP_CONTEXT.getExternalFilesDir(null).getPath() + "/" + fileName));
		}catch (IOException ex){
			Log.d(TAG,"Exception in readDecryptedFile: " + ex.getMessage());
			ex.printStackTrace();
		}*/
        return fileName;
    }

    public void saveNonEncryptedFile(String fileNameWithExt, String fileRelPath, String fileData, String isBase64){
        try{
			if(!(new File(AppConstants.FILE_ABS_PATH + fileRelPath)).exists())
				new File(AppConstants.FILE_ABS_PATH + fileRelPath).mkdir();
			File file = new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileNameWithExt);
            FileOutputStream stream = new FileOutputStream(file);
			if("Y".equalsIgnoreCase(isBase64))
                stream.write(Base64.decode(fileData, Base64.DEFAULT));
			else
				stream.write(fileData.getBytes());
			stream.close();

            /*try {
                copyFileUsingStream(new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileNameWithExt), new File(AppConstants.SA_APP_CONTEXT.getExternalFilesDir(null).getPath() + "/" + fileNameWithExt));
            }catch (IOException ex){
                Log.d(TAG,"Exception in readDecryptedFile: " + ex.getMessage());
                ex.printStackTrace();
            }*/
			Log.d(TAG, "SaveNonEncryptedFile fileExists(): " + file.exists() + " " + file.length());
        }catch (FileNotFoundException ex){
            Log.e(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex){
            Log.e(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void saveEncryptedFile(String fileName, String fileType, String fileRelPath, String fileData){
        try{
            File file = new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileName + fileType);
            FileOutputStream stream = new FileOutputStream(file);
            if(fileType.equals(".jpg"))
                stream.write(Base64.decode(fileData, Base64.DEFAULT));
            else
                stream.write(fileData.getBytes());
            /*if(fileType.equalsIgnoreCase(".html") || fileType.equalsIgnoreCase(".htm"))
                stream.write(fileData.getBytes());
            else
                stream.write(Base64.decode(fileData, Base64.DEFAULT));*/
            String rawFilePath = AppConstants.FILE_ABS_PATH + fileRelPath + fileName + fileType;
            String encFilePath = AppConstants.FILE_ABS_PATH + fileRelPath + fileName + AppConstants.ENCRYPT_FILE_PREFIX + fileType;
            AppConstants.EN_DE_OBJ.encrypt(rawFilePath, encFilePath);
        }catch (FileNotFoundException ex){
            Log.e(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex){
            Log.e(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void saveToClientEncryptedFile(String fileName, String fileType, String fileRelPath, String fileData, String isBase64){
        try{
            File file = new File(AppConstants.FILE_ABS_PATH + fileRelPath + fileName + fileType);
            FileOutputStream stream = new FileOutputStream(file);
            if(isBase64!=null && "N".equals(isBase64))
                stream.write(fileData.getBytes());
            else
                stream.write(Base64.decode(fileData, Base64.DEFAULT));
            String rawFilePath = AppConstants.FILE_ABS_PATH + fileRelPath + fileName + fileType;
            String encFilePath = AppConstants.FILE_ABS_PATH + fileRelPath + fileName + AppConstants.ENCRYPT_FILE_PREFIX + fileType;
            AppConstants.EN_DE_OBJ.encrypt(rawFilePath, encFilePath);
        }catch (FileNotFoundException ex){
            Log.e(TAG,"Exception in saveToClientEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex){
            Log.e(TAG,"Exception in saveToClientEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public boolean deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        return fileOrDirectory.delete();
    }

    public String deleteTmp() {
        File folder = new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME);
        if (deleteRecursive(folder)) {
            return "tmp folder deleted";
        }
        return "tmp folder does not exist";
    }

    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    public String getDataFromFileInAssets(String file) throws IOException {
        StringBuilder buf=new StringBuilder();
        InputStream json = AppConstants.SA_APP_CONTEXT.getAssets().open(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
        String str;
        while ((str=in.readLine()) != null) {
            buf.append(str);
        }
        in.close();
        return buf.toString();
    }

}
