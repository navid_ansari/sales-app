package com.talic.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.billdesk.sdk.LibraryPaymentStatusProtocol;
import com.talic.salesapp.SalesApp;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saurabhmac on 22/01/16.
 */

public class MainPlugin extends CordovaPlugin implements LibraryPaymentStatusProtocol, Parcelable {
    static CallbackContext callbackContext;
    String TAG="Callback ::: > ";
    public static String data;
    public MainPlugin()
    {

    }
    public boolean startPaymentGateway(JSONArray args, CallbackContext callbackContext){
        try {
//@Override
//public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
            this.callbackContext = callbackContext;
            JSONObject reqObj = new JSONObject(args.getString(0));
            String Amount = (String) reqObj.get("Amount");
            String PolicyNo = (String) reqObj.get("PolicyNo");
            String PremiumType = (String) reqObj.get("PremiumType");
            String OwnerName = (String) reqObj.get("OwnerName");
            String AgentCode = (String) reqObj.get("AgentCode");
            String PremiumDueDate = (String) reqObj.get("PremiumDueDate");
            String Mobile = (String) reqObj.get("Mobile");
            String EmailId = (String) reqObj.get("EmailId");
            String TrSeries = (String) reqObj.get("TrSeries");
            //String Domain = (String)reqObj.get("Domain");
            Log.d("USer Data", args.toString());
            Context context = this.cordova.getActivity();
            Intent intent = new Intent(this.cordova.getActivity(), MainPaymentActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Amount", Amount);
            intent.putExtra("PolicyNo", PolicyNo);
            intent.putExtra("PremiumType", PremiumType);
            intent.putExtra("OwnerName", OwnerName);
            intent.putExtra("AgentCode", AgentCode);
            intent.putExtra("PremiumDueDate", PremiumDueDate);
            intent.putExtra("Mobile", Mobile);
            intent.putExtra("EmailId", EmailId);
            intent.putExtra("TrSeries", TrSeries);
            intent.putExtra("plug", this);
            //intent.putExtra("Domain",Domain);
            context.startActivity(intent);
            finish();
            return true;
        }catch (JSONException ex) {
            Log.d(TAG, "Exception : " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }
    public MainPlugin(Parcel in) {
        Log.v(TAG, "CallBack(Parcel in)....");
    }

    @Override
    public void paymentStatus(String status, Activity context) {
        Log.v(TAG, "paymentStatus(String status, Activity context)....::::status:::::" + status.length());
        Log.d("LP", "onActivityResult Success!! :" + status);
        data=status;
        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status));

        // code for  the activity
        Intent mIntent = new Intent(context, SalesApp.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(mIntent);
        context.finish();

    }
    @Override
    public int describeContents() {
        Log.v(TAG, "describeContents()....");
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Log.v(TAG, "writeToParcel(Parcel dest, int flags)....");
        // TODO Auto-generated method stub
    }
    @SuppressWarnings("rawtypes")
    public static final Creator CREATOR = new Creator() {
        String TAG="Callback --- Parcelable.Creator ::: > ";
        @Override
        public MainPlugin createFromParcel(Parcel in) {
            Log.v(TAG, "CallBackActivity createFromParcel(Parcel in)....");
            return new MainPlugin(in);
//            return null;
        }

        @Override
        public Object[] newArray(int size) {
            Log.v(TAG, "Object[] newArray(int size)....");
            return new MainPlugin[size];
        }
    };
    private void finish() {
    }
}

