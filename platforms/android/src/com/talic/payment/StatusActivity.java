package com.talic.payment;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

public class StatusActivity extends Activity {
	TextView status ;

	String mStatus[];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);


		Bundle bundle = this.getIntent().getExtras();
		mStatus = bundle.getString("status").toString().split("\\|");

		Toast.makeText(StatusActivity.this,""+bundle.getString("status"),Toast.LENGTH_LONG).show();

	}

}
