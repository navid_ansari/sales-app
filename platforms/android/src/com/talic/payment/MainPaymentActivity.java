package com.talic.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.billdesk.sdk.PaymentOptions;
import com.talic.plugins.PluginHandler;
import com.talic.salesapp.SalesApp;

import org.apache.cordova.PluginResult;

import com.talic.plugins.AppConstants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by saurabhmac on 22/01/16.
 */
public class MainPaymentActivity extends Activity {

    String strToken = "NA";
    String strEmail = "";
    String strMobile = "";
    String strMsg = null;

    String commonUrl,strDomain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String currentDateandTime = sdf.format(new Date());
            Log.d("Current Date:", currentDateandTime);
            Intent intent = getIntent();
            String Amount = intent.getStringExtra("Amount");
            String PolicyNo = intent.getStringExtra("PolicyNo");
            String PremiumType = intent.getStringExtra("PremiumType");
            String OwnerName = intent.getStringExtra("OwnerName");
            String AgentCode = intent.getStringExtra("AgentCode");
            String PremiumDueDate = intent.getStringExtra("PremiumDueDate");
            String Mobile = intent.getStringExtra("Mobile");
            String EmailId = intent.getStringExtra("EmailId");
            String TrSeries = intent.getStringExtra("TrSeries");
            strEmail = EmailId;
            strMobile = Mobile;
            strDomain = AppConstants.DOMAIN_URL;
            Log.d("Amount:", Amount);
            Log.d("PolicyNo:", PolicyNo);
            Log.d("PremiumType:", PremiumType);
            Log.d("OwnerName:", OwnerName);
            Log.d("AgentCode:", AgentCode);
            Log.d("PremiumDueDate:", PremiumDueDate);
            Log.d("Mobile:", Mobile);
            Log.d("EmailId:", EmailId);
            Log.d("TrSeries:", TrSeries);
            commonUrl = "TALIC|" + PolicyNo + "|NA|" + Amount + "|NA|NA|NA|INR|NA|R|TALIC|NA|NA|F|" + PremiumType + "|"+ OwnerName + "|" + AgentCode + "|" + PremiumDueDate + "|" + Mobile + "|" + EmailId + "|" + TrSeries +"|" + strDomain + "BillDeskPG/ReturnURL.jsp";
            Log.d("Common Url",commonUrl);
            if (isNetworkAvailable()) {
                generateCheckSum();
            } else {
                Toast.makeText(this, "Please Check Internet Connection", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generateCheckSum() {
        String serverRequestUrl =  strDomain + "BillDeskPG/generateCheckSum?msg="+ commonUrl;// ;strDomain + "BillDeskPG/generateCheckSum?msg=" + commonUrl;
        new getCheckSumTask().execute(serverRequestUrl);
    }

    private class getCheckSumTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            String urlString = params[0]; // URL to call
            String response = null;

            BufferedReader reader = null;

            // Send data
            try {
                // Defined URL  where to send data
                URL url = new URL(urlString);
                // Send POST data request
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                // wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {

                    // Append server response in string
                    sb.append(line + "");
                }
                // Append Server Response To Content String
                response = sb.toString();
                Log.d("Response  background:", response);
            } catch (Exception ex) {
                //Error = ex.getMessage();
            } finally {
                try {
                    reader.close();
                } catch (Exception ex) {
                }
            }

            return response;

        }

        protected void onPostExecute(String result) {
            if (result != null && (!result.equalsIgnoreCase("ERROR|26"))) {
                String response = result;
                strMsg = response;
                Log.d("PG Server strMsg:", strMsg);
                PluginHandler callbackObj = (PluginHandler)getIntent().getExtras().get("plug");
                Intent intent = new Intent(MainPaymentActivity.this, PaymentOptions.class);
                if (strMsg != null) {
                    Log.d("PG Plugin strMsg:", commonUrl + "|" + strMsg);
                    intent.putExtra("msg", commonUrl + "|" + strMsg); //pg_msg
                }
                intent.putExtra("token", strToken);
                intent.putExtra("user-email", strEmail);
                intent.putExtra("user-mobile", strMobile);
                intent.putExtra("callback", callbackObj);
                intent.putExtra("orientation", Configuration.ORIENTATION_LANDSCAPE);
                startActivity(intent);
                Log.d("SA", "Payment Success callback");
                finish();
            }else {
                PluginHandler callbackObj = (PluginHandler)getIntent().getExtras().get("plug");
                Log.d("SA", "Payment Error callback");
				PluginResult pRes = new PluginResult(PluginResult.Status.ERROR, "Server not Responding.\nPlease try again later.");
				pRes.setKeepCallback(true);
                callbackObj.currentContext.sendPluginResult(pRes);
                Intent mIntent = new Intent(getApplicationContext(), SalesApp.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent);
                finish();
            }

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
