package com.talic.salesapp;

import android.content.Context;
import android.provider.Settings;


import com.talic.plugins.AppConstants;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;
import net.sqlcipher.database.SQLiteOpenHelper;


public class OpenHelper extends SQLiteOpenHelper {

	public OpenHelper(Context context) {
		super(context, AppConstants.SA_MAIN_DB_NAME, null, AppConstants.SA_DB_VER);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {

	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.println("DB oldVersion: " + oldVersion + 	" newVersion: " + newVersion);
		if (oldVersion < 1) {
			System.out.println("OLD DB");
			db.execSQL("ALTER TABLE LP_APP_HD_ANS_SCRN_C ADD COLUMN ANSWAR_DTLS3 TEXT(250)");
			db.execSQL("ALTER TABLE LP_APP_HD_ANS_SCRN_C ADD COLUMN ANSWAR_DTLS4 TEXT(250)");
			db.execSQL("ALTER TABLE LP_APP_HD_ANS_SCRN_C ADD COLUMN ANSWAR_DTLS5 TEXT(250)");
			db.execSQL("ALTER TABLE LP_APP_HD_ANS_SCRN_C ADD COLUMN ANSWAR_DTLS6 TEXT(250)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN COMPANY_CODE TEXT(4)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA TEXT(1)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN BIRTH_COUNTRY TEXT(50)");
			db.execSQL("CREATE TABLE LP_APP_COMPANY_MASTER (COMPANY_CODE TEXT(4) PRIMARY KEY  NOT NULL , COMPANY_NAME TEXT(50), TATA_FLAG TEXT(5), VERSION_NO TEXT(5))");
			db.execSQL("PRAGMA user_version = 1");
		}
		if(oldVersion < 2){
			System.out.println("OLD DB");
			db.execSQL("UPDATE LP_FHR_FINANCIAL_REQ_GRID SET CUSTOMER_MIN_AGE='56' WHERE CUSTOMER_MIN_AGE='15' AND CUSTOMER_MAX_AGE='999' AND SUM_ASSURED_MIN='1500000' AND SUM_ASSURED_MAX='9999999999999'");
			db.execSQL("PRAGMA user_version = 2");
		}
		if(oldVersion < 3) {

			db.execSQL("ALTER TABLE LP_SIS_MAIN ADD COLUMN DISCOUNTED_MODAL_PREMIUM TEXT(11)");
			db.execSQL("ALTER TABLE LP_SIS_MAIN ADD COLUMN DISCOUNTED_SERVICE_TAX TEXT(11)");
			db.execSQL("ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN DISCOUNTED_MODAL_PREMIUM TEXT(11)");
			db.execSQL("ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN DISCOUNTED_SERVICE_TAX TEXT(11)");
			db.execSQL("ALTER TABLE LP_SIS_SCREEN_A ADD COLUMN TATA_FLAG TEXT(1)");
			db.execSQL("ALTER TABLE LP_SIS_SCREEN_A ADD COLUMN COMPANY_CODE TEXT(20)");
			db.execSQL("ALTER TABLE LP_SIS_SCREEN_A ADD COLUMN COMPANY_NAME TEXT(250)");

			final String RENAME_PAY_DTLS_SCRN_G = "ALTER TABLE LP_APP_PAY_DTLS_SCRN_G RENAME TO LP_APP_PAY_DTLS_SCRN_G_BKP;";
			final String CRT_PAY_DTLS_SCRN_G = "CREATE TABLE LP_APP_PAY_DTLS_SCRN_G (APPLICATION_ID TEXT (15),AGENT_CD TEXT (10),POLICY_NO TEXT (10) NOT NULL ,PREMIUM_PAY_MODE TEXT(2) NOT NULL ,PAY_METHOD_CASH_FLAG TEXT (1),PAY_METHOD_CHEQ_FLAG TEXT (1),PAY_METHOD_DD_FLAG TEXT (1),PAY_METHOD_ECS_FLAG TEXT(1), PAY_METHOD_SI_FLAG TEXT (1),CASH_AMOUNT TEXT (13, 2),CHEQUE_NO TEXT (10),CHEQ_BANK_NAME TEXT (30),CHEQ_BANK_IFSC_CODE TEXT(20), CHEQ_AMOUNT TEXT (13),CHEQ_DATE TEXT (8),DEMAND_DRAFT_NO TEXT (10),DD_BANK_NAME TEXT (30),DD_BANK_IFSC_CODE TEXT(20), DD_BANK_BRANCH_NAME TEXT (30),DD_AMOUNT TEXT (13),CREDIT_CARD_NO TEXT (20),CC_AMOUNT TEXT (13),CC_EXP_DATE TEXT(8), ECS_BANK_ACCOUNT_NO TEXT (20),ECS_BANK_NAME TEXT (30),ECS_BANK_IFSC_CODE TEXT (20),ECS_BANK_BRANCH_NAME TEXT (30),ECS_AMOUNT TEXT(13), SI_BANK_ACCOUNT_NO TEXT (20),SI_BANK_NAME TEXT (30),SI_BANK_IFSC_CODE TEXT (20),SI_BANK_BRANCH_NAME TEXT (30),SI_AMOUNT TEXT(13), TOTAL_PAYMENT_AMOUNT TEXT (13),TRANSACTION_DATE TEXT (20),TOTAL_PREMIUM_AMOUNT TEXT (11),TOTAL_SERVICE_TAX TEXT(13) ,MONTHS_INITIAL_DEPOSIT TEXT (5),FIRST_ANIV_CHANGE_PAY_MODE TEXT (20) DEFAULT (null) ,SI_IMAGE_CAPTURED TEXT(1), CC_IMAGE_CAPTURED TEXT(1), MODIFIED_DATE TEXT (20),PAY_METHOD_CC_FLAG TEXT (1),PAY_METHOD_ONLINE_FLAG TEXT(1) ,CHEQ_BANK_BRANCH_NAME TEXT(30), CHEQ_IMAGE_CAPTURED TEXT (1),DD_IMAGE_CAPTURED TEXT (1),CC_AUTHORIZE_FORM TEXT(1) ,ECS_AUTHORIZE_FORM TEXT (1),SI_AUTHORIZE_FORM TEXT (1), ONLINE_AMOUNT TEXT (13),ONLINE_TRANS_REF_NO TEXT (20),DOC_ID TEXT(15), FIRST_ANIV_CHANGE_PAY_MODE_CODE TEXT(2), CC_CARD_TYPE TEXT(15), RYP_AUTOPAY_FLAG  TEXT(50), RYP_AUTOPAY_NACH_FLAG TEXT(50), RYP_AUTOPAY_SI_FLAG  TEXT(50), RYP_SI_BANK_ACCOUNT_NO  TEXT(50), RYP_SI_BANK_ACCOUNT_TYPE  TEXT(50), RYP_SI_BANK_NAME TEXT(50), RYP_SI_BANK_IFSC_CODE  TEXT(50), RYP_SI_BANK_BRANCH_NAME  TEXT(50), RYP_SI_AMOUNT  TEXT(50), RYP_SI_CHEQUE_IMAGE TEXT(50), RYP_SI_MANDATE_FORM  TEXT(50), RYP_NACH_BANK_ACCOUNT_NO  TEXT(50), RYP_NACH_BANK_ACCOUNT_TYPE  TEXT(50), RYP_NACH_BANK_NAME  TEXT(50), RYP_NACH_BANK_IFSC_CODE  TEXT(50), RYP_NACH_BANK_BRANCH_NAME  TEXT(50), RYP_NACH_AMOUNT TEXT(50), RYP_NACH_CHCK_IMAGE  TEXT(50), RYP_NACH_MANDATE_FORM TEXT(50),PRIMARY KEY('APPLICATION_ID','AGENT_CD'));";
			final String INS_INTO_SCRN_G = "INSERT INTO LP_APP_PAY_DTLS_SCRN_G SELECT * FROM LP_APP_PAY_DTLS_SCRN_G_BKP;";
			final String DROP_SCRN_G_BKP = "DROP TABLE LP_APP_PAY_DTLS_SCRN_G_BKP;";

			final String CRT_LP_BKP_STATUS = "CREATE TABLE LP_BKP_STATUS(TABLE_NAME TEXT(50),AGENT_CD TEXT(15),DATA_SYNC TEXT(2),MODIFIED_DATE TEXT(20))";

			final String INS_LP_BKP_STATUS = 	"INSERT INTO LP_BKP_STATUS VALUES ('LP_APPLICATION_MAIN',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_CONTACT_SCRN_A',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_EXIST_INS_SCRN_D',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_FH_SCRN_E',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_FUND_SCRN_I',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_HD_ANS_SCRN_C',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_HD_ANS_SCRN_C13',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_LS_ANS_SCRN_B',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_NOM_CONTACT_SCRN_F',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_PAY_DTLS_SCRN_G',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_APP_PAY_NEFT_SCRN_H',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_COMBO_DTLS',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_COMBO_RIDER_DTLS',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_MYCOMBO',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_DOCUMENT_CAPTURE',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_DOCUMENT_UPLOAD',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_DEPENDENT_DTLS_SCRN_A',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_EXIST_INS_INF_SCRN_A',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_FIN_SAV_INF_SCRN_B',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_FIN_SAV_INF_SCRN_C',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_FIN_SAV_INF_SCRN_D',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_FIN_SAV_INF_SCRN_E',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_GOAL_CHILD_EDU_SCRN_B',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_GOAL_CHILD_MRG_SCRN_C',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_GOAL_LIVING_STD_SCRN_F',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_GOAL_RETIREMENT_SCRN_E',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_GOAL_WEALTH_SCRN_D',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_MAIN',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_FHR_PERSONAL_INF_SCRN_A',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_MYLEAD',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_MYOPPORTUNITY',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_MYOPPORTUNITY_DEVIATION',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_ONLINE_PAYMENT_DTLS',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_POLICY_ASSIGNMENT',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_SIS_MAIN',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_SIS_SCREEN_A',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_SIS_SCREEN_B',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_SIS_SCREEN_C',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_SIS_SCREEN_D',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_EMAIL_USER_DTLS',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_LINKED_POLICY',null,null,null);" +
												"INSERT INTO LP_BKP_STATUS VALUES ('LP_MYCALENDAR',null,null,null);";

			final String CRT_LP_EKYC_DTLS = "CREATE TABLE LP_EKYC_DTLS (OPP_ID TEXT(15) , SIS_ID TEXT(10), CUST_TYPE TEXT(20), PUR_FOR_CD TEXT(3), BUY_FOR_CD TEXT(3), APP_ID TEXT(15), AGENT_CD TEXT(10), EKYC_FLAG TEXT(1), NAME TEXT(60), GENDER TEXT(5), DOB TEXT(20), EMAIL_ID TEXT(20), BUILDING TEXT(30), STREET TEXT(30), AREA TEXT(30), CITY TEXT(30), DISTRICT TEXT(30), STATE TEXT(20), PIN TEXT(15), POSTOFFICE TEXT(15), EKYC_TRAN_ID TEXT(15), CARE_OF TEXT(10), PHONE_NO TEXT(20), NATIONALITY TEXT(30), CONSUMER_AADHAR_NO TEXT(12), RESPONSE_CODE TEXT(10), RESPONSE_MSG TEXT(20), AUTH_CODE_UIDAI TEXT(10), AUTH_MSG TEXT(20), TIMESTAMP TEXT(20), ERROR_CD_UIDAI TEXT(10), ERROR_MSG_UIDAI TEXT(20), EKYC_DONE_DATE TEXT(20), SUBDISTRICT TEXT(30), LANDMARK TEXT(30),PRIMARY KEY(OPP_ID ,AGENT_CD,CUST_TYPE))";

			db.execSQL(RENAME_PAY_DTLS_SCRN_G);
			db.execSQL(CRT_PAY_DTLS_SCRN_G);
			db.execSQL(INS_INTO_SCRN_G);
			db.execSQL(DROP_SCRN_G_BKP);
			db.execSQL(CRT_LP_BKP_STATUS);
			db.execSQL(INS_LP_BKP_STATUS);
			db.execSQL(CRT_LP_EKYC_DTLS);
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN AGE_PROOF_DOB TEXT(8)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN DATE_OF_ISSUANCE TEXT(8)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN DATE_OF_EXPIRY TEXT(8)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN DATE_OF_BILL TEXT(8)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN ID_DATE_OF_ISSUANCE TEXT(8)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN ID_DATE_OF_EXPIRY TEXT(8)");
			db.execSQL("ALTER TABLE LP_FHR_FINANCIAL_REQ_GRID ADD COLUMN IS_IBL TEXT(5)");
			db.execSQL("ALTER TABLE LP_APP_PAY_NEFT_SCRN_H ADD COLUMN OTHER_BANK_NAME TEXT(60)");
			db.execSQL("ALTER TABLE LP_DOC_PROOF_MASTER ADD COLUMN OCCUPATION_CLASS TEXT(100)");

			db.execSQL("UPDATE LP_MYLEAD SET GENDER_CD='1' WHERE GENDER_CD='M' OR GENDER_CD='m'");
			db.execSQL("UPDATE LP_MYLEAD SET GENDER_CD='2' WHERE GENDER_CD='F' OR GENDER_CD='f'");

			db.execSQL("DELETE FROM LP_FHR_FINANCIAL_REQ_GRID");

			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('0','18','0','2500000','N','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('0','18','2500000','5000000','N','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('0','18','5000000','9999999999999','N','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('0','999','0','5000000','N','N','Y','1','Y')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('0','999','5000000','9999999999999','N','Y','Y','1','Y')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('19','21','0','2500000','Y','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('19','21','2500000','5000000','N','Y','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('19','21','5000000','9999999999999','N','Y','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('22','55','0','2500000','N','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('22','55','2500000','5000000','N','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('22','55','5000000','9999999999999','N','Y','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('56','999','0','2500000','Y','N','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('56','999','2500000','5000000','N','Y','Y','1','N')");
			db.execSQL("INSERT INTO LP_FHR_FINANCIAL_REQ_GRID VALUES ('56','999','5000000','9999999999999','N','Y','Y','1','N')");

			db.execSQL("PRAGMA user_version = 3");
		}
		if(oldVersion < 4){
			System.out.println("OLD DB < 4");
			db.execSQL("CREATE TABLE LP_COMBO_ADD_DETAILS (OPPORTUNITY_ID TEXT(15) NOT NULL , AGENT_CD TEXT(15) NOT NULL , LEAD_ID TEXT(15) NOT NULL , FHR_ID TEXT(15) NOT NULL , RESIDENT_STATUS TEXT(50), RESIDENT_STATUS_CODE TEXT(5), NATIONALITY TEXT(30), NATIONALITY_CODE TEXT(10), CURR_RESIDENT_COUNTRY TEXT(30), CURR_RESIDENT_COUNTRY_CODE TEXT(10), EDUCATION_QUALIFICATION TEXT(50), EDUCATION_QUALIFICATION_CODE TEXT(5), CURR_CITY TEXT(30), CURR_CITY_CODE TEXT(2), CURR_STATE TEXT(30), CURR_STATE_CODE TEXT(2), PLAN_NAME TEXT(60), PLAN_CODE TEXT(10), PREMIUM_PAY_MODE TEXT(2), POLICY_TERM TEXT(15), PREMIUM_PAY_TERM TEXT(10), SUM_ASSURED TEXT(15), BASE_PREMIUM TEXT(10), INVT_STRAT_FLAG TEXT(2), MODIFIED_DATE TEXT(15), OCCUPATION_CLASS TEXT, OCCUPATION_CLASS_NBFE_CODE TEXT, CITY_TERM_FLAG TEXT, COUNTRY_TERM_FLAG TEXT, CITY_TERM_NML_A TEXT, COUNTRY_TERM_NML_A TEXT, CITY_TERM_NML_B TEXT, COUNTRY_TERM_NML_B TEXT, PGL_ID TEXT, PREMIUM_PAY_MODE_DESC TEXT, PREM_MULTIPLIER TEXT, INPUT_ANNUAL_PREMIUM TEXT, PRIMARY KEY (OPPORTUNITY_ID, AGENT_CD, LEAD_ID, FHR_ID))");
			db.execSQL("CREATE TABLE LP_COMBO_NOM_CONTACT_SCRN_F (COMBO_ID TEXT NOT NULL , AGENT_CD TEXT NOT NULL , CUST_TYPE TEXT NOT NULL , TITLE TEXT, FIRST_NAME TEXT, MIDDLE_NAME TEXT, LAST_NAME TEXT, BIRTH_DATE TEXT, RELATIONSHIP_DESC TEXT, RELATIONSHIP_ID TEXT, MODIFIED_DATE TEXT, SEQ_NO TEXT, GENDER TEXT, GENDER_CODE TEXT, PERCENTAGE TEXT, DOC_ID TEXT, PRIMARY KEY (COMBO_ID, AGENT_CD, CUST_TYPE, SEQ_NO))");
			db.execSQL("CREATE TABLE LP_COMBO_PAY_DTLS_SCRN_G(COMBO_ID TEXT (15),AGENT_CD TEXT (10),POLICY_NO TEXT (10) NOT NULL ,PREMIUM_PAY_MODE TEXT(2) NOT NULL ,PAY_METHOD_CASH_FLAG TEXT (1),PAY_METHOD_CHEQ_FLAG TEXT (1),PAY_METHOD_DD_FLAG TEXT (1),PAY_METHOD_ECS_FLAG TEXT(1), PAY_METHOD_SI_FLAG TEXT (1),CASH_AMOUNT TEXT (13, 2),CHEQUE_NO TEXT (10),CHEQ_BANK_NAME TEXT (30),CHEQ_BANK_IFSC_CODE TEXT(20), CHEQ_AMOUNT TEXT (13),CHEQ_DATE TEXT (8),DEMAND_DRAFT_NO TEXT (10),DD_BANK_NAME TEXT (30),DD_BANK_IFSC_CODE TEXT(20), DD_BANK_BRANCH_NAME TEXT (30),DD_AMOUNT TEXT (13),CREDIT_CARD_NO TEXT (20),CC_AMOUNT TEXT (13),CC_EXP_DATE TEXT(8), ECS_BANK_ACCOUNT_NO TEXT (20),ECS_BANK_NAME TEXT (30),ECS_BANK_IFSC_CODE TEXT (20),ECS_BANK_BRANCH_NAME TEXT (30),ECS_AMOUNT TEXT(13), SI_BANK_ACCOUNT_NO TEXT (20),SI_BANK_NAME TEXT (30),SI_BANK_IFSC_CODE TEXT (20),SI_BANK_BRANCH_NAME TEXT (30),SI_AMOUNT TEXT(13), TOTAL_PAYMENT_AMOUNT TEXT (13),TRANSACTION_DATE TEXT (20),TOTAL_PREMIUM_AMOUNT TEXT (11),TOTAL_SERVICE_TAX TEXT(13) ,MONTHS_INITIAL_DEPOSIT TEXT (5),FIRST_ANIV_CHANGE_PAY_MODE TEXT (20) DEFAULT (null) ,SI_IMAGE_CAPTURED TEXT(1), CC_IMAGE_CAPTURED TEXT(1), MODIFIED_DATE TEXT (20),PAY_METHOD_CC_FLAG TEXT (1),PAY_METHOD_ONLINE_FLAG TEXT(1) ,CHEQ_BANK_BRANCH_NAME TEXT(30), CHEQ_IMAGE_CAPTURED TEXT (1),DD_IMAGE_CAPTURED TEXT (1),CC_AUTHORIZE_FORM TEXT(1) ,ECS_AUTHORIZE_FORM TEXT (1),SI_AUTHORIZE_FORM TEXT (1), ONLINE_AMOUNT TEXT (13),ONLINE_TRANS_REF_NO TEXT (20),DOC_ID TEXT(15), FIRST_ANIV_CHANGE_PAY_MODE_CODE TEXT(2), CC_CARD_TYPE TEXT(15),PRIMARY KEY('COMBO_ID','AGENT_CD','POLICY_NO'))");
			db.execSQL("CREATE TABLE LP_COMBO_PAY_NEFT_SCRN_H (COMBO_ID TEXT NOT NULL , AGENT_CD TEXT NOT NULL , NEFT_ACCOUNT_HOLDER_NAME TEXT, NEFT_BANK_ACCOUNT_NO TEXT, NEFT_BANK_NAME TEXT, NEFT_BANK_BRANCH_NAME TEXT, NEFT_BANK_ACCOUNT_TYPE TEXT, NEFT_BANK_IFSC_CODE TEXT, MODIFIED_DATE TEXT, CHQ_BANKST_IMAGE_CAPTURED TEXT, DOC_ID TEXT, NEFT_DOCUMENT TEXT, PRIMARY KEY (COMBO_ID, AGENT_CD))");
			db.execSQL("CREATE TABLE LP_COMBO_PAY_NEFT_SCRN_I (COMBO_ID TEXT NOT NULL , AGENT_CD TEXT NOT NULL , PREMIUM_PAY_MODE TEXT, RYP_AUTOPAY_FLAG TEXT, RYP_AUTOPAY_NACH_FLAG TEXT, RYP_AUTOPAY_SI_FLAG TEXT, RYP_SI_BANK_ACCOUNT_NO TEXT, RYP_SI_BANK_ACCOUNT_TYPE TEXT, RYP_SI_BANK_NAME TEXT, RYP_SI_BANK_IFSC_CODE TEXT, RYP_SI_BANK_BRANCH_NAME TEXT, RYP_SI_AMOUNT TEXT, RYP_SI_CHEQUE_IMAGE TEXT, RYP_SI_MANDATE_FORM TEXT, RYP_NACH_BANK_ACCOUNT_NO TEXT, RYP_NACH_BANK_ACCOUNT_TYPE TEXT, RYP_NACH_BANK_NAME TEXT, RYP_NACH_BANK_IFSC_CODE TEXT, RYP_NACH_BANK_BRANCH_NAME TEXT, RYP_NACH_AMOUNT TEXT, RYP_NACH_CHCK_IMAGE TEXT, RYP_NACH_MANDATE_FORM TEXT, PRIMARY KEY (COMBO_ID, AGENT_CD))");
			db.execSQL("CREATE TABLE LP_TERM_ATTACHMENT_NML (NML_ID TEXT PRIMARY KEY  NOT NULL , TERM_ID TEXT, TERM_DESC TEXT, MIN_AGE TEXT, MAX_AGE TEXT, SEGMENT TEXT, TSAR TEXT, VERSION_NO TEXT)");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_TERM_FLAG1 TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_TERM_FLAG2 TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_NML_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_NML_AMT TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN COMBO_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_FHRID TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_SIS_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_APPLICATION_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN REF_POLICY_NO TEXT");
			db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN COMBO_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN SIS_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN REF_SIS_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN APPLICATION_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN REF_APPLICATION_ID TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN POLICY_NO TEXT");
			db.execSQL("ALTER TABLE LP_MYCOMBO ADD COLUMN REF_POLICY_NO TEXT");
			db.execSQL("ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN COMBO_ID TEXT");
			db.execSQL("ALTER TABLE LP_CITY_MASTER ADD COLUMN TERM_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_CITY_MASTER ADD COLUMN TERM_NML_A TEXT");
			db.execSQL("ALTER TABLE LP_CITY_MASTER ADD COLUMN TERM_NML_B TEXT");
			db.execSQL("ALTER TABLE LP_COUNTRY_MASTER ADD COLUMN TERM_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_COUNTRY_MASTER ADD COLUMN TERM_NML_A TEXT");
			db.execSQL("ALTER TABLE LP_COUNTRY_MASTER ADD COLUMN TERM_NML_B TEXT");
			db.execSQL("ALTER TABLE LP_PLAN_MASTER ADD COLUMN TERM_ALLOWED_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_DOCUMENT_UPLOAD ADD COLUMN DOC_CAP_ID TEXT(15)");
			db.execSQL("ALTER TABLE LP_DOCUMENT_UPLOAD ADD COLUMN DOC_CAP_REF_ID TEXT(15)");
			db.execSQL("ALTER TABLE LP_APP_NOM_CONTACT_SCRN_F ADD COLUMN SAME_NOMINEE_FOR_TERM TEXT(1)");

			/*Income Multiple Grid*/
			db.execSQL("DROP TABLE LP_FHR_FINANCIAL_ELIGIBILITY_GRID");
			db.execSQL("CREATE TABLE LP_FHR_FIN_ELIGIBILITY_GRID (ELIGIBILITY_ID TEXT PRIMARY KEY  NOT NULL , CUSTOMER_MIN_AGE TEXT, CUSTOMER_MAX_AGE TEXT, PRODUCT_PGL_LIST TEXT, ANN_INCOME_MULTI TEXT, MODIFIED_DATE TEXT, ISACTIVE TEXT, VERSION_NO TEXT)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (1,18,40,'187,194,193,190,189,188,199,201,200,202,211,191,195,204,205,217,185,215,219,221',25,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (2,41,45,'187,194,193,190,189,188,199,201,200,202,211,191,195,204,205,217,185,215,219,221',20,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (3,46,50,'187,194,193,190,189,188,199,201,200,202,211,191,195,204,205,217,185,215,219,221',12,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (4,51,55,'187,194,193,190,189,188,199,201,200,202,211,191,195,204,205,217,185,215,219,221',10,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (5,56,999,'187,194,193,190,189,188,199,201,200,202,211,191,195,204,205,217,185,215,219,221',6,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (6,18,40,'224,225',25,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (7,41,45,'224,225',20,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (8,46,50,'224,225',15,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (9,51,55,'224,225',10,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (10,56,999,'224,225',6,null,'Y',1)");
			db.execSQL("INSERT INTO  LP_FHR_FIN_ELIGIBILITY_GRID VALUES (11,18,999,'222',5,null,'Y',1)");
			db.execSQL("PRAGMA user_version = 4");
		}
		if(oldVersion < 5){
			db.execSQL("ALTER TABLE LP_SIS_MAIN ADD COLUMN SAR TEXT(10)");
			db.execSQL("PRAGMA user_version = 5");
		}
		if(oldVersion < 6){
			db.execSQL("ALTER TABLE LP_APP_SOURCER_IMPSNT_SCRN_L ADD COLUMN AGENT_REL_FLAG TEXT(1) DEFAULT N ");
			db.execSQL("PRAGMA user_version = 6");
		}
		if(oldVersion < 7){
			System.out.println("LASTEST DB - 7");
			db.execSQL("CREATE TABLE 'LP_APP_FATCA_DOC' ('APPLICATION_ID' TEXT NOT NULL  DEFAULT null, 'AGENT_CD' TEXT NOT NULL  DEFAULT null, 'SEQ_NO' TEXT NOT NULL  DEFAULT null, 'COUNTRY_TAX' TEXT DEFAULT null, 'ADDRESS_TAX_BLDG' TEXT DEFAULT null, 'ADDRESS_TAX_STREET' TEXT DEFAULT null, 'ADDRESS_TAX_CITY' TEXT DEFAULT null, 'ADDRESS_TAX_STATE' TEXT DEFAULT null, 'ADDRESS_TAX_PINCODE' TEXT DEFAULT null, 'TIN' TEXT DEFAULT null, 'TIN_COUNTRY' TEXT DEFAULT null, 'TIN_COUNTRY_CODE' TEXT DEFAULT null, 'DOC_VALIDITY' TEXT DEFAULT null, 'MODIFIED_DATE' TEXT DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY ('APPLICATION_ID', 'AGENT_CD', 'SEQ_NO'))");

			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_FATHER_NAME TEXT(125)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_BIRTH_PLACE TEXT(100)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_BIRTH_COUNTRY TEXT(10)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_BIRTH_COUNTRY_CODE TEXT(10)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_OCC_TYPE TEXT(25)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_ID_TYPE TEXT(25)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FATCA_ID_NO TEXT(25)");

			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAOcc','1','Service','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAOcc','2','Business','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAOcc','3','Professional','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAOcc','4','Others','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAOcc','5','Non Categorized','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','1','Passport','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','2','Election ID Card','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','3','PAN Card','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','4','ID Card','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','5','Driving Licence','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','6','UIDAI Letter','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','7','NREGA Job Card','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','8','Others','1',null)");
			db.execSQL("INSERT INTO LP_LOOKUP_MASTER VALUES ('FATCAId','9','Not Categorized','1',null)");
			db.execSQL("PRAGMA user_version = 7");

		}
		if(oldVersion < 8){
			System.out.println("LASTEST DB - 8");
			db.execSQL("ALTER TABLE LP_APP_SOURCER_IMPSNT_SCRN_L ADD COLUMN 'DIGITAL_PSC_FLAG' TEXT(1)");
			db.execSQL("ALTER TABLE LP_APP_SOURCER_IMPSNT_SCRN_L ADD COLUMN 'ISDIGITAL_PSC_DONE' TEXT(1)");
			db.execSQL("ALTER TABLE LP_USERINFO ADD COLUMN 'PSC_SESSION_ID' TEXT(15)");
			db.execSQL("PRAGMA user_version = 8");
		}
		if(oldVersion < 9){
			System.out.println("LASTEST DB - 9");
			db.execSQL("CREATE TABLE LP_TERM_VALIDATION (AGENT_CD TEXT,LEAD_ID TEXT,FHR_ID TEXT,SIS_ID TEXT,POLICY_NO TEXT,GROUP_EMPLOYEE TEXT,NATIONALITY TEXT,RESIDENT_STATUS TEXT,RESIDENT_STATUS_CODE TEXT,CURR_RESIDENT_COUNTRY TEXT,CURR_RESIDENT_COUNTRY_CODE TEXT,CURR_STATE TEXT,CURR_STATE_CODE TEXT,CURR_CITY TEXT,CURR_CITY_CODE TEXT,EDUCATION_QUALIFICATION TEXT,EDUCATION_QUALIFICATION_CODE TEXT,OCCUPATION_CLASS TEXT,OCCUPATION_CLASS_NBFE_CODE TEXT,INCOME_PROOF TEXT,INCOME_PROOF_DOC_ID TEXT,S_FRM16_YEAR1 TEXT,S_FRM16_SAL1 TEXT,S_FRM16_YEAR2 TEXT,S_FRM16_SAL2 TEXT,S_FRM16_YEAR3 TEXT,S_FRM16_SAL3 TEXT,S_EMP_ISSDT TEXT,S_EMP_SAL TEXT,S_BNK_MONTH_YEAR1 TEXT,S_BNK_SAL1 TEXT,S_BNK_MONTH_YEAR2 TEXT,S_BNK_SAL2 TEXT,S_BNK_MONTH_YEAR3 TEXT,S_BNK_SAL3 TEXT,S_BNK_MONTH_YEAR4 TEXT,S_BNK_SAL4 TEXT,S_BNK_MONTH_YEAR5 TEXT,S_BNK_SAL5 TEXT,S_BNK_MONTH_YEAR6 TEXT,S_BNK_SAL6 TEXT,S_SAL_MONTH_YEAR1 TEXT,S_SAL_SAL1 TEXT,S_SAL_MONTH_YEAR2 TEXT,S_SAL_SAL2 TEXT,S_SAL_MONTH_YEAR3 TEXT,S_SAL_SAL3 TEXT,S_COI_YEAR1 TEXT,S_COI_SAL1 TEXT,S_COI_YEAR2 TEXT,S_COI_SAL2 TEXT,S_COI_YEAR3 TEXT,S_COI_SAL3 TEXT,SE_FRM16_YEAR1 TEXT,SE_FRM16_YEAR2 TEXT,SE_FRM16_YEAR3 TEXT,SE_FRM16_SAL1 TEXT,SE_FRM16_SAL2 TEXT,SE_FRM16_SAL3 TEXT,SE_COI_YEAR1 TEXT,SE_COI_PROFIT1 TEXT,SE_COI_SAL1 TEXT,SE_COI_YEAR2 TEXT,SE_COI_PROFIT2 TEXT,SE_COI_SAL2 TEXT,SE_COI_YEAR3 TEXT,SE_COI_PROFIT3 TEXT,SE_COI_SAL3 TEXT,SE_OTH_DOC TEXT,SE_OTH_PROFIT TEXT,UE_HOUSE_PROP TEXT,UE_RENT TEXT,UE_GAIN_DIV TEXT,UE_AGRI TEXT,UE_IOTH TEXT,UE_GIFT TEXT,UE_OTH TEXT,SAR TEXT,ELIGIBILITY_FLAG TEXT, ELIGIBILITY_CATEGORY TEXT, ELIGIBILITY_REASON TEXT, ELIGIBILITY_JUSTIFICATION TEXT, APPROVAL_FLAG TEXT, APPROVAL_DATE TEXT, APPROVAL_ID TEXT, APPROVAL_REMARKS TEXT, ISSYNCED TEXT, PRIMARY KEY(AGENT_CD, LEAD_ID, FHR_ID, SIS_ID))");
			db.execSQL("ALTER TABLE LP_DOC_PROOF_MASTER ADD COLUMN TERM_FLAG TEXT DEFAULT 'N'");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN MSAR TEXT(10) DEFAULT 0 ");
			db.execSQL("PRAGMA user_version = 9");
		}
		if(oldVersion < 10){
			try {
				db.execSQL("CREATE TABLE LP_FHR_PRODUCT_REC_GRID (CUSTOMER_NEED TEXT(50), CUSTOMER_MIN_AGE TEXT(5), CUSTOMER_MAX_AGE TEXT (5),MIN_TIME_HORIZON TEXT(5),MAX_TIME_HORIZON TEXT(5),SUGGESTED_PRODUCT1_UINNO TEXT(30),SUGGESTED_PRODUCT2_UINNO TEXT(30),SUGGESTED_PRODUCT3_UINNO TEXT(30),MIN_PPT TEXT(5),MAX_PPT TEXT(5),PAYMENT_TYPE TEXT(10),PLAN_TYPE TEXT(20),REINVESTMENT_REQUIRED TEXT(1),REINVESTMENT_MIN_AGE TEXT(5),REINVESTMENT_PRODUCT_UINNO TEXT(20),BUSINESS_TYPE_ID TEXT(50),ISACTIVE TEXT(1),VERSION_NO TEXT, PRIMARY KEY (CUSTOMER_NEED,CUSTOMER_MIN_AGE,CUSTOMER_MAX_AGE,MIN_TIME_HORIZON,MAX_TIME_HORIZON,BUSINESS_TYPE_ID))");
				db.execSQL("ALTER TABLE LP_MYLEAD ADD COLUMN Other_CityName TEXT(20)");
				db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN Insured_ProposerAddress_Flag TEXT(1)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_EDU_SCRN_B ADD COLUMN CLIENT_BIRTH_DATE TEXT (8)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_EDU_SCRN_B ADD COLUMN INFLATION_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_EDU_SCRN_B ADD COLUMN PRESENT_SAVINGS TEXT (15, 2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_EDU_SCRN_B ADD COLUMN ROI_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_MRG_SCRN_C ADD COLUMN CLIENT_BIRTH_DATE TEXT (8)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_MRG_SCRN_C ADD COLUMN INFLATION_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_MRG_SCRN_C ADD COLUMN PRESENT_SAVINGS TEXT (15, 2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_CHILD_MRG_SCRN_C ADD COLUMN ROI_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN ANN_EXP_TOTAL TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN FUTURE_COST TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN FUTURE_REQIRED_CORPUS TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN FUTURE_SAVINGS_VALUE TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN GOAL_GAP_AMOUNT TEXT (15)");
				db.execSQL("ALTER  TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN GOAL_TARGET_DUARTION TEXT (5)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN INFLATION_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN ROI_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN SUM_ASSURED TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN SUGEST_PLAN1_URNNO");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN SUGEST_PLAN2_URNNO");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN SUGEST_PLAN3_URNNO");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_LIVING_STD_SCRN_F ADD COLUMN INFLATION_FLAG TEXT (1)  DEFAULT Y");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_RETIREMENT_SCRN_E ADD COLUMN INFLATION_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_RETIREMENT_SCRN_E ADD COLUMN PRESENT_SAVINGS TEXT (15, 2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_RETIREMENT_SCRN_E ADD COLUMN ROI_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_WEALTH_SCRN_D ADD COLUMN INFLATION_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_WEALTH_SCRN_D ADD COLUMN PRESENT_SAVINGS TEXT (15, 2)");
				db.execSQL("ALTER TABLE LP_FHR_GOAL_WEALTH_SCRN_D ADD COLUMN ROI_RATE TEXT (2)");
				db.execSQL("ALTER TABLE LP_FHR_PERSONAL_INF_SCRN_A ADD COLUMN LIABILITY_EMI TEXT (15)");
				db.execSQL("ALTER TABLE LP_FHR_PERSONAL_INF_SCRN_A ADD COLUMN RESIDENTIAL_STATUS TEXT (30)");
				db.execSQL("ALTER TABLE LP_FHR_PERSONAL_INF_SCRN_A ADD COLUMN SMOKER TEXT (1)  DEFAULT N");
				db.execSQL("CREATE TABLE LP_FHR_GOAL_CHILD_WEALTH_SCRN_G ( AGENT_CD TEXT, CLIENT_BIRTH_DATE TEXT(8),CLIENT_FIRST_NAME   TEXT(20),CLIENT_LAST_NAME    TEXT(20),CLIENT_MIDDLE_NAME  TEXT(20),FHR_ID  TEXT,FUTURE_COST TEXT,FUTURE_SAVINGS_VALUE    TEXT,GOAL_CURRENT_AGE    TEXT,GOAL_DESC   TEXT(50),GOAL_GAP_AMOUNT TEXT,GOAL_TARGET_AGE TEXT,INFLATION_RATE  TEXT(2),PRESENT_COST    TEXT(15,2),PRESENT_SAVINGS TEXT(15,2),RELATION    TEXT,ROI_RATE    TEXT(2),SUGEST_PLAN1_URNNO  TEXT,SUGEST_PLAN2_URNNO  TEXT,	SUGEST_PLAN3_URNNO  TEXT, RELATION_SEQ_NO TEXT, RELATION_CODE TEXT)");
			}
			catch(SQLiteException ex){
				System.out.println("Exception: " + ex.getMessage());
				ex.printStackTrace();
			}
			finally {
				db.execSQL("PRAGMA user_version = 10");
			}
			
		}
		if(oldVersion < 11){
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'IBL_JOINING_DATE' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'OTHER_CITY' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'CITY_TERM_FLAG' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'COUNTRY_TERM_FLAG' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_FLAG_CITY' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_FLAG_COUNTRY' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_FLAG_OCCCLASS' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_FLAG_EDUCATION' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_FLAG_INCOME' TEXT");
			//NEW FIELDS ADDED
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'AVG_INCOME' TEXT");
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN 'ELIGIBILITY_VALUE' TEXT");


			/*Occupation db changes*/
			db.execSQL("CREATE TABLE LP_APP_DESIGNATION_LOOKUP (APP_SNO TEXT(10) PRIMARY KEY  NOT NULL ,DESIGNATION TEXT(50) NOT NULL , MPDOC_CODE TEXT(5),MPPROOF_CODE TEXT(5),MODIFY_DATE DATETIME, ISACTIVE TEXT(1),VERSION_NO TEXT(10),OCCU_CLASS_PRINT TEXT(50),NBFE_CODE TEXT(5) NOT NULL ,ISACTIVE_PAYOR TEXT(1))");
			db.execSQL("CREATE TABLE LP_APP_NATURE_OF_WORK (APP_SNO TEXT(10) PRIMARY KEY  NOT NULL ,APP_INDUSTRY TEXT(50) NOT NULL , APP_NATURE_OF_WORK TEXT(50) NOT NULL ,OCCUPATION_CODE TEXT(5),MPDOC_CODE TEXT(5),MPPROOF_CODE TEXT(5),FORMID VARCHAR(20),APP_PROMPT VARCHAR(100),VERSION_NO TEXT(10))");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN DESIGNATION TEXT(50)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN DESIGNATION_OTHER TEXT(50)");
			db.execSQL("ALTER TABLE LP_APP_NATURE_OF_WORK ADD COLUMN ISACTIVE TEXT(10)");
			db.execSQL("ALTER TABLE LP_APP_SUB_QUESTION_LOOKUP ADD COLUMN LOOKUP_TYPE_PRINT TEXT(10)");
			db.execSQL("PRAGMA user_version = 11");
		}
		if(oldVersion < 12){
			//NEW COLUMN ADDED
			db.execSQL("ALTER TABLE LP_POLICY_ASSIGNMENT ADD COLUMN 'ASSIGN_TO' TEXT(600)");
			db.execSQL("PRAGMA user_version = 12");
		}
		if(oldVersion < 13){
			db.execSQL("ALTER TABLE LP_TERM_VALIDATION ADD COLUMN ELIGIBILITY_FLAG_OCC TEXT");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN OCCU_TERM_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_APP_NATURE_OF_WORK ADD COLUMN TERM_FLAG TEXT");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FSAR TEXT(10) DEFAULT 0");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN FINPREM TEXT(15) DEFAULT 0");
			db.execSQL("PRAGMA user_version = 13");
		}
		if(oldVersion < 14){
			db.execSQL("CREATE  TABLE LP_AML_GRID (MIN_PREMIUM TEXT NOT NULL , MAX_PREMIUM TEXT NOT NULL , RISK_TYPE TEXT NOT NULL , RESI_STATUS_CODE TEXT NOT NULL , BANK_STMT_REQ TEXT, PROOF_OF_SOURCE_OF_INCOME_REQ TEXT, ISACTIVE TEXT, VERSION_NO TEXT, PRIMARY KEY (MIN_PREMIUM, MAX_PREMIUM, RISK_TYPE, RESI_STATUS_CODE))");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN PROOF_OF_SOURCE_OF_INCOME TEXT(50)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN PROOF_OF_SOURCE_OF_INCOME_DOC_ID TEXT(5)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN AML_BANK_STATEMENT_FLAG TEXT(1)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN AML_BANK_STATEMENT TEXT(5)");
			db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN AML_BANK_STATEMENT_DOC_ID TEXT(5)");
			db.execSQL("PRAGMA user_version = 14");
		}
		if(oldVersion < 15){
			try{
				db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN OCCU_QUES_SNO TEXT(10)");
				db.execSQL("CREATE TABLE LP_APP_OCCU_QUESTIONNAIRE (SNO TEXT(10) PRIMARY KEY  NOT NULL , APP_SNO TEXT(10) NOT NULL , OCCUPATION_CODE TEXT(5), MPDOC_CODE TEXT(5), MPPROOF_CODE TEXT(5), FORMID TEXT(20), ISACTIVE TEXT(10), VERSION_NO TEXT(10), MODIFY_DATE DATETIME, QUESTIONNAIRE_DESC TEXT(100))");
				//db.execSQL("ALTER TABLE LP_APP_OCCU_QUESTIONNAIRE ADD COLUMN QUESTIONNAIRE_DESC TEXT(100)");
			}catch(Exception ex) {
				System.out.println("Exception in deleteQuery: " + ex.getMessage());
			}finally {
				db.execSQL("PRAGMA user_version = 15");
			}
		}
		if(oldVersion < 16) {
			try {
				System.out.println("LASTEST DB - 16");
				db.execSQL("CREATE TABLE LP_LEAD_EKYC_DTLS (EKYC_ID TEXT(15) , LEAD_ID TEXT(15), CUST_TYPE TEXT(20), PUR_FOR_CD TEXT(3), BUY_FOR_CD TEXT(3), AGENT_CD TEXT(10), EKYC_FLAG TEXT(1), NAME TEXT(60), GENDER TEXT(5), DOB TEXT(20), EMAIL_ID TEXT(20), BUILDING TEXT(30), STREET TEXT(30), AREA TEXT(30), CITY TEXT(30), DISTRICT TEXT(30), STATE TEXT(20), PIN TEXT(15), POSTOFFICE TEXT(15), EKYC_TRAN_ID TEXT(15), CARE_OF TEXT(10), PHONE_NO TEXT(20), NATIONALITY TEXT(30), CONSUMER_AADHAR_NO TEXT(12), RESPONSE_CODE TEXT(10), RESPONSE_MSG TEXT(20), AUTH_CODE_UIDAI TEXT(10), AUTH_MSG TEXT(20), TIMESTAMP TEXT(20), ERROR_CD_UIDAI TEXT(10), ERROR_MSG_UIDAI TEXT(20), EKYC_DONE_DATE TEXT(20), SUBDISTRICT TEXT(30), LANDMARK TEXT(30),PRIMARY KEY(EKYC_ID,LEAD_ID,AGENT_CD))");
				db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN INS_EKYC_ID TEXT(15)");
				db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN INS_EKYC_FLAG TEXT(1)");
				db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN PR_EKYC_ID TEXT(15)");
				db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN PR_EKYC_FLAG TEXT(1)");
				db.execSQL("ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN BUY_FOR TEXT(3)");
				db.execSQL("ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN EKYC_ID TEXT(15)");
				db.execSQL("PRAGMA user_version = 16");
			}catch(Exception ex) {
				System.out.println("Exception in LASTEST DB - 16 query: " + ex.getMessage());
			}finally {
				db.execSQL("PRAGMA user_version = 16");
			}
		}
		if(oldVersion < 17){
			System.out.println("latest DB 17");
			db.execSQL("CREATE TABLE 'LP_DEDUPE_DATA' ('APPLICATION_ID' TEXT(15) NOT NULL , 'POLICY_NO' TEXT(10), 'AGENT_CD' TEXT(10) NOT NULL, 'RESP' TEXT(5) , 'RESULT_APPDATA' TEXT(300), 'RESULT_MEDSAR' TEXT(15), 'RESULT_FINSAR' TEXT(50), 'RESULT_TPREM' TEXT(50), PRIMARY KEY ('APPLICATION_ID', 'AGENT_CD'))");
			db.execSQL("PRAGMA user_version = 17");
		}
		else{
			try{

				db.execSQL("delete FROM LP_DOCUMENT_UPLOAD where CAST(DOC_PAGE_NO as NUMERIC) > 5");

			}catch(Exception ex){
				System.out.println("Exception in deleteQuery: " + ex.getMessage());
			}
			System.out.println("LASTEST DB");
		}
	}
}
