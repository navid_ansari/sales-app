package com.talic.salesapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.talic.plugins.AppConstants;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DBHelper extends SQLiteOpenHelper {

	/**
	 *
	 * This class is used to copy the database from assets folder to the databases folder of the app memory on the device.
	 *
	 **/

	public SQLiteDatabase database = null;
	public File databaseFile;
	public File sisDatabaseFile;
	public String databasePath = "";
	public String sisDatabasePath = "";
	public static SQLiteDatabase sisDbConn = null;
	private static final String TAG = "SA";
	Context mContext;

	public DBHelper(Context paramContext) {
		super(paramContext, AppConstants.SA_SIS_DB_NAME, null, 1);
		this.mContext = paramContext;
	}

	public void copyDatabase(){
		Log.d(TAG, "package name:" + mContext.getPackageName());
		databasePath = AppConstants.SA_MAIN_DB_PATH;
		sisDatabasePath = AppConstants.SA_SIS_DB_PATH;
		File direct = new File(mContext.getDatabasePath(AppConstants.SA_MAIN_DB_NAME).getParent());
		if(!direct.exists())
			direct.mkdir();
		databaseFile = new File(databasePath);
		sisDatabaseFile = new File(sisDatabasePath);
		try {
			if (!databaseFile.exists()) {
				deployDataBase(AppConstants.SA_MAIN_DB_NAME, databasePath);
			}
			deployDataBase(AppConstants.SA_SIS_DB_NAME, sisDatabasePath);
			encrypt(mContext, AppConstants.SA_MAIN_DB_NAME, "life");
			encrypt(mContext, AppConstants.SA_SIS_DB_NAME, "life");
		}
		catch (IOException ex) {
			Log.d(TAG,"Exception in copyDatabase(): " + ex.getMessage());
			ex.printStackTrace();
		}
		if(sisDbConn==null) {
			Log.d(TAG,"sisDbConn is null");
			sisDbConn = SQLiteDatabase.openDatabase(sisDatabasePath,  "life", null, SQLiteDatabase.OPEN_READWRITE);
			Log.d(TAG,"sisDbConn:: " + sisDbConn.getPath());
		}
		else{
			Log.d(TAG,"sisDbConn:: " + sisDbConn.getPath());
		}
	}

	private void deployDataBase(String dbNAme, String dbPath) throws IOException {
		InputStream localInputStream = this.mContext.getAssets().open("www/databases/" + dbNAme);
		FileOutputStream localFileOutputStream = new FileOutputStream(dbPath);
		byte[] arrayOfByte = new byte[1024];
		while (true) {
			int i = localInputStream.read(arrayOfByte);
			if (i <= 0) {
				localFileOutputStream.flush();
				localFileOutputStream.close();
				localInputStream.close();
				return;
			}
			localFileOutputStream.write(arrayOfByte, 0, i);
		}
	}

	public static void encrypt(Context ctxt, String dbName, String passphrase) {
		try {
			File originalFile = ctxt.getDatabasePath(dbName);
			Log.d(TAG, "Encryption called: " + dbName);
			Log.d(TAG, "DB FILE PATH: " + originalFile.getAbsolutePath());
			if (originalFile.exists()) {
				Log.d(TAG, "DB FILE EXIST: true");
				File newFile = File.createTempFile("sqlcipherutils", "tmp", ctxt.getCacheDir());
				SharedPreferences saSharedPreferences = AppConstants.SA_CLASS.getSharedPreferences("SA_SHARED_PREFERENCES", AppConstants.SA_CLASS.MODE_PRIVATE);
				SharedPreferences.Editor saSharedPreferencesEdit = saSharedPreferences.edit();
				SQLiteDatabase db;
				if (!(saSharedPreferences.contains("EncryptedDb") && saSharedPreferences.getBoolean("EncryptedDb", false) && (!AppConstants.SA_SIS_DB_NAME.equals(dbName)))) {
					Log.d(TAG, "originalFile.getAbsolutePath(): " + originalFile.getAbsolutePath());
					db = SQLiteDatabase.openDatabase(originalFile.getAbsolutePath(), "", null, SQLiteDatabase.OPEN_READWRITE);
					db.rawExecSQL(String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", newFile.getAbsolutePath(), passphrase));
					db.rawExecSQL("SELECT sqlcipher_export('encrypted')");
					db.rawExecSQL("DETACH DATABASE encrypted;");
					int version = db.getVersion();
					db.close();
					db = SQLiteDatabase.openDatabase(newFile.getAbsolutePath(), passphrase, null, SQLiteDatabase.OPEN_READWRITE);
					db.setVersion(version);
					db.close();
					originalFile.delete();
					newFile.renameTo(originalFile);
					if ((AppConstants.SA_MAIN_DB_NAME).equals(dbName)) {
						saSharedPreferencesEdit.putBoolean("EncryptedDb", true);
						saSharedPreferencesEdit.commit();
					}
				}
			}
		}catch (IOException ex){
			Log.d(TAG,"Exception in encrypt: " + ex.getMessage());
		}
	}


	public static void decrypt(String dbName, String passphrase) {
		File originalFile = new File(AppConstants.SA_MAIN_DB_PATH);
		Log.d(TAG, "Decryption called: " + dbName);
		Log.d(TAG, "DB FILE PATH: " + originalFile.getAbsolutePath());
		if (originalFile.exists()) {
			Log.d(TAG, "DB FILE EXIST: true");
			File newFile = new File(AppConstants.SA_APP_CONTEXT.getExternalFilesDir(null) + "/" + AppConstants.SA_MAIN_DB_NAME);
			try {
				newFile.createNewFile();
				SQLiteDatabase db;
				Log.d(TAG, "originalFile.getAbsolutePath(): " + originalFile.getAbsolutePath());
				db = SQLiteDatabase.openDatabase(originalFile.getAbsolutePath(), passphrase, null, SQLiteDatabase.OPEN_READWRITE);
				db.rawExecSQL(String.format("ATTACH DATABASE '%s' AS plaintext KEY '';", newFile.getAbsolutePath()));
				db.rawExecSQL("SELECT sqlcipher_export('plaintext')");
				db.rawExecSQL("DETACH DATABASE plaintext;");
				int version = db.getVersion();
				db.close();
				db = SQLiteDatabase.openDatabase(newFile.getAbsolutePath(), "", null, SQLiteDatabase.OPEN_READWRITE);
				db.setVersion(version);
				db.close();
			}
			catch (IOException ex){
				Log.d(TAG, "Exception in decrypt: " + ex.getMessage());
				ex.printStackTrace();
			}
		}
	}


	public static SQLiteDatabase getDatabaseConnection() throws SQLException {
		return sisDbConn;
	}

	@Override
	public synchronized void close() {
		if (database != null)
			database.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
}
