package com.talic.salesapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.talic.plugins.AppConstants;
import com.talic.plugins.AppUtils;
import com.talic.plugins.EncryptDecryptFile;
import io.fabric.sdk.android.Fabric;
import net.sqlcipher.database.SQLiteDatabase;
import org.apache.cordova.CordovaActivity;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import xmlwise.Plist;
import xmlwise.XmlParseException;

public class SalesApp extends CordovaActivity {
	private static final String TAG = "SA";
	DownloadManager downloadManager;
	String URL="";
	int versionCode = 0;
	long downloadReference;
	String version1="";
	Activity gsActivity = this;
	String pListURL = AppConstants.DOMAIN_URL + "GoodConnect.plist";
	String APK_NAME = "GoodConnect.apk"; //todo: change according to the application's name
	private SQLiteDatabase dbConn = null;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		// Set by <content src="index.html" /> in config.xml
		AppConstants.FILE_ABS_PATH = this.getFilesDir() + "/" + AppConstants.ROOT_FOLDER;
		AppConstants.SA_APP_CONTEXT = this.getApplicationContext();
		AppConstants.SA_CLASS = this;
		AppConstants.EN_DE_OBJ = new EncryptDecryptFile();
		Log.d(TAG, "AppConstants.FILE_ABS_PATH: " + AppConstants.FILE_ABS_PATH);
		handleDirsAndFiles();
		AppConstants.SA_MAIN_DB_PATH = this.getDatabasePath(AppConstants.SA_MAIN_DB_NAME).getPath();
		AppConstants.SA_SIS_DB_PATH = this.getDatabasePath(AppConstants.SA_SIS_DB_NAME).getPath();
		Log.d(TAG, "AppConstants.SA_MAIN_DB_PATH: " + AppConstants.SA_MAIN_DB_PATH);
		Log.d(TAG, "AppConstants.SA_SIS_DB_PATH: " + AppConstants.SA_SIS_DB_PATH);
		SQLiteDatabase.loadLibs(this.getApplicationContext());
		DBHelper dbh = new DBHelper(this.getApplicationContext());
		dbh.copyDatabase();
		updateCheck();
		dbConn = SQLiteDatabase.openDatabase(AppConstants.SA_MAIN_DB_PATH, "life", null, SQLiteDatabase.OPEN_READWRITE);
		OpenHelper o = new OpenHelper(this);
		o.onUpgrade(dbConn, dbConn.getVersion(), AppConstants.SA_DB_VER);
		loadUrl(launchUrl);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void updateCheck(){
		try {
			if (android.os.Build.VERSION.SDK_INT > 9) {
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
			}
			PackageInfo pInfo = null;
			try {
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			}
			catch (PackageManager.NameNotFoundException e) {
				e.printStackTrace();
			}
			String app_version = "1.0.0";
			if(pInfo!=null) {
				//get the app version Name for display
				app_version = pInfo.versionName;
				//get the app version Code for checking
				versionCode = pInfo.versionCode;
			}
			StringBuilder fileData = new StringBuilder(1024);
			URL sourceUrl = new URL(pListURL);
			BufferedReader in = new BufferedReader(new InputStreamReader(sourceUrl.openStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null){
				fileData.append(inputLine);
			}
			in.close();
			HashMap<?, ?> hashMap = (HashMap<?, ?>) Plist.objectFromXml(fileData.toString());
			ArrayList<?> items = (ArrayList<?>) hashMap.get("items");
			ArrayList<?> assets=(ArrayList<?>)((HashMap<?, ?>) items.get(0)).get("assets");
			HashMap<?,?> metadata=(HashMap<?,?>)((HashMap<?, ?>) items.get(0)).get("metadata");
			URL=(String)((HashMap<?, ?>) assets.get(0)).get("url");
			version1=(String)metadata.get("version");
			version1=version1.replace(".","");
			int server_version=Integer.parseInt(version1);
			Log.d(TAG, "showing :: " + (server_version > Integer.parseInt(app_version.replace(".", ""))));
			if(server_version > Integer.parseInt(app_version.replace(".",""))){
				//oh yeah we do need an upgrade, let the user know send an alert message
				AlertDialog.Builder builder = new AlertDialog.Builder(gsActivity); // gsActivity = main activity
				builder.setMessage("A newer version of this application is available, click OK to upgrade now.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					//if the user agrees to upgrade
					public void onClick(DialogInterface dialog, int id) {
						//start downloading the file using the download manager
						downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
						Uri Download_Uri = Uri.parse(URL);
						DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
						ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
						if (manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
							Log.d(TAG, "showing wifi");
							request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
						}
						else if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
							Log.d(TAG, "showing mobile");
							request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
						}
						else {
							Log.d(TAG, "showing toast");
							Toast.makeText(gsActivity.getApplicationContext(), "An update to this app is available, please connect to a network", Toast.LENGTH_LONG).show();
						}
						request.setAllowedOverRoaming(false);
						request.setTitle("Good Connect Update"); //todo: App name to replace Sales App
						request.setDestinationInExternalFilesDir(gsActivity, Environment.DIRECTORY_DOWNLOADS, APK_NAME); // gsActivity = main activity
						downloadReference = downloadManager.enqueue(request);
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        dialog.dismiss();
				    }
				});
				//show the alert message
				builder.create().show();
			}
			IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
			registerReceiver(downloadReceiver, filter);
		} catch (MalformedURLException ex){
			Log.e(TAG, "Exception in updateCheck(): " + ex.getMessage());
			ex.printStackTrace();
		} catch (IOException ex){
			Log.e(TAG, "Exception in updateCheck(): " + ex.getMessage());
			ex.printStackTrace();
		} catch (XmlParseException ex){
			Log.e(TAG, "Exception in updateCheck(): " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public void onReceive(Context context, Intent intent) {
			//check if the broadcast message is for our Enqueued download
			long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			if(downloadReference == referenceId){
				DownloadManager.Query query = new DownloadManager.Query();
				query.setFilterById(referenceId);
				DownloadManager dm = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
				Cursor downloadResult = dm.query(query);

				if (downloadResult.moveToFirst()) {
					int localFileNameId = downloadResult.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
					String downloadPathFile = downloadResult.getString(localFileNameId);
					Log.d(TAG, "Downloading of the new app version complete");
					//start the installation of the latest version
					Intent installIntent = new Intent(Intent.ACTION_VIEW);
					installIntent.setDataAndType(Uri.fromFile(new File(downloadPathFile)),
					"application/vnd.android.package-archive");
					installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(installIntent);
				}
			}
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			this.unregisterReceiver(downloadReceiver);
		}catch (Exception ex){
			Log.d(TAG, "Exception in onDestroy: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public void handleDirsAndFiles(){
		try {
			SharedPreferences saSharedPreferences = getSharedPreferences("SA_SHARED_PREFERENCES", MODE_PRIVATE);
			SharedPreferences.Editor saSharedPreferencesEdit = saSharedPreferences.edit();
			AppUtils AppUtils = new AppUtils();
			String path;
			String[] fList = this.getAssets().list("ToClient/APPOUTPUT");
			Log.d(TAG,"filelist length: " + fList.length);
			path = "/.Sales App/ToClient/APPOUTPUT/";
			if(fList.length > 0)
				AppUtils.copyFiles(this.getAssets(),"ToClient/APPOUTPUT/",path,fList);
			if(saSharedPreferences.getBoolean("ftlCopy",true)){
				/*
				*
				* Will copy all the files in specified folders as it is FTL.
				* Mention the folder in getAssets method, from asset folder where the files are kept.
				* Call the method again for every different folder.
				*
				**/
				Log.d(TAG,"this.getExternalFilesDir(null): " + AppConstants.FILE_ABS_PATH);
				File fileOrDirectory = new File(AppConstants.FILE_ABS_PATH);
				AppUtils.deleteGSInternalStorageFolder(fileOrDirectory);

				String upStreamDir = "/FromClient";
				String downStreamDir = "/ToClient";

				String[] dirList = {upStreamDir,downStreamDir,upStreamDir + "/APP",upStreamDir + "/FHR",upStreamDir + "/FF",upStreamDir + "/FORM",upStreamDir + "/SIS",upStreamDir + "/COMBO", upStreamDir + "/EKYC",upStreamDir + "/APP/HTML",upStreamDir + "/APP/PDF",upStreamDir + "/APP/SIGNATURE", upStreamDir + "/APP/IMAGES",upStreamDir + "/FHR/HTML",upStreamDir + "/FHR/PDF",upStreamDir + "/FHR/SIGNATURE", upStreamDir + "/FF/HTML",upStreamDir + "/FF/PDF",upStreamDir + "/FF/SIGNATURE", upStreamDir + "/SIS/HTML",upStreamDir + "/SIS/PDF",upStreamDir + "/SIS/SIGNATURE",  upStreamDir + "/APP/IMAGES/PHOTOS", upStreamDir + "/EKYC/HTML",downStreamDir + "/Products",downStreamDir + "/Profile",downStreamDir + "/MyResources",downStreamDir + "/CONF",downStreamDir + "/APPOUTPUT"};
				File files = new File(AppConstants.FILE_ABS_PATH);
				if(!files.isDirectory())
					files.mkdir();
				AppUtils.createGSInternalStorageFolder(dirList,0);

				saSharedPreferencesEdit.putBoolean("ftlCopy", false);
				saSharedPreferencesEdit.apply();
			}
			else{
				String upStreamDir = "/FromClient";
				String downStreamDir = "/ToClient";

				String[] dirList = {upStreamDir,downStreamDir,upStreamDir + "/APP",upStreamDir + "/FHR",upStreamDir + "/FF",upStreamDir + "/FORM",upStreamDir + "/SIS",upStreamDir + "/COMBO", upStreamDir + "/EKYC",upStreamDir + "/APP/HTML",upStreamDir + "/APP/PDF",upStreamDir + "/APP/SIGNATURE",upStreamDir + "/FHR/HTML",upStreamDir + "/FHR/PDF",upStreamDir + "/FHR/SIGNATURE", upStreamDir + "/FF/HTML",upStreamDir + "/FF/PDF",upStreamDir + "/FF/SIGNATURE", upStreamDir + "/SIS/HTML",upStreamDir + "/SIS/PDF",upStreamDir + "/SIS/SIGNATURE", upStreamDir + "/EKYC/HTML",downStreamDir + "/Products",downStreamDir + "/Profile",downStreamDir + "/MyResources",downStreamDir + "/CONF",downStreamDir + "/APPOUTPUT"};
				File files = new File(AppConstants.FILE_ABS_PATH);
				if(!files.isDirectory())
					files.mkdir();
				AppUtils.createGSInternalStorageFolder(dirList,0);
			}
		}
		catch (IOException ex){
			System.out.println("Exception: " + ex.getMessage());
		}
	}
}
