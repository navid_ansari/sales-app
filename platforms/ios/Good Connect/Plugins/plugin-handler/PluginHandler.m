//
//  PluginHandler.m
//
//
//  Created by admin on 02/09/16.
//
//

#import "PluginHandler.h"
#import "GalleryVC.h"
#import "BillDeskPG.h"

@implementation PluginHandler
@synthesize latestcommand,hasPendingOperation,arrayOfProfDetails;

//call PSC
-(void) CallPSC:(CDVInvokedUrlCommand *)command{
	__block CDVPluginResult *result;
	NSLog(@"the value is %@",command.arguments);
	NSString *decodeString = [command.arguments objectAtIndex:0];

	NSData *encodeData = [decodeString dataUsingEncoding:NSUTF8StringEncoding];
	NSString *base64String = [encodeData base64EncodedStringWithOptions:0];

	NSString *customURL = [NSString stringWithFormat:@"GoodCheck://?%@",base64String];

	if ([[UIApplication sharedApplication]
		 canOpenURL:[NSURL URLWithString:customURL]])
	{
		//if for version 10 and above
		//else for version 10 below..
		if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL] options:@{}
									 completionHandler:^(BOOL success) {
										 NSLog(@"Open %d",success);
										 if(success != 1){
											 result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
											 [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
										 }
									 }];
		} else {
			BOOL success = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
			NSLog(@"Open %d",success);
			if(success != 1){
				result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
				[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
			}
		}
	}
	else
	{
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
		[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
	}

	[[NSUserDefaults standardUserDefaults] setObject:command.callbackId forKey:@"CALLBACK"];
	[[NSUserDefaults standardUserDefaults] synchronize];


}

//generating sis..
-(void) startSIS:(CDVInvokedUrlCommand *)command{
	self.latestcommand = command;
	NSDictionary *reqSis = [command.arguments objectAtIndex:0];
	NSLog(@"the request in GoodConnect is %@",reqSis);
	NSLog(@"NATIVE FUNCTION CALLED SIS");

	ProcessDataForSIS *sisStart = [[ProcessDataForSIS alloc] init];
	NSMutableDictionary *sisResp =  [sisStart processDataForGeneratingSIS:reqSis];
	int respCode = [[sisResp objectForKey:@"code"] intValue];
	CDVPluginResult *result;
	if(respCode == 0){
		//error
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:sisResp];
	}else{
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:sisResp];
	}

	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)saveEncryptedFile:(CDVInvokedUrlCommand *)command{
	//	NSArray *fileContent = [command.arguments objectAtIndex:0];
	NSArray *fileContent = command.arguments;

	//first : sisid, second : type of file, third : filepath, fourth : filecontent, fifth : isbase64
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	CDVPluginResult *result;
	BOOL flag = [htmlToPdf generatePDF:fileContent];
	if(flag){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail"];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//save  the html file to give location..
-(void)saveToClientEncryptedFile:(CDVInvokedUrlCommand *)command{
	NSArray *fileContent = command.arguments;

	//first : sisid, second : type of file, third : filepath, fourth : filecontent, fifth : isbase64
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	CDVPluginResult *result;
	BOOL flag = [htmlToPdf generatePDF:fileContent];
	if(flag){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail"];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//in android it is used to decrypt the file.
//in ios no encryption..so directly return the file..
- (void)getDecryptedFileName:(CDVInvokedUrlCommand*)command {

	NSString *docName = [command.arguments objectAtIndex:0];
	NSString *docPath = [command.arguments objectAtIndex:1];
	NSArray *fileWithPath = [NSArray arrayWithObjects:docPath, docName, nil]; // [docPath, docName];

	CDVPluginResult *result;
	if(docName){
		//		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:docName];
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:fileWithPath]; // Sneha
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//convert the file to nsurl..
-(void)getTempFileURI:(CDVInvokedUrlCommand *)command{
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSURL *url = [htmlToPdf getFileURI:command.arguments];

	CDVPluginResult *result;
	if(url){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[url absoluteString]];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//read the contents of file into string..
-(void)readDecryptedFile:(CDVInvokedUrlCommand *)command{
	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	CDVPluginResult *result;
	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//all camera related functionality
-(void)camera:(CDVInvokedUrlCommand *)command{

	self.hasPendingOperation = YES;
	self.latestcommand = command;
	arrayOfProfDetails = command.arguments;
	[self orientationFaceUp];

	NSLog(@"Moving to Camera View Controller from html");
	NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	if (deviceOrientation == UIDeviceOrientationLandscapeLeft){

		[userDefaults setInteger:1  forKey:@"orient"];


	}else if (deviceOrientation == UIDeviceOrientationLandscapeRight){

		[userDefaults setInteger:0  forKey:@"orient"];

	}
	[userDefaults synchronize];

	//Sneha
	if ([arrayOfProfDetails[2] isEqualToString:@"Gallery"]) {

		self.imgSelector = [[MultiImgSelectVC alloc] initWithProfileData:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		[self.viewController presentViewController:self.imgSelector.elcPicker animated:YES completion:nil];

	} else if ([arrayOfProfDetails[2] isEqualToString:@"Camera"]) {

		self.custCamera = [[CustomCameraController alloc] initWithDataFromPhoneGap:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		NSLog(@"Moving to Camera View Controller from html");
		[self.viewController presentViewController:self.custCamera.picker animated:YES completion:nil];

	} else if ([arrayOfProfDetails[2] isEqualToString:@"Profile"]) {

		self.overlay = [[ProfileCamera alloc] initWithProfileData:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		self.overlay.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		NSLog(@"the camera view opened from profile plugin");
		[self.viewController presentViewController:self.overlay.picker animated:YES completion:nil];
	}
	else if ([arrayOfProfDetails[2] isEqualToString:@"viewimage"]  || [arrayOfProfDetails[2] isEqualToString:@"viewimageProfile"]) {
		[self gallery:command];
	}else if([arrayOfProfDetails[2] isEqualToString:@"GalleryProfile"]){

		//Added by Sneha for profile img selection 03-01-16
		self.custPicker = [[CustomPickerViewController alloc]init];
		self.custPicker.delegate = self;
		self.custPicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
		[self.viewController presentViewController:self.custPicker animated:YES completion:nil];

	}
}

-(void)gallery:(CDVInvokedUrlCommand *)command {

	NSLog(@"inside gallery plugin handler");
	NSLog(@"array: %@", arrayOfProfDetails);
	NSString *fileURI = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",command.arguments[0]]];

	NSError *error = nil;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fileURI error:&error];
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF contains[dc] %@",command.arguments[1]];
	NSLog(@"files: %@\ncount: %lu", [dirContents filteredArrayUsingPredicate:pred], (unsigned long)[dirContents filteredArrayUsingPredicate:pred].count);
	//Added by Sneha 30-12-16
	NSInteger imgCount;
	if ([[arrayOfProfDetails objectAtIndex:2] isEqualToString:@"viewimageProfile"]) {
		imgCount = 1;
	} else {
		imgCount = [dirContents filteredArrayUsingPredicate:pred].count;
	}

	GalleryVC *galleryVC = [[GalleryVC alloc] initWithArray:arrayOfProfDetails andWithImageCount:(int)imgCount];
	galleryVC.plugin = self;
	[self.viewController presentViewController:galleryVC animated:NO completion:^{}];
}

-(void)getTempNormalFileURI:(CDVInvokedUrlCommand *)command {

	NSLog(@"PluginHandler -> getTempNormalFileURI: %@", command.arguments);
	NSString *fileURI = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent: [NSString stringWithFormat:@"%@%@",command.arguments[0], command.arguments[1]]];
	CDVPluginResult *result;

	if(fileURI){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileURI];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)getDataFromFileInAssets:(CDVInvokedUrlCommand *)command {

	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSString *fileDataVal = [htmlToPdf getContentFromFile:command.arguments];
	CDVPluginResult *result;

	if(fileDataVal){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileDataVal];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)readOriginalFile:(CDVInvokedUrlCommand *)command {

	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	CDVPluginResult *result;

	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//Added by Sneha 30-12-16
-(void)paymentGateway:(CDVInvokedUrlCommand *)command {

	NSLog(@"PluginHandler -> paymentGateway: %@", command.arguments);
	self.latestcommand = command;

	NSDictionary *paymentReqObj = [NSDictionary dictionaryWithObjectsAndKeys:
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"Amount"]], @"Amount",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PolicyNo"]], @"PolicyNo",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PremiumType"]], @"PremiumType",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"OwnerName"]], @"OwnerName",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"AgentCode"]], @"AgentCode",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PremiumDueDate"]], @"PremiumDueDate",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"Mobile"]], @"Mobile",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"EmailId"]], @"EmailId",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"TrSeries"]], @"TrSeries", nil];

	NSLog(@"paymentReqObj: %@", paymentReqObj);

	[self createMessageForBillDesk:paymentReqObj];

}

-(void)openFile:(CDVInvokedUrlCommand *)command {

	NSLog(@"PluginHandler -> openFile: %@", command.arguments);
	OpenDocument *docVC = [[OpenDocument alloc] initWithDataToShow:[command.arguments objectAtIndex:0]];
	[self.viewController presentViewController:docVC animated:NO completion:^{}];

	[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:self.latestcommand.callbackId];
}

-(void)updateCheck:(CDVInvokedUrlCommand *)command {

	NSLog(@"PluginHandler -> updateCheck: %@", command.arguments);
	//	[(AppDelegate *)[[UIApplication sharedApplication] delegate] checkVersion];
	//[(AppDelegate *)[[UIApplication sharedApplication] delegate] checkDBVersion];

}

#pragma mark - Public Methods
-(void)orientationFaceUp{
	UIDeviceOrientation orientatio =[[UIDevice currentDevice]orientation];
	//UIDeviceOrientation orientapp =
	//NSLog(@"the device orientation %d",orientatio);
	if( orientatio != UIDeviceOrientationLandscapeRight ||
	   orientatio != UIDeviceOrientationLandscapeLeft)
	{
		if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeRight] forKey:@"orientation"];
		}
		else
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
		}
	}
}

-(void) capturedImageWithPath:(NSDictionary *)dict{

	//Added by Sneha 21/02/2017
	if ([[dict objectForKey:@"message"] isEqualToString:@"Fail"]) {
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	} else {
		[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	}
	// Unset the self.hasPendingOperation property
	self.hasPendingOperation = NO;
	// Hide the picker view
	[self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

// Sneha
-(void)closeCameraWithDict:(NSDictionary *)dict {

	[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	self.hasPendingOperation = NO;
	[self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

//Added by Sneha for profile img selection 03-01-16
#pragma mark - UIImagePickerControllerDelegate method
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
	// Dismiss the image selection, hide the picker and

	//show the image view with the picked image

	[self.custPicker dismissViewControllerAnimated:YES completion:nil];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetails objectAtIndex:0],[NSString stringWithFormat:@"%@.jpg",[arrayOfProfDetails objectAtIndex:1]]];
	NSLog(@"first profile picture path created :::%@",imageFilePath);


	//Added by Sneha to check the size of image 21/02/2017
	NSData *JpegImg = UIImageJPEGRepresentation(image, 1);
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	if ([JpegImg length] > 2097152) {
		[[[UIAlertView alloc] initWithTitle:@"Image size exceeded!"
										message: @"Image size should be less than 2MB."
									   delegate:nil
							  cancelButtonTitle:nil
							  otherButtonTitles:NSLocalizedString(@"Okay", nil), nil] show];
		[dict setObject:[NSNumber numberWithInt:0] forKey:@"code"];
		[dict setObject:@"Fail" forKey:@"message"];

	} else {
		NSData *JpegIamge = UIImageJPEGRepresentation(image, 0.4);
		[JpegIamge writeToFile:imageFilePath atomically:YES];

		//profile pic i.e thumbnail image
		NSString *thumbImagePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetails objectAtIndex:0],[NSString stringWithFormat:@"%@_thumb.jpg",[arrayOfProfDetails objectAtIndex:1]]];
		NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
		[returnData setValue:thumbImagePath forKey:@"path"];
		NSLog(@"thumb picture path created :::%@",thumbImagePath);
		NSData *JpegThumbImage = UIImageJPEGRepresentation([self imageResize:image andResizeTo:CGSizeMake(110, 110)], 1.0);
		[JpegThumbImage writeToFile:thumbImagePath atomically:YES];

		[dict setObject:[NSNumber numberWithInt:1] forKey:@"code"];
		[dict setObject:@"Success" forKey:@"message"];
	}
	[self capturedImageWithPath:dict];
}

#pragma mark ImageResizing Method
// ImageResizing Method (image size is less than 500 kb)
-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize{

	CGFloat scale = 1.0;
	UIGraphicsBeginImageContextWithOptions(newSize, YES, scale);
	[img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

#pragma mark - Bill Desk methods
-(void )createMessageForBillDesk:(NSDictionary *)_PGData {
	NSMutableString *MessageForPG = [[NSMutableString alloc] init];
	NSMutableString *MSGData = [[NSMutableString alloc] init];

	NSString *MerchantID = @"TALIC";
	NSString *CustomerID = [_PGData objectForKey:@"PolicyNo"];//@"12345555";
	NSString *Filler1 = @"NA";
    NSString *Amount = [_PGData objectForKey:@"Amount"]; //@"1";
	NSString *Filler2 = @"NA";
	NSString *Filler3 = @"NA";
	NSString *Filler4 = @"NA";
	NSString *CurrType = @"INR";
	NSString *Filler5 = @"NA";
	NSString *TypeField1 = @"R";
	//change security id
	NSString *securityID = @"TALICAPP-NA";
	NSString *Filler6 = @"NA";
	NSString *Filler7 = @"NA";
	NSString *TypeField2 = @"F";
	//    NSString *email = [_PGData objectForKey:@"EmailId"];
	//    NSString *mobile = [_PGData objectForKey:@"Mobile"];
	//********************************

	NSDateFormatter *formatter;
	NSString        *dateString;

	formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"YYYYMMddHHMMSS"];

	dateString = [formatter stringFromDate:[NSDate date]];
	//********************************
	//default NA
	NSString *addInfo1 = ([_PGData objectForKey:@"PremiumType"] == NULL)?@"NA":[_PGData objectForKey:@"PremiumType"];
	NSString *addInfo2 = ([_PGData objectForKey:@"OwnerName"] == NULL)?@"NA":[_PGData objectForKey:@"OwnerName"];
	NSString *addInfo3 = ([_PGData objectForKey:@"AgentCode"] == NULL)?@"NA":[_PGData objectForKey:@"AgentCode"];
	NSString *addInfo4 = ([_PGData objectForKey:@"PremiumDueDate"] == NULL)?@"NA":[_PGData objectForKey:@"PremiumDueDate"];
	NSString *addInfo5 = ([_PGData objectForKey:@"Mobile"] == NULL)?@"NA":[_PGData objectForKey:@"Mobile"];
	NSString *addInfo6 = ([_PGData objectForKey:@"EmailId"] == NULL)?@"NA":[_PGData objectForKey:@"EmailId"];
	NSString *addInfo7 = ([_PGData objectForKey:@"TrSeries"] == NULL)?@"NA":[_PGData objectForKey:@"TrSeries"];
	NSString *returnURL = [NSString stringWithFormat:@"%@%@",BILLDESK_URL,@"BillDeskPG/ReturnURL.jsp"];


	[MessageForPG appendFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",MerchantID,CustomerID,Filler1,Amount,Filler2,Filler3,Filler4,CurrType,Filler5,TypeField1,securityID,Filler6,Filler7,TypeField2,addInfo1,addInfo2,addInfo3,addInfo4,addInfo5,addInfo6,addInfo7,returnURL];

	HttpConnectionClass *classhttp = [[HttpConnectionClass alloc] init];

	//*****************************SERVLET CALL********************************
	NSString *checkSumUrl =  [NSString stringWithFormat:@"%@%@",BILLDESK_URL,@"BillDeskPG/generateCheckSum"];

	NSString *param = [NSString stringWithFormat:@"msg=%@",MessageForPG];
	NSLog(@"url created %@%@",checkSumUrl,param);

	__block NSString *checkSummValue = @"";

	NSLog(@"the data is %@",[param dataUsingEncoding:NSUTF8StringEncoding]);
	NSLog(@"data convert to strin %@",[[NSString alloc] initWithData:[param dataUsingEncoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding]);
	// changes 28th March
	[classhttp postToUrl:checkSumUrl withBody:[param dataUsingEncoding:NSUTF8StringEncoding] withCallback:^(NSString *response, int statusCode) {
		checkSummValue = response;
		NSLog(@"response is %@",response);
		if ([response containsString:@"ERROR"]) {
			//response contains errcodde as 26
			NSDictionary *dict = [[NSMutableDictionary alloc] init];
			NSArray *arr = [response componentsSeparatedByString:@"|"];
			[dict setValue:[arr objectAtIndex:1] forKey:[arr objectAtIndex:0]];
			[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
		}else{
			if (checkSummValue.length != 0) {
				[MSGData appendFormat:@"%@|%@",MessageForPG,checkSummValue];
				NSLog(@"message data is %@",MSGData);
				bdvc = [[BDViewController alloc]initWithMessage:(NSString *)MSGData andToken:@"NA" andEmail:addInfo6 andMobile:addInfo5 andAmount:[Amount floatValue]];
				UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:bdvc];
				bdvc.delegate=self;
				//                [controller pushViewController:bdvc animated:YES];
				//[self.viewController.view addSubview:bdvc.view];
				[self.viewController presentViewController:controller animated:YES completion:NULL];
			}else{
				NSLog(@"values is blank");
			}
		}
	}];

}

#pragma mark -- Payment Protcol
-(void)paymentStatus:(NSString*)message{
	NSLog(@"payment status is [%@]",[NSString stringWithFormat:@"%@", [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]);

	[self.viewController dismissViewControllerAnimated:YES completion:NULL];

	NSLog(@"payment status response [%@]",message);
	NSArray *responseComponents=[message componentsSeparatedByString:@"|"];
	NSString *statusCode=(NSString*)[responseComponents objectAtIndex:24];
	NSLog(@"status code is %@",statusCode);
	if([responseComponents count]>=25)
	{
		//[self showAlert:statusCode];
		[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message] callbackId:self.latestcommand.callbackId];
	}
	else
	{

		[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message] callbackId:self.latestcommand.callbackId];
		//[self showAlert:@"Something went wrong"];
	}
}

#pragma mark - Public methods
-(void)showAlert:(NSString*)message{
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Payment status" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}
@end
