//
//  CreateFolders.m
//  Sales App
//
//  Created by Sneha Sawant on 21/10/16.
//
//

#import "CreateFolders.h"

NSString *const upStreamDir = @"/FromClient";
NSString *const downStreamDir = @"/ToClient";

@implementation CreateFolders


- (void) createInitialFolders{
    BOOL isSuccess = false;
    
    NSArray *foldersArray = [NSArray arrayWithObjects: [NSString stringWithFormat:@"%@/FORM", upStreamDir],
                             [NSString stringWithFormat:@"%@/COMBO", upStreamDir],
                             [NSString stringWithFormat:@"%@/APP/HTML", upStreamDir],
                             [NSString stringWithFormat:@"%@/APP/PDF", upStreamDir],
                             [NSString stringWithFormat:@"%@/APP/SIGNATURE", upStreamDir],
                             [NSString stringWithFormat:@"%@/APP/IMAGES/PHOTOS", upStreamDir],
                             [NSString stringWithFormat:@"%@/FHR/HTML", upStreamDir],
                             [NSString stringWithFormat:@"%@/FHR/PDF", upStreamDir],
                             [NSString stringWithFormat:@"%@/FHR/SIGNATURE", upStreamDir],
                             [NSString stringWithFormat:@"%@/FF/HTML", upStreamDir],
                             [NSString stringWithFormat:@"%@/FF/PDF", upStreamDir],
                             [NSString stringWithFormat:@"%@/FF/SIGNATURE", upStreamDir],
                             [NSString stringWithFormat:@"%@/FF/IMAGES", upStreamDir],
                             [NSString stringWithFormat:@"%@/SIS/HTML", upStreamDir],
                             [NSString stringWithFormat:@"%@/SIS/PDF", upStreamDir],
                             [NSString stringWithFormat:@"%@/SIS/SIGNATURE", upStreamDir],
                             [NSString stringWithFormat:@"%@/SIS/IMAGES", upStreamDir],
                             [NSString stringWithFormat:@"%@/Products", downStreamDir],
                             [NSString stringWithFormat:@"%@/Profile", downStreamDir],
                             [NSString stringWithFormat:@"%@/MyResources", downStreamDir],
                             [NSString stringWithFormat:@"%@/CONF", downStreamDir],
                             [NSString stringWithFormat:@"%@/APPOUTPUT", downStreamDir],nil];
    
    isSuccess = [self createFolder:foldersArray];
}

- (BOOL) createFolder:(NSArray *)folderArray {
    
    BOOL isSuccess = false;
    NSError * error = nil;
    
    for (NSString *folderName in folderArray) {
        
        NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",folderName]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
            if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                           withIntermediateDirectories:YES
                                                            attributes:nil
                                                                 error:&error]) {
                
                NSLog(@"Error in creating folders! %@", error);
                
            } else {
                
                NSLog(@"Successfully created folders!");
                isSuccess = true;
                
            }
        }
        
    }

    return  isSuccess;
}

@end
