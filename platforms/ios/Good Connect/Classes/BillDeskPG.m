////
////  BillDeskPG.m
////  Good Connect
////
////  Created by Sneha Sawant on 30/12/16.
////
////
//
//#import "BillDeskPG.h"
//
//@implementation BillDeskPG
//@synthesize latestcommand;
//
//-(void)CallBillDesk:(CDVInvokedUrlCommand *)command{
//    
//    NSDictionary *PGData = [command.arguments objectAtIndex:0];
//    
//    self.latestcommand = command;
//    
//    //[self createMessageForBillDesk:PGData];
//    
//}
//
//-(void )createMessageForBillDesk:(NSDictionary *)_PGData andWithController:(UIViewController *)ctrl{
//    NSMutableString *MessageForPG = [[NSMutableString alloc] init];
//    NSMutableString *MSGData = [[NSMutableString alloc] init];
//    
//    NSString *MerchantID = @"TALIC";
//    NSString *CustomerID = [_PGData objectForKey:@"PolicyNo"];//@"12345555";
//    NSString *Filler1 = @"NA";
//    NSString *Amount = [_PGData objectForKey:@"Amount"];//@"21.00";
//    NSString *Filler2 = @"NA";
//    NSString *Filler3 = @"NA";
//    NSString *Filler4 = @"NA";
//    NSString *CurrType = @"INR";
//    NSString *Filler5 = @"NA";
//    NSString *TypeField1 = @"R";
//    //change security id
//    NSString *securityID = @"TALIC";
//    NSString *Filler6 = @"NA";
//    NSString *Filler7 = @"NA";
//    NSString *TypeField2 = @"F";
//    NSString *email = [_PGData objectForKey:@"EmailId"];
//    NSString *mobile = [_PGData objectForKey:@"Mobile"];
//    //********************************
//    
//    NSDateFormatter *formatter;
//    NSString        *dateString;
//    
//    formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYYMMddHHMMSS"];
//    
//    dateString = [formatter stringFromDate:[NSDate date]];
//    //********************************
//    //default NA
//    NSString *addInfo1 = [_PGData objectForKey:@"addInfo1"];
//    NSString *addInfo2 = [_PGData objectForKey:@"addInfo2"];
//    NSString *addInfo3 = [_PGData objectForKey:@"addInfo3"];
//    NSString *addInfo4 = [_PGData objectForKey:@"addInfo4"];
//    NSString *addInfo5 = [_PGData objectForKey:@"addInfo5"];
//    NSString *addInfo6 = [_PGData objectForKey:@"addInfo6"];
//    NSString *addInfo7 = [_PGData objectForKey:@"addInfo7"];
//    NSString *returnURL = [NSString stringWithFormat:@"%@%@",BILLDESK_URL,@"BillDeskPG/ReturnURL.jsp"];
//    
//    
//    [MessageForPG appendFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",MerchantID,CustomerID,Filler1,Amount,Filler2,Filler3,Filler4,CurrType,Filler5,TypeField1,securityID,Filler6,Filler7,TypeField2,addInfo1,addInfo2,addInfo3,addInfo4,addInfo5,addInfo6,addInfo7,returnURL];
//    
//    HttpConnectionClass *classhttp = [[HttpConnectionClass alloc] init];
//    
//    //*****************************SERVLET CALL********************************
//    NSString *checkSumUrl =  [NSString stringWithFormat:@"%@%@",BILLDESK_URL,@"BillDeskPG/generateCheckSum"];
//    
//    NSString *param = [NSString stringWithFormat:@"msg=%@",MessageForPG];
//    NSLog(@"url created %@%@",checkSumUrl,param);
//    
//    __block NSString *checkSummValue = @"";
//    
//    NSLog(@"the data is %@",[param dataUsingEncoding:NSUTF8StringEncoding]);
//    NSLog(@"data convert to strin %@",[[NSString alloc] initWithData:[param dataUsingEncoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding]);
//    // changes 28th March
//    [classhttp postToUrl:checkSumUrl withBody:[param dataUsingEncoding:NSUTF8StringEncoding] withCallback:^(NSString *response, int statusCode) {
//        checkSummValue = response;
//        NSLog(@"response is %@",response);
//        if ([response containsString:@"ERROR"]) {
//            //response contains errcodde as 26
//            NSDictionary *dict = [[NSMutableDictionary alloc] init];
//            NSArray *arr = [response componentsSeparatedByString:@"|"];
//            [dict setValue:[arr objectAtIndex:1] forKey:[arr objectAtIndex:0]];
//            [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
//        }else{
//            if (checkSummValue.length != 0) {
//                [MSGData appendFormat:@"%@|%@",MessageForPG,checkSummValue];
//                NSLog(@"Message data is %@",MSGData);
//                bdvc = [[BDViewController alloc]initWithMessage:(NSString *)MSGData andToken:@"NA" andEmail:email andMobile:mobile andAmount:[Amount floatValue]];
//                UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:bdvc];
//                bdvc.delegate=self;
////                [controller pushViewController:bdvc animated:YES];
//                //[self.viewController.view addSubview:bdvc.view];
//                [ctrl presentViewController:controller animated:YES completion:NULL];
//            }else{
//                NSLog(@"values is blank");
//            }
//        }
//    }];
//    
//}
//
//#pragma mark -- Payment Protcol
//-(void)paymentStatus:(NSString*)message{
//    NSLog(@"payment status is [%@]",[NSString stringWithFormat:@"%@", [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]);
//    
//    [self.viewController dismissViewControllerAnimated:YES completion:NULL];
//    
//    NSLog(@"payment status response [%@]",message);
//    NSArray *responseComponents=[message componentsSeparatedByString:@"|"];
//    NSString *statusCode=(NSString*)[responseComponents objectAtIndex:24];
//    NSLog(@"status code is %@",statusCode);
//    if([responseComponents count]>=25)
//    {
//        //[self showAlert:statusCode];
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message] callbackId:self.latestcommand.callbackId];
//    }
//    else
//    {
//        
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message] callbackId:self.latestcommand.callbackId];
//        //[self showAlert:@"Something went wrong"];
//    }
//}
//
//#pragma mark - Public methods
//-(void)showAlert:(NSString*)message{
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Payment status" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
//}
//
//#pragma mark - IBACTIONS
//- (IBAction)doneClicked:(id)sender{
//    //NSLog(@"Done Clicked.");
//    [self.viewController.view endEditing:YES];
//}
//
//
//#pragma mark -- Orientations method
//-(BOOL)shouldAutorotate{
//    //return [bdvc shouldAutorotate];
//    return NO;
//}
//
//-(NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskLandscape;
//}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
//    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//    //return [bdvc shouldAutorotateToInterfaceOrientation:interfaceOrientation];
//}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//    return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight;
//}
//
//@end
