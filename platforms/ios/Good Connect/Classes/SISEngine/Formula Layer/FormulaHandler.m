   //
//  FormulaHandler.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "FormulaHandler.h"
#import "Constant.h"



@implementation FormulaHandler

#pragma mark -- PUBLIC METHODS
-(NSObject *) evaluateFormula:(NSString *)_PlanCode :(NSString *)_calValue :(FormulaBean *)_bean{
    NSObject *obj = nil;
    PFRPlanFormulaRef *formulaRefObj =[DataAccessClass getFormula:_PlanCode andWithValue:_calValue];
    if(formulaRefObj != nil){
        obj = [[NSObject alloc] init];
        NSDictionary *formulaExp = [self getFormulaExpression:[formulaRefObj PER_FRM_CD]];
        obj = [self executeFormula:formulaExp :_bean];
    }
    return obj;
}

-(NSObject *)executeFormula:(NSDictionary *)_formulaExpression :(FormulaBean *)_bean{
    NSObject *obj = nil;
    NSObject *obj1 = nil;
    NSObject *obj2 = nil;
    NSString *operandType = @"";
    
    FEDFormulaExtDesc *formulaExtDescObj = [_formulaExpression objectForKey:[NSNumber numberWithInt:1]];
    if([TYPE_FORMULA isEqualToString:formulaExtDescObj.FED_OBJ_OPND_TYPE]){
        //NSLog(@"TYPE FORMULA");
        NSMutableDictionary *newForMap = [self getFormulaExpression:formulaExtDescObj.FED_OBJ_OPND];
        obj1 = [self executeFormula:newForMap :_bean];
    }else if([TYPE_MATH_FORMULA isEqualToString:formulaExtDescObj.FED_OBJ_OPND_TYPE]){
        NSLog(@"MATH FORMULA");
    }else if([TYPE_NUMBER isEqualToString:formulaExtDescObj.FED_OBJ_OPND_TYPE]){
       // NSLog(@"TYPE NUMBER");
        obj1 = [self getNumber:[formulaExtDescObj FED_OBJ_TYPE] andWithOperand:[formulaExtDescObj FED_OBJ_OPND]];
    }else{
        obj1 = [self getParameterValue:formulaExtDescObj.FED_OBJ_OPND andWithBeanObject:_bean];
    }
    
    operandType = formulaExtDescObj.FED_OBJ_TYPE;
    for(int count = 2;count <=[_formulaExpression count];count++){
        FEDFormulaExtDesc *formulaExtDescObj1 = [_formulaExpression objectForKey:[NSNumber numberWithInt:count]];
        if([TYPE_FORMULA isEqualToString:formulaExtDescObj1.FED_OBJ_OPND_TYPE]){
            //NSLog(@"TYPE FORMULA");
            NSMutableDictionary *newForMap = [self getFormulaExpression:formulaExtDescObj1.FED_OBJ_OPND];
            obj2 = [self executeFormula:newForMap :_bean];
        }else if([TYPE_MATH_FORMULA isEqualToString:formulaExtDescObj1.FED_OBJ_OPND_TYPE]){
            obj1 = [self evalMathFormula:obj1 :operandType :formulaExtDescObj1.FED_OBJ_OPND :formulaExtDescObj1.FED_OBJ_TYPE];
            operandType = formulaExtDescObj1.FED_OBJ_TYPE;
            continue;
        }else if([TYPE_NUMBER isEqualToString:formulaExtDescObj1.FED_OBJ_OPND_TYPE]){
            obj2 = [self getNumber:formulaExtDescObj1.FED_OBJ_TYPE andWithOperand:formulaExtDescObj1.FED_OBJ_OPND];
        }else{
            obj2 = [self getParameterValue:formulaExtDescObj1.FED_OBJ_OPND andWithBeanObject:_bean];
        }
        obj1 = [self evalFormula:obj1 :operandType :obj2 :formulaExtDescObj1.FED_OBJ_TYPE :formulaExtDescObj1.FED_OBJ_OPTR];
        operandType = [self getOprType:operandType :formulaExtDescObj1.FED_OBJ_TYPE];
    }
    obj = obj1;
    return obj;
}
     


-(NSObject *)evalFormula:(NSObject *)op1 :(NSString *)optype1 :(NSObject *)op2 :(NSString *)opType2 :(NSString *)oprt{
    NSObject *obj = [[NSObject alloc] init];
    
    int i_obj1 = -1;
    long l_obj1 = -1;
    double d_obj1 = -1;
    int i_obj2 =-1;
    long l_obj2 = -1;
    double d_obj2 = -1;

    if([DATA_TYPE_INT isEqualToString:optype1]){
        i_obj1 = [[NSString stringWithFormat:@"%@ ",op1] intValue];
    }
    if([DATA_TYPE_LONG isEqualToString:optype1]){
        l_obj1 = [[NSString stringWithFormat:@"%@ ",op1] longLongValue];
    }
    if([DATA_TYPE_DOUBLE isEqualToString:optype1]){
        d_obj1 = [[NSString stringWithFormat:@"%@ ",op1] doubleValue];
    }
    if([DATA_TYPE_INT isEqualToString:opType2]){
        i_obj2 = [[NSString stringWithFormat:@"%@ ",op2] intValue];
    }
    if([DATA_TYPE_LONG isEqualToString:opType2]){
        l_obj2 = [[NSString stringWithFormat:@"%@ ",op2] longLongValue];
    }
    if([DATA_TYPE_DOUBLE isEqualToString:opType2]){
        d_obj2 = [[NSString stringWithFormat:@"%@ ",op2] doubleValue];
    }
    
    if(i_obj1 != -1)
    {
        if(i_obj2 != -1){
            obj = [self getValueii:i_obj1 :i_obj2 :oprt];
        }else if (l_obj2 != -1){
            obj = [self getValueil:i_obj1 :l_obj2 :oprt];
        }else if (d_obj2 != -1){
            obj = [self getValueid:i_obj1 :d_obj2 :oprt];
        }
    }else if(l_obj1 != -1)
    {
        if(i_obj2 != -1){
            obj = [self getValueli:l_obj1 :i_obj2 :oprt];
        }else if (l_obj2 != -1){
            obj = [self getValuell:l_obj1 :l_obj2 :oprt];
        }else if (d_obj2 != -1){
            obj = [self getValueld:l_obj1 :d_obj2 :oprt];
        }
    }else if(d_obj1 != -1)
    {
        if(i_obj2 != -1){
            obj = [self getValuedi:d_obj1 :i_obj2 :oprt];
        }else if (l_obj2 != -1){
            obj = [self getValuedl:d_obj1 :l_obj2 :oprt];
        }else if (d_obj2 != -1){
            obj = [self getValuedd:d_obj1 :d_obj2 :oprt];
        }
    }
    return obj;
}

-(NSObject *)getValueii:(int)obj1 :(int)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithInt:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithInt:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithInt:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithInt:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }
    else if([OPERATOR_MOD isEqualToString:optr]){
        obj  = [NSNumber numberWithInt:((obj1 % obj2)==0)?1:0];
    }
    else if([OPERATOR_POWER isEqualToString:optr]){
        int val = 1;
        for (int i=1; i<=obj2; i++) {
            val = val * obj1;
        }
        obj = [NSNumber numberWithInt:val];
    }
    return obj;
    
}

-(NSObject *)getValueil:(int)obj1 :(long)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithInt:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    return obj;
}

-(NSObject *)getValueid:(int)obj1 :(double)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithInt:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    return obj;

}

-(NSObject *)getValueli:(long)obj1 :(int)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithLong:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithLong:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }
    else if([OPERATOR_POWER isEqualToString:optr]){
        long val = 1;
        for (int i=1; i<=obj2; i++) {
            val = val * obj1;
        }
        obj = [NSNumber numberWithLong:val];
    }

    return obj;

}

-(NSObject *)getValuell:(long)obj1 :(long)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithLong:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithLong:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithLong:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    return obj;
}

-(NSObject *)getValueld:(long)obj1 :(double)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    return obj;

}

-(NSObject *)getValuedi:(double)obj1 :(int)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithInt:obj2];
        }
    }else if([OPERATOR_POWER isEqualToString:optr]){
        double val = 1;
        for (int i=1; i<=obj2; i++) {
            val = val * obj1;
        }
        obj = [NSNumber numberWithDouble:val];
    }
    return obj;
}

-(NSObject *)getValuedl:(double)obj1 :(long)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithLong:obj2];
        }
    }
    return obj;
}

-(NSObject *)getValuedd:(double)obj1 :(double)obj2 :(NSString *)optr{
    NSObject *obj = [[NSObject alloc] init];
    if([OPERATOR_PLUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 + obj2)];
    }else if([OPERATOR_MINUS isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 - obj2)];
    }
    else if([OPERATOR_MUL isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 * obj2)];
    }
    else if([OPERATOR_DIVIDE isEqualToString:optr]){
        obj = [NSNumber numberWithDouble:(obj1 / obj2)];
    }
    else if([OPERATOR_MAX isEqualToString:optr]){
        if(obj1 > obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    else if([OPERATOR_MIN isEqualToString:optr]){
        if(obj1 < obj2){
            obj = [NSNumber numberWithDouble:obj1];
        }else{
            obj = [NSNumber numberWithDouble:obj2];
        }
    }
    return obj;

}

-(NSString *)getOprType:(NSString *)type1 :(NSString *)type2{
    NSString *type = @"";
    if([DATA_TYPE_INT isEqualToString:type1] && [DATA_TYPE_INT isEqualToString:type2]){
        type = DATA_TYPE_INT;
    }
    if([DATA_TYPE_INT isEqualToString:type1] && [DATA_TYPE_LONG isEqualToString:type2]){
        type = DATA_TYPE_LONG;
    }
    if([DATA_TYPE_INT isEqualToString:type1] && [DATA_TYPE_DOUBLE isEqualToString:type2]){
        type = DATA_TYPE_DOUBLE;
    }
    if([DATA_TYPE_DOUBLE isEqualToString:type1] && [DATA_TYPE_INT isEqualToString:type2]){
        type = DATA_TYPE_DOUBLE;
    }
    if([DATA_TYPE_DOUBLE isEqualToString:type1] && [DATA_TYPE_DOUBLE isEqualToString:type2]){
        type = DATA_TYPE_DOUBLE;
    }
    if([DATA_TYPE_DOUBLE isEqualToString:type1] && [DATA_TYPE_LONG isEqualToString:type2]){
        type = DATA_TYPE_DOUBLE;
    }
    if([DATA_TYPE_LONG isEqualToString:type1] && [DATA_TYPE_INT isEqualToString:type2]){
        type = DATA_TYPE_LONG;
    }
    if([DATA_TYPE_LONG isEqualToString:type1] && [DATA_TYPE_DOUBLE isEqualToString:type2]){
        type = DATA_TYPE_DOUBLE;
    }
    if([DATA_TYPE_LONG isEqualToString:type1] && [DATA_TYPE_LONG isEqualToString:type2]){
        type = DATA_TYPE_LONG;
    }
    return type;
}

-(NSObject *)getNumber:(NSString *)_type andWithOperand:(NSString *)_operand{
    NSObject *obj = 0;
    if([DATA_TYPE_INT isEqualToString:_type]){
        obj = [NSNumber numberWithInt: [_operand intValue]];
    }else if([DATA_TYPE_DOUBLE  isEqualToString:_type]){
        obj = [NSNumber numberWithDouble:[_operand doubleValue]];
    }else if([DATA_TYPE_LONG isEqualToString:_type]){
        obj = [NSNumber numberWithLong:[_operand longLongValue]];
    }
    return obj;
}

-(NSObject *)getParameterValue:(NSString *)_param andWithBeanObject:(FormulaBean *)_bean{
    NSObject *obj = [[NSObject alloc] init];
    if ([PARAM_PREMIUM isEqualToString:_param]) {
        obj=[NSNumber numberWithLong:_bean.premium];
    }
    
    if([PARAM_ANNUAL_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithLong:_bean.premium];
    }
    
    if ([PARAM_PREMIUM_MULTIPE isEqualToString:_param]) {
        obj=[NSNumber numberWithDouble:[DataAccessClass getPremiumMultiple:_bean.request andWithMIP:NO]];
    }
    
    if([PARAM_TERM isEqualToString:_param]){
        obj = [NSNumber numberWithInteger:_bean.request.PolicyTerm];
    }
    if([PARAM_COVERAGE isEqualToString:_param]){
        obj = [NSNumber numberWithLongLong:_bean.request.Sumassured];
    }
    if([PARAM_CASH_SURR_VAL_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getCSVFactor:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    if([PARAM_GUARANTEED_SURR_VAL_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getGSVFactor:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_RIDER_PREMIUM_MULTIPLE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRiderPremiumMultiple:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    //Added by Amruta for MIP WPP rider
    if([PARAM_RIDER_MODEL_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRiderModelFactor:_bean.request]];
    }
    
    if([PARAM_RIDER_SUM_ASSURED isEqualToString:_param]){
        obj = [NSNumber numberWithLong:[DataAccessClass getRiderSumAssuared:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_WP_LOAD_VALUE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getWpLoadValue:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    //FREEDOM
    if ([PARAM_TBPT_4_FACTOR isEqualToString:_param]) {
        obj =  [NSNumber numberWithDouble:[DataAccessClass getTB4Factor:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if ([PARAM_TBPT_8_FACTOR isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getTB8Factor:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if ([PARAM_DAGAI isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:_bean.doubleAGAI];
    }
    
    if ([PARAM_RBV isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:_bean.RBV];
    }
    
    //FREEDOM
    
    //Tax changes
    //18th may 2016
    if ([PARAM_RPST_H isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:RPST_H];
    }
    if ([PARAM_RPST_N isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:RPST_N];
    }
    if ([PARAM_BPST_H isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:BPST_H];
    }
    if ([PARAM_BPST_N isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:BPST_N];
    }
    if ([PARAM_RIDST_H isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:RIDST_H];
    }
    if ([PARAM_RIDST_N isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:RIDST_N];
    }
    //Tax changes
    
    //Added by Amruta on 19/8/15 for FG
    if ([PARAM_MATURITY_FACTOR_VALUE_FG isEqualToString:_param]&&[_bean.request.InsuredSex isEqualToString:@"M"]) {
        obj=[NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"MFV_M" andWithYear:0]];
    }
    
    if ([PARAM_MATURITY_FACTOR_VALUE_FG isEqualToString:_param]&&[_bean.request.InsuredSex isEqualToString:@"F"] ) {
        obj=[NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"MFV_F" andWithYear:0]];
    }
    
    //GIP
    if([PARAM_ANNUALIZED_NSAP_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithLong:_bean.ANSAP];
    }
    
    //Added by Amruta on 31/8/15 for MBP
    if ([PARAM_AGAISSV isEqualToString:_param]) {
        obj = [NSNumber numberWithLong:_bean.AGAISSV];
    }
    
    if ([PARAM_TOTAL_GURANTEED_ANNUAL_INCOME_MBP isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalGAIMBP:_bean]];
    }
    //********************************SGP*********************************
    if([PARAM_TERMINAL_BONUS_4_SGP isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB4SGP" andWithYear:_bean.CounterVariable]];
    }
    
    if([PARAM_TERMINAL_BONUS_8_SGP isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB8SGP" andWithYear:_bean.CounterVariable]];
    }
    
    if([PARAM_AGAI isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:_bean.AGAI];
    }
    
    if([PARAM_PREMIUM_DISCOUNT isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:_bean.premiumDiscount];
    }
    
    if([PARAM_AMOUNT isEqualToString:_param]){
        //changes 13th may
        NSDecimalNumber *num =[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.16lf",_bean.AmountForServiceTax]];
        //NSDecimalNumber *num2 =[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf",_bean.AmountForServiceTax]];
        //NSDecimalNumber *num1 = [NSDecimalNumber decimalNumberWithString:@"1.0100249999999997"];
        obj = num;
    }
    //********************************SGP*********************************
    
    //Good kid
    if([PARAM_TBPT_8_FACTOR_GK isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB8GK" andWithYear:_bean.CounterVariable]];
    }
    
    if([PARAM_TBPT_4_FACTOR_GK isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB4GK" andWithYear:_bean.CounterVariable]];
    }
    
    //good kid
    
    if([PARAM_INS_LOAD_VALUE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getInsLoadValue:_bean.request]];
    }
    
    if([PARAM_MODAL_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getModelFactor:_bean.request]];
    }
    
    if([PARAM_PREMIUM_PAYING_TERM isEqualToString:_param]){
        obj = [NSNumber numberWithInteger: _bean.request.PremiumPayingTerm];
    }
    
    if([PARAM_PREMIUM_ALLOCATION_CHARGES isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getPAChargeValue:_bean.request andWithPremium:_bean.premium andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_FIB_CHARGES isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getFIBCharges:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_WOP_CHARGES isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getWOPCharges:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_COUNTER isEqualToString:_param]){
        obj = [NSNumber numberWithInt: _bean.CounterVariable];
    }
    
    if([PARAM_MORTALITY_CHARGES_RATE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getMCRChares:_bean.request AndWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_POLICY_ADMIN_CHARGES isEqualToString:_param]){
        obj = [NSNumber numberWithInt: _bean.AdminCharges];
    }
    
    //*************ULIP*************************************
    if([PARAM_NET_P_HOLDER_ACCOUNT isEqualToString:_param]){
        obj = [NSNumber numberWithDouble: _bean.NetPremHolderAcc];
    }
    
    if ([PARAM_TAP isEqualToString:_param]) {
        obj = [NSNumber numberWithLong:[DataAccessClass getTotalAnnualPremium:_bean]];
    }
    
    if ([PARAM_ULIP_ANNUAL_PREMIUM isEqualToString:_param]) {
        obj = [NSNumber numberWithLong: _bean.ULIPAP];
    }
    //*************ULIP*************************************
    
    if([PARAM_SURR_CHARGE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getSurrenderChargeValue:_bean.request andWithPremium:_bean.premium andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_SURR_CHARGE_AMOUNT isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getSurrenderChargeAmount:_bean.request andWithPremium:_bean.premium andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_NETPH_AMOUNT_FOR_LOYALITY_BONUS isEqualToString:_param]){
        obj = [NSNumber numberWithInt: _bean.NetPHAmtForLoyalBonus];
    }
    
    if([PARAM_GUARANTEED_MATURITY_BONUS isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getGuaranteedMaturityBonus:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_RC isEqualToString:_param]){
        obj = [NSNumber numberWithLong: _bean.RC];
    }
    
    if([PARAM_FUND_MANAGEMENT_CHARGE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getFMC:_bean.request]];
    }
    
    if([PARAM_COMISSION_PERCENTAGE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getCommissionPercentage:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_MATURITY_FACTOR_VALUE isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getMVFactor:_bean.request]];
    }
    
    if([PARAM_TOTAL_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalPremium:_bean]];
    }
    
    if([PARAM_TOTAL_PREMIUM_ULIP isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalPremiumULIP:_bean]];
    }
    
    if([PARAM_TOTAL_REMAINING_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalRemainingPremium:_bean]];
    }
    
    if([PARAM_GUARANTED_ANNUAL_INCOME isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getGAIFactor:_bean]];
    }
    
    if([PARAM_TOTAL_GUARANTED_ANNUAL_INCOME isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalGAI:_bean]];
    }
    
    //SIP
    if ([PARAM_LARGE_PREMIUM_BOOST isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getPremiumBoost:_bean]];
    }
    if([PARAM_TOTAL_GUARANTED_ANNUAL_INCOME_MLG isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getTotalGAIMLG:_bean]];
    }
    
    if([PARAM_INFLATION_PROTECTION_COVER isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getIPCFactor:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_INFLATION_PROTECTION_COVER_MINUS1 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getIPCFactor:_bean.request andWithCount:_bean.CounterVariable - 1]];
    }
    
    if([PARAM_LOYALTY_BONUS isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"LB" andWithYear:_bean.CounterVariable]];
    }
    
    if([PARAM_REVERSIONARY_BONUS_4 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"RB4" andWithYear:0]];
    }
    
    if([PARAM_REVERSIONARY_BONUS_8 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"RB8" andWithYear:0]];
    }
    
    if([PARAM_TERMINAL_BONUS_4 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB4" andWithYear:12]];
    }
    
    if([PARAM_TERMINAL_BONUS_8 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB8" andWithYear:12]];
    }
    
    if([PARAM_TERMINAL_BONUS_4_MLM isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB4" andWithYear:10]];
    }
    
    if([PARAM_TERMINAL_BONUS_8_MLM isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB8" andWithYear:10]];
    }
    
    if([PARAM_BONUS_SURRENDER_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getBSVFactor:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_TERMINAL_BONUS_4_SMART7 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB4" andWithYear:8]];
    }
    
    if([PARAM_TERMINAL_BONUS_8_SMART7 isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"TB8" andWithYear:8]];
    }
    
    if([PARAM_PREMIUM_MULTIPLE_ULS isEqualToString:_param]){
        obj = _bean.request.Premium_Mul;
    }
    
    if([PARAM_RIDER_CHARGE_FACTOR isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getRLCF:_bean andWithRequestBean:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_FUND_MANAGMENT_CHARGE_AAA isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getAAAFMC:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_FUND_MANAGMENT_CHARGE_DEBT isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getAAADFV:_bean.request andWithCount:_bean.CounterVariable]];
    }
    
    if([PARAM_FUND_MANAGMENT_CHARGE_EQUITY isEqualToString:_param]){
        obj = [NSNumber numberWithDouble:[DataAccessClass getAAAEFV:_bean.request andWithCount:_bean.CounterVariable]];
    }
    //CHANGES 16TH OCT SA
    //***************************************************************
    if ([PARAM_FOP_RATE isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getFOPRate:_bean.request andWithCnt:_bean.CounterVariable]];
    }
    
    if ([PARAM_LOYALTY_BONUS_SA isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"LBSA" andWithYear:_bean.CounterVariable]];
    }
    
    if ([PARAM_LOYALTY_BONUS_IO isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getRBTBBonus:_bean andWithBonusTyrpe:@"LBIO" andWithYear:_bean.CounterVariable]];
    }
    
    if ([PARAM_NSAP_MORTALITY_CHARGES_RATE isEqualToString:_param]) {
        obj = [NSNumber numberWithDouble:[DataAccessClass getNSAPMCRCharges:_bean.request andWithTerm:_bean.CounterVariable]];
    }
    //***********************************************************************
    
    if([PARAM_QUOTED_MODAL_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithLong:_bean.QuotedModalPremium];
    }
    
    if([PARAM_REDUSED_PREMIUM_PAID_UP_YEAR isEqualToString:_param]){
        obj = _bean.request.RPU_Year;
    }
    if([PARAM_QAP isEqualToString:_param] || [PARAM_QUOTED_ANNUAL_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithLong:_bean.QuotedAnnualizedPremium];
    }
    
    if([TAXSLAB isEqualToString:_param]){
        obj = _bean.request.TaxSlab;
    }
    
    if([WITHDRAWAL_AMOUNT isEqualToString:_param]){
        obj = [NSNumber numberWithDouble: _bean.WithdrawalAmount];
    }
    
    if([PARAM_SERVICE_TAX_RIDERS isEqualToString:_param]){
        obj = [NSNumber numberWithLong: _bean.ServiceTaxRider];
    }
    
    if([RIDER_PREMIUM isEqualToString:_param]){
        obj = [NSNumber numberWithLong: _bean.RiderPremium];
    }
    
    if([PARAM_EXPERIENCED_BONUS_RATE isEqualToString:_param]){
        obj = _bean.request.Exp_Bonus_Rate;
    }
    
    if([MODAL_FREQUENCY isEqualToString:_param]){
        if ([_bean.request.Frequency caseInsensitiveCompare:ANNUAL_MODE]) {
            obj = [NSNumber numberWithInt:1];
        }else if ([_bean.request.Frequency caseInsensitiveCompare:SEMI_ANNUAL_MODE]) {
            obj = [NSNumber numberWithInt:2];
        }else if ([_bean.request.Frequency caseInsensitiveCompare:QUATERLY_MODE]) {
            obj = [NSNumber numberWithInt:4];
        }else if ([_bean.request.Frequency caseInsensitiveCompare:MONTHLY_MODE]) {
            obj = [NSNumber numberWithInt:12];
        }
    }
    
    //MIP changes 27 jan
    if ([PREMIUM_MULTIPLE_MIP isEqualToString:_param]) {
        obj=[NSNumber numberWithDouble:[DataAccessClass getPremiumMultiple:_bean.request andWithMIP:YES]];
    }
    
    return obj;

}

-(NSObject *)evalMathFormula:(NSObject *)obj1 :(NSString *)type1 :(NSString *)formula :(NSString *)type2{
    NSObject *obj = [[NSObject alloc] init];
    double d_obj1 = 0;
    float f_obj1 = 0;
    
    if([DATA_TYPE_DOUBLE isEqualToString:type1]){
        d_obj1 = [[NSString stringWithFormat:@"%@ ",obj1] doubleValue];
    }
    if([DATA_TYPE_FLOAT isEqualToString:type1]){
        d_obj1 = [[NSString stringWithFormat:@"%@ ",obj1] floatValue];
    }
    if([@"ROUND" isEqualToString:formula] &&
       [DATA_TYPE_DOUBLE isEqualToString:type1]){
        obj = [NSNumber numberWithDouble:round(d_obj1)];
    }
    if([@"ROUND" isEqualToString:formula] &&
       [DATA_TYPE_FLOAT isEqualToString:type1]){
        obj = [NSNumber numberWithFloat:round(f_obj1)];
    }
    if([@"ROUND2" isEqualToString:formula] &&
       [DATA_TYPE_DOUBLE isEqualToString:type1]){
       obj =  [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%0.2f",d_obj1] doubleValue]];
    }
    if([@"ROUND2" isEqualToString:formula] &&
       [DATA_TYPE_FLOAT isEqualToString:type1]){
        obj =  [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%0.2f",f_obj1] floatValue]];
    }
    //MIP
    if ([@"ROUNDDOWN2" isEqualToString:formula] && [DATA_TYPE_DOUBLE isEqualToString:type1]) {
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setRoundingMode:NSNumberFormatterRoundFloor];
        [formatter setMaximumFractionDigits:2];
        NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:d_obj1]];
        
        /*NSDecimalNumber *num = (NSDecimalNumber *)[NSDecimalNumber numberWithDouble:d_obj1];
        
        NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                           decimalNumberHandlerWithRoundingMode:NSRoundDown
                                           scale:2
                                           raiseOnExactness:NO
                                           raiseOnOverflow:NO
                                           raiseOnUnderflow:NO
                                           raiseOnDivideByZero:YES];*/
        
        //num = [num decimalNumberByRoundingAccordingToBehavior:roundUp];*/
        
        
        
        
       // NSString *str =[NSString stringWithFormat:@"%0.2f", floor(d_obj1)];
        obj = [NSNumber numberWithDouble:[numberString doubleValue]];
    }
    return obj;
}

-(NSMutableDictionary *)getFormulaExpression:(NSString *)_formulaCode{
    NSMutableDictionary *dict = [DataAccessClass getFormulaExpression:_formulaCode];
    return dict;
}

@end
