//
//  FormulaHandler.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FormulaBean.h"
#import "PlanFormulaRef.h"
#import "FEDFormulaExtDesc.h"
#import "DataAccessClass.h"

@interface FormulaHandler : NSObject

-(NSObject *) evaluateFormula:(NSString *)_PlanCode :(NSString *)_calValue :(FormulaBean *)_bean;
-(NSObject *) getNumber:(NSString *)_type andWithOperand:(NSString *)_operand;
-(NSMutableDictionary *)getFormulaExpression:(NSString *)_formulaCode;
-(NSObject *)executeFormula:(NSDictionary *)_formulaExpression :(FormulaBean *)_bean;
@end
