//
//  FundDetails.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FundDetails : NSObject

@property (nonatomic, assign) int PNL_PGD_ID;
@property (nonatomic, strong) NSString *Code;
@property (nonatomic, strong) NSString *fund_Description;
@property (nonatomic, assign) double Fund_Mgmt_charge;
@property (nonatomic, assign) int Percentage;
@property (nonatomic, assign) double PERF_3M;
@property (nonatomic, assign) double PERF_6M;
@property (nonatomic, assign) double PERF_1Y;
@property (nonatomic, assign) double PERF_2Y;
@property (nonatomic, assign) double PERF_3Y;
@property (nonatomic, assign) double PERF_4Y;
@property (nonatomic, assign) double PERF_5Y;
@property (nonatomic, assign) double PERF_Inception;


@end
