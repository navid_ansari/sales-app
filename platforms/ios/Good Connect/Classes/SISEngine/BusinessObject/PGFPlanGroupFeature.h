//
//  PGFPlanGroupFeature.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGFPlanGroupFeature : NSObject
@property(nonatomic , strong)NSString *PGF_PNL_CODE;
@property(nonatomic , strong)NSString *PGF_FEATURE;

@end
