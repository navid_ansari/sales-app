//
//  PMLPremiumMultiple.h
//  LifePlanner
//
//  Created by admin on 25/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMLPremiumMultiple : NSObject

@property (nonatomic, assign) int PML_ID;
@property (nonatomic, assign) int PML_PNL_ID;
@property (nonatomic, strong) NSString *PML_SEX;
@property (nonatomic, assign) int PML_TERM;
@property (nonatomic, assign) int PML_AGE;
@property (nonatomic, assign) double PML_PREM_MULT;
@property (nonatomic, assign) int PML_BAND;
@property (nonatomic, strong) NSString *SMOKER_FLAG;

@end
