//
//  ModelMaster.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "ModelMaster.h"

@implementation ModelMaster
@synthesize sisRequest, BaseAnnualizedPremium,AnnualPremium,DiscountedBasePremium,QuotedAnnualizedPremium,TotalModalPremium,ModalPremiumForNSAP,STRiderModalPremium,TotalRiderModalPremium,TotalAnnRiderModalPremium;
@synthesize PremiumMultiple,AnnualizedPremiumWithOccLoading,ModalPremium,riderData,PremiumAllocationCharges,AmountForInvest,OtherBenefitCharges,Agent_interMedDetail,
MortalityCharges,
PolicyAdminCharges,
ApplicableServiceTaxPH10,
DeathBenefitAtEndOfPolicyYear,
GuaranteedMaturityAdditionPH10,
SurrenderValuePH10,
FundManagementChargePH10,
RegularFundValuePH10,
RegularFundValueBeforeFMCPH10,
RegularFundValuePostFMCPH10,
TotalFundValueAtEndPolicyYrPH10,
TotalChargeForPh10,
LoyalityChargeForPh10,CommissionPayable,dfmc,AccGAI,premiumDiscount,RenewalModalPremium,ServiceTaxRenewal,ModalPremiumForAnnNSAP,TotalRenewalPremium_RS_Q,AccGuaranteedAnnualIncome,isCombo,BasicSumAssured,TOTALRB4_MIP,TOTALRB8_MIP,TOTALGAI_MIP,totalGuarBenefit;

-(id)init{
    if(self = [super init]){
        BaseAnnualizedPremium = 0;
        AnnualizedPremiumWithOccLoading = 0;
        ModalPremium = 0;
    }
    return self;
}

@end
