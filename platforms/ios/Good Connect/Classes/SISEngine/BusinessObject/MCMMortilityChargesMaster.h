//
//  MCMMortilityChargesMaster.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCMMortilityChargesMaster : NSObject
@property(nonatomic , assign) int MCM_ID;
@property(nonatomic , assign) int MCM_AGE;
@property(nonatomic , assign) double MCM_MORT_CHARGE;
@property (nonatomic, retain) NSString *MFM_SEX;
@end
