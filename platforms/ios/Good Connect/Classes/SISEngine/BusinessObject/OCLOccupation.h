//
//  OCLOccupation.h
//  LifePlanner
//
//  Created by admin on 25/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCLOccupation : NSObject

@property (nonatomic, assign) int OCL_ID;
@property (nonatomic, strong) NSString *OCL_CODE;
@property (nonatomic, strong) NSString *OCL_DESCRIPTION;
@property (nonatomic, strong) NSString *OCL_OCCUPATION;
@property (nonatomic, strong) NSString *OCL_BUSINESS;
@property (nonatomic, assign) double OCL_BLIFE;
@property (nonatomic, assign) double OCL_WP;
@property (nonatomic, assign) int OCL_CLASS;

@end
