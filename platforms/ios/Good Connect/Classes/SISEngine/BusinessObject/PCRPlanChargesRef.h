//
//  PCRPlanChargesRef.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCRPlanChargesRef : NSObject
@property(nonatomic , assign) int PCR_PNL_ID;
@property(nonatomic , assign) int PCR_ADMIN_ID;
@property(nonatomic , assign) int PCR_PREM_ID;
@property(nonatomic , assign) int PCR_MORT_ID;
@property(nonatomic , assign) int PCR_WOP_ID;
@property(nonatomic , assign) int PCR_FIB_ID;
@property(nonatomic , assign) int PCR_SUR_ID;
@property(nonatomic , assign) int PCR_GMM_ID;
@property(nonatomic , assign) int PCR_COMM_ID;


@end
