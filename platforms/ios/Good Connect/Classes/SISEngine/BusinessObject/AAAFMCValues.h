//
//  AAAFMCValues.h
//  LifePlanner
//
//  Created by Akhil Kulkarni on 04/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AAAFMCValues : NSObject
@property(nonatomic , assign)int afc_paln_id;
@property(nonatomic , assign)int afc_min_age;
@property(nonatomic , assign)int afc_max_age;
@property(nonatomic , assign)int afc_large_cap;
@property(nonatomic , assign)int afc_whole_life_income;
@property(nonatomic , assign)float afc_fmc;

@end
