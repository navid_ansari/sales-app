//
//  SPVSysParamValues.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPVSysParamValues : NSObject

@property(nonatomic, strong) NSString *SPV_SYS_PARAM;
@property(nonatomic, strong) NSString *SPV_VALUE;
@property(nonatomic, strong) NSString *SPV_FILLER;

@end
