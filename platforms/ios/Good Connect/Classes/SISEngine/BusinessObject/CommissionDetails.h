//
//  CommissionDetails.h
//  Life_Planner
//
//  Created by Ganesh on 1/27/15.
//
//

#import <Foundation/Foundation.h>

@interface CommissionDetails : NSObject

@property (nonatomic, retain) NSString *CMT_COMM_TEXT;
@property(nonatomic , assign) int CMT_PGL_ID;

@end
