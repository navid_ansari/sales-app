//
//  FormulaBean.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SISRequest.h"

@interface FormulaBean : NSObject

@property (nonatomic, strong) SISRequest *request;
@property (nonatomic, assign) long premium;
@property (nonatomic, assign) long ANSAP;
@property (nonatomic, assign) long DiscountedPremium;
@property (nonatomic, assign) long QuotedAnnualizedPremium;
@property (nonatomic, assign) int CounterVariable;
@property (nonatomic, assign) double AdminCharges;
@property (nonatomic, assign) double AmountForServiceTax;
@property (nonatomic, assign) double NetPremHolderAcc;
@property (nonatomic, assign) double NetPHAmtForLoyalBonus;
@property (nonatomic, assign) Boolean CNT_MINUS;
@property (nonatomic, assign) long QuotedModalPremium;
@property (nonatomic, assign) long ServiceTaxRider;
@property (nonatomic, assign) long RiderPremium;
@property (nonatomic, assign) double WithdrawalAmount;
@property (nonatomic, assign) long RC;
@property (nonatomic, assign) int MonthCount;

//SGP..
@property (nonatomic, assign) double premiumDiscount;
@property (nonatomic, assign) double AGAI;
@property (nonatomic, assign) double AGAISSV;

//ULIP..
@property (nonatomic, assign) double serviceTaxMain;
@property (nonatomic, assign) double serviceTaxFirstYear;
@property (nonatomic, assign) double serviceTaxRenewal;
@property (nonatomic, assign) long ULIPAP;

//Added on 31 dec for freedom
@property (nonatomic, assign) double RBV;
@property (nonatomic, assign) double doubleAGAI;


@end
