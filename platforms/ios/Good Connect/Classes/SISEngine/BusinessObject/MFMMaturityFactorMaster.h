//
//  MFMMaturityFactorMaster.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MFMMaturityFactorMaster : NSObject

@property(nonatomic, assign) int MFM_ID;
@property(nonatomic, assign) int MFM_PNL_ID;
@property(nonatomic, assign) int MFM_BAND;
@property(nonatomic, assign) int MFM_AGE;
@property(nonatomic, strong) NSString *MFM_USE_FLG;
@property(nonatomic, assign) double MFM_VALUE;
//SIP
@property (nonatomic, retain) NSString *MFM_SEX;

@end
