//
//  AgentDetail.h
//  LifePlanner
//
//  Created by admin on 02/01/15.
//  Copyright (c) 2015 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgentDetail : NSObject

@property (nonatomic, strong) NSString *agentName;
@property (nonatomic, strong) NSString *agentNumber;
@property (nonatomic, strong) NSString *agentContactNumber;
//changes 24th nov
@property (nonatomic, strong) NSString *agentPassportID;

@property (nonatomic, strong) NSString *pscSessionID;



@end
