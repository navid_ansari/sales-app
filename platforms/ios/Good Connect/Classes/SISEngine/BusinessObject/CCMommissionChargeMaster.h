//
//  CCMommissionChargeMaster.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCMommissionChargeMaster : NSObject
@property(nonatomic , assign) int CCM_ID;
@property(nonatomic , assign) int CCM_MIN_TERM;
@property(nonatomic , assign) int CCM_MAX_TERM;
@property(nonatomic , assign) double CCM_COMMISSION;

@end
