//
//  SURSurrenderChargesMaster.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SURSurrenderChargesMaster : NSObject
@property(nonatomic , assign) int SUR_ID;
@property(nonatomic , assign) int SUR_MIN_BAND;
@property(nonatomic , assign) int SUR_MAX_BAND;
@property(nonatomic , assign) long SUR_MIN_PREM;
@property(nonatomic , assign) long SUR_MAX_PREM;
@property(nonatomic , assign) double SUR_CHARGE;
@property(nonatomic , assign) long SUR_AMOUNT;

@end
