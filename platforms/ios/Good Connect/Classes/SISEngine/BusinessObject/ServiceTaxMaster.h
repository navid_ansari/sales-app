//
//  ServiceTaxMaster.h
//  Life Planner
//
//  Created by admin on 15/07/15.
//
//

#import <Foundation/Foundation.h>

@interface ServiceTaxMaster : NSObject

@property (nonatomic, retain) NSString *Stm_type;
@property (nonatomic, assign) double Stm_value;

@end
