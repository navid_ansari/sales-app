//
//  FormulaBean.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "FormulaBean.h"

@implementation FormulaBean

@synthesize request,
premium,
DiscountedPremium,
ANSAP,
QuotedAnnualizedPremium,
CounterVariable,
AdminCharges,
AmountForServiceTax,
NetPremHolderAcc,
NetPHAmtForLoyalBonus,
CNT_MINUS,
QuotedModalPremium,
ServiceTaxRider,
RiderPremium,
WithdrawalAmount,
RC,
MonthCount,
premiumDiscount,
AGAI,serviceTaxMain,serviceTaxFirstYear,serviceTaxRenewal,ULIPAP,AGAISSV,RBV,doubleAGAI;

@end
