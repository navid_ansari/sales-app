//
//  XMMXmlMetadataMaster.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMMXmlMetadataMaster : NSObject

@property(nonatomic, strong) NSString *PNL_CODE;
@property(nonatomic, assign) int SRNO;
@property(nonatomic, strong) NSString *ELEMENT_NAME;

@end
