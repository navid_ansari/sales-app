//
//  ValidatePrem.h
//  Life Planner
//
//  Created by admin on 31/07/15.
//
//

#import <Foundation/Foundation.h>

@interface ValidatePrem : NSObject

@property (nonatomic, assign) long maxPrem;
@property (nonatomic, assign) long minPrem;

@end
