//
//  SISRequest.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SISRequest : NSObject

@property (nonatomic, strong) NSString *InsuredName;
@property (nonatomic, assign) int InsuredAge;
@property (nonatomic, strong) NSString *InsuredDOB;
@property (nonatomic, strong) NSString *InsuredSex;
@property (nonatomic, strong) NSString *InsuredOccupation;
@property (nonatomic, strong) NSString *InsuredOccupation_Desc;
@property (nonatomic, strong) NSString *InsuredSmokerStatus;
@property (nonatomic, strong) NSString *OwnerName;
@property (nonatomic, strong) NSString *OwnerOccupation;
@property (nonatomic, strong) NSString *OwnerOccupationDesc;
@property (nonatomic, strong) NSString *OwnerSex;
@property (nonatomic, strong) NSString *PaymentMode;
@property (nonatomic, assign) int OwnerAge;

@property (nonatomic, strong) NSString *ProposerName;
@property (nonatomic, assign) NSInteger ProposerAge;
@property (nonatomic, strong) NSString *ProposerDOB;
@property (nonatomic, strong) NSString *ProposerOccupation;
@property (nonatomic, strong) NSString *ProposerOccupation_Desc;
@property (nonatomic, strong) NSString *ProposerSex;
@property (nonatomic, strong) NSString *ProposerType;
@property (nonatomic, assign) NSInteger PlanGroupID;
@property (nonatomic, strong) NSString *BasePlan;
@property (nonatomic, assign) long long Sumassured;
@property (nonatomic, assign) NSInteger Units;
@property (nonatomic, assign) long BasePremium;
@property (nonatomic, assign) long BasePremiumAnnual;
@property (nonatomic, strong) NSString *Frequency;
@property (nonatomic, assign) NSInteger PolicyTerm;
@property (nonatomic, assign) NSInteger PremiumPayingTerm;
@property (nonatomic, strong) NSString *Prem_Multiplier;
@property (nonatomic, strong) NSString *PolicyOption;
@property (nonatomic, strong) NSString *ProductType;
@property (nonatomic, strong) NSString *ProposalDate;
@property (nonatomic, strong) NSString *ProposalNo;
@property (nonatomic, strong) NSString *StartPoint;
@property (nonatomic, strong) NSString *TataEmployee;
@property (nonatomic, strong) NSString *Commission;
@property (nonatomic, strong) NSString *AgentCode;
@property (nonatomic, strong) NSString *AgentName;
@property (nonatomic, strong) NSMutableArray *RiderList;
@property (nonatomic, strong) NSMutableArray *FundList;
@property (nonatomic, strong) NSString *OperationType;
@property (nonatomic, strong) NSString *AgeProofFlag;
@property (nonatomic, strong) NSString *Premium_Mul;
@property (nonatomic, strong) NSString *RPU_Year;
@property (nonatomic, strong) NSString *TaxSlab;
@property (nonatomic, strong) NSString *FixWithDAmt;
@property (nonatomic, strong) NSString *FixWithDstYr;
@property (nonatomic, strong) NSString *FixWithDEndYr;
@property (nonatomic, strong) NSString *FixTopUpAmt;
@property (nonatomic, strong) NSString *FixTopUpStartYr;
@property (nonatomic, strong) NSString *FixTopUpEndYr;
@property (nonatomic, strong) NSString *VarWithDAmtYr;
@property (nonatomic, strong) NSString *VarTopUpAmtYr;
@property (nonatomic, strong) NSString *FundPerform;
@property (nonatomic, strong) NSString *SmartDebtFund;
@property (nonatomic, strong) NSString *SmartEquityFund;
@property (nonatomic, assign) double SmartDebtFundFMC;
@property (nonatomic, assign) double SmartEquityFundFMC;
@property (nonatomic, strong) NSString *AAA;
@property (nonatomic, strong) NSString *TopUpWDSelected;
@property (nonatomic, strong) NSString *AAASelected;
@property (nonatomic, strong) NSString *SmartSelected;
@property (nonatomic, strong) NSString *FpSelected;
@property (nonatomic, strong) NSString *Exp_Bonus_Rate;
@property (nonatomic, strong) NSString *AgeProof;
@property(nonatomic,assign)int incomeBooster;
@property (nonatomic, strong) NSString *BusinessType;
@property (nonatomic, strong) NSString *Illus_Page_No;

@property (nonatomic, assign) int percent;

//******************
@property (nonatomic, retain) NSString *AgentCITI;



@end
