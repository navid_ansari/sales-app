//
//  RDLChargesValue.h
//  LifePlanner
//
//  Created by Akhil Kulkarni on 04/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDLChargesValue : NSObject
@property(nonatomic , assign) int RCS_ID;
@property(nonatomic , assign) int RCS_PNL_ID;
@property(nonatomic , assign) int RCS_RDL_ID;
@property(nonatomic , strong) NSString *RCS_GENDER;
@property(nonatomic , strong) NSString *RCS_BAND;
@property(nonatomic , assign) int RCS_AGE;
@property(nonatomic , strong) NSString *RCS_OCCUR;
@property(nonatomic , assign) double RCS_NSP;
@property(nonatomic , strong) NSString *RCS_USE;
@property(nonatomic , assign) int RCS_NUM;
@property(nonatomic , assign) int RCS_DUR1;
@property(nonatomic , assign) double RCS_CVAL1;
@property(nonatomic , assign) int RCS_DUR2;
@property(nonatomic , assign) double RCS_CVAL2;
@property(nonatomic , assign) int RCS_DUR3;
@property(nonatomic , assign) double RCS_CVAL3;
@property(nonatomic , assign) int RCS_DUR4;
@property(nonatomic , assign) double RCS_CVAL4;
@property(nonatomic , assign) int RCS_DUR5;
@property(nonatomic , assign) double RCS_CVAL5;
@property(nonatomic , assign) int RCS_DUR6;
@property(nonatomic , assign) double RCS_CVAL6;
@property(nonatomic , assign) int RCS_DUR7;
@property(nonatomic , assign) double RCS_CVAL7;
@property(nonatomic , assign) int RCS_DUR8;
@property(nonatomic , assign) double RCS_CVAL8;
@property(nonatomic , assign) int RCS_DUR9;
@property(nonatomic , assign) double RCS_CVAL9;
@property(nonatomic , assign) int RCS_DUR10;
@property(nonatomic , assign) double RCS_CVAL10;

@end
