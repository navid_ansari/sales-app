//
//  iLLusPageList.h
//  Life Planner
//
//  Created by admin on 22/07/15.
//
//

#import <Foundation/Foundation.h>

@interface iLLusPageList : NSObject

@property (nonatomic, assign) int IPL_PGL_ID;
@property (nonatomic, assign) int IPL_MIN_PT;
@property (nonatomic, assign) int IPL_MAX_PT;
@property (nonatomic, retain) NSString* IPL_MIN_PAGE;
@property (nonatomic, retain) NSString* IPL_MAX_PAGE;

@end
