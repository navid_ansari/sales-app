//
//  ModelMaster.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SISRequest.h"
#import "AddressBO.h"
#import "AgentDetail.h"

@interface ModelMaster : NSObject

@property (nonatomic, strong) SISRequest *sisRequest;
@property (nonatomic, assign) long BaseAnnualizedPremium;
@property (nonatomic, strong) NSString *AnnualPremium;

@property (nonatomic, assign) long DiscountedBasePremium;
@property (nonatomic, assign) long QuotedAnnualizedPremium;
@property (nonatomic, assign) long TotalModalPremium;
@property (nonatomic, assign) long ModalPremiumForNSAP;
@property (nonatomic, assign) long ModalPremium;
@property (nonatomic, assign) long STRiderModalPremium;
//changes 18th feb ||| changes 19th feb Reverted
@property (nonatomic, assign) long TotalRiderModalPremium;
@property (nonatomic, assign) double PremiumMultiple;
@property (nonatomic, assign) long AnnualizedPremiumWithOccLoading;
@property (nonatomic, strong) NSMutableArray *riderData;

@property (nonatomic, strong) NSMutableDictionary *PremiumAllocationCharges;
@property (nonatomic, strong) NSMutableDictionary *AmountForInvest;
@property (nonatomic, strong) NSMutableDictionary *OtherBenefitCharges;
@property (nonatomic, strong) NSMutableDictionary *MortalityCharges;
@property (nonatomic, strong) NSMutableDictionary *PolicyAdminCharges;
@property (nonatomic, strong) NSMutableDictionary *ApplicableServiceTaxPH10;
@property (nonatomic, strong) NSMutableDictionary *DeathBenefitAtEndOfPolicyYear;
@property (nonatomic, strong) NSMutableDictionary *GuaranteedMaturityAdditionPH10;
@property (nonatomic, strong) NSMutableDictionary *SurrenderValuePH10;
@property (nonatomic, strong) NSMutableDictionary *FundManagementChargePH10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePH10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValueBeforeFMCPH10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePostFMCPH10;
@property (nonatomic, strong) NSMutableDictionary *TotalFundValueAtEndPolicyYrPH10;
@property (nonatomic, strong) NSMutableDictionary *TotalChargeForPh10;
@property (nonatomic, strong) NSMutableDictionary *LoyalityChargeForPh10;
@property (nonatomic, strong) NSMutableDictionary *CommissionPayable;
@property (nonatomic, strong) NSString *basePremium;
@property (nonatomic, assign) long QuotedModalPremium;
@property (nonatomic, assign) double NetYield8P;
@property (nonatomic, strong) NSString *header;
@property (nonatomic, assign) double NetYield8TW;
@property (nonatomic, assign) double dfmc;

@property (nonatomic, assign) double NetYield8SMart;
@property (nonatomic, strong) NSMutableDictionary *TotalChargesForPH6;
@property (nonatomic, strong) NSMutableDictionary *ApplicableSTPH6;
@property (nonatomic, strong) NSMutableDictionary *FundMgmtChargePH6;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePH6;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValueBeforeFMCPH6;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePostFMCPH6;
@property (nonatomic, strong) NSMutableDictionary *GuaranteedMaturityAdditionPH6;
@property (nonatomic, strong) NSMutableDictionary *TotalFundValueAtEndPolicyYrPH6;
@property (nonatomic, strong) NSMutableDictionary *LoyalityChargeForPh6;
@property (nonatomic, strong) NSMutableDictionary *SurrenderValuePH6;
@property (nonatomic, strong) NSMutableDictionary *MortalityChargesPH6;
@property (nonatomic, strong) NSMutableDictionary *DeathBenefitAtEndOfPolicyYearPH6;





@property (nonatomic, strong) NSMutableDictionary *MortalityCharges_SMART10;
@property (nonatomic, strong) NSMutableDictionary *ApplicableServiceTaxPH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *GuaranteedMaturityAdditionPH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *SurrenderValuePH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *FundManagementChargePH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValueBeforeFMCPH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePostFMCPH10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *TotalChargeForPh10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *LoyalityChargeForPh10_SMART10;
@property (nonatomic, strong) NSMutableDictionary *TotalDeathBenefitPH10_SMART10;

@property (nonatomic, strong) NSMutableDictionary *LifeCoverage;
@property (nonatomic, strong) NSMutableDictionary *MaturityValue;
@property (nonatomic, strong) NSMutableDictionary *GuaranteedAnnualIncome;
@property (nonatomic, strong) NSMutableDictionary *NONGuaranteedSurrBenefit;
@property (nonatomic, strong) NSMutableDictionary *GuaranteedSurrValue;

@property (nonatomic, strong) NSMutableDictionary *TotalMaturityValue;
@property (nonatomic, strong) NSMutableDictionary *VSRBonus4;
@property (nonatomic, strong) NSMutableDictionary *TerminalBonus4;
@property (nonatomic, strong) NSMutableDictionary *TotalDeathBenefit4;
@property (nonatomic, strong) NSMutableDictionary *TotalDeathBenefit;
@property (nonatomic, assign) long TotalCashDividend4;
@property (nonatomic, assign) long TotalCashDividend8;
@property (nonatomic, assign) long TotalGAnnualCoupon;

@property (nonatomic, strong) NSMutableDictionary *GuaranteedInflationCover;
@property (nonatomic, strong) NSMutableDictionary *VSRBonus8;
@property (nonatomic, strong) NSMutableDictionary *NONGuaranteed4SurrBenefit;
@property (nonatomic, strong) NSMutableDictionary *TerminalBonus8;
@property (nonatomic, strong) NSMutableDictionary *NONGuaranteedSurrBenefit_8;
@property (nonatomic, strong) NSMutableDictionary *TotalDeathBenefit8;
@property (nonatomic, assign) long RenewalPrem_A;
@property (nonatomic, assign) long RenewalPrem_SA;
@property (nonatomic, assign) long RenewalPrem_Q;
@property (nonatomic, assign) long RenewalPrem_M;
@property (nonatomic, strong) NSMutableDictionary *LifeCoverageAltSCE;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioMaturityVal;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioTotalDeathBenefit8;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioTotalDeathBenefit4;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioMaturityValue4;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioMaturityValue8;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioToMatBenCurrRate;
@property (nonatomic, strong) NSMutableDictionary *SurrenderBenefit8;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioSurrBenCurrRate;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioPremPaidCumulativeCurrRate;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioTaxSaving;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioAnnualizedEffectivePremium;
@property (nonatomic, strong) NSMutableDictionary *ALTScenarioCumulativeEffectivePremium;




@property (nonatomic, assign) long TotalRenewalPremium_A;
@property (nonatomic, assign) long TotalRenewalPremium_M;
@property (nonatomic, assign) long TotalRenewalPremium_Q;
@property (nonatomic, assign) long TotalRenewalPremium_SA;
@property (nonatomic, assign) long RenewalPremiumST_Q;
@property (nonatomic, assign) long RenewalPremiumST_SA;
@property (nonatomic, assign) long RenewalPremiumST_A;
@property (nonatomic, assign) long RenewalPremiumST_M;
@property (nonatomic, assign) long serviceTaxBasePaln;
@property (nonatomic, strong) NSMutableDictionary *TotalMaturityBenefit8;
@property (nonatomic, strong) NSMutableDictionary *TotalMaturityBenefit4;
@property (nonatomic, strong) NSMutableDictionary *AltScenarioTotMatBenCurrentRate;
@property (nonatomic, strong) NSMutableDictionary *TotalRiderAnnualPremium;
@property (nonatomic, strong) AddressBO *addressBOObj;
@property (nonatomic, strong) NSMutableDictionary *TotalPremiumAnnual;
@property (nonatomic, strong) NSMutableDictionary *AltScenarioTotalDeathBenefitCR;
@property (nonatomic, strong) NSMutableDictionary *AltScenario4MaturityBenefitTax;
@property (nonatomic, assign) long TotalMaturityValuePlusGAI;
@property (nonatomic, assign) long serviceTax;
@property (nonatomic, assign) long TotalModalPremiumPayble;
@property (nonatomic, assign) long TotModalPremiumPayble;




@property (nonatomic, strong)   NSString *UINNumber;
@property (nonatomic, strong)   NSString *planDescription;
@property (nonatomic, strong)   NSString *errorMessageAStr;
@property (nonatomic, strong)   NSString *PlanFeatureStr;
@property (nonatomic, strong) AgentDetail *Agent_interMedDetail;



@property (nonatomic, strong) NSMutableDictionary *RegularFundValueFP;
@property (nonatomic, strong) NSMutableDictionary *SurrenderValueFP;
@property (nonatomic, strong) NSMutableDictionary *DeathBenefitAtEndOfPolicyYearFP;
@property (nonatomic, strong) NSMutableDictionary *TopUpPremium;
@property (nonatomic, strong) NSMutableDictionary *WithDrawalAmount;
@property (nonatomic, strong) NSMutableDictionary *RegularFundValuePostFMC;
@property (nonatomic, strong) NSMutableDictionary *TopUpLoyalty;
@property (nonatomic, strong) NSMutableDictionary *TopUpRegularFundValueEndOfYear;
@property (nonatomic, strong) NSMutableDictionary *TopUpDeathBenefit;
@property (nonatomic, strong) NSMutableDictionary *TopUpSurreValue;
@property (nonatomic, strong) NSMutableDictionary *MortalityCharges_AS3;
@property (nonatomic, strong) NSMutableDictionary *MortalityCharges_SMART6;
@property (nonatomic, strong) NSMutableDictionary *VariableBonus;
@property (nonatomic, strong)   NSString *AltHeader;
@property (nonatomic, assign) double InitEquityFundFactor;
@property (nonatomic, assign) double InitDebtFundFactor;
@property (nonatomic, assign) long MLGP_TotalRB8;
@property (nonatomic, assign) long MLGP_TotalRB4;
@property (nonatomic, assign) long MLGP_TotalGAI;

//Added by Amruta on 20/8/15 for FG
@property (nonatomic, strong)   NSString *CommissionText;
@property(nonatomic,assign) long TotalAnnRiderModalPremium;
@property (nonatomic , assign) long ModalPremiumForAnnNSAP;
@property (nonatomic, assign) double ServiceTaxRenewal;

//********************************SGP*********************************
@property (nonatomic, retain) NSMutableDictionary *AccGAI;
@property (nonatomic, assign) double premiumDiscount;
//********************************SGP*********************************

//Added by Amruta on 24/8/15 for MBP
@property (nonatomic, retain) NSMutableDictionary *GAIAll;
@property (nonatomic,assign) long RenewalModalPremium;


//Added by Amruta on 8/9/15 for SMART7
@property (nonatomic,assign) long TotalRenewalPremium_RS_Q;

//added on 31st dec for freedom
@property (nonatomic, retain) NSMutableDictionary *AccGuaranteedAnnualIncome;
@property (nonatomic, assign) BOOL isCombo;

//11 jan
@property (nonatomic, retain) NSMutableDictionary *BasicSumAssured;
@property (nonatomic, assign) long TOTALRB4_MIP;
@property (nonatomic, assign) long TOTALRB8_MIP;
@property (nonatomic, assign) long TOTALGAI_MIP;

//SIP
@property (nonatomic, retain) NSMutableDictionary *totalGuarBenefit;

//good kid
@property (nonatomic,retain) NSMutableDictionary *MilestoneAdd8Dict;
@property (nonatomic,retain) NSMutableDictionary *MilestoneAdd4Dict;
@property (nonatomic,retain) NSMutableDictionary *MoneyBackBenefitDict;

@end
