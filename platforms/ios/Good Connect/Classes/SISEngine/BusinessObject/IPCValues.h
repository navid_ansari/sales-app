//
//  IPCValues.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPCValues : NSObject

@property(nonatomic, assign) int IPC_ID;
@property(nonatomic, assign) int IPC_PNL_ID;
@property(nonatomic, strong) NSString *IPC_SEX;
@property(nonatomic, assign) int IPC_TERM;
@property(nonatomic, assign) int IPC_AGE;
@property(nonatomic, assign) double IPC_PREM_MULT;
@property(nonatomic, assign) int IPC_BAND;
@property(nonatomic, strong) NSString *SMOKER_FLAG;

@end
