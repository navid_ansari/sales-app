//
//  GAIGuarAnnIncChMaster.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GAIGuarAnnIncChMaster : NSObject

@property(nonatomic, assign) int GAI_ID;
@property(nonatomic, assign) int GAI_PNL_ID;
@property(nonatomic, assign) int GAI_MIN_TERM;
@property(nonatomic, assign) int GAI_MAX_TERM;
@property(nonatomic, assign) int GAI_MIN_PPT;
@property(nonatomic, assign) int GAI_MAX_PPT;
@property(nonatomic, assign) long GAI_MIN_PREM;
@property(nonatomic, assign) long GAI_MAX_PREM;
@property(nonatomic, strong) NSString *GAI_FLAG;
@property(nonatomic, assign) double GAI_CHARGE;

@end
