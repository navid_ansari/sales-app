//
//  PlanFormulaRef.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanFormulaRef : NSObject

@property (nonatomic, strong) NSString *Plan_Code;
@property (nonatomic, strong) NSString *PFR_FRM_CD;
@property (nonatomic, strong) NSString *PFR_FRM_TYPE;
@property (nonatomic, strong) NSString *PFR_FRM_EXP;


@end
