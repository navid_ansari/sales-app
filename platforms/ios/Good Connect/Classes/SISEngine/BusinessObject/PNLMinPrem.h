//
//  PNLMinPrem.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNLMinPrem : NSObject

@property (nonatomic, assign) int PMML_ID;
@property (nonatomic, strong) NSString *PML_PLAN_CD;
@property (nonatomic, strong) NSString *PML_PREMIUM_MODE;
@property (nonatomic, assign) int PML_MIN_PREM_AMT;
@property (nonatomic, assign) int PML_MAX_PREM_AMT;

@end
