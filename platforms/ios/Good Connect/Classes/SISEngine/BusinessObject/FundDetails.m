//
//  FundDetails.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "FundDetails.h"

@implementation FundDetails

@synthesize PNL_PGD_ID,
Code,
fund_Description,
Fund_Mgmt_charge,
Percentage,
PERF_3M,
PERF_6M,
PERF_1Y,
PERF_2Y,
PERF_3Y,
PERF_4Y,
PERF_5Y,
PERF_Inception;

@end
