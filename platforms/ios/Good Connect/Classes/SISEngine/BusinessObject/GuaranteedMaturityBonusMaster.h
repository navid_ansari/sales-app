//
//  GuaranteedMaturityBonusMaster.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuaranteedMaturityBonusMaster : NSObject

@property(nonatomic, assign) int GMM_ID;
@property(nonatomic, assign) int GMM_MIN_BAND;
@property(nonatomic, assign) int GMM_MAX_BAND;
@property(nonatomic, assign) double GMM_BONUS;

@end
