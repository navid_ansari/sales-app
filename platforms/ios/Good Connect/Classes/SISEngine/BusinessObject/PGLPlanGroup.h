//
//  PGLPlanGroup.h
//  LifePlanner
//
//  Created by admin on 25/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGLPlanGroup : NSObject

@property (nonatomic, assign) int PGL_ID;
@property (nonatomic, strong) NSString *PGL_DESCRIPTION;
@property (nonatomic, strong) NSString *PGL_JUVENILE_IND;
@property (nonatomic, strong) NSString *PGL_ADULT_IND;
@property (nonatomic, strong) NSString *PGL_PRODUCT_TYPE;


@end
