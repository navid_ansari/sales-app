//
//  FOPRate.h
//  Life Planner
//
//  Created by admin on 16/10/15.
//
//

#import <Foundation/Foundation.h>

@interface FOPRate : NSObject

@property (nonatomic, assign) int FOP_PNL_ID;
@property (nonatomic, assign) int FOP_TERM;
@property (nonatomic, assign) double FOP_CHARGE;
@property (nonatomic, retain) NSString *FOP_MODE;

@end
