//
//  RMLRiderPremMultiple.h
//  LifePlanner
//
//  Created by admin on 02/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMLRiderPremMultiple : NSObject
@property(nonatomic , assign) int RML_ID;
@property(nonatomic , strong) NSString *RML_RIDER_CD;
@property(nonatomic , strong) NSString *RML_SEX;
@property(nonatomic , assign) int RML_TERM;
@property(nonatomic , assign) int RML_AGE;
@property(nonatomic , assign) double RML_PREM_MULT;
@property(nonatomic , assign) int RML_BAND;

@end
