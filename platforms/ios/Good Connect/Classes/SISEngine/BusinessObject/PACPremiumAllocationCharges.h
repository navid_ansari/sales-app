//
//  PACPremiumAllocationCharges.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACPremiumAllocationCharges : NSObject
@property(nonatomic , assign) int PAC_ID;
@property(nonatomic , assign) int PAC_MIN_BAND;
@property(nonatomic , assign) int PAC_MAX_BAND;
@property(nonatomic , assign) long PAC_MIN_PREM;
@property(nonatomic , assign) long PAC_MAX_PREM;
@property(nonatomic , assign) double PAC_CHARGE;
@end
