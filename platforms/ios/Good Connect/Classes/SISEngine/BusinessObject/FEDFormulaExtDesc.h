//
//  FEDFormulaExtDesc.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FEDFormulaExtDesc : NSObject

@property (nonatomic, strong) NSString *FED_ID;
@property (nonatomic, assign) int FED_OBJ_SEQ;
@property (nonatomic, strong) NSString *FED_OBJ_OPND;
@property (nonatomic, strong) NSString *FED_OBJ_OPND_TYPE;
@property (nonatomic, strong) NSString *FED_OBJ_OPTR;
@property (nonatomic, strong) NSString *FED_OBJ_TYPE;

@end
