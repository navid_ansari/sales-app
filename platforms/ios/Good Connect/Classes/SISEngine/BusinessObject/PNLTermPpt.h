//
//  PNLTermPpt.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNLTermPpt : NSObject

@property (nonatomic, strong) NSString *PNL_CODE;
@property (nonatomic, strong) NSString *TERM_FLAG;
@property (nonatomic, assign) int MIN_TERM;
@property (nonatomic, assign) int MAX_TERM;
@property (nonatomic, strong) NSString *TERM_FORMULA;
@property (nonatomic, assign) int TERM;
@property (nonatomic, strong) NSString *PPT_FLAG;
@property (nonatomic, strong) NSString *PPT_FORMULA;
@property (nonatomic, assign) int MATURITY_AGE;
@property (nonatomic, assign) int MIN_VESTING_AGE;
@property (nonatomic, assign) int PPT1;
@end
