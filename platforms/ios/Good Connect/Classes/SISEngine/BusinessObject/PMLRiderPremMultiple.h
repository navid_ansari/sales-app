//
//  PMLRiderPremMultiple.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMLRiderPremMultiple : NSObject

@property (nonatomic, assign) int PML_ID;
@property (nonatomic, strong) NSString *PML_PNL_ID;
@property (nonatomic, assign) int TERM;
@property (nonatomic, strong) NSString *PPT_FLAG;
@property (nonatomic, strong) NSString *PPT_FORMULA;
@property (nonatomic, assign) int PPT;
@property (nonatomic, assign) int MATURITY_AGE;
@property (nonatomic, assign) int MIN_VESTING_AGE;


@end
