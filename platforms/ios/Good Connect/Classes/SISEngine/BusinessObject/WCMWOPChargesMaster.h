//
//  WCMWOPChargesMaster.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WCMWOPChargesMaster : NSObject
@property(nonatomic , assign) int WCM_WOP_ID;
@property(nonatomic , assign) int WCM_AGE;
@property(nonatomic , assign) int WCM_BAND;
@property(nonatomic , assign) double WCM_CHARGE;
@end
