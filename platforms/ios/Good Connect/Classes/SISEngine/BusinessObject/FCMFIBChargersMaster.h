//
//  FCMFIBChargersMaster.h
//  LifePlanner
//
//  Created by admin on 03/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FCMFIBChargersMaster : NSObject
@property(nonatomic , assign) int FCM_FIB_ID;
@property(nonatomic , assign) int FCM_AGE;
@property(nonatomic , assign) int FCM_BAND;
@property(nonatomic , assign) double FCM_CHARGE;
@end
