//
//  AAAFMCValues.m
//  LifePlanner
//
//  Created by Akhil Kulkarni on 04/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "AAAFMCValues.h"

@implementation AAAFMCValues
@synthesize afc_fmc,afc_large_cap,afc_max_age,afc_min_age,afc_paln_id,afc_whole_life_income;

@end
