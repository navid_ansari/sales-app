//
//  PmdPlanPremiumMultDisc.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PmdPlanPremiumMultDisc : NSObject

@property (nonatomic, strong) NSString *PMD_PLAN_CODE;
@property (nonatomic, assign) long PMD_MIN_SA;
@property (nonatomic, assign) long PMD_MAX_SA;
@property (nonatomic, assign) double PMD_DISC_RATE;

@end
