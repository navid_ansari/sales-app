//
//  PMVPlanMaturityValue.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMVPlanMaturityValue : NSObject

@property (nonatomic, assign) int PMV_ID;
@property (nonatomic, assign) int PMV_PNL_ID;
@property (nonatomic, strong) NSString *PMV_GENDER;
@property (nonatomic, strong) NSString *PMV_BAND;
@property (nonatomic, assign) int PMV_AGE;
@property (nonatomic, strong) NSString *PMV_OCCUR;
@property (nonatomic, assign) double PMV_NSP;
@property (nonatomic, strong) NSString *PMV_USE;
@property (nonatomic, assign) int PMV_NUM;
@property (nonatomic, assign) int PMV_DUR1;
@property (nonatomic, assign) double PMV_CVAL1;
@property (nonatomic, assign) int PMV_DUR2;
@property (nonatomic, assign) double PMV_CVAL2;
@property (nonatomic, assign) int PMV_DUR3;
@property (nonatomic, assign) double PMV_CVAL3;
@property (nonatomic, assign) int PMV_DUR4;
@property (nonatomic, assign) double PMV_CVAL4;
@property (nonatomic, assign) int PMV_DUR5;
@property (nonatomic, assign) double PMV_CVAL5;
@property (nonatomic, assign) int PMV_DUR6;
@property (nonatomic, assign) double PMV_CVAL6;
@property (nonatomic, assign) int PMV_DUR7;
@property (nonatomic, assign) double PMV_CVAL7;
@property (nonatomic, assign) int PMV_DUR8;
@property (nonatomic, assign) double PMV_CVAL8;
@property (nonatomic, assign) int PMV_DUR9;
@property (nonatomic, assign) double PMV_CVAL9;
@property (nonatomic, assign) int PMV_DUR10;
@property (nonatomic, assign) double PMV_CVAL10;

@end
