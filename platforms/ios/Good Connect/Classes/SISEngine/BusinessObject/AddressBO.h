//
//  AddressBO.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressBO : NSObject

@property (nonatomic, strong) NSString *CO_NAME;
@property (nonatomic, strong) NSString *CO_ADDRESS;
@property (nonatomic, strong) NSString *CO_HELPLINE;
@property (nonatomic, strong) NSString *SIS_VERSION;
@property (nonatomic, strong) NSString *FILLER1;


@end
