//
//  PremiumBoost.h
//  Life Planner
//
//  Created by admin on 19/02/16.
//
//

#import <Foundation/Foundation.h>

@interface PremiumBoost : NSObject

@property (nonatomic, retain) NSString *LPB_PLAN_CODE;
@property (nonatomic, retain) NSString *LPB_RIDER_CODE;
@property (nonatomic, assign) long LPB_MIN_PREMIUM;
@property (nonatomic, assign) long LPB_MAX_PREMIUM;
@property (nonatomic, assign) double LPB_BOOST_RATE;

@end
