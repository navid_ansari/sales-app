//
//  PBAProductBand.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBAProductBand : NSObject

@property (nonatomic, strong) NSString *PBA_PLAN_CODE;
@property (nonatomic, strong) NSString *PBA_RIDER_CODE;
@property (nonatomic, strong) NSString *PBA_TYPE;
@property (nonatomic, strong) NSString *PBA_BAND;
@property (nonatomic, assign) long PBA_SUM_ASSURED;
@property (nonatomic, assign) int PBA_SEQ;

@end
