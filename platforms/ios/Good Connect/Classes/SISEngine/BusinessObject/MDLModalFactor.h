//
//  MDLModalFactor.h
//  LifePlanner
//
//  Created by admin on 25/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDLModalFactor : NSObject

@property (nonatomic, assign) int MDL_ID;
@property (nonatomic, assign) int MDL_PGL_ID;
@property (nonatomic, strong) NSString *MDL_PAY_FREQ;
@property (nonatomic, assign) double MDL_MODAL_FACTOR;

@end
