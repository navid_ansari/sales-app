//
//  ACMAdminChargeMaster.h
//  Life Planner
//
//  Created by admin on 07/07/15.
//
//

#import <Foundation/Foundation.h>

@interface ACMAdminChargeMaster : NSObject

@property (nonatomic, assign) int ACM_ID;
@property (nonatomic, assign) long ACM_MIN_PREM;
@property (nonatomic, assign) long ACM_MAX_PREM;
@property (nonatomic, assign) long ACM_CHARGE;


@end
