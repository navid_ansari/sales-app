//
//  PNLPlan.h
//  LifePlanner
//
//  Created by admin on 25/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNLPlan : NSObject

@property (nonatomic, assign) int PNL_ID;
@property (nonatomic, strong) NSString *PNL_CODE;
@property (nonatomic, strong) NSString *PNL_DESCRIPTION;
@property (nonatomic, assign) int PNL_PGL_ID;
@property (nonatomic, strong) NSString *PNL_TYPE;
@property (nonatomic, strong) NSDate *PNL_EFFECTIVE_DATE;
@property (nonatomic, strong) NSDate *PNL_END_DATE;
@property (nonatomic, assign) int PNL_MIN_AGE;
@property (nonatomic, assign) int PNL_MAX_AGE;
@property (nonatomic, assign) int PNL_MIN_SUM_ASSURED;
@property (nonatomic, assign) long PNL_MAX_SUM_ASSURED;
@property (nonatomic, assign) int PNL_MIN_OCCUPATION_CLASS;
@property (nonatomic, assign) int PNL_MAX_OCCUPATION_CLASS;
@property (nonatomic, assign) int PNL_PREMIUM_MULTIPLE;
@property (nonatomic, assign) int PNL_MAX_PREMIUM_PAYING_TERM;
@property (nonatomic, strong) NSString *PNL_PMAXUSE;
@property (nonatomic, strong) NSString *PNL_FREQUENCY;
@property (nonatomic, assign) double PNL_SERVICE_TAX;
@property (nonatomic, assign) int PNL_PREM_MULT_FORMULA;
@property (nonatomic, strong) NSString* UIN;
@property (nonatomic, assign) int PNL_SA_MULTIPLE;
@property (nonatomic, strong) NSString *PNL_AGENTTYPE;
@property (nonatomic, strong) NSString *PNL_ANNUITY_FREQUENCY;
@property (nonatomic, strong) NSString *URALLOW;
@property (nonatomic, assign) double PNL_URPERCENT;
@property (nonatomic, strong) NSString *PNL_START_PT;
@property (nonatomic, strong) NSString *PNL_BONUS_RATE;


@end
