//
//  Rider.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rider : NSObject


@property (nonatomic, assign) long SumAssured;
@property (nonatomic, assign) long Premium;
@property (nonatomic, assign) int RDL_RTL_ID;
@property (nonatomic, assign) int PolTerm;
@property (nonatomic, assign) int PremPayTerm;
@property (nonatomic, assign) long ModalPremium;
@property (nonatomic, assign) long RDLID;

@property (nonatomic, strong) NSString *RDL_UIN;
@property (nonatomic, strong) NSString *RDL_CODE;
@property (nonatomic, strong) NSString *RDL_DESCRIPTION;
@property (nonatomic, assign) double RDL_SERVICE_TAX;
@property (nonatomic, retain) NSString *ISSIMPLIFIED;
@property (nonatomic, retain) NSString *ISANNUITYPRODUCT;
@property (nonatomic, retain) NSString *ISBACKDATE;
@property (nonatomic, retain) NSString *NBFC;
@property (nonatomic, assign) int NOOFMONTH;
@property (nonatomic, assign) int FIXCVGID;
@property (nonatomic, assign) int MINOCC;
@property (nonatomic, assign) int MAXOCC;





@end
