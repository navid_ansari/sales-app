//
//  SISRequest.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "SISRequest.h"

@implementation SISRequest
@synthesize InsuredName,
            InsuredAge,
            InsuredDOB,
            InsuredSex,
            InsuredOccupation,
            InsuredOccupation_Desc,
            InsuredSmokerStatus,
            ProposerName,
            ProposerAge,
            ProposerDOB,
            ProposerOccupation,
            ProposerOccupation_Desc,
            ProposerSex,
            PlanGroupID,
            BasePlan,
            Sumassured,
            Units,
            BasePremium,
            BasePremiumAnnual,
            Frequency,
            PolicyTerm,
            PremiumPayingTerm,
            Prem_Multiplier,
            PolicyOption,
            ProductType,
            ProposalDate,
            ProposalNo,
            StartPoint,
            TataEmployee,
            Commission,
            AgentCode,
            AgentName,
            RiderList,
            FundList,
            OperationType,
            AgeProofFlag,
            Premium_Mul,
            RPU_Year,
            TaxSlab,
            FixWithDAmt,
            FixWithDstYr,
            FixWithDEndYr,
            FixTopUpAmt,
            FixTopUpStartYr,
            FixTopUpEndYr,
            VarWithDAmtYr,
            VarTopUpAmtYr,
            FundPerform,
            SmartDebtFund,
            SmartEquityFund,
            SmartDebtFundFMC,
            SmartEquityFundFMC,
            AAA,
            TopUpWDSelected,
            AAASelected,
            SmartSelected,
            FpSelected,
            Exp_Bonus_Rate,
            AgeProof,
            OwnerAge,
            Illus_Page_No,
            incomeBooster,
			BusinessType,
            percent;


@end
