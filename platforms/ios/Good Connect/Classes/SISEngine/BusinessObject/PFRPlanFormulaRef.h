//
//  PFRPlanFormulaRef.h
//  LifePlanner
//
//  Created by admin on 27/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFRPlanFormulaRef : NSObject


@property (nonatomic, strong) NSString *PER_PLAN_CD;
@property (nonatomic, strong) NSString *PER_FRM_CD;
@property (nonatomic, strong) NSString *PER_FRM_TYPE;
@property (nonatomic, strong) NSString *PER_FRM_EXP;

@end
