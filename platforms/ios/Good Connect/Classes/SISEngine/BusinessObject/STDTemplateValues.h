//
//  STDTemplateValues.h
//  Life Planner
//
//  Created by admin on 22/07/15.
//
//

#import <Foundation/Foundation.h>

@interface STDTemplateValues : NSObject

@property (nonatomic, assign) int STD_PGL_ID;
@property (nonatomic, retain) NSString *STD_TEMP_DESC;
@property (nonatomic, assign) int STD_SEQUENCE;
@property (nonatomic, assign) int STD_MIN_STARTP;
@property (nonatomic, assign) int STD_MAX_STARTP;
@property (nonatomic, assign) int STD_MIN_PT;
@property (nonatomic, assign) int STD_MAX_PT;
@property (nonatomic, assign) int STD_NO_PAGES;
@property (nonatomic, retain) NSString *STD_MANDATORY;
@property (nonatomic, retain) NSString *STD_SHOW;
@property (nonatomic, retain) NSString *STD_UPDATE_BY;
@property (nonatomic, retain) NSString *STD_UPDATE_DT;
@property (nonatomic, retain) NSString *STD_IS_RIDER;
@property (nonatomic, retain) NSString *STD_RDL_CODE;
@property (nonatomic, assign) int STD_MAX_NO_PAGES;

@end
