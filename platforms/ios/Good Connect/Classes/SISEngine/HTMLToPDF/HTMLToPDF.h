////
////  HTMLToPDF.h
////  Life_Planner
////
////  Created by admin on 02/02/15.
////
////
//
#import "NDHTMLtoPDF.h"

@interface HTMLToPDF : NSObject<NDHTMLtoPDFDelegate>{
    __block bool success;
}

@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;

-(BOOL)generatePDF:(NSArray *)params;
-(NSString *)readFileData:(NSArray *)fileParams;
-(NSURL *)getFileURI:(NSArray *)params;
-(NSString *)getContentFromFile:(NSArray *)fileName;
@end
