	////
////  HTMLToPDF.m
////  Life_Planner
////
////  Created by admin on 02/02/15.
////
////
//
#import "HTMLToPDF.h"
//#import "NSString+AESCrypt.h"

@implementation HTMLToPDF

-(BOOL)generatePDF:(NSArray *)params{
    NSLog(@"inside pdf");
    //NSLog(@"the inside generate pdf %@",params);
    
    //NSArray *urlArray =command.arguments;
    //first param : path , second : filename, third : sis or app.
    
    return [self saveToHtml:params];
}

//get file uri from the path
-(NSURL *)getFileURI:(NSArray *)params{
	NSString *fullPath = [self getFileFullPath:[params objectAtIndex:0][1] andWithPath:[params objectAtIndex:0][0]];
	NSURL *url = [NSURL fileURLWithPath:fullPath];
	
	return url;
}

-(NSString *)getFileFullPath:(NSString *)fileName andWithPath:(NSString *)filePath{
	NSString *FileName = [NSString stringWithFormat:@"%@%@",filePath,fileName];
	NSString *FilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:FileName];
	return FilePath;
	
}

-(NSString *)readFileData:(NSArray *)fileParams{
	NSString *fileData = @"";
	
	NSString *fileName = [NSString stringWithFormat:@"%@%@",[fileParams objectAtIndex:1],[fileParams objectAtIndex:0]];
	NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:fileName];
	NSError *error = NULL;
	
	fileData = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
	
	if(error){
        NSData *bytes = [[NSData alloc] initWithContentsOfFile:filePath];
        fileData = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
	}
    
	return fileData;
}

-(BOOL) saveToHtml:(NSArray *)sisData{
    
	BOOL isSuccess = false;
	NSString *fileName = [NSString stringWithFormat:@"%@%@%@",[sisData objectAtIndex:2],[sisData objectAtIndex:0],[sisData objectAtIndex:1]]; ///FromClient/SIS/HTML/160831427409.html
	
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:fileName];
	
	//Added by Sneha 27 Jan 2017
	if ([[sisData objectAtIndex:2] containsString:@"/SIGNATURE/"] || [[sisData objectAtIndex:2] containsString:@"ToClient"]) {
//	if ([[sisData objectAtIndex:2] isEqualToString:@"/FromClient/SIS/SIGNATURE/"]) {
		NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            // Delete previously uploaded imgs
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] == YES) {
                NSString *fileURI = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",[sisData objectAtIndex:2]]];
                NSError *error = nil;
                NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fileURI error:&error];
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF contains[dc] %@",[sisData objectAtIndex:0]];
                NSLog(@"files: %@\ncount: %lu", [dirContents filteredArrayUsingPredicate:pred], (unsigned long)[dirContents filteredArrayUsingPredicate:pred].count);
                for (NSString *oldFile in [dirContents filteredArrayUsingPredicate:pred]) {
                    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@%@%@",documentsDirectory,[sisData objectAtIndex:2],[sisData objectAtIndex:0],oldFile] error:&error];
                }
            }
            NSLog(@"File save to path %@",filePath);
			NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[sisData objectAtIndex:3] options:0];
			NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
			NSLog(@"%@", decodedString);
            [decodedData writeToFile:filePath atomically:YES];
			isSuccess = true;
	} else {
		//if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		@try{
			NSData *fileContents = [[sisData objectAtIndex:3] dataUsingEncoding:NSUTF8StringEncoding];
			[[NSFileManager defaultManager] createFileAtPath:filePath contents:fileContents attributes:nil];
			isSuccess = true;
		}@catch(NSException *ex){
			isSuccess = false;
		}
	}
    NSLog(@"%d", isSuccess);

	return  isSuccess;
}

-(BOOL)createPDF:(NSArray *)_pdfArray{
	__block BOOL isSuccess = false;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    documentsDir = [documentsDir stringByAppendingPathComponent:[_pdfArray objectAtIndex:0]];
    NSLog(@"getPath:%@",documentsDir);
	
    NSString *genPdfPath;
    NSURL *url = [NSURL URLWithString:documentsDir];
	
    if([[_pdfArray objectAtIndex:2]isEqualToString:@"SIS"])
    genPdfPath = [NSString stringWithFormat:@"~/Documents/FromClient/SIS/PDF/%@.pdf",[_pdfArray objectAtIndex:1]];
    if([[_pdfArray objectAtIndex:2]isEqualToString:@"APP"])
    genPdfPath = [NSString stringWithFormat:@"~/Documents/FromClient/APP/PDF/%@.pdf",[_pdfArray objectAtIndex:1]];
    if([[_pdfArray objectAtIndex:2]isEqualToString:@"FHR"])
    genPdfPath = [NSString stringWithFormat:@"~/Documents/FromClient/FHR/PDF/%@.pdf",[_pdfArray objectAtIndex:1]];
    
    NSLog(@"genPDFPath:%@",genPdfPath);
     NSLog(@"URL ::%@",url);
    if(url != nil){
        self.PDFCreator = [NDHTMLtoPDF createPDFWithURL:url pathForPDF:[genPdfPath stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(18, 10, 18, 10) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
            NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
            NSLog(@"%@",result);
            success = true;
            //CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
            
            //[self.commandDelegate sendPluginResult:pluginResult callbackId:_command.callbackId];
			isSuccess = true;
        } errorBlock:^(NDHTMLtoPDF *htmlToPDF) {
            NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
            NSLog(@"%@",result);
        }];
    }
    return  isSuccess;
}

-(NSString *)getContentFromFile:(NSArray *)fileName{
	NSBundle *bund = [NSBundle bundleForClass:[self class]];
	NSString *contents = NULL;
	NSError *error = NULL;
	@try{
//		NSArray *fileNameArr = [[fileName objectAtIndex:0] componentsSeparatedByString:@"/"];
//		NSString * fileNameVal1 =[[[fileNameArr objectAtIndex:fileNameArr.count - 1] componentsSeparatedByString:@"."] objectAtIndex:0];	
//		NSString *filePath1 = [bund pathForResource:fileNameVal ofType:@"html" inDirectory:@"www/templates/FF"];
		
		/* Sneha */
		NSRange range = [[fileName objectAtIndex:0] rangeOfString:@"/" options:NSBackwardsSearch];
		NSArray * fileNameArr = [[[fileName objectAtIndex:0] substringFromIndex:range.location+1] componentsSeparatedByString:@"."];
		NSString *filePath = [bund pathForResource:[fileNameArr objectAtIndex:0] ofType:[fileNameArr objectAtIndex:1] inDirectory:[[fileName objectAtIndex:0] substringToIndex:range.location]];
		/* Sneha */
		
		if (filePath)  {
			contents = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
			if(error){
				NSLog(@"data is not valid");
			}
		}
	}@catch(NSException *ex){
		contents = NULL;
	}
	
	return contents;
}


#pragma mark NDHTMLtoPDFDelegate
- (void)HTMLtoPDFDidSucceed:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
    NSLog(@"%@",result);
}

- (void)HTMLtoPDFDidFail:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
    NSLog(@"%@",result);
}

////added by Drastty
//-(void)readSISPDF:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *SisData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/SIS/PDF"];
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"SISFILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *bytes = [[NSData alloc] initWithContentsOfFile:folderDir];
//   // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    if(bytes!=NULL)
//    {
//        
//        [SisData setObject:@"1" forKey:@"SISFLAG"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//       // NSLog(@"SISINBYTES:%@",str);
//        [SisData setObject:str forKey:@"SIS"];
//    }
//    else
//    [SisData setObject:@"0" forKey:@"SISFLAG"];
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:SisData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//
//}
//
//-(void)readFHRPDF:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *FhrData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/FHR/PDF"];
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"FHRFILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *bytes = [[NSData alloc] initWithContentsOfFile:folderDir];
//    
//    // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    if(bytes!=NULL)
//    {
//        
//        [FhrData setObject:@"1" forKey:@"FILE"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//        
//        // NSLog(@"SISINBYTES:%@",str);
//        if(str!=NULL)
//        [FhrData setObject:str forKey:@"FHR"];
//        else
//        [FhrData setObject:@"0" forKey:@"FILE"];
//    }
//    else
//    [FhrData setObject:@"0" forKey:@"FILE"];
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:FhrData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}
//-(void)readAPPPDF:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *AppData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir;
//    if([[command.arguments objectAtIndex:1]isEqualToString:@"COMBO"])
//    folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/COMBO"];
//    else
//    folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/APP/PDF"];
//    
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"FILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *bytes = [[NSData alloc] initWithContentsOfFile:folderDir];
//    
//    // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    if(bytes!=NULL)
//    {
//        
//        [AppData setObject:@"1" forKey:@"FILE"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//        
//        // NSLog(@"COMBOINBYTES:%@",str);
//        if(str!=NULL)
//        [AppData setObject:str forKey:@"FILECONTENT"];
//        else
//        [AppData setObject:@"0" forKey:@"FILE"];
//    }
//    else
//    [AppData setObject:@"0" forKey:@"FILE"];
//    
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:AppData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}
//
//
///*-(void)readAPPPDF:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *AppData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/APP/PDF"];
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"APPFILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *bytes = [[NSData alloc] initWithContentsOfFile:folderDir];
//    
//    // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    if(bytes!=NULL)
//    {
//        
//        [AppData setObject:@"1" forKey:@"FILE"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//       
//        // NSLog(@"SISINBYTES:%@",str);
//        if(str!=NULL)
//        [AppData setObject:str forKey:@"APP"];
//        else
//            [AppData setObject:@"0" forKey:@"FILE"];
//    }
//    else
//    [AppData setObject:@"0" forKey:@"FILE"];
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:AppData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}*/
//-(void)readAPPIMAGES:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *AppData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/APP/IMAGES"];
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"APPImage_FILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *decryptedData = [NSData dataWithContentsOfFile:folderDir];
//    
//    
//    
//    NSData *bytes = [decryptedData AES128DecryptedDataWithKey:@"tataaiai"];
//    
//    NSData *base64Data = [bytes base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    
//    
//    
//    // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    NSString *str = @"";
//    if(bytes!=NULL)
//    {
//        
//        [AppData setObject:@"1" forKey:@"FILE"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        //NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//        // NSLog(@"SISINBYTES:%@",str);
//        //str=[NSString stringWithFormat:@"data:image/jpg;base64,%@",str];
//        str = [NSString stringWithUTF8String:[base64Data bytes]];
//        if(str!=NULL)
//        [AppData setObject:str forKey:@"APP"];
//        else
//        [AppData setObject:@"0" forKey:@"FILE"];
//    }
//    else
//    [AppData setObject:@"0" forKey:@"FILE"];
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:AppData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}
//-(void)readAPPSIGN:(CDVInvokedUrlCommand*)command
//{
//    NSMutableDictionary *AppData = [[NSMutableDictionary alloc] init];
//    
//    CDVPluginResult* pluginResult = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//    NSString *documentsDir = [paths objectAtIndex:0];
//    NSString *folderDir=[documentsDir stringByAppendingPathComponent:@"/FromClient/APP/SIGNATURE"];
//    folderDir=[folderDir stringByAppendingPathComponent:[command.arguments objectAtIndex:0]];
//    NSLog(@"APPSIGN_FILEPATH%@",folderDir);
//    // NSString *content = [NSString stringWithContentsOfFile:folderDir encoding:NSUTF8StringEncoding error:NULL];
//    NSData *bytes = [NSData dataWithContentsOfFile:folderDir];
//    
//    //NSData *bytes = [decryptedData AES128DecryptedDataWithKey:@"tataaiai"];
//    
//    NSData *base64Data = [bytes base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    
//    
//    
//    // NSLog(@"Bytes:%@", bytes);
//    //  NSLog(@"SISFILE%@",content);
//    NSString *str = @"";
//    if(bytes!=NULL)
//    {
//        
//        [AppData setObject:@"1" forKey:@"FILE"];
//        // NSData *sisContent=[NSData dataWithContentsOfFile:folderDir];
//        //NSString *str = [bytes base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//        // NSLog(@"SISINBYTES:%@",str);
//        //str=[NSString stringWithFormat:@"data:image/jpg;base64,%@",str];
//        str = [NSString stringWithUTF8String:[base64Data bytes]];
//        if(str!=NULL)
//        [AppData setObject:str forKey:@"APP"];
//        else
//        [AppData setObject:@"0" forKey:@"FILE"];
//    }
//    else
//    [AppData setObject:@"0" forKey:@"FILE"];
//    pluginResult=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:AppData];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}
@end
