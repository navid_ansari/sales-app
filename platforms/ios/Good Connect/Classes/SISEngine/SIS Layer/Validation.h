////
////  Validation.h
////  Life_Planner
////
////  Created by admin on 30/01/15.
////
////
//
//#import <Foundation/Foundation.h>
//
//#import "ModelMaster.h"
//#import "DataMapper.h"
//#import "GenerateSIS.h"
//#import "Constant.h"
//#import "AppDelegate.h"
//
//@interface Validation : CDVPlugin
//@property (nonatomic, assign) NSInteger versionNo;
//
//-(void )validateAnnualizedPremium:(CDVInvokedUrlCommand*)command;
//-(NSMutableDictionary *)validatePremium:(NSDictionary *)_request;
//-(void)convertFileToContent:(CDVInvokedUrlCommand *)command;
//-(void)FHRDeviationContent:(CDVInvokedUrlCommand *)command;
//
//-(void)checkVersionApp;
//@end
