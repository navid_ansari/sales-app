//
//  DataAccessClass.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "DataAccessClass.h"
#import "DataMapper.h"
#import "SPVSysParamValues.h"
#import "Constant.h"

@implementation DataAccessClass

DataMapper * oDataMapper = nil;

#pragma mark -- INITIALIZE
+(void)initialize{
    NSLog(@"Initialized");
    oDataMapper = [DataMapper getInstance];
}

#pragma mark -- PUBLIC STATIC METHODS
//get the SPV System parameters for the given plan code..
+(NSString *) getSPVSysParamValues:(NSString *)_planCode{
    NSString *strParamValues = @"";
    NSMutableDictionary *hasSpvSysParamValues = oDataMapper.SPVSysParamValues;
    NSLog(@"plan code is %@",_planCode);
    SPVSysParamValues *values = [hasSpvSysParamValues valueForKey:_planCode];
    strParamValues = values.SPV_VALUE;
    return strParamValues;
}

//get the formula id name
+(PFRPlanFormulaRef *) getFormula:(NSString *)_formulaKey andWithValue:(NSString *)_value{
    PFRPlanFormulaRef *formulaRef = NULL;
    NSMutableArray *formulaRefArr = oDataMapper.PFRPlanFormulaRef;
    for (int arrCount = 0; arrCount < formulaRefArr.count; arrCount++) {
        formulaRef = [formulaRefArr objectAtIndex:arrCount];
        if(formulaRef != NULL){
            if([_formulaKey caseInsensitiveCompare:formulaRef.PER_PLAN_CD] == NSOrderedSame && [_value caseInsensitiveCompare:formulaRef.PER_FRM_TYPE] == NSOrderedSame){
                return formulaRef;
            }
        }
    }
    return NULL;
}

//get the formula execution steps..
+(NSMutableDictionary *)getFormulaExpression:(NSString *)_formulaID{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    FEDFormulaExtDesc *fedFormulaExtDescObj = NULL;
    NSMutableArray *arr = oDataMapper.FEDFormulaExtDesc;
    for(int arrCount = 0;arrCount < arr.count;arrCount++){
        fedFormulaExtDescObj = [arr objectAtIndex:arrCount];
        if(fedFormulaExtDescObj !=NULL){
            if([_formulaID caseInsensitiveCompare:[fedFormulaExtDescObj FED_ID]] == NSOrderedSame){
                [dict setObject:fedFormulaExtDescObj forKey:[NSNumber numberWithInt:fedFormulaExtDescObj.FED_OBJ_SEQ]];
            }
        }
    }
    return dict;
}

+(double)getPremiumMultiple:(SISRequest *)_requestBean andWithMIP:(BOOL)_mip{
    
    double premiumMultiple=0;
    PNLPlan *planObj=[self getPnlPlan:_requestBean.BasePlan];
    NSMutableArray *pbaBandArr=[self getPbaBandArray:_requestBean.BasePlan];
    NSMutableArray *pmlPremiumMultipleArr=oDataMapper.PMLPremiumMultiple;
    
    for (int cnt=0; cnt<pmlPremiumMultipleArr.count; cnt++) {
        PMLPremiumMultiple *pmlPremiumMultipleObj=[pmlPremiumMultipleArr objectAtIndex:cnt];
        if (pmlPremiumMultipleObj.PML_PNL_ID ==planObj.PNL_ID) {
            NSString *sex = @"";
            int age = 0;
            int term = 0;
            NSString *smokerFlg = @"";
            if ([@"U" isEqualToString:pmlPremiumMultipleObj.PML_SEX])
                sex=@"U";
            else
                sex=_requestBean.InsuredSex;
            
            if (pmlPremiumMultipleObj.PML_AGE ==999)
                age=999;
            else
                age=_requestBean.InsuredAge;
            
            if (planObj.PNL_PREM_MULT_FORMULA == 3)
                term=(int)_requestBean.PolicyTerm;
            else
                term=pmlPremiumMultipleObj.PML_TERM;
            
            //Changed by Amruta from "Y" to "0" & "N" to "1"
            if ([@"0" isEqualToString:_requestBean.InsuredSmokerStatus])
                smokerFlg=@"Y";
            else if ([@"1" isEqualToString:_requestBean.InsuredSmokerStatus])
                smokerFlg=@"N";
            else
                smokerFlg=pmlPremiumMultipleObj.SMOKER_FLAG;
            
            //NSLog(@"the smoker is %@",smokerFlg);
            
            if (pmlPremiumMultipleObj.SMOKER_FLAG ==NULL)
                pmlPremiumMultipleObj.SMOKER_FLAG=@"";
            
            
           // NSLog(@"age is %d and sex is %@ term ==== %d and smoker flag is %@",age,sex,term,smokerFlg);
            if (pbaBandArr.count>0) {
                //NSLog(@"INSIDE IF PBA BAND ARRAY %f",pmlPremiumMultipleObj.PML_PREM_MULT);
                int pbaBand= [ self getPBABand:pbaBandArr andWithSumAssured:_requestBean.Sumassured];
                if([pmlPremiumMultipleObj PML_AGE] == age &&
                   [sex isEqualToString:pmlPremiumMultipleObj.PML_SEX] &&
                   pmlPremiumMultipleObj.PML_BAND == pbaBand &&
                   [pmlPremiumMultipleObj.SMOKER_FLAG isEqualToString: smokerFlg]){
                    premiumMultiple = pmlPremiumMultipleObj.PML_PREM_MULT;
                    break;
                }
            }
            else if([pmlPremiumMultipleObj PML_AGE] == age &&
                    [sex isEqualToString:pmlPremiumMultipleObj.PML_SEX] &&
                    pmlPremiumMultipleObj.PML_TERM == term &&
                    [pmlPremiumMultipleObj.SMOKER_FLAG isEqualToString: smokerFlg]){
                //NSLog(@"INSIDE ELSE IF PBA BAND ARRAY %f",pmlPremiumMultipleObj.PML_PREM_MULT);
                premiumMultiple = pmlPremiumMultipleObj.PML_PREM_MULT;
                break;
            }
        }
    }
    //changes for mip 27th jan
    double premDiscount, disc = 0;
    if (_mip) {
		//changes premium to annual as android passed annual in request for mip..
        premDiscount = [DataAccessClass getPremiumDiscount:planObj.PNL_CODE andWithSumAssured:_requestBean.BasePremiumAnnual * 12];
    }else
        premDiscount = [DataAccessClass getPremiumDiscount:planObj.PNL_CODE andWithSumAssured:_requestBean.Sumassured];
    
    //Added by Amruta for SR & SR+
    disc = (1 - premDiscount);
	
    if ([_requestBean.BasePlan containsString:@"SRL"] || [_requestBean.BasePlan containsString:@"SRP"] || [_requestBean.BasePlan containsString:@"SRRP"])
        premiumMultiple = premiumMultiple * disc;
    else
        premiumMultiple = premiumMultiple - premDiscount;
	
    
    int PGL_ID = planObj.PNL_PGL_ID;
    
    if(PGL_ID == 224 || PGL_ID == 225){
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        //[formatter setRoundingMode:NSNumberFormatterRoundFloor];
        //[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setRoundingMode:NSNumberFormatterRoundUp];
        [formatter setMaximumFractionDigits:3];
        NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:premiumMultiple]];
        double d= round ([numberString doubleValue] * 100.0) / 100.0;
        numberString = [NSString stringWithFormat:@"%.2f",d];
        
        premiumMultiple = [numberString doubleValue];
    }else{
        //MIP
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setRoundingMode:NSNumberFormatterRoundFloor];
        //[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        //[formatter setRoundingMode:NSNumberFormatterRoundUp];
        [formatter setMaximumFractionDigits:2];
        NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:premiumMultiple]];
        premiumMultiple = [numberString doubleValue];
    }
    
    return premiumMultiple;
}

//********************************SGP*********************************
+(double)getPremiumDiscount:(NSString *)_planCode andWithSumAssured:(long)_sumassured{
    double premDiscount= 0;
    NSMutableArray *arr = oDataMapper.PmdPlanPremMultiDisc;
    for(int cnt =0;cnt<arr.count;cnt++){
        PmdPlanPremiumMultDisc *pmdPlanPremiumMultDiscObj = [arr objectAtIndex:cnt];
        if([_planCode isEqualToString:pmdPlanPremiumMultDiscObj.PMD_PLAN_CODE] &&
           _sumassured >= pmdPlanPremiumMultDiscObj.PMD_MIN_SA &&
           _sumassured <= pmdPlanPremiumMultDiscObj.PMD_MAX_SA ){
            premDiscount = pmdPlanPremiumMultDiscObj.PMD_DISC_RATE;
            
            break;
        }
    }
    // NSLog(@"PREM DISCOUNT %f",premDiscount);
    return premDiscount;
}
//********************************SGP*********************************


+(int) getPBABand:(NSMutableArray *)_arr andWithSumAssured:(long)_sumAssured{
    int pbaBand = 0;
    for(int arrCnt=0;arrCnt <_arr.count;arrCnt++){
        PBAProductBand *pbaProductBandObj = [_arr objectAtIndex:arrCnt];
        if([@"B" isEqualToString:[pbaProductBandObj PBA_TYPE]] && [pbaProductBandObj PBA_SUM_ASSURED] >= _sumAssured){
            int band = [[pbaProductBandObj PBA_BAND] intValue];
            if(band > pbaBand){
                pbaBand = band;
            }
            break;
        }
    }
    //NSLog(@"PBA BAND SELECTED IS %d",pbaBand);
    return pbaBand;
}

+(PNLPlan *)getPnlPlan:(NSString *)_planCode{
    PNLPlan *pnlPlanObj=NULL;
    
    NSMutableDictionary *pnlPlanDict=oDataMapper.PNLPlan;
    pnlPlanObj=[pnlPlanDict valueForKey:_planCode];
    
    return pnlPlanObj;
}

+(double) getCSVFactor:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    double SVFactor;
    int polTerm = (int)_requestBean.PolicyTerm;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *SVFactorDict = [self getCashSurrenderValueFactor:pnlPlanObj.PNL_ID andWithTerm:polTerm];
    if([[SVFactorDict allKeys] count] != 0){
        SVFactor = [[SVFactorDict objectForKey:[NSNumber numberWithInt:_cnt]] doubleValue];
    }else
        SVFactor = 0;
    return  SVFactor;
}

+(NSMutableDictionary *)getCashSurrenderValueFactor:(int)_planID andWithTerm:(int)_term{
    NSMutableDictionary *SVFactorDict = [[NSMutableDictionary alloc] init];
    BOOL PVFlag = false;
    NSMutableArray *PMVMaturityValueArr = oDataMapper.PMVPlanMaturityValue;
    for(int cnt =0;cnt < PMVMaturityValueArr.count;cnt++){
        PMVPlanMaturityValue *PMVPlanMaturityValueObj = [PMVMaturityValueArr objectAtIndex:cnt];
        int band = [PMVPlanMaturityValueObj.PMV_BAND intValue];
        PVFlag = true;
        
        if([@"D" isEqualToString:PMVPlanMaturityValueObj.PMV_USE] &&
           PVFlag){
            if(_planID == PMVPlanMaturityValueObj.PMV_PNL_ID &&
               band == _term){
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL1] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR1]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL2] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR2]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL3] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR3]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL4] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR4]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL5] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR5]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL6] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR6]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL7] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR7]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL8] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR8]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL9] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR9]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL10] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR10]];
            }
        }
    }
    return SVFactorDict;
}

+(PNLPlan *)getPlan:(NSString *)_planCode{
    PNLPlan *planObj;
    NSMutableDictionary *dict = oDataMapper.PNLPlan;
    planObj = [dict objectForKey:_planCode];
    return planObj;
}

+(NSMutableArray *)getPbaBandArray:(NSString *)_planCode{
    NSMutableArray *pbaBandArr=[[NSMutableArray alloc]init];
    NSMutableArray *pbaProductBandArr=oDataMapper.PBAProductBand;
    
    for (int arrCnt=0; arrCnt<[pbaProductBandArr count]; arrCnt++) {
        PBAProductBand *productBandObj=[pbaProductBandArr objectAtIndex:arrCnt];
        if ([_planCode isEqualToString:productBandObj.PBA_PLAN_CODE]) {
            [pbaBandArr addObject:productBandObj];
        }
    }
    
    return pbaBandArr;
}

+(double) getGSVFactor:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    double SVFactor;
    int polTerm = (int)_requestBean.PolicyTerm;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *SVFactorDict = [self getGuaranteedSurrenderValueFactor:pnlPlanObj.PNL_ID andWithTerm:polTerm];
    if([SVFactorDict count] != 0){
        SVFactor = [[SVFactorDict objectForKey:[NSNumber numberWithInt:_cnt]] doubleValue];
    }else
        SVFactor = 0;
    return  SVFactor;
}

+(NSMutableDictionary *)getGuaranteedSurrenderValueFactor:(int)_planID andWithTerm:(int)_term{
    NSMutableDictionary *SVFactorDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *PMVMaturityValueArr = oDataMapper.PMVPlanMaturityValue;
    for(int cnt =0;cnt < PMVMaturityValueArr.count;cnt++){
        PMVPlanMaturityValue *PMVPlanMaturityValueObj = [PMVMaturityValueArr objectAtIndex:cnt];
        
        if([@"GV" isEqualToString:PMVPlanMaturityValueObj.PMV_BAND]){
            if(_planID == PMVPlanMaturityValueObj.PMV_PNL_ID && _term == PMVPlanMaturityValueObj.PMV_AGE){
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL1] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR1]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL2] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR2]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL3] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR3]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL4] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR4]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL5] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR5]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL6] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR6]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL7] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR7]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL8] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR8]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL9] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR9]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL10] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR10]];
            }
        }
    }
    return SVFactorDict;
}

+(double)getRiderPremiumMultiple:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    double riderPremiumMultiple=0;
    
    Rider *riderobj=[_requestBean.RiderList objectAtIndex:_cnt];
    
    if([riderobj.RDL_CODE hasPrefix:@"PB"])
    {
        riderPremiumMultiple = [self getPBRiderPremiumMultiple:_requestBean AndWithCount:_cnt];
    }else{
        riderPremiumMultiple = [self getNONPBRiderPremiumMultiple:_requestBean AndWithCount:_cnt];
    }
    return riderPremiumMultiple;
}

+(double)getNONPBRiderPremiumMultiple:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    int pbaBand =0,occClass =0,pbaBandBySA =0,occBandCount = 0;
    double riderPremMultiple = 0;
    Rider *riderObj = [_requestBean.RiderList objectAtIndex:_cnt];
    //NSLog(@"the rider info is %@",riderObj.RDL_CODE);
    
    NSMutableArray *riderArr = [self getPbaBandArray:riderObj.RDL_CODE];
    NSMutableDictionary *oclOccupationDict = oDataMapper.OCLOccupation;
    OCLOccupation *oclOccupationObj = [oclOccupationDict valueForKey:_requestBean.InsuredOccupation];
    occClass = oclOccupationObj.OCL_CLASS;
    
    if(riderArr.count > 0){
        for(int cnt =0;cnt<riderArr.count;cnt++){
            PBAProductBand *pbaProductBandObj = [riderArr objectAtIndex:cnt];
            if([@"R" isEqualToString:pbaProductBandObj.PBA_TYPE] && pbaProductBandObj.PBA_SUM_ASSURED >= riderObj.SumAssured){
                int pbaBand1 = [pbaProductBandObj.PBA_BAND intValue];
                if(pbaBandBySA == 0){
                    pbaBandBySA = pbaBand1;
                }else if(pbaBand1 < pbaBandBySA){
                    pbaBandBySA = pbaBand1;
                }
            }
            if([@"R" isEqualToString:pbaProductBandObj.PBA_TYPE] && [pbaProductBandObj.PBA_BAND intValue] == occClass){
                ++occBandCount;
            }
        }
    }
    
    NSMutableArray *riderPremMultipleArr = oDataMapper.RMLRiderPremMultiple;
    for(int arrCnt =0;arrCnt < riderPremMultipleArr.count;arrCnt++){
        RMLRiderPremMultiple *rmlRiderMultipleObj = [riderPremMultipleArr objectAtIndex:arrCnt];
        if([riderObj.RDL_CODE caseInsensitiveCompare:rmlRiderMultipleObj.RML_RIDER_CD] == NSOrderedSame){
            NSString *sex = @"";
            int age,term = 0;
            if([@"U" isEqualToString:rmlRiderMultipleObj.RML_SEX])
                sex = @"U";
            else
                sex = _requestBean.InsuredSex;
            
            if (rmlRiderMultipleObj.RML_AGE == 999 )
                age = 999 ;
            else
                age = _requestBean.InsuredAge;
            
            if(occBandCount > 0)
                pbaBand = occClass;
            else
                pbaBand = pbaBandBySA;
            
            if(rmlRiderMultipleObj.RML_TERM == 0){
                term = 0;
            }else if(riderObj.RDL_RTL_ID == 13 || riderObj.RDL_RTL_ID == 8){
                if(_requestBean.PolicyTerm > (65 - _requestBean.InsuredAge))
                    term = 65 - _requestBean.InsuredAge;
                else
                    term = (int)_requestBean.PolicyTerm;
            }else if(riderObj.RDL_RTL_ID == 14){
                if(_requestBean.PremiumPayingTerm > (65 - _requestBean.InsuredAge))
                    term = 65 - _requestBean.InsuredAge;
                else
                    term = (int)_requestBean.PremiumPayingTerm;
            }else if(riderObj.RDL_RTL_ID == 15){
                if(_requestBean.PremiumPayingTerm > (65 - _requestBean.ProposerAge))
                    term = (int)(65 - _requestBean.ProposerAge);
                else
                    term = (int)_requestBean.PremiumPayingTerm;
                sex = _requestBean.ProposerSex;
                age = (int)_requestBean.ProposerAge;
            }else {
                term = (int)_requestBean.PolicyTerm;
            }
            
            if(riderArr.count > 0){
                if(rmlRiderMultipleObj.RML_AGE == age &&
                   rmlRiderMultipleObj.RML_BAND == pbaBand &&
                   //Added by Amruta on 9/9/2015 for FG Rider
                   [sex caseInsensitiveCompare:rmlRiderMultipleObj.RML_SEX] == NSOrderedSame){
                    //NSLog(@"rider premium if age %d:: PBA band %d:: sex %@:: term %d::",age,pbaBand,sex,term);
                    riderPremMultiple = rmlRiderMultipleObj.RML_PREM_MULT;
                    break;
                }
            }else{
                if(rmlRiderMultipleObj.RML_AGE == age &&
                   rmlRiderMultipleObj.RML_TERM == term &&
                   [sex caseInsensitiveCompare:rmlRiderMultipleObj.RML_SEX] == NSOrderedSame)
                {
                    //NSLog(@"rider premium if age %d:: PBA band %d:: sex %@:: term %d::",age,pbaBand,sex,term);
                    riderPremMultiple = rmlRiderMultipleObj.RML_PREM_MULT;
                    break;
                    
                }
            }
        }
    }
    return riderPremMultiple;
    
}

+(double)getPBRiderPremiumMultiple:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    
    int  pbaBand = 0;
    double riderPremiumMultiple = 0;
    int min_band1 = 21 - _requestBean.InsuredAge;
    int min_band2 = 60 - _requestBean.InsuredAge;
    int min_band3 = (int)_requestBean.PremiumPayingTerm;
    
    Rider *riderObj = [_requestBean.RiderList objectAtIndex:_cnt];
    if ((min_band1 <= min_band2) && (min_band1 <= min_band3) ) {
        
        pbaBand = min_band1;
    }else if ((min_band2 <= min_band1) && (min_band2 <= min_band3) ) {
        
        pbaBand = min_band2;
    }else if ((min_band3 <= min_band1) && (min_band3 <= min_band2) ) {
        
        pbaBand = min_band3;
    }
    
    NSMutableArray *rmlRiderPremMultipleArr=oDataMapper.RMLRiderPremMultiple;
    
    for (int cnt = 0; cnt < [rmlRiderPremMultipleArr count]; cnt++) {
        
        RMLRiderPremMultiple *rmlRiderPremMultipleObj=[rmlRiderPremMultipleArr objectAtIndex:cnt];
        
        if ([riderObj.RDL_CODE caseInsensitiveCompare:rmlRiderPremMultipleObj.RML_RIDER_CD] == NSOrderedSame) {
            
            int age = 0;
            
            if (rmlRiderPremMultipleObj.RML_AGE == 999 ) {
                
                age = 999 ;
            }else
            {
                age = _requestBean.InsuredAge;
            }
            
            if ((rmlRiderPremMultipleObj.RML_AGE == age ) && (rmlRiderPremMultipleObj.RML_BAND == pbaBand)){
                
                riderPremiumMultiple = rmlRiderPremMultipleObj.RML_PREM_MULT ;
                
                break;
            }
            
        }
    }
    
    return riderPremiumMultiple;
    
    
}

+(double)getWpLoadValue:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    
    double wpLoadValue=1;
    Rider *riderObj=[_requestBean.RiderList objectAtIndex:_cnt];
    if (riderObj.RDL_RTL_ID == 12) {
        
        NSMutableDictionary *occupationDictionary=oDataMapper.OCLOccupation;
        OCLOccupation *oclOccupationObj = [occupationDictionary valueForKey:_requestBean.InsuredOccupation];
        wpLoadValue = oclOccupationObj.OCL_WP;
    }
    return wpLoadValue;
}

+(double)getInsLoadValue:(SISRequest *)_requestBean {
    
    double insLoadValue;
    
    NSMutableDictionary *occupationDictionary=oDataMapper.OCLOccupation;
    OCLOccupation *oclOccupationObj = [occupationDictionary valueForKey:_requestBean.InsuredOccupation];
    insLoadValue = oclOccupationObj.OCL_BLIFE;
    
    return insLoadValue;
}

+(double)getModelFactor:(SISRequest *)_requestBean {
    
    double modelFactor ;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSString *payMode = [@"O" isEqualToString:_requestBean.Frequency]?@"A":_requestBean.Frequency;
    modelFactor =[self getModelFactor:pnlPlanObj.PNL_PGL_ID withPaymentMode:payMode];
    return modelFactor;
}

+(double)getModelFactor:(int)pglId withPaymentMode:(NSString *)payMode{
    double modelFactor = 0;
    NSMutableArray *modelFactorArr = oDataMapper.MDLModalFactor;
    MDLModalFactor *mdlModelFactorObj = nil;
    
    for (int cnt = 0; cnt < modelFactorArr.count; cnt++) {
        mdlModelFactorObj =[modelFactorArr objectAtIndex:cnt];
        if (pglId == mdlModelFactorObj.MDL_PGL_ID && [payMode caseInsensitiveCompare:mdlModelFactorObj.MDL_PAY_FREQ]==NSOrderedSame) {
            
            modelFactor = mdlModelFactorObj.MDL_MODAL_FACTOR;
            break;
        }
    }
    //NSLog(@"modelFactor ==%f",modelFactor);
    return modelFactor;
}

+(long)getRiderSumAssuared:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    long riderSumAssuared=0;
    Rider *riderobj = [_requestBean.RiderList objectAtIndex:_cnt];
    riderSumAssuared=riderobj.SumAssured;
    return riderSumAssuared;
}

//Added by Amruta for MIP WPP Rider
+(double)getRiderModelFactor:(SISRequest *)_requestBean{
    double modelFactor = 1;
    NSString *payMode = [@"O" isEqualToString:_requestBean.Frequency]?@"A":_requestBean.Frequency;
    if([payMode isEqualToString:@"A"])
    modelFactor = 1;
    else if([payMode isEqualToString:@"S"])
    modelFactor = 0.51;
    else if ([payMode isEqualToString:@"Q"])
    modelFactor = 0.26;
    else if ([payMode isEqualToString:@"M"])
    modelFactor = 0.0883;
    
    return modelFactor;
}

//************************ULIP************************
+(long) getTotalAnnualPremium:(FormulaBean *)_bean{
    long totalPrem = 0;
    SISRequest *req = _bean.request;
    //changes 6th oct...
    int ppt = (int)req.PremiumPayingTerm;
    int count = 0;
    
    if (_bean.CounterVariable > ppt) {
        count = ppt;
    }else{
        count = _bean.CounterVariable;
    }
    
    if (_bean.CNT_MINUS) {
        count = count + 1;
    }
    
    totalPrem = _bean.premium * count;
    return totalPrem;
}

+(double)getPAChargeValue:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt{
    
    NSMutableDictionary *pcrPlanCharges=oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj =[self getPlan:_requestBean.BasePlan];
    int pnlId =pnlPlanObj.PNL_ID;
    
    if ([_requestBean.Frequency isEqualToString:@"M"]) {
        premium = _requestBean.BasePremiumAnnual;
    }
    
    double pacCahrges = 0;
    PCRPlanChargesRef *pcrPlanChargesRefObj =  [pcrPlanCharges objectForKey:[NSNumber numberWithInt:pnlId]];
    NSMutableArray *pacPremiumAllocationArr = oDataMapper.PACPremiumAllocationCharges;
    PACPremiumAllocationCharges *pacPremiumAllocationChargesObj;
    
    if ([@"Y" caseInsensitiveCompare:_requestBean.TataEmployee]==NSOrderedSame && _cnt ==1) {
        pacCahrges = 0.0;
    }else{
        for (int cnt = 0; cnt < pacPremiumAllocationArr.count ;cnt++) {
            pacPremiumAllocationChargesObj =[pacPremiumAllocationArr objectAtIndex:cnt];
           // NSLog(@"prem id %d",pcrPlanChargesRefObj.PCR_PREM_ID);
           // NSLog(@"pac id %d",pacPremiumAllocationChargesObj.PAC_ID);
           // NSLog(@"Check Values %d and premium %ld",cnt,premium);
           // NSLog(@"pac min band %d",pacPremiumAllocationChargesObj.PAC_MIN_BAND);
           // NSLog(@"pac max band %d",pacPremiumAllocationChargesObj.PAC_MAX_BAND);
            //NSLog(@"pac min prem %d",pacPremiumAllocationChargesObj.PAC_MIN_PREM);
            //NSLog(@"pac max prem %d",pacPremiumAllocationChargesObj.PAC_MAX_PREM);
            
            if((pcrPlanChargesRefObj.PCR_PREM_ID == pacPremiumAllocationChargesObj.PAC_ID) &&
               (_cnt >=pacPremiumAllocationChargesObj.PAC_MIN_BAND && _cnt <=pacPremiumAllocationChargesObj.PAC_MAX_BAND) &&
               (premium >=pacPremiumAllocationChargesObj.PAC_MIN_PREM && premium <=pacPremiumAllocationChargesObj.PAC_MAX_PREM)){
                
                pacCahrges =(double)pacPremiumAllocationChargesObj.PAC_CHARGE;
                break;
                
            }
        }
    }
    //NSLog(@" the plan charges is %f",pacCahrges);
    return pacCahrges;
}
//************************ULIP************************

+(double)getWOPCharges:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    int band = (int)_requestBean.PolicyTerm - _cnt;
    int age = (int)_requestBean.InsuredAge + (_cnt-1);
    double wopCharges = 0.0;
    PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges valueForKey:[NSString stringWithFormat:@"%d",pnlId]];
    NSMutableArray *wcmWOPChargesMasterArr = oDataMapper.WCMWOPChargesMaster;
    WCMWOPChargesMaster *wcmWOPChargesMasterObj;
    
    for (int cnt = 0; cnt < wcmWOPChargesMasterArr.count; cnt++) {
        wcmWOPChargesMasterObj = [wcmWOPChargesMasterArr objectAtIndex:cnt];
        if ((pcrPlanChargesRefObj.PCR_WOP_ID == wcmWOPChargesMasterObj.WCM_WOP_ID)&&(band == wcmWOPChargesMasterObj.WCM_BAND)&&(age == wcmWOPChargesMasterObj.WCM_AGE)) {
            
            wopCharges = wcmWOPChargesMasterObj.WCM_CHARGE;
            break;
        }
    }
    return wopCharges;
}

+(double)getFIBCharges:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    int band = (int)_requestBean.PolicyTerm - _cnt;
    int age = (int)_requestBean.InsuredAge + (_cnt-1);
    double fibCharges = 0.0;
    PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges valueForKey:[NSString stringWithFormat:@"%d",pnlId]];
    NSMutableArray *fcmFibChargesMasterArr = oDataMapper.FCMFIBChargesMaster;
    FCMFIBChargersMaster *fcmFIBChargersMasterObj;
    if (band > 10) {
        band = 10;
    }
    for (int cnt = 0;  cnt <fcmFibChargesMasterArr.count ;cnt++ ) {
        
        fcmFIBChargersMasterObj=[fcmFibChargesMasterArr objectAtIndex:cnt];
        if ((pcrPlanChargesRefObj.PCR_FIB_ID == fcmFIBChargersMasterObj.FCM_FIB_ID)&&(band== fcmFIBChargersMasterObj.FCM_BAND)&&(age == fcmFIBChargersMasterObj.FCM_AGE)) {
            fibCharges =fcmFIBChargersMasterObj.FCM_CHARGE;
            break;
        }
    }
    return fibCharges;
    
}

//****************************ULIP****************************
+(double)getMCRChares:(SISRequest *)_requestBean AndWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    int age = (int)_requestBean.InsuredAge + (_cnt-1);
    double mcrCharges = 0.0;
    
    //changes 16th oct
    NSString *sex = _requestBean.InsuredSex;
    
    PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges objectForKey:[NSNumber numberWithInt:pnlId]];
    NSMutableArray *mcmMortilityChargesMasterArr = oDataMapper.MCMMortilityChargeMaster;
    MCMMortilityChargesMaster *mcmMortilityChargesMasterObj;
    
    //changes 16th oct SA
    for (int cnt = 0; cnt < mcmMortilityChargesMasterArr.count; cnt++) {
        mcmMortilityChargesMasterObj = [mcmMortilityChargesMasterArr objectAtIndex:cnt];
        
        //changes 16th oct..
        if ([mcmMortilityChargesMasterObj.MFM_SEX caseInsensitiveCompare:@"U"] == NSOrderedSame) {
            sex = @"U";
        }else
            sex = _requestBean.InsuredSex;
        
        if((pcrPlanChargesRefObj.PCR_MORT_ID == mcmMortilityChargesMasterObj.MCM_ID)
           && (age == mcmMortilityChargesMasterObj.MCM_AGE
               && ([sex caseInsensitiveCompare:mcmMortilityChargesMasterObj.MFM_SEX] == NSOrderedSame) )
           ) {
            mcrCharges =mcmMortilityChargesMasterObj.MCM_MORT_CHARGE;
            break;
        }
    }
    return mcrCharges;
}


+(double)getGuaranteedMaturityBonus:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    double gurMaturityBonus = 0.0;
    PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges valueForKey:[NSString stringWithFormat:@"%d",pnlId]];
    NSMutableArray *guaranteeMaturityBonusMasterArr = oDataMapper.GuaranteeMaturityBonusMaster;
    GuaranteedMaturityBonusMaster *guaranteeMaturityBonusMasterObj;
    for (int cnt = 0; cnt < guaranteeMaturityBonusMasterArr.count ; cnt++) {
        guaranteeMaturityBonusMasterObj = [guaranteeMaturityBonusMasterArr objectAtIndex:cnt];
        if ((pcrPlanChargesRefObj.PCR_GMM_ID == guaranteeMaturityBonusMasterObj.GMM_ID)&&(cnt >= guaranteeMaturityBonusMasterObj.GMM_MIN_BAND)&&(cnt <= guaranteeMaturityBonusMasterObj.GMM_MAX_BAND)) {
            gurMaturityBonus = guaranteeMaturityBonusMasterObj.GMM_BONUS;
            break;
        }
    }
    return gurMaturityBonus;
    
}

+(double)getFMC:(SISRequest *)_requestBean{
    
    double fundChargerFactor = 0.0;
    FundDetails *fundObj = NULL;
    NSMutableArray *fundArr = _requestBean.FundList;
    for (int cnt = 0; cnt <fundArr.count; cnt++) {
        fundObj =[fundArr objectAtIndex:cnt];
        fundChargerFactor = fundChargerFactor + (fundObj.Percentage * fundObj.Fund_Mgmt_charge);
    }
    return (fundChargerFactor/100);
    
}

+(PGFPlanGroupFeature *)getPlanGroupFeature:(NSString *)_planCode{
    
    PGFPlanGroupFeature *pgfPlanGroupFeatureObj = NULL;
    NSMutableDictionary *planFeatureDcit = oDataMapper.PGFPlanGroupFeature;
    pgfPlanGroupFeatureObj =[planFeatureDcit valueForKey:_planCode];
    return pgfPlanGroupFeatureObj;
}

+(AddressBO *)getAddressBO{
    
    NSMutableArray *adrdressBOArr=oDataMapper.Address;
	AddressBO *addressBOObj=NULL;
	if(adrdressBOArr.count > 0)
		addressBOObj =[adrdressBOArr objectAtIndex:0];
    return addressBOObj;
}

+(AgentDetail *)getAgentDetail{
    AgentDetail *detail = oDataMapper.agent_intermedDetails;
    return detail;
}

/*+(NSString *)getCommissionDetails{
 NSString *detail = oDataMapper.Commission;
 return detail;
 }*/

+(NSString *)getPlanType:(NSString *)_planCode{
    
    PNLPlan *pnlPlanObj =[self getPlan:_planCode];
    NSMutableDictionary *pglPlanGrpDict=oDataMapper.PGLPlanGroup;
    PGLPlanGroup *pglPlanGroupObj =[pglPlanGrpDict valueForKey:[NSString stringWithFormat:@"%d",pnlPlanObj.PNL_PGL_ID]];
    return pglPlanGroupObj.PGL_PRODUCT_TYPE;
}

//**********************ULIP**********************
+(double)getCommissionPercentage:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    double commissionPercentage = 0.0;
    if ([@"Y" caseInsensitiveCompare:_requestBean.TataEmployee] == NSOrderedSame || !([@"Payable" caseInsensitiveCompare:_requestBean.Commission] == NSOrderedSame)) {
        
        commissionPercentage = 0;
    }else{
        
        PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges objectForKey:[NSNumber numberWithInt:pnlId]];
        NSMutableArray *ccmCommissionChargeMasterArr = oDataMapper.CommissionChargeMaster;
        CCMommissionChargeMaster *ccmCommissionChargeMasterObj;
        for (int cnt = 0; cnt <  ccmCommissionChargeMasterArr.count; cnt++) {
            ccmCommissionChargeMasterObj = [ccmCommissionChargeMasterArr objectAtIndex:cnt];
            if ((pcrPlanChargesRefObj.PCR_COMM_ID == ccmCommissionChargeMasterObj.CCM_ID) &&
                (_cnt >= ccmCommissionChargeMasterObj.CCM_MIN_TERM && _cnt <= ccmCommissionChargeMasterObj.CCM_MAX_TERM)) {
                commissionPercentage =ccmCommissionChargeMasterObj.CCM_COMMISSION;
                //NSLog(@"commission percent %f",commissionPercentage);
                break;
            }
        }
    }
    return commissionPercentage;
}

+(double)getSurrenderChargeValue:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    double surCharge = 0.0;
    PCRPlanChargesRef *pcrPlanChargesRefObj =[pcrPlanCharges objectForKey:[NSNumber numberWithInt:pnlId]];
    NSMutableArray *surSurrenderChargesMasterArr = oDataMapper.SURSurrenderChargesMaster;
    SURSurrenderChargesMaster *surSurrenderChargesMasterObj;
    for (int cnt = 0; cnt < surSurrenderChargesMasterArr.count;  cnt++) {
        surSurrenderChargesMasterObj = [surSurrenderChargesMasterArr objectAtIndex:cnt];
        if ((pcrPlanChargesRefObj.PCR_SUR_ID == surSurrenderChargesMasterObj.SUR_ID)&&
            (premium >=surSurrenderChargesMasterObj.SUR_MIN_PREM && premium <= surSurrenderChargesMasterObj.SUR_MAX_PREM) &&
            (_cnt >= surSurrenderChargesMasterObj.SUR_MIN_BAND && _cnt <= surSurrenderChargesMasterObj.SUR_MAX_BAND)) {
            surCharge = surSurrenderChargesMasterObj.SUR_CHARGE;
            //NSLog(@"the sur charge %f",surCharge);
            break;
            
        }
    }
    return surCharge;
}

+(double)getSurrenderChargeAmount:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt{
    
    NSMutableDictionary * pcrPlanCharges= oDataMapper.PCRPlanChargesRef;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    int pnlId = pnlPlanObj.PNL_ID;
    double surAmount = 0.0;
    PCRPlanChargesRef *pcrPlanChargesRefObj = [pcrPlanCharges objectForKey:[NSNumber numberWithInt:pnlId]];
    //[pcrPlanCharges valueForKey:[NSString stringWithFormat:@"%d",pnlId]];
    NSMutableArray *surSurrenderChargesMasterArr = oDataMapper.SURSurrenderChargesMaster;
    SURSurrenderChargesMaster *surSurrenderChargesMasterObj;
    for (int cnt = 0; cnt < surSurrenderChargesMasterArr.count;  cnt++) {
        surSurrenderChargesMasterObj = [surSurrenderChargesMasterArr objectAtIndex:cnt];
        if ((pcrPlanChargesRefObj.PCR_SUR_ID == surSurrenderChargesMasterObj.SUR_ID) &&
            (premium >=surSurrenderChargesMasterObj.SUR_MIN_PREM && premium <=surSurrenderChargesMasterObj.SUR_MAX_PREM)&&
            (_cnt >=surSurrenderChargesMasterObj.SUR_MIN_BAND && _cnt <=surSurrenderChargesMasterObj.SUR_MAX_BAND)) {
            surAmount =surSurrenderChargesMasterObj.SUR_AMOUNT;
            //NSLog(@"the sur amount is %f",surAmount);
            break;
            
        }
    }
    return surAmount;
}
//**********************ULIP**********************

+(double)getServiceTaxMain{
    double serviceTax = 0;
    NSMutableArray *arr = oDataMapper.ServiceMaster;
    
    for (int i=0; i<arr.count; i++) {
        ServiceTaxMaster *serviceMaster = [arr objectAtIndex:i];
        if ([SERVICE_TAX_MAIN isEqualToString:serviceMaster.Stm_type]) {
            serviceTax = serviceMaster.Stm_value;
        }
    }
    
    return serviceTax;
}

+(double)getServiceTaxFY{
    double serviceTax = 0;
    NSMutableArray *arr = oDataMapper.ServiceMaster;
    
    for (int i=0; i<arr.count; i++) {
        ServiceTaxMaster *serviceMaster = [arr objectAtIndex:i];
        if ([SERVICE_TAX_FIRST_YEAR isEqualToString:serviceMaster.Stm_type]) {
            serviceTax = serviceMaster.Stm_value;
        }
    }
    
    return serviceTax;
}

+(double)getServiceTaxRenewal{
    double serviceTax = 0;
    NSMutableArray *arr = oDataMapper.ServiceMaster;
    
    for (int i=0; i<arr.count; i++) {
        ServiceTaxMaster *serviceMaster = [arr objectAtIndex:i];
        if ([SERVICE_TAX_RENEWAL isEqualToString:serviceMaster.Stm_type]) {
            serviceTax = serviceMaster.Stm_value;
        }
    }
    
    return serviceTax;
}

+(double)getMVFactor:(SISRequest *)_requestBean{
    
    double mvFactor = 0.0;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableArray *maturityArr =oDataMapper.MFMMaturityFactorMaster;
    for (int cnt = 0; cnt < maturityArr.count; cnt++) {
        MFMMaturityFactorMaster *mfmMaturityFactorMaster =[maturityArr objectAtIndex:cnt];
        if (pnlPlanObj.PNL_ID == mfmMaturityFactorMaster.MFM_PNL_ID) {
            if (([@"A" isEqualToString:mfmMaturityFactorMaster.MFM_USE_FLG])&&(mfmMaturityFactorMaster.MFM_AGE == _requestBean.InsuredAge)) {
                mvFactor = mfmMaturityFactorMaster.MFM_VALUE;
                break;
            }else if (([@"T" isEqualToString:mfmMaturityFactorMaster.MFM_USE_FLG])&&(mfmMaturityFactorMaster.MFM_BAND == _requestBean.PolicyTerm)) {
                mvFactor = mfmMaturityFactorMaster.MFM_VALUE;
                break;
            }//SIP
            else if([@"S" isEqualToString:mfmMaturityFactorMaster.MFM_USE_FLG] && mfmMaturityFactorMaster.MFM_BAND == _requestBean.PolicyTerm
                     && mfmMaturityFactorMaster.MFM_AGE == _requestBean.InsuredAge && [mfmMaturityFactorMaster.MFM_SEX isEqualToString:_requestBean.InsuredSex]){
                mvFactor = mfmMaturityFactorMaster.MFM_VALUE;
            }
        }
    }
    //NSLog(@"the MV factor is %f",mvFactor);
    return mvFactor;
    
}

+(long)getDisCountedPremium:(FormulaBean *)_formulaBean{
    
    long disCountedPremium = 0;
    double discPrem = 0;
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    NSString *disCountedPremiumValue =(NSString *) [formulaHandlerObj evaluateFormula:_formulaBean.request.BasePlan : @"" :_formulaBean ];
    discPrem =[disCountedPremiumValue doubleValue];
    disCountedPremium = round(discPrem);
    return disCountedPremium;
}

/*+(long)getTotalPremium:(FormulaBean *)_formulaBean{
 
 long totalPremium = 0;
 SISRequest *requestBean = _formulaBean.request;
 int ppt =(int) requestBean.PremiumPayingTerm;
 int count = 0;
 if (_formulaBean.CounterVariable > ppt) {
 count = ppt;
 }else {
 count = _formulaBean.CounterVariable;
 }
 
 if (_formulaBean.CNT_MINUS) {
 
 count = count + 1;
 }
 
 totalPremium = _formulaBean.premium * count;
 return totalPremium;
 }*/
+(long)getTotalPremium:(FormulaBean *)_formulaBean{
    
    long totalPremium = 0;
    SISRequest *requestBean = _formulaBean.request;
    int ppt =(int) requestBean.PremiumPayingTerm;
    int count = 0;
    //NSLog(@"the countervar :   %d and PPT   ::  %d",_formulaBean.CounterVariable,ppt);
    if (_formulaBean.CounterVariable >= ppt) {
        count = ppt;
    }else {
        count = _formulaBean.CounterVariable;
    }
    //NSLog(@"the count   :  %d",count);
    if (_formulaBean.CNT_MINUS) {
        
        if(count < ppt)
            count = count + 1;
    }
    
    totalPremium = _formulaBean.premium * count;
    return totalPremium;
}

+(long)getTotalPremiumULIP:(FormulaBean *)_formulaBean{
    
    long totalPremium = 0;
    SISRequest *requestBean = _formulaBean.request;
    int ppt =(int) requestBean.PremiumPayingTerm;
    int count = 0;
    int monthCount = _formulaBean.MonthCount;
    if (_formulaBean.CounterVariable > ppt) {
        count = ppt;
    }else {
        if ([requestBean.Frequency isEqualToString:@"A"] || [requestBean.Frequency isEqualToString:@"O"]){
            count = _formulaBean.CounterVariable;
            
        }else if ([requestBean.Frequency isEqualToString:@"S"] || [requestBean.Frequency isEqualToString:@"Q"] || [requestBean.Frequency isEqualToString:@"M"]){
            
            count = _formulaBean.CounterVariable - 1;
            
        }
    }
    totalPremium = _formulaBean.premium * count;
    if ([requestBean.Frequency isEqualToString:@"S"] || [requestBean.Frequency isEqualToString:@"Q"] || [requestBean.Frequency isEqualToString:@"M"]){
        
        if(_formulaBean.CounterVariable <= ppt){
            if (monthCount != 12) {
                if ([requestBean.Frequency isEqualToString:@"S"]) {
                    monthCount = (monthCount - 1) / 6 + 1;
                    totalPremium = totalPremium + (_formulaBean.premium / 2 * monthCount);
                }else if ([requestBean.Frequency isEqualToString:@"Q"]) {
                    monthCount = (monthCount - 1) / 3 + 1;
                    totalPremium = totalPremium + (_formulaBean.premium / 4 * monthCount);
                }else if ([requestBean.Frequency isEqualToString:@"M"]) {
                    totalPremium = totalPremium + (_formulaBean.premium / 12 * monthCount);
                }
            }else{
                
                totalPremium = totalPremium + _formulaBean.premium;
            }
            
        }
    }
    return totalPremium;
    
}

+(long)getTotalRemainingPremium:(FormulaBean *)_formulaBean{
    
    long totalRemainingPremium = 0;
    SISRequest *requestBean = _formulaBean.request;
    
    int ppt =(int) requestBean.PremiumPayingTerm;
    int count = _formulaBean.CounterVariable;
    int monthCount = _formulaBean.MonthCount;
    
    
    int age = requestBean.InsuredAge;
    if (_formulaBean.AmountForServiceTax ==240)
        age = (int)requestBean.ProposerAge;
    if (age+ppt >= 65)
        ppt=65-age;
    
    long pendingPremiumOtherMode = 0;
    totalRemainingPremium = _formulaBean.premium * (ppt-count);
    
    if ([requestBean.Frequency isEqualToString:@"M"]) {
        pendingPremiumOtherMode = ((12 - monthCount) * (round(_formulaBean.premium / 12)) );
    }else if ([requestBean.Frequency isEqualToString:@"Q"]) {
        int monthRemaining = (12-monthCount) / 3;
        pendingPremiumOtherMode = monthRemaining * (round(_formulaBean.premium / 4)) ;
    }else if ([requestBean.Frequency isEqualToString:@"S"]) {
        int monthRemaining = (12-monthCount) / 6;
        pendingPremiumOtherMode = monthRemaining * (round(_formulaBean.premium / 2)) ;
    }
    totalRemainingPremium = totalRemainingPremium + pendingPremiumOtherMode;
    
    return totalRemainingPremium;
}

+(double)getGAIFactor:(FormulaBean *)_formulaBean{
    
    double gaiFactor = 0.0;
    PNLPlan *pnlPlanObj = [ self getPlan:_formulaBean.request.BasePlan];
    int pt =(int) _formulaBean.request.PolicyTerm;
    long premium = _formulaBean.premium;
    long coverage = _formulaBean.request.Sumassured;
    
    NSMutableArray *gaiFactorArr =oDataMapper.GAIGuarAnnInChMaster;
    
    for (int cnt = 0 ; cnt < gaiFactorArr.count ; cnt ++) {
        GAIGuarAnnIncChMaster *gaiGuarAnnIncChMasterObj = [gaiFactorArr objectAtIndex:cnt];
        if([@"P" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if ((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(premium >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(premium <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if([@"T" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(_formulaBean.CounterVariable >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(_formulaBean.CounterVariable <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if([@"PT" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&
               (pt >=gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if([@"CT" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&
               (_formulaBean.CounterVariable >=gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(_formulaBean.CounterVariable <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)&&(coverage >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(coverage <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if([@"TP" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&
               (_formulaBean.CounterVariable >=gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(_formulaBean.CounterVariable <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)&&(premium >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(premium <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                NSLog(@"gai factor %f",gaiFactor);
                break;
            }
        }
    }
    //NSLog(@"gai factor %f",gaiFactor);
    return gaiFactor;
    
}

+(double)getTotalGAI:(FormulaBean *)_formulaBean{
    
    double totalGAIValue = 0;
    
    FormulaHandler *formulaHandler = [[FormulaHandler alloc]init];
    int counter = _formulaBean.CounterVariable;
    
    for (int cnt = 1 ; cnt <= counter ; cnt++) {
        double gaiValue = 0;
        if (cnt > _formulaBean.request.PremiumPayingTerm) {
            [_formulaBean setCounterVariable:(cnt)];
            NSString *gaiValueStr =[NSString stringWithFormat:@"%@ ",[formulaHandler evaluateFormula:_formulaBean.request.BasePlan :CALCULATE_GUARANTEED_ANNUAL_INCOME_F :_formulaBean ]];
            gaiValue = [gaiValueStr doubleValue];
            
        }
        totalGAIValue = totalGAIValue + gaiValue;
    }
    return totalGAIValue;
}

+(double)getTotalGAIMLG:(FormulaBean *)_formulaBean{
    
    double totalGAIValue = 0;
    FormulaHandler *formulaHandler = [[FormulaHandler alloc]init];
    int counter = _formulaBean.CounterVariable;
    for (int cnt = 1 ; cnt < counter ; cnt++) {
        double gaiValue = 0;
        [_formulaBean setCounterVariable:(cnt)];
        NSString *gaiValueStr =[NSString stringWithFormat:@"%@ ",[formulaHandler evaluateFormula:_formulaBean.request.BasePlan :CALCULATE_GUARANTEED_ANNUAL_INCOME :_formulaBean ]];
        
        gaiValue = [gaiValueStr doubleValue];
        totalGAIValue = totalGAIValue + gaiValue;
    }
    [_formulaBean setCounterVariable:counter];
    return totalGAIValue;
    
}

+(long)getMinimumPremium:(NSString *)_planCode andwithPayMode:(NSString *)payMode{
    
    long annual_min_prem = 0;
    NSMutableArray *minPremiumarr =oDataMapper.PNLMinPrem;
    for (int cnt = 0; cnt < minPremiumarr.count ; cnt++ ) {
        PNLMinPrem *pnlMinPremObj = [minPremiumarr objectAtIndex:cnt];
        if (([pnlMinPremObj.PML_PLAN_CD caseInsensitiveCompare:_planCode])&&([pnlMinPremObj.PML_PREMIUM_MODE caseInsensitiveCompare:payMode])){
            
            annual_min_prem = pnlMinPremObj.PML_MIN_PREM_AMT;
            break;
            
        }
    }
    return annual_min_prem;
    
}

+(double)getRBTBBonus:(FormulaBean *)_formulaBean andWithBonusTyrpe:(NSString *)bonusType andWithYear:(int)polYear{
    double gaiFactor = 0.0;
    PNLPlan *pnlPlanObj = [ self getPlan:_formulaBean.request.BasePlan];
    int pt =(int) _formulaBean.request.PolicyTerm;
    int count = _formulaBean.CounterVariable;
    int age = _formulaBean.request.InsuredAge;
    
    //Added by Amruta
    long coverage = _formulaBean.request.Sumassured;
    //CHANGED on 13/10/2015 BasePremium TO BasePremiumAnnual
    long premium = _formulaBean.request.BasePremiumAnnual;
    NSMutableArray *gaiFactorArr =oDataMapper.GAIGuarAnnInChMaster;
    for (int cnt = 0 ; cnt < gaiFactorArr.count ; cnt ++) {
        GAIGuarAnnIncChMaster *gaiGuarAnnIncChMasterObj = [gaiFactorArr objectAtIndex:cnt];
        if (([@"RB4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB4" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) ){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
            
        }else if (([@"RB8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB8" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) ){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if (([@"TB4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"TB4" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) && count >= polYear){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }else if (count < polYear){
                break;
            }
        }else if (([@"TB8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"TB8" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) && count >= polYear){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }else if (count < polYear){
                break;
            }
        }else if (([@"LB" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"LB" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(polYear >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(polYear <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) ){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }
        }else if (([@"RBG4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB4" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) ){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }
        }else if (([@"RBG8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB8" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) ){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }
        }else if (([@"RBS4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB4" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) &&(coverage >=gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(coverage <=gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }
        }else if (([@"RBS8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB8" caseInsensitiveCompare:bonusType]==NSOrderedSame) ){
            if( (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) &&(coverage >=gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(coverage <=gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
                
            }
        }//********************************SGP*********************************
        else if(([@"TB4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame) && ([@"TB4SGP" caseInsensitiveCompare:bonusType] == NSOrderedSame) ){
            if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
            }
        }
        else if(([@"TB8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame) && ([@"TB8SGP" caseInsensitiveCompare:bonusType] == NSOrderedSame) ){
            if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && count >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && count <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
            }
        }//********************************SGP*********************************
  
        //*******Added by Amruta on 19/8/15 for FG******************************
        
       else if (([@"MF_M" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame)&&
                ([@"MFV_M" caseInsensitiveCompare:bonusType] == NSOrderedSame)){
           if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&& (age >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && age <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) && (premium >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM && premium <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
               
               gaiFactor=gaiGuarAnnIncChMasterObj.GAI_CHARGE;
               break;
        
           }
       }
        
       else if (([@"MF_F" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame)&&
                ([@"MFV_F" caseInsensitiveCompare:bonusType] == NSOrderedSame)){
           if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&& (age >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM) && (age <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) && (premium >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM) && (premium <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
               
               gaiFactor=gaiGuarAnnIncChMasterObj.GAI_CHARGE;
               break;
          }
       }
        //************************************************************************
        
        //****************Added by Amruta on 15/9/15 for InstaWealth****************
        else if(([@"RBIW8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame) && ([@"RB8" caseInsensitiveCompare:bonusType] == NSOrderedSame)){
            if ((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && (coverage >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM) && (coverage <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }
        
        else if (([@"RBIW4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG]==NSOrderedSame) && ([@"RB4" caseInsensitiveCompare:bonusType]==NSOrderedSame)){
            if ((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && (coverage >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM) && (coverage <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)) {
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }
        //***********************************************************************************
        
        
        //change 16th oct SA
        else if([@"LB" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame && [@"LBSA" caseInsensitiveCompare:bonusType] == NSOrderedSame){
            //NSLog(@"pol year %d ---------- pt is %d",polYear,pt);
            if (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID && pt == gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && pt == gaiGuarAnnIncChMasterObj.GAI_MAX_TERM && polYear == pt) {
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
            }
        }
        
        else if([@"LB" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame && [@"LBIO" caseInsensitiveCompare:bonusType] == NSOrderedSame){
            if (pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID && polYear >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && polYear <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM) {
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                if (polYear == pt) {
                    gaiFactor += 0.046;
                }
            }
        
        }
        //good kid
        else if(([@"TB4" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame) && ([@"TB4GK" caseInsensitiveCompare:bonusType] == NSOrderedSame) ){
            if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && (count >= gaiGuarAnnIncChMasterObj.GAI_MIN_PPT && count <= gaiGuarAnnIncChMasterObj.GAI_MAX_PPT) && (pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                //NSLog(@"gai factor for good kid %f",gaiFactor);
            }
        }
        else if(([@"TB8" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame) && ([@"TB8GK" caseInsensitiveCompare:bonusType] == NSOrderedSame) ){
            if((pnlPlanObj.PNL_ID == gaiGuarAnnIncChMasterObj.GAI_PNL_ID) && (count >= gaiGuarAnnIncChMasterObj.GAI_MIN_PPT && count <= gaiGuarAnnIncChMasterObj.GAI_MAX_PPT) && (pt >= gaiGuarAnnIncChMasterObj.GAI_MIN_TERM && pt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)){
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                //NSLog(@"gai factor for good kid %f",gaiFactor);
            }
        }
    }
    return gaiFactor;
}

+(double)getBSVFactor:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    double svFactor ;
    int polTerm  = (int)_requestBean.PolicyTerm;
    PNLPlan *pnlplanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *svFactorDict =[self getBSFVSurrenderValueFactor:pnlplanObj.PNL_ID andWithTerm:polTerm];
    if ([svFactorDict objectForKey:[NSNumber numberWithInt:_cnt]]!= NULL) {
        svFactor =[[svFactorDict objectForKey:[NSNumber numberWithInt:_cnt]]doubleValue];
    }else{
        svFactor = 0.0;
    }
    return svFactor;
}

+(NSMutableDictionary *)getBSFVSurrenderValueFactor:(int)_planId andWithTerm:(int)term{
    
    NSMutableDictionary *rbFactorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *pmvMaturityValueArr = oDataMapper.PMVPlanMaturityValue;
    for (int cnt = 0; cnt < pmvMaturityValueArr.count ; cnt++) {
        PMVPlanMaturityValue *pmvPlanMaturityValueObj = [pmvMaturityValueArr objectAtIndex:cnt];
        if ([@"RB" isEqualToString:pmvPlanMaturityValueObj.PMV_BAND]) {
            if ((_planId == pmvPlanMaturityValueObj.PMV_PNL_ID) &&(pmvPlanMaturityValueObj.PMV_AGE == term)){
                
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL1] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR1]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL2] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR2]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL3] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR3]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL4] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR4]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL5] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR5]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL6] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR6]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL7] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR7]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL8] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR8]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL9] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR9]];
                [rbFactorDict setObject:[NSNumber numberWithDouble:pmvPlanMaturityValueObj.PMV_CVAL10] forKey:[NSNumber numberWithInt:pmvPlanMaturityValueObj.PMV_DUR10]];
                
            }
        }
    }
    return rbFactorDict;
    
    
}

+(double)getIPCFactor:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    double IPCFactor;
    int polTerm =(int) _requestBean.PolicyTerm;
    PNLPlan *pnlPlanObj =[self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *ipcFactorDict =[self getInfaltionProtectionCoverFactor:pnlPlanObj.PNL_ID andWithterm:polTerm];
    if ([ipcFactorDict count] != 0) {
        IPCFactor =[[ipcFactorDict objectForKey:[NSNumber numberWithInt:_cnt]] doubleValue];
    }else{
        IPCFactor = 0.0;
    }
    return IPCFactor;
    
}

+(NSMutableDictionary *)getInfaltionProtectionCoverFactor:(int)_planId andWithterm:(int)term{
    NSMutableDictionary *ipcFactorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *ipcValueArr =oDataMapper.IPCValues;
    for (int cnt = 0 ; cnt < ipcValueArr.count; cnt++) {
        IPCValues *ipcValueObj =[ipcValueArr objectAtIndex:cnt];
        if (_planId == ipcValueObj.IPC_PNL_ID) {
            [ipcFactorDict setObject:[NSNumber numberWithDouble:ipcValueObj.IPC_PREM_MULT] forKey:[NSNumber numberWithDouble:ipcValueObj.IPC_AGE]];
        }
    }
    return ipcFactorDict;
    
}

+(double)getRLCF:(FormulaBean *)_formulaBean andWithRequestBean:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    double rlcFactor;
    NSMutableDictionary *rlcFactorDict = nil;
    rlcFactorDict = [self getRLCFactor:_formulaBean andwithRequest:_requestBean andWithCount:_cnt];
    int factorCount = _cnt;
    int age = _requestBean.InsuredAge + _cnt -1;
    if (_formulaBean.AmountForServiceTax == 240) {
        //changes 6th oct
        age = (int)_requestBean.ProposerAge + _cnt - 1;
    }
    
    if (_formulaBean.AmountForServiceTax == 240 || _formulaBean.AmountForServiceTax == 239) {
        //changes 6th oct
        factorCount = (int)_requestBean.PremiumPayingTerm + 1 - _cnt<65 - age?(int)_requestBean.PremiumPayingTerm + 1 - _cnt:65-age;
        
    }
    if (_formulaBean.AmountForServiceTax == 242 || _formulaBean.AmountForServiceTax == 241) {
        factorCount = 1;
        
    }
    if ([rlcFactorDict objectForKey:[NSNumber numberWithInt:factorCount]] != NULL) {
        rlcFactor = [[rlcFactorDict objectForKey:[NSNumber numberWithInt:factorCount]] doubleValue];
    }else{
        rlcFactor = 0.0;
    }
    return rlcFactor;
    
    
}

+(NSMutableDictionary *)getRLCFactor:(FormulaBean *)_formulaBean andwithRequest:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    NSMutableDictionary *rlcFactorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *rlcValuesArr =oDataMapper.RDLCharges;
    int age = _requestBean.InsuredAge;
    int tempAge = age;
    int page =(int) _requestBean.ProposerAge;
    int ppt =(int)_requestBean.PremiumPayingTerm;
    NSString *gender_ins = _requestBean.InsuredSex;
    NSString *gender = gender_ins;
    NSString *gender_prop = _requestBean.ProposerSex;
    
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    
    int planId = pnlPlanObj.PNL_ID;
    double rdlId = [_formulaBean AmountForServiceTax];
    
    for (int cnt = 0; cnt <rlcValuesArr.count ; cnt++) {
       // NSLog(@"the value is %d",cnt);
        RDLChargesValue *rlcValuesObj =[rlcValuesArr objectAtIndex:cnt];
        if (ppt<=10) {
            ppt = 0;
        }
        if (rdlId == 240) {
            tempAge = page;
            gender = gender_prop;
        }else if (rdlId == 239){
            tempAge = age;
        }else{
            tempAge = age;
            ppt = 0;
        }
        //changes 4th nov cnt --> _cnt
        if ((((tempAge + (_cnt-1)) == rlcValuesObj.RCS_AGE) ||(rlcValuesObj.RCS_AGE == 999)) &&
            ([gender isEqualToString:rlcValuesObj.RCS_GENDER] || [rlcValuesObj.RCS_GENDER isEqualToString:@"U"])
            && (planId == rlcValuesObj.RCS_PNL_ID)
            && (rdlId == rlcValuesObj.RCS_RDL_ID)
            && ([rlcValuesObj.RCS_BAND intValue]== ppt)) {
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL1] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR1]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL2] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR2]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL3] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR3]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL4] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR4]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL5] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR5]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL6] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR6]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL7] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR7]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL8] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR8]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL9] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR9]];
            [rlcFactorDict setObject:[NSNumber numberWithDouble:rlcValuesObj.RCS_CVAL10] forKey:[NSNumber numberWithInt:rlcValuesObj.RCS_DUR10]];
        }
    }
    return rlcFactorDict;
}

+(double)getAAAFMC:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    
    double aaaFMCFactor;
    int age = _requestBean.InsuredAge;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *aaaFMCFactorDict = [self getAAAFMCFactor:pnlPlanObj.PNL_ID andWithAge:age+_cnt-1];
    if ([[aaaFMCFactorDict allKeys]objectAtIndex:(age+_cnt-1)] != NULL) {
        
        aaaFMCFactor = [[aaaFMCFactorDict valueForKey:[NSString stringWithFormat:@"%d",(age+_cnt-1)]]doubleValue];
    }else{
        aaaFMCFactor = 0.0;
    }
    return aaaFMCFactor;
}

+(NSMutableDictionary *)getAAAFMCFactor:(int)_planID andWithAge:(int)age{
    
    NSMutableDictionary *aaaFMCFcatorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *aaaFMCValuesArr =oDataMapper.AAAFMCValues;
    for (int cnt = 0 ; cnt < aaaFMCValuesArr.count; cnt++) {
        AAAFMCValues *aaaFMCObj = [aaaFMCValuesArr objectAtIndex:cnt];
        if( (_planID == aaaFMCObj.afc_paln_id) &&(age >= aaaFMCObj.afc_min_age)&&(age <= aaaFMCObj.afc_max_age)){
            
            [aaaFMCFcatorDict setObject:[NSNumber numberWithFloat:aaaFMCObj.afc_fmc] forKey:[NSNumber numberWithInt:age]];
        }
        
    }
    return aaaFMCFcatorDict;
}

+(double)getAAADFV:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    double aaaDFVFactor;
    int age = _requestBean.InsuredAge;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *aaaDFVFactorDict = [self getAAAFMCFactor:pnlPlanObj.PNL_ID andWithAge:age+_cnt-1];
    if ([[aaaDFVFactorDict allKeys]objectAtIndex:(age+_cnt-1)] != NULL) {
        
        aaaDFVFactor = [[aaaDFVFactorDict valueForKey:[NSString stringWithFormat:@"%d",(age+_cnt-1)]]doubleValue];
    }else{
        aaaDFVFactor = 0.0;
    }
    return aaaDFVFactor;
    
}

+(NSMutableDictionary *)getAAADFVFactor:(int)_planID andWithAge:(int)age{
    
    NSMutableDictionary *aaaDEBTFcatorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *aaaFMCValuesArr =oDataMapper.AAAFMCValues;
    for (int cnt = 0 ; cnt < aaaFMCValuesArr.count; cnt++) {
        AAAFMCValues *aaaFMCObj = [aaaFMCValuesArr objectAtIndex:cnt];
        if( (_planID == aaaFMCObj.afc_paln_id) &&(age >= aaaFMCObj.afc_min_age)&&(age <= aaaFMCObj.afc_max_age)){
            
            [aaaDEBTFcatorDict setObject:[NSNumber numberWithInt:aaaFMCObj.afc_whole_life_income] forKey:[NSNumber numberWithInt:age]];
        }
        
    }
    return aaaDEBTFcatorDict;
    
}

+(double)getAAAEFV:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    
    
    double aaaEFVFactor;
    int age = _requestBean.InsuredAge;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *aaaEFVFactorDict = [self getAAAEFVFactor:pnlPlanObj.PNL_ID andWithAge:age+_cnt-1];
    if ([[aaaEFVFactorDict allKeys]objectAtIndex:(age+_cnt-1)] != NULL) {
        
        aaaEFVFactor = [[aaaEFVFactorDict valueForKey:[NSString stringWithFormat:@"%d",(age+_cnt-1)]]doubleValue];
    }else{
        aaaEFVFactor = 0.0;
    }
    return aaaEFVFactor;
    
    
}

+(NSMutableDictionary *)getAAAEFVFactor:(int)_planID andWithAge:(int)age{
    
    NSMutableDictionary *aaaEQFcatorDict = [[NSMutableDictionary alloc]init];
    NSMutableArray *aaaFMCValuesArr =oDataMapper.AAAFMCValues;
    for (int cnt = 0 ; cnt < aaaFMCValuesArr.count; cnt++) {
        AAAFMCValues *aaaFMCObj = [aaaFMCValuesArr objectAtIndex:cnt];
        if( (_planID == aaaFMCObj.afc_paln_id) &&(age >= aaaFMCObj.afc_min_age)&&(age <= aaaFMCObj.afc_max_age)){
            
            [aaaEQFcatorDict setObject:[NSNumber numberWithInt:aaaFMCObj.afc_large_cap] forKey:[NSNumber numberWithInt:age]];
        }
        
    }
    return aaaEQFcatorDict;
    
    
}

+(PNLPlan *)getPnlPlanObj:(NSString *)_planCode{
    
    PNLPlan *pnlPlanObj = NULL;
    NSMutableDictionary *pnlPlanDict = oDataMapper.PNLPlan;
    pnlPlanObj = [pnlPlanDict valueForKey:_planCode];
    return pnlPlanObj;
    
}

#pragma mark - HSW
+(double)getGAIFactorForHSW:(SISRequest *)request{
    
    double gaiFactor = 0.0;
    PNLPlan *pnlPlanObj = [ self getPlan:request.BasePlan];
    int ppt =(int) request.PremiumPayingTerm;
    long premium = request.BasePremium;
    long long coverage = request.Sumassured;
    
    NSMutableArray *gaiFactorArr =oDataMapper.GAIGuarAnnInChMaster;
    
    for (int cnt = 0 ; cnt < gaiFactorArr.count ; cnt ++) {
        GAIGuarAnnIncChMaster *gaiGuarAnnIncChMasterObj = [gaiFactorArr objectAtIndex:cnt];
        if([@"P" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if ((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&(premium >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(premium <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }else if([@"CT" caseInsensitiveCompare:gaiGuarAnnIncChMasterObj.GAI_FLAG] == NSOrderedSame){
            if((pnlPlanObj.PNL_ID == (int)gaiGuarAnnIncChMasterObj.GAI_PNL_ID)&&
               (ppt >=gaiGuarAnnIncChMasterObj.GAI_MIN_TERM)&&(ppt <= gaiGuarAnnIncChMasterObj.GAI_MAX_TERM)&&(coverage >= gaiGuarAnnIncChMasterObj.GAI_MIN_PREM)&&(coverage <= gaiGuarAnnIncChMasterObj.GAI_MAX_PREM)){
                
                gaiFactor = gaiGuarAnnIncChMasterObj.GAI_CHARGE;
                break;
            }
        }
    }
    //NSLog(@"gai factor %f",gaiFactor);
    return gaiFactor;
    
}

//Added by Amruta on 18/8/15
+(NSString *)getCommissionText:(NSString *)_planCode{
    
    NSString *comText = @"";
    NSMutableArray *ComTextList = oDataMapper.CommissionDetail;
    
   for (int count=0; count <ComTextList.count; count++) {
        CommissionDetails *comObj = [ComTextList objectAtIndex:count];
       //changes 6th oct
       if([_planCode  isEqual: [NSString stringWithFormat:@"%d",comObj.CMT_PGL_ID]]/**/){
           
           comText = comObj.CMT_COMM_TEXT;
           break;
       }
       
    }
    return comText;
    
}


//Added by Amruta on 31/8/15 for MBP

+(double)getTotalGAIMBP:(FormulaBean *)_formulabean{
    
    double TotalGAIValue = 0;
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
    //changes 6th oct
    int cnt = (int)_formulabean.request.PolicyTerm;
    int icnt =  _formulabean.CounterVariable;
    
    if (icnt <= cnt) {
        for(int i =1; i<=cnt; i++){
            double GAIValue = 0;
            _formulabean.CounterVariable = i;
            NSString *GAIValueStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:_formulabean.request.BasePlan: CALCULATE_GUARANTEED_ANNUAL_INCOME:_formulabean]];
            
            GAIValue = [GAIValueStr doubleValue];
            TotalGAIValue = TotalGAIValue + GAIValue;
            
        }
    }
    _formulabean.CounterVariable = icnt;
    //NSLog(@"%f",TotalGAIValue);
    return TotalGAIValue;
}

//FREEDOM

+(double) getTB8Factor:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    double SVFactor;
    int polTerm = (int)_requestBean.PolicyTerm;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *SVFactorDict = [self getTBPT8Factor:pnlPlanObj.PNL_ID andWithTerm:polTerm];
    if([[SVFactorDict allKeys] count] != 0){
        SVFactor = [[SVFactorDict objectForKey:[NSNumber numberWithInt:_cnt]] doubleValue];
    }else
        SVFactor = 0;
    return  SVFactor;
}

+(double) getTB4Factor:(SISRequest *)_requestBean andWithCount:(int)_cnt{
    double SVFactor;
    int polTerm = (int)_requestBean.PolicyTerm;
    PNLPlan *pnlPlanObj = [self getPlan:_requestBean.BasePlan];
    NSMutableDictionary *SVFactorDict = [self getTBPT4Factor:pnlPlanObj.PNL_ID andWithTerm:polTerm];
    if([[SVFactorDict allKeys] count] != 0){
        SVFactor = [[SVFactorDict objectForKey:[NSNumber numberWithInt:_cnt]] doubleValue];
    }else
        SVFactor = 0;
    return  SVFactor;
}

+(NSMutableDictionary *)getTBPT8Factor:(int)_planID andWithTerm:(int)_term{
    NSMutableDictionary *SVFactorDict = [[NSMutableDictionary alloc] init];
    //BOOL PVFlag = false;
    NSMutableArray *PMVMaturityValueArr = oDataMapper.PMVPlanMaturityValue;
    for(int cnt =0;cnt < PMVMaturityValueArr.count;cnt++){
        PMVPlanMaturityValue *PMVPlanMaturityValueObj = [PMVMaturityValueArr objectAtIndex:cnt];
        //int band = [PMVPlanMaturityValueObj.PMV_BAND intValue];
        //PVFlag = true;
        
        if([@"T8" isEqualToString:PMVPlanMaturityValueObj.PMV_BAND]){
            if(_planID == PMVPlanMaturityValueObj.PMV_PNL_ID &&
               PMVPlanMaturityValueObj.PMV_AGE == _term){
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL1] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR1]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL2] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR2]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL3] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR3]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL4] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR4]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL5] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR5]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL6] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR6]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL7] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR7]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL8] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR8]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL9] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR9]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL10] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR10]];
            }
        }
    }
    return SVFactorDict;
}
+(NSMutableDictionary *)getTBPT4Factor:(int)_planID andWithTerm:(int)_term{
    NSMutableDictionary *SVFactorDict = [[NSMutableDictionary alloc] init];
    //BOOL PVFlag = false;
    NSMutableArray *PMVMaturityValueArr = oDataMapper.PMVPlanMaturityValue;
    for(int cnt =0;cnt < PMVMaturityValueArr.count;cnt++){
        PMVPlanMaturityValue *PMVPlanMaturityValueObj = [PMVMaturityValueArr objectAtIndex:cnt];
        //int band = [PMVPlanMaturityValueObj.PMV_BAND intValue];
        //PVFlag = true;
        
        if([@"T4" isEqualToString:PMVPlanMaturityValueObj.PMV_BAND]){
            if(_planID == PMVPlanMaturityValueObj.PMV_PNL_ID &&
               PMVPlanMaturityValueObj.PMV_AGE == _term){
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL1] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR1]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL2] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR2]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL3] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR3]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL4] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR4]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL5] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR5]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL6] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR6]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL7] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR7]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL8] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR8]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL9] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR9]];
                [SVFactorDict setObject:[NSNumber numberWithDouble:PMVPlanMaturityValueObj.PMV_CVAL10] forKey:[NSNumber numberWithInt:PMVPlanMaturityValueObj.PMV_DUR10]];
            }
        }
    }
    return SVFactorDict;
}

//SIP
+(double)getPremiumBoost:(FormulaBean *)_requestBean{
    double premiumBoost = 0;
    PNLPlan *plan = [self getPlan:_requestBean.request.BasePlan];
    NSMutableArray *arr = oDataMapper.premiumBoost;
    
    for (int cnt = 0; cnt < arr.count; cnt++) {
        PremiumBoost *boost = [arr objectAtIndex:cnt];
        if ([plan.PNL_CODE isEqualToString:boost.LPB_PLAN_CODE] && ((_requestBean.premium >= boost.LPB_MIN_PREMIUM && _requestBean.premium <= boost.LPB_MAX_PREMIUM))) {
            premiumBoost = boost.LPB_BOOST_RATE;
        }
    }
    
    return premiumBoost;
}


//changes 16th oct SA
+( double )getFOPRate:(SISRequest *)_bean andWithCnt:(int)_cnt{
    double foprate = 0;
    //NSMutableArray *arr = oDataMapper.FOPList;
    NSMutableArray *arr =oDataMapper.FOPList;
    NSString *mode = _bean.Frequency;
    
    PNLPlan *plan = [oDataMapper.PNLPlan objectForKey:_bean.BasePlan];
    int planId = plan.PNL_ID;
    
    for (int i = 0; i < arr.count; i++) {
        FOPRate *rate = [arr objectAtIndex:i];
        if (_cnt == rate.FOP_TERM
            && [mode isEqualToString:rate.FOP_MODE]
            && planId == rate.FOP_PNL_ID) {
            foprate = rate.FOP_CHARGE;
        }
    }
    return foprate;
}

//changes 19th oct IO
+(double) getNSAPMCRCharges:(SISRequest *)_req andWithTerm:(int)_term{
    NSMutableDictionary *pcrPlanChargeRefDict = oDataMapper.PCRPlanChargesRef;
    PNLPlan *plan = [oDataMapper.PNLPlan objectForKey:_req.BasePlan];
    int plnId = plan.PNL_ID;
    int age = _req.InsuredAge + (_term - 1);
    
    double dMCRCharge = 0;
    double NSAP = 0;
    
    PCRPlanChargesRef *pcrPlanCharge = [pcrPlanChargeRefDict objectForKey:[NSNumber numberWithInt:plnId]];
    NSMutableArray *mcmMorChargesArr = oDataMapper.MCMMortilityChargeMaster;
    
    MCMMortilityChargesMaster *mcmMortChargeMaster;
    
    for (int count =0; count < mcmMorChargesArr.count; count++) {
        mcmMortChargeMaster = [mcmMorChargesArr objectAtIndex:count];
        
        if (pcrPlanCharge.PCR_MORT_ID == mcmMortChargeMaster.MCM_ID && age == mcmMortChargeMaster.MCM_AGE) {
            dMCRCharge = mcmMortChargeMaster.MCM_MORT_CHARGE;
            if ([@"N" caseInsensitiveCompare:_req.AgeProofFlag] == NSOrderedSame) {
                NSAP = 2.5;
                dMCRCharge += NSAP;
            }
            break;
        }
    }
    return dMCRCharge;
}


@end
