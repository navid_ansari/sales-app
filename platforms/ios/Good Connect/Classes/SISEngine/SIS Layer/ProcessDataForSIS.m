 //
//  ProcessDataForSIS.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "ProcessDataForSIS.h"

@implementation ProcessDataForSIS

#pragma mark -- PUBLIC METHODS
-(NSMutableDictionary *) processDataForGeneratingSIS:(NSDictionary *)req{
    NSMutableDictionary *dict = [self getHtmlContent:req];
    return dict;
}

-(NSMutableDictionary *)getHtmlContent:(NSDictionary *)frontEndDict{
    ModelMaster *masterObj = [[ModelMaster alloc] init];
    NSMutableDictionary *SisData = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *error = [[NSMutableDictionary alloc] init];
    // get the data from the database..
    DataMapper *dataMapperObj =  [DataMapper getInstance];
    
    // get the data in the form of dictionary...
    @try{
        SISRequest *sisRequestObj = [self getRequest:frontEndDict WithDataMapper:dataMapperObj];
        sisRequestObj.RiderList = [self getRiders:frontEndDict withDataMapper:dataMapperObj];
        sisRequestObj.FundList = [self getFund:frontEndDict withDataMapper:dataMapperObj];
        
        sisRequestObj.BasePremium = round([self getBasePremiumBasedOnMode:sisRequestObj]);
        
        masterObj.sisRequest = sisRequestObj;
        masterObj.sisRequest.Illus_Page_No = [self generatePageList:masterObj andWithMapper:dataMapperObj];
        
        //changes 6th oct
        masterObj.ServiceTaxRenewal = [DataAccessClass getServiceTaxRenewal];
        NSLog(@"SIS REQUEST ENGINE ENDED");
        
        NSLog(@"SIS GENERATION ENGINE STARTED");
        //using the above data, create the SIS Engine.
        
        //changes 2nd march
        // for MRS rider prem check
        if ([sisRequestObj.BasePlan hasPrefix:@"MRS"] || [sisRequestObj.BasePlan hasPrefix:@"SR"] || [sisRequestObj.BasePlan hasPrefix:@"SRP"]) {
            if ([[frontEndDict objectForKey:@"ADDLN1V1"] longLongValue] != 0) {
                //check if rider premium is 30% of Basepremium
                long ValidPrem = 0.3 * sisRequestObj.BasePremium;
                long riderSA = [[frontEndDict objectForKey:@"ADDLN1V1"] longLongValue];
                
                DBManager *manager = [[DBManager alloc] init];
                double oclClass = [manager getPremMultFoMRS:sisRequestObj.InsuredOccupation];
                double modFactor = [DataAccessClass getModelFactor:sisRequestObj];
                
                long riderPrem = ((oclClass * riderSA)/1000) * modFactor;
                
                NSLog(@"rider premi is %ld and base premium is %ld",riderPrem,sisRequestObj.BasePremium);
                if (riderPrem > ValidPrem) {
                    //not valid
                    [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
                    [error setObject:[NSString stringWithUTF8String:"Total Rider premium should not be greater than 30% of Base Premium"] forKey:@"message"];
                    return error;
                }
            }
        }
        
        GenerateSIS *generateSISObj = [[GenerateSIS alloc] initWithMaster:masterObj];
        ModelMaster *master = [generateSISObj getSISData];
        
        //Added by Sneha 12-01-16 for hsw FM & WM
        if ([frontEndDict objectForKey:@"NoHtmlFlag"]) {
        
         // For Fortune Maxima - 200 and Wealth Pro - 201
            if ([[frontEndDict objectForKey:@"PglId"] isEqualToString:@"200"] || [[frontEndDict objectForKey:@"PglId"] isEqualToString:@"201"]) {
                if ([[frontEndDict objectForKey:@"Option"] isEqualToString:@"4"]) {
                    
                    [SisData setObject:[master.LoyalityChargeForPh6 objectForKey:[frontEndDict objectForKey:@"PolicyTerm"]] forKey:@"LoyaltyChargeDetails"];
                    [SisData setObject:[master.TotalFundValueAtEndPolicyYrPH6 objectForKey:[frontEndDict objectForKey:@"PolicyTerm"]] forKey:@"TotalFundValueEndPolicyYear"];
//                    [SisData setObject:master.TotalMaturityBenefit4 forKey:@"MaturityBenefit"];
                    
                } else if ([[frontEndDict objectForKey:@"Option"] isEqualToString:@"8"]) {

                    [SisData setObject:[master.LoyalityChargeForPh10 objectForKey:[frontEndDict objectForKey:@"PolicyTerm"]] forKey:@"LoyaltyChargeDetails"];
                    [SisData setObject:[master.TotalFundValueAtEndPolicyYrPH10 objectForKey:[frontEndDict objectForKey:@"PolicyTerm"]] forKey:@"TotalFundValueEndPolicyYear"];
//                    [SisData setObject:master.TotalMaturityBenefit8 forKey:@"MaturityBenefit"];
                }
                
                [SisData setObject:[NSNumber numberWithInt:1] forKey:@"code"];
                [SisData setObject:[NSString stringWithUTF8String:"Success"] forKey:@"message"];
            }
            return SisData;
            
        }
        
        //check if base plan is Blank..
        if(master.sisRequest.BasePlan.length != 0 || master.sisRequest.BasePlan  != NULL){
            HtmlBuilder *builder = [[HtmlBuilder alloc] initWithData:master];
            NSString *htmlPage = [builder buildHtml];
            
            /**************CREATE HTML PAGE IN DOCUMENT FOR TESTING NATIVE*********************/
            NSError *err;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"htmlCont.html"];
            [htmlPage writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&err];
            /**************CREATE HTML PAGE IN DOCUMENT FOR TESTING NATIVE*********************/
            
            //check if HTML content is Blank..
            if(htmlPage != NULL || htmlPage.length != 0){
                [SisData setObject:htmlPage forKey:@"SIS"];
                //13th oct check for tata flag..
                long discountedVal = 0;
                if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame){
                    discountedVal = master.DiscountedBasePremium;
                    //for A and O flag, discount value will remain same...
                    if([master.sisRequest.Frequency caseInsensitiveCompare:@"S"] == NSOrderedSame){
                        discountedVal = master.DiscountedBasePremium / 2;
                    }
                    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame){
                        discountedVal = master.DiscountedBasePremium / 4;
                    }
                    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
                        discountedVal = master.DiscountedBasePremium / 12;
                    }
                }
                //changes 27th oct premium issue.
                if ([sisRequestObj.ProductType isEqualToString:@"U"]) {
                    if ([master.sisRequest.PaymentMode isEqualToString:@"S"]) {
                        [SisData setObject:[NSNumber numberWithLongLong:round([master.AnnualPremium longLongValue]*0.5)] forKey:@"ModalPremium"];
                    }else if ([master.sisRequest.PaymentMode isEqualToString:@"Q"]) {
                        [SisData setObject:[NSNumber numberWithLongLong:round([master.AnnualPremium longLongValue]*0.25)] forKey:@"ModalPremium"];
                    }else if ([master.sisRequest.PaymentMode isEqualToString:@"M"]) {
                        [SisData setObject:[NSNumber numberWithLongLong:round([master.AnnualPremium longLongValue]*0.0833)] forKey:@"ModalPremium"];
                    }else if ([master.sisRequest.PaymentMode isEqualToString:@"A"]){
                        [SisData setObject:[NSNumber numberWithLongLong:round([master.AnnualPremium longLongValue])] forKey:@"ModalPremium"];
                    }else{
                        //changes 29th oct single pay issue
                        [SisData setObject:[NSNumber numberWithLong:master.sisRequest.BasePremiumAnnual] forKey:@"ModalPremium"];
                    }
                    if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame)
                        [SisData setObject:[NSNumber numberWithLong:master.DiscountedBasePremium] forKey:@"AnnualPremiumST"];
                    else
                        [SisData setObject:[NSNumber numberWithLong:master.BaseAnnualizedPremium] forKey:@"AnnualPremiumST"];
                    
                    [SisData setObject:[NSNumber numberWithLong:0] forKey:@"ServiceTax"];
                    [SisData setObject:[NSNumber numberWithLong:discountedVal] forKey:@"DiscountedModalPremium"];
                    [SisData setObject:[NSNumber numberWithLong:[generateSISObj getServiceTax:discountedVal]] forKey:@"DiscountedServiceTax"];
                }else{
                    //changes 27th oct premium issue.
                    //changes 22jan added nsap
                    [SisData setObject:[NSNumber numberWithLong:master.ModalPremium + master.ModalPremiumForNSAP] forKey:@"ModalPremium"];
                    //changes 22 jan removed service tax and nsap
                    if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame)
                        [SisData setObject:[NSNumber numberWithLong:master.DiscountedBasePremium] forKey:@"AnnualPremiumST"];
                    else
                        [SisData setObject:[NSNumber numberWithLong:master.BaseAnnualizedPremium] forKey:@"AnnualPremiumST"];
                    
                    [SisData setObject:[NSNumber numberWithLong:master.serviceTax] forKey:@"ServiceTax"];
                    [SisData setObject:[NSNumber numberWithLong:discountedVal] forKey:@"DiscountedModalPremium"];
                    [SisData setObject:[NSNumber numberWithLong:[generateSISObj getServiceTax:discountedVal]] forKey:@"DiscountedServiceTax"];
                    //GIP SAR changes
                    //NSMutableDictionary * dict = ;
                    NSNumber *val = [master.LifeCoverage objectForKey:[NSNumber numberWithInt:1]];
                    [SisData setObject:[NSNumber numberWithLong:[val longValue]] forKey:@"Sar"];
                    
                    
                }
                
                //changes 4nov for rider data..
                NSMutableArray *riderArr = [[NSMutableArray alloc] init];
                
                if (master.riderData.count > 0) {
                    for (int i=0; i< master.riderData.count; i++) {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        
                        Rider *rid = [master.riderData objectAtIndex:i];
                        //change 21march  key changes..
                        [dict setObject:[NSNumber numberWithInt:rid.PolTerm] forKey:@"PolicyTerm"];
                        [dict setObject:[NSNumber numberWithInt:rid.PremPayTerm] forKey:@"PremiumPayTerm"];
                        [dict setObject:[NSNumber numberWithLong:rid.ModalPremium] forKey:@"ModalPremium"];
                        //changes 22 jan
                        [dict setObject:[NSNumber numberWithLong:master.STRiderModalPremium] forKey:@"RiderServiceTax"];
                        [dict setObject:rid.RDL_CODE forKey:@"RiderCode"];
                        [riderArr addObject:dict];
                    }
                }
                //change 21march  key changes..
                [SisData setObject:riderArr forKey:@"riderData"];
                [SisData setObject:[NSNumber numberWithInt:1] forKey:@"code"];
                [SisData setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
            }else{
                [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
                [error setObject:[NSString stringWithUTF8String:"SIS REPORT NOT GENERATED"] forKey:@"message"];
                return error;
            }
        }else{
            [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
            [error setObject:[NSString stringWithUTF8String:"PLEASE SELECT THE BASE PLAN"] forKey:@"message"];
            return error;
        }        
        
    }@catch(NSException *e){
        NSLog(@"the exceptio %@",e.debugDescription);
        [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
        [error setObject:[NSString stringWithFormat:@"Exception Occured %@",e.description] forKey:@"message"];
        return error;
    }
   
    
    return SisData;
}

#pragma mark -- PRIVATE METHODS
//get the Front end data and add it to the SIS Request Object...
-(SISRequest *) getRequest:(NSDictionary *)_reqDict WithDataMapper:(DataMapper *)_mapper{
    SISRequest *request = [[SISRequest alloc] init];
    request.InsuredName = [_reqDict valueForKey:@"InsName"];
    request.InsuredAge = [[_reqDict valueForKey:@"InsAge"] intValue];
    request.InsuredSex = [_reqDict valueForKey:@"InsSex"];
	//changes 27th jan InsDOB
    request.InsuredDOB = [_reqDict valueForKey:@"InsDOB"];
    request.InsuredOccupation_Desc = [_reqDict valueForKey:@"InsOccDesc"];
    request.InsuredOccupation = [_reqDict valueForKey:@"InsOcc"];
    request.OwnerName = [_reqDict valueForKey:@"OwnName"];
    request.OwnerAge = [[_reqDict valueForKey:@"OwnAge"] intValue];
    request.OwnerOccupation = [_reqDict valueForKey:@"OwnOcc"];
    request.OwnerSex = [_reqDict valueForKey:@"OwnSex"];
    request.OwnerOccupationDesc = [_reqDict valueForKey:@"OwnOccDesc"];
    request.InsuredSmokerStatus = [_reqDict valueForKey: @"InsSmoker"];
    request.PaymentMode = [_reqDict valueForKey:@"PaymentMode"];
    //changes 1 feb
    if ([[_reqDict valueForKey:@"BasePlan"] containsString:@"MI7"]) {
        request.BasePlan = @"MI7MV1P1";
    }else if([[_reqDict valueForKey:@"BasePlan"] containsString:@"MI10"]){
        request.BasePlan = @"MI10MV1P1";
    }else{
        request.BasePlan = [_reqDict valueForKey:@"BasePlan"];
    }
    if ([[_reqDict valueForKey:@"BasePlan"] containsString:@"GIP5I10"] || [[_reqDict valueForKey:@"BasePlan"] containsString:@"GIP12I10"]) {
        request.incomeBooster = 10;
    }else if ([[_reqDict valueForKey:@"BasePlan"] containsString:@"GIP12I15"] || [[_reqDict valueForKey:@"BasePlan"] containsString:@"GIP5I15"]){
        request.incomeBooster = 15;
    }
    NSString *SA = [_reqDict valueForKey:@"Sumassured"];
    request.Sumassured = [SA longLongValue];
    request.PolicyTerm = [[_reqDict valueForKey:@"PolicyTerm"] integerValue];
    request.PremiumPayingTerm = [[_reqDict valueForKey:@"PPTerm"] integerValue];
    request.BasePremium = [[_reqDict valueForKey:@"BasePremium"] intValue];
    request.ProposalNo = [_reqDict valueForKey:@"PropNo"];
    request.AgeProofFlag = [_reqDict valueForKey:@"SAgeProofFlg"];
	//changes on 27th jan PropDOB
    request.ProposerDOB = [_reqDict valueForKey:@"PropDOB"];
    request.ProductType = [_reqDict valueForKey:@"ProdTye"];
    request.RPU_Year = [_reqDict valueForKey:@"RpuYear"];
    request.TaxSlab = [_reqDict valueForKey:@"TaxSlab"];
    request.Exp_Bonus_Rate = [_reqDict valueForKey:@"ExpBonusRate"];
    request.Premium_Mul = [_reqDict valueForKey:@"PremiumMul"];
    request.Frequency = [_reqDict valueForKey:@"PaymentMode"];
    request.TataEmployee = [_reqDict valueForKey:@"TataEmployee"];
    request.ProposerName = [_reqDict valueForKey:@"ProposerName"];
    request.ProposerOccupation = [_reqDict valueForKey:@"ProposerOccupation"];
    request.ProposerOccupation_Desc = [_reqDict valueForKey:@"ProposerDescOcc"];
    request.ProposerSex = [_reqDict valueForKey:@"ProposerGender"];
    request.ProposerAge = [[_reqDict valueForKey:@"ProposerAge"] intValue];
	request.BusinessType = [_reqDict valueForKey:@"businessType"];
    //*********************************ULIP*********************************
    request.BasePremiumAnnual = [[_reqDict valueForKey:@"BasePremiumAnnual"] intValue];
    request.SmartDebtFund = [_reqDict valueForKey:@"SmartDebtFund"];
    request.SmartEquityFund = [_reqDict valueForKey:@"SmartEquFund"];
    //request.Premium_Mul = [_reqDict valueForKey:@"PremiumMult"];
    request.SmartDebtFundFMC = [self getFMCForFund:[_reqDict valueForKey:@"SmartDebtFund"] andWithMapper:_mapper];
    request.SmartEquityFundFMC = [self getFMCForFund:[_reqDict valueForKey:@"SmartEquFund"] andWithMapper:_mapper];
    request.Commission = [_reqDict valueForKey:@"commision"];
	
    //*********************************ULIP*********************************
    
    /*
         
         //    request.InsuredName  =[request valueForKey:@"InsuredName"];
         //    request.InsuredAge = [[request valueForKey:@"InsuredAge"] intValue];
         //    request.InsuredSex = [request valueForKey:@"InsuredSex"];
         //    request.InsuredSmokerStatus = [request valueForKey:@"InsuredSmokerStatus"];
         //    request.InsuredOccupation = [request valueForKey:@"InsuredOccupation"];
         //    request.ProposerName = [request valueForKey:@"ProposerName"];
         //    request.ProposerSex = [request valueForKey:@"ProposerSex"];
         //    request.ProposerOccupation = [request valueForKey:@"ProposerOccupation"];
         request.BasePlan = [request valueForKey:@"BasePlan"];
        request.Sumassured = [[request valueForKey:@"Sumassured"] longValue];
        request.PolicyTerm = [[request valueForKey:@"PolicyTerm"] integerValue];
        request.ProductType = [request valueForKey:@"ProductType"];
        request.PremiumPayingTerm = [[request valueForKey:@"PremiumPayingTerm"] integerValue];
        request.OwnerAge = [[request valueForKey:@"OwnerAge"] intValue];
        request.InsuredOccupation_Desc = [request valueForKey:@"InsuredOccupation_Desc"];
        request.ProposerOccupation_Desc = [request valueForKey:@"ProposerOccupation_Desc"];
        request.ProposalNo = [request valueForKey:@"ProposalNo"];
        request.ProposalDate = [request valueForKey:@"ProposalDate"];
        request.TataEmployee = [request valueForKey:@"TataEmployee"];
        request.InsuredDOB = [request valueForKey:@"InsuredDOB"];
        request.ProposerDOB = [request valueForKey:@"ProposerDOB"];
    
        request.Premium_Mul = [request valueForKey:@"Premium_Mul"];
        request.BasePremium = [[request valueForKey:@"BasePremium"] longValue];
        request.BasePremiumAnnual = [[request valueForKey:@"BasePremiumAnnual"] longValue];
    
        //to ask Vivek
        request.RPU_Year = [request valueForKey:@"RPU_Year"];
        request.OperationType = [request valueForKey:@"OperationType"];
        request.TaxSlab = [request valueForKey:@"TaxSlab"];
        request.FixWithDEndYr = [request valueForKey:@"FixWithDEndYr"];
        request.FixWithDAmt = [request valueForKey:@"FixWithDAmt"];
        request.FixTopUpAmt = [request valueForKey:@"FixTopUpAmt"];
        request.FixTopUpStartYr = [request valueForKey:@"FixTopUpStartYr"];
        request.FixTopUpEndYr = [request valueForKey:@"FixTopUpEndYr"];
        request.VarWithDAmtYr = [request valueForKey:@"VarWithDAmtYr"];
        request.VarTopUpAmtYr = [request valueForKey:@"VarTopUpAmtYr"];
        request.FundPerform = [request valueForKey:@"FundPerform"];
        request.SmartDebtFund = [request valueForKey:@"SmartDebtFund"];
        request.SmartDebtFundFMC = [request valueForKey:@"SmartDebtFundFMC"];
        request.SmartEquityFund = [request valueForKey:@"SmartEquityFund"];
        request.SmartEquityFundFMC = [request valueForKey:@"SmartEquityFundFMC"];
        request.AAA = [request valueForKey:@"AAA"];
        request.Exp_Bonus_Rate = [request valueForKey:@"Exp_Bonus_Rate"];
    
        request.AgeProofFlag = [request valueForKey:@"AgeProofFlag"];
        request.AgeProof = [request valueForKey:@"AgeProof"];
        request.Commission = [request valueForKey:@"Commission"];*/
    
    return  request;
}
//get riders..
-(NSMutableArray *) getRiders:(NSDictionary *)_reqDict withDataMapper:(DataMapper *)_mapper{
    NSMutableArray *riderList = [[NSMutableArray alloc] init];
    Rider *rider = NULL;
    NSMutableDictionary *riderDict = _mapper.RDLRider;
    
    for (id key in riderDict) {
        NSString *riderSA = [NSString stringWithFormat:@"%@",[_reqDict valueForKey:key]]; //Sneha [_reqDict valueForKey:key];
        Rider *riderObj = [riderDict objectForKey:key];
        NSLog(@"Rider SA %@",riderSA);
		NSLog(@"Rider code %@",riderObj.RDL_CODE);
		
        // && riderSA.length != 0
		//migration change
        if ((riderObj != NULL && [riderSA longLongValue] != 0 && riderSA != nil) ||
            (([riderObj.RDL_CODE isEqualToString:@"WOPULV1"] || [riderObj.RDL_CODE isEqualToString:@"WOPPULV1"] || [riderObj.RDL_CODE isEqualToString:@"WPPN1V1"]) && [riderSA isEqualToString:@"0"]) ) {
            rider = [[Rider alloc] init];
            rider.RDL_CODE = key;
            rider.RDL_DESCRIPTION = riderObj.RDL_DESCRIPTION;
            rider.SumAssured = [riderSA longLongValue];
            rider.RDL_RTL_ID = riderObj.RDL_RTL_ID;
            rider.RDLID = riderObj.RDLID;
            rider.RDL_UIN = riderObj.RDL_UIN;
            
            NSLog(@"the rdl code is %@",key);
            
            [riderList addObject:rider];
        }
    }
    
    
    return riderList;
}
//get fund details
-(NSMutableArray *) getFund:(NSDictionary *)_reqDict withDataMapper:(DataMapper *)_mapper{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    FundDetails *detail = NULL;
    
    NSMutableDictionary *dict = _mapper.FDLFund;
    //NSArray *arr = [dict allKeys];
    for (id key in dict) {
        NSString *fundPercent = [_reqDict valueForKey:key];
		//NSLog(@"fundPercent: %@ [_reqDict valueForKey:key]: %@", fundPercent, [_reqDict valueForKey:key]);
        
        FundDetails *detailFnd = [dict objectForKey:key];
        detail = [[FundDetails alloc] init];
        //NSLog(@"key: %@ detailFnd.fund_Description: %@ detailFnd.Fund_Mgmt_charge: %f", key, detailFnd.fund_Description, detailFnd.Fund_Mgmt_charge);
        detail.Code = key;
        detail.fund_Description = detailFnd.fund_Description;
        detail.Fund_Mgmt_charge = detailFnd.Fund_Mgmt_charge;
        if(fundPercent != nil && [fundPercent longLongValue] != 0  && fundPercent !=NULL) //![fundPercent isEqualToString:@"0"] ![fundPercent isEqualToString:@""]
            detail.Percentage = [fundPercent intValue];
        else
            detail.Percentage = 0;
        [array addObject:detail];
    }
    return array;
}

-(double)getFMCForFund:(NSString *)fundName andWithMapper:(DataMapper *)mapper{
    NSMutableDictionary *dict = mapper.FDLFund;
    for (id key in dict) {
        FundDetails *details = [dict objectForKey:key];
        if([details.Code isEqualToString:fundName]){
            NSLog(@"Fund Name%@",fundName);
            return details.Fund_Mgmt_charge;
        }
    }
    
    return 0;
}

-(double )getBasePremiumBasedOnMode:(SISRequest *)_request{
    double premiumAmt = 0;
    double premMult = [_request.Premium_Mul doubleValue];
    double modFactor = [DataAccessClass getModelFactor:_request];
    long long sumassured = _request.Sumassured;
    
    //Changed by Amruta from _request.Premium_Mul.length!=0 to premMult!=0
    if (premMult != 0 || _request.Premium_Mul != NULL) {
        //Added for IO
        if (modFactor != 0) {
            premiumAmt = (_request.Sumassured * modFactor )/ premMult;
        }else
            premiumAmt = _request.Sumassured / premMult;
        //premiumAmt = (_request.Sumassured * modFactor )/ premMult;
    }else {
        double premMultiple = [DataAccessClass getPremiumMultiple:_request andWithMIP:NO];
        //premiumAmt =  round(round(((sumassured * premMultiple)) /1000) * modFactor);
        //changes 6th oct
        if ([_request.BasePlan hasPrefix:@"MRS"] || [_request.BasePlan hasPrefix:@"SR"] || [_request.BasePlan hasPrefix:@"SRP"]) {
          premiumAmt =  round(round(((sumassured * premMultiple)) /1000) * modFactor);
        }else
        premiumAmt = _request.BasePremium;
    }

    //check for 198 ,207, 208, 146, 203 is removed as we dont have that plans..
    //also rider related premium removed..
    NSLog(@"premium amount during sis calculation %f",premiumAmt);
    return premiumAmt;
}

-(NSString *)generatePageList:(ModelMaster *)master andWithMapper:(DataMapper *)oDataMapper{
    NSString *pageList = @"";
    NSString *pageNos = @"";
    
    PNLPlan *plan = NULL;
    
    NSMutableDictionary *dict = oDataMapper.PNLPlan;
    plan = [dict valueForKey:master.sisRequest.BasePlan];
    
    int planID = plan.PNL_PGL_ID;
    //changes 6th oct
    int pt = (int)master.sisRequest.PolicyTerm;
    
    if (planID == 198 || planID == 199 || planID == 200 || planID == 201 || planID == 202) {
        int additionalPages = 0;
        NSMutableArray *stdTempValues = oDataMapper.StdTemplateValues;
        NSMutableArray *riderData = master.riderData;
        
        for (int i =0; i<stdTempValues.count; i++) {
            STDTemplateValues *values = [stdTempValues objectAtIndex:i];
            if (planID == values.STD_PGL_ID) {
                int NOP = (pt > values.STD_MIN_PT) ? values.STD_MAX_NO_PAGES : values.STD_NO_PAGES;
                int startPt = values.STD_MIN_STARTP + additionalPages;
                
                if ([values.STD_MANDATORY isEqualToString:@"Y"]) {
                    if ([values.STD_TEMP_DESC isEqualToString:@"Illustration"]) {
                        if (planID == 200 || planID == 201) {
                            pageNos = @"4 to 7";
                        }else{
                            pageNos = @"4 &amp; 5";
                        }
                        if (pt > values.STD_MIN_PT) {
                            if (planID == 200 || planID == 201) {
                                pageNos = @"4 to 9";
                            }else{
                                pageNos = @"4 to 7";
                            }
                        }
                        //master.sisRequest.Illus_Page_No = pageNos;
                    }
                    
                    for (int cnt = 0; cnt < NOP; cnt++) {
                        pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,cnt];
                    }
                }
                else{
                    if ([values.STD_TEMP_DESC isEqualToString:@"ADDL Rider"] ||
                        [values.STD_TEMP_DESC isEqualToString:@"WOP Rider"] ||
                        [values.STD_TEMP_DESC isEqualToString:@"WOPP Rider"]) {
                        for (int i = 0; i < riderData.count; i++) {
                            Rider *rider = [riderData objectAtIndex:i];
                            if ([rider.RDL_CODE isEqualToString:values.STD_RDL_CODE]) {
                                for (int j =0; j < NOP; j++) {
                                    pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                                }
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"Reduced Paid Up"]) {
                        if (master.sisRequest.RPU_Year!= NULL && ![master.sisRequest.RPU_Year isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"Tax Benefit"]) {
                        if (![master.sisRequest.TaxSlab isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"Experience Based Rate"]) {
                        if (master.sisRequest.Exp_Bonus_Rate!= NULL && ![master.sisRequest.Exp_Bonus_Rate isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"Experience based rate and Tax benefit"]) {
                        if (![master.sisRequest.Exp_Bonus_Rate isEqualToString:@"0"] &&
                            ![master.sisRequest.TaxSlab isEqualToString:@"0"] &&
                            master.sisRequest.TaxSlab != NULL) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"TOPUP AND PWD"]) {
                        if (![master.sisRequest.TopUpWDSelected isEqualToString:@"0"] &&
                            ![master.sisRequest.FpSelected isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"SMART"]) {
                        if (![master.sisRequest.SmartSelected isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                    if ([values.STD_TEMP_DESC isEqualToString:@"AAA"]) {
                        if (![master.sisRequest.AAASelected isEqualToString:@"0"]) {
                            for (int j =0; j < NOP; j++) {
                                pageList = [NSString stringWithFormat:@"%@%d%d",pageList,startPt,j];
                            }
                        }
                    }
                    
                }
                
                if (pt > values.STD_MIN_PT) {
                    additionalPages += values.STD_MAX_NO_PAGES - values.STD_NO_PAGES;
                }
            }
        }
        
        plan = [dict valueForKey:master.sisRequest.BasePlan];
        
        if (planID == 200 || planID == 199 || planID == 200 || planID == 201 || planID == 202) {
            NSMutableArray *arr = oDataMapper.IllusPageList;
            for (int cnt =0; cnt < arr.count; cnt++) {
                iLLusPageList *page = [arr objectAtIndex:cnt];
                if (planID == page.IPL_PGL_ID) {
                    if (pt > page.IPL_MIN_PT) {
                        pageNos = page.IPL_MAX_PAGE;
                    }else{
                        pageNos = page.IPL_MIN_PAGE;
                    }
                }
            }
        }
    }
    NSLog(@"the page no is %@",pageNos);
    return pageNos;
}

//Rider prem 2nd march
-(long) getRiderPrem:(SISRequest *)_request andWithRiderSA:(long)riderSA{
    long riderPrem = 0;
   
    long riderSAVal = riderSA;
    
    
    
    
    
    return riderPrem;
}
@end
