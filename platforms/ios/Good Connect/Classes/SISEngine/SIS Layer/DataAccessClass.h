//
//  DataAccessClass.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataMapper.h"
#import "PFRPlanFormulaRef.h"
#import "SISRequest.h"
#import "PNLPlan.h"
#import "Rider.h"
#import "RMLRiderPremMultiple.h"
#import "PCRPlanChargesRef.h"
#import "PACPremiumAllocationCharges.h"
#import "WCMWOPChargesMaster.h"
#import "FCMFIBChargersMaster.h"
#import "MCMMortilityChargesMaster.h"
#import "SURSurrenderChargesMaster.h"
#import "GuaranteedMaturityBonusMaster.h"
#import "FundDetails.h"
#import "PGFPlanGroupFeature.h"
#import "AddressBO.h"
#import "CCMommissionChargeMaster.h"
#import "FormulaBean.h"
#import "FormulaHandler.h"
#import "GAIGuarAnnIncChMaster.h"
#import "PMVPlanMaturityValue.h"
#import "RDLChargesValue.h"
#import "AAAFMCValues.h"


@interface DataAccessClass : NSObject

+(NSString *) getSPVSysParamValues:(NSString *)_planCode;
+(PFRPlanFormulaRef *) getFormula:(NSString *)_formulaKey andWithValue:(NSString *)_value;
+(NSMutableDictionary *)getFormulaExpression:(NSString *)_formulaID;

//changes 27th jan
+(double)getPremiumMultiple:(SISRequest *)_requestBean andWithMIP:(BOOL)_mip;

//+(double)getPremiumMultiple:(SISRequest *)_requestBean;
+(double) getCSVFactor:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double) getGSVFactor:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getRiderPremiumMultiple:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(long)getRiderSumAssuared:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getWpLoadValue:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getInsLoadValue:(SISRequest *)_requestBean ;
+(double)getPAChargeValue:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt;
+(double)getWOPCharges:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getFIBCharges:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getMCRChares:(SISRequest *)_requestBean AndWithCount:(int)_cnt;
+(double)getSurrenderChargeValue:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt;
+(double)getSurrenderChargeAmount:(SISRequest *)_requestBean andWithPremium:(long)premium andWithCount:(int)_cnt;
+(double)getGuaranteedMaturityBonus:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(double)getFMC:(SISRequest *)_requestBean;
+(PGFPlanGroupFeature *)getPlanGroupFeature:(NSString *)_planCode;
+(AddressBO *)getAddressBO;
+(NSString *)getPlanType:(NSString *)_planCode;
+(double)getCommissionPercentage:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(double)getMVFactor:(SISRequest *)_requestBean;
+(long)getDisCountedPremium:(FormulaBean *)_formulaBean;
+(long)getTotalPremium:(FormulaBean *)_formulaBean;
+(long)getTotalPremiumULIP:(FormulaBean *)_formulaBean;
+(long)getTotalRemainingPremium:(FormulaBean *)_formulaBean;
+(double)getGAIFactor:(FormulaBean *)_formulaBean;
+(double)getTotalGAI:(FormulaBean *)_formulaBean;
+(double)getTotalGAIMLG:(FormulaBean *)_formulaBean;
+(long)getMinimumPremium:(NSString *)_planCode andwithPayMode:(NSString *)payMode;
+(double)getRBTBBonus:(FormulaBean *)_formulaBean andWithBonusTyrpe:(NSString *)bonusType andWithYear:(int)polYear;
+(double)getBSVFactor:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getBSFVSurrenderValueFactor:(int)_planId andWithTerm:(int)term;
+(double)getIPCFactor:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getInfaltionProtectionCoverFactor:(int)_planId andWithterm:(int)term;
+(double)getRLCF:(FormulaBean *)_formulaBean andWithRequestBean:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getRLCFactor:(FormulaBean *)_formulaBean andwithRequest:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(double)getAAAFMC:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getAAAFMCFactor:(int)_planID andWithAge:(int)age;
+(double)getAAADFV:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getAAADFVFactor:(int)_planID andWithAge:(int)age;
+(double)getAAAEFV:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(NSMutableDictionary *)getAAAEFVFactor:(int)_planID andWithAge:(int)age;
+(PNLPlan *)getPnlPlanObj:(NSString *)_planCode;
+(int)getPBABand:(NSMutableArray *)pbaBandArr andWithSumAssured:(long)sumAssured;
+(double)getModelFactor:(SISRequest *)_requestBean;
+(AgentDetail *)getAgentDetail;
//Added by Amruta for MIP WPP Rider
+(double)getRiderModelFactor:(SISRequest *)_requestBean;
//+(AgentDetail *)getCommissionDetails;

//********************************SGP*********************************
+(double)getPremiumDiscount:(NSString *)_planCode andWithSumAssured:(long)_sumassured;
//********************************SGP*********************************
//NEW HSW
+(double)getGAIFactorForHSW:(SISRequest *)request;

//changes 16th oct for IO/SA
+(double)getFOPRate:(SISRequest *)_bean andWithCnt:(int)_cnt;
+(double) getNSAPMCRCharges:(SISRequest *)_req andWithTerm:(int)_term;

+(double)getServiceTaxMain;
+(double)getServiceTaxFY;
+(double)getServiceTaxRenewal;
+(long) getTotalAnnualPremium:(FormulaBean *)_bean;

//Added by Amruta on 18/8/15 for FG
+(NSString *)getCommissionText:(NSString *)_planCode;

//Added by Amruta on 31/8/15 for MBP
+(double)getTotalGAIMBP:(FormulaBean *)_bean;

//Freedom
+(double) getTB8Factor:(SISRequest *)_requestBean andWithCount:(int)_cnt;
+(double) getTB4Factor:(SISRequest *)_requestBean andWithCount:(int)_cnt;

//SIP
+(double)getPremiumBoost:(FormulaBean *)_requestBean;



@end
