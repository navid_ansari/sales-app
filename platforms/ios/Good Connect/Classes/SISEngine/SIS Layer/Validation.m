////
////  Validation.m
////  Life_Planner
////
////  Created by admin on 30/01/15.
////
////
//
//#import "Validation.h"
//
//@implementation Validation
//@synthesize versionNo;
//
////changes 24th nov..
//#pragma mark - Combo
//-(void)StartCombo{
//    CDVPluginResult *result;
//    NSLog(@"the value is %@",command.arguments);
//    NSString *decodeString = [command.arguments objectAtIndex:0];
//    
//    NSData *encodeData = [decodeString dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *base64String = [encodeData base64EncodedStringWithOptions:0];
//    
//    NSString *customURL = [NSString stringWithFormat:@"com.tataaia.goodsolutions://?%@",base64String];
//    if ([[UIApplication sharedApplication]
//         canOpenURL:[NSURL URLWithString:customURL]])
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
//        
//        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    }
//    else
//    {
//        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error"
//         message:[NSString stringWithFormat:
//         @"No custom URL defined for %@", customURL]
//         delegate:self cancelButtonTitle:@"Ok"
//         otherButtonTitles:nil];
//         [alert show];*/
//        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//        
//    }
//    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
//}
//
//#pragma Mark -- Plugins
//-(void )validateAnnualizedPremium:(CDVInvokedUrlCommand*)command{
//    NSString *_request1 = [command.arguments objectAtIndex:0];
//    NSData *data = [_request1 dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *err;
//    NSDictionary *_request = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
//    NSLog(@"the request is %@",[_request objectForKey:@"REQ"]);
//    NSLog(@"NATIVE FUNCTION CALLED 2");
//    
//    CDVPluginResult *pluginResult = [self validatePremium:[_request objectForKey:@"REQ"]];
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//-(void)getCustomerType:(CDVInvokedUrlCommand *)command{
// //AppDelegate *delegate =  [[UIApplication sharedApplication] delegate];
// //CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:delegate.BusinessTypeId];
//    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:1];
// 
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
// 
//-(void)convertFileToContent:(CDVInvokedUrlCommand *)command{
//    NSString *pglID = [command.arguments objectAtIndex:0];
//    NSString *inputFile;
//    //20April2016
//    if([pglID isEqualToString:@"185"] || [pglID isEqualToString:@"191"])
//    {
//        NSLog(@"IRS App form");
//        inputFile =[[NSBundle mainBundle] pathForResource:@"HTML/standard-proposal-withborders-irs" ofType:@"html" inDirectory:@"www"];
//    }
//    else{
//        NSLog(@"NON - IRS App form");
//      inputFile =[[NSBundle mainBundle] pathForResource:@"HTML/standard-proposal-withborders" ofType:@"html" inDirectory:@"www"];
//    }
//   
//    /*[[NSBundle mainBundle] pathForResource:@"standard-proposal" ofType:@"html"];*/
//    //
//    NSLog(@"the HTML file is %@",inputFile);
//    CDVPluginResult *pluginResult = NULL;
//    
//    if(inputFile != nil){
//        NSData *myData =[NSData dataWithContentsOfFile:inputFile];
//        NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];
//        
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:inputString];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
//    }
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//-(void)FHRDeviationContent:(CDVInvokedUrlCommand *)command{
//    NSString *inputFile =[[NSBundle mainBundle] pathForResource:@"FHR_Deviation" ofType:@"html"];
//    /*[[NSBundle mainBundle] pathForResource:@"standard-proposal" ofType:@"html"];*/
//    //
//    NSLog(@"the HTML file is %@",inputFile);
//    CDVPluginResult *pluginResult = NULL;
//    
//    if(inputFile != nil){
//        NSData *myData =[NSData dataWithContentsOfFile:inputFile];
//        NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];
//        
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:inputString];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
//    }
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//#pragma Mark - Public Methods..
//-(double )getBasePremiumBasedOnMode:(SISRequest *)_request{
//    double premiumAmt = 0;
//    double premMult = [_request.Premium_Mul doubleValue];
//    double modFactor = [DataAccessClass getModelFactor:_request];
//    long long sumassured = _request.Sumassured;
//    
//    
//    if (_request.Premium_Mul.length != 0 || _request.Premium_Mul != NULL) {
//        premiumAmt = (_request.Sumassured * modFactor )/ premMult;
//    }else
//    {
//        double premMultiple = [DataAccessClass getPremiumMultiple:_request andWithMIP:NO];
//        premiumAmt =  round(round((sumassured * premMultiple) /1000) * modFactor);
//    }
//    
//    //check for 198 ,207, 208, 146, 203 is removed as we dont have that plans..
//    //also rider related premium removed..
//    NSLog(@"premium amount during sis calculation %f",premiumAmt);
//    return premiumAmt;
//}
//
//-(CDVPluginResult *)validatePremium:(NSDictionary *)_request{
//    ModelMaster *masterObj = [[ModelMaster alloc] init];
//    NSMutableDictionary *error = [[NSMutableDictionary alloc] init];
//    // get the data from the database..
//    
//    SISRequest *sisRequestObj = [self getRequest:_request];
//    sisRequestObj.BasePremium = round([self getBasePremiumBasedOnMode:sisRequestObj]);
//    masterObj.sisRequest = sisRequestObj;
//    
//    NSLog(@"Premium calculation");
//    
//    GenerateSIS *generateSISObj = [[GenerateSIS alloc] initWithMaster:masterObj];
//    
//    PNLPlan *plan = [DataAccessClass getPnlPlanObj:masterObj.sisRequest.BasePlan];
//    
//    ValidatePrem *premVal = [[DBManager getSharedInstance] getValuesForPremium:plan.PNL_PGL_ID andWithMode:sisRequestObj.Frequency];
//    
//    if([masterObj.sisRequest.BasePlan isEqualToString:@"SEC7V1N1"]){
//        long premium = [generateSISObj getBasePremium:@"SEC7V1N1"];
//        if(premium >= MAX_ANNUALIZED_PREMIUM_SEC7){
//            NSLog(@"the premium %ld greater than 40000",premium);
//            [error setObject:[NSNumber numberWithInt:1] forKey:@"code"];
//            [error setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//        }else{
//            NSLog(@"the premium %ld lesser than 40000",premium);
//            [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
//            [error setObject:[NSString stringWithUTF8String:"Premium is less than 40000.\nPlease increase your Sum Assured."] forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:error];
//        }
//    }
//    else if([masterObj.sisRequest.BasePlan isEqualToString:@"MMXV1N1"]){
//        long premium = [generateSISObj getBasePremium:@"MMXV1N1"];
//        if(premium >= MAX_ANNUALIZED_PREMIUM_MM){
//            NSLog(@"the premium %ld greater than 12000",premium);
//            [error setObject:[NSNumber numberWithInt:1] forKey:@"code"];
//            [error setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//        }
//        else{
//            NSLog(@"the premium %ld lesser than 12000",premium);
//            [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
//            [error setObject:[NSString stringWithUTF8String:"Premium is less than 12000.\nPlease increase your Sum Assured."] forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:error];
//        }
//    }else if([masterObj.sisRequest.ProductType isEqualToString:@"U"]){
//        long premium = masterObj.sisRequest.BasePremium;
//        //[generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//        if (premium >= premVal.minPrem) {
//            if (premVal.maxPrem != 0) {
//                if (premium > premVal.maxPrem) {
//                    NSLog(@"the premium %ld greater than %ld",premium,premVal.maxPrem);
//                    [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
//                    
//                    [error setObject:[NSString stringWithUTF8String:[[NSString stringWithFormat:@"Premium is greater than maximum annualized premium"] UTF8String]] forKey:@"message"];
//                    return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:error];
//                }else{
//                    NSLog(@"the premium is correct");
//                    [error setObject:[NSNumber numberWithInt:1] forKey:@"code"];
//                    [error setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
//                    return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//                }
//            }else{
//                NSLog(@"the premium is correct");
//                [error setObject:[NSNumber numberWithInt:1] forKey:@"code"];
//                [error setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
//                return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//            }
//            
//        }else{
//            NSLog(@"the premium %ld lesser than %ld",premium,premVal.minPrem);
//            [error setObject:[NSNumber numberWithInt:0] forKey:@"code"];
//            [error setObject:[NSString stringWithUTF8String:[[NSString stringWithFormat:@"Premium is less than minimum annualized premium"] UTF8String]] forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:error];
//        }
//    }
//    else{
//        [error setObject:[NSNumber numberWithInt:1] forKey:@"code"];
//        [error setObject:[NSString stringWithUTF8String:""] forKey:@"message"];
//        return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//    }
//    
//    return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:error];
//}
//
//-(SISRequest *) getRequest:(NSDictionary *)_reqDict{
//    SISRequest *request = [[SISRequest alloc] init];
//    request.InsuredName = [_reqDict valueForKey:@"InsName"];
//    request.InsuredAge = [[_reqDict valueForKey:@"InsAge"] intValue];
//    request.InsuredSex = [_reqDict valueForKey:@"InsSex"];
//    request.InsuredOccupation_Desc = [_reqDict valueForKey:@"InsOccDesc"];
//    request.InsuredOccupation = [_reqDict valueForKey:@"InsOcc"];
//    request.OwnerName = [_reqDict valueForKey:@"OwnName"];
//    request.OwnerAge = [[_reqDict valueForKey:@"OwnAge"] intValue];
//    request.OwnerOccupation = [_reqDict valueForKey:@"OwnOcc"];
//    request.OwnerSex = [_reqDict valueForKey:@"OwnSex"];
//    request.OwnerOccupationDesc = [_reqDict valueForKey:@"OwnOccDesc"];
//    request.InsuredSmokerStatus = [_reqDict valueForKey: @"InsSmoker"];
//    request.PaymentMode = [_reqDict valueForKey:@"PaymentMode"];
//    request.BasePlan = [_reqDict valueForKey:@"BasePlan"];
//    request.Sumassured = [[_reqDict valueForKey:@"Sumassured"] intValue];
//    request.PolicyTerm = [[_reqDict valueForKey:@"PolicyTerm"] integerValue];
//    request.PremiumPayingTerm = [[_reqDict valueForKey:@"PPTerm"] integerValue];
//    request.BasePremium = [[_reqDict valueForKey:@"BasePremium"] intValue];
//    request.BasePremiumAnnual = [[_reqDict valueForKey:@"BasePremiumAnnual"] intValue];
//    request.ProposalNo = [_reqDict valueForKey:@"PropNo"];
//    request.AgeProofFlag = [_reqDict valueForKey:@"SAgeProofFlg"];
//    request.ProposerDOB = [_reqDict valueForKey:@"PropDOB"];
//    request.ProductType = [_reqDict valueForKey:@"ProdTye"];
//    request.RPU_Year = [_reqDict valueForKey:@"RpuYear"];
//    request.TaxSlab = [_reqDict valueForKey:@"TaxSlab"];
//    request.Exp_Bonus_Rate = [_reqDict valueForKey:@"ExpBonusRate"];
//    request.Premium_Mul = [_reqDict valueForKey:@"PremiumMul"];
//    request.Commission = [_reqDict valueForKey:@"Commision"];
//    request.Frequency = [_reqDict valueForKey:@"PaymentMode"];
//    request.TataEmployee = [_reqDict valueForKey:@"TataEmployee"];
//    request.ProposerName = [_reqDict valueForKey:@"ProposerName"];
//    request.ProposerDOB = [_reqDict valueForKey:@"ProposerDOB"];
//    request.ProposerOccupation = [_reqDict valueForKey:@"ProposerOccupation"];
//    request.ProposerOccupation_Desc = [_reqDict valueForKey:@"ProposerDescOcc"];
//    request.ProposerSex = [_reqDict valueForKey:@"ProposerGender"];
//    request.ProposerAge = [[_reqDict valueForKey:@"ProposerAge"] intValue];
//    
//    //*********************************ULIP*********************************
//    request.BasePremiumAnnual = [[_reqDict valueForKey:@"BasePremiumAnnual"] intValue];
//    request.SmartDebtFund = [_reqDict valueForKey:@"SmartDebtFund"];
//    request.SmartEquityFund = [_reqDict valueForKey:@"SmartEquFund"];
//    request.Premium_Mul = [_reqDict valueForKey:@"PremiumMult"];
//    //request.SmartDebtFundFMC = [self getFMCForFund:[_reqDict valueForKey:@"SmartDebtFund"] andWithMapper:_mapper];
//    //request.SmartEquityFundFMC = [self getFMCForFund:[_reqDict valueForKey:@"SmartEquFund"] andWithMapper:_mapper];
//    request.Commission = [_reqDict valueForKey:@"Commission"];
//    
//    //*********************************ULIP********************************
//    
//    return  request;
//}
//
//#pragma mark - VERSION CHECK
////check for current version..
//-(void)checkVersionApp{
//    
//    UIAlertView *alert= nil;
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    
//    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:PLISTURL]];
//    NSArray *itemArr = [dict objectForKey:KEY_ITEMS];
//    NSString *serverVersion = [[[itemArr objectAtIndex:0] objectForKey:KEY_METADATA] objectForKey:KEY_VERSION];
//    serverVersion = [serverVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
//    
//    versionNo = [serverVersion integerValue];
//    /*[userDefaults setObject:[NSNumber numberWithInt:100] forKey:USERDEFAULT_KEY_VERSION];
//    [userDefaults synchronize];
//     NSLog(@"version %@",versionNo);*/
//    
//    if ([userDefaults integerForKey:USERDEFAULT_KEY_VERSION]){
//        
//        NSInteger currentVersion = [userDefaults integerForKey:USERDEFAULT_KEY_VERSION];
//        if(currentVersion < versionNo){
//            alert = [[UIAlertView alloc] initWithTitle:@"Update" message:@"Update Available" delegate:self cancelButtonTitle:@"Update" otherButtonTitles:@"Cancel", nil];
//            alert.tag = 1;
//            NSLog(@"App require to update :: %@",[userDefaults objectForKey:USERDEFAULT_KEY_VERSION]);
//        }else{
//            NSLog(@"No Updates Required :: %@",[userDefaults objectForKey:USERDEFAULT_KEY_VERSION]);
//        }
//    }else{
//        [userDefaults setInteger:versionNo forKey:USERDEFAULT_KEY_VERSION];
//        NSLog(@"App is updated");
//    }
//    [alert show];
//    [userDefaults synchronize];
//}
//
//-(void)updateLifePlanner{
//    NSURL *url = [NSURL URLWithString:UPDATEURL];
//    UIApplication *app = [UIApplication sharedApplication];
//    if ([app canOpenURL:url]) [app openURL:url];
//    else NSLog(@"cant open");
//}
//
//-(void)ForceUpdateIPA:(CDVInvokedUrlCommand *)command{
//    NSURL *url = [NSURL URLWithString:UPDATEURL];
//    UIApplication *app = [UIApplication sharedApplication];
//    CDVPluginResult *pluginResult = NULL;
//    //changes 27th oct
//    [self alterDatabase];
//    if ([app canOpenURL:url]){
//        //************************************************************************************************************
//        //get the version from the server add to userdefaults...
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:PLISTURL]];
//        NSArray *itemArr = [dict objectForKey:KEY_ITEMS];
//        NSString *serverVersion = [[[itemArr objectAtIndex:0] objectForKey:KEY_METADATA] objectForKey:KEY_VERSION];
//        serverVersion = [serverVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
//        NSInteger versionNo1 = [serverVersion integerValue];
//        [userDefaults setInteger:versionNo1 forKey:USERDEFAULT_KEY_VERSION];
//        //changes 27th oct
//        [userDefaults setValue:@"" forKey:@"Launch"];
//        NSLog(@"first time value changes to blank");
//        [userDefaults synchronize];
//        //************************************************************************************************************
//        
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
//        [app openURL:url];
//    }
//    else {
//        NSLog(@"cant open");
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO];
//    }
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    exit(0);
//}
//
//-(void)checkVersion:(CDVInvokedUrlCommand *)command{
//    
//    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//    [self checkVersionApp];
//    
//}
//
//#pragma mark - UIALERTVIEW DELEGATE
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    if (alertView.tag == 1) {
//        if(buttonIndex == 0){
//            [userDefaults setInteger:versionNo forKey:USERDEFAULT_KEY_VERSION];
//            [userDefaults synchronize];
//            //changes 27th oct
//            [userDefaults setValue:@"" forKey:@"Launch"];
//            NSLog(@"first time value changes to blank");
//            //uncomment when production
//            [self alterDatabase];
//            [self updateLifePlanner];
//            exit(0);
//            
//        }
//    }
//    
//}
//
//#pragma mark -Sqlite DB
//-(void)alterDatabase{
//    //database updates
//    sqlite3 *database;
//    sqlite3_stmt *statement;
//    NSString *docFile = [self GetDataBasePath:@"Life_Planner.db"];
//    const char *dbpath = [docFile UTF8String];
//    
//    NSArray *tableArr = [[NSArray alloc] initWithObjects:@"LP_ACM_ADMIN_CHARGES_MASTER",@"LP_CCM_COMMISSION_CHARGE_MASTER",@"LP_COMPANY_MASTER",@"LP_COMPANY_MASTER"@"LP_FCM_FIB_CHARGES_MASTER", @"LP_FDL_FUND_LK", @"LP_FED_FORMULA_EXT_DESC", @"LP_GAI_GUAR_ANN_INC_CH_MASTER", @"LP_GMM_GUARANTEED_MATURITY_MASTER", @"LP_IPC_INF_PROT_COV_MASTER", @"LP_IPL_ILLUSS_PAGELIST",@"LP_MCM_MORTILITY_CHARGES_MASTER",@"LP_MDL_MODAL_FACTOR_LK",@"LP_MFM_MATURITY_FACTOR_MASTER",@"LP_OCL_OCCUPATION_LK",@"LP_PAC_PREMIUM_ALLOCATION_CHARGES",@"LP_PBA_PRODUCT_BAND",@"LP_PCR_PLAN_CHARGES_REF",@"LP_PER_PLAN_FORMULA_REF",@"LP_PFX_PLAN_FUND_XREF",@"LP_PGF_PLAN_GROUP_FEATURE",@"LP_PGL_PLAN_GROUP_LK",@"LP_PMD_PLAN_PREMIUM_MULT_DISC",@"LP_PML_PREMIUM_MULTIPLE_LK",@"LP_PMV_PLAN_MATURITY_VALUE",@"LP_PNL_MIN_PREM_LK",@"LP_PNL_PLAN_LK",@"LP_PNL_TERM_PPT_LK",@"LP_RDL_CHARGES",@"LP_RDL_RIDER_LK",@"LP_RML_RIDER_PREM_MULTIPLE_LK",@"LP_RPX_RIDER_PLAN_XREF",@"LP_RTL_RIDER_TYPE_LK",@"LP_SPV_SYS_PARAM_VALUES",@"LP_STD_SIS_TEMPLATE_DATA",@"LP_SUR_SURRENDER_CHARGE_MASTER",@"LP_WCM_WOP_CHARGES_MASTER",@"LP_XMM_XML_METADATA_MASTER",nil];
//    
//    NSArray *alterTable = [[NSArray alloc]
//                           initWithObjects:
//                          /* @"CREATE TABLE IF NOT EXISTS 'LP_FUND_MASTER' ('FUND_CODE' TEXT(5) PRIMARY KEY  NOT NULL , 'FUND_DESCRIPTION' TEXT(100), 'FUND_SFIN_NUMBER' TEXT(35), 'FUND_FDL_ID' TEXT(15), 'FUND_ORDER' TEXT(3), 'FUND_TYPE' TEXT(25), 'FUND_FROM' TEXT(1), 'FUND_TO' TEXT(1), 'ISACTIVE' TEXT(1), 'VERSION_NO' TEXT(15))",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('WLI','Whole Life Income Fund','(ULIF 012 04/01/07 WLI 110)','17','2','Dept','Y','N','Y','0')",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('WLF','Whole Life Short Term Fixed Income Fund','(ULIF 013 04/01/07 WLF 110)','2','3','Dept','Y','N','Y','0')",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('WLE','Whole Life Mid Cap Equity Fund','(ULIF 009 04/01/07 WLE 110)','5','1','Equity','N','Y','Y','0')",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('WLA','Whole Life Aggressive Growth Fund','(ULIF 010 04/01/07 WLA 110)','10','4',null,'N','N','Y','0')",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('WLS','Whole Life Stable Growth Fund','(ULIF 011 04/01/07 WLS 110)','6','5',null,'N','N','Y','0')",
//                           @"INSERT INTO LP_FUND_MASTER VALUES ('TLC','Large Cap Equity Fund','(ULIF 017 07/01/08 TLC 110)','8','6','Equity','N','Y','Y','0')",
//                           @"CREATE TABLE IF NOT EXISTS 'LP_APP_FUND_SCRN_I' ('APPLICATION_ID' TEXT(15) NOT NULL , 'AGENT_CD' TEXT(10) NOT NULL , 'INVEST_OPT' TEXT(25), 'FUND_CODE_FROM' TEXT(5), 'FUND_CODE_TO' TEXT(5), 'FUND1_CODE' TEXT(5), 'FUND1_PERCENT' TEXT(3), 'FUND2_CODE' TEXT(5), 'FUND2_PERCENT' TEXT(3), 'FUND3_CODE' TEXT(5), 'FUND3_PERCENT' TEXT(3), 'FUND4_CODE' TEXT(5), 'FUND4_PERCENT' TEXT(3), 'FUND5_CODE' TEXT(5), 'FUND5_PERCENT' TEXT(3), 'FUND6_CODE' TEXT(5), 'FUND6_PERCENT' TEXT(3), 'FUND7_CODE' TEXT(5), 'FUND7_PERCENT' TEXT(3), 'FUND8_CODE' TEXT(5), 'FUND8_PERCENT' TEXT(3), 'FUND9_CODE' TEXT(5), 'FUND9_PERCENT' TEXT(3), 'FUND10_CODE' TEXT(5), 'FUND10_PERCENT' TEXT(3),'OTHER_DETAILS' TEXT(100), 'MODIFIED_DATE' TEXT(20), PRIMARY KEY ('APPLICATION_ID', 'AGENT_CD'))",
//                           @"CREATE TABLE IF NOT EXISTS LP_SIS_SCREEN_C ('SIS_ID' TEXT(15) NOT NULL , 'AGENT_CD' TEXT(10) NOT NULL , 'INVEST_OPT' TEXT(25), 'FUND_CODE_FROM' TEXT(5), 'FUND_CODE_TO' TEXT(5), 'FUND1_CODE' TEXT(5), 'FUND1_PERCENT' TEXT(3), 'FUND2_CODE' TEXT(5), 'FUND2_PERCENT' TEXT(3), 'FUND3_CODE' TEXT(5), 'FUND3_PERCENT' TEXT(3), 'FUND4_CODE' TEXT(5), 'FUND4_PERCENT' TEXT(3), 'FUND5_CODE' TEXT(5), 'FUND5_PERCENT' TEXT(3), 'FUND6_CODE' TEXT(5), 'FUND6_PERCENT' TEXT(3), 'FUND7_CODE' TEXT(5), 'FUND7_PERCENT' TEXT(3), 'FUND8_CODE' TEXT(5), 'FUND8_PERCENT' TEXT(3), 'FUND9_CODE' TEXT(5), 'FUND9_PERCENT' TEXT(3), 'FUND10_CODE' TEXT(5), 'FUND10_PERCENT' TEXT(3), 'MODIFIED_DATE' TEXT(20), PRIMARY KEY ('SIS_ID', 'AGENT_CD'))",
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'IS_SCREEN12_COMPLETED' TEXT(1)",
//                           @"ALTER TABLE LP_SIS_MAIN ADD COLUMN 'IS_SCREEN3_COMPLETED' TEXT(1)",
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'EDIT_APP' TEXT(1)",
//                           @"CREATE TABLE IF NOT EXISTS LP_APP_EDIT_DTLS ('AGENT_CD' TEXT(10) NOT NULL, 'POLICY_NO' TEXT(20) NOT NULL, 'EDIT_FLAG' TEXT(1))",
//                           @"ALTER TABLE LP_NOMINEE_RELATION ADD COLUMN 'SEQ_NO' TEXT(2)",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='01' where RELATIONSHIP_DESC='Spouse'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='02' where RELATIONSHIP_DESC='Father'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='03' where RELATIONSHIP_DESC='Mother'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='04' where RELATIONSHIP_DESC='Son'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='05' where RELATIONSHIP_DESC='Daughter'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='06' where RELATIONSHIP_DESC='Brother'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='07' where RELATIONSHIP_DESC='Sister'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='08' where RELATIONSHIP_DESC='Grand son'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='09' where RELATIONSHIP_DESC='Grand Daughter'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='10' where RELATIONSHIP_DESC='Grand Father'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='11' where RELATIONSHIP_DESC='Grand Mother'",
//                           @"UPDATE LP_NOMINEE_RELATION SET SEQ_NO='12' where RELATIONSHIP_DESC='MWP'",
//                           @"CREATE TABLE IF NOT EXISTS LP_SIS_SCREEN_D (SIS_ID TEXT(15) NOT NULL,AGENT_CD TEXT(10) NOT NULL,RIDER_SEQ_NO TEXT(15) NOT NULL,RIDER_CODE TEXT(10),RIDER_NAME TEXT(60),RIDER_TERM TEXT(3),RIDER_PAY_TERM TEXT(3),RIDER_PAY_MODE TEXT(2) NOT NULL,RIDER_PAY_MODE_DESC TEXT(2) NOT NULL,RIDER_ANNUAL_PREMIUM TEXT(11),RIDER_MODAL_PREMIUM TEXT(11),RIDER_BASE_PREMIUM TEXT(11),RIDER_SERVICE_TAX TEXT(11),RIDER_PREM_MULTIPLIER TEXT(11), RIDER_COVERAGE TEXT(15),RIDER_UNIT TEXT(15), RIDER_INVT_STRAT_FLAG TEXT(1), RIDER_EFFECTIVE_DATE TEXT(20),MODIFIED_DATE TEXT(20),PRIMARY KEY(SIS_ID,AGENT_CD,RIDER_SEQ_NO))",
//                            @"CREATE TABLE IF NOT EXISTS LP_COMBO_DTLS (COMBO_ID TEXT (10),AGENT_CD TEXT (15), INSURED_SEQ_NO TEXT (15),INSURED_UNIQUE_NO TEXT (15),ANNUAL_PREMIUM_AMOUNT TEXT (11),PURCHASING_INSURANCE_FOR_CODE TEXT (2),INSURANCE_BUYING_FOR_CODE TEXT (2),INSURED_TITLE TEXT (5),INSURED_FIRST_NAME TEXT (20),INSURED_MIDDLE_NAME TEXT (20), INSURED_LAST_NAME TEXT (20),INSURED_OCCU_CODE TEXT (10), INSURED_DOB                   TEXT,INSURED_GENDER TEXT (2),INSURED_MOBILE TEXT (12),INSURED_EMAIL TEXT (60),IS_SMOKER TEXT (1),AGE_PROOF_DOC_ID TEXT (15),PROPOSER_TITLE TEXT (5),PROPOSER_FIRST_NAME TEXT (20),PROPOSER_MIDDLE_NAME TEXT (20),PROPOSER_LAST_NAME TEXT (20),PROPOSER_OCCU_CODE TEXT (10),PROPOSER_DOB TEXT,PROPOSER_GENDER TEXT (2),PROPOSER_MOBILE TEXT (12), PROPOSER_EMAIL                TEXT (60),PLAN_CODE TEXT (10),PREMIUM_PAY_MODE TEXT (2),POLICY_TERM TEXT (3), PREMIUM_PAY_TERM TEXT (3),SUM_ASSURED TEXT (15), PREM_MULTIPLIER TEXT (11),INVT_STRAT_FLAG               TEXT (1),INVEST_OPT TEXT (25),FUND_CODE_FROM TEXT (5),FUND_CODE_TO TEXT (5),FUND1_CODE TEXT (5),FUND1_PERCENT TEXT (3), FUND2_CODE TEXT (5),FUND2_PERCENT TEXT (3),FUND3_CODE TEXT (5),FUND3_PERCENT TEXT (3),FUND4_CODE TEXT (5),FUND4_PERCENT TEXT (3), FUND5_CODE TEXT (5),FUND5_PERCENT TEXT (3),FUND6_CODE TEXT (5), FUND6_PERCENT TEXT (3), FUND7_CODE TEXT (5),FUND7_PERCENT TEXT (3),FUND8_CODE TEXT (5),FUND8_PERCENT TEXT (3),FUND9_CODE TEXT (5),FUND9_PERCENT TEXT (3), FUND10_CODE TEXT (5),FUND10_PERCENT TEXT (3),RIDER_COUNT TEXT (3),SIS_ID TEXT (15),APPLICATION_ID TEXT (15),PRIMARY KEY ( COMBO_ID, AGENT_CD, INSURED_SEQ_NO))",
//                           @"CREATE TABLE IF NOT EXISTS LP_COMBO_RIDER_DTLS (COMBO_ID TEXT (10), AGENT_CD TEXT (15),INSURED_SEQ_NO        TEXT (15), RIDER_SEQ_NO TEXT (15), RIDER_CODE TEXT (10), RIDER_TERM TEXT (3), RIDER_PAY_TERM TEXT (3), RIDER_COVERAGE TEXT (15),RIDER_ANNUAL_PREMIUM  TEXT (11),RIDER_UNIT TEXT (15), RIDER_PREM_MULTIPLIER TEXT (11),RIDER_INVT_STRAT_FLAG TEXT (1), RIDER_EFFECTIVE_DATE TEXT (20),PRIMARY KEY (COMBO_ID,AGENT_CD,INSURED_SEQ_NO,RIDER_SEQ_NO))",
//                           @"CREATE TABLE IF NOT EXISTS LP_DOCUMENT_CAPTURE (DOC_CAP_ID TEXT(15), AGENT_CD TEXT(10), DOC_ID TEXT(15), DOC_CAT TEXT(10), DOC_CAT_ID TEXT(15), DOC_PAGE_NO TEXT(2), POLICY_NO TEXT(10), DOC_NAME TEXT(250), DOC_TIMESTAMP TEXT(6), ISPENDING TEXT(1), IS_FILE_SYNCED TEXT(1), IS_DATA_SYNCED TEXT(1), COMBO_ID TEXT(15), INSURED_SEQ_NO TEXT(15), INSURED_UNIQUE_NO TEXT(15), DOC_CAP_REF_ID TEXT(15), MODIFIED_DATE TEXT(6),PRIMARY KEY([DOC_ID], [AGENT_CD], [DOC_CAT_ID], [DOC_PAGE_NO]))",
//                           @"CREATE TABLE IF NOT EXISTS LP_MYCOMBO (COMBO_ID TEXT (10),AGENT_CD TEXT (15), LEAD_ID TEXT (15),FHR_ID TEXT (15),REF_FHR_ID TEXT (15), REF_FHR_GOAL_DESC TEXT (50), REF_FHR_REL_CODE TEXT (5),REF_FHR_REL_SEQ_NO  TEXT (5),RECO_COMBO_SOLUTION TEXT (50), COMBO_SOLUTION      TEXT (50),COMBO_GOAL TEXT (50), COMBO_STATUS TEXT (1),INSURED_COUNT TEXT (3),IS_SYNCED TEXT (1),MODIFIED_DATE TEXT (20),PRIMARY KEY (COMBO_ID,AGENT_CD))",
//                           @"ALTER TABLE LP_USERINFO ADD COLUMN 'CSA_PASSPORT_ID' TEXT(1)",
//                           @"ALTER TABLE LP_USERINFO ADD COLUMN 'TRAINING_EXPIRY_DATE' TEXT(1)",*/
//                           /* 20April2016 */
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'PROMO_OFFER' TEXT(1)",
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'APP_TNC_FLAG' TEXT(1)",
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'IS_PREVIEW_SAVED' TEXT(1)",
//                           @"ALTER TABLE LP_APPLICATION_MAIN ADD COLUMN 'IS_PDF_SAVED' TEXT(1)",
//  /* Comment for next build */              // @"DELETE from LP_PRODUCT_MASTER",
//   /* Comment for next build */             //  @"DELETE from LP_FHR_PRODUCT_RECOMND_GRID",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'RIDERIND' TEXT(1)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_NEED' TEXT(50)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_NEED_CD' TEXT(2)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_SOLUTION' TEXT(50)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_SOLUTION_CD' TEXT(2)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_TYPE' TEXT(50)",
//                           @"ALTER TABLE LP_PRODUCT_MASTER ADD COLUMN 'PRODUCT_TYPE_CD' TEXT(2)",
//                           @"ALTER TABLE LP_FHR_PRODUCT_RECOMND_GRID ADD COLUMN 'MIN_SAVING_COMITMENT' TEXT(11)",
//                           @"ALTER TABLE LP_FHR_PRODUCT_RECOMND_GRID ADD COLUMN 'MAX_SAVING_COMITMENT' TEXT(11)",
//                           @"ALTER TABLE LP_FHR_PRODUCT_RECOMND_GRID ADD COLUMN 'PREFERRED_SOLUTION' TEXT(30)",
//                           @"ALTER TABLE LP_FHR_PRODUCT_RECOMND_GRID ADD COLUMN 'MODIFIED_DATE' TEXT(10)",
//                           @"ALTER TABLE LP_DOC_FETCH_USER_MASTER ADD COLUMN 'PGL_ID' TEXT(5)",
///* CHANGES 16Feb2016*/     @"CREATE TABLE IF NOT EXISTS LP_ONLINE_PAYMENT_DTLS (CREATED_DATE TEXT(6),TALIC_MERCHANT_ID TEXT(10) NOT NULL ,POLICY_NO TEXT(10) NOT NULL ,TRANS_REF_ID TEXT(40) PRIMARY KEY  NOT NULL ,BANK_TRANS_REF_ID TEXT(40),TRANSACTION_AMT TEXT(15),BANK_ID TEXT(20),BANK_MERCHANT_ID TEXT(20),TRANS_TYPE TEXT(20),TRANS_ITEM_CODE TEXT(20),TRANSACTION_DATE TEXT(6) NOT NULL ,AUTHORISATION_STATUS TEXT(4) NOT NULL ,PREMIUM_TYPE TEXT(60),PAYEE_OWNER_NAME TEXT(60),AGENT_CD TEXT(10),PREMIUM_DUE_DATE TEXT(10),MOBILE TEXT(12),EMAIL_ID TEXT(60),TR_SERIES TEXT(100) ,ERROR_STAT TEXT(20),ERROR_DESC TEXT(100),PG_MERCHANT_ID TEXT(20),IS_TAB_SYNC TEXT(1))",nil];
//    
//    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
//        
//        //***DROP TABLE QUERY ****///
//        
//        for (int i=0; i<tableArr.count; i++) {
//            const char *query_stmt =  [[NSString stringWithFormat:@"DROP TABLE IF EXISTS %@",[tableArr objectAtIndex:i]] UTF8String];
//            int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
//            if(result == SQLITE_OK){
//                int altered = sqlite3_step(statement);
//                if(altered == SQLITE_DONE){
//                    NSLog(@"alter success");
//                }else{
//                    NSLog(@"alter failed");
//                }
//                sqlite3_finalize(statement);
//            }
//        }
//        
//        //**** ALTER SIS TABLE ******Added by Drastty///
//        
//        for (int i=0; i<alterTable.count; i++) {
//            const char *query_stmt =  [[alterTable objectAtIndex:i] UTF8String];
//            int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
//            if(result == SQLITE_OK){
//                int altered = sqlite3_step(statement);
//                if(altered == SQLITE_DONE){
//                    NSLog(@"alter table success");
//                }else{
//                    NSLog(@"alter failed");
//                }
//                sqlite3_finalize(statement);
//            }
//            else{
//                NSLog(@"already altered %s",query_stmt);
//            }
//        }
//        sqlite3_close(database);
//    }
//}
//
//-(NSString *)GetDataBasePath:(NSString *)_dbName{
//    NSString *docsDir;
//    NSArray *dirPaths;
//    // Get the documents directory
//    dirPaths = NSSearchPathForDirectoriesInDomains
//    (NSDocumentDirectory, NSUserDomainMask, YES);
//    docsDir = dirPaths[0];
//    
//    //create the path to database.
//    NSString *dbPath = [[NSString alloc] initWithString:
//                        [docsDir stringByAppendingPathComponent: _dbName]];
//    
//    return dbPath;
//}
//
//-(void)getSqliteJoinData:(CDVInvokedUrlCommand *)command{
//    NSArray *arr = command.arguments;
//    NSString *lifeplannerDB = [self GetDataBasePath:@"Life_Planner.db"];
//    NSString *SISDB = [self GetDataBasePath:@"SIS.db"];
//    NSString *query = [arr objectAtIndex:0];
//    NSLog(@"Inside getSqliteJoinData");
//    CDVPluginResult *pluginResult = [self getData:lifeplannerDB andWithAnotherDB:SISDB andWithQuery:query];
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//    
//}
//
//-(CDVPluginResult *)getData:(NSString *)_db andWithAnotherDB:(NSString *)_otherdb andWithQuery:(NSString *)_query{
//    sqlite3 *database = nil;
//    sqlite3_stmt *statement = nil;
//    int rc;
//    NSMutableDictionary *dictionary = NULL;
//    NSMutableDictionary *pluginDict = [[NSMutableDictionary alloc] init];
//    
//    NSMutableArray *arr = [[NSMutableArray alloc] init];
//    if (sqlite3_open([_db UTF8String], &database) == SQLITE_OK)
//    {
//        
//        NSString *strSQLAttach = [NSString stringWithFormat:@"ATTACH DATABASE \'%s\' AS SIS", [_otherdb UTF8String] ];
//        char *errorMessage;
//        
//        if (sqlite3_exec(database, [strSQLAttach UTF8String], NULL, NULL, &errorMessage) == SQLITE_OK)
//        {
//            NSString *strSQL = _query;
//            NSLog(@"getSqliteJoinData :query is %@",_query);
//            int val = sqlite3_prepare_v2(database, [strSQL UTF8String], -1, &statement, nil);
//            if (val == SQLITE_OK){
//                //get the column count from the query..
//                NSInteger columnCount = sqlite3_column_count(statement);
//                
//                id value;
//                //loop through the row.
//                while ((rc = sqlite3_step(statement)) == SQLITE_ROW) {
//                    dictionary = [[NSMutableDictionary alloc] init];
//                    for (NSInteger j = 0; j < columnCount; j++) {
//                        
//                        int i = (int)j;
//                        //get the column name..
//                        NSString *columnName   = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
//                        switch (sqlite3_column_type(statement, i)) {
//                            case SQLITE_NULL:
//                            value = [NSNull null];
//                            break;
//                            case SQLITE_TEXT:
//                            value = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, i)];
//                            break;
//                            case SQLITE_INTEGER:
//                            value = @(sqlite3_column_int64(statement, i));
//                            break;
//                            case SQLITE_FLOAT:
//                            value = @(sqlite3_column_double(statement, i));
//                            break;
//                            case SQLITE_BLOB:
//                            {
//                                NSInteger length  = sqlite3_column_bytes(statement, i);
//                                const void *bytes = sqlite3_column_blob(statement, i);
//                                value = [NSData dataWithBytes:bytes length:length];
//                                break;
//                            }
//                            default:
//                            NSLog(@"unknown column type");
//                            value = [NSNull null];
//                            break;
//                        }
//                        dictionary[columnName] = value;
//                    }
//                    [arr addObject:dictionary];
//                    
//                    //return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:pluginDict];
//                    
//                }
//                
//                if (rc != SQLITE_DONE) {
//                    NSLog(@"error returning results %d %s", rc, sqlite3_errmsg(database));
//                    [pluginDict setObject:[NSNumber numberWithInt:0] forKey:@"error"];
//                    [pluginDict setObject:[NSString stringWithFormat:@"error returning results %d %s", rc, sqlite3_errmsg(database)] forKey:@"message"];
//                    return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:pluginDict];
//                }
//                sqlite3_finalize(statement);
//            }
//            else{
//                NSLog(@"Error while attaching '%s'", sqlite3_errmsg(database));
//                //[pluginDict setObject:arr forKey:@"Data"];
//                [pluginDict setObject:[NSNumber numberWithInt:0] forKey:@"error"];
//                [pluginDict setObject:[NSString stringWithFormat:@"Error while attaching '%s'",sqlite3_errmsg(database)] forKey:@"message"];
//                return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:pluginDict];
//            }
//        }else{
//            NSLog(@"Error occured while executing the second database");
//            [pluginDict setObject:[NSNumber numberWithInt:0] forKey:@"error"];
//            [pluginDict setObject:@"Error occured while executing the second database" forKey:@"message"];
//            return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:pluginDict];
//        }
//    }else{
//        NSLog(@"Error occured while opening the database");
//        [pluginDict setObject:[NSNumber numberWithInt:0] forKey:@"error"];
//        [pluginDict setObject:@"Error occured while opening the database" forKey:@"message"];
//        return [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:pluginDict];
//    }
//    
//    [pluginDict setObject:arr forKey:@"Data"];
//    [pluginDict setObject:[NSNumber numberWithInt:1] forKey:@"error"];
//    [pluginDict setObject:@"" forKey:@"message"];
//    
//    return [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:pluginDict];;
//}
//
//
//@end
