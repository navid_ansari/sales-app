




//
//  GenerateSIS.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "GenerateSIS.h"
#import "FormulaHandler.h"
#import "Constant.h"
#import "NSString+Contains.h"

@implementation GenerateSIS
@synthesize MapperObj;
@synthesize master;
@synthesize bean;

#pragma mark-- INIT METHOD
-(id)initWithMaster:(ModelMaster *)_master{
    self = [super init];
    if(self){
        //NSLog(@"INIT method in generate sis");
        MapperObj = [DataMapper getInstance];
        bean = [[FormulaBean alloc] init];
        master = _master;
    }
    return self;
}

#pragma mark-- CALCULATION METHODS
-(ModelMaster *) getSISData{
    //calculation parameter for plan code for product...
    //call the method to get the sys_param values for base plan
    //sys_param = CAL_BAP,SB,etc..
    NSString *CalCulatorValue =
    [DataAccessClass getSPVSysParamValues:master.sisRequest.BasePlan];
    return [self CalculateResultCorrespondToValue:CalCulatorValue andWithMaster:master];
}

-(ModelMaster *)CalculateResultCorrespondToValue:(NSString *)_calValue andWithMaster:(ModelMaster *)_master{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if([_calValue containsString:@"CAL_BAP"]){
        //calculation for premium annually
        /*if ([_master.sisRequest.ProductType isEqualToString:@"U"]) {
            master.BaseAnnualizedPremium = [self getBasePremium:_master.sisRequest.BasePlan];
        }else {
            if(master.sisRequest.BasePremium == 0){
                master.BaseAnnualizedPremium = [self getBasePremium:_master.sisRequest.BasePlan];
            }else{
                master.BaseAnnualizedPremium = master.sisRequest.BasePremium;
            }
        }*/
        
//        if([master.sisRequest.BasePlan containsString:@"MI7"] || [master.sisRequest.BasePlan containsString:@"MI10"]){
//            master.BaseAnnualizedPremium = master.sisRequest.BasePremium * 12;
//        }else
        master.BaseAnnualizedPremium = [self getBasePremium:_master.sisRequest.BasePlan];
        
        //calculate annual premium mode wise..
        master.AnnualPremium = [self getAnnualPremium];
    }
    
    if([_calValue containsString:@"CAL_APWOL"]){
        //calculate annualized premium with Occupational loading..
        master.AnnualizedPremiumWithOccLoading = [self getAnnualizedPremiumWithOccLoading];
    }
    
    if([_calValue containsString:@"CALC_MP"]){
        //calculate Modal premium for base plan
        master.ModalPremium = [self getModalPremium];
    }
    
    if([_calValue containsString:@"CALC_NSAP"]){
        //calculate Modal premium for NSAP non standard age proof..
        master.ModalPremiumForNSAP = [self getNSAPLoadPremium];
        master.ModalPremiumForAnnNSAP = [self getNSAPLoadPremiumAnnual];
        //NSLog(@"annual modal prem %ld",master.ModalPremiumForAnnNSAP);
    
    }
    
    //calculate quoted annualized premium
    master.QuotedAnnualizedPremium = [self getQuotedAnnualizedPremium];
    
    if([_calValue containsString:@"CALDP"]){
        //calculate Discounted Base premium FY discount in case of Staff discount..
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame){
            master.DiscountedBasePremium = [self getDiscountedPremium];
        }
    }
    
    if([_calValue containsString:@"RP"]){
        //calculate Rider with premium
        master.riderData = [self getRiderWithPremium];
    }else{
        //so adding blank array to rider data..
        master.riderData = array;
    }
    
    //Good kid
    if ([_calValue containsString:@"CAL_TOT_MILE_ADD8"]) {
        master.MilestoneAdd8Dict = [self getMilestoneAdditions:@"CAL_TOT_MILE_ADD8"];
    }
    
    if ([_calValue containsString:@"CAL_TOT_MILE_ADD4"]) {
        master.MilestoneAdd4Dict = [self getMilestoneAdditions:@"CAL_TOT_MILE_ADD4"];
    }
    
    if ([_calValue containsString:@"CAL_MBB"]) {
        master.MoneyBackBenefitDict = [self getMoneyBackBenfit];
    }
    //Good kid

    
    //total Rider Modal Premium
    //Commented byb Amruta
    //master.TotalRiderModalPremium = [self getTotalRiderModePremium];
    //total modal premium
    master.TotalModalPremium = [self getTotalModalPremium];
    
    //Quoted Modal Premium
    master.QuotedModalPremium = master.TotalModalPremium + master.ModalPremiumForNSAP;
    //NSLog(@"the Quoted modal Premium %ld",master.QuotedModalPremium);
    
    if([_calValue containsString:@"CALCFMC"]){
        //fund management charge
        master.dfmc = [DataAccessClass getFMC:master.sisRequest];
    }
    
    if([_calValue containsString:@"LIFE_COV"]){
        //life coverage and death benefit are same..
        master.LifeCoverage = [self getLifeCoverage];
    }
    
    if([_calValue containsString:@"CALCMV"]){
        //maturity benefit..
        master.MaturityValue = [self getMaturityValue];
    }
    
    if ([_calValue containsString:@"CALCMV_MIP"]) {
        master.BasicSumAssured = [self getMaturityValue_MIP];
    }
    
    if([_calValue containsString:@"CALGAI"]){
        //guaranteed annual income...
        master.GuaranteedAnnualIncome = [self getGuaranteedAnnualIncome];
        //master.MLGP_TotalGAI = [self getTotalGAI_MLGP];
    }
    
    //**************FREEDOM************************
    if ([_calValue containsString:@"CALACCPTGAI"]) {
        master.AccGAI = [self getAccPTGuaranteedAnnualIncome];
    }
    //**************FREEDOM************************
    
    
    
    if([_calValue containsString:@"SB"]){
        // non guaranteed benefits..
        // special surrender value...
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefit];
    }
    
    if([_calValue containsString:@"CALGSV"]){
        //guaranteed benefits..
        //surrender value..
        master.GuaranteedSurrValue = [self getGuaranteedSurrValue];
    }
    
    if ([_calValue containsString:@"CALGSV_MIP"]) {
        master.GuaranteedSurrValue = [self getGuaranteedSurrValue_MIP];
    }
    
    if([_calValue containsString:@"TOTMATVAL"]){
        //total maturity benefits..
        master.TotalMaturityValue = [self getTotalMaturityValue];
    }
    
    if([_calValue containsString:@"STAXB"]){
        //service tax base plan @3.09 % for first year FY..
        master.serviceTaxBasePaln = [self getServiceTaxForBasePalnPremium];
    }
    
    //SIP
    if ([_calValue containsString:@"LIFE_SIP_COV"]) {
        master.LifeCoverage = [self getLifeCoverage_SIP];
    }
    
    if ([_calValue containsString:@"CALEPGAI"]) {
        master.GuaranteedAnnualIncome = [self getGuaranteeEPAnnualIncome];
    }
    
    if ([_calValue containsString:@"CALETOTGB"]) {
        master.totalGuarBenefit = [self getTotalEGuarBenefit];
    }
    
    if ([_calValue containsString:@"CALTOTGB"]) {
        master.totalGuarBenefit = [self getTotalGuarBenefit];
    }
    
    if ([_calValue containsString:@"SIPEB"]) {
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefit_SIP];
    }
    
    if ([_calValue containsString:@"CALGSV_SIP"]) {
        master.GuaranteedSurrValue = [self getGuaranteedSurrValue_SIP];
    }
    
    if ([_calValue containsString:@"CALGSV_ESIP"]) {
        master.GuaranteedSurrValue = [self getGuaranteedSurrValue_EMIP];
    }
    
    //SIP
    
    //Added by Amruta for GIP
    if([_calValue containsString:@"LIFE_GIP_COV"]){
        master.LifeCoverage = [self getLifeCoverage_GIP];
    }
    
    if([_calValue containsString:@"LTGIPSB"]){
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefit_GIP];
    }
    
    if([_calValue containsString:@"CALGIPTOTGB"]){
        master.totalGuarBenefit = [self getTotalGuarBenefit_GIP];
    }
    
    if([_calValue containsString:@"CALLTGSV"]){
        master.GuaranteedSurrValue = [self getGuaranteedSurrValue_LastYear];
    }
    
    if([_calValue containsString:@"CALGIPGAI"]){
        master.GuaranteedAnnualIncome = [self getGuaranteedAnnualIncome_GIP];
    }
    
    
    //same as service tax on premium..
    master.serviceTax = [self getServiceTaxOnPremium];
    
    //total modal premium payable...
    //changes 19th jan
    master.TotalModalPremiumPayble = master.QuotedModalPremium + master.serviceTax + round([self getServiceTax:master.TotalRiderModalPremium]);
    //NSLog(@"total modal premium payable is %ld",master.TotalModalPremiumPayble);
    
   /* if([_calValue containsString:@"RP"]){
        //calculate Rider with premium
        master.riderData = [self getRiderWithPremium];
    }else{
        //so adding blank array to rider data..
        master.riderData = array;
    }*/
    
    if([_calValue containsString:@"CAL_RP_A"]){
        /*//calculate renewal premium with annual mode...
        if ([master.sisRequest.BasePlan isEqualToString:@"MRSP2N1V2"] || [master.sisRequest.BasePlan isEqualToString:@"MRSP2N2V2"]) {
            master.RenewalPrem_A =0;
        }else{
            master.RenewalPrem_A = [self getRenewalModalPremium_A];
        }*/
        
        master.RenewalPrem_A = [self getRenewalPrem:@"CAL_RP_A"];
    }
    
    if([_calValue containsString:@"CAL_RP_SA"]){
        //calculate renewal premium with semi annual mode...
        if ([master.sisRequest.BasePlan isEqualToString:@"MRSP2N1V2"] || [master.sisRequest.BasePlan isEqualToString:@"MRSP2N2V2"]) {
            master.RenewalPrem_SA =0;
        }else{
            master.RenewalPrem_SA = [self getRenewalPrem:@"CAL_RP_SA"];
        }
    }
    
    if([_calValue containsString:@"CAL_RP_Q"]){
        //calculate renewal premium with Quarterly mode...
        if ([master.sisRequest.BasePlan isEqualToString:@"MRSP2N1V2"] || [master.sisRequest.BasePlan isEqualToString:@"MRSP2N2V2"]) {
            master.RenewalPrem_Q =0;
        }else{
            master.RenewalPrem_Q = [self getRenewalPrem:@"CAL_RP_Q"];
        }
    }
    if([_calValue containsString:@"CAL_RP_M"]){
        //calculate renewal premium with Monthly mode...
        if ([master.sisRequest.BasePlan isEqualToString:@"MRSP2N1V2"] || [master.sisRequest.BasePlan isEqualToString:@"MRSP2N2V2"]) {
            master.RenewalPrem_M =0;
        }else{
            //master.RenewalPrem_M = [self getRenewalPrem:@"CAL_RP_M"];
            //27th may
            if ([master.sisRequest.BasePlan containsString:@"MRS"]) {
                master.RenewalPrem_M = round(master.RenewalPrem_A * MON_MODFACT);
            }else{
                master.RenewalPrem_M = [self getRenewalPrem:@"CAL_RP_M"];
            }
            
        }
    }
    
    //------------------------------------------------------------------------------------
    //FOR MAHALIFE SUPREME
    if([_calValue containsString:@"CAL_RPST_LS_A"]){
        master.RenewalPremiumST_A =[self getRenewalPremiumServiceTax:@"CAL_RPST_LS_A"];
    }
    
    if([_calValue containsString:@"CAL_RPST_LS_SA"]){
        master.RenewalPremiumST_SA =[self getRenewalPremiumServiceTax:@"CAL_RPST_LS_SA"];
    }
    
    if([_calValue containsString:@"CAL_RPST_LS_Q"]){
        master.RenewalPremiumST_Q =[self getRenewalPremiumServiceTax:@"CAL_RPST_LS_Q"];
    }
    
    if([_calValue containsString:@"CAL_RPST_LS_M"]){
       // master.RenewalPremiumST_M =[self getRenewalPremiumServiceTax:@"CAL_RPST_LS_M"];
        //changes 7th march - WOPP changes
        //changes 27th may
        double l = master.RenewalPrem_M * RPST_H;
        master.RenewalPremiumST_M = round(l);
    }
    
    if([_calValue containsString:@"CAL_TOTRP_LS_SA"]){
        //Total  renewal premium for Semi annual with service tax....
        //master.TotalRenewalPremium_SA = [self getTotalRenewalPremium:@"CAL_TOTRP_LS_SA"];
        //changes 4th march
        master.TotalRenewalPremium_SA = master.RenewalPrem_SA + master.RenewalPremiumST_SA;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_LS_Q"]){
        //master.TotalRenewalPremium_Q =[self getTotalRenewalPremium:@"CAL_TOTRP_LS_Q"];
        //changes 4th march
        master.TotalRenewalPremium_Q =master.RenewalPrem_Q + master.RenewalPremiumST_Q;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_LS_M"]){
        //master.TotalRenewalPremium_M =[self getTotalRenewalPremium:@"CAL_TOTRP_LS_M"];
        //changes 4th march
        master.TotalRenewalPremium_M = master.RenewalPrem_M + master.RenewalPremiumST_M;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_LS_A"]){
        master.TotalRenewalPremium_A =[self getTotalRenewalPremium:@"CAL_TOTRP_LS_A"];
    }
    //------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------
    //FOR MAHARAKSHA SUPREME
    if([_calValue containsString:@"CAL_RPST_RS_A"]){
        master.RenewalPremiumST_A =[self getRenewalPremiumServiceTax:@"CAL_RPST_RS_A"];
    }
    if([_calValue containsString:@"CAL_RPST_RS_SA"]){
        master.RenewalPremiumST_SA =[self getRenewalPremiumServiceTax:@"CAL_RPST_RS_SA"];
    }
    if([_calValue containsString:@"CAL_RPST_RS_Q"]){
        master.RenewalPremiumST_Q =[self getRenewalPremiumServiceTax:@"CAL_RPST_RS_Q"];
    }
    if([_calValue containsString:@"CAL_RPST_RS_M"]){
        //changes 7th march
        //27th may..for raksha supreme 1.88 to 15
        double l = master.RenewalPrem_M * BPST_H;
        master.RenewalPremiumST_M = round(l);
    }
    
    if([_calValue containsString:@"CAL_TOTRP_RS_SA"]){
        //Total  renewal premium for Semi annual with service tax....
        //master.TotalRenewalPremium_SA =[self getTotalRenewalPremium:@"CAL_TOTRP_RS_SA"];
        
        //changes 4th march - ADDL
        master.TotalRenewalPremium_SA = master.RenewalPrem_SA + master.RenewalPremiumST_SA;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_RS_Q"]){
        //master.TotalRenewalPremium_Q =[self getTotalRenewalPremium:@"CAL_TOTRP_RS_Q"];
        //changes 4th march - ADDL
        master.TotalRenewalPremium_Q = master.RenewalPrem_Q + master.RenewalPremiumST_Q;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_RS_M"]){
        //master.TotalRenewalPremium_M =[self getTotalRenewalPremium:@"CAL_TOTRP_RS_M"];
        //changes 4th march - ADDL
        master.TotalRenewalPremium_M = master.RenewalPrem_M + master.RenewalPremiumST_M;
    }
    
    if([_calValue containsString:@"CAL_TOTRP_RS_A"]){
        master.TotalRenewalPremium_A =[self getTotalRenewalPremium:@"CAL_TOTRP_RS_A"];
    }
    //------------------------------------------------------------------------------------
    
    
    if([_calValue containsString:@"CAL_RPST_A"]){
        //service tax for renewal premium for annual...
        master.RenewalPremiumST_A =[self getRenewalPremiumServiceTax:@"CAL_RPST_A"];
    }
    
    //**********************added for service tax of fourtune guarantee 07/sept
    if ([_calValue containsString:@"CAL_RPST_FG_SA"]) {
        //master.RenewalPrem_SA = [self getRenewalPremiumSemi];
        //Added by Amruta on 10/9/2015 for FG Rider
        master.RenewalPremiumST_SA = [self getRenewalPremiumServiceTaxMonthly:[self getRenewalPremiumSemi]];
    }
    if ([_calValue containsString:@"CAL_RPST_FG_Q"]) {
         //master.RenewalPrem_Q = [self getRenewalPremiumQuaterly];
        //Added by Amruta on 10/9/2015 for FG Rider
        master.RenewalPremiumST_Q = [self getRenewalPremiumServiceTaxMonthly:[self getRenewalPremiumQuaterly]];
    }
    if ([_calValue containsString:@"CAL_RPST_FG_M"]) {
       // master.RenewalPrem_M = [self getRenewalPremiumMonthly];
        //Added by Amruta on 10/9/2015 for FG Rider
        master.RenewalPremiumST_M = [self getRenewalPremiumServiceTaxMonthly:[self getRenewalPremiumMonthly]];
    }
    
    if ([_calValue containsString:@"CAL_TOTRP_FG_SA"]) {
        master.TotalRenewalPremium_SA = [self getTotalRenewalPremiumFG:[self getRenewalPremiumSemi]];
    }
    if ([_calValue containsString:@"CAL_TOTRP_FG_Q"]) {
        master.TotalRenewalPremium_Q = [self getTotalRenewalPremiumFG:[self getRenewalPremiumQuaterly]];
    }
    if ([_calValue containsString:@"CAL_TOTRP_FG_M"]) {
        master.TotalRenewalPremium_M = [self getTotalRenewalPremiumFG:[self getRenewalPremiumMonthly]];
    }
    //Added by Amruta for SR+
    if ([_calValue containsString:@"CAL_RPST_FG_A"]) {
        master.RenewalPremiumST_A = [self getRenewalPremiumServiceTaxMonthly:[self getRenewalPremiumAnnual]];
    }
    //**********************added for service tax of fourtune guarantee 07/sept
    
    if([_calValue containsString:@"CAL_RPST_SA"]){
        //service tax for renewal premium for semi annual...
        master.RenewalPremiumST_SA =[self getRenewalPremiumServiceTax:@"CAL_RPST_SA"];
    }
    
    if([_calValue containsString:@"CAL_RPST_Q"]){
        //service tax for renewal premium for Quarterly...
        master.RenewalPremiumST_Q =[self getRenewalPremiumServiceTax:@"CAL_RPST_Q"];
    }
    
    if([_calValue containsString:@"CAL_RPST_M"]){
        //service tax for renewal premium for Monthly....
        master.RenewalPremiumST_M =[self getRenewalPremiumServiceTax:@"CAL_RPST_M"];
    }
    
    if([_calValue containsString:@"CAL_TOTRP_SA"]){
        //Total  renewal premium for Semi annual with service tax....
        master.TotalRenewalPremium_SA =[self getTotalRenewalPremium:@"CAL_TOTRP_SA"];
    }
    
    if([_calValue containsString:@"CAL_TOTRP_Q"]){
        master.TotalRenewalPremium_Q =[self getTotalRenewalPremium:@"CAL_TOTRP_Q"];
        //master.TotalRenewalPremium_RS_Q = [self getTotalRenewalPremium:@"CAL_TOTRP_RS_Q"];
    }
    
    if([_calValue containsString:@"CAL_TOTRP_M"]){
        master.TotalRenewalPremium_M =[self getTotalRenewalPremium:@"CAL_TOTRP_M"];
    }
    
    if([_calValue containsString:@"CAL_TOTRP_A"]){
        master.TotalRenewalPremium_A =[self getTotalRenewalPremium:@"CAL_TOTRP_A"];
    }
    
    //Added by Amruta for SR+
    if ([_calValue containsString:@"CAL_TOTRP_FG_A"]) {
        master.TotalRenewalPremium_A = [self getTotalRenewalPremiumFG:[self getRenewalPremiumAnnual]];
    }
    
    if([_calValue containsString:@"TOTMODAL_PREM_PAYBLE"]){
        master.TotModalPremiumPayble =[self getTotalModalPremiumPayble];
    }
    
    master.TotalRiderAnnualPremium = [self getTotalRiderAnnualPremium];
    
    //********************************SGP*********************************
    if ([_calValue containsString:@"CALACCGAI"]) {
        master.AccGAI = [self getAccGAI];
    }
    
    if([_calValue containsString:@"CAL_VSRB4"]){
        //master.VSRBonus4 = [self getDict:CALCULATE_VRSBONUS_4];
        master.VSRBonus4 = [self getVSRBonus4];
        //master.MLGP_TotalRB4 = [self getTotalRB_MLGP:CALCULATE_VRSBONUS_4];
    }
    
    if([_calValue containsString:@"CAL_VSRB4_MIP"]){
        master.VSRBonus4 = [self getVSRBonus4_MIP];
    }
    
    
    if([_calValue containsString:@"CAL_VSRB8"]){
        master.VSRBonus8 = [self getVSRBonus8];
    }
    
    if([_calValue containsString:@"CAL_VSRB8_MIP"]){
        master.VSRBonus8 = [self getVSRBonus8_MIP];
    }
    
    if([_calValue containsString:@"CAL_TB4"]){
        //master.TerminalBonus4 = [self getDict:CALCULATE_TERMINAL_BONUS_4];
        master.TerminalBonus4 = [self getTerminalBonus4];
    }
    
    if([_calValue containsString:@"CAL_TB4_MIP"]){
        master.TerminalBonus4 = [self getTerminalBonus4_MIP];
    }
    //********************************SGP*********************************
    if([_calValue containsString:@"CAL_TB8"]){
        master.TerminalBonus8 = [self getTerminalBonus8];
    }
    
    if([_calValue containsString:@"CAL_TB8_MIP"]){
        master.TerminalBonus8 = [self getTerminalBonus8_MIP];
    }
    
    if([_calValue containsString:@"CAL_TDB4"]){
        master.TotalDeathBenefit4 = [self getDict:CALCULATE_TOT_DEATH_BENEFIT_4 WithCNTMINUS:true andWithOtherParam:ONEPLUSRB4];
    }
    
    if([_calValue containsString:@"CAL_TDB4_MIP"]){
        master.TotalDeathBenefit4 = [self getDeathBenefit_MIP:CALCULATE_TOT_DEATH_BENEFIT_4 andWithOtherParam:ONEPLUSRB4];
    }
    
    if([_calValue containsString:@"CAL_TDB8"]){
        master.TotalDeathBenefit8 = [self getDict:CALCULATE_TOT_DEATH_BENEFIT_8 WithCNTMINUS:true andWithOtherParam:ONEPLUSRB8];
    }
    
    if([_calValue containsString:@"CAL_TDB8_MIP"]){
        master.TotalDeathBenefit8 = [self getDeathBenefit_MIP:CALCULATE_TOT_DEATH_BENEFIT_8 andWithOtherParam:ONEPLUSRB8];
    }
    
    //*********FREEDOM********************
    if ([_calValue containsString:@"CALFR_TDB4"]) {
        master.TotalDeathBenefit4 = [self getDeathBenefit:CALCULATE_TOT_DEATH_BENEFIT_4 andWithOtherParam:ONEPLUSRB4];
    }
    
    if ([_calValue containsString:@"CALFR_TDB8"]) {
        master.TotalDeathBenefit8 = [self getDeathBenefit:CALCULATE_TOT_DEATH_BENEFIT_8 andWithOtherParam:ONEPLUSRB8];
    }
    
    
    //*********FREEDOM********************
    
    if([_calValue containsString:@"SB4"]){
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefit_4];
        master.NONGuaranteed4SurrBenefit = [self getSurrenderBenefit_4_SMART7];
    }
    
    if([_calValue containsString:@"SBMIP4"]){
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefit_4_MIP];
    }
    
    if([_calValue containsString:@"SB8"]){
        master.NONGuaranteedSurrBenefit_8 = [self getNONGuaranteedSurrBenefit_8];
    }
    
    if([_calValue containsString:@"SBMIP8"]){
        master.NONGuaranteedSurrBenefit_8 = [self getNONGuaranteedSurrBenefit_8_MIP];
    }
    
    //***********FREEDOM**************
    if ([_calValue containsString:@"SBFR4"]) {
        master.NONGuaranteedSurrBenefit = [self getSurrenderBenefitFR_4];
    }
    
    if ([_calValue containsString:@"SBFR8"]) {
        master.NONGuaranteedSurrBenefit_8 = [self getSurrenderBenefitFR_8];
    }
    //***********FREEDOM**************
    
    if([_calValue containsString:MAT4VAL]){
        master.TotalMaturityBenefit4 = [self getTotMaturityBenefit4];
    }
    
    if([_calValue containsString:MAT8VAL]){
	if ([master.sisRequest.BasePlan containsString:@"MI"]) {
		master.TotalMaturityBenefit8 = [self getTotMaturityBenefit8_MIP];
	} else {
        master.TotalMaturityBenefit8 = [self getTotMaturityBenefit8];
	}
    }
    
    //Freedom
    if ([_calValue containsString:@"TOT_MATVAL8_FR"]) {
        master.TotalMaturityBenefit8 = [self getTotMaturityBenefit8FR];
    }
    
    if ([_calValue containsString:@"TOT_MATVAL4_FR"]) {
        master.TotalMaturityBenefit4 = [self getTotMaturityBenefit4FR];
    }
    //Freedom
    
    //MIP
    if ([_calValue containsString:@"TOT_MATVAL8_MIP"]) {
        master.TotalMaturityBenefit8 = [self getTotMaturityBenefit8_MIP];
    }
    
    if ([_calValue containsString:@"TOT_MATVAL4_MIP"]) {
        master.TotalMaturityBenefit4 = [self getTotMaturityBenefit4_MIP];
    }
    //MIP
    
    
    if([_calValue containsString:@"CALIPC"]){
        master.GuaranteedInflationCover = [self getGuaranteedInflationCover];
    }
    
    if([_calValue containsString:@"CAL_TDBG"]){
        master.TotalDeathBenefit = [self getDict:CALCULATE_TOT_DEATH_BENEFIT_4 WithCNTMINUS:false andWithOtherParam:@""];
    }
    
    if([_calValue containsString:@"TOT_CASHDIV4"]){
        master.TotalCashDividend4 = [self getTotalRB_MLGP:CALCULATE_VRSBONUS_4];
    }
    
    if([_calValue containsString:@"TOT_CASHDIV8"]){
        master.TotalCashDividend8 = [self getTotalRB_MLGP:CALCULATE_VRSBONUS_8];
    }
    
    if([_calValue containsString:@"TOT_ANNCOUP"]){
        master.TotalGAnnualCoupon = [self getTotalGAI_MLGP];
    }
    
    //*************************************FG**********************************************
    if ([_calValue containsString:@"CAL_RP_FG_M"]) {
        master.RenewalPrem_M = [self getRenewalPremiumMonthly];
    }
    
    if ([_calValue containsString:@"CAL_RP_FG_SA"]) {
        master.RenewalPrem_SA = [self getRenewalPremiumSemi];
    }
    
    if ([_calValue containsString:@"CAL_RP_FG_Q"]) {
        master.RenewalPrem_Q = [self getRenewalPremiumQuaterly];
    }
    
    
    //****************************************ULIP********************************************
    
    
    if([_calValue containsString:@"PHACC10"]){
        //calculate Charges for Ph10
        [self calculateChargesForPH10];
        [self calculateChargesForPH10WithoutST];
        //calculate charges form ph10 without ST
    }
    
    if([_calValue containsString:@"PHACC6"]){
        //calculate Charges for Ph6
        [self calculateChargesForPH6];
    }
   
    
    //**********************************************MBP*********************************************
    //Added by Amruta on 24/8/15 for MBP
    
    if ([_calValue containsString:@"CALALLGAI"]) {
        //calculate guranteed annual income all
        master.GAIAll = [self getGuaranteedAnnualIncomeAll];
    }
    
    if ([_calValue containsString:@"TOT_MATVAL8_MBP"]) {
        master.TotalMaturityBenefit8 = [self getTotMaturityBenefit8_MBP];
    }
    
    if ([_calValue containsString:@"TOT_MATVAL4_MBP"]) {
        master.TotalMaturityBenefit4 = [self getTotMaturityBenefit4_MBP];
    }
    
    if ([_calValue containsString:@"TOT_RB8_MIP"]) {
        master.TOTALRB4_MIP = [self getTotalVSRBonus4_MIP];
        master.TOTALRB8_MIP = [self getTotalVSRBonus8_MIP];
        master.TOTALGAI_MIP = [self getTotalGuranteedAnnualIncome];
    }
    
    //good kid
    if ([_calValue containsString:TOT_MATVAL8_GK]) {
        master.TotalMaturityBenefit8 = [self getTotBen8Payable];
    }
    
    if ([_calValue containsString:TOT_MATVAL4_GK]) {
        master.TotalMaturityBenefit4 = [self getTotBen4Payable];
    }
    
    if ([_calValue containsString:@"GKSB8"]) {
        master.NONGuaranteedSurrBenefit_8 = [self getNonGuarSurrBen8_GK];
    }
    
    if ([_calValue containsString:@"GKSB4"]) {
        master.NONGuaranteedSurrBenefit = [self getNonGuarSurrBen4_GK];
    }
    
    
    //good kid
    
    //***********************ULIP ALTERNATE SCENARIO******************************************
    //Fund Performance..
    /*if([_calValue containsString:@"PHACCFP"]){
        //calculate Charges for FPPH10
        if(master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length != 0){
            //need to implement
            [self calculateChargesForFPPH10];
            master.sisRequest.FpSelected = @"YES";
        }else
            master.sisRequest.FpSelected = @"NO";
        // //smart selected check..
    }
    
    //Top Up WithDrawal...
    if([_calValue containsString:@"PHACCTUWD"]){
        if((master.sisRequest.FixWithDAmt != NULL && master.sisRequest.FixWithDAmt.length != 0) || (master.sisRequest.VarWithDAmtYr != NULL && master.sisRequest.VarWithDAmtYr.length != 0) || (master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length !=0) || (master.sisRequest.FixTopUpAmt != NULL && master.sisRequest.FixTopUpAmt.length != 0) || (master.sisRequest.VarTopUpAmtYr != NULL && master.sisRequest.VarTopUpAmtYr.length != 0)){
            NSMutableString *header = [[NSMutableString alloc] initWithString:@""];
            int noHeader = 0;
            
            if ((master.sisRequest.FixTopUpAmt != NULL && master.sisRequest.FixTopUpAmt.length != 0) || (master.sisRequest.VarTopUpAmtYr != NULL && master.sisRequest.VarTopUpAmtYr.length != 0)) {
                noHeader = 1;
                header = [[NSMutableString alloc] initWithString: @"Top-ups"];
            }
            if ((master.sisRequest.FixWithDAmt != NULL && master.sisRequest.FixWithDAmt.length != 0) || (master.sisRequest.VarWithDAmtYr != NULL && master.sisRequest.VarWithDAmtYr.length != 0)) {
                if(noHeader == 1){
                    [header appendString:@" and "];
                }
                [header appendString:@"Partial Withdrawals"];
                noHeader = 1;
            }
            if((master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length !=0)){
                if(noHeader == 1){
                    [header appendString:@" and "];
                }
                [header appendString:@"Actual Fund Performance"];
            }
            [self calculateTopUpPartialWithDForPH10];
            master.NetYield8SMart = 4.87;
            master.sisRequest.TopUpWDSelected = @"YES";
            master.header= header;
        } else
            master.sisRequest.TopUpWDSelected = @"NO";
        
    }
    
    if([_calValue containsString:@"PHACC10_AAA"]){
        
    }
    
    //alternate scenario smart8
    if([_calValue containsString:@"PHACC10_SMART"]){
        //calculate alternate charges smartph10
        if(master.sisRequest.SmartEquityFund != NULL && master.sisRequest.SmartEquityFund.length != 0){
            [self calculateAlternateChargesSmartPH10];
            master.NetYield8SMart = 7.17;
            master.sisRequest.SmartSelected = @"YES";
        }else
            master.sisRequest.SmartSelected = @"NO";
        //calculate yield smart
        //smart selected check..
    }
    
    //alternate scenario smart4
    if([_calValue containsString:@"PHACC6_SMART"]){
        //calculate alternate charges smartph6
        if(master.sisRequest.SmartEquityFund != NULL && master.sisRequest.SmartEquityFund.length != 0){
            //need to implement
            [self calculateAlternateChargesSmartPH6];
            master.sisRequest.SmartSelected = @"YES";
        }else
            master.sisRequest.SmartSelected = @"NO";
        
        //smart selected check..
    }*/
    //***********************ULIP ALTERNATE SCENARIO******************************************
    //****************************************ULIP********************************************
    
    
    /*
    if([_calValue containsString:@"PHACC6"]){
        //calculate Charges for Ph6
        [self calculateChargesForPH6];
    }
    
    
    
    
    
    if([_calValue containsString:@"PHACC10_SMART"]){
        //calculate alternate charges smartph10
        if(master.sisRequest.SmartEquityFund != NULL && master.sisRequest.SmartEquityFund.length != 0){
            [self calculateAlternateChargesSmartPH10];
            master.NetYield8SMart = 7.17;
            master.sisRequest.SmartSelected = @"YES";
        }else
            master.sisRequest.SmartSelected = @"NO";
        //calculate yield smart
        //smart selected check..
    }
    
    if([_calValue containsString:@"PHACC6_SMART"]){
        //calculate alternate charges smartph6
        if(master.sisRequest.SmartEquityFund != NULL && master.sisRequest.SmartEquityFund.length != 0){
            //need to implement
            [self calculateAlternateChargesSmartPH6];
            master.sisRequest.SmartSelected = @"YES";
        }else
            master.sisRequest.SmartSelected = @"NO";
        
        //smart selected check..
    }*/
    
    //Added by Amruta on 1/9/15 for MBP
    master.RenewalModalPremium = [self getRenewalModalPremium_A];
    
    master.addressBOObj = [DataAccessClass getAddressBO];
    master.PlanFeatureStr =[self getPlanFeature];
    master.TotalPremiumAnnual = [self getTotalPremiumAnnual];
    master.UINNumber = [self getUINNumber];
    master.planDescription =[self getPlanDescription];
    master.Agent_interMedDetail = [self getAgentDetails];
    
    //Added by Amruta for FG
    master.CommissionText=[self getComissionText];
    
    //-------To ask Java team Needed or not..
    /*
     if([_calValue containsString:@"CALIPC"]){
     master.GuaranteedInflationCover = [self getGuaranteedInflationCover];
     }
     
     if([_calValue containsString:@"CAL_TOTMATGAI"]){
     master.TotalMaturityValuePlusGAI=[self getTotalMaturityValuePlusGAI];
     }
     
     //MAHALIFE GOLD
     if([_calValue containsString:@"CAL_TDBGP"]){
     master.TotalDeathBenefit = [self getDict:CALCULATE_TOT_DEATH_BENEFIT_4 WithCNTMINUS:false];
     }
     */
    
    //Alternate scenario..
   /****************************SMART7 Methods*********************************************/
    
    //Added by Amruta for SMART 7 on 7/9/2015
    if([_calValue containsString:@"ALTSCE_SBCR"]){
        master.SurrenderBenefit8 = [self getSurrenderBenefit8];
    }
    
    if([_calValue containsString:@"ALTSCE_MAT4BEN_TAX"]){
        master.AltScenario4MaturityBenefitTax = [self getAltScenario4MaturityBenefitTax];
    }
    /*************************************************************************/
    
    /*
     if([_calValue containsString:@"LIFE_COV_RPU"]){
     master.LifeCoverageAltSCE = [self getLifeCoverageAltSCE];
     }
     
     if([_calValue containsString:@"CAL_ALTSCE_TDB4"]){
     master.ALTScenarioTotalDeathBenefit4 = [self getALTScenarioTotalDeathBenefit4];
     }
     
     if([_calValue containsString:@"CAL_ALTSCE_TDB8"]){
     master.ALTScenarioTotalDeathBenefit8 = [self getALTScenarioTotalDeathBenefit8];
     }
     
     if([_calValue containsString:@"CALCALTSCEMV"]){
     master.ALTScenarioMaturityVal = [self getALTScenarioMaturityVal];
     }
     
     if([_calValue containsString:@"CAL_ALTSCE_TDB4"]){
     master.ALTScenarioTotalDeathBenefit4 = [self getALTScenarioTotalDeathBenefit4];
     }
     
     if([_calValue containsString:@"CAL_ALTSCE_TDB8"]){
     master.ALTScenarioTotalDeathBenefit8 = [self getALTScenarioTotalDeathBenefit8];
     }
     
     if([_calValue containsString:@"ALTSCE_MAT4VAL"]){
     master.ALTScenarioMaturityValue4 = [self getALTScenarioMaturityValue4];
     }
     
     if([_calValue containsString:@"ALTSCE_MAT8VAL"]){
     master.ALTScenarioMaturityValue8 = [self getALTScenarioMaturityValue8];
     }
     
     if([_calValue containsString:@"ALTSCE_TOTMATBENCR"]){
     master.ALTScenarioToMatBenCurrRate = [self getALTScenarioToMatBenCurrRate];
     }
     
     if([_calValue containsString:@"ALTSCE_TOTMATBENCR"]){
     master.AltScenarioTotMatBenCurrentRate =[self getAltScenarioTotMatBenCurrentRate];
     }
     
     if([_calValue containsString:@"ALTSCE_SBCR"]){
     master.SurrenderBenefit8 = [self getSurrenderBenefit8];
     master.ALTScenarioSurrBenCurrRate = [self getALTScenarioSurrBenCurrRate];
     }
     
     if([_calValue containsString:@"ALTSCE_VBCUMMULATIVECR"]){
     master.ALTScenarioPremPaidCumulativeCurrRate = [self getALTScenarioPremPaidCumulativeCurrRate];
     }
     
     if([_calValue containsString:@"ALTSCE_TAXSAVING"]){
     master.ALTScenarioTaxSaving = [self getALTScenarioTaxSaving];
     }
     
     if([_calValue containsString:@"ALTSCE_AEP"]){
     master.ALTScenarioAnnualizedEffectivePremium = [self getALTScenarioAnnualizedEffectivePremium];
     }
     
     if([_calValue containsString:@"ALTSCE_CEP"]){
     master.ALTScenarioCumulativeEffectivePremium = [self getALTScenarioCumulativeEffectivePremium];
     }
     
     if([_calValue containsString:@"ALTSCE_MAT4BEN_TAX"]){
     master.AltScenario4MaturityBenefitTax = [self getAltScenario4MaturityBenefitTax];
     }
     
     if([_calValue containsString:@"ALTSCE_VBSCR"]){
     // not present
     }
     
     if([_calValue containsString:@"ALTSCE_TDBCR"]){
     master.AltScenarioTotalDeathBenefitCR =[self getAltScenarioTotalDeathBenefitCR];
     
     }
     if([_calValue containsString:@"ALTSCE_TOTMATBENCR"]){
     master.TotalMaturityBenefit8=[self getTotMaturityBenefit8];
     master.AltScenarioTotMatBenCurrentRate =[self getAltScenarioTotMatBenCurrentRate];
     }
     //*/
    
    //No calculation ULIP Paramter
    /*
     if([_calValue containsString:@"PHACC1O"]){
     //calculate Charges for Ph10
     [self calculateChargesForPH10];
     [self calculateChargesForPH10WithoutST];
     //calculate charges form ph10 without ST
     }
     
     if([_calValue containsString:@"PHACC6"]){
     //calculate Charges for Ph6
     [self calculateChargesForPH6];
     }
     
     
     
     if([_calValue containsString:@"PHACCFP"]){
     //calculate Charges for FPPH10
     if(master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length != 0){
     //need to implement
     [self calculateChargesForFPPH10];
     master.sisRequest.FpSelected = @"YES";
     }else
     master.sisRequest.FpSelected = @"NO";
     // //smart selected check..
     }
     
     if([_calValue containsString:@"PHACCTUWD"]){
     if((master.sisRequest.FixWithDAmt != NULL && master.sisRequest.FixWithDAmt.length != 0) || (master.sisRequest.VarWithDAmtYr != NULL && master.sisRequest.VarWithDAmtYr.length != 0) || (master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length !=0) || (master.sisRequest.FixTopUpAmt != NULL && master.sisRequest.FixTopUpAmt.length != 0) || (master.sisRequest.VarTopUpAmtYr != NULL && master.sisRequest.VarTopUpAmtYr.length != 0)){
     NSMutableString *header = [[NSMutableString alloc] initWithString:@""];
     int noHeader = 0;
     
     if ((master.sisRequest.FixTopUpAmt != NULL && master.sisRequest.FixTopUpAmt.length != 0) || (master.sisRequest.VarTopUpAmtYr != NULL && master.sisRequest.VarTopUpAmtYr.length != 0)) {
     noHeader = 1;
     header = [[NSMutableString alloc] initWithString: @"Top-ups"];
     }
     if ((master.sisRequest.FixWithDAmt != NULL && master.sisRequest.FixWithDAmt.length != 0) || (master.sisRequest.VarWithDAmtYr != NULL && master.sisRequest.VarWithDAmtYr.length != 0)) {
     if(noHeader == 1){
     [header appendString:@" and "];
     }
     [header appendString:@"Partial Withdrawals"];
     noHeader = 1;
     }
     if((master.sisRequest.FundPerform != NULL && master.sisRequest.FundPerform.length !=0)){
     if(noHeader == 1){
     [header appendString:@" and "];
     }
     [header appendString:@"Actual Fund Performance"];
     }
     [self calculateTopUpPartialWithDForPH10];
     master.NetYield8SMart = 4.87;
     master.sisRequest.TopUpWDSelected = @"YES";
     master.header= header;
     } else
     master.sisRequest.TopUpWDSelected = @"NO";
     
     }
     //No calculation ULIP Parameter..
     **/
    
    master.errorMessageAStr = [self getValidateMinimumPremium];
    
    return master;
}

#pragma mark-- PRIVATE METHODS
-(FormulaBean *)getFormulaBean{
    //FormulaBean *bean = [[FormulaBean alloc] init];
    bean.request = master.sisRequest;
    bean.premium = master.BaseAnnualizedPremium;
    bean.DiscountedPremium = master.DiscountedBasePremium;
    bean.QuotedAnnualizedPremium = master.QuotedAnnualizedPremium;
    bean.QuotedModalPremium = master.TotalModalPremium + master.ModalPremiumForNSAP;
    bean.ServiceTaxRider = master.STRiderModalPremium;
    //GIP
    bean.ANSAP = master.BaseAnnualizedPremium + master.ModalPremiumForNSAP;
    bean.ServiceTaxRider = master.STRiderModalPremium;
    
    //changes 18th feb
    bean.RiderPremium = master.TotalRiderModalPremium;
    //********************************SGP*********************************
    bean.premiumDiscount = master.premiumDiscount;
    bean.serviceTaxMain = [DataAccessClass getServiceTaxMain];
    bean.serviceTaxFirstYear = [DataAccessClass getServiceTaxFY];
    bean.serviceTaxRenewal = [DataAccessClass getServiceTaxRenewal];
    //********************************SGP*********************************
    
    //********************************ULIP*********************************
    bean.ULIPAP = master.sisRequest.BasePremiumAnnual;
    //********************************ULIP*********************************
    return bean;
}

-(long) getBasePremium:(NSString *)basePlan{
    long basePrem = 0;
    FormulaBean *formulaBeanObj = [self getFormulaBean];
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
    NSString *basePremium = [NSString stringWithFormat:@"%@ ",[formulaHandlerObj evaluateFormula:basePlan :CALCULATE_BASE_ANNUALIZED_PREMIUM :formulaBeanObj]];
    basePrem = [basePremium longLongValue];
    if(master.sisRequest.ProductType != NULL &&
       [master.sisRequest.ProductType isEqualToString:@"U"])
        master.PremiumMultiple = [master.sisRequest.Premium_Mul doubleValue];
    else
        master.PremiumMultiple = [DataAccessClass getPremiumMultiple:formulaBeanObj.request andWithMIP:NO];
    
    //********************************SGP*********************************
    master.premiumDiscount = [DataAccessClass getPremiumDiscount:master.sisRequest.BasePlan andWithSumAssured:master.sisRequest.Sumassured];
    //********************************SGP*********************************
    
    if(master.sisRequest.ProductType != NULL &&
       [master.sisRequest.ProductType isEqualToString:@"U"])
    {
        if([master.sisRequest.Frequency isEqualToString:@"S"]){
            return master.sisRequest.BasePremium * 2;
        }else if([master.sisRequest.Frequency isEqualToString:@"Q"])
            return master.sisRequest.BasePremium * 4;
        else if([master.sisRequest.Frequency isEqualToString:@"M"])
            return master.sisRequest.BasePremium * 12;
        else
            return master.sisRequest.BasePremium;
    }
    return basePrem;
}

-(NSString *)getAnnualPremium{
    NSString *AnnPrem = NULL;
    NSString *basePlan = master.sisRequest.BasePlan;
    if(master.sisRequest.ProductType != NULL && [master.sisRequest.ProductType isEqualToString:@"U"]){
        if([@"IPR1ULN1" isEqualToString:basePlan] || [@"IMX1ULN1" isEqualToString:basePlan] || [@"WPR1ULN1" isEqualToString:basePlan] || [@"WMX1ULN1" isEqualToString:basePlan]){
            if([master.sisRequest.Frequency isEqualToString:@"S"])
                return @"NA";
            else if([master.sisRequest.Frequency isEqualToString:@"Q"])
                return @"NA";
            else if([master.sisRequest.Frequency isEqualToString:@"M"])
                return @"NA";
            else
                return @"NA";
        }else{
            return [NSString stringWithFormat:@"%ld", master.sisRequest.BasePremiumAnnual];
        }
    }
    //NSLog(@"ANNUAL PREMIUM %@",AnnPrem);
    return AnnPrem;
}

-(long) getAnnualizedPremiumWithOccLoading{
    long annualPremiumOccLoading = 0;
    
    FormulaBean *formulaBeanObj = [self getFormulaBean];
    formulaBeanObj.premium = master.BaseAnnualizedPremium;
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
    NSString *annualPremiumOccLoadingStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ANNUAL_PREMIUM_WITH_OCCUP_LOADING :formulaBeanObj]];
    annualPremiumOccLoading = [annualPremiumOccLoadingStr longLongValue];
    
    //NSLog(@"ANNUALIZED PREMIUM WITH OCC %ld",annualPremiumOccLoading);
    return annualPremiumOccLoading;
}

-(long) getModalPremium{
    long modalPremium = 0;
    FormulaBean *formulaBeanObj = [self getFormulaBean];
    if(master.AnnualizedPremiumWithOccLoading == 0){
        if(master.sisRequest.ProductType != NULL && [master.sisRequest.ProductType isEqualToString:@"U"])
            formulaBeanObj.premium = master.sisRequest.BasePremium;
        else
            formulaBeanObj.premium = master.BaseAnnualizedPremium;
    }else{
        formulaBeanObj.premium = master.AnnualizedPremiumWithOccLoading;
    }
    
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
    NSString *modalPremiumStr = [NSString stringWithFormat:@"%@ ",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MODAL_PREMIUM :formulaBeanObj]];
    modalPremium = [modalPremiumStr longLongValue];
    
    //NSLog(@"MODAL PREMIUM %ld",modalPremium);
    return modalPremium;
}

-(long) getNSAPLoadPremium{
    long NSAPPremium = 0;
    if([@"N" caseInsensitiveCompare:master.sisRequest.AgeProofFlag] == NSOrderedSame){
        FormulaBean *formulaBeanObj = [self getFormulaBean];
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
        NSString *BasePremium = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_NON_STANDARD_AGE_PROOF :formulaBeanObj]];
        NSAPPremium = [BasePremium longLongValue];
        
    }else{
        NSAPPremium = 0;
    }
    //NSLog(@"NSAP LOAD PREMIUM %ld",NSAPPremium);
    return NSAPPremium;
}

-(long) getQuotedAnnualizedPremium{
    long QuotedAnnualizedPrem = 0;
    
    if([master.sisRequest.Frequency caseInsensitiveCompare:@"A"] == NSOrderedSame){
        QuotedAnnualizedPrem = master.ModalPremiumForNSAP + master.ModalPremium;
    }
    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"S"] == NSOrderedSame){
        QuotedAnnualizedPrem = (master.ModalPremiumForNSAP + master.ModalPremium) * 2;
    }
    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"Q"]== NSOrderedSame){
        QuotedAnnualizedPrem = (master.ModalPremiumForNSAP + master.ModalPremium) * 4;
    }
    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"M"]== NSOrderedSame){
        QuotedAnnualizedPrem = (master.ModalPremiumForNSAP + master.ModalPremium) * 12;
    }
    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"O"]== NSOrderedSame){
        QuotedAnnualizedPrem = (master.ModalPremiumForNSAP + master.ModalPremium);
    }
    
    //NSLog(@"QuotedAnnualizedPrem ::::::   %ld",QuotedAnnualizedPrem);
    return QuotedAnnualizedPrem;
}

-(long) getDiscountedPremium{
    long discountPremium = 0, NSAPPremium = 0;
    double discountedBasePremium = 0,occLoadValueOnPremium = 0;
    
    NSString *strparamValues = [DataAccessClass getSPVSysParamValues:master.sisRequest.BasePlan];
    
    FormulaBean *formulaBeanObj = [self getFormulaBean];
    formulaBeanObj.premium = master.BaseAnnualizedPremium;
    
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
    NSString *discountedBasePremiumValue = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_DISCOUNTED_PREMIUM :formulaBeanObj]];
    
    discountedBasePremium = [discountedBasePremiumValue doubleValue];
    //NSLog(@"Discounted Base prem %f",discountedBasePremium);
    
    if([strparamValues containsString:@"CAL_APWOL"]){
        NSString *occuLoadValueOnPremium = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OCCUPATION_LOAD_VALUE_ON_PREMIUM :formulaBeanObj]];
        occLoadValueOnPremium = [occuLoadValueOnPremium doubleValue];
        //NSLog(@"occupation load prem in discount calculation %f",occLoadValueOnPremium);
    }
    if([strparamValues containsString:@"CALC_NSAP"]){
        NSAPPremium = master.ModalPremiumForNSAP;
        //NSLog(@"NSAP Premium in discount calculation %ld",NSAPPremium);
    }
    //Changed by Amruta for staffDiscount on 19/10/2016
    if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || [master.sisRequest.BasePlan containsString:@"MRSP"] || [master.sisRequest.BasePlan containsString:@"MLS"] || [master.sisRequest.BasePlan containsString:@"MBPV"] || [master.sisRequest.BasePlan containsString:@"SEC7V1N1"])
    {
        if([master.sisRequest.Frequency caseInsensitiveCompare:@"S"] == NSOrderedSame){
            discountPremium = round((discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 2);
        }
        else if([master.sisRequest.Frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame){
            discountPremium = round((discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 4);
        }
        else if([master.sisRequest.Frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
            discountPremium = round((discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 12);
        }
    }
    else
    {
        if([master.sisRequest.Frequency caseInsensitiveCompare:@"S"] == NSOrderedSame){
            discountPremium = round(discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 2;
        }
        else if([master.sisRequest.Frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame){
            discountPremium = round(discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 4;
        }
        else if([master.sisRequest.Frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
            discountPremium = round(discountedBasePremium + occLoadValueOnPremium + NSAPPremium)* 12;
        }
    }
    if([master.sisRequest.Frequency caseInsensitiveCompare:@"O"] == NSOrderedSame){
        discountPremium = round(discountedBasePremium + occLoadValueOnPremium + NSAPPremium);
    }
    else if([master.sisRequest.Frequency caseInsensitiveCompare:@"A"] == NSOrderedSame){
        discountPremium = round(discountedBasePremium + occLoadValueOnPremium + NSAPPremium);
    }
    //NSLog(@"final discount prem %ld",discountPremium);
    return discountPremium;
}

-(NSMutableArray *)getRiderWithPremium{
    NSMutableArray *riderArr = master.sisRequest.RiderList;
    NSMutableArray *riderPremiumArr = [[NSMutableArray alloc] init];
    //changes 18th feb
    long lTotalRiderModalPrem = 0;
    //added by amruta
    long lTotalAnnualRiderModalPrem = 0;
    bean = [self getFormulaBean];
    FormulaHandler *formulaHandlerObj;
    if(riderArr.count > 0){
        for (int cnt = 0; cnt < riderArr.count; cnt++) {
            Rider *riderObj = [riderArr objectAtIndex:cnt];
            //changes 18th feb
            long riderPremium = 0;
            long riderAnnualPremium = 0;
            bean.CounterVariable = cnt;
            if([riderObj.RDL_CODE hasPrefix:@"PB"]){
                formulaHandlerObj = [[FormulaHandler alloc] init];
                NSString *riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:FORALL : CALCULATE_PB_RIDER_PREMIUM :bean]];
                riderPremium = [riderPremiumStr longLongValue];
            }else if([riderObj.RDL_CODE hasPrefix:@"ADDL"]){
                formulaHandlerObj = [[FormulaHandler alloc] init];
                NSString *riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:ADDL : CALCULATE_RIDER_PREMIUM :bean]];
                //changes 18th feb
                riderPremium = [riderPremiumStr longLongValue];
                //added by amruta`
                riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:ADDL : CALCULATE_ANNUAL_RIDER_PREMIUM :bean]];
                riderAnnualPremium = [riderPremiumStr longLongValue];
            }else if([riderObj.RDL_CODE hasPrefix:@"WOP"]){
                formulaHandlerObj = [[FormulaHandler alloc] init];
                NSString *riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:WOP : CALCULATE_RIDER_PREMIUM :bean]];
                riderPremium = [riderPremiumStr longLongValue];
                //Added by Amruta for WPP rider
            }else if([riderObj.RDL_CODE hasPrefix:@"WPP"]){
                formulaHandlerObj = [[FormulaHandler alloc] init];
                NSString *riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:WPP : CALCULATE_RIDER_PREMIUM :bean]];
                riderPremium = [riderPremiumStr longLongValue];
                
                riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:WPP : CALCULATE_ANNUAL_RIDER_PREMIUM_WPP :bean]];
                riderAnnualPremium = [riderPremiumStr longLongValue];
            }
            else{
                formulaHandlerObj = [[FormulaHandler alloc] init];
                NSString *riderPremiumStr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:FORALL : CALCULATE_RIDER_PREMIUM :bean]];
                riderPremium = [riderPremiumStr longLongValue];
            }
            //changes 18th feb
            //Added by Amruta for WPP rider
            long modalPremium = 0;
            if([riderObj.RDL_CODE hasPrefix:@"ADDL" ] || [riderObj.RDL_CODE hasPrefix:@"WOP"] || [riderObj.RDL_CODE hasPrefix:@"WPP"]){
                modalPremium = riderPremium;
                //Added by Amruta(NSOrderedSame) on 14/9/2015 for FG Rider
                if([master.sisRequest.Frequency caseInsensitiveCompare:SEMI_ANNUAL_MODE] == NSOrderedSame){
                    riderPremium = riderPremium * 2;
                }else if([master.sisRequest.Frequency caseInsensitiveCompare:QUATERLY_MODE]==NSOrderedSame){
                    riderPremium = riderPremium * 4;
                }
                else if([master.sisRequest.Frequency caseInsensitiveCompare:MONTHLY_MODE]==NSOrderedSame){
                    riderPremium = riderPremium * 12;
                }
            }
            else {
                if([master.sisRequest.Frequency caseInsensitiveCompare:ANNUAL_MODE]==NSOrderedSame){
                    modalPremium = riderPremium;
                }else if([master.sisRequest.Frequency caseInsensitiveCompare:SEMI_ANNUAL_MODE]==NSOrderedSame){
                    modalPremium = riderPremium / 2;
                }
                else if([master.sisRequest.Frequency caseInsensitiveCompare:QUATERLY_MODE]==NSOrderedSame){
                    modalPremium = riderPremium / 4;
                }
                else if([master.sisRequest.Frequency caseInsensitiveCompare:MONTHLY_MODE]==NSOrderedSame){
                    modalPremium = riderPremium / 12;
                }
                else if([master.sisRequest.Frequency caseInsensitiveCompare:SINGLE_MODE]==NSOrderedSame){
                    modalPremium = riderPremium;
                }
            }
            //}
            //changes 18th feb
            riderObj.ModalPremium = modalPremium;
            riderObj.Premium = riderPremium;
            riderObj.PolTerm = [self getRiderPolTerm:riderObj];
            riderObj.PremPayTerm = [self getRiderPremiumPayingTerm:riderObj];
            [riderPremiumArr addObject:riderObj];
            lTotalRiderModalPrem = lTotalRiderModalPrem + modalPremium;
            lTotalAnnualRiderModalPrem = lTotalAnnualRiderModalPrem + riderAnnualPremium;
        }
        //added by amruta
        double dServiceTaxRiderPrem = 0;
        //changes 19th jan
        /*if ([master.sisRequest.BasePlan isEqualToString:@"FGV1N1"]) {
         dServiceTaxRiderPrem = [self getServiceTax_Rider:lTotalRiderModalPrem];//[self getserv]
         }else*/
        dServiceTaxRiderPrem = [self getServiceTax:lTotalRiderModalPrem];
        master.TotalAnnRiderModalPremium = lTotalAnnualRiderModalPrem;
        master.TotalRiderModalPremium = lTotalRiderModalPrem;
        master.STRiderModalPremium = round(dServiceTaxRiderPrem);
    }
    return riderPremiumArr;
}

-(double) getServiceTax_Rider:(double) amount {
    double serviceTax = 0;
    bean = [self getFormulaBean];
    bean.AmountForServiceTax = amount;
    bean.premium = master.BaseAnnualizedPremium;
    FormulaHandler *formulaHandler = [[FormulaHandler alloc] init];
    NSString *serviceTaxValue = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SERVICE_TAX_RIDER :bean]];
    serviceTax = [serviceTaxValue doubleValue];
    
    return serviceTax;
}

-(int)getRiderPolTerm:(Rider *)riderObj{
    int pt = 0;
    PFRPlanFormulaRef *formularRefObj =    [DataAccessClass getFormula:master.sisRequest.BasePlan andWithValue:[NSString stringWithFormat:@"%@_PT", riderObj.RDL_CODE]];
    if(formularRefObj != NULL){
        bean = [self getFormulaBean];
        FormulaHandler *handle = [[FormulaHandler alloc] init];
        NSMutableDictionary *dict = [handle getFormulaExpression:formularRefObj.PER_FRM_CD];
        //Added by Amruta for WPP rider
        if([riderObj.RDL_CODE isEqualToString:@"WOPPULV1"] || [riderObj.RDL_CODE isEqualToString:@"WPPN1V1"]){
            bean.AmountForServiceTax = master.sisRequest.ProposerAge;
        }else
            bean.AmountForServiceTax = master.sisRequest.InsuredAge;
        
        NSString *ptstr = [NSString stringWithFormat:@"%@ ",(NSString *)[handle executeFormula:dict :bean]];
        pt = [ptstr intValue];
    }
    return pt;
}

-(int)getRiderPremiumPayingTerm:(Rider *)riderObj{
    int pt = 0;
    int ridTerm = 0;
    //changes 2nd march
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int insuredAge = master.sisRequest.InsuredAge;
    int planMatVal = insuredAge + ppt;
    
    PFRPlanFormulaRef *planFormulaRefObj = [DataAccessClass getFormula:master.sisRequest.BasePlan andWithValue:[NSString stringWithFormat:@"%@_PPT", riderObj.RDL_CODE]];
    if(planFormulaRefObj != NULL){
        bean = [self getFormulaBean];
        FormulaHandler *handle = [[FormulaHandler alloc] init];
        NSMutableDictionary *dict = [handle getFormulaExpression:planFormulaRefObj.PER_FRM_CD];
        
        //Added by Amruta for WPP rider
        if([riderObj.RDL_CODE isEqualToString:@"WOPPULV1"] || [riderObj.RDL_CODE isEqualToString:@"WPPN1V1"]){
            bean.AmountForServiceTax = master.sisRequest.ProposerAge;
        }else
        bean.AmountForServiceTax = master.sisRequest.InsuredAge;
        
        NSString *ptstr = [NSString stringWithFormat:@"%@ ",(NSString *)[handle executeFormula:dict :bean]];
        ridTerm = [ptstr intValue];
        //Added by Amruta for WPP and ADDLN1V1 rider
        if ([riderObj.RDL_CODE isEqualToString:@"ADDLN1V1"] && planMatVal > 70) {
            int riderCOvTerm = planMatVal - 70;
            pt = ppt - riderCOvTerm;
        }/*else if ([riderObj.RDL_CODE isEqualToString:@"WPPN1V1"] && planMatVal > 65) {
            pt = MIN(ppt, 65- proposerAge);
        }*/else
        pt = ridTerm;
        
    }
    return pt;
}

-(long) getTotalRiderModePremium{
    long riderPremiumTotal = 0;
    NSMutableArray *arr = master.riderData;
    for (int i =0; i<arr.count; i++) {
        Rider *riderObj = [arr objectAtIndex:i];
        riderPremiumTotal = riderPremiumTotal + riderObj.ModalPremium;
    }
    return riderPremiumTotal;
}

-(long) getTotalModalPremium{
    long riderPremTotal = 0;
    NSMutableArray *arr = master.riderData;
    for (int i =0; i<arr.count; i++) {
        Rider *riderObj = [arr objectAtIndex:i];
        riderPremTotal = riderPremTotal + riderObj.ModalPremium;
    }
    //NSLog(@"Total Modal Premium %ld",riderPremTotal + master.ModalPremium);
    return riderPremTotal + master.ModalPremium;
}

-(double) getRiderCharges:(int)count :(int)age :(int)ppt :(int)mnth{
    double riderCharges = 0;
    NSMutableArray *arr = master.riderData;
    NSString *rtCode;
    long rtlId = 0.0,rtlCoverage = 0.0,rtlTerm = 0.0;
    for (int i=0; i<arr.count; i++) {
        Rider *riderObj = [arr objectAtIndex:i];
        rtlId = riderObj.RDLID;
        rtCode = [riderObj RDL_CODE];
        rtlCoverage = [riderObj SumAssured];
        rtlTerm = [riderObj PolTerm];
        
        riderCharges = riderCharges + [self getRiderCharges:rtCode :rtlId :count :age :ppt :rtlCoverage :rtlTerm : mnth];
    }
    return riderCharges;
}

-(double) getRiderCharges:(NSString *)rtCode :(long)rtlID :(int)count :(int)age :(int)ppt :(long)rtlCoverage :(long)rtlterm :(int)mnth{
    double riderCharges = 0;
    bean = [self getFormulaBean];
    bean.MonthCount = mnth;
    if([rtCode hasPrefix:@"ADDL"]){
        bean = [self getFormulaBean];
        bean.CounterVariable = count;
        bean.AmountForServiceTax =rtlID;
        bean.RC = rtlCoverage;
        
        if(rtlterm >= count){
            FormulaHandler *handle = [[FormulaHandler alloc] init];
            NSString *ADDLRiderCharge =  [NSString stringWithFormat:@"%@ ",(NSString *)[handle evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ADDL_RIDER_CHARGE :bean]];
            riderCharges = [ADDLRiderCharge doubleValue];
        }else{
            riderCharges = 0;
        }
        
    }else if([rtCode hasPrefix:@"WOPP"] && count <= ppt){
        bean = [self getFormulaBean];
        bean.CounterVariable = count;
        bean.AmountForServiceTax =rtlID;
        bean.RC = rtlCoverage;
        
        if(rtlterm >= count){
            FormulaHandler *handle = [[FormulaHandler alloc] init];
            NSString *WOPPRiderCharge =  [NSString stringWithFormat:@"%@ ",(NSString *)[handle evaluateFormula:master.sisRequest.BasePlan : CALCULATE_WOP_RIDER_CHARGE :bean]];
            riderCharges = [WOPPRiderCharge doubleValue];
        }else{
            riderCharges = 0;
        }
    }else if([rtCode hasPrefix:@"WOP"] && count <= ppt){
        bean = [self getFormulaBean];
        bean.CounterVariable = count;
        bean.AmountForServiceTax =rtlID;
        bean.RC = rtlCoverage;
        
        if(rtlterm >= count){
            FormulaHandler *handle = [[FormulaHandler alloc] init];
            NSString *WOPRiderCharge =  [NSString stringWithFormat:@"%@ ",(NSString *)[handle evaluateFormula:master.sisRequest.BasePlan : CALCULATE_WOP_RIDER_CHARGE :bean]];
            riderCharges = [WOPRiderCharge doubleValue];
        }else{
            riderCharges = 0;
        }
    }
    return riderCharges;
}

-(long)getGuaranteedMaturityAddition :(int)cnt :(double)netPhAcc{
    long lGuarMatAdd = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    //double dGuarMaturityAddition = 0;
    if(cnt == polTerm){
        bean = [self getFormulaBean];
        bean.AmountForServiceTax = netPhAcc;
        bean.CounterVariable = cnt;
        
        FormulaHandler *handle = [[FormulaHandler alloc] init];
        NSString *GuarMatAddStr =  [NSString stringWithFormat:@"%@ ",(NSString *)[handle evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_MATURITY_ADDITION :bean]];
        lGuarMatAdd = [GuarMatAddStr longLongValue];
    }else{
        lGuarMatAdd = 0;
    }
    return lGuarMatAdd;
}

//changes 5 aug long to long long
-(long long)getSurrenderValue :(int)cnt :(double)netPhAcc{
    long long lSurrValue = 0.0;
    double dSurrValue = 0.0;
    double dCalculatedSurrValue = 0.0;
    
    //FormulaBean *bean = [self getFormulaBean];
    bean.NetPremHolderAcc = netPhAcc;
    bean.CounterVariable = cnt;
    
    FormulaHandler *handle = [[FormulaHandler alloc] init];
    NSString *SurrValStr =  [NSString stringWithFormat:@"%@ ",(NSString *)[handle evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURR_CHARGE :bean]];
    dSurrValue = [SurrValStr doubleValue];
    //double dSerTax_SurCharge = [self getServiceTax:dSurrValue];
    
    dCalculatedSurrValue = netPhAcc - dSurrValue;
    //changes 5 aug lround to roundl
    lSurrValue = roundl(dCalculatedSurrValue);
    
    return  lSurrValue;
}

-(void) calculateChargesForPH10{
    double PAC = 0.0;
    NSMutableDictionary *PremiumAllocationDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *InvestmentAmtDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *OtherbenefitChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *MortalityChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *PolicyAdminChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ApplicableServiceTaxDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *FundMgmtChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueBeforeFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValuePostFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *GuaranteedMaturityAmtDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalFundValueAtEndPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *SurrenderValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *DeathBenefitAtEndOfPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *CommissionPayableDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *LoyaltyChargeDict = [[NSMutableDictionary alloc] init];
    
    long lOtherBenefitCharges = 0;
    double dOtherBenefitCharges = 0.0;
    int age = master.sisRequest.InsuredAge;
    NSString *frequency = master.sisRequest.Frequency;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt =(int)master.sisRequest.PremiumPayingTerm;
    long lMortalityCharges=0;
    double lAdminCharge=0.0;
    double dAdminCharge=0.0;
    double PremiumAllocChargesSTAX = 0.0;
    double OtherBenefitSTAX=0.0;
    double AdminChargesSTAX=0.0;
    double PHAccount=0.0;
    double PHInvestmentFee=0.0;
    double netPhAccount = 0.0;
    double MortChargesStax = 0.0;
    double tempNetHolderAcc = 0.0;
    double tempMortalityCharges=0.0;
    double tempFullMortalityCharges=0.0;
    double tempNetHolderInvestment = 0.0;
    double tempPHAccBeforeGrouth = 0.0;
    double tempPHAccInvestGrouth=0.0;
    double tempPHAccHolderer = 0.0;
    double tempPHInvestmentFee = 0.0;
    double PHInvestFeesTax = 0.0;
    double tempPHNetHolderAcc = 0.0;
    double tempAdminChargesSTAX = 0.0;
    double FundGrouthPM = 0.00643403011;
    //changes 5 aug
    long long totalFundValueAtEndPolicyYear=0;
    long totalCharges=0;
    double tempFinalPHNetHolderAcc = 0.0;
    long long surrenderValue=0.0;
    //FormulaBean *formulaBeanObj;
    for(int cnt=1;cnt<=polTerm;cnt++){
        long ApplicableST = 0;
        double TotalST = 0;
        double PHInvestFeeTotal = 0.0;
        long RegularFundValueBeforeFMC = 0;
        long RegularFundValuePostFMC = 0;
        NSString *totalInvest = NULL;
        double riderCharges = 0.0;
        
        double TotalRiderCharges = 0.0;
        double loyalityCharges = 0.0;
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
        NSString *premAllocCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PREMIUM_ALLOCATION_CHARGES :bean]];
        if(cnt <= ppt){
            PAC = [premAllocCharges doubleValue];
        }else{
            PAC = 0.0;
        }
        
        [PremiumAllocationDict setObject:[NSNumber numberWithDouble:round(PAC)] forKey:[NSNumber numberWithInt:cnt]];
        bean = [self getFormulaBean];
        bean.AmountForServiceTax = PAC;
        bean.CounterVariable = cnt;
        if(cnt <= ppt){
            //Changed in formula of STAX_U (14.5 changed to 14) for IO
            totalInvest =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TOTAL_INVESTMENT_CHARGES :bean]];
        }
        else {
            totalInvest = @"0";
        }
        PremiumAllocChargesSTAX = [self getServiceTax:PAC];
        TotalST = TotalST + PremiumAllocChargesSTAX;
        
        double investAmt = [totalInvest doubleValue];
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *strCommPayable =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_COMMISSION_PAYABLE :bean]];
        double comm = [strCommPayable doubleValue];
        
        if(cnt <= ppt){
            [InvestmentAmtDict setObject:[NSNumber numberWithDouble:round(investAmt)] forKey:[NSNumber numberWithInt:cnt]];
            [CommissionPayableDict setObject:[NSNumber numberWithDouble:round(comm)] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [InvestmentAmtDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
            [CommissionPayableDict setObject:[NSNumber numberWithDouble:0] forKey:[NSNumber numberWithInt:cnt]];
        }
        
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        NSString *otherBenCharges =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES :bean]];
        lOtherBenefitCharges = [otherBenCharges longLongValue];
        //NSLog(@"benefit charges %ld",lOtherBenefitCharges);
        
        NSString *otherBenChargesMnthly = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY :bean]];
        dOtherBenefitCharges = [otherBenChargesMnthly doubleValue];
        
        OtherBenefitSTAX = [self getServiceTax:dOtherBenefitCharges];
        TotalST = TotalST + (OtherBenefitSTAX * 12);
        
        bean = [self getFormulaBean];
        bean.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *AdminCharges= [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ADMIN_CHARGES :bean]];
        dAdminCharge = [AdminCharges doubleValue];
        
        //admin fee
        lAdminCharge = dAdminCharge * 12;
        
        [PolicyAdminChargesDict setObject:[NSNumber numberWithDouble:round(lAdminCharge)] forKey:[NSNumber numberWithInteger:cnt]];
        
        AdminChargesSTAX = [self getServiceTax:dAdminCharge];
        TotalST = TotalST + (AdminChargesSTAX * 12);
        
        double serviceTaxOnInvestFee = 0.0;
        tempFullMortalityCharges = 0.0;
        
        for(int i=1;i<=12;i++){
            double finalLoyalty = 0;
            double tempHolderNetToInvest = investAmt;
            if(([frequency caseInsensitiveCompare:@"A"] == NSOrderedSame || [frequency caseInsensitiveCompare:@"O"] == NSOrderedSame) && i == 1){
                tempHolderNetToInvest = investAmt;
            }else if([frequency caseInsensitiveCompare:@"S"] == NSOrderedSame && (i == 1 || i == 7)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/2 - PAC/2 - [self getServiceTax:(PAC/2)];
            }else if([frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame && (i == 1 || i == 7 || i == 4 || i == 10)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/4 - PAC/4 - [self getServiceTax:(PAC/4)];
            }else if([frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/12 - PAC/12 - [self getServiceTax:(PAC/12)];
            }else{
                tempHolderNetToInvest = 0.0;
            }
            if(cnt > ppt){
                tempHolderNetToInvest = 0.0;
            }
            
            bean.AmountForServiceTax = tempHolderNetToInvest + tempFinalPHNetHolderAcc;
            bean.CounterVariable = cnt;
            bean.MonthCount = i;
            
            NSString *mortalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MORTALITY_CHARGES_MONTHLY :bean]];
            tempMortalityCharges = [mortalityCharges doubleValue];
            
            tempFullMortalityCharges += tempMortalityCharges;
            riderCharges = [self getRiderCharges:cnt :age :ppt :i];
            
            //serive tax on cost of cover
            MortChargesStax = [self getServiceTax:(tempMortalityCharges + riderCharges)];
            TotalRiderCharges = TotalRiderCharges + riderCharges;
            //cost of cover
            lMortalityCharges = lround(tempFullMortalityCharges);
            
            //NSLog(@"the mortality charge %f",tempMortalityCharges);
            [MortalityChargesDict setObject:[NSNumber numberWithLong:lMortalityCharges] forKey:[NSNumber numberWithInt:cnt]];
            TotalST = TotalST + MortChargesStax;
            
            //service tax on admin value
            tempAdminChargesSTAX = [self getServiceTax:dAdminCharge];
            //net p/holder account
            tempNetHolderInvestment = tempHolderNetToInvest - tempMortalityCharges - MortChargesStax - dAdminCharge - tempAdminChargesSTAX - riderCharges;
            tempPHAccBeforeGrouth = tempNetHolderAcc + tempNetHolderInvestment;
            
            bean.AmountForServiceTax = 0.1;
            
            //p/holder inv growth
            tempPHAccInvestGrouth = tempPHAccBeforeGrouth * FundGrouthPM;
            
            //p/holder account
            tempPHAccHolderer = tempPHAccBeforeGrouth + tempPHAccInvestGrouth;
            
            bean.AmountForServiceTax = tempPHAccHolderer;
            
            NSString *tempPHInvestFeesValue = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PH_INVESTMENT_FEE :bean]];
            //p/holder inv fee..
            tempPHInvestmentFee = [tempPHInvestFeesValue doubleValue];
            
            //service tax on fee..
            PHInvestFeesTax = [self getServiceTax:tempPHInvestmentFee];
            TotalST = TotalST + PHInvestFeesTax;
            
            //net p/holder account..
            tempPHNetHolderAcc = tempPHAccHolderer - tempPHInvestmentFee - PHInvestFeesTax;
            
            double InvestmentFeesTax = 0.0;
            PHAccount = tempPHAccHolderer;
            PHInvestmentFee = tempPHInvestmentFee;
            PHInvestFeeTotal = PHInvestFeeTotal + PHInvestmentFee;
            
            InvestmentFeesTax = PHInvestFeesTax;
            serviceTaxOnInvestFee = serviceTaxOnInvestFee + InvestmentFeesTax;
            netPhAccount = PHAccount - PHInvestmentFee - InvestmentFeesTax;
            
            if(i == 12){
                [OtherbenefitChargesDict setObject:[NSNumber numberWithDouble:round(TotalRiderCharges)] forKey:[NSNumber numberWithInt:cnt]];
                
                bean.AmountForServiceTax = tempPHNetHolderAcc;
                NSString *tempLoyalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LC :bean]];
                loyalityCharges = [tempLoyalityCharges doubleValue];
                
                finalLoyalty = loyalityCharges;
                [LoyaltyChargeDict setObject:[NSNumber numberWithDouble:round(finalLoyalty)] forKey:[NSNumber numberWithInt:cnt]];
                
                RegularFundValueBeforeFMC = round(netPhAccount + PHInvestFeeTotal + serviceTaxOnInvestFee);
                [RegularFundValueBeforeFMCDict setObject:[NSNumber numberWithDouble:RegularFundValueBeforeFMC] forKey:[NSNumber numberWithInt:cnt]];
                
                long guaranteedMaturityAmt = [self getGuaranteedMaturityAddition:cnt :netPhAccount];
                [GuaranteedMaturityAmtDict setObject:[NSNumber numberWithLong:guaranteedMaturityAmt] forKey:[NSNumber numberWithInt:cnt]];
                
                RegularFundValuePostFMC = round(tempPHNetHolderAcc);
                [RegularFundValuePostFMCDict setObject:[NSNumber numberWithDouble:RegularFundValuePostFMC] forKey:[NSNumber numberWithInt:cnt]];
            }
            
            tempFinalPHNetHolderAcc = tempPHNetHolderAcc + finalLoyalty;
            tempNetHolderAcc = tempFinalPHNetHolderAcc;
            //changes 5 aug lround to roundl
            totalFundValueAtEndPolicyYear = roundl(tempNetHolderAcc);
            
            //changes 27th oct
            //issue was in iwealth
            bean.premium = master.sisRequest.BasePremiumAnnual;
             surrenderValue = [self getSurrenderValue:cnt :tempFinalPHNetHolderAcc];
            //changes 5 aug long to long long.
            [SurrenderValueDict setObject:[NSNumber numberWithLongLong:surrenderValue] forKey:[NSNumber numberWithInt:cnt]];
            
        }
        age++;
        if(cnt <= ppt){
            double tc = PAC+TotalRiderCharges+lAdminCharge+tempFullMortalityCharges;
            totalCharges = lround(tc);
        }else{
            totalCharges = lround(TotalRiderCharges+lAdminCharge+tempFullMortalityCharges);
        }
        
        [TotalChargeDict setObject:[NSNumber numberWithLong:totalCharges] forKey:[NSNumber numberWithInt:cnt]];
        
        ApplicableST = round(TotalST);
        [ApplicableServiceTaxDict setObject:[NSNumber numberWithLong:ApplicableST] forKey:[NSNumber numberWithInt:cnt]];
        [FundMgmtChargeDict setObject:[NSNumber numberWithLong:lround(PHInvestFeeTotal)] forKey:[NSNumber numberWithInt:cnt]];
        //changes 5 aug long to long long
        [TotalFundValueAtEndPolicyYrDict setObject:[NSNumber numberWithLongLong:totalFundValueAtEndPolicyYear] forKey:[NSNumber numberWithInt:cnt]];
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalFundValueAtEndPolicyYear;
        
        NSString *DBEPstr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR :bean]];
        
        double dDBEP = [DBEPstr doubleValue];
        
        [DeathBenefitAtEndOfPolicyYrDict setObject:[NSNumber numberWithDouble:round(dDBEP)] forKey:[NSNumber numberWithInt:cnt]];
    }
    
    master.AmountForInvest = InvestmentAmtDict;
    master.PremiumAllocationCharges = PremiumAllocationDict;
    master.OtherBenefitCharges = OtherbenefitChargesDict;
    master.MortalityCharges = MortalityChargesDict;
    master.PolicyAdminCharges = PolicyAdminChargesDict;
    master.ApplicableServiceTaxPH10 = ApplicableServiceTaxDict;
    master.TotalChargeForPh10 = TotalChargeDict;
    master.LoyalityChargeForPh10 = LoyaltyChargeDict;
    master.FundManagementChargePH10 = FundMgmtChargeDict;
    master.RegularFundValuePH10 = RegularFundValueDict;
    master.RegularFundValueBeforeFMCPH10 = RegularFundValueBeforeFMCDict;
    master.RegularFundValuePostFMCPH10 = RegularFundValuePostFMCDict;
    master.GuaranteedMaturityAdditionPH10 = GuaranteedMaturityAmtDict;
    master.TotalFundValueAtEndPolicyYrPH10 = TotalFundValueAtEndPolicyYrDict;
    master.SurrenderValuePH10 = SurrenderValueDict;
    master.DeathBenefitAtEndOfPolicyYear = DeathBenefitAtEndOfPolicyYrDict;
    master.CommissionPayable = CommissionPayableDict;
}

-(void) calculateChargesForPH10WithoutST{
    double PAC =0;
    
    int age = master.sisRequest.InsuredAge;
    NSString *frequency = master.sisRequest.Frequency;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    
    double lAdminCharge = 0;
    double dAdminCharge = 0;
    double phAccount = 0;
    double phInvestFee = 0;
    double netPHAcc = 0;
    double tempNetHolderAcc = 0;
    double tempNetHolderInvest = 0;
    double tempPHAccBeforeGrowth = 0;
    double tempPHInvestGrowth = 0;
    double tempPHAccHolder = 0;
    double tempPHInvestFee = 0;
    double tempPHNetHolderAcc = 0;
    
    double fundGrowthPM = 0.00643403011;
    
    long totalFundValueAtEndPolicyYr = 0;
    double totalCharges = 0;
    NSDecimalNumber *tempFinalPhNetHolderAcc = [[NSDecimalNumber alloc] initWithDouble:0.0];
    
    NSMutableDictionary *PHIRRTerm = [[NSMutableDictionary alloc] init];
    long tempPremium = master.sisRequest.BasePremiumAnnual;
    //FormulaBean *bean = nil;
    
    for (int cnt = 1; cnt <= polTerm; cnt++) {
        double phInvestmentFeeTotal = 0;
        long regularFundValueBeforeFMC = 0;
        double loyaltyCharges =0;
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *premAllocCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PREMIUM_ALLOCATION_CHARGES :bean]];
        
        if (cnt <= ppt) {
            PAC = [premAllocCharges doubleValue];
        }else{
            PAC = 0.0;
        }
        
        double investmentAmt = 0;
        if(cnt <= ppt){
            investmentAmt = tempPremium - PAC;
        }
        bean.premium = master.sisRequest.BasePremiumAnnual;
        NSString *adminCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ADMIN_CHARGES :bean]];
        
        dAdminCharge = [adminCharges doubleValue];
        lAdminCharge = dAdminCharge * 12;
        
        for (int i=1; i<=12; i++) {
            double finalLoyalty = 0;
            double tempHolderNetToInvest = investmentAmt;
            
            if(cnt <= ppt){
                if(([frequency caseInsensitiveCompare:@"A"] == NSOrderedSame || [frequency caseInsensitiveCompare:@"O"] == NSOrderedSame)  && i==1){
                    tempHolderNetToInvest = investmentAmt;
                    [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:tempPremium] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                }else if([frequency caseInsensitiveCompare:@"S"] == NSOrderedSame && (i==1 || i == 7)){
                    [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:tempPremium * 0.5] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                    tempHolderNetToInvest = master.BaseAnnualizedPremium / 2 - PAC/2;
                }else if([frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame && (i==1 || i == 7 || i == 4 || i== 10)){
                    [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:tempPremium * 0.25] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                    tempHolderNetToInvest = master.BaseAnnualizedPremium / 4 - PAC/4;
                }
                else if([frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
                    long t = (tempPremium * 0.0833);
                    double tempMPremium = [[NSString stringWithFormat:@"%ld",t] doubleValue];
                    [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:tempMPremium] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                    tempHolderNetToInvest = master.BaseAnnualizedPremium / 12 - PAC / 12;
                }else {
                    [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:0.0] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                    tempHolderNetToInvest = 0.0;
                }
            }else{
                [PHIRRTerm setObject:[NSDecimalNumber numberWithDouble:0.0] forKey:[NSNumber numberWithInt:(cnt - 1) *12 + i]];
                tempHolderNetToInvest = 0.0;
            }
            
            //NSLog(@"temp holder invest ======= %f",tempHolderNetToInvest);
            //NSLog(@"dAdminCharge  ========= %f",dAdminCharge);
            
            tempNetHolderInvest = tempHolderNetToInvest - dAdminCharge;
            //NSLog(@"tempNetHolderInvest is  ====== %f",tempNetHolderInvest);
            
            tempPHAccBeforeGrowth = tempNetHolderAcc + tempNetHolderInvest;
            //NSLog(@" tempPHAccBeforeGrowth is ======= %f",tempPHAccBeforeGrowth);
            
            bean.AmountForServiceTax =  0.1;
            
            //NSLog(@" fundGrowthPM ====== %f",fundGrowthPM);
            tempPHInvestGrowth = tempPHAccBeforeGrowth * fundGrowthPM;
            //NSLog(@" tempPHInvestGrowth is ====== %f",tempPHInvestGrowth);
            
            tempPHAccHolder = tempPHAccBeforeGrowth + tempPHInvestGrowth;
            //NSLog(@"tempPHAccHolder is ====== %f",tempPHAccHolder);
            
            bean.AmountForServiceTax = tempPHAccHolder;
            
            NSString *tempPhInvestFeeValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PH_INVESTMENT_FEE :bean]];
            tempPHInvestFee = [tempPhInvestFeeValue doubleValue];
            //NSLog(@" tempPHInvestFee ==== %f",tempPHInvestFee);
            
            tempPHNetHolderAcc = tempPHAccHolder - tempPHInvestFee;
            //NSLog(@"tempPHNetHolderAcc ===== %f",tempPHNetHolderAcc);
            
            phAccount = tempPHAccHolder;
            //NSLog(@" phAccount ==== %f",phAccount);
            
            phInvestFee = tempPHInvestFee;
            //NSLog(@"phInvestFee ==== %f",phInvestFee);
            
            phInvestmentFeeTotal = phInvestmentFeeTotal + phInvestFee;
            //NSLog(@"phInvestmentFeeTotal === %f",phInvestmentFeeTotal);
            
            netPHAcc = phAccount - phInvestFee;
            //NSLog(@"netPHAcc ==== %f",netPHAcc);
            
            if(i == 12){
                bean.AmountForServiceTax = tempPHNetHolderAcc;
                NSString *tempLoyaltyCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LC :bean]];
                loyaltyCharges = [tempLoyaltyCharges doubleValue];
                
                finalLoyalty = loyaltyCharges;
                
                regularFundValueBeforeFMC = round(netPHAcc + phInvestmentFeeTotal);
                long guaranteedMaturityAmt = [self getGuaranteedMaturityAddition:cnt :netPHAcc];
                //NSLog(@"guarantee maturity amt %ld, %ld",guaranteedMaturityAmt,regularFundValueBeforeFMC);
            }
            
            tempFinalPhNetHolderAcc = (NSDecimalNumber *)[NSDecimalNumber numberWithDouble:(tempPHNetHolderAcc + finalLoyalty)];
            tempNetHolderAcc = [tempFinalPhNetHolderAcc doubleValue];
            totalFundValueAtEndPolicyYr = round(tempNetHolderAcc);
            
            //changes 5 aug
            long long surrenderValue = [self getSurrenderValue:cnt :(tempPHNetHolderAcc + finalLoyalty)];
            //NSLog(@"guarantee maturity amt %ld",surrenderValue);
        }
        age++;
        if(cnt <= ppt){
            totalCharges = round(PAC+ lAdminCharge);
        }else
            totalCharges = round(lAdminCharge);
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalFundValueAtEndPolicyYr;
        
        NSString *DBEP = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR :bean]];
        double dDBEP = [DBEP doubleValue];
        //NSLog(@"DDBEP ==== %f, %f",dDBEP,totalCharges);
    }
    
    //[PHIRRTerm setObject:[zeroVal decimalNumberBySubtracting:tempFinalPhNetHolderAcc] forKey:[NSNumber numberWithInt:0]];
    //NSLog(@"the last value is %@",tempFinalPhNetHolderAcc);
    [PHIRRTerm setObject:tempFinalPhNetHolderAcc forKey:[NSNumber numberWithInt:((polTerm * 12)+1)]];
    
    /*int count = [[PHIRRTerm allValues] count];
    double * floatArray;
    int index;

    floatArray = (double *) malloc(sizeof(double) * count);
    for (index = 0; index < count; index++) {
        floatArray[index] = [[PHIRRTerm objectForKey:[NSNumber numberWithInt:index]] doubleValue];
    }
    
    
    double netYeild1 = computeIRR(floatArray, polTerm * 12);*/
    double netYeild = [self getIRRAlternate:PHIRRTerm :polTerm * 12 ];
    
    
    netYeild = netYeild * 100;
    netYeild = [[NSString stringWithFormat:@"%0.02f",netYeild] doubleValue];
    
    master.NetYield8P = netYeild;
    
}

/*-(double) getIRR:(NSMutableDictionary *)_term :(int)polTerm{
    double guess = 0.0049;
    double inc = 0.0001;
    NSDecimalNumber *num = nil;
    do {
        guess += inc;
        num = (NSDecimalNumber *)[NSDecimalNumber numberWithDouble:0.0];
        for (int i= 0; i <= polTerm; i++) {
            NSDecimalNumber *pnum = (NSDecimalNumber *)[NSDecimalNumber numberWithDouble:(pow(1+guess, i))];
            NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                               decimalNumberHandlerWithRoundingMode:NSRoundUp
                                               scale:6
                                               raiseOnExactness:NO
                                               raiseOnOverflow:NO
                                               raiseOnUnderflow:NO
                                               raiseOnDivideByZero:YES];
            
            num = [num decimalNumberByAdding:[[_term objectForKey:[NSNumber numberWithInt:i]] decimalNumberByDividingBy:pnum withBehavior:roundUp]];
        }
    } while ([num compare:(NSDecimalNumber *)[NSDecimalNumber numberWithDouble:0.0]] == NSOrderedSame);
    
    guess = guess * 100;
    double netYield = pow((1+guess)/100, 12) -1;
    return netYield;
    
}
*/

-(double) getIRRAlternate:(NSMutableDictionary *)_term :(int)polTerm{
    double prevGuess = 0.0005;
    double currGuess = 0.0005;
    float inc = 0.0001;
    BOOL notFound = true;
    double diffGuess = 0.0;
    
    NSDecimalNumber *zeroVal = [[NSDecimalNumber alloc] initWithDouble:0.0];
    NSDecimalNumber *npv = [[NSDecimalNumber alloc] initWithDouble:0.0];
    NSDecimalNumber *cashFlow = [[NSDecimalNumber alloc] initWithDouble:0.0];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:6
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:NO];
    
    do {
        npv = [[NSDecimalNumber alloc] initWithDouble:0.0];
        for (int i=1; i<=polTerm+1; i++) {
            NSDecimalNumber *powerNPV = [[NSDecimalNumber alloc] initWithDouble:pow(1+currGuess, i)];
            
            if(i == polTerm+1)
                cashFlow = [_term objectForKey:[NSNumber numberWithInt:i]];
            else{
                cashFlow = [zeroVal decimalNumberBySubtracting:[_term objectForKey:[NSNumber numberWithInt:i]]];
            }
            
            NSDecimalNumber *divide = [cashFlow decimalNumberByDividingBy:powerNPV withBehavior:roundUp];
            npv = [npv decimalNumberByAdding:divide];
        }
        if([npv compare:[[NSDecimalNumber alloc] initWithDouble:0.0]] == NSOrderedDescending){
            prevGuess = currGuess;
            currGuess = currGuess + inc;
        }else{
            if([npv compare:[[NSDecimalNumber alloc] initWithDouble:0.0]] == NSOrderedAscending){
                diffGuess = round((currGuess - prevGuess) * 1000000.0);
                if((currGuess == 0.0005) || (diffGuess == 1) || (prevGuess == currGuess && currGuess != 0.0005)){
                    notFound = false;
                }else{
                    double minusPoint = (currGuess - prevGuess)/2;
                    minusPoint = [[NSString stringWithFormat:@"%0.6f",minusPoint] doubleValue];
                    currGuess = currGuess - minusPoint;
                    currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
                    double curby100 = [[NSString stringWithFormat:@"%0.2f",currGuess * 1000000] doubleValue];
                    if (fmod(curby100, 1) > 0) {
                        currGuess += 0.000005;
                    }
                    currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
                    inc = ((float)(currGuess - prevGuess)) / 2;
                }
            }
        }
        
        currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
        prevGuess = [[NSString stringWithFormat:@"%0.6f",prevGuess] doubleValue];
        
        if(currGuess == prevGuess){
            notFound = false;
            if ([npv compare:[[NSDecimalNumber alloc] initWithDouble:0.0]] == NSOrderedDescending) {
                currGuess = currGuess - 0.000001;
                currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
            }
        }
        NSLog(@"npv for new prev guess %f",prevGuess);
        NSLog(@"mpv for new curr guess %f",currGuess);
    } while (notFound);
    
    if (prevGuess > currGuess) {
        currGuess = currGuess + 0.000002;
        currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
    }
    
    if (diffGuess == 1 && currGuess != 0.0005) {
        npv = [[NSDecimalNumber alloc] initWithDouble:0.0];
        for (int i=1; i<=polTerm+1; i++) {
            NSDecimalNumber *powerNPV = [[NSDecimalNumber alloc] initWithDouble:pow(1+prevGuess, i)];
            
            if(i == polTerm+1)
                cashFlow = [_term objectForKey:[NSNumber numberWithInt:i]];
            else
                cashFlow = [zeroVal decimalNumberBySubtracting:[_term objectForKey:[NSNumber numberWithInt:i]]];
            
            npv = [npv decimalNumberByAdding:[cashFlow decimalNumberByDividingBy:powerNPV withBehavior:roundUp]];
        }
        NSLog(@"npv for prev guess %f is %@",prevGuess,npv);
    }
    
    double prevGuess7p = [[NSString stringWithFormat:@"%0.7f",currGuess] doubleValue];
    double currGuess7p = [[NSString stringWithFormat:@"%0.7f",prevGuess + 0.0000001] doubleValue];
    
    if([npv compare:[[NSDecimalNumber alloc] initWithDouble:0.0]] == NSOrderedAscending){
        currGuess = prevGuess;
        currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
    }else{
        do {
            npv = [[NSDecimalNumber alloc] initWithDouble:0.0];
            for (int i=1; i<=polTerm+1; i++) {
                NSDecimalNumber *powerNPV = [[NSDecimalNumber alloc] initWithDouble:pow(1+currGuess7p, i)];
                if (i == polTerm+1) {
                    cashFlow = [_term objectForKey:[NSNumber numberWithInt:i]];
                }else{
                    cashFlow = [zeroVal decimalNumberBySubtracting:[_term objectForKey:[NSNumber numberWithInt:i]]];
                }
                npv = [npv decimalNumberByAdding:[cashFlow decimalNumberByDividingBy:powerNPV withBehavior:roundUp]];
            }
            NSLog(@"NPV for curr guess 7p %f is %@",currGuess7p,npv);
            if([npv compare:[[NSDecimalNumber alloc] initWithDouble:0.0]] == NSOrderedAscending){
                prevGuess7p = currGuess7p;
                prevGuess7p = [[NSString stringWithFormat:@"%0.7f",prevGuess7p] doubleValue];
            }else{
                currGuess7p = currGuess7p + 0.0000001;
                currGuess7p = [[NSString stringWithFormat:@"%0.7f",currGuess7p] doubleValue];
            }
            
        } while (currGuess7p != prevGuess7p);
    }
    
    if (currGuess7p != prevGuess7p) {
        currGuess = currGuess * 100;
        currGuess = [[NSString stringWithFormat:@"%0.7f",currGuess] doubleValue];
    }else{
        currGuess = currGuess7p * 100;
        currGuess = [[NSString stringWithFormat:@"%0.6f",currGuess] doubleValue];
    }
    
    //NSLog(@"final value curr guess is %f",currGuess);
    double netYield = pow((1+(currGuess/100)), 12) - 1;
    //NSLog(@"final value net yield is %f",netYield);
    return netYield;
}

/*-(void)roundValueToTwoDigits:(double)val{
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];

	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[formatter setMaximumFractionDigits:2];
	[formatter setRoundingMode: NSNumberFormatterRoundUp];

	NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:val]];
	return

}*/

-(void)calculateChargesForPH6{
    double PAC = 0.0;
    NSMutableDictionary *PremiumAllocationDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *OtherbenefitChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *MortalityChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ApplicableServiceTaxDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *FundMgmtChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueBeforeFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValuePostFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *GuaranteedMaturityAmtDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalFundValueAtEndPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *SurrenderValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *DeathBenefitAtEndOfPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *LoyaltyChargeDict = [[NSMutableDictionary alloc] init];
    
    long lOtherBenefitCharges = 0;
    double dOtherBenefitCharges = 0.0;
    int age = master.sisRequest.InsuredAge;
    NSString *frequency = master.sisRequest.Frequency;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt =(int)master.sisRequest.PremiumPayingTerm;
    long lMortalityCharges=0;
    long lAdminCharge=0.0;
    double dAdminCharge=0.0;
    double PremiumAllocChargesSTAX = 0.0;
    double OtherBenefitSTAX=0.0;
    double AdminChargesSTAX=0.0;
    double PHAccount=0.0;
    double PHInvestmentFee=0.0;
    double netPhAccount = 0.0;
    double MortChargesStax = 0.0;
    double tempNetHolderAcc = 0.0;
    double tempMortalityCharges=0.0;
    double tempFullMortalityCharges=0.0;
    double tempNetHolderInvestment = 0.0;
    double tempPHAccBeforeGrouth = 0.0;
    double tempPHAccInvestGrouth=0.0;
    double tempPHAccHolderer = 0.0;
    double tempPHInvestmentFee = 0.0;
    double PHInvestFeesTax = 0.0;
    double tempPHNetHolderAcc = 0.0;
    double tempAdminChargesSTAX = 0.0;
    double FundGrouthPM = 0.00327373978220;
    long totalFundValueAtEndPolicyYear=0;
    long totalCharges=0.0;
    double loyaltyCharges = 0.0;
    double tempFinalPHNetHolderAcc = 0.0;
    double totRiderCharges = 0.0;
    double TotalRiderCharges=0;
    
    //bean = nil;
    
    for (int cnt = 1; cnt <= polTerm; cnt++) {
        long ApplicableST = 0;
        double TotalST = 0;
        double PHInvestFeeTotal = 0;
        long RegularFundValueBeforeFMC = 0;
        long RegularFundValuePostFMC = 0;
        NSString *totalInvest = NULL;
        double riderCharges = 0.0;
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
        NSString *premAllocCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PREMIUM_ALLOCATION_CHARGES :bean]];
        if(cnt <= ppt){
            PAC = [premAllocCharges doubleValue];
        }else{
            PAC = 0.0;
        }
        
        [PremiumAllocationDict setObject:[NSNumber numberWithDouble:round(PAC)] forKey:[NSNumber numberWithInt:cnt]];
        bean = [self getFormulaBean];
        bean.AmountForServiceTax = PAC;
        bean.CounterVariable = cnt;
        if(cnt <= ppt){
            totalInvest =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TOTAL_INVESTMENT_CHARGES :bean]];
        }
        else {
            totalInvest = @"0";
        }
        PremiumAllocChargesSTAX = [self getServiceTax:PAC];
        TotalST = TotalST + PremiumAllocChargesSTAX;
        
        double investAmt = [totalInvest doubleValue];
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        //bean.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *otherBenCharges =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES :bean]];
        lOtherBenefitCharges = [otherBenCharges longLongValue];
        //NSLog(@"%ld",lOtherBenefitCharges);
        
        //newly added
        [OtherbenefitChargesDict setObject:[NSNumber numberWithLong:lOtherBenefitCharges] forKey:[NSNumber numberWithInt:cnt]];
        
        NSString *otherBenChargesMnthly = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY :bean]];
        dOtherBenefitCharges = [otherBenChargesMnthly doubleValue];
        
        OtherBenefitSTAX = [self getServiceTax:dOtherBenefitCharges];
        TotalST = TotalST + (OtherBenefitSTAX * 12);
        
        //bean = [self getFormulaBean];
        bean.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *AdminCharges= [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ADMIN_CHARGES :bean]];
        dAdminCharge = [AdminCharges doubleValue];
        lAdminCharge = round(dAdminCharge * 12);
        
        //[PolicyAdminChargesDict setObject:[NSNumber numberWithDouble:round(lAdminCharge)] forKey:[NSNumber numberWithInteger:cnt]];
        
        AdminChargesSTAX = [self getServiceTax:dAdminCharge];
        TotalST = TotalST + (AdminChargesSTAX * 12);
        
        double serviceTaxOnInvestFee = 0.0;
        //lMortalityCharges = 0;
        tempFullMortalityCharges = 0.0;
        //riderCharges=0;
        TotalRiderCharges=0;
        
        for(int i=1;i<=12;i++){
            double finalLoyalty = 0;
            double tempHolderNetToInvest = investAmt;
            if(([frequency caseInsensitiveCompare:@"A"] == NSOrderedSame || [frequency caseInsensitiveCompare:@"O"] == NSOrderedSame) && i == 1){
                tempHolderNetToInvest = investAmt;
            }else if([frequency caseInsensitiveCompare:@"S"] == NSOrderedSame && (i == 1 || i == 7)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/2 - PAC/2 - [self getServiceTax:(PAC/2)];
            }else if([frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame && (i == 1 || i == 7 || i == 4 || i == 10)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/4 - PAC/4 - [self getServiceTax:(PAC/4)];
            }else if([frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/12 - PAC/12 - [self getServiceTax:(PAC/12)];
            }else{
                tempHolderNetToInvest = 0.0;
            }
            if(cnt > ppt){
                tempHolderNetToInvest = 0.0;
            }
            
            bean = [self getFormulaBean];
            bean.AmountForServiceTax = tempHolderNetToInvest + tempFinalPHNetHolderAcc;
            bean.CounterVariable = cnt;
            bean.MonthCount = i;
            
            NSString *mortalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MORTALITY_CHARGES_MONTHLY :bean]];
            tempMortalityCharges = [mortalityCharges doubleValue];
            
            tempFullMortalityCharges += tempMortalityCharges;
            riderCharges = [self getRiderCharges:cnt :age :ppt :i];
            
            MortChargesStax = [self getServiceTax:(tempMortalityCharges + riderCharges)];
            TotalRiderCharges = TotalRiderCharges + riderCharges;
            lMortalityCharges = lround(tempFullMortalityCharges);
            //NSLog(@" MOrt charges %ld rider charged %f",lMortalityCharges,riderCharges);
            [MortalityChargesDict setObject:[NSNumber numberWithLong:lMortalityCharges] forKey:[NSNumber numberWithInt:cnt]];
            TotalST = TotalST + MortChargesStax;
            
            tempAdminChargesSTAX = [self getServiceTax:dAdminCharge];
            tempNetHolderInvestment = tempHolderNetToInvest - tempMortalityCharges - MortChargesStax - dAdminCharge - tempAdminChargesSTAX - riderCharges;
            tempPHAccBeforeGrouth = tempNetHolderAcc + tempNetHolderInvestment;
            
            bean.AmountForServiceTax = 0.1;
            tempPHAccInvestGrouth = tempPHAccBeforeGrouth * FundGrouthPM;
            
            tempPHAccHolderer = tempPHAccBeforeGrouth + tempPHAccInvestGrouth;
            
            bean.AmountForServiceTax = tempPHAccHolderer;
            
            NSString *tempPHInvestFeesValue = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PH_INVESTMENT_FEE :bean]];
            tempPHInvestmentFee = [tempPHInvestFeesValue doubleValue];
            
            PHInvestFeesTax = [self getServiceTax:tempPHInvestmentFee];
            TotalST = TotalST + PHInvestFeesTax;
            
            tempPHNetHolderAcc = tempPHAccHolderer - tempPHInvestmentFee - PHInvestFeesTax;
            
            double InvestmentFeesTax = 0.0;
            PHAccount = tempPHAccHolderer;
            PHInvestmentFee = tempPHInvestmentFee;
            PHInvestFeeTotal = PHInvestFeeTotal + PHInvestmentFee;
            
            InvestmentFeesTax = PHInvestFeesTax;
            serviceTaxOnInvestFee = serviceTaxOnInvestFee + InvestmentFeesTax;
            netPhAccount = PHAccount - PHInvestmentFee - InvestmentFeesTax;
            RegularFundValuePostFMC=round(tempPHNetHolderAcc);
            
            if(i == 12){
                [OtherbenefitChargesDict setObject:[NSNumber numberWithDouble:round(TotalRiderCharges)] forKey:[NSNumber numberWithInt:cnt]];
                bean.AmountForServiceTax=tempPHNetHolderAcc;
                //formulaBeanObj.AmountForServiceTax = tempPHNetHolderAcc;
                NSString *tempLoyalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LC :bean]];
                loyaltyCharges = [tempLoyalityCharges doubleValue];
                
                finalLoyalty = loyaltyCharges;
                [LoyaltyChargeDict setObject:[NSNumber numberWithDouble:round(finalLoyalty)] forKey:[NSNumber numberWithInt:cnt]];
                
                RegularFundValueBeforeFMC = round(netPhAccount + PHInvestFeeTotal + serviceTaxOnInvestFee);
                [RegularFundValueBeforeFMCDict setObject:[NSNumber numberWithDouble:RegularFundValueBeforeFMC] forKey:[NSNumber numberWithInt:cnt]];
                
                long guaranteedMaturityAmt = [self getGuaranteedMaturityAddition:cnt :netPhAccount];
                [GuaranteedMaturityAmtDict setObject:[NSNumber numberWithLong:guaranteedMaturityAmt] forKey:[NSNumber numberWithInt:cnt]];
                
                //RegularFundValuePostFMC = round(tempPHNetHolderAcc);
                [RegularFundValuePostFMCDict setObject:[NSNumber numberWithDouble:RegularFundValuePostFMC] forKey:[NSNumber numberWithInt:cnt]];
            }
            tempFinalPHNetHolderAcc = tempPHNetHolderAcc + finalLoyalty;
            tempNetHolderAcc = tempFinalPHNetHolderAcc;
            totalFundValueAtEndPolicyYear = lround(tempNetHolderAcc);
            
            //changes 5 aug
            long long surrenderValue = [self getSurrenderValue:cnt:tempFinalPHNetHolderAcc];
            //changes 5 aug long to long long.
            [SurrenderValueDict setObject:[NSNumber numberWithLongLong:surrenderValue] forKey:[NSNumber numberWithInt:cnt]];
            
        }
        age++;
        if(cnt <= ppt){
            double tc = PAC+TotalRiderCharges+lAdminCharge+tempFullMortalityCharges;
            totalCharges = lround(tc);
        }else{
            totalCharges = lround(TotalRiderCharges+lAdminCharge+tempFullMortalityCharges);
        }
        
        [TotalChargeDict setObject:[NSNumber numberWithLong:totalCharges] forKey:[NSNumber numberWithInt:cnt]];
        
        ApplicableST = lround(TotalST);
        [ApplicableServiceTaxDict setObject:[NSNumber numberWithLong:ApplicableST] forKey:[NSNumber numberWithInt:cnt]];
        [FundMgmtChargeDict setObject:[NSNumber numberWithLong:lround(PHInvestFeeTotal)] forKey:[NSNumber numberWithInt:cnt]];
        
        [TotalFundValueAtEndPolicyYrDict setObject:[NSNumber numberWithLong:totalFundValueAtEndPolicyYear] forKey:[NSNumber numberWithInt:cnt]];
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalFundValueAtEndPolicyYear;
        //changes 27th oct
        //issue was in iwealth
        bean.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *DBEPstr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR :bean]];
        
        double dDBEP = [DBEPstr doubleValue];
        
        [DeathBenefitAtEndOfPolicyYrDict setObject:[NSNumber numberWithDouble:round(dDBEP)] forKey:[NSNumber numberWithInt:cnt]];
    }
    
    master.PremiumAllocationCharges = PremiumAllocationDict;
    master.TotalChargesForPH6 = TotalChargeDict;
    master.MortalityChargesPH6 = MortalityChargesDict;
    master.ApplicableSTPH6 = ApplicableServiceTaxDict;
    master.LoyalityChargeForPh6 = LoyaltyChargeDict;
    master.FundMgmtChargePH6 = FundMgmtChargeDict;
    master.RegularFundValuePH6 = RegularFundValueDict;
    master.RegularFundValueBeforeFMCPH6 = RegularFundValueBeforeFMCDict;
    master.RegularFundValuePostFMCPH6 = RegularFundValuePostFMCDict;
    master.GuaranteedMaturityAdditionPH6 = GuaranteedMaturityAmtDict;
    master.TotalFundValueAtEndPolicyYrPH6 = TotalFundValueAtEndPolicyYrDict;
    master.SurrenderValuePH6 = SurrenderValueDict;
    master.DeathBenefitAtEndOfPolicyYearPH6 = DeathBenefitAtEndOfPolicyYrDict;
    
}

-(void)calculateAlternateChargesSmartPH10{
    double PAC = 0.0;
    NSMutableDictionary *PremiumAllocationDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *InvestmentAmtDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *OtherbenefitChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *MortalityChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *PolicyAdminChargesDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ApplicableServiceTaxDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *FundMgmtChargeDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValueBeforeFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *RegularFundValuePostFMCDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *GuaranteedMaturityAmtDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *TotalFundValueAtEndPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *SurrenderValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *DeathBenefitAtEndOfPolicyYrDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *CommissionPayableDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *LoyaltyChargeDict = [[NSMutableDictionary alloc] init];
    
    long lOtherBenefitCharges = 0;
    double dOtherBenefitCharges = 0.0;
    int age = master.sisRequest.InsuredAge;
    NSString *frequency = master.sisRequest.Frequency;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt =(int)master.sisRequest.PremiumPayingTerm;
    
    long lMortalityCharges=0;
    long lAdminCharge=0.0;
    double dAdminCharge=0.0;
    double PremiumAllocChargesSTAX = 0.0;
    double OtherBenefitSTAX=0.0;
    double AdminChargesSTAX=0.0;
    double PHAccount=0.0;
    double PHInvestmentFee=0.0;
    double netPhAccount = 0.0;
    double MortChargesStax = 0.0;
    double tempNetHolderAcc = 0.0;
    double tempMortalityCharges=0.0;
    double tempFullMortalityCharges=0.0;
    double tempNetHolderInvestment = 0.0;
    double tempPHAccBeforeGrouth = 0.0;
    double tempPHAccInvestGrouth=0.0;
    double tempPHAccHolderer = 0.0;
    double tempPHInvestmentFee = 0.0;
    double PHInvestFeesTax = 0.0;
    double tempPHNetHolderAcc = 0.0;
    double tempAdminChargesSTAX = 0.0;
    double FundGrouthPM = 0.00643403011;
    long totalFundValueAtEndPolicyYear=0;
    double totalCharges=0.0;
    double tempFinalPHNetHolderAcc = 0.0;
    double loyaltyCharges = 0.0;
    double riderCharges = 0.0;
    double AmtInDebtFundEnd = 0.0;
    double debtFundProp = 0.0;
    double equityFMC = 0.0;
    double debtFMC = 0.0;
    double amtDebtFundBeforeTransfer = 0.0;
    double FMCDebtFund = 0.0;
    
    NSString *withdrawalAmt = @"";
    NSString *withdrawalStartYear = @"";
    NSString *withdrawalEndYear = @"";
    NSString *withDrawalAmtYear = @"";
    
    if(master.sisRequest.FixWithDAmt != NULL && master.sisRequest.FixWithDAmt.length != 0 ){
        withdrawalAmt = master.sisRequest.FixWithDAmt;
        withdrawalStartYear = master.sisRequest.FixWithDstYr;
        withdrawalEndYear = master.sisRequest.FixWithDEndYr;
    }else if(master.sisRequest.VarWithDAmtYr != NULL && master.sisRequest.VarWithDAmtYr.length != 0){
        withDrawalAmtYear = master.sisRequest.VarWithDAmtYr;
        withDrawalAmtYear = [NSString stringWithFormat:@"-%@",[withDrawalAmtYear substringWithRange:NSMakeRange(0, withDrawalAmtYear.length -1)]];
    }
    
    double partialWithdrawal = 0.0;
    double fullPartialWithdrawal = 0.0;
    double completePartialWithdrawal = 0.0;
    double mainPartialWithdrawal = 0.0;
    
    NSMutableDictionary *withdrawalAmtSM10Dict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *withdrawalAmtDoubleSM10Dict = [[NSMutableDictionary alloc] init];
    double yearlyPartialWithDrawal = 0.0;
    
    double dFMC = 0.0;
    
    FormulaBean *formulaBeanObj = nil;
    for(int cnt=0;cnt<=polTerm;cnt++){
        long ApplicableST = 0;
        double TotalST = 0;
        double PHInvestFeeTotal = 0.0;
        long RegularFundValueBeforeFMC = 0;
        long RegularFundValuePostFMC = 0;
        NSString *totalInvest = NULL;
        double TotalRiderCharges = 0.0;
        
        formulaBeanObj = [self getFormulaBean];
        formulaBeanObj.CounterVariable = cnt;
        
        //        double FMC = [DataAccessClass get]
        
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc] init];
        NSString *premAllocCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_PREMIUM_ALLOCATION_CHARGES :formulaBeanObj]];
        if(cnt <= ppt){
            PAC = [premAllocCharges doubleValue];
        }else{
            PAC = 0.0;
        }
        
        [PremiumAllocationDict setObject:[NSNumber numberWithDouble:round(PAC)] forKey:[NSNumber numberWithInt:cnt]];
        formulaBeanObj = [self getFormulaBean];
        formulaBeanObj.AmountForServiceTax = PAC;
        if(cnt <= ppt){
            totalInvest =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TOTAL_INVESTMENT_CHARGES :formulaBeanObj]];
        }
        else {
            totalInvest = @"0";
        }
        PremiumAllocChargesSTAX = [self getServiceTax:PAC];
        TotalST = TotalST + PremiumAllocChargesSTAX;
        
        double investAmt = [totalInvest doubleValue];
        formulaBeanObj = [self getFormulaBean];
        formulaBeanObj.CounterVariable = cnt;
        formulaBeanObj.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *strCommPayable =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_COMMISSION_PAYABLE :formulaBeanObj]];
        double comm = [strCommPayable doubleValue];
        
        if(cnt <= ppt){
            [InvestmentAmtDict setObject:[NSNumber numberWithDouble:round(investAmt)] forKey:[NSNumber numberWithInt:cnt]];
            [CommissionPayableDict setObject:[NSNumber numberWithDouble:round(comm)] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [InvestmentAmtDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
            [CommissionPayableDict setObject:[NSNumber numberWithDouble:0] forKey:[NSNumber numberWithInt:cnt]];
        }
        
        
        formulaBeanObj = [self getFormulaBean];
        formulaBeanObj.CounterVariable = cnt;
        
        NSString *otherBenCharges =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES :formulaBeanObj]];
        lOtherBenefitCharges = [otherBenCharges longLongValue];
        //NSLog(@"other benefit charges %ld",lOtherBenefitCharges);
        NSString *otherBenChargesMnthly = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_OTHER_INBUILT_BENEFIT_CHARGES_MONTHLY :formulaBeanObj]];
        dOtherBenefitCharges = [otherBenChargesMnthly doubleValue];
        
        OtherBenefitSTAX = [self getServiceTax:dOtherBenefitCharges];
        TotalST = TotalST + (OtherBenefitSTAX * 12);
        
        formulaBeanObj = [self getFormulaBean];
        formulaBeanObj.premium = master.sisRequest.BasePremiumAnnual;
        
        NSString *AdminCharges= [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ADMIN_CHARGES :formulaBeanObj]];
        dAdminCharge = [AdminCharges doubleValue];
        lAdminCharge = dAdminCharge * 12;
        
        [PolicyAdminChargesDict setObject:[NSNumber numberWithDouble:round(lAdminCharge)] forKey:[NSNumber numberWithInteger:cnt]];
        
        AdminChargesSTAX = [self getServiceTax:dAdminCharge];
        TotalST = TotalST + (AdminChargesSTAX * 12);
        
        double serviceTaxOnInvestFee = 0.0;
        //lMortalityCharges = 0;
        fullPartialWithdrawal = 0.0;
        completePartialWithdrawal = 0.0;
        yearlyPartialWithDrawal = 0.0;
        tempFullMortalityCharges = 0.0;
        
        for(int i=0;i<=12;i++){
            double finalLoyalty = 0;
            double tempHolderNetToInvest;
            if(([frequency caseInsensitiveCompare:@"A"] == NSOrderedSame || [frequency caseInsensitiveCompare:@"O"] == NSOrderedSame) && i == 1){
                tempHolderNetToInvest = investAmt;
            }else if([frequency caseInsensitiveCompare:@"S"] == NSOrderedSame && (i == 1 || i == 7)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/2 - PAC/2 - [self getServiceTax:(PAC/2)];
            }else if([frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame && (i == 1 || i == 7 || i == 4 || i == 10)){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/4 - PAC/4 - [self getServiceTax:(PAC/4)];
            }else if([frequency caseInsensitiveCompare:@"M"] == NSOrderedSame){
                tempHolderNetToInvest = master.BaseAnnualizedPremium/12 - PAC/12 - [self getServiceTax:(PAC/12)];
            }else{
                tempHolderNetToInvest = 0.0;
            }
            if(cnt > ppt){
                tempHolderNetToInvest = 0.0;
            }
            
            if(([frequency caseInsensitiveCompare:@"A"]== NSOrderedSame && i!=1) ||
               ([frequency caseInsensitiveCompare:@"S"]== NSOrderedSame && (i!=1 && i!=7) ) ||
               ([frequency caseInsensitiveCompare:@"Q"]== NSOrderedSame && (i!=1 && i!=7 && i!=4 && i!=10))){
                partialWithdrawal = 0.0;
            }else{
                if(withdrawalAmt.length != 0){
                    if(cnt>= [withdrawalStartYear intValue] && cnt <= [withdrawalEndYear intValue]){
                        partialWithdrawal = [withdrawalAmt doubleValue];
                        yearlyPartialWithDrawal = partialWithdrawal;
                        completePartialWithdrawal += partialWithdrawal;
                    }else{
                        partialWithdrawal = 0.0;
                    }
                }else if(withDrawalAmtYear.length != 0){
                    NSArray *withdrawArr = [withDrawalAmtYear componentsSeparatedByString:@"-"];
                    for (int l=0; l<withdrawArr.count; l++) {
                        NSString *comp = [[withdrawArr objectAtIndex:l] substringWithRange:NSMakeRange([[withdrawArr objectAtIndex:l] indexOfObject:@","]+1, [[withdrawArr objectAtIndex:l] length])];
                        if ([[withdrawArr objectAtIndex:l] isEqualToString:comp] == NSOrderedSame) {
                            partialWithdrawal = [comp doubleValue];
                            yearlyPartialWithDrawal = partialWithdrawal;
                            completePartialWithdrawal += partialWithdrawal;
                            l = (int)withdrawArr.count;
                        }else{
                            partialWithdrawal = 0.0;
                        }
                    }
                }
            }
            
            formulaBeanObj.AmountForServiceTax = tempHolderNetToInvest + tempFinalPHNetHolderAcc;
            formulaBeanObj.CounterVariable = cnt;
            formulaBeanObj.MonthCount = i;
            formulaBeanObj.WithdrawalAmount = mainPartialWithdrawal;
            
            NSString *mortalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MORTALITY_CHARGES_MONTHLY :formulaBeanObj]];
            tempMortalityCharges = [mortalityCharges doubleValue];
            
            tempFullMortalityCharges += tempMortalityCharges;
            riderCharges = [self getRiderCharges:cnt :age :ppt :i];
            
            MortChargesStax = [self getServiceTax:(tempMortalityCharges + riderCharges)];
            TotalRiderCharges = TotalRiderCharges + riderCharges;
            lMortalityCharges = round(tempFullMortalityCharges);
            [MortalityChargesDict setObject:[NSNumber numberWithDouble:lMortalityCharges] forKey:[NSNumber numberWithInt:cnt]];
            TotalST = TotalST + MortChargesStax;
            
            tempAdminChargesSTAX = [self getServiceTax:dAdminCharge];
            tempNetHolderInvestment = tempNetHolderInvestment - tempMortalityCharges - MortChargesStax - dAdminCharge - tempAdminChargesSTAX - riderCharges;
            tempPHAccBeforeGrouth = tempNetHolderAcc + tempNetHolderInvestment;
            
            formulaBeanObj.AmountForServiceTax = 0.1;
            tempPHAccInvestGrouth = tempPHAccBeforeGrouth * FundGrouthPM;
            
            tempPHAccHolderer = tempPHAccBeforeGrouth + tempPHAccInvestGrouth;
            
            double propTransferDtoE = (i == 12?1.0:(1.0/(13.0 -(i%12))));
            if(cnt == 1 && i == 1){
                amtDebtFundBeforeTransfer = AmtInDebtFundEnd - FMCDebtFund +tempHolderNetToInvest;
            }else{
                amtDebtFundBeforeTransfer = AmtInDebtFundEnd - FMCDebtFund - (AmtInDebtFundEnd * dFMC / 12*0.1236)+tempHolderNetToInvest;
            }
            double transferFromDebtFund = amtDebtFundBeforeTransfer * propTransferDtoE;
            double amtDebtFundStartPrem = (i == 12?0.0 :(transferFromDebtFund *(12.0 - (i%12))));
            AmtInDebtFundEnd = (tempPHAccHolderer / tempFinalPHNetHolderAcc+tempHolderNetToInvest)*amtDebtFundStartPrem;
            
            debtFundProp = AmtInDebtFundEnd/tempPHAccHolderer;
            //NSLog(@"the debt in fun prop is %f",debtFundProp);
            if(master.sisRequest.SmartDebtFund != NULL){
                debtFMC = master.sisRequest.SmartDebtFundFMC;
                equityFMC = master.sisRequest.SmartEquityFundFMC;
            }
            FMCDebtFund =AmtInDebtFundEnd *debtFMC/1200;
            double FMCEquityFund = (tempPHAccHolderer - AmtInDebtFundEnd)*equityFMC/1200;
            
            tempPHInvestmentFee = FMCEquityFund + FMCDebtFund;
            PHInvestFeesTax = [self getServiceTax:tempPHInvestmentFee];
            TotalST = TotalST + PHInvestFeesTax;
            
            formulaBeanObj.AmountForServiceTax = tempPHAccHolderer;
            
            tempPHNetHolderAcc = tempPHAccHolderer - tempPHInvestmentFee - PHInvestFeesTax;
            
            double InvestmentFeesTax = 0.0;
            PHAccount = tempPHAccHolderer;
            PHInvestmentFee = tempPHInvestmentFee;
            PHInvestFeeTotal = PHInvestFeeTotal + PHInvestmentFee;
            
            InvestmentFeesTax = PHInvestFeesTax;
            serviceTaxOnInvestFee = serviceTaxOnInvestFee + InvestmentFeesTax;
            netPhAccount = PHAccount - PHInvestmentFee - InvestmentFeesTax;
            
            if(i == 12){
                [OtherbenefitChargesDict setObject:[NSNumber numberWithDouble:round(TotalRiderCharges)] forKey:[NSNumber numberWithInt:cnt]];
                formulaBeanObj.AmountForServiceTax = tempPHNetHolderAcc;
                NSString *tempLoyalityCharges = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LC :formulaBeanObj]];
                loyaltyCharges = [tempLoyalityCharges doubleValue];
                
                finalLoyalty = loyaltyCharges;
                [LoyaltyChargeDict setObject:[NSNumber numberWithDouble:round(finalLoyalty)] forKey:[NSNumber numberWithInt:cnt]];
                
                [RegularFundValueDict setObject:[NSNumber numberWithDouble:round(netPhAccount)] forKey:[NSNumber numberWithInt:cnt]];
                
                RegularFundValueBeforeFMC = round(netPhAccount + PHInvestFeeTotal + serviceTaxOnInvestFee);
                [RegularFundValueBeforeFMCDict setObject:[NSNumber numberWithDouble:RegularFundValueBeforeFMC] forKey:[NSNumber numberWithInt:cnt]];
                
                long guaranteedMaturityAmt = [self getGuaranteedMaturityAddition:cnt :netPhAccount];
                [GuaranteedMaturityAmtDict setObject:[NSNumber numberWithLong:guaranteedMaturityAmt] forKey:[NSNumber numberWithInt:cnt]];
                
                RegularFundValuePostFMC = round(tempPHNetHolderAcc);
                [RegularFundValuePostFMCDict setObject:[NSNumber numberWithDouble:RegularFundValuePostFMC] forKey:[NSNumber numberWithInt:cnt]];
            }
            if ([frequency isEqualToString:@"O"] || [frequency isEqualToString:@"M"]) {
                partialWithdrawal = partialWithdrawal / 12;
            }else if ([frequency isEqualToString:@"S"]) {
                partialWithdrawal = partialWithdrawal / 2;
            }else if ([frequency isEqualToString:@"Q"]) {
                partialWithdrawal = partialWithdrawal / 4;
            }
            fullPartialWithdrawal += partialWithdrawal;
            
            if(i == 1){
                [withdrawalAmtSM10Dict setObject:[NSNumber numberWithDouble:round(yearlyPartialWithDrawal)] forKey:[NSNumber numberWithInt:cnt]];
            }
            int currMnth = (cnt-1)*12+i;
            if(([frequency caseInsensitiveCompare:@"A"] == NSOrderedSame && i!=1) || ([frequency caseInsensitiveCompare:@"S"] == NSOrderedSame && (i!=1 && i!=7)) || ([frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame && (i!=1 && i!=4 && i!=7 && i!=10))){
                [withdrawalAmtDoubleSM10Dict setObject:[NSNumber numberWithDouble:0.0] forKey:[NSNumber numberWithInt:currMnth]];
            }else{
                [withdrawalAmtDoubleSM10Dict setObject:[NSNumber numberWithDouble:partialWithdrawal] forKey:[NSNumber numberWithInt:currMnth]];
            }
            
            mainPartialWithdrawal = 0.0;
            if (master.sisRequest.InsuredAge + cnt -1 >= 60) {
                for (int k=currMnth; k>0; k--) {
                    int inAge = (k%12!=0 ? k/12+1:k/12);
                    if (([withdrawalAmtDoubleSM10Dict objectForKey:[NSNumber numberWithInt:k]] != NULL) &&
                        (master.sisRequest.InsuredAge + inAge - 1 >= 58)) {
                        mainPartialWithdrawal += [[withdrawalAmtDoubleSM10Dict objectForKey:[NSNumber numberWithInt:k]] doubleValue];
                    }
                }
            }else{
                for (int k=currMnth; k>=currMnth-23; k--) {
                    if (([withdrawalAmtDoubleSM10Dict objectForKey:[NSNumber numberWithInt:k]] != NULL) &&
                        (k>=1)) {
                        mainPartialWithdrawal += [[withdrawalAmtDoubleSM10Dict objectForKey:[NSNumber numberWithInt:k]] doubleValue];
                    }
                }
            }
            tempFinalPHNetHolderAcc = tempPHNetHolderAcc + finalLoyalty;
            tempNetHolderAcc = tempFinalPHNetHolderAcc;
            totalFundValueAtEndPolicyYear = round(tempNetHolderAcc);
            RegularFundValuePostFMC = round(tempPHNetHolderAcc);
            [RegularFundValuePostFMCDict setObject:[NSNumber numberWithLong:RegularFundValuePostFMC] forKey:[NSNumber numberWithLong:cnt]];
            
            double surrenderCharges = [self getSurrenderValuePH10_AS3:cnt :tempFinalPHNetHolderAcc];
            double totSurCharge_SMART10 = tempFinalPHNetHolderAcc - surrenderCharges;
            [SurrenderValueDict setObject:[NSNumber numberWithDouble:totSurCharge_SMART10] forKey:[NSNumber numberWithInt:cnt]];
            
            age++;
            if(cnt <= ppt){
                double tc = PAC+TotalRiderCharges+lAdminCharge+tempFullMortalityCharges;
                totalCharges = round(tc);
            }else{
                totalCharges = round(TotalRiderCharges+lAdminCharge+tempFullMortalityCharges);
            }
            
            [TotalChargeDict setObject:[NSNumber numberWithDouble:round(totalCharges)] forKey:[NSNumber numberWithInt:cnt]];
            
            ApplicableST = round(TotalST);
            [ApplicableServiceTaxDict setObject:[NSNumber numberWithLong:ApplicableST] forKey:[NSNumber numberWithInt:cnt]];
            [FundMgmtChargeDict setObject:[NSNumber numberWithDouble:round(PHInvestFeeTotal)] forKey:[NSNumber numberWithInt:cnt]];
            
            [TotalFundValueAtEndPolicyYrDict setObject:[NSNumber numberWithLong:totalFundValueAtEndPolicyYear] forKey:[NSNumber numberWithInt:cnt]];
            
            formulaBeanObj = [self getFormulaBean];
            formulaBeanObj.CounterVariable = cnt;
            formulaBeanObj.AmountForServiceTax = totalFundValueAtEndPolicyYear;
            
            NSString *DBEPstr = [NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan : CALCULATE_DEATH_BENEFIT_AT_END_OF_POLICY_YEAR :formulaBeanObj]];
            
            double dDBEP = [DBEPstr doubleValue];
            
            [DeathBenefitAtEndOfPolicyYrDict setObject:[NSNumber numberWithDouble:round(dDBEP)] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    //master.AmountForInvest = InvestmentAmtDict;
    //master.PremiumAllocationCharges = PremiumAllocationDict;
    //master.OtherBenefitCharges = OtherbenefitChargesDict;
    master.MortalityCharges_SMART10 = MortalityChargesDict;
    //master.PolicyAdminCharges = PolicyAdminChargesDict;
    master.ApplicableServiceTaxPH10_SMART10 = ApplicableServiceTaxDict;
    master.TotalChargeForPh10_SMART10 = TotalChargeDict;
    master.LoyalityChargeForPh10_SMART10 = LoyaltyChargeDict;
    master.FundManagementChargePH10_SMART10 = FundMgmtChargeDict;
    master.RegularFundValuePH10_SMART10 = RegularFundValueDict;
    master.RegularFundValueBeforeFMCPH10_SMART10 = RegularFundValueBeforeFMCDict;
    master.RegularFundValuePostFMCPH10_SMART10 = RegularFundValuePostFMCDict;
    master.GuaranteedMaturityAdditionPH10_SMART10 = GuaranteedMaturityAmtDict;
    //master.tot = TotalFundValueAtEndPolicyYrDict;
    master.SurrenderValuePH10_SMART10 = SurrenderValueDict;
    master.TotalDeathBenefitPH10_SMART10 = DeathBenefitAtEndOfPolicyYrDict;
    //master.CommissionPayable = CommissionPayableDict;
}

#pragma mark -- Methods
-(NSMutableDictionary *) getLifeCoverage{
    NSMutableDictionary *LifeCoverageMapDict = [[NSMutableDictionary alloc] init];
    long LifeCoverage = 0;
    double LifeCoverage_IW = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long lGAIValue = 0;
    long lGaiVal = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        lGaiVal = [self calculateGAI:cnt];
        lGAIValue += lGaiVal;
        bean.AmountForServiceTax = lGAIValue;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *LifeCovValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LIFE_COVERAGE :bean]];
        
        
        //Added by Amruta for InstaWealth on 14/9/15
        if ([master.sisRequest.BasePlan hasPrefix:@"IWV1N"]) {
            LifeCoverage_IW = [LifeCovValueStr doubleValue];
            LifeCoverage = round(LifeCoverage_IW);
        }
        LifeCoverage = [LifeCovValueStr longLongValue];
        [LifeCoverageMapDict setObject:[NSNumber numberWithLong:LifeCoverage] forKey:[NSNumber numberWithInt:cnt]];
    }
    //NSLog(@"The Life coverage %@",LifeCoverageMapDict);
    return LifeCoverageMapDict;
}

//GIP
-(NSMutableDictionary *)getLifeCoverage_GIP{
    NSMutableDictionary *lifeCoverageDict = [[NSMutableDictionary alloc] init];
    long LifeCoverage = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double NPRVal = 0;
    NPRVal = [self getTotalGaiSumGIP];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        //GaiValue = [self calculateGAI:polTerm];
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = NPRVal;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *lifeCoverageValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LIFE_COVERAGE :bean]];
        
        LifeCoverage = [lifeCoverageValueStr longLongValue];
        
        [lifeCoverageDict setObject:[NSNumber numberWithLong:LifeCoverage] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return lifeCoverageDict;
}

-(NSMutableDictionary *) getMaturityValue{
    NSMutableDictionary *HVDict = [[NSMutableDictionary alloc] init];
    long maturityValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long GAIVAl = 0;
    long GAIValue = 0;
    //27th may : deleted the (* 0.55 and round) calc for MLG and MLGP in DB.
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        //Addded by Amruta
        GAIVAl = [self calculateGAI:cnt];
        GAIValue = GAIValue + GAIVAl;
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            bean.AmountForServiceTax = GAIValue;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            maturityValue = [maturityValueStr longLongValue];
            [HVDict setObject:[NSNumber numberWithLong:maturityValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [HVDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    //NSLog(@"the maturity value %@",HVDict);
    return HVDict;
}

-(NSMutableDictionary *) getGuaranteedAnnualIncome{
    NSMutableDictionary *GAIDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]) {
            long GAIValue = [self calculateGAI:cnt];
            [GAIDict setObject:[NSNumber numberWithLong:GAIValue] forKey:[NSNumber numberWithInt:cnt]];
        }else if(cnt > master.sisRequest.PremiumPayingTerm){
            long GAIValue = [self calculateGAI:cnt];
            [GAIDict setObject:[NSNumber numberWithLong:GAIValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [GAIDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    //NSLog(@"Guaranteed annual income %@",GAIDict);
    return GAIDict;
}

//Added for GIP
-(NSMutableDictionary *) getGuaranteedAnnualIncome_GIP{
    NSMutableDictionary *GAIDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int incomeTerm = master.sisRequest.incomeBooster;
    double GAIValue = 0;
    
    for (int cnt = 1; cnt<=polTerm+incomeTerm; cnt++) {
        GAIValue = [self calculateGAIGIP:cnt andWith:GAIValue];
        [GAIDict setObject:[NSNumber numberWithLong:round(GAIValue)] forKey:[NSNumber numberWithInt:cnt]];
    }
    //NSLog(@"Guaranteed annual income %@",GAIDict);
    return GAIDict;
}

-(long) getTotalGAI_MLGP{
    long ltotalGAI = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]) {
            long GAIValue = [self calculateGAI:cnt];
            ltotalGAI += GAIValue;
        }
    }
    return ltotalGAI;
}

-(NSMutableDictionary *)getSurrenderBenefit{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double totalGaiSum = [self getTotalGaiSum];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalGaiSum;
        
        //if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            //bean.premium = master.DiscountedBasePremium;
        //}
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT :bean]];
        
        surrBenefit = [surrBenValueStr longLongValue];
		//changes 13feb remove sec7 check
        if(cnt == polTerm && ![master.sisRequest.BasePlan containsString:@"SRP"] && ![master.sisRequest.BasePlan containsString:@"SRL10"]){
            surrBenefit = 0.0;
        }
        
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

//GIP
-(NSMutableDictionary *)getSurrenderBenefit_GIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double sumAssuredMaturity = [self getTotalGaiSumGIP];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = sumAssuredMaturity;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT :bean]];
        
        surrBenefit = [surrBenValueStr longLongValue];
        
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

-(double)getTotalGaiSum{
    double TotalGaiSum = 0.0;
    double GAIValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if(cnt > master.sisRequest.PremiumPayingTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *GAIValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_ANNUAL_INCOME_D :bean]];
            
            GAIValue = [GAIValueStr doubleValue];
            TotalGaiSum = TotalGaiSum + GAIValue;
        }
    }
    return TotalGaiSum;
}

//GIP getSumAssuredMatGIP
-(double)getTotalGaiSumGIP{
    double TotalGaiSum = 0.0;
    double GAIValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int incomeBooster = master.sisRequest.incomeBooster;
    
    for (int cnt = polTerm+1; cnt<=polTerm+incomeBooster; cnt++) {
        if(cnt > master.sisRequest.PremiumPayingTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            if(GAIValue == 0)
                bean.AmountForServiceTax = master.sisRequest.Sumassured;
            else
                bean.AmountForServiceTax = GAIValue;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *GAIValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_ANNUAL_INCOME_F :bean]];
            
            GAIValue = [GAIValueStr doubleValue];
            TotalGaiSum = TotalGaiSum + (GAIValue / (pow((1+(7.5/100.0)),cnt-polTerm)));
        }
    }
    return TotalGaiSum;
}


-(double)getTotalGaiSumMLS{
    double TotalGaiSum = 0.0;
    double GAIValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if(cnt > master.sisRequest.PremiumPayingTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *GAIValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_ANNUAL_INCOME :bean]];
            
            GAIValue = [GAIValueStr doubleValue];
            TotalGaiSum = TotalGaiSum + GAIValue;
        }
    }
    return TotalGaiSum;
}

-(long )calculateGAI:(int )polYear{
    
    long gaiValue = 0;
    FormulaBean *formulaBeanObj=[self getFormulaBean];
    [formulaBeanObj setCounterVariable:polYear];
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    NSString *gaiValueValueStr =(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_GUARANTEED_ANNUAL_INCOME :formulaBeanObj];
    if(gaiValueValueStr != nil)
    gaiValue = [gaiValueValueStr longLongValue];
    return gaiValue;
    
}

//Added for GIP
-(double )calculateGAIGIP:(int)polYear andWith:(double)GAIVal{
    double GAIValue = 0;
    FormulaBean *formulaBeanObj=[self getFormulaBean];
    [formulaBeanObj setCounterVariable:polYear];
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    if(GAIVal == 0)
        bean.AmountForServiceTax = master.sisRequest.Sumassured;
    else
        bean.AmountForServiceTax = GAIVal;
    NSString *gaiValueValueStr =[NSString stringWithFormat:@"%@ ",(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_GUARANTEED_ANNUAL_INCOME_F :formulaBeanObj]];
    if(gaiValueValueStr != nil)
        GAIValue = [gaiValueValueStr doubleValue];
    return GAIValue;
    
}

-(long) getSurrenderValuePH10_AS3:(int)term :(double)netPhAccount{
    double dSurrenderValue = 0;
    bean = [self getFormulaBean];
    bean.NetPHAmtForLoyalBonus = netPhAccount;
    bean.CounterVariable = term;
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *surVal = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_CHARGE_AS3 :bean]];
    dSurrenderValue = [surVal doubleValue];
    
    return dSurrenderValue;
}

-(NSMutableDictionary *)getGuaranteedSurrValue{
    NSMutableDictionary *GSBDict = [[NSMutableDictionary alloc] init];
    double guarSurrValue = 0;
    double guranteedSurrenderVal = 0;
    long totalGAIval = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        //Added by Amruta on 26/8/15 for MBP
        if ([master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] && cnt!=polTerm) {
            long GAIValue = [self calculateGAI:cnt];
            totalGAIval = totalGAIval + GAIValue;
        }
        
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalGAIval;
        bean.AGAI = totalGAIval;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *guarSurrValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE :bean]];
        
        guarSurrValue = [guarSurrValueStr doubleValue];
        
        //Added by Amruta on 20/8/2015 for FG
        //added on 31 dec for freedom
        if (([master.sisRequest.BasePlan isEqualToString:@"FGV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"FR7V1P1"]
             ||[master.sisRequest.BasePlan isEqualToString:@"FR10V1P1"] || [master.sisRequest.BasePlan isEqualToString:@"FR15V1P1"]
             || [master.sisRequest.BasePlan isEqualToString:@"FR7V1P2"] || [master.sisRequest.BasePlan isEqualToString:@"FR10V1P2"] ||
             [master.sisRequest.BasePlan isEqualToString:@"FR15V1P2"] || [master.sisRequest.BasePlan isEqualToString:@"GKIDV1P1"])  && cnt==polTerm) {
            guarSurrValue=0;
        }
        [GSBDict setObject:[NSNumber numberWithLong:roundl(guarSurrValue)] forKey:[NSNumber numberWithInt:cnt]];
    }
    return GSBDict;
}

//GIP
-(NSMutableDictionary *)getGuaranteedSurrValue_LastYear{
    NSMutableDictionary *GSBDict = [[NSMutableDictionary alloc] init];
    double guarSurrValue = 0;
    double guranteedSurrenderVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *guarSurrValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE :bean]];
        
        guarSurrValue = [guarSurrValueStr doubleValue];
        
        [GSBDict setObject:[NSNumber numberWithLong:roundl(guarSurrValue)] forKey:[NSNumber numberWithInt:cnt]];
    }
    return GSBDict;
}

-(NSMutableDictionary *)getTotalMaturityValue{
    NSMutableDictionary *totalMatValueDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *GAIDict = master.GuaranteedAnnualIncome;
    NSMutableDictionary *MatValueDict = master.MaturityValue;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long LtotMatVal = 0;
    LtotMatVal = [[NSString stringWithFormat:@"%@ ", [GAIDict objectForKey:[NSNumber numberWithInt:polTerm]]] longLongValue] + [[NSString stringWithFormat:@"%@ ", [MatValueDict objectForKey:[NSNumber numberWithInt:polTerm]]] longLongValue];
    
    for (int cnt = 1; cnt <= polTerm; cnt++) {
        if(cnt == polTerm){
            [totalMatValueDict setObject:[NSNumber numberWithLong:LtotMatVal] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [totalMatValueDict setObject:[NSNumber numberWithLong:0.0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return totalMatValueDict;
}

-(NSMutableDictionary *)getDict:(NSString *)_calParamter{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    double dVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : _calParamter :bean]];
        
        dVal = [ValueStr doubleValue];
        lval = round(dVal);
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getGuaranteedInflationCover{
    NSMutableDictionary *ipcDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        long IPCValue = [self calculateIPC:cnt];
        [ipcDict setObject:[NSNumber numberWithLong:IPCValue] forKey:[NSNumber numberWithInt:cnt]];
    }
    return ipcDict;
}

-(long)calculateIPC:(int)polYr{
    long lval = 0.0;
    
    bean = [self getFormulaBean];
    bean.CounterVariable = polYr;
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *ValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_ANNUAL_INFLATION_COVER :bean]];
    
    lval = [ValueStr longLongValue];
    
    return lval;
}

-(long)getTotalRB_MLGP:(NSString *)_RBPercentParam{
    long TotRB8 = 0.0;
    double dtotRB8;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : _RBPercentParam :bean]];
        
        dtotRB8 = [VSRValueStr doubleValue];
        TotRB8 += round(dtotRB8);
    }
    return TotRB8;
}

-(long)getRenewalPrem:(NSString *)calParam{
    long lval = 0.0;
    
    bean = [self getFormulaBean];
    bean.AmountForServiceTax = [self getRenewalModalPremium_A];
    //changes 28th march
    if ([master.sisRequest.BasePlan hasPrefix:@"MLS"] || [master.sisRequest.BasePlan hasPrefix:@"MRS"]) {
        bean.RiderPremium = master.TotalAnnRiderModalPremium;
    }
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *ValueStr = [NSString stringWithFormat:@"%@",[handler evaluateFormula:master.sisRequest.BasePlan : calParam :bean]];
    
    lval = [ValueStr longLongValue];
    return lval;
}

-(long)getRenewalModalPremium_A{
    long renewalAnnual = 0;
    long renewalNSAP = 0;
    renewalAnnual = master.BaseAnnualizedPremium;
    if(master.ModalPremiumForNSAP != 0){
        /*if ([master.sisRequest.Frequency isEqualToString:@"A"]) {
            renewalNSAP = master.ModalPremiumForNSAP;
        }
        if ([master.sisRequest.Frequency isEqualToString:@"S"]) {
            renewalNSAP = round(master.ModalPremiumForNSAP / 0.51);
        }
        if ([master.sisRequest.Frequency isEqualToString:@"Q"]) {
            renewalNSAP = round(master.ModalPremiumForNSAP / 0.26);
        }
        if ([master.sisRequest.Frequency isEqualToString:@"M"]) {
            renewalNSAP = round(master.ModalPremiumForNSAP / 0.0883);
        }*/
        //renewalNSAP = master.ModalPremiumForNSAP;
    }
    //Commented by Amruta
    //if ([master.sisRequest.BasePlan isEqualToString: @"FGV1N1"]) {
        renewalNSAP = master.ModalPremiumForAnnNSAP !=0 ? master.ModalPremiumForAnnNSAP : 0;
    //}
    if (master.TotalAnnRiderModalPremium !=0) {
        renewalAnnual = renewalAnnual + master.TotalAnnRiderModalPremium;
    }
    renewalAnnual += renewalNSAP;
    return renewalAnnual;
}

-(NSMutableDictionary *)getSurrenderBenefit8{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    double onePlusRBD = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 1;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(cnt == 1){
            bean.AmountForServiceTax = 0;
        }
        else
        {
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            bean.CounterVariable = cnt - 1;
            if ([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || ([master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] && (cnt > startPt))) {
                
                NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
                onePlusRBD = [onePlusRBStr doubleValue];
                onePlusRBD = pow(onePlusRBD, (cnt-startPt));
                bean.AmountForServiceTax = onePlusRBD;
                
            }
            else
            {
            bean.AmountForServiceTax = 0;
            }
        
        NSString *vsrBonusstr = @"0";
        if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
           [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
            vsrBonusstr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VSRBONUS_RB8_D :bean]];
        }
        else{
            vsrBonusstr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        }
        
        //NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
        bean.AmountForServiceTax = [vsrBonusstr doubleValue];
    }
    
    bean.CounterVariable = cnt;
    
    if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
        bean.premium = master.DiscountedBasePremium;
    }
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_8 :bean]];
    
    surrBenefit = [surrBenValue longLongValue];
        
        
        //Added by Amruta on 7/9/15 for SMART7
        if ([master.sisRequest.BasePlan hasPrefix:@"SMART7V1N1"] && cnt==polTerm) {
            [SBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
        else
            
            [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

-(long)getServiceTaxForBasePalnPremium{
    
    long serviceTax = 0;
    //Added by Amruta on 11/9/15 for FG Rider
    serviceTax =round([self getServiceTaxForBasePlan:master.QuotedModalPremium-master.TotalRiderModalPremium]);
    return serviceTax;
    
}

-(double)getServiceTaxForBasePlan:(double)amount{
    
    double serviceTax = 0;
    bean = [self getFormulaBean];
    bean.AmountForServiceTax = amount;
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
    NSString *serviceTaxValueStr =(NSString *) [formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_SERVICE_TAX_BASE_PLAN :bean];
    serviceTax =[serviceTaxValueStr doubleValue];
    return serviceTax;
    
}

-(long)getTotalModalPremiumPayble{
    
    long totaMPP = 0;
    FormulaBean *formulaBeanObj = [self getFormulaBean];
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
    NSString *totaMPPStrt =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :TOTMODAL_PREM_PAYBLE :formulaBeanObj]];
    totaMPP =[totaMPPStrt longLongValue];
    return totaMPP;
    
}

-(long)getRenewalPremiumServiceTax:(NSString *)formula{
    
    long RenewalPremiumST = 0;
    bean = [self getFormulaBean];
    //Added by Amruta on 9/9/2015 for FG Rider
    bean.AmountForServiceTax = [self getRenewalModalPremium_A];
    
    //Added by Amruta for MIP WPP rider
    //changes 28th March
    if([master.sisRequest.BasePlan hasPrefix:@"MI"]){
        bean.premium = master.BaseAnnualizedPremium;
    }
    else
    //changes 4th march ADDL
    bean.premium = [self getRenewalModalPremium_A];//AP
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
    NSString *RenewalPremiumSTStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :formula :bean]];
    RenewalPremiumST =[RenewalPremiumSTStr longLongValue];
    return RenewalPremiumST;
}

-(long)getTotalRenewalPremium:(NSString *
                               )formula{
    
    long totalRenewalPremium = 0;
    bean = [self getFormulaBean];
    //Added by Amruta on 9/9/2015 for SMART7
    bean.AmountForServiceTax = [self getRenewalModalPremium_A];
    
    //Added by Amruta for MIP WPP rider
    if ([master.sisRequest.BasePlan hasPrefix:@"MI"]) {
        bean.premium = master.BaseAnnualizedPremium;
    }
    else
    //changes 4th march ADDL..
    bean.premium = [self getRenewalModalPremium_A];
    FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
    NSString *totalRenewalPremiumStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :formula :bean]];
    totalRenewalPremium =[totalRenewalPremiumStr longLongValue];
    return totalRenewalPremium;
}

-(NSString *)getValidateMinimumPremium{
    
    NSString *errorMessageStr = @"";
    NSString *frequencyStr = @"";
    if ([master.sisRequest.Frequency caseInsensitiveCompare:ANNUAL_MODE]) {
        frequencyStr = @"Annual";
    }else  if ([master.sisRequest.Frequency caseInsensitiveCompare:SEMI_ANNUAL_MODE]) {
        frequencyStr = @"Semi Annual";
    }else  if ([master.sisRequest.Frequency caseInsensitiveCompare:QUATERLY_MODE]) {
        frequencyStr = @"Quaterly";
    }else  if ([master.sisRequest.Frequency caseInsensitiveCompare:MONTHLY_MODE]) {
        frequencyStr = @"Monthly";
    }else  if ([master.sisRequest.Frequency caseInsensitiveCompare:SINGLE_MODE]) {
        frequencyStr = @"Single";
    }
    long annualMinPremium =[DataAccessClass getMinimumPremium:master.sisRequest.BasePlan andwithPayMode:master.sisRequest.Frequency];
    if (master.QuotedAnnualizedPremium < annualMinPremium) {
        errorMessageStr =[NSString stringWithFormat:@"%@ %@",@"Premium is less than minimum premium for",frequencyStr];
    }
    return errorMessageStr;
    
}

-(NSString *)getPlanDescription{
    
    PNLPlan *pnlPlanObj = [DataAccessClass getPnlPlanObj:master.sisRequest.BasePlan];
    if (pnlPlanObj != NULL && pnlPlanObj.PNL_DESCRIPTION != NULL) {
        return pnlPlanObj.PNL_DESCRIPTION;
    }else{
        return @"";
    }
}

-(AgentDetail *)getAgentDetails{
    return [DataAccessClass getAgentDetail];
}

-(NSString *)getUINNumber{
    PNLPlan *pnlPlanObj = [DataAccessClass getPnlPlanObj:master.sisRequest.BasePlan];
    if (pnlPlanObj != NULL && pnlPlanObj.UIN != NULL) {
        return pnlPlanObj.UIN;
    }else{
        return @"";
    }
    
}

-(NSMutableDictionary *)getTotalPremiumAnnual{
    long riderPremTotal = 0;
    int polterm = (int)master.sisRequest.PolicyTerm;
    NSString *modeStr =master.sisRequest.Frequency;
    int ppt =(int) master.sisRequest.PremiumPayingTerm;
    //Added by Amruta for ADDL Rider
    int insuredAge = master.sisRequest.InsuredAge;
    NSString *planTypeStr =[DataAccessClass getPlanType:master.sisRequest.BasePlan];
    NSMutableDictionary *hmTotalPrem = [[NSMutableDictionary alloc]init];
    NSMutableArray *array = master.riderData;
    
    for (int cnt = 1; cnt <=polterm; cnt++) {
        for (int j = 0; j < array.count; j++) {
            /*Changes made by Amruta
             j=0
             added cnt in else if
             */
            Rider *riderObj = [array objectAtIndex:j];
            if ([riderObj.RDL_CODE isEqualToString:@"WOPSM7V1"] &&(cnt > (65 - master.sisRequest.InsuredAge))){
                
            }
            else if(([riderObj.RDL_CODE isEqualToString:@"WOPPSM7V1"])&&(cnt >(65 - master.sisRequest.ProposerAge))) {
                // NSLog(@"ridercode,%@,%d",riderObj.RDL_CODE,cnt);
            }//Added by Amruta for ADDL Rider
            else if(([riderObj.RDL_CODE isEqualToString:@"ADDLN1V1"]) && (insuredAge > 69)){
                
            }else{
                // NSLog(@"else::ridercode,%@,%d",riderObj.RDL_CODE,cnt);
                
                riderPremTotal = riderPremTotal + riderObj.Premium;
            }
            
            insuredAge++;
            
        }
        if (([@"O" isEqualToString:modeStr] && cnt > 1 ) || ( cnt > ppt)) {
            
            [hmTotalPrem setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            
            if (([@"C" isEqualToString:planTypeStr]) && ([@"Y" isEqualToString:master.sisRequest.TataEmployee]) && ( cnt == 1)) {
                
                [ hmTotalPrem setObject:[NSNumber numberWithLong:(master.DiscountedBasePremium + riderPremTotal)]  forKey:[NSNumber numberWithInt:cnt]];
            }else{
                
                [ hmTotalPrem setObject:[NSNumber numberWithLong:(master.QuotedAnnualizedPremium + riderPremTotal)]  forKey:[NSNumber numberWithInt:cnt]];
                
            }
            
        }
        riderPremTotal = 0;
        
    }
    return hmTotalPrem;
    
    
}

-(NSString *)getPlanFeature{
    
    PGFPlanGroupFeature *pgfPalnGroupFeatureObj = [DataAccessClass getPlanGroupFeature:master.sisRequest.BasePlan];
    if (pgfPalnGroupFeatureObj != NULL && pgfPalnGroupFeatureObj.PGF_FEATURE !=NULL ) {
        return pgfPalnGroupFeatureObj.PGF_FEATURE;
    }else{
        return @"";
    }
    
}

-(long)getServiceTaxOnPremium{
    long serviceTax = 0;
    //changes 19 jan
    // issue of modal premium
    serviceTax = round([self getServiceTax:master.QuotedModalPremium - master.TotalRiderModalPremium ]);
    return serviceTax;
}

-(double)getServiceTax:(double)amount{
    
    double serviceTax = 0;
    FormulaBean *formulaBeanObj =[self getFormulaBean];
    [formulaBeanObj setAmountForServiceTax:amount];
    [formulaBeanObj setPremium:master.BaseAnnualizedPremium];
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    NSString *serviceTaxValueStr = [NSString stringWithFormat:@"%@ ",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_SERVICE_TAX :formulaBeanObj]];
    serviceTax = [serviceTaxValueStr doubleValue];
    return serviceTax;
    
}

-(NSMutableDictionary *)getTotalRiderAnnualPremium{
    NSMutableDictionary *hmRiderPrem = [[NSMutableDictionary alloc]init];
    long riderPremTotal = 0;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int insuredAge = (int)master.sisRequest.InsuredAge;
    NSMutableArray *array = master.riderData;
    for (int cnt = 1; cnt <=polterm; cnt++) {
        for (int j = 0; j < array.count; j++) {
            
            Rider *riderObj = [array objectAtIndex:j];
            if (([riderObj.RDL_CODE caseInsensitiveCompare:@"WOPSM7V1"]==NSOrderedSame) &&(cnt > (65 - master.sisRequest.InsuredAge))){
                
            }else if(([riderObj.RDL_CODE caseInsensitiveCompare:@"WOPPSM7V1"]==NSOrderedSame)&&( cnt > (65 - master.sisRequest.ProposerAge))) {
                
            }//Added by Amruta for ADDL rider
            else if(([riderObj.RDL_CODE caseInsensitiveCompare:@"ADDLN1V1"]==NSOrderedSame) && (insuredAge > 69)){
                
            }
            else{
                riderPremTotal = riderPremTotal + riderObj.Premium;
            }
            
            insuredAge++;
        }
        //Added by Amruta for vital Pro
        /*if([master.sisRequest.BasePlan containsString:@"VCPFPPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPSPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPSPPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFPV1N"] ||[master.sisRequest.BasePlan containsString:@"VCPSDV1N"] || [master.sisRequest.BasePlan containsString:@"VCPSDPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFDV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFDPV1N"]){
            riderPremTotal = master.QuotedAnnualizedPremium;
        }*/
        [hmRiderPrem setObject:[NSNumber numberWithLong:riderPremTotal] forKey:[NSNumber numberWithInt:cnt]];
        riderPremTotal = 0;
    }
    return hmRiderPrem;
    
}

-(long)getTotalMaturityValuePlusGAI{
    
    NSMutableDictionary *hmGAIDict =[self getGuaranteeAnnualIncome];
    NSMutableDictionary *hmMatval = [self getMaturityValue];
    int polterm = (int)master.sisRequest.BasePlan;
    long totMatVal = 0;
    if ([hmMatval count]>0 && [hmGAIDict count ]>0) {
        for (int cnt = 1; cnt <=polterm; cnt++) {
            totMatVal = totMatVal + (long)[hmGAIDict count] +  (long)[hmMatval count];
        }
    }
    
    return totMatVal;
}

-(NSMutableDictionary *)getGuaranteeAnnualIncome{
    NSMutableDictionary *gaiDict = [[NSMutableDictionary alloc]init];
    int polTerm =(int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1 ; cnt <=polTerm ; cnt++) {
        if ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"])) {
            long gaiValue =[self calculateGAI:cnt];
            [gaiDict setObject:[NSNumber numberWithFloat:gaiValue] forKey:[NSNumber numberWithInt:cnt]];
        }else if(cnt > master.sisRequest.PremiumPayingTerm){
            long gaiValue =[self calculateGAI:cnt];
            [gaiDict setObject:[NSNumber numberWithFloat:gaiValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [gaiDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
            
        }
    }
    
    return gaiDict;
    
}

#pragma mark -- Smart Growth Plus
-(NSMutableDictionary *) getTotalVestedCRB:(int)percent{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    //long TotalLVal = 0;
    long lval = 0.0;
    double dVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]) && cnt > startPt){
            NSString *onePlusRBStr = @"";
            if(percent == 4){
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            }else{
                bean.CNT_MINUS = false;
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            }
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *ValueStr = @"";
        if(percent == 4){
            ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        }else{
            
            ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        }
        
        dVal = [ValueStr doubleValue];
        lval = round(dVal);
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
        //TotalLVal += lval;
    }
    return Dict;
}

-(NSMutableDictionary *)getVSRBonus4{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    double dVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        //Added by Amruta for MBP and InstaWealth
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        //added on 31 dec for Freedom
        if(
           (
            ([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
             [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]
             )
            && cnt > startPt
            ) || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"GKIDV1P1"]
           ){
            
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        
        dVal = [ValueStr doubleValue];
        lval = round(dVal);
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getVSRBonus8{
    NSMutableDictionary *VSRDict = [[NSMutableDictionary alloc] init];
    long vsrBonus = 0.0;
    double dVsrBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        //Added by Amruta for InstaWealth o 14/9/15
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        //added on 31 dec for Freedom
        if(
           (
            ([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]
            )
            && cnt > startPt
            ) || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"GKIDV1P1"]){
            
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
        dVsrBonus = [VSRValueStr doubleValue];
        vsrBonus = round(dVsrBonus);
        [VSRDict setObject:[NSNumber numberWithLong:vsrBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return VSRDict;
}

-(NSMutableDictionary *)getTotalTermBonus:(int)percent{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc] init];
    //long totTerminalBonus = 0;
    long terminalBonus = 0.0;
    double dterminalBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]) && cnt > startPt){
            
            NSString *onePlusRBStr = @"";
            if(percent == 8){
                bean.CNT_MINUS = false;
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            }else{
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            }
            
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *terminalValueStr = @"";
        if(percent == 8){
            terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_8 :bean]];
        }else{
            terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_4 :bean]];
        }
        
        dterminalBonus = [terminalValueStr doubleValue];
        terminalBonus = round(dterminalBonus);
        [TBDict setObject:[NSNumber numberWithLong:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
        //totTerminalBonus += terminalBonus;
    }
    return TBDict;
}

-(NSMutableDictionary *)getTerminalBonus8{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc] init];
    long terminalBonus = 0;
    double dterminalBonus = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        //Added by Amruta for MBP and Insta Wealth
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]) && cnt > startPt){
            
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_8 :bean]];
        
        dterminalBonus = [terminalValueStr doubleValue];
        terminalBonus = round(dterminalBonus);
        [TBDict setObject:[NSNumber numberWithLong:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TBDict;
}

-(NSMutableDictionary *)getTerminalBonus4{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc] init];
    long terminalBonus = 0.0;
    double dterminalBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        //Added by Amruta for MBP and InstaWealth
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]) && cnt > startPt){
            
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_4 :bean]];
        
        dterminalBonus = [terminalValueStr doubleValue];
        terminalBonus = round(dterminalBonus);
        [TBDict setObject:[NSNumber numberWithLong:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TBDict;
}

-(NSMutableDictionary *)getDict:(NSString *)_calParamter WithCNTMINUS:(BOOL)_cntMinus andWithOtherParam:(NSString *)_param{
    
    //changes 16th may added round parameter in DB for GK..
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    long totalGAIVal = 0;
    double onePlusRBD = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    //changes 6th oct
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int startPt = 1;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 6;
    }
    NSString *param = _param;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        if (_cntMinus == true) {
            bean.CounterVariable = cnt-1;
        }else{
            bean.CounterVariable = cnt;
        }
        
        bean.CNT_MINUS = _cntMinus;
        
        
        //Added by Amruta on 26/8/15 for MBP & for InstaWealth on 15/9/15
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
            [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]))
        {
            
            long GAIValue = [self calculateGAI:cnt];
            totalGAIVal = totalGAIVal + GAIValue;
            
            //added 29th sept
            if (cnt == polTerm && [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]) {
                totalGAIVal = totalGAIVal - GAIValue;
            }
            
            if(cnt > startPt){
                NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : param :bean]];
                onePlusRBD = [onePlusRBStr doubleValue];
                onePlusRBD = pow(onePlusRBD, (cnt-startPt));
                
            }else{
                onePlusRBD = 0;
            }
            bean.AmountForServiceTax = onePlusRBD;
            
            //added 29th sept
            if (cnt > ppt) {
                bean.CNT_MINUS = false;
            }
        }else{
            bean.AmountForServiceTax = 0;
        }
        bean.AGAI = totalGAIVal;
		//changes 13th feb
        //bean.CounterVariable = cnt-1;
		if (_cntMinus == true) {
            bean.CounterVariable = cnt-1;
        }else{
            bean.CounterVariable = cnt;
        }
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : _calParamter :bean]];
        
        lval = [ValueStr longLongValue];
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getNONGuaranteedSurrBenefit_8{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 1;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 6;
    }
    
    double onePlusRBD = 0;
    long totalGAIVal = 0;
    long finalTotalGAIVal = 0;
    long finalTotalGAIValSSv = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(cnt == 1){
            bean.AmountForServiceTax = 0;
            bean.AGAI = totalGAIVal;
            bean.AGAISSV = finalTotalGAIValSSv;
        }
        else{
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            long GAIValue = [self calculateGAI:cnt];
            bean.CounterVariable = cnt - 1;
            
            totalGAIVal = totalGAIVal + GAIValue;
            finalTotalGAIVal = totalGAIVal;
            finalTotalGAIValSSv = finalTotalGAIVal;
            
            //Added by Amruta on 26/8/15 for MBP
			//changes 13th feb
            if (cnt==polTerm && [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]) {
                finalTotalGAIVal = finalTotalGAIVal - GAIValue;
            }
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
                [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]|| [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
                
                //long GAIValue = [self calculateGAI:cnt];
                //totalGAIVal = totalGAIVal + GAIValue;
                
                //Added by Amruta on 15/9/15 for InstaWealth
                if(cnt > startPt){
                    NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
                    onePlusRBD = [onePlusRBStr doubleValue];
                    onePlusRBD = pow(onePlusRBD, (cnt-startPt));
                    bean.AmountForServiceTax = onePlusRBD;
                    
                }/*else{
                    onePlusRBD = 0;
                }*/
               
            }else{
                bean.AmountForServiceTax = 0;
            }
            
            bean.AGAI = finalTotalGAIVal;
            bean.AGAISSV = finalTotalGAIValSSv;
            
            NSString *vsrBonusstr = @"0";
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
                [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
                vsrBonusstr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VSRBONUS_RB8_D :bean]];
            }
            else{
                vsrBonusstr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
            }
            
            //NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
            
            bean.AmountForServiceTax = [vsrBonusstr doubleValue];
        }
        
        bean.CounterVariable = cnt;
        
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_8 :bean]];
        
        surrBenefit = [surrBenValue longLongValue];
        
        //commented as data is not matching with excel sheet provided..
        /* if(cnt == polTerm){
         surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_BENEFIT_8 :bean]];
         surrBenefit = [surrBenValue longLongValue];
         }*/
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *) getSurrenderBenefit_4{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long finalTotalGAIVal = 0;
    long finalTotalGAIValSSV = 0;
    int startPt = 1;
    double onePlusRBD = 0;
    long totalGAIVal = 0;
    
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 6;
    }
        for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(cnt == 1){
            bean.AmountForServiceTax = 0;
            bean.AGAI = totalGAIVal;
            bean.AGAISSV = finalTotalGAIValSSV;
        }
        else{
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            
            long GAIValue = [self calculateGAI:cnt];
            bean.CounterVariable = cnt - 1;
            
            totalGAIVal = totalGAIVal + GAIValue;
            finalTotalGAIVal = totalGAIVal;
            finalTotalGAIValSSV = finalTotalGAIVal;
            
            //Added by Amruta on 26/8/15 for MBP
            if (cnt==polTerm && [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]) {
                finalTotalGAIVal = finalTotalGAIVal - GAIValue;
            }

            //Added by Amruta on 15/9/2015 for InstaWealth
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
               [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]|| [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
                
                //long GAIValue = [self calculateGAI:cnt];
                //totalGAIVal = totalGAIVal + GAIValue;
                
                if(cnt > startPt){
                    NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
                    onePlusRBD = [onePlusRBStr doubleValue];
                    onePlusRBD = pow(onePlusRBD, (cnt-startPt));
                    bean.AmountForServiceTax = onePlusRBD;
                    
                }
                /*else{
                    onePlusRBD = 0;
                }*/
                
            }else
            
            {
            bean.AmountForServiceTax = 0;
            }
            bean.AGAI = finalTotalGAIVal;
            bean.AGAISSV = finalTotalGAIValSSV;
            
            NSString *surrBenValueStr = @"0";
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
               [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
                surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VSRBONUS_RB4_D :bean]];
            }else{
                 surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
            }
        
           // NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
            bean.AmountForServiceTax = [surrBenValueStr doubleValue];
        }
        
        bean.CounterVariable = cnt;
        
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        //bean.CounterVariable = cnt - 1;
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_4 :bean]];
        
        surrBenefit = [surrBenValue longLongValue];
        
        /*if(cnt == polTerm){
            surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_BENEFIT_4 :bean]];
            surrBenefit = [surrBenValue longLongValue];
        }*/
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotalBenefit4MM{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 0; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        if (cnt == polterm) {
            NSString *surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotalBenefit8MM{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 0; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        if (cnt == polterm) {
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
    
}

-(NSMutableDictionary *)getTotMaturityBenefit4{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
       [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    //Added by Amruta for InstaWealth on 15/9/2015
    for (int cnt = 1; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
           [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]){
            long GAIValue = [self calculateGAI:cnt];
            totalGAIValue = totalGAIValue + GAIValue;
        }
        
        
        if (cnt == polterm) {
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
               [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]){
                NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB4 :formulaBeanObj]];
                onePlusRBD = [onePlusRBStr doubleValue];
                onePlusRBD = pow(onePlusRBD, (cnt - startPt));
                formulaBeanObj.AmountForServiceTax = onePlusRBD;
                formulaBeanObj.AGAI = totalGAIValue;
                
            }
            NSString *surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
    
}

-(NSMutableDictionary *)getTotMaturityBenefit8{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
       [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        //Added by AMruta on 15/9/2015 for InstaWealth
        
        if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
           [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
            long GAIValue = [self calculateGAI:cnt];
            totalGAIValue = totalGAIValue + GAIValue;
        }
        
        
        if (cnt == polterm) {
            if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
               [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"]){
                NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB8 :formulaBeanObj]];
                onePlusRBD = [onePlusRBStr doubleValue];
                onePlusRBD = pow(onePlusRBD, (cnt - startPt));
                formulaBeanObj.AmountForServiceTax = onePlusRBD;
                formulaBeanObj.AGAI = totalGAIValue;
                
            }
            
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
    
}

-(NSMutableDictionary *)getAccGAI{
    NSMutableDictionary *accGAIDict = [[NSMutableDictionary alloc] init];
    //changes 6th oct
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long GAIValue = 0;
    
    for(int cnt = 1;cnt <= polTerm;cnt++){
        GAIValue += [self calculateGAI:cnt];
        [accGAIDict setObject:[NSNumber numberWithLong:GAIValue] forKey:[NSNumber numberWithInt:cnt]];
    }
    return accGAIDict;
}

-(long)getTotalAccGAddition{
    //changes 6th oct
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long GAIValue = 0;
    
    for(int cnt = 1;cnt <= polTerm;cnt++){
        GAIValue += [self calculateGAI:cnt];
        //[accGAIDict setObject:[NSNumber numberWithLong:GAIValue] forKey:[NSNumber numberWithInt:cnt]];
    }
    return GAIValue;
}

#pragma mark -- HOW SOLUTION WOORKS
-(long )getTotalVSRBonus:(SISRequest *)request{
    long totVSRValue = 0;
    long lval = 0.0;
    double dVal = 0;
    //changes 6th oct
    int polTerm = (int)request.PolicyTerm;
    
    int startPt = 0;
    if([request.BasePlan isEqualToString:@"SGPV1N1"] || [request.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    
    double onePlusRBD = 0;
    NSString *onePlusRBStr = @"";
    NSString *ValueStr = @"";
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(([request.BasePlan isEqualToString:@"SGPV1N1"] ||
            [request.BasePlan isEqualToString:@"SGPV1N2"]) && cnt > startPt){
            
            if(request.percent == 4)
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:request.BasePlan : ONEPLUSRB4 :bean]];
            else
                onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:request.BasePlan : ONEPLUSRB8 :bean]];
            
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        if(request.percent == 4)
            ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:request.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        else
            ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:request.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
        dVal = [ValueStr doubleValue];
        lval = round(dVal);
        //[Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
        totVSRValue += lval;
    }
    return totVSRValue;
}

-(long )getTotMaturityBenefit:(SISRequest *)request{
    
    long TotMatVal = 0;
    long surrenderBenefit;
    int polterm = (int)request.PolicyTerm;
    int startPt = 0;
    if([request.BasePlan isEqualToString:@"SGPV1N1"] ||
       [request.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    NSString *onePlusRBStr = @"";
    NSString *surrBenStr = @"";
    
    for (int cnt = 0; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        if([request.BasePlan isEqualToString:@"SGPV1N1"] ||[request.BasePlan isEqualToString:@"SGPV1N2"]){
            long GAIValue = [self calculateGAI:cnt];
            totalGAIValue = totalGAIValue + GAIValue;
        }
        
        
        if (cnt == polterm) {
            if([request.BasePlan isEqualToString:@"SGPV1N1"] || [request.BasePlan isEqualToString:@"SGPV1N2"]){
                if(request.percent == 4){
                    onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:request.BasePlan :ONEPLUSRB4 :formulaBeanObj]];
                }else{
                    onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:request.BasePlan :ONEPLUSRB8 :formulaBeanObj]];
                }
                onePlusRBD = [onePlusRBStr doubleValue];
                onePlusRBD = pow(onePlusRBD, (cnt - startPt));
                formulaBeanObj.AmountForServiceTax = onePlusRBD;
                formulaBeanObj.AGAI = totalGAIValue;
                
            }
            if(request.percent == 4){
                surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:request.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :formulaBeanObj]];
            }else{
                surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:request.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :formulaBeanObj]];
            }
            surrenderBenefit = [surrBenStr longLongValue];
            TotMatVal += surrenderBenefit;
        }
    }
    return TotMatVal;
    
}

-(long)getGuaranteedSurrValueForSolution{
    long guarSurrValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    bean = [self getFormulaBean];
    bean.CounterVariable = polTerm;
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *guarSurrValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE :bean]];
    guarSurrValue = [guarSurrValueStr longLongValue];
    
    return guarSurrValue;
}

-(long)getVSRBonusForSolution{
    long vsrBonus = 0;
    double dVsrBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    bean = [self getFormulaBean];
    bean.CounterVariable = polTerm;
    bean.CNT_MINUS = false;
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *VSRValueStr = @"";
    
    if (master.sisRequest.percent == 8) {
        
        VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
    }else if (master.sisRequest.percent == 4){
        
        VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        dVsrBonus = [VSRValueStr doubleValue];
        
    }
    dVsrBonus = [VSRValueStr doubleValue];
    vsrBonus = round(dVsrBonus);
    return vsrBonus;
    
}

-(long)getTerminalBonusForSolution{
    
    long terminalBonus = 0;
    double dterminalBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    bean = [self getFormulaBean];
    bean.CounterVariable = polTerm;
    bean.CNT_MINUS = false;
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *terminalValueStr = @"";
    if (master.sisRequest.percent == 8) {
        
        terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_8 :bean]];
        
    }else if (master.sisRequest.percent == 4){
        
        terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_4 :bean]];
    }
    
    dterminalBonus = [terminalValueStr doubleValue];
    terminalBonus = round(dterminalBonus);
    
    return terminalBonus;
}

-(long)getTotMaturityBenefitForSolution{
    
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    FormulaBean *formulaBeanObj =[self getFormulaBean];
    [formulaBeanObj setCounterVariable:polterm];
    [formulaBeanObj setCNT_MINUS:false];
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    NSString *surrBenStr = @"";
    if (master.sisRequest.percent == 8){
        surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :formulaBeanObj]];
    }
    else if (master.sisRequest.percent == 4){
        surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :formulaBeanObj]];
    }
    
    surrenderBenefit = [surrBenStr longLongValue];
    
    return surrenderBenefit;
}

-(long) getMaturityValueForSolution{
    long LMaturityValue = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    bean = [self getFormulaBean];
    bean.CounterVariable = polTerm;
    
    FormulaHandler *handler = [[FormulaHandler alloc] init];
    NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
    
    LMaturityValue = [maturityValueStr longLongValue];
    return LMaturityValue;
}

-(long) getGAIForPolicyTermForSolution{
    long GAIValue = 0;
    if ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]) {
        //changes 6th oct
        GAIValue = [self calculateGAI:(int)master.sisRequest.PolicyTerm];
    }else if(master.sisRequest.PolicyTerm > master.sisRequest.PremiumPayingTerm){
        //changes 6th oct
        GAIValue = [self calculateGAI:(int)master.sisRequest.PolicyTerm];
    }
    return GAIValue;
}

-(long)getTotalMaturityValueForSolution{
    long lGaiVal = [self getGAIForPolicyTermForSolution];
    long lMatVal = [self getMaturityValueForSolution];
    
    long LTotMatVal = lGaiVal + lMatVal;
    return LTotMatVal;
}

#pragma mark - ALternate Scenario
-(NSMutableDictionary *)getAltScenarioTotMatBenCurrentRate{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    for (int cnt = 0; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        if (cnt == polterm) {
            [formulaBeanObj setAmountForServiceTax:0.25];
            NSString *surrBenStr =(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ALTSCE_TOTMATBENCR :formulaBeanObj];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithFloat:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}

-(NSMutableDictionary *)getAltScenarioTotalDeathBenefitCR{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc]init];
    long terminalBonus ;
    int polTerm = (int)master.sisRequest.BasePlan;
    for (int cnt = 1; cnt <= polTerm ; cnt++) {
        if (cnt > 8) {
            FormulaBean *formulaBeanObj = [self getFormulaBean];
            [formulaBeanObj setCounterVariable:(cnt-1)];
            [formulaBeanObj setCNT_MINUS:true];
            [formulaBeanObj setAmountForServiceTax:0.25];
            FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
            NSString *terminalBonusStr =(NSString *) [formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ALTSCE_TOTDB_CR :formulaBeanObj];
            terminalBonus = [terminalBonusStr longLongValue];
            [TBDict setObject:[NSNumber numberWithFloat:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            FormulaBean *formulaBeanObj = [self getFormulaBean];
            [formulaBeanObj setCounterVariable:(cnt-1)];
            [formulaBeanObj setCNT_MINUS:true];
            [formulaBeanObj setAmountForServiceTax:0];
            FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
            NSString *terminalBonusStr =(NSString *) [formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ALTSCE_TOTDB_CR :formulaBeanObj];
            terminalBonus = [terminalBonusStr longLongValue];
            [TBDict setObject:[NSNumber numberWithFloat:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
            
            
        }
    }
    
    return TBDict;
}

-(NSMutableDictionary *)getAltScenario4MaturityBenefitTax{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit ;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double TotalGaiSum = [self getTotalGaiSum];
    for (int cnt = 1; cnt <= polTerm ; cnt++) {
        
        FormulaBean *formulaBeanObj = [self getFormulaBean];
        [formulaBeanObj setCounterVariable:(cnt)];
        [formulaBeanObj setAmountForServiceTax:TotalGaiSum];
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] && (cnt == 1)){
            
            [formulaBeanObj setPremium:master.DiscountedBasePremium];
        }
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
        
        if(cnt == polTerm){
            
            NSString *surreBenStr =(NSString *) [formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ALTSCE_MAT4BEN_TAX :formulaBeanObj];
            surrenderBenefit = [surreBenStr longLongValue];
        }else{
            
            surrenderBenefit = 0;
        }
        
        [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    
    return SBDict;
}

-(NSMutableDictionary *)getALTScenarioCumulativeEffectivePremium{
    NSMutableDictionary *CEPDict = [[NSMutableDictionary alloc] init];
    long cep;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    long tempcep = 0;
    
    //FormulaBean *bean;
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(ppt >= cnt){
            bean.CounterVariable = cnt - 1;
            NSString *cepStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_CEP :bean]];
            cep = [cepStr longLongValue];
            
            [CEPDict setObject:[NSNumber numberWithLong:cep] forKey:[NSNumber numberWithInt:cnt]];
            tempcep = cep;
        }else{
            [CEPDict setObject:[NSNumber numberWithLong:tempcep] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return CEPDict;
}

-(NSMutableDictionary *)getALTScenarioSurrBenCurrRate{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    NSString *ebrate = [[[self getFormulaBean] request] Exp_Bonus_Rate];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(cnt == 1){
            bean.request.Exp_Bonus_Rate = @"0";
        }else{
            bean.request.Exp_Bonus_Rate = ebrate;
        }
        if(cnt >= 8)
        {
            bean.AmountForServiceTax = 0.25;
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            bean.CounterVariable = cnt - 1;
            NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_SBCR :bean]];
            
            surrBenefit = [surrBenValueStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ALTSCE_SBCR :bean]];
        
        surrBenefit = [surrBenValue longLongValue];
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getALTScenarioPremPaidCumulativeCurrRate{
    NSMutableDictionary *PaidCumulativeDict = [[NSMutableDictionary alloc] init];
    long lvsrBonus = 0.0;
    double dvsrBonus;
    long PaidCumulativeTotal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    //FormulaBean *bean;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        
        if(cnt >= 8)
        {
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            bean.CNT_MINUS = false;
            bean.AmountForServiceTax = 0.25;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            bean.CounterVariable = cnt - 1;
            NSString *paidCumm = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_VBCUMMULATIVECR :bean]];
            
            dvsrBonus = [paidCumm doubleValue];
            lvsrBonus = round(dvsrBonus);
            PaidCumulativeTotal = PaidCumulativeTotal + lvsrBonus;
            
            [PaidCumulativeDict setObject:[NSNumber numberWithLong:PaidCumulativeTotal] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            bean.CNT_MINUS = false;
            bean.AmountForServiceTax = 0;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            bean.CounterVariable = cnt - 1;
            NSString *paidCumm = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_VBCUMMULATIVECR :bean]];
            
            dvsrBonus = [paidCumm doubleValue];
            lvsrBonus = round(dvsrBonus);
            PaidCumulativeTotal = PaidCumulativeTotal + lvsrBonus;
            
            [PaidCumulativeDict setObject:[NSNumber numberWithLong:PaidCumulativeTotal] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return PaidCumulativeDict;
    
}

-(NSMutableDictionary *)getALTScenarioTaxSaving{
    NSMutableDictionary *TSDict = [[NSMutableDictionary alloc] init];
    long taxSave;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    
    //FormulaBean *bean;
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(ppt >= cnt){
            bean.CounterVariable = cnt - 1;
            NSString *taxSaveStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_TAXSAVING :bean]];
            taxSave = [taxSaveStr longLongValue];
            
            [TSDict setObject:[NSNumber numberWithLong:taxSave] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [TSDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return TSDict;
}

-(NSMutableDictionary *)getALTScenarioAnnualizedEffectivePremium{
    NSMutableDictionary *AEPDict = [[NSMutableDictionary alloc] init];
    long aep;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    
    // FormulaBean *bean;
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(ppt >= cnt){
            bean.CounterVariable = cnt - 1;
            NSString *aepStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan :ALTSCE_AEP :bean]];
            aep = [aepStr longLongValue];
            
            [AEPDict setObject:[NSNumber numberWithLong:aep] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [AEPDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return AEPDict;
}

-(NSMutableDictionary *)getSurrenderBenefit_4_SMART7{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(cnt == 1){
            bean.AmountForServiceTax = 0;
        }
        else{
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            bean.CounterVariable = cnt - 1;
            NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
            
            bean.AmountForServiceTax = [surrBenValueStr doubleValue];
        }
        
        bean.CounterVariable = cnt;
        
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        //bean.CounterVariable = cnt - 1;
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_4 :bean]];
        
        surrBenefit = [surrBenValue longLongValue];
        
        if ([master.sisRequest.BasePlan hasPrefix:@"SMART7V1N1"] && cnt==polTerm) {
            [SBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
        else
            
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getLifeCoverageAltSCE{
    NSMutableDictionary *LifeCoverageMapDict = [[NSMutableDictionary alloc] init];
    long LifeCoverage = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int rpuYear = [master.sisRequest.RPU_Year intValue];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(rpuYear == 0){
            [LifeCoverageMapDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }else if(cnt < rpuYear){
            NSString *LifeCovValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LIFE_COVERAGE :bean]];
            
            LifeCoverage = [LifeCovValueStr longLongValue];
            [LifeCoverageMapDict setObject:[NSNumber numberWithLong:LifeCoverage] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            NSString *LifeCovValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LIFE_COVERAGE_RPU :bean]];
            
            LifeCoverage = [LifeCovValueStr longLongValue];
            [LifeCoverageMapDict setObject:[NSNumber numberWithLong:LifeCoverage] forKey:[NSNumber numberWithInt:cnt]];
        }
        
    }
    return LifeCoverageMapDict;
}

-(NSMutableDictionary *)getALTScenarioMaturityVal{
    NSMutableDictionary *HVDict = [[NSMutableDictionary alloc] init];
    long maturityValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ALT_SCENARIO_MATURITY_VALUE :bean]];
            
            maturityValue = round([maturityValueStr doubleValue]);
            [HVDict setObject:[NSNumber numberWithDouble:maturityValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [HVDict setObject:[NSNumber numberWithDouble:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return HVDict;
}

-(NSMutableDictionary *)getALTScenarioTotalDeathBenefit8{
    NSMutableDictionary *ASTDBDict = [[NSMutableDictionary alloc] init];
    long lASTDB = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int rpuYear = [master.sisRequest.RPU_Year intValue];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        if(rpuYear > cnt)
            bean.CounterVariable = cnt-1;
        else
            bean.CounterVariable = rpuYear -1;
        bean.CNT_MINUS = true;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(rpuYear == 0){
            [ASTDBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }else if(cnt < rpuYear){
            NSString *ASTDBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TOT_DEATH_BENEFIT_8 :bean]];
            
            lASTDB = [ASTDBStr longLongValue];
            [ASTDBDict setObject:[NSNumber numberWithLong:lASTDB] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            NSString *ASTDBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ALT_SCE_TOT_DEATH_BENF_8 :bean]];
            
            lASTDB = [ASTDBStr longLongValue];
            [ASTDBDict setObject:[NSNumber numberWithLong:lASTDB] forKey:[NSNumber numberWithInt:cnt]];
        }
        
    }
    return ASTDBDict;
}

-(NSMutableDictionary *)getALTScenarioTotalDeathBenefit4{
    NSMutableDictionary *ASTDBDict = [[NSMutableDictionary alloc] init];
    long lASTDB = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int rpuYear = [master.sisRequest.RPU_Year intValue];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        if(rpuYear > cnt)
            bean.CounterVariable = cnt-1;
        else
            bean.CounterVariable = rpuYear -1;
        bean.CNT_MINUS = true;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(rpuYear == 0){
            [ASTDBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }else if(cnt < rpuYear){
            NSString *ASTDBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TOT_DEATH_BENEFIT_4 :bean]];
            
            lASTDB = [ASTDBStr longLongValue];
            [ASTDBDict setObject:[NSNumber numberWithLong:lASTDB] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            NSString *ASTDBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ALT_SCE_TOT_DEATH_BENF_4 :bean]];
            
            lASTDB = [ASTDBStr longLongValue];
            [ASTDBDict setObject:[NSNumber numberWithLong:lASTDB] forKey:[NSNumber numberWithInt:cnt]];
        }
        
    }
    return ASTDBDict;
}

-(NSMutableDictionary *)getALTScenarioMaturityValue4{
    NSMutableDictionary *ASTMVDict = [[NSMutableDictionary alloc] init];
    long lASTMV = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int rpuYear = [master.sisRequest.RPU_Year intValue];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            if(rpuYear == 0){
                bean.CounterVariable = cnt;
            }else if(rpuYear > cnt)
                bean.CounterVariable = cnt-1;
            else
                bean.CounterVariable = rpuYear -1;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            
            NSString *ASTMVStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ALT_SCE_MATURITY_BENEFIT_4 :bean]];
            
            lASTMV = [ASTMVStr longLongValue];
            [ASTMVDict setObject:[NSNumber numberWithLong:lASTMV] forKey:[NSNumber numberWithInt:cnt]];
        }else
        {
            [ASTMVDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return ASTMVDict;
    
}

-(NSMutableDictionary *)getALTScenarioMaturityValue8{
    NSMutableDictionary *ASTMVDict = [[NSMutableDictionary alloc] init];
    long lASTMV = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int rpuYear = [master.sisRequest.RPU_Year intValue];
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            if(rpuYear == 0){
                bean.CounterVariable = cnt;
            }else if(rpuYear > cnt)
                bean.CounterVariable = cnt-1;
            else
                bean.CounterVariable = rpuYear -1;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            
            NSString *ASTMVStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ALT_SCE_MATURITY_BENEFIT_8 :bean]];
            
            lASTMV = [ASTMVStr longLongValue];
            [ASTMVDict setObject:[NSNumber numberWithLong:lASTMV] forKey:[NSNumber numberWithInt:cnt]];
        }else
        {
            [ASTMVDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return ASTMVDict;
    
}

-(NSMutableDictionary *)getALTScenarioToMatBenCurrRate{
    NSMutableDictionary *ASTMBDict = [[NSMutableDictionary alloc] init];
    long lASTMB = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt == polTerm){
            bean.AmountForServiceTax = 0.25;
            
            NSString *ASTMBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ALTSCE_TOTMATBENCR :bean]];
            
            lASTMB = [ASTMBStr longLongValue];
            [ASTMBDict setObject:[NSNumber numberWithDouble:lASTMB] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [ASTMBDict setObject:[NSNumber numberWithDouble:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return ASTMBDict;
}

-(void)calculateAlternateChargesSmartPH6{
    
}

-(void)calculateChargesForFPPH10{
    
}

-(void) calculateTopUpPartialWithDForPH10{
    
}

//Added by Amruta on 18/8/15
-(NSString *)getComissionText{
    
    NSString *comText=@"";
    NSString *PlanCode= master.sisRequest.BasePlan;
    comText=[DataAccessClass getCommissionText:PlanCode];
    return comText;
    
}

-(long)getRenewalPremiumMonthly{
    long renewalMonthly = 0;
    long renewalNSAP = 0;
    renewalMonthly = master.BaseAnnualizedPremium;
        renewalMonthly = round(renewalMonthly * 0.0883);
    
    if(master.ModalPremiumForNSAP!=0){
        //Changed by Amruta for ADDL rider (master.ModalPremiumForNSAP to master.ModalPremiumForAnnNSAP) for Mahalife Magic
        renewalNSAP = round(master.ModalPremiumForAnnNSAP*0.0883);
    }
    
        if (master.TotalAnnRiderModalPremium!=0) {
            renewalMonthly = (long) (renewalMonthly + round(master.TotalAnnRiderModalPremium * 0.0883));
        }
        
    renewalMonthly = renewalMonthly+renewalNSAP;
    return  renewalMonthly;
}

-(long)getRenewalPremiumQuaterly{
    long renewalQuaterly = 0;
    long renewalNSAP = 0;
    renewalQuaterly = master.BaseAnnualizedPremium;
    renewalQuaterly = round(renewalQuaterly * 0.26);
    
    if(master.ModalPremiumForNSAP!=0){
        renewalNSAP = round(master.ModalPremiumForAnnNSAP*0.26);
    }
    
    if (master.TotalAnnRiderModalPremium!=0) {
        renewalQuaterly = (long) (renewalQuaterly + round(master.TotalAnnRiderModalPremium * 0.26));
    }
    
    renewalQuaterly = renewalQuaterly+renewalNSAP;
    return  renewalQuaterly;
}

-(long)getRenewalPremiumAnnual{
    long renewalAnnually = 0;
    long renewalNSAP = 0;
    renewalAnnually = master.BaseAnnualizedPremium;
    renewalAnnually = round(renewalAnnually * 1);
    
    if(master.ModalPremiumForNSAP!=0){
        renewalNSAP = round(master.ModalPremiumForAnnNSAP * 1);
    }
    
    if (master.TotalAnnRiderModalPremium!=0) {
        renewalAnnually = (long) (renewalAnnually + round(master.TotalAnnRiderModalPremium * 1));
    }
    
    renewalAnnually = renewalAnnually+renewalNSAP;
    return  renewalAnnually;
}


-(long)getRenewalPremiumSemi{
    long renewalSemi = 0;
    long renewalNSAP = 0;
    renewalSemi = master.BaseAnnualizedPremium;
    renewalSemi = round(renewalSemi * 0.51);
    
    if(master.ModalPremiumForNSAP!=0){
        //Changed by Amruta for ADDL rider (master.ModalPremiumForNSAP to master.ModalPremiumForAnnNSAP) for Mahalife Magic
        renewalNSAP = round(master.ModalPremiumForAnnNSAP*0.51);
    }
    
    if (master.TotalAnnRiderModalPremium!=0) {
        renewalSemi = (long) (renewalSemi + round(master.TotalAnnRiderModalPremium * 0.51));
    }
    
    renewalSemi = renewalSemi+renewalNSAP;
    return  renewalSemi;
}

-(long) getTotalRenewalPremiumFG:(long) renewalPremiumM {
    long renewalPremiumST = 0;
    if (renewalPremiumM != 0)
        renewalPremiumST = round(renewalPremiumM * master.ServiceTaxRenewal / 100);
    return (renewalPremiumM + renewalPremiumST);
}

-(long) getNSAPLoadPremiumAnnual{
    long NSAPPremium = 0;
    if ([master.sisRequest.AgeProofFlag caseInsensitiveCompare:@"N"] == NSOrderedSame) {
        bean = [self getFormulaBean];
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *Basepremium = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_ANNUAL_NON_STANDARD_AGE_PROOF :bean]];
        NSAPPremium = [Basepremium longLongValue];
    }else{
        NSAPPremium = 0;
    }
    
    return NSAPPremium;
}


-(NSMutableDictionary *) getGuaranteedAnnualIncomeAll{
    
    NSMutableDictionary *gaiMapDict = [[NSMutableDictionary alloc]init];
    //changes 6th oct
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int count=1; count <= polTerm; count++) {
        long GAIValue = [self calculateGAI:count];
        if (count==polTerm)
            [gaiMapDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:count]];
        else
            
            [gaiMapDict setObject:[NSNumber numberWithLong:GAIValue] forKey:[NSNumber numberWithInt:count]];
    }
    return gaiMapDict;
}

//Added by Amruta on 1/9/2015 for MBP

-(NSMutableDictionary *) getTotMaturityBenefit8_MBP{
    
    NSMutableDictionary *SBMapDict = [[NSMutableDictionary alloc]init];
    long surrenderBenifit ;
    //changes 6th oct
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double onePlusRBD = 0;
    long totalGAIVal = 0;
    
    for (int cnt = 1; cnt <= polTerm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
        long GAIValue = [self calculateGAI:cnt];
        totalGAIVal = GAIValue;
        if (cnt==polTerm) {
             NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB8 :formulaBeanObj]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, cnt);
            formulaBeanObj.AmountForServiceTax = onePlusRBD;
            formulaBeanObj.AGAI = totalGAIVal;
            
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8:formulaBeanObj]];
            surrenderBenifit =[surrBenStr longLongValue];
                [SBMapDict setObject:[NSNumber numberWithLong:surrenderBenifit+totalGAIVal] forKey:[NSNumber numberWithInt:cnt]];
        }
            else
            {
                [SBMapDict setObject:[NSNumber numberWithLong:totalGAIVal] forKey:[NSNumber numberWithInt:cnt]];
            
        }
    }
   
    return SBMapDict;
}

-(NSMutableDictionary *) getTotMaturityBenefit4_MBP{
    
    NSMutableDictionary *SBMapDict = [[NSMutableDictionary alloc]init];
    long surrenderBenifit ;
    //changes 6th oct
    int polTerm = (int)master.sisRequest.PolicyTerm;
    double onePlusRBD = 0;
    long totalGAIVal = 0;
    
    for (int cnt = 1; cnt <= polTerm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj = [[FormulaHandler alloc]init];
        long GAIValue = [self calculateGAI:cnt];
        totalGAIVal = GAIValue;
        
        if (cnt==polTerm) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB4 :formulaBeanObj]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, cnt);
            formulaBeanObj.AmountForServiceTax = onePlusRBD;
            formulaBeanObj.AGAI = totalGAIVal;
            
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4:formulaBeanObj]];
            surrenderBenifit =[surrBenStr longLongValue];
            [SBMapDict setObject:[NSNumber numberWithLong:surrenderBenifit+totalGAIVal] forKey:[NSNumber numberWithInt:cnt]];
        }
        else
        {
            [SBMapDict setObject:[NSNumber numberWithLong:totalGAIVal] forKey:[NSNumber numberWithInt:cnt]];
            
        }
    }
    
    return SBMapDict;
}

//Added by Amruta on 10/9/2015 for FG Rider
-(long)getRenewalPremiumServiceTaxMonthly:(long)renewalPremiumM{
    
    long renewalPremiumST = 0;
    if (renewalPremiumM != 0) {
        renewalPremiumST = round(renewalPremiumM * master.ServiceTaxRenewal / 100);
        return renewalPremiumST;
    }
    //changes 6th oct
    return renewalPremiumST;
}

//*****************FREEDOM**********************
-(NSMutableDictionary *) getAccPTGuaranteedAnnualIncome{
    NSMutableDictionary *gaiMap = [[NSMutableDictionary alloc] init];
    
    int polterm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    double gaiValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        if (cnt <= ppt) {
            gaiValue += [self calculateGAIDouble:cnt];
        }
        [gaiMap setObject:[NSNumber numberWithDouble:round(gaiValue)] forKey:[NSNumber numberWithInt:cnt]];
    }
    return gaiMap;
}

-(NSMutableDictionary *)getSurrenderBenefitFR_8{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
	//changes 13th feb
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    
    double finalTotalGAIVal = 0;
    long finalTotalGAIValSSV = 0;
    int startPt = 0;
    double onePlusRBD = 0;
    double totalGAIVal = 0;
    bean.RBV = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        
        double GAIValue = [self calculateGAIDouble:cnt];
        bean.CounterVariable = cnt - 1;
        bean.AGAISSV = finalTotalGAIValSSV;
        bean.doubleAGAI = totalGAIVal;
        
        //GAIValue = [self calculateGAIDouble:cnt];
        
        if (cnt <= ppt) {
            totalGAIVal = totalGAIVal + GAIValue;
        }
        //NSLog(@"total gai valiue is %f",totalGAIVal);
        finalTotalGAIVal = totalGAIVal;
        finalTotalGAIValSSV = finalTotalGAIVal;
        
        NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
        onePlusRBD = [onePlusRBStr doubleValue];
        onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        
        bean.AmountForServiceTax = onePlusRBD;
        bean.doubleAGAI = finalTotalGAIVal;
        bean.AGAISSV = finalTotalGAIValSSV;
        
        NSString *surrBenValueStr = @"0";
        surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8_D :bean]];
        
        bean.AmountForServiceTax = [surrBenValueStr doubleValue];
        
        bean.CounterVariable = cnt;
        
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }
        if (cnt != polTerm) {
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_8 :bean]];
        
        surrBenefit = [surrBenValue longLongValue];
        bean.RBV = [surrBenValueStr doubleValue];
        }else{
            surrBenefit = 0;
        }
        
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getSurrenderBenefitFR_4{
    
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    
    double finalTotalGAIVal = 0;
    long finalTotalGAIValSSV = 0;
    int startPt = 0;
    double onePlusRBD = 0;
    double totalGAIVal = 0;
    bean.RBV = 0;

    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
       
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        
        double GAIValue = [self calculateGAIDouble:cnt];
        bean.CounterVariable = cnt - 1;
        bean.AGAISSV = finalTotalGAIValSSV;
        bean.doubleAGAI = totalGAIVal;
        
        GAIValue = [self calculateGAIDouble:cnt];

        if (cnt <= ppt) {
            totalGAIVal = totalGAIVal + GAIValue;
        }
        finalTotalGAIVal = totalGAIVal;
        finalTotalGAIValSSV = finalTotalGAIVal;
        
        NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
        onePlusRBD = [onePlusRBStr doubleValue];
        onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        
        bean.AmountForServiceTax = onePlusRBD;
        bean.doubleAGAI = finalTotalGAIVal;
        bean.AGAISSV = finalTotalGAIValSSV;
        
        NSString *surrBenValueStr = @"0";
        surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4_D :bean]];
        
        bean.AmountForServiceTax = [surrBenValueStr doubleValue];
        
        bean.CounterVariable = cnt;
        
        if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }
        if (cnt != polTerm) {
            NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_4 :bean]];
            
            surrBenefit = [surrBenValue longLongValue];
            bean.RBV = [surrBenValueStr doubleValue];
        }else{
            surrBenefit = 0;
        }
        
 
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
    
}

-(double )calculateGAIDouble:(int )polYear{
    
    double gaiValue = 0;
    FormulaBean *formulaBeanObj=[self getFormulaBean];
    [formulaBeanObj setCounterVariable:polYear];
    FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
    NSString *gaiValueValueStr =(NSString *)[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_GUARANTEED_ANNUAL_INCOME_D :formulaBeanObj];
    //NSLog(@"calculate gai double is %f",[gaiValueValueStr doubleValue]);
    if(gaiValueValueStr != nil)
        gaiValue = [gaiValueValueStr doubleValue];
    return gaiValue;
    
}

-(NSMutableDictionary *) getDeathBenefit:(NSString *)_calParamter  andWithOtherParam:(NSString *)_param{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    double totalGAIVal = 0;
    double onePlusRBD = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int startPt = 1;
    
    NSString *param = _param;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt-1;
        bean.CNT_MINUS = true;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        double GAIValue = [self calculateGAIDouble:cnt];
        if (cnt <= ppt) {
            totalGAIVal = totalGAIVal + GAIValue;
        }
        if (cnt == polTerm) {
            totalGAIVal = 0;
        }
        if (cnt > startPt) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : param :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        }else
            onePlusRBD = 0;
        
        bean.AmountForServiceTax = onePlusRBD;
        
        if (cnt > ppt) {
            bean.CNT_MINUS = false;
        }
        bean.CounterVariable = cnt-1;
        bean.doubleAGAI = totalGAIVal;
        
        
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : _calParamter :bean]];
        
        lval = [ValueStr longLongValue];
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getTotMaturityBenefit8FR{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int startPt = 0;
  
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        bean =[self getFormulaBean];
        [bean setCounterVariable:cnt];
        [bean setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        long GAIValue = [self calculateGAIDouble:cnt];
        
        if (cnt <= ppt) {
            totalGAIValue = totalGAIValue + GAIValue;
        }
        
        if (cnt == polterm) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt - startPt));
            bean.AmountForServiceTax = onePlusRBD;
            bean.AGAI = totalGAIValue;
        
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :bean]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotMaturityBenefit4FR{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int ppt = master.sisRequest.PremiumPayingTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        bean =[self getFormulaBean];
        [bean setCounterVariable:cnt];
        [bean setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        long GAIValue = [self calculateGAIDouble:cnt];
        
        if (cnt <= ppt) {
            totalGAIValue = totalGAIValue + GAIValue;
        }
        
        if (cnt == polterm) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt - startPt));
            bean.AmountForServiceTax = onePlusRBD;
            bean.AGAI = totalGAIValue;
            
            NSString *surrBenStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :bean]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}


//*************************MIP******************************************
-(NSMutableDictionary *)getMaturityValue_MIP{
    NSMutableDictionary *HVDict = [[NSMutableDictionary alloc] init];
    long maturityValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long GAIVAl = 0;
    long GAIValue = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        //Addded by Amruta
        GAIVAl = [self calculateGAI:cnt];
        GAIValue = GAIValue + GAIVAl;
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = GAIValue;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
        
        maturityValue = [maturityValueStr longLongValue];
        [HVDict setObject:[NSNumber numberWithLong:maturityValue] forKey:[NSNumber numberWithInt:cnt]];
    }
    return HVDict;
}

-(NSMutableDictionary *)getGuaranteedSurrValue_MIP{
    NSMutableDictionary *GSBDict = [[NSMutableDictionary alloc] init];
    double guarSurrValue = 0;
    double guranteedSurrenderVal = 0;
    long totalGAIval = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = totalGAIval;
        bean.AGAI = totalGAIval;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *guarSurrValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE :bean]];
        
        guarSurrValue = [guarSurrValueStr doubleValue];
        
        
        if (cnt == polTerm) {
            guranteedSurrenderVal = 0;
        }else{
            guranteedSurrenderVal = round(guarSurrValue);
        }
       
        [GSBDict setObject:[NSNumber numberWithDouble:guranteedSurrenderVal] forKey:[NSNumber numberWithInt:cnt]];
    }
    return GSBDict;
}

-(NSMutableDictionary *)getVSRBonus4_MIP{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    double dVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    int startPt = 0;
    if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"]){
        startPt = 5;
    }
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        //Added by Amruta for MBP and InstaWealth
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        //added on 31 dec for Freedom
        if(cnt > startPt){
            
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        
        dVal = [ValueStr doubleValue];
        lval = round(dVal);
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getVSRBonus8_MIP{
    NSMutableDictionary *VSRDict = [[NSMutableDictionary alloc] init];
    long vsrBonus = 0.0;
    double dVsrBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
   
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt > startPt){
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
        dVsrBonus = [VSRValueStr doubleValue];
        vsrBonus = round(dVsrBonus);
        [VSRDict setObject:[NSNumber numberWithLong:vsrBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return VSRDict;
}

-(NSMutableDictionary *)getTerminalBonus4_MIP{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc] init];
    long terminalBonus = 0.0;
    double dterminalBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt > startPt){
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_4 :bean]];
        
        dterminalBonus = [terminalValueStr doubleValue];
        terminalBonus = round(dterminalBonus);
        [TBDict setObject:[NSNumber numberWithLong:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TBDict;
}

-(NSMutableDictionary *)getTerminalBonus8_MIP{
    NSMutableDictionary *TBDict = [[NSMutableDictionary alloc] init];
    long terminalBonus = 0;
    double dterminalBonus = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt > startPt){
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *terminalValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_TERMINAL_BONUS_8 :bean]];
        
        dterminalBonus = [terminalValueStr doubleValue];
        terminalBonus = round(dterminalBonus);
        [TBDict setObject:[NSNumber numberWithLong:terminalBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TBDict;
}

-(NSMutableDictionary *)getDeathBenefit_MIP:(NSString *)_calParamter andWithOtherParam:(NSString *)_param{
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc] init];
    long lval = 0.0;
    double totalGAIVal = 0;
    double onePlusRBD = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int startPt = 1;
    
    NSString *param = _param;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt-1;
        bean.CNT_MINUS = true;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        
        double GAIValue = [self calculateGAI:cnt];
        totalGAIVal = totalGAIVal + GAIValue;
        
        if (cnt == polTerm) {
            totalGAIVal = 0;
        }
        if (cnt > startPt) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : param :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        }else
            onePlusRBD = 0;
        
        bean.AmountForServiceTax = onePlusRBD;
        
        if (cnt > ppt) {
            bean.CNT_MINUS = false;
        }
        bean.CounterVariable = cnt-1;
        bean.AGAI = totalGAIVal;
        
        
        NSString *ValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : _calParamter :bean]];
        
        lval = [ValueStr longLongValue];
        [Dict setObject:[NSNumber numberWithLong:lval] forKey:[NSNumber numberWithInt:cnt]];
    }
    return Dict;
}

-(NSMutableDictionary *)getSurrenderBenefit_4_MIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long totalGAIVal = 0;
    long finalTotalGAIVal = 0;
    long finalTotalGAIValSSV = 0;
    
    int startPt = 0;
    double onePlusRBD = 0;
    bean.RBV = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        
        long GAIValue = [self calculateGAI:cnt];
        bean.doubleAGAI = totalGAIVal;
        bean.AGAISSV = finalTotalGAIValSSV;
        bean.CounterVariable = cnt - 1;
        
        GAIValue = [self calculateGAI:cnt];
        totalGAIVal = totalGAIVal + GAIValue;
        
        finalTotalGAIVal = totalGAIVal;
        finalTotalGAIValSSV = finalTotalGAIVal;
        
        NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
        onePlusRBD = [onePlusRBStr doubleValue];
        onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        bean.AmountForServiceTax = onePlusRBD;
        
        bean.doubleAGAI = finalTotalGAIVal;
        bean.AGAISSV = finalTotalGAIValSSV;
        
        NSString *surrBenValueStr = @"0";
        
        surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4_D :bean]];
        bean.AmountForServiceTax = [surrBenValueStr doubleValue];

        
        bean.CounterVariable = cnt;
		
		//changes 14th feb as per prince we removed the check
        /*if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }*/
        
        //bean.AGAI = totalGAIVal;
        //FormulaHandler *handler = [[FormulaHandler alloc] init];
        //bean.CounterVariable = cnt - 1;
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_4 :bean]];
        bean.RBV = [surrBenValueStr doubleValue];
        
        if (cnt == polTerm) {
            surrBenefit = 0;
        }else
            surrBenefit = [surrBenValue longLongValue];
  
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getNONGuaranteedSurrBenefit_8_MIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    
    double onePlusRBD = 0;
    long totalGAIVal = 0;
    
    //changes 28th jan
    long finalTotalGaiVal = 0;
    long finalTotalGaiValSSV = 0;
    bean.RBV = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        long GAIValue = [self calculateGAI:cnt];
        
        //changes 28th jan
        bean.doubleAGAI = totalGAIVal;
        bean.AGAISSV = finalTotalGaiValSSV;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        bean.CounterVariable = cnt - 1;
        
        totalGAIVal = totalGAIVal + GAIValue;
        finalTotalGaiVal = totalGAIVal;
        finalTotalGaiValSSV = finalTotalGaiVal;
        
        NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
        onePlusRBD = [onePlusRBStr doubleValue];
        onePlusRBD = pow(onePlusRBD, (cnt-startPt));
        bean.AmountForServiceTax = onePlusRBD;
        bean.doubleAGAI = finalTotalGaiVal;
        bean.AGAISSV = finalTotalGaiValSSV;
        
        NSString *vsrBonusstr = @"0";
        
        vsrBonusstr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8_D :bean]];
        
        bean.AmountForServiceTax = [vsrBonusstr doubleValue];
        
        bean.CounterVariable = cnt;
		
		//changes 14th feb as per prince we removed the check
        /*if([@"Y" caseInsensitiveCompare:master.sisRequest.TataEmployee] == NSOrderedSame && cnt == 1){
            bean.premium = master.DiscountedBasePremium;
        }*/
        
        NSString *surrBenValue = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT_8 :bean]];
        
        bean.RBV = [vsrBonusstr doubleValue];
        
        if (cnt == polTerm) {
            surrBenefit = 0;
        }else
            surrBenefit = [surrBenValue longLongValue];

        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotMaturityBenefit8_MIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        long GAIValue = [self calculateGAI:cnt];
        totalGAIValue = GAIValue;
        
        if (cnt == polterm) {
           
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB8 :formulaBeanObj]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt - startPt));
            formulaBeanObj.AmountForServiceTax = onePlusRBD;
            formulaBeanObj.AGAI = totalGAIValue;
            
            NSString *surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_8 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotMaturityBenefit4_MIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    long totalGAIValue = 0;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        FormulaBean *formulaBeanObj =[self getFormulaBean];
        [formulaBeanObj setCounterVariable:cnt];
        [formulaBeanObj setCNT_MINUS:false];
        FormulaHandler *formulaHandlerObj =[[FormulaHandler alloc]init];
        
        long GAIValue = [self calculateGAI:cnt];
        totalGAIValue = GAIValue;
        
        
        if (cnt == polterm) {
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :ONEPLUSRB4 :formulaBeanObj]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt - startPt));
            formulaBeanObj.AmountForServiceTax = onePlusRBD;
            formulaBeanObj.AGAI = totalGAIValue;
            
            NSString *surrBenStr =[NSString stringWithFormat:@"%@",[formulaHandlerObj evaluateFormula:master.sisRequest.BasePlan :CALCULATE_MATURITY_BENEFIT_4 :formulaBeanObj]];
            surrenderBenefit = [surrBenStr longLongValue];
            [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            
        }else{
            [SBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt] ];
            
        }
    }
    return SBDict;
}

-(long)getTotalVSRBonus4_MIP{
    long TOTALRB4_MIP = 0;
    
    long vsrBonus = 0.0;
    double dVsrBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;

        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt > startPt){
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_4 :bean]];
        
        dVsrBonus = [VSRValueStr doubleValue];
        vsrBonus = round(dVsrBonus);
        //[VSRDict setObject:[NSNumber numberWithLong:vsrBonus] forKey:[NSNumber numberWithInt:cnt]];
        TOTALRB4_MIP += vsrBonus;
    }
    return TOTALRB4_MIP;
}

-(long)getTotalVSRBonus8_MIP{
    long TOTALRB8_MIP = 0;
    long vsrBonus = 0.0;
    double dVsrBonus;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int startPt = 0;
    
    double onePlusRBD = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if(cnt > startPt){
            NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
            onePlusRBD = [onePlusRBStr doubleValue];
            onePlusRBD = pow(onePlusRBD, (cnt-startPt));
            bean.AmountForServiceTax = onePlusRBD;
            
        }else{
            bean.AmountForServiceTax = 0;
        }
        
        NSString *VSRValueStr = [NSString stringWithFormat:@"%@ ",[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_VRSBONUS_8 :bean]];
        
        dVsrBonus = [VSRValueStr doubleValue];
        vsrBonus = round(dVsrBonus);
        TOTALRB8_MIP += vsrBonus;
        //[VSRDict setObject:[NSNumber numberWithLong:vsrBonus] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TOTALRB8_MIP;
}

-(long)getTotalGuranteedAnnualIncome{
    long gaiTotal = 0;
    long GAIValue = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if ([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]) {
            GAIValue = [self calculateGAI:cnt];
        }else if(cnt > master.sisRequest.PremiumPayingTerm){
            GAIValue = [self calculateGAI:cnt];
        }else
            GAIValue = 0;
        gaiTotal += GAIValue;
    }
    return gaiTotal;
}

//SIP
-(NSMutableDictionary *)getTotalEGuarBenefit{
    NSMutableDictionary *TGBDict = [[NSMutableDictionary alloc] init];
    long maturityValue = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long GAIVAl = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        GAIVAl = [self calculateGAI:cnt];
        if(cnt >= polTerm-1){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            maturityValue = [maturityValueStr longLongValue];
            [TGBDict setObject:[NSNumber numberWithLong:maturityValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [TGBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return TGBDict;
}

-(NSMutableDictionary *)getSurrenderBenefit_SIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long surrBenefit = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long maturityValue = 0.0;
    long totalMaturityvalue = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if (cnt == polTerm- 1) {
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            maturityValue = [maturityValueStr longLongValue];
            totalMaturityvalue += maturityValue;
        }
        
        bean.AmountForServiceTax = totalMaturityvalue;
        
        NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_SURRENDER_BENEFIT :bean]];
        
        surrBenefit = [surrBenValueStr longLongValue];
        if(cnt == polTerm){
            surrBenefit = 0.0;
        }
        
        [SBDict setObject:[NSNumber numberWithLong:surrBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

-(NSMutableDictionary *)getGuaranteeEPAnnualIncome{
    NSMutableDictionary *TGBDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long GAIVAl = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        if(cnt == polTerm-1){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            bean.AmountForServiceTax = GAIVAl;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            GAIVAl = [maturityValueStr longLongValue];
            [TGBDict setObject:[NSNumber numberWithLong:GAIVAl] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [TGBDict setObject:[NSNumber numberWithLong:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    return TGBDict;
}

-(NSMutableDictionary *)getGuaranteedSurrValue_SIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long guarSurrValue = 0.0;
    double guarSurrVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long maturityValue = 0.0;
    long totalMaturityvalue = 0;
    //long totalAgaiValue = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        //long GAIValue = [self calculateGAI:cnt];
        //totalAgaiValue = totalAgaiValue + GAIValue;
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        //bean.AGAI = totalAgaiValue;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        if (cnt == polTerm- 1) {
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            maturityValue = [maturityValueStr longLongValue];
            totalMaturityvalue += maturityValue;
        }
        
        bean.AmountForServiceTax = totalMaturityvalue;
        
        NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE:bean]];
        
        guarSurrVal = [surrBenValueStr doubleValue];
        if(cnt == polTerm){
            guarSurrValue = 0;
        }else
            guarSurrValue = round(guarSurrVal);
        
        [SBDict setObject:[NSNumber numberWithLong:guarSurrValue] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

-(NSMutableDictionary *)getGuaranteedSurrValue_EMIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long guarSurrValue = 0.0;
    double guarSurrVal = 0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long maturityValue = 0.0;
    long totalGaiValue = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        long GAIValue = [self calculateGAI:cnt];
        totalGaiValue = totalGaiValue + GAIValue;
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AGAI = totalGaiValue;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
        
        maturityValue = [maturityValueStr longLongValue];
        bean.AmountForServiceTax = maturityValue;
        
        
        NSString *surrBenValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_GUARANTEED_SURRENDER_VALUE:bean]];
        
        guarSurrVal = [surrBenValueStr doubleValue];
        if(cnt == polTerm){
            guarSurrValue = 0;
        }else
            guarSurrValue = round(guarSurrVal);
        
        [SBDict setObject:[NSNumber numberWithLong:guarSurrValue] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

-(NSMutableDictionary *)getTotalGuarBenefit{
    NSMutableDictionary *TGBDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    
    long GAIVAl = 0;
    long matVal = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        GAIVAl = [self calculateGAI:cnt];
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            matVal = [maturityValueStr longLongValue];
            
        }else{
            matVal = 0;
        }
        [TGBDict setObject:[NSNumber numberWithLong:matVal + GAIVAl] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TGBDict;
}

//GIP
-(NSMutableDictionary *)getTotalGuarBenefit_GIP{
    NSMutableDictionary *TGBDict = [[NSMutableDictionary alloc] init];
    int polTerm = (int)master.sisRequest.PolicyTerm;
    int incomeBooster = (int)master.sisRequest.incomeBooster;
    double GAIVal = 0;
    long matVal = 0;
    
    for (int cnt = 1; cnt<=polTerm+incomeBooster; cnt++) {
        GAIVal = [self calculateGAIGIP:cnt andWith:GAIVal];
        if(cnt == polTerm){
            bean = [self getFormulaBean];
            bean.CounterVariable = cnt;
            
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MATURITY_VALUE :bean]];
            
            matVal = [maturityValueStr longLongValue];
            
        }else{
            matVal = 0;
        }
        [TGBDict setObject:[NSNumber numberWithLong:matVal + round(GAIVal)] forKey:[NSNumber numberWithInt:cnt]];
    }
    return TGBDict;
}

-(NSMutableDictionary *)getLifeCoverage_SIP{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc] init];
    long LifeCoverage = 0.0;
    int polTerm = (int)master.sisRequest.PolicyTerm;
    long GaiValue = 0;
    
    for (int cnt = 1; cnt<=polTerm; cnt++) {
        GaiValue = [self calculateGAI:polTerm];
        
        bean = [self getFormulaBean];
        bean.CounterVariable = cnt;
        bean.AmountForServiceTax = GaiValue;
        
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        NSString *maturityValueStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_LIFE_COVERAGE :bean]];
        
        LifeCoverage = [maturityValueStr longLongValue];
        
        [SBDict setObject:[NSNumber numberWithLong:LifeCoverage] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return SBDict;
}

//Good Kid
-(NSMutableDictionary *) getMilestoneAdditions:(NSString *)param{
    int pt = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *milestoneDict = [[NSMutableDictionary alloc] init];
    
    long milestoneValue = 0;
    for (int cnt=1; cnt<=pt; cnt++) {
        if (cnt != pt -4) {
            [milestoneDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *milestonestr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : param :bean]];
            
            milestoneValue = [milestonestr longLongValue];
            
            [milestoneDict setObject:[NSNumber numberWithLong:milestoneValue] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    
    return milestoneDict;
}

-(NSMutableDictionary *)getTotBen8Payable{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *mileStoneDict = master.MilestoneAdd8Dict;
    NSMutableDictionary *moneyBackBenDict = master.MoneyBackBenefitDict;
    NSMutableDictionary *vsrbonusDict = master.VSRBonus8;
    NSMutableDictionary *termBonusDict = master.TerminalBonus8;
    NSMutableDictionary *GSurrValDict = master.MaturityValue;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        
        surrenderBenefit = [[moneyBackBenDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[mileStoneDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        if (cnt == polterm) {
            long val = [[vsrbonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[termBonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[GSurrValDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
            surrenderBenefit += val;
            
        }
        [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
            

    }
    return SBDict;
}

-(NSMutableDictionary *)getTotBen4Payable{
    NSMutableDictionary *SBDict = [[NSMutableDictionary alloc]init];
    long surrenderBenefit;
    int polterm = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *mileStoneDict = master.MilestoneAdd4Dict;
    NSMutableDictionary *moneyBackBenDict = master.MoneyBackBenefitDict;
    NSMutableDictionary *vsrbonusDict = master.VSRBonus4;
    NSMutableDictionary *termBonusDict = master.TerminalBonus4;
    NSMutableDictionary *GSurrValDict = master.MaturityValue;
    
    for (int cnt = 1; cnt <= polterm; cnt++) {
        
        surrenderBenefit = [[moneyBackBenDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[mileStoneDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        if (cnt == polterm) {
            long val = [[vsrbonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[termBonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] + [[GSurrValDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
            surrenderBenefit += val;
            
        }
        [SBDict setObject:[NSNumber numberWithLong:surrenderBenefit] forKey:[NSNumber numberWithInt:cnt]];
        
        
    }
    return SBDict;
}

-(NSMutableDictionary *)getMoneyBackBenfit{
    int pt = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *MBBDict = [[NSMutableDictionary alloc] init];
    
    long MBBValue = 0;
    for (int cnt=1; cnt<=pt; cnt++) {
        
        if ((pt - cnt) <= 3 && (pt - cnt) > 0) {
            FormulaHandler *handler = [[FormulaHandler alloc] init];
            NSString *MBBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : CALCULATE_MONEYBACK_BENEFIT :bean]];
            
            MBBValue = [MBBStr longLongValue];
            
            [MBBDict setObject:[NSNumber numberWithLong:MBBValue] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            [MBBDict setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
        }
    }
    
    return MBBDict;
}

-(NSMutableDictionary *)getNonGuarSurrBen8_GK{
    int pt = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *NonGuarSurrBenDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *gsvDict = master.GuaranteedSurrValue;
    NSMutableDictionary *vsrBonusDict = master.VSRBonus8;
    NSMutableDictionary *totBenPayableDict = master.TotalMaturityBenefit8;
    long totBenPayable = 0;
    long RBVal = 0;
    long totMilStones = 0;
    
    //16th may
    double surrBen = 0;
    long ssvVal = 0;
    long nonGuarSurrBen = 0;
    
    int startPt = 0;
    double onePlusRBD = 0;
    long sumOfMBBAndMat = 0;
    
    for (int cnt=1; cnt<=pt; cnt++) {
        bean = [self getFormulaBean];
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        surrBen = [[gsvDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        
        //double GSVFactor = [DataAccessClass getGSVFactor:bean.request AndWithCount:cnt-1];
        double GSVFactor = [DataAccessClass getBSVFactor:bean.request andWithCount:cnt];
        
        if (cnt < (pt - 4)) {
            long mileStone = 0;
            if(cnt <= 5){
                NSString *milestonestr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : @"CAL_MILE_ADD8" :bean]];
                mileStone = [milestonestr longLongValue];
            }else{
                mileStone = 0;
            }
            totMilStones += mileStone;
            
            surrBen += GSVFactor * (RBVal + totMilStones);
        }else{
            totMilStones = 0;
            surrBen += GSVFactor * RBVal;
        }
        
        RBVal = [[vsrBonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        long benPayable = [[totBenPayableDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        totBenPayable += benPayable;
        
        surrBen -= totBenPayable;
        
        //************************************************************************************************************************************
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(
           (
            ([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
             [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]
             )
            && cnt > startPt
            ) || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"GKIDV1P1"]){
               
               NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB8 :bean]];
               onePlusRBD = [onePlusRBStr doubleValue];
               onePlusRBD = pow(onePlusRBD, (cnt-startPt));
               bean.AmountForServiceTax = onePlusRBD;
               
           }else{
               bean.AmountForServiceTax = 0;
           }
        
        NSString *calSSVVal = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : @"CAL_SSV8_GK" :bean]];
        //ssvVal = [calSSVVal longLongValue];
        
        long mbbMatAddition = [[master.MoneyBackBenefitDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] +
        [[master.MaturityValue objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        sumOfMBBAndMat += mbbMatAddition;
        
        double csvFactor = [DataAccessClass getCSVFactor:bean.request AndWithCount:cnt];
        ssvVal = round(([calSSVVal doubleValue] + totMilStones - sumOfMBBAndMat) * csvFactor);
        
        nonGuarSurrBen = MAX(surrBen, ssvVal);
        if (cnt == pt) {
            nonGuarSurrBen = 0;
        }
        [NonGuarSurrBenDict setObject:[NSNumber numberWithLong:nonGuarSurrBen] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return NonGuarSurrBenDict;
}

-(NSMutableDictionary *)getNonGuarSurrBen4_GK{
    int pt = (int)master.sisRequest.PolicyTerm;
    NSMutableDictionary *NonGuarSurrBenDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *gsvDict = master.GuaranteedSurrValue;
    NSMutableDictionary *vsrBonusDict = master.VSRBonus4;
    NSMutableDictionary *totBenPayableDict = master.TotalMaturityBenefit4;
    long totBenPayable = 0;
    long RBVal = 0;
    long totMilStones = 0;
    
    //16th may
    double surrBen = 0;
    long ssvVal = 0;
    long nonGuarSurrBen = 0;
    
    int startPt = 0;
    double onePlusRBD = 0;
    long sumOfMBBAndMat = 0;
    
    for (int cnt=1; cnt<=pt; cnt++) {
        bean = [self getFormulaBean];
        FormulaHandler *handler = [[FormulaHandler alloc] init];
        surrBen = [[gsvDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        
        //double GSVFactor = [DataAccessClass getGSVFactor:bean.request AndWithCount:cnt-1];
        double GSVFactor = [DataAccessClass getBSVFactor:bean.request andWithCount:cnt];
        
        if (cnt < (pt - 4)) {
            long mileStone = 0;
            if(cnt <= 5){
                NSString *milestonestr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : @"CAL_MILE_ADD4" :bean]];
                mileStone = [milestonestr longLongValue];
            }else{
                mileStone = 0;
            }
            totMilStones += mileStone;
            
            surrBen += GSVFactor * (RBVal + totMilStones);
        }else{
            totMilStones = 0;
            surrBen += GSVFactor * RBVal;
        }
        
        RBVal = [[vsrBonusDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        long benPayable = [[totBenPayableDict objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        totBenPayable += benPayable;
        
        surrBen -= totBenPayable;
        
        //************************************************************************************************************************************
        bean.CounterVariable = cnt;
        bean.CNT_MINUS = false;
        
        if(
           (
            ([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] ||
             [master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan hasPrefix:@"MBPV1N"]
             )
            && cnt > startPt
            ) || [master.sisRequest.BasePlan hasPrefix:@"IWV1N"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P1"] || [master.sisRequest.BasePlan hasPrefix:@"FR7V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR10V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"FR15V1P2"] || [master.sisRequest.BasePlan hasPrefix:@"GKIDV1P1"]){
               
               NSString *onePlusRBStr = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : ONEPLUSRB4 :bean]];
               onePlusRBD = [onePlusRBStr doubleValue];
               onePlusRBD = pow(onePlusRBD, (cnt-startPt));
               bean.AmountForServiceTax = onePlusRBD;
               
           }else{
               bean.AmountForServiceTax = 0;
           }
        
        NSString *calSSVVal = [NSString stringWithFormat:@"%@ ",(NSString *)[handler evaluateFormula:master.sisRequest.BasePlan : @"CAL_SSV4_GK" :bean]];
        //ssvVal = [calSSVVal longLongValue];
        
        long mbbMatAddition = [[master.MoneyBackBenefitDict objectForKey:[NSNumber numberWithInt:cnt]] longValue] +
        [[master.MaturityValue objectForKey:[NSNumber numberWithInt:cnt]] longValue];
        sumOfMBBAndMat += mbbMatAddition;
        
        double csvFactor = [DataAccessClass getCSVFactor:bean.request AndWithCount:cnt];
        ssvVal = round(([calSSVVal doubleValue] + totMilStones - sumOfMBBAndMat) * csvFactor);
        
        nonGuarSurrBen = MAX(round(surrBen), ssvVal);
        if (cnt == pt) {
            nonGuarSurrBen = 0;
        }
        [NonGuarSurrBenDict setObject:[NSNumber numberWithLong:nonGuarSurrBen] forKey:[NSNumber numberWithInt:cnt]];
        
    }
    return NonGuarSurrBenDict;
}

@end
