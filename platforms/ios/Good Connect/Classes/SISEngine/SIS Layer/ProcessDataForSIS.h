//
//  ProcessDataForSIS.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataMapper.h"
#import "ModelMaster.h"
#import "SISRequest.h"
#import "GenerateSIS.h"
#import "Rider.h"
#import "HtmlBuilder.h"

#import <Cordova/CDVPlugin.h>

@interface ProcessDataForSIS  : NSObject

-(NSMutableDictionary *) processDataForGeneratingSIS:(NSDictionary *)req;
-(NSMutableDictionary *)getHtmlContent:(NSDictionary *)frontEndDict;

@end
