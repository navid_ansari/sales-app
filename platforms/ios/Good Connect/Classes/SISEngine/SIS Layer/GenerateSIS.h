//
//  GenerateSIS.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelMaster.h"
#import "DataMapper.h"
#import "FormulaBean.h"
#import "FormulaHandler.h"
#import "DataAccessClass.h"

@interface GenerateSIS : NSObject


@property (nonatomic, strong) DataMapper *MapperObj;
@property (nonatomic, strong) ModelMaster *master;
@property (nonatomic, retain) FormulaBean *bean;

-(id)initWithMaster:(ModelMaster *)_master;
-(ModelMaster *) getSISData;
-(ModelMaster *)CalculateResultCorrespondToValue:(NSString *)_calValue andWithMaster:(ModelMaster *)_master;
-(long) getBasePremium:(NSString *)basePlan;

//********************HOW SOLUTION WORKS************************
-(long)getGAIForPolicyTermForSolution;
-(long)getTotalMaturityValueForSolution;
-(long)getMaturityValueForSolution;
-(long)getVSRBonusForSolution;
-(long)getTerminalBonusForSolution;
-(long)getTotMaturityBenefitForSolution;
-(long)getTotalRB_MLGP:(NSString *)_RBPercentParam;
-(long)getGuaranteedSurrValueForSolution;
-(long)getTotalAccGAddition;
-(NSMutableDictionary *)getTotalVestedCRB:(int)percent;
-(NSMutableDictionary *)getTotalTermBonus:(int)percent;
//new HSW
-(NSMutableDictionary *)getVSRBonus8;
-(NSMutableDictionary *)getVSRBonus4;
-(NSMutableDictionary *)getGuaranteedAnnualIncome;
-(NSMutableDictionary *)getTotMaturityBenefit8;
-(NSMutableDictionary *)getTotMaturityBenefit4;
-(NSMutableDictionary *)getTotalBenefit4MM;
-(NSMutableDictionary *)getTotalBenefit8MM;
-(NSMutableDictionary *)getGuaranteedAnnualIncomeAll;
//********************HOW SOLUTION WORKS************************

//changes 01/06/2015
-(double)getTotalGaiSum;
-(long) getTotalGAI_MLGP;
-(long )getTotalVSRBonus:(SISRequest *)request;
-(double)getTotalGaiSumMLS;

//*************ULIP**************
-(long long)getSurrenderValue :(int)cnt :(double)netPhAcc;
//*************ULIP**************
-(double)getServiceTax:(double)amount;

@end
