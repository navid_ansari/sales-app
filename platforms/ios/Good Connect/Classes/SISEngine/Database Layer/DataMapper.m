//
//  DataMapper.m
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "DataMapper.h"

static DataMapper *sharedInstance = nil;
@implementation DataMapper


@synthesize MDLModalFactor,OCLOccupation,PGLPlanGroup,PMLPremiumMultiple,PNLChkExcludes,PNLPlan,PGLGrpMinPrem,PNLMinPrem,PNLTermPpt,RMLRiderPremMultiple,PmdPlanPremMultiDisc,PBAProductBand,RDLRider,FDLFund,PMVPlanMaturityValue,PFRPlanFormulaRef,FEDFormulaExtDesc,Address,PGFPlanGroupFeature,PACPremiumAllocationCharges,ACMAdminChargeMaster,MCMMortilityChargeMaster,PCRPlanChargesRef,WCMWOPChargesMaster,FCMFIBChargesMaster,SURSurrenderChargesMaster,GuaranteeMaturityBonusMaster,CommissionChargeMaster,MFMMaturityFactorMaster,GAIGuarAnnInChMaster,XmlMetaDataMaster,SPVSysParamValues,RDLCharges,IPCValues,StdTemplateValues,AAAFMCValues,IllusPageList,CommissionDetail;

#pragma mark -- SINGELTON METHOD
+(DataMapper*)getInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance GetDBData];
    }
    return sharedInstance;
}

#pragma mark -- PRIVATE METHODS
-(DataMapper *)GetDBData{
    //DataMapper *map = [[DataMapper alloc] init];
    
    [[DBManager getSharedInstance] getPremiumMultiple:NULL];
    //*******************ULIP*******************
    sharedInstance.FDLFund = [[DBManager getSharedInstance] getFundDetails];
    sharedInstance.PCRPlanChargesRef = [[DBManager getSharedInstance] getPCRPlanChargesRef];
    sharedInstance.PACPremiumAllocationCharges = [[DBManager getSharedInstance] getPACPremiumAllocationCharges];
    sharedInstance.CommissionChargeMaster = [[DBManager getSharedInstance] getCCMCommissionChargeMaster];
    sharedInstance.MCMMortilityChargeMaster = [[DBManager getSharedInstance] getMCMMortilityChargesMaster];
    sharedInstance.ACMAdminChargeMaster = [[DBManager getSharedInstance] getACMAdminChargeMaster];
    sharedInstance.FCMFIBChargesMaster = [[DBManager getSharedInstance] getFCMFIBChargesMaster];
    sharedInstance.WCMWOPChargesMaster = [[DBManager getSharedInstance] getWCMWOPChargesMaster];
    sharedInstance.SURSurrenderChargesMaster = [[DBManager getSharedInstance] getSURSurrenderChargeMaster];
    sharedInstance.StdTemplateValues = [[DBManager getSharedInstance] getSTDTemplateValues];
    sharedInstance.IllusPageList = [[DBManager getSharedInstance] getIllusPageList];
    sharedInstance.RMLRiderPremMultiple = [[DBManager getSharedInstance] getRMLRiderPremMultiple];
    sharedInstance.RDLCharges = [[DBManager getSharedInstance] getRDLCharges];
    sharedInstance.RDLRider = [[DBManager getSharedInstance] getRDLRider];
    //SIP
    sharedInstance.premiumBoost = [[DBManager getSharedInstance] getPremiumBoost];
    //*******************ULIP*******************
    
    sharedInstance.MDLModalFactor = [[DBManager getSharedInstance] getModalFactorDetails];
    sharedInstance.OCLOccupation = [[DBManager getSharedInstance] getOCLOccupation];
    sharedInstance.PGLPlanGroup = [[DBManager getSharedInstance] getPGLPlanGroup];
    sharedInstance.PMLPremiumMultiple = [[DBManager getSharedInstance] getPMLPremiumMultiple];
    sharedInstance.PNLPlan = [[DBManager getSharedInstance] getPNLPlan];
    sharedInstance.PGLGrpMinPrem = [[DBManager getSharedInstance] getPGLGrpMinPrem];
    sharedInstance.PNLMinPrem = [[DBManager getSharedInstance] getPNLMinPrem];
    sharedInstance.PmdPlanPremMultiDisc = [[DBManager getSharedInstance] getPmdPlanPremiumMultDisc];
    sharedInstance.PBAProductBand = [[DBManager getSharedInstance] getPBAProductBand];
    sharedInstance.RDLRider = [[DBManager getSharedInstance] getRDLRider];
    sharedInstance.FDLFund = [[DBManager getSharedInstance] getFundDetails];
    sharedInstance.PMVPlanMaturityValue = [[DBManager getSharedInstance] getPMVPlanMaturityValue];
    sharedInstance.PFRPlanFormulaRef = [[DBManager getSharedInstance] getPFRPlanFormulaRef];
    sharedInstance.FEDFormulaExtDesc = [[DBManager getSharedInstance] getFEDFormulaExtDesc];
    sharedInstance.Address = [[DBManager getSharedInstance] getAddress];
    sharedInstance.PGFPlanGroupFeature = [[DBManager getSharedInstance] getPGFPlanGrpFeature];
    sharedInstance.GuaranteeMaturityBonusMaster = [[DBManager getSharedInstance] getGuaranteeMaturityBonusMaster];
    sharedInstance.XmlMetaDataMaster = [[DBManager getSharedInstance] getXMLMetaDataMaster];
    sharedInstance.MFMMaturityFactorMaster = [[DBManager getSharedInstance] getMFMmaturityFactorMaster];
    sharedInstance.GAIGuarAnnInChMaster = [[DBManager getSharedInstance] getGAIGuarAnnInChMaster];
    sharedInstance.SPVSysParamValues = [[DBManager getSharedInstance] getSPVSysParamValues];
    sharedInstance.IPCValues = [[DBManager getSharedInstance] getIPCValues];
    sharedInstance.agent_intermedDetails = [[DBManager getSharedInstance] getAgentDetail];
    //Added by Amruta on 11/9/2015 for FG Rider
    sharedInstance.ServiceMaster = [[DBManager getSharedInstance]getServiceTaxMaster];
    
    //Added by Amruta on 18/8/15
    sharedInstance.CommissionDetail = [[DBManager getSharedInstance] getCommissionDetail];
    
    //changes 16th oct SA
    sharedInstance.FOPList = [[DBManager getSharedInstance] getFOPRate];
    
    //sharedInstance.AAAFMCValues = [[DBManager getSharedInstance] getAAAFMCValues];
    
    return sharedInstance;
}

@end
