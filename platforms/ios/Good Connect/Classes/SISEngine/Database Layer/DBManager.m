//
//  DBManager.m
//  TestGitSetup
//
//  Created by Admin on 19/11/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "DBManager.h"



static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;


@implementation DBManager

#pragma mark- SINGLETON Method
+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

#pragma mark- PUBLIC METHODS
-(void)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",SIS]]];
    
    NSLog(@"THE DATABASE PATH is %@",databasePath);
    
    /*BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        NSString *defaultDataPath = [[NSBundle mainBundle] pathForResource:DATABASENAME ofType:DATABASETYPE];
        
        if(defaultDataPath){
            [filemgr copyItemAtPath:defaultDataPath toPath:databasePath error:NULL];
        }
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            sqlite3_close(database);
            return  isSuccess;
        }
        else
        {
            isSuccess = NO;
            NSLog(@"Failed to open database");
        }
        
    }
    return isSuccess;*/
}

-(NSString *)GetDataBasePath:(NSString *)_dbName{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    //create the path to database.
    NSString *dbPath = [[NSString alloc] initWithString:
                        [docsDir stringByAppendingPathComponent: _dbName]];
    
    return dbPath;
}
//  databasePath = [self GetDataBasePath:@"LifePlanner.db"];

-(ValidatePrem *)getValuesForPremium:(int )pglID andWithMode:(NSString *)mode{
    ValidatePrem *valPrem = NULL;
    
    const char *dbpath = [databasePath UTF8String];
    
    NSString *query = [NSString stringWithFormat:@"SELECT PGL_MIN_PREM_AMT,PGL_MAX_PREM_AMT from LP_PGL_GRP_MIN_PREM_LK where PGL_GRP_ID = '%d' and PGL_PREMIUM_MODE = '%@'",pglID,mode];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        const char *query_stmt = [query UTF8String];
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if(sqlite3_step(statement) == SQLITE_ROW){
                valPrem = [[ValidatePrem alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) !=NULL){
                    NSString *PGL_MIN_PREM_AMT =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    valPrem.minPrem = [PGL_MIN_PREM_AMT longLongValue];
                }else
                valPrem.minPrem = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) !=NULL){
                    NSString *PGL_MAX_PREM_AMT =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                    valPrem.maxPrem= [PGL_MAX_PREM_AMT longLongValue];
                }else
                valPrem.maxPrem = 0;
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
        
    }
    return valPrem;
}

//2nd march
-(double)getPremMultFoMRS:(NSString *)oclCode{
    double premMult = 0;
    const char *dbpath = [[self GetDataBasePath:[NSString stringWithFormat:@"%@",SIS]] UTF8String];
    
    NSString *query = [NSString stringWithFormat:@"select RML_PREM_MULT from LP_RML_RIDER_PREM_MULTIPLE_LK where RML_BAND = (select OCL_CLASS from LP_OCL_OCCUPATION_LK where OCL_CODE = '%@') and RML_RIDER_CD = 'ADDLN1V1'",oclCode];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        const char *query_stmt = [query UTF8String];
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if(sqlite3_step(statement) == SQLITE_ROW){
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *RML_PREM_MULT =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    premMult = [RML_PREM_MULT doubleValue];
                }else
                premMult = 0;
            }
        }
    }
    NSLog(@"the valie of prem mult for MRS for code %@ is %f",oclCode,premMult);
    return premMult;
}

//get the modal factor Details from the database
-(NSMutableArray *)getModalFactorDetails{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    MDLModalFactor *factor = NULL;
    NSString *query = @"select MDL_ID,MDL_PGL_ID,MDL_PAY_FREQ,MDL_MODAL_FACTOR from LP_MDL_MODAL_FACTOR_LK";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
        const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
              factor = [[MDLModalFactor alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) !=NULL){
                    NSString *MDL_ID =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    factor.MDL_ID = [MDL_ID intValue];
                }else{
                    factor.MDL_ID = 0;
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *MDL_PGL_ID =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                    factor.MDL_PGL_ID = [MDL_PGL_ID intValue];
                }else{
                    factor.MDL_PGL_ID = 0;
                }
              
                if ((const char *) sqlite3_column_text(statement, 2)!=NULL){
                    NSString *MDL_PAY_FREQ =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                    factor.MDL_PAY_FREQ = MDL_PAY_FREQ;
                }else{
                    factor.MDL_PAY_FREQ = @"";
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *MDL_MODAL_FACTOR =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                    factor.MDL_MODAL_FACTOR = [MDL_MODAL_FACTOR doubleValue];
                }else{
                    factor.MDL_MODAL_FACTOR = 0.0;
                }
                
              [arr addObject:factor];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//get the occupation data..
-(NSMutableDictionary *)getOCLOccupation{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    OCLOccupation *occupation = NULL;
    NSString *query = @"select OCL_ID,OCL_CODE,OCL_DESCRIPTION,OCL_OCCUPATION,OCL_BUSINESS,OCL_BLIFE,OCL_WP,OCL_CLASS from LP_OCL_OCCUPATION_LK";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
        const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                occupation = [[OCLOccupation alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *OCL_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    occupation.OCL_ID = [OCL_ID intValue];
                }else{
                    occupation.OCL_ID = 0;
                }
                
                NSString *OCL_CODE = NULL;
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    
                    OCL_CODE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 1)];
                    occupation.OCL_CODE = OCL_CODE;
                }else
                    occupation.OCL_CODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *OCL_DESCRIPTION =   [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 2)];
                    occupation.OCL_DESCRIPTION = OCL_DESCRIPTION;
                }else
                    occupation.OCL_DESCRIPTION = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *OCL_OCCUPATION =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 3)];
                    occupation.OCL_OCCUPATION = OCL_OCCUPATION;
                }else
                    occupation.OCL_OCCUPATION = @"";
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *OCL_BUSINESS =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                    occupation.OCL_BUSINESS = OCL_BUSINESS;
                }else
                    occupation.OCL_BUSINESS = @"";
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                    NSString *OCL_BLIFE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 5)];
                    occupation.OCL_BLIFE = [OCL_BLIFE doubleValue];
                }else
                    occupation.OCL_BLIFE = 0;
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                    NSString *OCL_WP =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 6)];
                    occupation.OCL_WP = [OCL_WP doubleValue];
                }else
                    occupation.OCL_WP = 0;
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                    NSString *OCL_CLASS =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 7)];
                    occupation.OCL_CLASS = [OCL_CLASS intValue];
                }else
                    occupation.OCL_CLASS = 0;
                
                [dict setValue:occupation forKey:OCL_CODE];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

//get the plan in plan group..
-(NSMutableDictionary *)getPGLPlanGroup{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    PGLPlanGroup *planGrp = NULL;
    NSString *query = @"select PGL_ID,PGL_DESCRIPTION,PGL_JUVENILE_IND,PGL_ADULT_IND,PGL_PRODUCT_TYPE from LP_PGL_PLAN_GROUP_LK";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                planGrp = [[PGLPlanGroup alloc] init];
                
                NSString *PGL_ID = NULL;
                if((const char *) sqlite3_column_text(statement, 0)){
                    PGL_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    planGrp.PGL_ID  = [PGL_ID intValue];
                }else
                    planGrp.PGL_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1)){
                    NSString *PGL_DESCRIPTION =   [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 1)];
                    planGrp.PGL_DESCRIPTION  = PGL_DESCRIPTION;
                }else
                    planGrp.PGL_DESCRIPTION = @"";
                    
                if((const char *) sqlite3_column_text(statement, 2)){
                    NSString *PGL_JUVENILE_IND =   [[NSString alloc] initWithUTF8String:
                                                    (const char *) sqlite3_column_text(statement, 2)];
                    planGrp.PGL_JUVENILE_IND  = PGL_JUVENILE_IND;
                }else
                    planGrp.PGL_JUVENILE_IND = @"";
                    
                if((const char *) sqlite3_column_text(statement, 3)){
                    NSString *PGL_ADULT_IND =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 3)];
                    planGrp.PGL_ADULT_IND  = PGL_ADULT_IND;
                }else
                    planGrp.PGL_ADULT_IND = @"";
                
                if((const char *) sqlite3_column_text(statement, 4)){
                    NSString *PGL_PRODUCT_TYPE =   [[NSString alloc] initWithUTF8String:
                                                    (const char *) sqlite3_column_text(statement, 4)];
                    planGrp.PGL_PRODUCT_TYPE  = PGL_PRODUCT_TYPE;
                }else
                    planGrp.PGL_PRODUCT_TYPE = @"";
                
                [dict setValue:planGrp forKey:PGL_ID];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

//get the premium multiple for plan..
-(NSMutableArray *)getPMLPremiumMultiple{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PMLPremiumMultiple *premMulti = NULL;
    NSString *query = @"select PML_ID,PML_PNL_ID,PML_SEX,PML_TERM,PML_AGE,PML_PREM_MULT,PML_BAND,SMOKER_FLAGAG from LP_PML_PREMIUM_MULTIPLE_LK";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                premMulti = [[PMLPremiumMultiple alloc] init];
                
                if( (const char *) sqlite3_column_text(statement, 0) !=NULL){
                NSString *PML_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    premMulti.PML_ID = [PML_ID intValue];
                }else
                    premMulti.PML_ID = 0;
                
                if( (const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PML_PNL_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 1)];
                    premMulti.PML_PNL_ID = [PML_PNL_ID intValue];
                }else
                    premMulti .PML_PNL_ID = 0;
                
                if( (const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PML_SEX =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    premMulti.PML_SEX = PML_SEX;
                }else{
                    premMulti.PML_SEX = @"";
                }

                if( (const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PML_TERM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 3)];
                    premMulti.PML_TERM = [PML_TERM intValue];
                }else
                    premMulti.PML_TERM = 0;
                
                if( (const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *PML_AGE =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 4)];
                    premMulti.PML_AGE = [PML_AGE intValue];
                }else
                    premMulti.PML_AGE = 0;
                
                if( (const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *PML_PREM_MULT =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 5)];
                    premMulti.PML_PREM_MULT = [PML_PREM_MULT doubleValue];
                }else
                    premMulti.PML_PREM_MULT = 0;
                
                if( (const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *PML_BAND =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 6)];
                    premMulti.PML_BAND = [PML_BAND intValue];
                }else
                    premMulti.PML_BAND = 0;
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                   NSString *SMOKER_FLAG =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 7)];
                   premMulti.SMOKER_FLAG = SMOKER_FLAG;
                }else{
                   premMulti.SMOKER_FLAG = @"";
                }
                
                [arr addObject:premMulti];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//get the plan details for particular plan..
-(NSMutableDictionary *)getPNLPlan{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    PNLPlan *plan = NULL;
    NSString *query = @"SELECT PNL_ID,PNL_CODE,PNL_DESCRIPTION,PNL_PGL_ID,PNL_TYPE,PNL_EFFECTIVE_DATE,PNL_END_DATE,PNL_MIN_AGE,PNL_MAX_AGE,PNL_MIN_SUM_ASSURED,PNL_MAX_SUM_ASSURED,PNL_MIN_OCCUPATION_CLASS,PNL_MAX_OCCUPATION_CLASS,PNL_PREMIUM_MULTIPLE,PNL_MAX_PREMIUM_PAYING_TERM,PNL_PAMXUSE,PNL_FREQUENCY,PNL_SERVICE_TAX,PNL_PRREM_MULT_FORMULA,UIN,PNL_SA_MULTIPLEOF,PNL_AGENTTYPE,PNL_ANNUITY_FREQUENCY,URALLOW,URPERCENT,PNL_START_PT,PNL_EXP_BONUS_RATE FROM LP_PNL_PLAN_LK";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                plan = [[PNLPlan alloc] init];
                
                if( (const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *PNL_ID =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    plan.PNL_ID  = [PNL_ID intValue];
                }else
                    plan.PNL_ID = 0;
                
                NSString *PNL_CODE = NULL;
                if( (const char *) sqlite3_column_text(statement, 1) != NULL){
                    PNL_CODE =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 1)];
                    plan.PNL_CODE  = PNL_CODE;
                }else
                    plan.PNL_CODE = @"";
                
                if( (const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PNL_DESCRIPTION =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    plan.PNL_DESCRIPTION  = PNL_DESCRIPTION;
                }else
                    plan.PNL_DESCRIPTION = @"";
                
                if( (const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PNL_PGL_ID =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 3)];
                    plan.PNL_PGL_ID  = [PNL_PGL_ID intValue];
                }else
                    plan.PNL_PGL_ID = 0;
                
                if( (const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *PNL_TYPE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                    plan.PNL_TYPE  = PNL_TYPE;
                }else
                    plan.PNL_TYPE = @"";
                
                if(sqlite3_column_text(statement, 5) != NULL){
                NSString *PNL_EFFECTIVE_DATE =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 5)];
                    plan.PNL_EFFECTIVE_DATE  = [formatter dateFromString:PNL_EFFECTIVE_DATE];
                }else{
                    plan.PNL_EFFECTIVE_DATE  = NULL;
                }
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *PNL_END_DATE =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 6)];
                
                    plan.PNL_END_DATE  = [formatter dateFromString:PNL_END_DATE];
                }else {
                    plan.PNL_END_DATE  = NULL;
                }
                
                if( (const char *) sqlite3_column_text(statement, 7) != NULL){
                NSString *PNL_MIN_AGE =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 7)];
                    plan.PNL_MIN_AGE  = [PNL_MIN_AGE intValue];
                }else
                    plan.PNL_MIN_AGE = 0;
                
                if( (const char *) sqlite3_column_text(statement, 8) != NULL){
                NSString *PNL_MAX_AGE =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 8)];
                    plan.PNL_MAX_AGE  = [PNL_MAX_AGE intValue];
                }else
                    plan.PNL_MAX_AGE = 0;
                
                if((const char *) sqlite3_column_text(statement, 9) != NULL){
                NSString *PNL_MIN_SUM_ASSURED =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 9)];
                    plan.PNL_MIN_SUM_ASSURED  = [PNL_MIN_SUM_ASSURED intValue];
                }else {
                    plan.PNL_MIN_SUM_ASSURED = 0;
                }
                
                if((const char *) sqlite3_column_text(statement, 10) != NULL){
                NSString *PNL_MAX_SUM_ASSURED =   [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 10)];
                    plan.PNL_MAX_SUM_ASSURED  = [PNL_MAX_SUM_ASSURED longLongValue];
                }else {
                    plan.PNL_MAX_SUM_ASSURED = 0;
                }
                
                if( (const char *) sqlite3_column_text(statement, 11) != NULL ){
                NSString *PNL_MIN_OCCUPATION_CLASS =   [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 11)];
                    plan.PNL_MIN_OCCUPATION_CLASS  = [PNL_MIN_OCCUPATION_CLASS intValue];
                }else
                    plan.PNL_MIN_OCCUPATION_CLASS = 0;
                
                if((const char *) sqlite3_column_text(statement, 12)!=NULL){
                NSString *PNL_MAX_OCCUPATION_CLASS =   [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 12)];
                    plan.PNL_MAX_OCCUPATION_CLASS  = [PNL_MAX_OCCUPATION_CLASS intValue];
                }else{
                    plan.PNL_MAX_OCCUPATION_CLASS  = 0;
                }
                if( (const char *) sqlite3_column_text(statement, 13)!=NULL){
                NSString *PNL_PREMIUM_MULTIPLE =   [[NSString alloc] initWithUTF8String:
                                                        (const char *) sqlite3_column_text(statement, 13)];
                    plan.PNL_PREMIUM_MULTIPLE  = [PNL_PREMIUM_MULTIPLE intValue];
                }else{
                    plan.PNL_PREMIUM_MULTIPLE  = 0;
                }
                
                if( (const char *) sqlite3_column_text(statement, 14) != NULL){
                NSString *PNL_MAX_PREMIUM_PAYING_TERM =   [[NSString alloc] initWithUTF8String:
                                                    (const char *) sqlite3_column_text(statement, 14)];
                    plan.PNL_MAX_PREMIUM_PAYING_TERM  = [PNL_MAX_PREMIUM_PAYING_TERM intValue];
                }else
                    plan.PNL_MAX_PREMIUM_PAYING_TERM = 0;
                
                if( (const char *) sqlite3_column_text(statement, 15) != NULL){
                NSString *PNL_PMAXUSE =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 15)];
                    plan.PNL_PMAXUSE  = PNL_PMAXUSE;
                }else
                    plan.PNL_PMAXUSE = @"";
                
                if( (const char *) sqlite3_column_text(statement, 16) != NULL){
                NSString *PNL_FREQUENCY =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 16)];
                    plan.PNL_FREQUENCY  = PNL_FREQUENCY;
                }else
                    plan.PNL_FREQUENCY = @"";
                
                if( (const char *) sqlite3_column_text(statement, 17) != NULL){
                NSString *PNL_SERVICE_TAX =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 17)];
                    plan.PNL_SERVICE_TAX  = [PNL_SERVICE_TAX doubleValue];
                }else
                    plan.PNL_SERVICE_TAX = 0;
                
                if( (const char *) sqlite3_column_text(statement, 18) != NULL){
                NSString *PNL_PREM_MULT_FORMULA =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 18)];
                    plan.PNL_PREM_MULT_FORMULA  = [PNL_PREM_MULT_FORMULA doubleValue];
                }else{
                    plan.PNL_PREM_MULT_FORMULA = 0;
                }
                
                if( (const char *) sqlite3_column_text(statement, 19) != NULL ){
                NSString *UIN =   [[NSString alloc] initWithUTF8String:
                                                     (const char *) sqlite3_column_text(statement, 19)];
                    plan.UIN  = UIN;
                }else
                    plan.UIN = @"";
                
                if( (const char *) sqlite3_column_text(statement, 20) != NULL ){
                NSString *PNL_SA_MULTIPLE =   [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 20)];
                    plan.PNL_SA_MULTIPLE  = [PNL_SA_MULTIPLE intValue];
                }else
                    plan.PNL_SA_MULTIPLE = 0;
                
                if( (const char *) sqlite3_column_text(statement, 21) != NULL){
                NSString *PNL_AGENTTYPE =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 21)];
                    plan.PNL_AGENTTYPE  = PNL_AGENTTYPE;
                }else
                    plan.PNL_AGENTTYPE = @"";
                
                if( (const char *) sqlite3_column_text(statement, 22) != NULL){
                NSString *PNL_ANNUITY_FREQUENCY =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 22)];
                    plan.PNL_ANNUITY_FREQUENCY  = PNL_ANNUITY_FREQUENCY;
                }else
                    plan.PNL_ANNUITY_FREQUENCY = @"";

                if((const char *) sqlite3_column_text(statement, 23) != NULL){
                NSString *URALLOW =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 23)];
                    plan.URALLOW  = URALLOW;
                }else{
                    plan.URALLOW  = @"";
                }
                
                if((const char *) sqlite3_column_text(statement, 24) != NULL){
                NSString *PNL_URPERCENT =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 24)];
                    plan.PNL_URPERCENT  = [PNL_URPERCENT doubleValue];
                }else{
                    plan.PNL_URPERCENT  = 0;
                }
                
                if( (const char *) sqlite3_column_text(statement, 25) != NULL){
                NSString *PNL_START_PT =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 25)];
                    plan.PNL_START_PT  = PNL_START_PT;
                }else
                    plan.PNL_START_PT = @"";
                
                if( (const char *) sqlite3_column_text(statement, 26) != NULL){
                NSString *PNL_BONUS_RATE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 26)];
                    plan.PNL_BONUS_RATE  = PNL_BONUS_RATE;
                }else
                    plan.PNL_BONUS_RATE = @"";
                
                [dict setValue:plan forKey:PNL_CODE];
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

//***********************ULIP***********************

//*******************RIDER*******************
-(NSMutableArray *)getRMLRiderPremMultiple{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    RMLRiderPremMultiple *riderMul = nil;
    NSString *query = @"SELECT RML_ID,RML_RIDER_CD,RML_SEX,RML_TERM,RML_AGE,RML_PREM_MULT,RML_BAND from LP_RML_RIDER_PREM_MULTIPLE_LK";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                riderMul = [[RMLRiderPremMultiple alloc] init];
                
                if((const char *)sqlite3_column_text(statement, 0) != NULL){
                    NSString *RML_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    riderMul.RML_ID = [RML_ID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 1) != NULL){
                    NSString *RML_RIDER_CD = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    riderMul.RML_RIDER_CD = RML_RIDER_CD;
                }
                if((const char *)sqlite3_column_text(statement, 2) != NULL){
                    NSString *RML_SEX = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    riderMul.RML_SEX = RML_SEX;
                }
                if((const char *)sqlite3_column_text(statement, 3) != NULL){
                    NSString *RML_TERM = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    riderMul.RML_TERM = [RML_TERM intValue];
                }
                if((const char *)sqlite3_column_text(statement, 4) != NULL){
                    NSString *RML_AGE = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                    riderMul.RML_AGE = [RML_AGE intValue];
                }
                if((const char *)sqlite3_column_text(statement, 5) != NULL){
                    NSString *RML_PREM_MULT = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
                    //changes 3 feb..
                    riderMul.RML_PREM_MULT = [RML_PREM_MULT doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 6) != NULL){
                    NSString *RML_BAND = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                    riderMul.RML_BAND = [RML_BAND intValue];
                }
                
                [arr addObject:riderMul];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

-(NSMutableDictionary *) getRDLRider{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    Rider *rider = nil;
    NSString *query = @"SELECT RDL_ID,RDL_CODE,RDL_DESCRIPTION,RDL_RTL_ID,RDL_SERVICE_TAX,ISSIMPLIFIED,ISANNUITYPRODUCT,ISBACKDATE,NOOFMONTH,NBFC, FIXCVGID,MINOCC,MAXOCC,RDL_UIN FROM LP_RDL_RIDER_LK";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                rider = [[Rider alloc] init];
                NSString *RDL_CODE = @"";
                
                if((const char *)sqlite3_column_text(statement, 0) != NULL){
                    NSString *RDL_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    rider.RDLID = [RDL_ID longLongValue];
                }
                if((const char *)sqlite3_column_text(statement, 1) != NULL){
                    RDL_CODE = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    rider.RDL_CODE = RDL_CODE;
                }
                if((const char *)sqlite3_column_text(statement, 2) != NULL){
                    NSString *RDL_DESCRIPTION = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    rider.RDL_DESCRIPTION = RDL_DESCRIPTION;
                }
                if((const char *)sqlite3_column_text(statement, 3) != NULL){
                    NSString *RDL_RTL_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    rider.RDL_RTL_ID = [RDL_RTL_ID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 4) != NULL){
                    NSString *RDL_SERVICE_TAX = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                    rider.RDL_SERVICE_TAX = [RDL_SERVICE_TAX doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 5) != NULL){
                    NSString *ISSIMPLIFIED = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
                    rider.ISSIMPLIFIED = ISSIMPLIFIED;
                }
                if((const char *)sqlite3_column_text(statement, 6) != NULL){
                    NSString *ISANNUITYPRODUCT = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                    rider.ISANNUITYPRODUCT = ISANNUITYPRODUCT;
                }
                if((const char *)sqlite3_column_text(statement, 7) != NULL){
                    NSString *ISBACKDATE = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 7)];
                    rider.ISBACKDATE = ISBACKDATE;
                }
                if((const char *)sqlite3_column_text(statement, 8) != NULL){
                    NSString *NOOFMONTH = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 8)];
                    rider.NOOFMONTH = [NOOFMONTH intValue];
                }
                if((const char *)sqlite3_column_text(statement, 9) != NULL){
                    NSString *NBFC = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 9)];
                    rider.NBFC = NBFC;
                }
                if((const char *)sqlite3_column_text(statement, 10) != NULL){
                    NSString *FIXCVGID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 10)];
                    rider.FIXCVGID = [FIXCVGID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 11) != NULL){
                    NSString *MINOCC = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 11)];
                    rider.MINOCC = [MINOCC intValue];
                }
                if((const char *)sqlite3_column_text(statement, 12) != NULL){
                    NSString *MAXOCC = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 13)];
                    rider.MAXOCC = [MAXOCC intValue];
                }
                if((const char *)sqlite3_column_text(statement, 13) != NULL){
                    NSString *RDL_UIN = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 13)];
                    rider.RDL_UIN = RDL_UIN;
                }
                
                [dict setObject:rider forKey:RDL_CODE];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

-(NSMutableArray *)getRDLCharges{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    RDLChargesValue *charge = nil;
    NSString *query = @"SELECT RCS_ID, RCS_PNL_ID, RCS_RDL_ID, RCS_GENDER, RCS_BAND, RCS_AGE, RCS_OCCUR, RCS_NSP, RCS_USE, RCS_NUM, RCS_DUR1, RCS_CVAL1, RCS_DUR2,RCS_CVAL2,RCS_DUR3,RCS_CVAL3,RCS_DUR4,RCS_CVAL4,RCS_DUR5,RCS_CVAL5,RCS_DUR6,RCS_CVAL6,RCS_DUR7,RCS_CVAL7,RCS_DUR8,RCS_CVAL8,RCS_DUR9,RCS_CVAL9,RCS_DUR10,RCS_CVAL10 FROM LP_RDL_CHARGES";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                charge = [[RDLChargesValue alloc] init];
                if((const char *)sqlite3_column_text(statement, 0) != NULL){
                    NSString *RCS_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    charge.RCS_ID = [RCS_ID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 1) != NULL){
                    NSString *RCS_PNL_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    charge.RCS_PNL_ID = [RCS_PNL_ID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 2) != NULL){
                    NSString *RCS_RDL_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    charge.RCS_RDL_ID = [RCS_RDL_ID intValue];
                }
                if((const char *)sqlite3_column_text(statement, 3) != NULL){
                    NSString *RCS_GENDER = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    charge.RCS_GENDER = RCS_GENDER;
                }
                if((const char *)sqlite3_column_text(statement, 4) != NULL){
                    NSString *RCS_BAND = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                    charge.RCS_BAND = RCS_BAND;
                }
                if((const char *)sqlite3_column_text(statement, 5) != NULL){
                    NSString *RCS_AGE = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
                    charge.RCS_AGE = [RCS_AGE intValue];
                }
                if((const char *)sqlite3_column_text(statement, 6) != NULL){
                    NSString *RCS_OCCUR = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                    charge.RCS_OCCUR = RCS_OCCUR;
                }
                if((const char *)sqlite3_column_text(statement, 7) != NULL){
                    NSString *RCS_NSP = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 7)];
                    charge.RCS_NSP = [RCS_NSP doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 8) != NULL){
                    NSString *RCS_USE = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 8)];
                    charge.RCS_USE = RCS_USE;
                }
                if((const char *)sqlite3_column_text(statement, 9) != NULL){
                    NSString *RCS_NUM = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 9)];
                    charge.RCS_NUM = [RCS_NUM intValue];
                }
                if((const char *)sqlite3_column_text(statement, 10) != NULL){
                    NSString *RCS_DUR1 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 10)];
                    charge.RCS_DUR1 = [RCS_DUR1 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 11) != NULL){
                    NSString *RCS_CVAL1 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 11)];
                    charge.RCS_CVAL1 = [RCS_CVAL1 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 12) != NULL){
                    NSString *RCS_DUR2 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 12)];
                    charge.RCS_DUR2 = [RCS_DUR2 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 13) != NULL){
                    NSString *RCS_CVAL2 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 13)];
                    charge.RCS_CVAL2 = [RCS_CVAL2 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 14) != NULL){
                    NSString *RCS_DUR3 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 14)];
                    charge.RCS_DUR3 = [RCS_DUR3 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 15) != NULL){
                    NSString *RCS_CVAL3 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 15)];
                    charge.RCS_CVAL3 = [RCS_CVAL3 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 16) != NULL){
                    NSString *RCS_DUR4 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 16)];
                    charge.RCS_DUR4 = [RCS_DUR4 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 17) != NULL){
                    NSString *RCS_CVAL4 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 17)];
                    charge.RCS_CVAL4 = [RCS_CVAL4 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 18) != NULL){
                    NSString *RCS_DUR5 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 18)];
                    charge.RCS_DUR5 = [RCS_DUR5 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 19) != NULL){
                    NSString *RCS_CVAL5 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 19)];
                    charge.RCS_CVAL5 = [RCS_CVAL5 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 20) != NULL){
                    NSString *RCS_DUR6 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 20)];
                    charge.RCS_DUR6 = [RCS_DUR6 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 21) != NULL){
                    NSString *RCS_CVAL6 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 21)];
                    charge.RCS_CVAL6 = [RCS_CVAL6 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 22) != NULL){
                    NSString *RCS_DUR7 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 22)];
                    charge.RCS_DUR7 = [RCS_DUR7 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 23) != NULL){
                    NSString *RCS_CVAL7 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 23)];
                    charge.RCS_CVAL7 = [RCS_CVAL7 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 24) != NULL){
                    NSString *RCS_DUR8 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 24)];
                    charge.RCS_DUR8 = [RCS_DUR8 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 25) != NULL){
                    NSString *RCS_CVAL8 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 25)];
                    charge.RCS_CVAL8 = [RCS_CVAL8 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 26) != NULL){
                    NSString *RCS_DUR9 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 26)];
                    charge.RCS_DUR9 = [RCS_DUR9 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 27) != NULL){
                    NSString *RCS_CVAL9 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 27)];
                    charge.RCS_CVAL9 = [RCS_CVAL9 doubleValue];
                }
                if((const char *)sqlite3_column_text(statement, 28) != NULL){
                    NSString *RCS_DUR10 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 28)];
                    charge.RCS_DUR10 = [RCS_DUR10 intValue];
                }
                if((const char *)sqlite3_column_text(statement, 29) != NULL){
                    NSString *RCS_CVAL10 = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 29)];
                    charge.RCS_CVAL10 = [RCS_CVAL10 doubleValue];
                }
            
                [arr addObject:charge];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//*******************RIDER*******************

//*******************FUND*******************
-(NSMutableDictionary *) getFundDetails{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    FundDetails *details = NULL;
    NSString *query = @"SELECT DISTINCT PNL.PNL_PGL_ID AS PGL_ID, FDL.FDL_CODE AS CODE,FDL.FDL_DESCRIPTION||' ('||FDL.SFIN_NUMBER||')' AS DESCRIPTION,FDL.FDL_FUND_MGMT_CHRG AS CHARGE,FDL.FDL_PERFORMANCE_3M AS PERF_3M,FDL.FDL_PERFORMANCE_6M AS PERF_6M,FDL.FDL_PERFORMANCE_1Y AS PERF_1Y,FDL.FDL_PERFORMANCE_2Y AS PERF_2Y,FDL.FDL_PERFORMANCE_3Y AS PERF_3Y,FDL.FDL_PERFORMANCE_4Y AS PERF_4Y,FDL.FDL_PERFORMANCE_5Y AS PERF_5Y,FDL.FDL_PERFORMANCE_INCEPTION AS PERF_INCEPTION FROM LP_PFX_PLAN_FUND_XREF PFX,LP_PNL_PLAN_LK PNL,LP_FDL_FUND_LK FDL WHERE PFX.PFX_PNL_ID=PNL.PNL_ID AND PFX.PFX_FDL_ID=FDL.FDL_ID AND	(PFX.END_DATE >= DATE('NOW') OR  PFX.END_DATE IS NULL ) ORDER BY  PNL.PNL_PGL_ID;";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                details = [[FundDetails alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PNL_PGD_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    details.PNL_PGD_ID = [PNL_PGD_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *CODE =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                    details.Code = CODE;
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *description =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    details.fund_Description = description;
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *charge =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 3)];
                    details.Fund_Mgmt_charge = [charge doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *perf3M =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                    details.PERF_3M = [perf3M doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                    NSString *perf6M =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 5)];
                    details.PERF_6M= [perf6M doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                    NSString *perf1Y =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 6)];
                    details.PERF_1Y = [perf1Y doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                    NSString *perf2Y =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 7)];
                    details.PERF_2Y = [perf2Y doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 8) != NULL){
                    NSString *perf3Y =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 8)];
                    details.PERF_3Y = [perf3Y doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 9) != NULL){
                    NSString *perf4Y =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 9)];
                    details.PERF_4Y = [perf4Y doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 10) != NULL){
                    NSString *perf5Y =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 10)];
                    details.PERF_5Y = [perf5Y doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 11) != NULL){
                    NSString *fdl_perf_inspection =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 11)];
                    details.PERF_Inception = [fdl_perf_inspection doubleValue];
                }
                
                [dict setObject:details forKey:details.Code];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}
//*******************FUND*******************

-(NSMutableDictionary *) getPCRPlanChargesRef{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    PCRPlanChargesRef *planChargeRef = NULL;
    NSString *query = @"SELECT PCR_PNL_ID,PCR_ADMIN_ID,PCR_PREM_ID,PCR_MORT_ID,PCR_WOP_ID,PCR_FIB_ID,PCR_SUR_ID,PCR_GMM_ID,PCR_COMM_ID FROM LP_PCR_PLAN_CHARGES_REF";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                planChargeRef = [[PCRPlanChargesRef alloc] init];
                
                if((const char *)sqlite3_column_text(statement, 0) != NULL){
                    NSString *pcr_pnl_id = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    planChargeRef.PCR_PNL_ID = [pcr_pnl_id intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 1) != NULL){
                    NSString *pcr_admin_id = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    planChargeRef.PCR_ADMIN_ID = [pcr_admin_id intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 2) != NULL){
                    NSString *PCR_PREM_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    planChargeRef.PCR_PREM_ID = [PCR_PREM_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 3) != NULL){
                    NSString *PCR_MORT_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    planChargeRef.PCR_MORT_ID = [PCR_MORT_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 4) != NULL){
                    NSString *PCR_WOP_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                    planChargeRef.PCR_WOP_ID = [PCR_WOP_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 5) != NULL){
                    NSString *PCR_FIB_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
                    planChargeRef.PCR_FIB_ID = [PCR_FIB_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 6) != NULL){
                    NSString *PCR_SUR_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                    planChargeRef.PCR_SUR_ID = [PCR_SUR_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 7) != NULL){
                    NSString *PCR_GMM_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 7)];
                    planChargeRef.PCR_GMM_ID = [PCR_GMM_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 8) != NULL){
                    NSString *PCR_COMM_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 8)];
                    planChargeRef.PCR_COMM_ID = [PCR_COMM_ID intValue];
                }
                
                [dict setObject:planChargeRef forKey:[NSNumber numberWithInt: planChargeRef.PCR_PNL_ID]];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

-(NSMutableArray *) getPACPremiumAllocationCharges{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PACPremiumAllocationCharges *charges = NULL;
    NSString *query = @"SELECT PAC_ID,PAC_MIN_BAND,PAC_MAX_BAND,PAC_MIN_PREM,PAC_MAX_PREM,PAC_CHARGE FROM LP_PAC_PREMIUM_ALLOCATION_CHARGES";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                charges = [[PACPremiumAllocationCharges alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *PAC_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    charges.PAC_ID = [PAC_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *PAC_MIN_BAND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    charges.PAC_MIN_BAND = [PAC_MIN_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *PAC_MAX_BAND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    charges.PAC_MAX_BAND = [PAC_MAX_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *PAC_MIN_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 3)];
                    charges.PAC_MIN_PREM = [PAC_MIN_PREM longLongValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *PAC_MAX_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                    charges.PAC_MAX_PREM = [PAC_MAX_PREM longLongValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                    NSString *PAC_CHARGE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 5)];
                    charges.PAC_CHARGE = [PAC_CHARGE doubleValue];
                }
                
                [arr addObject:charges];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

-(NSMutableArray *) getCCMCommissionChargeMaster{
    const char *dbPath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    CCMommissionChargeMaster *commMaster = NULL;
    NSString *query = @"SELECT CCM_ID,CCM_MIN_TERM,CCM_MAX_TERM,CCM_COMMISSION FROM LP_CCM_COMMISSION_CHARGE_MASTER";
    
    if(sqlite3_open(dbPath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW) {
                commMaster = [[CCMommissionChargeMaster alloc] init];
                
                if((const char *)sqlite3_column_text(statement, 0) != NULL){
                    NSString *CCM_ID = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    commMaster.CCM_ID = [CCM_ID intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 1) != NULL){
                    NSString *CCM_MIN_TERM = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    commMaster.CCM_MIN_TERM = [CCM_MIN_TERM intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 2) != NULL){
                    NSString *CCM_MAX_TERM = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    commMaster.CCM_MAX_TERM = [CCM_MAX_TERM intValue];
                }
                
                if((const char *)sqlite3_column_text(statement, 3) != NULL){
                    NSString *CCM_COMMISSION = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    commMaster.CCM_COMMISSION = [CCM_COMMISSION doubleValue];
                }
                
                [arr addObject:commMaster];
            }
        }
    }
    
    return arr;
}

-(NSMutableArray *) getMCMMortilityChargesMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    MCMMortilityChargesMaster *mortilityChargeMaster = NULL;
    NSString *query = @"SELECT MCM_ID,MCM_AGE,MCM_MORT_CHARGE,MCM_SEX FROM LP_MCM_MORTILITY_CHARGES_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                mortilityChargeMaster = [[MCMMortilityChargesMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) !=NULL){
                    NSString *MCM_ID =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    mortilityChargeMaster.MCM_ID = [MCM_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) !=NULL){
                    NSString *MCM_AGE =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                    mortilityChargeMaster.MCM_AGE = [MCM_AGE intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) !=NULL){
                    NSString *MCM_MORT_CHARGE =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                    mortilityChargeMaster.MCM_MORT_CHARGE = [MCM_MORT_CHARGE doubleValue];
                }
                //Changes for SA
                if((const char *) sqlite3_column_text(statement, 3) !=NULL){
                    NSString *MFM_SEX =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                    mortilityChargeMaster.MFM_SEX = MFM_SEX;
                }
                
                [arr addObject:mortilityChargeMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

-(NSMutableArray *) getACMAdminChargeMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    ACMAdminChargeMaster *adminChargeMaster = NULL;
    NSString *query = @"SELECT ACM_ID,ACM_MIN_PREM,ACM_MAX_PREM,ACM_CHARGE from LP_ACM_ADMIN_CHARGES_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                adminChargeMaster = [[ACMAdminChargeMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *ACM_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    adminChargeMaster.ACM_ID = [ACM_ID intValue];
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *ACM_MIN_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    adminChargeMaster.ACM_MIN_PREM = [ACM_MIN_PREM longLongValue];
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *ACM_MAX_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    adminChargeMaster.ACM_MAX_PREM = [ACM_MAX_PREM longLongValue];
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *ACM_CHARGE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 3)];
                    adminChargeMaster.ACM_CHARGE = [ACM_CHARGE longLongValue];
                }
                
                [arr addObject:adminChargeMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

-(NSMutableArray *) getWCMWOPChargesMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    WCMWOPChargesMaster *wopChargeMaster = NULL;
    NSString *query = @"SELECT WCM_WOP_ID,WCM_AGE,WCM_BAND,WCM_CHARGE FROM LP_WCM_WOP_CHARGES_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                wopChargeMaster = [[WCMWOPChargesMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *WCM_WOP_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    wopChargeMaster.WCM_WOP_ID = [WCM_WOP_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *WCM_AGE =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                    wopChargeMaster.WCM_AGE = [WCM_AGE intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *WCM_BAND =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 2)];
                    wopChargeMaster.WCM_BAND = [WCM_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *WCM_CHARGE =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 3)];
                    wopChargeMaster.WCM_CHARGE = [WCM_CHARGE doubleValue];
                }
                
                [arr addObject:wopChargeMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return arr;
}

-(NSMutableArray *) getFCMFIBChargesMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    FCMFIBChargersMaster *fibChargeMaster = NULL;
    NSString *query = @"SELECT FCM_FIB_ID,FCM_AGE,FCM_BAND,FCM_CHARGE FROM LP_FCM_FIB_CHARGES_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                fibChargeMaster = [[FCMFIBChargersMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *FCM_FIB_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    fibChargeMaster.FCM_FIB_ID = [FCM_FIB_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *FCM_AGE =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                    fibChargeMaster.FCM_AGE = [FCM_AGE intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *FCM_BAND =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 2)];
                    fibChargeMaster.FCM_BAND = [FCM_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *FCM_CHARGE =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 3)];
                    fibChargeMaster.FCM_CHARGE = [FCM_CHARGE doubleValue];
                }
                
                [arr addObject:fibChargeMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

-(NSMutableArray *) getSURSurrenderChargeMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    SURSurrenderChargesMaster *surenderChargeMaster = NULL;
    NSString *query = @"SELECT SUR_ID,SUR_MIN_BAND,SUR_MAX_BAND,SUR_MIN_PREM,SUR_MAX_PREM,SUR_CHARGE,SUR_AMOUNT FROM LP_SUR_SURRENDER_CHARGE_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                surenderChargeMaster = [[SURSurrenderChargesMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *SUR_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    surenderChargeMaster.SUR_ID = [SUR_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *SUR_MIN_BAND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    surenderChargeMaster.SUR_MIN_BAND = [SUR_MIN_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *SUR_MAX_BAND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    surenderChargeMaster.SUR_MAX_BAND = [SUR_MAX_BAND intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *SUR_MIN_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 3)];
                    surenderChargeMaster.SUR_MIN_PREM = [SUR_MIN_PREM longLongValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *SUR_MAX_PREM =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                    surenderChargeMaster.SUR_MAX_PREM = [SUR_MAX_PREM longLongValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                    NSString *SUR_CHARGE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 5)];
                    surenderChargeMaster.SUR_CHARGE = [SUR_CHARGE doubleValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                    NSString *SUR_AMOUNT =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 6)];
                    surenderChargeMaster.SUR_AMOUNT = [SUR_AMOUNT longLongValue];
                }
                
                [arr addObject:surenderChargeMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}
//************************FRONT END ****************************
-(NSMutableArray *)getPremiumMultiple:(NSMutableDictionary *) data{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    int prodId = /*201;//*/[[data objectForKey:@"pglid"] intValue];
    int age = /*50;//*/[[data objectForKey:@"age"] intValue];
    int polTerm =/* 50;//*/[[data objectForKey:@"polTerm"] intValue];
    NSString *planCode =/* @"WMX1ULN1";//*/[data objectForKey:@"PlanCode"];
    
    NSLog(@"pglID %d%d%d%@",prodId,age,polTerm,planCode);
    const char *dbPath = [databasePath UTF8String];
    NSMutableString *query = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"SELECT DISTINCT PML_PREM_MULT FROM LP_PML_PREMIUM_MULTIPLE_LK,LP_PNL_PLAN_LK WHERE PNL_PGL_ID = '%d' AND PML_PNL_ID = PNL_ID AND '%d' BETWEEN PNL_MIN_AGE AND PNL_MAX_AGE AND (PNL_END_DATE IS NULL OR PNL_END_DATE >=DATE('NOW')) AND PML_AGE = (CASE WHEN PML_AGE= '999' THEN '999' else '%d' END) AND IFNULL(PML_TERM,0) = (CASE WHEN PNL_PRREM_MULT_FORMULA = '3' then '%d' else IFNULL(PML_TERM,0) END)",prodId,age,age,polTerm]];
    
    if (prodId == 199) {
        if (![planCode isEqualToString:@"IPR1ULN1"]) {
            [query appendString:@"AND PNL_CODE != 'IPR1ULN1'"];
        }else{
            [query appendString:@"AND PNL_CODE = 'IPR1ULN1'"];
        }
    }
    
    if (prodId == 182 || prodId == 201 || prodId == 200 || prodId == 202 || prodId == 199) {
        [query appendFormat:@"AND PNL_CODE ='%@' ORDER BY PML_PREM_MULT",planCode];
    }else
        [query appendString:@"ORDER BY PML_PREM_MULT"];
    
    NSLog(@"the query is %@",query);
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        const char *query_Stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if (sqlite3_prepare(database, query_Stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                float po_term;
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *prem_mult =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 0)];
                    po_term = [prem_mult floatValue];
                }
                
                if(prodId == 202){
                    float policyTerm = 0.0;
                    policyTerm = (float)polTerm;
                    
                    if ([planCode isEqualToString:@"WPR7ULN1"] || [planCode isEqualToString:@"WPR5ULN1"] || [planCode isEqualToString:@"WPR2ULN1"]) {
                        if (polTerm /2 > 30) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 30) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"WPR7ULN2"] || [planCode isEqualToString:@"WPR5ULN2"] || [planCode isEqualToString:@"WPR2ULN2"]) {
                        if (polTerm /2 > 20) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 20) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"WPR7ULN3"] || [planCode isEqualToString:@"WPR5ULN3"] || [planCode isEqualToString:@"WPR2ULN3"]) {
                        if (polTerm /2 > 15) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 15) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"WPR7ULN4"] || [planCode isEqualToString:@"WPR5ULN4"] || [planCode isEqualToString:@"WPR2ULN4"]) {
                        if (polTerm /2 > 10) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 10) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if([planCode isEqualToString:@"WPR1ULN1"]){
                        [arr addObject:[NSNumber numberWithFloat:po_term]];
                    }
                }else if(prodId == 199){
                    float policyTerm = 0.0;
                    policyTerm = (float)polTerm;
                    
                    if ([planCode isEqualToString:@"IPR7ULN1"] || [planCode isEqualToString:@"IPR5ULN1"] || [planCode isEqualToString:@"IPR2ULN1"]) {
                        if (polTerm /2 > 30) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 30) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"IPR7ULN2"] || [planCode isEqualToString:@"IPR5ULN2"] || [planCode isEqualToString:@"IPR2ULN2"]) {
                        if (polTerm /2 > 20) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 20) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"IPR7ULN3"] || [planCode isEqualToString:@"IPR5ULN3"] || [planCode isEqualToString:@"IPR2ULN3"]) {
                        if (polTerm /2 > 15) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 15) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if ([planCode isEqualToString:@"IPR7ULN4"] || [planCode isEqualToString:@"IPR5ULN4"] || [planCode isEqualToString:@"IPR2ULN4"]) {
                        if (polTerm /2 > 10) {
                            if (po_term <= (policyTerm/2)) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }else{
                            if (po_term <= 10) {
                                [arr addObject:[NSNumber numberWithFloat:po_term]];
                            }
                        }
                    }else if([planCode isEqualToString:@"IPR1ULN1"]){
                        [arr addObject:[NSNumber numberWithFloat:po_term]];
                    }
                }else
                    [arr addObject:[NSNumber numberWithFloat:po_term]];
                
                
            }
        }
    }
    
    return arr;
}

//************************FRONT END ****************************

-(NSMutableArray *) getServiceTaxMaster{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    ServiceTaxMaster *ServiceTax = NULL;
    NSString *query = @"SELECT STM_TYPE,STM_VALUE FROM LP_STM_SERVICETAX_MASTER";
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                ServiceTax = [[ServiceTaxMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *STM_TYPE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    ServiceTax.Stm_type = STM_TYPE;
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *STM_VALUE =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 1)];
                    ServiceTax.Stm_value = [STM_VALUE doubleValue];
                }
                
                [arr addObject:ServiceTax];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    return arr;
}

-(NSMutableArray *) getSTDTemplateValues{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    STDTemplateValues *values;
    NSString *query = @"SELECT STD_PGL_ID,STD_TEMP_DESC,STD_SEQUENCE,STD_MIN_STARTP,STD_MAX_STARTP,STD_MIN_PT,STD_MAX_PT,STD_NO_PAGES,STD_MANDATORY,STD_SHOW,STD_UPDATE_BY,STD_UPDATE_DT,STD_IS_RIDER,STD_RDL_CODE,STD_MAX_NO_PAGES FROM LP_STD_SIS_TEMPLATE_DATA ORDER BY STD_PGL_ID,STD_SEQUENCE";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                values = [[STDTemplateValues alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *STD_PGL_ID =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 0)];
                    values.STD_PGL_ID = [STD_PGL_ID intValue];
                }
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *STD_TEMP_DESC =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 1)];
                    values.STD_TEMP_DESC = STD_TEMP_DESC;
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *STD_SEQUENCE =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 2)];
                    values.STD_SEQUENCE = [STD_SEQUENCE intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *STD_MIN_STARTP =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 3)];
                    values.STD_MIN_STARTP = [STD_MIN_STARTP intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *STD_MAX_STARTP =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 4)];
                    values.STD_MAX_STARTP = [STD_MAX_STARTP intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                    NSString *STD_MIN_PT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 5)];
                    values.STD_MIN_PT = [STD_MIN_PT intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                    NSString *STD_MAX_PT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 6)];
                    values.STD_MAX_PT = [STD_MAX_PT intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                    NSString *STD_NO_PAGES =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 7)];
                    values.STD_NO_PAGES = [STD_NO_PAGES intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 8) != NULL){
                    NSString *STD_MANDATORY =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 8)];
                    values.STD_MANDATORY = STD_MANDATORY;
                }
                
                if((const char *) sqlite3_column_text(statement, 9) != NULL){
                    NSString *STD_SHOW =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 9)];
                    values.STD_SHOW = STD_SHOW;
                }
                
                if((const char *) sqlite3_column_text(statement, 10) != NULL){
                    NSString *STD_UPDATE_BY =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 10)];
                    values.STD_UPDATE_BY = STD_UPDATE_BY;
                }
                
                if((const char *) sqlite3_column_text(statement, 11) != NULL){
                    NSString *STD_UPDATE_DT =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 11)];
                    values.STD_UPDATE_DT = STD_UPDATE_DT;
                }
                
                if((const char *) sqlite3_column_text(statement, 12) != NULL){
                    NSString *STD_IS_RIDER =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 12)];
                    values.STD_IS_RIDER = STD_IS_RIDER;
                }
                
                if((const char *) sqlite3_column_text(statement, 13) != NULL){
                    NSString *STD_RDL_CODE =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 13)];
                    values.STD_RDL_CODE = STD_RDL_CODE;
                }
                
                if((const char *) sqlite3_column_text(statement, 14) != NULL){
                    NSString *STD_MAX_NO_PAGES =   [[NSString alloc] initWithUTF8String:
                                                    (const char *) sqlite3_column_text(statement, 14)];
                    values.STD_MAX_NO_PAGES = [STD_MAX_NO_PAGES intValue];
                }
                [arr addObject:values];
                
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return arr;
}

-(NSMutableArray *) getIllusPageList{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    iLLusPageList *values;
    NSString *query = @"SELECT IPL_PGL_ID,IPL_MIN_PT,IPL_MAX_PT,IPL_MIN_PAGE,IPL_MAX_PAGE FROM LP_IPL_ILLUSS_PAGELIST";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                values = [[iLLusPageList alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    NSString *IPL_PGL_ID =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 0)];
                    values.IPL_PGL_ID = [IPL_PGL_ID intValue];
                }
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    NSString *IPL_MIN_PT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                    values.IPL_MIN_PT = [IPL_MIN_PT intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    NSString *IPL_MAX_PT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 2)];
                    values.IPL_MAX_PT = [IPL_MAX_PT intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    NSString *IPL_MIN_PAGE =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 3)];
                    values.IPL_MIN_PAGE = IPL_MIN_PAGE;
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *IPL_MAX_PAGE =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                    values.IPL_MAX_PAGE = IPL_MAX_PAGE;
                }
                
                [arr addObject:values];
                
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return arr;
}
//***********************ULIP***********************

-(sqlite3_stmt *) getStatementData:(NSString *)_query{
    const char *dbpath = [databasePath UTF8String];
    //NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    //PMLPremiumMultiple *premMulti = NULL;
    NSString *query = _query;
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return statement;
}

-(NSMutableArray *) getPGLGrpMinPrem{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    //PMLPremiumMultiple *premMulti = NULL;
    NSString *query = @"SELECT ";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                //premMulti = [[PMLPremiumMultiple alloc] init];
                
                //NSString *PML_ID =   [[NSString alloc] initWithUTF8String:
                                      //(const char *) sqlite3_column_text(statement, 0)];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//get the Min Premium Data for Plan..
-(NSMutableArray *) getPNLMinPrem{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PNLMinPrem *MinPrem = NULL;
    NSString *query = @"select PMML_ID,PML_PLAN_CD,PML_PREMIUM_MODE,PML_MIN_PREM_AMT,PML_MAX_PREM_AMT from LP_PNL_MIN_PREM_LK;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                MinPrem = [[PNLMinPrem alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PMML_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    MinPrem.PMML_ID = [PMML_ID intValue];
                }else
                    MinPrem.PMML_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PML_PLAN_CD =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    MinPrem.PML_PLAN_CD = PML_PLAN_CD;
                }else
                    MinPrem.PML_PLAN_CD = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PML_PREMIUM_MODE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                    MinPrem.PML_PREMIUM_MODE = PML_PREMIUM_MODE;
                }else
                    MinPrem.PML_PREMIUM_MODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PML_MIN_PREM_AMT =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 3)];
                    MinPrem.PML_MIN_PREM_AMT = [PML_MIN_PREM_AMT intValue];
                }else
                    MinPrem.PML_MIN_PREM_AMT = 0;
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *PML_MAX_PREM_AMT =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                    MinPrem.PML_MAX_PREM_AMT = [PML_MAX_PREM_AMT intValue];
                }else
                    MinPrem.PML_MAX_PREM_AMT = 0;
                
                [arr addObject:MinPrem];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
        return arr;
}

//get the terms for the plan..
-(NSMutableArray *) getPNLTermPpt{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PNLTermPpt *TermPpt = NULL;
    NSString *query = @"select PNL_CODE,TERM_FLAG,MIN_TERM,MAX_TERM,TERM_FORMULA,TERM,PPT_FLAG,PPT_FORMULA,PPT,MATURITY_AGE,MIN_VESTING_AGE from LP_PNL_TERM_PPT_LK;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                TermPpt = [[PNLTermPpt alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PNL_CODE =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    TermPpt.PNL_CODE = PNL_CODE;
                }else
                    TermPpt.PNL_CODE = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *TERM_FLAG =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 1)];
                    TermPpt.TERM_FLAG = TERM_FLAG;
                }else
                    TermPpt.TERM_FLAG = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *MIN_TERM =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 2)];
                    TermPpt.MIN_TERM = [MIN_TERM intValue];
                }else
                    TermPpt.MIN_TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL ){
                NSString *MAX_TERM =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 3)];
                    TermPpt.MAX_TERM = [MAX_TERM intValue];
                }else
                    TermPpt.MAX_TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *TERM_FORMULA =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                    TermPpt.TERM_FORMULA = TERM_FORMULA;
                }else
                    TermPpt.TERM_FORMULA = @"";
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL ){
                NSString *TERM =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 5)];
                    TermPpt.TERM = [TERM intValue];
                }else
                    TermPpt.TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *PPT_FLAG =   [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 6)];
                    TermPpt.PPT_FLAG = PPT_FLAG;
                }else
                    TermPpt.PPT_FLAG = @"";
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                NSString *PPT_FORMULA =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 7)];
                    TermPpt.PPT_FORMULA = PPT_FORMULA;
                }else
                    TermPpt.PPT_FORMULA = @"";
                
                if((const char *) sqlite3_column_text(statement, 8) != NULL){
                NSString *MATURITY_AGE =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 8)];
                    TermPpt.MATURITY_AGE = [MATURITY_AGE intValue];
                }else
                    TermPpt.MATURITY_AGE = 0;
                
                
                if((const char *) sqlite3_column_text(statement, 9) != NULL){
                NSString *MIN_VESTING_AGE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 9)];
                    TermPpt.MIN_VESTING_AGE = [MIN_VESTING_AGE intValue];
                }else
                    TermPpt.MIN_VESTING_AGE = 0;

                
                [arr addObject:TermPpt];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

//get the premium multi discount for the plan..
-(NSMutableArray *) getPmdPlanPremiumMultDisc{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PmdPlanPremiumMultDisc *PlanPremiumMultDisc = NULL;
    NSString *query = @"select PMD_PLAN_CODE,PMD_MIN_SA,PMD_MAX_SA,PMD_DISC_RATE from LP_PMD_PLAN_PREMIUM_MULT_DISC;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                PlanPremiumMultDisc = [[PmdPlanPremiumMultDisc alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0)!= NULL){
                NSString *PMD_PLAN_CODE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 0)];
                    PlanPremiumMultDisc.PMD_PLAN_CODE = PMD_PLAN_CODE;
                }else
                    PlanPremiumMultDisc.PMD_PLAN_CODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PMD_MIN_SA =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    PlanPremiumMultDisc.PMD_MIN_SA = [PMD_MIN_SA longLongValue];
                }else
                    PlanPremiumMultDisc.PMD_MIN_SA = 0.0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PMD_MAX_SA =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    PlanPremiumMultDisc.PMD_MAX_SA = [PMD_MAX_SA longLongValue];
                }else
                    PlanPremiumMultDisc.PMD_MAX_SA = 0.0;
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PMD_DISC_RATE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 3)];
                    PlanPremiumMultDisc.PMD_DISC_RATE = [PMD_DISC_RATE doubleValue];
                }else
                    PlanPremiumMultDisc.PMD_DISC_RATE = 0;
                
                [arr addObject:PlanPremiumMultDisc];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

    
}

//get the rider code for the plan
-(NSMutableArray *) getPBAProductBand{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PBAProductBand *ProductBand = NULL;
    NSString *query = @"select PBA_PLAN_CODE,PBA_RIDER_CODE,PBA_TYPE,PBA_BAND,PBA_SUM_ASSURED,PBA_SEQ from LP_PBA_PRODUCT_BAND;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                ProductBand = [[PBAProductBand alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PBA_PLAN_CODE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 0)];
                    ProductBand.PBA_PLAN_CODE = PBA_PLAN_CODE;
                }else
                    ProductBand.PBA_PLAN_CODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PBA_RIDER_CODE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    ProductBand.PBA_RIDER_CODE = PBA_RIDER_CODE;
                }else
                    ProductBand.PBA_RIDER_CODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PBA_TYPE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    ProductBand.PBA_TYPE = PBA_TYPE;
                }else
                    ProductBand.PBA_TYPE = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PBA_BAND =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 3)];
                    ProductBand.PBA_BAND = PBA_BAND;
                }else
                    ProductBand.PBA_BAND = @"";
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *PBA_SUM_ASSURED =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 4)];
                    ProductBand.PBA_SUM_ASSURED = [PBA_SUM_ASSURED longLongValue];
                }else
                    ProductBand.PBA_SUM_ASSURED = 0.0;
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *PBA_SEQ =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 5)];
                    ProductBand.PBA_SEQ = [PBA_SEQ intValue];
                }else
                    ProductBand.PBA_SEQ = 0;
                
                [arr addObject:ProductBand];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

//get the plan maturity value..
-(NSMutableArray *) getPMVPlanMaturityValue{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PMVPlanMaturityValue *PlanMaturityValue = NULL;
    NSString *query = @"select PMV_ID,PMV_PNL_ID,PMV_GENDER,PMV_BAND,PMV_AGE,PMV_OCCUR,PMV_NSP,PMV_USE,PMV_NUM,PMV_DUR1,PMV_CVAL1,PMV_DUR2,PMV_CVAL2,PMV_DUR3,PMV_CVAL3,PMV_DUR4,PMV_CVAL4,PMV_DUR5,PMV_CVAL5,PMV_DUR6,PMV_CVAL6,PMV_DUR7,PMV_CVAL7,PMV_DUR8,PMV_CVAL8,PMV_DUR9,PMV_CVAL9,PMV_DUR10,PMV_CVAL10 FROM LP_PMV_PLAN_MATURITY_VALUE;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                PlanMaturityValue = [[PMVPlanMaturityValue alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PMV_ID =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 0)];
                    PlanMaturityValue.PMV_ID = [PMV_ID intValue];
                }else
                    PlanMaturityValue.PMV_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PMV_PNL_ID =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    PlanMaturityValue.PMV_PNL_ID = [PMV_PNL_ID intValue];
                }else
                    PlanMaturityValue.PMV_PNL_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PMV_GENDER =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    PlanMaturityValue.PMV_GENDER = PMV_GENDER;
                }else
                    PlanMaturityValue.PMV_GENDER = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PMV_BAND =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 3)];
                    PlanMaturityValue.PMV_BAND = PMV_BAND;
                }else
                    PlanMaturityValue.PMV_BAND = @"";
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *PMV_AGE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 4)];
                    PlanMaturityValue.PMV_AGE = [PMV_AGE intValue];
                }else
                    PlanMaturityValue.PMV_AGE = 0;
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *PMV_OCCUR =   [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 5)];
                    PlanMaturityValue.PMV_OCCUR = PMV_OCCUR;
                }else
                    PlanMaturityValue.PMV_OCCUR = @"";
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *PMV_NSP =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 6)];
                    PlanMaturityValue.PMV_NSP = [PMV_NSP doubleValue];
                }else
                    PlanMaturityValue.PMV_NSP = 0;
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                NSString *PMV_USE =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 7)];
                    PlanMaturityValue.PMV_USE = PMV_USE;
                }else
                    PlanMaturityValue.PMV_USE = @"";
                
                if((const char *) sqlite3_column_text(statement, 8) != NULL){
                NSString *PMV_NUM =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 8)];
                    PlanMaturityValue.PMV_NUM = [PMV_NUM intValue];
                }else
                    PlanMaturityValue.PMV_NUM = 0;
                
                NSString *PMV_DUR1 =   [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 9)];
                PlanMaturityValue.PMV_DUR1 = [PMV_DUR1 intValue];
                
                NSString *PMV_CVAL1 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 10)];
                PlanMaturityValue.PMV_CVAL1 = [PMV_CVAL1 doubleValue];
                
                NSString *PMV_DUR2 =   [[NSString alloc]initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement,11)];
                PlanMaturityValue.PMV_DUR2 = [PMV_DUR2 intValue];
                
                NSString *PMV_CVAL2 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 12)];
                PlanMaturityValue.PMV_CVAL2 = [PMV_CVAL2 doubleValue];
                
                NSString *PMV_DUR3 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 13)];
                PlanMaturityValue.PMV_DUR3 = [PMV_DUR3 intValue];
                
                NSString *PMV_CVAL3 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 14)];
                PlanMaturityValue.PMV_CVAL3 = [PMV_CVAL3 doubleValue];
                
                NSString *PMV_DUR4 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 15)];
                PlanMaturityValue.PMV_DUR4 = [PMV_DUR4 intValue];
                
                NSString *PMV_CVAL4 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 16)];
                PlanMaturityValue.PMV_CVAL4 = [PMV_CVAL4 doubleValue];
                
                NSString *PMV_DUR5 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 17)];
                PlanMaturityValue.PMV_DUR5 = [PMV_DUR5 intValue];
                
                NSString *PMV_CVAL5 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 18)];
                PlanMaturityValue.PMV_CVAL5 = [PMV_CVAL5 doubleValue];
                
                NSString *PMV_DUR6 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 19)];
                PlanMaturityValue.PMV_DUR6 = [PMV_DUR6 intValue];
                
                NSString *PMV_CVAL6 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 20)];
                PlanMaturityValue.PMV_CVAL6 = [PMV_CVAL6 doubleValue];
                
                NSString *PMV_DUR7 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 21)];
                PlanMaturityValue.PMV_DUR7 = [PMV_DUR7 intValue];
                
                NSString *PMV_CVAL7 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 22)];
                PlanMaturityValue.PMV_CVAL7 = [PMV_CVAL7 doubleValue];
                
                NSString *PMV_DUR8 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 23)];
                PlanMaturityValue.PMV_DUR8 = [PMV_DUR8 intValue];
                
                NSString *PMV_CVAL8 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 24)];
                PlanMaturityValue.PMV_CVAL8 = [PMV_CVAL8 doubleValue];
                
                NSString *PMV_DUR9 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 25)];
                PlanMaturityValue.PMV_DUR9 = [PMV_DUR9 intValue];
                
                NSString *PMV_CVAL9 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 26)];
                PlanMaturityValue.PMV_CVAL9 = [PMV_CVAL9 doubleValue];
                
                NSString *PMV_DUR10 =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 27)];
                PlanMaturityValue.PMV_DUR10 = [PMV_DUR10 intValue];
                
                NSString *PMV_CVAL10 =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 28)];
                PlanMaturityValue.PMV_CVAL10 = [PMV_CVAL10 doubleValue];
                
                [arr addObject:PlanMaturityValue];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

//get the reference of the sis param values..
-(NSMutableArray *) getPFRPlanFormulaRef{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PFRPlanFormulaRef *PlanFormulaRef = NULL;
    NSString *query = @"select PER_PLAN_CD,PER_FRM_CD,PER_FRM_TYPE,PER_FRM_EXP from LP_PER_PLAN_FORMULA_REF;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                PlanFormulaRef = [[PFRPlanFormulaRef alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PER_PLAN_CD =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 0)];
                    PlanFormulaRef.PER_PLAN_CD = PER_PLAN_CD;
                }else
                    PlanFormulaRef.PER_PLAN_CD = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *PER_FRM_CD =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    PlanFormulaRef.PER_FRM_CD = PER_FRM_CD;
                }else
                    PlanFormulaRef.PER_FRM_CD = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *PER_FRM_TYPE =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    PlanFormulaRef.PER_FRM_TYPE = PER_FRM_TYPE;
                }else
                    PlanFormulaRef.PER_FRM_TYPE = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *PER_FRM_EXP =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 3)];
                    PlanFormulaRef.PER_FRM_EXP = PER_FRM_EXP;
                }else
                    PlanFormulaRef.PER_FRM_EXP = @"";
                
                [arr addObject:PlanFormulaRef];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

//get the formula expression evaluation for the formula reference param values
-(NSMutableArray *) getFEDFormulaExtDesc{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    FEDFormulaExtDesc *FormulaExtDesc = NULL;
    NSString *query = @"select FED_ID,FED_OBJ_SEQ,FED_OBJ_OPND,FED_OBJ_OPND_TYPE,FED_OBJ_OPTR,FED_OBJ_TYPE from LP_FED_FORMULA_EXT_DESC;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                FormulaExtDesc = [[FEDFormulaExtDesc alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *FED_ID =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 0)];
                    FormulaExtDesc.FED_ID = FED_ID;
                }else
                    FormulaExtDesc.FED_ID = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *FED_OBJ_SEQ =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 1)];
                    FormulaExtDesc.FED_OBJ_SEQ = [FED_OBJ_SEQ intValue];
                }else
                    FormulaExtDesc.FED_OBJ_SEQ = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *FED_OBJ_OPND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                   FormulaExtDesc.FED_OBJ_OPND = FED_OBJ_OPND;
                }else
                   FormulaExtDesc.FED_OBJ_OPND = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *FED_OBJ_OPND_TYPE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                    FormulaExtDesc.FED_OBJ_OPND_TYPE = FED_OBJ_OPND_TYPE;
                }else
                    FormulaExtDesc.FED_OBJ_OPND_TYPE = @"";
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    NSString *FED_OBJ_OPTR =   [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                    FormulaExtDesc.FED_OBJ_OPTR = FED_OBJ_OPTR;
                }else{
                    FormulaExtDesc.FED_OBJ_OPTR = NULL;
                }
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *FED_OBJ_TYPE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 5)];
                    FormulaExtDesc.FED_OBJ_TYPE = FED_OBJ_TYPE;
                }else
                    FormulaExtDesc.FED_OBJ_TYPE = @"";

                
                [arr addObject:FormulaExtDesc];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
    
}

//get the Details for Company..
-(NSMutableArray *) getAddress{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
     AddressBO *oAddressBO = NULL;
    NSString *query = @"select CO_NAME,CO_ADDRESS,CO_HELPLINE,SIS_VERSION,FILLER1 from LP_COMPANY_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                oAddressBO = [[AddressBO alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *CO_NAME =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    oAddressBO.CO_NAME = CO_NAME;
                }else
                    oAddressBO.CO_NAME = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *CO_ADDRESS =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 1)];
                    oAddressBO.CO_ADDRESS = CO_ADDRESS;
                }else
                    oAddressBO.CO_ADDRESS = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *CO_HELPLINE =   [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 2)];
                    oAddressBO.CO_HELPLINE = CO_HELPLINE;
                }else
                    oAddressBO.CO_HELPLINE = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *SIS_VERSION =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                    oAddressBO.SIS_VERSION = SIS_VERSION;
                }else
                    oAddressBO.SIS_VERSION = @"";
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *FILLER1 =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 4)];
                    oAddressBO.FILLER1 = FILLER1;
                }else
                    oAddressBO.FILLER1 = @"";
                
                [arr addObject:oAddressBO];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

-(NSMutableDictionary *) getPGFPlanGrpFeature{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    PGFPlanGroupFeature *planFeature = NULL;
    NSString *query = @"select PGF_ID,PGF_PGL_ID,PGF_PNL_CODE,PGF_FEATURE,PGF_DESCRIPTION,PGF_FILLER1,PGF_FILLER2,PGF_FILLER3 from LP_PGF_PLAN_GROUP_FEATURE;";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                 planFeature = [[PGFPlanGroupFeature alloc] init];
                
                 NSString *PGF_PNL_CODE =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 2)];
                 planFeature.PGF_PNL_CODE  = PGF_PNL_CODE;
                
                 NSString *PGF_FEATURE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                 planFeature.PGF_FEATURE  = PGF_FEATURE;
                
                [dict setValue:planFeature forKey:PGF_PNL_CODE];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

//get the guarantee maturity Bonus..
-(NSMutableArray *) getGuaranteeMaturityBonusMaster{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    GuaranteedMaturityBonusMaster *oGuaranteedMaturityBonusMaster = NULL;
    NSString *query = @"select GMM_ID,GMM_MIN_BAND,GMM_MAX_BAND,GMM_BONUS from LP_GMM_GUARANTEED_MATURITY_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                oGuaranteedMaturityBonusMaster = [[GuaranteedMaturityBonusMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *GMM_ID =   [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    oGuaranteedMaturityBonusMaster.GMM_ID = [GMM_ID intValue];
                }else
                    oGuaranteedMaturityBonusMaster.GMM_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *GMM_MIN_BAND =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    oGuaranteedMaturityBonusMaster.GMM_MIN_BAND = [GMM_MIN_BAND intValue];
                }else
                    oGuaranteedMaturityBonusMaster.GMM_MIN_BAND = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *GMM_MAX_BAND =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 2)];
                    oGuaranteedMaturityBonusMaster.GMM_MAX_BAND= [GMM_MAX_BAND intValue];
                }else
                    oGuaranteedMaturityBonusMaster.GMM_MAX_BAND = 0;
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *GMM_BONUS =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 3)];
                    oGuaranteedMaturityBonusMaster.GMM_BONUS = [GMM_BONUS doubleValue];
                }else
                    oGuaranteedMaturityBonusMaster.GMM_BONUS = 0;
                
                [arr addObject:oGuaranteedMaturityBonusMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//get the xml data master..
-(NSMutableArray *) getXMLMetaDataMaster{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
     XMMXmlMetadataMaster *XmlMetadataMaster = NULL;
    NSString *query = @"select PNL_CODE,SRNO,ELEMENT_NAME from LP_XMM_XML_METADATA_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                XmlMetadataMaster = [[XMMXmlMetadataMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *PNL_CODE =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    XmlMetadataMaster.PNL_CODE = PNL_CODE;
                }else
                    XmlMetadataMaster.PNL_CODE = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *SRNO =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 1)];
                    XmlMetadataMaster.SRNO = [SRNO intValue];
                }else
                    XmlMetadataMaster.SRNO = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *ELEMENT_NAME =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                    XmlMetadataMaster.ELEMENT_NAME= ELEMENT_NAME;
                }else
                    XmlMetadataMaster.ELEMENT_NAME = @"";
                
                [arr addObject:XmlMetadataMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
    
}

//get the maturity factor as per age..
-(NSMutableArray *) getMFMmaturityFactorMaster{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    MFMMaturityFactorMaster *MaturityFactorMaster = NULL;
    NSString *query = @"select MFM_ID,MFM_PNL_ID,MFM_BAND,MFM_AGE,MFM_USE_FLG,MFM_VALUE,MFM_SEX from LP_MFM_MATURITY_FACTOR_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
		
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                MaturityFactorMaster = [[MFMMaturityFactorMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *MFM_ID =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 0)];
                    MaturityFactorMaster.MFM_ID = [MFM_ID intValue];
                }else
                    MaturityFactorMaster.MFM_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *MFM_PNL_ID =   [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 1)];
                    MaturityFactorMaster.MFM_PNL_ID = [MFM_PNL_ID intValue];
                }else
                    MaturityFactorMaster.MFM_PNL_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *MFM_BAND =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                    MaturityFactorMaster.MFM_BAND= [MFM_BAND intValue];
                }else
                    MaturityFactorMaster.MFM_BAND = 0;
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *MFM_AGE =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 3)];
                    MaturityFactorMaster.MFM_AGE= [MFM_AGE intValue];
                }else
                    MaturityFactorMaster.MFM_AGE = 0;
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *MFM_USE_FLG =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                    MaturityFactorMaster.MFM_USE_FLG = MFM_USE_FLG;
                }else
                    MaturityFactorMaster.MFM_USE_FLG = @"";
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *MFM_VALUE =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 5)];
                    MaturityFactorMaster.MFM_VALUE= [MFM_VALUE doubleValue];
                }else
                    MaturityFactorMaster.MFM_VALUE = 0;
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                    NSString *MFM_VALUE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 6)];
                    MaturityFactorMaster.MFM_SEX= MFM_VALUE;
                }else
                    MaturityFactorMaster.MFM_SEX = @"";

                
                [arr addObject:MaturityFactorMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}

//get GAI Guarantee annual in master..
-(NSMutableArray *) getGAIGuarAnnInChMaster{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    GAIGuarAnnIncChMaster *GuarAnnIncChMaster = NULL;
    NSString *query = @"select GAI_ID,GAI_PNL_ID,GAI_MIN_TERM,GAI_MAX_TERM,GAI_MIN_PREM,GAI_MAX_PREM,GAI_FLAG,GAI_CHARGE,GAI_MIN_PPT,GAI_MAX_PPT from LP_GAI_GUAR_ANN_INC_CH_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                GuarAnnIncChMaster = [[GAIGuarAnnIncChMaster alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *GAI_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    GuarAnnIncChMaster.GAI_ID = [GAI_ID intValue];
                }else
                    GuarAnnIncChMaster.GAI_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *GAI_PNL_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    GuarAnnIncChMaster.GAI_PNL_ID = [GAI_PNL_ID intValue];
                }else
                    GuarAnnIncChMaster.GAI_PNL_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *GAI_MIN_TERM =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    GuarAnnIncChMaster.GAI_MIN_TERM= [GAI_MIN_TERM intValue];
                }else
                    GuarAnnIncChMaster.GAI_MIN_TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *GAI_MAX_TERM =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 3)];
                    GuarAnnIncChMaster.GAI_MAX_TERM = [GAI_MAX_TERM intValue];
                }else
                    GuarAnnIncChMaster.GAI_MAX_TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *GAI_MIN_PREM =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 4)];
                    GuarAnnIncChMaster.GAI_MIN_PREM= [GAI_MIN_PREM longLongValue];
                }else
                    GuarAnnIncChMaster.GAI_MIN_PREM = 0;
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *GAI_MAX_PREM =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 5)];
                    GuarAnnIncChMaster.GAI_MAX_PREM = [GAI_MAX_PREM longLongValue];
                }else
                    GuarAnnIncChMaster.GAI_MAX_PREM = 0;
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *GAI_FLAG =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 6)];
                    GuarAnnIncChMaster.GAI_FLAG= GAI_FLAG;
                }else
                    GuarAnnIncChMaster.GAI_FLAG = @"";
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                NSString *GAI_CHARGE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 7)];
                    GuarAnnIncChMaster.GAI_CHARGE = [GAI_CHARGE doubleValue];
                }else
                    GuarAnnIncChMaster.GAI_CHARGE = 0;
                
                if((const char *) sqlite3_column_text(statement, 8) != NULL){
                    NSString *GAI_MIN_PPT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 8)];
                    GuarAnnIncChMaster.GAI_MIN_PPT = [GAI_MIN_PPT intValue];
                }else
                    GuarAnnIncChMaster.GAI_MIN_PPT = 0;
                
                if((const char *) sqlite3_column_text(statement, 9) != NULL){
                    NSString *GAI_MAX_PPT =   [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 9)];
                    GuarAnnIncChMaster.GAI_MAX_PPT = [GAI_MAX_PPT intValue];
                }else
                    GuarAnnIncChMaster.GAI_MAX_PPT = 0;
                
                [arr addObject:GuarAnnIncChMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

}

//get the SIS Param Calculation  values..
-(NSMutableDictionary *) getSPVSysParamValues{
    const char *dbpath = [databasePath UTF8String];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    SPVSysParamValues *SysParamValues = NULL;
    NSString *query = @"select SPV_SYS_PARAM,SPV_VALUE,SPV_FILLER from LP_SPV_SYS_PARAM_VALUES;";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                SysParamValues = [[SPVSysParamValues alloc] init];
                NSString *SPV_SYS_PARAM= NULL;
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    SPV_SYS_PARAM =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    SysParamValues.SPV_SYS_PARAM  = SPV_SYS_PARAM;
                }else
                    SysParamValues.SPV_SYS_PARAM = @"";
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *SPV_VALUE =   [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 1)];
                    SysParamValues.SPV_VALUE  = SPV_VALUE;
                }else
                    SysParamValues.SPV_VALUE = @"";
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *SPV_FILLER =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 2)];
                    SysParamValues.SPV_FILLER  = SPV_FILLER;
                }else{
                    SysParamValues.SPV_FILLER = NULL;
                }
                
                [dict setValue:SysParamValues forKey:SPV_SYS_PARAM];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return dict;
}

//get IPC Values..
-(NSMutableArray *) getIPCValues{

    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    IPCValues *oIPCValues = NULL;
    NSString *query = @"select IPC_ID,IPC_PNL_ID,IPC_SEX,IPC_TERM,IPC_AGE,IPC_PREM_MULT,IPC_BAND,SMOKER_FLAG from LP_IPC_INF_PROT_COV_MASTER;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                oIPCValues = [[IPCValues alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                NSString *IPC_ID =   [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)];
                    oIPCValues.IPC_ID = [IPC_ID intValue];
                }else
                    oIPCValues.IPC_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                NSString *IPC_PNL_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                    oIPCValues.IPC_PNL_ID = [IPC_PNL_ID intValue];
                }else
                    oIPCValues.IPC_PNL_ID = 0;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                NSString *IPC_SEX =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    oIPCValues.IPC_SEX= IPC_SEX;
                }else
                    oIPCValues.IPC_SEX = @"";
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                NSString *IPC_TERM =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                    oIPCValues.IPC_TERM= [IPC_TERM intValue];
                }else
                    oIPCValues.IPC_TERM = 0;
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                NSString *IPC_AGE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 4)];
                    oIPCValues.IPC_AGE = [IPC_AGE intValue];
                }else
                    oIPCValues.IPC_AGE = 0;
                
                if((const char *) sqlite3_column_text(statement, 5) != NULL){
                NSString *IPC_PREM_MULT =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 5)];
                    oIPCValues.IPC_PREM_MULT= [IPC_PREM_MULT doubleValue];
                }else
                    oIPCValues.IPC_PREM_MULT = 0;
                
                if((const char *) sqlite3_column_text(statement, 6) != NULL){
                NSString *IPC_BAND =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 6)];
                    oIPCValues.IPC_BAND = [IPC_BAND intValue];
                }else
                    oIPCValues.IPC_BAND = 0;
                
                if((const char *) sqlite3_column_text(statement, 7) != NULL){
                NSString *SMOKER_FLAG =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 7)];
                    oIPCValues.SMOKER_FLAG= SMOKER_FLAG;
                }else
                    oIPCValues.SMOKER_FLAG = @"";
                
                [arr addObject:oIPCValues];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;

    
}


-(AgentDetail *) getAgentDetail{
    const char *dbpath = [[self GetDataBasePath:LIFE_PLANNER] UTF8String];
    
    AgentDetail *agentDetailObj = NULL;
    //changes 24th nov
    NSString *query = @"Select AGENT_NAME,MOBILENO,AGENT_CD,CSA_PASSPORT_ID,PSC_SESSION_ID from LP_USERINFO;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if(sqlite3_step(statement) == SQLITE_ROW){
                agentDetailObj = [[AgentDetail alloc] init];
                NSString *agentName = @"";
                NSString *agentNumber = @"";
                NSString *agentContactNumber = @"";
                NSString *agentPassportID = @"";
                NSString *pscSessionID = @"";
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                agentName =   [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 0)];
                }
                agentDetailObj.agentName = agentName;
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                agentNumber =   [[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 2)];
                }
                agentDetailObj.agentNumber = agentNumber;
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                agentContactNumber =   [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 1)];
                }
                agentDetailObj.agentContactNumber = agentContactNumber;
                
                //changes 24th nov.
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    agentPassportID =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                }
                agentDetailObj.agentPassportID = agentPassportID;
                //PSC changes
                if((const char *) sqlite3_column_text(statement,4) != NULL){
                    pscSessionID =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 4)];
                }
                agentDetailObj.pscSessionID = pscSessionID;
                
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return agentDetailObj;
}

//Added by Amruta on 18/8/15
-(NSMutableArray *) getCommissionDetail{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    CommissionDetails *oCommissionDetails = NULL;
    NSString *query = @"Select CMT_PGL_ID,CMT_COMM_TEXT from LP_COMMISSION_TEXT;";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        
        const char *query_stmt = [query UTF8String];
		
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            int count=0;
            while(sqlite3_step(statement) == SQLITE_ROW){
                
                count++;
                oCommissionDetails = [[CommissionDetails alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    
                NSString *pglId =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 0)];
                    //changes 6th oct
                oCommissionDetails.CMT_PGL_ID = [pglId intValue];
                
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    
                NSString *commText =   [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 1)];
                oCommissionDetails.CMT_COMM_TEXT = commText;
                
                }
                
                [arr addObject:oCommissionDetails];
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return arr;
}

//Get the Data for Given Query.
-(void)getDataForUser:(NSString *)_Query{
    const char *dbpath = [databasePath UTF8String];
    
    int rc;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        const char *query_stmt = [_Query UTF8String];
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        //prepare the statement..
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            //get the column count from the query..
            NSInteger columnCount = sqlite3_column_count(statement);
            
            id value;
            //NSMutableArray *returnArray = [NSMutableArray array];
            //loop through the row.
            while ((rc = sqlite3_step(statement)) == SQLITE_ROW) {
                for (NSInteger j = 0; j < columnCount; j++) {
                    int i = (int)j;
                    //get the column name..
                    NSString *columnName   = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
                    switch (sqlite3_column_type(statement, i)) {
                        case SQLITE_NULL:
                            value = [NSNull null];
                            break;
                        case SQLITE_TEXT:
                            value = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, i)];
                            break;
                        case SQLITE_INTEGER:
                            value = @(sqlite3_column_int64(statement, i));
                            break;
                        case SQLITE_FLOAT:
                            value = @(sqlite3_column_double(statement, i));
                            break;
                        case SQLITE_BLOB:
                        {
                            NSInteger length  = sqlite3_column_bytes(statement, i);
                            const void *bytes = sqlite3_column_blob(statement, i);
                            value = [NSData dataWithBytes:bytes length:length];
                            break;
                        }
                        default:
                            NSLog(@"unknown column type");
                            value = [NSNull null];
                            break;
                    }
                    dictionary[columnName] = value;
                }
                [arr addObject:dictionary];
                //[returnArray addObject:dictionary];
            }
            
            if (rc != SQLITE_DONE) {
                NSLog(@"error returning results %d %s", rc, sqlite3_errmsg(database));
            }
            
            sqlite3_finalize(statement);
        }
    }else{
       // NSLog(@"select failed %d: %s", rc, sqlite3_errmsg(database));
    }
    //sqlite3_reset(statement);
}

#pragma mark- Test Methods
//Get the Path for Database.

//Open the Database connection.
-(BOOL) OpenDB:(NSString *)_dbName{
    //Get the Database path
    NSString *dbPath = [self GetDataBasePath:_dbName];
    
    BOOL isSuccess = YES;
    //check if database can be opened.
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
        return  isSuccess;
    else
    {
        isSuccess = NO;
        NSLog(@"Failed to open database");
    }
    return isSuccess;
}

//close the Database Connection.
-(void)CloseDB:(NSString *)_dbName{
    //Get the Database path
    NSString *dbPath = [self GetDataBasePath:_dbName];
    
    //check if database is opened, then close the database.
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
        sqlite3_close(database);
}


//SIP
-(NSMutableArray *)getPremiumBoost{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    PremiumBoost *premBoost = NULL;
    NSString *query = @"SELECT LPB_PLAN_CODE, LPB_RIDER_CODE,LPB_MIN_PREMIUM,LPB_MAX_PREMIUM,LPB_BOOST_RATE  FROM LP_LPB_PREMIUM_BOOST";
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
		const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        
        const char *query_stmt = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            
            while(sqlite3_step(statement) == SQLITE_ROW){
                
                premBoost = [[PremiumBoost alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) != NULL){
                    
                    NSString *LPB_PLAN_CODE =   [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 0)];
                    premBoost.LPB_PLAN_CODE = LPB_PLAN_CODE;
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 1) != NULL){
                    
                    NSString *LPB_RIDER_CODE =   [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 1)];
                    premBoost.LPB_RIDER_CODE = LPB_RIDER_CODE;
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 2) != NULL){
                    
                    NSString *LPB_MIN_PREMIUM =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 2)];
                    premBoost.LPB_MIN_PREMIUM = [LPB_MIN_PREMIUM longLongValue];
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 3) != NULL){
                    
                    NSString *LPB_MAX_PREMIUM =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 3)];
                    premBoost.LPB_MAX_PREMIUM = [LPB_MAX_PREMIUM longLongValue];
                    
                }
                
                if((const char *) sqlite3_column_text(statement, 4) != NULL){
                    
                    NSString *LPB_BOOST_RATE =   [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 4)];
                    premBoost.LPB_BOOST_RATE = [LPB_BOOST_RATE doubleValue];
                    
                }
                
                [arr addObject:premBoost];
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return arr;
}


//CHANGES 16TH OCT SA 
-(NSMutableArray *) getFOPRate{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    
    FOPRate *FOPRateMaster = NULL;
    NSString *query = @"SELECT FOP_PNL_ID,FOP_TERM,FOP_MODE,FOP_CHARGE FROM LP_FOP_CHARGE_MASTER";
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
        const char* key = [@"life" UTF8String];
        sqlite3_key(database, key, (int)strlen(key));
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                FOPRateMaster = [[FOPRate alloc] init];
                
                if((const char *) sqlite3_column_text(statement, 0) !=NULL){
                    NSString *FOP_PNL_ID =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                    FOPRateMaster.FOP_PNL_ID = [FOP_PNL_ID intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 1) !=NULL){
                    NSString *FOP_TERM =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                    FOPRateMaster.FOP_TERM = [FOP_TERM intValue];
                }
                
                if((const char *) sqlite3_column_text(statement, 2) !=NULL){
                    NSString *FOP_MODE =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                    FOPRateMaster.FOP_MODE = FOP_MODE;
                }
                
                if((const char *) sqlite3_column_text(statement, 3) !=NULL){
                    NSString *FOP_CHARGE =   [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                    FOPRateMaster.FOP_CHARGE = [FOP_CHARGE doubleValue];
                }
                
                [arr addObject:FOPRateMaster];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return arr;
}


@end
