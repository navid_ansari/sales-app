//
//  DBManager.h
//  TestGitSetup
//
//  Created by Admin on 19/11/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "MDLModalFactor.h"
#import "OCLOccupation.h"
#import "PGLPlanGroup.h"
#import "PMLPremiumMultiple.h"
#import "PNLPlan.h"
#import "PNLMinPrem.h"
#import "PNLTermPpt.h"
#import "PmdPlanPremiumMultDisc.h"
#import "PBAProductBand.h"
#import "PMVPlanMaturityValue.h"
#import "PFRPlanFormulaRef.h"
#import "FEDFormulaExtDesc.h"
#import "AddressBO.h"
#import "GuaranteedMaturityBonusMaster.h"
#import "XMMXmlMetadataMaster.h"
#import "MFMMaturityFactorMaster.h"
#import "GAIGuarAnnIncChMaster.h"
#import "SPVSysParamValues.h"
#import "PGFPlanGroupFeature.h"
#import "IPCValues.h"
#import "AgentDetail.h"
#import "FundDetails.h"
#import "PCRPlanChargesRef.h"
#import "PACPremiumAllocationCharges.h"
#import "CCMommissionChargeMaster.h"
#import "MCMMortilityChargesMaster.h"
#import "ACMAdminChargeMaster.h"
#import "FCMFIBChargersMaster.h"
#import "WCMWOPChargesMaster.h"
#import "SURSurrenderChargesMaster.h"
#import "ServiceTaxMaster.h"
#import "STDTemplateValues.h"
#import "iLLusPageList.h"
#import "RMLRiderPremMultiple.h"
#import "Rider.h"
#import "RDLChargesValue.h"
#import "ValidatePrem.h"
#import "CommissionDetails.h"
#import "PremiumBoost.h"
#import "FOPRate.h"
#import "Constant.h"

@interface DBManager : NSObject{
    NSString *databasePath;
}

+(DBManager*)getSharedInstance;
-(void)getDataForUser:(NSString *)_Query;
-(NSString *)GetDataBasePath:(NSString *)_dbName;

-(NSMutableArray *)getModalFactorDetails;
-(NSMutableDictionary *)getOCLOccupation;
-(NSMutableDictionary *)getPGLPlanGroup;
-(NSMutableArray *)getPMLPremiumMultiple;
//-(NSMutableDictionary *) getPNLChkExcludes;
-(NSMutableDictionary *) getPNLPlan;

-(NSMutableArray *) getPGLGrpMinPrem;
-(NSMutableArray *) getPNLMinPrem;
-(NSMutableArray *) getPNLTermPpt;
-(NSMutableArray *) getPmdPlanPremiumMultDisc;
-(NSMutableArray *) getPBAProductBand;
-(NSMutableArray *) getPMVPlanMaturityValue;
-(NSMutableArray *) getPFRPlanFormulaRef;
-(NSMutableArray *) getFEDFormulaExtDesc;
-(NSMutableArray *) getAddress;
-(NSMutableDictionary *) getPGFPlanGrpFeature;
//-(NSMutableArray *) getPACPremiumAllocationCharges;
//-(NSMutableArray *) getACMAdminChargeMaster;
//-(NSMutableArray *) getMCMMortilityChargesMaster;

//-(NSMutableArray *) getWCMWOPChargesMaster;
//-(NSMutableArray *) getFCMFIBChargesMaster;
//-(NSMutableArray *) getSURSurrenderChargeMaster;
-(NSMutableArray *) getGuaranteeMaturityBonusMaster;
-(NSMutableArray *) getXMLMetaDataMaster;
//-(NSMutableArray *) getCCMCommissionChargeMaster;
-(NSMutableArray *) getMFMmaturityFactorMaster;
-(NSMutableArray *) getGAIGuarAnnInChMaster;
-(NSMutableDictionary *) getSPVSysParamValues;
-(NSMutableArray *) getIPCValues;
//-(NSMutableArray *) getAAAFMCValues;
-(AgentDetail *) getAgentDetail;

//********************ULIP************************
-(NSMutableArray *) getPACPremiumAllocationCharges;
-(NSMutableDictionary *) getFundDetails;
-(NSMutableDictionary *) getPCRPlanChargesRef;
-(NSMutableArray *) getCCMCommissionChargeMaster;
-(NSMutableArray *) getMCMMortilityChargesMaster;
-(NSMutableArray *) getACMAdminChargeMaster;
-(NSMutableArray *) getWCMWOPChargesMaster;
-(NSMutableArray *) getFCMFIBChargesMaster;
-(NSMutableArray *) getSURSurrenderChargeMaster;
-(NSMutableArray *) getServiceTaxMaster;
//********************ULIP************************
-(NSMutableArray *)getPremiumMultiple:(NSMutableDictionary *) data;
-(NSMutableArray *) getSTDTemplateValues;
-(NSMutableArray *) getIllusPageList;
-(NSMutableArray *)getRMLRiderPremMultiple;
-(NSMutableDictionary *) getRDLRider;
-(NSMutableArray *)getRDLCharges;

//Added by Amruta on 18/8/15
-(NSMutableArray *)getCommissionDetail;

-(ValidatePrem *)getValuesForPremium:(int )pglID andWithMode:(NSString *)mode;

//SIP
-(NSMutableArray *)getPremiumBoost;

//2nd march
-(double)getPremMultFoMRS:(NSString *)oclCode;

//CHANGES 16TH OCT SA
-(NSMutableArray *) getFOPRate;
@end
