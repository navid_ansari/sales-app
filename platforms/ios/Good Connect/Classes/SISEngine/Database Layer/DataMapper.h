//
//  DataMapper.h
//  LifePlanner
//
//  Created by admin on 24/11/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

@interface DataMapper : NSObject

@property (nonatomic, strong) NSMutableArray *MDLModalFactor;
@property (nonatomic, strong) NSMutableDictionary *OCLOccupation;
@property (nonatomic, strong) NSMutableDictionary *PGLPlanGroup;
@property (nonatomic, strong) NSMutableArray *PMLPremiumMultiple;
@property (nonatomic, strong) NSMutableDictionary *PNLChkExcludes;
@property (nonatomic, strong) NSMutableDictionary *PNLPlan;
@property (nonatomic, strong) NSMutableArray *PGLGrpMinPrem;
@property (nonatomic, strong) NSMutableArray *PNLMinPrem;
@property (nonatomic, strong) NSMutableArray *PNLTermPpt;
@property (nonatomic, strong) NSMutableArray *RMLRiderPremMultiple;
@property (nonatomic, strong) NSMutableArray *PmdPlanPremMultiDisc;
@property (nonatomic, strong) NSMutableArray *PBAProductBand;
@property (nonatomic, strong) NSMutableDictionary *RDLRider;
@property (nonatomic, strong) NSMutableDictionary *FDLFund;
@property (nonatomic, strong) NSMutableArray *PMVPlanMaturityValue;
@property (nonatomic, strong) NSMutableArray *PFRPlanFormulaRef;
@property (nonatomic, strong) NSMutableArray *FEDFormulaExtDesc;
@property (nonatomic, strong) NSMutableArray *Address;
@property (nonatomic, strong) NSMutableDictionary *PGFPlanGroupFeature;
@property (nonatomic, strong) NSMutableArray *PACPremiumAllocationCharges;
@property (nonatomic, strong) NSMutableArray *ACMAdminChargeMaster;
@property (nonatomic, strong) NSMutableArray *MCMMortilityChargeMaster;
@property (nonatomic, strong) NSMutableDictionary *PCRPlanChargesRef;
@property (nonatomic, strong) NSMutableArray *WCMWOPChargesMaster;
@property (nonatomic, strong) NSMutableArray *FCMFIBChargesMaster;
@property (nonatomic, strong) NSMutableArray *SURSurrenderChargesMaster;
@property (nonatomic, strong) NSMutableArray *GuaranteeMaturityBonusMaster;
@property (nonatomic, strong) NSMutableArray *CommissionChargeMaster;
@property (nonatomic, strong) NSMutableArray *MFMMaturityFactorMaster;
@property (nonatomic,strong) NSMutableArray *GAIGuarAnnInChMaster;
@property (nonatomic, strong) NSMutableArray *XmlMetaDataMaster;
@property (nonatomic, strong) NSMutableDictionary *SPVSysParamValues;
@property (nonatomic, strong) NSMutableArray *RDLCharges;
@property (nonatomic, strong) NSMutableArray *IPCValues;
@property (nonatomic, strong) NSMutableArray *AAAFMCValues;
@property (nonatomic, strong) NSMutableArray *StdTemplateValues;
@property (nonatomic, strong) AgentDetail *agent_intermedDetails;
@property (nonatomic, strong) NSMutableArray *CommissionDetail;
+(DataMapper*)getInstance;

//*****ULIP*****
@property (nonatomic ,retain) NSMutableArray *ServiceMaster;
@property (nonatomic, retain) NSMutableArray *IllusPageList;

//SIP
@property (nonatomic, retain) NSMutableArray *premiumBoost;

//changes 16th oct SA
@property (nonatomic, retain) NSMutableArray *FOPList;
@end
