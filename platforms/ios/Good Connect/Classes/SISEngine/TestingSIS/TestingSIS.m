//
//  TestingSIS.m
//  Life_Planner
//
//  Created by admin on 19/03/15.
//
//

#import "TestingSIS.h"
#import "ProcessDataForSIS.h"
#import "SolutionWorks.h"
#import "Validation.h"

@implementation TestingSIS

-(id)init{
    self = [super init];
    if(self){
        [self callSIS];
    }
    return self;
}

-(void)callHtmlConvert{
    //Validation *val = [[Validation alloc] init];
}

-(void)callSIS{
    NSMutableDictionary *reqDict = [self getFortunePro];
    
    ProcessDataForSIS *processDataForSISObj = [[ProcessDataForSIS alloc] init];
    [processDataForSISObj getHtmlContent:reqDict];
}

-(void)callHowSolutionWorks{
    NSMutableDictionary *request = [[NSMutableDictionary alloc] init];
    [request setValue:@"MLS2NB" forKey:@"BasePlan"];
    [request setValue:@"5500000" forKey:@"Sumassured"];
    //[request setValue:@"4" forKey:@"Percent"];
    [request setValue:@"15" forKey:@"PolicyTerm"];
    [request setValue:@"12" forKey:@"PPTerm"];
    [request setValue:@"30" forKey:@"Age"];
    [request setValue:@"550000" forKey:@"BasePremium"];
    //SolutionWorks *works = [[SolutionWorks alloc] init];
    //[works getDataForProduct:request];
}

#pragma mark -- Public Methods

-(NSMutableDictionary *)getGoodKid{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    
    /*
     pnlid - 732
     pglid - 221
     
     age : 25 to 45
     SA : 250000 to infinite
     
     Freq : ASQ
     Start pt : S
     
     PT : 12 to 25
     PPT : PT - 5
     
     */
    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"45" forKey:@"InsAge"];//18 to 50
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"B02" forKey:@"OwnOcc"];
    [reqDict setValue:@"F" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"GKIDV1P1" forKey:@"BasePlan"];
    [reqDict setValue:@"250000" forKey:@"Sumassured"];
    
    //[reqDict setObject:@"18000" forKey:@"BasePremium"];
    [reqDict setValue:@"25" forKey:@"PolicyTerm"];
    [reqDict setValue:@"20" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"Y" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Female" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"45" forKey:@"ProposerAge"];
    
    //rider
    //[reqDict setValue:@"250000" forKey:@"ADDLN1V1"];
    
    return reqDict;
}

-(NSMutableDictionary *)getSIP{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    /*
     Pglid : 219
     Age : 3 to 50
     Min prem : 18000
     start point : P
     Min vesting age : 18
     SIP7IV1N1
     id : 725
     ppt : 7
     pt : 15
     Ma : 65
     
     SIP10IV1N1 
      id : 726
     ppt : 10
     pt : 21
     Ma : 71
     
     SIP12IV1N1
     id : 727
     ppt : 12
     pt : 25
     Ma : 75
     
     SIP7EV1N1
     id : 730
     ppt : 7
     pt : 15
     Ma : 65
     
     SIP10EV1N1
     id : 728
     ppt : 10
     pt : 21
     Ma : 71
     
     SIP12EV1N1
     id : 729
     ppt : 12
     pt : 25
     Ma : 75
     */
    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//18 to 50
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"26" forKey:@"OwnAge"];
    [reqDict setValue:@"B02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"A" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SIP12IV1N1" forKey:@"BasePlan"];
    [reqDict setValue:@"198000" forKey:@"Sumassured"];
    
    [reqDict setObject:@"18000" forKey:@"BasePremium"];
    [reqDict setValue:@"25" forKey:@"PolicyTerm"];
    [reqDict setValue:@"12" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"A" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    //rider
    [reqDict setValue:@"198000" forKey:@"ADDLN1V1"];
    
    return reqDict;
}

-(NSMutableDictionary *) getFreedom{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    
    
    /*
     
     SA : 200000 to infinite
     Term is calculated  : Maturity age - age
     Mode : ASM
     
     Age : 25 to 45
     term : 10 to 30
     FR7V1P1 :
     707
     PPT : 7
     
     FR10V1P1 : 
     708
     PPT : 10
     
     FR15V1P1 :
     709
     PPT : 15
     
     Age : 25 to 50
     term : 10 to 35
     FR7V1P2 :
     710
     PPT : 7
     
     
     FR10V1P2 :
     711
     PPT : 10
     
    
     FR15V1P2 : 
     712
     PPT : 15
     */
  
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"25" forKey:@"InsAge"];//18 to 50
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"26" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"A" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"FR10V1P2" forKey:@"BasePlan"];
    [reqDict setValue:@"500000" forKey:@"Sumassured"];

    //[reqDict setObject:@"35000" forKey:@"BasePremium"];
    [reqDict setValue:@"35" forKey:@"PolicyTerm"];
    [reqDict setValue:@"10" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"A" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Female" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"25" forKey:@"ProposerAge"];
    
    //rider
    [reqDict setValue:@"500000" forKey:@"ADDLN1V1"];
    
    return reqDict;
}

-(NSMutableDictionary *) getMIP{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    /*
     PGL ID - 215
     AGE - 0 TO 50
     FOR 7 : POLICY TERM - 15 : PPT - 7
     FOR 10 : POLICY TERM - 20 : PPT - 10
     MODE : M
     
     MALE :
     --------------
     MIN PREMUM - 2000
     MI7MV1P1
     696
     
     MI10MV1P1
     697
     ----------------
     
     ----------------
     MIN PREMUM - 50000
     MI7MV1P2
     698
     
     MI10MV1P2
     699
     ----------------
     
     ----------------
     MIN PREMUM - 100000
     MI7MV1P3
     700
     
     MI10MV1P3
     701
     ----------------
     
     FEMALE :
     ----------------
     MIN PREMUM - 2000
     MI7FV1P1
     713
     
     MI10FV1P1
     714
     ----------------
     
     ----------------
     MIN PREMUM - 50000
     MI7FV1P2
     715
     
     MI10FV1P2
     716
     ----------------
     
     ----------------
     MIN PREMUM - 100000
     MI7FV1P3
     717

     MI10FV1P3
     718
     ----------------
     */
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"50" forKey:@"InsAge"];//18 to 50
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"B02" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"26" forKey:@"OwnAge"];
    [reqDict setValue:@"B02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MI10MV1P1" forKey:@"BasePlan"];
    [reqDict setValue:@"4524887" forKey:@"Sumassured"];
    
    [reqDict setObject:@"40000" forKey:@"BasePremium"];
    [reqDict setValue:@"20" forKey:@"PolicyTerm"];
    [reqDict setValue:@"10" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"B02" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"50" forKey:@"ProposerAge"];
    
    //rider
    [reqDict setValue:@"4524000" forKey:@"ADDLN1V1"];
    
    return reqDict;
}

-(NSMutableDictionary *)getFortunePro{
    /*
     product id 199
     All has min vesting age of 18yrs
     and maturity age 75yrs
     all have sum assures 500000 to infinte excluding single pay..
     No start point..
     Frequecy is ASQM excluding single pay..
     
     Sum Assured = Premium * Prem_mult
     
     Premium value for Single Pay is 100000 to 500000
     
     for Juvenile eq:--- if age is 2, policy term = 16 to 40
     
     632 IPR1ULN1 -----     Term 15-40   Single Pay  PPT -> 1   age 0 to 59  SA 125000 to infinite  Frequency A
     633 IPR5ULN1 -----     Term 15-40   PPT - 5  Age 0 to 29
     644 IPR5ULN2 -----     Term 15-40   PPT - 5  Age 30 to 35
     645 IPR5ULN3 -----     Term 15-40   PPT - 5  Age 36 to 45
     646 IPR5ULN4 -----     Term 15-40   PPT - 5  Age 46 to 59
     647 IPR7ULN1 -----     Term 15-40   PPT - 7  Age 0 to 29
     648 IPR7ULN2 -----     Term 15-40   PPT - 7  Age 30 to 35
     649 IPR7ULN3 -----     Term 15-40   PPT - 7  Age 36 to 45
     650 IPR7ULN4 -----     Term 15-40   PPT - 7  Age 46 to 59
     651 IPR2ULN1 -----     Term 15-40   PPT - same as pol term  Age 0 to 29
     652 IPR2ULN2 -----     Term 15-40   PPT - same as pol term  Age 30 to 35
     653 IPR2ULN3 -----     Term 15-40   PPT - same as pol term  Age 36 to 45
     654 iPR2ULN4 -----     Term 15-40   PPT - same as pol term  Age 46 to 59
     
     
     sum_assured = base_premium * prem_mult;
     base_premium = sum_assured / prem_mult;
     
     key ->Base premium value ->premium value according to mode value.
     key ->Base Premium Annual -> premium value base on annual mode.
     */
    
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Fortune Pro" forKey:@"InsName"];
    [reqDict setValue:@"36" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"O" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"IPR1ULN1" forKey:@"BasePlan"];
    [reqDict setValue:@"125000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"15" forKey:@"PolicyTerm"];
    [reqDict setValue:@"1" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"O" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"36" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"100000" forKey:@"BasePremiumAnnual"];
    [reqDict setObject:@"Payable" forKey:@"Commission"];
    [reqDict setValue:@"1.25" forKey:@"PremiumMult"];
    //[reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    //[reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"100" forKey:@"WLA"];
    [reqDict setValue:@"0" forKey:@"WLE"];
    [reqDict setValue:@"0" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    
    [reqDict setValue:@"" forKey:@"WOPULV1"];
    [reqDict setValue:@"" forKey:@"WOPPULV1"];
    [reqDict setValue:@"" forKey:@"ADDLULV2"];
    //*********************************ULIP*********************************
    return reqDict;

}

-(NSMutableDictionary *) getWealthPro{
    
    /*
     product id 202
     All has min vesting age of 18yrs
     and maturity age 75yrs
     all have sum assures 2500000 to infinte excluding single pay..
     No start point..
     Frequecy is ASQM excluding single pay..
     
     Sum Assured = Premium * Prem_mult
     
     for Juvenile eq:--- if age is 2, policy term = 16 to 40
     
     642 WPR1ULN1 -----     Term 15-40   Single Pay  PPT -> 1   age 0 to 60  SA 625000 to infinite  Frequency A
     643 WPR5ULN1 -----     Term 15-40   PPT - 5  Age 0 to 30
     657 WPR5ULN2 -----     Term 15-40   PPT - 5  Age 31 to 40
     658 WPR5ULN3 -----     Term 15-40   PPT - 5  Age 41 to 50
     659 WPR5ULN4 -----     Term 15-40   PPT - 5  Age 51 to 60
     660 WPR7ULN1 -----     Term 15-40   PPT - 7  Age 0 to 30
     661 WPR7ULN2 -----     Term 15-40   PPT - 7  Age 31 to 40
     662 WPR7ULN3 -----     Term 15-40   PPT - 7  Age 41 to 50
     663 WPR7ULN4 -----     Term 15-40   PPT - 7  Age 51 to 60
     664 WPR2ULN1 -----     Term 15-40   PPT - same as pol term  Age 0 to 30
     665 WPR2ULN2 -----     Term 15-40   PPT - same as pol term  Age 31 to 40
     666 WPR2ULN3 -----     Term 15-40   PPT - same as pol term  Age 41 to 50
     667 WPR2ULN4 -----     Term 15-40   PPT - same as pol term  Age 51 to 60
     
     
     sum_assured = base_premium * prem_mult;
     base_premium = sum_assured / prem_mult;
     
     key ->Base premium value ->premium value according to mode value.
     key ->Base Premium Annual -> premium value base on annual mode.
     */
    
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Wealth Maxima" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"O" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"WPR1ULN1" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"]; // 150000 to infinite
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"15" forKey:@"PolicyTerm"]; //max  10 to 30
    [reqDict setValue:@"1" forKey:@"PPTerm"]; //max ppt 30
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"O" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"4000000" forKey:@"BasePremiumAnnual"];
    [reqDict setObject:@"Payable" forKey:@"Commission"];
    [reqDict setValue:@"1.25" forKey:@"PremiumMult"];
    [reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    [reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"0" forKey:@"WLA"];
    [reqDict setValue:@"100" forKey:@"WLE"];
    [reqDict setValue:@"0" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    //*********************************ULIP*********************************
    return reqDict;
}

-(NSMutableDictionary *)getFortuneMaxima{
    /*
     no start point
     age 0 to 60
     base premium = 250000 min
     premium 40 to 100
     
     ProductID = 200
     
     636 IMX1ULN1                                  --> Single pay                  PPT ---> 1
     669 IMX2ULN1                                  --> Limited Pay                 PPT ---> 15,10,20
     638 IMX7ULN1                                  --> Limited Pay                 PPT ---> 7
     637 IMX8ULN1                                  --> Limited Pay                 PPT ---> 8
     668 IMX9ULN1                                  --> Limited Pay                 PPT ---> 9
     
     Policy Term  ---> 100 - InsuredAge
     Sum Assured  ---> Premium * prem_mult
     Death Benefit Option.
     base premium multiple  --->if ppt = 1,1.25,MAX(10,(70-insage)/2))
     
     IMX7ULN1 - >
     age -> 40
     SA -> 5000000
     PT -> 60
     PPT ->7
     PM -> 15
     B.Premium -> 333333
     Freq -> M
     
     IMX1ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->1
     PM -> 1.25
     B.Premium -> 4000000
     Freq -> O
     
     IMX2ULN1 - >
     age -> 60
     SA -> 5000000
     PT -> 40
     PPT ->15
     PM -> 10
     B.Premium -> 500000
     Freq -> M
     
     IMX8ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->8
     PM -> 10
     B.Premium -> 500000
     Freq -> A
     
     IMX9ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->9
     PM -> 10
     B.Premium -> 500000
     Freq -> M
     */
    
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Fortune Maxima" forKey:@"InsName"];
    [reqDict setValue:@"34" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"Q" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"IMX2ULN1" forKey:@"BasePlan"];
    [reqDict setValue:@"900000" forKey:@"Sumassured"]; // 150000 to infinite
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"66" forKey:@"PolicyTerm"]; //max  10 to 30
    [reqDict setValue:@"15" forKey:@"PPTerm"]; //max ppt 30
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"Q" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"34" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"50000" forKey:@"BasePremiumAnnual"];
    [reqDict setValue:@"18" forKey:@"PremiumMult"];
    //[reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    //[reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"0" forKey:@"WLA"];
    [reqDict setValue:@"100" forKey:@"WLE"];
    [reqDict setValue:@"0" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    //rider
    [reqDict setValue:@"900000" forKey:@"ADDLULV2"];
    [reqDict setValue:@"Payable" forKey:@"Commission"];
    
    //*********************************ULIP*********************************
    return reqDict;
}

-(NSMutableDictionary *)getWealthMaxima{
    /* 
     no start point
     age 0 to 60
     base premium = 250000 min
     premium 40 to 100
     
     639 WMX1ULN1                                  --> Single pay                  PPT ---> 1
     656 WMX2ULN1                                  --> Limited Pay                 PPT ---> 15,10,20
     641 WMX7ULN1                                  --> Limited Pay                 PPT ---> 7
     640 WMX8ULN1                                  --> Limited Pay                 PPT ---> 8
     655 WMX9ULN1                                  --> Limited Pay                 PPT ---> 9
    
     Policy Term  ---> 100 - InsuredAge
     Sum Assured  ---> Premium * prem_mult
     Death Benefit Option.
     base premium multiple  --->if ppt = 1,1.25,MAX(10,(70-insage)/2))
     
     WMX7ULN1 - >
     age -> 40
     SA -> 5000000
     PT -> 60
     PPT ->7
     PM -> 15
     B.Premium -> 333333
     Freq -> M
     
     WMX1ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->1
     PM -> 1.25
     B.Premium -> 4000000
     Freq -> O
     
     WMX2ULN1 - >
     age -> 60
     SA -> 5000000
     PT -> 40
     PPT ->15
     PM -> 10
     B.Premium -> 500000
     Freq -> M
    
     WMX8ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->8
     PM -> 10
     B.Premium -> 500000
     Freq -> A
    
     WMX9ULN1 - >
     age -> 50
     SA -> 5000000
     PT -> 50
     PPT ->9
     PM -> 10
     B.Premium -> 500000
     Freq -> M
    */
    
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Wealth Maxima" forKey:@"InsName"];
    [reqDict setValue:@"45" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"O" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"WMX1ULN1" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"]; // 150000 to infinite
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"55" forKey:@"PolicyTerm"]; //max  10 to 30
    [reqDict setValue:@"1" forKey:@"PPTerm"]; //max ppt 30
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"O" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"45" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"4000000" forKey:@"BasePremiumAnnual"];
    [reqDict setValue:@"1.25" forKey:@"PremiumMult"];
    [reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    [reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"0" forKey:@"WLA"];
    [reqDict setValue:@"100" forKey:@"WLE"];
    [reqDict setValue:@"0" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    [reqDict setValue:@"Payable" forKey:@"Commission"];
    //*********************************ULIP*********************************
    return reqDict;
}

-(NSMutableDictionary *)getSmartGrowthPlus{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    //SGPV1N1 ..
    //age : 0 to 50
    [reqDict setValue:@"Smart Growth Plus" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"A" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SGPV1N1" forKey:@"BasePlan"];
    [reqDict setValue:@"300000" forKey:@"Sumassured"]; // 150000 to infinite
    [reqDict setValue:@"10" forKey:@"PolicyTerm"]; //max  10 to 30
    [reqDict setValue:@"10" forKey:@"PPTerm"]; //max ppt 30
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"A" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"26" forKey:@"ProposerAge"];
    
    //reduced paid year...
    //ALternate sis Reduced paid up...
    //[reqDict setValue:@"4" forKey:@"RPU_Year"];
    //[reqDict setValue:@"0.3" forKey:@"TaxSlab"];
    return reqDict;
}
//no 8% and 4%
-(NSMutableDictionary *)getSecure7FrontEndData{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    //secure 7
    [reqDict setValue:@"SECURE7" forKey:@"InsName"];
    [reqDict setValue:@"40" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SEC7V1N1" forKey:@"BasePlan"];
    
    [reqDict setValue:@"5500000" forKey:@"Sumassured"]; // 250000 to 99999999
    [reqDict setValue:@"14" forKey:@"PolicyTerm"];
    [reqDict setValue:@"7" forKey:@"PPTerm"]; //max ppt 15
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //ASQM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER SECURE7" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"26" forKey:@"ProposerAge"];
    
    //reduced paid year...
    //ALternate sis Reduced paid up...
    [reqDict setValue:@"4" forKey:@"RPU_Year"];
    [reqDict setValue:@"0.3" forKey:@"TaxSlab"];
    return reqDict;
}

-(NSMutableDictionary *)getMoneyMaximaFrondEndData{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //secure 7
    [reqDict setValue:@"MONEY MAXIMA" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//12 to 50
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"INSURANCE" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MMXV1N1" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"]; // 100000 to 999999999
    [reqDict setValue:@"30" forKey:@"PolicyTerm"];
    //ppterm will be equal to pol term...
    [reqDict setValue:@"30" forKey:@"PPTerm"]; //max 30
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    [reqDict setValue:@"0" forKey:@"RpuYear"];
    [reqDict setValue:@"0.1" forKey:@"TaxSlab"];
    [reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"22.0" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    //ASM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"26" forKey:@"ProposerAge"];
    
    return reqDict;
}

-(NSMutableDictionary *)getMahaLifeGoldPlusFrontEndData{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //secure 7
    [reqDict setValue:@"MAHALIFE GOLD PLUS" forKey:@"InsName"];
    [reqDict setValue:@"55" forKey:@"InsAge"];//0 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"INSURANCE" forKey:@"InsOccDesc"];
    [reqDict setValue:@"WO2" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"A" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MLGPV1N1" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"];//200000 to infinite
    //if insured age is blank then NA else 85-InsuredAge i.e 85-50
    [reqDict setValue:@"30" forKey:@"PolicyTerm"];
    [reqDict setValue:@"15" forKey:@"PPTerm"];// max = 15
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    [reqDict setValue:@"0" forKey:@"RpuYear"];
    [reqDict setValue:@"0.1" forKey:@"TaxSlab"];
    [reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"22.0" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    //ASM
    [reqDict setValue:@"A" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"15" forKey:@"ProposerAge"];
    return reqDict;
}

-(NSMutableDictionary *)getMahaLifeMagicFrontEndData{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //secure 7
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];// 12 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"INSURANCE" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MLMV1N1" forKey:@"BasePlan"];
    [reqDict setValue:@"5500000" forKey:@"Sumassured"];//200000 to 999999999
    [reqDict setValue:@"18" forKey:@"PolicyTerm"];
    [reqDict setValue:@"9" forKey:@"PPTerm"]; //max = 7
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    
    ///ASM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"15" forKey:@"ProposerAge"];
    //reduced paid year...
    //ALternate sis Reduced paid up...
    [reqDict setValue:@"5" forKey:@"RPU_Year"];
    [reqDict setValue:@"0.3" forKey:@"TaxSlab"];
    return reqDict;
}

-(NSMutableDictionary *)getMahaLifeGoldFrontEndData{
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //secure 7
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//0 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"INSURANCE" forKey:@"InsOccDesc"];
    [reqDict setValue:@"WO2" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MLGV2N1" forKey:@"BasePlan"];
    
    [reqDict setValue:@"4000000" forKey:@"Sumassured"];//200000 to infinite
    //if insured age is blank, then its NA or it will be 85-insuredage..
    [reqDict setValue:@"55" forKey:@"PolicyTerm"];
    [reqDict setValue:@"15" forKey:@"PPTerm"]; //max 15
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    [reqDict setValue:@"0" forKey:@"RpuYear"];
    [reqDict setValue:@"0.1" forKey:@"TaxSlab"];
    [reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"22.0" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    //ASM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"15" forKey:@"ProposerAge"];
    return reqDict;
}
//no 8% and 4%
-(NSMutableDictionary *)getMahaLifeSupremeFrontEndData{
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //FOR MAHALIFE SUPREME
    //CHANGED THE DATABSE DATA
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"25" forKey:@"InsAge"];//18 to 50
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"26" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MLS2NB" forKey:@"BasePlan"];
    //[reqDict setValue:@"MLS2NA" forKey:@"BasePlan"];
    /*
     if(mls2na)
     // 150000 to 999999999
     //max ppt = 15
     else
     // 200000 to 999999999
     //max ppt = 12
     */
    [reqDict setValue:@"3000000" forKey:@"Sumassured"];
    //for mahalife supreme base premium is annualized premium...
    [reqDict setObject:@"35000" forKey:@"BasePremium"];
    [reqDict setValue:@"30" forKey:@"PolicyTerm"];
    //[reqDict setValue:@"35" forKey:@"PolicyTerm"];
    [reqDict setValue:@"12" forKey:@"PPTerm"];
    //[reqDict setValue:@"15" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
   
    //ASQM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"15" forKey:@"ProposerAge"];
    
    return reqDict;
}
//no 8% and 4%
-(NSMutableDictionary *)getMahaRakshaSupremeFrontEndData{
    /*
     if(MRSP1N1V2){
     age= 18 to 49
     sum assured is 5000000 to 999999999
     max ppt = 25
     ASQM
     }
     if(MRSP1N2V2){
     age= 18 to 70
     sum assured is 5000000 to 999999999
     max ppt = 25
     ASQM
     }
     if(MRSP2N1V2){
     age= 18 to 49
     sum assured is 5000000 to 999999999
     max ppt = 1
     O
     }
     if(MRSP2N2V2){
     age= 18 to 70
     sum assured is 5000000 to 999999999
     max ppt = 1
     O
     }
     */
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc] init];
    //secure 7
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"25" forKey:@"InsAge"];//18 to 49
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"INSURANCE" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"O" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MRSP2N2V2" forKey:@"BasePlan"];
    
    [reqDict setValue:@"10000000" forKey:@"Sumassured"];
    [reqDict setValue:@"10" forKey:@"PolicyTerm"];
    //same as pol term
    [reqDict setValue:@"1" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //[reqDict setValue:@"0" forKey:@"RpuYear"];
    //[reqDict setValue:@"0.1" forKey:@"TaxSlab"];
    //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"1.58" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    [reqDict setValue:@"O" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"F" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"25" forKey:@"ProposerAge"];
    
    //[reqDict setValue:@"1000000" forKey:@"ADDLN1V1"];
    
    return reqDict;
}
//Added by Amruta
-(NSMutableDictionary *)getFortuneGurantee{
    
    /*
    Plan code=FGV1N1
    PGL_ID=211
    PNL_ID=691
    Age = 8 to 55
    PolicyTerm=10;
    PPT=5;
    Premium multiple=10
    Min SA = 500000
    Min Premium = 50000
    Starting Point = P
     */
    
    
    NSMutableDictionary *reqDict=[[NSMutableDictionary alloc]init];
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"32" forKey:@"InsAge"];
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"Insurance" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"F" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"FGV1N1" forKey:@"BasePlan"];
    
    [reqDict setValue:@"500000" forKey:@"Sumassured"];
    [reqDict setValue:@"10" forKey:@"PolicyTerm"];
    [reqDict setObject:@"50000" forKey:@"BasePremium"];
    [reqDict setValue:@"5"forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1990" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //[reqDict setValue:@"0" forKey:@"RpuYear"];
    //[reqDict setValue:@"" forKey:@"TaxSlab"];
    //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Female" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"32" forKey:@"ProposerAge"];
    
    //[reqDict setObject:@"600000" forKey:@"BasePremiumAnnual"];
    //[reqDict setValue:@"" forKey:@"WOPULV1"];
    //[reqDict setValue:@"" forKey:@"WOPPULV1"];
    //[reqDict setValue:@"" forKey:@"ADDLULV2"];
    //max limit sumassured..
   // [reqDict setValue:@"3000000" forKey:@"ADDLV1N2"];
    
    
    return reqDict;
}

-(NSMutableDictionary *)getMoneyBackPlus{
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc]init];
    
    /*
     PGL_ID = 205
     
     plan_code = MBPV1N1
     pnl_id = 676
     PPT = 8
     age = 2 to 51
     Min SA = 200000
     Term = 16
     
     plan_code = MBPV1N2
     pnl_id = 677
     PPT = 8
     Min age = 2 to 51
     Min SA = 200000
     Term = 20
 
     plan_code = MBPV1N3
     pnl_id = 678
     PPT = 8
     Min age = 2 to 51
     Min SA = 200000
     Term = 24
     */

    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    //[reqDict setValue:@"27" forKey:@"InsAge"];
    [reqDict setValue:@"25" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"Insurance" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"MBPV1N3" forKey:@"BasePlan"];
    
    [reqDict setValue:@"5000000" forKey:@"Sumassured"];
    //[reqDict setValue:@"16" forKey:@"PolicyTerm"];
    //[reqDict setValue:@"20" forKey:@"PolicyTerm"];
    [reqDict setValue:@"24" forKey:@"PolicyTerm"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    //[reqDict setValue:@"10" forKey:@"PPTerm"];
    [reqDict setValue:@"12" forKey:@"PPTerm"];
    //[reqDict setValue:@"8" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1964" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //[reqDict setValue:@"0" forKey:@"RpuYear"];
    //[reqDict setValue:@"" forKey:@"TaxSlab"];
    //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"25" forKey:@"ProposerAge"];
    
    return reqDict;

}

-(NSMutableDictionary *)getSmart7{
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc]init];
    
    /*
     Plan Code = SMART7V1N1
     pnl_pgl_id = 198
     pnl_id = 631
     age = 6 to 55
     min SA = 200000
     pol term = 12
     ppt = 7
     */
    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"Insurance" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"18" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SMART7V1N1" forKey:@"BasePlan"];
    
    [reqDict setValue:@"200000" forKey:@"Sumassured"];
    [reqDict setValue:@"12" forKey:@"PolicyTerm"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"7"forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1985" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //[reqDict setValue:@"0" forKey:@"RpuYear"];
    //[reqDict setValue:@"" forKey:@"TaxSlab"];
    //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    //[reqDict setValue:@"200000" forKey:@"ADDLN1V1"];
    [reqDict setValue:@"" forKey:@"WPPN1V1"];
    
    
    return reqDict;

}

-(NSMutableDictionary *)getInstaWealth{
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc]init];
    
    /*
     pgl_id - 206
     
     Plan Code - IWV1N10
     pnl_id - 679
     age - 8 to 45
     min SA - 100000
     PPT - 7
     SA bove 1000000 not accepted for age 23
     
     Plan Code - IWV1N15
     pnl_id - 680
     age - 3 to 45
     min SA - 100000
     PPT - 10
     
     Plan Code - IWV1N20
     pnl_id - 681
     min age - 0 to 45
     min SA - 100000
     PPT - 15
     
     */
    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"19" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"Insurance" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"IWV1N10" forKey:@"BasePlan"];
    
    [reqDict setValue:@"1000000" forKey:@"Sumassured"];
    [reqDict setValue:@"10" forKey:@"PolicyTerm"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"7"forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1985" forKey:@"PropDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    //[reqDict setValue:@"0" forKey:@"RpuYear"];
    //[reqDict setValue:@"" forKey:@"TaxSlab"];
    //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Male" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"19" forKey:@"ProposerAge"];
    
    return reqDict;
    
    
}

-(NSMutableDictionary *)getiRaksha{
    
    NSMutableDictionary *reqDict = [[NSMutableDictionary alloc]init];
    
    /*
     pgl_id = 191
     SA = 5000000
     start pt = S
     
     pnl_id = 622
     pnl_code = IRSRP1N1V2
     age = 18 to 70
     PPT = 10
     
     pnl_id = 623
     pnl_code = IRSL51N1V2
     age = 18 to 70
     
     pnl_id = 624
     pnl_code = IRSL10N1V2
     age = 18 to 65
     
     pnl_id = 625
     pnl_code = IRSSP1N1V2
     age = 18 to 70
     */
    
    [reqDict setValue:@"ABC" forKey:@"InsName"];
    [reqDict setValue:@"45" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"Insurance" forKey:@"InsOccDesc"];
    [reqDict setValue:@"W12" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"45" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ACTUARY" forKey:@"OwnOccDesc"];
    [reqDict setValue:@"Y" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    //Added by Amruta on 23/10/2015 for iRaksha
    [reqDict setValue:@"10/13/1955" forKey:@"InsDob"];
    
    //plan code
    [reqDict setValue:@"IRSL51N1V2" forKey:@"BasePlan"];
    
    [reqDict setValue:@"10000000" forKey:@"Sumassured"];
    [reqDict setValue:@"11" forKey:@"PolicyTerm"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"5"forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1985" forKey:@"ProDOB"];
    [reqDict setValue:@"T" forKey:@"ProdTye"];
    [reqDict setValue:@"0" forKey:@"RpuYear"];
    [reqDict setValue:@"" forKey:@"TaxSlab"];
    [reqDict setValue:@"0" forKey:@"ExpBonusRate"];
    [reqDict setValue:@"100" forKey:@"PremiumMul"];
    [reqDict setValue:@"1" forKey:@"Commision"];
    
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"ABC" forKey:@"ProposerName"];
    [reqDict setValue:@"Female" forKey:@"ProposerGender"];
    [reqDict setValue:@"W12" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"45" forKey:@"ProposerAge"];
    
    return reqDict;
    
}


-(NSMutableDictionary *)getSuperAchiever{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    //PPT : 10
    //POLTERM : 10 to 20
    //SA : 240000 to 999999999
    //Premium : 24000
    /*
     Super Achiever :
     SAULN1
     ID : 692
     PGL ID : 212
     AGE : 25 to 50
     PREM_MULT : 10
     */
    
    [reqDict setValue:@"Super Achiever" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SAULN1" forKey:@"BasePlan"];
    [reqDict setValue:@"600000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"15" forKey:@"PolicyTerm"];
    [reqDict setValue:@"10" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"60000" forKey:@"BasePremiumAnnual"];
    [reqDict setObject:@"Payable" forKey:@"Commission"];
    [reqDict setValue:@"10" forKey:@"PremiumMult"];
    //[reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    //[reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"0" forKey:@"WLA"];
    [reqDict setValue:@"100" forKey:@"WLE"];
    [reqDict setValue:@"0" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    //new two funds
    [reqDict setValue:@"0" forKey:@"MCF"];
    [reqDict setValue:@"0" forKey:@"ICF"];
    
    //[reqDict setValue:@"" forKey:@"WOPULV1"];
    //[reqDict setValue:@"" forKey:@"WOPPULV1"];
    //[reqDict setValue:@"" forKey:@"ADDLULV2"];
    [reqDict setValue:@"" forKey:@"WPPN1V1"];
    //*********************************ULIP*********************************
    return reqDict;
    
}

-(NSMutableDictionary *)getInvestOne{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    //PPT : 15
    //POLTERM : 15 to 30
    //SA : 125000 to 999999999
    //Premium : 100000
    /*
     Super Achiever :
     IONEV1N1
     ID : 670
     PGL ID : 203
     AGE : 0 to 20
     
     IONEV1N2
     ID : 671
     PGL ID : 203
     AGE : 21 to 29
     
     IONEV1N3
     ID : 672
     PGL ID : 203
     AGE : 30 to 38
     
     IONEV1N4
     ID : 673
     PGL ID : 203
     AGE : 39 to 60
     */
    
    [reqDict setValue:@"Invest One" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];//18 to 55
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"30" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"O" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"IONEV1N3" forKey:@"BasePlan"];
    [reqDict setValue:@"2000000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"15" forKey:@"PolicyTerm"];
    [reqDict setValue:@"1" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"U" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"O" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    //*********************************ULIP*********************************
    //new parameters..
    [reqDict setObject:@"1600000" forKey:@"BasePremiumAnnual"];
    [reqDict setObject:@"Payable" forKey:@"Commission"];
    [reqDict setValue:@"1.25" forKey:@"PremiumMult"];
    //[reqDict setValue:@"TLC" forKey:@"SmartDebtFund"];
    //[reqDict setValue:@"TLC" forKey:@"SmartEquFund"];
    [reqDict setValue:@"0" forKey:@"TLC"];
    [reqDict setValue:@"0" forKey:@"WLA"];
    [reqDict setValue:@"0" forKey:@"WLE"];
    [reqDict setValue:@"50" forKey:@"WLF"];
    [reqDict setValue:@"0" forKey:@"WLI"];
    [reqDict setValue:@"0" forKey:@"WLS"];
    //new two funds
    [reqDict setValue:@"50" forKey:@"MCF"];
    [reqDict setValue:@"0" forKey:@"ICF"];
    
    //[reqDict setValue:@"" forKey:@"WOPULV1"];
    //[reqDict setValue:@"" forKey:@"WOPPULV1"];
    //Added by Amruta
    [reqDict setValue:@"600000" forKey:@"ADDLULV2"];

    //*********************************ULIP*********************************
    return reqDict;
}

-(NSMutableDictionary *)getGIP{
    
    /*Plan code:
     
     PGL_ID - 226
     
     1. GIP5I10N1 - 781 - 6 to 65
     2. GIP5I15N1 - 782 - 6 to 65
     3. GIP12I10N1 - 783 - 3 to 55
     4. GIP12I15N1 - 784 - 3 to 55
     
     Policy term for 5 pay - 12
     Policy term for 12 pay - 15
     
     PPT for 5 pay - 5
     PPT for 12 pay - 12
     */
    
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Gold Income Plan" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"19" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"A" forKey:@"PaymentMode"];
    
    
    //plan code
    [reqDict setValue:@"GIP5I10N1" forKey:@"BasePlan"];
    [reqDict setValue:@"164500" forKey:@"Sumassured"];
    [reqDict setObject:@"50000" forKey:@"BasePremium"];
    [reqDict setValue:@"12" forKey:@"PolicyTerm"];
    [reqDict setValue:@"5" forKey:@"PPTerm"];
    [reqDict setValue:@"10" forKey:@"incomeBooster"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"A" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"35" forKey:@"ProposerAge"];
    
    //[reqDict setValue:@"160000" forKey:@"ADDLN1V1"];
    
    return reqDict;
    
    
}

-(NSMutableDictionary *)getVitalPro{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    /*
     Min age - 18
     Max Age - 65
     Policy Term = PPT = 10/15/20/25/30
     
     VCPSPV1N1 - 733
     VCPSPV1N2 - 734
     VCPSPV1N3 - 735
     VCPSPV1N4 - 736
     */
    [reqDict setValue:@"Vital Pro" forKey:@"InsName"];
    [reqDict setValue:@"26" forKey:@"InsAge"];
    [reqDict setValue:@"M" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"19" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"1" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"VCPFPPV1N3" forKey:@"BasePlan"];
    [reqDict setValue:@"5500000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"30" forKey:@"PolicyTerm"];
    [reqDict setValue:@"30" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"M" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"26" forKey:@"ProposerAge"];
    
    return reqDict;
}

-(NSMutableDictionary *)getSampooranaRaksha{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    /*
     SAMPOORNA RAKSHA
     
     Plan code - SRL5V1N1,SRL5V1N2,SRL5V1N3,SRL5V1N4
     Age - 18 to 70
     Policy Term - 10 to 40
     Premium Term - 5 to 40
     Min SA - 5000000
     
     Plan code - SRL10V1N1,SRL10V1N2,SRL10V1N3,SRL10V1N4
     Age - 18 to 65
     Policy Term - 10 to 40
     Premium Term - 5 to 40
     Min SA - 5000000
     
     Plan code - SRRPV1N1,SRRPV1N2,SRRPV1N3,SRRPV1N4
     Age - 18 to 70
     Policy Term - 10 to 40
     Premium Term - 5 to 40
     Min SA - 5000000
     
     SAMPOORNA RAKSHA PLUS
     
     PlanCode
     
     
     */
    
    [reqDict setValue:@"Sampoorna Raksha" forKey:@"InsName"];
    [reqDict setValue:@"30" forKey:@"InsAge"];
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"19" forKey:@"OwnAge"];
    [reqDict setValue:@"W02" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"Y" forKey:@"InsSmoker"];
    [reqDict setValue:@"M" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SRL10V1N4" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"15" forKey:@"PolicyTerm"];
    [reqDict setValue:@"10" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"M" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"F" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"30" forKey:@"ProposerAge"];
    
    [reqDict setValue:@"500000" forKey:@"ADDLN1V1"];
    return reqDict;
    
    
}


-(NSMutableDictionary *)getSampooranaRakshaPlus{
    NSMutableDictionary *reqDict = [NSMutableDictionary dictionary];
    
    [reqDict setValue:@"Sampoorna Raksha +" forKey:@"InsName"];
    [reqDict setValue:@"22" forKey:@"InsAge"];
    [reqDict setValue:@"F" forKey:@"InsSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"InsOccDesc"];
    [reqDict setValue:@"Z07" forKey:@"InsOcc"];
    [reqDict setValue:@"Amit" forKey:@"OwnName"];
    [reqDict setValue:@"19" forKey:@"OwnAge"];
    [reqDict setValue:@"Z07" forKey:@"OwnOcc"];
    [reqDict setValue:@"M" forKey:@"OwnSex"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"OwnOccDesc"];
    //1 is non smoker and 0 is smoker
    [reqDict setValue:@"N" forKey:@"InsSmoker"];
    [reqDict setValue:@"S" forKey:@"PaymentMode"];
    
    //plan code
    [reqDict setValue:@"SRPL5V1N2" forKey:@"BasePlan"];
    [reqDict setValue:@"5000000" forKey:@"Sumassured"];
    [reqDict setObject:@"" forKey:@"BasePremium"];
    [reqDict setValue:@"10" forKey:@"PolicyTerm"];
    [reqDict setValue:@"5" forKey:@"PPTerm"];
    [reqDict setValue:@"165464646" forKey:@"PropNo"];
    [reqDict setValue:@"Y" forKey:@"SAgeProofFlg"];
    [reqDict setValue:@"11/01/1998" forKey:@"PropDOB"];
    [reqDict setValue:@"C" forKey:@"ProdTye"];
    
    //ASQM
    [reqDict setValue:@"S" forKey:@"Frequency"];
    [reqDict setValue:@"N" forKey:@"TataEmployee"];
    [reqDict setValue:@"PROPOSER smart" forKey:@"ProposerName"];
    [reqDict setValue:@"F" forKey:@"ProposerGender"];
    [reqDict setValue:@"Z07" forKey:@"ProposerOccupation"];
    [reqDict setValue:@"ADMINISTRATOR/CLERK" forKey:@"ProposerDescOcc"];
    [reqDict setValue:@"22" forKey:@"ProposerAge"];
    
    [reqDict setValue:@"500000" forKey:@"ADDLN1V1"];
    
    return reqDict;
    
    
}

/*
     //[reqDict setValue:@"200000" forKey:@"BasePremAnnual"];
 //    [reqDict setValue:@"Agent_xyz" forKey:@"ProposerName"];
 //    [reqDict setValue:@"Male" forKey:@"ProposerSex"];
 //    [reqDict setValue:@"Part Time" forKey:@"ProposerOccupation"];
 //    [reqDict setValue:@"Q" forKey:@"Frequency"];
 //    [reqDict setValue:@"Main_Plan" forKey:@"BasePlan"];
 //    [reqDict setValue:@"50000" forKey:@"Sumassured"];
 //    [reqDict setValue:@"10" forKey:@"PolicyTerm"];
 //    [reqDict setValue:@"Prod_type" forKey:@"ProductType"];
 //    [reqDict setValue:@"8" forKey:@"PremiumPayingTerm"];
 //    [reqDict setValue:@"40" forKey:@"OwnerAge"];
 //
 //    [reqDict setValue:@"Call center" forKey:@"ProposerOccupation_Desc"];
 //    [reqDict setValue:@"123445" forKey:@"ProposalNo"];
 //    [reqDict setValue:@"1/12/2014" forKey:@"ProposalDate"];
 //    [reqDict setValue:@"Yes" forKey:@"TataEmployee"];
 //    [reqDict setValue:@"5/4/1978" forKey:@"InsuredDOB"];
 //    [reqDict setValue:@"3/12/1967" forKey:@"ProposerDOB"];
 //    [reqDict setValue:@"Yes" forKey:@"Premium_Mul"];
 //
 //
 //    //to ask Vivek
 //    [reqDict setValue:@"rpu_yr" forKey:@"RPU_Year"];
 //    [reqDict setValue:@"op_type" forKey:@"OperationType"];
 //    [reqDict setValue:@"tax_slab" forKey:@"TaxSlab"];
 //    [reqDict setValue:@"FixWithDEndYr" forKey:@"FixWithDEndYr"];
 //    [reqDict setValue:@"FixWithDAmt" forKey:@"FixWithDAmt"];
 //    [reqDict setValue:@"FixTopUpAmt" forKey:@"FixTopUpAmt"];
 //    [reqDict setValue:@"FixTopUpStartYr" forKey:@"FixTopUpStartYr"];
 //    [reqDict setValue:@"FixTopUpEndYr" forKey:@"FixTopUpEndYr"];
 //    [reqDict setValue:@"VarWithDAmtYr" forKey:@"VarWithDAmtYr"];
 //    [reqDict setValue:@"VarTopUpAmtYr" forKey:@"VarTopUpAmtYr"];
 //    [reqDict setValue:@"FundPerform" forKey:@"FundPerform"];
 //    [reqDict setValue:@"SmartDebtFund" forKey:@"SmartDebtFund"];
 //    [reqDict setValue:@"SmartDebtFundFMC" forKey:@"SmartDebtFundFMC"];
 //    [reqDict setValue:@"SmartEquityFund" forKey:@"SmartEquityFund"];
 //    [reqDict setValue:@"SmartEquityFundFMC" forKey:@"SmartEquityFundFMC"];
 //    [reqDict setValue:@"AAA" forKey:@"AAA"];
 //    [reqDict setValue:@"Exp_Bonus_Rate" forKey:@"Exp_Bonus_Rate"];
 //    [reqDict setValue:@"yes" forKey:@"AgeProofFlag"];
 //    [reqDict setValue:@"Non Standard" forKey:@"AgeProof"];
 //    [reqDict setValue:@"comm" forKey:@"Commission"];
 //[reqDict setValue:@"0" forKey:@"RpuYear"];
 //[reqDict setValue:@"0.1" forKey:@"TaxSlab"];
 //[reqDict setValue:@"0" forKey:@"ExpBonusRate"];
 //[reqDict setValue:@"22.0" forKey:@"PremiumMul"];
 //[reqDict setValue:@"1" forKey:@"Commision"];
 */

@end
