//
//  NSString+Contains.m
//  LifePlanner
//
//  Created by DrasttyMac on 19/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (Contains)

//implemented for ios 8.0 below..
-(BOOL)containsString:(NSString *)subString{
    NSRange range = [self rangeOfString:subString];
    BOOL found = (range.location != NSNotFound);
    return found;
}

@end
