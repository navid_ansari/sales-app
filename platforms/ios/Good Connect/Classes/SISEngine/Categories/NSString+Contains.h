//
//  NSString+Contains.h
//  LifePlanner
//
//  Created by DrasttyMac on 19/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Contains)

-(BOOL)containsString:(NSString *)str;

@end
