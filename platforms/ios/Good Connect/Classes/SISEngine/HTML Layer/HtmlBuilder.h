//
//  HtmlBuilder.h
//  LifePlanner
//
//  Created by DrasttyMac on 08/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelMaster.h"
#import "Constant.h"
#import "AddressBO.h"
#import "DataAccessClass.h"
#import "Rider.h"

@interface HtmlBuilder : NSObject

@property (nonatomic, strong) ModelMaster *master;

-(NSString *)buildHtml;
-(id)initWithData:(ModelMaster *)_master;
-(NSString *)getAccTableGai:(int)pol andWithCount:(int)count;



@end
