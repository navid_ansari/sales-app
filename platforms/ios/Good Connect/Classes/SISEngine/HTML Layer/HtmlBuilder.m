//
//  HtmlBuilder.m
//  LifePlanner
//
//  Created by DrasttyMac on 08/12/14.
//  Copyright (c) 2014 talic. All rights reserved.
//

#import "HtmlBuilder.h"


long count = 0;

@implementation HtmlBuilder
@synthesize master;



#pragma mark -- INIT METHODS
-(id)initWithData:(ModelMaster *)_master{
    self = [super init];
    if(self){
        master = _master;
    }
    return self;
}

#pragma CREATE SIS HTML REPORT
-(NSString *)buildHtml{

    NSString *altYesNo = @"No";
    if ((master.sisRequest.TopUpWDSelected != NULL   && [master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) || (master.sisRequest.FpSelected != NULL   && [master.sisRequest.FpSelected isEqualToString:@"YES"]) || (master.sisRequest.SmartSelected != NULL   && [master.sisRequest.SmartSelected isEqualToString:@"YES"]) ){

        altYesNo = @"Yes";
    }

    NSString *fileNameOptional = @".html";
    NSString *outputFile = nil;
    if (outputFile == NULL) {

        //outputFile = [OUTPUT_DIR stringByAppendingString:master.planDescription];
    }

    if (((master.sisRequest.TopUpWDSelected != NULL   && [master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) || (master.sisRequest.FpSelected != NULL   && [master.sisRequest.FpSelected isEqualToString:@"YES"])) && (master.sisRequest.SmartSelected != NULL   && [master.sisRequest.SmartSelected isEqualToString:@"YES"])){

        fileNameOptional  = @"_all.html";

    }else if ((master.sisRequest.TopUpWDSelected != NULL   && [master.sisRequest.TopUpWDSelected isEqualToString:@"YES"])|| (master.sisRequest.FpSelected != NULL   && [master.sisRequest.FpSelected isEqualToString:@"YES"])){

        fileNameOptional  = @"_alt1.html";

    }else if(master.sisRequest.SmartSelected != NULL   && [master.sisRequest.SmartSelected isEqualToString:@"YES"]){

        fileNameOptional  = @"_alt2.html";
    }
    //NSLog(@"the alt yes no %@ and filename option is %@ and op file %@",altYesNo,fileNameOptional,outputFile);
     //NSString *inputFile1 = [NSString stringWithFormat:@"%@ %@%@",INPUT_DIR,master.planDescription,fileNameOptional];
     //NSString *outputTempFile =[TEMP_OUTPUT_DIR stringByAppendingString:[self getUniquePDFFileName:master.planDescription]];
     //NSString *outputFilePath = outputFile;
    //NSString *inputFile =[NSString stringWithFormat:@"%@%@",master.planDescription,fileNameOptional];
//    NSString *inputFile = [[NSBundle mainBundle] pathForResource:master.planDescription ofType:@"html"];
//    NSLog(@"the HTML file is %@",inputFile);

    // Sneha 7 Dec 16
    NSString *filePath = [[NSBundle mainBundle] pathForResource:master.planDescription ofType:@"html" inDirectory:@"www/templates/SIS"];

//    NSData *myData =[NSData dataWithContentsOfFile:inputFile];
//    NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];
    NSData *myData =[NSData dataWithContentsOfFile:filePath];
    NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];

    NSError *err;
    NSString *regexStr = @"\\|\\|[a-zA-Z_0-9\\p{Punct}]+\\|\\|" ;
    NSRegularExpression *regex =[NSRegularExpression regularExpressionWithPattern:regexStr options:0 error:&err];

//    NSString *finalHtml =  [self createHTMLSISReport:inputString:regex:inputFile];
    NSString *finalHtml =  [self createHTMLSISReport:inputString:regex:filePath];
    return finalHtml;
}

-(NSMutableString *) createHTMLSISReport:(NSString *)inputString :(NSRegularExpression *)regex :(NSString *)_fileName{

    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:inputString];
    SISRequest *requestBean = master.sisRequest;

    FILE *file = fopen([_fileName UTF8String], "r");
    char buffer[256];
    while (fgets(buffer, 256, file) != NULL){
        NSTextCheckingResult *matchingResult = [regex firstMatchInString:mutableString options:0 range:NSMakeRange(0,[mutableString length])];
        NSString *patterenStr = [[mutableString substringWithRange:[matchingResult rangeAtIndex:0]] stringByReplacingOccurrencesOfString:@"|" withString:@""];
        Boolean testing = matchingResult!=NULL;
        while (testing) {
            NSLog(@"the string is %@",patterenStr);
            testing = false;
            if ([patterenStr hasPrefix:ONLYVERSION]) {

                AddressBO *addressBO = nil;
                NSString *replaceMentStr = @"";

                if(master.addressBOObj != nil || master.addressBOObj != NULL){
                    addressBO = master.addressBOObj;
                    replaceMentStr =addressBO.SIS_VERSION;
                }

                if(replaceMentStr != nil || replaceMentStr.length != 0){
                    replaceMentStr = [replaceMentStr substringWithRange:NSMakeRange(0, 5)];
                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if([patterenStr hasPrefix:IMAGE]){
                NSString *imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Header_%@",master.planDescription] ofType:PNG inDirectory:@"www/templates/SIS"];
                NSString *repalceStr = @"";

                if(imagePath != nil){
                    NSData *data = [NSData dataWithContentsOfFile:imagePath];
                    NSString *str = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                    repalceStr = [NSString stringWithFormat:@"'data:image/png;base64,%@'",str];
                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:repalceStr];

            }

            if([patterenStr hasPrefix:WITHOUT_SALES]){
                NSString *imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Header_%@",master.planDescription] ofType:JPG inDirectory:@"www/templates/SIS"];
                NSLog(@"IMAGENAME:,%@",imagePath);
                NSString *repalceStr = @"";

                if(imagePath != nil){
                    NSData *data = [NSData dataWithContentsOfFile:imagePath];
                    NSString *str = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                    repalceStr = [NSString stringWithFormat:@"'data:image/jpg;base64,%@'",str];
                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:repalceStr];
            }

            if([patterenStr hasPrefix:AGENTNAME]){
                //if (master.Agent_interMedDetail.agentName != nil) {
                   // [mutableString replaceCharactersInRange:[matchingResult range] withString:master.Agent_interMedDetail.agentName];
                //}else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@"@@AGENTNAME@@"];

            }
            if([patterenStr hasPrefix:AGENTCONTACTNUMBER]){
                if(master.Agent_interMedDetail.agentContactNumber != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:master.Agent_interMedDetail.agentContactNumber];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }
            if([patterenStr hasPrefix:AGENTNUMBER]){
                //if(master.Agent_interMedDetail.agentNumber != nil)
                    //[mutableString replaceCharactersInRange:[matchingResult range] withString:master.Agent_interMedDetail.agentNumber];
                //else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@"@@AGENTNUMBER@@"];
            }

            if([patterenStr hasPrefix:PROPOSERNAME]){
                if(master.sisRequest.ProposerName != nil){
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:master.sisRequest.ProposerName];
                }else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }

            if([patterenStr hasPrefix:DATE]){
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                NSString *dateInString = [dateFormatter stringFromDate:[NSDate date]];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:dateInString];
            }

            //********************************SGP*********************************
            if([patterenStr hasPrefix:@"SGP_TABLEGAI"]){
                //changes 6th oct
                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getAccTableGai:1 andWithCount:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

           /* if ([patterenStr hasPrefix:@"COMISSIONTEXT"]) {
                NSString *replaceMentStr=[NSString stringWithFormat:master.CommissionText];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }*/

            if([patterenStr hasPrefix:@"POLICYOPTION"]){
                NSString *replaceStr = @"";
                if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"MBPV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"FR7V1P1"] || [master.sisRequest.BasePlan isEqualToString:@"FR10V1P1"] || [master.sisRequest.BasePlan isEqualToString:@"FR15V1P1"]){
                    replaceStr = @"Option 1";
                }
                else if([master.sisRequest.BasePlan isEqualToString:@"SGPV1N2"] || [master.sisRequest.BasePlan isEqualToString:@"MBPV1N2"] || [master.sisRequest.BasePlan isEqualToString:@"FR7V1P2"] || [master.sisRequest.BasePlan isEqualToString:@"FR10V1P2"] || [master.sisRequest.BasePlan isEqualToString:@"FR15V1P2"]){
                    replaceStr = @"Option 2";
                }
                else if ([master.sisRequest.BasePlan isEqualToString:@"MBPV1N3"]){
                    replaceStr = @"Option 3";
                }
                //changes 19th feb...
                else if([master.sisRequest.BasePlan isEqualToString:@"SIP7IV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SIP10IV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SIP12IV1N1"]){
                    replaceStr = @"Regular Income Benefit";
                }
                else if([master.sisRequest.BasePlan isEqualToString:@"SIP7EV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SIP10EV1N1"] || [master.sisRequest.BasePlan isEqualToString:@"SIP12EV1N1"]){
                    replaceStr = @"Endowment Benefit";
                }
                //Added by Amruta for Vital Pro
                else if ([master.sisRequest.BasePlan containsString:@"VCPSPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFPV1N"]){
                    replaceStr = @"Pro Care";
                }
                else if ([master.sisRequest.BasePlan containsString:@"VCPSPPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFPPV1N"]){
                    replaceStr = @"Pro Care Plus";
                }
                else if ([master.sisRequest.BasePlan containsString:@"VCPSDV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFDV1N"]){
                    replaceStr = @"Duo Care";
                }
                else if ([master.sisRequest.BasePlan containsString:@"VCPSDPV1N"] || [master.sisRequest.BasePlan containsString:@"VCPFDPV1N"]){
                    replaceStr = @"Duo Care Plus";
                }
                else if ([master.sisRequest.BasePlan containsString:@"SR"] && [master.sisRequest.BasePlan containsString:@"V1N1"]){
                    replaceStr = @"Option 1 : \"Sum Assured on Death\" payble on death";
                }
                else if ([master.sisRequest.BasePlan containsString:@"SR"] && [master.sisRequest.BasePlan containsString:@"V1N2"]){
                    replaceStr = @"Option 2 : \"Sum Assured on Death\" payble on death &amp; monthly Income thereafter for 10 years";
                }
                else if ([master.sisRequest.BasePlan containsString:@"SR"] && [master.sisRequest.BasePlan containsString:@"V1N3"]){
                    replaceStr = @"Option 3 : Enhanced \"Sum Assured on Death\" payble on death";
                }
                else if ([master.sisRequest.BasePlan containsString:@"SR"] && [master.sisRequest.BasePlan containsString:@"V1N4"]){
                    replaceStr = @"Option 4 : Enhanced \"Sum Assured on Death\" payble on death &amp; monthly Income thereafter for 10 years";
                }	
                else if ([master.sisRequest.BasePlan containsString:@"GIP5I10"] || [master.sisRequest.BasePlan containsString:@"GIP12I10"]){
                    replaceStr = @"Income Term 10";
                }
                else if ([master.sisRequest.BasePlan containsString:@"GIP5I15"] || [master.sisRequest.BasePlan containsString:@"GIP12I15"]){
                    replaceStr = @"Income Term 15";
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceStr];
            }
            if([patterenStr hasPrefix:@"RIDERTEXT"])
            {
                NSString *replaceStr = @"";
                for (Rider *_rider in master.riderData) {
                    Rider *rider = _rider;
                    NSLog(@"rider code is %@",rider.RDL_CODE);
                    if ([rider.RDL_CODE isEqualToString:@"WOPULV1"] || [rider.RDL_CODE isEqualToString:@"WOPPULV1"] || [rider.RDL_CODE isEqualToString:@"WPPN1V1"]) {
                        replaceStr = @"** The benefit equals all future premiums payable under the base policy from the date of claim for a period as specified in the policy contract. Please refer the policy contract for more details.";
                    }else
						replaceStr = @"";
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceStr];
            }
            //********************************SGP*********************************

            //SIP
            if ([patterenStr hasPrefix:TOTGUARBENEFIT]) {
                int pt = (int)master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTableTotGuarBenefit:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:@"AGENTCHANNEL"]) {
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }

            if ([patterenStr hasPrefix:TableSIPSSURRBEN]) {
                int pt = (int)master.sisRequest.PolicyTerm;
                int count  = pt > 15 ? pt : 15;
                NSString *replaceMentStr = [self getTableSurBenefit:1 andWithPolTerm:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //SIP

            if ([patterenStr hasPrefix:AGEPROOF]) {

                NSString *replaceMentStr = requestBean.AgeProof;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PROPNO]) {

                NSString *replaceMentStr =requestBean.ProposalNo;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PROPDATE]) {
                NSDateFormatter *dateForamtter = [[NSDateFormatter alloc]init];
                //changes 13-4-2015..
                //6th may changes iraksha
                if ([requestBean.BasePlan containsString:@"IRS"] || [requestBean.BasePlan containsString:@"IRTR"]) {
                    [dateForamtter setDateFormat:@"MM-dd-yyyy hh:mm:ss.SSS"];
                }else{
                    [dateForamtter setDateFormat:@"MM-dd-yyyy,hh:mm aa"];
                }
                NSDate *date =[[NSDate alloc]init];
                NSString *replaceMentStr =[dateForamtter stringFromDate:date];
                NSLog(@"proposdal date is %@",replaceMentStr);
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            
            if ([patterenStr hasPrefix:SISUIN]) {

                NSString *replaceMentStr =master.UINNumber;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:INSNAME]) {

                NSString *replaceMentStr =requestBean.InsuredName;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:COVPINSNAME]) {

                NSString *replaceMentStr =@"";
                replaceMentStr = [NSString stringWithFormat:@"%@",requestBean.InsuredName];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
			
			//changes 7th feb..
			if([patterenStr hasPrefix:@"COVERPAGE"]){
				NSString *replaceMentStr =@"";
				
				if([master.sisRequest.BusinessType isEqualToString:@"IndusSolution"]){
					replaceMentStr =@"";
				}else{
					replaceMentStr =@"none";
				}
				
				[mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
			}
			
			if([patterenStr hasPrefix:@"FIRSTPAGE"]){
				NSString *replaceMentStr =@"";
				
				if([master.sisRequest.BusinessType isEqualToString:@"IndusSolution"]){
					replaceMentStr =@"always";
				}else{
					replaceMentStr =@"none";
				}
				
				[mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
			}
			
            if ([requestBean.BasePlan containsString:@"VCPSP"] || [requestBean.BasePlan containsString:@"VCPFP"]){
                if ([patterenStr hasPrefix:PROPNAME])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                if ([patterenStr hasPrefix:PROPDOB])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                if ([patterenStr hasPrefix:PROPOCC])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                if ([patterenStr hasPrefix:PROPDESCOCC])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                if ([patterenStr hasPrefix:PROPAGE])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                if ([patterenStr hasPrefix:PROPGENDER])
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }else{
                if ([patterenStr hasPrefix:PROPNAME]) {
                    
                    NSString *replaceMentStr =requestBean.ProposerName;
                    if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                    
                }
                if ([patterenStr hasPrefix:PROPDOB]) {
                    //changes 21-12-2015 dob issue
                    //NSString *replaceMentStr =[self getFormattedDate:requestBean.ProposerDOB];
                    NSString *replaceMentStr = requestBean.ProposerDOB;
					//changes 27th jan added [requestBean.BasePlan containsString:@"MRS"]
                    if ([requestBean.BasePlan containsString:@"IRS"] || [requestBean.BasePlan containsString:@"IRTR"] || [requestBean.BasePlan containsString:@"MRS"] || [requestBean.BasePlan containsString:@"MLS"]) {
                        replaceMentStr = [self getDOB:requestBean.ProposerDOB];
                    }
                    if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                }
                if ([patterenStr hasPrefix:PROPOCC]) {
                    NSString *replaceMentStr = @"";
                    if(requestBean.ProposerOccupation != nil)
                    replaceMentStr =requestBean.ProposerOccupation;
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                }
                if ([patterenStr hasPrefix:PROPDESCOCC]) {
                    
                    NSString *replaceMentStr = @"";//requestBean.ProposerOccupation_Desc;
                    if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                }
                if ([patterenStr hasPrefix:PROPAGE]) {
                    
                    NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithInteger:requestBean.ProposerAge],@""];
                    if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                    
                }
                if ([patterenStr hasPrefix:PROPGENDER]) {
                    
                    //Sneha
                    //NSString *replaceMentStr =requestBean.ProposerSex;
                    //6th may iraksha changes
                    //                NSString *replaceMentStr = @"";
                    //                if ([requestBean.BasePlan containsString:@"IRS"] || [requestBean.BasePlan containsString:@"IRTR"]) {
                    //                    replaceMentStr = [self getFullGender:requestBean.ProposerSex];
                    //                }else{
                    ////                    replaceMentStr =requestBean.ProposerSex;
                    //                }
                    NSString *replaceMentStr = [self getFullGender:requestBean.ProposerSex];
                    if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
                    
                }
            }
            

            if ([patterenStr hasPrefix:INSDOB]) {
                //changes 21-12-2015 dob issue
                //NSString *replaceMentStr =[self getFormattedDate:requestBean.InsuredDOB];
                NSString *replaceMentStr = requestBean.InsuredDOB;
                if ([requestBean.BasePlan containsString:@"IRS"] || [requestBean.BasePlan containsString:@"IRTR"] || [requestBean.BasePlan containsString:@"MRS"] || [requestBean.BasePlan containsString:@"MLS"]) {
                    replaceMentStr = [self getDOB:requestBean.InsuredDOB];
                }

                if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }

            if ([patterenStr hasPrefix:INSOCC]) {
                NSString *replaceMentStr = @"";
                if(requestBean.InsuredOccupation != nil)
                    replaceMentStr =requestBean.InsuredOccupation;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:INSDESCOCC]) {

                NSString *replaceMentStr = @"";// requestBean.InsuredOccupation_Desc;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:INSAGE]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%d%@",requestBean.InsuredAge,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:INSGENDER]) {
                //Sneha
                //6th may iraksha changes
//                NSString *replaceMentStr = @"";
//                if ([requestBean.BasePlan containsString:@"IRS"] || [requestBean.BasePlan containsString:@"IRTR"]) {
//                    replaceMentStr = [self getFullGender:requestBean.InsuredSex];
//                }else{
//                   replaceMentStr =requestBean.InsuredSex;
//                }
                 NSString *replaceMentStr = [self getFullGender:requestBean.InsuredSex];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            


            //MIP
            if([patterenStr hasPrefix:@"MIP_TableRIDPEM"]){
                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getTableRider_MIP:1 andWithPolTerm:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:@"MLG_TableBasicSum"]) {
                int pt = master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getTableBasicSumMIP:1 andWithPolTerm:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if([patterenStr hasPrefix:@"MIP_TableTOTPEM"]){
                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30;
                NSString *replaceMentStr = [self getTableTotalPremium_MIP:1 andWithPolTerm:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //MIP


            

            if ([patterenStr hasPrefix:PNLTERM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithInteger:requestBean.PolicyTerm],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:PNLPM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",requestBean.Premium_Mul,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:PNLPPT]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithInteger:requestBean.PremiumPayingTerm],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:INSSMOKER]) {

                NSString *replaceMentStr =@"";
                //Changed by Amruta from "N"
                if ([requestBean.InsuredSmokerStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {

                    replaceMentStr =@"Non Smoker";
                }else{

                    replaceMentStr =@"Smoker";

                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PNLMODE]) {

                NSString *replaceMentStr =@"";
                if (requestBean.Frequency != NULL && requestBean.Frequency.length != 0) {

                    if ([requestBean.Frequency caseInsensitiveCompare:@"A"] == NSOrderedSame) {

                        replaceMentStr = @"Annual";
                    }
                    if ([requestBean.Frequency caseInsensitiveCompare:@"S"] == NSOrderedSame) {

                        replaceMentStr =@"SemiAnnual";//@"HalfYearly";

                    }
                    if ([requestBean.Frequency caseInsensitiveCompare:@"Q"] == NSOrderedSame) {

                        replaceMentStr = @"Quarterly";
                    }
                    if ([requestBean.Frequency caseInsensitiveCompare:@"M"] == NSOrderedSame) {

                        replaceMentStr = @"Monthly";
                    }
                    if ([requestBean.Frequency caseInsensitiveCompare:@"O"] == NSOrderedSame) {

                        replaceMentStr = @"Single";
                    }
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PNLSA]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@",[NSNumber numberWithLongLong:requestBean.Sumassured]];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PNLMODPREM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithLong:master.ModalPremium],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:TOTPREM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithLong:master.TotalModalPremium],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:TOTSERVTAX]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithLong:master.serviceTax],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:TOTSTPREM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",[NSNumber numberWithLong:master.TotalModalPremiumPayble],@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:TablePA]) {

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;

                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }

                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:TableULIPPA]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt;
                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMaximaPA]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt;
                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //****************************************************************************************
            //changes 05/06/2015
            //for MAHARAKSHA SUPREME PREVIOUS TEMPLATE...
            //FOR OTHER TEMPLATE USE 2 TABLES...
            if ([patterenStr hasPrefix:MRSTablePA]) {

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;

                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if([patterenStr hasPrefix:MRSTableAGE]){
                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                int age =(int)master.sisRequest.InsuredAge;
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:MRSTablePEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MRSTableRIDPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                NSString *replaceMentStr =[self getTableRider:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MRSTableTOTPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                //NSString *replaceMentStr =[self getTableTotalPremium:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MRSTableCOV]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                //NSString *replaceMentStr =[self getTableCoverage:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:MRSTableSURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //****************************************************************************************



            if ([patterenStr hasPrefix:TableAGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                int age =(int)master.sisRequest.InsuredAge;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableULIPAGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt  ;
                int age =(int)master.sisRequest.InsuredAge;
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMaximaAGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt  ;
                int age =(int)master.sisRequest.InsuredAge;
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMaximaPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt  ;
                NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableULIPPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt  ;
                NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableRIDPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getTableRider:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTOTPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //added by Ganesh 03-03-2016
               /* if([master.sisRequest.BasePlan isEqualToString:@"MLS2NA"]){
                    count = 35;
                }
                else if([master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = 30;
                }*/

                //uncommented Amruta- MLS issue resolved
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableTotalPremium:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableCOV]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableCoverage:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMATVAL] ) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLS2NA"]){
                    count = 35;
                }
                else if([master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = 30;
                }
                //Commented by Amruta
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] /*||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]*/){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableMaturityValue:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MaturityValue withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if([patterenStr hasPrefix:Table4MATVAL] || [patterenStr hasPrefix:MLG_Table4MATVAL]){
                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ; //for mahalife gold plus changed the value 30 to 36..
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableMaturityBenefit4:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalMaturityBenefit4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //FREEDOM
            if ([patterenStr hasPrefix:@"TableFR4MATVAL"]) {
                int pt = (int) master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTableMaturityBenefit4:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //FREEDOM

            if ([patterenStr hasPrefix:TableSURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"] ||
                   [master.sisRequest.BasePlan containsString:@"SRL"] ||
                   [master.sisRequest.BasePlan containsString:@"SRR"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableSurrenderBenefit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2PA]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                NSString *replaceMentStr =[self getTablePA:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2AGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                int age = (int)master.sisRequest.InsuredAge + 30;
                NSString *replaceMentStr =[self getTableAGE:31 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2PEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                NSString *replaceMentStr =[self getTablePremium:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2RIDPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                NSString *replaceMentStr =[self getTableRider:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2TOTPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableTotalPremium:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2COV]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableCoverage:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2MATVAL]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableMaturityValue:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MaturityValue withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2SURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                //changes 02/06/2015
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableSurrenderBenefit:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TablePA]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85 ;
                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableAGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                int age = (int)master.sisRequest.InsuredAge;
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            /*if ([patterenStr hasPrefix:MLG_TablePEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }*/
			
            if ([patterenStr hasPrefix:MLG_TableRIDPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr =[self getTableRider:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableTOTPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                //NSString *replaceMentStr =[self getTableTotalPremium:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableCOV]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }



            //Added by Amruta on 24/8/15 for MoneyBackPlus
            if ([patterenStr hasPrefix:MBP_TABLEGAI]) {

                int pt = (int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GAIAll withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

             if ([patterenStr hasPrefix:MLG_TableMATVAL]) {
                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MaturityValue withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableGSURRBEN] || [patterenStr hasPrefix:TableGSURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedSurrValue withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TABLEGAI] || [patterenStr hasPrefix:TableGAI]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedAnnualIncome withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TABLE4ANNCASH]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TABLE8ANNCASH]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_Table4TOTALDEATHBENEFIT] ||
                [patterenStr hasPrefix:MLG_Table4TOTALDEATHBENEFIT1]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //************************************GIP*********************//
            if ([patterenStr hasPrefix:GIP_TablePA]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr =[self getTablePA:1 andWithEndPt:pt+incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //GIP
            if ([patterenStr hasPrefix:GIP_TableAGE]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                int age = (int)master.sisRequest.InsuredAge;
                NSString *replaceMentStr =[self getTableAGE:1 andWithEndPt:pt+incomeTerm andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TablePEM]) {
                NSString *replaceMentStr = @"";
                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                replaceMentStr =[self getTablePremium:1 andWithEndPt:pt+incomeTerm];

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TableTOTPEM]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:1 andWithEndPt:pt+incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP(pending)
            if ([patterenStr hasPrefix:GIP_TableCOV]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getHTMLForParticularTagGIP:master.LifeCoverage withStartPt:1 andWithEndPt:pt andWithIncTerm:incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TableMATVAL]) {
                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getHTMLForParticularTagGIP:master.MaturityValue withStartPt:1 andWithEndPt:pt andWithIncTerm:incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TableGSURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getHTMLForParticularTagGIP:master.GuaranteedSurrValue withStartPt:1 andWithEndPt:pt andWithIncTerm:incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TableGAI]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedAnnualIncome withStartPt:1 andWithEndPt:pt+incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableGIPSSURRBEN]) {
                int pt = (int)master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getTableSurBenefitGIP:1 andWithPolTerm:pt andWithIncTerm:incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //GIP
            if ([patterenStr hasPrefix:GIP_TableRIDPEM]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int ppt =(int) master.sisRequest.PremiumPayingTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getTableRiderOnPPTSmart7:1 andWithEndPt:pt+incomeTerm andWithShowPt:ppt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //GIP
            if ([patterenStr hasPrefix:TOTGIPGUARBENEFIT]) {
                int pt = (int)master.sisRequest.PolicyTerm;
                int incomeTerm = master.sisRequest.incomeBooster;
                NSString *replaceMentStr = [self getTableTotGuarBenefit:1 andWithPolTerm:pt+incomeTerm];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //Good kid

            if ([patterenStr hasPrefix:Table4COV_GK]) {

            }

            if ([patterenStr hasPrefix:GK_TableMILESTONEADD4]) {
                int pt =(int) master.sisRequest.PolicyTerm;

                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MilestoneAdd4Dict withStartPt:1 andWithEndPt:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:GK_TableMILESTONEADD8]) {
                int pt =(int) master.sisRequest.PolicyTerm;

                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MilestoneAdd8Dict withStartPt:1 andWithEndPt:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:GK_TableMBB]) {
                int pt =(int) master.sisRequest.PolicyTerm;

                NSString *replaceMentStr = [self getHTMLForParticularTag:master.MoneyBackBenefitDict withStartPt:1 andWithEndPt:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:COV4_GK]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getSA4GK:NULL withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }


            //good kid

            if ([patterenStr hasPrefix:MLG_Table8TOTALDEATHBENEFIT] ||
                [patterenStr hasPrefix:MLG_Table8TOTALDEATHBENEFIT1]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableNG4SURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableNG8SURRBEN]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit_8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TOT_CASH_DIV_4]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalCashDividend4];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TOT_CASH_DIV_8]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalCashDividend8];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TOT_G_ANNCOUP]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalGAnnualCoupon];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableGuaranteedInflationCover] || [patterenStr hasPrefix:MLG_TABLEIPC]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedInflationCover withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:MLG_TableDeathBenefit] || [patterenStr hasPrefix:MLG_TableTDB]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //changes 4th march ADDL changes in html tag also in MLGP
            if ([patterenStr hasPrefix:@"MLG_TDB8"]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"MLG_DeathBen4"]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //changes 4th march ADDL

            if ([patterenStr hasPrefix:MLG_Table4VSRBOUNS]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if([patterenStr hasPrefix:TOTALRB8]){
                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalCashDividend8];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if([patterenStr hasPrefix:TOTALRB4]){
                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalCashDividend4];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if([patterenStr hasPrefix:TOTALGAI]){
                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld",master.TotalGAnnualCoupon];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:MLG_Table8VSRBOUNS]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }


            if ([patterenStr hasPrefix:ADDR]) {

                NSString *replaceMentStr =[self getAddress];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //CHANGES 02/06/2015
            if ([patterenStr hasPrefix:TABADDR]) {

                NSString *replaceMentStr =[self getAddress];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:RIDNAME]) {

                //NSString *replaceMentStr =[self getRiderNames];
                NSString *replaceMentStr = @"";
                if (master.riderData.count != 0) {
                    replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_DESCRIPTION];
                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RIDSA]) {

                //NSString *replaceMentStr =[self getRiderSA];
                NSString *replaceMentStr = @"";
                if (master.riderData.count != 0) {
                    replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_SUMASSURED];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:RIDUIN]) {
                NSString *replaceMentStr = @"";
                if (master.riderData.count != 0) {
                    replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:@"RIDER_UIN"];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }



            if ([patterenStr hasPrefix:RIDMODPREM]) {

                //NSString *replaceMentStr =[self getRiderModalPremium];
                NSString *replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_MODALPREMIUM];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PNLFEATURE] || [patterenStr hasPrefix:PNLBENTYPE]) {

                NSString *replaceMentStr =master.PlanFeatureStr;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:RIDTERM]) {

                NSString *replaceMentStr =@"";
                if (master.riderData.count != 0) {
                    replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_POLICYTERM];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:RIDPPT]) {

                //NSString *replaceMentStr =[self getRiderPremPayTerm];
                NSString *replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_PREMIUMPAYINGTERM];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if (master.sisRequest.FundList != NULL && master.sisRequest.FundList.count != 0) {
                NSMutableArray *fundListArr = master.sisRequest.FundList;
                for (int cnt = 0 ; cnt < fundListArr.count; cnt++) {
                    FundDetails *fundDetailsObj = [fundListArr objectAtIndex:cnt];
                    if ([patterenStr hasPrefix:fundDetailsObj.Code]) {

                        NSString *replaceMentStr =[NSString stringWithFormat:@"%d%@",fundDetailsObj.Percentage,@""];
                        [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                    }
                }
            }

            if ([patterenStr hasPrefix:STRIDERPREM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.STRiderModalPremium,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTALRIDPREM]) {
                //changes 18th feb
                long d = master.TotalRiderModalPremium;
                NSString *replaceMentStr =@(d).stringValue;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTALPREMRIDST]) {
                //changes 18th feb
                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%ld%ld%@",master.ModalPremium,master.TotalRiderModalPremium,master.STRiderModalPremium,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PNLBASEPREM]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.BaseAnnualizedPremium,@""];
                if ([master.sisRequest.ProductType isEqualToString:@"U"]) {

                    replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.sisRequest.BasePremiumAnnual,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RIDBASEPREM]) {

                //NSString *replaceMentStr =[self getRiderBasePrem];
                NSString *replaceMentStr = [self getHTMLRiderTagContent:master.riderData withCheckValue:RIDER_P];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            if ([patterenStr hasPrefix:PREMIUMMULTI]) {

                NSString *replaceMentStr =[NSString stringWithFormat:@"%f%@",master.PremiumMultiple,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePACVAL]) {

                //NSString *replaceMentStr =[self getTablePremiumAllocationCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.PremiumAllocationCharges];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PACPPTVAL]) {

                NSString *replaceMentStr =[self getTablePremiumAllocationChargesPPT];
                if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }



            if ([patterenStr hasPrefix:TableAMTINV]) {

                //NSString *replaceMentStr =[self getTableAmountFprInvestment];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.AmountForInvest];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableOBIC]) {

                //NSString *replaceMentStr =[self getTableOtherBenefitInbuiltCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.OtherBenefitCharges];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableADMCHRG]) {

                //NSString *replaceMentStr =[self getTablePolicyAdminCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.PolicyAdminCharges];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMORTCHRG]) {

                //NSString *replaceMentStr =[self getTableMortalityCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.MortalityCharges];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6MORTCHRG]) {

                //NSString *replaceMentStr =[self getTableMortalityChargesPH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.MortalityChargesPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableOTHCHRG]) {

                //NSString *replaceMentStr =[self getTableOtherCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.OtherBenefitCharges];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableAPPSERTAX]) {

                //NSString *replaceMentStr =[self getTableApplicableServiceTax];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.ApplicableServiceTaxPH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTOTC]) {

                //NSString *replaceMentStr =[self getTableTotalCharge];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.TotalChargeForPh10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableCTOTAL6]) {

                //NSString *replaceMentStr =[self getTablePH6TotalCharge];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.TotalChargesForPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableRFVBFMC]) {

                //NSString *replaceMentStr =[self getTableRegularFundValueBeforeFMC];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValueBeforeFMCPH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableRFVPFMC]) {

                //NSString *replaceMentStr =[self getTableRegularFundValuePostFMC];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValuePostFMCPH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table6RFVPFMC]) {

                //NSString *replaceMentStr =[self getTableRegularFundValuePostFMC6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValuePostFMCPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableFMC]) {

                //NSString *replaceMentStr =[self getTableFundManagementCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.FundManagementChargePH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableRFVAEPYBGMA]) {

                //NSString *replaceMentStr =[self getTableRegularFundValueAtEndPolicyYearBeforeGMA];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValuePH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableGMA]) {

                //NSString *replaceMentStr =[self getTableGuaranteedMaturityAddition];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.GuaranteedMaturityAdditionPH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTOTFUNDVAL]) {

                //NSString *replaceMentStr =[self getTableTotalFundValue];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.TotalFundValueAtEndPolicyYrPH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSURRENDERVAL]) {

                //NSString *replaceMentStr =[self getTableSurrenderValue];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.SurrenderValuePH10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableDBEP]) {

                //NSString *replaceMentStr =[self getTableDeathBenifitAtEndOfPolicyYear];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.DeathBenefitAtEndOfPolicyYear];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6DBEP]) {

                //NSString *replaceMentStr =[self getTableDeathBenifitAtEndOfPolicyYearPH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.DeathBenefitAtEndOfPolicyYearPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6APPSERTAX]) {

                //NSString *replaceMentStr =[self getTableApplicableServiceTax_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.ApplicableSTPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6FMC]) {

                //NSString *replaceMentStr =[self getTableFundManagementCharges_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.FundMgmtChargePH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6RFVAEPYBGMA]) {

                //NSString *replaceMentStr =[self getTableRegularFundValueAtEndPolicyYearBeforeGMA_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValuePH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6GMA]) {

                //NSString *replaceMentStr =[self getTableGuaranteedMaturityAddition_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.GuaranteedMaturityAdditionPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6TOTFUNDVAL]) {

                //NSString *replaceMentStr =[self getTableTotalFundValue_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.TotalFundValueAtEndPolicyYrPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6SURRENDERVAL]) {

                //NSString *replaceMentStr =[self getTableSurrenderValue_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.SurrenderValuePH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePH6RFVBFMC]) {

                //NSString *replaceMentStr =[self getTableRegularFundValueBeforeFMC_PH6];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValueBeforeFMCPH6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableLoyaltyCharges]) {

                //NSString *replaceMentStr =[self getTableLoyaltyCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.LoyalityChargeForPh10];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableMOYALTY6]) {

               // NSString *replaceMentStr =[self getTablePH6LoyaltyCharges];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.LoyalityChargeForPh6];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableGuaranteedAnnualIncome]) {

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTableGAI:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedAnnualIncome withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2GuaranteedAnnualIncome]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableGAI:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedAnnualIncome withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableGuaranteedSurrValue]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTableGuaranteedSurrValue:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedSurrValue withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2GuaranteedSurrValue]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableGuaranteedSurrValue:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.GuaranteedSurrValue withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PNLNSAP]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.ModalPremiumForNSAP,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:QUOTEDMODPREM]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.QuotedModalPremium,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:S_AAASURRENDERVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                   //replaceMentStr =[self getS_AAASurrenderValue];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:D_AAADBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                   // replaceMentStr =[self getS_AAADeathBenefitAtEndOfPolYear];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDL1]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>0) {
                    Rider *riderObj = [rdArr objectAtIndex:0];
                    replaceMentStr= [NSString stringWithFormat:@"%@ %@ %@",riderObj.RDL_DESCRIPTION,@"&",@"&amp"];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLSA1]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>0) {
                    Rider *riderObj = [rdArr objectAtIndex:0];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.SumAssured,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLTERM1]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>0) {
                    Rider *riderObj = [rdArr objectAtIndex:0];
                    replaceMentStr= [NSString stringWithFormat:@"%d%@",riderObj.PolTerm,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLMODPREM1]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>0) {
                    Rider *riderObj = [rdArr objectAtIndex:0];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.ModalPremium,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLUIN1]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>0) {
                    Rider *riderObj = [rdArr objectAtIndex:0];
                    replaceMentStr= [NSString stringWithFormat:@"%@%@ ",riderObj.RDL_UIN,@""];
                }
                if (replaceMentStr != NULL) {
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }
            if ([patterenStr hasPrefix:RDL2]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>1) {
                    Rider *riderObj = [rdArr objectAtIndex:1];
                    replaceMentStr= [NSString stringWithFormat:@"%@ %@ %@",riderObj.RDL_DESCRIPTION,@"&",@"&amp"];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLSA2]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>1) {
                    Rider *riderObj = [rdArr objectAtIndex:1];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.SumAssured,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLTERM2]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>1) {
                    Rider *riderObj = [rdArr objectAtIndex:1];
                    replaceMentStr= [NSString stringWithFormat:@"%d%@",riderObj.PolTerm,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLMODPREM2]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>1) {
                    Rider *riderObj = [rdArr objectAtIndex:1];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.ModalPremium,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLUIN2]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>1) {
                    Rider *riderObj = [rdArr objectAtIndex:1];
                    replaceMentStr= [NSString stringWithFormat:@"%@%@ ",riderObj.RDL_UIN,@""];
                }
                if (replaceMentStr != NULL) {
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }
            if ([patterenStr hasPrefix:RDL3]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>2) {
                    Rider *riderObj = [rdArr objectAtIndex:2];
                    replaceMentStr= [NSString stringWithFormat:@"%@ %@ %@",riderObj.RDL_DESCRIPTION,@"&",@"&amp"];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLSA3]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>2) {
                    Rider *riderObj = [rdArr objectAtIndex:2];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.SumAssured,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLTERM3]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>2) {
                    Rider *riderObj = [rdArr objectAtIndex:2];
                    replaceMentStr= [NSString stringWithFormat:@"%d%@",riderObj.PolTerm,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLMODPREM3]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rdArr = master.riderData;
                if (rdArr.count>2) {
                    Rider *riderObj = [rdArr objectAtIndex:2];
                    replaceMentStr= [NSString stringWithFormat:@"%ld%@",riderObj.ModalPremium,@""];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTOTMATVAL]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableTotalMaturityValue:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalMaturityValue withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table2TOTMATVAL]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt > 30 ? pt : 0 ;
                //NSString *replaceMentStr =[self getTableTotalMaturityValue:31 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalMaturityValue withStartPt:31 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table4VSRBONUS]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTable4VSRBonus:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //******FREEDOM**********
            if ([patterenStr hasPrefix:@"TableFR4VSRBONUS"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable4VSRBonus:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }


            //******FREEDOM**********


            if ([patterenStr hasPrefix:Table4TERMINALBONUS]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTable4TerminalBonus:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TerminalBonus4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //FREEDOM

            if ([patterenStr hasPrefix:@"TableFR4TERMINALBONUS"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTableTerminalBonus4:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //FREEDOM
            if ([patterenStr hasPrefix:Table8VSRBONUS]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTable8VSRBonus:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.VSRBonus8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //******FREEDOM**********
            if ([patterenStr hasPrefix:TABLEFR8VSRBONUS]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable8VSRBonus:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }


            //******FREEDOM**********

            if ([patterenStr hasPrefix:Table8TERMINALBONUS]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TerminalBonus8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //FREEDOM

            if ([patterenStr hasPrefix:@"TableFR8TERMINALBONUS"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTableTerminalBonus8:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //FREEDOM


            if ([patterenStr hasPrefix:Table8TOTALDEATHBENEFIT] || [patterenStr hasPrefix:Table8TDB]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTable8TotalDeathbenefit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //FREEDOM
            if ([patterenStr hasPrefix:@"TableFR8TOTALDEATHBENEFIT"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable8TotalDeathBenefit:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:@"TableFR4TOTALDEATHBENEFIT"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable4TotalDeathBenefit:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //FREEDOM

            if ([patterenStr hasPrefix:Table4TOTALDEATHBENEFIT] || [patterenStr hasPrefix:Table4TDB]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTable4TotalDeathbenefit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalDeathBenefit4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }


            if ([patterenStr hasPrefix:Table8SURRBEN]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTable8surrenderBenefit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit_8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //FREEDOM
            if ([patterenStr hasPrefix:@"TableFRNG8SURRBEN"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable8FRSurrenderBenefit:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:@"TableFRNG4SURRBEN"]) {
                int pt = master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTable4FRSurrenderBenefit:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }


            //FREEDOM

            if ([patterenStr hasPrefix:VERSION]){
                //changes 13-04-2015
                //db changes for new version in company master..
                AddressBO *addressObj = master.addressBOObj;
                NSString *replaceMentStr = addressObj.SIS_VERSION;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableCOMMISSION]){

                //NSString *replaceMentStr = [self getTableCommissionPayble];
                NSString *replaceMentStr = [self getHTMLDataForParticularTag:master.CommissionPayable];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:SMART7_TableRIDPEM]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int ppt =(int) master.sisRequest.PremiumPayingTerm;
                NSString *replaceMentStr = [self getTableRiderOnPPTSmart7:1 andWithEndPt:pt andWithShowPt:ppt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
			
			if ([patterenStr hasPrefix:MLG_TablePEM]) {
                NSString *replaceMentStr = @"";
                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 85 ? pt : 85;
                //Added by Amruta for Vital Pro
                int age =(int)master.sisRequest.ProposerAge;
                /*if([master.sisRequest.BasePlan containsString:@"VCPFPPV1N"] ||
                   [master.sisRequest.BasePlan containsString:@"VCPSPV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPSPPV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPFPV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPSDV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPSDPV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPFDV1N"] ||
				   [master.sisRequest.BasePlan containsString:@"VCPFDPV1N"]){
                    if([requestBean.BasePlan containsString:@"VCPSP"] || [requestBean.BasePlan containsString:@"VCPFP"])
                    replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:0];
                    else
                    replaceMentStr =[self getTableAGE:1 andWithEndPt:count andWithAge:age];
                }
                else*/
                replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                /* NSString *replaceMentStr =[self getTablePremium:1 andWithEndPt:count];
                 [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];*/
            }

			
            if ([patterenStr hasPrefix:RPANNUAL]) {
                NSString *replaceMentStr = @"";
                /*if(master.RenewalPrem_A == 0){
                    replaceMentStr = @"0";
                }
                else{*/
                    replaceMentStr = [NSString stringWithFormat:@"%ld",master.RenewalPrem_A];
                //}
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //Added by Amruta on 1/9/15 for MBP

            if ([patterenStr hasPrefix:RENEWALMODPREM]) {

                NSString *replaceMentStr = @"";
                replaceMentStr = [NSString stringWithFormat:@"%ld",master.RenewalModalPremium];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:RPSEMIANNUAL]){

                NSString *replaceMentStr = @"";
                if(master.RenewalPrem_SA == 0){
                    replaceMentStr = @"0";
                }else{
                    replaceMentStr = [NSString stringWithFormat:@"%ld",master.RenewalPrem_SA];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPQUARTERLY]){

                NSString *replaceMentStr = @"";
                /*if(master.RenewalPrem_Q == 0){
                    replaceMentStr = @"0";
                }else{*/
                    replaceMentStr = [NSString stringWithFormat:@"%ld",master.RenewalPrem_Q];

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //30th may
            //MRS
            if ([patterenStr hasPrefix:@"ANNUAL_QUOTEDMODPREM"]) {
                NSString *replaceMentStr = [NSString stringWithFormat:@"%ld",master.QuotedAnnualizedPremium];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"ANNUAL_TOTALRIDPREM"]) {
                long ridPrem =  master.TotalRiderModalPremium; //0;
                if ([master.sisRequest.Frequency isEqualToString:@"S"]) {
                    ridPrem = master.TotalRiderModalPremium * 2;
                }else if ([master.sisRequest.Frequency isEqualToString:@"Q"]) {
                    ridPrem = master.TotalRiderModalPremium * 4;
                }else if ([master.sisRequest.Frequency isEqualToString:@"M"]) {
                    ridPrem = master.TotalRiderModalPremium * 12;
                }
                NSString *replaceMentStr = [NSString stringWithFormat:@"%ld",ridPrem];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"TableMRSPA"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                NSString *replaceMentStr = [self getTablePA:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"TableMRSAGE"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getTableAGE:1 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"TableMRSPEM"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getTablePremium:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"TableMRSRIDPEM"]) {
                int pt = (int)master.sisRequest.PolicyTerm;
                int ppt = (int)master.sisRequest.PremiumPayingTerm;
                //changes 19th jan 2017
                //count = pt < 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getTableRiderPPTMRS:1 andWithEndPt:pt andWithShowPt:ppt];
                //30th May 2016 - Drastty
                if(replaceMentStr !=NULL)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];
            }
            if ([patterenStr hasPrefix:@"TableMRSTOTPEM"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"TableMRSCOV"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"TableMRSSURRBEN"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt < 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"Table2MRSPA"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                NSString *replaceMentStr = [self getTablePA:16 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"Table2MRSAGE"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                int age = master.sisRequest.InsuredAge + 15;
                NSString *replaceMentStr = [self getTableAGE:16 andWithEndPt:count andWithAge:age];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"Table2MRSPEM"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getTablePremium:16 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"Table2MRSRIDPEM"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                int ppt = (int)master.sisRequest.PremiumPayingTerm;
                count = pt > 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getTableRiderPPTMRS:16 andWithEndPt:count andWithShowPt:ppt];
               //30th May 2016 - Drastty
                if(replaceMentStr !=NULL)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }
            if ([patterenStr hasPrefix:@"Table2MRSTOTPEM"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalPremiumAnnual withStartPt:16 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"Table2MRSCOV"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverage withStartPt:16 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:@"Table2MRSSURRBEN"]) {
                int count = 0;
                int pt = (int)master.sisRequest.PolicyTerm;
                count = pt > 15 ? pt : 15;
                //int age = master.sisRequest.InsuredAge;
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteedSurrBenefit withStartPt:16 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //30th may
            //MRS

            if ([patterenStr hasPrefix:RPMONTHLY]){
                 NSString *replaceMentStr = @"";
                //Commented by Amruta
                /*if(master.RenewalPrem_M == 0){
                    replaceMentStr = @"0";
                }else{*/
                    replaceMentStr = [NSString stringWithFormat:@"%ld",master.RenewalPrem_M];
                //}
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPYEAR]){

                NSString *replaceMentStr = [NSString stringWithFormat:@"%@%@",requestBean.RPU_Year,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePREMPAIDDURINGYR]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getTablePremiumPaidDuringYear:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePREMPAIDCUMULATIVE]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getTablePremiumPaidCummilative:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCELIFECOV]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableCoverageAltSce:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.LifeCoverageAltSCE withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCEMATVAL]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableAltSceMaturityvalue:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioMaturityVal withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_Table4TOTALDEATHBENEFIT]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableAltSce4TotalDeathBenfit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioTotalDeathBenefit4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }


            if ([patterenStr hasPrefix:ALTSCE_Table8TOTALDEATHBENEFIT]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableAltSce8TotalDeathBenfit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioTotalDeathBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_MATVAL4]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableAltSceMaturityvalue4:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioMaturityValue4 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_MATVAL8]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableAltSceMaturityvalue8:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioMaturityValue8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_PREMPAIDCUMULATIVE]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getAltScePremiumPaidCummilative:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_PREMPAIDDURINGYR]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                NSString *replaceMentStr =[self getAltScePremiumPaidDuringYear:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_TOTMATBEN_CURRATE]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceTotalMaturityBenefitCurrentRate:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioToMatBenCurrRate withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_SURRBEN_CURRATE]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceSurrenderBenefitCurrentRate:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioSurrBenCurrRate withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_CURRATEPREMPAIDCUMULATIVE_CURRATE]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableTotalPremiumPaidCummilativeCurrentRate:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioPremPaidCumulativeCurrRate withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableFPRPFVEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.FpSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableFPRegularFundvalue];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValueFP];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableFPLOYALTY]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.FpSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableFPSurrenderBenefit];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.SurrenderValueFP];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableFPDBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.FpSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableFPDeathBenefit];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.DeathBenefitAtEndOfPolicyYearFP];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table_ALTSCE_TAXSAVING]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceTableTaxSaving:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioTaxSaving withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_ANN_EFE_PREM]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceTableAnnualEffectivePremium:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioAnnualizedEffectivePremium withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_CUMMULATIVE_EFE_PREM]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceTableCummulativeEffectivePremium:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.ALTScenarioCumulativeEffectivePremium withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_TOT4MATBEN_TAX]){

                int pt =(int) master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSce4MaturityBenfitTax:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.AltScenario4MaturityBenefitTax withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:EXPBONUSRATE]){

                NSString *replaceMentStr =master.sisRequest.Exp_Bonus_Rate;
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TAXSLAB]){

                double value = [master.sisRequest.TaxSlab doubleValue] * 100 ;
                NSString *replaceMentStr =[NSString stringWithFormat:@"%f",value];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTopUPPREM]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableTopUpPrem];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.TopUpPremium];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TablePartialW]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTablePartialW];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.WithDrawalAmount];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableRPFVTUFVPFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableRPFVTUFVPFMC];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.RegularFundValuePostFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTopUpLoyalty]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableTOPUPLoyalty];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.TopUpLoyalty];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableROFVTUFVEOY]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableROFVTUFVEOY];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.TopUpRegularFundValueEndOfYear];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTopUpSURRENDERVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableTopUpSURRENDERVAL];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.TopUpSurreValue];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableTopUpDBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.TopUpWDSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableTopUpDBEP];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.TopUpDeathBenefit];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:M_AAAMORTCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getM_AAAMortalityCharges];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.MortalityCharges_AS3];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:O_AAATOTC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getT_AAATotalCharges];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:AAA_APPSERTAX]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getAAA_ApplicableServiceTax];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:R_AAARFVBFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                   // replaceMentStr =[self getR_AAARegularFundValueBeforeFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:P_AAARFVPFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getP_AAARegularFundValuePostFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:L_AAALOYALTYCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getL_AAALoyaltyCharges];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_AAATotalFundValue];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_LCEF_AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_LCEF_AAATotalFundValue];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_WLEF_AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_WLEF_AAATotalFundValue];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:M_PH6AAAMORTCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getM_AAAMortalityChargesPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:O_PH6AAATOTC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getT_AAATotalChargesPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:AAA_PH6APPSERTAX]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getAAA_ApplicableServiceTaxPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:R_PH6AAARFVBFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getR_AAARegularFundValueBeforeFMCPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:F_PH6AAAFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getF_AAAFundManagamentChargesPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:P_PH6AAARFVPFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getP_AAARegularFundValuePostFMCPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:L_PH6AAALOYALTYCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getL_AAALoyaltyChargesPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_PH6AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_AAATotalFundValuePH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_LCEF_PH6AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_LCEF_AAATotalFundValuePH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TF_WLEF_PH6AAATOTFUNDVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTF_WLEF_AAATotalFundValuePH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:S_PH6AAASURRENDERVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getS_AAASurrenderValuePH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:D_PH6AAADBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.AAASelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getD_AAADeathBenefitAtEndOfPolYearPH6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART8RPFVE]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART8RPFVE];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART8SURRENDERVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART8SURRENDERVAL];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART8DBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART8DBEP];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART4RPFVE]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART4RPFVE];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART4SURRENDERVAL]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART4SURRENDERVAL];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART4DBEP]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART4DBEP];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10MORTCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10MORTCHRG];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.MortalityCharges_SMART10];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10TOTC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10TOTC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10APPSERTAX]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10APPSERTAX];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10RFVBFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10RFVBFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10FMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10FMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10RFVPFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10RFVPFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART10LOYALTYCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART10LOYALTYCHRG];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6MORTCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6MORTCHRG];
                    replaceMentStr = [self getHTMLDataForParticularTag:master.MortalityCharges_SMART6];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6TOTC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6TOTC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6APPSERTAX]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6APPSERTAX];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6RFVBFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6RFVBFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6FMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6FMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMART6RFVPFMC]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMART6RFVPFMC];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TableSMARTLOYALTYCHRG]){

                NSString *replaceMentStr = @"";
                if ([master.sisRequest.SmartSelected isEqualToString:@"YES"]) {
                    //replaceMentStr =[self getTableSMARTLOYALTYCHRG];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:BASEPLANTOTSERVTAX]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.serviceTaxBasePaln,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTMODALPREMPAYABLE]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalModalPremiumPayble,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPSTANNUAL]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.RenewalPremiumST_A,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPSTSEMIANNUAL]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.RenewalPremiumST_SA,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPSTQUARTERLY]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.RenewalPremiumST_Q,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RPSTMONTHLY]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.RenewalPremiumST_M,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTRPSEMIANNUAL]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalRenewalPremium_SA,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTRPQUARTERLY]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalRenewalPremium_Q,@""];
                // NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalRenewalPremium_RS_Q,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTRPMONTHLY]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalRenewalPremium_M,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:TOTRPANNUAL]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%ld%@",master.TotalRenewalPremium_A,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table4SURRBEN]){

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getTable4SurrenderBenefit:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.NONGuaranteed4SurrBenefit withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ALTSCE_VBONUS]){

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getAltSceVariableBonus:1 andWithEndPt:count];
                NSString *replaceMentStr =[self getHTMLForParticularTag:master.VariableBonus withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:Table8MATVAL]||[patterenStr hasPrefix:MLG_Table8MATVAL]){

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;//for maha life gold plus...value from 30 to 36..
                if([master.sisRequest.BasePlan isEqualToString:@"MLGPV1N1"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NA"] ||
                   [master.sisRequest.BasePlan isEqualToString:@"MLS2NB"] || [master.sisRequest.BasePlan isEqualToString:@"MLGV2N1"]){
                    count = pt < 85 ? pt : 85;
                }
                //NSString *replaceMentStr =[self getTableMaturityBenefit8:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.TotalMaturityBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            //FREEDOM
            if ([patterenStr hasPrefix:@"TableFR8MATVAL"]) {
                int pt = (int) master.sisRequest.PolicyTerm;
                NSString *replaceMentStr = [self getTableMaturityBenefit8:1 andWithPolTerm:pt];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }
            //FREEDOM

            if ([patterenStr hasPrefix:SMART7_8SURRBEN]){

                int pt = (int)master.sisRequest.PolicyTerm;
                int count = pt < 30 ? pt : 30 ;
                //NSString *replaceMentStr =[self getSurrenderBenefit8:1 andWithEndPt:count];
                NSString *replaceMentStr = [self getHTMLForParticularTag:master.SurrenderBenefit8 withStartPt:1 andWithEndPt:count];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:INITDFF]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%f%@",round(master.InitDebtFundFactor),@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:INITEFF]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%f%@",round(master.InitEquityFundFactor),@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:HEADERTOPUP]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",master.AltHeader,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:SMDB1]){

                NSString *replaceMentStr =@"0";
                if ([master.sisRequest.SmartDebtFund isEqualToString:@"WLI"]) {
                    replaceMentStr = @"100";
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:SMDB2]){

                NSString *replaceMentStr =@"0";
                if ([master.sisRequest.SmartDebtFund isEqualToString:@"WLF"]) {
                    replaceMentStr = @"100";
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:ILLUSPAGE]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@%@",master.sisRequest.Illus_Page_No,@""];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            /*if ([patterenStr hasPrefix:RIDERTEXT]){

                NSString *replaceMentStr = @"";
                NSMutableArray *rArr = master.riderData;
                for (int cnt = 0; cnt < rArr.count; cnt++) {
                    Rider *riderObj = [rArr objectAtIndex:cnt];
                    if ([riderObj.RDL_CODE isEqualToString:@"WOPULV1"] || [riderObj.RDL_CODE isEqualToString:@"WOPPULV1"]) {
                        replaceMentStr =RIDERTEXT;
                    }
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }*/
            if ([patterenStr hasPrefix:ACTUALFP]){

                NSString *replaceMentStr = master.sisRequest.FundPerform != NULL && ![master.sisRequest.FundPerform isEqualToString:@""] ? [NSString stringWithFormat:@"%@%@",master.sisRequest.FundPerform,@""] : @"8";
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            //*****************************ULIP*****************************
            if ([patterenStr hasPrefix:@"COMISSIONTEXT"]) {
                [mutableString replaceCharactersInRange:[matchingResult range] withString:master.CommissionText];
            }

            if ([patterenStr hasPrefix:NETYIELD8P]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%0.02f%@",master.NetYield8P,@"%"];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:NETYIELD8TW]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%f%@",master.NetYield8TW,@"%"];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:NETYIELD8SMART]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%f%@",master.NetYield8SMart,@"%"];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            if ([patterenStr hasPrefix:TAG_TABLELOYALTYCHARGES_8]) {

                NSString *replaceMentStr =[self getTableLoyaltyCharges_8];
                if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }

            if ([patterenStr hasPrefix:TAG_TABLELOYALTYCHARGES_4]) {

                NSString *replaceMentStr =[self getTableLoyaltyCharges_4];
                if(replaceMentStr != nil)
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
                else
                    [mutableString replaceCharactersInRange:[matchingResult range] withString:@""];

            }

            if ([patterenStr hasPrefix:PNLANNUALPREM]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@",master.AnnualPremium];
                if (master.sisRequest.ProductType != NULL && [master.sisRequest.ProductType isEqualToString:@"U"]) {
                    if ([master.AnnualPremium isEqualToString:@"NA"]) {
                        replaceMentStr = master.AnnualPremium;
                    }else
                        replaceMentStr = [NSString stringWithFormat:@"%0.3lld",[master.AnnualPremium longLongValue]];
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PNLSEMIPREM]){

                NSString *basePlan = master.sisRequest.BasePlan;
                NSString *replaceMentStr =NULL;
                if ([@"IPR1ULN1" isEqualToString:basePlan]  || [@"IMX1ULN1" isEqualToString:basePlan]  || [@"WPR1ULN1" isEqualToString:basePlan] || [@"WMX1ULN1" isEqualToString:basePlan]) {
                    replaceMentStr = [NSString stringWithFormat:@"%@",master.AnnualPremium];
                }else{

                    replaceMentStr =[NSString stringWithFormat:@"%1.0f",round([master.AnnualPremium longLongValue]*0.5)] ;
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:PNLQUARTERPREM]){

                NSString *basePlan = master.sisRequest.BasePlan;
                NSString *replaceMentStr =NULL;
                if ([@"IPR1ULN1" isEqualToString:basePlan]  || [@"IMX1ULN1" isEqualToString:basePlan]  || [@"WPR1ULN1" isEqualToString:basePlan] || [@"WMX1ULN1" isEqualToString:basePlan]) {

                    replaceMentStr = [NSString stringWithFormat:@"%@",master.AnnualPremium];
                }else{

                    replaceMentStr =[NSString stringWithFormat:@"%1.0f",round([master.AnnualPremium longLongValue]*0.25)] ;
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:PNLMONTHLYRPREM]){

                NSString *basePlan = master.sisRequest.BasePlan;
                NSString *replaceMentStr =NULL;
                if ([@"IPR1ULN1" isEqualToString:basePlan]  || [@"IMX1ULN1" isEqualToString:basePlan]  || [@"WPR1ULN1" isEqualToString:basePlan] || [@"WMX1ULN1" isEqualToString:basePlan]) {

                    replaceMentStr = [NSString stringWithFormat:@"%@",master.AnnualPremium];
                }else{

                    replaceMentStr =[NSString stringWithFormat:@"%1.0f",round([master.AnnualPremium longLongValue]*0.0833)] ;
                }
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:FMC]) {

                //changes 30 july 2015..
                NSString *replaceMentStr =[NSString stringWithFormat:@"%1.03f",master.dfmc];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:@"ADDLRID"]) {
                NSString *replaceMentStr = [self getRiderDisclaimer];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];
            }

            //*****************************ULIP*****************************
            //COMMISSION added by Ganesh
            if ([patterenStr hasPrefix:COMMISSION]){

                NSString *replaceMentStr =[NSString stringWithFormat:@"%@",master.CommissionText];
                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }

            if ([patterenStr hasPrefix:RDLDYNTERM1]){

                NSString *replaceMentStr = @"";
                int term = 0;
                Rider *riderObj;
                NSMutableArray *rArr = master.riderData;
                if (rArr.count  > 0) {

                    riderObj = [rArr objectAtIndex:0];

                    if (riderObj.RDL_RTL_ID == 14) {
                        if (requestBean.PremiumPayingTerm >  ( 65 - requestBean.InsuredAge)) {

                            term =  65 - requestBean.InsuredAge ;

                        }else{

                            term =  (int)requestBean.PremiumPayingTerm ;

                        }

                    }else if(riderObj.RDL_RTL_ID == 15){

                        if (requestBean.PremiumPayingTerm >  ( 65 - requestBean.ProposerAge)) {

                            term =  (int)(65 - requestBean.ProposerAge) ;

                        }else{

                            term =  (int)requestBean.PremiumPayingTerm ;

                        }
                    }else{

                        term =  (int)requestBean.PremiumPayingTerm ;

                    }
                    replaceMentStr = [NSString stringWithFormat:@"%d%@",term,@""];

                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
            if ([patterenStr hasPrefix:RDLDYNTERM2]){

                NSString *replaceMentStr = @"";
                int term = 0;
                Rider *riderObj;
                NSMutableArray *rArr = master.riderData;
                if (rArr.count  > 1) {

                    riderObj = [rArr objectAtIndex:1];

                    if (riderObj.RDL_RTL_ID == 14) {
                        if (requestBean.PremiumPayingTerm >  ( 65 - requestBean.InsuredAge)) {

                            term =  65 - requestBean.InsuredAge ;

                        }else{

                            term =  (int)requestBean.PremiumPayingTerm ;

                        }

                    }else if(riderObj.RDL_RTL_ID == 15){

                        if (requestBean.PremiumPayingTerm >  ( 65 - requestBean.ProposerAge)) {

                            term =  (int)(65 - requestBean.ProposerAge) ;

                        }else{

                            term =  (int)requestBean.PremiumPayingTerm ;

                        }
                    }else{

                        term =  (int)requestBean.PremiumPayingTerm ;

                    }
                    replaceMentStr = [NSString stringWithFormat:@"%d%@",term,@""];

                }

                [mutableString replaceCharactersInRange:[matchingResult range] withString:replaceMentStr];

            }
        }
    }

    //NSLog(@"the string is %@",mutableString);
	[mutableString replaceOccurrencesOfString:@"@@AGENTNAME@@" withString:@"||AGENTNAME||" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
	[mutableString replaceOccurrencesOfString:@"@@AGENTNUMBER@@" withString:@"||AGENTNUMBER||" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
	
    return mutableString;

}

#pragma mark -- PRIVATE METHODS
-(NSString *)getRiderDisclaimer{
    NSMutableArray *arr = master.riderData;
    AddressBO *addr = master.addressBOObj;

    NSString *riderDisc = @"";
    NSMutableString *versionStr = [[NSMutableString alloc] init];
    //NSString *imagePath = @"";

    if ((master.sisRequest.AgentCITI != NULL && [master.sisRequest.AgentCITI isEqualToString:@"Y"] ) ||
        [master.sisRequest.BasePlan containsString:@"IONEV1N"] ||
        [master.sisRequest.BasePlan containsString:@"FGV1N1"]) {
        if (master.sisRequest.AgentCITI != NULL && [master.sisRequest.AgentCITI isEqualToString:@"Y"]) {
            [versionStr appendString:@"CITI_"];
        }else
            [versionStr appendString:@""];
    }

    [versionStr appendString:addr.SIS_VERSION];

    NSMutableString *discStr = NULL;
    for (int i=0; i < arr.count; i++) {
        Rider *rider = [arr objectAtIndex:i];
        NSString *inputFile = @"";

        if ([master.sisRequest.BasePlan containsString:@"IONEV1N"]) {
            inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_IO",rider.RDL_CODE] ofType:@"html" inDirectory:@"www/templates/SIS"];
//            NSData *myData =[NSData dataWithContentsOfFile:filePath];
//            NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];
//            inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_IO",rider.RDL_CODE] ofType:@"html"];
        }else if([master.sisRequest.BasePlan containsString:@"FGV1N1"] && [rider.RDL_CODE isEqualToString:@"ADDLN1V1"]){
        inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_FG",rider.RDL_CODE] ofType:@"html" inDirectory:@"www/templates/SIS"];
//            inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_FG",rider.RDL_CODE] ofType:@"html"];
        }else{
            inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",rider.RDL_CODE] ofType:@"html" inDirectory:@"www/templates/SIS"];
//            inputFile = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",rider.RDL_CODE] ofType:@"html"];
        }

        NSData *myData =[NSData dataWithContentsOfFile:inputFile];
        NSString *inputString =[[NSString alloc]initWithData:myData encoding:NSASCIIStringEncoding];

        discStr = [[NSMutableString alloc] initWithString:inputString];

    }

    NSString *imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Header_%@",master.planDescription] ofType:JPG inDirectory:@"www/templates/SIS"];
    NSLog(@"IMAGENAME:,%@",imagePath);
    NSString *repalceStr = @"";

    if(imagePath != nil){
        NSData *data = [NSData dataWithContentsOfFile:imagePath];
        NSString *str = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        repalceStr = [NSString stringWithFormat:@"'data:image/jpg;base64,%@'",str];
    }

    if (discStr != nil) {
        riderDisc = [discStr stringByReplacingOccurrencesOfString:@"||IMGNAME||" withString:repalceStr];
        riderDisc =  [riderDisc stringByReplacingOccurrencesOfString:@"||VERSION||" withString:versionStr];
    }else{
        riderDisc = @"";
    }

    return riderDisc;
}


-(NSString *) getHTMLForParticularTag:(NSDictionary *)tagDict withStartPt:(int)startPt andWithEndPt:(int)endPt{
    NSDictionary *ValueDict = tagDict;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];

    if (ValueDict != nil) {
        if (startPt <= endPt) {
            for (int i = startPt; i<=endPt; i++) {
                [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
                if([ValueDict objectForKey:[NSNumber numberWithInt:i]] == NULL)
                    [mutableStr appendString:@"0"];
                else
                    [mutableStr appendString:[[ValueDict objectForKey:[NSNumber numberWithInt:i]]stringValue]];
                if(i==endPt)
                    [mutableStr appendString:@"</div>"];
                else
                    [mutableStr appendString:@"<br/></div>"];
            }
        }else {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<br/></div>"];
        }
    }

    ValueDict = NULL;
    return (NSString *)mutableStr;
}

//GIP***
-(NSString *) getHTMLForParticularTagGIP:(NSDictionary *)tagDict withStartPt:(int)startPt andWithEndPt:(int)endPt andWithIncTerm:(int)incomeTerm{
    NSDictionary *ValueDict = tagDict;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];

    if (ValueDict != nil) {
        if (startPt <= endPt) {
            for (int i = startPt; i<=endPt+incomeTerm; i++) {
                [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
                if(i>endPt)
                    [mutableStr appendString:@"0"];
                else
                    [mutableStr appendString:[[ValueDict objectForKey:[NSNumber numberWithInt:i]]stringValue]];
                if(i==endPt)
                    [mutableStr appendString:@"</div>"];
                else
                    [mutableStr appendString:@"<br/></div>"];
            }
        }else {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<br/></div>"];
        }
    }

    ValueDict = NULL;
    return (NSString *)mutableStr;
}

//good kid
-(NSString *) getSA4GK:(NSDictionary *)tagDict withStartPt:(int)startPt andWithEndPt:(int)endPt{
    NSMutableString *mutableStr = [[NSMutableString alloc]init];

    if (startPt <= endPt) {
        for (int i = startPt; i<=endPt; i++) {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:[NSString stringWithFormat:@"%lld",master.sisRequest.Sumassured]];
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];
        }
    }else {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];
    }

    return (NSString *)mutableStr;
}

-(NSString *)getHTMLRiderTagContent:(NSArray *)_arr withCheckValue:(NSString *)_check{
    NSArray *riderArr = _arr;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    Rider *riderObj = NULL;
    for (int i = 0; i< riderArr.count ; i++) {
        riderObj = [riderArr objectAtIndex:i];
        [mutableStr appendString:@"<div style=\"height:40px\">"];
        if ([_check isEqualToString:RIDER_P]) {
            [mutableStr appendString:[NSString stringWithFormat:@"%ld",riderObj.Premium]];
        }else if ([_check isEqualToString:RIDER_PREMIUMPAYINGTERM]) {
            [mutableStr appendString:[NSString stringWithFormat:@"%d",riderObj.PremPayTerm]];
        }else if ([_check isEqualToString:RIDER_POLICYTERM]) {
            [mutableStr appendString:[NSString stringWithFormat:@"%d",riderObj.PolTerm]];
        }else if ([_check isEqualToString:RIDER_MODALPREMIUM]) {
            [mutableStr appendString:[NSString stringWithFormat:@"%ld",riderObj.ModalPremium]];
        }else if ([_check isEqualToString:RIDER_SUMASSURED]) {
            if (riderObj.SumAssured == 0) {
                [mutableStr appendString:@"**"];
            }else{
                [mutableStr appendString:[NSString stringWithFormat:@"%ld",riderObj.SumAssured]];
            }
        }else if ([_check isEqualToString:RIDER_DESCRIPTION]) {
            [mutableStr appendString:[NSString stringWithFormat:@"%@",[riderObj.RDL_DESCRIPTION stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]]];
        }else if([_check isEqualToString:RIDER_UIN]){
            if (riderObj.RDL_UIN != NULL && riderObj.RDL_UIN.length != 0) {
                [mutableStr appendString:riderObj.RDL_UIN];
            }else
                [mutableStr appendString:@""];
        }
        if(i==riderArr.count-1)
            [mutableStr appendString:@"</div>"];
        else
            [mutableStr appendString:@"<br/></div>"];
    }
   // [mutableStr appendString:@"</table>"];
    riderArr = NULL;

    return (NSString *)mutableStr;
}


-(NSString *)getAltScePremiumPaidCummilative:(int)startPt andWithEndPt:(int)endPt{

    long premPaidCummilativeTotal = 0;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    int pt =(int) master.sisRequest.PolicyTerm;
    long qap = master.QuotedAnnualizedPremium;
    NSMutableDictionary *premPaidCummilativeDic =[[NSMutableDictionary alloc]init];
    for (int i = 0 ; i <= pt ; i++) {
        if (ppt >= i) {
            premPaidCummilativeTotal = premPaidCummilativeTotal + qap ;
            [premPaidCummilativeDic setObject:[NSNumber numberWithLong:premPaidCummilativeTotal] forKey:[NSNumber numberWithInt:i]];
        }else{
            [premPaidCummilativeDic setObject:[NSNumber numberWithLong:premPaidCummilativeTotal] forKey:[NSNumber numberWithInt:i]];
        }
    }
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<= endPt ; i++) {
            //[mutableStr appendString:@"<tr><td style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:[[premPaidCummilativeDic allKeys] objectAtIndex:i]];
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];
        }
    }else {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }

   // [mutableStr appendString:@"</table>"];
    premPaidCummilativeDic = NULL;

    return (NSString *)mutableStr;
}

-(NSString *)getAltScePremiumPaidDuringYear:(int)startPt andWithEndPt:(int)endPt{

    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    long prem = master.QuotedAnnualizedPremium;
    NSString *planTypeStr =[DataAccessClass getPlanType:master.sisRequest.BasePlan];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<=endPt; i++) {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (ppt >= i) {

                if ([@"C" isEqualToString:planTypeStr] && ([@"Y" isEqualToString:master.sisRequest.TataEmployee])) {

                    [mutableStr appendString:[NSString stringWithFormat:@"%ld",master.DiscountedBasePremium]];

                }else{
                    [mutableStr appendString:[NSString stringWithFormat:@"%ld",prem]];

                }

            }else{
                [mutableStr appendString:@"0"];

            }
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];

        }
    }else {

        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }
   // [mutableStr appendString:@"</table>"];


    return (NSString *)mutableStr;

}

-(NSString *)getTablePremiumPaidCummilative:(int)startPt andWithEndPt:(int)endPt{

    long premPaidCummilativeTotal = 0;
    int rpuy = [master.sisRequest.RPU_Year intValue];
    int pt =(int) master.sisRequest.PolicyTerm;
    long qap = master.QuotedAnnualizedPremium;
    NSMutableDictionary *premPaidCummilativeDic =[[NSMutableDictionary alloc]init];
    for (int i = 0 ; i <= pt ; i++) {
        if (rpuy > i) {
            premPaidCummilativeTotal = premPaidCummilativeTotal + qap ;
            [premPaidCummilativeDic setObject:[NSNumber numberWithLong:premPaidCummilativeTotal] forKey:[NSNumber numberWithInt:i]];
        }else{
            [premPaidCummilativeDic setObject:[NSNumber numberWithLong:premPaidCummilativeTotal] forKey:[NSNumber numberWithInt:i]];
        }
    }
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<= endPt ; i++) {
            //[mutableStr appendString:@"<tr><td style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:[[premPaidCummilativeDic allKeys] objectAtIndex:i]];
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];
        }
    }else {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }

    //[mutableStr appendString:@"</table>"];
    premPaidCummilativeDic = NULL;

    return (NSString *)mutableStr;
}

-(NSString *)getTablePremiumPaidDuringYear:(int)startPt andWithEndPt:(int)endPt{

    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    int rpuy = [master.sisRequest.RPU_Year intValue];
    long prem = master.QuotedAnnualizedPremium;
    NSString *planTypeStr =[DataAccessClass getPlanType:master.sisRequest.BasePlan];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
        if (startPt <= endPt) {
            for (int i = startPt; i<=endPt; i++) {
                [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
                if (rpuy > i) {

                    if ([@"C" isEqualToString:planTypeStr] && ([@"Y" isEqualToString:master.sisRequest.TataEmployee])) {

                        [mutableStr appendString:[NSString stringWithFormat:@"%ld",master.DiscountedBasePremium]];

                    }else{
                        [mutableStr appendString:[NSString stringWithFormat:@"%ld",prem]];

                    }

                }else{
                    [mutableStr appendString:@"0"];

                }
                if(i==endPt)
                    [mutableStr appendString:@"</div>"];
                else
                    [mutableStr appendString:@"<br/></div>"];

            }
        }else {

            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<br/></div>"];

        }
        //[mutableStr appendString:@"</table>"];


    return (NSString *)mutableStr;

}

-(NSString *)getTableRiderOnPPTSmart7:(int)startPt andWithEndPt:(int)endPt andWithShowPt:(int)showPt{

    int pt =(int) master.sisRequest.PolicyTerm;
    int ppt =(int) master.sisRequest.PremiumPayingTerm;

    NSMutableDictionary *totalRiderPrem = master.TotalRiderAnnualPremium;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
   // [mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
   //changes 19th jan 2017
    for (int i = 1; i<= endPt ; i++) {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        if (i <= ppt) {
            //[mutableStr appendString:[[totalRiderPrem allKeys] objectAtIndex:i]];
            [mutableStr appendString:[[totalRiderPrem objectForKey:[NSNumber numberWithInt:i]] stringValue]];
        }else{
            [mutableStr appendString:@"0"];
        }
        if(i==endPt)
            [mutableStr appendString:@"</div>"];
        else
            [mutableStr appendString:@"<br/></div>"];
    }
    //[mutableStr appendString:@"</table>"];
    totalRiderPrem = NULL;

    return (NSString *)mutableStr;
}

-(NSString *)getTableRider:(int)startPt andWithEndPt:(int)endPt{

    long totalRiderPrem = master.TotalRiderModalPremium;
    //Added by Amruta for ADDL Rider
    int ppt =(int) master.sisRequest.PremiumPayingTerm;

    //changes 3rd march..
    if (master.riderData.count != 0) {
        for(Rider *rid in master.riderData){
            if ([rid.RDL_CODE isEqualToString:@"ADDLN1V1"]) {
                ppt = rid.PremPayTerm;
            }
        }
    }

    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<=endPt; i++) {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            //Added by Amruta for ADDL Rider
            if(i<=ppt){
                long riderprem = 0;
                if ([master.sisRequest.Frequency isEqualToString:@"M"]) {
                    riderprem = totalRiderPrem * 12;
                }else if ([master.sisRequest.Frequency isEqualToString:@"Q"]) {
                    riderprem = totalRiderPrem * 4;
                }else if ([master.sisRequest.Frequency isEqualToString:@"S"]) {
                    riderprem = totalRiderPrem * 2;
                }else{
                    riderprem = totalRiderPrem;
                }

                [mutableStr appendString:[NSString stringWithFormat:@"%ld",riderprem]];
            }
            else
                [mutableStr appendString:@"0"];

            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];

        }
    }else {

        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }
    //  [mutableStr appendString:@"</table>"];

    return (NSString *)mutableStr;
}

-(NSString *)getTablePremium:(int)startPt andWithEndPt:(int)endPt{

    NSMutableString *mutableStr = [[NSMutableString alloc]init];

    NSString *mode = master.sisRequest.Frequency;
    int ppt =(int) master.sisRequest.PremiumPayingTerm;
    long prem = 0;
    if (master.sisRequest.ProductType != NULL && [master.sisRequest.ProductType isEqualToString:@"U"]) {
        if (![master.AnnualPremium isEqualToString:@"NA"]) {
            prem = [master.AnnualPremium longLongValue];
        }
        if ([master.sisRequest.Frequency isEqualToString:@"O"]) {
            prem = master.BaseAnnualizedPremium;
        }
        NSLog(@"the value for prem is %ld",prem);
    }else
        prem = master.QuotedAnnualizedPremium;

        NSString *planTypeStr =[DataAccessClass getPlanType:master.sisRequest.BasePlan];
        if (startPt <= endPt) {
            for (int i = startPt; i<=endPt; i++) {
                [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
                if (([@"O" isEqualToString:mode] && i > 1) || (i > ppt)) {
                    [mutableStr appendString:@"0"];
                }else{

                    if ([@"C" isEqualToString:planTypeStr] && ([@"Y" isEqualToString:master.sisRequest.TataEmployee]) && (i == 1)) {

                        [mutableStr appendString:[NSString stringWithFormat:@"%ld",master.DiscountedBasePremium]];

                    }else{
                        [mutableStr appendString:[NSString stringWithFormat:@"%ld",prem]];

                    }

                }
                if(i==endPt)
                    [mutableStr appendString:@"</div>"];
                else
                    [mutableStr appendString:@"<br/></div>"];

            }
        }else {

            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:@"<br/></div>"];
        }
        //[mutableStr appendString:@"</table>"];


    return (NSString *)mutableStr;

}

-(NSString *)getTableAGE:(int)startPt andWithEndPt:(int)endPt andWithAge:(int)age{

    NSMutableString *mutableStr = [[NSMutableString alloc]init];
   // [mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<=endPt; i++) {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:[NSString stringWithFormat:@"%d",age]];
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];
            if(age > 0)
                age++;

        }
    }else {

        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }
    //[mutableStr appendString:@"</table>"];

    return (NSString *)mutableStr;

}

-(NSString *)getTablePA:(int)startPt andWithEndPt:(int)endPt{

    NSMutableString *mutableStr = [[NSMutableString alloc]init];
   // [mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (startPt <= endPt) {
        for (int i = startPt; i<=endPt; i++) {
            [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [mutableStr appendString:[NSString stringWithFormat:@"%d",i]];
            if(i==endPt)
                [mutableStr appendString:@"</div>"];
            else
                [mutableStr appendString:@"<br/></div>"];
        }
    }else {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<br/></div>"];

    }

   // [mutableStr appendString:@"</table>"];
    return (NSString *)mutableStr;
}



-(NSString *)getFormattedDate:(NSString *)strDate{

    NSDateFormatter *sourceDateFormat = [[NSDateFormatter alloc]init];
    [sourceDateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *date =[sourceDateFormat dateFromString:strDate];
    NSDateFormatter *destinationDateFormat = [[NSDateFormatter alloc]init];
    [destinationDateFormat setDateFormat:@"dd-MMM-yyyy"];
    strDate =[destinationDateFormat stringFromDate:date];
    return strDate;

}

-(NSString *)getUniquePDFFileName:(NSString *)productName{

    NSString *fileName = @"";
    long currentTimeMillS =[[NSDate date] timeIntervalSince1970];
    fileName =[NSString stringWithFormat:@"%ld",currentTimeMillS];
    fileName = [NSString stringWithFormat:@"%@ %@ %@ %ld %@",productName,fileName,@"_" ,[self countIncrementer],@".pdf"];

    return fileName;
}

-(long)countIncrementer{
    return ++count;
}

//******************ULIP******************
-(NSString *)getTablePremiumAllocationChargesPPT{

    int ppt =(int) master.sisRequest.PremiumPayingTerm;
    int pt =(int) master.sisRequest.PolicyTerm;
    NSMutableDictionary *allocationChargesPPTDict = master.PremiumAllocationCharges;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    for (int i = 1; i<= pt ; i++) {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        if (i <= ppt) {
            [mutableStr appendString:[[allocationChargesPPTDict objectForKey:[NSNumber numberWithInt:i]] stringValue]];
        }else{
            [mutableStr appendString:@"0"];
        }
        if(i==pt)
            [mutableStr appendString:@"</div>"];
        else
            [mutableStr appendString:@"<br/></div>"];
    }
    //[mutableStr appendString:@"</table>"];
    allocationChargesPPTDict = NULL;

    return (NSString *)mutableStr;
}

-(NSString *)getHTMLDataForParticularTag:(NSDictionary *)_tagDict{
    int pt =(int) master.sisRequest.PolicyTerm;
    NSDictionary *Dict = _tagDict;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    //[mutableStr appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    for (int i = 1; i<= pt ; i++) {
       // [mutableStr appendString:@"<tr><td style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [mutableStr appendString:[[Dict objectForKey:[NSNumber numberWithInt:i]]stringValue]];
        if(i==pt)
            [mutableStr appendString:@"</div>"];
        else
            [mutableStr appendString:@"<br/></div>"];
        //[mutableStr appendString:@"</td></tr>"];
    }
    //[mutableStr appendString:@"</table>"];
    Dict = NULL;

    return (NSString *)mutableStr;
}

-(NSString *)getTableLoyaltyCharges_8{
    //changes 6th oct
    int pt = (int)master.sisRequest.PolicyTerm;

    NSMutableDictionary *dict = master.LoyalityChargeForPh10;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    for (int i=1; i<=pt; i++) {
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:[[dict objectForKey:[NSNumber numberWithInt:i]] stringValue]];
        if(i==pt)
            [str appendString:@"</div>"];
        else
            [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    dict = NULL;
    return str;
}

-(NSString *)getTableLoyaltyCharges_4{
    int pt = (int)master.sisRequest.PolicyTerm;

    NSMutableDictionary *dict = master.LoyalityChargeForPh6;
    NSMutableString *str = [[NSMutableString alloc] init];
   // [str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    for (int i=1; i<=pt; i++) {
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:[[dict objectForKey:[NSNumber numberWithInt:i]] stringValue]];
        if(i==pt)
            [str appendString:@"</div>"];
        else
            [str appendString:@"<br/></div>"];
    }
   // [str appendString:@"</table>"];
    dict = NULL;
    return str;
}
//******************ULIP******************

-(NSString *)getAddress{

    AddressBO *addressBOObj = master.addressBOObj;
    NSMutableString *mutableStr = [[NSMutableString alloc]init];
    [mutableStr appendString:@"<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">"];
    [mutableStr appendString:@"<tr><td align=\"center\">"];
    [mutableStr appendString:addressBOObj.CO_NAME];
    [mutableStr appendString:@"</td></tr>"];

    [mutableStr appendString:@"<tr><td align=\"center\">"];
    [mutableStr appendString:addressBOObj.CO_ADDRESS];
    [mutableStr appendString:@"</td></tr>"];

    [mutableStr appendString:@"<tr><td align=\"center\">"];
    [mutableStr appendString:addressBOObj.CO_HELPLINE];
    [mutableStr appendString:@"</td></tr>"];

    [mutableStr appendString:@"<tr><td>"];
    [mutableStr appendString:addressBOObj.FILLER1];
    [mutableStr appendString:@"</td></tr>"];

    [mutableStr appendString:@"</table>"];
    addressBOObj = NULL;

    return (NSString *)mutableStr;

}

//********************************SGP*********************************
-(NSString *)getAccTableGai:(int)pol andWithCount:(int)count{
    NSMutableDictionary *GaiDict = master.AccGAI;
    NSMutableString *str = [[NSMutableString alloc] init];

    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];
    if (GaiDict != NULL && GaiDict != nil) {
        if(pol<= count){
            for (int i=pol; i<=count; i++) {
                [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
                [str appendString:[[GaiDict objectForKey:[NSNumber numberWithInt:i]] stringValue]];
                if(i==count)
                    [str appendString:@"</div>"];
                else
                    [str appendString:@"<br/></div>"];
            }
        }else{
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            [str appendString:@"<br/></div>"];
        }
    }

    GaiDict = NULL;
    return (NSString *)str;
}
//********************************SGP*********************************



//****************FREEDOM*********************
-(NSString *)getTable8VSRBonus:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *VSRBonusDict = master.VSRBonus8;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    VSRBonusDict = NULL;
    return (NSString *)str;
}

-(NSString *)getTable4VSRBonus:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *VSRBonusDict = master.VSRBonus4;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    VSRBonusDict = NULL;
    return (NSString *)str;
}

-(NSString *)getTable8TotalDeathBenefit:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TDB8 = master.TotalDeathBenefit8;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[TDB8 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    TDB8 = NULL;
    return (NSString *)str;
}

-(NSString *)getTable4TotalDeathBenefit:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TDB4 = master.TotalDeathBenefit4;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[TDB4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    TDB4 = NULL;
    return (NSString *)str;
}

-(NSString *)getTable8FRSurrenderBenefit:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *Surr8 = master.NONGuaranteedSurrBenefit_8;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[Surr8 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    Surr8 = NULL;
    return (NSString *)str;
}

-(NSString *)getTable4FRSurrenderBenefit:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *Surr4 = master.NONGuaranteedSurrBenefit;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[Surr4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    Surr4 = NULL;
    return (NSString *)str;
}

-(NSString *)getTableMaturityBenefit4:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *MatBen4 = master.TotalMaturityBenefit4;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[MatBen4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    MatBen4 = NULL;
    return (NSString *)str;
}

-(NSString *)getTableMaturityBenefit8:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *MatBen8 = master.TotalMaturityBenefit8;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[MatBen8 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    MatBen8 = NULL;
    return (NSString *)str;
}

-(NSString *)getTableTerminalBonus8:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TermBon8 = master.TerminalBonus8;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[TermBon8 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    TermBon8 = NULL;
    return (NSString *)str;
}

-(NSString *)getTableTerminalBonus4:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TermBon4 = master.TerminalBonus4;
    NSMutableString *str = [[NSMutableString alloc] init];
    //[str appendString:@"<table width=\"100%\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (master.isCombo) {
                //[str appendFormat:[[VSRBonusDict objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }else{
                [str appendString:[[TermBon4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    //[str appendString:@"</table>"];
    TermBon4 = NULL;
    return (NSString *)str;
}

//MIP
-(NSString *)getTableTotalPremium_MIP:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *dictTotalPremAnnual = master.TotalPremiumAnnual;
    long riderPremTotal = 0;
    int pt = (int)master.sisRequest.PolicyTerm;
    NSString *mode = master.sisRequest.Frequency;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    NSString *planType = [DataAccessClass getPlanType:master.sisRequest.BasePlan];
    NSMutableArray *array = master.riderData;

    for (int cnt = 0; cnt <= pt; cnt++) {
        for (int j=0; j<array.count; j++) {
            //Rider *riderObj = [array objectAtIndex:j];
            riderPremTotal = riderPremTotal + master.TotalAnnRiderModalPremium;
        }

        if (([mode isEqualToString:@"O"] && cnt > 1) || cnt > ppt) {
            [dictTotalPremAnnual setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:cnt]];
        }else{
            if (([@"C" isEqualToString:planType] && [@"Y" isEqualToString:master.sisRequest.TataEmployee]) && cnt == 1) {
                [dictTotalPremAnnual setObject:[NSNumber numberWithLong:master.DiscountedBasePremium + riderPremTotal] forKey:[NSNumber numberWithInt:cnt]];
            }else{
                [dictTotalPremAnnual setObject:[NSNumber numberWithLong:master.QuotedAnnualizedPremium + riderPremTotal] forKey:[NSNumber numberWithInt:cnt]];
            }
        }
        riderPremTotal = 0;
    }

    NSMutableString *str = [[NSMutableString alloc] init];
    if (strPt <= endPt) {
        for (int i= strPt; i<=endPt; i++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (dictTotalPremAnnual!= NULL) {
                if (master.isCombo) {

                }else{
                    [str appendString:[[dictTotalPremAnnual objectForKey:[NSNumber numberWithInt:i]] stringValue]];
                }
            }else
                [str appendString:@"O"];

            if (i == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    dictTotalPremAnnual = NULL;
    return (NSString *)str;
}

-(NSString *)getTableRider_MIP:(int)strPt andWithPolTerm:(int)endPt{
    long totalRiderPrem = master.TotalAnnRiderModalPremium;
    int ppt = (int)master.sisRequest.PremiumPayingTerm;
    NSMutableString *str = [[NSMutableString alloc] init];
    if (strPt <= endPt) {
        for (int i = strPt; i<= endPt; i++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if (i<=ppt) {
                [str appendString:[NSString stringWithFormat:@"%ld",totalRiderPrem]];
            }else{
                [str appendString:@"0"];
            }
            if (i == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    return (NSString *)str;
}

-(NSString *)getTableBasicSumMIP:(int)strPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *matVal = master.BasicSumAssured;
    NSMutableString *str = [[NSMutableString alloc] init];

    if (strPt <= endPt) {
        for (int cnt = strPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            /*if (master.isCombo) {
            }else{
                [str appendString:[[matVal objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            }*/
            if (cnt == endPt) {
                if ([[matVal objectForKey:[NSNumber numberWithInt:cnt]] stringValue].length != 0
                    && [[matVal objectForKey:[NSNumber numberWithInt:cnt]] stringValue] != NULL) {
                    [str appendString:[[matVal objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
                }
                [str appendString:@"</div>"];
            }else{
                [str appendString:@"0"];
                [str appendString:@"<br/></div>"];
            }
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    matVal = NULL;
    return (NSString *)str;
}

//SIP
-(NSString *)getTableTotGuarBenefit:(int)startPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TermBon4 = master.totalGuarBenefit;
    NSMutableString *str = [[NSMutableString alloc] init];

    if (startPt <= endPt) {
        for (int cnt = startPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];

            [str appendString:[[TermBon4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];

            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    TermBon4 = NULL;
    return (NSString *)str;

}

-(NSString *)getTableSurBenefit:(int) startPt andWithPolTerm:(int)endPt{
    NSMutableDictionary *TermBon4 = master.NONGuaranteedSurrBenefit;
    NSMutableString *str = [[NSMutableString alloc] init];

    if (startPt <= endPt) {
        for (int cnt = startPt; cnt<=endPt; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];

            [str appendString:[[TermBon4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];

            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    TermBon4 = NULL;
    return (NSString *)str;

}

//Added for GIP
-(NSString *)getTableSurBenefitGIP:(int) startPt andWithPolTerm:(int)endPt andWithIncTerm:(int)incomeTerm{
    NSMutableDictionary *TermBon4 = master.NONGuaranteedSurrBenefit;
    NSMutableString *str = [[NSMutableString alloc] init];

    if (startPt <= endPt) {
        for (int cnt = startPt; cnt<=endPt+incomeTerm; cnt++) {
            [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
            if(cnt > endPt)
                [str appendString:@"0"];
            else
                [str appendString:[[TermBon4 objectForKey:[NSNumber numberWithInt:cnt]] stringValue]];
            NSLog(@"Count is::: %d",cnt);
            if (cnt == endPt) {
                [str appendString:@"</div>"];
            }else
                [str appendString:@"<br/></div>"];
        }
    }else{
        [str appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        [str appendString:@"<br/></div>"];
    }
    TermBon4 = NULL;
    return (NSString *)str;

}

-(int) getTerm{
    int pt =(int) master.sisRequest.PolicyTerm;

    for(Rider *rid in master.riderData){
        if ([rid.RDL_CODE isEqualToString:@"ADDLN1V1"]) {
            pt = rid.PremPayTerm;
        }
    }
    int count = pt < 85 ? pt : 85 ;
    NSLog(@"inside get term ::::: %d",count);
    return count;
}

//for gender 6th may
-(NSString *)getFullGender:(NSString *)gender{
    //16th may orderedsame removed..
    if ([gender isEqualToString:@"M"]) {
        return @"Male";
    }else{
        return @"Female";
    }
}

//6th may date changes
-(NSString *)getDOB:(NSString *)dobDate{
    NSString *str = @"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	 
    //16th may formatter changes
    [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dobDate];
    dateFormat.dateFormat = @"dd-MMM-yyyy";
    str = [dateFormat stringFromDate:date];
    NSLog(@"DOB is %@",str);
    return  str;
}

//30th may
-(NSString *)getTableRiderPPTMRS:(int)startPt andWithEndPt:(int)endPt andWithShowPt:(int)showPt{


    //[[master.riderData]]
    //30th May 2016 - Drastty
    NSMutableDictionary *totalRiderPrem;
    NSMutableString *mutableStr;

    int ppt = 0;
    NSArray *arr = master.riderData;
    //changes 19th jan 2017
    if([arr count] != 0){
        Rider *rid =  [arr objectAtIndex:0];
        ppt = rid.PremPayTerm;
    }
    totalRiderPrem = master.TotalRiderAnnualPremium;
    mutableStr= [[NSMutableString alloc]init];
    
    for (int i = startPt; i<= endPt ; i++) {
        [mutableStr appendString:@"<div style=\"font-size:7.0pt; font-weight:400; font-style:normal; font-family:Tahoma,sans-serif;\">"];
        if (i <= ppt) {
            [mutableStr appendString:[[totalRiderPrem objectForKey:[NSNumber numberWithInt:i]] stringValue]];
        }else{
            [mutableStr appendString:@"0"];
        }
        if(i==endPt)
            [mutableStr appendString:@"</div>"];
        else
            [mutableStr appendString:@"<br/></div>"];
    }
    
    totalRiderPrem = NULL;

    return (NSString *)mutableStr;
}

@end
