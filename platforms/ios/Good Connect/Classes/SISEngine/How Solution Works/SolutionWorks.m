////
////  SolutionWorks.m
////  Life_Planner
////
////  Created by admin on 03/02/15.
////
////
//
//#import "SolutionWorks.h"
//
//@implementation SolutionWorks
//
//-(void)GetBasicDataForProductSolution{
//    NSString *solutionData = [command.arguments objectAtIndex:0];
//    NSData *data = [solutionData dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *err;
//    NSDictionary *_request = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
//    NSLog(@"the request is %@",[_request objectForKey:@"REQ"]);
//    
//    NSMutableDictionary *dict = [self getDataForProduct:[_request objectForKey:@"REQ"]];
//    
//    return dict;
//}
//
//-(NSMutableDictionary *)getDataForProduct:(NSDictionary *)request{
//    //for secure 7..
//    
//    NSMutableDictionary *SolutionData = [[NSMutableDictionary alloc] init];
//    ModelMaster *masterObj = [[ModelMaster alloc] init];
//    SISRequest *sisRequestObj = [self getRequest:request];
//    
//    masterObj.sisRequest = sisRequestObj;
//    
//    GenerateSIS *generateSISObj = [[GenerateSIS alloc] initWithMaster:masterObj];
//    NSMutableArray *pnlMinPremData =  generateSISObj.MapperObj.PNLMinPrem;
//    
//    if(sisRequestObj.BasePlan.length != 0 && sisRequestObj.Sumassured != 0 && sisRequestObj.PolicyTerm != 0 && sisRequestObj.PremiumPayingTerm != 0){
//        @try {
//            if([sisRequestObj.BasePlan containsString:SECURE7]){
//                long annualPremium = [generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                if([self checkPremium:masterObj.sisRequest.BasePlan andWithFrequency:@"A" andWithPremium:annualPremium andWithArray:pnlMinPremData]){
//                    masterObj.BaseAnnualizedPremium = annualPremium;
//                    //changes 01/06/2015
//                    long TotalGAI = [generateSISObj getTotalGaiSum];
//                    //changes 09/06/2015
//                    //long TotalMaturityValue = [generateSISObj getTotalMaturityValueForSolution];
//                    long TotalMaturityValue = (25 * sisRequestObj.Sumassured)/100 + TotalGAI;
//                    [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                    [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                    [SolutionData setObject:[NSNumber numberWithLong:TotalGAI] forKey:RESPONSE_TOTALGAI];
//                    [SolutionData setObject:[NSNumber numberWithLong:(25 * sisRequestObj.Sumassured)/100] forKey:RESPONSE_SUMASSURED];
//                    [SolutionData setObject:[NSNumber numberWithLong:TotalMaturityValue] forKey:RESPONSE_TOTBENEFIT];
//                    NSLog(@"Native Return: %@",SolutionData);
//                    return SolutionData;
//                }else{
//                    [SolutionData setObject:[NSNumber numberWithInt:0] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:[NSString stringWithFormat:@"Premium is less"] forKey:RESPONSE_MESSAGE];
//                    NSLog(@"Native Return: %@",SolutionData);
//                    return SolutionData;
//                }
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MAHALIFEMAGIC]){
//                long annualPremium =[generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                NSLog(@"annunalPremium==%ld",annualPremium);
//                long bonusVsr = [generateSISObj getVSRBonusForSolution] ;
//                long terminalBonus =[generateSISObj getTerminalBonusForSolution];
//                long totMaturityBenifit = [generateSISObj getTotMaturityBenefitForSolution];
//                [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                [SolutionData setObject:[NSNumber numberWithLong:bonusVsr] forKey:RESPONSE_TOTVSRBONUS];
//                [SolutionData setObject:[NSNumber numberWithLong:terminalBonus] forKey:RESPONSE_TOTTERMINALBONUS];
//                [SolutionData setObject:[NSNumber numberWithLong:totMaturityBenifit] forKey:RESPONSE_TOTBENEFIT];
//                NSLog(@"Native Return: %@",SolutionData);
//                return SolutionData;
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MAHALIFEGOLDPLUS]){
//                long annualPremium = [generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                long totalCashDividend = 0;
//                long totalBenefit = 0;
//                long totalAnnualCoupon = [generateSISObj getTotalGAI_MLGP];
//                totalCashDividend = [generateSISObj getTotalVSRBonus:masterObj.sisRequest];
//                //changes 03/06/2015
//                totalBenefit = masterObj.sisRequest.Sumassured + totalCashDividend + totalAnnualCoupon;
//                //changes 02/06/2015
//                double gaiFactor = [DataAccessClass getGAIFactorForHSW:masterObj.sisRequest];
//                
//                NSLog(@"annunalPremium==%ld",annualPremium);
//                [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                [SolutionData setObject:[NSNumber numberWithLong:totalCashDividend] forKey:RESPONSE_TOTCASHDIVIDEND];
//                [SolutionData setObject:[NSNumber numberWithLong:totalAnnualCoupon] forKey:RESPONSE_TOTANNCOUPON];
//                [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                [SolutionData setObject:[NSNumber numberWithLong:totalBenefit] forKey:RESPONSE_TOTBENEFIT];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                //changes 02/06/2015
//                [SolutionData setObject:[NSNumber numberWithDouble:gaiFactor] forKey:RESPONSE_GAIFACTOR];
//                NSLog(@"Native Return: %@",SolutionData);
//                return SolutionData;
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MONEYMAXIMA]){
//                long annualPremium =[generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                if([self checkPremium:masterObj.sisRequest.BasePlan andWithFrequency:@"A" andWithPremium:annualPremium andWithArray:pnlMinPremData]){
//                    NSLog(@"annunalPremium==%ld",annualPremium);
//                    masterObj.BaseAnnualizedPremium = annualPremium;
//                    //changes in 01/06/2015
//                    double d = ((masterObj.sisRequest.PolicyTerm / 2) / 100.0);
//                    long GuarLoyalityAddtn = (d * masterObj.sisRequest.Sumassured);
//                    long bonusVsr = [generateSISObj getVSRBonusForSolution] ;
//                    long terminalBonus =[generateSISObj getTerminalBonusForSolution];
//                    double totMaturityBenifit = 0;
//                    
//                    if(masterObj.sisRequest.percent == 4){
//                        //changes 6th oct
//                        totMaturityBenifit = [[[generateSISObj getTotalBenefit4MM] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] doubleValue];
//                    }else{
//                        //changes 6th oct
//                        totMaturityBenifit = [[[generateSISObj getTotalBenefit8MM] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] doubleValue];
//                    }
//                    
//                    [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                    [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                    [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                    [SolutionData setObject:[NSNumber numberWithLong:GuarLoyalityAddtn] forKey:RESPONSE_TOTGUARSURR];
//                    [SolutionData setObject:[NSNumber numberWithLong:bonusVsr] forKey:RESPONSE_TOTVSRBONUS];
//                    [SolutionData setObject:[NSNumber numberWithLong:terminalBonus] forKey:RESPONSE_TOTTERMINALBONUS];
//                    [SolutionData setObject:[NSNumber numberWithLong:totMaturityBenifit] forKey:RESPONSE_TOTBENEFIT];
//                    NSLog(@"Native Return: %@",SolutionData);
//                    return SolutionData;
//                }else{
//                    [SolutionData setObject:[NSNumber numberWithInt:0] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:[NSString stringWithFormat:@"Premium is less"] forKey:RESPONSE_MESSAGE];
//                    NSLog(@"Native Return: %@",SolutionData);
//                    return SolutionData;
//                }
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MAHALIFESUPREME]){
//                masterObj.BaseAnnualizedPremium = sisRequestObj.BasePremium;
//                //changes 01/06/2015
//                long TotalGAI = [generateSISObj getTotalGaiSumMLS];
//                long TotalMaturityValue = [generateSISObj getMaturityValueForSolution];
//                //changes 08/06/2015
//                long totMaturityBenifit =  TotalGAI + TotalMaturityValue +masterObj.sisRequest.Sumassured;
//                double gaiFactor = [DataAccessClass getGAIFactorForHSW:masterObj.sisRequest];
//                [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.BasePremium] forKey:RESPONSE_ANNUALPREMIUM];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                [SolutionData setObject:[NSNumber numberWithDouble:gaiFactor] forKey:RESPONSE_GAIFACTOR];
//                [SolutionData setObject:[NSNumber numberWithLong:TotalGAI] forKey:RESPONSE_TOTALGAI];
//                [SolutionData setObject:[NSNumber numberWithLong:TotalMaturityValue] forKey:RESPONSE_TOTMATURITY];
//                [SolutionData setObject:[NSNumber numberWithLong:totMaturityBenifit] forKey:RESPONSE_TOTBENEFIT];
//                NSLog(@"Native Return: %@",SolutionData);
//                return SolutionData;
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MAHALIFEGOLD]){
//                long annualPremium = [generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                long totalCashDividend = 0;
//                long totalBenefit = 0;
//                long totalAnnualCoupon = [generateSISObj getTotalGAI_MLGP];
//                totalCashDividend = [generateSISObj getTotalVSRBonus:masterObj.sisRequest];
//                //changes 03/06/2015
//                totalBenefit = masterObj.sisRequest.Sumassured + totalCashDividend + totalAnnualCoupon;
//                //changes 02/06/2015
//                double gaiFactor = [DataAccessClass getGAIFactorForHSW:masterObj.sisRequest];
//                
//                [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                [SolutionData setObject:[NSNumber numberWithLong:totalCashDividend] forKey:RESPONSE_TOTCASHDIVIDEND];
//                [SolutionData setObject:[NSNumber numberWithLong:totalAnnualCoupon] forKey:RESPONSE_TOTANNCOUPON];
//                [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                [SolutionData setObject:[NSNumber numberWithLong:totalBenefit] forKey:RESPONSE_TOTBENEFIT];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                //changes 02/06/2015
//                [SolutionData setObject:[NSNumber numberWithDouble:gaiFactor] forKey:RESPONSE_GAIFACTOR];
//                NSLog(@"Native Return: %@",SolutionData);
//                return SolutionData;
//            }
//            else if([masterObj.sisRequest.BasePlan containsString:MAHARAKSHASUPREME]){
//                long annualPremium =[generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                NSLog(@"Native Return: %@",SolutionData);
//                return SolutionData;
//            }else if([masterObj.sisRequest.BasePlan containsString:SMARTGROWTHPLUS]){
//                long annualPremium = [generateSISObj getBasePremium:masterObj.sisRequest.BasePlan];
//                if([self checkPremium:masterObj.sisRequest.BasePlan andWithFrequency:@"A" andWithPremium:annualPremium andWithArray:pnlMinPremData]){
//                    long TotVestedCRB = 0;
//                    long TotTerminalBonus = 0;
//                    long totBenefit = 0;
//                    long TotGuarAdditon = [generateSISObj getTotalAccGAddition];
//                    
//                    if(masterObj.sisRequest.percent == 4){
//                        //changes 6th oct
//                        TotVestedCRB = [[[generateSISObj getTotalVestedCRB:masterObj.sisRequest.percent] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] longValue];
//                        TotTerminalBonus = [[[generateSISObj getTotalTermBonus:masterObj.sisRequest.percent] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] longValue];
//                    }else{
//                        //changes 6th oct
//                        TotVestedCRB = [[[generateSISObj getTotalVestedCRB:masterObj.sisRequest.percent] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] longValue];
//                        TotTerminalBonus = [[[generateSISObj getTotalTermBonus:masterObj.sisRequest.percent] objectForKey:[NSNumber numberWithInt:(int)masterObj.sisRequest.PolicyTerm]] longValue];
//                    }
//                    totBenefit = masterObj.sisRequest.Sumassured + TotGuarAdditon + TotVestedCRB + TotTerminalBonus;
//                    
//                    [SolutionData setObject:[NSNumber numberWithInt:1] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:@"" forKey:RESPONSE_MESSAGE];
//                    [SolutionData setObject:[NSNumber numberWithLong:annualPremium] forKey:RESPONSE_ANNUALPREMIUM];
//                    [SolutionData setObject:[NSNumber numberWithLong:sisRequestObj.Sumassured] forKey:RESPONSE_SUMASSURED];
//                    [SolutionData setObject:[NSNumber numberWithLong:TotGuarAdditon] forKey:RESPONSE_GUARANTEEDADDITIONS];
//                    [SolutionData setObject:[NSNumber numberWithLong:TotVestedCRB] forKey:RESPONSE_VESTEDCRB];
//                    [SolutionData setObject:[NSNumber numberWithLong:TotTerminalBonus] forKey:RESPONSE_TOTTERMINALBONUS];
//                    [SolutionData setObject:[NSNumber numberWithLong:totBenefit] forKey:RESPONSE_TOTBENEFIT];
//                    NSLog(@"Native Return: %@",SolutionData);
//                    return SolutionData;
//                }else{
//                    [SolutionData setObject:[NSNumber numberWithInt:0] forKey:RESPONSE_CODE];
//                    [SolutionData setObject:[NSString stringWithFormat:@"Premium is less"] forKey:RESPONSE_MESSAGE];
//                    return SolutionData;
//                }
//            }
//        }
//        @catch (NSException *e) {
//            NSLog(@"the exceptio %@",e.debugDescription);
//            [SolutionData setObject:[NSNumber numberWithInt:0] forKey:RESPONSE_CODE];
//            [SolutionData setObject:[NSString stringWithFormat:@"Exception Occured %@",e.description] forKey:RESPONSE_MESSAGE];
//            NSLog(@"Native Return: %@",SolutionData);
//            return SolutionData;
//        }
//    }else{
//        [SolutionData setObject:[NSNumber numberWithInt:0] forKey:RESPONSE_CODE];
//        [SolutionData setObject:[NSString stringWithFormat:@"InputData is not proper"] forKey:RESPONSE_MESSAGE];
//        NSLog(@"Native Return: %@",SolutionData);
//        return SolutionData;
//    }
//    
//    //mahalife magic..
//}
//
//-(BOOL)checkPremium:(NSString *)basePlan andWithFrequency:(NSString *)freq andWithPremium:(long)_prem andWithArray:(NSMutableArray *)pnlMinPremData{
//    long premiumVal = 0;
//    BOOL premGreater = false;
//    for (int cnt = 0; cnt < pnlMinPremData.count ; cnt++ ) {
//        PNLMinPrem *pnlMinPremObj = [pnlMinPremData objectAtIndex:cnt];
//        if([pnlMinPremObj.PML_PLAN_CD isEqualToString:basePlan] && [pnlMinPremObj.PML_PREMIUM_MODE isEqualToString:freq]){
//            premiumVal = pnlMinPremObj.PML_MIN_PREM_AMT;
//            if(_prem >= premiumVal){
//                premGreater = true;
//            }else{
//                premGreater =  false;
//            }
//        }
//    }
//    return premGreater;
//}
//
//-(SISRequest *)getRequest:(NSDictionary *)_reqDict{
//    SISRequest *request = [[SISRequest alloc] init];
//    NSString *Strval = @"";
//    int intval = 0;
//    
//    if ([_reqDict valueForKey:@"BasePlan"] != [NSNull null]) {
//        request.BasePlan = [_reqDict valueForKey:@"BasePlan"];
//    }else
//        request.BasePlan = Strval;
//    
//    if ([_reqDict valueForKey:@"Sumassured"] != [NSNull null]) {
//        request.Sumassured = [[_reqDict valueForKey:@"Sumassured"] intValue];
//    }else
//        request.Sumassured = intval;
//    
//    if ([_reqDict valueForKey:@"PolicyTerm"] != [NSNull null]) {
//        request.PolicyTerm = [[_reqDict valueForKey:@"PolicyTerm"] integerValue];
//    }else
//        request.PolicyTerm = intval;
//    
//    if ([_reqDict valueForKey:@"PPTerm"] != [NSNull null]) {
//        request.PremiumPayingTerm = [[_reqDict valueForKey:@"PPTerm"] integerValue];
//    }else
//        request.PremiumPayingTerm = intval;
//    
//    if ([_reqDict valueForKey:@"Age"] != [NSNull null]) {
//        request.InsuredAge = [[_reqDict valueForKey:@"Age"] intValue];
//    }else
//        request.InsuredAge = intval;
//    
//    if ([_reqDict valueForKey:@"Percent"] != [NSNull null]) {
//        request.percent = [[_reqDict valueForKey:@"Percent"]intValue];
//    }else
//        request.percent = intval;
//    
//    if ([_reqDict valueForKey:@"BasePremium"] != [NSNull null]) {
//        request.BasePremium = [[_reqDict valueForKey:@"BasePremium"] intValue];
//    }else
//        request.BasePremium = intval;
//    
//    if ([_reqDict valueForKey:@"SmokerFlag"] != [NSNull null]) {
//        request.InsuredSmokerStatus = [_reqDict valueForKey:@"SmokerFlag"];
//    }else
//        request.InsuredSmokerStatus = @"";
//    
//    if ([_reqDict valueForKey:@"Sex"] != [NSNull null]) {
//        request.InsuredSex = [_reqDict valueForKey:@"Sex"];
//    }else
//        request.InsuredSex = @"";
//    
//    return request;
//}
//
//
//
//@end
