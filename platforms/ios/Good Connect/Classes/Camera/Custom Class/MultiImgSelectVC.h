//
//  MultiImgSelectVC.h
//  Good Connect
//
//  Created by Sneha Sawant on 19/12/16.
//
//

#import <UIKit/UIKit.h>
#import "ELCImagePickerHeader.h"
#import "ELCAssetSelectionDelegate.h"
@class PluginHandler;

@interface MultiImgSelectVC : UIViewController <ELCImagePickerControllerDelegate>

@property (nonatomic, retain) ELCImagePickerController *elcPicker;
@property (nonatomic, retain) NSMutableArray *arrayOfProfDetail;
@property (nonatomic, copy) NSArray *chosenImages;

-(id)initWithProfileData:(NSMutableArray *)_profArray andWithDelegate:(PluginHandler *)plugin;

@end
