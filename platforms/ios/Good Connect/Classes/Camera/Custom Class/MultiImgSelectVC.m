//
//  MultiImgSelectVC.m
//  Good Connect
//
//  Created by Sneha Sawant on 19/12/16.
//
//

#import "MultiImgSelectVC.h"
#import "PluginHandler.h"
#import "GalleryVC.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface MultiImgSelectVC ()
@property (strong, nonatomic) PluginHandler *pluginHandler;
@end

@implementation MultiImgSelectVC
@synthesize pluginHandler;

-(id)initWithProfileData:(NSMutableArray *)_profArray andWithDelegate:(PluginHandler *)plugin {
    
    if(self){
        
        self.arrayOfProfDetail = _profArray;
        pluginHandler = plugin;
        
        self.elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        self.elcPicker.maximumImagesCount = 5; //Set the maximum number of images to select to 100
        self.elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
        self.elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
        self.elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
        self.elcPicker.mediaTypes = @[(NSString *)kUTTypeImage]; //Supports image and movie types
        self.elcPicker.imagePickerDelegate = self;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ELCImagePickerControllerDelegate Method

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if ([info count] > 0) {
        
        NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
        for (NSDictionary *dict in info) {
            if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
                if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                    
                    UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    [images addObject:image];
                    
                } else {
                    NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                }
            } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
                if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                    
                    UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    [images addObject:image];
                    
                } else {
                    NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                }
            } else {
                NSLog(@"Uknown asset type");
            }
        }
        
        self.chosenImages = images;
        
        // Save imgs to device storage
        for (int i = 0 ; i<self.chosenImages.count; i++){
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            NSString *str =[[self.arrayOfProfDetail objectAtIndex:1] stringByAppendingString:[NSString stringWithFormat:@"_%d.jpg",i+1]];
            NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[self.arrayOfProfDetail objectAtIndex:0],str];
            
            // Delete previously uploaded imgs
            if ([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath] == YES) {
                NSString *fileURI = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",[self.arrayOfProfDetail objectAtIndex:0]]];
                NSError *error = nil;
                NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fileURI error:&error];
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF contains[dc] %@",[self.arrayOfProfDetail objectAtIndex:1]];
                NSLog(@"files: %@\ncount: %lu", [dirContents filteredArrayUsingPredicate:pred], (unsigned long)[dirContents filteredArrayUsingPredicate:pred].count);
                for (NSString *oldFile in [dirContents filteredArrayUsingPredicate:pred]) {
                    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[self.arrayOfProfDetail objectAtIndex:0],oldFile] error:&error];
                }
            }
            NSLog(@"File save to path %@",imageFilePath);
            NSData *JpegIamge = UIImageJPEGRepresentation([self.chosenImages objectAtIndex:i], 0.4);
            [JpegIamge writeToFile:imageFilePath atomically:YES];
        }
        
        GalleryVC *galleryVC = [[GalleryVC alloc] initWithArray:self.arrayOfProfDetail andWithImageCount:(int)[self.chosenImages count]];
        galleryVC.plugin = pluginHandler;
        [self.elcPicker presentViewController:galleryVC animated:NO completion:^{}];
        
    } else {
        [self.elcPicker dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
    [self.elcPicker dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - ELCAssetSelectionDelegate Method
- (BOOL)shouldSelectAsset:(ELCAsset *)asset previousCount:(NSUInteger)previousCount {
    
    return true;
}

@end
