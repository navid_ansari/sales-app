//
//  CustomCameraController.m
//  Life_Planner
//
//  Created by DrasttyMac on 25/03/15.
//
//

#import "CustomCameraController.h"
#import "GalleryVC.h"
#import "PluginHandler.h"

@interface CustomCameraController ()
@property (strong, nonatomic) PluginHandler *camera;
@end

@implementation CustomCameraController
@synthesize picker,imagesArray,arrayOfDocDetails, camera;

#pragma mark - Controller Methods
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
	
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if(self){
		self.picker = [[CustomPickerViewController alloc] init];
		if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
			self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
			CGRect screenFrame = [[UIScreen mainScreen] bounds];
			self.view.frame = screenFrame;
			self.picker.view.frame = screenFrame;
			self.picker.showsCameraControls = NO;
			self.picker.cameraOverlayView = self.view;
		} else if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
			self.picker =[[CustomPickerViewController alloc] init];
			self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
			self.picker.allowsEditing = NO;
		}
	}
	return self;
}

-(id)initWithDataFromPhoneGap:(NSArray *)_arrayOfDocDetails andWithDelegate:(PluginHandler *)plugin {
	
	self = [super initWithNibName:@"CustomCameraController" bundle:nil];
	if(self){
		self.arrayOfDocDetails = _arrayOfDocDetails;
		camera = plugin;
		self.picker = [[CustomPickerViewController alloc] init];
		if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
			self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
			CGRect screenFrame = [[UIScreen mainScreen] bounds];
			self.view.frame = screenFrame;
			self.picker.view.frame = screenFrame;
			self.picker.showsCameraControls = NO;
			self.picker.cameraOverlayView = self.view;
		}
		else if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			self.picker =[[CustomPickerViewController alloc] init];
			self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
			self.picker.allowsEditing = NO;
		}
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	imagesArray = [[NSMutableArray alloc] init];
	
	//for auotfocus camera...
	//changes 28th march for lock config
	AVCaptureDevice *cameraDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	if ([cameraDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus] && [cameraDevice lockForConfiguration:NULL]) {
		[cameraDevice setFocusMode:AVCaptureFocusModeAutoFocus];
		// if ([cameraDevice lockForConfiguration:NULL]) {
		[cameraDevice unlockForConfiguration];
		//}
	}else{
		NSLog(@"focus mode not supported");
	}
	
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	self.picker.delegate = self;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

// ************* Application auto rotation***********************
- (BOOL)shouldAutorotate {
	return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orient {
	return (orient == UIInterfaceOrientationLandscapeLeft) | (orient == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
	
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	NSUserDefaults *defaultuser = [NSUserDefaults standardUserDefaults];
	if (deviceOrientation == UIDeviceOrientationLandscapeLeft ){
		
		return UIInterfaceOrientationLandscapeRight;
		
	}else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
		
		return UIInterfaceOrientationLandscapeLeft;
		
	}else{
		if([[defaultuser objectForKey:@"orient"] integerValue] == 1){
			return UIInterfaceOrientationLandscapeRight;
		}else{
			return UIInterfaceOrientationLandscapeLeft;
		}
	}
	
}

- (NSUInteger)supportedInterfaceOrientations{
	return UIInterfaceOrientationMaskLandscape;
}


#pragma mark ImagePickerViewController Delegate Method
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	// Get a reference to the captured image
	UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
	
	ImageCropViewController *cropController = [[ImageCropViewController alloc] initWithImage:image];
	cropController.delegate = self;
	cropController.blurredBackground = YES;
	[self presentViewController:cropController animated:NO completion:nil];
	//[[self.controller navigationController] pushViewController:controller1 animated:NO];
	// Tell the plugin class that we're finished processing the image
	//[self.plugin capturedImageWithPath:imagePath];
}

#pragma mark ImageCroppingDelegate Methods
-(void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
	NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[arrayOfDocDetails objectAtIndex:0]];
	NSError *error;
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
	NSLog(@"folder created ::::::::: %@",dataPath);
	
	//changes 11-05-2015..
	/*UIGraphicsBeginImageContextWithOptions(croppedImage.size, YES, 1.0);
	 CGRect imageRect = CGRectMake(0, 0, croppedImage.size.width,croppedImage.size.height);
	 [croppedImage drawInRect:imageRect blendMode:kCGBlendModeLuminosity alpha:1.0];
	 UIImage *filteredImage = UIGraphicsGetImageFromCurrentImageContext();
	 UIGraphicsEndImageContext();*/
	
	//changes 08-05-2015
	//[imagesArray addObject:[self imageResize:filteredImage andResizeTo:CGSizeMake(1024, 768)]];
	//changes 09-05-2015
	//[imagesArray addObject:filteredImage];
	//changes 11-05-2015..
	//[imagesArray addObject:[self resizeImageGreaterThan500:filteredImage]];
	
	//changes 10th march removed 500 resolution
	//[imagesArray addObject:[self resizeImageGreaterThan500:croppedImage]];
	
	//Added by Sneha to check the size of image 21/02/2017
	NSData *JpegIamge = UIImageJPEGRepresentation(croppedImage, 1);
	if ([JpegIamge length] > 4194304) { //i.e. > 4MB
		[[[UIAlertView alloc] initWithTitle:@"Image size exceeded!"
									message: @"Image size should be less than 4MB."
								   delegate:nil
								cancelButtonTitle:nil
								otherButtonTitles:NSLocalizedString(@"Okay", nil), nil] show];
		//dismiss crop view and move to camera
		[self dismissViewControllerAnimated:NO completion:NULL];
		
	} else {
		[imagesArray addObject:croppedImage];
		[self writeToDocument:YES andResizingImage:nil];
		
		if ([self.imagesArray count] < 5) {
			
			UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:nil message:@"Do you want another click ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] ;
			alertView.title = @"Message";
			[alertView show];
			
		} else {
			
			//move to gallery section
			NSLog(@"first dismiss the cropview as user clicked NO");
			[self dismissViewControllerAnimated:NO completion:NULL];
			//NSLog(@"moving to gallery view with data count %d",arrayOfDocDetails.count);
			GalleryVC *galleryVC = [[GalleryVC alloc] initWithArray:arrayOfDocDetails andWithImageCount:(int)[imagesArray count]];
			galleryVC.plugin = camera;
			[self.picker presentViewController:galleryVC animated:NO completion:^{
				NSLog(@"dismiss the camera view so that when user click back button it should move to html page...");
			}];
		}
	}
}

-(UIImage *)resizeImageGreaterThan500:(UIImage *)_cropImage{
	UIImage *resizedImage;
	if (_cropImage.size.width > 500 || _cropImage.size.height > 500) {
		resizedImage = [self imageResize:_cropImage andResizeTo:CGSizeMake(500, 500)];
	}
	else
		resizedImage = _cropImage;
	return resizedImage;
}


- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
	NSLog(@"crop view cancel clicked dismiss the crop view");
	[self dismissViewControllerAnimated:NO completion:nil];
	//[self.navigationController popViewControllerAnimated:YES];
}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
	if (error) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail!"
														message:[NSString stringWithFormat:@"Saved with error %@", error.description]
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
		
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Succes!"
														message:@"Saved to camera roll"
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
		
	}
}

#pragma mark ImageResizing Method

// ImageResizing Method (image size is less than 500 kb)
-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize{
	//changes 05/06/2015
	//CGFloat scale = [[UIScreen mainScreen]scale];
	CGFloat scale = 1.0;
	/*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
	//UIGraphicsBeginImageContext(newSize);
	UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
	[img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

#pragma mark UIAlertView Delegate method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if ([alertView.title isEqualToString: @"retake"]){
		//NSLog(@"the alert is %d",alertView.tag);
		if (buttonIndex == 1){
			//retake..
		}
	}else if ([alertView.title isEqualToString:@"Message"]){
		if(buttonIndex == 0){
			//move to gallery section
			NSLog(@"first dismiss the cropview as user clicked NO");
			[self dismissViewControllerAnimated:NO completion:NULL];
			//NSLog(@"moving to gallery view with data count %d",arrayOfDocDetails.count);
			GalleryVC *galleryVC = [[GalleryVC alloc] initWithArray:arrayOfDocDetails andWithImageCount:(int)[imagesArray count]];
			galleryVC.plugin = camera;
			[self.picker presentViewController:galleryVC animated:NO completion:^{
				NSLog(@"dismiss the camera view so that when user click back button it should move to html page...");
			}];
		} else {
			NSLog(@"dismissing the crop view and moving to camera view as user clicked YES");
			[self dismissViewControllerAnimated:NO completion:NULL];
		}
	}
}

#pragma mark -- IBACTIONS
- (IBAction)takePhotoBtnClicked:(id)sender {
	[self.picker takePicture];
}

- (IBAction)cancelPhotoBtnClicked:(id)sender {
	NSLog(@"cancel foto clicked and move to html page");
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	[dict setObject:[NSNumber numberWithInt:0] forKey:@"code"];
	[dict setObject:@"Error" forKey:@"message"];
	[self.picker dismissViewControllerAnimated:NO completion:NULL];
}

#pragma mark Write Methods..
-(void)writeToDocument:(BOOL)boolvalue andResizingImage:(UIImage *)imageProcessing{
	if (boolvalue == TRUE){
		for (int i = 0 ; i<imagesArray.count; i++){
			NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *documentsDirectory = [paths objectAtIndex:0];
			//changes from jpeg to jpg 25/05/2015
			NSString *str =[[arrayOfDocDetails objectAtIndex:1] stringByAppendingString:[NSString stringWithFormat:@"_%d.jpg",i+1]];
			NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfDocDetails objectAtIndex:0],str];
			NSLog(@"File save to path %@",imageFilePath);
			//changes 12-05-2015
			//NSData *JpegIamge = UIImageJPEGRepresentation([imagesArray objectAtIndex:i], 1.0);
			NSData *JpegIamge = UIImageJPEGRepresentation([imagesArray objectAtIndex:i], 0.4);
			//changes 30-04-2015 uncomment to encrypt..
			//NSData *encryptedData = [JpegIamge AES128EncryptedDataWithKey:@"tataaiai"];
			
			//added by Sneha 24 Jan 2017
			// Delete previously uploaded imgs
			if ([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath] == YES) {
				NSString *fileURI = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",[arrayOfDocDetails objectAtIndex:0]]];
				NSError *error = nil;
				NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fileURI error:&error];
				NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF contains[dc] %@",[arrayOfDocDetails objectAtIndex:1]];
				NSLog(@"files: %@\ncount: %lu", [dirContents filteredArrayUsingPredicate:pred], (unsigned long)[dirContents filteredArrayUsingPredicate:pred].count);
				for (NSString *oldFile in [dirContents filteredArrayUsingPredicate:pred]) {
					[[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfDocDetails objectAtIndex:0],oldFile] error:&error];
				}
			}
			
			[JpegIamge writeToFile:imageFilePath atomically:YES];
		}
	} else {
		for (int i = 0 ; i<imagesArray.count; i++){
			NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *documentsDirectory = [paths objectAtIndex:0];
			//changes from jpeg to jpg 25/05/2015
			NSString *str =[[arrayOfDocDetails objectAtIndex:1] stringByAppendingString:[NSString stringWithFormat:@"_%d.jpg",i+1]];
			NSString *encryptedImageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfDocDetails objectAtIndex:0],str];
			NSLog(@"PATH ::::::::::: %@",encryptedImageFilePath);
			NSData *encryptedData =[NSData dataWithContentsOfFile:encryptedImageFilePath];
			////changes 30-04-2015 uncomment to decrypt..
			//NSData *decryptedData = [encryptedData AES128DecryptedDataWithKey:@"tataaiai"];
			[encryptedData writeToFile:encryptedImageFilePath atomically:YES];
			[self writetoDatabase:encryptedImageFilePath];
		}
	}
}

#pragma mark - DBMANAGER METHODS
-(NSString *)GetDataBasePath:(NSString *)_dbName{
	NSString *docsDir;
	NSArray *dirPaths;
	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains
	(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = dirPaths[0];
	
	//create the path to database.
	NSString *databasePath = [[NSString alloc] initWithString:
							  [docsDir stringByAppendingPathComponent:_dbName]];
	NSFileManager *filemgr = [NSFileManager defaultManager];
	if ([filemgr fileExistsAtPath: databasePath ] == NO)
	{
		NSString *defaultDataPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:_dbName];
		
		if(defaultDataPath){
			[filemgr copyItemAtPath:defaultDataPath toPath:databasePath error:NULL];
		}
	}
	return databasePath;
}

-(void)writetoDatabase :(NSString *)imagePath
{
	sqlite3_stmt *statement;
	sqlite3 *db;
	@try{
		const char *dbpath = [[self GetDataBasePath:@"Life_Planner.db"]UTF8String];
		
		if(sqlite3_open(dbpath, &db) == SQLITE_OK){
			NSString *insertSQL = [NSString stringWithFormat:@"insert into LP_DOCUMENT_UPLOAD(DOC_NAME) values('%@')",imagePath];
			NSLog(@"insert Query:%@",insertSQL);
			const char *insert_stmt = [insertSQL UTF8String];
			sqlite3_prepare_v2(db, insert_stmt, -1, &statement, NULL);
			if(sqlite3_step(statement) == SQLITE_DONE){
				NSLog(@"Insert successfuly");
			}else{
				NSLog(@"Insert failed");
			}
			sqlite3_finalize(statement);
		}
		sqlite3_close(db);
	}
	@catch (NSException *e) {
		NSLog(@"Error addcalendar :%@",e);
	}
}

@end
