//
//  CustomCameraController.h
//  Life_Planner
//
//  Created by DrasttyMac on 25/03/15.
//
//

#import <UIKit/UIKit.h>
#import "ImageCropView.h"
#import "NSString+AESCrypt.h"
#import <sqlite3.h>
#import "CustomPickerViewController.h"
#import <AVFoundation/AVFoundation.h>
@class PluginHandler;

@interface CustomCameraController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageCropViewControllerDelegate>

@property (retain, nonatomic) CustomPickerViewController *picker;
@property(nonatomic ,strong)  NSMutableArray *imagesArray;
@property (nonatomic, retain) NSArray *arrayOfDocDetails;

- (IBAction)takePhotoBtnClicked:(id)sender;
- (IBAction)cancelPhotoBtnClicked:(id)sender;
-(id)initWithDataFromPhoneGap:(NSArray *)_arrayOfDocDetails andWithDelegate:(PluginHandler *)plugin;


@end
