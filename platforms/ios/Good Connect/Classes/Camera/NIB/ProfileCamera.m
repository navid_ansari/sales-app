//
//  ProfileCamera.m
//  Life Planner
//
//  Created by admin on 15/05/15.
//
//

#import "ProfileCamera.h"
#import "PluginHandler.h"

@interface ProfileCamera ()
@property (strong, nonatomic) PluginHandler *camera;
@end

@implementation ProfileCamera
@synthesize picker,camera,arrayOfProfDetail,cropController;

-(id)initWithProfileData:(NSMutableArray *)_profArray andWithDelegate:(PluginHandler *)plugin{
    self = [super initWithNibName:@"ProfileCamera" bundle:nil];
    if(self){
        self.arrayOfProfDetail = _profArray;
        camera = plugin;
        self.picker = [[CustomPickerViewController alloc] init];
        if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            CGRect screenFrame = [[UIScreen mainScreen] bounds];
            self.view.frame = screenFrame;
            self.picker.view.frame = screenFrame;
            self.picker.showsCameraControls = NO;
            self.picker.cameraOverlayView = self.view;
        }
        else if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            self.picker =[[CustomPickerViewController alloc] init];
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            self.picker.allowsEditing = NO;
        }
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //for auotfocus camera...
    //changes 28th march for lock config
    AVCaptureDevice *cameraDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([cameraDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus] && [cameraDevice lockForConfiguration:NULL]) {
        [cameraDevice setFocusMode:AVCaptureFocusModeAutoFocus];
        // if ([cameraDevice lockForConfiguration:NULL]) {
        [cameraDevice unlockForConfiguration];
        //}
    }else{
        NSLog(@"focus mode not supported");
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.picker.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBACTIONS
-(IBAction)RetakeProfilePicture:(id)sender{
    [self.picker takePicture];
}
-(IBAction)CancelProfilePicture:(id)sender{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInt:0] forKey:@"code"];
    [dict setObject:@"error" forKey:@"message"];
    [dict setObject:@"" forKey:@"path"];
    [cropController dismissViewControllerAnimated:YES completion:nil];
    [camera capturedImageWithPath:dict];
}

#pragma mark ImagePickerViewController Delegate Method
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // Get a reference to the captured image
    UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    cropController = [[ImageCropViewController alloc] initWithImage:image];
    cropController.delegate = self;
    cropController.blurredBackground = YES;
    [self presentViewController:cropController animated:NO completion:nil];    
}

#pragma mark ImageCroppingDelegate Methods
-(void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[arrayOfProfDetail objectAtIndex:0]];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
    NSLog(@"folder created ::::::::: %@",dataPath);
    
    NSLog(@"image cropped from crop view delegate");
    //changes 08-05-2015
    //UIImage *resizedImage = [self imageResize:croppedImage andResizeTo:CGSizeMake(1024, 768)];
    //changes 09-05-2015
    //UIImage *resizedImage = croppedImage;
    //UIImage *resizedImage = [self resizeImageGreaterThan500:croppedImage];
    NSLog(@"resize the image to 500 * 500 or the same image and write to documents");
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict = [self writeTodocuments:croppedImage];
	if (dict.count) {
		[dict setObject:[NSNumber numberWithInt:1] forKey:@"code"];
		[dict setObject:@"success" forKey:@"message"];
	} else {
		[dict setObject:[NSNumber numberWithInt:0] forKey:@"code"];
		[dict setObject:@"Fail" forKey:@"message"];
	}
	
    NSLog(@"pass the data and move to html page");
    [cropController dismissViewControllerAnimated:YES completion:nil];
    [camera capturedImageWithPath:dict];
    
}

-(UIImage *)resizeImageGreaterThan500:(UIImage *)_cropImage{
    UIImage *resizedImage;
    if (_cropImage.size.width > 500 || _cropImage.size.height > 500) {
        resizedImage = [self imageResize:_cropImage andResizeTo:CGSizeMake(500, 500)];
    }
    else
        resizedImage = _cropImage;
    return resizedImage;
}


- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
    NSLog(@"crop view cancel clicked dismiss the crop view");
    [controller dismissViewControllerAnimated:NO completion:nil];
    //[self.viewController presentViewController:self.picker animated:YES completion:nil];
}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail!"
                                                        message:[NSString stringWithFormat:@"Saved with error %@", error.description]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Succes!"
                                                        message:@"Saved to camera roll"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

#pragma mark ImageResizing Method
// ImageResizing Method (image size is less than 500 kb)
-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize{
    //changes 05/06/2015
    //CGFloat scale = [[UIScreen mainScreen]scale];
    CGFloat scale = 1.0;
    UIGraphicsBeginImageContextWithOptions(newSize, YES, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark -- Public methods

-(NSMutableDictionary *)writeTodocuments:(UIImage *)image{
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //changes from jpeg to jpg 25/05/2015
    NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetail objectAtIndex:0],[NSString stringWithFormat:@"%@.jpg",[arrayOfProfDetail objectAtIndex:1]]];
    NSLog(@"first profile picture path created :::%@",imageFilePath);
    //changes 12-05-2015
    //NSData *JpegIamge = UIImageJPEGRepresentation(image, 1.0);
    //changes from 50 to 100 on 25-05-2015
//    NSData *JpegIamge = UIImageJPEGRepresentation([self imageResize:image andResizeTo:CGSizeMake(110, 110)], 1.0); //Sneha 30-12-16
    //NSData *encryptedData = [JpegIamge AES128EncryptedDataWithKey:@"tataaiai"];
	
	//Added by Sneha to check the size of image 21/02/2017
	
	if ([UIImageJPEGRepresentation(image, 1) length] > 2097152) { //i.e. > 2MB
		[[[UIAlertView alloc] initWithTitle:@"Image size exceeded!"
								message: @"Image size should be less than 2MB."
								delegate:nil
								cancelButtonTitle:nil
								otherButtonTitles:NSLocalizedString(@"Okay", nil), nil] show];
	} else {
		NSData *JpegIamge = UIImageJPEGRepresentation(image, 0.4); //Sneha 30-12-16
		[JpegIamge writeToFile:imageFilePath atomically:YES];
		NSLog(@"encryption of first profile picture created :::%@",imageFilePath);
    
		//profile pic i.e thumbnail image
		//changes from jpeg to jpg 25/05/2015
		NSString *thumbImagePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetail objectAtIndex:0],[NSString stringWithFormat:@"%@_thumb.jpg",[arrayOfProfDetail objectAtIndex:1]]];
		//********
		[returnData setValue:thumbImagePath forKey:@"path"];
		//********
    
		NSLog(@"thumb picture path created :::%@",thumbImagePath);
		//changes from 50 to 100 on 25-05-2015
		NSData *JpegThumbImage = UIImageJPEGRepresentation([self imageResize:image andResizeTo:CGSizeMake(110, 110)], 1.0);
		//NSData *encryptedThumbImageData = [JpegThumbImage AES128EncryptedDataWithKey:@"tataaiai"];
		[JpegThumbImage writeToFile:thumbImagePath atomically:YES];
		NSLog(@"encryption of thumb picture created :::%@",thumbImagePath);
	}
    return returnData;
}


@end
