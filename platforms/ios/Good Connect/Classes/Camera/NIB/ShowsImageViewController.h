//
//  ShowsImageViewController.h
//  Life_Planner
//
//  Created by DrasttyMac on 24/03/15.
//
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "NSData+AESCrypt.h"
#import "NSString+AESCrypt.h"
#import "ImageCropView.h"
#import "CustomCameraController.h"
#import "CustomPickerViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ShowsImageViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageCropViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *zoomScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic,strong)UIImage *image;
@property(nonatomic,strong) NSMutableArray *ImagesArray;
@property(nonatomic,assign) int indexPath;
@property (strong, nonatomic) CustomPickerViewController* imagePicker;
@property (nonatomic, retain) NSArray *arrayOfDocDetails;


- (IBAction)retakeBtnClicked:(id)sender;
- (IBAction)backbtnClicked:(id)sender;
-(id)initWithArrayOfDocDetails:(NSArray *)_array;


@end
