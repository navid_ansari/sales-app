//
//  GalleryVC.m
//  Life_Planner
//
//  Created by DrasttyMac on 25/03/15.
//
//

#import "GalleryVC.h"
#import "NSString+Contains.h"

@interface GalleryVC ()

@end

@implementation GalleryVC
@synthesize arrayOfImages,arrayOfDocDetails,count;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //register the nib
    UINib *cellNib = [UINib nibWithNibName:@"CameraImagesCollectionViewCell" bundle:nil];
    [self.galleryCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"Cell"];
    self.galleryCollectionView.backgroundColor =[UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id)initWithArray:(NSArray *)_arrayOfImages andWithImageCount:(int)_count{
    self = [super initWithNibName:@"GalleryVC" bundle:nil];
    if(self){
        arrayOfDocDetails = _arrayOfImages;
        self.count = _count;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path =[documentsDirectory stringByAppendingPathComponent:[arrayOfDocDetails objectAtIndex:0]];
    //NSLog(@"view will appear gallery %@",path);
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    arrayOfImages = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<directoryContent.count; i++){
        BOOL isDir = NO;
        if([[NSFileManager defaultManager]
            fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",path,[directoryContent objectAtIndex:i]] isDirectory:&isDir] && isDir){
            //NSLog(@"Is directory %@",[directoryContent objectAtIndex:i]);
        }else{
            //NSLog(@"is file %@",[directoryContent objectAtIndex:i]);
            //changes 10-04-2015..
            //show only those pictures which user has captured in gallery...
            if([[directoryContent objectAtIndex:i] containsString:[arrayOfDocDetails objectAtIndex:1]]){
                //NSLog(@"contains");
                NSData *encryptedData =[NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",path,[directoryContent objectAtIndex:i]]];
                @try {
                    [arrayOfImages addObject:[[UIImage alloc]initWithData:encryptedData]];
                }
                @catch (NSException *exception) {
                    //NSLog(@"exception occured in gallery");
                    //commented 30-04-2015 as their is no encrypted file stored..
                    //NSData *decryptedData = [encryptedData AES128DecryptedDataWithKey:@"tataaiai"];
                    //[arrayOfImages addObject:[[UIImage alloc]initWithData:decryptedData]];
                }
            }else{
                NSLog(@"not contains");
            }
        }
    }
    [self.galleryCollectionView reloadData];
    
}


#pragma mark UICOllection View Delegate Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return arrayOfImages.count;
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CameraImagesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.collectionViewCellImage.image =[arrayOfImages objectAtIndex:indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ShowsImageViewController *showsImageViewController = [[ShowsImageViewController alloc]initWithArrayOfDocDetails:arrayOfDocDetails];
    showsImageViewController.image = [arrayOfImages objectAtIndex:indexPath.row];
    showsImageViewController.indexPath = (int)indexPath.item;
    //[self.navigationController pushViewController:showsImageViewController animated:YES];
    //NSLog(@"move to showimage controller so that user can retake the image");
    [self presentViewController:showsImageViewController animated:NO completion:NULL];
}

#pragma mark - IBACTIONS
- (IBAction)backBtnClicked:(id)sender {
    
    //NSLog(@"back button clicked from the Gallery View");
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"code"];
    [dict setObject:@"Success" forKey:@"message"];
    [dict setObject:[NSNumber numberWithInt:self.count] forKey:@"count"];
    [self.plugin closeCameraWithDict:dict];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
// ************* Application auto rotation***********************
- (BOOL)shouldAutorotate {
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orient {
    return (orient == UIInterfaceOrientationLandscapeLeft) | (orient == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    NSUserDefaults *defaultuser = [NSUserDefaults standardUserDefaults];
    if (deviceOrientation == UIDeviceOrientationLandscapeLeft ){
        
        return UIInterfaceOrientationLandscapeRight;
        
    }else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
        
        return UIInterfaceOrientationLandscapeLeft;
        
    }else{
        if([[defaultuser objectForKey:@"orient"] integerValue] == 1){
            return UIInterfaceOrientationLandscapeRight;
        }else{
            return UIInterfaceOrientationLandscapeLeft;
        }
    }
    
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

//*******************************************************************
@end
