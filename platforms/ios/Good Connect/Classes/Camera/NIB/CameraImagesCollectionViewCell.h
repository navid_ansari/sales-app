//
//  CameraImagesCollectionViewCell.h
//  Life_Planner
//
//  Created by DrasttyMac on 24/03/15.
//
//

#import <UIKit/UIKit.h>

@interface CameraImagesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *collectionViewCellImage;

@end
