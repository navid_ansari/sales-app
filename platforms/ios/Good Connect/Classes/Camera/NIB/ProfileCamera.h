//
//  ProfileCamera.h
//  Life Planner
//
//  Created by admin on 15/05/15.
//
//

#import <UIKit/UIKit.h>
#import "ImageCropView.h"
#import "CustomCameraController.h"
#import "ImageCropView.h"
#import "CustomPickerViewController.h"
#import <AVFoundation/AVFoundation.h>
@class PluginHandler;

@interface ProfileCamera : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageCropViewControllerDelegate>

@property (retain, nonatomic) CustomPickerViewController *picker;
@property (nonatomic, retain) NSMutableArray *arrayOfProfDetail;
@property (nonatomic, retain) ImageCropViewController *cropController;
-(IBAction)RetakeProfilePicture:(id)sender;
-(IBAction)CancelProfilePicture:(id)sender;
-(id)initWithProfileData:(NSMutableArray *)_profArray andWithDelegate:(PluginHandler *)plugin;

@end
