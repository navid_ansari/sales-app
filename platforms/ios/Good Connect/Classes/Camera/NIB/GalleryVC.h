//
//  GalleryVC.h
//  Life_Planner
//
//  Created by DrasttyMac on 25/03/15.
//
//

#import <UIKit/UIKit.h>
#import "CameraImagesCollectionViewCell.h"
#import "ShowsImageViewController.h"
#import "CameraPluginClass.h"
#import "PluginHandler.h"

@interface GalleryVC : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *galleryCollectionView;
@property (nonatomic, retain) NSMutableArray *arrayOfImages;
@property (nonatomic, retain) NSArray *arrayOfDocDetails;
@property (nonatomic, strong) PluginHandler *plugin;
@property (nonatomic, assign) int count;

- (IBAction)backBtnClicked:(id)sender;
-(id)initWithArray:(NSArray *)_arrayOfImages andWithImageCount:(int)_count;

@end
