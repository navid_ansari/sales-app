//
//  ShowsImageViewController.m
//  Life_Planner
//
//  Created by DrasttyMac on 24/03/15.
//
//

#import "ShowsImageViewController.h"

@interface ShowsImageViewController ()

@end

@implementation ShowsImageViewController
@synthesize imageView,zoomScrollView,image,ImagesArray,indexPath,imagePicker,arrayOfDocDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = NO;
    [zoomScrollView setShowsHorizontalScrollIndicator:NO];
    [zoomScrollView setShowsVerticalScrollIndicator:NO];
    [zoomScrollView setMaximumZoomScale:30.0];
    [zoomScrollView setContentSize:CGSizeMake(1024, 1500)];
    zoomScrollView.delegate =self;
    imageView.image = image;
    
    //for autofocus camera...
    //changes 28th march for lock config
    AVCaptureDevice *cameraDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([cameraDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus] && [cameraDevice lockForConfiguration:NULL]) {
        [cameraDevice setFocusMode:AVCaptureFocusModeAutoFocus];
        // if ([cameraDevice lockForConfiguration:NULL]) {
        [cameraDevice unlockForConfiguration];
        //}
    }else{
        NSLog(@"focus mode not supported");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id)initWithArrayOfDocDetails:(NSArray *)_array{
    self = [super initWithNibName:@"ShowsImageViewController" bundle:nil];
    if(self){
        self.arrayOfDocDetails = _array;
    }
    return self;
}

#pragma mark- ScrollView Delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}

#pragma mark - Public methods..
-(void)cameraRoll{
    if(!imagePicker){
        imagePicker =[[CustomPickerViewController alloc] init];
        imagePicker.delegate = self;
        if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        else if ([CustomPickerViewController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        imagePicker.allowsEditing = NO;
    }
    [self presentViewController:imagePicker animated:NO completion:nil];
}


#pragma mark imagePickerView Delegate Method
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // Get a reference to the captured image
    UIImage* orginalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    //imageView.image = orginalImage;
    [self dismissViewControllerAnimated:NO completion:nil];
    
    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:orginalImage];
    controller.delegate = self;
    controller.blurredBackground = YES;
    [self presentViewController:controller animated:NO completion:nil];
}

#pragma mark -- IMAGE CROP DELEGATES
-(void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    //changes 11-05-2015...
    /*UIGraphicsBeginImageContextWithOptions(croppedImage.size, YES, 1.0);
    CGRect imageRect = CGRectMake(0, 0, croppedImage.size.width,croppedImage.size.height);
    [croppedImage drawInRect:imageRect blendMode:kCGBlendModeLuminosity alpha:1.0];
    UIImage *filteredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();*/
    //changes 08-05-2015
    //imageView.image = [self imageResize:filteredImage andResizeTo:CGSizeMake(1024, 768)];
    //changes 09-05-2015
    //imageView.image = filteredImage;
    //changes 11-05-2015
    //imageView.image = [self resizeImageGreaterThan500:filteredImage];
    
    //changes 10th march removed 500 resolution
    //imageView.image = [self resizeImageGreaterThan500:croppedImage];
    imageView.image = croppedImage;
    
    //changes 12-05-2015
    //NSData *JpegIamge = UIImageJPEGRepresentation(imageView.image, 1.0);
    NSData *JpegIamge = UIImageJPEGRepresentation(imageView.image, 0.4);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //changes from jpeg to jpg 25/05/2015
    NSString *str =[[arrayOfDocDetails objectAtIndex:1] stringByAppendingString:[NSString stringWithFormat:@"_%d.jpg",indexPath+1]];
    NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfDocDetails objectAtIndex:0],str];
    NSString *newPath = imageFilePath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    if ([fileManager fileExistsAtPath:imageFilePath] == YES) {
        [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    newPath = [NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfDocDetails objectAtIndex:0],str];
    
    //30-04-2015 uncomment to encrypt the image..
    //NSData *encryptedData = [JpegIamge AES128EncryptedDataWithKey:@"tataaiai"];
    [JpegIamge writeToFile:newPath atomically:YES];
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(UIImage *)resizeImageGreaterThan500:(UIImage *)_cropImage{
    UIImage *resizedImage;
    if (_cropImage.size.width > 500 || _cropImage.size.height > 500) {
        resizedImage = [self imageResize:_cropImage andResizeTo:CGSizeMake(500, 500)];
    }
    else
    resizedImage = _cropImage;
    return resizedImage;
}

- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark ImageResizing Method
-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    //changes 05/06/2015
    //CGFloat scale = [[UIScreen mainScreen]scale];
    CGFloat scale = 1.0;
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - IBAction methods..
- (IBAction)retakeBtnClicked:(id)sender {
    [self performSelectorOnMainThread:@selector(cameraRoll) withObject:nil waitUntilDone:0.3];
}

- (IBAction)backbtnClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:NULL];
}
// ************* Application auto rotation***********************
- (BOOL)shouldAutorotate {
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orient {
    return (orient == UIInterfaceOrientationLandscapeLeft) | (orient == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    NSUserDefaults *defaultuser = [NSUserDefaults standardUserDefaults];
    if (deviceOrientation == UIDeviceOrientationLandscapeLeft ){
        
        return UIInterfaceOrientationLandscapeRight;
        
    }else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
        
        return UIInterfaceOrientationLandscapeLeft;
        
    }else{
        if([[defaultuser objectForKey:@"orient"] integerValue] == 1){
            return UIInterfaceOrientationLandscapeRight;
        }else{
            return UIInterfaceOrientationLandscapeLeft;
        }
    }
    /* NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
     UIInterfaceOrientation applicationOrientation = [UIApplication sharedApplication].statusBarOrientation;
     if (applicationOrientation == UIInterfaceOrientationLandscapeLeft ){
     
     return UIInterfaceOrientationLandscapeRight;
     
     }else if (applicationOrientation == UIInterfaceOrientationLandscapeRight){
     
     return UIInterfaceOrientationLandscapeLeft;
     
     }else{
     if([[userDefaults objectForKey:@"orient"] integerValue] == 1){
     return UIInterfaceOrientationLandscapeRight;
     }else{
     return UIInterfaceOrientationLandscapeLeft;
     }
     }*/
    
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
@end
