//
//  CameraPluginClass.m
//  Life_Planner
//
//  Created by DrasttyMac on 16/03/15.
//
//

#import "CameraPluginClass.h"
//@class CustomCameraController;
#import "CustomCameraController.h"

@implementation CameraPluginClass
@synthesize overlay;


// Cordova command method
-(void) openCamera:(CDVInvokedUrlCommand *)command {
    
    // Set the hasPendingOperation field to prevent the webview from crashing
    self.hasPendingOperation = YES;
    
    // Save the CDVInvokedUrlCommand as a property.  We will need it later.
    self.latestCommand = command;
    
    NSArray *documentData = command.arguments;
    
    //******
    // Make the overlay view controller.
    [self orientationFaceUp];
    
    //******
    // Make the overlay view controller.
    self.overlay = [[CustomCameraController alloc] initWithDataFromPhoneGap:documentData andWithDelegate:self];
//    self.overlay.camera = self;
    // Display the view.  This will "slide up" a modal view from the bottom of the screen.
    NSLog(@"Moving to Camera View Controller from html");
    NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
        
        [userDefaults setInteger:1  forKey:@"orient"];
        
        
    }else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
        
        [userDefaults setInteger:0  forKey:@"orient"];
        
    }
    [userDefaults synchronize];
    [self.viewController presentViewController:self.overlay.picker animated:YES completion:nil];
    //[self.viewController.navigationController pushViewController:self.overlay.picker animated:YES];
    //******
}

// Method called by the overlay when the image is ready to be sent back to the web view
-(void) capturedImageWithPath:(NSDictionary *)dict {
    
    self.hasPendingOperation = NO;
    if([[dict objectForKey:@"code"] intValue]  == 0){
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dict] callbackId:self.latestCommand.callbackId];
    }else{
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestCommand.callbackId];
         [self.viewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)orientationFaceUp{
    
    UIDeviceOrientation orientatio =[[UIDevice currentDevice]orientation];
    
    if( orientatio != UIDeviceOrientationLandscapeRight ||
       orientatio != UIDeviceOrientationLandscapeLeft)
    {
        if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            [[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeRight] forKey:@"orientation"];
        }
        else
        {
            [[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
        }
    }
}

@end
