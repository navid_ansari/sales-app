//
//  CameraPluginClass.h
//  Life_Planner
//
//  Created by DrasttyMac on 16/03/15.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
//#import "CustomCameraController.h"
@class CustomCameraController;

@interface CameraPluginClass : CDVPlugin
// Cordova command method
-(void) openCamera:(CDVInvokedUrlCommand*)command;

// Create and override some properties and methods (these will be explained later)
-(void) capturedImageWithPath:(NSDictionary *)dict;

@property (nonatomic ,retain) CustomCameraController *overlay;
@property (strong, nonatomic) CDVInvokedUrlCommand* latestCommand;
@property (nonatomic, assign) BOOL hasPendingOperation;

@end
