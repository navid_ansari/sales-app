//
//  OpenDocument.m
//  Good Connect
//
//  Created by Sneha Sawant on 13/02/17.
//
//

#import "OpenDocument.h"

@interface OpenDocument ()

@end

@implementation OpenDocument

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithDataToShow:(NSArray *)dataToShow{
	self = [super initWithNibName:@"OpenDocument" bundle:nil];
	if(self){
		NSLog(@"datato show %@",dataToShow);
		self.arrayOfData = dataToShow;
	}
	return self;
}

- (IBAction)backClicked:(id)sender {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
	//NSString *type = @"";
	NSString *nameOfDoc = [self.arrayOfData objectAtIndex:1];
	NSString *path = [self.arrayOfData objectAtIndex:0];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
	
	NSString *fullPath = [documentsDir stringByAppendingPathComponent:path];
	NSString *filePath = [NSString stringWithFormat:@"%@/%@",fullPath,nameOfDoc];
	
	/*if([self.arrayOfData count] > 1){
		NSArray *splitStr = [[self.arrayOfData objectAtIndex:1] componentsSeparatedByString:@"."];
		nameOfDoc = [splitStr objectAtIndex:0];
		type = [splitStr objectAtIndex:1];
	}
	*/
	
	//NSURL *targetURL = [[NSBundle mainBundle] URLForResource:nameOfDoc withExtension:type subdirectory:fullPath];
	NSURL *Url = [NSURL fileURLWithPath:filePath];
	NSURLRequest *request = [NSURLRequest requestWithURL:Url];
	[self.webviewShowDoc loadRequest:request];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
