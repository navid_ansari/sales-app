//
//  OpenDocument.h
//  Good Connect
//
//  Created by Sneha Sawant on 13/02/17.
//
//

#import <UIKit/UIKit.h>

@interface OpenDocument : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webviewShowDoc;
- (id)initWithDataToShow:(NSArray *)dataToShow;

- (IBAction)backClicked:(id)sender;
@property (nonatomic, retain) NSArray *arrayOfData;

@end
