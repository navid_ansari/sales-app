//
//  CreateFolders.h
//  Sales App
//
//  Created by Sneha Sawant on 21/10/16.
//
//

#import <Cordova/CDVPlugin.h>

@interface CreateFolders : CDVPlugin

- (void) createInitialFolders;
- (BOOL) createFolder: (NSArray*)folderArray;

@end
