//
//  DBAccess.h
//  Life_Planner
//
//  Created by Admin on 14/11/14.
//
//
#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Constant.h"

@interface DBAccess : NSObject

- (NSString *) getDatabasePath:(NSString *)dbName;
- (void) copyDatabaseIfNeeded:(NSString *)dbName;
- (void)encryptDataBase:(NSString *)dbName;
- (int) queryUserVersion;
- (void)updatePragmaUserVersion:(int)version;

@end
