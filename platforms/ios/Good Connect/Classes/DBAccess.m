//
//  DBAccess.m
//  Life_Planner
//
//  Created by Admin on 14/11/14.
//
//
#import "DBAccess.h"

@implementation DBAccess
- (NSString *) getDatabasePath:(NSString *)dbName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSLog(@"getPath:%@",[documentsDir stringByAppendingPathComponent:dbName]);
    return [documentsDir stringByAppendingPathComponent:dbName];
}

- (void) copyDatabaseIfNeeded:(NSString *)dbName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSLog(@"FileManager: %@",fileManager);
    NSError *error;
    NSString *path_Doc = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]stringByAppendingPathComponent:dbName];
	NSError *err;
	if([dbName isEqualToString:SIS]){
		BOOL delSucc = [fileManager removeItemAtPath:path_Doc error:&err];
		if(delSucc)
			NSLog(@"SIS deleted successfully");
	}
    BOOL success = [fileManager fileExistsAtPath:path_Doc];
    if(!success) {
        NSString *defaultDBPath = [[NSBundle mainBundle] pathForResource:[[dbName componentsSeparatedByString:@"."] objectAtIndex:0] ofType:[[dbName componentsSeparatedByString:@"."] objectAtIndex:1] inDirectory:@"www/databases"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:path_Doc error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
}

-(void)encryptDataBase:(NSString *)dbName
{
    sqlite3 *database;
	sqlite3 *newDatabase;
	sqlite3_stmt *statement;
    int err;
	
    NSString *path_u = @"";
	NSFileManager *manager = [NSFileManager defaultManager];
	
	BOOL isencrypted = [[NSUserDefaults standardUserDefaults] boolForKey:@"DBENCRYPTED"];
	
	int databaseVersion = 0;
	NSString *encryptedPath = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),LIFE_ENCRYPT];

	if(isencrypted == NO || [dbName isEqualToString:SIS]){
		[self copyDatabaseIfNeeded:dbName];
		
		path_u = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),dbName];
		NSLog(@"path is %@",path_u);
		
		if (sqlite3_open([path_u UTF8String], &database) == SQLITE_OK)
		{
			int pgmVerRes = sqlite3_prepare_v2(database,[@"PRAGMA user_version;" UTF8String], -1, &statement, NULL);
			if(pgmVerRes == SQLITE_OK){
				
				while(sqlite3_step(statement) == SQLITE_ROW) {
					databaseVersion = sqlite3_column_int(statement, 0);
					NSLog(@"%s: encryptDataBase before version %d", __FUNCTION__, databaseVersion);
				}
				NSLog(@"encryt is %@",encryptedPath);
				err = sqlite3_exec(database, [[NSString stringWithFormat:@"attach database '%@' as encrypted key '%@';",encryptedPath, @"life"] UTF8String], NULL, NULL, NULL);
				
				if (err == SQLITE_OK){
					err = sqlite3_exec(database,[@"SELECT sqlcipher_export('Encrypted');" UTF8String], NULL, NULL, NULL);
					if (err == SQLITE_OK){
						err = sqlite3_exec(database, [@"DETACH DATABASE Encrypted;" UTF8String], NULL, NULL, NULL);
						if (err == SQLITE_OK){
							NSLog(@"DONE");
							BOOL delSucc = [manager removeItemAtPath:path_u error:NULL];
							if(delSucc){
								BOOL rename = [manager moveItemAtPath:encryptedPath toPath:path_u error:NULL];
								if(rename){
									NSLog(@"rename success");
									[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DBENCRYPTED"];
									[[NSUserDefaults standardUserDefaults] synchronize];
								}
							}else{
								NSLog(@"Delete fail");
							}
						}else{
							NSLog(@"fail to detach database code == %d",err);
						}
					}else
					{
						NSLog(@"fail to export database with code == %d",err);
					}
				}else{
					NSLog(@"fail to attach database with code == %d",err);
				}
			}
			sqlite3_close(database);
		}else
		{
			NSLog(@"fail to open the database");
		}
		if(sqlite3_open([path_u UTF8String], &database) == SQLITE_OK){
			const char* key = [@"life" UTF8String];
			sqlite3_key(database, key, (int)strlen(key));
			NSString *updatedQuery = [NSString stringWithFormat:@"pragma user_version = %d",databaseVersion];
			int pgmVerNewRes = sqlite3_exec(database,[updatedQuery UTF8String],NULL, NULL, NULL);
			if(pgmVerNewRes == SQLITE_OK){
				NSLog(@"%s: encryptDataBase after version %d", __FUNCTION__, databaseVersion);
			}
			sqlite3_close(database);
		}
	}
}

-(void)queryEncryptedDB
{
    NSString *encryptedPath = [NSString stringWithFormat:@"%@/Documents/Encrypted.db",NSHomeDirectory()];
    sqlite3 *db;
    sqlite3_stmt *statement;
    if (sqlite3_open([encryptedPath UTF8String], &db) == SQLITE_OK) {
        const char* key = [@"life" UTF8String];
        sqlite3_key(db, key, (int)strlen(key));
        // NSLog(@"the value is %d",sqlite3_exec(db, (const char*) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL));
        if (sqlite3_exec(db, (const char*) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL) == SQLITE_OK) {
            NSLog(@"Password is correct, or a new database has been initialized");
            NSString *query = @"select * from LP_PNL_PLAN_LK";
            const char *query_stmt = [query UTF8String];
            
            if(sqlite3_prepare_v2(db, query_stmt, -1, &statement, NULL) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    
                    NSString *PNL_ID =   [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                    NSLog(@"the alue is %@",PNL_ID);
                    
                }
                
                sqlite3_finalize(statement);
                sqlite3_close(db);
            }else{
                NSLog(@"the data base prepare %d",sqlite3_prepare_v2(db, query_stmt, -1, &statement, NULL));
            }
        } else {
            NSLog(@"Incorrect password!");
        }
        sqlite3_close(db);
    }
}

sqlite3* db = NULL;
-(int)queryUserVersion
{
	// get current database version of schema
	static sqlite3_stmt *stmt_version;
	int databaseVersion = 0;
	NSString *docsDir;
	NSArray *dirPaths;
	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains
	(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = dirPaths[0];
	
	NSString *databasePath = [[NSString alloc] initWithString:
							  [docsDir stringByAppendingPathComponent: [NSString stringWithFormat:@"SA_DB_MAIN.db"]]];
	const char *dbpath = [databasePath UTF8String];
	
	if(sqlite3_open(dbpath, &db) == SQLITE_OK){
		const char* key = [@"life" UTF8String];
        sqlite3_key(db, key, (int)strlen(key));
		
		if(sqlite3_prepare_v2(db, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
			while(sqlite3_step(stmt_version) == SQLITE_ROW) {
				databaseVersion = sqlite3_column_int(stmt_version, 0);
				NSLog(@"%s: version %d", __FUNCTION__, databaseVersion);
			}
			NSLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
		} else {
			NSLog(@"%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(db) );
		}
	}
	
	sqlite3_finalize(stmt_version);
	
	return databaseVersion;
}

-(void)updatePragmaUserVersion:(int)version
{
	NSString *docsDir;
	NSArray *dirPaths;
	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains
	(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = dirPaths[0];
	
	NSString *databasePath = [[NSString alloc] initWithString:
							  [docsDir stringByAppendingPathComponent: [NSString stringWithFormat:@"SA_DB_MAIN.db"]]];
	const char *dbpath = [databasePath UTF8String];
	
	if (sqlite3_open(dbpath, &db)==SQLITE_OK) {
		NSLog(@"database Opened");
		const char* key = [@"life" UTF8String];
		sqlite3_key(db, key, (int)strlen(key));
		NSString *updatedQuery = [NSString stringWithFormat:@"pragma user_version = %d",version];
		//const char* updateQuery= [updatedQuery UTF8String];
		sqlite3_stmt *stmt;
		if (sqlite3_prepare_v2(db, [updatedQuery UTF8String], -1, &stmt, NULL)==SQLITE_OK) {
			int stepValue = sqlite3_step(stmt);
			if(stepValue == SQLITE_DONE)
				NSLog(@"Query Executed");
		}
	}
}

@end
