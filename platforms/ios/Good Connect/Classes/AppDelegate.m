/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  Good Connect
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "CreateFolders.h"
#import "DBManager.h"
#import <Cordova/CDVPlugin.h>

@implementation AppDelegate

@synthesize window, viewController,versionNo;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
    DBAccess *access = [[DBAccess alloc] init];
    [access encryptDataBase:@"SA_DB_MAIN.db"];
	[access encryptDataBase:@"SA_DB_SIS.db"];

	//Check DB and Version changes
	[self checkVersion];
	[self checkDBVersion];

    [[CreateFolders new] createInitialFolders];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];

}

//check for version 8, 9 and 10.
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    //changes 8th feb
    //goodsolutions changes to goodsolutionsengine
    if ([[url scheme] isEqualToString:@"GoodConnect"]) {
        //here check for buynow or exit call in url paramters
        //if buynow insert data to combo tables
        //if exit stay in the same page
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleOpenURL:)name:@"GoodConnect" object:nil];
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];
    }
    return YES;
}

// repost all remote and local notification using the default NSNotificationCenter so multiple plugins may respond
- (void)            application:(UIApplication*)application
    didReceiveLocalNotification:(UILocalNotification*)notification
{
    // re-post ( broadcast )
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}

#pragma mark - Public methods
//check for current version..
-(void)checkVersion{
    UIAlertView *alert= nil;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfURL:[NSURL URLWithString:PLISTURL]];
    NSArray *itemArr = [dict objectForKey:KEY_ITEMS];
    NSString *serverVersion = [[[itemArr objectAtIndex:0] objectForKey:KEY_METADATA] objectForKey:KEY_VERSION];
    serverVersion = [serverVersion stringByReplacingOccurrencesOfString:@"." withString:@""];

    versionNo = [serverVersion integerValue];
	//testing purpose
    //versionNo = 100;
    if ([userDefaults integerForKey:USERDEFAULT_KEY_VERSION]){
        NSInteger currentVersion = [userDefaults integerForKey:USERDEFAULT_KEY_VERSION];
        if(currentVersion < versionNo){
            alert = [[UIAlertView alloc] initWithTitle:@"Update" message:@"Update Available" delegate:self cancelButtonTitle:@"Update" otherButtonTitles:@"Cancel", nil];
            alert.tag = 1;
            NSLog(@"App require to update :: %@",[userDefaults objectForKey:USERDEFAULT_KEY_VERSION]);
        }else{
            NSLog(@"No Updates Required :: %@",[userDefaults objectForKey:USERDEFAULT_KEY_VERSION]);
        }
    }else{
        NSLog(@"App is updated");
		[userDefaults setInteger:versionNo forKey:USERDEFAULT_KEY_VERSION]; //Sneha 06 Feb 2017
    }
    [alert show];
    [userDefaults synchronize];
}

//check for DB version check..
-(void)checkDBVersion{
	int versionDb = [[[DBAccess alloc] init] queryUserVersion];
	sqlite3 *database;
	sqlite3_stmt *statement;
	NSString *docsDir;
	NSArray *dirPaths;
	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains
	(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = dirPaths[0];
	NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: [NSString stringWithFormat:@"SA_DB_MAIN.db"]]];
	const char *dbpath = [databasePath UTF8String];
	if(sqlite3_open(dbpath, &database) == SQLITE_OK){
		const char* key = [@"life" UTF8String];
		sqlite3_key(database, key, (int)strlen(key));
		if(versionDb < 15){
			NSLog(@"the version is %d",versionDb);
			NSArray *alterTable = [[NSArray alloc]
									initWithObjects:
                                    @"ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN OCCU_QUES_SNO TEXT(10)",
									@"CREATE TABLE LP_APP_OCCU_QUESTIONNAIRE (SNO TEXT(10) PRIMARY KEY  NOT NULL , APP_SNO TEXT(10) NOT NULL , OCCUPATION_CODE TEXT(5), MPDOC_CODE TEXT(5), MPPROOF_CODE TEXT(5), FORMID TEXT(20), ISACTIVE TEXT(10), VERSION_NO TEXT(10), MODIFY_DATE DATETIME, QUESTIONNAIRE_DESC TEXT(100))", nil];
									//@"ALTER TABLE LP_APP_OCCU_QUESTIONNAIRE ADD COLUMN QUESTIONNAIRE_DESC TEXT(100)",
			for (int i=0; i<alterTable.count; i++) {
				const char *query_stmt =  [[alterTable objectAtIndex:i] UTF8String];
				int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
				if(result == SQLITE_OK){
					int altered = sqlite3_step(statement);
					if(altered == SQLITE_DONE){
						NSLog(@"alter success");
					}else{
						NSLog(@"alter failed");
					}
					sqlite3_finalize(statement);
				}
			}
			[[[DBAccess alloc] init] updatePragmaUserVersion:15];
		}
        if(versionDb < 16){
            NSLog(@"the version is %d",versionDb);
            NSArray *alterTable = [[NSArray alloc]
                                   initWithObjects:
                                   @"CREATE TABLE LP_LEAD_EKYC_DTLS (EKYC_ID TEXT(15) , LEAD_ID TEXT(15), CUST_TYPE TEXT(20), PUR_FOR_CD TEXT(3), BUY_FOR_CD TEXT(3), AGENT_CD TEXT(10), EKYC_FLAG TEXT(1), NAME TEXT(60), GENDER TEXT(5), DOB TEXT(20), EMAIL_ID TEXT(20), BUILDING TEXT(30), STREET TEXT(30), AREA TEXT(30), CITY TEXT(30), DISTRICT TEXT(30), STATE TEXT(20), PIN TEXT(15), POSTOFFICE TEXT(15), EKYC_TRAN_ID TEXT(15), CARE_OF TEXT(10), PHONE_NO TEXT(20), NATIONALITY TEXT(30), CONSUMER_AADHAR_NO TEXT(12), RESPONSE_CODE TEXT(10), RESPONSE_MSG TEXT(20), AUTH_CODE_UIDAI TEXT(10), AUTH_MSG TEXT(20), TIMESTAMP TEXT(20), ERROR_CD_UIDAI TEXT(10), ERROR_MSG_UIDAI TEXT(20), EKYC_DONE_DATE TEXT(20), SUBDISTRICT TEXT(30), LANDMARK TEXT(30),PRIMARY KEY(EKYC_ID,LEAD_ID,AGENT_CD))",
                                   @"ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN INS_EKYC_ID TEXT(15)",
                                   @"ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN INS_EKYC_FLAG TEXT(1)",
                                   @"ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN PR_EKYC_ID TEXT(15)",
                                   @"ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN PR_EKYC_FLAG TEXT(1)",
                                   @"ALTER TABLE LP_MYOPPORTUNITY ADD COLUMN BUY_FOR TEXT(3)",
                                   @"ALTER TABLE LP_APP_CONTACT_SCRN_A ADD COLUMN EKYC_ID TEXT(15)", nil];
            //@"ALTER TABLE LP_APP_OCCU_QUESTIONNAIRE ADD COLUMN QUESTIONNAIRE_DESC TEXT(100)",
            for (int i=0; i<alterTable.count; i++) {
                const char *query_stmt =  [[alterTable objectAtIndex:i] UTF8String];
                int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
                if(result == SQLITE_OK){
                    int altered = sqlite3_step(statement);
                    if(altered == SQLITE_DONE){
                        NSLog(@"alter success");
                    }else{
                        NSLog(@"alter failed");
                    }
                    sqlite3_finalize(statement);
                }
            }
            [[[DBAccess alloc] init] updatePragmaUserVersion:16];
        }
        if(versionDb < 17){
            NSLog(@"the version is %d",versionDb);
            NSArray *alterTable = [[NSArray alloc]
                                   initWithObjects:
                                   @"CREATE TABLE 'LP_DEDUPE_DATA' ('APPLICATION_ID' TEXT(15) NOT NULL , 'POLICY_NO' TEXT(10), 'AGENT_CD' TEXT(10) NOT NULL, 'RESP' TEXT(5) , 'RESULT_APPDATA' TEXT(300), 'RESULT_MEDSAR' TEXT(15), 'RESULT_FINSAR' TEXT(50), 'RESULT_TPREM' TEXT(50), PRIMARY KEY ('APPLICATION_ID', 'AGENT_CD'))", nil];
            //@"ALTER TABLE LP_APP_OCCU_QUESTIONNAIRE ADD COLUMN QUESTIONNAIRE_DESC TEXT(100)",
            for (int i=0; i<alterTable.count; i++) {
                const char *query_stmt =  [[alterTable objectAtIndex:i] UTF8String];
                int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
                if(result == SQLITE_OK){
                    int altered = sqlite3_step(statement);
                    if(altered == SQLITE_DONE){
                        NSLog(@"alter success");
                    }else{
                        NSLog(@"alter failed");
                    }
                    sqlite3_finalize(statement);
                }
            }
            [[[DBAccess alloc] init] updatePragmaUserVersion:17];
        }
	}
	sqlite3_close(database);
}

//Database Alter or update happens if any..
-(void)alterDatabase{
    //database updates
    sqlite3 *database;
    //sqlite3_stmt *statement;
    NSString *docFile = [[[DBAccess alloc] init] getDatabasePath:LIFE_PLANNER];// [self GetDataBasePath:@"SA_DB_MAIN.db"];
    const char *dbpath = [docFile UTF8String];

    //NSArray *tableArr = [[NSArray alloc] initWithObjects:@"",nil];

    /*NSArray *alterTable = [[NSArray alloc]
                           initWithObjects:
                           @"",@"", nil]; // TEST Update*/

    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        /*for (int i=0; i<tableArr.count; i++) {
            const char *query_stmt =  [[NSString stringWithFormat:@"DROP TABLE LP_APPLICATION_MAIN"] UTF8String];
            int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
            if(result == SQLITE_OK){
                int altered = sqlite3_step(statement);
                if(altered == SQLITE_DONE){
                    NSLog(@"alter success");
                }else{
                    NSLog(@"alter failed");
                }
                sqlite3_finalize(statement);
            }
        }

        **** ALTER SIS TABLE ******Added by Drastty

        for (int i=0; i<alterTable.count; i++) {
            const char *query_stmt =  [[alterTable objectAtIndex:i] UTF8String];
            int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
            if(result == SQLITE_OK){
                int altered = sqlite3_step(statement);
                if(altered == SQLITE_DONE){
                    NSLog(@"alter table success %s",query_stmt);
                }else{
                    NSLog(@"alter failed %s",query_stmt);
                }
                sqlite3_finalize(statement);
            }
            else{
                NSLog(@"already altered %s",query_stmt);
            }
        }*/
        sqlite3_close(database);
    }
}

//updates the life planner...
-(void)updateLifePlanner{
    NSURL *url = [NSURL URLWithString:UPDATEURL];
    UIApplication *app = [UIApplication sharedApplication];
    if ([app canOpenURL:url]) [app openURL:url];
    else NSLog(@"cant open");
}

#pragma mark - UIALERTVIEW DELEGATE
//if update button clicked..
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (alertView.tag == 1) {
        if(buttonIndex == 0){
            [userDefaults setInteger:versionNo forKey:USERDEFAULT_KEY_VERSION];
            [userDefaults synchronize];
            [self updateLifePlanner];
            exit(0);
        }
    }
}


@end
