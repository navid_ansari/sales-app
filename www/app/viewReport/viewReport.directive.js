viewReportModule.directive('viewReport',['ViewReportService', function (ViewReportService) {
	"use strict";
    return {
		template: function(tElement, tAttrs) {
					return ViewReportService.getReport();
				}
	};
}]);
