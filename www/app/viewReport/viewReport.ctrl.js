viewReportModule.controller('ViewReportCtrl',['$q','$state', 'ReportType', 'ReportID', 'ReportParams', 'PreviousState', 'PreviousStateParams', 'ViewReportService', 'SavedReport', 'CommonService','AppSyncService','onlinePaymentService','LoadApplicationScreenData', 'SisFormService','AddLeadService', function($q,$state, ReportType, ReportID, ReportParams, PreviousState, PreviousStateParams, ViewReportService, SavedReport, CommonService, AppSyncService, onlinePaymentService,LoadApplicationScreenData, SisFormService, AddLeadService){
	var vrc = this;
	this.reportType = ReportType;
	this.reportId = ReportID;
	this.reportParams = ReportParams;
	this.previousState = PreviousState;
	this.previousStateParams = PreviousStateParams;

	this.confirmed = false;
	this.isDataSynced = true;
	this.saveCondition = (!!ReportParams && !!ReportParams.refFlag) ? ReportParams.refFlag : null;
	if(!!ReportParams && !!ReportParams.APP_ID && !!ReportParams.AGENT_CD){
		LoadApplicationScreenData.loadApplicationFlags(ReportParams.APP_ID, ReportParams.AGENT_CD).then(function(mainData){
				debug(vrc.isDataSynced+"mainData :"+JSON.stringify(mainData));
				if(mainData.applicationFlags.ISSYNCED == 'Y')
					{
						vrc.isDataSynced = false;
						debug(vrc.isDataSynced+"ISSYNCED :1:"+mainData.applicationFlags.ISSYNCED);
					}
					else {
						debug(vrc.isDataSynced+"ISSYNCED :2:"+mainData.applicationFlags.ISSYNCED);
					}

		});

		onlinePaymentService.getPaymentData(ReportParams.APP_ID, ReportParams.AGENT_CD).then(
			function(resp){
				vrc.onlinePayCond = resp;
				debug("value :"+JSON.stringify(vrc.onlinePayCond));
				vrc.showPayNow = (vrc.reportType == "APP") && (!vrc.saveCondition) && (ReportParams.COMBO_FLAG!='T') && (vrc.onlinePayCond);
			}
		);
	}
	CommonService.hideLoading();

    /*SIS Header Calculator*/
    setTimeout(function(){
        var viewReportGetHeight = $(".viewreport-header-wrap").height();
        $(".view-report .custom-position").css({top:viewReportGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	debug("this.reportType: " + this.reportType);
	debug("this.reportID: " + this.reportId);
	debug("this.reportParams: " + JSON.stringify(this.reportParams));
	debug("this.previousState: " + JSON.stringify(this.previousState));
	this.showShareButton = (this.reportType == "SIS" || this.reportType == "FHR");
	this.showSaveButton = (this.reportType != "SIS" && this.reportType != "FF" && this.reportType != "FHR" && this.reportType!= "APP" && this.reportType!= "EKYC") || (this.saveCondition);
	this.showConfirmButton = (this.reportType == "EKYC") && (!this.reportParams.HIDE_CONFIRM_BUTTON);
	//Drastty
	this.showSync = (this.reportType == "APP") && (!this.saveCondition) && (ReportParams.COMBO_FLAG!='T');
	this.showPayNow = (this.reportType == "APP") && (!this.saveCondition) && (ReportParams.COMBO_FLAG!='T') && (this.onlinePayCond);

	//console.log(ReportParams.COMBO_FLAG+"this.showPayNow :"+this.showPayNow)
	this.shareReport = function(){
		if(CommonService.checkConnection()!=="offline"){
			ViewReportService.shareReport(this.reportType, this.reportId, this.reportParams);
		}
		else{
			navigator.notification.alert("Please connect to the internet to share report.",null,"Report","OK");
		}
	};

	this.saveReport = function(){
		ViewReportService.saveReport(this.reportType,this.reportParams,this.reportId).then(
			function(res){
				if(!!res)
					navigator.notification.alert('Saved Successfuly',null,"Report","OK");
			}
		);
	};

	this.exit = function(flag){
		if(this.reportType=="EKYC"){
			if(!!flag){
				if(this.reportParams.TYPE=="insured")
					AddLeadService.insConfirmed = true;
				else
					AddLeadService.propConfirmed = true;
			}
			else{
				if(this.reportParams.TYPE=="insured")
					AddLeadService.insConfirmed = false;
				else
					AddLeadService.propConfirmed = false;
			}
		}
		if(!!this.previousState)
			$state.go(this.previousState, this.previousStateParams);
	};

	//Drastty
	this.syncAppData = function(){
		var PGL_ID = vrc.reportParams.PGL_ID;
		if(!!ReportParams){
			if(PGL_ID != "185" && PGL_ID != "191" && PGL_ID!=FG_PGL_ID && (((BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner') && PGL_ID != SIP_PGL_ID) || (BUSINESS_TYPE!='iWealth' && BUSINESS_TYPE!='GoodSolution' && BUSINESS_TYPE!='LifePlaner'))){
				navigator.notification.confirm("Do you want to Sync Application Data ?",
					function(buttonIndex){
						if(buttonIndex=="1"){
							CommonService.showLoading("Loading...");
							if(vrc.reportParams.EMPLOYEE_FLAG == 'E')
									navigator.notification.alert("Note: Final submission for this policy will not be done, since data entry is done in Training Role.",null,"Trainee","OK");
											if(!!vrc.reportParams.COMBO_ID)
												LoadApplicationScreenData.loadOppFlags(vrc.reportParams.COMBO_ID, vrc.reportParams.AGENT_CD, 'combo', 'T').then(function(oppData){
														debug("complete loadOppFlags");
													AppSyncService.startAppDataSync(ReportParams.FHR_ID, null, ReportParams.OPP_ID,ReportParams.SIS_ID, ReportParams.APP_ID, ReportParams.CUST_EMAIL_ID,null,null,'T').then(function(res){
															//console.log("COMBO: inside complete Base syncAppData ::"+JSON.stringify(oppData));

															if(!!oppData.oppFlags && !!oppData.oppFlags.FHR_ID && !!res && res!=false)
																AppSyncService.startAppDataSync(oppData.oppFlags.FHR_ID,oppData.oppFlags.LEAD_ID, oppData.oppFlags.OPPORTUNITY_ID, oppData.oppFlags.SIS_ID, oppData.oppFlags.APPLICATION_ID, ReportParams.CUST_EMAIL_ID, oppData.oppFlags.POLICY_NO,null,'T').then(function(Tres){
																			if(!!Tres && Tres!=false)
																			vrc.updateLPAppMain(ReportParams.APP_ID, vrc.reportParams.AGENT_CD).then(function(){
                                                                                vrc.updateLPAppMain(oppData.oppFlags.APPLICATION_ID, vrc.reportParams.AGENT_CD).then(function(){
                                                                                   CommonService.hideLoading();
                                                                                   navigator.notification.alert(APP_DATA_SYN_MSG,function(){
																					vrc.isDataSynced = false;
                                                                                   } ,"Application Sync","OK");
                                                                                   console.log("COMBO: inside complete syncAppData ::"+JSON.stringify(res));

                                                                                });

                                                                            });
																	});
															else
																navigator.notification.alert(REQ_FAIL_MSG,null,"Application Sync","OK");
															});
													});
											else
												AppSyncService.startAppDataSync(ReportParams.FHR_ID, null, ReportParams.OPP_ID,ReportParams.SIS_ID, ReportParams.APP_ID, ReportParams.CUST_EMAIL_ID,null).then(function(res){
														console.log("NONCOMBO :inside complete syncAppData ::"+JSON.stringify(res));
														vrc.isDataSynced = false;
												});
						}
					},
					'Confirm Data Sync',
					'Yes, No'
				);
			}
			else{
				if(PGL_ID==FG_PGL_ID || (PGL_ID==SIP_PGL_ID && (BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner')))
					navigator.notification.alert("The sale of this product via tabsales has been discontinued.",null,"App Form","OK");
				else
					navigator.notification.alert("Please buy this product online - through WebSales.",null,"App Form","OK");
			}
		}
		else
			debug("\n \n No reportParams found !!");
	}
    this.updateLPAppMain = function(APP_ID, AGENT_CD){
			var issynced = 'Y';
			if(vrc.reportParams.EMPLOYEE_FLAG == 'E')
				issynced = 'E';
        var dfd = $q.defer();
              CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":APP_ID,"AGENT_CD":AGENT_CD}).then(
                    function(res){
                        console.log("ISSYNCED update success !!"+APP_ID);
                        dfd.resolve(true);
                    });
        return dfd.promise;
    }
	//Added by Amruta for OnlinePayment
	this.olPaymentData = function(){
		debug("inside callpopup");
		if(!!ReportParams){
			debug("App id::"+ReportParams.APP_ID);
			onlinePaymentService.checkPayDtls(ReportParams.APP_ID);
		}
	};

	this.reportPopup = true;

	this.calOlPop = function(){
		var PGL_ID = vrc.reportParams.PGL_ID;
		debug("PGL_ID : " + PGL_ID);
		if(PGL_ID != "185" && PGL_ID != "191" && PGL_ID!=FG_PGL_ID && (((BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner') && PGL_ID != SIP_PGL_ID) || (BUSINESS_TYPE!='iWealth' && BUSINESS_TYPE!='GoodSolution' && BUSINESS_TYPE!='LifePlaner'))){
			if(!!ReportParams){
				onlinePaymentService.getProposerData(ReportParams.APP_ID).then(function(CustData){

					console.log("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
					debug("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
					vrc.CustName = CustData[0].FNAME;
					vrc.MobNo = CustData[0].MOBILENO;
					vrc.Email = CustData[0].EMAIL;
				vrc.TransAmt = (!!CustData[0].TERM_AMOUNT?parseInt(CustData[0].AMOUNT)+parseInt(CustData[0].TERM_AMOUNT):CustData[0].AMOUNT);
				});
			}
			this.reportPopup = !this.reportPopup;
		}
		else{
			if(PGL_ID==FG_PGL_ID || (PGL_ID==SIP_PGL_ID && (BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner')))
				navigator.notification.alert("The sale of this product via tabsales has been discontinued.",null,"App Form","OK");
			else
				navigator.notification.alert("Please buy this product online - through WebSales.",null,"App Form","OK");
		}
	}

	this.closePopUpOverlay = function(){
		this.reportPopup = !this.reportPopup;
	};

	this.onConfirm = function(){
		if(!this.confirmed){
			navigator.notification.confirm("Do you want to use this details?",
				function(buttonIndex){
					if(buttonIndex=="1"){
						debug("confirming");
						ViewReportService.onConfirm(vrc.reportType,vrc.reportParams,vrc.reportId).then(
							function(response){
								if(!response){
									navigator.notification.alert("Could not save the E-KYC report.",null,"E-KYC Report","OK");
								}
								else{
									vrc.confirmed = true;
									vrc.exit(true);
								}
								debug("response: " + JSON.stringify(response));
							}
						);
					}
					else{
						debug("not confirming");
						vrc.confirmed = false;
						vrc.exit(false);
					}
				}, "Confirm E-KYC Data", ["Yes", "No"]
			);
		}
	};
}]);

viewReportModule.service('ViewReportService',['SisFormService', 'LoginService', 'CommonService', '$q', 'leadEKYCConfirmService', 'FhrService', function(SisFormService, LoginService, CommonService, $q, leadEKYCConfirmService, FhrService){
	var vr = this;
	this.shareReport = function(reportType, reportId, reportParams){
		var agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
		if(reportType=="SIS"){
			CommonService.showLoading("Sharing SIS...please wait...");
			SisFormService.shareSIS(agentCd, reportId, reportParams.CUST_EMAIL_ID, null, reportParams.LEAD_ID).then(
				function(res){
					CommonService.hideLoading();
					if(!!res && res=="S")
						navigator.notification.alert("SIS shared successfully.",null,"SIS Report","OK");
					else
						navigator.notification.alert("Could not share SIS. Please try again later.",null,"SIS Report","OK");
				}
			);
		}
		else if(reportType=="FHR"){
			navigator.notification.confirm("After Sharing FHR you can not add or edit any FHR Details. Output will be generated. If Ok then Press 'Yes'.",
				function(buttonIndex) {
					if (buttonIndex == "1") {
						CommonService.showLoading("Sharing FHR...please wait...");
						debug(reportParams);
						FhrService.shareFHR(agentCd, reportId, reportParams.custEmailId, null, reportParams.LEAD_ID).then(
							function(res){
								CommonService.hideLoading();
								if(!!res && res=="S")
									navigator.notification.alert("FHR shared successfully.",null,"FHR Report","OK");
								else
									navigator.notification.alert("Could not share FHR. Please try again later.",null,"FHR Report","OK");
							}
						);
					}
				},
				'Confirm Share FHR',
				'Yes, No'
			);
		}
	};

	this.fetchSavedReport = function(reportType, reportId, reportParams){
		debug("reportId : " + reportId);
		var dfd = $q.defer();
		CommonService.readFile(reportId + ".html", "/FromClient/" + ((!!reportType && (reportType=='EKYC'))?'APP':reportType) + "/HTML/").then(
			function(res){
				if(!!res){
					debug("res: " + res.length);
					vr.savedReport = res;
					dfd.resolve(res);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getReport = function(){
		return this.savedReport;
	};

	this.saveReport = function(reportType,reportParams,fileName){
		var dfd = $q.defer();
		var docCapId = CommonService.getRandomNumber();
		if(reportType == 'APP' && !!reportParams){
			CommonService.transaction(db,
				function(tx){
					//changes 4nov
					CommonService.executeSql(tx,"INSERT OR REPLACE INTO LP_DOCUMENT_UPLOAD values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[reportParams.docId,reportParams.agentCd,"FORM",reportParams.appId,reportParams.polNo,fileName + ".html",CommonService.getCurrDate(),0,"N","N",null,docCapId,null],
						function(tx,res){
							debug("LP_DOCUMENT_UPLOAD record inserted Successfully");
							if(!!reportParams.TermData && !!reportParams.TermData.COMBO_ID){
							    var termFileNane = reportParams.agentCd + "_" + reportParams.TermData.REF_APPLICATION_ID + "_" + reportParams.docId;
                                CommonService.executeSql(tx,"INSERT OR REPLACE INTO LP_DOCUMENT_UPLOAD values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[reportParams.docId,reportParams.agentCd,"FORM",reportParams.TermData.REF_APPLICATION_ID,reportParams.TermData.REF_POLICY_NO,termFileNane + ".html",CommonService.getCurrDate(),0,"Y","N",null,CommonService.getRandomNumber(),docCapId],
                                    function(tx,res){
                                        debug("LP_DOCUMENT_UPLOAD for term policy record inserted Successfully");
                                        dfd.resolve(res);
                                    },
                                    function(tx,err){
                                        dfd.resolve(null);
                                    }
                                );
                            }else
							    dfd.resolve(res);
						},
						function(tx,err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				},null
			);
		}
		return dfd.promise;
	};

	this.onConfirm = function(reportType,reportParams,fileName){
		var dfd = $q.defer();
		if(!!reportParams){
			CommonService.saveFile(fileName, ".html", "/FromClient/APP/HTML/", vr.savedReport, "N").then(
				function(saveFileRes){
					if(!!saveFileRes && saveFileRes=="success"){
						debug("inside savereport: "+JSON.stringify(reportParams));
						/*CommonService.insertOrReplaceRecord(db, "lp_document_upload", {"DOC_ID": reportParams.EKYC_DOC_ID, "AGENT_CD": reportParams.AGENT_CD, "DOC_CAT": "FORM", "DOC_CAT_ID": reportParams.SIS_ID, "DOC_NAME": fileName + ".html", "DOC_TIMESTAMP": CommonService.getCurrDate(), "DOC_PAGE_NO": "0", "IS_FILE_SYNCED": "N", "IS_DATA_SYNCED": "N"}).then(
							function(res){
								if(!!res)*/
								leadEKYCConfirmService.postSaveReport(saveFileRes, reportParams.TYPE);
								dfd.resolve(saveFileRes || null);
							/*}
						);*/
					}
					else{
						dfd.resolve(null);
					}
				}
			);
		}
		return dfd.promise;
	};
}]);
