fhrModule.controller('FhrCtrl',['CommonService', 'FhrService', '$state', function(CommonService, FhrService, $state){
	"use strict";
	debug("Inside FHR Controller");
	var fhrm = this;
	this.redirObj = FhrService.redirObjService;

	CommonService.hideLoading();

	this.onTabClick = function(type){
		debug("type: " + type);
		if(type=="mndFields")
			this.gotoMndFieldsTab();
		else if(type=="needId")
			this.gotoNeedIdTab();
		else if(type=="prefPayout")
			this.gotoPrefPayoutTab();
		else if(type=="prodRec")
			this.gotoProdRecTab();
		else if(type=="ffOutput")
			this.gotoFFOutputTab();
	};

}]);