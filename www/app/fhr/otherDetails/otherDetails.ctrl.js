otherDetailsModule.controller('FhrOtherDetailsCtrl', ['$state', '$q', 'CommonService', 'FhrService', 'FhrOtherDetailsService', 'CategoryWiseGoalsCount', function($state, $q, CommonService, FhrService, FhrOtherDetailsService, categoryWiseGoalsCount) {
    debug("Inside Other Details Module");
    CommonService.hideLoading();

    var fhrother = this;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.fhrOtherDetailsData = {};
    debug(this.personalDetails);

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;
    debug(JSON.stringify(this.goalsCountData));

    this.goalsAddedFlag = FhrService.checkIfGoalsAdded(this.goalsCountData);

    this.minRetirementAge = (this.personalDetails.CURRENT_AGE > 40 ? this.personalDetails.CURRENT_AGE : 40);

    //Model values for screen
    this.annualIncome = (!!this.personalDetails.ANN_INCOME_TOTAL ? parseInt(this.personalDetails.ANN_INCOME_TOTAL) : "");
    this.loanEmi = (!!this.personalDetails.LIABILITY_EMI ? parseInt(this.personalDetails.LIABILITY_EMI) : "");
    this.liabilityTotal = (!!this.personalDetails.LIABILITY_TOTAL ? parseInt(this.personalDetails.LIABILITY_TOTAL) : "");
    this.expHouseHold = (!!this.personalDetails.ANN_EXP_HOUSEHOLD ? parseInt(this.personalDetails.ANN_EXP_HOUSEHOLD) : "");
    this.retireAge = (!!this.personalDetails.RETIREMENT_AGE ? parseInt(this.personalDetails.RETIREMENT_AGE) : "");

    // Function to Personal data object from model
    this.setOthersDetailsData = function(form) {
        debug("Setting Other Data Object");
        this.fhrOtherDetailsData.FHR_ID = fhrother.personalDetails.FHR_ID;
        this.fhrOtherDetailsData.AGENT_CD = fhrother.personalDetails.AGENT_CD;
        this.fhrOtherDetailsData.ANNUAL_INCOME = this.annualIncome;
        this.fhrOtherDetailsData.LOAN_EMI = this.loanEmi;
        this.fhrOtherDetailsData.LIABILITY_TOTAL = this.liabilityTotal;
        this.fhrOtherDetailsData.EXP_HOUSEHOLD = this.expHouseHold;
        this.fhrOtherDetailsData.RETIRE_AGE = this.retireAge;
        debug(this.fhrOtherDetailsData);
    };

    // On click of Next Button
    this.onNext = function(form) {
        debug("submit called");
        debug(form.$valid);
        if (form.$valid) {
            debug("Form valid");
            this.setOthersDetailsData(form);
            var dfd = $q.defer();
            FhrOtherDetailsService.onSubmit(JSON.parse(JSON.stringify(this.fhrOtherDetailsData))).then(
                function(res) {
                    if (!!res) {
                        FhrService.setPersonalDetailsDataInFhrMain(res.FHR_ID, res.AGENT_CD).then(function(setRes) {
                            debug("Result after success " + JSON.stringify(setRes));
                            navigator.notification.alert("Other Details updated successfully.", function() {
                                $state.go(FHR_GOALS, { LEADID: setRes[0].LEAD_ID, FHRID: setRes[0].FHR_ID, OPPID: setRes[0].OPP_ID, AGENTCD: setRes[0].AGENT_CD });
                            }, "Other Details", "OK");
                            dfd.resolve(this.fhrOtherDetailsData);
                        });
                    } else {
                        navigator.notification.alert("Other Details updated successfully.", null, "Lead", "OK");
                    }
                }
            );
            return dfd.promise;
        }
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();
	
	/*
	$(".goup").focusin(function() {
		$(".txt-upprwrap").animate({ marginTop: '-70px' }, "fast");
	});
	*/
}]);

otherDetailsModule.service('FhrOtherDetailsService', ['$q', '$state', 'CommonService', function($q, $state, CommonService) {
    var othds = this;

    // Updating Other Details based on FHR Id
    this.updateOtherDetails = function(pageData) {
        debug("Inside insert personal details");
        var params = [];

        params.FHR_ID = pageData.FHR_ID;
        params.AGENT_CD = pageData.AGENT_CD;

        params.ANN_INCOME_TOTAL = pageData.ANNUAL_INCOME;
        params.ANN_EXP_HOUSEHOLD = pageData.EXP_HOUSEHOLD;
        params.LIABILITY_TOTAL = pageData.LIABILITY_TOTAL;
        params.RETIREMENT_AGE = pageData.RETIRE_AGE;
        params.LIABILITY_EMI = pageData.LOAN_EMI;
        params.MODIFIED_DATE = CommonService.getCurrDate();

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "UPDATE LP_FHR_PERSONAL_INF_SCRN_A SET ANN_INCOME_TOTAL=?, ANN_EXP_HOUSEHOLD=?, LIABILITY_TOTAL=?, LIABILITY_EMI=?,RETIREMENT_AGE=?, MODIFIED_DATE=?    WHERE FHR_ID=? and AGENT_CD=?", [params.ANN_INCOME_TOTAL, params.ANN_EXP_HOUSEHOLD, params.LIABILITY_TOTAL, params.LIABILITY_EMI, params.RETIREMENT_AGE, params.MODIFIED_DATE, params.FHR_ID, params.AGENT_CD],
                    function(tx, res) {
                        console.log("updated LP_FHR_PERSONAL_INF_SCRN_A");
                        dfd.resolve(params);
                    },
                    function(tx, err) {
                        console.log("Error in insertOtherDetails.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                console.log("Error in insertOtherDetails(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Submit Other Details
    this.onSubmit = function(pageData) {
        debug("Inside On Submit");
        debug(JSON.stringify(pageData));

        var dfd = $q.defer();
        if (!!pageData.FHR_ID) { // If FHR_ID is generated

            debug("Inside if On Submit");
            // Updating LP_FHR_PERSONAL_INF_SCRN_A table						
            othds.updateOtherDetails(pageData).then(function(res) {
                dfd.resolve(res);
            });
        } else { // If FHR_ID not generated

            debug("Inside On Submit");


        }
        return dfd.promise;
    };

}]);
