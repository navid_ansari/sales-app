// Directive for Common Header
fhrModule.directive('fhrHeader', ['FhrService', function(FhrService) {
    "use strict";
    debug('in fhrHeader');
    return {
        restrict: 'E',
        templateUrl: "fhr/fhrCommon.html",
        link: function(scope, element, attributes) {
            var eventName = 'click';
            var openMenuButton = angular.element(element[0].querySelector('#menu-show-button'));
            openMenuButton.on(eventName, function() {
                FhrService.openMenu();
            });
        }
    };
}]);

// Directive for Left Side Common Personal Details Tab
fhrModule.directive('fhrLeftPersonalDetails', ['$filter', '$timeout', 'CommonService', 'FhrService', 'FhrGoalsMainService', function($filter, $timeout, CommonService, FhrService, FhrGoalsMainService) {
    "use strict";
    debug('in fhrLeftPersonalDetails');
    return {
        restrict: 'E',
        scope: {
            redirObj: '=',
            fullName: '@',
            email: '@',
            contactNumber: '@',
            city: '@',
            dob: '@',
            smoker: '@',
            gender: '@',
            goalsAddedFlag: '@'
        },
        templateUrl: "fhr/personalDetails/leftPersonalDetailsTab.html",
        link: function(scope, element, attrs) {
            debug("Inside link of fhrLeftPersonalDetails");
            var eventName = 'click';
            var personalTabLeft = angular.element(element[0].querySelector('#personal-tab-left'));
            personalTabLeft.on(eventName, function() {
                FhrService.redirObjService.gotoPersonalDetailsTab();
            });

            var pdData = FhrService.getPersonalDetailsDataInFhrMain();

            attrs.goalsCountData = FhrGoalsMainService.getGoalsWiseCount();
			attrs.goalsAddedFlag = (FhrService.checkIfGoalsAdded(attrs.goalsCountData)==null?"N":"Y");

            attrs.fullName = pdData.FIRST_NAME + " " + pdData.LAST_NAME;
            attrs.email = pdData.EMAIL_ID;
            attrs.contactNumber = pdData.MOBILE_NO;
            attrs.city = pdData.CITY;
            attrs.dob = FhrService.getFormattedDate(pdData.BIRTH_DATE);
            attrs.smoker = pdData.SMOKER;
            attrs.gender = pdData.TITLE;
            $timeout(function() {
                scope.$apply();
            }, 500);
        }
    };
}]);