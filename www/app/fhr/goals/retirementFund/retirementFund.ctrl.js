retirementFundModule.controller('FhrRetireFundCtrl',['$q','$state','$stateParams','CommonService','FhrService', 'FhrGoalsMainService', 'FhrRetireFundService', 'ExistingRetireFundGoalsData', 'CategoryWiseGoalsCount',function($q, $state, stateParams, CommonService, FhrService, FhrGoalsMainService, FhrRetireFundService, ExistingRetireFundGoalsData, categoryWiseGoalsCount){
    debug("Inside Retirement Fund Module");
    CommonService.hideLoading();

    var fhrretfund = this;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

	this.fhrRetireFundData = {};
    this.stateData = stateParams;
    this.existingRetireFundGoalsData = ExistingRetireFundGoalsData;
	
    //Setting goals count
	this.goalsCountData = categoryWiseGoalsCount;    

    debug("State params from service are "+JSON.stringify(this.stateData));  
    debug("Existing retirement goal data "+JSON.stringify(this.existingRetireFundGoalsData));  

    //Model values for screen
    this.retireAge = parseInt(this.personalDetails.RETIREMENT_AGE);
    this.incomeNeed = "";
    this.savedAmt = "";
    this.retireIncomeVal = 80;
    this.amountGrowRate = 5;
    this.inflRate = 5;
    this.RetireCrePlan = "Retirement";

    // Function to set Retirement Fund from model
    this.setRetirementFundData = function(form){
        debug("Setting Retirement fund goals data Object");

        this.fhrRetireFundData.FHR_ID = this.stateData.FHRID;
        this.fhrRetireFundData.AGENT_CD = this.stateData.AGENTCD;
        this.fhrRetireFundData.GOAL_DESC = this.RetireCrePlan;
        this.fhrRetireFundData.GOAL_FIRST_NAME = (!!this.personalDetails.FIRST_NAME?this.personalDetails.FIRST_NAME:"");
        this.fhrRetireFundData.GOAL_MIDDLE_NAME = (!!this.personalDetails.MIDDLE_NAME?this.personalDetails.MIDDLE_NAME:"");
        this.fhrRetireFundData.GOAL_LAST_NAME = (!!this.personalDetails.LAST_NAME?this.personalDetails.LAST_NAME:"");
        this.fhrRetireFundData.GOAL_CURRENT_AGE = this.personalDetails.CURRENT_AGE; 

        // Data from form input
        this.fhrRetireFundData.GOAL_TARGET_AGE = this.retireIncomeVal;
        this.fhrRetireFundData.RETIREMENT_AGE = this.retireAge;
        this.fhrRetireFundData.PRESENT_COST = this.incomeNeed;
        this.fhrRetireFundData.PRESENT_SAVINGS = this.savedAmt;
        this.fhrRetireFundData.ROI_RATE = this.amountGrowRate;
        this.fhrRetireFundData.INFLATION_RATE = this.inflRate;
        this.fhrRetireFundData.GOAL_TARGET_DUARTION = this.fhrRetireFundData.GOAL_TARGET_AGE - this.fhrRetireFundData.RETIREMENT_AGE; 
        this.fhrRetireFundData.MODIFIED_DATE = CommonService.getCurrDate(); 

        /**
         *  Retirement Calculation Logic
         *  Current Age	:	A1
         *  Retirement Age	:	A2
         *  Retirement Income needed upto	:	A3
         *  Monthly Income needed as on Today	:	A
         *  Rate at which Amount will grow	:	R1
         *  Inflation  Rate	:	R2
         *  Amount already saved (Single corpus)	:	B
         *  Term1=Retirement Income Neede Upto - Retirement age	:	T1=A3-A2
         *  TErm2  = Retirement Age - Current Age	:	T2=A2-A1
         *  FV of Retirement Income	:	E=12 x A x (1+R2)^(T2)
         *  FV of Current Saving	:	F = B x (1+R1)^(T2)
         *  Retirement Corpus	:	G = Retirement Corpus = E x ((1-(1+i)^(-T1))/d)
         *  Real Rate of Return post retirement 
         *  (Assuming Savings rate is 7% andInflation is 5%)	:	i=((1+interest rate)/(1+inflation rate))-1 = ((1+7%)/(1+5%))
         *  d =1- (1+i)^(-1)
         *  Gap Analysis - Retirement Corpus	:	H = Retirement Corpus or G - F
         *  Projected Annual Income Needed at retirement	:	E
         *  Corpus needed	:	H
         *  Projected income @ age "retirementage to be mentioned"	:	E
         *  Years to Goal	:	A3-A2
        */

        /**
         *  T1 = Retirement Income Neede Upto - Retirement age
         *  T2 = Retirement age - Current age
         */
        var T1 = this.fhrRetireFundData.GOAL_TARGET_AGE - this.fhrRetireFundData.RETIREMENT_AGE;
        var T2 = this.fhrRetireFundData.RETIREMENT_AGE - this.fhrRetireFundData.GOAL_CURRENT_AGE;
        debug("T1 "+T1);
        debug("T2 "+T2);

        // Rates Percentage
        var roiRatePercent = (this.fhrRetireFundData.ROI_RATE/100);
        var infRatePercent = (this.fhrRetireFundData.INFLATION_RATE/100);

        // Fixed Rates for calculation of I
        var roiRateFixedPercent = (7/100);
        var infRateFixedPercent = (5/100);

        debug(roiRatePercent +" |-------| "+ infRatePercent);
        debug(roiRateFixedPercent +" |-------| "+ infRateFixedPercent);

        /**
         *  I - Real Rate of Return post retirement (Assuming Savings rate is 7% andInflation is 5%)
         *  I=((1+interest rate)/(1+inflation rate))-1
         */
        var I = ((1+roiRateFixedPercent)/(1+infRateFixedPercent))-1;
        debug("I :"+ I);

        /**
         * Future Cost Calculation
         * 
         */
        this.fhrRetireFundData.FUTURE_COST = (12 * parseInt(this.fhrRetireFundData.PRESENT_COST) * Math.pow((1 + infRatePercent),T2));

        //Future Required Corpus Cost Calculation - [FUTURE_COST x ((1-(1.01904762)^(-T1))/0.0186916)]
        var part1 = 1-(Math.pow(1+(I),(-T1)));
        var d = 1-(Math.pow(1+(I),(-1)));
        debug("part1 "+part1);
        debug("d "+d);

        this.fhrRetireFundData.FUTURE_REQIRED_CORPUS = this.fhrRetireFundData.FUTURE_COST * (part1/d);

        //Future savings Calculation
        this.fhrRetireFundData.FUTURE_SAVINGS_VALUE = parseInt(this.fhrRetireFundData.PRESENT_SAVINGS) * Math.pow((1 + roiRatePercent),T2);

        //Goal gap amount calculation
        this.fhrRetireFundData.GOAL_GAP_AMOUNT = (this.fhrRetireFundData.FUTURE_REQIRED_CORPUS - this.fhrRetireFundData.FUTURE_SAVINGS_VALUE);

        // Converting values upto 2 decimal formats
        this.fhrRetireFundData.FUTURE_COST = this.fhrRetireFundData.FUTURE_COST.toFixed(2);
        this.fhrRetireFundData.FUTURE_REQIRED_CORPUS = this.fhrRetireFundData.FUTURE_REQIRED_CORPUS.toFixed(2);
        this.fhrRetireFundData.FUTURE_SAVINGS_VALUE = this.fhrRetireFundData.FUTURE_SAVINGS_VALUE.toFixed(2);
        this.fhrRetireFundData.GOAL_GAP_AMOUNT = this.fhrRetireFundData.GOAL_GAP_AMOUNT.toFixed(2)

        // If Shortfall less than 0, then show 0 only
        if (this.fhrRetireFundData.GOAL_GAP_AMOUNT < 0) {
            this.fhrRetireFundData.GOAL_GAP_AMOUNT = 0;
        }
        
        debug("FUTURE_COST " + this.fhrRetireFundData.FUTURE_COST);
        debug("FUTURE_REQIRED_CORPUS " + this.fhrRetireFundData.FUTURE_REQIRED_CORPUS);
        debug("FUTURE_SAVINGS_VALUE " + this.fhrRetireFundData.FUTURE_SAVINGS_VALUE);
        debug("GOAL_GAP_AMOUNT " + this.fhrRetireFundData.GOAL_GAP_AMOUNT);

        var dfd = $q.defer();
        FhrGoalsMainService.getPlanURNNos(this.fhrRetireFundData).then(function(res){
            debug("Res of getPlanURNNos "+JSON.stringify(res));
            fhrretfund.fhrRetireFundData.SUGEST_PLAN1_URNNO = res.SUGGESTED_PRODUCT1_UINNO;
            fhrretfund.fhrRetireFundData.SUGEST_PLAN2_URNNO = res.SUGGESTED_PRODUCT2_UINNO;
            fhrretfund.fhrRetireFundData.SUGEST_PLAN3_URNNO = res.SUGGESTED_PRODUCT3_UINNO;
            dfd.resolve(fhrretfund.fhrRetireFundData);
        });
        return dfd.promise;
    };

    // On click of Next Button
    this.onNext = function(form){
        debug("submit called");     
        debug(form.$valid);
        if (form.$valid) {
            debug("Form valid");
            this.setRetirementFundData(form).then(
                function(){
                    debug("After setting setRetirementFundData");
                    var dfd = $q.defer();
                    FhrRetireFundService.onSubmit(JSON.parse(JSON.stringify(fhrretfund.fhrRetireFundData))).then(
                        function(res){
                            if(!!res){
                                debug("Result after success "+JSON.stringify(res));
                                navigator.notification.alert("Retirement Fund  Goal added successfully.",function(){
                                    $state.go(FHR_GOALS_SUCCESS, {GOALTYPE: "Retirement", LEADID: fhrretfund.stateData.LEADID, FHRID: fhrretfund.stateData.FHRID, OPPID: fhrretfund.stateData.OPPID, AGENTCD: fhrretfund.stateData.AGENTCD});
                                },"Retirement Fund Goal","OK");
                                dfd.resolve(fhrretfund.fhrRetireFundData);
                            }
                            else{
                                navigator.notification.alert("Retirement Fund Goal addition failed.",null,"Retirement Fund Goal","OK");
                            }
                        }
                    );
                    return dfd.promise;
				}
			);	                    
        }
    };

    // On click of Prev button
    this.onPrev = function(){
        fhrretfund.redirObj.gotoGoalsTab();
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();
    
}]);

retirementFundModule.service('FhrRetireFundService',['$q', '$state', 'CommonService', function($q, $state, CommonService){
    var fhrretfundsc = this;

    // Function to Insert Wealth Creation Goals Data
    this.insertRetirementGoals=function(pageData){
        debug("Inside insert retirement fund details");
        
        var dfd = $q.defer();
        CommonService.transaction(db,
        function(tx){
            CommonService.executeSql(tx,"INSERT INTO LP_FHR_GOAL_RETIREMENT_SCRN_E (FHR_ID,AGENT_CD,GOAL_DESC,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,GOAL_CURRENT_AGE,GOAL_TARGET_DUARTION,GOAL_TARGET_AGE,PRESENT_COST,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,FUTURE_COST,FUTURE_REQIRED_CORPUS,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    [pageData.FHR_ID,pageData.AGENT_CD,pageData.GOAL_DESC,pageData.GOAL_FIRST_NAME,pageData.GOAL_MIDDLE_NAME,pageData.GOAL_LAST_NAME,pageData.GOAL_CURRENT_AGE,pageData.GOAL_TARGET_DUARTION,pageData.GOAL_TARGET_AGE,pageData.PRESENT_COST,pageData.INFLATION_RATE,pageData.PRESENT_SAVINGS,pageData.ROI_RATE,pageData.SUGEST_PLAN1_URNNO,pageData.SUGEST_PLAN2_URNNO,pageData.SUGEST_PLAN3_URNNO,pageData.FUTURE_COST,pageData.FUTURE_REQIRED_CORPUS,pageData.FUTURE_SAVINGS_VALUE,pageData.GOAL_GAP_AMOUNT],
                function(tx,res){
                    debug("inserted into LP_FHR_GOAL_RETIREMENT_SCRN_E");
                    dfd.resolve(pageData);
                },
                function(tx,err){
                    debug("Error in insertRetirementGoals.transaction(): " + err.message);
                    dfd.resolve(null);
                }
            );
        },
        function(err){
            debug("Error in insertRetirementGoals(): " + err.message);
            dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    // Submit RetirementFund Goals
    this.onSubmit = function(pageData){
        debug("Inside On Submit");

        var dfd = $q.defer();
        if(!!pageData.FHR_ID){ // If FHR_ID is generated
            debug("Inside if On Submit");
            debug(pageData);

            // Inserting into LP_FHR_GOAL_RETIREMENT_SCRN_E table                       
            fhrretfundsc.insertRetirementGoals(pageData).then(function(res){                      
                dfd.resolve(res);
            });                                                                     

        }else{  // If FHR_ID not generated
            debug("Inside else On Submit"); 
            dfd.resolve(null);
        }
        return dfd.promise;
    };
}]);
