wealthCreationModule.controller('FhrWealthCreationCtrl', ['$q', '$state', '$stateParams', 'CommonService', 'FhrService', 'FhrGoalsMainService', 'FhrWealthCreationService', 'ExistingWealthGoalsData', 'CategoryWiseGoalsCount', function($q, $state, stateParams, CommonService, FhrService, FhrGoalsMainService, FhrWealthCreationService, ExistingWealthGoalsData, categoryWiseGoalsCount) {
    debug("Inside Wealth Creation Module");
    CommonService.hideLoading();

    var fhrweacreat = this;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();
    this.wealthPlanList = FhrGoalsMainService.getWealthPlanList();

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.fhrWealthCreationData = {};
    this.stateData = stateParams;
    this.existingWealthGoalsData = ExistingWealthGoalsData;
    this.relationCode = FhrService.getFhrRelationList("Self") || "SL";
    debug("Relation Code is " + this.relationCode);

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;

    debug("State params from service are " + JSON.stringify(this.stateData));
    debug("Existing Wealth Goals are " + JSON.stringify(this.existingWealthGoalsData));

    //Model values for screen
    this.wealthCrePlan = "";
    this.currCostCurrGoal = 0;
    this.currSaving = 0;
    this.moneyReqAge = parseInt(this.personalDetails.CURRENT_AGE);
    this.growthRateSaving = 5;
    this.inflationRate = 5;
    this.currAgePersonalInfo = parseInt(this.personalDetails.CURRENT_AGE);

    this.liabilityTotal = (!!this.personalDetails.LIABILITY_TOTAL ? parseInt(this.personalDetails.LIABILITY_TOTAL) : "");
    this.expHouseHold = (!!this.personalDetails.ANN_EXP_HOUSEHOLD ? parseInt(this.personalDetails.ANN_EXP_HOUSEHOLD) : "");

    this.minAgeWhenMoneyReq = parseInt(this.personalDetails.CURRENT_AGE) + parseInt(10); // Minimum should be Proposer Current age + 10 Years
    this.moneyReqAge = this.minAgeWhenMoneyReq;

    // Function to set Wealth Creation from model
    this.setWealthCreationData = function(form) {
        debug("Setting Wealth Creation goals data Object");

        this.fhrWealthCreationData.FHR_ID = this.stateData.FHRID;
        this.fhrWealthCreationData.AGENT_CD = this.stateData.AGENTCD;
        this.fhrWealthCreationData.GOAL_DESC_SUB = this.wealthCrePlan;
        this.fhrWealthCreationData.GOAL_DESC = "Wealth";
        this.fhrWealthCreationData.GOAL_FIRST_NAME = (!!this.personalDetails.FIRST_NAME ? this.personalDetails.FIRST_NAME : "");
        this.fhrWealthCreationData.GOAL_MIDDLE_NAME = (!!this.personalDetails.MIDDLE_NAME ? this.personalDetails.MIDDLE_NAME : "");
        this.fhrWealthCreationData.GOAL_LAST_NAME = (!!this.personalDetails.LAST_NAME ? this.personalDetails.LAST_NAME : "");
        this.fhrWealthCreationData.GOAL_CURRENT_AGE = this.personalDetails.CURRENT_AGE;
        this.fhrWealthCreationData.RETIREMENT_AGE = this.personalDetails.RETIREMENT_AGE;
        this.fhrWealthCreationData.RELATION = "Self";
        this.fhrWealthCreationData.RELATION_CODE = this.relationCode;

        // Data from form input
        this.fhrWealthCreationData.PRESENT_COST = this.currCostCurrGoal;
        this.fhrWealthCreationData.GOAL_TARGET_AGE = this.moneyReqAge;
        this.fhrWealthCreationData.PRESENT_SAVINGS = this.currSaving;
        this.fhrWealthCreationData.ROI_RATE = this.growthRateSaving;
        this.fhrWealthCreationData.INFLATION_RATE = this.inflationRate;
        this.fhrWealthCreationData.MODIFIED_DATE = CommonService.getCurrDate();
        this.fhrWealthCreationData.ANN_EXP_TOTAL = parseInt(this.expHouseHold) + parseInt(this.liabilityTotal);

        // GOAL_GAP_AMOUNT Calculation
        var roiRatePercent = (this.fhrWealthCreationData.ROI_RATE / 100);
        var infRatePercent = (this.fhrWealthCreationData.INFLATION_RATE / 100);
        debug(roiRatePercent + " |-------| " + infRatePercent);

        var ageDiff = parseInt(this.fhrWealthCreationData.GOAL_TARGET_AGE) - parseInt(this.fhrWealthCreationData.GOAL_CURRENT_AGE);
        debug(ageDiff);

        // Future cost calculation
        this.fhrWealthCreationData.FUTURE_COST = (parseInt(this.fhrWealthCreationData.PRESENT_COST) * Math.pow((1 + infRatePercent), ageDiff));

        // Future savings value calculation
        this.fhrWealthCreationData.FUTURE_SAVINGS_VALUE = (parseInt(this.fhrWealthCreationData.PRESENT_SAVINGS) * Math.pow((1 + roiRatePercent), ageDiff));

        // Goal gap amount calculation
        this.fhrWealthCreationData.GOAL_GAP_AMOUNT = (this.fhrWealthCreationData.FUTURE_COST - this.fhrWealthCreationData.FUTURE_SAVINGS_VALUE);

        // Converting values upto 2 decimal formats
        this.fhrWealthCreationData.FUTURE_COST = this.fhrWealthCreationData.FUTURE_COST.toFixed(2);
        this.fhrWealthCreationData.FUTURE_SAVINGS_VALUE = this.fhrWealthCreationData.FUTURE_SAVINGS_VALUE.toFixed(2);
        this.fhrWealthCreationData.GOAL_GAP_AMOUNT = this.fhrWealthCreationData.GOAL_GAP_AMOUNT.toFixed(2);

        // If Shortfall less than 0, then show 0 only
        if (this.fhrWealthCreationData.GOAL_GAP_AMOUNT < 0) {
            this.fhrWealthCreationData.GOAL_GAP_AMOUNT = 0;
        }

        var dfd = $q.defer();
        FhrGoalsMainService.getPlanURNNos(this.fhrWealthCreationData).then(function(res) {
            debug("Res of getPlanURNNos " + JSON.stringify(res));
            fhrweacreat.fhrWealthCreationData.SUGEST_PLAN1_URNNO = res.SUGGESTED_PRODUCT1_UINNO;
            fhrweacreat.fhrWealthCreationData.SUGEST_PLAN2_URNNO = res.SUGGESTED_PRODUCT2_UINNO;
            fhrweacreat.fhrWealthCreationData.SUGEST_PLAN3_URNNO = res.SUGGESTED_PRODUCT3_UINNO;
            debug(fhrweacreat.fhrWealthCreationData);
            dfd.resolve(fhrweacreat.fhrWealthCreationData);
        });
        return dfd.promise;
    };

    // On click of Next Button
    this.onNext = function(form) {
        debug("submit called");
        debug(form.$valid);

        if (form.$valid) {
            debug("Form valid");
            this.setWealthCreationData(form).then(
                function() {
                    debug("After setting setWealthCreationData");
                    var dfd = $q.defer();
                    FhrWealthCreationService.onSubmit(JSON.parse(JSON.stringify(fhrweacreat.fhrWealthCreationData))).then(
                        function(res) {
                            if (!!res) {
                                debug("Result after success " + JSON.stringify(res));
                                navigator.notification.alert("Wealth Goal added successfully.", function() {
                                    $state.go(FHR_GOALS_SUCCESS, { GOALTYPE: "Wealth", LEADID: fhrweacreat.stateData.LEADID, FHRID: fhrweacreat.stateData.FHRID, OPPID: fhrweacreat.stateData.OPPID, AGENTCD: fhrweacreat.stateData.AGENTCD });
                                }, "Wealth Creation Goal", "OK");
                                dfd.resolve(fhrweacreat.fhrWealthCreationData);
                            } else {
                                navigator.notification.alert("Wealth Goal addition failed.", null, "Wealth Goal", "OK");
                            }
                        }
                    );
                    return dfd.promise;
                }
            );
        }
    };

    // On click of Prev button
    this.onPrev = function() {
        fhrweacreat.redirObj.gotoGoalsTab();
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();

}]);

wealthCreationModule.service('FhrWealthCreationService', ['$q', '$state', 'CommonService', function($q, $state, CommonService) {
    var fhrweacreasc = this;

    // Function to get last RELATION_SEQ_NO related to Child Goal
    this.getLastRelSeqNoRelatedToFhr = function(fhdId, agendCd) {
        debug("Inside getLastRelSeqNoRelatedToFhr");
        var dfd = $q.defer();
        try {
            var query = "select (RELATION_SEQ_NO+1) RELATION_SEQ_NO from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? order by RELATION_SEQ_NO desc limit 1";
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [fhdId, agendCd],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                debug("Resolved with data getLastRelSeqNoRelatedToFhr");
                                dfd.resolve(res.rows.item(0).RELATION_SEQ_NO);
                            } else {
                                debug("Resolved without data getLastRelSeqNoRelatedToFhr");
                                dfd.resolve(1);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(null);
                }
            );
        } catch (ex) {
            dfd.resolve(null);
        }
        debug("Exiting getLastRelSeqNoRelatedToFhr");
        return dfd.promise;
    };

    // Function to Insert Wealth Creation Goals Data
    this.insertWealthCreationGoals = function(pageData) {
        debug("Inside insert wealth goals details");

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT INTO LP_FHR_GOAL_WEALTH_SCRN_D (FHR_ID,AGENT_CD,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,PRESENT_COST,GOAL_TARGET_AGE,PRESENT_SAVINGS,ROI_RATE,INFLATION_RATE,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,MODIFIED_DATE, RELATION_SEQ_NO,RELATION,RELATION_CODE) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [pageData.FHR_ID, pageData.AGENT_CD, pageData.GOAL_DESC_SUB, pageData.GOAL_CURRENT_AGE, pageData.GOAL_FIRST_NAME, pageData.GOAL_MIDDLE_NAME, pageData.GOAL_LAST_NAME, pageData.FUTURE_COST, pageData.FUTURE_SAVINGS_VALUE, pageData.GOAL_GAP_AMOUNT, pageData.PRESENT_COST, pageData.GOAL_TARGET_AGE, pageData.PRESENT_SAVINGS, pageData.ROI_RATE, pageData.INFLATION_RATE, pageData.SUGEST_PLAN1_URNNO, pageData.SUGEST_PLAN2_URNNO, pageData.SUGEST_PLAN3_URNNO, pageData.MODIFIED_DATE, pageData.RELATION_SEQ_NO, pageData.RELATION, pageData.RELATION_CODE],
                    function(tx, res) {
                        debug("inserted into LP_FHR_GOAL_WEALTH_SCRN_D");
                        dfd.resolve(pageData);
                    },
                    function(tx, err) {
                        debug("Error in insertWealthCreationGoals.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                debug("Error in insertWealthCreationGoals(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Submit Wealth Creation Goals
    this.onSubmit = function(pageData) {
        debug("Inside On Submit");
        var dfd = $q.defer();
        if (!!pageData.FHR_ID) { // If FHR_ID is generated
            debug("Inside if On Submit");
            debug(pageData);

            // Inserting into LP_FHR_GOAL_WEALTH_SCRN_D table      
            fhrweacreasc.getLastRelSeqNoRelatedToFhr(pageData.FHR_ID, pageData.AGENT_CD).then(function(relSeqNo) {
                debug("RELATION_SEQ_NO" + relSeqNo);
                pageData.RELATION_SEQ_NO = relSeqNo;
                fhrweacreasc.insertWealthCreationGoals(pageData).then(function(res) {
                    dfd.resolve(res);
                });
            });

        } else { // If FHR_ID not generated
            debug("Inside else On Submit");
            dfd.resolve(null);
        }
        return dfd.promise;
    };
}]);