protectionModule.controller('FhrProtectionCtrl', ['$q', '$state', '$stateParams', 'CommonService', 'FhrService', 'FhrGoalsMainService', 'FhrProtectionService', 'ExistingProtectionGoalsData', 'CategoryWiseGoalsCount', function($q, $state, stateParams, CommonService, FhrService, FhrGoalsMainService, FhrProtectionService, ExistingProtectionGoalsData, categoryWiseGoalsCount) {
    debug("Inside Protection Module");
    CommonService.hideLoading();

    var fhrprot = this;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.fhrProtectionData = {};
    this.stateData = stateParams;
    this.existingProtectionGoalsData = ExistingProtectionGoalsData;

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;

    debug("State params from service are " + JSON.stringify(this.stateData));
    debug("Existing protection goal data " + JSON.stringify(this.existingProtectionGoalsData));

    //Model values for screen
    this.insInfFlag = "Y";
    this.inflRate = 5;
    this.exInsCover = 0;
    this.expMonth = 0; // Monthly Expenses(Excluded Loan) + Loan EMI
    this.currInterest = 5;

    this.goalPlan = "Protection";

    this.retireAge = (!!this.personalDetails.RETIREMENT_AGE ? parseInt(this.personalDetails.RETIREMENT_AGE) : "");
    this.liabilityTotal = (!!this.personalDetails.LIABILITY_TOTAL ? parseInt(this.personalDetails.LIABILITY_TOTAL) : "");
    this.loanEmi = (!!this.personalDetails.LIABILITY_EMI ? parseInt(this.personalDetails.LIABILITY_EMI) : "");
    this.expHouseHold = (!!this.personalDetails.ANN_EXP_HOUSEHOLD ? parseInt(this.personalDetails.ANN_EXP_HOUSEHOLD) : "");
    this.annualIncome = (!!this.personalDetails.ANN_INCOME_TOTAL ? parseInt(this.personalDetails.ANN_INCOME_TOTAL) : "");

    // Manage monthly family expense = Monthly Expenses(Excluded Loan) + Loan EMI
    this.expMonth = parseInt(this.expHouseHold) + parseInt(this.loanEmi);

    // Function to set Protection Goals from model
    this.setProtectionGoalsData = function(form) {
        debug("Setting Protection goals data Object");

        this.fhrProtectionData.FHR_ID = this.stateData.FHRID;
        this.fhrProtectionData.AGENT_CD = this.stateData.AGENTCD;
        this.fhrProtectionData.GOAL_DESC = this.goalPlan;
        this.fhrProtectionData.GOAL_FIRST_NAME = (!!this.personalDetails.FIRST_NAME ? this.personalDetails.FIRST_NAME : "");
        this.fhrProtectionData.GOAL_MIDDLE_NAME = (!!this.personalDetails.MIDDLE_NAME ? this.personalDetails.MIDDLE_NAME : "");
        this.fhrProtectionData.GOAL_LAST_NAME = (!!this.personalDetails.LAST_NAME ? this.personalDetails.LAST_NAME : "");
        this.fhrProtectionData.GOAL_CURRENT_AGE = this.personalDetails.CURRENT_AGE;
        this.fhrProtectionData.GOAL_TARGET_AGE = this.personalDetails.RETIREMENT_AGE;
        this.fhrProtectionData.PREMIUM_PAY_MODE = "OL";

        // Data from form input
        this.fhrProtectionData.EX_INSURENCE_COVER = this.exInsCover;
        this.fhrProtectionData.ROI_RATE = this.currInterest;
        this.fhrProtectionData.INFLATION_RATE = this.inflRate;
        this.fhrProtectionData.INFLATION_FLAG = this.insInfFlag;
        this.fhrProtectionData.MODIFIED_DATE = CommonService.getCurrDate();
        this.fhrProtectionData.MANAGE_MONTHLY_EXP = parseInt(this.expHouseHold);

        /**
         * [Calculation logic of REQ_INSURANCE_PROTECTION, PROVISION_GAP_EMERGENCY_FUND, GOAL_GAP_AMOUNT]
         * Current  Age	:	A1
         * Retirement Age	:	A2
         * Amount needed by family to manage basic expense as on today	:	X
         * Inflation %	:	R2
         * Current Interest Rate %	:	R1
         * Existing Insurance Cover	:	Y
         * Monthly Expenses(Excluded Loan)	:	A=X-L
         * Existing O/S Loan Liabilities	:	B
         * Term	:	T=A2-A1
         * Real Rate of Return (with inflation else i=Current Interest Rate)	:	i=((1+interest rate)/(1+inflation rate))-1
         * PV Factor (with inflation or real rate)	:	PV1=((1-(1+i)^(-Term))/((1-(1+i)^(-1)))
         * PV Factor (without inflation or real rate)	:	PV2=((1-(1+i)^(-Term))/((1-(1+i)^(-1)))
         * Loan EMI- new field not provided earlier	:	L
         * Yearly Expense	:	C=12*A
         * Insurance Required (With Inflation)	:	I1=C*PV1
         * Insurance Required (Without Inflation)	:	I2= C*PV2
         * Gap (With Inflation) = Insurance Required + Existing O/S Liabilities - Existing Insurance Cover	:	G1=I1+B-Y
         * Gap (Without Inflation) = Insurance Required + Existing O/S Liabilities - Existing Insurance Cover	:	G2=I2+B-Y
         * Protection cover required to fund family expenses	:	I1/I2
         * Years to Goal	:	A2-A1
         */

        var rate = 0;
        var pvFactor = 0;

        // Term Calculation
        var term = (this.fhrProtectionData.GOAL_TARGET_AGE - this.fhrProtectionData.GOAL_CURRENT_AGE); // Retirement age - Current age
        debug("Term " + term);

        var roiRatePercent = (this.fhrProtectionData.ROI_RATE / 100);
        var infRatePercent = (this.fhrProtectionData.INFLATION_RATE / 100);
        debug(roiRatePercent + " |-------| " + infRatePercent);

        // Rate calculation based on Inflation required flag
        if (this.insInfFlag == "Y") {
            rate = (((1 + roiRatePercent) / (1 + infRatePercent)) - 1);
        } else {
            rate = (roiRatePercent);
        }
        debug("Rate  " + rate);

        // PV Factor Calculation
        var part1 = 1 - (Math.pow((1 + rate), (-term)));
        debug("Part1 Val  " + part1);

        var divideVal = 1 - (Math.pow((1 + rate), (-1)));
        debug("Divide Val  " + divideVal);

        pvFactor = (part1 / divideVal);
        pvFactor = (isNaN(pvFactor) ? 0 : pvFactor);
        debug("PV  " + pvFactor.toFixed(2));

        // REQ_INSURANCE_PROTECTION Calculation
        this.fhrProtectionData.REQ_INSURANCE_PROTECTION = (this.fhrProtectionData.MANAGE_MONTHLY_EXP * 12 * pvFactor);

        // PROVISION_GAP_EMERGENCY_FUND Calculation
        this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND = parseFloat(this.fhrProtectionData.REQ_INSURANCE_PROTECTION) + parseInt(this.personalDetails.LIABILITY_TOTAL) - parseInt(this.fhrProtectionData.EX_INSURENCE_COVER);

        // GOAL_GAP_AMOUNT Calculation  - To be checked - Not required
        this.fhrProtectionData.GOAL_GAP_AMOUNT = parseFloat(this.fhrProtectionData.REQ_INSURANCE_PROTECTION) + parseInt(this.personalDetails.LIABILITY_TOTAL) - parseInt(this.fhrProtectionData.EX_INSURENCE_COVER);

        // Converting values upto 2 decimal formats
        this.fhrProtectionData.REQ_INSURANCE_PROTECTION = this.fhrProtectionData.REQ_INSURANCE_PROTECTION.toFixed(2);
        this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND = this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND.toFixed(2);
        this.fhrProtectionData.GOAL_GAP_AMOUNT = this.fhrProtectionData.GOAL_GAP_AMOUNT.toFixed(2);

        // If Shortfall less than 0, then show 0 only
        if (this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND < 0) {
            this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND = 0;
        }

        debug("REQ_INSURANCE_PROTECTION " + this.fhrProtectionData.REQ_INSURANCE_PROTECTION);
        debug("PROVISION_GAP_EMERGENCY_FUND " + this.fhrProtectionData.PROVISION_GAP_EMERGENCY_FUND);
        debug("GOAL_GAP_AMOUNT " + this.fhrProtectionData.GOAL_GAP_AMOUNT);

        var dfd = $q.defer();
        FhrGoalsMainService.getPlanURNNos(this.fhrProtectionData).then(function(res) {
            debug("Res of getPlanURNNos " + JSON.stringify(res));
            fhrprot.fhrProtectionData.SUGEST_PLAN1_URNNO = res.SUGGESTED_PRODUCT1_UINNO;
            fhrprot.fhrProtectionData.SUGEST_PLAN2_URNNO = res.SUGGESTED_PRODUCT2_UINNO;
            fhrprot.fhrProtectionData.SUGEST_PLAN3_URNNO = res.SUGGESTED_PRODUCT3_UINNO;
            dfd.resolve(fhrprot.fhrProtectionData);
        });
        return dfd.promise;
    };

    // On click of Next Button
    this.onNext = function(form) {
        debug("submit called");
        debug(form.$valid);
        if (form.$valid) {
            debug("Form valid");
            this.setProtectionGoalsData(form).then(
                function() {
                    debug("After setting setProtectionGoalsData");
                    var dfd = $q.defer();
                    FhrProtectionService.onSubmit(JSON.parse(JSON.stringify(fhrprot.fhrProtectionData))).then(
                        function(res) {
                            if (!!res) {
                                debug("Result after success " + JSON.stringify(res));
                                navigator.notification.alert("Protection Goal added successfully.", function() {
                                    $state.go(FHR_GOALS_SUCCESS, { GOALTYPE: "Protection", LEADID: fhrprot.stateData.LEADID, FHRID: fhrprot.stateData.FHRID, OPPID: fhrprot.stateData.OPPID, AGENTCD: fhrprot.stateData.AGENTCD });
                                }, "Protection Goal", "OK");
                                dfd.resolve(fhrprot.fhrProtectionData);
                            } else {
                                navigator.notification.alert("Protection Goal addition failed.", null, "Child Goal", "OK");
                            }
                        }
                    );
                    return dfd.promise;
                }
            );
        }
    };

    // On click of Prev button
    this.onPrev = function() {
        fhrprot.redirObj.gotoGoalsTab();
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();

}]);

protectionModule.service('FhrProtectionService', ['$q', '$state', 'CommonService', function($q, $state, CommonService) {
    var fhrprotgsc = this;

    // Function to Insert Protection Goals Data
    this.insertProtectionGoals = function(pageData) {
        debug("Inside insert protection goals details");

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT INTO LP_FHR_GOAL_LIVING_STD_SCRN_F (FHR_ID,AGENT_CD,GOAL_DESC,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,GOAL_CURRENT_AGE,ANN_EXP_TOTAL,REQ_INSURANCE_PROTECTION,PROVISION_GAP_EMERGENCY_FUND,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,MODIFIED_DATE,INFLATION_RATE,INFLATION_FLAG,ROI_RATE,SUM_ASSURED,PREMIUM_PAY_MODE) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [pageData.FHR_ID, pageData.AGENT_CD, pageData.GOAL_DESC, pageData.GOAL_FIRST_NAME, pageData.GOAL_MIDDLE_NAME, pageData.GOAL_LAST_NAME, pageData.GOAL_CURRENT_AGE, pageData.ANN_EXP_TOTAL, pageData.REQ_INSURANCE_PROTECTION, pageData.PROVISION_GAP_EMERGENCY_FUND, pageData.GOAL_GAP_AMOUNT, pageData.SUGEST_PLAN1_URNNO, pageData.SUGEST_PLAN2_URNNO, pageData.SUGEST_PLAN3_URNNO, pageData.MODIFIED_DATE, pageData.INFLATION_RATE, pageData.INFLATION_FLAG, pageData.ROI_RATE, pageData.EX_INSURENCE_COVER, pageData.PREMIUM_PAY_MODE],
                    function(tx, res) {
                        debug("inserted into LP_FHR_GOAL_LIVING_STD_SCRN_F");
                        dfd.resolve(pageData);
                    },
                    function(tx, err) {
                        debug("Error in insertProtectionGoals.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                debug("Error in insertProtectionGoals(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Submit Protection Goals
    this.onSubmit = function(pageData) {
        debug("Inside On Submit");

        var dfd = $q.defer();
        if (!!pageData.FHR_ID) { // If FHR_ID is generated
            debug("Inside if On Submit");
            debug(pageData);

            // Inserting into LP_FHR_GOAL_LIVING_STD_SCRN_F table						
            fhrprotgsc.insertProtectionGoals(pageData).then(function(res) {
                dfd.resolve(res);
            });

        } else { // If FHR_ID not generated
            debug("Inside else On Submit");
            dfd.resolve(null);
        }
        return dfd.promise;
    };
}]);