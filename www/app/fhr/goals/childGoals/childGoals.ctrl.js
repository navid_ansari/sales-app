childGoalsModule.controller('FhrChildGoalsCtrl', ['$q', '$state', '$stateParams', 'CommonService', 'FhrService', 'FhrGoalsMainService', 'FhrChildGoalsService', '$filter', 'ExistingChildGoalsData', 'CategoryWiseGoalsCount', function($q, $state, stateParams, CommonService, FhrService, FhrGoalsMainService, FhrChildGoalsService, $filter, ExistingChildGoalsData, categoryWiseGoalsCount) {
    debug("Inside Child Goals Module");
    CommonService.hideLoading();

    var fhrchlg = this;

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.fhrChildGoalsData = {};
    this.stateData = stateParams;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();
    this.existingChildGoalsData = ExistingChildGoalsData;
    this.relationCode = FhrService.getFhrRelationList("Child") || "CH";
    debug("Relation Code is " + this.relationCode);

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;
    debug(JSON.stringify(this.goalsCountData));

    debug("State params from service are " + JSON.stringify(this.stateData));
    debug("Existing Child Goals are " + JSON.stringify(this.existingChildGoalsData));

    // Validation vars
    this.nameRegex = NAME_REGEX;

    //Master List
    this.datesList = FhrService.getDateRange();
    this.monthList = FhrService.getMonthRange();
    this.yearsList = FhrService.getChildYearRange();

    //Model values for screen
    this.childFirstName = "";
    this.childMiddleName = "";
    this.childLastName = "";
    this.childDobDate = this.datesList[0];
    this.childDobMonth = this.monthList[0];
    this.childDobYear = this.yearsList[0];
    this.goalPlan = "";
    this.moneyReqAge = 18; // Min recommendation age is 18
    this.currSavings = 0;
    this.expReturn = 5; // Min return value is 4
    this.inflRate = 5; // Min inflation reate value is 5

    var childAgeStr = this.childDobDate.LABEL + "-" + this.childDobMonth.LABEL + "-" + this.childDobYear.LABEL + " 00:00:00";
    this.childCurrentAge = CommonService.getAge(childAgeStr);
    this.proposerAge = parseInt(this.personalDetails.CURRENT_AGE);
    this.minAgeWhenMoneyReq = parseInt(this.childCurrentAge) + parseInt(10); // Minimum should be Childs Current age + 10 Years
    this.moneyReqAge = this.minAgeWhenMoneyReq;

    debug("Child Current age " + this.childCurrentAge);

    this.currentCost = {
        value: 100000,
        minValue: 100000,
        maxValue: 5000000,
        options: {
            floor: 100000,
            ceil: 5000000,
            step: 100000,
            showTicks: true,
            showSelectionBar: true,
            interval: 500000,
            translate: function(value) {
                if (value == 100000)
                    return value / 100000 + ' L';
                else
                    return value / 100000 + ' L';
            }
        }
    };

    // Calculate current age and money required age on change of date of birth
    this.calculateChildCurrentAge = function() {
        var childAgeStr = this.childDobDate.LABEL + "-" + this.childDobMonth.LABEL + "-" + this.childDobYear.LABEL + " 00:00:00";
        this.childCurrentAge = CommonService.getAge(childAgeStr);
        this.minAgeWhenMoneyReq = parseInt(this.childCurrentAge) + parseInt(10); // Minimum should be Childs Current age + 10 Years
        this.moneyReqAge = this.minAgeWhenMoneyReq;
        debug("Child Current age now is " + this.childCurrentAge);
    };

    // Function to set Child Goals from model
    this.setChildGoalsData = function(form) {
        debug("Setting Child Goal Object");
        this.fhrChildGoalsData.FHR_ID = this.stateData.FHRID;
        this.fhrChildGoalsData.AGENT_CD = this.stateData.AGENTCD;
        this.fhrChildGoalsData.CLIENT_FIRST_NAME = this.childFirstName;
        this.fhrChildGoalsData.CLIENT_MIDDLE_NAME = this.childMiddleName;
        this.fhrChildGoalsData.CLIENT_LAST_NAME = this.childLastName;
        this.fhrChildGoalsData.DOB_DATE = this.childDobDate.LABEL;
        this.fhrChildGoalsData.DOB_MONTH = this.childDobMonth.LABEL;
        this.fhrChildGoalsData.DOB_YEAR = this.childDobYear.LABEL;
        this.fhrChildGoalsData.CLIENT_BIRTH_DATE = this.childDobDate.LABEL + "-" + this.childDobMonth.LABEL + "-" + this.childDobYear.LABEL + " 00:00:00";
        this.fhrChildGoalsData.GOAL_CURRENT_AGE = CommonService.getAge(this.fhrChildGoalsData.CLIENT_BIRTH_DATE);
        this.fhrChildGoalsData.PROPOSER_CURRENT_AGE = this.proposerAge;
        this.fhrChildGoalsData.GOAL_DESC = this.goalPlan;
        this.fhrChildGoalsData.PRESENT_COST = this.currentCost.value || 100000;
        this.fhrChildGoalsData.GOAL_TARGET_AGE = this.moneyReqAge;
        this.fhrChildGoalsData.PRESENT_SAVINGS = this.currSavings;
        this.fhrChildGoalsData.ROI_RATE = this.expReturn;
        this.fhrChildGoalsData.INFLATION_RATE = this.inflRate;
        this.fhrChildGoalsData.RELATION = "Child";
        this.fhrChildGoalsData.RELATION_CODE = this.relationCode;
        this.fhrChildGoalsData.MODIFIED_DATE = CommonService.getCurrDate();

        //Future Cost Calculation
        this.fhrChildGoalsData.FUTURE_COST = (parseInt(this.fhrChildGoalsData.PRESENT_COST) * Math.pow((1 + ((this.fhrChildGoalsData.INFLATION_RATE) / 100)), (parseInt(this.fhrChildGoalsData.GOAL_TARGET_AGE) - parseInt(this.fhrChildGoalsData.GOAL_CURRENT_AGE)))).toFixed(2);

        this.fhrChildGoalsData.FUTURE_SAVINGS_VALUE = (parseInt(this.fhrChildGoalsData.PRESENT_SAVINGS) * Math.pow((1 + (this.fhrChildGoalsData.ROI_RATE / 100)), (parseInt(this.fhrChildGoalsData.GOAL_TARGET_AGE) - parseInt(this.fhrChildGoalsData.GOAL_CURRENT_AGE)))).toFixed(2);

        this.fhrChildGoalsData.GOAL_GAP_AMOUNT = (this.fhrChildGoalsData.FUTURE_COST - this.fhrChildGoalsData.FUTURE_SAVINGS_VALUE).toFixed(2);

        // If Shortfall less than 0, then show 0 only
        if (this.fhrChildGoalsData.GOAL_GAP_AMOUNT < 0) {
            this.fhrChildGoalsData.GOAL_GAP_AMOUNT = 0;
        }

        var dfd = $q.defer();
        FhrGoalsMainService.getPlanURNNos(this.fhrChildGoalsData).then(function(res) {
            debug("Res of getPlanURNNos " + JSON.stringify(res));
            fhrchlg.fhrChildGoalsData.SUGEST_PLAN1_URNNO = res.SUGGESTED_PRODUCT1_UINNO;
            fhrchlg.fhrChildGoalsData.SUGEST_PLAN2_URNNO = res.SUGGESTED_PRODUCT2_UINNO;
            fhrchlg.fhrChildGoalsData.SUGEST_PLAN3_URNNO = res.SUGGESTED_PRODUCT3_UINNO;
            dfd.resolve(fhrchlg.fhrChildGoalsData);
        });
        return dfd.promise;
    };

    // On click of Next Button
    this.onNext = function(form) {
        debug("submit called");
        debug(form.$valid);
        if (form.$valid) {
            debug("Form valid");
            this.setChildGoalsData(form).then(
                function() {
                    debug("After setting setChildGoalsData");
                    var dfd = $q.defer();
                    FhrChildGoalsService.onSubmit(JSON.parse(JSON.stringify(fhrchlg.fhrChildGoalsData))).then(
                        function(res) {
                            if (!!res) {
                                debug("Result after success " + JSON.stringify(res));
                                navigator.notification.alert("Child Goal added successfully.", function() {
                                    $state.go(FHR_GOALS_SUCCESS, { GOALTYPE: "Child", LEADID: fhrchlg.stateData.LEADID, FHRID: fhrchlg.stateData.FHRID, OPPID: fhrchlg.stateData.OPPID, AGENTCD: fhrchlg.stateData.AGENTCD });
                                }, "Personal Details", "OK");
                                dfd.resolve(fhrchlg.fhrChildGoalsData);
                            } else {
                                navigator.notification.alert("Child Goal addition failed.", null, "Child Goal", "OK");
                                dfd.resolve(null);
                            }
                        }
                    );
                    return dfd.promise;
                }
            );
        }
    };

    // On click of Prev button
    this.onPrev = function() {
        fhrchlg.redirObj.gotoGoalsTab();
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();

}]);

childGoalsModule.service('FhrChildGoalsService', ['$q', '$state', 'CommonService', function($q, $state, CommonService) {
    var fhrchlgsc = this;

    // Function to get last RELATION_SEQ_NO related to Child Goal
    this.getLastRelSeqNoRelatedToFhr = function(tableName, fhdId, agendCd) {
        debug("Inside getLastRelSeqNoRelatedToFhr");
        var dfd = $q.defer();
        try {
            var query = "select (RELATION_SEQ_NO+1) RELATION_SEQ_NO from " + tableName + " WHERE FHR_ID=? AND AGENT_CD=? order by RELATION_SEQ_NO desc limit 1";
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [fhdId, agendCd],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                debug("Resolved with data getLastRelSeqNoRelatedToFhr");
                                dfd.resolve(res.rows.item(0).RELATION_SEQ_NO);
                            } else {
                                debug("Resolved without data getLastRelSeqNoRelatedToFhr");
                                dfd.resolve(1);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(null);
                }
            );
        } catch (ex) {
            dfd.resolve(null);
        }
        debug("Exiting getLastRelSeqNoRelatedToFhr");
        return dfd.promise;
    };

    // Function to Insert Child Goals Data
    this.insertChildGoals = function(tableName, pageData) {
        debug("Inside insert child goals details");

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT INTO " + tableName + " ( FHR_ID,AGENT_CD,CLIENT_FIRST_NAME,CLIENT_MIDDLE_NAME,CLIENT_LAST_NAME,RELATION_SEQ_NO,RELATION,RELATION_CODE,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_TARGET_AGE,PRESENT_COST,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,CLIENT_BIRTH_DATE,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [pageData.FHR_ID, pageData.AGENT_CD, pageData.CLIENT_FIRST_NAME, pageData.CLIENT_MIDDLE_NAME, pageData.CLIENT_LAST_NAME, pageData.RELATION_SEQ_NO, pageData.RELATION, pageData.RELATION_CODE, pageData.GOAL_DESC, pageData.GOAL_CURRENT_AGE, pageData.GOAL_TARGET_AGE, pageData.PRESENT_COST, pageData.FUTURE_COST, pageData.FUTURE_SAVINGS_VALUE, pageData.GOAL_GAP_AMOUNT, pageData.SUGEST_PLAN1_URNNO, pageData.SUGEST_PLAN2_URNNO, pageData.SUGEST_PLAN3_URNNO, pageData.CLIENT_BIRTH_DATE, pageData.INFLATION_RATE, pageData.PRESENT_SAVINGS, pageData.ROI_RATE],
                    function(tx, res) {
                        debug("inserted into " + tableName);
                        dfd.resolve(pageData);
                    },
                    function(tx, err) {
                        debug("Error in insertChildGoals.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                debug("Error in insertChildGoals(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Submit Child Goals
    this.onSubmit = function(pageData) {
        debug("Inside On Submit");

        var tableName = "LP_FHR_GOAL_CHILD_EDU_SCRN_B";
        if (pageData.GOAL_DESC == "Childs Marriage")
            tableName = "LP_FHR_GOAL_CHILD_MRG_SCRN_C";
        else if (pageData.GOAL_DESC == "Childs Wealth")
            tableName = "LP_FHR_GOAL_CHILD_WEALTH_SCRN_G";

        var dfd = $q.defer();
        if (!!pageData.FHR_ID) { // If FHR_ID is generated
            debug("Inside if On Submit");
            debug(pageData);

            // Inserting into Child Goals related table	
            fhrchlgsc.getLastRelSeqNoRelatedToFhr(tableName, pageData.FHR_ID, pageData.AGENT_CD).then(function(relSeqNo) {
                debug("RELATION_SEQ_NO" + relSeqNo);
                pageData.RELATION_SEQ_NO = relSeqNo;
                fhrchlgsc.insertChildGoals(tableName, pageData).then(function(res) {
                    dfd.resolve(res);
                });
            });


        } else { // If FHR_ID not generated
            debug("Inside else On Submit");
            dfd.resolve(null);
        }
        return dfd.promise;
    };
}]);