goalsMainModule.controller('FhrGoalsMainCtrl',['$state', 'CommonService', 'FhrService', 'FhrGoalsMainService', 'CategoryWiseGoalsCount','PersonalDetailsExistingData', function($state, CommonService, FhrService, FhrGoalsMainService, categoryWiseGoalsCount, PersonalDetailsExistingData){
    debug("Inside Goals Main Module");
	CommonService.hideLoading();   

    var fhrgoals = this;

	// Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.stateData = FhrGoalsMainService.getStateParams();
	this.fhrPersonalDetailsData = PersonalDetailsExistingData[0];
	debug("Existing Personal Details data "+JSON.stringify(this.fhrPersonalDetailsData));

	//Setting goals count
	this.goalsCountData = categoryWiseGoalsCount;
	debug(JSON.stringify(this.goalsCountData));

	this.showCompletedFlag = function(msg){
		navigator.notification.alert("Goals addition completed.",null,msg,"OK");
	};	

	this.gotoChildGoalsTab = function(){
        debug("Clicked Child Goals");
        $state.go(FHR_CHILD_GOALS, {FHRID: this.stateData.FHRID, AGENTCD: this.stateData.AGENTCD});
	};    

	this.gotoRetFundTab = function(){
        debug("Clicked Retire Fund");
        $state.go(FHR_RETIRE_FUND, {FHRID: this.stateData.FHRID, AGENTCD: this.stateData.AGENTCD});
	};    

	this.gotoProtectionTab = function(){
        debug("Clicked Protection");
        $state.go(FHR_PROTECTION, {FHRID: this.stateData.FHRID, AGENTCD: this.stateData.AGENTCD});
	};    

	this.gotoWealthCreateTab = function(){
        debug("Clicked Wealth Creation");
		$state.go(FHR_WEALTH_CREATION, {FHRID: this.stateData.FHRID, AGENTCD: this.stateData.AGENTCD});
	}; 

}]);

goalsMainModule.controller('FhrGoalsSuccessCtrl',['$state', 'CommonService', 'FhrService', 'FhrGoalsMainService', function($state, CommonService, FhrService, FhrGoalsMainService){
    debug("Inside Goals Success Module");
	CommonService.hideLoading();   

    var fhrsuccessgoals = this;

	// Common redirection object
    this.redirObj = FhrService.redirObjService;	

	this.stateData = FhrGoalsMainService.getStateParams();
	this.goalIcon = "";
	this.goalMsg = this.stateData.GOALTYPE;

	if(this.stateData.GOALTYPE == "Child"){
		this.goalIcon = "icon_child_goal";
	}
	else if(this.stateData.GOALTYPE == "Wealth"){
		this.goalIcon = "icon_wealth_creation";
	}
	else if(this.stateData.GOALTYPE == "Protection"){
		this.goalIcon = "icon_protection";
	}
	else if(this.stateData.GOALTYPE == "Retirement"){
		this.goalIcon = "icon_retirement_fund";		
	}
	debug("Goal Type : "+this.stateData.GOALTYPE);
	debug("Goal Icon : "+this.goalIcon);

}]);	


goalsMainModule.service('FhrGoalsMainService',['$q','$state', 'CommonService', 'FhrService', function($q, $state, CommonService, FhrService){
    var as = this;
    var customerNeedList = {"CHEDU": "Childs Education","CHMARR": "Childs Marriage","CHWEA": "Childs Wealth","PROTE": "Protection","RETIR": "Retirement","WEALT": "Wealth"};   

    var wealthPlanList = {HOMTWO: "2nd Home Down Payment",CARBY: "Buying New Car",INTVAC: "International Vacation",OTHGOAL: "Other Goal",REPAYHL: "Repaying Home Loan",NWBUSI: "Starting New Business"};

    this.goalsStateParams = [];

	this.categoryWiseGoalsCount = {};

    this.setStateParams = function(stateParams){
        as.goalsStateParams = stateParams;
    };

    this.getStateParams = function(){
        return as.goalsStateParams;
    };

    this.getCustomerNeedList = function(){
        return customerNeedList;
    };

    this.getWealthPlanList = function(){
        return wealthPlanList;
    };

	this.resetCategoryWiseGoalsCount = function(){
		as.categoryWiseGoalsCount = {childsCount:0,protectionCount:0,retirementCount:0,wealthCount:0};
	};

    // Function to retrieve Plan URN No's based on criteria  
    this.getPlanURNNos = function(inputObj){
		debug("Inside getPlanURNNos");
		var dfd = $q.defer();
        var retData = {};
        var query = "SELECT  CUSTOMER_NEED,CUSTOMER_MIN_AGE,CUSTOMER_MAX_AGE,MIN_TIME_HORIZON,MAX_TIME_HORIZON,MIN_PPT,MAX_PPT,PAYMENT_TYPE,PLAN_TYPE,REINVESTMENT_REQUIRED,REINVESTMENT_MIN_AGE,REINVESTMENT_PRODUCT_UINNO,BUSINESS_TYPE_ID,ISACTIVE,VERSION_NO, "+
			"(SELECT SUGGESTED_PRODUCT1_UINNO from LP_PRODUCT_MASTER WHERE PRODUCT_UINNO=SUGGESTED_PRODUCT1_UINNO and BUSINESS_TYPE_LIST LIKE '%" + BUSINESS_TYPE + "%' AND ISACTIVE='Y') SUGGESTED_PRODUCT1_UINNO,"+
			"(SELECT SUGGESTED_PRODUCT2_UINNO from LP_PRODUCT_MASTER WHERE PRODUCT_UINNO=SUGGESTED_PRODUCT2_UINNO and BUSINESS_TYPE_LIST LIKE '%" + BUSINESS_TYPE + "%' AND ISACTIVE='Y') SUGGESTED_PRODUCT2_UINNO,"+
			"(SELECT SUGGESTED_PRODUCT3_UINNO from LP_PRODUCT_MASTER WHERE PRODUCT_UINNO=SUGGESTED_PRODUCT3_UINNO and BUSINESS_TYPE_LIST LIKE '%" + BUSINESS_TYPE + "%' AND ISACTIVE='Y') SUGGESTED_PRODUCT3_UINNO"+
			" FROM LP_FHR_PRODUCT_REC_GRID LFPRG "+
            " WHERE (CAST(CUSTOMER_MIN_AGE AS INTEGER)<=? AND CAST(CUSTOMER_MAX_AGE AS INTEGER)>=?) "+
            " AND (CAST(MIN_TIME_HORIZON AS INTEGER)<=? AND CAST(MAX_TIME_HORIZON AS INTEGER)>=?) "+
            " AND CUSTOMER_NEED=? AND ISACTIVE='Y' ";
            // " AND BUSINESS_TYPE_ID='LifePlaner' "; 
        
		var AGE_PARAM = 0;    
        var TIME_HORIZONE_PARAM = 0;    

        if(inputObj.GOAL_DESC == "Childs Education" || inputObj.GOAL_DESC == "Childs Marriage" || inputObj.GOAL_DESC == "Childs Wealth"){
			AGE_PARAM = inputObj.PROPOSER_CURRENT_AGE;
			TIME_HORIZONE_PARAM = parseInt(inputObj.GOAL_TARGET_AGE) - parseInt(inputObj.GOAL_CURRENT_AGE);       //  Money Required age - Childs Current Age
        }else if(inputObj.GOAL_DESC == "Protection"){
			AGE_PARAM = inputObj.GOAL_CURRENT_AGE;
			TIME_HORIZONE_PARAM = parseInt(inputObj.GOAL_TARGET_AGE) - parseInt(inputObj.GOAL_CURRENT_AGE);       //  Retirement Age - Personal Details Current Age   
        }else if(inputObj.GOAL_DESC == "Retirement"){
			AGE_PARAM = inputObj.GOAL_CURRENT_AGE;
			TIME_HORIZONE_PARAM = parseInt(inputObj.RETIREMENT_AGE) - parseInt(inputObj.GOAL_CURRENT_AGE);         //  T2 = Retirement age - Personal Details Current Age  
        }else if(inputObj.GOAL_DESC == "Wealth"){
			AGE_PARAM = inputObj.GOAL_CURRENT_AGE;       
			TIME_HORIZONE_PARAM = parseInt(inputObj.GOAL_TARGET_AGE) - parseInt(inputObj.GOAL_CURRENT_AGE);       	//  Money Required age - Proposer Current Age
        }
		debug(query+" |----| "+AGE_PARAM+" |----| "+TIME_HORIZONE_PARAM);
		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [AGE_PARAM, AGE_PARAM, TIME_HORIZONE_PARAM, TIME_HORIZONE_PARAM, inputObj.GOAL_DESC],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getPlanURNNos");
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getPlanURNNos");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting setFHRPersonalDetails");
		return dfd.promise;
    };

	// Function to get Goals count list for FHR_ID
	this.getGoalsWiseCount = function(){
		return as.categoryWiseGoalsCount;
	};

    // Function to set Goals count list for FHR_ID
    this.setGoalsWiseCount = function(inputObj){
        debug("Inside setGoalsWiseCount");

        var dfd = $q.defer();
        // var retData = {childsCount:0,protectionCount:0,retirementCount:0,wealthCount:0};
		as.resetCategoryWiseGoalsCount();
        var query = "SELECT (chEduCount+chMrgCount+chWeaCount) childsCount,protectionCount,retirementCount,wealthCount FROM ( "+
                    " SELECT "+
                    " (SELECT count(*) FROM LP_FHR_GOAL_CHILD_EDU_SCRN_B WHERE FHR_ID=? AND AGENT_CD=?) chEduCount, "+
                    " (SELECT count(*) FROM LP_FHR_GOAL_CHILD_MRG_SCRN_C WHERE FHR_ID=? AND AGENT_CD=?) chMrgCount, "+
                    " (SELECT count(*) FROM LP_FHR_GOAL_CHILD_WEALTH_SCRN_G WHERE FHR_ID=? AND AGENT_CD=?) chWeaCount, "+
                    " (SELECT COUNT(*) FROM LP_FHR_GOAL_LIVING_STD_SCRN_F WHERE FHR_ID=? AND AGENT_CD=?) protectionCount, "+
                    " (SELECT COUNT(*) FROM LP_FHR_GOAL_RETIREMENT_SCRN_E WHERE FHR_ID=? AND AGENT_CD=?) retirementCount, "+
                    " (SELECT COUNT(*) FROM LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=?) wealthCount "+
                " FROM LP_FHR_MAIN WHERE FHR_ID=? AND AGENT_CD=? "+
                ") as mySelect WHERE 1 = 1";
        try{
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res){
                            if(!!res && res.rows.length>0){
                                as.categoryWiseGoalsCount = res.rows.item(0);
								FhrService.setGoalsCountDataRelatedToFHR(inputObj.FHRID, as.categoryWiseGoalsCount);
								// as.categoryWiseGoalsCount.protectionCount = 1; // Remove condition after final protection checkup
                                debug("Resolved with data setGoalsWiseCount"+ JSON.stringify(as.categoryWiseGoalsCount));
                                dfd.resolve(as.categoryWiseGoalsCount);                               
                            }
                            else{
                                debug("Resolved without data setGoalsWiseCount");
                                dfd.resolve(as.categoryWiseGoalsCount);
                            }
                        },
                        function(tx, err){
                            dfd.resolve(as.categoryWiseGoalsCount);
                        }
                    );
                },
                function(err){
                    dfd.resolve(as.categoryWiseGoalsCount);
                }
            );
        }
        catch(ex){
            dfd.resolve(as.categoryWiseGoalsCount);
        }
        debug("Exiting getGoalsWiseCount");
        return dfd.promise;
    };


    // Function to retrieve already added Child Goals List for FHR_ID
    this.getChildGoalsRelatedFHR = function(inputObj){
		debug("Inside getChildGoalsRelatedFHR");

		var customerNeeds = as.getCustomerNeedList();
		var dfd = $q.defer();
        var retData = {CHILD_EDU:null,CHILD_MAR:null,CHILD_WEA:null};
        var query = "SELECT " +
			" (select FHR_ID from LP_FHR_GOAL_CHILD_EDU_SCRN_B WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Education' LIMIT 1) CHILD_EDU, "+
			" (select FHR_ID from LP_FHR_GOAL_CHILD_MRG_SCRN_C WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Marriage' LIMIT 1) CHILD_MAR, "+
			" (select FHR_ID from LP_FHR_GOAL_CHILD_WEALTH_SCRN_G WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Wealth' LIMIT 1) CHILD_WEA "+
			" FROM LP_FHR_GOAL_CHILD_EDU_SCRN_B LFGCEDSC  "+
			" LIMIT 1";
		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getChildGoalsRelatedFHR"+ JSON.stringify(retData));
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getChildGoalsRelatedFHR");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting getChildGoalsRelatedFHR");
		return dfd.promise;
    };

    // Function to retrieve already added Wealth Goals List for FHR_ID
    this.getWealthGoalsRelatedFHR = function(inputObj){
		debug("Inside getWealthGoalsRelatedFHR");
		var dfd = $q.defer();
        var retData = {NWBUSI:null,HOMTWO:null,CARBY:null,INTVAC:null,OTHGOAL:null,REPAYHL:null};
        var query = "SELECT "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Starting New Business' LIMIT 1) NWBUSI, "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='2nd Home Down Payment' LIMIT 1) HOMTWO, "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Buying New Car' LIMIT 1) CARBY, "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='International Vacation' LIMIT 1) INTVAC , "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Other Goal' LIMIT 1) OTHGOAL, "+
			" (select FHR_ID from LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Repaying Home Loan' LIMIT 1) REPAYHL "+   
			" FROM LP_FHR_GOAL_WEALTH_SCRN_D LFGCEDSC "+
			" LIMIT 1";
		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD,inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getWealthGoalsRelatedFHR");
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getWealthGoalsRelatedFHR");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting getWealthGoalsRelatedFHR");
		return dfd.promise;
    };    

    // Function to retrieve already added Retirement Fund List for FHR_ID
    this.getRetirementFundGoalsRelatedFHR = function(inputObj){
		debug("Inside getRetirementFundGoalsRelatedFHR");
		var dfd = $q.defer();
		var retData = {};
        var query = "SELECT * FROM LP_FHR_GOAL_RETIREMENT_SCRN_E WHERE FHR_ID=? AND AGENT_CD=? LIMIT 1";
		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getRetirementFundGoalsRelatedFHR");
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getRetirementFundGoalsRelatedFHR");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting getRetirementFundGoalsRelatedFHR");
		return dfd.promise;
    };	    

    // Function to retrieve already added Protection List for FHR_ID
    this.getProtectionGoalsRelatedFHR = function(inputObj){
		debug("Inside getProtectionGoalsRelatedFHR");
		var dfd = $q.defer();
		var retData = {};
		var query = "SELECT * FROM LP_FHR_GOAL_LIVING_STD_SCRN_F WHERE FHR_ID=? AND AGENT_CD=? LIMIT 1";

		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getProtectionGoalsRelatedFHR");
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getProtectionGoalsRelatedFHR");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting getProtectionGoalsRelatedFHR");
		return dfd.promise;
    };	 

    // Function to retrieve relation code related to value
    this.getFhrRelationData = function(relVal){
		debug("Inside getFhrRelationData");
		var dfd = $q.defer();
		var retData = {};
		var query = "SELECT * FROM LP_FHR_RELATION WHERE RELATIONSHIP_DESC=?";

		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [relVal],
						function(tx, res){
							if(!!res && res.rows.length>0){
                                retData = res.rows.item(0);
                                debug("Resolved with data getFhrRelationData");
                                dfd.resolve(retData);								
                            }
							else{
								debug("Resolved without data getFhrRelationData");
								dfd.resolve(retData);
							}
						},
						function(tx, err){
							dfd.resolve(retData);
						}
					);
				},
				function(err){
					dfd.resolve(retData);
				}
			);
		}
		catch(ex){
			dfd.resolve(retData);
		}
		debug("Exiting getFhrRelationData");
		return dfd.promise;
    };	 
}]);
