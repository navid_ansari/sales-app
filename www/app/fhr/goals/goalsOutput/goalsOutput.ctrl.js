goalsOutputModule.controller('GoalsOutputCtrl', ['$state', '$timeout', '$stateParams', '$filter', 'LoginService', 'ApplicationFormDataService', 'CommonService', 'FhrService', 'GoalsOutputService', 'CategoryWiseGoalsCount', 'ViewReportService', function($state, $timeout, $stateParams, $filter, LoginService, ApplicationFormDataService, CommonService, FhrService, GoalsOutputService, categoryWiseGoalsCount, ViewReportService) {
    debug("Inside Goals Output Module");
    CommonService.showLoading();

    var goalsout = this;
    this.personalDetails = FhrService.getPersonalDetailsDataInFhrMain();
    this.outputDetails = GoalsOutputService.getOutputData();
    this.firstTimeRenderChardCalled = false;

    this.personalDetailsBirthDate = FhrService.getFormattedDate(goalsout.personalDetails.BIRTH_DATE);
    this.activeTab = "CHEDU";

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    FhrService.setOutputDetailsDataInFhrMain(this.outputDetails);

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;
    debug(JSON.stringify(this.goalsCountData));

    debug(this.personalDetails);
    debug(this.outputDetails);

    this.setActiveTab = function(tab) {
        debug($('#chart_container_' + tab));
        goalsout.activeTab = tab;
        goalsout.initiateChartForTab(goalsout.activeTab);
    };

    // Function to initiate Chart for related tab
    this.initiateChartForTab = function(tab) {
        debug("Chart containes is " + 'chart_container_' + tab);
		
        for (key in this.outputDetails[tab]) {
            var chartData = goalsout.getRequiredDataForChart(tab, this.outputDetails[tab][key]); // Fetch required values from Main array
            debug("Chartdata is " + JSON.stringify(chartData));
			
			if(chartData.value1 == undefined || chartData.value2 == undefined)
				continue;
				
            var divId = 'chart_container_' + tab;
            if (tab == "CHEDU" || tab == "CHMARR" || tab == "CHWEA") {
                divId += "_" + this.outputDetails[tab][key].RELATION_SEQ_NO;
            }
            debug("Div Id is " + divId);
            // debug(document.getElementById("chart_container_CHEDU_1") != null);
			goalsout.renderChartData(chartData, tab, divId);
        }
        CommonService.hideLoading();
    };

    this.renderChartData = function(chartData, tab, divId) {
        goalsout.activeTab = tab;
        Highcharts.chart(divId, {
            tooltip: { enabled: false },
            chart: {
                type: 'column',
                margin: 70,
                options3d: {
                    enabled: true,
                    alpha: 2,
                    beta: 62,
                    depth: 110,
                    viewDistance: 80
                }
            },
            plotOptions: {
                column: {
                    depth: 80,
                    stacking: true,
                    grouping: false,
                    groupZPadding: 10
                },
                series: {
                    events: {
                        legendItemClick: function() {
                            return false;
                        }
                    }
                }
            },
            series: [{
                name: 'Shortfall',
                data: [parseInt(chartData.value1)],
                stack: 0,
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#f9c74e'],
                        [1, '#f9c74e']
                    ]
                }
            }, {
                name: 'Existing Provision',
                data: [parseInt(chartData.value2)],
                stack: 0,
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#f7f7f7'],
                        [1, '#f7f7f7']
                    ]
                }
            }]
        });
    };

    // Function to get chart related data from outputdata
    this.getRequiredDataForChart = function(tab, tabData) {
        var retData = {};

        if (tab == "CHEDU" || tab == "CHMARR" || tab == "CHWEA") { //  Child Goals
            retData.value1 = tabData.GOAL_GAP_AMOUNT;
            retData.value2 = tabData.FUTURE_SAVINGS_VALUE;
        } else if (tab == "RETIR") { //  Retirement
            retData.value1 = tabData.GOAL_GAP_AMOUNT;
            retData.value2 = tabData.FUTURE_SAVINGS_VALUE;
        } else if (tab == "PROTE") { //  Protection
            retData.value1 = tabData.PROVISION_GAP_EMERGENCY_FUND;
            retData.value2 = tabData.SUM_ASSURED;
        } else if (tab == "WEAHOMTWO" || tab == "WEACARBY" || tab == "WEAINTVAC" || tab == "WEAREPAYHL" || tab == "WEANWBUSI" || tab == "WEAOTHGOAL") { //  Wealth
            retData.value1 = tabData.GOAL_GAP_AMOUNT;
            retData.value2 = tabData.FUTURE_SAVINGS_VALUE;
        }
        return retData;
    };

    // Function to initiate chart for the first time
    this.firstTimeInitiation = function() {
        var key;
        var firstTab = "";
        for (key in goalsout.outputDetails) {
            if (goalsout.outputDetails[key].length > 0) {
                firstTab = key;
                break;
            }
        }
        debug("First tab is " + firstTab);

        if (firstTab !== "" && goalsout.firstTimeRenderChardCalled === false) {
            goalsout.firstTimeRenderChardCalled = true;
            goalsout.initiateChartForTab(firstTab);
        }
    };

    // Function to destroy existing charts
    this.destroyAllExistingCharts = function() {


    };

    // Function to generate Fhr Output
    this.generateFhrOutput = function() {
        CommonService.showLoading();
        debug("Clicked Generate Final Output");
        FhrService.generateFhrFinalOutput(goalsout.personalDetails, goalsout.outputDetails).then(function(resp) {
            debug("Response of Generate FHR Final Output is " + resp);
            if (resp == "success") {
                goalsout.viewFHRReport(goalsout.personalDetails.FHR_ID, goalsout.personalDetails.EMAIL_ID);
            } else {
                CommonService.hideLoading();
                navigator.notification.alert("Error occured while generating output.", null, "Error", "OK");
                return false;
            }
        });
    };

    // Function to View FHR Report based on FHR Id
    this.viewFHRReport = function(fhrid, custEmailId) {
        if (!!fhrid) {
            ViewReportService.fetchSavedReport("FHR", fhrid, null).then(
                function(res) {

                    var reportParams = {};
                    reportParams.agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                    reportParams.appId = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
                    reportParams.custEmailId = custEmailId;
                    reportParams.polNo = "";

                    debug("Report params for view report " + JSON.stringify(reportParams));

                    CommonService.hideLoading();
                    $state.go("viewReport", { 'REPORT_TYPE': 'FHR', 'REPORT_ID': fhrid, 'PREVIOUS_STATE': FHR_GOALS_OUTPUT, "REPORT_PARAMS": reportParams, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams });
                }
            );
        }
    };

    $timeout(function() {
        debug("Timeout called.");
        goalsout.firstTimeInitiation();
    }, 500);
}]);

goalsOutputModule.service('GoalsOutputService', ['$q', '$state', 'CommonService', 'FhrService', 'FhrGoalsMainService', function($q, $state, CommonService, FhrService, FhrGoalsMainService) {
    var goalsoutsc = this;

    this.fhrId = "";
    this.outputData = [];

    // Function to Retrieve Output Data related to any particulat FHR Id
    this.getOutputData = function() {
        debug("Inside getOutputData");
        return goalsoutsc.outputData;
    };

    // Function to retrieve Child Goals Data related to FHR Id
    this.setAllChildGoalsDataRelatedFHRId = function(inputObj) {
        debug("Inside setAllChildGoalsDataRelatedFHRId");
        var dfd = $q.defer();
        // var retData = [];
        goalsoutsc.outputData.CHEDU = [];
        goalsoutsc.outputData.CHMARR = [];
        goalsoutsc.outputData.CHWEA = [];

        var query = "SELECT a.*, " +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN1_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS1," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN2_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS2," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN3_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS3" +
            " from (" +
            " SELECT FHR_ID,AGENT_CD,CLIENT_FIRST_NAME,CLIENT_MIDDLE_NAME,CLIENT_LAST_NAME,RELATION_SEQ_NO,RELATION,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_TARGET_AGE,PRESENT_COST,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,CLIENT_BIRTH_DATE,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE FROM LP_FHR_GOAL_CHILD_EDU_SCRN_B WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Education'" +
            " UNION " +
            " SELECT FHR_ID,AGENT_CD,CLIENT_FIRST_NAME,CLIENT_MIDDLE_NAME,CLIENT_LAST_NAME,RELATION_SEQ_NO,RELATION,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_TARGET_AGE,PRESENT_COST,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,CLIENT_BIRTH_DATE,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE FROM LP_FHR_GOAL_CHILD_MRG_SCRN_C WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Marriage'" +
            " UNION " +
            " SELECT FHR_ID,AGENT_CD,CLIENT_FIRST_NAME,CLIENT_MIDDLE_NAME,CLIENT_LAST_NAME,RELATION_SEQ_NO,RELATION,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_TARGET_AGE,PRESENT_COST,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,CLIENT_BIRTH_DATE,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE FROM LP_FHR_GOAL_CHILD_WEALTH_SCRN_G WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Wealth' " +
            " ) a";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD, inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    res.rows.item(i).CLIENT_BIRTH_DATE = FhrService.getFormattedDate(res.rows.item(i).CLIENT_BIRTH_DATE.split(" ")[0]);
                                    res.rows.item(i).PRODUCT_DETAILS1 = (!!res.rows.item(i).PRODUCT_DETAILS1 ? res.rows.item(i).PRODUCT_DETAILS1.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS2 = (!!res.rows.item(i).PRODUCT_DETAILS2 ? res.rows.item(i).PRODUCT_DETAILS2.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS3 = (!!res.rows.item(i).PRODUCT_DETAILS3 ? res.rows.item(i).PRODUCT_DETAILS3.replace(/\s+/g, '-').toLowerCase() : "");
                                    if (res.rows.item(i).GOAL_DESC == "Childs Education")
                                        goalsoutsc.outputData.CHEDU.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Childs Marriage")
                                        goalsoutsc.outputData.CHMARR.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Childs Wealth")
                                        goalsoutsc.outputData.CHWEA.push(res.rows.item(i));
                                }
                                debug("Resolved with data setAllChildGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            } else {
                                debug("Resolved without data setAllChildGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(goalsoutsc.outputData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(goalsoutsc.outputData);
                }
            );
        } catch (ex) {
            dfd.resolve(goalsoutsc.outputData);
        }
        debug("Exiting setAllChildGoalsDataRelatedFHRId");
        return dfd.promise;
    };

    // Function to retrieve Protection Goals Data related to FHR Id
    this.setAllProtectionGoalsDataRelatedFHRId = function(inputObj) {
        debug("Inside setAllProtectionGoalsDataRelatedFHRId");
        var dfd = $q.defer();
        // var retData = [];
        goalsoutsc.outputData.PROTE = [];

        var query = "SELECT a.*, " +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN1_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS1," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN2_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS2," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN3_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS3" +
            " from ( " +
            " SELECT FHR_ID,AGENT_CD,GOAL_DESC,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,GOAL_CURRENT_AGE,ANN_EXP_TOTAL,REQ_INSURANCE_PROTECTION,PROVISION_GAP_EMERGENCY_FUND,GOAL_GAP_AMOUNT,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,MODIFIED_DATE,INFLATION_RATE,INFLATION_FLAG,ROI_RATE,SUM_ASSURED,PREMIUM_PAY_MODE FROM LP_FHR_GOAL_LIVING_STD_SCRN_F WHERE FHR_ID=? AND AGENT_CD=? ) a";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    res.rows.item(i).PRODUCT_DETAILS1 = (!!res.rows.item(i).PRODUCT_DETAILS1 ? res.rows.item(i).PRODUCT_DETAILS1.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS2 = (!!res.rows.item(i).PRODUCT_DETAILS2 ? res.rows.item(i).PRODUCT_DETAILS2.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS3 = (!!res.rows.item(i).PRODUCT_DETAILS3 ? res.rows.item(i).PRODUCT_DETAILS3.replace(/\s+/g, '-').toLowerCase() : "");
                                    goalsoutsc.outputData.PROTE.push(res.rows.item(i));
                                }
                                debug("Resolved with data setAllProtectionGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            } else {
                                debug("Resolved without data setAllProtectionGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(goalsoutsc.outputData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(goalsoutsc.outputData);
                }
            );
        } catch (ex) {
            dfd.resolve(goalsoutsc.outputData);
        }
        debug("Exiting setAllProtectionGoalsDataRelatedFHRId");
        return dfd.promise;
    };

    // Function to set Retirement Goals Data related to FHR Id
    this.setAllRetirementGoalsDataRelatedFHRId = function(inputObj) {
        debug("Inside setAllRetirementGoalsDataRelatedFHRId");
        var dfd = $q.defer();
        // var retData = [];
        goalsoutsc.outputData.RETIR = [];

        var query = "SELECT a.*, " +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN1_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS1," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN2_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS2," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN3_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS3" +
            " from ( " +
            " SELECT FHR_ID,AGENT_CD,GOAL_DESC,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,GOAL_CURRENT_AGE,GOAL_TARGET_DUARTION,GOAL_TARGET_AGE,PRESENT_COST,INFLATION_RATE,PRESENT_SAVINGS,ROI_RATE,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,FUTURE_COST,FUTURE_REQIRED_CORPUS,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT FROM LP_FHR_GOAL_RETIREMENT_SCRN_E WHERE FHR_ID=? AND AGENT_CD=? ) a";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    res.rows.item(i).PRODUCT_DETAILS1 = (!!res.rows.item(i).PRODUCT_DETAILS1 ? res.rows.item(i).PRODUCT_DETAILS1.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS2 = (!!res.rows.item(i).PRODUCT_DETAILS2 ? res.rows.item(i).PRODUCT_DETAILS2.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS3 = (!!res.rows.item(i).PRODUCT_DETAILS3 ? res.rows.item(i).PRODUCT_DETAILS3.replace(/\s+/g, '-').toLowerCase() : "");
                                    goalsoutsc.outputData.RETIR.push(res.rows.item(i));
                                }
                                debug("Resolved with data setAllRetirementGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            } else {
                                debug("Resolved without data setAllRetirementGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(goalsoutsc.outputData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(goalsoutsc.outputData);
                }
            );
        } catch (ex) {
            dfd.resolve(goalsoutsc.outputData);
        }
        debug("Exiting setAllRetirementGoalsDataRelatedFHRId");
        return dfd.promise;
    };

    // Function to set Wealth Goals Data related to FHR Id
    this.setAllWealthGoalsDataRelatedFHRId = function(inputObj) {
        debug("Inside setAllWealthGoalsDataRelatedFHRId");
        var dfd = $q.defer();
        var wealthPlanList = FhrGoalsMainService.getWealthPlanList();

        goalsoutsc.outputData.WEAHOMTWO = [];
        goalsoutsc.outputData.WEACARBY = [];
        goalsoutsc.outputData.WEAINTVAC = [];
        goalsoutsc.outputData.WEAREPAYHL = [];
        goalsoutsc.outputData.WEANWBUSI = [];
        goalsoutsc.outputData.WEAOTHGOAL = [];

        var query = "SELECT a.*, " +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN1_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS1," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN2_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS2," +
            " (SELECT PRODUCT_NAME from LP_PRODUCT_MASTER WHERE SUGEST_PLAN3_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS3" +
            " from ( " +
            " SELECT FHR_ID,AGENT_CD,GOAL_DESC,GOAL_CURRENT_AGE,GOAL_FIRST_NAME,GOAL_MIDDLE_NAME,GOAL_LAST_NAME,FUTURE_COST,FUTURE_SAVINGS_VALUE,GOAL_GAP_AMOUNT,PRESENT_COST,GOAL_TARGET_AGE,PRESENT_SAVINGS,ROI_RATE,INFLATION_RATE,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO,MODIFIED_DATE FROM LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? ) a";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    res.rows.item(i).PRODUCT_DETAILS1 = (!!res.rows.item(i).PRODUCT_DETAILS1 ? res.rows.item(i).PRODUCT_DETAILS1.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS2 = (!!res.rows.item(i).PRODUCT_DETAILS2 ? res.rows.item(i).PRODUCT_DETAILS2.replace(/\s+/g, '-').toLowerCase() : "");
                                    res.rows.item(i).PRODUCT_DETAILS3 = (!!res.rows.item(i).PRODUCT_DETAILS3 ? res.rows.item(i).PRODUCT_DETAILS3.replace(/\s+/g, '-').toLowerCase() : "");
                                    if (res.rows.item(i).GOAL_DESC == "2nd Home Down Payment")
                                        goalsoutsc.outputData.WEAHOMTWO.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Buying New Car")
                                        goalsoutsc.outputData.WEACARBY.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "International Vacation")
                                        goalsoutsc.outputData.WEAINTVAC.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Repaying Home Loan")
                                        goalsoutsc.outputData.WEAREPAYHL.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Starting New Business")
                                        goalsoutsc.outputData.WEANWBUSI.push(res.rows.item(i));
                                    else if (res.rows.item(i).GOAL_DESC == "Other Goal")
                                        goalsoutsc.outputData.WEAOTHGOAL.push(res.rows.item(i));
                                }
                                debug("Resolved with data setAllWealthGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            } else {
                                debug("Resolved without data setAllWealthGoalsDataRelatedFHRId");
                                dfd.resolve(goalsoutsc.outputData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(goalsoutsc.outputData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(goalsoutsc.outputData);
                }
            );
        } catch (ex) {
            dfd.resolve(goalsoutsc.outputData);
        }
        debug("Exiting setAllWealthGoalsDataRelatedFHRId");
        return dfd.promise;
    };

}]);
