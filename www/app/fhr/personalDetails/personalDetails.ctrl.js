personalDetailsModule.controller('FhrPersonalDetailsCtrl', ['$state', '$q', 'CommonService', 'FhrService', 'FhrPersonalDetailsService', 'LeadData', 'StateList', 'CityList', '$filter', 'FHR_ID', 'LEAD_ID', 'PersonalDetailsExistingData', 'CategoryWiseGoalsCount','EKYCData', function($state, $q, CommonService, FhrService, FhrPersonalDetailsService, LeadData, StateList, CityList, $filter, FHR_ID, LEAD_ID, PersonalDetailsExistingData, categoryWiseGoalsCount,EKYCData) {
    debug("Inside Personal Details Module");
    CommonService.hideLoading();
    FhrService.setOuterWrapInnerHeight();
    //FhrPersonalDetailsService.truncateTables();
    debug(BUSINESS_TYPE);

    var persd = this;

    // Common redirection object
    this.redirObj = FhrService.redirObjService;

    this.fhrPersonalDetailsData = {};
    this.fhrLeadData = LeadData;
    if (!!PersonalDetailsExistingData)
        this.fhrPersonalDetailsData = PersonalDetailsExistingData[0];
    else
        FhrService.clearPersonalDetailsDataInFhrMain();

    // Setting Lead data in FHR Service
    FhrService.setLeadData(LeadData);
    this.fhrPersonalDetailsData.LEAD_ID = LeadData.IPAD_LEAD_ID;

    //Setting goals count
    this.goalsCountData = categoryWiseGoalsCount;
    this.goalsAddedFlag = FhrService.checkIfGoalsAdded(this.goalsCountData);
    debug(this.goalsAddedFlag);

    var nameArr = FhrPersonalDetailsService.getSplitName(LeadData.LEAD_NAME);

    // Validation vars
    this.mobileRegex = MOBILE_REGEX;
    this.emailRegex = EMAIL_REGEX;
    this.nameRegex = NAME_REGEX;

    var ekyc_fname ;
    var ekyc_mname ;
    var ekyc_lname ;
    var ekyc_dob ;
    //Populate EKYC DATA
    if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y')
    {
        var ekycArr = EKYCData.proposer.NAME.split(" ");
            ekyc_fname = ekycArr[0];
            if(ekycArr.length>2){
              ekyc_mname = "";
              for(var i=1; i<(ekycArr.length-1); i++){
                ekyc_mname += ekycArr[i];
                if(i!=(ekycArr.length-2))
                  ekyc_mname += " ";
              }
              ekyc_lname = ekycArr[(ekycArr.length - 1)];
            }
            else{
              ekyc_mname = null;
              if(ekycArr.length==2){
                ekyc_lname = ekycArr[1];
              }
            }
      if(!!EKYCData.proposer.DOB){
        ekyc_dob = EKYCData.proposer.DOB + " 00:00:00";
      }
    }
    // Masters List
    this.stateList = StateList;
    this.cityList = CityList;
    this.residentalStatusList = CommonService.getResidentalStatusList();
    this.datesList = FhrService.getDateRange();
    this.monthList = FhrService.getMonthRange();
    this.yearsList = FhrService.getYearRange();

    //Model values for screen
    this.firstName = (EKYCData.proposer.EKYC_FLAG == 'Y') ? ekyc_fname : nameArr.FIRST_NAME;
    this.middleName = (EKYCData.proposer.EKYC_FLAG == 'Y') ? ekyc_mname : nameArr.MIDDLE_NAME;
    this.lastName = (EKYCData.proposer.EKYC_FLAG == 'Y') ? ekyc_lname : nameArr.LAST_NAME;
    this.dobDate = this.datesList[0];
    this.dobMonth = this.monthList[0];
    this.dobYear = this.yearsList[0];
    this.gender = LeadData.GENDER_CD;
    this.smoker = this.fhrPersonalDetailsData.SMOKER || "";
    this.contactNumber = LeadData.MOBILE_NO;
    this.email = LeadData.EMAIL_ID;
    this.resiStatus = "";
    this.stateOptionSelected = this.stateList[0];
    this.cityOptionSelected = this.cityList[0];
    this.resiStatusSelected = this.residentalStatusList[0];

    // Birth date from Lead details
    var DOB;
    if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y')
        DOB = ekyc_dob;
    else
        DOB = LeadData.BIRTH_DATE;

    if (!!DOB) {
        var dobArr = DOB.split(" ")[0].split("-");
        debug("DOB array is " + JSON.stringify(dobArr));
        var dobD = $filter('filter')(this.datesList, { "LABEL": dobArr[0] })[0];
        this.dobDate = dobD || this.dobDate;

        var dobM = $filter('filter')(this.monthList, { "LABEL": dobArr[1] })[0];
        this.dobMonth = dobM || this.dobMonth;

        var dobY = $filter('filter')(this.yearsList, { "LABEL": dobArr[2] })[0];
        this.dobYear = dobY || this.dobYear;

        debug("Selected date " + JSON.stringify(dobD));
        debug("Selected month " + JSON.stringify(dobM));
        debug("Selected year " + JSON.stringify(dobY));
    }

    // Residental status selection block
    if (!!LeadData.CURNT_STATE || !!LeadData.CURNT_CITY) { // Set Resident indian if Lead data contains State or City data
        this.resiStatusSelected = this.residentalStatusList[1];
    }

    if (!!this.fhrPersonalDetailsData.RESIDENTIAL_STATUS) { // Set Resident indian if Personal Details are filled
        var resStatus = $filter('filter')(this.residentalStatusList, { "RESIDENT_CD": this.fhrPersonalDetailsData.RESIDENTIAL_STATUS }, true)[0];
        this.resiStatusSelected = resStatus || this.resiStatusSelected;
    }

    // State selection code block
    var state = "";
    // City selection code block
    if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y' && !!EKYCData.proposer.STATE)
    {
      var resStatus = $filter('filter')(persd.residentalStatusList, { "RESIDENT_CD": "RI"}, true)[0];
        persd.resiStatusSelected = resStatus || this.resiStatusSelected;
          state = $filter('filter')(this.stateList, { "STATE_DESC": EKYCData.proposer.STATE.toUpperCase()}, true)[0];
          FhrPersonalDetailsService.getCityList(state.STATE_CODE).then(
            function(res) {
                persd.cityList = res;
                persd.cityOptionSelected = persd.cityList[0];
                city = $filter('filter')(persd.cityList, { "CITY_DESC": EKYCData.proposer.CITY.toUpperCase()}, true)[0];
                persd.cityOptionSelected = (!!city && !!city.CITY_CODE)?city:$filter('filter')(persd.cityList, {"CITY_CODE": "OT"}, true)[0];
            }
        );


    }
    else if (!!LeadData.CURNT_STATE && this.fhrPersonalDetailsData.STATE_CODE === undefined) { // If personal details are not set
        state = $filter('filter')(this.stateList, { "STATE_CODE": LeadData.CURNT_STATE }, true)[0];
    } else if (!!this.fhrPersonalDetailsData.STATE_CODE) {
        state = $filter('filter')(this.stateList, { "STATE_CODE": this.fhrPersonalDetailsData.STATE_CODE }, true)[0];
    }
    this.stateOptionSelected = state || this.stateOptionSelected;

    var city = "";
    if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y')
    {
        /*EKYC CITY*/
    }
    if (!!LeadData.CURNT_CITY && this.fhrPersonalDetailsData.CITY_CODE === undefined) { // If personal details are not set
        city = $filter('filter')(this.cityList, { "CITY_CODE": LeadData.CURNT_CITY }, true)[0];
    } else if (!!this.fhrPersonalDetailsData.CITY_CODE) {
        city = $filter('filter')(this.cityList, { "CITY_CODE": this.fhrPersonalDetailsData.CITY_CODE }, true)[0];
    }
    this.cityOptionSelected = city || this.cityOptionSelected;

    debug("FHR_ID " + FHR_ID);
    debug("LEAD DATA IS " + JSON.stringify(LeadData));
    debug("Existing Personal Details data " + JSON.stringify(this.fhrPersonalDetailsData));
    debug("Selected state " + JSON.stringify(this.stateOptionSelected));
    debug("Selected city " + JSON.stringify(this.cityOptionSelected));

    // Function to Personal data object from model
    this.setPersonalDetailsData = function(form) {
        debug("Setting Personal Data Object");
        this.fhrPersonalDetailsData.FIRST_NAME = this.firstName;
        this.fhrPersonalDetailsData.MIDDLE_NAME = this.middleName;
        this.fhrPersonalDetailsData.LAST_NAME = this.lastName;
        this.fhrPersonalDetailsData.DOB_DATE = this.dobDate.LABEL;
        this.fhrPersonalDetailsData.DOB_MONTH = this.dobMonth.LABEL;
        this.fhrPersonalDetailsData.DOB_YEAR = this.dobYear.LABEL;
        this.fhrPersonalDetailsData.BIRTH_DATE = this.dobDate.LABEL + "-" + this.dobMonth.LABEL + "-" + this.dobYear.LABEL;
        this.fhrPersonalDetailsData.CURRENT_AGE = CommonService.getAge(this.fhrPersonalDetailsData.BIRTH_DATE);
        this.fhrPersonalDetailsData.GENDER = this.gender;
        this.fhrPersonalDetailsData.SMOKER = this.smoker;
        this.fhrPersonalDetailsData.CONTACT_NUMBER = this.contactNumber;
        this.fhrPersonalDetailsData.EMAIL_ID = this.email;
        this.fhrPersonalDetailsData.RESIDENTIAL_STATUS = this.resiStatusSelected.RESIDENT_CD;
        this.fhrPersonalDetailsData.STATE_CODE = this.stateOptionSelected.STATE_CODE || null;
        this.fhrPersonalDetailsData.STATE = (this.stateOptionSelected.STATE_DESC == "Select State" ? null : this.stateOptionSelected.STATE_DESC) || null;
        this.fhrPersonalDetailsData.CITY_CODE = this.cityOptionSelected.CITY_CODE || null;
        this.fhrPersonalDetailsData.CITY = (this.cityOptionSelected.CITY_DESC == "Select City" ? null : this.cityOptionSelected.CITY_DESC) || null;
        debug(this.fhrPersonalDetailsData);
    };

    // On click of Next Button
    this.onNext = function(form) {
        debug("submit called");
        debug(form.$valid);
        if (form.$valid) {
            debug("Form valid");
            this.setPersonalDetailsData(form);

            if (!!FHR_ID) {
                LeadData.FHR_ID = FHR_ID;
            }

            var dfd = $q.defer();
            FhrPersonalDetailsService.onSubmit(LeadData, JSON.parse(JSON.stringify(this.fhrPersonalDetailsData))).then(
                function(res) {
                    if (!!res) {
                        FhrService.setPersonalDetailsDataInFhrMain(res.FHR_ID, res.AGENT_CD).then(function(setRes) {
                            debug("Result after success " + JSON.stringify(setRes));
                            navigator.notification.alert("Personal Details added successfully.", function() {
                                $state.go(FHR_OTHERS, { LEADID: setRes[0].LEAD_ID, FHRID: setRes[0].FHR_ID, OPPID: setRes[0].OPP_ID, AGENTCD: setRes[0].AGENT_CD });
                            }, "Personal Details", "OK");
                            dfd.resolve(LeadData);
                        });
                    } else {
                        navigator.notification.alert("Personal Details added successfully.", null, "Lead", "OK");
                    }
                }
            );
            return dfd.promise;
        }
    };

    //On Resident status change
    this.onResidentChange = function(form) {
        debug(this.resiStatusSelected);

        this.stateOptionSelected = this.stateList[0];
        this.cityOptionSelected = this.cityList[0];
        this.fhrPersonalDetailsData.STATE_CODE = this.stateOptionSelected.STATE_CODE;
        this.fhrPersonalDetailsData.CITY_CODE = this.stateOptionSelected.CITY_CODE;

        if (this.resiStatusSelected.RESIDENT_CD != "RI") {
            form.state.$setValidity('selectRequired', true);
            form.city.$setValidity('selectRequired', true);
        } else {
            form.state.$setValidity('selectRequired', false);
            form.city.$setValidity('selectRequired', false);
        }
    };

    //On state option change
    this.onStateChange = function() {
        debug(this.stateOptionSelected);
        this.fhrPersonalDetailsData.STATE_CODE = this.stateOptionSelected.STATE_CODE;
        FhrPersonalDetailsService.getCityList(this.fhrPersonalDetailsData.STATE_CODE).then(
            function(res) {
                persd.cityList = res;
                persd.cityOptionSelected = persd.cityList[0];
            }
        );
    };
    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();

}]);

personalDetailsModule.service('FhrPersonalDetailsService', ['$q', '$state', 'CommonService', function($q, $state, CommonService) {
    var psd = this;

    this.getSplitName = function(name) {
        var retArr = [];
        var splitArr = name.split(" ");
        if (splitArr.length == 2) {
            retArr.FIRST_NAME = splitArr[0];
            retArr.MIDDLE_NAME = "";
            retArr.LAST_NAME = splitArr[1];

        } else if (splitArr.length == 3) {
            retArr.FIRST_NAME = splitArr[0];
            retArr.MIDDLE_NAME = splitArr[1];
            retArr.LAST_NAME = splitArr[2];
        }
        return retArr;
    };

    this.getStateList = function() {
        var dfd = $q.defer();
        var stateList = [];
        stateList.push({ "STATE_DESC": "Select State", "STATE_CODE": null });
        try {
            CommonService.selectRecords(db, 'lp_state_master', true, 'state_desc, state_code', null, "state_desc").then(
                function(res) {
                    if (!!res && res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            stateList.push(res.rows.item(i));
                        }
                        dfd.resolve(stateList);
                    } else {
                        dfd.resolve(stateList);
                    }
                }
            );
        } catch (ex) {
            dfd.resolve(stateList);
        }
        return dfd.promise;
    };

    this.getCityList = function(stateCode) {
        var dfd = $q.defer();
        var cityList = [];
        cityList.push({ "CITY_DESC": "Select City", "CITY_CODE": null });
        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, 'select city_desc, city_code from lp_city_master where state_code in (?,?) and isactive=? order by city_desc', [stateCode, "0", "Y"],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    cityList.push(res.rows.item(i));
                                }
                                dfd.resolve(cityList);
                            } else {
                                dfd.resolve(cityList);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(null);
                }
            );
        } catch (ex) {
            dfd.resolve(cityList);
        }
        return dfd.promise;
    };

    this.insertDataIntoFHRMain = function(setObj) {
        debug("Inside insert FHR Main");
        var params = [];
        var retObj = [];

        params.LEAD_ID = setObj.IPAD_LEAD_ID;
        params.FHR_ID = CommonService.getRandomNumber();
        params.AGENT_CD = setObj.AGENT_CD;

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT into LP_FHR_MAIN (LEAD_ID,FHR_ID,AGENT_CD) VALUES (?,?,?)", [params.LEAD_ID, params.FHR_ID, params.AGENT_CD],
                    function(tx, res) {
                        console.log("inserted into LP_FHR_MAIN");
                        retObj.FHR_ID = params.FHR_ID;
                        dfd.resolve(retObj);
                    },
                    function(tx, err) {
                        console.log("Error in setDataIntoFHRMain.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                console.log("Error in setDataIntoFHRMain(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    this.insertIntoOpportunity = function(setObj) {
        debug("Inside insert opportunity");
        var params = [];
        var retObj = [];

        params.LEAD_ID = setObj.IPAD_LEAD_ID;
        params.FHR_ID = setObj.FHR_ID;
        params.AGENT_CD = setObj.AGENT_CD;
        params.LEAD_NAME = setObj.LEAD_NAME;
        params.OPP_ID = CommonService.getRandomNumber();

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT into LP_MYOPPORTUNITY (LEAD_ID,FHR_ID,AGENT_CD,PROPOSER_NAME,OPPORTUNITY_ID,LAST_UPDATED_DATE) VALUES (?,?,?,?,?,?)", [params.LEAD_ID, params.FHR_ID, params.AGENT_CD, params.LEAD_NAME, params.OPP_ID, CommonService.getCurrDate()],
                    function(tx, res) {
                        console.log("inserted into LP_MYOPPORTUNITY");
                        retObj.OPP_ID = params.OPP_ID;
                        dfd.resolve(retObj);
                    },
                    function(tx, err) {
                        console.log("Error in insertintoOpportunity.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                console.log("Error in insertIntoOpportunity(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Function to Insert Personal Details Data
    this.insertPersonalDetails = function(setObj, pageData) {
        debug("Inside insert personal details");
        var params = {};

        params.FHR_ID = setObj.FHR_ID;
        params.AGENT_CD = setObj.AGENT_CD;
        params.TITLE = pageData.GENDER;
        params.FIRST_NAME = pageData.FIRST_NAME;
        params.MIDDLE_NAME = pageData.MIDDLE_NAME;
        params.LAST_NAME = pageData.LAST_NAME;
        params.MOBILE_NO = pageData.CONTACT_NUMBER;
        params.EMAIL_ID = pageData.EMAIL_ID;
        params.BIRTH_DATE = pageData.BIRTH_DATE;
        params.CURRENT_AGE = pageData.CURRENT_AGE;
        params.SMOKER = pageData.SMOKER;
        params.CITY = pageData.CITY;
        params.CITY_CODE = pageData.CITY_CODE;
        params.STATE = pageData.STATE;
        params.STATE_CODE = pageData.STATE_CODE;
        params.MODIFIED_DATE = CommonService.getCurrDate();
        params.RESIDENTIAL_STATUS = pageData.RESIDENTIAL_STATUS;

        debug("Params to insert " + JSON.stringify(params));

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "INSERT into LP_FHR_PERSONAL_INF_SCRN_A (FHR_ID,AGENT_CD,TITLE,FIRST_NAME,MIDDLE_NAME,LAST_NAME,MOBILE_NO,EMAIL_ID,BIRTH_DATE,CURRENT_AGE,SMOKER,CITY,CITY_CODE,STATE,STATE_CODE,MODIFIED_DATE,RESIDENTIAL_STATUS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [params.FHR_ID, params.AGENT_CD, params.TITLE, params.FIRST_NAME, params.MIDDLE_NAME, params.LAST_NAME, params.MOBILE_NO, params.EMAIL_ID, params.BIRTH_DATE, params.CURRENT_AGE, params.SMOKER, params.CITY, params.CITY_CODE, params.STATE, params.STATE_CODE, params.MODIFIED_DATE, params.RESIDENTIAL_STATUS],
                    function(tx, res) {
                        console.log("inserted into LP_FHR_PERSONAL_INF_SCRN_A");
                        pageData.FHR_ID = params.FHR_ID;
                        pageData.AGENT_CD = params.AGENT_CD;
                        dfd.resolve(pageData);
                    },
                    function(tx, err) {
                        console.log("Error in insertPersonalDetails.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                console.log("Error in insertPersonalDetails(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Updating Personal Details based on FHR Id
    this.updatePersonalDetails = function(pageData) {
        debug("Inside update personal details");
        var params = {};

        params.FHR_ID = pageData.FHR_ID;
        params.AGENT_CD = pageData.AGENT_CD;

        params.TITLE = pageData.GENDER;
        params.FIRST_NAME = pageData.FIRST_NAME;
        params.MIDDLE_NAME = pageData.MIDDLE_NAME;
        params.LAST_NAME = pageData.LAST_NAME;
        params.MOBILE_NO = pageData.CONTACT_NUMBER;
        params.EMAIL_ID = pageData.EMAIL_ID;
        params.BIRTH_DATE = pageData.BIRTH_DATE;
        params.CURRENT_AGE = pageData.CURRENT_AGE;
        params.SMOKER = pageData.SMOKER;
        params.CITY = pageData.CITY;
        params.CITY_CODE = pageData.CITY_CODE;
        params.STATE = pageData.STATE;
        params.STATE_CODE = pageData.STATE_CODE;
        params.MODIFIED_DATE = CommonService.getCurrDate();
        params.RESIDENTIAL_STATUS = pageData.RESIDENTIAL_STATUS;

        debug("params to update params " + JSON.stringify(params));
        debug("Params to update pagedata " + JSON.stringify(pageData));

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, "UPDATE LP_FHR_PERSONAL_INF_SCRN_A SET TITLE=?,FIRST_NAME=?,MIDDLE_NAME=?,LAST_NAME=?,MOBILE_NO=?,EMAIL_ID=?,BIRTH_DATE=?,CURRENT_AGE=?,SMOKER=?,CITY=?,CITY_CODE=?,STATE=?,STATE_CODE=?,MODIFIED_DATE=?,RESIDENTIAL_STATUS=?  WHERE FHR_ID=? and AGENT_CD=?", [params.TITLE, params.FIRST_NAME, params.MIDDLE_NAME, params.LAST_NAME, params.MOBILE_NO, params.EMAIL_ID, params.BIRTH_DATE, params.CURRENT_AGE, params.SMOKER, params.CITY, params.CITY_CODE, params.STATE, params.STATE_CODE, params.MODIFIED_DATE, params.RESIDENTIAL_STATUS, params.FHR_ID, params.AGENT_CD],
                    function(tx, res) {
                        console.log("updated LP_FHR_PERSONAL_INF_SCRN_A");
                        dfd.resolve(params);
                    },
                    function(tx, err) {
                        console.log("Error in updatePersonalDetails.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                console.log("Error in updatePersonalDetails(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    // Submit Personal Details
    this.onSubmit = function(leadData, pageData) {
        debug("Inside On Submit");
        debug(JSON.stringify(leadData));
        var dfd = $q.defer();
        if (!!leadData.FHR_ID) { // If FHR_ID is generated
            debug("Inside if On Submit");
            debug(pageData);
            // Updting LP_FHR_PERSONAL_INF_SCRN_A table
            psd.updatePersonalDetails(pageData).then(function(res) {
                dfd.resolve(res);
            });

        } else { // If FHR_ID not generated
            debug("Inside else On Submit");
            debug(pageData);
            // Inserting into LP_FHR_MAIN for FHR ID Generation
            psd.insertDataIntoFHRMain(leadData).then(function(res) {
                leadData.FHR_ID = res.FHR_ID;
                leadData.OPP_ID = res.OPP_ID;

                // Inserting into LP_MYOPPORTUNITY table
                psd.insertIntoOpportunity(leadData).then(function(res) {
                    // Inserting into LP_FHR_PERSONAL_INF_SCRN_A table
                    psd.insertPersonalDetails(leadData, pageData).then(function(res) {
                        dfd.resolve(res);
                    });
                });
            });

        }
        return dfd.promise;
    };

    this.truncateTables = function() {
        debug("Inside truncateTables");
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                // CommonService.executeSql(tx, "ALTER TABLE LP_FHR_GOAL_CHILD_WEALTH_SCRN_G ADD COLUMN RELATION_SEQ_NO TEXT");
                // CommonService.executeSql(tx,"delete from  LP_FHR_MAIN;");
                // CommonService.executeSql(tx,"delete from  LP_FHR_PERSONAL_INF_SCRN_A;");
                // CommonService.executeSql(tx,"delete from  LP_FHR_GOAL_CHILD_EDU_SCRN_B  where FHR_ID='318221943306' and AGENT_CD='16036'");
                CommonService.executeSql(tx,"Update LP_MYOPPORTUNITY set FHR_SIGNED_TIMESTAMP=null where FHR_ID='262881237578' and AGENT_CD='16036'");
                CommonService.executeSql(tx,"Update LP_FHR_MAIN set COMPLETION_TIMESTAMP=null,SIGNED_TIMESTAMP=null,IS_FHR_SYNCED=null where FHR_ID='262881237578' and AGENT_CD='16036'");
                CommonService.executeSql(tx,"Update LP_APPLICATION_MAIN set APP_SUBMITTED_FLAG=null,COMPLETION_TIMESTAMP=null where APPLICATION_ID='803581243777' and AGENT_CD='16036'");
                CommonService.executeSql(tx,"Update LP_DOCUMENT_UPLOAD set IS_FILE_SYNCED='N' WHERE DOC_CAT_ID='262881237578' AND AGENT_CD='16036'");
                CommonService.executeSql(tx,"delete from LP_DOCUMENT_UPLOAD WHERE POLICY_NO='C999002533' AND AGENT_CD='16036' AND DOC_CAT NOT IN ('FHR','DEV')");
            },
            function(err) {
                console.log("Error in truncateTables(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;

    };


}]);
