recommendationsModule.controller('FhrRecommendationsCtrl', ['$q', '$state', '$stateParams', 'CommonService', 'FhrService', 'FhrRecommendationsService', 'CategoryWiseGoalsCount', 'ProdRecommData', function($q, $state, stateParams, CommonService, FhrService, FhrRecommendationsService, categoryWiseGoalsCount, prodRecommData) {
    debug("Inside Recommendations Module");
    CommonService.hideLoading();

    var fhrrecomm = this;

    this.productRecommDetails = {};

    // Common redirection object
    this.redirObj = FhrService.redirObjService;
    this.stateData = stateParams;

    this.productRecommDetails = prodRecommData;
    debug(this.productRecommDetails);

    this.imgError = function(obj) {
        debug(obj);

    };

    // Function to redirect to Product landing page with respect to PGL Id
    this.gotoProductLandingPage = function(pglId) {
        debug("Inside gotoProductLandingPage");
        var dfd = $q.defer();
        FhrRecommendationsService.getOppDetailsRelatedFHRId(this.stateData).then(function(res) {
            debug("Returned data is " + JSON.stringify(res));

            var currTimeStamp = CommonService.getCurrDate();
            var myOppUpdateParams = { "RECO_PRODUCT_DEVIATION_FLAG": "N", "FHR_SIGNED_TIMESTAMP": currTimeStamp }; // Default parameters to update
            var myFhrMainUpdateParams = { "IS_SCREEN5_DONE": "Y" }; // Default parameters to update

            // If FHR is already synced then do not update timestamps
            if (!!res.IS_FHR_SYNCED && res.IS_FHR_SYNCED != "Y") {

                // FHR_SIGNED_TIMESTAMP updation in LP_MYOPPORTUNITY
                // myOppUpdateParams.FHR_SIGNED_TIMESTAMP = currTimeStamp;

                // COMPLETION_TIMESTAMP and SIGNED_TIMESTAMP updation in LP_FHR_MAIN
                myFhrMainUpdateParams.COMPLETION_TIMESTAMP = currTimeStamp;
                myFhrMainUpdateParams.SIGNED_TIMESTAMP = currTimeStamp;
            }

            // Get Product details first from LP_PRODUCT_MASTER
            CommonService.selectRecords(db, 'LP_PRODUCT_MASTER', true, "PRODUCT_SOLUTION, PRODUCT_NAME, PRODUCT_UINNO", { "PRODUCT_PGL_ID": pglId }).then(
                function(selectRes) {
                    if (!!selectRes && selectRes.rows.length > 0) {
                        if (!!selectRes.rows.item(0).PRODUCT_SOLUTION) {
                            selectRes.rows.item(0).PRODUCT_SOLUTION = selectRes.rows.item(0).PRODUCT_SOLUTION.replace(" Solutions", "");
                        }

                        //Updating lp_myopportunity flags
                        CommonService.updateRecords(db, "LP_MYOPPORTUNITY", myOppUpdateParams, { "FHR_ID": res.FHR_ID, "AGENT_CD": res.AGENT_CD }).then(
                            function(updateResOpp) {

                                //Updating LP_FHR_MAIN flags
                                CommonService.updateRecords(db, "LP_FHR_MAIN", myFhrMainUpdateParams, { "FHR_ID": res.FHR_ID, "AGENT_CD": res.AGENT_CD }).then(
                                    function(updateResFhr) {

                                        //Inserting LP_FHR_PRODUCT_RCMND
                                        CommonService.insertOrReplaceRecord(db, "LP_FHR_PRODUCT_RCMND", { "FHR_ID": res.FHR_ID, "AGENT_CD": res.AGENT_CD, "PRODUCT_PGL_ID": pglId, "RCMND_PRODUCT_URNNO": selectRes.rows.item(0).PRODUCT_UINNO }, true).then(
                                            function(insRes) {

                                                // Final redirection to Product Landing page
                                                $state.go('dashboard.productLandingPage', { "PGL_ID": pglId.toString(), "SOL_TYPE": selectRes.rows.item(0).PRODUCT_SOLUTION, "PROD_NAME": selectRes.rows.item(0).PRODUCT_NAME, "FHR_ID": res.FHR_ID, "LEAD_ID": res.LEAD_ID, "OPP_ID": res.OPPORTUNITY_ID });
                                                dfd.resolve("S");

                                            }
                                        );
                                    }
                                );
                            }
                        );
                    } else {

                        navigator.notification.alert("Product unavailable.", null, "SIS", "OK");
                    }
                }
            );
        });
        return dfd.promise;
    };

    // Signature related functions starts here
    this.showSignModal = function(action) {
        FhrRecommendationsService.showRecommDeviaSignModal(action);
    };

    // Outer wrap height set
    this.outerWrapInnerHeight = FhrService.getOuterWrapInnerHeight();

}]);

// Controller for Recommendation Deviation
recommendationsModule.controller('FhrRecommendationsDeviCtrl', ['$uibModalInstance', 'SignatureService', 'Action', 'FhrRecommendationsService', function($uibModalInstance, SignatureService, Action, FhrRecommendationsService) {
    var recommdev = this;
    debug("in FhrRecommendationsDeviCtrl " + Action);

    this.reasonRecomm = "";

    SignatureService.action = Action;

    this.clearCanvas = function() {
        SignatureService.clearSignaturePad();
        SignatureService.initSignaturePad();
    };

    this.hideSign = function() {
        $uibModalInstance.close();
    };

    this.saveSign = function() {
        FhrRecommendationsService.setProdDeviReason(recommdev.reasonRecomm);
        SignatureService.saveSign($uibModalInstance);
    };

}]);

recommendationsModule.service('FhrRecommendationsService', ['$q', '$state', '$uibModal', 'CommonService', 'FhrService', function($q, $state, $uibModal, CommonService, FhrService) {
    var fhrrecommsc = this;

    this.prodRecommData = {};
    this.reasonCaptured = "";

    /**
     * Function to Set Product Recommendation data
     *
     * @param {object} prodRecommData
     */
    this.setProdRecommData = function(prodRecommData) {
        fhrrecommsc.prodRecommData = prodRecommData;
    };

    this.resetProdRecommData = function() {
        fhrrecommsc.prodRecommData = {};
    };

    this.setProdDeviReason = function(reason) {
        fhrrecommsc.reasonCaptured = reason;
    };

    this.getProdDeviReason = function() {
        return fhrrecommsc.reasonCaptured;
    };

    // Function to get Opportunity details related to FHR Id
    this.getOppDetailsRelatedFHRId = function(inputObj) {
        debug("Inside getOppDetailsRelatedFHRId");
        var dfd = $q.defer();
        var retData = {};
        var query = "SELECT LMO.FHR_ID,LMO.AGENT_CD,LMO.LEAD_ID,LMO.OPPORTUNITY_ID,LMO.FHR_SIGNED_TIMESTAMP,LFM.IS_FHR_SYNCED,COMPLETION_TIMESTAMP,SIGNED_TIMESTAMP,IS_SCREEN1_DONE,IS_SCREEN2_DONE,IS_SCREEN3_DONE,IS_SCREEN4_DONE,IS_SCREEN5_DONE,FHR_STATUS " +
            " FROM LP_MYOPPORTUNITY LMO " +
            " INNER JOIN LP_FHR_MAIN LFM ON LFM.FHR_ID=LMO.FHR_ID  WHERE LFM.FHR_ID=? AND LFM.AGENT_CD=?";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHRID, inputObj.AGENTCD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                retData = res.rows.item(0);
                                debug("Resolved with data getOppDetailsRelatedFHRId");
                                dfd.resolve(retData);
                            } else {
                                debug("Resolved without data getOppDetailsRelatedFHRId");
                                dfd.resolve(retData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(retData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(retData);
                }
            );
        } catch (ex) {
            dfd.resolve(retData);
        }
        debug("Exiting getOppDetailsRelatedFHRId");
        return dfd.promise;
    };

    // Function to fetch All Recommendations of goals related to FHR_ID
    this.getAllRecommendationsForFhr = function(inputObj) {
        debug("Inside getAllRecommendationsForFhr");
        var dfd = $q.defer();
        var retData = {};

        fhrrecommsc.resetProdRecommData(); // Reset existing recommendation data

        var query = "SELECT a.*, " +
            " (SELECT PRODUCT_NAME || '-' || PRODUCT_PGL_ID from LP_PRODUCT_MASTER WHERE SUGEST_PLAN1_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS1, " +
            " (SELECT PRODUCT_NAME || '-' || PRODUCT_PGL_ID from LP_PRODUCT_MASTER WHERE SUGEST_PLAN2_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS2, " +
            " (SELECT PRODUCT_NAME || '-' || PRODUCT_PGL_ID from LP_PRODUCT_MASTER WHERE SUGEST_PLAN3_URNNO=PRODUCT_UINNO) PRODUCT_DETAILS3 " +
            " FROM " +
            " (SELECT CLIENT_FIRST_NAME || ' ' || CLIENT_MIDDLE_NAME || ' ' || CLIENT_LAST_NAME as FULLNAME,'CHILD' DATA,'CHEDU' GOAL_CODE,GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO " +
            " FROM LP_FHR_GOAL_CHILD_EDU_SCRN_B WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Education' " +
            " UNION " +
            " SELECT CLIENT_FIRST_NAME || ' ' || CLIENT_MIDDLE_NAME || ' ' || CLIENT_LAST_NAME as FULLNAME,'CHILD' DATA,'CHMARR' GOAL_CODE,GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO  " +
            " FROM LP_FHR_GOAL_CHILD_MRG_SCRN_C WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Marriage' " +
            " UNION " +
            " SELECT CLIENT_FIRST_NAME || ' ' || CLIENT_MIDDLE_NAME || ' ' || CLIENT_LAST_NAME as FULLNAME,'CHILD' DATA,'CHWEA' GOAL_CODE,GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO  " +
            " FROM LP_FHR_GOAL_CHILD_WEALTH_SCRN_G WHERE FHR_ID=? AND AGENT_CD=? AND GOAL_DESC='Childs Wealth' " +
            " UNION " +
            " SELECT '' as FULLNAME,'PROTECTION' DATA,'PROTE' GOAL_CODE, GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO  " +
            " FROM LP_FHR_GOAL_LIVING_STD_SCRN_F WHERE FHR_ID=? AND AGENT_CD=? " +
            " UNION " +
            " SELECT '' as FULLNAME,'RETIREMENT' DATA,'RETIR' GOAL_CODE,GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO  " +
            " FROM LP_FHR_GOAL_RETIREMENT_SCRN_E WHERE FHR_ID=? AND AGENT_CD=? " +
            " UNION " +
            " SELECT '' as FULLNAME,'WEALTH' DATA,  " +
            " CASE GOAL_DESC " +
            " WHEN '2nd Home Down Payment' THEN 'WEAHOMTWO' " +
            " WHEN 'Buying New Car' THEN 'WEACARBY' " +
            " WHEN 'International Vacation' THEN 'WEAINTVAC' " +
            " WHEN 'Repaying Home Loan' THEN 'WEAREPAYHL' " +
            " WHEN 'Starting New Business' THEN 'WEANWBUSI' " +
            " WHEN 'Other Goal' THEN 'WEAOTHGOAL' " +
            " END GOAL_CODE,GOAL_DESC,SUGEST_PLAN1_URNNO,SUGEST_PLAN2_URNNO,SUGEST_PLAN3_URNNO  " +
            " FROM LP_FHR_GOAL_WEALTH_SCRN_D WHERE FHR_ID=? AND AGENT_CD=? ) a order by DATA";

        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [inputObj.FHR_ID, inputObj.AGENT_CD, inputObj.FHR_ID, inputObj.AGENT_CD, inputObj.FHR_ID, inputObj.AGENT_CD, inputObj.FHR_ID, inputObj.AGENT_CD, inputObj.FHR_ID, inputObj.AGENT_CD, inputObj.FHR_ID, inputObj.AGENT_CD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {

                                    debug("Row -> " + JSON.stringify(res.rows.item(i)));
                                    var tempData = {};
                                    tempData['DATA'] = res.rows.item(i).DATA;
                                    tempData['GOAL_CODE'] = res.rows.item(i).GOAL_CODE;
                                    tempData['GOAL_DESC'] = res.rows.item(i).GOAL_DESC;
                                    tempData['FULL_NAME'] = res.rows.item(i).FULLNAME;

                                    var prod1 = (res.rows.item(i).PRODUCT_DETAILS1 !== null ? res.rows.item(i).PRODUCT_DETAILS1.split("-") : "");
                                    var prod2 = (res.rows.item(i).PRODUCT_DETAILS2 !== null ? res.rows.item(i).PRODUCT_DETAILS2.split("-") : "");
                                    var prod3 = (res.rows.item(i).PRODUCT_DETAILS3 !== null ? res.rows.item(i).PRODUCT_DETAILS3.split("-") : "");

                                    tempData['PRODUCT_DETAILS'] = [];

                                    if (prod1 != "") {
                                        var prodObj1 = { "NAME": prod1[0], "PGL_ID": prod1[1], "CODE": res.rows.item(i).SUGEST_PLAN1_URNNO, "IMG": prod1[0].replace(/\s+/g, '-').toLowerCase() };
                                        tempData['PRODUCT_DETAILS'].push(prodObj1);
                                    }

                                    if (prod2 != "") {
                                        var prodObj2 = { "NAME": prod2[0], "PGL_ID": prod2[1], "CODE": res.rows.item(i).SUGEST_PLAN2_URNNO, "IMG": prod2[0].replace(/\s+/g, '-').toLowerCase() };
                                        tempData['PRODUCT_DETAILS'].push(prodObj2);
                                    }

                                    if (prod3 != "") {
                                        var prodObj3 = { "NAME": prod3[0], "PGL_ID": prod3[1], "CODE": res.rows.item(i).SUGEST_PLAN3_URNNO, "IMG": prod3[0].replace(/\s+/g, '-').toLowerCase() };
                                        tempData['PRODUCT_DETAILS'].push(prodObj3);
                                    }

                                    // Pushing Other Product By default
                                    var prodObj4 = { "NAME": "Others", "PGL_ID": 0, "CODE": "OTHERS", "IMG": "OTHERS" };
                                    tempData['PRODUCT_DETAILS'].push(prodObj4);

                                    if (!retData[tempData['GOAL_CODE']])
                                        retData[tempData['GOAL_CODE']] = [];

                                    retData[tempData['GOAL_CODE']].push(tempData);
                                }
                                debug("Resolved with data getAllRecommendationsForFhr");
                                fhrrecommsc.setProdRecommData(retData); // Set recomm data before return
                                dfd.resolve(retData);
                            } else {
                                debug("Resolved without data getAllRecommendationsForFhr");
                                dfd.resolve(retData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(retData);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(retData);
                }
            );
        } catch (ex) {
            dfd.resolve(retData);
        }
        debug("Exiting getAllRecommendationsForFhr");
        return dfd.promise;
    };

    // Function to show signature modal with reason capture
    this.showRecommDeviaSignModal = function(action) {
        var opts = {
            animation: true,
            templateUrl: 'fhr/recommendations/recommDeviation.html',
            controller: "FhrRecommendationsDeviCtrl as recommdev",
            windowClass: 'recomm-dev-modal',
            resolve: {
                Action: [
                    function() {
                        debug("resolving action: " + action);
                        return action;
                    }
                ]
            }
        };
        $uibModal.open(opts);
    };

    /**
     * Function to Save Recommendation Deviation Signature
     * @param {any} sign
     */
    this.saveRecommDeviaSignature = function(sign) {
        debug("Inside saveRecommDeviaSignature");

        var personalDetailsData = FhrService.getPersonalDetailsDataInFhrMain();
        var fhrDetails = { LEAD_ID: personalDetailsData.LEAD_ID, OPP_ID: personalDetailsData.OPP_ID, FHR_ID: personalDetailsData.FHR_ID, AGENT_CD: personalDetailsData.AGENT_CD };
        var fileName = "";
        var finalHtml = "";

        debug(personalDetailsData);

        // Get Recommendation data
        fhrrecommsc.getAllRecommendationsForFhr(fhrDetails).then(function(productDetails) {

            // Get template file
            FhrService.getAssetFile("product_deviation").then(function(templateHtml) {

                // If Asset file found
                if (templateHtml.product_deviation !== "") {

                    // Get Processed HTML with Recommendation
                    finalHtml = fhrrecommsc.getProductDeviationHtml(templateHtml.product_deviation, sign, productDetails);

                    // Get DocId related to Recommendation Deviation
                    FhrService.getDocId("PR", FhrService.PDMPROOF_CODE).then(function(docId) {

                        // If valid Doc Id found then save report file and deviation related data
                        if (docId !== null) {
                            fileName = "FHR_" + personalDetailsData.FHR_ID + "_" + docId;

                            debug(docId);
                            debug(fileName);

                            // Saving file and Inserting in LP_DOCUMENT_UPLOAD table
                            fhrrecommsc.saveFhrRecommDeviReportFile(fhrDetails, fileName, finalHtml, docId).then(function(fileSaveRes) {
                                if (fileSaveRes !== null) {

                                    // Update deviation flag , Insert into LP_MYOPPORTUNITY_DEVIATION table , Insert into LP_FHR_PRODUCT_RCMND and LP_FHR_MAIN table
                                    fhrrecommsc.saveDeviationData(fhrDetails, fileName).then(function(devRes) {
                                        if (devRes == "success") {
                                            debug("All Success.");
                                            navigator.notification.alert("Data saved successfully.", null, "FHR", "OK");
                                            $state.go("dashboard.solutions", { "FHR_ID": fhrDetails.FHR_ID, "LEAD_ID": fhrDetails.LEAD_ID, "OPP_ID": fhrDetails.OPP_ID });
                                        } else {
                                            debug("Save fail.");
                                            navigator.notification.alert("Error occurred while saving data.", null, "FHR", "OK");
                                            $state.go(FHR_RECOMMENDATIONS, { "FHRID": fhrDetails.FHR_ID, "LEADID": fhrDetails.LEAD_ID, "OPPID": fhrDetails.OPP_ID, "AGENTCD": fhrDetails.AGENT_CD });
                                        }
                                    });
                                } else {
                                    navigator.notification.alert("Error occured while generating report.", null, "FHR", "OK");
                                    $state.go(FHR_RECOMMENDATIONS, { "FHRID": fhrDetails.FHR_ID, "LEADID": fhrDetails.LEAD_ID, "OPPID": fhrDetails.OPP_ID, "AGENTCD": fhrDetails.AGENT_CD });
                                }
                            });
                        } else {
                            navigator.notification.alert("No document type found for product deviation.", null, "FHR", "OK");
                            $state.go(FHR_RECOMMENDATIONS, { "FHRID": fhrDetails.FHR_ID, "LEADID": fhrDetails.LEAD_ID, "OPPID": fhrDetails.OPP_ID, "AGENTCD": fhrDetails.AGENT_CD });
                        }
                    });
                } else {
                    navigator.notification.alert("No template found for product deviation.", null, "FHR", "OK");
                    $state.go(FHR_RECOMMENDATIONS, { "FHRID": fhrDetails.FHR_ID, "LEADID": fhrDetails.LEAD_ID, "OPPID": fhrDetails.OPP_ID, "AGENTCD": fhrDetails.AGENT_CD });
                }
            });
        });
    };

    /**
     * Function to get final output content from HTML Template
     *
     * @param {string} templateHtml
     * @param {object} sign
     * @param {object} productDetails
     * @returns string  HTML
     */
    this.getProductDeviationHtml = function(templateHtml, sign, productDetails) {
        debug("Inside getProductDeviationHtml");

        var prodRecommData = productDetails; // Product details related to FHR Id
        var currDate = CommonService.getCurrDate().split(" ")[0];
        var mainReason = fhrrecommsc.getProdDeviReason().match(/.{1,140}/g); // Get reason entered in UI

        var line1Reason = (!!mainReason[0] ? mainReason[0] : " ");
        var line2Reason = (!!mainReason[1] ? mainReason[1] : " ");
        var business_type = BUSINESS_TYPE;

        var prodRecommStr = "";
        var counter = 1;
        debug(productDetails);
        // Logic to Generate Recommended product rows
        for (prodDetail in prodRecommData) {
            for (prodSubDetail in prodRecommData[prodDetail]) {
                var rowStr = "";
                rowStr += "<tr>";
                rowStr += "<td style='word-wrap: break-word;'>" + counter + "</td>";
                rowStr += "<td style='word-wrap: break-word;'>" + prodRecommData[prodDetail][prodSubDetail].GOAL_DESC + "</td>";
                rowStr += "<td style='word-wrap: break-word;'>" + (!!prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[0] && prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[0].NAME != "Others" ? prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[0].NAME : "-") + "</td>";
                rowStr += "<td style='word-wrap: break-word;'>" + (!!prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[1] && prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[1].NAME != "Others" ? prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[1].NAME : "-") + "</td>";
                rowStr += "<td style='word-wrap: break-word;'>" + (!!prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[2] && prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[2].NAME != "Others" ? prodRecommData[prodDetail][prodSubDetail].PRODUCT_DETAILS[2].NAME : "-") + "</td>";
                rowStr += "</tr>";

                prodRecommStr += rowStr;
                counter++;
            }
        }
        debug(prodRecommStr);

        // Default Life Planner Image
        var imgSrc = "data:image/png;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjVGMzkxNjAzRTRDNDExRTQ4NzM3QUE5MDc0QTM3NEE0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjVGMzkxNjA0RTRDNDExRTQ4NzM3QUE5MDc0QTM3NEE0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUYzOTE2MDFFNEM0MTFFNDg3MzdBQTkwNzRBMzc0QTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUYzOTE2MDJFNEM0MTFFNDg3MzdBQTkwNzRBMzc0QTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAAuwAAAOcgAAFTsAAB0i/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wgARCAA5BAADAREAAhEBAxEB/8QA1AABAAIDAQAAAAAAAAAAAAAAAAUGAgMEAQEBAAMBAQEAAAAAAAAAAAAAAAMEBQIBBhAAAgEDAwMDBQEBAAAAAAAAAQIDABEEEDESIRMFYDI0IDCAMxQiFREAAgEBBQQIBQUBAAAAAAAAAQIAESExQRIDEFGBImFxkbEychMzYPDBQgQwgKFSI+ESAAEDAwMEAwAAAAAAAAAAAAEAEBEgITFggIEwQFBRcGFxEwEAAgIABAUEAwEBAAAAAAABABEhMRBBUWHwcYGhsWCRwdEwgOHxIP/aAAwDAQACEQMRAAABlvm9sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACRswWTWz/XkfWsaY+pa7Wq+Po2fXzsvfOaKSJpWp/So1TG0+OGUd08U3oUqvkaVh06Ffzb8tdq7++IDOugAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAS1ytw15sOfZS5X3d8RFK1hz70S8Ze+bOucOetfPvNFJ0S8c0UnRJHp47z65ee4ee75OOSGQAACavVJvQp03C1rv9Bj6I+4qnZ44JrZtZdB+b27nu5I44Jaxk6Is+tnauOo6rYlrtWt5WjadjM189VzKv6ueuiTjnj73d84c+7O+c+vOaGSQswR9afwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE7o04ynYx89sepn6+eumWPHz2Bzr3njPrzolj44JZm9UrmXoTN6pF1LPZPDvk49NEfcXTs4gAAE1eqTehTgs67NaFOAzb1l1c+tZOha9rMqOJq2rYzKjialx3cqkfP7AsepnxdOzI24PXkXTs99mHmi7z68g8+5OaFOGoW+ueLnik7J4tEffNFJZtbOrGRo+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAndGnu745opJGzX4oJu+xDxQSx9axw15sfFl1c+Czrs7o0q1laE/pUoTPud9iD31q46298xdSwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMvWXvnngeee59c4+e4+e+Ay9Z9c4cdYmXrzwMvfD3zxiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//2gAIAQEAAQUC/APBVDP2ceuxj1mxwjH8fjqU/ngpIuGX2cav54Kmix+z4+AO/wDPBWVF2ptMSHuzSY2NKleOijaE74GMr1lQQtB6P8fDHI+QAs0Vu5JFE+RmpjRokEzh45ErDRXyDOlwsM0eXGBP/Jk0VYN/Jk0QQVxp2DxuhSGV6eGVKSN3ovLH9jxn7cn46qWaNAiQZAlPk4q8f8rTFi7UKZAafyMXKLTAj4QYceSk2dFwn8Z+g+7tPHh4UcqxSIUkqWExioozJIw4tDEZZJIVUU+JwPoXxfuyfkY/75/l5Ts084yakR3xMD5RhhvzihiyHkTK7M92ADc3JRe6s0sjSR3kxMqLILYsM9SlocYkn7HjP25Px/GxcpcuXtwePk4TzR9yLAFsusKLuTyuEjgmKZBAYSxmOSKMySZmQYE/6ORWaomxfGfow4u5k5ebJHNH5CYyeTis1T480sbKVbC+VN+3x/yppg9LvkLOZSLH0J4v3S+OLyHCMEk/y81+GZkRSy1PEYoMD5Tb1Je4gnJJGmN7JPfAbYmVG0jJjf4y/wBPqwMRXceubVyaiSaWSRaZmYgka8mv3ZaudOTaXNK7LRZmq5/AX//aAAgBAgABBQL8A4t+IrgKlAtClcBQWzcVrgKZRaFb1wFSLY6RrclFOkKi1QpepFFvR8K3p913Ki8oUUEJoi1Ri5510NSDr22q1ds6cDRFqCk0VIoC9XI+xBu+woC1I96nWofdpGtgH6zr00hFhEGBlWxg2rjZYgbEW0ZbaKL0aVb0V0MdvQ2PT7pu/ukPVuVEXWL3cRVwA5PLidL0OtM3UdVkU0imm6D7EG77QL1kawhPVhcRe6oluWNqRutMLFRcyvxrvGpeqwbRrdpJSCJjeddGQkEVF7m3h9zNo4N/QuPTQ3Pa40/ulNmdSaZbCL3aGuB1SjS+2QXoJUm3q29Xq+gY1f6eR+q9X/Ab/9oACAEDAAEFAvwDk25GuRqMm8rVyNX6cjXI0pN5WrkaQ3GjtYBiNJT1qRqRjf0fI1qXY0D0jJNFhQNOenGuopD05jTmNOQoGiwoNRNWv9ibZN6JpltURqTbRzclekR1kPVyLRnpLvV7tIaB0BvoTbRjag2gf0NNSbNsvtQdFtQPWTbkasTSjpyGlqPSlFHoUIpyKHU/Ym2TeU0guZB0U2qTapDYAUw6UDeibUi3rtCk6GXdz0SO4MQqI6KwGkmw2k2UaLb0NNQlrnel9sY/ypApTcybaCuQ1ahTbobUWpN/Vtqtrb6rfgb/AP/aAAgBAgIGPwLYnamRpP1TjuZpjowoPSzokKFZXbLWafpg9sKCh29mmmWFeNED9QayzQfbcMGKkK6G4X//2gAIAQMCBj8C2MQflMqVdWbFHLHwprzoktdYq5YsFCsjuF//2gAIAQEBBj8C/YHRgCKYz207BPbXsEJVVBsuAhd1BrYKz217BBptg08Cdgntr2CPRFrlNLBuhZhVVwO+e2vYIy4Xr1baHwi1o66YAdN2/YSyhjmxHRDC7iqi4T1tEUpu3fCDZxXLhHAuBi1FRUWTTQabaYPirCipR7KHjKqhI3znUiKrCots4T2U/mPlTJqIM3QRMqC8CwT2zMpHNuntmUIod0qENJRxQyqKSJzqROVS3VPTYW0pb89P6DeWanlMCi8wILhHp9ppwi6o6jF47VXG89cfS/r8mZxendtL05mtjF1se89MO5rRG830EM9NBVyO++Np6q0XDjGQ4bEJ+8VGwILzCu6yBBYTK+orHcNmVtVAePwNqcJqeYzT8w75+Pxifj/Y9C2+/wD5ANCgEI1RzgVs3iJx7of9x2GOqNnd7CcAJ/mKsVAEq5ydLN8mep4mTRqG+srU1n4zPaakHpAjEnGOGtyFcnGBNNT6SCixtPUU+m4xwM0lSzPzMZbb+g3lmp5TM5uTvjHE2CUweyMm+AHp2DctpjOcINQ7+bjCDcYyHCKgxiLp2H6CYdkGquFvDGN5voJ0LaZlTC+LmplrbF1BjYdmgUWtEEKm8XxI/mMXjKemq23iCHJoI4/s1K98p8C6nCM+fxGt00mzV51E/H4zTfcB3mK2hqZeJp/Erqa7lzhWyJx7odmsE93ItN9MaSmQ16pqqDXJo5T17PxvM0brmuele+etpjMj7sDG1NWqKLt5M/H8vxbYaTxGXmXmW2zlYjqMqxqemWba1t3zxnt237eUkdUtNev9g3//2gAIAQEDAT8h/oGTtFgCe8H0kvF34l3s0QO+0v8AypF4NuYuJu6SAaII3pL/ACQbTQ8PfiEcIVFlwW8lAWK/RPD34laGf3H61xMvxR09ZgDlYquiawyz3BYLVOs99G9c00vP7QWYbUq7Vrt9IKtEFcmf+Qb6SBAj2UdpevWW2loKus4yyzgiIcHd6TyQgxHaRdWSy05eRMGjU+G4icqSoN7g7lxmynAbYC1Y5vymC/Zj5wbWGWw1pqWY9JIf3KDEJsV5pj7xKmDdLhDS0Ngb/D/g8d3J4x0mQFaPWaXGoW9G78j60zKDf+c9h8ONqfuoXuUR6/4slYXU83+8XzrCOxo9YzcucXdcux++37z32HvI6Vo11g41OatYJib0HXBKBDq6PXgiwaV1HS21ekfMsZdYiMEda/iBaHWCo3sf8RKUu65/QvtPyniHWeH9E8b2jhr3gYgNcpm69AIaq57Ak8N3zvp8GJhA9UTmN7uWnQotLNneV0PNQH7XFIOURtzDcG2u4GY7T3Bf2jM9gLwHQlvtkdl6ZXRijTjLNnpL05DHCWEWFelxC0rq/wAHju5PGOktNRjzf5Kpfm2Ur0ffyhscmPPl7xAlJRPR4Z2fite80oG4wu8nbaZygpOzNgDr05M5xGvTm/aVJLrnUF+nyjYIQ+bSe+wyLvfSce8uGUdy8v8AkE2HSpyZyFffmvbhSSichyOqQsaeh3nuX4Z4Z1nsPhAqpLV0z3RFjB6BLHcREWzD9C+0/KU8llO71iDN6lVzv8TxvadPnPlkhdarNAjkwiA1Bdl7NtTw3fPdPC7c8OAbF7qj3Zjor01QeHhXRnunzEE2KTacAXZopGIUJY65ILnh/L6t2Z5Gp/1GK1bVky7i6Lcabi1pXfMHo/oh8SwDrK2IWqepx5gpq2YoUqOy0AulLw1wAqkVrPCgQcOycyXdqjFs+quKAKoa/oL/AP/aAAgBAgMBPyH+gZHOdsnZPtB4hCS2dkmBes7ROyfaXWDUvW6J2T7TB8fLoamxwZmG4wcnUV9IUU3BTmjzjCUwBQZmgIu0IRiOiALRSSmpOxFXXOdmJUH5RdprCbwi6RwHx4v+Dd5Tf5QW1KlTJ7P/AJhg5Y+iWW6ccrzZlJuesz547lqW4JBiXq4LTvwtVBTUu1DDY8LVI+htmb/OafMnyoqeVhi/UTV45R6SWQNrAdSp18ebEzzJNoKt6xmM2+SVEtDCZuhiTHOL/Bu8pv8AKWW6cJp88xkPB6DKVym8SyYSYSUA4MYk+eekSoErL1M3BUnSUNM0TZ58C7kENzRBj9C7MtG42K+SfKlCyhXN2r85q8co74dLkILyYuzpw6+dm7H7UVbhmG8Cex+rRqX6y0tFuA0xTuXxtncZfC3hcEaincv+gv8A/9oACAEDAwE/If6BpMJ3WdxmwuLdE7zG9zpO6zvMpMu5QUTvP/lIWF08FMOkI5ggPpAMMRWJrFG3cSu8QG2C6jGMt1ZaltjLbM7ssndg3EucB1AbgOoBuAyP4TRFqWNzA7zl/wDnSgPWUtdeN9OkbhylvknxQlbHUFbJcXwzu3CpcG5SuWcnhbofobkmibvLgKFznBEdKb4QWC8BCc9XOhmDy5MUi0QQmDXOUrds2jZDavL+I0Skrrwy/wAktX/40ualtINSpcqXLqvBeGfFK/PCsZQxuY64KW6wbm2auHTzWMdMo+huSVFVMaduAsRMYJpBU3w4cl6tiHOBp68G/kJpPlSlbZMtZM9x9WpKSkolRLuBX/ipXpK4VxQYFf0G/9oADAMBAAIRAxEAABC222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222ucEPPtu1c+RW222222222222222222222222222222222222221bhPjNN6OVSW221s5cS7O398Ge6l2fe2222222222222222222222222222222222fv/DS0jSnZW221tGhwBgzudj25nDSe2222222222222222222222222222222222GMriX/WR4i222222222222222222222222222222222222222222222222222222T9c+2VWH8K2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222223/2gAIAQEDAT8Q/oHY6S3gGMYR+mOgv4iQqIbWAdXoYWbpFwsjUsmkAmXHpH6IEUDnohZAROZFIpRxWSIUp0HfjgEPdUKAkRC7uV7iQ7BcP5DgEx4P2toHmv0cTfUWqyxgsdSEQYm0AWJ5jpiKQpMI7GB5BDYLVhxmECCgVHrCXdjBtPk+Zm8lcGYvkV9IW4lMpZOWkuoEoZegHBEMsBBUbApnTccsuhPQjySWVXUFAbbzES7z6CtXRcMF84iB8nTAWZ6umg13Jb8ENWoYulVXWiqyMpV2wKcijLmYr9g/cVFliRdy5oIBQl5o/ZzMfJwoeYwLvgUs6l1LjdkUWdTrF4Q07jpbiDsBhmXTkiMxWEod61GskMikzM8yeX8XVBaUp9VUTVKy9aMvq5lF2enoV6qFWKZZ1Mv7WfaeD9fDcqVSO7tb8tekoKs45v2BM9ja7Xp+1H78bbr48KaZuq31hgnSWZIVL1T1lPa8mtY+54BCpBasA85f9UaB561NChFHCtiENo+frN6Dv1DT6meA70K1gDVjeeDwkSxShc1fSKQKQaVVj7QAIIt0LyHpF96Ft+adgxGN2gPWUcAVWLLIIQBIDTXM+hfF+v8A4cBFGYTZTYGQ3QYcpiKc6KrCBQBM7ybIyPSWGfXgqpEVVmnMCz34W6Gy8O+iP0t4E90bqt2vDd0SfIisu78ODHnRzjV5bNXfW7gYmVk2Q6+SIboAVBoDkEd71pqole6p1KGxZ0gFclttSiOuBYMhlqLvUdwlUci6i952ir6v8XVFHr9Zwn2t7TDgHd12eRbLWUxdOZ/cr1lX1uN5HL9BEwDa2IScN8vL8sPWnpNiDr1TR6uJeZvu5rTfvcOu264KZu7Yeu3qGZsSNJy29CzGpwAQAqqnrZDgFbu/YysHa+YTSHoYXy4BKwb6BXg+yYftrUc3nhEPlQa8lXari7lYsFh5yfPD04XMkuMrSWxoujQwllnC14T1zwfrjB0kpaspVcN3PCusSDSyhSxUYcagQ06HRGnX0L4v1ghBAaatdXARmtul5Lt4FAoWLm0fEiNVwKrIZZ9IryBlhqyAN54K+FdeGmbHOHXmuunaIirWAvuAHrFloQLyAPLi18P6p7iCgGE6qpSAGZNS5k9p01Bx1anuHx9W7pYd3W+08PfmbtgsShpLdxSjfJS+jcqTBQpVescteUX9kRc2YWfcbgFh6RE5bJvhg9/CldHcdY1IoncWHgw0pLOjXAADNSIpd10iqqtrtgihXUtNasiSqcwt50kODmBC94eMaKqB2/oL/9oACAECAwE/EP6B0gCU7g+vtk/46OUDjQdYpMb1cXK9olRyoE/WT/lIVy23I6QVC+p1n/KS85Nnlxpx0ywAA6XWJESh6geRN2IJ2PmU+qunT/PpCvN1BAaFgGwspjrmD8ju+flliupUr79YVaJHqKQyLM/DAtfL+5h497sSHQ7DBO9gLDyTv46pKYdY6jVBGDWiTYRGqC+UNSzXP1/C/g8Z3J79FIbYYnQQED/Dr8zKDyfxNXr8caDm2+cLxDrKY7+HFVrzPtqKHFv5y4rWX7mzzfBN3nGnbZ87lYL9txlfJ4Apdb4KZ2y0XJiiWlliGdC4FtQqBfX9RKfoX4P5nv08U68C6QZeu/8AISigRiEqP3NT53ylt+JmOL1egJja0T/kSbfgesMyZAe+cxdtW4eYtjzDrEyvONIr9AtqDGUgfuY4aufJjs1u9Yi25/g8Z3J79LR6+TLJ5uD1lecsP1BZ8/nlGCO8/HCuvWTAV8iIb659dw0HTEZ8oxHmxMIfwRfp9oJcjPo78dps83wTcay/UwtwZ843AyzKAOeHx41weTdHmdO8RthPB8p758zV6/EHEzNk1RCT+qF/MCNP0L8H8xcCtvURS2DXAu2wfLC6H1QftGRe+Wj/AJPnfKbuF1HTutc6iFFnlBxN4nzzw8I7z3TKl7/OUo7HNPRlTvDXVek8Z5fVq6NTuvvFtrFG7YmzcBpDyWMWle8ENnHNd5ihT9xgjXALnwFVTcE8pmEsU7/oL//aAAgBAwMBPxD+gdxSN8oh+xn/AHGKIkZ3cAGSt1Af2Mu+dD/oM/6jEg0U5vWKSUs/6jKx58+OSNuo4K/BwCCTzd2aErUyy0n0gUtVxkO6ib01hj4FdVAC5nxFaAZs1xEqnHzBj9UciBXcY1hzZ2kGLHE7SALGyLUiC2riNIJswwW0EyVz/X6/g0ec90QgrojMucc25PeY1+ZN3p88bR5covhXSZ1r5caO2PBhoWdPKVfXCa/DbNIWHVvjUCvn9QDHPgKBzVwFVylQesG9GVURaiG1HpBs+hdfVPZE90+J8SF1Qr7f7GrbrAbzPmfC+SUNvuSzDY7rMl0Ey0oeg8EROXJPxACqKiM4FD5LAIHKVVyG/QuDSl2ZhuXfchZpoQAwfwaPOe6JjW/hKE5bZm+eUQeiIWnbha9XEQxzhuemPSIhNkERzhquUPUfyw733js+eP1NfhtnnWE6kdQjyi5PlngTKs+sMCaZ4HnPaE3enzFzcDnNGDNl0Lr4g2X9C6+qCfIOsDTVmz4k7kL8EsbXofmBWTyZnwvkmhwqD6p0vlcEtH3iZur8cPFO01eUFl2+ECxr3JaMx9gniPP6tB2TtEA5ENVQBqaoPpBFBREHfGlVWIDyfaId8EPLhRubQuDoVAD+gv8A/9k=";

        if (business_type == "GoodSolution") {
            imgSrc = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QNfaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IjQ2OUQxRkJDN0ZBNEI4QzE2RkU3MEUwOTFDQ0JFNUFEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQwODY0OEUxQTVCMTExRTZBOTBFQzUxMTQ5QTRGMUUxIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQwODY0OEUwQTVCMTExRTZBOTBFQzUxMTQ5QTRGMUUxIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ODcxRTREOUVBNEE1RTYxMUE2REFDNTFDQ0M2RjJCMzAiIHN0UmVmOmRvY3VtZW50SUQ9IjQ2OUQxRkJDN0ZBNEI4QzE2RkU3MEUwOTFDQ0JFNUFEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPgO/AwERAAIRAQMRAf/EAN8AAQACAgMAAwAAAAAAAAAAAAAICQYHBAUKAQIDAQEAAgMBAQEBAQAAAAAAAAAABgcEBQgDCQIBChAAAAYCAQMDAgMCBwoPAQAAAQIDBAUGAAcIERITFBUJIRYxIhdBI2Eyp+c4aBlCsyR0tDV1tncYgZFiYzZGVmZ2JzeHt8dIOREAAQMDAgQEAgQGCgwGEwAAAQACAxEEBRIGITETB0EiFAhRYXGBMhWxQlJyIxaRoWKyMyQ0dBcYknOj47RldaXlNmY3U4OzJTU4wdGCwkPD00RUZISU1EVVhcUmRv/aAAwDAQACEQMRAD8AkRnF6/0XphEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwim5w44cf72v6jf8AmN9gfYH2h/1Q+6vdvur7o/70Vv0HoPtv/nvL5v7js/NNNobQ/Wv1H8Y6HQ6f/g9erXr/AHbKU0fOtfCnHnPv/wB//wCgz7p/5p+9PvT1X/nXpul6b0//AKvca9fqP3GnR+Nq8s3P7HD+sZ/JD/Ofk0/oh/xj/cP78udP6/v+yf8AnT/Ryf2OH9Yz+SH+c/H9EP8AjH+4f35P6/v+yf8AnT/RyrX5NcenHHfb6mpWtmVvjr2qBkW0mhXjQKztxPEOKLBGIJMT5xVIoUCFEFzCoJvoUPwyudyYB238scU2Qzu0tIdo01LvDTqd+HiuvOzvdWLutsMb5ns24yHrzRujM/WDWwkVeZTFCKEcSCwaQOZVjET8PEg7iox1Lb8SiZVzHsnEnFoau9yRjZBZsmo9j0ZENjswfpM3JjJlX8KXlAvd2F69oWFF2jkdE10t+GSloLmiGtDTiK9YVoeFaCvOgXJt97+bWC9mhsdrunsmSvbHIcj0zIwOIY8x+gfoL20cWanaSaajSpgxy+4jTHFCxVGNXtX3xA3GHevo6xlrxq0BJWKeFQloU8f73YCidm1ds1wV9QHkBz07A7BE0J3btOba1xFGZevBMwkP0aPM00c2mp3IFprXjXlwXSfYbvtj+9+JvryOy+7cnYXDGSW/X9RWORlYpQ/owcHubKwt0eUx11HVQSa0N8Yn626jpO1P1u+2fvGOcyHsX6a+9e3enlH8b4vdPv8AifWd/ou/u9Ol07unQenUZJg+2v31iYcp63pdZpOno6qUcR9rqtry+AVO9zfeR/RzvvI7K/Vz1n3fK1nW+8Ol1NUbJK9P0UummulOo7lWvGg27/Y4f1jP5If5z8239EP+Mf7h/flBP6/v+yf+dP8ARyf2OH9Yz+SH+c/H9EP+Mf7h/fk/r+/7J/50/wBHKsvjzx9mOQe5mGpIqX9kROWdeTVoNFmlUoSHgkVhVklYoshHi49U98DVNP1KYeZyTqcA6jlbYDATZ/MNxUT9A8xc/Tq0tb46aitTQAVHEjiuw+6vdKw7Wdvpd9X0HqXgwtit+p0zNLMRSMSaH6dLNcjndN3ljdRteCs0/scP6xn8kP8AOflk/wBEP+Mf7h/flx5/X9/2T/zp/o5VBbPoExqvYdy1zPh1labYZKCcrgkZFN6Rk4MRpJt0jGOYrSVZim5R6iIikqX65UuTsJsXkJsfP/CwyFpPxoeBHycKEfIrvLZu6MfvbauP3Zi/5DkLWOZorUsL2gujcRSro36o38PtNKwTMFSVMIph8S+HNw5WSlhNHT7KmVCqgzSmrS+jlphQZGRKsdnFxMQk8jfcXfhbmVWEzlBNBPtExu5QhTS7au0LvdEshjkbDaRU1PI1cTya1tRU8KniABT4hUH3y7/YDslZ2ou7WTIZ691GG2ZIIh046B8kspZJ021cGspG9z3VoKNcR+PLPh/cOKk1AJSk6yuNTtab0IC1MI9aJMd7G+AX8XKxCruR9sfJpuk1EwK5XTWSMIlP3EUKT+bq2ld7XmjEr2zWstdLwNPEUq1zamh4gjiQRyPAgenY7v1gO9uPun2VtJj85YlnXtnvEtGSatEkcobH1GEtc11Y2OY4AObRzHOjRSq794XGpVL1nt33RZoGu+4en9X6H3uVaxnrPSedt6n03qe/x+RPv7encXr1CN2Vv6u8itK6erK1laVpqcBWlRWla0qK/FXBuLLfcO377O9Pq+is5p9GrTr6MbpNGqjtOrTTVpdStaHkpe8xeGP+6Y21+4/Uj7/++l7Oj2fZ/wBq+1fbicCfu7vumyeu9Z7306dEfH4v7ru/LLd37O/VVtu71PX65eP4PRp0af3b611fKlPFUP2B9wf9OU2Vi+6Puv7sbbmvqvU9TrmYf+jW+jR0f3WrV4U4wygYOVs85DVqCZKyM3YJWPhIePQ7fM+lJV2ixYNEu4Sl8jl0uQgdRAOo/Uch0EEtzOy2gaXTSODWgcy5xoB9ZK6CyeSscNjbjL5ORsOOtYHzSvPJkcbS97jTjRrWkmnwVqlp+JXZcFrl5Z4rZEBZLzHRKkm6oTSvPWrR2q2bi4cxcLalZVU8hIHAgkbgrGtU1lehROmA9wWjddqslBjjcxXMcl81moxBhANBUta/VxPgKsaCfELijC++bZ+T3ZHhr7EXVntuWcRtvXTsc5oc7S2Sa2EYDIxUF+meRzG1Ia4iiqV/D6D9BD8QyqV3KmETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwim7xw4Fbn5FxKVuYDFUegLKrpM7XafWd82ZuZRBca1DM0FHcqi3dk8Z3Ch2rTuKoUix1UjphNdu7FzG4Yhds0wWBJo99fNTgdDQKuAPAk6W86EkELnPu37nO3vae+dgrnr5LdDWgutrbRSHVQt9RK8hsZc06gxokloWl0bWPa8zEnPh3nm8aurWt9REtLlD/BmM5rx5Xo1Ue030XlWFws7puHd0DqVmr9BEf2dBl0/aKdsZNtfMfL4B0RYPrcJHkf2JVBY3384yW7azL7YngsD9p8N8yeQfRG+1t2u+uVv/aq43PpHYug7m4o2yYX2qWIgR8wdtlgeQ85FqnOmjKwkiQpE3rJRRMxB6gRVJQpk1SEUKYoVlmMLkMFeGyyLNMtKgji1zfymnxH7YPAgHgu0O3vcbafc/b7dybRuOvYlxY9rholhkABMc0ZqWPAII4lrmkOY5zSHHUualTlMImETCJhEwinLszhYvrbi5ReSLjYyMqpdW1OeFpSNUO1JGoXFitIte6yqWNYztZm2IQFC+3pgZQwgBuhQMabZLZrsdtmDcTrgOMwjPT0UoJBUefXxoOfkHFc3bP9w0W7u8+S7RxYl0DMc+6b6s3IcZHWjxG7+LiAaQ9xOk9dxDQCRUkCDWQldIq07THxV7W2HXI2z7BucTqlrMsEpCOhVIJ1arUkg5Ims095iQk66xiFF0FO8UheKuEf4qqRD9xC2fh+1+UyFu25v5mWrXtqG6S99Dy1N1MDajw1EjkQDwXFfcL3sbJ2plpsNtbHz5ue3lLJJRM22tiWkh3Sl6c75Q0imrpNjf8AaY9zaOPWb6+MDZeoqdYL7VLzBbIr9UiXc5PtTRLmo2JtExrY7yUkGcatJT0a+QjWiKiqhfXEXMmT92mc35M88720yWJs5L+1nZc28TC5w0mN4aBVxAq4EAVJ81aDgCeCzO2PvL2fvvP2u2M3jbnEZW+nbDA7qtuoHSyODI2OkEcMjHSOLWtPRLA4+Z7R5lWJlaLshMItwcf9fw21d0621zYXMmzhLjaY+Dk3UKs1byqDR2JwUUYLvmUi0ScF7fyiogqUP2lHNvgbCHKZm2x1wXCGaUNcW0DgD8CQRX6QVAu6O6chsnt7l92YpkMmRx9k+aNsoc6Nzm0oHhj43FvxDXtPzW5eb/HumcatvxVAosnZ5aGfUOGtCrm2vYp/JlkJGZsccuimtDwsC1BmVCISEpRRE4HMYROICAF3G9cBZ7cyzbCxdK+F0DX1kLSalzwfstaKUaPCvPiq/wDbj3U3D3f2HPujcsNlBkI8nLbhtsyVkeiOKCQEiWaZ2smVwJ1gUAo0GpMOsiCv1MIp0624Ur7G4tXTkm32OjFqU9lcn5qStUzuSSCFNbA9dAWzJ2NIzVZ42A/jD25QCqABRHoImLN8dsx2Q2xNuNtwGmFsh6eitRGKnz6+FRy8h4/srmvd3uIi2l3ox3aKXEOmZfyWjPVi5DSx127Q3+LmA6gx1NR67SWkkCoDTDylRcBN3GqQ1qnvtasS1jhY2xWX0p3v2/CPZFs2lJr0af53PtjJQ63YH1N2dMiNnFBNeRQ3T+lbPkaHvpXQ0kBzqeOkVNFfm4rzJ47AX2Qwlt63MwWk0kFvqDOvMyNzo4tR4N6jwGV8K1UjuXOptCakt9didCbZ/VOHkoVZ5OKe+V20+xP03BE2yP3JU2UfBvPXomMbwFSBZt4v3gj5C9JFuvFYLFXccWCu/VQuZV3mY/Sa8POwBpr8KVFOPMKpexW+e52+sDd33c7B/cuQhuAyEdGe26zC0lx9PcvfMzQaDWXaJNXlA0uUS8iivJMImETCJhEwiYRMImETCJhEwiYRSX47crdn8YhuA63Qqy/3uFf95CyxLuUAv23737cLL0kpGCgI+/r+Tu7+78vTp0+sk2/unJ7a633cIj1tOrW0u+xqpSjhT7Rr9Sp/uv2R2Z3k9B+tzr1v3d1+l6eVsf8AKOj1NeqOSv8AAM00pTjzrw9DvCTedz5DaST2FfEIJvPDa56DEleYuo+PFnGJx5m5xbu38ir6gwujd4gp2j9OhQzoDZebvM/hfX3wYJ+q5vkBAo2lOBJ48fivlR7i+223u1XcU7V2y65fjPQwzVne179UhfqGprIxp8ooNNfiStL/ACE8q9o8ZP0i/TUlZP8Ae339719xRC8p0+2/sr270fgkGHg6+/r+Tr3935enTp9dNv7dGT216T7u6X6bq6tbS77HTpShFPtGv1Kwvav2S2X3i+/v1vN4Pu70XS6ErY/5R6vqa6xv1fwDNNKU4868K+uLLq581uZ9f2hstnDrJ69iGFtngho5WPijjUzptqe08C7l6AuVbE7brHIY4+VBsr9OgD0gW2HXm8t4x5PJNYRbsEjtIo3ycIxQk8dZB+YBXUvemHb/ALd/b5dbM2fJcNdlZ32sPVkD5R6kF107U1rPKIGvYCANL5GcakL0FT9ogauEMM9Jt437gn4yrw3qDCX189MGUJGxiHQB7nDoUT9of8kcvue6gttHXcG9SQMbXxc7kB8yvlpjMNk8ybgYyF83pbWS4l0/iQxU6kjv3Laiv0qG/wAh+m/1b422h0wa+os2tTBsKCFMncuo2hm6xLKxIJQFU5HNbWcqlSKA+Vw3RDp1ABCH9wMP97bdlcwVubb9K340aDrH1sqaeJAV/wDtT3/+ovd2yhun6MPlx6Gap8odK4G3efAFtwI2lx+zG+TjxKpm1V8iW/dPUGs63qrHXq9dqjRZlGGma7KO5A6C751IHB25bWFiRU3ndn6CVMn5egfw5T+L7g57EWEeOtRbm3iFG6mEmhJPEh48T8F9BN7e1Hthv7dF5u7NyZVuWvnh8nSnjawFrGsGlroHkeVo5uPGq9N0O7VfxEU+XAgLPY1i7WBMBKmCrhqksoBCiJhKQDnHoAiPQM6TheZIWSO+05oP7IXxzv4GWt/PbR16ccz2ivOjXECvzoFSJyh+Q3kBqHfeyNb1JKhmrtVl2bKLGVrbx5ICivCxj8/qXSc02Isfzuz9BAhfy9A/ZlL7m7gZ7E524x1oIPTxPAbqYSeLWnidQ8T8F9GuzPtU7Xb87Y4jd2cdkxlr2B75OncMYyrZpGDS0wuIGlo8Txqts/FBqVSNpN73rMtSklNhzCtdrqpk+3srkE7OvMumhvqPppayqCicoiPQ0WX/AIdp2sxRjsp83MP0tw/Qz8xpq4j5Ofw/7hQb3v75bd7jxnbXHvJssVbiecV53EzQImu/dRW4DwfhcH6rVW9ogXVnlKahJt1LLCwsLYZOJKYfUtYewPJuPiXpw6dokcu687L0ARMTsATAAHIJrRbdQOuXWbXA3LGNeW+Ia8uDT9ZY79j5hcTy4XJw4aHcEsLxh7i4lgjl/FdLAyF8rB82tnjPHgakCpa6lFXy0ab9jvtN3ZFtO1heGAVS0LJk6ELZ66374d05P0+q8tXP3JA6j+SLH8P20h3Vw/Qvoc1EP0c7dD/z2Dyk/NzOA/MX0p9jW/8A7y2xkO3V6+t1jZfU24J4+nndSVrR8Irjzn53I+qoPKlXeSYRX9Raa3Dr41nMikopDbD2TElfEXROZpIo2zaSaLZgZBQolWaytWoyRFPx7iLsDCHQfoF8RA7Q7cmQeTIXLK15EPmoB9DmR8fkWr5fXro+/vu9ZaPAuNqYifQQRqjNtjiXPqOTo7m8JbyoWTgGq4E+RbmD8ajKbUUUmNiaqjhfrrqnM7kVrFrBJZnLmcrGEzh3I2SguDORL1Eyrp2mIgIgGec4O7u3AmJ15C1bUnmS+Hg6viS+I1+bnBZOLdH2E930mOYBb7UzcuhrQA2MQZEh0WkfZbHb3rRGDybHE4VAJVMGlv8A1j1N/tMof+tMVlPYb/pi0/nMX79q+hHcP/UDOf5Hvf8ABpFb18xf+beP3+PbN/yeh5bPd3+DsPzpvwRLg/2CfyvdP9rx/wC+vVF34vtQ/qByE+95Br5oHUcOpYTGOTvQPaJgF4mrtlPp+VRIBePkh6/RVgXIx2zxPr8/62QVgtGa/lrdVrB++cPm1XN7zN9/qt2r/Vy1fpyeduBBQGjhbRaZbhw+R/RQuHi2YqdOtOWjyxfItsTXS044V1/LxTvWFXjjvFBi29r14mtJLySKYnFmC0nKITSBFCFAy5V25e43aQBm+N3U+47hXGPLybB7DCwV8ofFxJ+HmcJBXxq0eAXNe8OxsGJ9p2J3ZHbMbumCduRuJAwdR1tfERiMmmqkcZtHlpNGFkpoNTlVPzg1F+jPJLYMA0a+lr1hfBeqqUpPGgEJalFnp2rUnToVtETJXbEn4/lahlXb1xP3PuK4gYKW8juqz4aX8aD5Ndqb9S7b9uO+/wCkHtFi8pO/XlbSP0dzxqetbAMDnH8qWLpTH5yKJWRRXmuQdo6SQQdKNnCbV0KgNnJ0VCIOBRN2KggsYoJqikcOhu0R7R+g5+ixwaHEHSeR8D9C8mzwvldAx7TMympoILm14io5io4ivNcfPyvVcxjHv5RyRnGMXki8UA5k2jFss7cnKmUTqGIg3IoqYCEKIiIB9ADqOftkckrtEbS5/wAACT+wF4XN1a2UJuLySOKAUq57g1orwFS4gcTwHFcPPwvddpGQc1NCsWHiJSWM3AguCxke7figComBMVgapKikCgkN293Tr0Hp+GescM01eixz6c6Amn7Cw7zI4/Hhpv54YA+unqPaytOdNRFaVFacqr4koWZhjpEmImTiTrlMdAkkwdMTrEIIAcyRXSSQqFIJgARDqACOJIZoSBMxzCeVQR+FLPIY/INc6wnhna00Jje14BPKukmlfmvrGxEtMqnbxEXIyq6SYrKIxrJy+VTSAxSCqdNqkqciYHOAdwh06iAftz+RxSzHTE1znAcgCfwL9Xd9Y2DBLfzRQRE0Bke1gJ50BcQCaAmnyXIkq5YYZIjiXgpmKQVUBFNaSi3zFJRUSmOCRFHSCRDqCQgj2gPXoAj+zP1Jb3EI1Sse1pPMtI/CF5WmWxV+8xWFzbzygVIjkY8gcqkNJIFSBX5rps8VnphEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImEWT0ivfd10qNU8h0vuezwFe8qZiFUS96lWkb5CGUKdMpyep6gJgEAEPqAhmVZW/q7yK15dWVrP7JwH/ZWm3HlfuLb1/m6B3o7KeehrQ9KJ0lDShodPgQVe58lG4bDoTV2rNM6kdr0mOtTCVjnbuCMsyfR1OpzOEjWVejJJNQqzJGRNKFBc6QguZJt2CcCKnBS8O42XuMFjLXD4kmGOVrgS3gRHGGgMafCurjTjQUrQmvzR9oewcV3O3nmu4O+o25G7spY5Gtmo9kl3dPmkfPJGRR5j6Z0Bw0B0moNLmNLautB8e+Xd3QQ3JoSEnlBQmXrFO4RV/qVZklZRms1eSDdck5aoiTeonWUTMsCqR27gR6G7+hgyssFgN2XrRmMEyTg8jqNljYdQoSDqe0kcq1BB+a7P7n91OxG3JHbA7nXNqNVux5tZLK5uIxG8OaxzTDbSxsIAcG6XB7OY08Csr5qxfL5/I0y68pq6yggPHGqlSJFydKcRxzxqCDuXXLH1OfmjoSMouqDl0sqBEzqHBNICIppop5W8ot2ySQ3m542s8uiPS6MjgAXGjHOoXHi4mgrwFAABo/bxe9hrW0yG3uyt3JckS+pujJHdtkAkJbENdzBCDHGB042tq4AFzy6R75Hy71Lwd456m0zAbt5i2FykSwNI98FZUlJqIhYks6wXdxEGdCplLcbBaBZ9XKqLNUoJmSMTxKESUOeWYrZW3sVh481u+QgSAHRqc1rdQJa2jP0j304kNPChFCASaI3z7kO7O+O4N1267A2rHOtXvZ6gRwyyymF4bLMDc/xWC21/o2uladQc12trnsa3vrPwh4n8jdZz144b2ZVlYK+RdJvDFlrM/hZKSbIGclhpyKvhT3CAfSpCdrR0KibfqIm8ShOokyLnZe1dw42S92hIW3EdaN1PLSQK6XNl/SNLvxXVA+RHLWYb3G98O028LXbnf8AsxJi7otLpelbsljjc7T1YZbKlrOyImssYa6TkNbHcHQ04P8ADUeTlksUjb5GTr2u6OszaTh4wqSM3NTbzzKJQEes8QcIsAbt0BUeLGSUOkU6ZCk7lO9OH7K2f+stzJJdudHj4CA6n2nOP4grWlAKuNDTgAONR0F7jvcCOzmItLTAxQ3W68k17oRJUwwwsoDO8NLS/U5wbEwOaHEPcXUZpfO2G0X8WWx7D+jVLn+zYanfEtZeIt+yCPpGSZI9i54easgPdaTMk6MQTlTapLpKn6gil2h2hOIcJ2wyNx9z2cn/ADgfKHNkmqSOelz6wuJ50aCD4DwXNGQ7le9TaWK/pA3Da12oKSuiltbAsjjeagSw2+jIRRtFAXSOY5ooZH1NVX/feDl4q/KevcboiWRkk7qCU3Vbe6aHSQCmGJJOJGXlWiJzFI+gEIV4RdEigAuqiXsEoLEAIFfbJvbbc8e3YXhwm8zJCOHT4kucPi0NdUA8SBSlQuotse5DbeZ7K3Xdy/gdC/HVhubVrgT6usbY4o3EcWTOliLHlvka86gTG5T+tfHD40eN6MfTN4WF7L3V3HoulncnN7HeTxAVSEvrjwOrSFZQjJyqUx2xHiBxMUADyKgAmGeXW3e3G3Q2zzUjn3haCSXTF306YeDQeY1D6yuXsJ3b94HdySXcPbi0jg26yUtDY4bBkJofsdbInXM9ooJDE8AE10sqAMt55saZF8CKNG67khmKHHyesGNPlDOfWGfVxpFvm8Q4M68LcVzqMkydTCQhhH8QAeuZe+WWcWxYI8e7XYtdCI3VrVgaQ014V4fJaP2yXO4Lz3O5K83XD6fc0sORfdR6dOid0jHStDauoA8mgBIpyJCoFgpMIWbhpkzVJ8WJlY6TFksPai8Bg7RdC1VESKACTgEuww9pvoP4D+GUPBL0Z2TUDtDw6nxoa0+tfUHJWZyOOuMeHmMzwPj1jm3W0t1DiOLa1HEcRzXoe3nE1j5FdTVFpozeUHAyEUu5nZnXk07WQUkHa7JsDZlc4VgsrMRy9ceInBB0Vm9am8yp0vIHYfOgM3FbdwcVEzCXrI5GEudE4kVJAoJGjzAsPJ2lzeJIrwK+U/bW+zPtP3xfz9ydt3NzaztbDFfQtDgxrXu1PtJXgRSNnaRrjMsUg0sa/SdTVWPtHjrzS4ya/tcPIPpxzp2ZaHjLUrRrM4n6UZgMiDjzSMMsRrJwTR266CZ2oxalOVcUlFP3p0xrbJ7e3jtqwlhkc84h4o/pvLo6VrxbwLQT+MWt50J4kLsbZfdj29d4t02N/ax2zN/W7+pbC8t2wXevp6aRygujmc1vKJs0hBYHtb5GvHc8GuFcXyMTsWwtkTMhAaqp74Y1YItduwkLFMtmiMo+ajKvW7lrFQkSwXRO9WAgqnKuBEzJiBlU/bZOzYtwiS/yL3R4uF1PKQC9wGojUQQ1rQRqPPjQEcxr/cl7h73tM602rtK3iut7X8fUHUa57IInOMbHdNjmukmle1wiZXSCwueHijHTQrugfi+3fKuNWarnlWd/Km7I0fwdn2SlKP1Y5o7F0rDKX0H1OsZUyIHdHBiksBk0u8ggj16zC3wPbPNSnF4uQi/40LXzajQGunq1jfy1HSDwFRwXPeW7oe8ztxYs3rva1bJtclpcya3x5jYJHN0iUWWi6gqXCMGZzaOdpcDJSkJdfaNneO/P/VmsZx4lKhGbBrb+FnEERbIztfkyKOI2SK1MqsZosId6K6QnOCTlFQpTnIBTmhlhhJ9v78tcbOQ/TcMLXUpqaeINOND4EeBBFSOK6K3T3Ixvdf2u5veWNjMBmxVwyaEnUYZ4yGyR6qAOHJ7HUGqNzCWtcS0bj+SCKYTvNnUcHKoeqi5mo6wipJr5VkPUsJHYdqaPEPO2URcI+ZusYvcmcpy9epRAeg5t+4kUc+87SCUVifFC0jlUGV4IqOPL4KAe0i+usZ7dc7krF2i9t7/IyxuoDpfHY2zmOo4Fpo4A0cCDyIIWDfJLx71BoKd1Oz1LUftNtZom2OZtP3+zzvrV4x5BJMT99lmplRt4E3iodERTA3d+YBEA6YXcXAYnAz2rMVF0myMeXeZ7qkFtPtudSlTyopL7Re6m/O6GNzlxvm/9dNZz2zYT0LeHQJGTF4pbwxB1Sxv2g4inClTXJqrxk0tM/HLK73d1JUNrso+zPkbYjYrMQRUidjvINompBjMHrQpe0olQN0ZAYwfn7gU/PmTa7bw03bx2cfEfvVrXnXrf+LMWjy6tH2eH2fnz4rTZrvF3Dx/uzh7ZwXzf1Iklt2G2MFueEtgyZxE3SFxXqkvH6WgPlpo8qljwheU6O+PW+v8AYcTITtDZhttzcYWKUOjJy1ZRjQUmo6PVSkoZRN47jyqETMV22EDmDoqT+MEq2W+zj2BPJkGOfYjrmRrebmAeYDi3iRUDzD6QqP8Acbb5+791OLttqzxW25pPuxtrNIAY4rgyUhkeDHKCxr9LnAxSCgPkdyNZV2V4obX3ZoqraB1pd9f1CeuNbrOwGtpmHq76YLYbbDRxTRaji63hRj6eLWWL5CLo9TqAPj6k7hra9O1spmbG1wNtNBaPmYyUPcSXa5Gjy1kkpRpPGo58uC7E26zvfsjt1uXNd0MxjspnrXH3FxYutomBkXQtpZP0gbaWYfqkDDpLHcGnz+agzvnrxioOhth6wpuo46dUNdYNwuozk5RSYePZhScJFMG7QyiaIJ+UTlKBQD6mN+OZ2+ttWGDyFtZ4lr6zMPAu1Eu1aQAo17ZO8m5+5u1MzuDfctsBjrloDo4xExkQhMj3OoTWlCSfADkpfMeFvDPjFRK5Pcu7SrM2ayImbnaKStwaRCcqQrJd82q8JrsiVrlEYQFipLO1jqoCCwHOmj3pFLLWbN2ftqxjn3ZKX3MgpTVIG6uBIY2Lzu08i41HGpAqFQ1z7hvcD3j3Nd4zsRZC3w9m7UHCO1dKYzrax1zNfE20ZmoXMiaGv8ha10ml5Ovt+cHdDXLSUvyE4i2Nd3CwEbIzshWfdJKbh5CFhEjLziUcaeIa2QdjiGhDrqNX6iplSEAgJpHMUx9fndlYK8wr8/tOQmGNpcWai5pa3i6mr9I17RUlria0pQHnKu2HuP7m7f7iwdq++1o1mRupo4WXHTjhlZLMdMJk6J9NNBK4hjZIWtDSS4ue0ENwvh/wi07sjTb3kHu+7zLCoxytmM4g4xVvBR0bHVoVUX0rOzp03792kIEOoRBqm1OUxCdVFe4U8w9pbLxGRw7s/mpnttGl9WijQAzgXOdxJ+IDQ3kOJ5KQ9+vcZv7aPcCPtX24x1vLnpRb6ZpA6aSSS4oWRwwgsY08Q0vkdICC7ysoHqTFV4x/G3yYa2CraHlHkVbYdiq/WewM7spOYZNwOmzTkfY9oHctJmEQfukSrnbIgHVQhPOmZQo5JLXbXbrcjZLXBuLbtja1a6bUByrpmqHNBIqQPEDUKhU9m+8fu57PzWua7mQRz4K4kDAyaHHmJ7qFxZ1scGuimLGvLBI7k1zum8NIUCeOfCpTZ/JfYGlbzPrRkJqNxOHtr+EBBKSn0ImcRhI9CCF2V2RgSbM4K58yqS3hbgJRL5DF6QXb2zTk9yXGGvZC2C0LuoW0q4NdpAbWtNVa1INBwpWi6c7s+4dmzez+L7ibbtWzZHOshFqyapjgMsJmeZtOkvMOkx6GubreQQdAKkPc0vi51/e5/Tdq1Lt6Nm6xKPKpN7CNI29SPZyTADoOZgrYmwBfO0gXJ1TURr6jdcBKokkdAxTDILwdsrC+kw91aXbZonFjpayUBHN1OrU8eRERB5gFvFVTt9/vP3Rtm13/AITOYGbHXkLbmGx6dqHvjfQti1Gx0NND5mvvmvZQte9sgIFU96TpqVztCWvFptzRU5ySTqTixg3LOrwBHShYxaVK1SQbg8VbAUxwKmT8fqUB6hlXXwsxeSjHl5sQ89MvpqLa8C6lBWnyC7b207cD9v2T91tt2blNtGbltvq6LZy0dQR6i52gOqBUn6SOKxTMRbtMImETCJhEwiYRelD4r/6LKf8AtEt/96h86M7Yf6sf+0Sf96vkR71v99J/yTa/hlUZ/mP/APzn/wC73/1hkb7vf/Lv+P8A/Eq4PYF//Wf/AGv/APIqQ3xb6e+wtCuthSLXwz23Jc0smdQnYsnUoA7qKrqBgMHcBXLs754QwD0URdJj0+nUZB2yxHocEchIKT3b9X/Ftq1n7J1O+YcFVPvR37+s/c1m1bR+rGYKDpGhqDdThsk5H5rRDERzD43jxUcPlQ3xJwmxdO68qkiLaQ147ZbbkFEjiYqFpB6KNNBUpTFEjqJaMXK/T8RTfkHqGR3ufnJIchZ4+1dSS3InPyfWkf1tAcfocFbfsq7Z2eR2nn91ZuLXa5WN+MYCOJttFbunxbK58bPzoXBXFUC3w20NeVK7xyaS8LearEzqbVXscEK2m41FyvHuiGKJDqNhXMgsQwfQ5TFEPxDLdsLuHJ4+K9joYZ4mupz4OFSD9FaFcC7owOQ2Zuu+25dlzcjjb2WEuFWnVDIWh7TzAdQPYQeRBBXlC5PahV0bvTYWuQRUTioubVf1hRTuN56pNFLK14wLG6guo3jXZG6xwHp6hFQPoICActblxJwmbuMfQ9Jr6s+bHeZn00BofmCvt92a34zuT21xW7C4G+mtwy4A/FuYT0p+H4odI0vaPyHtPIgr1s1n/o5X/wDQkV/kCGdV238mj/Mb+AL4X5j/AKWuv5zJ+/cvL/zMr0tbubezqpAtjPZuy3uswEO0L17nMnLwlbj2KACACIeVy4KHXp9Ouc07xt5bvelzawCs0k7GtHxc5rAB+yV9l/b7lbHBe3TDZvJvEeOs8ZcTSu/JjimuHvP1NaSvS5q2gRGqNb0vXUN2BGUytxkIRx2gl6xZm2KEhKLl69pXEo+FVyqP4eRUw50djLCLF46HHw/wUMYbX40HFx+bjUn5lfH/AHpui/3vu7I7syFfWZC7kmLa10B7jojH7mNmmNv7loVJOiuVo2H5G7Ha1ZETUvbMlI6oiRMp/gwQrH07LXbpBER8abiSlYJp16D+U8isICPcPdTGD3T6juHJdF38Tu3GBvw0igiIHxLmt/s3fFfRbuV2RGK9pdphI4v/ANhwcMeSloPN1X6n3zSeZbHHNLT4iCOtKClr/LzTobz4/bAo7ZqDmwJxg2OngBe5YLVXQNJRbduI9QTUlypqMDG6D0Sdny092Yj77wFxZNFbjTrj/PZxaB+dxb9DiuIOw+/z227pYvccz9GLM3p7r4emn/RyOd8RFVswHi6Jq8kwgJREpgEpiiIGKICAgID0EBAfqAgOcpr7oAgio4gqQnFXUht3b81xr9ZuZxDPZxKVtIdB8ZarAFNMTyap/wAEfXMWZmqZh+nnXIHQREAHf7XxRzWdt7AisLn6n/mN8zv2QNI+ZCqvvZvpvbnthlt0MdpyEdsYrb4+pn/RQkDx0PcJHAfiMceFKr0C8meSnE2k2Rnp3kDBNrMZKLjLagxkKM0uUDEndqSccwMBDFdOI6bI0RVEBTQAxWrgOh+igly+tybj2rZXAxGeYJKNbIAYxI1tagfEh1K8h9k8+K+W3Z7tD3x3FiJN/drbl9mHTyWpey8daTShojkfx8rZIS4tHmeQZGHy1YCvpxp5J8SrzY3umdBV9rWwcxMpanEexorOn1+YFqaNjpADpAVsvJTKjJdMw+VuYTNUDdT9EwLn825uPal7cOw+BjEdWOeQIhGx1KA/Al1Kcx9kc+C/Xd/tD3z23iY+4HdC6fd6J47Zr33j7qeLV1JGUNXCOIPDgNLxSR4o2riVR7dNTKaR5sxmuionSi4nddKeVsTdwgrVpqzw8xXRBUfosdCKepoqmD6edI4fQQEApW8xRwu9G4+lImXsZZ+Y57XM/YaQD8wV9Htv74Z3G9us27C4OvJ9u3bbinhcw28sU/DwBkY57R+Q5p5EFWA/MX/m3j9/j2zf8noeT3u7/B2H5034Ily57BP5Xun+14/99erdHCiMrHFPhc83RsFN0yTtY/qPYDtWya0oaEfuGkFR4tkkqs1TcmkGiiLluQxyB5ZIwd3Qc3GzIrba2zjmb+rRL+mfQVdpJDY2jlWooRx5vVe+4i8zPe73Cx9vdrFkjrH+IQBziI+sxrpryR5AcW6HB8chAPltwaLE2fMH43IGZJfYbXcQ2uSDs0s3fRelmDSwt5UTmXF80fHaNWTWUOscxhXI4IcTiIif69cxWbu7dQTeuht2C8B1AttwHh3OoNAA75g/Wt3cdhPdzk8edsZDLTv2+5nScyTLPdA6OlNDmBznujAAGgsIpQafBY58n9Gido6R1XySp5ReM4lKLI6fETAqjmi7CatH8C+dmKKgFLHTRkEiEAfynkz/AFHMbuXZRZPC2u4rTixgbU/GKUAtJ+h1B9Lytv7NNyX2zO42b7RZ89O4ndIWsJ4NvLFzmTMby4yRa3E04i3byVEGUcvpgp8bt/oIcJv9Ob//APkV/k7zX+o+F/Puv+VK5i7df9ZjuL/NsJ/gDFAfIIunVPj4z/6XlC/0He/9T5jJ323/ANbIPzJf+TcuYveD/uHyf85s/wDColAfIIunVaF8esmtD635cSaGxCamWZ1TWqiexVItaaJVRGbsxPcDRbcii7wDgbw9pSiP7zr+zLM2BIYcdlpW3HpSIof0uku0eZ/HSOJ+H1rjP3U2cd/u7YlnLiTnI5L7IA2AkEJuf0NudHUcQGUpqqT+LTxUXuVN3m7bcYBrJcgw5GMIivArH21OtuashEOJJ+6CQgSR7xu3cqqJkYILmVHqQwLFKHQSmyM7ovZru7jbJf8A3gxkfCTQWBpJNW0IB8Aa/NXN2U25jsFgLqa02r+qV1PdUfam4bcmVsbG6Ji9jnNAJe9gbwI0kngQpA/G4+CMs/IySNaz0Msfxpvb4bwlGLTSlMBpIQbgbWnDNwFxLHroJ+sBqn+dcUewv1MGb/t0/p3ORk6vQ042U9Smrp0LTr0ji7R9qg50oqt93NsbzD7SsxYjJmXeFmz0ZkEIu9TJm+mMrvLEJ69IyO4M16jwCwPlbsk9uq9XiUOYsjyWapzy8i4r7vVUzrxKtuG8es2azBXcqmUsidwm+WQBMg9SAImH8QzB3TkTd20UTcu7JN6hJYYHRaCBQOq7nWpFApN2R2i3BZm9vpNgQ7PnNqI2zsyUV8bhrnhzotMR/RhpYx+o8+Q8VBnIQukkwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCLsYeVewUvFzkaoCMjDSLGVYKiXuBJ7HOUnbVQS/TuAi6JR6ft6Z6QyvglbPHwkY4OH0g1H7axL+yt8nYz427Gq0uIXxvHxZI0tcPrBK9EO5Ne0/wCSXj7SbzrGxRkRfamd25YsZVYTIRExKNGJLTRrR6RNw9jRVWj26rZ2VJQDlRTUKQyS3cXoHMWFp3FwEN7jZGsv4qkB3JrnAa430qRyBDqGtAaUNV8pe3+6s/7Re6eR23vK0mn2xfBrXvjHGWKNzzbXlvqLWSUD3tkiLmkF72lwfHpMWdJaI+UDULYutdfO4fXtHXsLyQWlpSQ05ZoNu5ematHcumDtpa7iRk4bsyKg3TagIdBHwlVOcBjGFwfczEt+7bAst7IyE6nG3e0E0BdxD5KEAGgb9QJKuruL3M9mm/Jju/dLLjK7kbatYIo2ZW3mc1mpzYjpdbWpe1zi0vdIfAdQsa2me/L/AGOBVhtOVRKWYq2RrL2WcdQqS5FJBpEOGMczbP3aBBMZq3dukzkRFTt8opn7O7sP253dq4gMNnah7Tch73FteIaQACR4AnlXnQ05FRj2F4jJsv8AP5x8EjcQ+C3hbMQQx0rXyPcxrj9pzWkFwbXSHN1U1Nruzlrq2b5t8bNV3LRLqJl3LBdva2VZM/aRaMk3kIc8dJQrd49XbxsfPV5+XwCg6OgmXtWKZQhilKfdbrxk29NuWt5gyx7mkPDKhtQW0LQSQA5h4UdQcxUeNddjN6Y726d3c3t/uWyeCKVrrZ9xodIY3MlEkcrmMDpHwzsOvXGHuNY3BjgSW9XwD4/XviZStx7B3wvHUhpMNYd45hlJqMlSQsJSkLA7eT0u/hH0lDGO7JMCVumisoqUiZu7oZQpA8tiYC+2rZXl/nC2Bjw0luprtLY9ZLnFpLeOrgASeBrzos33Qd0ttd8txbf2t2ybNkp7d8rWyiGSMzTXZgayGJkzI5aNMVXucxrSXClQ0uOGfGzfqrsSlckNXIP0oOyWy9W+8s0xIQsn9tXqGZQJXTRLyJ+rCuu48BUIU4Akd0TqIeQBzD7dX9rkLPI4xrtFzLPJKPytErQ2o+Ogjj8NQ+KkHu72vm9qbi2jvOSI3OIscba2bzU9P1FnK+bS80Onrtf5SR5hG7gdNFGrSnxx8lKnyDosnY4uHh6VRr3B2dxfmNnhHjWUY1eXZzCacLENZH7qSezHpASR9SyblRMYTKCAFADRzDdvNx2ufgkuGsZZQTteZQ9pDgxwd5Wg66upQaminM/O3+4nu07Q5ztZkrPEzXFxuLJYya3bZPt5mujfcxPiJmldH6Ysi1andOZ5eAAwEnhMrcW+9eVr5EtJRUlMx6SFdos7SrRLHWQKzr9hvhXa8GwkXSiqaTU5BIz8xjD2oJvwEwh0P2zDL53H23cGyike3THA6N7qijHy1LQT4fi1+Ad9K5/2D2x3XmPajuO9tLeV0l3kobu3iAJfPBZaRM+NoBLq1l0AcXuhIH4tdCc6OCe+9nb2sW1dWQjG7w1za14zqO+5K/BSsC+hK7HV5ZFVOzSUKzcR6yUOmqkdFdRQDLGKYhQL3G0O99j53JZyTKYtjZoZgyo1sa5pawM/HLQR5QRQk8aU4KzfbZ7lu2Oze2lpsnetzJjchj3z6X+nnmjmZNPJOCDbxyva8GVzXB7Gto0EOJNBsXmNUJ7X/wAceraTaWibCx1dbVkLNsknLZ6RpJsYt+i7bFds1VmjnwLFEonSOdMwh1KYQ6COw3faT2Hby1sroabiIwtcKg0IBqKjgafLgop2Bz2M3R7tM1uLCvMuJvW5KWF5a5hdG+Rha7S4BzajjRwDh4gHgqJYSIdz8zEQUf4fXzcmwiGXqV02zf1ck6SZtvUOVRKk3R8yxe85hApC9RH6BlIQxPnmZBHTqPcGipoKk0FT4D5r6W5G+gxePnyd1q9NbwvlfpBc7TG0vdpaOLjQGgHEngFY1sP4u+TFLlmymuywWzGIGaOG0lCWCKqMtHuikFVQzlnbJSHRRMzdpdE1GztcxyiQ/aQROROwsh2z3JZyg4/Rcs4EFr2xuB+Ye5vI8iHHwPDiByZtX3ndntw2L2brNzh7mjmujmgkuontrQaX20cpOppq5skTACHNq4aXOtK4l6825qDQV3YctLUhIQhkH7sISx2JO0lqVGTghRnIyZnxcO2qjRyQVABog5ctkEifuz9VTELZ21cflsTgZ2bqlDoaE6Xv19OPT5g59SKHj5QSAOR40HF3fPdWxd+d0Mbc9jbJ0WRDmN61vAbb1V4ZqwyRQaWuDmnSeq+OOR7j52+QOMc/j3kaxtjibuvQMHMlgbQ5U2HGAVwdM0k1gdhV/wBrhLKdJExFHiTNydRBwKf8QyBQES+RPrHtgyW2V2re4GB/TuT1W8eYbK3S1/DnQ1Bp8PmFbHuotczsfvjt3uhkrc3WGYLGSrQem6axn6k1uCahhe0NezVzDyQDofTRvFn4++RtC5EUi4XuEiqzUaFYvfHFha2iBliz5I9NyDZnDR8VIrzBUpVbsIcXrdn2N1DCYvcHjHSbY2FuGw3BBd3zGR2kEmovD2u1UrQNDSXeY/lBvAnx4Kye9Xul7S7n7UZLA7auZ7zO5O06LYHW00XQLy3U6V8kbYqxipHSfLV7QAdJ1jPN9Xiv3H5Q9FR8A5bvT0Nek0qectjprJhYG81Z598z8yYiQysajYEW6xOomRcJqJm6GKJQzs7e2953MsY4CCYDHG4j8sOe4j6g8A/AgjmFGe2O28pgPZluW6ybHxtybby7ha4EHoOht4GPoeNJDA57TydG5jhUEE6z+SWXZ1/mpqiekDGIwhKdrOXfGIUTGKzjdg2t65MUodRMYEUTdA/aOa7uLMy33lazyfwbIYXH6BK8n8Cl/tFsJ8r7eM5jLUA3VzkMhEwHlqksbZjf2yFLH5FONO0uSbbUdv0rHRV2bwcdPJOWSFggopR1HWQYKQiZmLkJuRjod/Hqosj93a4A/QyZiAcphEsp7g7bye422l3hmtmaxrqgPa2ofpLXNLiGkcPj8KVrwo/2od4Nl9oZc7ge4cs+OluZoS15gmkDX2/WZLFIyGOSVjwXilWEVDg4tIGrgbDpDzjZ8YstrXYr+ObWtxGOIj0jNyVyirYrhsBxYm8IxXIAkeuI6McKGWMTqQQaqnKIkKBs/GQsn7c7avx2Qc0XRaW0Br55JS8NB8SGk18OBPJZW1Nx2/d33kQbv2nFM/CNmbLqc3SRBa2LYHTPB4sbJI1oYDR36RjSA40XX8Ha+52d8fG09c1lyxNZJs+26g3TeLig2bTc/XGpotN8qQip27dYsmiYygEN2kMIgA9BDPPZVu7J7BusdbFvqH9eMVNAHOYNNfgPMOKyvchlYdne6fCbszDJBiLYYy6cWCrnQwXDuoWAkBzh03gNqKkUqK1Vale43bp0BvnjcrtumDU07VuKkJwJ/uGqzwPjwtyqppIo/bM5M+kFuWUQEPP4/J3/AJO7tN21xb7dzOBzmOOWh6QlvI9PnY6umRlfsOdSmoc6V8ORXX2V7udve6PbLdzNi5D1zrLAXhmHQuYdAltLnp/yiGLVq6b/ALGrTTzUq2s7fkls8fSOUPFa6SyPqIuoqwtnkkPEK3nj4DYcfKvEfCACKvkbtDF7f7rr0yc9xbmOy3Ni7yUViiLXkc6hsocf2guaPaLhrrcfZje23rF2i9v2zW8bq0o+exfEw18KOcDXw5rafyCcY9icn2mrNmaNPE3ltHQTtn7MlORMWEjETq7OVjLDBycy+YQjpBRITAuU66KnYCQpgp1MBNnv3bWQ3My1yWE0TtawjTqa2rXUcHtc4hpHxqQeVK+EL9rXePanZqfNbP7kCfGzS3LXdUwyydOWEOjkgmjiY+ZrgaaCGObXWHaOBd2+idcTfDPhLt5fdj6PiZSaJb7IeupyLWQSi307V42qwVZB00cOI1/OTMiwTAwtVFEf35C94gmY2euDx02z9mXbsy5rJX9R+ioOkuYGNZUEgucQPskjiOPAlYHcvduO9wXuKwMXbqKWeytza24nMbozIyG4kuZrjS5rZGQxRvdTqNa/yOOkag1a14/f/wArNp/+Gtyf355muwP+6+6/tdx+EqX90v8ArsYX+eYr8DFF34mv6TFh/wBkFo/1oo+RntX/AKySfzR/7+NXP74/9z9p/l63/wAGvF3REORqfPvkNOcaGbZ/bq/LW19Ox8ooySg5iti8jfLAynuDlk1XNKSRG/p0wXQV86ZVCqJ+MTl9g3cI33kJ9uAOu43yFwdTS5lR5XVIHmNKCoNRUEUqte6XtK72wbUxveCR8WBuoLVkL4w8zRXGiSk0fTa9w6cZfrdoe3Q4tcx2oNM8tKcgORm4b9Gav37w6cxFWcovTTlwk61PsajDv4qNdmF+RvcoyUhJhCSfAVs3RbvzrlI4E5DrlIbrOcNntw5e/bjM7iCy1IOqRzHCNpaDxpI1zXAngAHVoaguAK5l7idru02wdsTbz7Yb+ZPmmOYIbWO4gfdSskkb5C60kjmidGysj3vgDCWBrmxFwpT1zwoFB1pyavdX1w0ZRUAmhASS8DGiX2+vy8tCs38lFskS9Ss2wqLA4K3AexuC/jIBEylIWo982FhjdyT2uODWwUaS0cmOc0EtHwHjTwrQUFAu9/bPujdG8OzuMzW7ZJJ8oXTxiaT7c8UUz2RyPP47qAsL+b9Gtxc4ucYe5EVfaYRMImETCJhEwim9oHnxuHjnQS65pFb1rKQhZqSnQdWqHtD6V9XKFbFcJivEXGDaemIDUvYHg7g6j1MP06TXA76y+3rD7vso7Z0Osuq9ry6rqV4tkaKcPgucu6Hti2F3Z3R+tm47vLw5H08cOm2lt2R6Y9RaaS2sztR1Gp105UAWDcluXGyOU/2X+oMJSIf7E+4/aPs2Nno/1P3P7D6/3H3uy2HzeH7eR8Pi8Pb3H7u/qXtwdx7ryO5+j69kLOhr09MOFdemtdT3/kClKePPwknaDsVtHsr94/qtc5G4+8+h1fVyQv0+n62jp9G3gpXrv1atVaNppoayKrXyn76qNcgapA0DR7KErUNGQEO0LW72INoyIZIx7FABDZBQHxNm5S9en16ZIbbufnbS3jtYILIQxsa1o0S8GtAAH8N8AqozHsq7Y53LXWbyeU3HJkby4knld6iz80kry97v5B4ucSoL7c2laN1bFs+zrkLH7itTtB09RjEnKEYzTaMmsaxYRqDx2/dIsWLBmkkmVRZU/aT8xjD1EYRlspdZnIS5K80+olIJ01DRQAAAEk0AAAqT9K6T2LsvC9vNp2eztv8AU+6rKMtYZC10ji57pHvkcxrGl73vc5xaxoqeAA4KV2lfkU3novXEFrCswet5yv1w8l7U6tsPaX8wihJyTqVVZmdRNyhGp2rd29U8IeDuIQQL3CAAASnDdwc3hMczGWzLZ9vHXSZGvLqEl1KtkaKAk04fJUj3E9qHbbuVu253lmLnL22UuxH1G20tsyIujjbGH6ZbSZwc5rG6jroSK0BJrpHkZyTuPJuzQduvNbo8HOwcKNfI6pkdOx3uMaV6u/apSZZqx2AVjsHDtcUTJil0BYwG7vy9ul3DuO83Lcsu76OBk7GaaxhwqKkjVqe/kSaUpzPNWL2m7Q4Ds5h7nBbau8lc425uOuW3ckMnTk0BjjH0YIKB7Ws1B2riwEU41lyx+WXkXHsmbBGl6UMiyat2iRla5ejKGTbJERIZQSbHIUTiUgdRAADr+wMlkfdTcMbBG2Gyo0AfYl8OH/DKirn2Odprq4kuZMhuISSPc40ns6VcSTStgTSp4cSooI8n7qTkUryacVqiSF6PKHmk4V9GTylNbSfsQQDd0hGp2VGY8jFAoLo978/Y5KB/qAAUIsNzXv6wncjo4HX2rVpId0wdOkEDXq4DiPNz4q739mtuu7Tjs7FeZOLbQhERmZJCLt0fW67mmQ25io91WPpCKxkt4VJUmbX8qHIu2Vex1Zet6lh0LJBysEvKwcDcm8zHISzFdgs9inDy/P2reRbJridE6iCpSKAAiUenTJJddz9w3dtJaujtGNkY5pc1sgcA4EVaTKQCK8CQePgqewfsp7T4PM2maju85cSWlzHMIpprR0UhieHhkrWWTHOjcW0eGvaS0kBw5quSKlH8HKRs1FOVGcnEP2cpHPEh6KtX8e4Tds3KY/sUQcJFMX+EMryKWSCVs0RpKxwcD8CDUH6ius76ytclZTY69YJLOeJ8cjTycx7S1zT8i0kH6VZ3/a4ckP8AsVpEf4ftu9/X/i2SAZZX9K+4v+Bsv7CX/wAsuOP6ivaT/wCo7j/94sv/AIBVr2+xq3C12S2LxcTCuLNOSk84iYBB02hI5xLPVnzhpEtnryRdNo9FZcwJJnXVFMnQvcIAGV1d3Bu7qS6c1jHSPc4taCGguNSGgkkDjwFTRdd4HEswGEtMHFNPcRWdtHC2WYtdNI2JgY10rmMY1zyGgucGN1GppxW5OOfJO48ZLPN2+i1ykTk5OQf28dzc46ckSx8ad82kHScaWFsVfFFR64ZI+Uygq9SpFAvb+bu2+3tx3m2rl93YxwvnezRWQONBUE00vZzIFa15KAd2e0WA7x4a2wO5bvI22NtrnrhtpJDHrkDHMaZOtBPUMa9+kNDeLiTXhTBdw7YtW8NjWTZ9zCPJYbOszUdtohF02iWSMfHNIpkyjWz17IukGjZkyTKUp11DCPUwmEREcwcvlbrNZGTJ3mn1EpFQ2oaKANAAJJAAA5kqS7B2Phe3G0rTZu3uqcVZteGulLXSvL5HSPfI5jI2lznvcSQxopQAAAL40/te06Q2NWtn0z289hq67tVm3l0XTmJeJP493FvmUk3ZPY90uzdMnqhDFIukYBEBAwCADjEZS6wuRjydnp9RETQOqWmoLSCAQSCCeRCb92Rhe4207zZu4eqMVetaHOiLWysLHtkY+Nz2PaHNexpBLHDwIIK2luLlbe927Ro+3rPWKBE26iBClYDWYuwMo6WTr86pYItOdbyVnl3LoEHiyhO5FZucUTiXu+hBLtMvum+zOUgy9zFAy7g000NeA7Q7W3UC9xNDXkQacPgoXsHsjtnt1svJbDw15lJ8Fk+tr9RJA+SIzwiCQwujt4mt1MDTR7HgPGqnFwPO5Jcv9lcpEqajsGCosSnSFpxeLCnxtgjhdGsBYgj0skMzZ58VSELDJePxeES9x+om6h2/vce7cjucQtv2QMEJcW9MPFdWmtdT3fkilKeKxu0XYbaHZd+Qk2rc5Kd2SbCJPVSQP09Dqlhj6VvBQnqu1atYNG0Aoa5NurnTtveOr2OoJyu65qlMYO4NdNtR4iyxrlRnXG6iEVDLKTVusDYYlucUlewqRVPK2SEDgAGKbJzO98rm8Y3Ezx28Vm0tNI2vBowUa3zSOGkcDSlagcfjqO3ftr2L233nJvzG3eWvtwSxzNLryW3kaHTuDpZQIrWB3VcNTalxbpkeNNSCIXZDV0IpmRfOPbDDj8pxueV3XVgoh61KVYklOxNlcWlCOfu3T1mqg/Z25jFFeV5dwQWBxZGKkDZLvKp2j3TCLe2VjwJ26+O3ksem5lXNeXgEkihEgbVhPl8vCgrWi5+vPbfse67pDu5b3eWtdyi8juTHDLbttnSMa1jwWPtXyaJ2tPWAmBd1H6SyopDPIeugVMGk8ybPUNXUrUcnpvj5sqsUBSwq1tfaNAlbdKtFbPPSFhlVAVUtbOPROq7kBTAUGyIiikmU/cYvcMust4XNpjIcTLZ4+5toNegzROkcNbi9344A4mnADgBWvNULuL2/4bPbzyO+rPcG6sRmcoIBcNx17HaxuFvCyCMUFs95AazVR8jqPc8t0g0Eabzagu9sm7WFaqlPCadFdfbVGiDQNTiO1BFD08JEGdPTMWpvD3iQVT9VDGHr9emRy9uvW3T7rpxQ6zXRG3SxvClGtqaD6+at/beE/VzB22EN5fX/AKdmn1F5L1rmXzF2qaXSzW7jSukeUAeCzLRu6LRoDY8Rs+msICSn4ZrLM2rSzNZF5DqJTEa5i3RnDeKlYZ6Y6bd0YUxK4IAHABEDB1AcvCZi6wORZk7Nsbp2BwAeCW+YFpqGuaeR4cVH+5Hb3C90dpT7N3BLdQ4u4fE5zrd0bJQYpGyN0ukjlYAXNAdVhqK0oeK1FmpU7W2qHuWz68ou2NfQrCBdQ24oiChbM5lGsgvJsWtefupFkpBLNJRk1auFV3hgVFwi6KYgABSlHqI7axzFzj7G6sIWxmG8Y1ry4EuAYSRpo4AGp41DvqUG3N2/w26ty4PdORlumZDATzS27Y3MEb3TsbG8TB0b3OaA0FoY+Mg1qSOC1LmpU5W9dD7/ALPx+mbXL1utUa2pXWnyFFsMHsGHkZ2vv69KumTmQarx0fNQnqSvCsgRUIsdVFRBQ5TJj16hvMHnrnAzSy28cEomhMT2ytLmljiCQQHNrWlDWoIJFFWvcztdhu6WPsbHL3mSsX46/ZeQTWMscM7J42vaxwkfDNp0a9TSwNe17WuDhTj2G2eQRdr1xlXQ0dx61mLKbbTXv2ptcK1Cxu/TsZJj7Q9klJ6UBeEX9x8yiHjATLt0T9wdnQfTK5/71t22/osfbaXh2qCHpvNARpJ1Oq01qRTmAfBYmxu1h2RlpMsdybqzHUt3Q9HJ34uoG6nxv6rIxDHSZvT0NfqNGPkbTzVEesj6tVMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRZzQtmbC1bLHnNdXOx0yVWR9O5dV6VdR3rWwCJitpBFBQreQbFOPcVNcihCnADAHcACGdY5LIYyXr4+aSGUihLHEVHwIHAj5GoUb3Ps7au9LEY3dmPtMhZNdqa2eNsmh35TCRqY6nAuYWkioJoSFJKQ+QfmFJx6kY53RIptlSolMpH1SgREgAIKpqkFOWiqoylUTGOkAHEixRUKIlP3FMYBkUm/t3SRmN144NPwZE08P3TWB37fHxVRWvtY7CWd0LyHb0RmaTQPub2VnEEGsUty+M8DwBYdJoW0IBESZydnLNLPp6yTMrYZ2UWFzJTU5Iu5aWkXAlKQXD6RfrOHjtYSEAO9Q5jdAAOv0yKTzzXMrp7l7pJ3Gpc4lzifiSakn6VemNxmNw9jHjMRbwWuNhbpjihjbFFG3npZGwNY0VJNGgDitp6o5E7s0edYdW7Enaq2crHcOYpMWUrAOHKiB2xnTiuTrSUgVnfhP0BUzYVAEpRAQEhRDaYrcGZwpP3ZcPiaTUt4OaTSlSxwc2vzpX9gKFb37Udu+47W/rpiba9mY0NbKdcU7WhwdpbPC6OYNqK6RIGmpBFHOB7XavKTf8AuxgWI2Zs2dsUKU6ChoNFCJr8E4WanFVsu8hKzHQ0U+cN1B7iKLInOUwAID1AOnrlNz57NR9LJXL5IeHlo1jTTkS1ga0kfEgrB2T2X7Xduro3+z8NbWmQo4CYmWeZocKOaya4kllY1w4FrXAEVBHErT9atFkpk2wstSnZatWCLV88fMwj9zGyTNToJTCg7aKJLEKoQwlOXr2nIIlMAgIhmotrm4s5m3No98dw01Dmkgj6wp9mMNiNwY6XEZ22gvMXM2kkUzGyRuHPi1wINDxBpUEAgggFSjf8+uXklCfb7jdc4mw9Mg087CDp8VN+JuQhEz/csXXWdj9SYCB3rer8yo9ROYwiIjJ5N97skh6Dr1+igFQ2NruH7trA+vxOqp8SqXtfbB2HtMj96Rbdtjc6y7S+a6khq4kkenknfBp48GdLQ0UDWgAKI7t27kHbp+/dOHz584XdvXrtdRy7du3KhlnDp04WMdZw4cLHE5znETHMIiIiI5E3vfI8ySEue4kkk1JJ5knxJ8Sr1gggtYGWtqxkdtGwNYxoDWta0Ua1rRQNa0AAAAAAUHBSeonNjlNreHSgKpuKwJRDZFBs0ZzrCu3ErJs1FYW7ZgrcYaecMG6QLiUE0TkJ2FIXp2pkAsmsd57nx0IgtbyQRAAAODJKAcgOo1xA48hTwHgFTe5fbt2W3dfuymbwFq6/e5znPhfPa63Opqc8WssLXuOkGrwTUudWrnE4FtTkXu7dgES2fsixWlkk4I8Sh1VW0ZXkniaXhTdo1uEbRkCk6TSEQKoVsBwAxug/mN1wMpuHNZnhk7iSVgNdPBrK/HQ0BtfnT4/FSbZXaftz27Jfs3EWllclhaZQHSTlpNS03EzpJi0mhLTJQ0HDyimls0ysNSgofNHlFrWLShKluKyIxTdEjZqxnW0HcUGTdIRFNuwC4xM+Zggn16FIiJCgXoUA6AAZJrHeO5sdEIbS8kEQFAHBsgA+A6jXU+qipnc3t67MbvvXZHO4C0dfOcXOfC6a1L3Hm5/pZYdZPMl9STx5mqxjanJ/fm62ftuy9nWGxRAqIqqQSYR8DXl1mwpmbrua9W2UPCul250gMmdRA5yH6mKIGMYRxspuXO5lnTyVzJJD+Twaw05EsYGtJHgSOC3OyuzXbDt3P6zZ+GtLS/oQJjrmnAdXUGz3D5ZmtcCQ4NeARQEEAAatp91t+vp9naaPZZup2JgJvSzEBIuYx8mmfoCzc6zVRMV2bkgdqyCncisQRKcpiiIDrLS9u7CcXVlI+K4byc0kH6OHMHxB4EcCKKaZ/buB3Ti5MLuSzt77FS/ainjbIwkcnAOBo5p4te2jmGjmkEAqSs9z05c2OFUgJLdM4nHrIi3UUh4Wo1yVMkLdRqIe/wBdr0XPAcySo9TA5Awn6HEe8AMEjn31uy4hMEl48RkU8rY2O5U+0xjXft/Pmqhxntk7FYnIjKWm3rY3TXagJZbq4irqDv4CeeSGlQOHTpSraaSQY0Vi4WWm2qHu9al3MZa4GWQnIqaKCLpy3lW63qE3ZyPUnLd2YVfqcqxFCKgIgcpgEQGN213c2d0y9tnlt1G8Oa7mQ4GteNQfnWoPirgzOBw+4MJcbcy8DJsJdQOhki4ta6Nw0loLC1zeHIsLS3gWkEArKts7l2TvKzN7jtKx/dFjaQ7WAbyPs8DCeOJZOnz1s09JXYuIYG8bmSWN5DJCqPf0EwgBQDKyuYyObuRd5OTq3AYGg6Wt8oJIFGNaOZPGlVpNjdv9o9tsO/AbLtPRYl9w6Z0fVmmrK9rGOdqnkleKtjYNIdpFKgAkk7G1rzD5K6jg0a1QtsTkXX2qQIMYeSYwFqj4xADicG8S2tsROJxLcDmEfG2BInUR+n1HNjjt3bjxMAtrG6e23AoGkNeAPg0SNdpHyFFEt39hO0G+8k7MbnwdtNlHmr5Y3z2z5HUpqldaywmV1OGqTUeA48FgO0957b3ZINpLaV8nLesxFY0e1fKoNYiNO5N3OFIyBjEGMJGqr/QDmQbpmOQhCiIlIUAwMpm8tmpBJk53zFvIGgaK86NaA0V+QHgPAKUbK7bbF7dWr7TZeMtrCOSmtzAXSyBv2RJNI580gbxID3uAJcRxc4n9tTb62/o188kNVXuYqKkl4xkmjYrGRh5I6CayTdWRgZlpJQj9dqm4UBJRVuc6XePYIdc/WKzuXwjzJi53xF3MChafhVrgWkipoSKjwXnvjtjsLuTbR2u9sZb37Ia9NztccsYJBcI5onRzMa4tGprXgOoNQK7zY/Jzeu2rJTLdsDYD2dn9eOwf0t6nE12FSr78H7GTF8zj4CHi40z1R7GNjnVUROooDdMphEhClD2yO5c5lbiG7v7hz57c1jOljdJqDUBrWitQOJBJoByAWt2l2c7a7GxGQwW18XHbYvKx6LthlnlM7ND49D3zyySBgZJIA1rw1ut5ADnEnpdwb62xvuTiJnbFr+65KBYLRkS59ircF6Ri4cC6WQ8Nah4dBfvXHu7lCnOH4AIB9M8cvncrnZGTZWXqyRtIadLG0BNaeRrQePxWx2F2x2P2xs58fsex9DaXUoklb1ribU9rdINbiWVwo3hRpA8aVWS6s5WchdLRow2t9ozsFCdqxUoN23iLLCM/UKprrnjoe1Rs3GxqyqqYGMdukkcRE31/OfuycXunP4aPo465eyH8khr2ivE0a8OA+oDx+JWn3r2S7Vdw7v7w3dhba5yNRWZrpbeZ2kFoEkttJDJIADQB7nDgOHlbTHdrcgdzbwXbK7T2DO21JksLllGuRaR0GydGR9OZ0yr0M1joJo6Oh1KKqbcpxKIgI/UeuPlM9mM04HKXD5Q01ANA0HlUMaA0GniBVbbZHa3t924jezZWKtrF8jdL5G6pJntrq0vnldJM5oPENdIQCBw4BdrX+TO7qtqqV0lA3b0GsZttMs5Os/bdRdepbWATml0/eXkC4sCPqxUH6puyGT6/kEuelvuTNWuLdhYJtOMeHAs0Rmod9rzFpfx+TuHgsHKdnu3Oa3tB3FyeO6u8rd8T47j1F03S6CnSPSZM2A6aDg6Ih34wKxHVW39jaStI3TV9lWq1lNGu4dSQTj4iVItGPzoKOmbiPnI+TjXCKirVI4d6JhIomUxRAxQEMXF5bIYW69ZjJDFc6S2tGu4GlQQ4OB5A8RzFVvt7bD2n3Fwv6vbzs23uIEzZQwvljIkYHBr2vhfHI0gOcODxVri01BIXNT3jtttsaZ23H3ydhtiWF26ezVmgFka85klXp01HablpCIx8YZk4OiQTtgQBuYSgIk+gZ+xm8q3Ivy0c72ZCQkue3yE151DQBQ/ClPksd/bfYs20rfYt1jLa42nasayG3nBnbGGAhpa6Yvk1tBIEmvWKnzcVvGU+QHmBLxasO73TKpNFkk0jrRdao0JKAVISGKKU5C1dhNIKiJA7jpuCnOHUDCICPXdSb93bLEYXXjgwjwZG139k1gcPpBVb2ftc7C2F62/g29A6dpJAkuLyaOprzhluHwuHHgCwgcKAUCiG/fvpV88k5R67kpKQcrvX8g/crPHz545UMs5dvHbg6i7ly4WOJzqHMYxzCIiIiORN8j5XmSUl0jiSSTUknmSTxJPiVe9ra21lbR2dnGyG0iYGMYxoYxjGijWta0BrWtAADQAABQBcTPwvdMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImETCJhEwiYRMImEX//2Q==";
        } else if (business_type == "iWealth") {
            imgSrc = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QNfaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IjQ2OUQxRkJDN0ZBNEI4QzE2RkU3MEUwOTFDQ0JFNUFEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI5QTlDMUI3QTVCMDExRTZCNjkyRDhGNTJGQzY2MUY1IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI5QTlDMUI2QTVCMDExRTZCNjkyRDhGNTJGQzY2MUY1IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ODcxRTREOUVBNEE1RTYxMUE2REFDNTFDQ0M2RjJCMzAiIHN0UmVmOmRvY3VtZW50SUQ9IjQ2OUQxRkJDN0ZBNEI4QzE2RkU3MEUwOTFDQ0JFNUFEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPgO/AwERAAIRAQMRAf/EANYAAQACAgIDAQAAAAAAAAAAAAAHCAYJBAUBAwoCAQEAAwADAQEBAAAAAAAAAAAABgcIBAUJAwECEAAABgEDAgQDAAsKDgMAAAAAAQIDBAUGERIHEwghFBUWMSIJQTIjs3S2F2en5xlRYUIkNDV1Nxg4gWJTY7SVJkZWZnbWR3eHt8cRAAEDAwMCBAIDCA0IBwkAAAEAAgMRBAUSBgchEzEiFAhBYVEVFnEyUiMzNOQYgbFCYnJzJKS0ZaUXN7JDY1R0dTZmU5OzxNQlNURkhEWFtUZ2OP/aAAwDAQACEQMRAD8A2GjwHXsAgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIgIrd9q3at/aZ99/wC3fsn2T7X/AN1/cnqfuT3F/wAxUHkvJegf53qdX+Dt+a/uDeDf75/rT/zT6t+rfTf+zeo7nqPUf+8QaNHY/fatX7nT1p/lflf+7D0H8g9d67v/AOf7Ojs9n/Qy6tXd/e00/GvS3f7K38+/6MP1hi/v1G/+aP7N/T1T/wCtd/UH8+/Q0/ZW/n3/AEYfrDD9Rv8A5o/s39PT9a7+oP59+hqgfcHwc9wZygvjONkDmZyPTaWcxYs0aqV2S9cpUbUJqsTaXSjcQsiSkyeM1mf2pDKvLHGcnGO9jsyG7ORm7ML2yCHslxmrRgjEs3UHoDrOqvgFoLjzfTN97WG55bcWUfdkaWGXuBoj8XF+iPpTqfL0+kq91Z9LOdJra+RZ81N1tlIgxHrCuZ478+1XznWG1y4TU735F863FfUptL3Sb6hJ3bE66FqGy9j9zNZxTXu5BDePiaZIxYawx5aC5gf61usNdVodpbqpXSK0VCXXurgiuZI7XCGW2bI4MebzQXtBIa4t9K7SXChLdTqVpU0qqd90XbFa9tV7jFe7knvClymqlzIN8VGePkmyrZRM2dSuD6vdkaoseTFeJzrlvKRpsLYZnnvm3hW+4byllaSXn1hjr6Bz2T9nsUkjdSSIs7s3VrXRu1a+uumkaam5eLOUbXk2wurhlt6O9tJWtdF3e75HtqyTV24vvi17dOnporU1oLB8MfT0/K7xjiPI/wCV7297qgSJvo3sD1byHQsZkDpeo+9azzW/ym/d0G9N2mnhqdscde0v7f7Kx+8PtB6T10Tn9n0Pd0aZHsp3PWR6q6K10N8aU6VNeb19xP2P3Reba+p/UekkDe56vt66sa+uj0z9P31Kaj4V+Sk/9lb+ff8ARh+sMTX9Rv8A5o/s39PUW/Wu/qD+ffoafsrfz7/ow/WGH6jf/NH9m/p6frXf1B/Pv0Na+uDeEbTm/leHxlW2npDSiuJVtkR1x2LdRV07TpuT3K1M2Eb/AJiX0Y6EddBdWQjVWmpjJ/GXGl9yXvuPZdnP6dh7zpbjt9wRRRA1eY9bNWp+iMDWPNI2rqLQ++t82mxtpP3Rcxd5w7bY4degySSEUYH6XUo3U8nSfKw9KrYL+yt/Pv8Aow/WGNYfqN/80f2b+nrPH6139Qfz79DWrvkPCbXjfOcrwS6LWyxW8n0z7xNm0iWiI+pMawYbUpakxrKIaJDWpn9zcT4jEu7dt32z9zX218l+eWNy+FxpQPDXUbI0Gvlkbpe3964LU+3c5ablwVpn7H82u4GSAVqWlw8zCfwmOqx3zBWGiOruUBFabtm7V8p7krG8VAuomKYxjZRWrbI5kB20Wc+cl1cWurKxuVXlPldJlTju6QyhlvaZq1WhKrw4Y4OznMV3cm1uY7DCWekS3D2GU631LY44w5mt1AXOq9gY2lTVzQaq5P5XxXGltALiB93lLnUY4WvDBpbQOe95a/Q2pAbRji41oKAkenuY7XMp7bbakbsbiJlONZI3L9FySHBdrFLl1/RObXWVY5JnenzG0SUON6PvNvNqM0q3IcSj58zcI5zh2/tmXlxHfYa8DuzcMYY6vjprjkjLn9t4DmubR72uaSQ6rXBv98Y8qYrku0ndbQvtMnbFvdhc4P8AK+ul7HhrdbSWkHyNLSOooWk1/wASovdGVYzjPmvI+4sgpqLzvQ8z5P1exjV/mvLdaP5jy/mN+zqI36abk66lVOBxf13nLLC6+16y7hg16dWjuyNj1aat1adVdOptaUqPFWFl7/6qxN1lNHc9NbyS6a6dXbY5+nVQ0rSlaGnjQ+CtD3UdqP8AZnj4Q/7996+8nsha2+1vbfp3oKKVe7d7jvvOea9X+GjXT6f8Ld8t2c48E/3MxY2X61+svrF04p6b0/b7AhP+sT69Xd/e6dPxr0qzinlr+86S+j+r/Q+ibCfy/e190yf6GLTp7f76tfhTrVGlp7LIbiqoKaI5Pt7uyg1FVBZ06syxspLUOFFb3GlO9+S8lJamRan4iisdj7zLZCDFY6My5C5mZFEweL5JHBjGj5ucQB91W1e3ltjrOXIXrxHZwROkkcfBrGNLnOPyABK2QZH9MzkCmwSVkNbn1Lf5hArHLCRhcWjlx40lyOwb8iuqckdsnFzZyiSaGCdgRm3XNCNSCPcWwsx7MN147a78tZ5W2utwxQmR1m2F7WuLRqdHFcGQl7zSjNUEbXOoC5oNVmvG+57b17nmY65x89vhpJQwXLpWlzQTQPkhDBpb8XaZXloqQHEUWsz4eB+Bl8SGMFp1ARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARWQ4x7WOUeWuN8t5RxX24jHsScsmXotrbPQ7e5kVFa1a2TFNHbgSYZqjRJDfzS5ERtal6IUoyVtuHZXB299+7Ov974L0YxNgZAWyylkszooxLI2FoY5lWtc3rK+JpJo0mhpWu6OVtq7R3LabVy3qTkbwMIdHGHRxiR5jYZCXtd5nA9I2SEAVIFRXlRO1TP7Hgl/uDrr/B5+GxYMmdKqY1lfKyqMmFcnSTo79evG0VjcmHISbqyOZt8uW9KlapI/vb8Gbqu+MHcsWl1jJdusic90TZJzct0Tdl7XRm3EYcx1XEd6nbGoE1APym5Y2/b79bx5c299HmnyBrZHMi7DtUfdaQ/vF5a4eUfi66/KQKGkf8LcM5ZzvmzeB4a/TxLVdZYW7s2/kT4tVEg1xNE65Jfra22loNx6Q20jRhRG44kjMiPURPjjjvPcoblbtfbz7eO+MEkpfO57YmsjpUudHHK4VLmtbRhq5wBI8VId7b0xGwsGc/mmzPtBKyMNiDXSOc+tA0PfG00ALjVw6A0r4L2c28KZdwJmicGzORSzLRyng3seZj8ifKq5NfYOymGlsP2VZUylKbkwXW16skRLQZEZ/EfTknjfP8WbjG2dxvtpL11uyZr4HPdE5khc0FpkjicSHMc0+QCoNCV/Ox974ff+EOdwrZ2WomdEWyhjXtewNJBDHyN6hzSPN4EVAUgn2m8jN8Ef2hZVzhMLDDrCtk1cq0ukZOuM7eFQREphox5yoN+fLWhbKfPfM24nUyWZoKWHgfeDOL/72Z7jGx7c7HdEbpJhclpm7DAGCAxanuILB3urXNrR3lEd/vc227fv93cUN8/Nd3t62sjMAcIu67zGYSUY2oce10cDSrfMqwCk1aSAiAiAisjz92vZ/wBubOLP5vb4faIy524arSxWwupymFUiaxcs5xW+P0ZNpcK1b6fTN3XarXboWtxcq8Jbr4gjsZdy3GPnbfulEfppJn07IjLtfdghpXut06dVaGtOla14+5T2/wAkPuo8HDeRGzEZf32Rtr3denT25pa07ZrXT4iletMK5A4J5W4toMbyjO8TXR0OXJQrH7ErjH7Rud1ITVg2lTVNbWMiEtyG8laUyENKUWpEWqVEUb3ZxfvrY+Ls81uiwNri78DsSd2CUPqwSCohlkcwlhDgJA0nrTqCB3e3t+7T3VkLnF4G7E9/Z/lmduZhbRxYeskbA4BwIJYXAdPgRXIeBe3PN+4mxyOswmzxatkYzAhWE5WUTraC0+zPfejsohqqqS6Ut0nGD3E4TaSIy0M/sdtxbxDuXly8vLLbU9jDNZRMkf6l8rA4PcWgMMUM1TUGuoNHzXXb/wCSMHxzbW11nIruWO6kcxvYbG4gtAJLu5LHQUPShJ+Sg6xgSaqwn1c1CW5lbMlQJbaVpcSiTDfXHfQlxBmhaUutmRGRmR/EhWd5azWN3LY3IAuIZHMcK1o5ji1wqOh6g9R4qdW1xFd28d1Aawysa9p8KtcAR0+HQrhjjr7ICICICICICICKwHBfcnyH29HlPsJnHHvd/onqxZBWyrEk+ger+QOJ5axrzZ19ae36793y6aaeNrcY8x7u4l9d9lm2bvrDs93vxuk/Id3Rp0yMp+WfqrWvTwp1r7fnGe3ORfS/X7rkej7vb7T2s/K9vXq1MfX8m2nhTr41W8ztF5hyvnHiNGcZmzTsXJ5LdU5oo4ciDCOLXogqYX0JM2e511HIVuPftPw0Ih6bcBchZ3kzYI3LuJtu3I+tmipCxzGaYwzSdLnvOrzGprT5BYS5g2bidi7wOCwrpnWXpY5Pxrg52p5dXq1rBToKdK/NRN3w9yPIvb5+TD2CjH1e7vevq3rtY9Y6ege0vIeV6M6F0dfWnt+u7d8vw08YJ7mOYt3cTfUn2VFofrD1nd78Zk/Iel0aaPZT8s/V416eFOsu4L4025yJ9afaA3A9H6bt9p4Z+V9Rr1Va6v5NtPCnXxqqRduUjK+7XuvpORM/i1bqMGrIWTXXpUFyFWrPGlIj4tF6L0iWXmHb2Sw8pJqPqsx3PDQj0zVxBNneeudrbd262QObjIGXM3aYWRn05DbZtC53mMzmOIJOprH9OhV48kR4niLiWfbm3nyh1/K6CPuPDnjvdZ3VAb0ETXNBp5XPb1W7u6yKlx0qo7mwYget3VfjtV11Gnzt1aKcTAr2dCPc/JNpW0v8Ux6VZLL47ECA5GVsXqbmO3i1fu5pa6Ix++dQ0+4sOWWNvcj3fRRuk7ED5pKfuY2U1vPyFRX7qqr3zcVflO4DyKRCjdfIMAV74ptiNXnGKph5OQQ0mkjcUl+hdfcJtJH1H2Gi01ItKN9zexvtpxXdzWzNWWxR9bDQdS2IETs+mjoC9waPvnsZ9AVr8E7s+y/IFtHO7Tj8gPSyfQDIR2nfR0lDRU+DXPWqPjfvp5q4twrH8CxuJg71FjUZ2JXna0VjKnKZemSJqykyI95DQ4rrSlaGSE/LoX74wps73O8kbI23abWw8eMdjLNhbH3YJHPIL3POpzZmA9XHwA6UWs9y8D7I3Vm7jP5N982/uXBz+3Kxrahob5QYnU6NHiT1X0I1UlybWV0x4kk7LgQ5LpIIyQTj8dt1ZIIzUZJJSz0IzPwHrLYzPubKG4kp3JImONPCrmgmnyqV533cTYLqWFldDJHNFfGgJAWofuI75ebeL+aM9wLGW8MOixuziRK47KhlS5ptPVNdNX5iQi2jodX1pStDJCfDQhgHlz3Nck7J5Gyu1sKMd9V2c7WR9yBzn0MUbzqcJWgmrj8B0WweOeCdj7p2Tj8/kze+vuYnOfola1tRI9vQGM06NHxKkz6avGa4GI5lzHaxkpsc5tXaKicU3tNNDTSVvWsiMr/IWeQLNpRGZ6Kri/wzP2bbMda4DI8h3zKXmTnMMJp/mIXEyub+9knJaR9NuFGPczucXGYstmWjq21hEJZRX/OyNpGHfNkQ1D5TFbIWMipZOQ2OKs2DDmQVNTU3lhWEr+MR6u8lW8KslqLTQ0SJNHJToRmadhGoiJaNdhx5fGzZabBRytOVgt4ppI/3TYpnSsjcfk50Mg+kUFaBza5rfjb2PHR5Z8bhj5ppImP+BfE2Nz2/sNlYfnU0rQ004/Uy4q9HzTFOXK6NthZhCLGsidbTohOQ0TG+rkvr08XrOh+5ILU/lrj+H2fPP3nbG+r9x2O/rNlLbIRenuCB078IrE5x+mSDyj5W5WzPbFuv1mEu9oXLqz2b+9CD/wBDKfxjR8mS+Y/OYLV4MSrUyAi3W1yHe1jsDfntOLqs5z6sTMQ80pUae1kvIyGmISmXEml2PZY7h7aHPjuQ9CUZaH4F6RWjX8He1V10wmDc+Vh1gg6Xi4yADWUPi2S3tAHfSHwkiixHclnK3uCbbuAlwWPl0kHzMMNmSXVHgWTXBI+gtlC4d0h3uk7AYlu4tdpnXG8E5rzzi1SZzt7x427Fs1Puman5M+/wp5Ugy13OSJKDMjMiHGyTX83+1SO/eTPufDxaySdTzNYAtk1Hxc+ezcZKeLpJGnqV9rIs4r9wb7NoEWBycmkAeVgiuyHMoPBrYrkBlfAMYfALU9xN/Wpxn/7Awz8Y60YR2H/xzhv97Wn9IjWtd3f8KZP/AHdc/wDYvW0H6p/8g4R/C+QvvOGDbPvi/NNtfxl/+1aLLXtT/OM5/AtP27lV2+nfxh735w93zo/VpeMatd4pS0bmV5FaE9WY6wv9xxojlTGz18HIZCovaPsn7S8l/aC5Zqx2FgM3UVBuJax27T8x+Mlafg6EKx/cZun6j2L9TwOpe5SURfPsso+Yj5HyRn5SFXG4/wC5iVe99mc4K7bvuYRaVsnjvHYK5SzrmMlwdt2e9YNNms4pO2FizbMpWkiU8TzCdytqddDbU5nnyfufye2H3DnbanhdYW7C49ttxZAvMgFdNZJBdMDgKvD4xU0CpncPGMVhwLYZ5kLRnIpW3kzg0azDdEMDCfvqMYbdxBNG6XmgqVrb7vuMPyU8+ZvSRo/l6S8me8cbSlGxkqjJHHpao8dOnyx6y1TKho+Pyxxjn3AbJ+wvKeSxsLNGMuZPV2/Sg7VwS8tb+9jl7kQ+Ua0tw7un7Wcf2N9K7VfQM9PN8T3IQG1PzfHokPzeqyimFZ69640ltlmS5HfRHkGso8hbTiWXzaVscJl1SSbdNtRaK2meh/EfR0MzI2zOY4RPrpcQaGnQ0PgaHxp4L+BLG55ia5pkbSoqKivhUeIr8Kr0D5r+1yocGbYyExa+HKnSlko0Rocd2VIWSEmtZpZYQtxRIQRmeheBFqPvb21zdyiC0jfLOa0axpc406mgAJ6DqV8pp4LaMy3D2RxDxc4ho6+HU0C4o+C+q7GvqLa2N1NVV2NkbBIN4q+FJmGyTm4mzdKM04bZLNB6a6a6H+4OXaY+/vy4WME0xbSvbY59K+FdINK0NK+NFx7i8tLQA3cscQd4a3NbWnjSpFaLxYVNrUqbRaVlhWreSpTKLCFJhqdSkyJSm0yG2zWlJmRGZa6ahdWF9YkNvoZYXOFQHscytPGmoCv7CW93aXYLrWWOUN8dDg6n3aE0X5gVllaurYq6+dZPNtm64zAiSJjqGiUlBuLbjtuLS2S1kWplpqZEP5tbK8vnmOyhlmkAqQxrnkDwqQ0E0qQKr9uLq2tGh91JHGwmgL3BoJ+ipI6r3z6K8qmkPWlNa1rLjnSbdn10uG0t00qWTaHJDLaFObEmehHroRmPrdYvJ2LBJe288MZNAXxvYCfGgLgBWgJov4t7+xu3FlrNFK8CpDHtcQPpoCei6ocFcpARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARX87cOCLDkTt65jzWLyrneHQKV3J4tth2PTn2Mbyxmgw6FfMIv4bc2OzNS+qcthROtuETfwLxPXVXD3F93u7iXcO44c7lMfa2zrlstpA8tt7oQWjJ2idge0PrrLDqa4afD4rP3JW/bfbnImFwkuJsL24nEDo7iZgM1uZbh0RMTi0ltNIcNJb5lPf0/wC0a5F4R5z4JsXWldWNYPV6ZB67YGeUMyjlk0XzbWa6xq0vGZFqlyVqRmZlpaftSvmbv413PxfeOaaskMYd/wBHewPhfT5RyRh9aVDpaj5V/wC4S1ftvfOB37bA9HMD6fh2srZW1+b2PLfoIjp93Ffp9VLeAY53Dc13scmU4Rjb9G0Txaarp4k3J8jgqcTqe/qV1ckkoMzUpXw1269H7T7Bu1MRu3kjJsDRjLN0Ir9MTX3Nwyv01jgFASST9yvbe4a7duHJbd2RYOqb65Epp/pHNghdT6KPmNT4AfdXn6gENrkHj/t451qmScRlVA1TTjYbNZ9a+q4eT0sBO0lLS7GfKzQbZmaiXqReJK1e623j3ZtPaXJ9g2rb61EL9Ir1njZcws6VNWu9QC01Neg6gr89vcz9u7h3HsO7dQ2lwZG1Pwie6CV/3HDsmvhTr8QpX7wKi4o+Ee3bthxCImZk2XzcYx9qEw4lvzbeGU9fBkG+akpQy1Kv7WPJckOGlKSYcUrw3Gmd+4LH5DG8a7R4U2/GJM1kJLaAMaaahaRRsdq+ADp5Y5HSOIADHOPTURE+HLyzvt87j5RzDyzF2bJ5i4iuk3Mj3CnxJbFG9gYASdTQPgDiUTsE7fMFrqmFzhzyqly21YQ81GiZThOD1z6zWbbzNTFy6vtbO2YYeUTSXy6JuGnU2kGrYnoYPatxNti0gtuTN0m2z87QQ1tzZ2UbjWhETbqOWSVoPlD/AC6iKljSdI7eb3A8h565mn2LgBPiInEFzoLm6eB4gyOt3xsjJHmLPNStNbqajVTul7RLPt9j02V0mQlmXHmQykQIluqO3GsKqxejOTIkOyTGcehy49jEYccjSmTSlfTWlTaDJBuUXzhwDe8TxW+dxt39Y7Su3hjZdIbJFIWl7WSaSWObI0OdHKwgO0uDmt8pfbHFXMNryHJNib639FuK3ZrdHUuZIwODXOZUBzSxxAex1SKghzvMG2fpexjtlx2FUVvKvcVHZzG1iV8xqHT5ngGJRJCbJhl6MmqrslhXVtZxn/MI8vI1a8wg0rJpG4kldmN9sfC+Itrez3zu5jdwzxxvDIryxtWu7gBaIo7hk0sjXahok8vcBDgxurSKtved+T8jPNc7T2444aJ72l0ltd3DhoJDu4+F0UbHCh1s82ggjUaVNTu7DtYmdt1zj70G9dybD8rRNTU2MqKiJZQbCsTFVNrbNtha4zilNy0OMPI2E6nenYk2zNVD878HXHD2QtJLa6de7fvw8RSOaGyMkj0645A0lpqHhzHimoahpBYSbb4k5Wh5Ksrhk8AtczaFvcY1xcxzH6tL2E0cOrS1zTXSdJ1HVQXH+qf/ADfwl+Gcg/eMMGhvfF+aba/jL/8AybRUz7U/znOfwLT9u5U49wlBwFkHCHCzHP8Am+UYPRMQKB6im4vEky5M22PEWEOxZKI2IZepDCYZqWRm00W4vtz+B2by1iuKsrxrtyLlbJ3uMxjYoDA+2a57ny+laC1wbaXdGhlT963r+6+CgvHeQ5Ax++c3Jx7Y2t9fuklErZ3Na1sfqCQ5uq4t+uqg++d0+HxUFfTziYdB5j7gIXH1nZ3WERYdTHxe3uGujZ2VM1dWSIc2Y0cGrUh2S2W/RUaOvQy3NoVqkqv9pUG3rXkTddttOaa521HHE22llFJJIRNIGPeNEdC4descZoRVjTUCee4qXMz7L29PuKKKDOPfIZ44zVjJDGwua06n1APTo94+hzh1PaxexTt3yjIsiobPniRacrzrG8tJeO4vkmDtO070l1c9UeRh8iNcZJIaqjkfdlqkRjdb0PazqOfD7YeI83l7zF3u6Hz76lmmldb21xZAxFxLy11o5stw4RavM4yRlzaGkdVxJeeeRsXjrbIWuAbFtJkcTGzTw3REgaA2ouA6OEGSnlAY/Sair6LWjz1wzd8DckW3H11MatUxmIllT3TDCorVzSWCVnDnlEW6+uI6TrTjLzRrWSH2VklS07Vqxnylx1kuLt4z7TyMjZwxrZIpmt0iaGQHQ/SS4tNQ5j21NHscA5zaOOnNgb1sd/bah3DZMMRc5zJIydRjlZ98zVQBwoQ5rqCrXNJDTVohoV2pmgIgIgIgIgIgIt+304v7uSP+uso+91Q9U/Z//hAP96XP7US8/fcn/iSf9gg/bkVf/qpf+CP/AJP/APzwVR75P/xf/wCpf9wVhe1H/wCf/wDwP/fFOX06eLfZfDEjOJ8bpXXJ1odkhS07XUYzSKkV1E0ojLcRSJK5kpJkei2pLZ6eGos32h7I+znHL9zXTNORzU/cFRQi2hLo4Qf4Tu7KD8WyMPwUF9x+6vrvercFburZYuLQaeBnlo+U/sN7cZ+hzHKBfqO8zWFRnXFmDY1ONibg0mJybOcbUaks5H5vpYqTiUqSaJNZGhyHtPiaJqT1IVb7weRbuw3Pg9s4aXTc4x7ci8j4XGqlrX6HRtZI/wDgzNU+9tmy7e8wOVzuTj1QX7XWTQfjDprPT5Pc5jfuxlbTsJyiq5EwbGcvgoaeqcwxusuW47m15CWLeA0+9BkJURpUtg3lMuoUXgpKkmXxIbh23m7Hd22bLP2oa6wyFnHMGmhGmVgcWOHgS2pY4H4gghZTzmKu9uZ26w85LbuzuXxkjoaxvIDh8jQOaR8CCF81ncPxg7w9zHnGCE04itrrdyZjy17j62N2yU2VGonT1J5bECShh1ReHXaWXgZGReN3LeyX8e8hZPa4aRZw3BfAT8beX8ZD1+JaxwY4/htcPEL0y463S3eWzLHPEg3MkIbMB8Jo/JL0+AL2lzR+C5p+K+mvH/5hpP6Irf8AQ2R7Q4n/ANKtv9nj/wAgLzByP/qE/wDHP/yivng7rqOyyfu65Cxumjql2+QZlj9LVxU66yLC0qKGFDZIyIzLqSH0lrp4ajyR51xl5mufsth8cwyZC7yMEMbfwpJIoGMH7LiAvRjia/tsXw/jsneu0WdvZSyvd9DGSSucf2ACvoA45wmr41wLE8FqdpV+KUNfUIe2k35p2LHSU2xeL4JfsZhuSHT+G9xRj1a2htuy2btaw2xY09JYWscQPhqLW+eQ/OR+p7v3zivPfcmcutzbgu89d19Rd3D5KeOkOPlYPkxtGN+QC1GcOdyZ3nfhe5K7O3YnyZPnca1pqc/i5VMToRMFkMtGfTQ/YWVPG10+1VPdMjPceuBOPOYzk/dFdZl8v/kOZlfjo+vl7TNLbJwHgHSSRR/cM8njU11/vPjMWPAcGMbHTL4yNt6/p17jquugT40YyR/7ETPCnTZV3QcWFzDwjm+IR45SLtFed9i5Enc6WSUW6fXMMGepIXZpbchKVoejclQ2TzZscchca5Lb8TNeSEXftvp9RB54w36DJR0JP4Mjlmbi3df2N3xY5iR2mxMnan+jsy+R5PyZUSgfSwL5lDI0mZGRkZGZGRloZGXgZGR+JGRjxeIIND4r0+BBFR4KcO2/jFXL3NOB4S6wb1VLuG7LIvA9icbpUqtLlDi/g15yHFOM2o/DrPILxM9BZfD2yzv/AJHxe23t1WMlwJLj6PTw/jZgT8NbGmNp/Ce0fFQXkrdA2fsm/wA412m7ZCWQ/wAdL5IyB8dLnayPwWlbt+4Ln7toxG/i8V820zGQm3XV+TMw52HxcrpqxcldhBhGSFJkvwbdMZpwyNDJGmO8Wi9FmkelHLHKvDOAysex+Srdl2RDHchj7Rt1DGXGRjOnmLJdIcfKwERvHmo4hYe484+5OzGPfuvY87rcGR8Bc24dBI/SGPd16B0eojxd1e09Ktqvz2/8+9s2YX0vinhWkj0BP1ljkj8GHh0TFqS1OOqBAnEpskx3rC1ciPIM+oyo1RmVar0QSR+cU8qcL7gykmxeN7ZlrrhkuHMZaNtoZdOhj6jyl8paWk6mGsbDV1GgL95B4/5Pw1gzdm953XGmVkIc65dPLHXW5vXqGRhwPg7o9w6eaq1AZZxmviLu4r8ES0tqurOW8SlUBq1MnMctshq7SiMnD8HVs1sttpxReHWbWXgZGRefue2Y7YHPkW2A0ttIc9aug+dvLPFLB1+JEb2tcfw2uHiCFsTEbnbvDiCTPEg3MuIuGy/KaOF7Jenwq9pcB+CQfirtfVP/AJBwj+F8hfecMGk/fF+aba/jL/8AatFR/tT/ADjOfwLT9u5Us9pNfj3bb2nSuWc3RIiIyU/fl2uPHQ7YqqJr0WmxCuiNuux0SFTYy2n2EqWkupYKLdoJ3wJaYnhzgh+/Nyh8bbz+WzFrQZO08thtI2glodraWvYCR5pyKqJcvXGR5L5bZtHBFr3Wv8liBJDO40OkuHuIBI0uDmOIB6RDosai90nYRTWqM0qcFq4+VsyVWbEyu4mhRrxiyNZvHMjTFRY8SPYqdWZ9ZD6V7jM9/jqOng5v9rGOvxuOxxcDM61/ca+PFsbMJK11tfpa1slSTrDwa9dXxXZy8V8/3tocJd38rsS5ugtffudEWeGlzdRcWU6aS0inSi6H6h+H1nIvEPHHPmLJ83FrW65MmYhBEuRh2cR4s2mmSjSaySmDbKZbQnX5V2C/Ex1Xu329Zbu2Bh+VMGO5BCI9TwOrrS9a18L3eP3kuhoFehncuf7dMzdbc3hkuP8AK+SWUvo2vQXFqXNka3+FHqJPxEQWmgedq2irp8u/3M+0f+mObPx7mjRu/wD/APnfYP8AtGY/pr1Sez/8aN4fxOM/orVSwZyV2K6f0/f70GGf0PmX4r2g0b7Uf8bMb/s93/RpVSfuF/wtvf462/7dipYM5K7FsR7HbB2qwLucsGc6Txm7FxrAHEZ2uudtk42Z2+QI86quYQ47LJZK6W1JGf3TX7A1z7Z7t9jtXet3HlBhnss7Ei9MZlFv+NnGvttBLq/e0A/dV+Cznzpbsu8/te3fYHJsfc3YNqHiPvfi4fLrNA2n31Sfgq79x+X2+TZTSxp/N5c7wqujJyDk7dA/jrNY/YTZBTqZMGUwy+442mEy8pzxSonSIvFJiouYNwZDNZy2huty/ai2gtqsuRA63EbnvdrhDHNa4kBjHl3gdQA8CrG41w9ni8VPJb4L6gnlno6AyiYvDGjTJqaSADqc0N8RpJPiFN3YRMKvyDneeeSrwwoPAGZTDzBuvdtnMUKNNp3jyVFUxq/ZroiR5oo6PneNrYXiYsv2sXAtMtui6N4ccItqXb/ViMym10viPqBE3rIYadwRjq/TpHUqDc/wm4x2AtxbC9Mm4bZvpy8RifU2QdkyHowS10az0bq1HwWGdyeeryfHcdrGe6edz/Gbunpz9JK43tcHboH2ILseNalKskEU9b7ct1npoPVJGZn9gRzmPdTs1iLSyj3xLuuEXJe6F2PlshA5rC1suqQecuD3N0jw8T8F3fGe3xi8jc3T9qR7elMAaJW3sd0ZQXAuj0sPkoWtdU+PgqdjPauRARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARbguyL+533EfhvI3/1dSj0F9tP/APPm7v4zIf8A22FY55y/xk25/As/6dIqodgmdFhfcbjkJ9xTcDOqy2wuUe75CfmttWtSakH4KU5cVDDJGWhpJ0/HTUjoj2rboG3OX7O2lJFrlIJbN30angSxdPiTLExg+I1H4VBtr3A4E5vje5njANxYSx3Lfpo0mOTr8o5HO+g6R8iL291FNXdv/a1yTjlS7HZl8v8ALt3LQiLqlLDOWZFIyNyI0lTaFqTGxHHm4bhmRpQZ7SUZbDPUHOOOs+KOD8zh7BzGz7gz8zwG/uRdXDrgtHQEhtrbthdXoK0BPlJoXim9ueQuVcZkrwOdDhsPE3zfEwQiEOPU/fXExkHxPiQOoHB7VaWB3B9qWHYVaymjmcVctUb6lSNVOHAx7Ka7LVMFolxafOYrdyq+OsiJJLIiM9EqHG4Mx1ryzwVjtt30g9Rg89Cau8dFvcx3Wn4nz2s0sEZoACAD0Dl9+V724475Zvc3aMPZy2IlHTw1zQPt6/D72eKOZ48aeHUhYHytzRQxPqIcev385iLivGrDGEvTH3Upr4trk+N3RyLOQ4s0ojJg22VsMyHFGSWyh7j8EmYi++uR8XB7uMTJlZWsweGa2zL3H8W2W5t5tUjiejdEt0xj3Ho0Q1PQLv8AaeychL7c8izHsc7LZNxuQ0DzujgmjowDxdqjge5gHU9yg6lWG7kMI5sn5sm7487du2zmOom18Bhdnn+KU0zOIMiM0tt6PY2ORZVjsSXWoNJHG6ClKQle1SC27lW1zDtrkm63KMltLaOzdw4+SFjTJfWsL71jmggtkkuLm3a+MdO3oJIB0lvTUa641zmx7fB+h3HuPc2GvGSOOi0nkbauDiCCxkMEzmvP7vUACRUHrQQN3CwubXcD4R4v5eY4MwTAs/5LxPFbGo41q7Wqn4VDjz2m4qYTlpe2dIVVFrXnHHlxYyGoriG2zcNtw91W8tW3JUm1ttbJ3+3bGL2tlcza20kWOjlifZtbIA0MMk8kPabGXOeY42ticGMLix5rPuOp9jsz+c3Vs52dv9wY/GXE7JL18cjblxYS7UI4mS9xzwA0PeXSAudpDm9Mi7g8Kl8IzcRxrgvs6475Px+TSE9Z5NkPFtrypZtWcd469FZJlQ3zt4RswIrLxvSnltvqfPYSVIcM+35Z23cca3NhhuMePcRm8U+2rJcT42XJyCRp7Yjc5ju6yjGseXSPLXl50gFryeu47zcO+ILzJ773nkcXkGz0ZDDfR2DCwjWXhrh23Ve5zdLGhzQ0aiQ5tOt+pn1fyacNdeO1De9dsutEZb6TMV30CJ1I7TRKV02mF6pSnU9CLTUcP3n6/sZt3usbHJ6qSrAKBp7DatA60DT0A+AC5Pth0/afNaHF7PTso4mpcO66hJ+JPiT8V131T/5v4S/DOQfvGGDh++L8021/GX/+TaLk+1P85zn8C0/buVJ3dBwznvO3btwixxdXQ8lnVEPFrlcErerrFzKufhrUdEyDMt5cGrdQ2pxCjJT6FKQrVBK00E15t463TyhxHtqPZMMd5dW8dtMWd2KMviktA0PY+VzIyBUEgvBINW18FF+Ld6YDYXI+cfumR9tBM+eMO7b36ZGXJJa5sbXPBNCOjSARQ0UT/TqwzJePeWOccNzCtOnyWiocXjWtacuDP8q89LlS20ebrJU2A+S48hCiNt1adFaa66kIH7RNu5naW/Nzbd3BD6fM2trbNlj1sfpJe54GuNz2Oq1wNWuI6/Spb7js1jNxbSwWaw0vexlxcTujfpczUA1rT5Xta8dQR5mg9FSDid11nu7wlbLrjSz55rmjU0tTajafzUmH2jUkyM23mHFIWn4KQoyPwMxmrYkj4+f8a6Nxa47pjFQadHXmlw6fAtJBHxBIPQq893MY/h6+DwCPqB56ivUW1QfuggEH4EAqf/qc/wBfWJf+oqH8c8+Fq+9P/FKw/wD1+D+l3yr32vf8AXn++Jf6NaLXOMhLSCAiAiAiAiAiAit5wp3pcpcEYUWCYhQYBY1BW0+4KTklXkUyy8zYpjpfb61ZlVPF6CSjJ2F0dxanqo/sX/xv7jt78Xbb+y+AtcVNj+++bVcRXD5NUmmorHcxN0jSKDRXxqSqf3vwntXfub+vsxcZCO87LI6QyQtZpZWho+CR1fMa+anyCw/n/ucz3uO9pe96jEKr2b696Z7VgXMHr+4fRvO+f9Xv7zq9L0NnpdPpbdy927VO2Pcrc07p5g9B9pbfHwfV3f7fpWTM1d/s69fdnmrTst06dNKurWop3HH3F+A429X9RzXkvre1r7743U7Pc06O3FFSvddqrqrQUp1rOtB9RzmjGKKlxumwriCJUY/VV9LVxU0OZaR6+riNQobJGWekR9OOwktdPHQWhiveByNhcXbYfHY3b8ePtII4Y29i78scbQxg/Pvg0AKB5D227Kyl/Pk72+zD7y4lfI93dtur3uLnH81+JJVOuTuRsi5azrIeQsrOH67kkpmRLarm5DNfFbjRI8CHDgMypU2Q1DhworbbaVuuK2p8VGepjPW9d35ffu57vdud7f1nePDniMOEbQ1jWMYwOc9wYxjWtaC5xoOpJ6q5dr7bxu0cDb7dxOv0FswhpeQXuLnF7nPLWtBc5ziSQ0Cp6ABWU4l76uYuHMDp+PMep8CuKShXP9Nk5NV5HMtGmbCfJsnIqpFbldRHXGYky19IujuQgyTqZEWlx7C9z3IXHm17faWJt8VcY21L+265iuHygSPdIW6o7qJpaHOdpGioBpUgClabv4H2bvPPzbjyM2QhvrgM1thkhawljAwOo+CQ1LWjV5qE9aVJURc78+5T3CZBT5PmNBh9PcU9SdIiTikC4g+erylvTY7dgm2vrw3VQ35L3SNBt6E6olbvl2wDlDlTOcs5a3zW4bXH2+Qt7fsh1qyVmuPUXtEndnmqWOc7SW6ejiDXpSX7C4/xXHePmxeGuLyazmm7umd8btL9IaSztxRU1BrdVa/eilOtbOw/qX87QokWE1ifEimokZiK2pyizE3FNx2ktINZpzxCTWaUFroRFr9gXVb+8zlC2t47aOwwBZGxrRWC7rRoAFf5cOvTr0Cq+b2x7CnmfO+7y4e9xcaS29Kk1NP5L4KtTXcPlqednO4N/H8Nm5iuxXbIqZlfcuYqxYejFSsSWYDd+1ab4bKSea3TVbJCSX4kRJKm2ct55vJ55YltMdJuEzGURPjmNq2Ts9kODBOJasFHtrMaSAO60AVmO46xB2GOPGXF6zDCPtmRr4xOWdzukF5iLKOPldSMVYSPjVWCyX6jvO2TY7e449Q8Z1bN/T2VM9ZU9NlbFrAZs4b0J2XWvy80mxmJzDbxqaWtlxKVkRmk9NBbGZ94HKGaxF1h5LXCwR3VvJCZIobpsrBIwsLo3OvHta9oNWkscAaGhVeYz22bCxmRt8ky4ykr7eZkgZJJAY3ljg4NeG2zXFpIo4BzSRUVCofW2M2osYFtXPri2FXNi2MCU2ejkabCfbkxX2z+wtl9pKi/fIZds7u5x95Ff2bzHdwSNkY4eLXscHNcPmHAEK/Lm2gvLaS0uWh9tKxzHtPg5rgWuB+RBIWwv9pxz1/wlxF/qHMv+/hrT9dPlL/UMB/1F3/45Z1/Ve2B/rmY/wCttv8AwioJlF85lOSX+TPV1ZUv5BcWN0/WUrMmPUQX7OW7MfjVseXLnSY8Fp14yabW84aEaFroQytm8o/OZi6zMkMMEl3cSTOjhDmxMdI4vLY2vc9zWAk6Wl7qCgr0WgcVYNxWMt8YySWZlvCyMPkLTI4MaGhzy1rWlxA8xDRU9aKVeCOfMp7e8ht8ow6hxC3uLeo9DXIyuDcTig165kedIRXpqb2kNpct+Iz1DWbmpNJJO35tZzxfypnOJstcZvb1rj7jIXFv2S66ZK/Qwva9wj7U8NC9zW6idXRoAp1rFN+8f4rkTHQ4rM3F5DZwzd0CB0bdT9JaC/uRS1DQ51AKffGtelMO5T5KyPl/PL/kPLPIovMhdiuSWKxqSxWRGoMGLWxIkBiXLnSGYseJEQkiW84oz1M1GZmYj2+N5ZjkDdF1u3O9oZO7c0ubGHNjaGMbGxrGuc9wa1rAAC9x8SSSV3O1Ns43Z2At9uYnuGxtg4NLyC9xc5z3OeWtaC4ucT0aB8AKBeOLeSsj4hzug5DxPyK7zHXpTkVizakP1stqbBk10uJPYiS4Mh6LIiS1pUSHm1EZkZKIyIx+bI3lmNgbotN24LtHJ2jnFrZA50bg9jo3te1rmOLXMe4EBzT8QQQv3dW2cbvDAXG3ctrFjctaHFhAe0tc17XMLmuAcHNBFWkfAghSNyp3J5ly5yLiHKGQ49hVZk+GlUphe3667iQLNFJcLu65FyxPyK0fkEzKdWnVp1hZtL26+CTTL98cx7i39u/H72y1pjYc1ju1o7EczWSCGUzRiYPuJHOo4kVa5h0mlejSI3tTjTC7Q23ebWx1xfS4u97mrvPic5hlj7TzGWQsAq0A+ZrhqFadSDy+fO6LPu4tvFWs3psOrUYg7cPVxYtAu4JyDu01aZaZ52uQ3RuJQVS30+n0jLcrU1alt5HKnNu6uXmWMe5bfHQtx7pTH6ZkzNXe7Ydr7s81QO03Tp00qak9KfHj/izb/G7rt+CmvZXXgjD+++J1O1r06O3DHT8oa6tXgKU61yDlrvG5N5g47h8X3FFgmN4pCk07zcfEKu/gSHItEw4zW1Tq7bJ7uOdYwo23NiWkr6jDZksiIyV22/fcLvTkHaMeycha4uzwUb4iG2kc7HFsDSI4iZbmZvbb5XUDQdTGeagIPXbQ4Z2vs7cb902c9/c5Z7ZATcPieA6UgvkAjgiOs9RUuIo53SpBFThQ6ttWtru8HkqFwi5wJKosEu8NXQWOOJn3NZfv5GzBmypMuK4zMi5PDrUyqN59BwlHENLZMNb0ubT3Xrae4LeVtxqeLJ7XF3O3TaSW4fNHO64DHuc9pD23LI9UJcOyTEQ3QzUHUNamueHNsz75G/4ri/gzQuGTaI3xCEua0NcC10Dn6ZQD3R3Ku1uoW1FKpCilbKtJiPdXkOL8dYlxjYcVcIZ/j2FLvHKF7kXCbLJ7KM5kNzNvLJZOLyWLBaW5KnGjVmO0ZtNoJW407ju7Ac55bCbRsNl3eC2zlcTjTMYDkLOS5kaZ5nzSGpuGsBLn08jG1a1odUipqvMcT47K7jvN0W+WzuPyN8IhKLO5ZAxwhjbEwUELnGjW18z3eYuIoDQQBmGSFl+S2+SFQY1ixW0hMj2/h9Yqmxqs2stM9Coq1SJaocdXS3mnqL+dSj18dBVW4cx9oMzcZkWlnY99+rsWkfZt4+gGmKPU7Q3pWmo9ST8VYOGxn1PjIcZ6i6u+y2nduH9yZ/Umsj6N1HrStB0ACyrh7ljIuE88rOQ8VhUthdVUazix4uQRp0qrW3awH66QbzNdY1UtS0MSFGja+kiWRGZGXgfecfb7y/G26YN24OO2lyUDJGtbO17oiJY3RuqI5InVDXEijx1pWo6Lqt5bSxu98BLt3LPnjspnMcXQlrXgxvDxQvZI3qQK1aenhTxUYCFKUKTcM5WyHBsO5Lwiph0siq5Tq6apyCRYx5z1hDjUc2TPiLpnY1jEjR33HpSicN9qQk0kWhJPUzmm3d9ZfbO3sztqwjtn2OcghindI15kY2F7ntMJbIxrSS46tbZARSgB6qMZraeOzuZxmcu3ztu8VLJJCGFoY4ytDHCQOY5xADRTS5hr4k+CjIQtSdTHwzzZkPCVrktnQ4/h+TN5bi03Drynzern3FJMo7GTEkTozsCDbVHXTKTEJpxDqnGlsrWlSD11Kw+OuSctxrfXl7i7TH3rL+xfaTRXkT5YXwyOa57SxksWrVp0uDi5paXAtNekM3rsjHb4tLa1yFxeWrrS6bcRSW0jI5WysDg1we6OSmnVqBaA4OAIcKLncmc3J5KoYlEXD/B3H/lLdi29Z4zwNzF76T0Ic+H6ZLsF3NiT1Q95/quM7C3PMtK3Fs0Pk7z5LG8sXHjBt/bOJ7dw2XvY6xNtO7Sx7O26QzSViOvU5lBV7GGvlofhtjYx2zfvvzmc7kdcJj7d7dCeJtXMdraztspINGkOr0a54p16QcKzU6QEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEQEUvYPzxyvxviWSYLhmWLpsUy7z3uGoOmx6ybnnZViaecpMm2qZ82CuTWoS0pUd1o9EkZGSiIyn+2eUd97OwN5tjbl+bfBZDX34uzBIH9yPtPIdLE97C6MBpMbmnoD4gFQ/O7B2luXL22ezVoJstZ6ezJ3JmFmh/cb5Y5GNcGvJcA9rvEjwJCjWjurTG7qoyKjmO111Q2cG4qLBkkKdhWVbKamQZbaXUONKXHkspWRKSpJmWhkZeAh2MyV9hsjb5fGSOhyVrMyWKQUqySNwexwqCKtcAaEEdOoIUmvrG0ydlNjr9gksriJ0cjDWjmPaWuaaUNC0kdCD9BUqcpdwvMPNMKpruS8wVkkKjlSJtZHKixqmRHlSmkMPPKPH6aqVJUplBJLqmskEZ7dNT1nG9+WuQeR7aCz3nkDeW1s9z429m3hDXOAaT+IhiLjQUGqtOtKVNYptXjvZuyZ5rnbFmLaedga892aQlrTUD8bJJTqa+Wlela0C/HFvcFy/wAKxbiFxnmC8biX0iJKtWDpMbum5MiE28zGeSWQU9t5VaWn1JUbPT6habt21On87H5Y5A44guLbZmQNnBdPa6VvZt5g5zAQ0jvxS6TRxB0adXTVWgp+7q482dvaWGfc9mLma3a5sZ7s0ZaHEEj8TJHqFQCNVadaUqaxnkWQXGWX91lGQzV2V7kNpPurie42wyqZZWUlyZMkdCM0xFYS6+6oybaQhpstEoSlJERQzL5bIZ7K3Oby0pmyl3O+aV5ABfJI4ve7S0BrauJo1oDWjo0AABSjG46zxGPgxeOYIrC2ibHGwEnSxjQ1oq4lxoAOriXHxJJJKnHCu7PuJ49qGqLFuUbqPUR2WI8SFbQqLKG4MeN1ehGr1ZTVXLsCM2l0yJtlSEbSSnTRCCTZm2+eeXNpWDcXg83cssGNDWslZBchjW1o1huYpixorTSwtFABSjW0guc4j443FeG/yuKgdeOJLnRulgLiaVL+xJGHk08XAmtTWpNYtz3krPeULdN7yBldzlVm2hbUd61lKcYgsuK6i49bBbJqBWRluFuNqO002avHTXxEI3TvLdO9r8ZPdd/cX14AQ0yuq1gJqWxsFGRtJ6lsbWtJ60qpVgNs7f2tZ+g29aQ2lqTUiNtC4joC9xq97gOmp7nGnStFMmNd5XczidUxTU/K9uqBGShDCbqqxnJ5TTbbTbLbSbHJqS3sSZbbaIko6uwvEyLUzM7Dw3uI5nwNi3HY/O3BtWABveitrlwAAAAkuYZZKAAUGqg8aVJULyfC/GGXu3Xt5iYRcO8e3JPA0kkknRBLGypJ6nTX59Aov5F5k5Q5aksSeRc2u8p8o6+/CiTX22KqC9KUpT7sCmgtRKmC44R7TNlhB9NKUfaJSRQnd/Ie9t+zNm3dkrm+7bnOYx7gImFxq4shYGxMJ8KtYPKA371oAlW29mbW2jG6LbljBa6wA5zQTI4N8A+RxdI4Dx8zj1JPiST2HJvO3K/MkXHofJWWu5OxiyZqaPrVFBXvRPUW4LU1bsqnqq+TYOyEVrO5clbytUa66qUZ8venKG+uQ4LS33lfuvYrEP7NYoIy3uBgeS6KKN0hcI2VMheaitakk8ba+wtpbMluJts2YtZLvT3aSSvDtBcW0bJI9rAC91AwNHWngBTKMB7q+4HjGjZxrDOSbOuoYqUtwqyfW4/kUavaQt1aWK0slqLdddG3vKM22DbbMzLUvAtO72rznyvsvGNw23MzPDi2ABkb44LhsYBJ0x+oilMbak+Vha35dBTq9wcT8e7ov3ZPNYyKS/eauex80LnnoKv7Mkes9B1dU/PqV12LdyvNuF5blmdY7nUiJlmcqZVlNzKpcat3rXyy1LjpNi5prCHDQwatEJjttJSgiSRbUkRcTB8yclbcz9/ujEZN8eeydPUzOht5TLpNW+WaGRjA3wAja0AUaOgAHIyvGex81iLTA5Kwa/EWFexG2SaMR6hQ9Y5GOcT4kvLiTU+JJUW1+V5BVZVDzeusnImUwL5nJ4ds0zGJxi8jz02bU5MY2ThGaZySX0zbNo/tTSafAQi0zuWsc7HuW0mMeciuhcslAbVszX9wPDaaOj+unTp+BbTopVcYnHXeJfg7mIPxUluYHRkuoYizQW1rq+96V1avjWvVZLydy1yDzJfRMn5IyD3HeQKiPQxZ3pVJUdKpizbCwYieWoa2rhr2TLR9fUU2bh79DUaUpIu43rv3dnImVjzW8bv1mTit2wNf2oYqRNe+RrdMEcbDR8jzqLS46qE0AA6za+0dvbLx78Xtq39NYyTGVze5LJWRzWMLtUr3uFWsaKA6elaVJJjkRBSRARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARARf//Z";
        }

        // Final replacement logic
        templateHtml = CommonService.replaceAll(templateHtml, "||IMG_SRC||", imgSrc);
        templateHtml = CommonService.replaceAll(templateHtml, "||GOALPRDUCTTBL||", prodRecommStr);
        templateHtml = CommonService.replaceAll(templateHtml, "||line1FhrD||", line1Reason);
        templateHtml = CommonService.replaceAll(templateHtml, "||line2FhrD||", line2Reason);
        templateHtml = CommonService.replaceAll(templateHtml, "||DATE||", currDate);
        templateHtml = CommonService.replaceAll(templateHtml, "||DEVIATION_SIGN||", sign);

        return templateHtml;
    };

    /**
     * Function to Generate FHR Report File
     * @param {any} fhrPersonalDetails  - All the Details related FHR Id
     * @returns     success or null
     */
    this.saveFhrRecommDeviReportFile = function(fhrData, fileName, htmlOutput, docId) {
        debug("Inside saveFhrRecommDeviReportFile " + JSON.stringify(fhrData));
        var dfd = $q.defer();
        CommonService.saveFile(fileName, ".html" /*File Type*/ , "/FromClient/FHR/HTML/", htmlOutput, "N" /*isBase64*/ ).then(
            function(res) {
                cordova.exec(
                    function(fileName) {
                        debug("fileName: " + fileName);
                        FhrService.saveDocumentData(fhrData, "PR", FhrService.PDMPROOF_CODE, fileName, docId, "DEV").then(
                            function(res) {
                                dfd.resolve(res);
                            }
                        );
                    },
                    function(errMsg) {
                        dfd.resolve(null);
                    },
                    "PluginHandler", "getDecryptedFileName", [(fileName + ".html"), "/FromClient/FHR/HTML/"]
                );
            }
        );
        return dfd.promise;
    };

    /**
     * Function to Save Deviation Related Data
     * @param {object} personalDetailsData
     */
    this.saveDeviationData = function(fhrDetails, fileName) {
        debug("Inside saveDeviationData " + JSON.stringify(fhrDetails));

        var recomDeviReason = fhrrecommsc.getProdDeviReason();
        var currDate = CommonService.getCurrDate();
        var dfd = $q.defer();
        try {
            CommonService.transaction(db,
                function(tx) {

                    // Insert into LP_FHR_PRODUCT_RCMND
                    CommonService.executeSql(tx, "insert or replace into LP_FHR_PRODUCT_RCMND (FHR_ID, AGENT_CD, PRODUCT_PGL_ID, RCMND_PRODUCT_URNNO, MODIFIED_DATE) VALUES (?,?,?,?,?) ", [fhrDetails.FHR_ID, fhrDetails.AGENT_CD, null, 0, currDate],
                        function(tx, res) {
                            debug("successfully inserted LP_FHR_PRODUCT_RCMND");

                            // Insert into LP_MYOPPORTUNITY_DEVIATION
                            CommonService.executeSql(tx, "insert or replace into LP_MYOPPORTUNITY_DEVIATION (OPPORTUNITY_ID, FHR_ID, RECO_PRODUCT_DEVIATION_REASON, DEVIATION_SIGNED_TIMESTAMP, ISSYNCED) VALUES (?,?,?,?,?) ", [fhrDetails.OPP_ID, fhrDetails.FHR_ID, recomDeviReason, currDate, "N"],
                                function(tx, res) {
                                    debug("successfully inserted LP_MYOPPORTUNITY_DEVIATION");

                                    // Update LP_MYOPPORTUNITY
                                    CommonService.updateRecords(db, "LP_MYOPPORTUNITY", { "RECO_PRODUCT_DEVIATION_FLAG": "Y", "FHR_SIGNED_TIMESTAMP": currDate }, { "FHR_ID": fhrDetails.FHR_ID, "AGENT_CD": fhrDetails.AGENT_CD }).then(
                                        function(updateResFhr) {
                                            debug("successfully updated LP_MYOPPORTUNITY");

                                            //  Update LP_FHR_MAIN flags
                                            CommonService.updateRecords(db, "LP_FHR_MAIN", { "IS_SCREEN5_DONE": "Y", "COMPLETION_TIMESTAMP": currDate, "SIGNED_TIMESTAMP": currDate }, { "FHR_ID": fhrDetails.FHR_ID, "AGENT_CD": fhrDetails.AGENT_CD }).then(
                                                function(updateMainResFhr) {
                                                    debug("successfully updated LP_FHR_MAIN");
                                                    dfd.resolve("success");
                                                }
                                            );

                                        }
                                    );
                                }
                            );
                        },
                        function(tx, err) {
                            debug("Error in saveDeviationData().transaction(): " + err.message);
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    debug("Error in saveDeviationData(): " + err.message);
                    dfd.resolve(null);
                }, null
            );
        } catch (ex) {
            debug("Exception in saveDeviationData(): " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    };

}]);