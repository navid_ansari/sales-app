fhrModule.service('FhrService', ['$q', '$filter', '$state', '$stateParams', 'CommonService', 'LoginService', function($q, $filter, $state, $stateParams, CommonService, LoginService) {
    "use strict";
    debug("in FhrService");

    var fhrmain = this;
    this.outerWrapInnerHeight = "";
    this.fhrId = "";
    this.dateList = [];
    this.monthList = [];
    this.yearList = [];

    // FHR Relation List
    this.fhrRelationList = [];

    this.leadData = [];
    this.personalDetailsData = [];
    this.otherDetailsData = [];
    this.childGoalsData = [];
    this.outputDetailsData = [];

    this.goalsCountData = [];

    //  MPROOF Codes for Documents
    this.FHRMPROOF_CODE = "FH";
    this.PDMPROOF_CODE = "PD";

    // HTML OUTPUT Variables
    this.HtmlFormOutput = "";
    this.fhrOpHtmlData = "";

    // Templates Files List Object
    this.templateFilesList = {
        "child": "www/templates/FHR/ChildGoal.html",
        "protection": "www/templates/FHR/ProtectionGoal.html",
        "retirement": "www/templates/FHR/RetirementGoal.html",
        "wealth": "www/templates/FHR/WealthGoal.html",
        "product_deviation": "www/templates/FHR/FHR_Deviation.html"
    };

    /**
     * Redirection objects
     */
    this.redirObjService = {};

    this.redirObjService.gotoPersonalDetailsTab = function() {
        debug("Personal Details tab clicked");
        $state.go(FHR_PERSONAL_DETAILS, { LEADID: fhrmain.personalDetailsData[0].LEAD_ID, FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
    };

    this.redirObjService.gotoOthersTab = function() {
        debug("Others tab clicked");
        if (!!fhrmain.personalDetailsData[0].FHR_ID)
            $state.go(FHR_OTHERS, { LEADID: fhrmain.personalDetailsData[0].LEAD_ID, FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
        else
            fhrmain.showErrorFhrNoGen();
    };

    this.redirObjService.gotoGoalsTab = function() {
        debug("Goals tab clicked");
        if (fhrmain.personalDetailsData.length == "0") {
            fhrmain.showErrorFhrNoGen();
            return;
        }

        if (!!fhrmain.personalDetailsData[0].FHR_ID && !!fhrmain.personalDetailsData[0].ANN_INCOME_TOTAL) // Allow only if Both Personal Details and Other Details are added
            $state.go(FHR_GOALS, { FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
        else
            fhrmain.showErrorFhrNoGen();
    };

    this.redirObjService.gotoOutputTab = function() {
        debug("Output tab clicked");
        if (fhrmain.personalDetailsData.length == "0") {
            fhrmain.showErrorFhrNoGen();
            return;
        }

        if (!!fhrmain.personalDetailsData[0].FHR_ID && !!fhrmain.personalDetailsData[0].ANN_INCOME_TOTAL) // Allow only if Both Personal Details and Other Details are added
            $state.go(FHR_GOALS_OUTPUT, { FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
        else
            fhrmain.showErrorFhrNoGen();
    };

    this.redirObjService.gotoRecommendationTab = function() {
        debug("Recommendation tab clicked");
        if (fhrmain.personalDetailsData.length == "0") {
            fhrmain.showErrorFhrNoGen();
            return;
        }

        if (!!fhrmain.personalDetailsData[0].FHR_ID && !!fhrmain.personalDetailsData[0].ANN_INCOME_TOTAL) { // Allow only if Both Personal Details and Other Details are added
            var goalsCount = fhrmain.getGoalsCountDataRelatedToFHR(fhrmain.personalDetailsData[0].FHR_ID);
            // Only allow to recommendation tab if atleast 1 goal is added
            if (goalsCount.childsCount == '0' && goalsCount.protectionCount == '0' && goalsCount.retirementCount == '0' && goalsCount.wealthCount == '0') {
                navigator.notification.alert("Please add atleast 1 Goal first.", null, "Error", "OK");
            } else if (fhrmain.personalDetailsData[0].IS_SCREEN1_DONE != "Y" && fhrmain.personalDetailsData[0].IS_SCREEN2_DONE != "Y" && fhrmain.personalDetailsData[0].IS_SCREEN3_DONE != "Y" && fhrmain.personalDetailsData[0].IS_SCREEN4_DONE != "Y") {

                navigator.notification.confirm("After clicking Recommendation tab you can not add or edit any FHR Details. Output will be generated. If Ok then Press 'Yes'.",
                    function(buttonIndex) {
                        if (buttonIndex == "1") {
                            CommonService.showLoading();
                            // Generating output file on recommendation click
                            fhrmain.generateFhrFinalOutput(fhrmain.personalDetailsData[0], fhrmain.getOutputDetailsDataFromFhrMain()).then(function(resp) {
                                debug("Response of Generate FHR Final Output is " + resp);
                                if (resp == "success") {
                                    CommonService.hideLoading();

                                    // freezeFhrMainData function call to Freeze FHR Data 
                                    fhrmain.freezeFhrMainData(fhrmain.personalDetailsData[0].FHR_ID, fhrmain.personalDetailsData[0].AGENT_CD).then(function(res) {
                                        $state.go(FHR_RECOMMENDATIONS, { FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
                                    });
                                } else {
                                    CommonService.hideLoading();
                                    navigator.notification.alert("Error occured while generating output.", null, "Error", "OK");
                                    return false;
                                }
                            });

                        }
                    },
                    'Confirm Recommendation',
                    'Yes, No'
                );
            } else {
                $state.go(FHR_RECOMMENDATIONS, { FHRID: fhrmain.personalDetailsData[0].FHR_ID, AGENTCD: fhrmain.personalDetailsData[0].AGENT_CD });
            }
        } else
            fhrmain.showErrorFhrNoGen();
    };

    this.redirObjService.submitOnNext = function(form) {
        debug("Next button clicked");
        form.submit();
    };

    this.redirObjService.showError = function(msg) {
        navigator.notification.alert(msg, null, "Error", "OK");
    };

    this.openMenu = function() {
        CommonService.openMenu();
    };

    this.showErrorFhrNoGen = function() {
        navigator.notification.alert("Please fill Personal Details and Other Details first.", null, "Personal Details", "OK");
    };

    this.setGoalsCountDataRelatedToFHR = function(FHR_ID, goalsDataCount) {
        debug("Inside setGoalsCountDataRelatedToFHR");
        debug(FHR_ID + "|---|" + JSON.stringify(goalsDataCount));
        fhrmain.goalsCountData[FHR_ID] = goalsDataCount;
    };

    this.getGoalsCountDataRelatedToFHR = function(FHR_ID) {
        debug("Inside getGoalsCountDataRelatedToFHR");
        debug(FHR_ID + "|---|" + JSON.stringify(fhrmain.goalsCountData[FHR_ID]));
        return fhrmain.goalsCountData[FHR_ID];
    };

    this.setOuterWrapInnerHeight = function() {
        fhrmain.outerWrapInnerHeight = parseInt(window.innerHeight) - 50;
    };

    this.getOuterWrapInnerHeight = function() {
        return fhrmain.outerWrapInnerHeight;
    };

    // Function to generate array from start to end range
    this.arrayInitTo = function(n, v) {
        return Array(n).join().split(',').map(function() {
            var retArr = {};
            var label = "";
            label = (v < 10 ? '0' : '') + v;
            retArr.VAL = v;
            retArr.LABEL = label;
            v++;
            return retArr;
        });
    };

    // Functions to get Date, Month and Year range
    this.getDateRange = function() {
        this.dateList = this.arrayInitTo(31, 1);
        return this.dateList;
    };

    this.getMonthRange = function() {
        this.monthList = this.arrayInitTo(12, 1);
        return this.monthList;
    };

    this.getYearRange = function() {
        var d = new Date();
        var n = d.getFullYear();
        this.yearList = this.arrayInitTo(100, parseInt(n) - parseInt(100));
        return this.yearList;
    };

    this.getChildYearRange = function() {
        var d = new Date();
        var n = d.getFullYear();
        this.yearList = this.arrayInitTo(19, parseInt(n) - parseInt(18));
        return this.yearList;
    };

    this.setLeadData = function(leadObj) {
        this.leadData = leadObj;
    };

    this.getLeadData = function() {
        return this.leadData;
    };


    /**
     * Function to Set FHR Relation List
     *
     * @returns Relation List
     */
    this.setFhrRelationList = function() {
        debug("Inside setFhrRelationList");

        fhrmain.fhrRelationList = [];
        var dfd = $q.defer();
        var query = "SELECT * from LP_FHR_RELATION WHERE ISACTIVE='Y' ";
        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    fhrmain.fhrRelationList.push(res.rows.item(i));
                                }
                                debug("Resolved with data setFhrRelationList");
                                debug(fhrmain.fhrRelationList);
                                dfd.resolve(fhrmain.fhrRelationList);
                            } else {
                                debug("Resolved without data setFhrRelationList");
                                dfd.resolve(fhrmain.fhrRelationList);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(fhrmain.fhrRelationList);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(fhrmain.fhrRelationList);
                }
            );
        } catch (ex) {
            dfd.resolve(fhrmain.fhrRelationList);
        }
        debug("Exiting setFhrRelationList");
        return dfd.promise;
    };


    /**
     * Function to retrieve FHR Relation code based on Description
     *
     * @param {string} retVal
     * @returns relationCode
     */
    this.getFhrRelationList = function(retVal) {
        var relationCode = "";
        for (var i = 0; i < fhrmain.fhrRelationList.length; i++) {
            if (fhrmain.fhrRelationList[i].RELATIONSHIP_DESC == retVal) {
                relationCode = fhrmain.fhrRelationList[i].RELATIONSHIP_CODE;
                break;
            }
        }

        return relationCode;
    };

    // function to set personal details data in FHR Main Service
    this.setPersonalDetailsDataInFhrMain = function(FHR_ID, AGENT_CD) {
        debug("Inside setFHRPersonalDetails");
        fhrmain.personalDetailsData = [];
        var dfd = $q.defer();
        try {
            var query = "select LMO.LEAD_ID,LMO.OPPORTUNITY_ID OPP_ID,LMO.FHR_SIGNED_TIMESTAMP OPP_FHR_SIGNED_TIMESTAMP,LPF.*,LFM.SIGNED_TIMESTAMP MAIN_SIGNED_TIMESTAMP,LFM.COMPLETION_TIMESTAMP MAIN_COMPLETION_TIMESTAMP,LFM.IS_SCREEN1_DONE,LFM.IS_SCREEN2_DONE,LFM.IS_SCREEN3_DONE,IS_SCREEN4_DONE from LP_FHR_PERSONAL_INF_SCRN_A LPF " +
                "INNER JOIN  LP_MYOPPORTUNITY LMO on LMO.FHR_ID=LPF.FHR_ID " +
                "INNER JOIN  LP_FHR_MAIN LFM on LFM.FHR_ID=LPF.FHR_ID " +
                "where LPF.FHR_ID=? and LPF.AGENT_CD=?";

            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, query, [FHR_ID, AGENT_CD],
                        function(tx, res) {
                            if (!!res && res.rows.length > 0) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    fhrmain.personalDetailsData.push(res.rows.item(i));
                                }
                                debug("Resolved with data setFHRPersonalDetails");
                                dfd.resolve(fhrmain.personalDetailsData);
                            } else {
                                debug("Resolved without data setFHRPersonalDetails");
                                dfd.resolve(fhrmain.personalDetailsData);
                            }
                        },
                        function(tx, err) {
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    dfd.resolve(null);
                }
            );
        } catch (ex) {
            dfd.resolve(null);
        }
        debug("Exiting setFHRPersonalDetails");
        return dfd.promise;
    };

    // function to get personal details data in FHR Main Service
    this.getPersonalDetailsDataInFhrMain = function() {
        debug("Inside getFHRPersonalDetails");
        return fhrmain.personalDetailsData[0];
    };

    this.clearPersonalDetailsDataInFhrMain = function() {
        fhrmain.personalDetailsData = [];
    };

    // function to set output details data in FHR Main Service
    this.setOutputDetailsDataInFhrMain = function(outputData) {
        debug("Inside setOutputDetailsDataInFhrMain");
        fhrmain.outputDetailsData = [];
        fhrmain.outputDetailsData = outputData;
    };

    // function to get output details data in FHR Main Service
    this.getOutputDetailsDataFromFhrMain = function() {
        debug("Inside getOutputDetailsDataFromFhrMain");
        return fhrmain.outputDetailsData;
    };

    // Function to check whether atleast one goal is added or not
    this.checkIfGoalsAdded = function(obj) {
        if (obj.childsCount > 0 || obj.protectionCount > 0 || obj.retirementCount > 0 || obj.wealthCount > 0) {
            return true;
        } else {
            return null;
        }
    };

    // Function to repeate date in [1st Jan 1990] Format
    this.getFormattedDate = function(date) {
        var dateArr = date.split('-');
        var datePart = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
        var datePre = CommonService.getOrdinalSuffixOf(datePart.getDate());
        date = datePre + " " + $filter('date')(new Date(datePart), 'MMM yyyy');
        return date;
    };

    // Function to freeze FHR Data on Recommendation tab click
    /**
     * This will freeze FHR Data so that we can not edit Personal Details or Other details or Goals
     * Only Output form and Recommendation tab will be editable
     */
    this.freezeFhrMainData = function(FHR_ID, AGENT_CD) {
        debug("Inside freezeFhrMainData");
        var params = {};

        params.FHR_ID = FHR_ID;
        params.AGENT_CD = AGENT_CD;
        params.COMPLETION_TIMESTAMP = CommonService.getCurrDate(); // FHR Completion Timestamp
        params.SIGNED_TIMESTAMP = CommonService.getCurrDate();

        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx) {
                // Updating LP_FHR_MAIN
                CommonService.executeSql(tx, "UPDATE LP_FHR_MAIN SET IS_SCREEN1_DONE=?,IS_SCREEN2_DONE=?,IS_SCREEN3_DONE=?,IS_SCREEN4_DONE=? WHERE FHR_ID=? and AGENT_CD=?", ['Y', 'Y', 'Y', 'Y', params.FHR_ID, params.AGENT_CD],
                    function(tx, res) {
                        debug("updated LP_FHR_MAIN");
                        dfd.resolve(params);
                    },
                    function(tx, err) {
                        debug("Error in freezeFhrMainData.transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                debug("Error in freezeFhrMainData(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    this.setFHRId = function(fhrId) {
        fhrmain.fhrId = fhrId;
    };

    this.getFHRId = function() {
        return fhrmain.fhrId;
    };

    /**
     * FHR Output Related functions
     */

    // Function to Set FHR Output HTML Data
    this.setFHROpHtmlData = function(data) {
        debug("Setting FHR Op Html data");
        debug(data.substr(1, 1000));
        fhrmain.fhrOpHtmlData = data;
    };

    // Function to Get FHR Output HTML Data
    this.getFHROpHtmlData = function() {
        debug("Getting FHR Op Html data");
        debug(fhrmain.fhrOpHtmlData.substr(1, 1000));
        return fhrmain.fhrOpHtmlData;
    };

    /**
     * Function to retrieve Template content of FHR
     * Refer object - templateFilesList
     * @param {string} templateName ["all" or (child or protection or retirement or wealth)]
     * @returns template content
     */
    this.getAssetFile = function(templateName) {
        debug("Inside getAssetFile " + templateName);
        var retData = {};

        var dfd = $q.defer();
        if (templateName == "all") {
            // If only all templates are required
            var key = "";
            for (key in fhrmain.templateFilesList) {
                (function(key) {
                    debug("inside getFileAssets " + key);
                    fhrmain.getTemplateContent(fhrmain.templateFilesList[key]).then(function(content) {
                        retData[key] = content;
                        if (Object.keys(retData).length == Object.keys(fhrmain.templateFilesList).length) {
                            dfd.resolve(retData); // Resolve only if all data is fetched
                        }
                    });
                }(key));
            }
        } else {
            // If only 1 template is required
            fhrmain.getTemplateContent(fhrmain.templateFilesList[templateName]).then(function(content) {
                retData[templateName] = content;
                dfd.resolve(retData);
            });
        }
        return dfd.promise;
    };

    /**
     * Function to retrieve Template Contents of FHR templates
     * @param {string} path     Path of template
     * @returns                 Template content
     */
    this.getTemplateContent = function(path) {
        debug("Inside getTemplateContent " + path);
        var dfd = $q.defer();
        cordova.exec(
            function(fileContent) {
                debug("fileContent : " + (!!fileContent) ? (fileContent.length) : null);
                dfd.resolve(fileContent);
            },
            function(errMsg) {
                debug("inside cordova error :::" + errMsg);
                dfd.resolve(null);
            },
            "PluginHandler", "getDataFromFileInAssets", [path]
        );
        return dfd.promise;
    };


    /**
     * Function to append template header footer on generated html
     * @param {string} content  -   Final HTML Output with appended Header and Footer
     * @returns
     */
    this.appendTemplateHeaderFooter = function(content) {

        // Header HTML from template
        var headerHtml = "<html>" +
            "<head>" +
            "    <title></title>" +
            "    <style>        " +
            "        body{margin:0; font-family: 'tata_aia_printregular';}" +
            "        .logo-wrapper{padding-right:25px}" +
            "        .lbl-block{margin-bottom: 7px}" +
            "        .lbl{color:#fff; font-size:13px; margin-bottom:2px; line-height:14px}" +
            "        .lbl-var{font-size:15px; line-height:15px; font-weight:600}" +
            "        .solution-img{display: inline-block;padding: 0px 20px;}" +
            "        .contact-details {text-align: center;color: #5d5f5e; font-size:11px; line-height:11px}" +
            "        .rup-sign:after{content:'\\20B9'; font-size:15px; color:#fff}" +
            "        .output-left-table, .output-right-table {display:inline-block}" +
            "       .mainGoalTable{display:block;padding:0px 30px}" +
            "    </style>" +
            "</head>" +
            "<body style='font-family: serif'>";

        if (content == "") {
            content = "No data.";
        }

        //Footer HTML from template
        var footerHtml = "</body></html>";

        return headerHtml + content + footerHtml;
    };

    /**
     * Function to Generate FHR Output File
     * @returns Replaced output content
     */
    this.generateFhrFinalOutput = function(personalDetails, outputDetails) {
        debug("Inside generateFhrFinalOutput");
        var fhrPersonalDetails = personalDetails;
        var goalsOutputDetails = outputDetails;

        fhrmain.setFHRId(fhrPersonalDetails.FHR_ID);
        if (fhrmain.getFHROpHtmlData() !== "") {


        }

        var dfd = $q.defer();
        fhrmain.getAssetFile("all").then(
            function(fileData) {
                debug(fileData);
                debug(fhrPersonalDetails);
                debug(goalsOutputDetails);

                var childGoalTemplate = fileData.child;
                var protectionGoalTemplate = fileData.protection;
                var retirementGoalTemplate = fileData.retirement;
                var wealthGoalTemplate = fileData.wealth;
                var currDate = CommonService.getCurrDate();
                var nameTitle = (fhrPersonalDetails.TITLE == "1" ? "Mr." : "Ms.");

                fhrmain.HtmlFormOutput = "";

                // Child Goal - Education
                if (goalsOutputDetails.CHEDU.length > 0) {

                    var childEduDetail;
                    for (childEduDetail in goalsOutputDetails.CHEDU) {
						if(childEduDetail == "includes")
							continue;
                        var childEduHtml = childGoalTemplate;
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||CURRDATE||", currDate);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||TITLE||", nameTitle);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||PROPSERNAME||", fhrPersonalDetails.FIRST_NAME + " " + fhrPersonalDetails.MIDDLE_NAME + " " + fhrPersonalDetails.LAST_NAME);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||NAME||", goalsOutputDetails.CHEDU[childEduDetail].CLIENT_FIRST_NAME + " " + goalsOutputDetails.CHEDU[childEduDetail].CLIENT_MIDDLE_NAME + " " + goalsOutputDetails.CHEDU[childEduDetail].CLIENT_LAST_NAME);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||DOB||", goalsOutputDetails.CHEDU[childEduDetail].CLIENT_BIRTH_DATE);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||SUBGOAL||", goalsOutputDetails.CHEDU[childEduDetail].GOAL_DESC);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||AGE||", goalsOutputDetails.CHEDU[childEduDetail].GOAL_TARGET_AGE);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||CURRENTCOST||", goalsOutputDetails.CHEDU[childEduDetail].PRESENT_COST);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||CURRENTSAVINGS||", goalsOutputDetails.CHEDU[childEduDetail].PRESENT_SAVINGS);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.CHEDU[childEduDetail].ROI_RATE);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||RATEOFINFLATION||", goalsOutputDetails.CHEDU[childEduDetail].INFLATION_RATE);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||SUBGOALNAME||", goalsOutputDetails.CHEDU[childEduDetail].GOAL_DESC);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||TOTALAMOUNT||", goalsOutputDetails.CHEDU[childEduDetail].FUTURE_COST);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.CHEDU[childEduDetail].FUTURE_SAVINGS_VALUE);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.CHEDU[childEduDetail].GOAL_GAP_AMOUNT);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHEDU[childEduDetail].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHEDU[childEduDetail].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                        childEduHtml = CommonService.replaceAll(childEduHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHEDU[childEduDetail].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                        fhrmain.HtmlFormOutput += childEduHtml;
                    }
                }

                // Child Goal - Marriage
                if (goalsOutputDetails.CHMARR.length > 0) {

                    var childMarrDetail;
                    for (childMarrDetail in goalsOutputDetails.CHMARR) {
						if(childMarrDetail == "includes")
							continue;
                        var childMarriageHtml = childGoalTemplate;
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||CURRDATE||", currDate);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||TITLE||", nameTitle);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||PROPSERNAME||", fhrPersonalDetails.FIRST_NAME + " " + fhrPersonalDetails.MIDDLE_NAME + " " + fhrPersonalDetails.LAST_NAME);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||NAME||", goalsOutputDetails.CHMARR[childMarrDetail].CLIENT_FIRST_NAME + " " + goalsOutputDetails.CHMARR[childMarrDetail].CLIENT_MIDDLE_NAME + " " + goalsOutputDetails.CHMARR[childMarrDetail].CLIENT_LAST_NAME);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||DOB||", goalsOutputDetails.CHMARR[childMarrDetail].CLIENT_BIRTH_DATE);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||SUBGOAL||", goalsOutputDetails.CHMARR[childMarrDetail].GOAL_DESC);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||AGE||", goalsOutputDetails.CHMARR[childMarrDetail].GOAL_TARGET_AGE);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||CURRENTCOST||", goalsOutputDetails.CHMARR[childMarrDetail].PRESENT_COST);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||CURRENTSAVINGS||", goalsOutputDetails.CHMARR[childMarrDetail].PRESENT_SAVINGS);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.CHMARR[childMarrDetail].ROI_RATE);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||RATEOFINFLATION||", goalsOutputDetails.CHMARR[childMarrDetail].INFLATION_RATE);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||SUBGOALNAME||", goalsOutputDetails.CHMARR[childMarrDetail].GOAL_DESC);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||TOTALAMOUNT||", goalsOutputDetails.CHMARR[childMarrDetail].FUTURE_COST);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.CHMARR[childMarrDetail].FUTURE_SAVINGS_VALUE);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.CHMARR[childMarrDetail].GOAL_GAP_AMOUNT);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHMARR[childMarrDetail].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHMARR[childMarrDetail].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                        childMarriageHtml = CommonService.replaceAll(childMarriageHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHMARR[childMarrDetail].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                        fhrmain.HtmlFormOutput += childMarriageHtml;
                    }
                }

                // Child Goal - Wealth
                if (goalsOutputDetails.CHWEA.length > 0) {

                    var childWeaDetail;
                    for (childWeaDetail in goalsOutputDetails.CHWEA) {
						if(childWeaDetail == "includes")
							continue;
                        var childWealthHtml = childGoalTemplate;
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||CURRDATE||", currDate);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||TITLE||", nameTitle);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||PROPSERNAME||", fhrPersonalDetails.FIRST_NAME + " " + fhrPersonalDetails.MIDDLE_NAME + " " + fhrPersonalDetails.LAST_NAME);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||NAME||", goalsOutputDetails.CHWEA[childWeaDetail].CLIENT_FIRST_NAME + " " + goalsOutputDetails.CHWEA[childWeaDetail].CLIENT_MIDDLE_NAME + " " + goalsOutputDetails.CHWEA[childWeaDetail].CLIENT_LAST_NAME);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||DOB||", goalsOutputDetails.CHWEA[childWeaDetail].CLIENT_BIRTH_DATE);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||SUBGOAL||", goalsOutputDetails.CHWEA[childWeaDetail].GOAL_DESC);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||AGE||", goalsOutputDetails.CHWEA[childWeaDetail].GOAL_TARGET_AGE);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||CURRENTCOST||", goalsOutputDetails.CHWEA[childWeaDetail].PRESENT_COST);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||CURRENTSAVINGS||", goalsOutputDetails.CHWEA[childWeaDetail].PRESENT_SAVINGS);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.CHWEA[childWeaDetail].ROI_RATE);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||RATEOFINFLATION||", goalsOutputDetails.CHWEA[childWeaDetail].INFLATION_RATE);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||SUBGOALNAME||", goalsOutputDetails.CHWEA[childWeaDetail].GOAL_DESC);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||TOTALAMOUNT||", goalsOutputDetails.CHWEA[childWeaDetail].FUTURE_COST);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.CHWEA[childWeaDetail].FUTURE_SAVINGS_VALUE);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.CHWEA[childWeaDetail].GOAL_GAP_AMOUNT);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHWEA[childWeaDetail].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHWEA[childWeaDetail].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                        childWealthHtml = CommonService.replaceAll(childWealthHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.CHWEA[childWeaDetail].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                        fhrmain.HtmlFormOutput += childWealthHtml;
                    }
                }

                // Retirement
                if (goalsOutputDetails.RETIR.length > 0) {
                    var retirementHtml = retirementGoalTemplate;
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||CURRDATE||", currDate);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||TITLE||", nameTitle);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||NAME||", goalsOutputDetails.RETIR[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.RETIR[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.RETIR[0].GOAL_LAST_NAME);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||CURRENTAGE||", goalsOutputDetails.RETIR[0].GOAL_CURRENT_AGE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||RETIREMENTAGE||", fhrPersonalDetails.RETIREMENT_AGE); // From Personal Info
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||RETIREMENTINCOMENEEDED||", goalsOutputDetails.RETIR[0].GOAL_TARGET_AGE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||INCOMENEEDAMOUNT||", goalsOutputDetails.RETIR[0].PRESENT_COST);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||RATEOFINFLATION||", goalsOutputDetails.RETIR[0].INFLATION_RATE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||LUMSUMAMOUNTSAVEDAMOUNT||", goalsOutputDetails.RETIR[0].PRESENT_SAVINGS);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||RATEATWHICHAMOUNTWILLFROW||", goalsOutputDetails.RETIR[0].ROI_RATE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||ANNUALINCOMEPROJECTEDAGE||", fhrPersonalDetails.RETIREMENT_AGE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||WITHINFLATIONAMOUNT||", goalsOutputDetails.RETIR[0].FUTURE_COST);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||REQUIREDCORPUSAMOUNT||", goalsOutputDetails.RETIR[0].FUTURE_REQIRED_CORPUS);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.RETIR[0].FUTURE_SAVINGS_VALUE);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.RETIR[0].GOAL_GAP_AMOUNT);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.RETIR[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.RETIR[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    retirementHtml = CommonService.replaceAll(retirementHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.RETIR[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += retirementHtml;
                }

                // Protection Goal
                if (goalsOutputDetails.PROTE.length > 0) {
                    var protectionHtml = protectionGoalTemplate;
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||CURRDATE||", currDate);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||TITLE||", nameTitle);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||NAME||", goalsOutputDetails.PROTE[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.PROTE[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.PROTE[0].GOAL_LAST_NAME);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||MONTHLYHOUSEEXPENSES||", parseInt(fhrPersonalDetails.ANN_EXP_HOUSEHOLD) + parseInt(fhrPersonalDetails.LIABILITY_EMI)); // From Personal Info
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||RATEINFLATION||", goalsOutputDetails.PROTE[0].INFLATION_RATE);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||CURRENTRATE||", goalsOutputDetails.PROTE[0].ROI_RATE);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||EXISTINGINSURANCECOVER||", goalsOutputDetails.PROTE[0].SUM_ASSURED);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||MONTHLYEXPENSES||", parseInt(fhrPersonalDetails.ANN_EXP_HOUSEHOLD));
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||EXISTINGLOAN||", fhrPersonalDetails.LIABILITY_TOTAL); // From personal info
                    // protectionHtml = CommonService.replaceAll(protectionHtml, "||CURRENTSAVINGS||", goalsOutputDetails.PROTE[0].PRESENT_SAVINGS); // Need to check
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||TOTALINSURANCECOVER||", (parseFloat(goalsOutputDetails.PROTE[0].PROVISION_GAP_EMERGENCY_FUND) + parseFloat(goalsOutputDetails.PROTE[0].SUM_ASSURED)));
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||INSURANCECOVERREQUIRED||", goalsOutputDetails.PROTE[0].PROVISION_GAP_EMERGENCY_FUND);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.PROTE[0].SUM_ASSURED);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.PROTE[0].PROVISION_GAP_EMERGENCY_FUND);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.PROTE[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.PROTE[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    protectionHtml = CommonService.replaceAll(protectionHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.PROTE[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += protectionHtml;
                }

                // Wealth - 2nd Home Down Payment
                if (goalsOutputDetails.WEAHOMTWO.length > 0) {
                    var wealthHomTwoHtml = wealthGoalTemplate;
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||CURRDATE||", currDate);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||TITLE||", nameTitle);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||NAME||", goalsOutputDetails.WEAHOMTWO[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEAHOMTWO[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEAHOMTWO[0].GOAL_LAST_NAME);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||SUBGOAL||", goalsOutputDetails.WEAHOMTWO[0].GOAL_DESC);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||AGE||", goalsOutputDetails.WEAHOMTWO[0].GOAL_TARGET_AGE);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||CURRENTCOST||", goalsOutputDetails.WEAHOMTWO[0].PRESENT_COST);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEAHOMTWO[0].PRESENT_SAVINGS);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEAHOMTWO[0].ROI_RATE);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEAHOMTWO[0].INFLATION_RATE);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||SUBGOALNAME||", goalsOutputDetails.WEAHOMTWO[0].GOAL_DESC);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEAHOMTWO[0].FUTURE_COST);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEAHOMTWO[0].FUTURE_SAVINGS_VALUE);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEAHOMTWO[0].GOAL_GAP_AMOUNT);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAHOMTWO[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAHOMTWO[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthHomTwoHtml = CommonService.replaceAll(wealthHomTwoHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAHOMTWO[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthHomTwoHtml;
                }

                // Wealth - Buying New Car
                if (goalsOutputDetails.WEACARBY.length > 0) {
                    var wealthCarBuyHtml = wealthGoalTemplate;
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||CURRDATE||", currDate);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||TITLE||", nameTitle);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||NAME||", goalsOutputDetails.WEACARBY[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEACARBY[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEACARBY[0].GOAL_LAST_NAME);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||SUBGOAL||", goalsOutputDetails.WEACARBY[0].GOAL_DESC);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||AGE||", goalsOutputDetails.WEACARBY[0].GOAL_TARGET_AGE);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||CURRENTCOST||", goalsOutputDetails.WEACARBY[0].PRESENT_COST);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEACARBY[0].PRESENT_SAVINGS);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEACARBY[0].ROI_RATE);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEACARBY[0].INFLATION_RATE);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||SUBGOALNAME||", goalsOutputDetails.WEACARBY[0].GOAL_DESC);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEACARBY[0].FUTURE_COST);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEACARBY[0].FUTURE_SAVINGS_VALUE);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEACARBY[0].GOAL_GAP_AMOUNT);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEACARBY[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEACARBY[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthCarBuyHtml = CommonService.replaceAll(wealthCarBuyHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEACARBY[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthCarBuyHtml;
                }

                // Wealth - International Vacation
                if (goalsOutputDetails.WEAINTVAC.length > 0) {
                    var wealthIntVacHtml = wealthGoalTemplate;
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||CURRDATE||", currDate);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||TITLE||", nameTitle);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||NAME||", goalsOutputDetails.WEAINTVAC[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEAINTVAC[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEAINTVAC[0].GOAL_LAST_NAME);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||SUBGOAL||", goalsOutputDetails.WEAINTVAC[0].GOAL_DESC);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||AGE||", goalsOutputDetails.WEAINTVAC[0].GOAL_TARGET_AGE);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||CURRENTCOST||", goalsOutputDetails.WEAINTVAC[0].PRESENT_COST);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEAINTVAC[0].PRESENT_SAVINGS);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEAINTVAC[0].ROI_RATE);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEAINTVAC[0].INFLATION_RATE);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||SUBGOALNAME||", goalsOutputDetails.WEAINTVAC[0].GOAL_DESC);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEAINTVAC[0].FUTURE_COST);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEAINTVAC[0].FUTURE_SAVINGS_VALUE);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEAINTVAC[0].GOAL_GAP_AMOUNT);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAINTVAC[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAINTVAC[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthIntVacHtml = CommonService.replaceAll(wealthIntVacHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAINTVAC[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthIntVacHtml;
                }

                // Wealth - Repaying Home Loan
                if (goalsOutputDetails.WEAREPAYHL.length > 0) {
                    var wealthRepayHlHtml = wealthGoalTemplate;
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||CURRDATE||", currDate);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||TITLE||", nameTitle);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||NAME||", goalsOutputDetails.WEAREPAYHL[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEAREPAYHL[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEAREPAYHL[0].GOAL_LAST_NAME);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||SUBGOAL||", goalsOutputDetails.WEAREPAYHL[0].GOAL_DESC);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||AGE||", goalsOutputDetails.WEAREPAYHL[0].GOAL_TARGET_AGE);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||CURRENTCOST||", goalsOutputDetails.WEAREPAYHL[0].PRESENT_COST);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEAREPAYHL[0].PRESENT_SAVINGS);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEAREPAYHL[0].ROI_RATE);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEAREPAYHL[0].INFLATION_RATE);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||SUBGOALNAME||", goalsOutputDetails.WEAREPAYHL[0].GOAL_DESC);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEAREPAYHL[0].FUTURE_COST);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEAREPAYHL[0].FUTURE_SAVINGS_VALUE);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEAREPAYHL[0].GOAL_GAP_AMOUNT);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAREPAYHL[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAREPAYHL[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthRepayHlHtml = CommonService.replaceAll(wealthRepayHlHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAREPAYHL[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthRepayHlHtml;
                }

                // Wealth - Starting New Business
                if (goalsOutputDetails.WEANWBUSI.length > 0) {
                    var wealthNewBusiHtml = wealthGoalTemplate;
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||CURRDATE||", currDate);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||TITLE||", nameTitle);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||NAME||", goalsOutputDetails.WEANWBUSI[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEANWBUSI[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEANWBUSI[0].GOAL_LAST_NAME);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||SUBGOAL||", goalsOutputDetails.WEANWBUSI[0].GOAL_DESC);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||AGE||", goalsOutputDetails.WEANWBUSI[0].GOAL_TARGET_AGE);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||CURRENTCOST||", goalsOutputDetails.WEANWBUSI[0].PRESENT_COST);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEANWBUSI[0].PRESENT_SAVINGS);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEANWBUSI[0].ROI_RATE);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEANWBUSI[0].INFLATION_RATE);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||SUBGOALNAME||", goalsOutputDetails.WEANWBUSI[0].GOAL_DESC);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEANWBUSI[0].FUTURE_COST);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEANWBUSI[0].FUTURE_SAVINGS_VALUE);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEANWBUSI[0].GOAL_GAP_AMOUNT);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEANWBUSI[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEANWBUSI[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthNewBusiHtml = CommonService.replaceAll(wealthNewBusiHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEANWBUSI[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthNewBusiHtml;
                }

                // Wealth - Other Goal
                if (goalsOutputDetails.WEAOTHGOAL.length > 0) {
                    var wealthOtherGoalHtml = wealthGoalTemplate;
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||CURRDATE||", currDate);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||TITLE||", nameTitle);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||NAME||", goalsOutputDetails.WEAOTHGOAL[0].GOAL_FIRST_NAME + " " + goalsOutputDetails.WEAOTHGOAL[0].GOAL_MIDDLE_NAME + " " + goalsOutputDetails.WEAOTHGOAL[0].GOAL_LAST_NAME);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||DOB||", fhrPersonalDetails.BIRTH_DATE); // From Personal Info
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||SUBGOAL||", goalsOutputDetails.WEAOTHGOAL[0].GOAL_DESC);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||AGE||", goalsOutputDetails.WEAOTHGOAL[0].GOAL_TARGET_AGE);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||CURRENTCOST||", goalsOutputDetails.WEAOTHGOAL[0].PRESENT_COST);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||CURRENTSAVINGS||", goalsOutputDetails.WEAOTHGOAL[0].PRESENT_SAVINGS);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||INTERESTRATESAVINGS||", goalsOutputDetails.WEAOTHGOAL[0].ROI_RATE);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||RATEOFINFLATION||", goalsOutputDetails.WEAOTHGOAL[0].INFLATION_RATE);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||SUBGOALNAME||", goalsOutputDetails.WEAOTHGOAL[0].GOAL_DESC);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||TOTALAMOUNT||", goalsOutputDetails.WEAOTHGOAL[0].FUTURE_COST);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||EXISTINGPROVISIONAMOUNT||", goalsOutputDetails.WEAOTHGOAL[0].FUTURE_SAVINGS_VALUE);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||SHORTFALLAMOUNT||", goalsOutputDetails.WEAOTHGOAL[0].GOAL_GAP_AMOUNT);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||PRODUCT1||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAOTHGOAL[0].PRODUCT_DETAILS1.replace(/-+/g, '')]);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||PRODUCT2||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAOTHGOAL[0].PRODUCT_DETAILS2.replace(/-+/g, '')]);
                    wealthOtherGoalHtml = CommonService.replaceAll(wealthOtherGoalHtml, "||PRODUCT3||", PRODUCT_IMAGE_CONSTS[goalsOutputDetails.WEAOTHGOAL[0].PRODUCT_DETAILS3.replace(/-+/g, '')]);

                    fhrmain.HtmlFormOutput += wealthOtherGoalHtml;
                }

                fhrmain.HtmlFormOutput = fhrmain.HtmlFormOutput.replace("page-break-before:always;", "");

                fhrmain.HtmlFormOutput = fhrmain.appendTemplateHeaderFooter(fhrmain.HtmlFormOutput); // Getting Final HTML
                fhrmain.setFHROpHtmlData(fhrmain.HtmlFormOutput);

                // Saving FHR Report File
                fhrmain.saveFhrReportFile(fhrPersonalDetails).then(function(resp) {
                    dfd.resolve(resp);
                });

            }
        );
        return dfd.promise;
    };

    /**
     * Function to Generate FHR Report File
     * @param {any} fhrPersonalDetails  - All the Details related FHR Id
     * @returns     success or null
     */
    this.saveFhrReportFile = function(fhrPersonalDetails) {
        debug("Inside saveFhrReportFile " + JSON.stringify(fhrPersonalDetails));
        debug(fhrmain.getFHROpHtmlData().substr(1, 200));
        var dfd = $q.defer();
        CommonService.saveFile(fhrPersonalDetails.FHR_ID, ".html" /*File Type*/ , "/FromClient/FHR/HTML/", fhrmain.getFHROpHtmlData(), "N" /*isBase64*/ ).then(
            function(res) {
                cordova.exec(
                    function(fileName) {
                        debug("fileName: " + fileName);
                        // Fetching Doc Id
                        fhrmain.getDocId("PR", fhrmain.FHRMPROOF_CODE).then(function(docId) {

                            // Saving Document related to FHR
                            fhrmain.saveDocumentData(fhrPersonalDetails, "PR", fhrmain.FHRMPROOF_CODE, fhrPersonalDetails.FHR_ID + ".html", docId, "FHR").then(
                                function(res) {
                                    dfd.resolve(res);
                                }
                            );

                        });
                        //CommonService.openFile(fileName);
                    },
                    function(errMsg) {
                        dfd.resolve(null);
                    },
                    "PluginHandler", "getDecryptedFileName", [(fhrPersonalDetails.FHR_ID + ".html"), "/FromClient/FHR/HTML/"]
                );
            }
        );
        return dfd.promise;
    };

    /**
     *
     * Function to Save Document related to FHR Id
     * @param {obj} fhrPersonalDetails - All the Details related FHR Id
     * @param {string} customerCategory -   "PR" or "IN"
     * @param {string} mproofCode   -   FH -> Financial Health Review , PD -> Recommended Product Deviation
     * @param {string} documentName
     * @param {string} docId
     * @param {string} docCat   - FHR / DEV
     * @returns     success or null
     */
    this.saveDocumentData = function(fhrPersonalDetails, customerCategory, mproofCode, documentName, docId, docCat) {

        var dfd = $q.defer();
        try {
            CommonService.transaction(db,
                function(tx) {
                    CommonService.executeSql(tx, "insert or replace into LP_DOCUMENT_UPLOAD (DOC_ID, AGENT_CD, DOC_CAT, DOC_CAT_ID , POLICY_NO , DOC_NAME, DOC_TIMESTAMP, DOC_PAGE_NO, IS_FILE_SYNCED, IS_DATA_SYNCED) VALUES (?,?,?,?,?,?,?,?,?,?) ", [docId, fhrPersonalDetails.AGENT_CD, docCat, fhrPersonalDetails.FHR_ID, null, documentName, CommonService.getCurrDate(), "0", "N", "N"],
                        function(tx, res) {
                            debug("successfully updated LP_DOCUMENT_UPLOAD");
                            dfd.resolve("success");
                        },
                        function(tx, err) {
                            debug("Error in LP_DOCUMENT_UPLOAD().transaction(): " + err.message);
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    debug("Error in LP_DOCUMENT_UPLOAD(): " + err.message);
                    dfd.resolve(null);
                }, null
            );
        } catch (ex) {
            debug("Exception in LP_DOCUMENT_UPLOAD(): " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    };

    /**
     * Function to get Doc Id related to FHR
     * @param {string} CUSTOMER_CATEGORY
     * @param {string} MPPROOF_CODE
     * @returns     DOC_ID or null
     */
    this.getDocId = function(CUSTOMER_CATEGORY, MPPROOF_CODE) {
        var dfd = $q.defer();
        try {
            db.transaction(
                function(tx) {
                    var query = "Select * from LP_DOC_PROOF_MASTER where CUSTOMER_CATEGORY = ? AND MPDOC_CODE= 'FD' AND MPPROOF_CODE=?";
                    tx.executeSql(query, [CUSTOMER_CATEGORY, MPPROOF_CODE],
                        function(tx, results) {
                            if (results.rows.length > 0) {
                                dfd.resolve(results.rows.item(0).DOC_ID);
                            } else {
                                debug("No record of Doc ");
                                dfd.resolve(null);
                            }
                        },
                        function(tx, err) {
                            debug("Error: " + err.message);
                            dfd.resolve(null);
                        }
                    );
                },
                function(err) {
                    debug("Error: " + err.message);
                    dfd.resolve(null);
                }, null
            );
        } catch (ex) {
            debug("Exception in DocId(): " + ex.message);
        }
        return dfd.promise;
    };


    /**
     * FHR Share Logic Starts here
     */

    this.fhrDataSync = {};

    /**
     * Main function to Share FHR
     * Called from ViewReport Controller
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} custEmailId
     * @param {bool} isCombo
     * @param {string} leadId
     * @returns Success or null
     */
    this.shareFHR = function(agentCd, fhrId, custEmailId, isCombo, leadId) {
        var dfd = $q.defer();
        debug("Agent Cd: " + agentCd);
        debug("fhrId: " + fhrId);
        debug("custEmailId: " + custEmailId);
        debug("isCombo: " + isCombo);

        fhrmain.createEmailEntry(agentCd, fhrId, custEmailId).then(
                function(createEmailEntryRes) {
                    debug("in createEmailEntryRes");
                    if (!!createEmailEntryRes && createEmailEntryRes == "S") {
                        return fhrmain.syncFHRData(agentCd, fhrId).then(
                            function(fhrDataSyncRes) {
                                debug("fhrDataSyncRes: " + fhrDataSyncRes);
                                if (!!fhrDataSyncRes && fhrDataSyncRes == "S") {
                                    debug("updating LP_FHR_MAIN");
                                    return CommonService.updateRecords(db, 'LP_FHR_MAIN', { 'IS_FHR_SYNCED': 'Y' }, { 'FHR_ID': fhrId }).then(
                                        function(res) {
                                            return fhrDataSyncRes;
                                        }
                                    );
                                } else
                                    return null;
                            }
                        );
                    } else
                        return null;
                }
            )
            .then(
                function(fhrDataSyncRes) {
                    debug("in fhrDeviationDataSyncRes" + fhrDataSyncRes);
                    if (!!fhrDataSyncRes) {

                        //Retrieving FHR Sync Data
                        var fhrSyncData = fhrmain.getFHRDataForSync();
                        var deviFlagSynced = fhrSyncData.ISDEVSYNCED || null;

                        if (!!deviFlagSynced && deviFlagSynced == "N") {

                            return fhrmain.syncFHRDeviationDetails(agentCd, fhrId).then(
                                function(syncDeviationDetRes) {
                                    debug("in syncDeviationDetRes response: " + JSON.stringify(syncDeviationDetRes));
                                    return syncDeviationDetRes;
                                }
                            );
                        }else{
                            return fhrDataSyncRes;
                        }
                    } else
                        return "S";
                }
            )
            .then(
                function(fhrDataSyncRes) {
                    debug("in then fhrDataSyncRes" + fhrDataSyncRes);
                    if (!!fhrDataSyncRes) {
                        return fhrmain.syncDocumentUpload(agentCd, fhrId, isCombo).then(
                            function(syncDocUploadRes) {
                                debug("in syncDocUploadRes " + JSON.stringify(syncDocUploadRes));
                                if (!!syncDocUploadRes && syncDocUploadRes == "S") {
                                    if (!!isCombo) {
                                        return CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD', { 'is_data_synced': 'Y' }, { 'doc_cat_id': fhrId, 'doc_cat': 'FHR', 'agent_cd': agentCd }).then(
                                            function(res) {
                                                debug("If combo sync doc upload " + JSON.stringify(syncDocUploadRes));
                                                return syncDocUploadRes;
                                            }
                                        );
                                    } else {
                                        return fhrmain.updateFHROpDocShareFlag(fhrId, agentCd).then(
                                            function(res) {
                                                debug("If not combo sync doc upload " + JSON.stringify(res));
                                                return syncDocUploadRes;
                                            }
                                        );
                                    }
                                } else
                                    return syncDocUploadRes;
                            }
                        );
                    } else
                        return "S";
                }
            )
            .then(
                function(syncDocUploadRes) { // ONLY IF PRODUCT RECOMMENDATION DEVIATION
                    if (!!syncDocUploadRes) {

                        //Retrieving FHR Sync Data
                        var fhrSyncData = fhrmain.getFHRDataForSync();
                        var deviFlag = fhrSyncData.RECO_PRODUCT_DEVIATION_FLAG;

                        if (!!deviFlag && deviFlag == "Y") {

                            // Getting Doc Proof Id before sync
                            return fhrmain.getDocId("PR", fhrmain.PDMPROOF_CODE).then(function(docId) {

                                // Sync FHR Deviation Data with Server
                                return fhrmain.syncFHRDeviationDoc(agentCd, fhrId, isCombo).then(
                                    function(syncFhrSignRes) {
                                        debug("in syncFhrSignRes " + JSON.stringify(syncFhrSignRes));
                                        if (!!syncFhrSignRes && syncFhrSignRes == "S") {
                                            if (!!isCombo) { // Need to change doc format
                                                return CommonService.updateRecords(db, 'lp_document_capture', { 'is_file_synced': 'Y' }, { 'doc_cat_id': fhrId, 'doc_cat': 'FHR', 'agent_cd': agentCd, 'doc_name': "FHR_" + fhrId + "_" + docId + ".html" }).then(
                                                    function(res) {
                                                        debug("Update document upload flag DEV COMBO " + JSON.stringify(res));
                                                        return syncFhrSignRes;
                                                    }
                                                );
                                            } else {
                                                return CommonService.updateRecords(db, 'lp_document_upload', { 'is_file_synced': 'Y' }, { 'doc_cat_id': fhrId, 'doc_cat': 'DEV', 'agent_cd': agentCd, 'doc_name': "FHR_" + fhrId + "_" + docId + ".html" }).then(
                                                    function(res) {
                                                        debug("Update document upload flag DEV " + JSON.stringify(res));
                                                        return syncFhrSignRes;
                                                    }
                                                );
                                            }
                                        } else {
                                            return syncFhrSignRes;
                                        }
                                    }
                                );
                            });
                        } else {
                            return "S";
                        }
                    } else {
                        return null;
                    }
                }
            )
            .then(
                function(syncFhrSignRes) { // data sync ='Y' AND doc sync ='N'
                    debug("in then syncFhrSignRes " + syncFhrSignRes);
                    if (!!syncFhrSignRes && syncFhrSignRes != 'SKIP') {
                        return fhrmain.syncFHRReport(agentCd, fhrId, isCombo).then(
                            function(syncFhrReportRes) {
                                debug("in syncFhrReportRes " + JSON.stringify(syncFhrReportRes));
                                if (!!syncFhrReportRes && syncFhrReportRes == "S") {
                                    if (!!isCombo) {
                                        return CommonService.updateRecords(db, 'lp_document_capture', { 'is_file_synced': 'Y' }, { 'doc_cat_id': fhrId, 'doc_cat': 'FHR', 'agent_cd': agentCd, 'doc_name': fhrId + ".html" }).then(
                                            function(res) {
                                                return syncFhrReportRes;
                                            }
                                        );
                                    } else {
                                        return fhrmain.updateFHRShareFlag(fhrId, agentCd).then(
                                            function(res) {
                                                return syncFhrReportRes;
                                            }
                                        );

                                    }
                                } else
                                    return syncFhrReportRes;
                            }
                        );
                    } else if (!!syncFhrSignRes && syncFhrSignRes == "SKIP") {
                        return "S";
                    } else
                        return null;
                }
            )
            .then(
                function(syncFHREmail) {
                    debug("in then syncFHREmail " + syncFHREmail);
                    if (!!syncFHREmail) {
                        debug("inside syncFhrReportRes");
                        return fhrmain.syncFHREmail(agentCd, fhrId, fhrmain.EMAIL_ID_KEY, isCombo).then(
                            function(syncEmailRes) {
                                debug("inside syncEmailRes " + syncEmailRes);
                                if (!!syncEmailRes && syncEmailRes == "S") {
                                    return CommonService.updateRecords(db, 'lp_email_user_dtls', { 'issynced': 'Y' }, { 'email_cat_id': fhrId, 'email_cat': 'FHR', 'agent_cd': agentCd }).then(
                                        function(res) {
                                            return syncEmailRes;
                                        }
                                    );
                                } else
                                    return null;
                            }
                        );
                    } else
                        return null;
                }
            )
            .then(
                function(syncEmailRes) {
                    dfd.resolve(syncEmailRes);
                }
            );
        return dfd.promise;
    };


    /**
     * Function to create email entry related to FHR Id
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} custEmailId
     * @returns
     */
    this.createEmailEntry = function(agentCd, fhrId, custEmailId) {
        var dfd = $q.defer();

        function createNewEntry() {
            var dfd_new = $q.defer();
            fhrmain.EMAIL_ID_KEY = CommonService.getRandomNumber();
            var insertRecord = { "email_id": fhrmain.EMAIL_ID_KEY, "agent_cd": agentCd, "email_to": custEmailId, "email_cc": "", "email_subject": "", "email_body": "", "email_cat": "FHR", "email_cat_id": fhrId, "email_timestamp": CommonService.getCurrDate(), "issynced": "N" };
            CommonService.insertOrReplaceRecord(db, "lp_email_user_dtls", insertRecord, true).then(
                function(res) {
                    dfd_new.resolve("S");
                }
            );
            return dfd_new.promise;
        }

        CommonService.selectRecords(db, "lp_email_user_dtls", null, "issynced, email_id", { email_cat: "FHR", email_cat_id: fhrId, agent_cd: agentCd }).then(
            function(res) {
                if (!!res && res.rows.length > 0) {
                    if (res.rows.item(0).ISSYNCED != "Y") {
                        fhrmain.EMAIL_ID_KEY = res.rows.item(0).EMAIL_ID;
                        dfd.resolve("S");
                    } else {
                        createNewEntry().then(
                            function(newEntryRes) {
                                if (!!newEntryRes)
                                    dfd.resolve("S");
                                else
                                    dfd.resolve(null);
                            }
                        );
                    }
                } else {
                    createNewEntry().then(
                        function(newEntryRes) {
                            if (!!newEntryRes)
                                dfd.resolve("S");
                            else
                                dfd.resolve(null);
                        }
                    );
                }
            }
        );
        return dfd.promise;
    };

    /**
     * Function to Sync FHR Data with Server
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.syncFHRData = function(agentCd, fhrId) { // If FHR SYNCED Flag is already synced then do not sync
        debug("in syncFHRData");
        var dfd = $q.defer();
        var fhrDataShareRequest = { "REQ": { "ACN": "SFD", "AC": agentCd, "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID, "FHRDepDet": [], "FHRInsDet": [], "FHRCldEduGl": [], "FHRCldEduSav": [], "FHRCldMrgGl": [], "FHRCldMrgSav": [], "FHRCldWeaGl": [], "FHRCldWeaSav": [], "FHRWelGl": [], "FHRWelSav": [], "FHRRetGl": [], "FHRRetSav": [], "FHRLivStdGl": [] } };

        this.getFHRData(agentCd, fhrId).then(function(mainRes) {
            debug("in mainRes");
            debug(mainRes);
            if (!!fhrId && !!mainRes) {
                if (!!mainRes && mainRes != "S") {

                    // Adding FHR Main values in fhrDataShareRequest
                    var mainkeys = Object.keys(mainRes);
                    for (var i = 0; i < mainkeys.length; i++) {
                        fhrDataShareRequest.REQ[mainkeys[i]] = mainRes[mainkeys[i]];
                    }
                }
            }
            return mainRes;
        }).then(
            function(mainRes) {
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.getFHRAndPersonalData(agentCd, fhrId).then(function(persDetResp) {
                        debug(persDetResp);

                        // Adding Personal Details values in fhrDataShareRequest
                        var persDetKeys = Object.keys(persDetResp);
                        for (var i = 0; i < persDetKeys.length; i++) {
                            fhrDataShareRequest.REQ[persDetKeys[i]] = persDetResp[persDetKeys[i]];
                        }
                        return mainRes;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(mainRes) {
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.getFHRAndChildGoalsData(agentCd, fhrId).then(function(chGoalResp) {
                        debug(chGoalResp);

                        if (!!chGoalResp) {
                            // Adding Child Goals values in fhrDataShareRequest
                            for (var i = 0; i < chGoalResp.length; i++) {
                                if (chGoalResp[i].GOAL == "Childs Education")
                                    fhrDataShareRequest.REQ.FHRCldEduGl.push(chGoalResp[i]);
                                else if (chGoalResp[i].GOAL == "Childs Marriage")
                                    fhrDataShareRequest.REQ.FHRCldMrgGl.push(chGoalResp[i]);
                                else if (chGoalResp[i].GOAL == "Childs Wealth")
                                    fhrDataShareRequest.REQ.FHRCldWeaGl.push(chGoalResp[i]);
                            }
                        }
                        return mainRes;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(mainRes) {
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.getFHRAndProtectionGoalsData(agentCd, fhrId).then(function(protGoalResp) {
                        debug(protGoalResp);

                        if (!!protGoalResp) {
                            // Adding Protection Goals values in fhrDataShareRequest
                            for (var i = 0; i < protGoalResp.length; i++) {
                                fhrDataShareRequest.REQ.FHRLivStdGl.push(protGoalResp[i]);
                            }
                        }
                        return mainRes;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(mainRes) {
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.getFHRAndRetirementGoalsData(agentCd, fhrId).then(function(retGoalResp) {
                        debug(retGoalResp);

                        if (!!retGoalResp) {
                            // Adding Retirement Goals values in fhrDataShareRequest
                            for (var i = 0; i < retGoalResp.length; i++) {
                                fhrDataShareRequest.REQ.FHRRetGl.push(retGoalResp[i]);
                            }
                        }
                        return mainRes;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(mainRes) {
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.getFHRAndWealthGoalsData(agentCd, fhrId).then(function(weaGoalResp) {
                        debug(weaGoalResp);

                        if (!!weaGoalResp) {
                            // Adding Wealth Goals values in fhrDataShareRequest
                            for (var i = 0; i < weaGoalResp.length; i++) {
                                fhrDataShareRequest.REQ.FHRWelGl.push(weaGoalResp[i]);
                            }
                        }
                        return mainRes;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(mainRes) {
                debug(JSON.stringify(fhrDataShareRequest));
                if (!!mainRes && mainRes != "S") {
                    return fhrmain.syncFhrDataWithServer(fhrDataShareRequest).then(function(ajaxResp) {
                        return ajaxResp;
                    });
                } else
                    return mainRes;
            }
        ).then(
            function(ajaxResp) {
                debug("resolving syncRes " + JSON.stringify(ajaxResp));
                if (!!ajaxResp && ((ajaxResp == "S") || (!!ajaxResp && !!ajaxResp.RS && !!ajaxResp.RS.RESP && ajaxResp.RS.RESP == "S"))) {

                    // Update FHR Data Share flag after successfully syncing data
                    return fhrmain.updateFHRDataShareFlag(fhrId, agentCd).then(function(updateStat) {
                        if (!!updateStat) {
                            dfd.resolve("S");
                        } else {
                            dfd.resolve(null);
                        }
                    });
                } else
                    dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to Sync FHR Data with Server
     *
     * @param {object} fhrDataShareRequest
     * @returns
     */
    this.syncFhrDataWithServer = function(fhrDataShareRequest) {

        var dfd = $q.defer();
        CommonService.APP_AJAX_CALL(FHR_DATA_SYNC_URL, AJAX_TYPE, TYPE_JSON, fhrDataShareRequest, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp) {
                debug("resolving Ajax Call Success " + JSON.stringify(ajaxResp));
                dfd.resolve(ajaxResp);
            },
            function(data, status, headers, config, statusText) {
                debug("resolving Ajax Call Error" + JSON.stringify(statusText));
                dfd.resolve(statusText);
            }
        );
        return dfd.promise;
    }

    /**
     * Function to update FHR data share flag
     *
     * @param {string} fhrId
     * @param {string} agentCd
     * @returns true/false or null
     */
    this.updateFHRDataShareFlag = function(fhrId, agentCd) {
        var dfd = $q.defer();

        var currTimeStamp = CommonService.getCurrDate();

        CommonService.transaction(db,
            function(tx) {
                // Updating LP_FHR_MAIN
                var queryFhrMain = "UPDATE LP_FHR_MAIN set COMPLETION_TIMESTAMP = ?, SIGNED_TIMESTAMP=?, IS_SCREEN1_DONE=?, IS_SCREEN2_DONE=?, IS_SCREEN3_DONE=?, IS_SCREEN4_DONE=?  WHERE FHR_ID = ? AND AGENT_CD =? ";
                var parametersFhrMain = [currTimeStamp, currTimeStamp, "Y", "Y", "Y", "Y", fhrId, agentCd];

                CommonService.executeSql(tx, queryFhrMain, parametersFhrMain,
                    function(tx, resFhrMain) {
                        debug("Updated FHR LP_FHR_MAIN Flag");
                        debug(resFhrMain);
                        dfd.resolve(resFhrMain);
                    },
                    function(tx, err) {
                        debug("Error in LP_FHR_MAIN Flag");
                        dfd.resolve(null);
                    }
                );

                // Updating LP_MYOPPORTUNITY
                /*var queryOpp = "UPDATE LP_MYOPPORTUNITY set FHR_SIGNED_TIMESTAMP = ? WHERE FHR_ID = ? AND AGENT_CD =? ";
                var parametersOpp = [null, fhrId, agentCd];

                CommonService.executeSql(tx, queryOpp, parametersOpp,
                    function(tx, resOpp) {
                        debug("Updated FHR LP_MYOPPORTUNITY Flag");
                        debug(resOpp);

                        // Updating LP_FHR_MAIN
                        var queryFhrMain = "UPDATE LP_FHR_MAIN set COMPLETION_TIMESTAMP = ?, SIGNED_TIMESTAMP=?, IS_SCREEN1_DONE=?, IS_SCREEN2_DONE=?, IS_SCREEN3_DONE=?, IS_SCREEN4_DONE=?  WHERE FHR_ID = ? AND AGENT_CD =? ";
                        var parametersFhrMain = [currTimeStamp, currTimeStamp, "Y", "Y", "Y", "Y", fhrId, agentCd];

                        CommonService.executeSql(tx, queryFhrMain, parametersFhrMain,
                            function(tx, resFhrMain) {
                                debug("Updated FHR LP_FHR_MAIN Flag");
                                debug(resFhrMain);
                                dfd.resolve(resFhrMain);
                            },
                            function(tx, err) {
                                debug("Error in LP_FHR_MAIN Flag");
                                dfd.resolve(null);
                            }
                        );
                    },
                    function(tx, err) {
                        debug("Error in LP_MYOPPORTUNITY Flag");
                        dfd.resolve(null);
                    }
                );*/
            },
            function(err) {
                debug("Failed transaction");
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    }


    /**
     * Function to Send Deviation data to Server
     * 
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} fhrSyncData
     * @returns
     */
    this.syncFHRDeviationDetails = function(agentCd, fhrId) {
        debug("Inside syncFHRDeviationDetails - DATA only");

        //Retrieving FHR Sync Data
        // var fhrSyncData = fhrmain.getFHRDataForSync();
        var dfd = $q.defer();

        var fhrDevSyncRequest = { "REQ": { "AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID, "ACN": "MOD", "MODDET": [] } };

        debug("URL: " + FHR_DEVIATION_DATA_URL);

        var whereClauseObj = {};
        whereClauseObj.FHR_ID = fhrId;
        CommonService.selectRecords(db, "LP_MYOPPORTUNITY_DEVIATION", true, "OPPORTUNITY_ID, FHR_ID, RECO_PRODUCT_DEVIATION_REASON, DEVIATION_SIGNED_TIMESTAMP, ISSYNCED", whereClauseObj, "OPPORTUNITY_ID asc").then(
            function(res) {
                if (!!res && res.rows.length > 0) {

                    if (res.rows.item(0).ISSYNCED == "Y") {
                        debug("LP_MYOPPORTUNITY_DEVIATION already synced " + JSON.stringify(res));
                        dfd.resolve("S");
                    } else {

                        var modDetObj = { "OPPID": res.rows.item(0).OPPORTUNITY_ID, "FHRID": res.rows.item(0).FHR_ID, "RPDR": res.rows.item(0).RECO_PRODUCT_DEVIATION_REASON };

                        fhrDevSyncRequest.REQ.MODDET.push(modDetObj);
                        debug("fhrDevSyncRequest: " + JSON.stringify(fhrDevSyncRequest));

                        // Sending data to server
                        CommonService.ajaxCall(FHR_DEVIATION_DATA_URL, AJAX_TYPE, TYPE_JSON, fhrDevSyncRequest, AJAX_ASYNC, AJAX_TIMEOUT,
                            function(ajaxResp) {
                                debug("fhrDevSyncRes: " + JSON.stringify(ajaxResp));
                                if (!!ajaxResp && !!ajaxResp.RS) {
                                    if (ajaxResp.RS.MODDET[0].RESP == "S") {

                                        // Update LP_MYOPPORTUNITY_DEVIATION if successful server sync
                                        CommonService.updateRecords(db, 'LP_MYOPPORTUNITY_DEVIATION', { 'ISSYNCED': 'Y' }, { 'FHR_ID': fhrId }).then(
                                            function(res) {
                                                debug("LP_MYOPPORTUNITY_DEVIATION synced successfully" + JSON.stringify(res));
                                                dfd.resolve("S");
                                            }
                                        );

                                    } else {
                                        dfd.resolve(null);
                                    }
                                } else {
                                    dfd.resolve(null);
                                }
                            },
                            function(data, status, headers, config, statusText) {
                                dfd.resolve(null);
                            }
                        );
                    }
                } else {
                    dfd.resolve(null);
                }

            }
        );
        return dfd.promise;
    };

    /**
     * Function to Sync Document Upload data with Server
     * Used for the final submission as well
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} isCombo
     * @param {string} finalSubmission
     * @param {string} polNo
     * @returns S or null
     */
    this.syncDocumentUpload = function(agentCd, fhrId, isCombo, finalSubmission, polNo) {
        debug("Inside syncDocumentUpload - DATA only");
        var dfd = $q.defer();

        var acn = "FDS";
        var query = "SELECT IS_DATA_SYNCED, DOC_ID AS DOCID, DOC_CAT AS CAT,DOC_CAT_ID AS CATID,POLICY_NO AS POLNO,DOC_NAME AS DOCNM,DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG FROM LP_DOCUMENT_UPLOAD WHERE AGENT_CD=? AND DOC_CAT IN ('FHR','DEV') AND DOC_NAME NOT LIKE 'TRM%'";
        var parameterList = [agentCd];

        var URL = DOC_UPLOAD_SYNC_URL;

        if (!!isCombo) {
            query = "SELECT IS_DATA_SYNCED, DOC_CAP_ID AS DOCCAPID, DOC_CAP_REF_ID AS DOCCAPREFID, DOC_ID AS DOCID, DOC_CAT AS CAT,DOC_CAT_ID AS CATID,POLICY_NO AS POLNO,DOC_NAME AS DOCNM,DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG FROM LP_DOCUMENT_CAPTURE WHERE AGENT_CD=? AND DOC_CAT=? AND DOC_NAME NOT LIKE 'TRM%'";
            acn = "CFDS";
            URL = COMBO_DOC_UPLOAD_SYNC_URL;
        }

        if (!!fhrId) {
            query += " AND DOC_CAT_ID=?";
            parameterList.push(fhrId);
        }

        debug("syncDocumentUpload query: " + query);
        debug("syncDocumentUpload Parameter list: " + parameterList);
        debug("syncDocumentUpload URL: " + URL);

        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        debug("syncDocumentUpload response");
                        if (!!res && res.rows.length > 0) {
                            if (res.rows.item(0).IS_DATA_SYNCED != "Y" || (!!finalSubmission)) {
                                debug("res.rows.length: " + res.rows.length);
                                delete res.rows.item(0).IS_DATA_SYNCED;
                                if (res.rows.length > 1)
                                    delete res.rows.item(1).IS_DATA_SYNCED;
                                if (!!fhrId) {
                                    var fhrDet = CommonService.resultSetToObject(res);
                                    if (!(fhrDet instanceof Array))
                                        fhrDet = [fhrDet];
                                    if (!!fhrDet) {
                                        fhrDet[0].POLNO = fhrDet[0].POLNO || polNo || "0";
                                        if (fhrDet.length > 1) {
                                            // Signature...
                                            fhrDet[1].POLNO = fhrDet[1].POLNO || polNo || "0";
                                        }
                                        var fhrDocUploadRequest = { "REQ": { "AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID } };
                                        if (!!isCombo) {
                                            fhrDocUploadRequest.REQ.ComDocUpload = [];
                                            fhrDocUploadRequest.REQ.ComDocUpload = fhrDet;
                                        } else {
                                            fhrDocUploadRequest.REQ.DOCUPLOAD = [];
                                            fhrDocUploadRequest.REQ.DOCUPLOAD = fhrDet;
                                        }
                                        fhrDocUploadRequest.REQ.ACN = acn;
                                        debug("URL: " + URL);
                                        debug("fhrDocUploadRequest: " + JSON.stringify(fhrDocUploadRequest));
                                        CommonService.ajaxCall(URL, AJAX_TYPE, TYPE_JSON, fhrDocUploadRequest, AJAX_ASYNC, AJAX_TIMEOUT,
                                            function(ajaxResp) {
                                                debug("sisDocUploadRes: " + JSON.stringify(ajaxResp));
                                                if (!!ajaxResp && !!ajaxResp.RS) {
                                                    if ((!!isCombo) && (!!ajaxResp.RS.ComDocUpload && !!ajaxResp.RS.ComDocUpload[0] && !!ajaxResp.RS.ComDocUpload[0].RESP && ajaxResp.RS.ComDocUpload[0].RESP == "S")) {
                                                        dfd.resolve("S");
                                                    } else if ((!isCombo) && (!!ajaxResp.RS.DOCUPLOAD && !!ajaxResp.RS.DOCUPLOAD[0] && !!ajaxResp.RS.DOCUPLOAD[0].RESP && ajaxResp.RS.DOCUPLOAD[0].RESP == "S")) {
                                                        dfd.resolve("S");
                                                    } else {
                                                        dfd.resolve(null);
                                                    }
                                                } else {
                                                    dfd.resolve(null);
                                                }
                                            },
                                            function(data, status, headers, config, statusText) {
                                                dfd.resolve(null);
                                            }
                                        );
                                    } else
                                        dfd.resolve(fhrDet || null);
                                }
                            } else {
                                dfd.resolve("S");
                            }
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to update FHR Output Document upload flag
     *
     * @param {string} fhrId
     * @param {string} agentCd
     * @returns true/false or null
     */
    this.updateFHROpDocShareFlag = function(fhrId, agentCd) {
        debug("Inside updateFHROpDocShareFlag");
        var dfd = $q.defer();
        var query = "update lp_document_upload set IS_DATA_SYNCED = ? WHERE DOC_CAT_ID = ? AND DOC_CAT IN ('FHR','DEV') AND AGENT_CD = ? AND DOC_NAME NOT LIKE 'TRM%'"; //1714
        var parameters = ["Y", fhrId, agentCd];
        debug(query);
        debug(parameters);
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameters,
                    function(tx, res) {
                        debug("Updated FHR Share IS_DATA_SYNCED Flag");
                        debug(res);
                        dfd.resolve(res);
                    },
                    function(tx, err) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    }


    /**
     * Function to Set FHR Data used for Sync process
     * 
     * @param {object} data
     */
    this.setFHRDataForSync = function(data) {
        fhrmain.fhrDataSync = data;
        debug("FHR Data set before data sync is :" + JSON.stringify(fhrmain.fhrDataSync));
    };

    /**
     * Function to retrieve FHR Data used for Sync process
     * 
     * @returns fhrDataSync Object
     */
    this.getFHRDataForSync = function() {
        debug("Returning FHR Data :" + JSON.stringify(fhrmain.fhrDataSync));
        return fhrmain.fhrDataSync;
    };

    /**
     * Function to retrieve FHR and update FHR Signed timestamps before sync
     * 
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRData = function(agentCd, fhrId) {
        debug("in getFHRData");
        var dfd = $q.defer();

        var query = "SELECT  LPO.OPPORTUNITY_ID OPPID, LFM.FHR_ID as FHID, LFM.LEAD_ID LEAD, LFM.IS_FHR_SYNCED ISSYNCED,LFM.SIGNED_TIMESTAMP SGTS, LFM.COMPLETION_TIMESTAMP CPTS, LFM.IS_SCREEN1_DONE, LFM.IS_SCREEN2_DONE, LFM.IS_SCREEN3_DONE,LFM.IS_SCREEN4_DONE,LPO.RECO_PRODUCT_DEVIATION_FLAG, LPO.FHR_SIGNED_TIMESTAMP, LMO.RECO_PRODUCT_DEVIATION_REASON, LMO.DEVIATION_SIGNED_TIMESTAMP, LMO.ISSYNCED ISDEVSYNCED from LP_FHR_MAIN LFM " +
            " INNER JOIN LP_MYOPPORTUNITY LPO on LPO.FHR_ID=LFM.FHR_ID " +
            " LEFT JOIN LP_MYOPPORTUNITY_DEVIATION LMO on LMO.FHR_ID=LFM.FHR_ID " +
            " where LFM.AGENT_CD=?";

        var params = [agentCd];

        if (!!fhrId) {
            query += " AND LFM.FHR_ID=?";
            params.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                // Fetching FHR Data after updation of FHR_MAIN
                CommonService.executeSql(tx, query, params,
                    function(tx, selectRes) {
                        if (!!selectRes && selectRes.rows.length > 0) {
                            var fhrDet = CommonService.resultSetToObject(selectRes);
                            fhrmain.setFHRDataForSync(fhrDet); // Set FHR Data for Sync
                            if (selectRes.rows.item(0).ISSYNCED == 'Y') {
                                dfd.resolve("S");
                            } else {
                                if (!!fhrId) {
                                    dfd.resolve(fhrDet || null);
                                }
                            }
                        } else {
                            debug("FHR data null");
                            dfd.resolve(null);
                        }
                    },
                    function(tx, err) {
                        dfd.resolve(retData);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to retrieve FHR and Personal Data
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRAndPersonalData = function(agentCd, fhrId) {
        debug("in getFHRAndPersonalData");

        var persDetailData = [];

        var dfd = $q.defer();
        var query = "SELECT  CASE WHEN LPF.TITLE==1 THEN 'Mr.' ELSE 'Ms.' END AS TITL, LPF.FIRST_NAME AS FNM, LPF.MIDDLE_NAME AS MNM, LPF.LAST_NAME AS LNM, LPF.MOBILE_NO AS MOB, LPF.EMAIL_ID AS EML, LPF.BIRTH_DATE AS DOB, LPF.CURRENT_AGE AS CAGE, LPF.RETIREMENT_AGE AS RAGE, LPF.OCCUPATION AS OCCU, LPF.NO_OF_DEPENDENT AS NODP, LPF.ADDRESS1 AS ADD1, LPF.ADDRESS2 AS ADD2, LPF.ADDRESS3 AS ADD3, LPF.DISTRICT_LANDMARK AS DIST, LPF.CITY AS CTY, LPF.CITY_CODE AS CTYCD, LPF.STATE AS STE, LPF.STATE_CODE AS STECD, LPF.PINCODE AS PIN, LPF.ANN_INCOME_SALARY AS ISAL, LPF.ANN_INCOME_SPOUSE AS ISPO, LPF.ANN_INCOME_OTHER AS IOTH, LPF.ANN_INCOME_AGRICULTURE AS IAGR, LPF.ANN_INCOME_TOTAL AS ITOT, LPF.ANN_EXP_HOUSEHOLD AS EHH, LPF.ANN_EXP_AUTO_LOAN_EMI AS EAUL, LPF.ANN_EXP_HOME_LOAN_EMI AS EHML, LPF.ANN_EXP_PERSONAL_LOAN_EMI AS EPLE, LPF.ANN_EXP_PERSONAL AS EPER, LPF.ANN_EXP_INSURANCE_PREMIUM AS EINS, LPF.ANN_EXP_TOTAL AS ETOT, LPF.EMERGENCY_FUNDS AS EMFD, LPF.LIABILITY_HOME_LOAN AS LHL, LPF.LIABILITY_CAR_LOAN AS LCL, LPF.LIABILITY_PERSONAL_LOAN AS LPL, LPF.LIABILITY_CREDITCARD_BILL AS LCC, LPF.LIABILITY_TOTAL AS LTOT from LP_FHR_PERSONAL_INF_SCRN_A LPF " +
            "where LPF.AGENT_CD=?";

        var parameterList = [agentCd];
        if (!!fhrId) {
            query += " AND LPF.FHR_ID=?";
            parameterList.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            if (res.rows.item(0).IS_FHR_SYNCED == 'Y') {
                                dfd.resolve("S");
                            } else {
                                if (!!fhrId) {
                                    res.rows.item(0).DOB = res.rows.item(0).DOB + " 00:00:00";
                                    var fhrDet = CommonService.resultSetToObject(res);
                                    dfd.resolve(fhrDet || null);
                                }
                            }
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to get FHR Child Goals Data
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRAndChildGoalsData = function(agentCd, fhrId) {
        debug("in getFHRAndChildGoalsData");

        var childGoalData = [];

        var dfd = $q.defer();
        var query = " SELECT * FROM ( " +
            " SELECT FHR_ID AS FHRID, RELATION_SEQ_NO AS RSEQ, RELATION AS REL, RELATION_CODE AS RELCD, GOAL_DESC AS GOAL, GOAL_TITLE AS GTITL, GOAL_FIRST_NAME AS GFNM, GOAL_MIDDLE_NAME AS GMNM, GOAL_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE, GOAL_TARGET_AGE AS TAGE, PRESENT_COST AS PRCST, FUTURE_COST AS FRCST, FUTURE_SAVINGS_VALUE AS FRSAV, GOAL_GAP_AMOUNT AS GPAMT, COMMIT_SAVINGS_AMOUNT AS CMSAV, PREMIUM_TYPE AS PMTYP, RETURN_SCENARIO_PERCENT AS RPRT, PLAN_TYPE AS PLTYP, PREMIUM_PAY_MODE AS PYMDCD, PREMIUM_PAY_MODE_DESC AS PYMD, ACHIVING_DUARTION AS ACDU, SUGEST_PLAN1_URNNO AS SGP1U, SUGEST_PLAN1_SUM_ASSURED AS SGP1SA, SUGEST_PLAN1_PREMIUM_AMT AS SGP1PA, SUGEST_PLAN1_PAY_TERM AS SGP1PT, SUGEST_PLAN2_URNNO AS SGP2U, SUGEST_PLAN2_SUM_ASSURED AS SGP2SA, SUGEST_PLAN2_PREMIUM_AMT AS SGP2PA, SUGEST_PLAN2_PAY_TERM AS SGP2PT, SUGEST_PLAN3_URNNO AS SGP3U, SUGEST_PLAN3_SUM_ASSURED AS SGP3SA, SUGEST_PLAN3_PREMIUM_AMT AS SGP3PA, SUGEST_PLAN3_PAY_TERM AS SGP3PT, CLIENT_BIRTH_DATE CLBD, INFLATION_RATE INFR, PRESENT_SAVINGS PRSV, ROI_RATE ROIR   " +
            " FROM LP_FHR_GOAL_CHILD_EDU_SCRN_B " +
            " WHERE AGENT_CD = ? " +
            " UNION " +
            " SELECT FHR_ID AS FHRID, RELATION_SEQ_NO AS RSEQ, RELATION AS REL, RELATION_CODE AS RELCD, GOAL_DESC AS GOAL, GOAL_TITLE AS GTITL, GOAL_FIRST_NAME AS GFNM, GOAL_MIDDLE_NAME AS GMNM, GOAL_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE, GOAL_TARGET_AGE AS TAGE, PRESENT_COST AS PRCST, FUTURE_COST AS FRCST, FUTURE_SAVINGS_VALUE AS FRSAV, GOAL_GAP_AMOUNT AS GPAMT, COMMIT_SAVINGS_AMOUNT AS CMSAV, PREMIUM_TYPE AS PMTYP, RETURN_SCENARIO_PERCENT AS RPRT, PLAN_TYPE AS PLTYP, PREMIUM_PAY_MODE AS PYMDCD, PREMIUM_PAY_MODE_DESC AS PYMD, ACHIVING_DUARTION AS ACDU, SUGEST_PLAN1_URNNO AS SGP1U, SUGEST_PLAN1_SUM_ASSURED AS SGP1SA, SUGEST_PLAN1_PREMIUM_AMT AS SGP1PA, SUGEST_PLAN1_PAY_TERM AS SGP1PT, SUGEST_PLAN2_URNNO AS SGP2U, SUGEST_PLAN2_SUM_ASSURED AS SGP2SA, SUGEST_PLAN2_PREMIUM_AMT AS SGP2PA, SUGEST_PLAN2_PAY_TERM AS SGP2PT, SUGEST_PLAN3_URNNO AS SGP3U, SUGEST_PLAN3_SUM_ASSURED AS SGP3SA, SUGEST_PLAN3_PREMIUM_AMT AS SGP3PA, SUGEST_PLAN3_PAY_TERM AS SGP3PT, CLIENT_BIRTH_DATE CLBD, INFLATION_RATE INFR, PRESENT_SAVINGS PRSV, ROI_RATE ROIR   " +
            " FROM LP_FHR_GOAL_CHILD_MRG_SCRN_C " +
            " WHERE AGENT_CD = ? " +
            " UNION " +
            " SELECT FHR_ID AS FHRID, RELATION_SEQ_NO AS RSEQ, RELATION AS REL, RELATION_CODE AS RELCD, GOAL_DESC AS GOAL, null AS GTITL, CLIENT_FIRST_NAME AS GFNM, CLIENT_MIDDLE_NAME AS GMNM, CLIENT_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE, GOAL_TARGET_AGE AS TAGE, PRESENT_COST AS PRCST, FUTURE_COST AS FRCST, FUTURE_SAVINGS_VALUE AS FRSAV, GOAL_GAP_AMOUNT AS GPAMT, null AS CMSAV, null AS PMTYP, null AS RPRT, null AS PLTYP, null AS PYMDCD, null AS PYMD, null AS ACDU, SUGEST_PLAN1_URNNO AS SGP1U, null AS SGP1SA,  null AS SGP1PA, null AS SGP1PT, SUGEST_PLAN2_URNNO AS SGP2U, null AS SGP2SA, null AS SGP2PA, null AS SGP2PT, SUGEST_PLAN3_URNNO AS SGP3U, null AS SGP3SA, null AS SGP3PA, null AS SGP3PT, CLIENT_BIRTH_DATE CLBD, INFLATION_RATE INFR, PRESENT_SAVINGS PRSV, ROI_RATE ROIR " +
            " FROM LP_FHR_GOAL_CHILD_WEALTH_SCRN_G " +
            " WHERE AGENT_CD = ?" +
            " ) a";

        var parameterList = [agentCd, agentCd, agentCd];
        if (!!fhrId) {
            query += " WHERE a.FHRID=?";
            parameterList.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                res.rows.item(i).CLBD = res.rows.item(i).CLBD + " 00:00:00";
                                childGoalData.push(res.rows.item(i));
                            }
                            dfd.resolve(childGoalData);
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to get FHR Protection Goals Data
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRAndProtectionGoalsData = function(agentCd, fhrId) {
        debug("in getFHRAndProtectionGoalsData");

        var protectionGoalData = [];

        var dfd = $q.defer();
        var query = " SELECT * FROM ( " +
            " SELECT FHR_ID AS FHRID, GOAL_DESC AS GOAL,GOAL_TITLE AS GTITL,GOAL_FIRST_NAME AS GFNM, GOAL_MIDDLE_NAME AS GMNM, GOAL_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE,GOAL_TARGET_AGE AS TAGE,REQ_EMERGENCY_FUND as EMFND,REQ_INSURANCE_PROTECTION as PRCST,EXIT_INSURANCE_PROTECTION as FRCST,REQ_INSURANCE_SOL as FRCOR,PROVISION_GAP_EMERGENCY_FUND as GPAMT,PREMIUM_TYPE AS PRTYP, PLAN_TYPE AS PLTYP, PREMIUM_PAY_MODE AS PYMDCD, PREMIUM_PAY_MODE_DESC AS PYMD,SUGGESTED_PLAN_NAME as SGPLN,SUGGESTED_SUM_ASSURED as SGSUM,EXPECTED_PREMIUM_AMOUNT_EX_ST as SGPRST,EXPECTED_PREMIUM_AMOUNT as SGPRAM,EXPECTED_PREMIUM_PAY_TERM as SGPYTRM,GENDER as GND,GENDER_CODE as GNDCD,IS_SMOKER as SMOK, INFLATION_RATE INFR, ROI_RATE ROIR, SUGEST_PLAN1_URNNO AS SGP1U, SUGEST_PLAN2_URNNO AS SGP2U, SUGEST_PLAN3_URNNO AS SGP3U, 'Self' AS REL, 'SL' AS RELCD " +
            " FROM LP_FHR_GOAL_LIVING_STD_SCRN_F" +
            " WHERE AGENT_CD = ? " +
            " ) a ";

        var parameterList = [agentCd];
        if (!!fhrId) {
            query += " WHERE a.FHRID=?";
            parameterList.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                protectionGoalData.push(res.rows.item(i));
                            }
                            dfd.resolve(protectionGoalData);
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to get FHR Retirement Goals Data
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRAndRetirementGoalsData = function(agentCd, fhrId) {
        debug("in getFHRAndRetirementGoalsData");

        var retirementGoalData = [];

        var dfd = $q.defer();
        var query = " SELECT * FROM ( " +
            " SELECT FHR_ID AS FHRID, GOAL_DESC AS GOAL, GOAL_TITLE AS GTITL, GOAL_FIRST_NAME AS GFNM, GOAL_MIDDLE_NAME AS GMNM, GOAL_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE, GOAL_TARGET_AGE AS TAGE, GOAL_TARGET_DUARTION AS TDUR, PRESENT_COST AS PRCST, FUTURE_COST AS FRCST, FUTURE_REQIRED_CORPUS AS FRCOR, FUTURE_SAVINGS_VALUE AS FRSAV, GOAL_GAP_AMOUNT AS GPAMT, COMMIT_SAVINGS_AMOUNT AS CMSAV, SALARY_BASIC AS SLBAS, SALARY_DA AS SLDA, SALARY_GROWTH_PRCENT AS SLGRP, PF_CORPUS AS PFCOR, TOTAL_SERVICE_IN_YEARS AS SRYER, EXPECTED_PF_CORPUS AS EPFCR, EXPECTED_GRATUITY_AMOUNT AS EGRAM, TOTAL_EMPLOYER_BENEFIT AS TOTBEN, PREMIUM_TYPE AS PMTYP, RETURN_SCENARIO_PERCENT AS RPRT, RETURN_REINVEST_PERCENT AS RRPRT, PLAN_TYPE AS PLTYP, PREMIUM_PAY_MODE AS PYMDCD, PREMIUM_PAY_MODE_DESC AS PYMD, ACHIVING_DUARTION AS ACDU, SUGEST_PLAN1_URNNO AS SGP1U, SUGEST_PLAN1_SUM_ASSURED AS SGP1SA, SUGEST_PLAN1_PREMIUM_AMT AS SGP1PA, SUGEST_PLAN1_PAY_TERM AS SGP1PT, SUGEST_PLAN2_URNNO AS SGP2U, SUGEST_PLAN2_SUM_ASSURED AS SGP2SA, SUGEST_PLAN2_PREMIUM_AMT AS SGP2PA, SUGEST_PLAN2_PAY_TERM AS SGP2PT, SUGEST_PLAN3_URNNO AS SGP3U, SUGEST_PLAN3_SUM_ASSURED AS SGP3SA, SUGEST_PLAN3_PREMIUM_AMT AS SGP3PA, SUGEST_PLAN3_PAY_TERM AS SGP3PT, INFLATION_RATE INFR, PRESENT_SAVINGS PRSV, ROI_RATE ROIR, 'Self' AS REL, 'SL' AS RELCD   " +
            " FROM LP_FHR_GOAL_RETIREMENT_SCRN_E " +
            " WHERE AGENT_CD = ? ) a";

        var parameterList = [agentCd];
        if (!!fhrId) {
            query += " WHERE a.FHRID=?";
            parameterList.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                retirementGoalData.push(res.rows.item(i));
                            }
                            dfd.resolve(retirementGoalData);
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to get FHR Wealth Goals Data
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @returns
     */
    this.getFHRAndWealthGoalsData = function(agentCd, fhrId) {
        debug("in getFHRAndWealthGoalsData");

        var wealthGoalData = [];

        var dfd = $q.defer();
        var query = " SELECT * FROM ( " +
            " SELECT FHR_ID AS FHRID, GOAL_DESC AS GOAL, GOAL_TITLE AS GTITL, GOAL_FIRST_NAME AS GFNM, GOAL_MIDDLE_NAME AS GMNM, GOAL_LAST_NAME AS GLNM, GOAL_CURRENT_AGE AS CAGE, GOAL_TARGET_AGE AS TAGE, PRESENT_COST AS PRCST, FUTURE_COST AS FRCST, FUTURE_SAVINGS_VALUE AS FRSAV, GOAL_GAP_AMOUNT AS GPAMT, COMMIT_SAVINGS_AMOUNT AS CMSAV, PREMIUM_TYPE AS PMTYP, RETURN_SCENARIO_PERCENT AS RPRT, PLAN_TYPE AS PLTYP, PREMIUM_PAY_MODE AS PYMDCD, PREMIUM_PAY_MODE_DESC AS PYMD, ACHIVING_DUARTION AS ACDU, SUGEST_PLAN1_URNNO AS SGP1U, SUGEST_PLAN1_SUM_ASSURED AS SGP1SA, SUGEST_PLAN1_PREMIUM_AMT AS SGP1PA, SUGEST_PLAN1_PAY_TERM AS SGP1PT, SUGEST_PLAN2_URNNO AS SGP2U, SUGEST_PLAN2_SUM_ASSURED AS SGP2SA, SUGEST_PLAN2_PREMIUM_AMT AS SGP2PA, SUGEST_PLAN2_PAY_TERM AS SGP2PT, SUGEST_PLAN3_URNNO AS SGP3U, SUGEST_PLAN3_SUM_ASSURED AS SGP3SA, SUGEST_PLAN3_PREMIUM_AMT AS SGP3PA, SUGEST_PLAN3_PAY_TERM AS SGP3PT, INFLATION_RATE INFR, PRESENT_SAVINGS PRSV, ROI_RATE ROIR, RELATION AS REL, RELATION_CODE AS RELCD, RELATION_SEQ_NO AS RSEQ " +
            " FROM LP_FHR_GOAL_WEALTH_SCRN_D" +
            " WHERE AGENT_CD = ? " +
            " ) a ";

        var parameterList = [agentCd];
        if (!!fhrId) {
            query += " WHERE a.FHRID=?";
            parameterList.push(fhrId);
        }
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                wealthGoalData.push(res.rows.item(i));
                            }
                            dfd.resolve(wealthGoalData);
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to Sync Deviation Document with Server   -- DOCUMENT will be shared on final submission
     * Only if Product Deviation is selected
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} isCombo
     * @param {string} finalSubmission
     * @returns
     */
    this.syncFHRDeviationDoc = function(agentCd, fhrId, isCombo, finalSubmission) {
        debug("Inside syncFHRDeviationDoc - FHR Deviation only");
        var dfd = $q.defer();

        var query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND IS_DATA_SYNCED=?";
        var parameterList = ["DEV", agentCd, "FHR_%.html", 'Y'];
        if (!!finalSubmission) {
            query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?";
            parameterList = ["DEV", agentCd, "FHR_%.html"];
        }

        if (!!isCombo) {
            query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND IS_DATA_SYNCED=?";
            if (!!finalSubmission) {
                query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?";
                parameterList = ["DEV", agentCd, "FHR_%.html"];
            }
        }

        if (!!fhrId) {
            query += " AND DOC_CAT_ID=?";
            parameterList.push(fhrId);
        }

        function sendFile(file, options) {
            var innerDfd = $q.defer();
            var ft = new FileTransfer();
            debug("file: " + file);
            ft.upload(file, HTML_SYNC_URL, function(succ) {
                    debug("Code = " + succ.responseCode);
                    debug("Response = " + succ.response);
                    debug("Sent = " + succ.bytesSent);
                    if (!!succ && !!succ.response && !!JSON.parse(succ.response).RS && JSON.parse(succ.response).RS.RESP === "S") {
                        innerDfd.resolve("S");
                    } else {
                        innerDfd.resolve(null);
                    }
                },
                function(err) {
                    debug("FT upload error: " + JSON.stringify(err));
                    innerDfd.resolve(null);
                }, options);
            return innerDfd.promise;
        }

        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            if (res.rows.item(0).IS_FILE_SYNCED == 'Y' && !finalSubmission) {
                                dfd.resolve("S");
                            } else {
                                delete res.rows.item(0).IS_FILE_SYNCED;
                                var options = new FileUploadOptions();
                                options.fileKey = "file";
                                options.fileName = res.rows.item(0).DOC_NAME;
                                options.mimeType = "application/octet-stream";
                                options.headers = { 'contentType': 'multipart/form-data' };
                                options.httpMethod = "POST";
                                var params = {};
                                params.AC = agentCd + "";
                                params.DVID = localStorage.DVID;
                                params.PWD = LoginService.lgnSrvObj.password;
                                params.ACN = "SCFD";
                                if (!!isCombo) {
                                    params.COMBO = "C";
                                } else {
                                    params.COMBO = "A";
                                }
                                params.CATID = res.rows.item(0).FRM;
                                params.CAT = "DEV";
                                params.DOCID = res.rows.item(0).DOC;
                                params.ISASUB = null;
                                params.FILETYPE = "HTML";

                                debug("paramList: " + JSON.stringify(params));
                                debug("file details: " + JSON.stringify(params));
                                options.params = params;

                                CommonService.getTempFileURI("/FromClient/FHR/HTML/", res.rows.item(0).DOC_NAME).then(
                                    function(file) {
                                        sendFile(file, options).then(
                                            function(resp) {
                                                dfd.resolve(resp);
                                            }
                                        );
                                    }
                                );
                            }
                        } else
                            dfd.resolve("S");
                    },
                    function(tx, err) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );

        return dfd.promise;
    };

    /**
     * Function to Sync FHR Report with Server
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} isCombo
     * @param {string} finalSubmission
     * @returns
     */
    this.syncFHRReport = function(agentCd, fhrId, isCombo, finalSubmission) {
        debug("Inside syncFHRReport - FHR Report file only");
        var dfd = $q.defer();

        var query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?  AND DOC_NAME NOT LIKE ? AND IS_DATA_SYNCED=?";
        var parameterList = ["FHR", agentCd, "%.html", "TRM%", 'Y'];
        if (!!finalSubmission) {
            query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?  AND DOC_NAME NOT LIKE ?";
            parameterList = ["FHR", agentCd, "%.html", "TRM%"];
        }


        if (!!isCombo) {
            query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND DOC_NAME NOT LIKE ? AND IS_DATA_SYNCED=?";
            if (!!finalSubmission) {
                query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND DOC_NAME NOT LIKE ?";
                parameterList = ["FHR", agentCd, "%.html", "TRM%"];
            }
        }

        if (!!fhrId) {
            query += " AND DOC_CAT_ID=?";
            parameterList.push(fhrId);
        }

        function sendFile(file, options) {
            var innerDfd = $q.defer();
            var ft = new FileTransfer();
            debug("file: " + file);
            ft.upload(file, HTML_SYNC_URL, function(succ) {
                    debug("Code = " + succ.responseCode);
                    debug("Response = " + succ.response);
                    debug("Sent = " + succ.bytesSent);
                    if (!!succ && !!succ.response && JSON.parse(succ.response).RS.RESP === "S") {
                        innerDfd.resolve("S");
                    } else {
                        innerDfd.resolve(null);
                    }
                },
                function(err) {
                    debug("FT upload error: " + err);
                    innerDfd.resolve(null);
                }, options);
            return innerDfd.promise;
        }

        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        debug("share report ");
                        debug(res);
                        if (!!res && res.rows.length > 0) {
                            if (res.rows.item(0).IS_FILE_SYNCED == 'Y' && !finalSubmission) {
                                dfd.resolve("S");
                            } else {
                                delete res.rows.item(0).IS_FILE_SYNCED;
                                var options = new FileUploadOptions();
                                options.fileKey = "file";
                                options.fileName = res.rows.item(0).DOC_NAME;
                                options.mimeType = "application/octet-stream";
                                options.headers = { 'contentType': 'multipart/form-data' };
                                options.httpMethod = "POST";
                                var params = {};
                                params.AC = agentCd + "";
                                params.DVID = localStorage.DVID;
                                params.PWD = LoginService.lgnSrvObj.password;
                                params.ACN = "SCFD";
                                if (!!isCombo) {
                                    params.COMBO = "C";
                                } else {
                                    params.COMBO = "A";
                                }
                                params.CATID = res.rows.item(0).FRM;
                                params.CAT = "FHR";
                                params.DOCID = res.rows.item(0).DOC;
                                params.ISASUB = null;
                                params.FILETYPE = "HTML";
                                debug("paramList: " + JSON.stringify(params));
                                debug("file details: " + JSON.stringify(params));
                                options.params = params;

                                CommonService.getTempFileURI("/FromClient/FHR/HTML/", res.rows.item(0).DOC_NAME).then(
                                    function(file) {
                                        sendFile(file, options).then(
                                            function(resp) {
                                                dfd.resolve(resp);
                                            }
                                        );
                                    }
                                );
                            }
                        } else
                            dfd.resolve(null);
                    },
                    function(tx, err) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    /**
     * Function to Update FHR Share flag
     *
     * @param {string} fhrId
     * @param {string} agentCd
     * @returns
     */
    this.updateFHRShareFlag = function(fhrId, agentCd) {
        debug("Inside updateFHRShareFlag " + fhrId);
        var dfd = $q.defer();
        var query = "update lp_document_upload set IS_FILE_SYNCED = ? WHERE DOC_CAT_ID = ? AND DOC_CAT =? AND AGENT_CD = ? AND DOC_NAME NOT LIKE 'TRM%'"; //1714
        var parameters = ["Y", fhrId, "FHR", agentCd];
        debug(query);
        debug(parameters);
        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameters,
                    function(tx, res) {
                        debug("updated FHR Share document upload IS_FILE_SYNCED Flag");
                        debug(res);
                        dfd.resolve(res);

                    },
                    function(tx, err) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    }

    /**
     * Function to Sync SIS Email with Server
     *
     * @param {string} agentCd
     * @param {string} fhrId
     * @param {string} emailIdKey
     * @param {bool} isCombo
     * @returns
     */
    this.syncFHREmail = function(agentCd, fhrId, emailIdKey, isCombo) {
        debug("inside sync FHR email");
        var dfd = $q.defer();

        var query = "SELECT EMAIL_ID AS EML, EMAIL_CAT AS TYP, EMAIL_TO, EMAIL_CC AS CC, EMAIL_CAT_ID AS ID, EMAIL_SUBJECT AS SUB, EMAIL_BODY AS BDY, EMAIL_TIMESTAMP AS TMP FROM LP_EMAIL_USER_DTLS WHERE AGENT_CD=? AND EMAIL_CAT=? AND EMAIL_ID=?";
        var parameterList = [agentCd, "FHR", emailIdKey];

        if (!!isCombo) {
            query += " AND EMAIL_CAT_ID=?";
            parameterList.push(fhrId);
        }

        CommonService.transaction(db,
            function(tx) {
                CommonService.executeSql(tx, query, parameterList,
                    function(tx, res) {
                        if (!!res && res.rows.length > 0) {
                            if (!!fhrId) {
                                var seDet = CommonService.resultSetToObject(res);
                                seDet.TO = seDet.EMAIL_TO;
                                delete seDet.EMAIL_TO;

                                var sisEmailRequest = { "REQ": { "ACN": "SE", "AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID, "SEDET": [] } };

                                sisEmailRequest.REQ.SEDET.push(seDet);

                                CommonService.ajaxCall(EMAIL_SYNC_URL, AJAX_TYPE, TYPE_JSON, sisEmailRequest, AJAX_ASYNC, AJAX_TIMEOUT,
                                    function(ajaxResp) {
                                        debug("email sync resp: " + JSON.stringify(ajaxResp));
                                        if (!!ajaxResp && !!ajaxResp.RS) {
                                            if (!!ajaxResp.RS.SEDET && !!ajaxResp.RS.SEDET[0] && !!ajaxResp.RS.SEDET[0].RESP && ajaxResp.RS.SEDET[0].RESP == "S") {
                                                dfd.resolve("S");
                                            } else {
                                                dfd.resolve(null);
                                            }
                                        } else {
                                            dfd.resolve(null);
                                        }
                                    },
                                    function(data, status, headers, config, statusText) {
                                        dfd.resolve(null);
                                    }
                                );
                            }
                        } else {
                            dfd.resolve(null);
                        }
                    },
                    function(tx, res) {
                        dfd.resolve(null);
                    }
                );
            },
            function(err) {
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

}]);
