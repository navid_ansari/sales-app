syncAppModule.service('AppSyncService', ['$q','$state','CommonService', 'LoginService','LoadApplicationScreenData','SavePageOutputData','SisFormService','CcrQuesService','FhrService', function($q ,$state, CommonService, LoginService, LoadApplicationScreenData, SavePageOutputData, SisFormService,CcrQuesService,FhrService){
    var syncObj = this;
    syncObj.APP_ID;
    syncObj.SIS_ID;
    syncObj.FHR_ID;
    syncObj.LEAD_ID;
    syncObj.OPP_ID;
    syncObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    syncObj.CUST_EMAIL_ID;
    syncObj.EMPLOYEE_FLAG;
    syncObj.netStatus = CommonService.checkConnection();
    syncObj.MyOpportunityData;

this.startAppDataSync = function(FhrId,leadId, OppId, sisId, AppId, custEmailId, polNo, contDocUpload, popup){

    var dfd = $q.defer();
    syncObj.FHR_ID = FhrId;
    syncObj.OPP_ID = OppId;
    syncObj.SIS_ID = sisId;
    syncObj.APP_ID = AppId;
    syncObj.CUST_EMAIL_ID = custEmailId;
    syncObj.EMPLOYEE_FLAG = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;

    if(!!popup)
    	syncObj.popup = popup
    else
    	syncObj.popup = null;
	console.log(popup+"syncObj.popup :"+syncObj.popup);
    if(!!leadId)
    	syncObj.LEAD_ID = leadId;
    else
    	syncObj.LEAD_ID = null;

    if(!!polNo)
    	syncObj.POLICY_NO = polNo
    else
    	syncObj.POLICY_NO = null;

    if(!!contDocUpload)
    	syncObj.contDocUpload = contDocUpload;
    else
    	syncObj.contDocUpload = false;

    LoadApplicationScreenData.loadOppFlags(syncObj.FHR_ID, syncObj.AGENT_CD, 'fhr').then(function(OppData){
        debug(JSON.stringify("OppData within syncObj:"+JSON.stringify(OppData)));
      //  syncObj.MyOpportunityData = OppData;
        syncObj.setMyOpportunityData(OppData);
        if(CommonService.checkConnection() == "online"){
			if(OppData.oppFlags.IS_CUST_DET == "Y"){
				CommonService.showLoading("Data Sync in Progress...");
				if(BUSINESS_TYPE=="IndusSolution"){
					syncObj.syncFHRData().then(function(resp){
						debug("Callback resp:0"+resp);
						dfd.resolve(!!resp);
					});
				}
				else{
					// change here in case of Life Planner, iWealth iOS
					if(OppData.oppFlags.RECO_PRODUCT_DEVIATION_FLAG == 'Y' || OppData.oppFlags.RECO_PRODUCT_DEVIATION_FLAG == 'N') // FHR Not skipped
							FhrService.syncFHRData(syncObj.AGENT_CD, syncObj.FHR_ID).then(function(fhrResp){
								if (!!fhrResp && fhrResp == "S")
								{
									CommonService.updateRecords(db, 'LP_FHR_MAIN', { 'IS_FHR_SYNCED': 'Y' }, { 'FHR_ID': syncObj.FHR_ID }).then(
										function(res) {
											//Start SIS Sync
											syncObj.syncSISData().then(function(resp){
												debug("Callback resp:2"+resp);
												dfd.resolve(!!resp);
											});
										}
									);
								}
								else {
									CommonService.hideLoading();
									navigator.notification.alert(REQ_FAIL_MSG,null,"FHR Sync","OK");
								}
							});
					else {
							//Start SIS Sync
							syncObj.syncSISData().then(function(resp){
								debug("Skipped FHR resp:2"+resp);
								dfd.resolve(!!resp);
							});
					}
				}
			}
			else{

                CommonService.hideLoading();
                navigator.notification.alert("Please complete Customer Details and Signature",null,"Application Sync","OK");
            	dfd.resolve(false);
            }
        }
         else
         {
            CommonService.hideLoading();
            navigator.notification.alert(OFFLINE_SYNC_MSG,null,"Application Sync","OK");
            console.log(CommonService.checkConnection()+" :: "+syncObj.netStatus);
        	dfd.resolve(false);
         }

        });
return dfd.promise;

}
this.setMyOpportunityData = function(data){
   debug("setMyOpportunityData"+JSON.stringify(data));
  syncObj.MyOpportunityData = data;
}


 this.syncComboAppDTLS = function(){
var dfd = $q.defer();
        		 var comSyncReq = {};
                 var tempReq={};
                 comSyncReq.REQ = {};
                 comSyncReq.REQ.AC = syncObj.AGENT_CD;
                 comSyncReq.REQ.PWD = syncObj.PWD;
                 comSyncReq.REQ.DVID = localStorage.DVID;
                 comSyncReq.REQ.ACN = "SCOM";
                 comSyncReq.REQ.ComDtl = [];
                 comSyncReq.REQ.ComRid = [];
				var sqlOpp = "select * from LP_MYOPPORTUNITY where APPLICATION_ID=?";
                 CommonService.transaction(db, function(tx){
						CommonService.executeSql(tx, sqlOpp,[syncObj.APP_ID], function(tx,resOpp){
                        									if(resOpp.rows.length!=0 && resOpp.rows.item(0).COMBO_ID!=null)
                        									{
                        										var r = resOpp.rows.item(0);
                        										comSyncReq.REQ.COMID = r.COMBO_ID;
                        										comSyncReq.REQ.SID = r.SIS_ID;
                        										comSyncReq.REQ.RSID = r.REF_SIS_ID;
                        										comSyncReq.REQ.APPID = r.APPLICATION_ID;
                        										comSyncReq.REQ.RAPPID = r.REF_APPLICATION_ID;
                        										comSyncReq.REQ.PNO = r.POLICY_NO;
                        										comSyncReq.REQ.RPNO = r.REF_POLICY_NO;
                        										dfd.resolve(comSyncReq);
                        									}
                        									else
                        									{
                        										dfd.resolve(null);
                        									}
                                         			 //
                                         			}, function(tx, err){
                                         				console.log("Error 1");
                                         				CommonService.hideLoading();
                                                        navigator.notification.alert(REQ_FAIL_MSG,null,"Application Sync","OK");
                                         			});

                 }, function(error){
                 	console.log("Error 2");
					CommonService.hideLoading();
					navigator.notification.alert(REQ_FAIL_MSG,null,"Application Sync","OK");
                 });

       // else
		return dfd.promise;
    }
    this.sendComboAppData = function(comSyncReq){
	var dfd = $q.defer();
    console.log( "JSON ComboData request: "+JSON.stringify(comSyncReq));
	CommonService.APP_AJAX_CALL(COMBO_DATA_SYNC_URL, AJAX_TYPE, TYPE_JSON, comSyncReq, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
    		debug("SAD response " + JSON.stringify(ajaxResp));
                if(!!ajaxResp && !!ajaxResp.RS){
                    dfd.resolve(ajaxResp)
                }
                else
    				{
    					dfd.resolve(null);
            			CommonService.hideLoading();
            			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
            		}
    		},
    		function(data, status, headers, config, statusText){
    			dfd.resolve(null);
    			CommonService.hideLoading();
    			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
    		});
	return dfd.promise;
    }


 this.getMyOpportunityData= function(){
                var dfd = $q.defer();

				var sqlOpp = "select * from LP_MYOPPORTUNITY where APPLICATION_ID=?";
                 CommonService.transaction(db, function(tx){
						CommonService.executeSql(tx, sqlOpp,[syncObj.APP_ID], function(tx,resOpp){
                                if(resOpp.rows.length!=0 && resOpp.rows.item(0).COMBO_ID!=null)
                                {
                                    dfd.resolve(resOpp.rows.item(0));
                                }
                                else
                                {
                                    dfd.resolve(null);
                                }
                        }, function(tx, err){
                            dfd.resolve(null);
                        });

                 }, function(error){
                        dfd.resolve(null);
                 });

		return dfd.promise;
    }

this.syncFHRData = function(){
var dfd = $q.defer();
console.log(syncObj.PWD+"syncObj.FHR_ID :"+syncObj.FHR_ID);
syncObj.PWD = LoginService.lgnSrvObj.password;
SavePageOutputData.getFHRData(syncObj.FHR_ID).then(
    	function(FHRObj){
    	    var FHRArray = FHRObj.FFNA;
    	    var FHRMain = FHRObj.MAIN;
    	    console.log("FHRArray :"+JSON.stringify(FHRArray));
    	    if(!!FHRArray && (FHRArray.ISFFN_SYNCED == undefined || FHRArray.ISFFN_SYNCED != 'Y')){
    		var mainObj = {};
			var headobj = {};
			syncObj.LEAD_ID = FHRMain.LEAD_ID;
			headobj.AC = syncObj.AGENT_CD;
            headobj.PWD = syncObj.PWD;
            headobj.DVID = localStorage.DVID;
            headobj.ACN = "SFFD";
            headobj.FHID = syncObj.FHR_ID;
            headobj.LEAD = FHRMain.LEAD_ID;
            headobj.BTYPE = BUSINESS_TYPE;

			var obj = {};
			obj.SCG = FHRArray.SALES_CHNL_SEGMENT;
			obj.CS = FHRArray.CONSUMER_SEGMENT;
			obj.CTL = FHRArray.CONSUMER_TITLE;
			obj.CFNM = FHRArray.CONSUMER_FIRST_NAME;
			obj.CMNM = FHRArray.CONSUMER_MIDDLE_NAME;
			obj.CLNM = FHRArray.CONSUMER_LAST_NAME;
			obj.CAGE = FHRArray.CONSUMER_AGE;
			obj.CDOB = FHRArray.CONSUMER_BIRTH_DATE;
			obj.EBCF = FHRArray.EXIST_BANK_CUST_FLAG;  //Drastty
			obj.EBAOD = FHRArray.EXIST_BANK_AC_OPEN_DATE;
			obj.EBANO = FHRArray.EXIST_BANK_ACCOUNT_NO;
			obj.EBATYP = FHRArray.EXIST_BANK_ACCOUNT_TYPE;
			obj.EBNAM = FHRArray.EXIST_BANK_NAME;
			obj.EBIFSC = FHRArray.EXIST_BANK_IFSC_CODE;
			obj.EBBNAM = FHRArray.EXIST_BANK_BRANCH_NAME;
			obj.BRT = FHRArray.BANK_RELATION_TYPE;
			obj.MOB = FHRArray.MOBILE_NO;
			obj.PRFN = FHRArray.PROPOSAL_FORM_NO;
			obj.LSC = FHRArray.LIFE_STAGE_CODE;
			obj.LSST = FHRArray.LIFE_STAGE_STATUS;
			obj.LSRE = FHRArray.LIFE_STAGE_REMARKS;
			obj.OCOT = FHRArray.OCCUPATION_OTHER;
			obj.OCCO = FHRArray.OCCUPATION_CODE;
			obj.OCDE = FHRArray.OCCUPATION_DESC;
			obj.OOTC = FHRArray.OCCU_ORG_TYPE_CODE;
			obj.OOTD = FHRArray.OCCU_ORG_TYPE_DESC;
			obj.OOTO = FHRArray.OCCU_ORG_TYPE_OTHERS;
			obj.ANIN = FHRArray.ANNUAL_INCOME;
			obj.ANBU = FHRArray.ANNUAL_INCOME_BUCKET;
			obj.PRND = FHRArray.PRIMARY_NEED_DTLS;
			obj.PRPYO = FHRArray.PREF_PAYOUT_OPTION;
			obj.RAEP = FHRArray.RISK_ASMNT_EQUITY_PCT;
			obj.RABP = FHRArray.RISK_ASMNT_BALANCED_PCT;
			obj.RADP = FHRArray.RISK_ASMNT_DEBT_PCT;
			obj.CUIF = FHRArray.CMFRT_ULIP_INVEST_FLAG; //Drastty
			obj.UAEP = FHRArray.ULIP_ALOC_EQUITY_PCT;
			obj.UABP = FHRArray.ULIP_ALOC_BALANCED_PCT;
			obj.UADP = FHRArray.ULIP_ALOC_DEBT_PCT;
			obj.CCA = FHRArray.CUST_CNT_AGREE;
			obj.CCNA = FHRArray.CUST_CNT_NOT_AGREE;
			obj.CCPA = FHRArray.CUST_CNT_PARTIAL_AGREE;
			obj.VF = FHRArray.VERNACULAR_FLAG;
			obj.VFNM = FHRArray.VRNC_FIRST_NAME;
			obj.VMNM = FHRArray.VRNC_MIDDLE_NAME;
			obj.VLNM = FHRArray.VRNC_LAST_NAME;
			obj.VIDNM = FHRArray.VRNC_ID_PROOF_NAME;
			obj.VIDNO = FHRArray.VRNC_ID_PROOF_NO;
			obj.WFNM = FHRArray.WITNS_FIRST_NAME;
			obj.WMNM = FHRArray.WITNS_MIDDLE_NAME;
			obj.WLNM = FHRArray.WITNS_LAST_NAME;
			obj.WIDPT = FHRArray.WITNS_ID_PROOF_TYPE;
			obj.WIDNO = FHRArray.WITNS_ID_PROOF_NO;
			mainObj.REQ = {};
			mainObj.REQ = headobj;
			mainObj.REQ.FFDet = obj;
			var innerArray = [];
			var invest1Obj = {};
			invest1Obj.INGN=FHRArray.INVEST_GOAL_NEED_1;
			invest1Obj.INGR=FHRArray.INVEST_GOAL_RANK_1;
			invest1Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_1;

			var invest2Obj={};
			invest2Obj.INGN=FHRArray.INVEST_GOAL_NEED_2;
			invest2Obj.INGR=FHRArray.INVEST_GOAL_RANK_2;
			invest2Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_2;

			var invest3Obj={};
			invest3Obj.INGN=FHRArray.INVEST_GOAL_NEED_3;
			invest3Obj.INGR=FHRArray.INVEST_GOAL_RANK_3;
			invest3Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_3;

			var invest4Obj={};
			invest4Obj.INGN=FHRArray.INVEST_GOAL_NEED_4;
			invest4Obj.INGR=FHRArray.INVEST_GOAL_RANK_4;
			invest4Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_4;

			var invest5Obj={};
			invest5Obj.INGN=FHRArray.INVEST_GOAL_NEED_5;
			invest5Obj.INGR=FHRArray.INVEST_GOAL_RANK_5;
			invest5Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_5;

			var invest6Obj={};
			invest6Obj.INGN=FHRArray.INVEST_GOAL_NEED_6;
			invest6Obj.INGR=FHRArray.INVEST_GOAL_RANK_6;
			invest6Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_6;

			var invest7Obj={};
			invest7Obj.INGN=FHRArray.INVEST_GOAL_NEED_7;
			invest7Obj.INGR=FHRArray.INVEST_GOAL_RANK_7;
			invest7Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_7;

			var invest8Obj={};
			invest8Obj.INGN=FHRArray.INVEST_GOAL_NEED_8;
			invest8Obj.INGR=FHRArray.INVEST_GOAL_RANK_8;
			invest8Obj.EIDET=FHRArray.EXIST_INVEST_DETLS_8;

			innerArray.push(invest1Obj);
			innerArray.push(invest2Obj);
			innerArray.push(invest3Obj);
			innerArray.push(invest4Obj);
			innerArray.push(invest5Obj);
			innerArray.push(invest6Obj);
			innerArray.push(invest7Obj);
			innerArray.push(invest8Obj);
			mainObj.REQ.IGDet=innerArray;

			//var innerArrayReccProd=[];

			SavePageOutputData.getRecommendedProduct(syncObj.FHR_ID).then(
            function(RecProdArray){

            	var reccProdObj={};
				reccProdObj.RPUNO=RecProdArray.RCMND_PRODUCT_URNNO;
				reccProdObj.RPDTLS=RecProdArray.RCMND_PRODUCT_DTLS;
				reccProdObj.RINO=RecProdArray.RCMND_INVEST_OPT;
				reccProdObj.RFCF=RecProdArray.RCMND_FUND_CODE_FROM;
				reccProdObj.RFCT=RecProdArray.RCMND_FUND_CODE_TO;
				reccProdObj.RFDT=RecProdArray.RCMND_FUND_DTLS;
				mainObj.REQ.FRCPDet=reccProdObj;
				var innerArrayReccProdDetails=[];

				var reccProcObj1={};
				reccProcObj1.RFC=RecProdArray.RCMND_FUND1_CODE_1;
				reccProcObj1.RFP=RecProdArray.RCMND_FUND1_PERCENT_1;

				var reccProcObj2={};
				reccProcObj2.RFC=RecProdArray.RCMND_FUND1_CODE_2;
				reccProcObj2.RFP=RecProdArray.RCMND_FUND1_PERCENT_2;

				var reccProcObj3={};
				reccProcObj3.RFC=RecProdArray.RCMND_FUND1_CODE_3;
				reccProcObj3.RFP=RecProdArray.RCMND_FUND1_PERCENT_3;

				var reccProcObj4={};
				reccProcObj4.RFC=RecProdArray.RCMND_FUND1_CODE_4;
				reccProcObj4.RFP=RecProdArray.RCMND_FUND1_PERCENT_4;

				var reccProcObj5={};
				reccProcObj5.RFC=RecProdArray.RCMND_FUND1_CODE_5;
				reccProcObj5.RFP=RecProdArray.RCMND_FUND1_PERCENT_5;

				var reccProcObj6={};
				reccProcObj6.RFC=RecProdArray.RCMND_FUND1_CODE_6;
				reccProcObj6.RFP=RecProdArray.RCMND_FUND1_PERCENT_6;

				var reccProcObj7={};
				reccProcObj7.RFC=RecProdArray.RCMND_FUND1_CODE_7;
				reccProcObj7.RFP=RecProdArray.RCMND_FUND1_PERCENT_7;

				var reccProcObj8={};
				reccProcObj8.RFC=RecProdArray.RCMND_FUND1_CODE_8;
				reccProcObj8.RFP=RecProdArray.RCMND_FUND1_PERCENT_8;

				innerArrayReccProdDetails.push(reccProcObj1);
				innerArrayReccProdDetails.push(reccProcObj2);
				innerArrayReccProdDetails.push(reccProcObj3);
				innerArrayReccProdDetails.push(reccProcObj4);
				innerArrayReccProdDetails.push(reccProcObj5);
				innerArrayReccProdDetails.push(reccProcObj6);
				innerArrayReccProdDetails.push(reccProcObj7);
				innerArrayReccProdDetails.push(reccProcObj8);

				mainObj.REQ.FRCPDet.RPFDTLS=innerArrayReccProdDetails;
				console.log("FactFinder Output: "+JSON.stringify(mainObj));
				SavePageOutputData.sendDataToServer(mainObj).then(
					function(res){

						console.log("Server Response : "+JSON.stringify(res));
						//dfd.resolve(res);
						if(!!res && res.RS.RESP == "S")
						    {
						    	CommonService.updateRecords(db, 'LP_FHR_FFNA',{"ISFFN_SYNCED" : 'Y'}, {"FHR_ID":syncObj.FHR_ID,"AGENT_CD":syncObj.AGENT_CD}).then(
												function(res){
													console.log("FFNA SYNCED update success !!");
														  syncObj.syncSISData().then(function(resp){
														  debug("Callback resp:1"+resp);
														  if(resp)
														  	dfd.resolve(true);
														  else
														  	dfd.resolve(false);
														  });
												});


						    }
						else
						    {
						       CommonService.hideLoading();
                               navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                               dfd.resolve(false);
						    }
					}
				);

            });
            }//If END
            else
            {	debug("FFNA already synced : call next");
            	syncObj.syncSISData().then(function(resp){
				  debug("Callback resp:2"+resp);
				  if(resp)
					dfd.resolve(true);
				  else
				  	dfd.resolve(false);
				});
            }

    	});
    	return dfd.promise;
}


this.syncSISData = function(){
var dfd = $q.defer();
console.log("My syncSISData");
SisFormService.syncSISData(syncObj.AGENT_CD, syncObj.SIS_ID).then(function(res){
		if(!!res){
			debug("inside complete SIS sync :"+JSON.stringify(res));
			CommonService.updateRecords(db, 'lp_sis_main', {'issynced': 'Y'}, {'SIS_ID': syncObj.SIS_ID}).then(
				function(res){
					console.log("calling syncComboAppDTLS");
					syncObj.syncComboAppDTLS().then(function(comSyncReq){
						console.log("calling sendComboAppData");
						if(!!comSyncReq)
							syncObj.sendComboAppData(comSyncReq).then(function(json){

								if(!!json && json.RS.RESP == 'S')
								{
									syncObj.getAppSyncData().then(function(appSyncReq){
										if(appSyncReq!=undefined && appSyncReq.REQ!=undefined)
										{

  											LoadApplicationScreenData.loadLeadEKYCData(syncObj.OPP_ID, syncObj.AGENT_CD,true, syncObj.LEAD_ID).then(function(EKYCData){
  											//	console.log(JSON.stringify(appSyncReq.REQ.AppCntA[0])+"::inside EKYC method::"+JSON.stringify(EKYCData));

  													if(!!EKYCData.Insured.EKYC_FLAG)
  														{console.log("idfffffff");
  														appSyncReq.REQ.AppCntA[0].EKYC = EKYCData.Insured.EKYC_FLAG;}
  													else
  														{console.log("elseeeeeeee");
  														appSyncReq.REQ.AppCntA[0].EKYC = 'N';}


  												if(!!appSyncReq.REQ.AppCntA[1])
  													if(!!EKYCData.Proposer.EKYC_FLAG)
  														appSyncReq.REQ.AppCntA[1].EKYC = EKYCData.Proposer.EKYC_FLAG;
  													else
  														appSyncReq.REQ.AppCntA[1].EKYC = 'N';
  												debug("HER APP  :"+JSON.stringify(appSyncReq.REQ.AppCntA));
  												syncObj.syncAppData(appSyncReq).then(function(resp){
  				                                      var issynced = 'Y';
  				                                        if(syncObj.EMPLOYEE_FLAG == 'E')
  				                                          issynced = 'E';
  																								if(!!resp && resp.RS.RESP == "S")
  																								{
  																								  if(syncObj.popup == undefined && syncObj.popup!='T')
  				                                              CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":syncObj.APP_ID,"AGENT_CD":syncObj.AGENT_CD}).then(
  				                                                  function(res){
  				                                                      if(syncObj.contDocUpload==false)
  				                                                      {	CommonService.hideLoading();
  				                                                          navigator.notification.alert(APP_DATA_SYN_MSG,function(){
                                                                var abc = {"LEAD_ID": syncObj.LEAD_ID, "FHR_ID": syncObj.FHR_ID, "SIS_ID": syncObj.SIS_ID, "APP_ID": syncObj.APP_ID, "OPP_ID": syncObj.OPP_ID, 'POLICY_NO': syncObj.POLICY_NO};
  				                                                                  debug("PARAMS b4 LeadDash :"+JSON.stringify(abc));
  				                                                          } ,"Application Sync","OK");
  				                                                      }
  				                                                      console.log("ISSYNCED update success !!");
  				                                                      dfd.resolve(true);
  															    });
  															else
  															    dfd.resolve(true);

  														}
  														else
  														 {
  															 CommonService.hideLoading();
  															 navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
  															dfd.resolve(false);
  														 }
  												});
  											});

										}
										else
											{
												CommonService.hideLoading();
												debug("Already synced APP data ");
                                      var issynced = 'Y';
                                        if(syncObj.EMPLOYEE_FLAG == 'E')
                                          issynced = 'E';
												if(syncObj.popup == undefined && syncObj.popup!='T')
																			CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":syncObj.APP_ID,"AGENT_CD":syncObj.AGENT_CD}).then(
																function(res){
																	if(syncObj.contDocUpload==false)
																	{
																		CommonService.hideLoading();
																		navigator.notification.alert(APP_DATA_SYN_MSG,function(){
																					var abc = {"LEAD_ID": syncObj.LEAD_ID, "FHR_ID": syncObj.FHR_ID, "SIS_ID": syncObj.SIS_ID, "APP_ID": syncObj.APP_ID, "OPP_ID": syncObj.OPP_ID, 'POLICY_NO': syncObj.POLICY_NO};
																					debug("PARAMS b4 LeadDash :"+JSON.stringify(abc));
																		} ,"Application Sync","OK");
																	}
																	console.log("ISSYNCED update success !!");
																	dfd.resolve(true);
																});
												else
												    dfd.resolve(true);
											}
									});
								}
								else
								{
									CommonService.hideLoading();
									 navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
									dfd.resolve(false);
								}
							});
							else
								syncObj.getAppSyncData().then(function(appSyncReq){
									console.log("inside NON combo flow")
										if(appSyncReq!=undefined && appSyncReq.REQ!=undefined)
										{
                    				//console.log(appSyncReq.REQ.AppCntA.length+"syncObj.OPP_ID :"+syncObj.OPP_ID+ " :: " +syncObj.AGENT_CD);

                                          LoadApplicationScreenData.loadLeadEKYCData(syncObj.OPP_ID, syncObj.AGENT_CD,null, syncObj.LEAD_ID).then(function(EKYCData){
                                    				//	console.log(JSON.stringify(appSyncReq.REQ.AppCntA[0])+"::inside EKYC method::"+JSON.stringify(EKYCData));

															if(!!EKYCData.Insured.EKYC_FLAG)
																{console.log("idfffffff");
																appSyncReq.REQ.AppCntA[0].EKYC = EKYCData.Insured.EKYC_FLAG;}
															else
																{console.log("elseeeeeeee");
																appSyncReq.REQ.AppCntA[0].EKYC = 'N';}


														if(!!appSyncReq.REQ.AppCntA[1])
															if(!!EKYCData.Proposer.EKYC_FLAG)
																appSyncReq.REQ.AppCntA[1].EKYC = EKYCData.Proposer.EKYC_FLAG;
															else
																appSyncReq.REQ.AppCntA[1].EKYC = 'N';
														debug("HER APP  :"+JSON.stringify(appSyncReq.REQ.AppCntA));
											syncObj.syncAppData(appSyncReq).then(function(resp){
													if(!!resp && resp.RS.RESP == "S")
													{
                                          var issynced = 'Y';
                                            if(syncObj.EMPLOYEE_FLAG == 'E')
                                              issynced = 'E';
													    if(syncObj.popup == undefined && syncObj.popup!='T')
                                                  CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":syncObj.APP_ID,"AGENT_CD":syncObj.AGENT_CD}).then(
                                                            function(res){
                                                                if(syncObj.contDocUpload==false)
                                                                {	CommonService.hideLoading();
                                                                    navigator.notification.alert(APP_DATA_SYN_MSG,function(){
                                                                            var abc = {"LEAD_ID": syncObj.LEAD_ID, "FHR_ID": syncObj.FHR_ID, "SIS_ID": syncObj.SIS_ID, "APP_ID": syncObj.APP_ID, "OPP_ID": syncObj.OPP_ID, 'POLICY_NO': syncObj.POLICY_NO};
                                                                            debug("PARAMS b4 LeadDash :"+JSON.stringify(abc));
                                                                    } ,"Application Sync","OK");
                                                                }
                                                                console.log("ISSYNCED update success !!");
                                                                dfd.resolve(true);
                                                            });
                                                        else
                                                            dfd.resolve(true);

													}
													else
													 {
														 CommonService.hideLoading();
														 navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
														dfd.resolve(false);
													 }
											})
                                    			});

										}
										else
											{
												CommonService.hideLoading();
												debug("Already synced APP data ");
                                      var issynced = 'Y';
                                        if(syncObj.EMPLOYEE_FLAG == 'E')
                                          issynced = 'E';
												if(syncObj.popup == undefined && syncObj.popup!='T')
                                                                                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":syncObj.APP_ID,"AGENT_CD":syncObj.AGENT_CD}).then(
                                                                    function(res){
                                                                        if(syncObj.contDocUpload==false)
                                                                        {
                                                                            CommonService.hideLoading();
                                                                            navigator.notification.alert(APP_DATA_SYN_MSG,function(){
                                                                                        var abc = {"LEAD_ID": syncObj.LEAD_ID, "FHR_ID": syncObj.FHR_ID, "SIS_ID": syncObj.SIS_ID, "APP_ID": syncObj.APP_ID, "OPP_ID": syncObj.OPP_ID, 'POLICY_NO': syncObj.POLICY_NO};
                                                                                        debug("PARAMS b4 LeadDash :"+JSON.stringify(abc));
                                                                            } ,"Application Sync","OK");
                                                                        }
                                                                        console.log("ISSYNCED update success !!");
                                                                        dfd.resolve(true);
                                                                    });
												else
													dfd.resolve(true);
											}
									});
					});
					/**/
				}
			);
		}
		else{
			navigator.notification.alert(REQ_FAIL_MSG, function(){
				CommonService.hideLoading();
			},"Application Sync","OK");
		}
});
return dfd.promise;
}

this.syncAppData = function(appSyncReq){

var dfd = $q.defer();

CommonService.APP_AJAX_CALL(APP_DATA_SYNC_URL, AJAX_TYPE, TYPE_JSON, appSyncReq, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
		debug("SAD response " + JSON.stringify(ajaxResp));
            if(!!ajaxResp && !!ajaxResp.RS){
                dfd.resolve(ajaxResp)
            }
            else
				{dfd.resolve(null);
        			CommonService.hideLoading();
        			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
        		}
		},
		function(data, status, headers, config, statusText){
			dfd.resolve(null);
			CommonService.hideLoading();
			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
		});

return dfd.promise;

};


this.getAppSyncData = function(){

    var dfd = $q.defer();
    var appSyncReq = {};
    var tempReq={};
    appSyncReq.REQ = {};
    var isDataSync ;


CommonService.transaction(db, function(tx){

var sql1 = "SELECT APPLICATION_ID AS APPID,SIS_ID AS SISID,POLICY_NO AS POL,PLAN_CODE AS PLCD,PLAN_NAME AS PLNM,POLICY_TERM AS POLTRM,PREMIUM_PAY_TERM AS PAYTRM,PREMIUM_PAY_MODE AS PYMDCD,PREMIUM_PAY_MODE_DESC AS PYMDDS,ANNUAL_PREMIUM_AMOUNT AS APRM,MODEL_PREMIUM_AMOUNT AS MPRM,DISCOUNTED_MODAL_PREMIUM AS DMP,DISCOUNTED_SERVICE_TAX AS DST,SUM_ASSURED AS SUMAU,SUB_OFFC_CODE AS SBOF,OFFICE_CODE AS OFCD,CHANNEL_CODE AS CLCD,ISINSURED_SIGNED AS ISGN,ISAGENT_SIGNED AS ASGN,ISPROPOSER_SIGNED AS PSGN,APPLICATION_DATE AS APDT,APPLICATION_PLACE AS APPL,EAI_NO AS EAINO,EAI_RECEIVE_FLAG AS EAIRV,INS_OBJ_RISK AS ORSK,INS_OBJ_SAVINGS AS OSAV,INS_OBJ_CHILD_EDU AS OCLED,INS_OBJ_MARRIAGE AS OMRG,INS_OBJ_RETIREMENT AS ORET,INS_OBJ_LEGACY_PLAN AS OLEG,INS_OBJ_OTHER_DETAILS AS OOTH,TATAAIA_INS_FLAG AS TATAINS,OTH_COMPANY_INS_FLAG AS OTHINS,INSURANCE_BACK_DATE AS BCKDT,ISSYNCED,RECO_PRODUCT_DEVIATION_FLAG AS PRDEFLG FROM LP_APPLICATION_MAIN WHERE APPLICATION_ID=? AND AGENT_CD=?";
    CommonService.executeSql(tx, sql1,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){

                        isDataSync=res.rows.item(0).ISSYNCED;
                        appSyncReq.REQ = CommonService.resultSetToObject(res);

    if(isDataSync != 'Y' && isDataSync != 'E'){
            appSyncReq.REQ.AC = syncObj.AGENT_CD;
            appSyncReq.REQ.PWD = LoginService.lgnSrvObj.password;
            appSyncReq.REQ.DVID = localStorage.DVID;
            appSyncReq.REQ.ACN = "SAD";
            if(!!syncObj.EMPLOYEE_FLAG)
              appSyncReq.REQ.IDS = syncObj.EMPLOYEE_FLAG;

            appSyncReq.REQ.VERNO = VERSION_NO;
			var sqlOpp = "select * from LP_MYOPPORTUNITY where APPLICATION_ID=?";
			CommonService.executeSql(tx, sqlOpp,[syncObj.APP_ID], function(tx,resOpp){
            				console.log("resOpp leng:"+resOpp.rows.length);
            				if(resOpp.rows.length!=0)
							{
								if(!!resOpp && resOpp.rows.length!=0)
								{
									console.log("resOpp:"+JSON.stringify(resOpp.rows.item(0)));
										appSyncReq.REQ.CMB = resOpp.rows.item(0).RECO_PRODUCT_DEVIATION_FLAG;
										appSyncReq.REQ.CMBID = resOpp.rows.item(0).COMBO_ID;
									if(!!resOpp && resOpp.rows.item(0).RECO_PRODUCT_DEVIATION_FLAG == 'P')
										appSyncReq.REQ.CMBP = resOpp.rows.item(0).REF_POLICY_NO;
									else if(!!resOpp && resOpp.rows.item(0).RECO_PRODUCT_DEVIATION_FLAG == 'T')
										appSyncReq.REQ.CMBP = resOpp.rows.item(0).POLICY_NO;
									else
										{
										    appSyncReq.REQ.CMB = null;
										    appSyncReq.REQ.CMBID = null;
										    appSyncReq.REQ.CMBP = null;
										}

									console.log("resOpp:appSyncReq:"+JSON.stringify(appSyncReq));
								}
								else
								{
									appSyncReq.REQ.CMB = null;
									appSyncReq.REQ.CMBID = null;
									appSyncReq.REQ.CMBP = null;
								}
							}
            				}, function(tx, err){console.log("Error 1");});
            var sql2 = "SELECT PAY_METHOD_CASH_FLAG AS CSFLG, PAY_METHOD_CHEQ_FLAG AS CHFLG, PAY_METHOD_DD_FLAG AS DDFLG, PAY_METHOD_ECS_FLAG AS ECSFLG, PAY_METHOD_SI_FLAG AS SIFLG, PAY_METHOD_ONLINE_FLAG ONLFLG, CASH_AMOUNT AS CAAMT, CHEQUE_NO AS CHNO, CHEQ_BANK_NAME AS CHBKNM, CHEQ_BANK_IFSC_CODE AS CHIFSC, CHEQ_AMOUNT AS CHAMT, CHEQ_DATE AS CHDT, DEMAND_DRAFT_NO AS DNO, DD_BANK_NAME AS DBKNM, DD_BANK_IFSC_CODE AS DIFSC, DD_BANK_BRANCH_NAME AS DBRNM, DD_AMOUNT AS DAMT, CREDIT_CARD_NO AS CCNO, CC_AMOUNT AS CCAMT, CC_EXP_DATE AS CCEXDT, ECS_BANK_ACCOUNT_NO AS EACNO, ECS_BANK_NAME AS EBKNM, ECS_BANK_IFSC_CODE AS EIFSC, ECS_BANK_BRANCH_NAME AS EBRNM, ECS_AMOUNT AS EAMT, SI_BANK_ACCOUNT_NO AS SACNO, SI_BANK_NAME AS SIBKNM, SI_BANK_IFSC_CODE AS SIFSC, SI_BANK_BRANCH_NAME AS SIBRNM, SI_AMOUNT AS SIAMT, TOTAL_PAYMENT_AMOUNT AS TPYAM, TRANSACTION_DATE AS TRNDT, TOTAL_PREMIUM_AMOUNT AS TPRAM, TOTAL_SERVICE_TAX AS TSVAM, MONTHS_INITIAL_DEPOSIT AS INITMN, FIRST_ANIV_CHANGE_PAY_MODE AS MDCHNG, FIRST_ANIV_CHANGE_PAY_MODE_CODE AS MDCHNGCD, SI_IMAGE_CAPTURED AS SIIMG, PAY_METHOD_CC_FLAG AS CCFLG, CHEQ_BANK_BRANCH_NAME AS CHBRNM, CHEQ_IMAGE_CAPTURED AS CHIMF, DD_IMAGE_CAPTURED AS DDIMF,CC_IMAGE_CAPTURED CCIMG, CC_AUTHORIZE_FORM AS CCAF,CC_CARD_TYPE AS CCTYP, ECS_AUTHORIZE_FORM AS ECAF, SI_AUTHORIZE_FORM AS SIAF FROM LP_APP_PAY_DTLS_SCRN_G WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql2,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                        if(res!=undefined && res.rows.length>0){
                                                             var keys = Object.keys(res.rows.item(0));
                                                             for(var i=0;i<keys.length;i++)
                                                             appSyncReq.REQ[keys[i]] = res.rows.item(0)[keys[i]];

                                                             }
                                                    }, function(tx, err){console.log("Error 1");});
			console.log("SAD APPID :"+syncObj.APP_ID);
            var sql3 = "SELECT NEFT_ACCOUNT_HOLDER_NAME AS NAME, NEFT_BANK_ACCOUNT_NO AS ACNO, NEFT_BANK_NAME AS BNKNM, NEFT_BANK_BRANCH_NAME AS BRNM, NEFT_BANK_ACCOUNT_TYPE AS ACTYP, NEFT_BANK_IFSC_CODE AS IFSC, CHQ_BANKST_IMAGE_CAPTURED AS CBNIC FROM LP_APP_PAY_NEFT_SCRN_H WHERE APPLICATION_ID=? AND AGENT_CD=?";

			/*CommonService.executeSql(tx, sqlOpp,[syncObj.APP_ID], function(tx,resOpp){
				console.log("resOpp leng:"+resOpp.rows.length);*/
				CommonService.executeSql(tx, sql3,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
					if(res!=undefined && res.rows.length>0){
												 var keys = Object.keys(res.rows.item(0));
												 for(var i=0;i<keys.length;i++)
												 {
													appSyncReq.REQ[keys[i]] = res.rows.item(0)[keys[i]];

												 }

												 }
												 //
												}, function(tx, err){console.log("Error 1");});
			 //
			//}, function(tx, err){console.log("Error 1");});


            var sql13 = "SELECT RYP_AUTOPAY_FLAG AS RPF, RYP_AUTOPAY_NACH_FLAG AS RPNF, RYP_AUTOPAY_SI_FLAG AS RSIF, RYP_SI_BANK_ACCOUNT_NO AS RSAN, RYP_SI_BANK_ACCOUNT_TYPE AS RSBT, RYP_SI_BANK_NAME AS RSB, RYP_SI_BANK_IFSC_CODE AS RSIFSC, RYP_SI_BANK_BRANCH_NAME AS RSBB, RYP_SI_AMOUNT AS RSA, RYP_SI_CHEQUE_IMAGE AS RSCI, RYP_SI_MANDATE_FORM AS RSMF, RYP_NACH_BANK_ACCOUNT_NO AS RNBA, RYP_NACH_BANK_ACCOUNT_TYPE AS RNBAT, RYP_NACH_BANK_NAME AS RNBN, RYP_NACH_BANK_IFSC_CODE AS RNIFSC, RYP_NACH_BANK_BRANCH_NAME AS RNBB, RYP_NACH_AMOUNT AS RNA, RYP_NACH_CHCK_IMAGE AS RNCI, RYP_NACH_MANDATE_FORM AS RNMF from LP_APP_PAY_NEFT_SCRN_I where APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql13,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                if(res!=undefined && res.rows.length>0){
                                                 var keys = Object.keys(res.rows.item(0));
                                                 for(var i=0;i<keys.length;i++)
                                                 appSyncReq.REQ[keys[i]] = res.rows.item(0)[keys[i]];
                                                 }
                                                 }, function(tx, err){console.log("Error 1");});
 			/*console.log("syncObj.OPP_ID :"+syncObj.OPP_ID+ " :: " +syncObj.AGENT_CD);
			LoadApplicationScreenData.loadEKYCData(syncObj.OPP_ID, syncObj.AGENT_CD).then(function(EKYCData){
				console.log("::inside EKYC method::"+JSON.stringify(EKYCData));*/
				//Fatca Key added
				var sql4 = "SELECT CUST_TYPE AS CSTTYP, TITLE AS TITL, FIRST_NAME AS FNM, MIDDLE_NAME AS MNM, LAST_NAME AS LNM, HUSB_FATH_FIRST_NAME AS HFFNM, HUSB_FATH_LAST_NAME AS HFLNM, MAIDEN_NAME AS MDNM, GENDER AS GND, GENDER_CODE AS GNDCD, BIRTH_DATE AS DOB, IDENTIFICATION_MARK AS IDMRK, MARTIAL_STATUS AS MRTL, MARTIAL_STATUS_CODE AS MRTLCD, AGE_PROOF AS AGEPRF, AGE_PROOF_OTHER AS AGEPROT, RESIDENT_STATUS AS RESI, RESIDENT_STATUS_CODE AS RESICD, NATIONALITY AS NAT, NATIONALITY_CODE AS NATCD, CURR_RESIDENT_COUNTRY AS RCNT, CURR_RESIDENT_COUNTRY_CODE AS RCNTCD, EDUCATION_QUALIFICATION AS EDU, OTHER_EDUCATION_DETS AS EDUOT, EDUCATION_QUALIFICATION_CODE AS EDUCD, CURR_ADD_LINE1 AS CADD1, CURR_ADD_LINE2 AS CADD2, CURR_ADD_LINE3 AS CADD3, CURR_DISTRICT_LANDMARK AS CDIST, CURR_CITY AS CCTY, CURR_CITY_CODE AS CCTYCD, CURR_STATE AS CSAT, CURR_STATE_CODE AS CSATCD, CURR_PIN AS CPIN, IS_CURRADD_PERMADD_SAME AS CRISPM, PERM_ADD_LINE1 AS PADD1, PERM_ADD_LINE2 AS PADD2, PERM_ADD_LINE3 AS PADD3, PERM_DISTRICT_LANDMARK AS PDIST, PERM_CITY AS PCITY, PERM_CITY_CODE AS PCITYCD, PERM_STATE AS PSAT, PERM_STATE_CODE AS PSATCD, PERM_PIN AS PPIN, MOBILE_NO AS MOB, RESI_STD_CODE AS RSTD, RESI_LANDLINE_NO AS RLND, EMAIL_ID AS EML, COMMUNICATE_ADD_FLAG AS COMAD, OCCUPATION_CLASS AS OCCL, OCCUPATION_CLASS_OTHER AS OCCLSOT, OCCUPATION_CLASS_NBFE_CODE AS OCCLSCD, STUDENT_STANDARD AS SSTD, EMPLOYER_SCH_BUSI_NAME AS ESBNM, EMPLOYER_SCH_BUSI_ADDRESS AS ESBADD, OCCUPATION AS OCU, OCCUPATION_OTHERS AS OCUOT, OCCUPATION_INDUSTRY AS OCIND, OCCUP_INDUSTRY_OTHERS AS OCINDOT, OCCU_WORK_NATURE AS OCNAT, OCCU_WORK_NATURE_OTHERS AS OCNATOT, OCCUPATION_CD AS OCUCD, ANNUAL_INCOME AS ANINCM, IDENTITY_PROOF AS IDPRF, IDENTITY_PROOF_OTHER AS IDPRFOT, IDENTITY_PROOF_NO AS IDPRFNO, INCOME_PROOF AS INPRF, INCOME_PROOF_OTHER AS INPRFOT, PAN_CARD_NO AS PANNO, PAN_CARD_EXISTS AS PANEXT, AADHAR_CARD_NO AS ADRNO, AADHAR_CARD_EXISTS AS ADREXT, PEP_SELP_FLAG AS PERSLF, PEP_DETAILS AS PERDTL, PEP_FAMILY_FLAG AS PERFML, RELATIONSHIP_DESC AS RELDSC, RELATIONSHIP_ID AS RELID, DOMINANT_HAND AS DMHND, AGE_PROOF_DOC_ID AS AGEPRFDID, OFF_STD_CODE AS OSTD, OFF_LANDLINE_NO AS OLND, IDENTITY_PROOF_DOC_ID AS IDPRFCD, INCOME_PROOF_DOC_ID AS INPRFCD, ADDRESS_PROOF_DOC_ID AS ADPRFCD, PEP_FAMILY_DETAILS AS PERFDTL, ADDRESS_PROOF AS ADPRF, ADDRESS_PROOF_OTHER AS ADPRFOT, COMPANY_CODE AS CMPCODE, FATCA AS FATCA ,FATCA_FATHER_NAME AS FAFN, FATCA_BIRTH_PLACE AS FABP, FATCA_BIRTH_COUNTRY AS FABC, FATCA_BIRTH_COUNTRY_CODE AS FABCC, FATCA_OCC_TYPE AS FAOT, FATCA_ID_TYPE AS FAIT,FATCA_ID_NO AS FAIN FROM LP_APP_CONTACT_SCRN_A WHERE APPLICATION_ID=? AND AGENT_CD=?";
				CommonService.executeSql(tx, sql4,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
									 appSyncReq.REQ.AppCntA = [];
									 console.log("01 EKYC flag::"+res.rows.length);
									 if(res.rows.length!=0){
										 for(var i=0;i<res.rows.length;i++)
										 {
										 	console.log(" EKYC CNT::"+i);
											appSyncReq.REQ.AppCntA.push(res.rows.item(i));
											console.log("appending EKYC flag::"+i);

											/*if(i == 0)
												if(!!EKYCData.Insured.EKYC_FLAG)
													appSyncReq.REQ.AppCntA[0].EKYC = EKYCData.Insured.EKYC_FLAG;
												else
													appSyncReq.REQ.AppCntA[0].EKYC = 'N';

											if(i == 1)
												if(!!EKYCData.Proposer.EKYC_FLAG)
													appSyncReq.REQ.AppCntA[1].EKYC = EKYCData.Proposer.EKYC_FLAG;
												else
													appSyncReq.REQ.AppCntA[1].EKYC = 'N';*/

										 }

									 }

										}, function(tx, err){console.log("Error 1");});

			//});


            var sql5 = "SELECT CUST_TYPE AS CSTTYP, QUESTION_ID AS QUESID, ANSWAR_FLAG AS ANSFL, ANSWAR_DTLS1 AS ANS1, ANSWAR_DTLS2 AS ANS2, ANSWAR_DTLS3 AS ANS3, ANSWAR_DTLS4 AS ANS4, ANSWAR_DTLS5 AS ANS5, ANSWAR_DTLS6 AS ANS6 FROM LP_APP_LS_ANS_SCRN_B WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql5,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                appSyncReq.REQ.AppLSB = [];
                                                 if(res!=undefined && res.rows.length>0){
                                                 for(var i=0;i<res.rows.length;i++)
                                                 appSyncReq.REQ.AppLSB.push(res.rows.item(i));

                                                 }
                                                    }, function(tx, err){console.log("Error 1");});

            var sql6 = "SELECT CUST_TYPE AS CSTTYP, QUESTION_ID AS QUESID, ANSWAR_FLAG AS ANSFLG, ANSWAR_DTLS1 AS ANS1, ANSWAR_DTLS2 AS ANS2 FROM LP_APP_HD_ANS_SCRN_C WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql6,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                appSyncReq.REQ.AppHLC = [];
                                                 if(res!=undefined && res.rows.length>0){
                                                 for(var i=0;i<res.rows.length;i++)
                                                 appSyncReq.REQ.AppHLC.push(res.rows.item(i));

                                                 }

                                                    }, function(tx, err){console.log("Error 1");});
            var sql7 = "SELECT CUST_TYPE AS CSTTYP, SUB_QUESTION_ID AS QUESID, ANSWAR_DTLS1 AS ANSFAG FROM LP_APP_HD_ANS_SCRN_C13 WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql7,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                appSyncReq.REQ.AppSubHLC13 = [];
                                                 if(res!=undefined && res.rows.length>0){
                                                 for(var i=0;i<res.rows.length;i++)
                                                 appSyncReq.REQ.AppSubHLC13.push(res.rows.item(i));
                                                 }
                                                    }, function(tx, err){console.log("Error 1");});

            var sql8 = "SELECT CUST_TYPE AS CSTTYP, INS_SEQ_NO AS INSEQ, TATA_OTHER_FLAG AS TATFLG, POLICY_NO AS POLNO, SUM_ASSURED AS SUMAU, INSURANCE_PROVIDER AS INSPRV, TYPE_OF_INSURANCE AS INSTYP, ANNUAL_PREMIUM AS ANPRM, POLICY_TERM AS POLTRM, DECISION AS DECI, DECISION_DETAILS AS DECID FROM LP_APP_EXIST_INS_SCRN_D WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql8,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                    appSyncReq.REQ.AppExtInsD = [];
                                                     if(res!=undefined && res.rows.length>0){
                                                     for(var i=0;i<res.rows.length;i++)
                                                     appSyncReq.REQ.AppExtInsD.push(res.rows.item(i));


                                                     }

                                                        }, function(tx, err){console.log("Error 1");});

            var sql9 = "SELECT RELATIONSHIP_ID AS RELID,ALIVE_OR_DECEASED AS ALIVE,HEALTH_STATUS_DECEASED_REASON AS HLSTA,DEATH_AGE AS DAGE,FIRST_NAME AS FNM,MIDDLE_NAME AS MNM,LAST_NAME AS LNM,GENDER AS GND,BIRTH_DATE AS DOB,OCCUPATION_DESC AS OCCU,ANNUAL_INCOME AS ANINC,INSURANCE_DETAILS AS INSDTL FROM LP_APP_FH_SCRN_E WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql9,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                    appSyncReq.REQ.AppFamilyE = [];
                                                     if(res!=undefined && res.rows.length>0){
                                                     for(var i=0;i<res.rows.length;i++)
                                                     appSyncReq.REQ.AppFamilyE.push(res.rows.item(i));

                                                     }
                                                        }, function(tx, err){console.log("Error 1");});

            var sql10 =  "SELECT CUST_TYPE AS CSTTYP, TITLE AS TITL, FIRST_NAME AS FNM, MIDDLE_NAME AS MNM, LAST_NAME AS LNM, BIRTH_DATE AS DOB, RELATIONSHIP_DESC AS RELDSC, RELATIONSHIP_ID AS RELID,  SEQ_NO AS SQNO, GENDER AS GNDR, GENDER_CODE AS GNDRC, PERCENTAGE AS PRCNT FROM LP_APP_NOM_CONTACT_SCRN_F WHERE APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql10,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                    appSyncReq.REQ.AppNomApptF = [];

                                                     if(res!=undefined && res.rows.length>0){
                                                     for(var i=0;i<res.rows.length;i++)
                                                     appSyncReq.REQ.AppNomApptF.push(res.rows.item(i));

                                                     console.log("My REQUEST"+JSON.stringify(appSyncReq));
                                                     }
                                                        }, function(tx, err){console.log("Error 1");});

            var sql11 = "SELECT INVEST_OPT AS INVOPT,FUND_CODE_FROM AS FFRM,FUND_CODE_TO AS FTO,FUND1_CODE AS F1CD,FUND1_PERCENT AS F1PT, FUND2_CODE AS F2CD,FUND2_PERCENT AS F2PT, FUND3_CODE AS F3CD,FUND3_PERCENT AS F3PT, FUND4_CODE AS F4CD, FUND4_PERCENT AS F4PT, FUND5_CODE AS F5CD, FUND5_PERCENT AS F5PT,FUND6_CODE AS F6CD, FUND6_PERCENT AS F6PT, FUND7_CODE AS F7CD, FUND7_PERCENT AS F7PT, FUND8_CODE AS F8CD, FUND8_PERCENT F8PT, FUND9_CODE AS F9CD, FUND9_PERCENT AS F9PT, FUND10_CODE AS F10CD, FUND10_PERCENT AS F10PT, OTHER_DETAILS AS OTDT from LP_APP_FUND_SCRN_I where APPLICATION_ID=? AND AGENT_CD=?"
                CommonService.executeSql(tx, sql11,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
                                                     appSyncReq.REQ.AppFundtI = [];
                                                     if(res!=undefined && res.rows.length>0){
                                                     for(var i=0;i<res.rows.length;i++){
                                                     console.log("Here"+res.rows.item(i).F1PT);
                                                     //if(res.rows.item(i).F1PT==null)
                                                     appSyncReq.REQ.AppFundtI.push(res.rows.item(i));
                                                     }

                                                     console.log("My REQUEST FUNDS:"+JSON.stringify(appSyncReq));
                                                     dfd.resolve(appSyncReq);
                                                     }
                                                      else
                                                         dfd.resolve(appSyncReq)

                                                        }, function(tx, err){console.log("Error 1");});

            var sql12 = "SELECT CUST_TYPE AS CTYPE,RISK_AGRT_VALUE AS RAV,SUM_AT_RISK_VALUE AS SAR,FINANCIAL_LIMIT_VALUE AS FLV,HUMAN_LIFE_VALUE AS HLV,MG_MEDICAL_FLAG AS MGMF,MG_MEDICAL_TEST_LIST AS MGMT,MG_SELECTED_DC_FLAG AS MGSDC, MG_TPA_CODE AS MGTPCD,MG_TPA_NAME AS MGTPNM, MG_DC_CODE AS MGDCCD,MG_DC_NAME AS MGDCNM,MG_DC_ADDRESS AS MGDCAD, MG_DC_CITY AS MGDCCY,MG_DC_PIN AS MGPIN,MG_DC_HOME_VISIT_FLAG AS MGDHV,MG_PREFERED1_DATE AS MGPD1,MG_PREFERED1_TIME AS MGPT1,MG_PREFERED2_DATE AS MGPD2,MG_PREFERED2_TIME AS MGPT2,MG_PREFERED3_DATE AS MGPD3,MG_PREFERED3_TIME AS MGPT3,MG_MTRF_SRNO AS MGSNO from LP_APP_SAR_MED_SCREEN_K where APPLICATION_ID=? AND AGENT_CD=?";
                CommonService.executeSql(tx, sql12,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
            appSyncReq.REQ.SARMEDICALDETAILS = [];

                                                 if(res!=undefined && res.rows.length>0){
                                                 for(var i=0;i<res.rows.length;i++)
                                                 appSyncReq.REQ.SARMEDICALDETAILS.push(res.rows.item(i));

                                                 console.log("SARMEDICALDETAILS :"+JSON.stringify(appSyncReq.REQ.SARMEDICALDETAILS));
                                                 dfd.resolve(appSyncReq)
                                                 }
                                                 else
                                                   dfd.resolve(appSyncReq);
                                                    }, function(tx, err){console.log("Error 1");});

                //Added for FATCA
            var sql14 = "SELECT SEQ_NO AS SEQN, COUNTRY_TAX AS FACN, ADDRESS_TAX_BLDG AS FAABL, ADDRESS_TAX_STREET AS FAATS, ADDRESS_TAX_CITY AS FAACI, ADDRESS_TAX_STATE AS FAAST, ADDRESS_TAX_PINCODE AS FAAPI, TIN AS FATIN, TIN_COUNTRY AS FATICN, TIN_COUNTRY_CODE AS FATICC, DOC_VALIDITY AS FATIDV FROM LP_APP_FATCA_DOC WHERE APPLICATION_ID=? AND AGENT_CD=?";
            CommonService.executeSql(tx, sql14,[syncObj.APP_ID, syncObj.AGENT_CD], function(tx,res){
            appSyncReq.REQ.AppFAD = [];
            console.log(JSON.stringify(res));
                                             if(res!=undefined && res.rows.length>0){
                                             for(var i=0;i<res.rows.length;i++)

                                                appSyncReq.REQ.AppFAD.push(res.rows.item(i));
                                                console.log("FATCA :"+JSON.stringify(appSyncReq.REQ.AppFAD));
                                             }
                                                }, function(tx, err){console.log("Error 1");});

    }
    else
        dfd.resolve(null);

        }, function(tx, err){console.log("Error 1");});

    },function(err){console.log("Error 1");});

return dfd.promise;

}


this.startFinalSubmission = function(Params, ComboParams){
debug("Final Submission started :"+JSON.stringify(Params))

if(!!ComboParams)
	debug("Final Submission ComboParams :"+JSON.stringify(ComboParams));

if(CommonService.checkConnection() == "online"){
    syncObj.FHR_ID = Params.FHR_ID;
    //syncObj.OPP_ID = ;
    syncObj.SIS_ID = Params.SIS_ID;
    syncObj.APP_ID = Params.APPLICATION_ID;
    if(!!ComboParams)
    	syncObj.ComboParams = ComboParams;
    else
    	syncObj.ComboParams = null;

    var sql = "select * from LP_EMAIL_USER_DTLS where EMAIL_CAT=? AND EMAIL_CAT_ID=?";
    var param = ["APP", Params.APPLICATION_ID];
     CommonService.showLoading("Policy submission in Progress...\n Please wait..");
     this.getExceptionalApprovalData(Params.AGENT_CD, Params.SIS_ID).then(
         function(isASubFlaf){
             debug("isASubFlaf : " + isASubFlaf);
             Params.termISASUB = isASubFlaf;
             CommonService.transaction(db, function(tx){
                                             CommonService.executeSql(tx, sql,param, function(tx,res){
                                                  if(res.rows.length==0)
                                                  {
                                                     syncObj.insertEmailDet(Params);
                                                  }
                                                  else
                                                    syncObj.syncAgntImpData(Params);

                         					}, function(tx, err){console.log("Error 1");
                         					CommonService.hideLoading();
                         					});
                         					},function(err){
                         					CommonService.hideLoading();
                         					});

         });
     }
     else
     	navigator.notification.alert(OFFLINE_SYNC_MSG,null,"Application Sync","OK");
}


this.insertEmailDet = function(Params){

var sql = "insert or replace into LP_EMAIL_USER_DTLS values(?,?,?,?,?,?,?,?,?,?)";
var params = [];
SisFormService.loadSavedSISData(Params.AGENT_CD, Params.SIS_ID).then(
    function(SISFormData){
        var email = SISFormData.sisFormAData.PROPOSER_EMAIL;
        debug("PROPOSER EMAIL :: "+email);
        params = [CommonService.getRandomNumber(), Params.AGENT_CD, email, "", "", "", "APP", Params.APPLICATION_ID, CommonService.getCurrDate(), "N"];
        CommonService.transaction(db, function(tx){
                                        CommonService.executeSql(tx, sql,params, function(tx,res){
                                               syncObj.syncAgntImpData(Params);
                    					}, function(tx, err){console.log("Error 1");});
                    					},function(err){});
    });


}

this.syncAgntImpData = function(Params){

syncObj.getAgentImpData(Params).then(
    function(agentData){
        if(!!agentData){
            debug("JSON REQ agentData :"+JSON.stringify(agentData));
        CommonService.APP_AJAX_CALL(APP_AGNT_IMP_URL, AJAX_TYPE, TYPE_JSON, agentData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
        		debug("syncAgntImpData response " + JSON.stringify(ajaxResp));
                    if(!!ajaxResp && !!ajaxResp.RS){
                          if(ajaxResp.RS.RESP == 'S')
                          {
                            CommonService.updateRecords(db, 'LP_APP_SOURCER_IMPSNT_SCRN_L',{"ISSYNCED" : 'Y'}, {"APPLICATION_ID":Params.APPLICATION_ID,"AGENT_CD":Params.AGENT_CD}).then(
                                        function(res){
                                            console.log("syncAgntImpData SYNCED update success !!");
                                            syncObj.syncCCRTableData(Params);
                                        });
                          }
                          else
						  {
							 CommonService.hideLoading();
							 navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
						 }
                    }
                    else
					{
						CommonService.hideLoading();
						navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
					}
        		},
        		function(data, status, headers, config, statusText){
        			CommonService.hideLoading();
        			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
        		});
        }
        else
           syncObj.syncCCRTableData(Params);
    });
}


this.getAgentImpData = function(Params){

var dfd = $q.defer();
var agntImpReq = {};
agntImpReq.REQ = {};
//Ashwin changes 29-12-2016 removed 29th parameter related to CCR
var sql = "select POLICY_NO AS POL, SIS_ID AS SISID, FHR_ID AS FHRID, PREFERRED_LANGUAGE_CD AS PRLC, PREFERRED_CALL_TIME AS PRCT, IMP_RM_CAMS_CD AS RMC, IMP_AGENT_CAMS_CD AS SPC, SOURCER_CODE AS SOCD, SOURCER_NAME AS SONM, SOURCER_CONTACT_NO AS SOCT, IMP_RM_NAME AS RMN, IMP_AGENT_NAME AS SPN, IRDA_LIC_NO AS LICN, IRDA_LIC_EXP_DT AS LIEXD, MOBILENO AS MOB, CHANNEL_NAME AS CHN, OFFICE_CODE AS OFCD, SUBOFFICECODE AS SOFCD, OFFICE_NAME AS OFNM, OFFICE_ADDRESS1 AS OFCADD1, OFFICE_ADDRESS2 AS OFCADD2, OFFICE_STATE AS OFCST, OFFICE_ZIP_CODE AS OFCPIN, OFFICE_OFFICE_TEL AS OFCTEL, DIGITAL_PSC_FLAG AS DPSCF, ISDIGITAL_PSC_DONE AS DPSCS,AGENT_REL_FLAG AS ARN from LP_APP_SOURCER_IMPSNT_SCRN_L where AGENT_CD = ? AND APPLICATION_ID = ?";
var param = [Params.AGENT_CD, Params.APPLICATION_ID];
				// DPSCF - DPSCS
            agntImpReq.REQ.AC = Params.AGENT_CD;
            agntImpReq.REQ.PWD = LoginService.lgnSrvObj.password;
            agntImpReq.REQ.DVID = localStorage.DVID;
            agntImpReq.REQ.ACN = "SSPD";
            agntImpReq.REQ.APPID = Params.APPLICATION_ID;
    CommonService.transaction(db, function(tx){
         CommonService.executeSql(tx, "select ISSYNCED from LP_APP_SOURCER_IMPSNT_SCRN_L where AGENT_CD = ? AND APPLICATION_ID = ?",param, function(tx,res){
                     if(res!=undefined && res.rows.length>0){

                        if(!!res && res.rows.item(0).ISSYNCED != 'Y')
                            CommonService.executeSql(tx, sql,param, function(tx,res){
                                     if(res!=undefined && res.rows.length>0){
                                         var keys = Object.keys(res.rows.item(0));
                                         for(var i=0;i<keys.length;i++)
                                         {
                                             agntImpReq.REQ[keys[i]] = res.rows.item(0)[keys[i]];
                                             dfd.resolve(agntImpReq);
                                         }
                                     }
                                     else
                                       {    dfd.resolve(null);
                                            debug("Call next function");

                                       }

                                }, function(tx, err){console.log("Error 1");});
                        else
                            {
                                dfd.resolve(null);
                                debug("Call next function");
                            }
                     }
                     else
                     {
                        dfd.resolve(null);
                        debug("Call next function");
                     }
        },function(err){});
         },function(err){});
return dfd.promise;
}

this.syncProfileSignTableData = function(Params){

    var dfd = $q.defer();
    var sql = "select DOC_ID AS DOCID,DOC_CAT AS CAT,DOC_CAT_ID AS CATID, POLICY_NO AS POLNO,DOC_NAME AS DOCNM, DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG,DOC_CAP_ID AS DOCCID,DOC_CAP_REF_ID AS DOCRID from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT IN ('PHOTOGRAPH','APP') and (IS_DATA_SYNCED = ? or IS_DATA_SYNCED is null) and DOC_CAT_ID IN (?,?) AND DOC_NAME like '%.jpg'";
    var parameters = [Params.AGENT_CD,"N",Params.APPLICATION_ID, ''];
    if(!!syncObj.ComboParams && !!syncObj.ComboParams.APPLICATION_ID)
    	parameters = [Params.AGENT_CD,"N",Params.APPLICATION_ID, syncObj.ComboParams.APPLICATION_ID];

    CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx, sql, parameters,
                    function(tx,res){
                        debug("syncProfileTableData res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("inside calling FDS");
                        syncObj.getFDSReqParams(Params, res).then(function(docData){
                        	debug("ProfileFDS REQ :"+JSON.stringify(docData));
                            CommonService.ajaxCall(DOCUMENT_UPLOAD_URL,AJAX_TYPE,TYPE_JSON,docData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                                                debug("syncProfileTableData success: "+JSON.stringify(ajaxResponse));
                                                                //dfd.resolve(syncObj.updateDocTableData(ajaxResponse));
                                                                syncObj.updateDocTableData(ajaxResponse).then(
                                                                    function(res){
                                                                        if(res)
                                                                            {
                                                                                debug("syncProfileTableData success");
                                                                                //Call next sync
                                                                                syncObj.syncImageFileData(Params);
                                                                            }
                                                                        else
                                                                           {
                                                                           	CommonService.hideLoading();
                                                                            navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                                                           }
                                                                    }
                                                                )
                                                            },function(data, status, headers, config, statusText){
                                                                debug("syncProfileTableData fail:"+JSON.stringify(data));
																	CommonService.hideLoading();
                                                            });

                        });

                        }
                        else
                            {
                                //Call next sync
                                syncObj.syncImageFileData(Params);
                            }
                    },
                    function(tx,err)
                    {
						CommonService.hideLoading();
						navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
				  	}
                );
            },
            function(err){
            CommonService.hideLoading();
                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
            },null
    );

}


var isError = false
this.syncImageFileData = function(Params){
isError = false;
var sql = "select * from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT IN ('PHOTOGRAPH','APP') and (IS_FILE_SYNCED = ? or IS_FILE_SYNCED is null) and DOC_CAT_ID = ? AND DOC_NAME like '%.jpg'";
var params = [Params.AGENT_CD,"N",Params.APPLICATION_ID];

CommonService.transaction(db, function(tx){
                                CommonService.executeSql(tx, sql, params, function(tx,res){
                                debug("ImageData :: "+res.rows.length);
                                if(!!res && res.rows.length!=0)
                                {
                                	var i = 0;
                                	if(res.rows.item(i).IS_FILE_SYNCED != 'Y')
									{	if(res.rows.item(i).DOC_CAT == 'PHOTOGRAPH')
										{ var imagePath = "/FromClient/APP/IMAGES/PHOTOS/";
											syncObj.sendImageData(Params, res, i, imagePath, true);
										}
										else
										{
											var imagePath = "/FromClient/APP/SIGNATURE/";
											syncObj.sendImageData(Params, res, i, imagePath, true);
										}
									}
                                	/*for(var i = 0; i<res.rows.length ; i++)
										(function(i){

										}(i));*/

								}
								else
								{
									if(BUSINESS_TYPE=='IndusSolution'){
										syncObj.syncFFNATableData(Params);
									}
									else {
										if(Params.RECO_DEV_FLAG == 'Y' || Params.RECO_DEV_FLAG == 'N')
											syncObj.syncFFHRFile(Params).then(function(res) {
													if(!!res)
														syncObj.callSISSync(Params);
													else {
														navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
														 CommonService.hideLoading();
													}
											});
										else
											syncObj.callSISSync(Params);
									}
								}

            					}, function(tx, err){console.log("Error 1");});
            					},function(err){}, function(){
            						debug("imageSyncComplete"+isError);
            					});



}

this.getFDSReqParams = function(Params, res){
var dfd = $q.defer();

console.log("inside getFDSReqParams :"+JSON.stringify(res.rows.item(0)));
 var docData = {};
 var request = {};
 request.AC = Params.AGENT_CD;
 request.PWD = LoginService.lgnSrvObj.password;
 request.DVID = localStorage.DVID;
 request.ACN = "FDS";
 request.DOCUPLOAD = [];
 docData.REQ = request;

   for(var i=0;i<res.rows.length;i++)
   {
     docData.REQ.DOCUPLOAD.push(res.rows.item(i));
     if(i == (res.rows.length - 1))
        dfd.resolve(docData);
   }

return dfd.promise;
}


this.syncCCRTableData = function(Params){
	var CCR_DOC_ID = "6033010334";
    var dfd = $q.defer();
    var sql = "select DOC_ID AS DOCID,DOC_CAT AS CAT,DOC_CAT_ID AS CATID, POLICY_NO AS POLNO,DOC_NAME AS DOCNM, DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG,DOC_CAP_ID AS DOCCID,DOC_CAP_REF_ID AS DOCRID from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT = ? and (IS_DATA_SYNCED = ? or IS_DATA_SYNCED is null) and DOC_CAT_ID IN (?,?) AND DOC_ID=?"
   	var parameters = [Params.AGENT_CD,"FORM","N",Params.APPLICATION_ID, '',CCR_DOC_ID];
   	if(!!syncObj.ComboParams && !!syncObj.ComboParams.APPLICATION_ID)
    	parameters = [Params.AGENT_CD,"FORM","N",Params.APPLICATION_ID, syncObj.ComboParams.APPLICATION_ID,CCR_DOC_ID];

    CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx, sql, parameters,
                    function(tx,res){
                        debug("sync data res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("CCR :inside calling FDS")
                        syncObj.getFDSReqParams(Params, res).then(function(docData){
                            CommonService.ajaxCall(DOCUMENT_UPLOAD_URL,AJAX_TYPE,TYPE_JSON,docData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                                                debug("CCR HTML success: "+JSON.stringify(ajaxResponse));
                                                                //dfd.resolve(syncObj.updateDocTableData(ajaxResponse));
                                                                syncObj.updateDocTableData(ajaxResponse).then(
                                                                    function(res){
                                                                        if(res)
                                                                            {
                                                                                debug("CCR docupload success");
                                                                                syncObj.syncCCRFile(Params);
                                                                            }
                                                                        else
                                                                            {
                                                                            	navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                                                            	CommonService.hideLoading();
                                                                            }
                                                                    }
                                                                )
                                                            },function(data, status, headers, config, statusText){
                                                                debug("CCR HTML fail:"+JSON.stringify(data));
																CommonService.hideLoading();
                                                            });

                        });

                        }
                        else
                            {
                                syncObj.syncCCRFile(Params);//syncObj.callSISSync(Params);
                            }
                    },
                    function(tx,err){
                        navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                        CommonService.hideLoading();
                    }
                );
            },
            function(err){
                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                CommonService.hideLoading();
            },null
    );

}


this.syncCCRFile = function(Params){
var CCR_DOC_ID = "6033010334";
var imagePath = "/FromClient/APP/HTML/";
    CcrQuesService.getIsAgentRelatedInformation(Params.APPLICATION_ID).then(//1788
    function(responceIsAgentRelated){
        debug("responceIsAgentRelated is "+JSON.stringify(responceIsAgentRelated))
        var sql = "Select * from LP_DOCUMENT_UPLOAD where DOC_CAT_ID=? AND AGENT_CD = ? AND DOC_CAT = ? AND (IS_FILE_SYNCED=? or IS_FILE_SYNCED is null) AND DOC_ID = ? AND DOC_NAME NOT LIKE '%.html'";

        if((!!responceIsAgentRelated && responceIsAgentRelated.AGENT_REL_FLAG != "Y") || (responceIsAgentRelated == undefined))
            sql = "Select * from LP_DOCUMENT_UPLOAD where DOC_CAT_ID=? AND AGENT_CD = ? AND DOC_CAT = ? AND (IS_FILE_SYNCED=? or IS_FILE_SYNCED is null) AND DOC_ID = ? AND DOC_NAME LIKE '%.html'";


        debug("Query CCR is " +sql);
        var params = [Params.APPLICATION_ID , Params.AGENT_CD, "FORM", 'N',CCR_DOC_ID];
        CommonService.transaction(db, function(tx){
                CommonService.executeSql(tx, sql, params, function(tx,res){
                debug("CCR :: "+res.rows.length);
                 debug("CCR :RECORD: data "+JSON.stringify(res));
                    if(!!res && res.rows.length!=0 && res.rows.item(0).IS_FILE_SYNCED != 'Y')
                    {
                        debug("CCR :RECORD: "+JSON.stringify(res.rows.item(0)));
                        if(((!!responceIsAgentRelated && responceIsAgentRelated.AGENT_REL_FLAG != "Y") || (responceIsAgentRelated == undefined)) && res.rows.length == 1 && res.rows.item(0).DOC_NAME.indexOf("jpg") == -1 && res.rows.item(0).DOC_NAME.indexOf("pdf") == -1){
                            //there is only one html file in the stack

                            syncObj.sendFileData(Params, res.rows.item(0), imagePath, "CCR");//1788
                        }else{
                               imagePath = "/FromClient/APP/IMAGES/";
                                debug("doc uploaded for CCR Image 1788");
                                syncObj.sendAgentRelatedDataCCR(Params, res, imagePath, "CCR").then(function(data){
                                    //changes 29th august..for server response false..
                                    debug(" check the application "+JSON.stringify(data));
                                    if(!!data){
                                        for(var j=0;j<data.length;j++){
                                            if(data[j] == false){
                                                 debug("unable to upload please check ");
                                                CommonService.hideLoading();
                                                return;
                                            }
                                        }
                                       syncObj.syncProfileSignTableData(Params);
                                    }
                                    else{
                                        debug("upload is successfull");
                                        CommonService.hideLoading();
                                        return;
                                    }


                                });

                        }
                    }
                     else
                      syncObj.syncProfileSignTableData(Params);

                }, function(tx, err){console.log("Error 1");
                     CommonService.hideLoading();
                });
                },function(err){});



    });
}


this.syncFFNATableData = function(Params){

    var dfd = $q.defer();
    var sql = "select DOC_ID AS DOCID,DOC_CAT AS CAT,DOC_CAT_ID AS CATID, POLICY_NO AS POLNO,DOC_NAME AS DOCNM, DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT = ? and (IS_DATA_SYNCED = ? or IS_DATA_SYNCED is null) and DOC_CAT_ID = ? AND DOC_NAME like '%.html'"
    var parameters = [Params.AGENT_CD,"FHR","N",Params.FHR_ID];

    CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx, sql, parameters,
                    function(tx,res){
                        debug("sync data res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("inside calling FDS")
                        syncObj.getFDSReqParams(Params, res).then(function(docData){
                            CommonService.ajaxCall(DOCUMENT_UPLOAD_URL,AJAX_TYPE,TYPE_JSON,docData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                                                debug("FF HTML success: "+JSON.stringify(ajaxResponse));
                                                                //dfd.resolve(syncObj.updateDocTableData(ajaxResponse));
                                                                syncObj.updateDocTableData(ajaxResponse).then(
                                                                    function(res){
                                                                        if(res)
                                                                            {
                                                                                debug("FF docupload success");
                                                                                syncObj.syncFFNAFile(Params);
                                                                            }
                                                                        else
                                                                            {
																				CommonService.hideLoading();
																				navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
																		}
                                                                    }
                                                                )
                                                            },function(data, status, headers, config, statusText){
                                                                debug("FF HTML fail:"+JSON.stringify(data));
																	 CommonService.hideLoading();
																	 navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                                            });

                        });

                        }
                        else
                            {
                                syncObj.syncFFNAFile(Params);//syncObj.callSISSync(Params);
                            }
                    },
                    function(tx,err){
                        navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                         CommonService.hideLoading();
                    }
                );
            },
            function(err){
                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                 CommonService.hideLoading();
            },null
    );

}


this.syncFFNAFile = function(Params){

var imagePath = "/FromClient/FF/HTML/";
var sql = "Select * from LP_DOCUMENT_UPLOAD where DOC_CAT_ID=? AND AGENT_CD = ? AND DOC_CAT = ? AND DOC_NAME like '%.html'"
var params = [Params.FHR_ID , Params.AGENT_CD, "FHR"];
CommonService.transaction(db, function(tx){
                                CommonService.executeSql(tx, sql, params, function(tx,res){
                                debug("FF :: "+res.rows.length);
                                            if(!!res && res.rows.length!=0 && res.rows.item(0).IS_FILE_SYNCED != 'Y')
                                                {
                                                    syncObj.sendFileData(Params, res.rows.item(0), imagePath, "FFN");
                                                }

            					}, function(tx, err){console.log("Error 1");});
            					},function(err){});



}

this.updateAPPSISShareFlag =  function(sisId,agentCd){
    var dfd = $q.defer();
    var query = "update lp_document_upload set is_data_synced = ? WHERE DOC_CAT_ID = ? AND DOC_CAT =? AND AGENT_CD = ? AND DOC_NAME NOT LIKE 'TRM%'";//1714
    var parameters = ["Y",sisId,"SIS",agentCd];

    CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx, query, parameters,
                    function(tx,res){
                        debug("update SIS Share Flag");
                        debug(res);
                        dfd.resolve(res);

                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            }
        );
    return dfd.promise;
    }


this.callSISSync = function(Params){
        SisFormService.syncDocumentUpload(Params.AGENT_CD, Params.SIS_ID, null, true, Params.POLICY_NO).then(
                        function(sisRes){
							if(!!sisRes && sisRes=="S"){
							    //CommonService.updateRecords(db, 'lp_document_upload', {'is_data_synced': 'Y'}, {'doc_cat_id': Params.SIS_ID, 'doc_cat': 'SIS', 'agent_cd': Params.AGENT_CD})
								syncObj.updateAPPSISShareFlag(Params.SIS_ID,Params.AGENT_CD).then(
										function(sisRes){
											debug("call next service ");
                      SisFormService.syncSISSignature(Params.AGENT_CD, Params.SIS_ID, null, true).then(
                          function(sisSignRes){
                              if(!!sisSignRes){
                                    CommonService.updateRecords(db, "lp_document_upload", {"is_file_synced": "Y"}, {"agent_cd": Params.AGENT_CD, "doc_cat": "SIS", "doc_cat_id": Params.SIS_ID, "doc_name": Params.AGENT_CD+"_"+Params.SIS_ID+"_C02.jpg"}).then(
                                      function(updtRes){
                                            SisFormService.syncSISReport(Params.AGENT_CD, Params.SIS_ID, null, true).then(
                                                function(sisFileRes){
                                                    if(!!sisFileRes && sisFileRes == "S"){
                                                        SisFormService.updateSISDataShareFlag(Params.SIS_ID, Params.AGENT_CD).then(
                                                            function(res){
                                                               //call the singleSISShare when the combo is present 1788
                                                                syncObj.getMyOpportunityData().then(function(MyOpportunityData){

                                                               if(!!MyOpportunityData && !!MyOpportunityData.COMBO_ID){
                                                                    debug("updateSISDataShareFlag is "+JSON.stringify(MyOpportunityData));
                                                                    debug("syncObj.APP_ID "+JSON.stringify(syncObj.APP_ID));

                                                                    var SIS_ID = MyOpportunityData.SIS_ID;
                                                                    var REF_SIS_ID = MyOpportunityData.REF_SIS_ID;
                                                                    /*SisFormService.shareSingleSIS(Params.AGENT_CD, SIS_ID, REF_SIS_ID,null, null,true).then(
                                                                        function(shareSISRes){
                                                                            debug("share single sis responce  "+JSON.stringify(shareSISRes) );
                                                                            if(!!shareSISRes && shareSISRes=="S"){
                                                                                debug("success in SIS File Sync");*/
                                                                                CommonService.showLoading("Policy submission in Progress...\n Please wait..");
                                                                                syncObj.syncAppTableData(Params);
                                                                            /*}else{
                                                                                  debug("failure in SIS File Sync");
                                                                            }

                                                                        }
                                                                    );*/

                                                                    }else{
                                                                        debug("success in SIS File Sync");
                                                                        CommonService.showLoading("Policy submission in Progress...\n Please wait..");
                                                                        syncObj.syncAppTableData(Params);
                                                                    }
                                                                });
                                                                //2

                                                            }
                                                        );
                                                    }
                                                    else
                                                        {
                                                        	navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                                        	 CommonService.hideLoading();
                                                        }
                                                }
                                            );
                                          }
                                        );
                                      }
                                      else{
              				navigator.notification.alert(REQ_FAIL_MSG, null, "Application Sync", "OK");
					CommonService.hideLoading();
			             }
                                  }
                              );

									});
							}
							else
								{
								//alert("Request could not be synced.\n Try again later.");
								syncObj.syncAppTableData(Params);
								}
						}
        );

}

this.syncAppTableData = function(Params){

            var dfd = $q.defer();
            var sql = "select DOC_ID AS DOCID,DOC_CAT AS CAT,DOC_CAT_ID AS CATID, POLICY_NO AS POLNO,DOC_NAME AS DOCNM, DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT = ? and DOC_CAT_ID = ? and (IS_DATA_SYNCED = ? or IS_DATA_SYNCED is null) AND DOC_NAME like '%.html'"
            var parameters = [Params.AGENT_CD,"APP",Params.APPLICATION_ID, 'N'];

            CommonService.transaction(db,
                    function(tx){
                        CommonService.executeSql(tx, sql, parameters,
                            function(tx,rSet){
                                debug("App table res.rows.length: " + rSet.rows.length);
                                if(!!rSet && rSet.rows.length>0){
                                debug("inside calling APP FDS")
                                syncObj.getFDSReqParams(Params, rSet).then(function(docData){
                                    CommonService.ajaxCall(DOCUMENT_UPLOAD_URL,AJAX_TYPE,TYPE_JSON,docData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                                                        debug("App HTML success: "+JSON.stringify(ajaxResponse));
                                                                        syncObj.updateDocTableData(ajaxResponse).then(
                                                                            function(res){
                                                                                if(res)
                                                                                    syncObj.syncAppFile(Params);
                                                                            }
                                                                        );

                                                                    },function(data, status, headers, config, statusText){
                                                                        debug("APP HTML fail:"+JSON.stringify(data));
                                                                        dfd.resolve(null);
                                                                         CommonService.hideLoading();
                                                                    });

                                });
                                }
                                else
                                    {
                                         debug("Table already synced ... now sending file.")
                                             syncObj.syncAppFile(Params);
                                            debug("inside else :APP")
                                    }
                            },
                            function(tx,err){
                                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                 CommonService.hideLoading();
                            }
                        );
                    },
                    function(err){
                        navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                         CommonService.hideLoading();
                    },null);

        };


this.updateDocTableData = function(resp){
var dfd = $q.defer();
        var sql = "update LP_DOCUMENT_UPLOAD set IS_DATA_SYNCED='Y' where DOC_CAT_ID=? AND DOC_ID = ?";
                var data = resp.RS.DOCUPLOAD[0];
                var flag = data.RESP;
                if(flag === "S"){
                    var params = [data.CATID,data.DOCID];
                            CommonService.transaction(db,
                            function(tx){
                                    CommonService.executeSql(tx,sql,params,
                                        function(tx,res){
                                            debug("APP HTML update res.rows.length: " + res.rows.length);
                                            if(!!res){
                                                debug("updated success ");
                                                dfd.resolve(true);
                                            }
                                            else{
                                                debug("updated failed ");
                                                dfd.resolve(false);
                                            }
                                        },
                                        function(tx,err){
                                            dfd.resolve(false);
                                        }
                                    );
                                },
                                function(err){
                                    dfd.resolve(false);
                                },null
                            );
                }else{
                    debug("failed to update for doc id "+data.DOC_ID);
                    //dfd.resolve(false);
                     CommonService.hideLoading();
                    navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                }


        return dfd.promise;
    };

this.syncAppFile = function(Params){
var imagePath = "/FromClient/APP/HTML/";
var sql = "Select * from LP_DOCUMENT_UPLOAD where DOC_CAT_ID=? AND AGENT_CD = ? AND DOC_CAT = ? AND DOC_NAME like '%.html'"
var params = [Params.APPLICATION_ID, Params.AGENT_CD, "APP"];
CommonService.transaction(db, function(tx){
                                CommonService.executeSql(tx, sql, params, function(tx,res){
                                debug(Params.APPLICATION_ID+"syncAppFile APP :: "+res.rows.length);
                                            if(!!res && res.rows.length!=0 && res.rows.item(0).IS_FILE_SYNCED != 'Y')
                                                {
                                                    syncObj.sendFileData(Params, res.rows.item(0), imagePath, "APP");
                                                }
                                            else if(!!res && res.rows.length!=0 && res.rows.item(0).IS_FILE_SYNCED == 'Y')
                                                {
                                                 if(!!syncObj.ComboParams)
                                                 {
                                                 	if(syncObj.ComboParams.COMBO_FLAG == 'T')
                                                 	{
                                                 		syncObj.ComboParams.COMBO_FLAG = 'P';
                                                        syncObj.startFinalSubmission(syncObj.ComboParams,syncObj.ComboParams);
                                                 	}
                                                 	else
                                                 	navigator.notification.alert("Policy already submitted",function(){
													   $state.go("dashboard.home");
													  },"Application","OK");

                                                 }
                                                 else{
													if(BUSINESS_TYPE!='IndusSolution'){
														syncObj.updateOnSubmit(Params.APPLICATION_ID);
													}
                                                 	navigator.notification.alert("Policy already submitted",function(){
													   $state.go("dashboard.home");
													  },"Application","OK");
												}
												}


            					}, function(tx, err){console.log("Error 1");
            					 CommonService.hideLoading();
            					});
            					},function(err){});



}

this.sendFileData = function(Params, fileData, imagePath, flag){

                    var options = new FileUploadOptions();
                    options.fileKey = "file";
                    options.mimeType = "application/octet-stream";
                    options.headers={'contentType':'multipart/form-data'};
                    options.httpMethod = "POST";


                    var params = {};
                    options.fileName = fileData.DOC_NAME;

                    params.AC = Params.AGENT_CD;
                    params.DVID = localStorage.DVID;
                    params.PWD = LoginService.lgnSrvObj.password;
                    params.ACN = "SCFD";
                    params.CATID = fileData.DOC_CAT_ID;
                    params.CAT = fileData.DOC_CAT;
                    params.DOCID = fileData.DOC_ID;
                    params.ISASUB = null;
                    if(flag == "APP"){
                        params.ISASUB = 'Y';
                        if(!!Params.termISASUB && Params.termISASUB=="T")
                            params.ISASUB = 'T';
                        params.VERNO = VERSION_NO;
                    }
                    if(flag == 'CCR')
					{
						 params.DOCCID = fileData.DOC_CAP_ID;
						 params.DOCRID = fileData.DOC_CAP_REF_ID;
					}
                    params.FILETYPE = "HTML";
                    params.COMBO = null;
                    debug("SYNC params: "+JSON.stringify(params));
                    options.params = params;

                    CommonService.getTempFileURI(imagePath, fileData.DOC_NAME).then(
                                        function(file){
                                            debug("OPTIONS is >>"+JSON.stringify(options));
                                            debug(file+"OPTIONS is >>"+imagePath);
                                            if(!!file)
                                               syncObj.uploadFile(Params, file, HTML_SYNC_URL,options, flag);
                                        }
                                    );


}
this.sendAgentRelatedDataCCR = function(Params, res, imagePath, flag){
        //8888
        var docFileData = [];
        for(var i=0;i<res.rows.length;i++){

            var fileData = res.rows.item(i);
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.mimeType = "image/jpeg";
            options.headers={'contentType':'multipart/form-data'};
            options.httpMethod = "POST";
            var params = {};
            options.fileName = fileData.DOC_NAME;
            params.AC = Params.AGENT_CD;
            params.DVID = localStorage.DVID;
            params.PWD = LoginService.lgnSrvObj.password;
            params.ACN = "SID";
            params.FRM = fileData.DOC_CAT_ID;
            params.POL = fileData.POLICY_NO;
            params.DOC = fileData.DOC_ID;
            params.PAG = fileData.DOC_PAGE_NO;
            params.TST = fileData.DOC_TIMESTAMP;
            params.DOC_NAME = fileData.DOC_NAME;

            if(flag == "APP")
                params.ISASUB = 'Y';
            if(flag == 'CCR')
            {
                 params.DOCCID = fileData.DOC_CAP_ID;
                 params.DOCRID = fileData.DOC_CAP_REF_ID;
            }

            debug("SYNC params: "+JSON.stringify(params));
            options.params = params;
            docFileData.push(syncObj.uploadJPGImageFileData(Params.AGENT_CD, fileData.DOC_ID, LoginService.lgnSrvObj.password,localStorage.DVID,imagePath,options,fileData.DOC_NAME,flag,Params));

          /*  CommonService.getTempNormalFileURI(imagePath, fileData.DOC_NAME).then(
                function(file){
                    debug("OPTIONS is >>"+JSON.stringify(options));
                    debug(file+"OPTIONS is >>"+imagePath);
                    if(!!file)
                       //docFileData.push(syncObj.uploadFile(Params, file, DOCUMENT_FILE_UPLOAD_URL,options, flag));
                        docFileData.push(syncObj.uploadJPGImageFileData(Params.AGENT_CD, fileData.DOC_ID, LoginService.lgnSrvObj.password,localStorage.DVID,imagePath,options,fileData.DOC_NAME,flag,Params));
                }
            );*/

        }

        return $q.all(docFileData);
}

this.uploadJPGImageFileData = function(AGENT_CD,APP_ID,PWD,DVID,imagePath,options,name,flag,Params){
        var dfd = $q.defer();
        debug("flag is -----> "+flag);
        function sendFile(file,options){
            var innerDfd = $q.defer();
            if(flag == "HTML"){
                debug("INSIDE HTML");
                syncObj.uploadMultipleFiles(file,HTML_SYNC_URL,options,flag).then(
                    function(uploaded){
                        debug("uploaded");
						//changes 27-10-2016
                        innerDfd.resolve(uploaded);
                    }
                );
            }
            else{
                debug("INSIDE IMAGE");
                syncObj.uploadMultipleFiles(file,DOCUMENT_FILE_UPLOAD_URL,options,flag).then(
                    function(uploaded){
                        debug("uploaded");
						//changes 27-10-2016
                        innerDfd.resolve(uploaded);
                    }
                );
            }
            return innerDfd.promise;
        }

        if(flag == "HTML"){
            //servlet call the decrypted url...
            CommonService.getTempFileURI(imagePath, name).then(
                function(file){
                    debug("OPTIONS is >>"+JSON.stringify(options));
                    sendFile(file,options).then(
                        function(resp){
                            dfd.resolve(resp);
                        }
                    );
                }
            );
        }else{
            CommonService.getTempNormalFileURI(imagePath, name).then(
                function(file){
                    debug("OPTIONS is CCR >>"+JSON.stringify(options));
                    sendFile(file,options).then(
                        function(resp){
                            dfd.resolve(resp);
                        }
                    );
                }
            );
        }

        return dfd.promise;
    };

//HTTP url code for file upload...
    this.uploadMultipleFiles = function(file,name,options,flag){
        var dfd = $q.defer();

        var ft = new FileTransfer();
        debug("file: " + file);
        debug("SERVLET TO CALL IS "+name);

        ft.upload(file, name,function(succ){
            debug("Code = " + succ.responseCode);
            debug("Response = " + succ.response);
            debug("Sent = " + succ.bytesSent);
            if(!!succ.response && succ.response != "{}"){
                if(JSON.parse(succ.response).RS.RESP==="S"){
                    debug("file uploaded successfull CCR Image");
                    syncObj.updateSyncFileData(JSON.parse(succ.response),options,flag).then(
                        function(insert){
                            dfd.resolve(insert);
                        }
                    );
                }
                else{
                    dfd.resolve(false);
                }
            }else{
                dfd.resolve(false);
            }
        },
        function(err){
            debug("FT upload error: " + JSON.stringify(err));
            dfd.resolve(false);
            CommonService.hideLoading();
        },options);

        return dfd.promise;
    };

        //common query for insertion...
        this.innerUpdateData = function(query,params){
            var dfd = $q.defer();
            CommonService.transaction(db,
            function(tx){
                    CommonService.executeSql(tx,query,params,
                        function(tx,res){
                            debug("update res.rows.length: " + res.rows.length);
                            if(!!res){
                                debug("updated success ");
                                dfd.resolve(true);
                            }
                            else{
                                debug("updated failed ");
                                dfd.resolve(false);
                            }
                        },
                        function(tx,err){
                            dfd.resolve(false);
                            CommonService.hideLoading();
                        }
                    );
                },
                function(err){
                    dfd.resolve(false);
                },null
            );
            return dfd.promise;
        };

 this.updateSyncFileData = function(resp,data,flagcheck){
        debug("<<<<<< UPDATION FILE SYNC STARTED CCR >>>>>>");
        var dfd = $q.defer();
        var query = "UPDATE LP_DOCUMENT_UPLOAD set IS_FILE_SYNCED='Y' where DOC_CAT_ID=? AND DOC_CAT =? AND DOC_ID = ?";
        var response = resp.RS;
        debug("response to parse >>>"+JSON.stringify(response));
        debug("OPTIONS PARAMS >>>"+JSON.stringify(data));
        if(!!response){
            var flag = response.RESP;
            var param = data.params;
            if(flag === "S"){
                var parametes = [];
                if(flagcheck == "HTML"){
                    parameters = [param.CATID,"FORM",param.DOCID];
                }else{
                    parameters = [param.FRM,"FORM",param.DOC];
                }
                debug("parameters >>> "+JSON.stringify(parameters));
                syncObj.innerUpdateData(query,parameters).then(
                    function(inserted){
                        debug("data inserted");
                        dfd.resolve(true);
                    }
                );
            }else{
                debug("failed to update for doc id "+data.DOC_ID);
                dfd.resolve(false);
                CommonService.hideLoading();
            }
        }
        return dfd.promise;
    };


this.sendImageData = function(Params, res, i, imagePath, flag){
                    var fileData = res.rows.item(i);
                    var options = new FileUploadOptions();
                    options.fileKey = "file";
                    options.mimeType = "image/jpeg";
                    options.headers={'contentType':'multipart/form-data'};
                    options.httpMethod = "POST";
                    var params = {};
                    options.fileName = fileData.DOC_NAME;
                    params.AC = Params.AGENT_CD;
                    params.DVID = localStorage.DVID;
                    params.PWD = LoginService.lgnSrvObj.password;
                    params.ACN = "SID";
                  	params.FRM = fileData.DOC_CAT_ID;
				   	params.POL = fileData.POLICY_NO;
				   	params.DOC = fileData.DOC_ID;
				   	params.PAG = fileData.DOC_PAGE_NO;
				  	params.TST = fileData.DOC_TIMESTAMP;
				  	params.DOC_NAME = fileData.DOC_NAME;
				  	params.DOCCID = fileData.DOC_CAP_ID;
				  	params.DOCRID = fileData.DOC_CAP_REF_ID;

                    debug("SYNC params: "+JSON.stringify(params));
                    options.params = params;
					function getTempFileURISuccess (file){
						debug("OPTIONS is >>"+JSON.stringify(options));
						debug(file+"OPTIONS is >>"+imagePath);
						if(!!file)
						   syncObj.uploadImageData(Params, file, DOCUMENT_FILE_UPLOAD_URL,options, flag).then(
								function(data){
									if(data == undefined || data == false)
										{
											isError = true;
											navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
											 CommonService.hideLoading();
										}
									else
										{
											if(i<res.rows.length-1)
											{ 	i++;
												if(res.rows.item(i).IS_FILE_SYNCED != 'Y')
												{	if(res.rows.item(i).DOC_CAT == 'PHOTOGRAPH')
													{ var imagePath = "/FromClient/APP/IMAGES/PHOTOS/";
														syncObj.sendImageData(Params, res, i, imagePath, true);
													}
													else
													{
														var imagePath = "/FromClient/APP/SIGNATURE/";
														syncObj.sendImageData(Params, res, i, imagePath, true);
													}
												}
											}
											else
											{
												if(BUSINESS_TYPE=='IndusSolution'){
													syncObj.syncFFNATableData(Params);
												}
												else {
														if(Params.RECO_DEV_FLAG == 'Y' || Params.RECO_DEV_FLAG == 'N')
															syncObj.syncFFHRFile(Params).then(function(res) {
																	if(!!res)
																		syncObj.callSISSync(Params);
																	else {
																		navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
																		 CommonService.hideLoading();
																	}
															});
														else
															syncObj.callSISSync(Params);
												}
											}
										}
								}
						   );
					}
					if(fileData.DOC_CAT=="PHOTOGRAPH"){
						CommonService.getTempNormalFileURI(imagePath, fileData.DOC_NAME).then(
                                        function(file){
											getTempFileURISuccess(file);
										}
                                    );
					}
					else{
						CommonService.getTempFileURI(imagePath, fileData.DOC_NAME).then(
                                        function(file){
											getTempFileURISuccess(file);
										}
                                    );
					}


}

this.syncFFHRFile = function(Params){
	var dfd = $q.defer();

	FhrService.syncDocumentUpload(Params.AGENT_CD, Params.FHR_ID, null, true).then(function(syncFhrResp){
		if (!!syncFhrResp && syncFhrResp == "S") {
            CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD', { 'is_data_synced': 'Y' }, { 'doc_cat_id': Params.FHR_ID, 'agent_cd': Params.AGENT_CD });
			FhrService.syncFHRReport(Params.AGENT_CD, Params.FHR_ID, null, true).then(function(syncFhrReportResp){
				if(!!syncFhrReportResp && syncFhrReportResp == "S")
				{
					CommonService.updateRecords(db, 'lp_document_upload', { 'is_file_synced': 'Y' }, { 'doc_cat_id': Params.FHR_ID, 'doc_cat': 'FHR', 'agent_cd': Params.AGENT_CD, 'doc_name': Params.FHR_ID + ".html" }).then(
						function(res) {
                            if(Params.RECO_DEV_FLAG == 'Y'){
                                // Sync FHR Deviation Details data
                                FhrService.syncFHRDeviationDetails(Params.AGENT_CD, Params.FHR_ID).then(function(fhrDevDataResp){
                                    if (!!fhrDevDataResp && fhrDevDataResp == "S") {

                                        // Sync FHR Deviation Document
                                        FhrService.syncFHRDeviationDoc(Params.AGENT_CD, Params.FHR_ID, null).then(function(fhrDevResp){
                                            if (!!fhrDevResp && fhrDevResp == "S"){
                                                FhrService.getDocId("PR", FhrService.PDMPROOF_CODE).then(function(docId) {
                                                    CommonService.updateRecords(db, 'lp_document_upload', { 'is_file_synced': 'Y' }, { 'doc_cat_id': Params.FHR_ID, 'doc_cat': 'DEV', 'agent_cd': Params.AGENT_CD, 'doc_name': "FHR_"+ Params.FHR_ID + "_"+docId+".html" }).then(function(res){
                                                            dfd.resolve(true);
                                                    });
                                                });
                                            }else{
                                                dfd.resolve(null);
                                                CommonService.hideLoading();
                                                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                            }
                                        });

                                    }else{
                                        dfd.resolve(null);
                                        CommonService.hideLoading();
                                        navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                                    }
                                });

                            }else
							    dfd.resolve(true);
						}
					);
				}
				else{
					dfd.resolve(null);
					CommonService.hideLoading();
					navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
				}
			});
		}
		else{
			dfd.resolve(null);
			CommonService.hideLoading();
			navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
		}

	});
	return dfd.promise;
}

this.uploadImageData = function(Params, file, URL, options, flag){

  var dfd = $q.defer();

            var ft = new FileTransfer();

            ft.upload(file, URL,function(succ){
                debug("Code = " + succ.responseCode);
                debug("Response = " + succ.response);
                debug("Sent = " + succ.bytesSent);
                if(!!succ.response && succ.response != "{}"){
                    if(JSON.parse(succ.response).RS.RESP==="S"){
                    var data = JSON.parse(succ.response).RS;

                       CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'DOC_ID': data.DOCID, 'agent_cd': Params.AGENT_CD}).then(
                                               function(res){
                                                   debug("success in Image Sync:"+flag);
                                                   dfd.resolve(true);
                                               }
                                           );
                    }
                    else{
                        dfd.resolve(false);
                    }
                }else{
                    dfd.resolve(false);
                }
            },
            function(err){
                debug("FT upload error: " + JSON.stringify(err));
                 CommonService.hideLoading();
                dfd.resolve(false);
            },options);

            return dfd.promise;
}

this.uploadFile = function(Params, file,URL,options, flag){
            var dfd = $q.defer();

            var ft = new FileTransfer();

            ft.upload(file, URL,function(succ){
                debug("Code = " + succ.responseCode);
                debug("Response = " + succ.response);
                debug("Sent = " + succ.bytesSent);
                if(!!succ.response && succ.response != "{}"){
                    if(JSON.parse(succ.response).RS.RESP==="S"){
                    var data = JSON.parse(succ.response).RS;

                       CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'DOC_ID': data.DOCID, 'agent_cd': Params.AGENT_CD}).then(
                                               function(res){
                                                   debug("success in APP File Sync"+flag);
                                                   if(!!flag && flag == "APP")
                                                    syncObj.syncAppEmail(Params);
                                                   else if(flag == "FFN")
                                                    syncObj.callSISSync(Params);
                                                   else if(flag == "CCR")
													syncObj.syncProfileSignTableData(Params);
                                               }
                                           );
                    }
                    else{
                        dfd.resolve(false);
                        navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                        CommonService.hideLoading();
                    }
                }else{
                    dfd.resolve(false);
                    navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                    CommonService.hideLoading();
                }
            },
            function(err){
                debug("FT upload error: " + JSON.stringify(err));
                navigator.notification.alert(REQ_FAIL_MSG,null ,"Application Sync","OK");
                 CommonService.hideLoading();
                dfd.resolve(false);
            },options);

            return dfd.promise;
};

this.syncAppEmail = function(Params){

var sql = "SELECT EMAIL_ID AS EML,EMAIL_CAT AS TYP,EMAIL_CAT_ID AS ID,EMAIL_TO,EMAIL_CC AS CC,EMAIL_SUBJECT AS SUB,EMAIL_BODY AS BDY,EMAIL_TIMESTAMP AS TMP FROM LP_EMAIL_USER_DTLS WHERE EMAIL_CAT=? AND EMAIL_CAT_ID=? AND ISSYNCED=?";
var params = ["APP",Params.APPLICATION_ID,"N"];


CommonService.transaction(db,function(tx){
                    CommonService.executeSql(tx,sql,params,
                        function(tx,res){
                            debug("APP Eml res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length!=0){
                            syncObj.EML_ID = res.rows.item(0).EML;
                                syncObj.getAppEmailReq(Params, res).then(
                                function(emlData){
                                CommonService.ajaxCall(EMAIL_SYNC_URL,AJAX_TYPE,TYPE_JSON,emlData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                        debug("App HTML EMAIL success: "+JSON.stringify(ajaxResponse));
                                /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN', {'APP_SUBMITTED_FLAG':'Y', 'COMPLETION_TIMESTAMP': CommonService.getCurrDate()}, {'APPLICATION_ID': Params.APPLICATION_ID, 'AGENT_CD': Params.AGENT_CD}).then(
                                                                               function(res){*/

                                                                               if(!!syncObj.ComboParams)
																				{
																					if(syncObj.ComboParams.COMBO_FLAG == 'T')
																					{
																					    syncObj.ComboParams.COMBO_FLAG = 'P';
																						syncObj.startFinalSubmission(syncObj.ComboParams,syncObj.ComboParams);
																					}
																					else
																					{	syncObj.updateApplicationMain(Params.BASE_APP_ID, Params.AGENT_CD).then(function(){

																							syncObj.updateApplicationMain(Params.APPLICATION_ID, Params.AGENT_CD).then(function(){

																								navigator.notification.alert("Policy Submitted successfully",function(){
																									CommonService.hideLoading();
																									$state.go("dashboard.home");
																								  },"Application","OK");

																							   debug("update eml Sync");
																							});
																						});
																					}

																				}
																				else
																				{
																						syncObj.updateApplicationMain(Params.APPLICATION_ID, Params.AGENT_CD).then(function(){
																							if(BUSINESS_TYPE!='IndusSolution')
																								syncObj.updateOnSubmit(Params.APPLICATION_ID);
																							navigator.notification.alert("Policy Submitted successfully",function(){
																								CommonService.hideLoading();
																								$state.go("dashboard.home");
																							  },"Application","OK");

																						   debug("update eml Sync");
																						});

                                                                                }
                                                                              /* }
                                                                           );*/
                                CommonService.updateRecords(db, 'LP_EMAIL_USER_DTLS', {'ISSYNCED': 'Y'}, {'EMAIL_ID': syncObj.EML_ID, 'AGENT_CD': Params.AGENT_CD}).then(
                                                                               function(res){
                                                                                   debug("update eml Sync");
                                                                               }
                                                                           );


                                    },function(data, status, headers, config, statusText){
                                        debug("APP HTML EMAIL fail:"+JSON.stringify(data));
                                        navigator.notification.alert(REQ_FAIL_MSG,null,"Application Sync","OK");
                                         CommonService.hideLoading();

                                    });

                                }
                                );
                            }
                            else{
                            /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN', {'APP_SUBMITTED_FLAG':'Y', 'COMPLETION_TIMESTAMP': CommonService.getCurrDate()}, {'APPLICATION_ID': Params.APPLICATION_ID, 'AGENT_CD': Params.AGENT_CD}).then(
								   function(res){*/

									if(!!syncObj.ComboParams)
									{
										if(syncObj.ComboParams.COMBO_FLAG == 'T')
										{
                                            syncObj.ComboParams.COMBO_FLAG = 'P';
                                            syncObj.startFinalSubmission(syncObj.ComboParams,syncObj.ComboParams);
										}
										else
										{	syncObj.updateApplicationMain(Params.BASE_APP_ID, Params.AGENT_CD).then(function(){

												syncObj.updateApplicationMain(Params.APPLICATION_ID, Params.AGENT_CD).then(function(){

													navigator.notification.alert("Policy Submitted successfully",function(){
														CommonService.hideLoading();
														$state.go("dashboard.home");
													  },"Application","OK");

												   debug("update eml Sync");
												});
											});
										}

									}
									else
									{

											syncObj.updateApplicationMain(Params.APPLICATION_ID, Params.AGENT_CD).then(function(){
												if(BUSINESS_TYPE!='IndusSolution')
													syncObj.updateOnSubmit(Params.APPLICATION_ID);
												navigator.notification.alert("Policy Submitted successfully",function(){
													CommonService.hideLoading();
													$state.go("dashboard.home");
												  },"Application","OK");

											   debug("update eml Sync");
											});

									}
								   /*}
							   );*/

                            }
                        },
                        function(tx,err){

                        }
                    );
                    },
                function(err){},null);

}

this.updateOnSubmit = function(APPID){
	var dfd = $q.defer();
    var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    debug("update Lead for APPID"+APPID);
	var query = "update LP_MYLEAD set LEAD_STATUS_CD='191',LEAD_SUB_STATUS_CD='226' where IPAD_LEAD_ID IN(select LEAD_ID from LP_MYOPPORTUNITY where APPLICATION_ID=? AND AGENT_CD=?)";
	var parameterList = [APPID, AGENT_CD];
	CommonService.transaction(db,function(tx){
									CommonService.executeSql(tx, query, parameterList,
										function(tx,res){
											debug("Lead updated successfully :"+APPID);
											dfd.resolve(true);
										},function(err){
											dfd.resolve(null);
										})},function(err){
											dfd.resolve(null);
										});
	return dfd.promise;
};

this.updateApplicationMain = function(APP_ID, AGENT_CD){
var dfd = $q.defer();
CommonService.updateRecords(db, 'LP_APPLICATION_MAIN', {'APP_SUBMITTED_FLAG':'Y', 'COMPLETION_TIMESTAMP': CommonService.getCurrDate()}, {'APPLICATION_ID': APP_ID, 'AGENT_CD': AGENT_CD}).then(
			   function(res){
			   	dfd.resolve(true);
			   }
		   );
	return dfd.promise;

}

this.getAppEmailReq = function(Params, results){
var dfd = $q.defer();
        var emlData = new Object();
         var request = new Object();
         request.AC = Params.AGENT_CD;
         request.PWD = LoginService.lgnSrvObj.password;
         request.DVID = localStorage.DVID;
         request.ACN = "SE";
         request.SEDET = [];
         emlData.REQ = request;

         if(results.rows.item(0)!=undefined){
             emlData.REQ.SEDET.push(results.rows.item(0));
             emlData.REQ.SEDET[0].TO = results.rows.item(0).EMAIL_TO;
             delete emlData.REQ.SEDET[0].EMAIL_TO;
             dfd.resolve(emlData);
         }

return dfd.promise;

}

this.udpateLeadStatus = function(){
	var dfd = $q.defer();
	if(BUSINESS_TYPE!='IndusSolution'){
		CommonService.updateRecords(db, "lp_mylead", {"lead_status_cd": "191", "lead_sub_status_cd": "226"}, {"ipad_lead_id": syncObj.LEAD_ID}).then(
			function(){
				dfd.resolve("S");
			}
		);
	}
	else{
		dfd.resolve("S");
	}
	return dfd.promise;
};

this.getExceptionalApprovalData = function(AGENT_CD, SIS_ID){
    var dfd = $q.defer();
    var whereClauseObj={};
    whereClauseObj.AGENT_CD = AGENT_CD;
    whereClauseObj.SIS_ID = SIS_ID;
    debug("whereClauseObj : " + JSON.stringify(whereClauseObj));
    CommonService.selectRecords(db,"LP_TERM_VALIDATION",false,null,whereClauseObj).then(
        function(res){
            if(res.rows.length>0){
                if(!!res.rows.item(0).ELIGIBILITY_FLAG && res.rows.item(0).ELIGIBILITY_FLAG=='N' && !!res.rows.item(0).ELIGIBILITY_JUSTIFICATION){
                    //debug("LP_TERM_VALIDATION : " + JSON.stringify(res.rows.item(0)));
                    dfd.resolve("T");
                }
                else{
                    debug("term eligible")
                    dfd.resolve("Y");
                }
            }else {
                debug("term eligible not applicable");
                dfd.resolve("NA");
            }
        }
    );

	return dfd.promise;
};

}]);
