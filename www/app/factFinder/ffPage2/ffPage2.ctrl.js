ffaModule2.controller('ffPage2Ctrl',['$state','SavePage2Data','factFinderService','$ionicHistory', 'CommonService', 'FFTimelineService', function($state,SavePage2Data,factFinderService,$ionicHistory, CommonService, FFTimelineService){
    "use strict";
	CommonService.hideLoading();
                                     
    /* Header Hight Calculator */
    setTimeout(function(){
               var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
               console.log("getHeight Test : " + factFinderAnalysisCalHeight);
               $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    });
    /* End Header Hight Calculator */

                                     
    debug("date from previous page is "+JSON.stringify(factFinderService.getFHRPage1Data()));
    debug("date from previous page is "+JSON.stringify(factFinderService.getFHRPage2Data()));
    FFTimelineService.setActiveTab("needId");
    var self = this;
    this.click = false;
    var page2Data=factFinderService.getFHRPage2Data();
	this.rdoccupation=page2Data.rdoccupation;
    this.rdbusiness=page2Data.rdbusiness;
    this.occOther=page2Data.bussinessOther == null ? "" :page2Data.bussinessOther;
    this.occOther11=page2Data.occupationOther == null ? "" :page2Data.occupationOther;
    this.AnnualIncome=page2Data.bussinessOther == null ? "" :parseInt(page2Data.AnnualIncome);
    this.investDetail1 =page2Data.investDetail1 == null ? "" :parseInt(page2Data.investDetail1);
    this.investDetail2 =page2Data.investDetail2 == null ? "" :parseInt(page2Data.investDetail2);
    this.investDetail3 =page2Data.investDetail3== null ? "" :parseInt(page2Data.investDetail3);
    this.investDetail4 =page2Data.investDetail4== null ? "" :parseInt(page2Data.investDetail4);
    this.firstSelect=page2Data.firstSelect;
    this.secondSelect=page2Data.secondSelect;
    this.thirdSelect=page2Data.thirdSelect;
    this.forthSelect=page2Data.forthSelect;
    this.Backbtn2 = function(){
       $state.go(FACT_FINDER_STATE);
     };
	 this.openMenu = function(){
 		CommonService.openMenu();
 	};
 	this.OccupationChanged=function(){
           this.occOther="";
    }
    this.bussinessChanged=function(){
           this.occOther11="";
        }
     this.check1 = function(){

            var firstSelectedValue=this.firstSelect;
            var secondSelectedValue=this.secondSelect;
            var thirdSelectedValue=this.thirdSelect;
            var forthSelectedValue=this.forthSelect;
            if(firstSelectedValue ==secondSelectedValue || firstSelectedValue ==thirdSelectedValue || firstSelectedValue ==forthSelectedValue)
            {
									 navigator.notification.alert("This Rank is already selected1!!",function completed(){
																  debug("second value set to blank ::: "+page2Data.secondSelect);
																  self.firstSelect="";
																  document.getElementById("firstSelect").selectedIndex = "0";
																  },
																  "Fact Finder","OK");
;
									 
            }
        };
       this.check2 = function(){
            var firstSelectedValue=this.firstSelect;
            var secondSelectedValue=this.secondSelect;
            var thirdSelectedValue=this.thirdSelect;
            var forthSelectedValue=this.forthSelect;
            if(secondSelectedValue ==firstSelectedValue || secondSelectedValue ==thirdSelectedValue || secondSelectedValue ==forthSelectedValue)
            {
                navigator.notification.alert("This Rank is already selected!!",function completed(){
                    debug("second value set to blank ::: "+page2Data.secondSelect);
                    self.secondSelect="";
					document.getElementById("secondSelect").selectedIndex = "0";
                },
                "Fact Finder","OK");
            }
        };
                                     
        
        this.check3 = function(){
            var firstSelectedValue=this.firstSelect;
            var secondSelectedValue=this.secondSelect;
            var thirdSelectedValue=this.thirdSelect;
            var forthSelectedValue=this.forthSelect;
            if(thirdSelectedValue ==secondSelectedValue || thirdSelectedValue ==firstSelectedValue || thirdSelectedValue ==forthSelectedValue)
            {
									 navigator.notification.alert("This Rank is already selected!!",function completed(){
																  debug("second value set to blank ::: "+page2Data.secondSelect);
																  self.thirdSelect="";
																  document.getElementById("thirdSelect").selectedIndex = "0";
																  },
																  "Fact Finder","OK");
									 
            }
        };
        this.check4 = function(){
            var firstSelectedValue=this.firstSelect;
            var secondSelectedValue=this.secondSelect;
            var thirdSelectedValue=this.thirdSelect;
            var forthSelectedValue=this.forthSelect;
            if(forthSelectedValue ==secondSelectedValue || forthSelectedValue ==thirdSelectedValue || forthSelectedValue ==firstSelectedValue)
            {
									 navigator.notification.alert("This Rank is already selected!!",function completed(){
																  debug("second value set to blank ::: "+page2Data.secondSelect);
																  self.forthSelect="";
																  document.getElementById("forthSelect").selectedIndex = "0";
																  },
																  "Fact Finder","OK");

                             }
        };
        this.ContSavePage2 = function(ffPage2Form){
        this.click = true;
        var occupation=null;
        var occupationOther=null;
        var bussinessOther=null;
        var bussiness=null;

        if(ffPage2Form.$invalid!=true){
                if(this.rdoccupation=='Other')//use model name
                {
                    this.occupation="Other";
                    this.occupationOther=this.occOther;
                    //this.rdbusiness = null;
                }
                else
                {
                    this.occupation=this.rdoccupation;
                    this.occupationOther="";
                }
                if(this.rdbusiness=='Other')//use model name
                {
                    this.bussiness="Other";
                    this.bussinessOther=this.occOther11;
                }
                else
                {
                    this.bussiness=(this.rdbusiness== null ? "" :this.rdbusiness);
                    this.bussinessOther="";
                }
                var firstSelectedValue=this.firstSelect;
                var secondSelectedValue=this.secondSelect;
                var thirdSelectedValue=this.thirdSelect;
                var forthSelectedValue=this.forthSelect;
                var invest1=this.investDetail1 ;//== null ? "" :this.investDetail1 ;
                var invest2=this.investDetail2 ;//== null ? "" :this.investDetail2 ;
                var invest3=this.investDetail3 ;//== null ? "" :this.investDetail3 ;
                var invest4=this.investDetail4 ;//== null ? "" :this.investDetail4 ;
                console.log("invest1"+invest1);
                console.log("invest2"+invest2);
                console.log("invest3"+invest3);
                console.log("invest4"+invest4);


                SavePage2Data.getDataPage2(this.occupation,this.bussiness,this.occupationOther,this.bussinessOther, this.AnnualIncome,firstSelectedValue,invest1,secondSelectedValue,invest2,thirdSelectedValue,invest3,forthSelectedValue,invest4).then(
                function(res){
                    if(res=="success"){
                        SavePage2Data.UpdateFFPage2Status().then(
                            function(res){
                               FFTimelineService.isNeedIdTabComplete = true;
                               $state.go(FACT_FINDER_STATE2);
                            }
                            );

                    }
                });
            }
           else
           {
           navigator.notification.alert("Please fill all mandatory data!!",null,"Fact Finder","OK");
           }
       }

}]);

    ffaModule2.service('SavePage2Data',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){
        this.getDataPage2 = function(rdoccupation,rdbusiness,occOther,occOther11,AnnualIncome,firstValue,investDetail1,secondValue,investDetail2,thirdValue,investDetail3,forthValue,investDetail4){
            var AnnualIncome1= AnnualIncome;
            var temp;
             if (AnnualIncome1 <= 500000) {
                temp = "Less than or equal to 5lakhs";
            }else if (AnnualIncome1 >500000 && AnnualIncome1<=1000000) {
                temp = "5-10 lakhs";
            }else if (AnnualIncome1 >1000000 && AnnualIncome1<=25000000) {
                temp = "10-25 lakhs";
            }else if (AnnualIncome1 >25000000 ) {
                temp = "25 lakhs and above";
            }
            console.log("getFHRId : "+factFinderService.getFHRId());
            var dfd = $q.defer();
            try{
                    CommonService.transaction(db,
                    function(tx){
                        console.log("occupation1: "+rdoccupation);
//                        CommonService.executeSql(tx,"update LP_FHR_FFNA set OCCUPATION_DESC= '"+rdoccupation+"',OCCUPATION_OTHER = '"+ occOther +"' ,OCCU_ORG_TYPE_DESC='"+ rdbusiness +"' , OCCU_ORG_TYPE_OTHERS = '"+occOther11+"' , ANNUAL_INCOME= '"+ AnnualIncome +"' , ANNUAL_INCOME_BUCKET= '"+ temp +"', INVEST_GOAL_RANK_1 = '"+ firstValue +"',EXIST_INVEST_DETLS_1='"+ investDetail1 +"', INVEST_GOAL_RANK_2 = '"+ secondValue +"',EXIST_INVEST_DETLS_2='"+ investDetail2+"', INVEST_GOAL_RANK_3 = '"+ thirdValue +"',EXIST_INVEST_DETLS_3='"+ investDetail3 +"', INVEST_GOAL_RANK_4 = '"+ forthValue +"',EXIST_INVEST_DETLS_4 = '"+ investDetail4  +"' where FHR_ID = ? ",[factFinderService.getFHRId()],
                        CommonService.executeSql(tx,"update LP_FHR_FFNA  set OCCUPATION_DESC = ? , OCCUPATION_OTHER = ? , OCCU_ORG_TYPE_DESC = ? , OCCU_ORG_TYPE_OTHERS = ? , ANNUAL_INCOME = ? , ANNUAL_INCOME_BUCKET = ? , INVEST_GOAL_RANK_1 = ? , EXIST_INVEST_DETLS_1 = ?, INVEST_GOAL_RANK_2 = ?, EXIST_INVEST_DETLS_2 = ?, INVEST_GOAL_RANK_3 = ?, EXIST_INVEST_DETLS_3 = ?, INVEST_GOAL_RANK_4 = ?, EXIST_INVEST_DETLS_4 = ? WHERE FHR_ID = ? ",
                        [rdoccupation , occOther, rdbusiness , occOther11 , AnnualIncome , temp , firstValue , investDetail1 , secondValue , investDetail2 , thirdValue , investDetail3 , forthValue , investDetail4 , factFinderService.getFHRId()],
                        function(tx,res){
                            dfd.resolve("success");

//                            console.log("investDetail1"+investDetail1):
//                            console.log("investDetail2"+investDetail2):
//                            console.log("investDetail3"+investDetail3):
//                            console.log("investDetail4"+investDetail4):
                            factFinderService.setFHRPage2Data(rdoccupation,rdbusiness,occOther,occOther11,AnnualIncome,firstValue,investDetail1,secondValue,investDetail2,thirdValue,investDetail3,forthValue,investDetail4);
                        },
                        function(tx,err){
                           navigator.notification.alert("Error!!",null,"Fact Finder","OK");
                            dfd.resolve(null);
                        });
                    },
                    function(err){
                        debug("Error in updateUserInfo(): " + err.message);
                        dfd.resolve(null);
                        },null
                    );
                }catch(ex){
                    debug("Exception in updateUserInfo(): " + ex.message);
                    dfd.resolve(null);
                }
                return dfd.promise;
         };
        this.UpdateFFPage2Status = function(){
	 	var dfd = $q.defer();
	 	try{
	 		CommonService.transaction(db,
	 		function(tx){
	 			console.log("update LP_FHR_MAIN set IS_SCREEN2_DONE = 'Y'  where FHR_ID= ? ");
	 			CommonService.executeSql(tx,"update LP_FHR_MAIN set IS_SCREEN2_DONE = 'Y' where FHR_ID= ? ",[factFinderService.getFHRId()],
	 			function(tx,res){
	 				debug(" updated LP_FHR_FFNA2 Status");
	 				dfd.resolve("success");
	 			},
	 			function(tx,err){
	 				navigator.notification.alert("Error!!",null,"Fact Finder","OK");
	 				debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
	 				dfd.resolve(null);
	 			});
	 		},
	 		function(err){
	 			debug("Error in LP_FHR_FFNA(): " + err.message);
	 			dfd.resolve(null);
	 		},null
	 		);
	 	}catch(ex){
	 		debug("Exception in LP_FHR_FFNA(): " + ex.message);
	 		dfd.resolve(null);
	 	}
	 return dfd.promise;
	 };
    }]);
