ffaModule.service('factFinderService',['$state',function($state){

	//Page1 Variable
	var fhrId = "";
	var leadId = "";
	var oppId = "";
	var agentCd = "";
	var docId="";
	var leadName = "";
	var channelOfSales=null;
	var customerSegment=null;
	var tittle=null;
	var firstName=null;
	var middleName=null;
	var lastName=null;
	var mobileNo=null;
	var isIndusBankCust=null;
	var dob=null;
	var indusBankOpeningDate=null;
	var indusBankAccNo=null;
	var indusBankRelation=null;
	var indusBankAccType=null;
	var indusBankIfsc=null;
	var indusBankBranchName=null;
	var currentLifeStage=null;

	//Page2 Variable
	var rdoccupation=null;
	var rdbusiness=null;
	var bussinessOther=null;
	var occupationOther=null;
	var AnnualIncome=null;
	var investDetail1=null;
	var investDetail2=null;
	var investDetail3=null;
	var investDetail4=null;
	var firstSelect=null;
	var secondSelect=null;
	var thirdSelect=null;
	var forthSelect=null;

	//Page 3 Variable
	var EquityFund=null;
    var BalancedFund=null;
    var DebtFund=null;
    var Total=null;
    var payoption=null;
    var invest=null;
    var rdInvesting=null;

    //PAGE 4 Variable
    var recommendedProducts=null;
    var recommendedFundsOptions=null;
    var productName=null;
    var fundDescription=null;
    var customerConcent=null;
    var pgl_id=null;

	this.clearFHRData=function(){
		fhrId = "";
		leadId = "";
		oppId = "";
		agentCd = "";
		leadName = "";
		channelOfSales=null;
		customerSegment=null;
		tittle=null;
		firstName=null;
		middleName=null;
		lastName=null;
		mobileNo=null;
		isIndusBankCust=null;
		dob=null;
		indusBankOpeningDate=null;
		indusBankAccNo=null;
		indusBankRelation=null;
		indusBankAccType=null;
		indusBankIfsc=null;
		indusBankBranchName=null;
		currentLifeStage=null;

		//Page2 Variable
		rdoccupation=null;
		rdbusiness=null;
		bussinessOther=null;
		occupationOther=null;
		AnnualIncome=null;
		investDetail1=null;
		investDetail2=null;
		investDetail3=null;
		investDetail4=null;
		firstSelect=null;
		secondSelect=null;
		thirdSelect=null;
		forthSelect=null;

		//Page 3 Variable
		EquityFund=null;
		BalancedFund=null;
		DebtFund=null;
		Total=null;
		payoption=null;
		invest=null;
		rdInvesting=null;

		//PAGE 4 Variable
		recommendedProducts=null;
		recommendedFundsOptions=null;
		productName=null;
		fundDescription=null;
		customerConcent=null;
		pgl_id=null;
		docId="";
		console.log("Clear");
	}
	this.setFHRPage4Data=function(recommendedproducts , recommendedfundsOptions, productname , funddescription,customerconcent,pglid)
    {
    	recommendedProducts=recommendedproducts;
    	recommendedFundsOptions=recommendedfundsOptions;
    	productName=productname;
    	fundDescription=funddescription;
    	customerConcent=customerconcent;
    	pgl_id=pglid;
    	console.log("service customerConcent"+customerConcent);
    }
    this.getFHRPage4Data1=function(){
		var obj = {};
		obj.PRODUCT_NAME=productName;
		obj.PRODUCT_UINNO=recommendedProducts;
		return obj;
        }
	this.getFHRPage4Data2=function(){
		var obj = {};
		obj.FUND_CODE=recommendedFundsOptions;
		obj.FUND_DESCRIPTION=fundDescription;
		return obj;
	}


    this.setFHRPage3Data=function(payOption , rdInvest1, rdinvesting , equityFund , balancedFund , debtFund,total)
    {
    	payoption=payOption;
    	EquityFund=equityFund;
    	invest=rdInvest1;
    	rdInvesting=rdinvesting;
    	BalancedFund=balancedFund;
    	DebtFund=debtFund;
    	Total=total;
    }
    this.getFHRPage3Data=function(){
		var obj = {};
		obj.payoption=payoption;
		obj.EquityFund=EquityFund;
		obj.invest=invest;
		obj.rdInvesting=rdInvesting;
		obj.BalancedFund=BalancedFund;
		obj.DebtFund=DebtFund;
		obj.Total=Total;
		return obj;
    }
    this.setFHRPage2Data=function(rdOccupation,rdBusiness,occOther,occOther11,annualIncome,FirstValue,InvestDetail1,SecondValue,InvestDetail2,ThirdValue,InvestDetail3,ForthValue,InvestDetail4)
	{

		rdoccupation=rdOccupation;
		rdbusiness=rdBusiness;
		bussinessOther=occOther;
		occupationOther=occOther11;
		AnnualIncome=annualIncome;
		firstSelect=FirstValue;
		investDetail1=InvestDetail1;
		secondSelect=SecondValue;
		investDetail2=InvestDetail2;
		thirdSelect=ThirdValue;
		investDetail3=InvestDetail3;
		forthSelect=ForthValue;
		investDetail4=InvestDetail4;
	}
	this.getFHRPage2Data=function(){
		var obj = {};
		obj.rdoccupation = rdoccupation;
		obj.rdbusiness = rdbusiness;
		obj.bussinessOther=bussinessOther;
		obj.occupationOther=occupationOther;
		obj.AnnualIncome=AnnualIncome;
		obj.firstSelect=firstSelect;
		obj.investDetail1=investDetail1;
		obj.secondSelect=secondSelect;
		obj.investDetail2=investDetail2;
		obj.thirdSelect=thirdSelect;
		obj.investDetail3=investDetail3;
		obj.forthSelect=forthSelect;
		obj.investDetail4=investDetail4;
		return obj;
    }
	this.setFHRPage1Data=function(ChannelOfSales,CustomerSegment,Tittle,FirstName,MiddleName,LastName,MobileNo,IsIndusBankCust,Dob,IndusBankOpeningDate,IndusBankAccNo,IndusBankRelation,IndusBankAccType,IndusBankIfsc,IndusBankBranchName,CurrentLifeStage){
		channelOfSales=ChannelOfSales;
		customerSegment=CustomerSegment;
		tittle=Tittle;
		firstName=FirstName;
		middleName=MiddleName;
		lastName=LastName;
		mobileNo=MobileNo;
		isIndusBankCust=IsIndusBankCust;
		dob=Dob;
		indusBankOpeningDate=IndusBankOpeningDate;
		indusBankAccNo=IndusBankAccNo;
		indusBankRelation=IndusBankRelation;
		indusBankAccType=IndusBankAccType;
		indusBankIfsc=IndusBankIfsc;
		indusBankBranchName=IndusBankBranchName;
		currentLifeStage=CurrentLifeStage;
		console.log("set isIndusBankCust"+isIndusBankCust);
		};
	this.getFHRPage1Data=function(){
		var obj = {};
		obj.channelOfSales = channelOfSales;
		obj.customerSegment = customerSegment;
		obj.tittle=tittle;
		obj.firstName=firstName;
		obj.middleName=middleName;
		obj.lastName=lastName;
		obj.mobileNo=mobileNo;
		obj.dob=dob;
		obj.isIndusBankCust=isIndusBankCust;
		obj.indusBankOpeningDate=indusBankOpeningDate;
		obj.indusBankAccNo=indusBankAccNo;
		obj.indusBankRelation=indusBankRelation;
		obj.indusBankAccType=indusBankAccType;
		obj.indusBankIfsc=indusBankIfsc;
		obj.indusBankBranchName=indusBankBranchName;
		obj.currentLifeStage=currentLifeStage;
		console.log("get isIndusBankCust"+isIndusBankCust);
		return obj;
		}

	this.getcustomerConcent = function(){
		console.log("service get  customerConcent"+customerConcent);
    	return customerConcent;
    };
	this.getleadId = function(){
    	return leadId;
    };
    this.setleadId = function(leadid){
    		leadId=leadid;
    };
	this.getFHRId = function(){
		return fhrId;
	};
	this.getleadName = function(){
    		return leadName;
    }
	this.setOppId = function(opportunityId){
       		oppId = opportunityId;
     }
    this.getOppId = function(){
       		return oppId;
     }
	this.setDocId = function(docid){
		docId = docid;
	}
	this.getDocId = function(){
		return docId;
	}
    this.getAgentCD = function(){
       		return agentCd;
    }
    this.getPgl_id = function(){
		return pgl_id;
	};

    this.setFHRData = function(fhrid,leadname,oppid,agentcd){
		fhrId=fhrid;
		leadName=leadname;
		oppId=oppid;
		agentCd=agentcd;
	};

}]);
