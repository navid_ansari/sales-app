ffaModule4.controller('ffPage4Ctrl',['$state','SavePage4Data','page2Data','page3Data','factFinderService','$filter', 'CommonService','FFTimelineService', function($state,SavePage4Data,page2Data,page3Data,factFinderService,$filter, CommonService,FFTimelineService){
	"use strict";
	CommonService.hideLoading();
                                     
    /* Header Hight Calculator */
    setTimeout(function(){
               var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
               console.log("getHeight Test : " + factFinderAnalysisCalHeight);
               $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    });
    /* End Header Hight Calculator */

                                     
                                     
	FFTimelineService.setActiveTab("prodRec");
	this.LPProductList = page2Data;
	this.LPFundList = page3Data;
	var page4DataProduct=factFinderService.getFHRPage4Data1();
	var page4DataFund=factFinderService.getFHRPage4Data2();
	this.recProduct = {};
	this.recFund = {};
	var customerconcent=factFinderService.getcustomerConcent();
	console.log("customerconcent"+customerconcent);
	this.rdInvest=customerconcent == null ? "" :customerconcent;
	this.recProduct=$filter('filter')(this.LPProductList, {"PRODUCT_UINNO": page4DataProduct.PRODUCT_UINNO})[0];
	this.recFund=$filter('filter')(this.LPFundList, {"FUND_CODE": page4DataFund.FUND_CODE})[0];
	this.Backbtn4= function(){
		  $state.go(FACT_FINDER_STATE2);
	};
	this.openMenu = function(){
		CommonService.openMenu();
   	};
	this.CheckForULIP=function(){
		var name = this.recProduct.PRODUCT_SOLUTION;
		console.log("Is ULIP"+name);
		this.recFund={};
	};
	var self=this;
	self.click = false;
	var flag="";
	SavePage4Data.getFHRData().then(
	function(res)
	{
		if(res.rows.length > 0){
			var iagree=res.rows.item(0).CUST_CNT_AGREE;
			var idisagree=res.rows.item(0).CUST_CNT_NOT_AGREE;
			var ipartialagree=res.rows.item(0).CUST_CNT_PARTIAL_AGREE;
			if(iagree=="Y")
				self.rdInvest="The above recommendation is based on the information provided by me and as documented in this form. I have explained about the features of the products / and / funds / and believe it would be suitable for me based on my insurance needs and financial objectives.";
			else if(idisagree=='Y')
			 	self.rdInvest="I do not agree with the above recommendation and have opted for alternate products for my need.";
			else if(ipartialagree=='Y')
				self.rdInvest="I agree with the product recommendation made above but DO NOT agree with the choice of fund ( only for ULIP products.)";
			SavePage4Data.getProductRecomended().then(
			function(res)
			{
				if(res.rows.length > 0){
					flag="update";
					var urlNo=res.rows.item(0).RCMND_PRODUCT_URNNO;
					var fundcode=res.rows.item(0).RCMND_FUND1_CODE;
					self.recProduct=$filter('filter')(self.LPProductList, {"PRODUCT_UINNO": urlNo})[0];
                    self.recFund=$filter('filter')(self.LPFundList, {"FUND_CODE": fundcode})[0];
				}
				else {
					flag="insert";
				}
			});
		}
	});
	this.ContSavePage6 = function(){
	self.click = true;
	console.log("this.recProduct"+ this.recProduct);//1788
	debug("Select is "+self.recProduct.PRODUCT_NAME);

		if(self.recProduct==undefined || self.recProduct.PRODUCT_NAME=="Select" || self.recProduct=="Select"|| self.recProduct=="Select" ||self.rdInvest==null ||self.rdInvest=="")
		{
			navigator.notification.alert("Please fill all mandatory data!!",null,"Fact Finder","OK");
		}
		else{
			var name=self.recProduct.PRODUCT_SOLUTION;
			var ulipCode,ulipdiscption;
			if(name=="Wealth Solutions"){
				ulipCode = this.recFund.FUND_CODE;
				ulipdiscption = this.recFund.FUND_DESCRIPTION;
			}
			else{
				ulipCode = null;
				ulipdiscption=null;
			}

			if(name=="Wealth Solutions"){
				if(self.recFund.FUND_CODE==undefined || self.recFund.FUND_CODE=="" || self.recFund.FUND_CODE=="Select")
					navigator.notification.alert("Please fill all mandatory data!!",null,"Fact Finder","OK");
				else
				{
					SavePage4Data.getDataPage6(self.recProduct.PRODUCT_UINNO,ulipCode,self.recProduct.PRODUCT_NAME,ulipdiscption,self.recProduct.PRODUCT_PGL_ID,self.rdInvest,flag).then(
					function(res){
						if(res=="success"){
							SavePage4Data.getDataPage7(self.rdInvest).then(
							function(res){
								SavePage4Data.UpdateFFPage4Status().then(
								function(res){
									factFinderService.ffData=null;
									FFTimelineService.isProdRecTabComplete = true;
									$state.go("ffPageOutput");
								});
							});
						}
					});
				}
			}
			else
			{
					SavePage4Data.getDataPage6(self.recProduct.PRODUCT_UINNO,ulipCode,self.recProduct.PRODUCT_NAME,ulipdiscption,self.recProduct.PRODUCT_PGL_ID,self.rdInvest,flag).then(
					function(res){
						if(res=="success"){
							SavePage4Data.getDataPage7(self.rdInvest).then(
							function(res){
								SavePage4Data.UpdateFFPage4Status().then(
								function(res){
									factFinderService.ffData=null;
									FFTimelineService.isProdRecTabComplete = true;
									$state.go("ffPageOutput");
								});
							});
						}
					});
			}
        }
	};
}]);

ffaModule4.service('SavePage4Data',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){

this.LPProductMaster=function(){
	var dfd = $q.defer();
	CommonService.transaction(db,
	function(tx){
		CommonService.executeSql(tx,"SELECT PRODUCT_UINNO,PRODUCT_NAME,PRODUCT_PGL_ID,PRODUCT_SOLUTION FROM LP_PRODUCT_MASTER WHERE ISACTIVE='Y' AND BUSINESS_TYPE_LIST like '%" + BUSINESS_TYPE + "%'  Order BY PRODUCT_NAME",[],
		function(tx,res){
			if(!!res && res.rows.length>0){
				var LPProductList = [];
				LPProductObj = {};
				LPProductObj.PRODUCT_NAME = "Select";
//				LPProductObj.PRODUCT_UINNO = "";
//				LPProductObj.PRODUCT_PGL_ID = "";
//				LPProductObj.PRODUCT_SOLUTION = "";
				LPProductList.push(LPProductObj);
				for(var i=0;i<res.rows.length;i++){
					LPProductObj = {};
					LPProductObj.PRODUCT_NAME = res.rows.item(i).PRODUCT_NAME;
					LPProductObj.PRODUCT_UINNO = res.rows.item(i).PRODUCT_UINNO;
					LPProductObj.PRODUCT_PGL_ID = res.rows.item(i).PRODUCT_PGL_ID;
					LPProductObj.PRODUCT_SOLUTION = res.rows.item(i).PRODUCT_SOLUTION;
					LPProductList.push(LPProductObj);
				}
				dfd.resolve(LPProductList);
			}
			else{
				console.log("lp_product_master is empty");
				dfd.resolve(null);
			}
		},
		function(tx,err){
			console.log("Error in fetchErrDescMasterData().transaction(): " + err.message);
			dfd.resolve(null);
		}
		);
	},
	function(err){
		console.log("Error in fetchErrDescMasterData(): " + err.message);
		dfd.resolve(null);
		},null
	);
	return dfd.promise;
  };
//fund master table data
this.LPFundMaster=function(){
       var dfd = $q.defer();
         CommonService.transaction(db,
          		function(tx){
          	CommonService.executeSql(tx,"SELECT FUND_CODE ,FUND_DESCRIPTION FROM LP_FUND_MASTER ORDER BY FUND_DESCRIPTION",[],
          		 function(tx,res){
          		 // alert("call fun"+res);
          		if(!!res && res.rows.length>0){
          			var LPFundList = [];
                     console.log("res.rows.length: " + res.rows.length);
                     for(var i=0;i<res.rows.length;i++){
                     LPFundObj = {};
                     LPFundObj.FUND_CODE = res.rows.item(i).FUND_CODE;
                     LPFundObj.FUND_DESCRIPTION = res.rows.item(i).FUND_DESCRIPTION;
                     LPFundList.push(LPFundObj);
                         }
                         dfd.resolve(LPFundList);
          			  }
          	            else{
          				console.log("lp_error_desc_master is empty");
          							dfd.resolve(null);
          						}
          					},
          					function(tx,err){
          						console.log("Error in fetchErrDescMasterData().transaction(): " + err.message);
          						dfd.resolve(null);
          					}
          				);
          			},
          			function(err){
          				console.log("Error in fetchErrDescMasterData(): " + err.message);
          				dfd.resolve(null);
          			},null
          		);
          return dfd.promise;
  };
this.getDataPage7 = function(rdInvest){
		var dfd = $q.defer();
		console.log("getFHRId : "+factFinderService.getFHRId());
		console.log("Selection : "+rdInvest);
		var flagAgree="";
		var flagDisAgree="";
		var flagPartial="";
		if(rdInvest=="The above recommendation is based on the information provided by me and as documented in this form. I have explained about the features of the products / and / funds / and believe it would be suitable for me based on my insurance needs and financial objectives.")
		{
		    flagAgree="Y";
		    flagDisAgree="N";
		    flagPartial="N";
		}
		else if(rdInvest=="I do not agree with the above recommendation and have opted for alternate products for my need.")
		{
            flagAgree="N";
            flagDisAgree="Y";
            flagPartial="N";
		}
		else
		{
		    flagAgree="N";
        	flagDisAgree="N";
        	flagPartial="Y";
		}
		try{
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"update LP_FHR_FFNA set  CUST_CNT_AGREE = '"+flagAgree +"', CUST_CNT_NOT_AGREE ='"+ flagDisAgree +"', CUST_CNT_PARTIAL_AGREE ='"+flagPartial+"', MODIFIED_DATE ='"+CommonService.getCurrDate()+"' where FHR_ID=?",[factFinderService.getFHRId()],
						function(tx,res){
						debug("successfully updated LP_FHR_FFNA");
							dfd.resolve("success");
						},
						function(tx,err){

							debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
							dfd.resolve(null);
						}
					);
				},
				function(err){
					debug("Error in LP_FHR_FFNA(): " + err.message);
					dfd.resolve(null);
				},null
			);
		}catch(ex){
			debug("Exception in LP_FHR_FFNA(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

 this.getDataPage6 = function(recommendedProducts,recommendedFundsOptions,productName,fundDescription,product_pgl_id,rdinvestval,dbflag){


		console.log("rdinvestval : "+rdinvestval);
		var dfd = $q.defer();
		try{
			if(dbflag=="insert"){
				debug("Querry"+"insert");
				CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"insert into LP_FHR_PRODUCT_RCMND ( RCMND_PRODUCT_URNNO, RCMND_PRODUCT_DTLS, RCMND_FUND_DTLS ,FHR_ID , AGENT_CD, PRODUCT_PGL_ID, RCMND_INVEST_OPT, RCMND_FUND1_CODE, RCMND_FUND1_PERCENT, MODIFIED_DATE) VALUES (?,?,?,?,?,?,?,?,?,?) ",
						 [recommendedProducts ,productName,fundDescription , factFinderService.getFHRId(),factFinderService.getAgentCD(), product_pgl_id, "Manual", recommendedFundsOptions , "100",CommonService.getCurrDate()],
						 function(tx,res){
								debug("successfully insert LP_FHR_PRODUCT_RCMND");
								factFinderService.setFHRPage4Data(recommendedProducts,recommendedFundsOptions,productName ,fundDescription,rdinvestval,product_pgl_id);
								dfd.resolve("success");
							},
							function(tx,err){
							//alert("error");
								debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
								dfd.resolve(null);
							}
						);
					},
					function(err){
						debug("Error in LP_FHR_FFNA(): " + err.message);
						dfd.resolve(null);
					},null
				);
			}
			else {
				debug("Querry"+"update");
				CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"update LP_FHR_PRODUCT_RCMND SET RCMND_PRODUCT_URNNO = ? , RCMND_PRODUCT_DTLS = ? , RCMND_FUND_DTLS = ? , PRODUCT_PGL_ID = ? , RCMND_INVEST_OPT = ? , RCMND_FUND1_CODE = ?  , MODIFIED_DATE = ?  WHERE FHR_ID = ? AND AGENT_CD = ?",
						 [recommendedProducts ,productName,fundDescription , product_pgl_id, "Manual", recommendedFundsOptions ,CommonService.getCurrDate(), factFinderService.getFHRId(),factFinderService.getAgentCD()],
						 function(tx,res){
								debug("successfully insert LP_FHR_PRODUCT_RCMND");
								factFinderService.setFHRPage4Data(recommendedProducts,recommendedFundsOptions,productName ,fundDescription,rdinvestval,product_pgl_id);
								dfd.resolve("success");
							},
							function(tx,err){
							//alert("error");
								debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
								dfd.resolve(null);
							}
						);
					},
					function(err){
						debug("Error in LP_FHR_FFNA(): " + err.message);
						dfd.resolve(null);
					},null
				);
			}

		}catch(ex){
			debug("Exception in LP_FHR_FFNA(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};
	this.getFHRData=function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
		function(tx){
			CommonService.executeSql(tx,"SELECT * from LP_FHR_FFNA where FHR_ID = '"+factFinderService.getFHRId()+"'",[],
				function(tx,res){
				dfd.resolve(res);
			},
			function(tx,err){
				console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
				dfd.resolve(null);
			});
			},
			function(err){
				console.log("Error in fetchErrDescMasterData(): " + err.message);
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};
	this.getProductRecomended=function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
		function(tx){
			CommonService.executeSql(tx,"SELECT * from LP_FHR_PRODUCT_RCMND where FHR_ID = '"+factFinderService.getFHRId()+"'",[],
				function(tx,res){
				dfd.resolve(res);
			},
			function(tx,err){
				console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
				dfd.resolve(null);
			});
			},
			function(err){
				console.log("Error in fetchErrDescMasterData(): " + err.message);
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};
	this.UpdateFFPage4Status = function(){
		var dfd = $q.defer();
		try{
			CommonService.transaction(db,
			function(tx){
				console.log("update LP_FHR_MAIN set IS_SCREEN4_DONE = 'Y'  where FHR_ID= ? ");
				CommonService.executeSql(tx,"update LP_FHR_MAIN set IS_SCREEN4_DONE = 'Y' where FHR_ID= ? ",[factFinderService.getFHRId()],
				function(tx,res){
					debug(" updated LP_FHR_FFNA4 Status");
					dfd.resolve("success");
				},
				function(tx,err){
					navigator.notification.alert("Error!!",null,"Fact Finder","OK");
					debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
					dfd.resolve(null);
				});
			},
			function(err){
				debug("Error in LP_FHR_FFNA(): " + err.message);
				dfd.resolve(null);
			},null
			);
		}catch(ex){
			debug("Exception in LP_FHR_FFNA(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};
}]);
