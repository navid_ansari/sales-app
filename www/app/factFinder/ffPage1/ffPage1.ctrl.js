ffaModule1.controller('ffPage1Ctrl',['$state','$location','SavePage1Data','page1Data','factFinderService','CommonService','leadData', 'FFTimelineService','maxDate','FhrMainData','EKYCData', function($state,$location,SavePage1Data,page1Data,factFinderService,CommonService,leadData,FFTimelineService,maxDate,FhrMainData, EKYCData){
    "use strict";
	CommonService.hideLoading();

    /* Header Hight Calculator */
        
        var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
        console.log("getHeight Test : " + factFinderAnalysisCalHeight);
        $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    
    /* End Header Hight Calculator */


     debug("SET FHR ID : "+factFinderService.getFHRId());
     debug("MaxDate"+maxDate);
      debug("FHRMAIN"+JSON.stringify(FhrMainData));
      if(FhrMainData.rows.length == 0){
        FFTimelineService.reset();
      }
      else{
        FFTimelineService.reset();
        debug("IS_SCREEN1_DONE"+FhrMainData.rows.item(0).IS_SCREEN1_DONE);
        debug("IS_SCREEN2_DONE"+FhrMainData.rows.item(0).IS_SCREEN2_DONE);
        debug("IS_SCREEN3_DONE"+FhrMainData.rows.item(0).IS_SCREEN3_DONE);
        debug("IS_SCREEN4_DONE"+FhrMainData.rows.item(0).IS_SCREEN4_DONE);
        if(FhrMainData.rows.item(0).IS_SCREEN1_DONE=="Y")
        {
            FFTimelineService.isMndFieldsTabComplete = true;
            if(FhrMainData.rows.item(0).IS_SCREEN2_DONE=="Y")
            {
                 FFTimelineService.isNeedIdTabComplete = true;
                 if(FhrMainData.rows.item(0).IS_SCREEN3_DONE=="Y")
                 {
                      FFTimelineService.isPrefPayoutTabComplete = true;
                      if(FhrMainData.rows.item(0).IS_SCREEN3_DONE=="Y")
                      {
                         FFTimelineService.isPrefPayoutTabComplete = true;
                         if(FhrMainData.rows.item(0).IS_SCREEN4_DONE=="Y")
                         {
                            FFTimelineService.isProdRecTabComplete = true;
                         }
                      }
                 }
            }
        }
      }
     this.maxdate = new Date(maxDate);
    FFTimelineService.setActiveTab("mndFields");
    var self=this;
    self.click = false;
	this.numberRegex = NUM_REG;
    this.nameRegex = NAME_REGEX;
    this.fullnameRegex=USERNAME_REGEX;
    this.mobileRegex= MOBILE_REGEX;
    this.ifscRegex = IFSC_REG;
    var pageData=factFinderService.getFHRPage1Data();
    debug("date from previous page is "+JSON.stringify(pageData));
    
    var ekyc_fname = null;
    var ekyc_mname = null;
    var ekyc_lname = null;
    var ekyc_dob = null;

    //Populate EKYC DATA
    if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y')
    {
        var nameArr = EKYCData.proposer.NAME.split(" ");
            ekyc_fname = nameArr[0];
            if(nameArr.length>2){
              ekyc_mname = "";
              for(var i=1; i<(nameArr.length-1); i++){
                ekyc_mname += nameArr[i];
                if(i!=(nameArr.length-2))
                  ekyc_mname += " ";
              }
              ekyc_lname = nameArr[(nameArr.length - 1)];
            }
            else{
              ekyc_mname = null;
              if(nameArr.length==2){
                ekyc_lname = nameArr[1];
              }
            }
      if(!!EKYCData.proposer.DOB){
        ekyc_dob = EKYCData.proposer.DOB + " 00:00:00";
      }
    }

    this.rdValue=pageData.isIndusBankCust;
    this.SalesChannel=pageData.channelOfSales;
    this.CustomerSegmentSelect=pageData.customerSegment;
    this.Dob = (EKYCData.proposer.EKYC_FLAG=='Y') ? ((!!ekyc_dob) ? CommonService.formatDobFromDb(ekyc_dob) : ((!!pageData.dob) ? CommonService.formatDobFromDb(pageData.dob) : null)) : ((!!pageData.dob) ? CommonService.formatDobFromDb(pageData.dob) : null);
    this.Title=pageData.tittle== null ? "" :pageData.tittle;
    this.AccountOpeningDate=pageData.indusBankOpeningDate== null ? "" :CommonService.formatDobFromDb(pageData.indusBankOpeningDate);
    this.FName = (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_fname : ((!!pageData.firstName) ? pageData.firstName : null);
    this.BankAccountNo=pageData.indusBankAccNo== null ? "" :parseInt(pageData.indusBankAccNo);
    this.MiddleName = (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_mname : ((!!pageData.middleName) ? pageData.middleName:null);
    this.LName = (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_lname : ((!!pageData.lastName) ? pageData.lastName:null);
    this.MobileNo=pageData.mobileNo== null ? "" :parseInt(pageData.mobileNo);
    this.Relationship=pageData.indusBankRelation;
    this.CurrentLifeStage=pageData.currentLifeStage;
    this.ifscCode=pageData.indusBankIfsc== null ? "" :pageData.indusBankIfsc;
    this.branchName=pageData.indusBankBranchName== null ? "" :pageData.indusBankBranchName;
    this.accountType=pageData.indusBankAccType;

	this.openMenu = function(){
		CommonService.openMenu();
	};
	this.onCustSegChange = function(){
        if(this.rdValue == 'Y' && !!this.CustomerSegmentSelect){
            this.Relationship = this.CustomerSegmentSelect;
            /*if(this.CustomerSegmentSelect == 'EXCLUSIVE')
                this.Relationship = this.CustomerSegmentSelect;
            else if(this.CustomerSegmentSelect == 'OTHERS')
                this.Relationship = this.CustomerSegmentSelect;
            else
                this.Relationship = "";*/
       }else{
            this.Relationship = "";
       }
	};
    this.Changed=function(){
       this.ifscCode="";
       this.branchName="";
       this.BankAccountNo="";
       this.AccountOpeningDate="";
       this.Relationship="";
       this.accountType="";
       if(this.rdValue == 'Y' && !!this.CustomerSegmentSelect){
            this.Relationship = this.CustomerSegmentSelect;
            /*if(this.CustomerSegmentSelect == 'EXCLUSIVE')
                this.Relationship = this.CustomerSegmentSelect;
            else if(this.CustomerSegmentSelect == 'OTHERS')
                this.Relationship = this.CustomerSegmentSelect;
            else
                this.Relationship = "";*/
       }else{
            this.Relationship = "";
       }
    }
    SavePage1Data.getFHRData().then(
        function(res)
        {
            console.log("FHR DATA : "+JSON.stringify(res));
              if(res.rows.length == 0){
                   SavePage1Data.getLeadData().then(
                    function(res)
                    {
                        if(res.rows.length > 0){
                           console.log("DOB FORM LEAD"+res.rows.item(0).BIRTH_DATE);
                           console.log("MOBILE_NO FORM LEAD"+res.rows.item(0).MOBILE_NO);
                           console.log("Name FORM LEAD"+res.rows.item(0).LEAD_NAME);
                           self.Dob= (EKYCData.proposer.EKYC_FLAG=='Y') ? CommonService.formatDobFromDb(ekyc_dob) : (res.rows.item(0).BIRTH_DATE==null ? "" : CommonService.formatDobFromDb(res.rows.item(0).BIRTH_DATE));
                           self.MobileNo= parseInt(res.rows.item(0).MOBILE_NO);

                           var name=res.rows.item(0).LEAD_NAME;
                            if(!!EKYCData && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=='Y')
                                name = EKYCData.proposer.NAME;
                           var tempName=name.split(' ');
						   if(tempName.length>0){
	                           self.FName= tempName[0];
							   if(tempName.length>2){
	                           		self.MiddleName= tempName[1];
									self.LName= tempName[2];
								}
								else if(tempName.length>1)
	                           		self.LName= tempName[1];
						   }
						   else{
							   self.FName = null;
							   self.MiddleName = null;
							   self.LName = null;
						   }
                           console.log("tempName SIZE"+tempName.length);
                        }
                    }
                   );
              }
              else
              {
                   factFinderService.setFHRPage1Data(res.rows.item(0).SALES_CHNL_SEGMENT,res.rows.item(0).CONSUMER_SEGMENT,res.rows.item(0).CONSUMER_TITLE,res.rows.item(0).CONSUMER_FIRST_NAME,res.rows.item(0).CONSUMER_MIDDLE_NAME,res.rows.item(0).CONSUMER_LAST_NAME,res.rows.item(0).MOBILE_NO,res.rows.item(0).EXIST_BANK_CUST_FLAG,res.rows.item(0).CONSUMER_BIRTH_DATE,res.rows.item(0).EXIST_BANK_AC_OPEN_DATE,res.rows.item(0).EXIST_BANK_ACCOUNT_NO,res.rows.item(0).BANK_RELATION_TYPE,res.rows.item(0).EXIST_BANK_ACCOUNT_TYPE,res.rows.item(0).EXIST_BANK_IFSC_CODE,res.rows.item(0).EXIST_BANK_BRANCH_NAME,res.rows.item(0).LIFE_STAGE_STATUS);
                   var pageData=factFinderService.getFHRPage1Data();
                   console.log("Is Indus bank Cust"+pageData.isIndusBankCust);
                   console.log("Is Indus indusBankIfsc"+pageData.indusBankIfsc);
                   console.log("Is Indus indusBankBranchName"+pageData.indusBankBranchName);

                   self.rdValue=pageData.isIndusBankCust;
                   self.SalesChannel=pageData.channelOfSales;
                   self.CustomerSegmentSelect=pageData.customerSegment;
                   self.Dob= (EKYCData.proposer.EKYC_FLAG=='Y') ? CommonService.formatDobFromDb(ekyc_dob) : (pageData.dob== null ? "" :CommonService.formatDobFromDb(pageData.dob));
                   self.Title=pageData.tittle== null ? "" :pageData.tittle;
                   self.AccountOpeningDate=pageData.indusBankOpeningDate== null ? "" :CommonService.formatDobFromDb(pageData.indusBankOpeningDate);
                   self.FName= (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_fname : (pageData.firstName== null ? "" :pageData.firstName);
                   self.BankAccountNo=pageData.indusBankAccNo== null ? "" :parseInt(pageData.indusBankAccNo);
                   self.MiddleName = (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_mname : (pageData.middleName== null ? "" :pageData.middleName);
                   self.LName= (EKYCData.proposer.EKYC_FLAG=='Y') ? ekyc_lname : (pageData.lastName== null ? "" :pageData.lastName);
                   self.MobileNo=pageData.mobileNo== null ? "" :parseInt(pageData.mobileNo);
                   self.Relationship=pageData.indusBankRelation;
                   self.CurrentLifeStage=pageData.currentLifeStage;
                   self.ifscCode=pageData.indusBankIfsc==null ? "" :pageData.indusBankIfsc;
                   self.branchName=pageData.indusBankBranchName==null ? "" :pageData.indusBankBranchName;
                   self.accountType=pageData.indusBankAccType;
                   factFinderService.setFHRPage2Data(res.rows.item(0).OCCUPATION_DESC,res.rows.item(0).OCCU_ORG_TYPE_DESC,res.rows.item(0).OCCUPATION_OTHER,res.rows.item(0).OCCU_ORG_TYPE_OTHERS,res.rows.item(0).ANNUAL_INCOME,res.rows.item(0).INVEST_GOAL_RANK_1,res.rows.item(0).EXIST_INVEST_DETLS_1,res.rows.item(0).INVEST_GOAL_RANK_2,res.rows.item(0).EXIST_INVEST_DETLS_2,res.rows.item(0).INVEST_GOAL_RANK_3,res.rows.item(0).EXIST_INVEST_DETLS_3,res.rows.item(0).INVEST_GOAL_RANK_4,res.rows.item(0).EXIST_INVEST_DETLS_4);
                   var total=parseInt(res.rows.item(0).ULIP_ALOC_DEBT_PCT)+parseInt(res.rows.item(0).ULIP_ALOC_BALANCED_PCT) +parseInt(res.rows.item(0).ULIP_ALOC_EQUITY_PCT);
                   factFinderService.setFHRPage3Data(res.rows.item(0).PREF_PAYOUT_OPTION , res.rows.item(0).RISK_ASMNT_EQUITY_PCT, res.rows.item(0).CMFRT_ULIP_INVEST_FLAG , res.rows.item(0).ULIP_ALOC_EQUITY_PCT , res.rows.item(0).ULIP_ALOC_BALANCED_PCT , res.rows.item(0).ULIP_ALOC_DEBT_PCT,total);
//                   factFinderService.setFHRPage4Data(res.rows.item(0).RCMND_PRODUCT_URNNO,res.rows.item(0).RCMND_FUND1_CODE,res.rows.item(0).RCMND_PRODUCT_DTLS ,res.rows.item(0).RCMND_FUND_DTLS,res.rows.item(0).rdinvestval);
             }


        }
    );
    this.ContSave = function(ffPage1Form){
    self.click = true;
      var context=this;

      console.log("context.accountType"+context.accountType);
      console.log("context.Relationship"+context.Relationship);
      console.log("context.BankAccountNo"+context.BankAccountNo);
      console.log("context.ifscCode"+context.ifscCode);
      console.log("context.Title"+context.Title);
      console.log("context.rdValue"+self.rdValue);
      console.log("context.CurrentLifeStage"+context.CurrentLifeStage);
      console.log("context.SalesChannel"+context.SalesChannel);
      console.log("context.CustomerSegmentSelect"+context.CustomerSegmentSelect);
      console.log("context.branchName"+context.branchName);
      console.log("context.BankAccountNo"+context.BankAccountNo);
      console.log("context.AccountOpeningDate"+context.AccountOpeningDate);
      console.log("context.Relationship"+context.Relationship);
      console.log("context.accountType"+context.accountType);
      console.log("context.branchName"+context.branchName);

       if(ffPage1Form.$invalid!=true){
       if(this.rdValue=="Y")
        {

            if(context.Title == undefined|| context.Title=="" ||  context.Title=="Select"|| context.accountType == undefined ||context.Relationship == undefined || context.BankAccountNo=="" || context.AccountOpeningDate=="" || context.branchName=="" ||context.branchName==null
                || context.accountType ==null || context.Relationship ==null || context.ifscCode==null || !context.CurrentLifeStage==undefined || context.CurrentLifeStage=="Select" || context.accountType == "Select" ||context.Relationship == "Select"
                || context.CurrentLifeStage=="" || context.accountType == "" ||context.Relationship == "" || (context.BankAccountNo+"").length < 12 || (context.BankAccountNo+"").length > 12 ||
                context.SalesChannel == undefined|| context.SalesChannel=="" ||  context.SalesChannel=="Select" || context.CustomerSegmentSelect == undefined|| context.CustomerSegmentSelect=="" ||  context.CustomerSegmentSelect=="Select")
            {
                navigator.notification.alert("Please fill all the mandatory data!!",null,"Fact Finder","OK");
            }
            else
            {
                    debug("PAge FOrm "+ffPage1Form.$invalid);

//                    console.log("Error Log"+ $state.ffPage1Form.$error.required);
//                   angular.element("[name='" + ffPage1Form.$name + "']").find('.ng-invalid:visible:first').focus();

                        SavePage1Data.checkIfFHRExist().then(
                        function(res)
                        {
                            if(!!res && res.rows.length>0){
                             SavePage1Data.getData(context.SalesChannel,  context.CustomerSegmentSelect,context.Dob, context.rdValue,context.Title,((!!context.AccountOpeningDate)?CommonService.formatDobToDb(context.AccountOpeningDate):null),context.FName,context.BankAccountNo,context.MiddleName,context.LName,
                                   context.Relationship,context.MobileNo,context.CurrentLifeStage,context.accountType,context.ifscCode,context.branchName,"update").then(
                                    function(res){
                                        if(res=="success"){
                                             SavePage1Data.UpdateFFPage1Status().then(
                                             function(res){
                                                FFTimelineService.isMndFieldsTabComplete = true;
                                                $state.go(FACT_FINDER_STATE1);
                                             });

                                        }
                                    });
                            }
                            else
                            {
                             SavePage1Data.insertIntoOpportunity().then(
                                function(res)
                                {
                                    SavePage1Data.insertIntoFHRMain().then(
                                        function(res)
                                        {
                                            SavePage1Data.getData(context.SalesChannel,  context.CustomerSegmentSelect,context.Dob, context.rdValue,context.Title,((!!context.AccountOpeningDate)?CommonService.formatDobToDb(context.AccountOpeningDate):null),context.FName,context.BankAccountNo,context.MiddleName,context.LName,
                                            context.Relationship,context.MobileNo,context.CurrentLifeStage,context.accountType,context.ifscCode,context.branchName,"insert").then(
                                            function(res){
                                                if(res=="success"){
                                                     SavePage1Data.UpdateFFPage1Status().then(
                                                     function(res){
                                                        FFTimelineService.isMndFieldsTabComplete = true;
                                                        $state.go(FACT_FINDER_STATE1);
                                                     });
                                                }
                                            });
                                         });
                                });
                            }
                        });

            }
        }
        else
        {
            SavePage1Data.checkIfFHRExist().then(
            function(res)
            {
              if(context.SalesChannel==undefined ||context.CustomerSegmentSelect==undefined ||context.Title==undefined ||context.Dob=="" ||context.FName==""||context.LName==""||context.MobileNo==""||context.CurrentLifeStage==undefined || context.rdValue==null || context.CurrentLifeStage=="Select" || context.CustomerSegmentSelect == "Select" || context.SalesChannel == "Select" || context.CurrentLifeStage=="" || context.CustomerSegmentSelect == "" ||context.SalesChannel == ""
              || context.Title == undefined|| context.Title=="" ||  context.Title=="Select"||context.SalesChannel == undefined|| context.SalesChannel=="" ||  context.SalesChannel=="Select" || context.CustomerSegmentSelect == undefined|| context.CustomerSegmentSelect=="" ||  context.CustomerSegmentSelect=="Select")
              {
                navigator.notification.alert("Please fill all the mandatory data!!",null,"Fact Finder","OK");
              }
              else
              {
                if(ffPage1Form.$invalid!=true){
                  if(!!res && res.rows.length>0){
                                   SavePage1Data.getData(context.SalesChannel,  context.CustomerSegmentSelect,context.Dob, context.rdValue,context.Title,null,context.FName,null,context.MiddleName,context.LName,
                                    null,context.MobileNo,context.CurrentLifeStage,null,null,null,"update").then(
                                    function(res){
                                        if(res=="success"){
                                            SavePage1Data.UpdateFFPage1Status().then(
                                            function(res){
                                               FFTimelineService.isMndFieldsTabComplete = true;
                                               $state.go(FACT_FINDER_STATE1);
                                            });
                                        }
                                     });

                                 }
                                 else
                                 {
                                    SavePage1Data.insertIntoOpportunity().then(
                                    function(res)
                                    {
                //                        if(!!res && res.rows.length>0){
                                        SavePage1Data.insertIntoFHRMain().then(
                                            function(res)
                                            {
                //                                if(!!res && res.rows.length>0){
                                                    SavePage1Data.getData(context.SalesChannel,  context.CustomerSegmentSelect,context.Dob, context.rdValue,context.Title,null,context.FName,null,context.MiddleName,context.LName,
                                                    null,context.MobileNo,context.CurrentLifeStage,null,null,null,"insert").then(
                                                    function(res){
                                                        if(res=="success"){
                                                            SavePage1Data.UpdateFFPage1Status().then(
                                                            function(res){
                                                               FFTimelineService.isMndFieldsTabComplete = true;
                                                               $state.go(FACT_FINDER_STATE1);
                                                            });
                                                        }
                                                     });
                //                                }
                                            });
                //                        }
                                    });

                                 }
                    }
                    else
                    {
                       navigator.notification.alert("Please fill all the mandatory data!!",null,"Fact Finder","OK");
                    }
              }

               });
        }
        }
        else
        {

            navigator.notification.alert("Please fill all the mandatory data!!",null,"Fact Finder","OK");
        }
       }

    }]);
    ffaModule1.service('SavePage1Data',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){

    this.getData = function(SalesChannel,CustomerSegmentSelect,Dob,rdValue,Title,AccountOpeningDate,FName,BankAccountNo,MiddleName,LName,Relationship,MobileNo,CurrentLifeStage,accountType,ifscCode,branchName,flag){
        var temp="";
        var self=this;
        var dob=CommonService.formatDobToDb(Dob);
        var Age=CommonService.getAge(dob);
        console.log("Age "+Age);
        if (Age<=25) {
            temp = "Less than or equal to 25 year";
        } else if (Age>25 && Age<=35) {
            temp = "26-35 year";
        }else if (Age>35 && Age<=45) {
            temp = "36-45 year";
        }else if (Age>45 && Age<=55) {
            temp = "46-55year";
        }else if (Age>55 ) {
            temp = "56 year and above";
        }
        var bankName="";
        if(rdValue=="Y")
            bankName="IndusInd Bank";
        console.log("Age bucket"+temp);
        console.log("FHR ID"+factFinderService.getFHRId());
        console.log("Lead Id"+factFinderService.getleadId());
        var dfd = $q.defer();
        try{
            if(flag=="insert")
            {
                CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"insert into LP_FHR_FFNA (FHR_ID,AGENT_CD,SALES_CHNL_SEGMENT,CONSUMER_SEGMENT,CONSUMER_TITLE,CONSUMER_FIRST_NAME,CONSUMER_MIDDLE_NAME,CONSUMER_LAST_NAME,CONSUMER_BIRTH_DATE,CONSUMER_AGE,CONSUMER_AGE_BUCKET,EXIST_BANK_CUST_FLAG,EXIST_BANK_AC_OPEN_DATE,EXIST_BANK_ACCOUNT_NO,BANK_RELATION_TYPE,MOBILE_NO,LIFE_STAGE_STATUS,EXIST_BANK_NAME,EXIST_BANK_IFSC_CODE,EXIST_BANK_BRANCH_NAME,EXIST_BANK_ACCOUNT_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    [factFinderService.getFHRId(),factFinderService.getAgentCD(),SalesChannel,CustomerSegmentSelect,Title,FName,MiddleName,LName,(!!Dob)?(CommonService.formatDobToDb(Dob)):null,Age,temp,rdValue,AccountOpeningDate,BankAccountNo,Relationship,MobileNo,CurrentLifeStage,bankName,ifscCode,branchName,accountType],
                    function(tx,res){
                        console.log("Data added successfully! " +res);
                        factFinderService.setFHRPage1Data(SalesChannel,CustomerSegmentSelect,Title,FName,MiddleName,LName,MobileNo,rdValue,CommonService.formatDobToDb(Dob),AccountOpeningDate,BankAccountNo,Relationship,accountType,ifscCode,branchName,CurrentLifeStage);
                        dfd.resolve("success");
                    },
                    function(tx,err){
                        console.log("Error in doUpdateAgentDetails().transaction(): " + err.message);
                        dfd.resolve(null);
                    });
                },
                function(err){
                    console.log("DataBase Error in InsertToMylead_SelfLead1: " + err.message);
                    dfd.resolve(null);
                },null
                );
            }
            else
            {
                CommonService.transaction(db,
                function(tx){
                    console.log("Update Querry :" +"update LP_FHR_FFNA  set SALES_CHNL_SEGMENT = '"+SalesChannel+"' , CONSUMER_SEGMENT = '"+CustomerSegmentSelect+"' , CONSUMER_TITLE = '"+Title+"' , CONSUMER_FIRST_NAME = '"+FName+"' , CONSUMER_MIDDLE_NAME = '"+MiddleName+"' , CONSUMER_LAST_NAME = '"+LName+"' , CONSUMER_BIRTH_DATE = '"+CommonService.formatDobToDb(Dob)+"' , CONSUMER_AGE = '"+Age+"' , CONSUMER_AGE_BUCKET = '"+temp+"' , EXIST_BANK_CUST_FLAG = '"+rdValue+"' , EXIST_BANK_AC_OPEN_DATE = '"+AccountOpeningDate+"' , EXIST_BANK_ACCOUNT_NO = '"+BankAccountNo+"' , BANK_RELATION_TYPE = '"+Relationship+"' , MOBILE_NO = '"+MobileNo+"' , LIFE_STAGE_STATUS = '"+CurrentLifeStage+"' ,EXIST_BANK_NAME = '"+bankName+"' , EXIST_BANK_IFSC_CODE = '"+ifscCode+"' , EXIST_BANK_BRANCH_NAME = '"+branchName+"' , EXIST_BANK_ACCOUNT_TYPE = '"+accountType+"' WHERE FHR_ID = ? ");
                    CommonService.executeSql(tx,"update LP_FHR_FFNA  set SALES_CHNL_SEGMENT = ? , CONSUMER_SEGMENT = ? , CONSUMER_TITLE = ? , CONSUMER_FIRST_NAME = ? , CONSUMER_MIDDLE_NAME = ? , CONSUMER_LAST_NAME = ? , CONSUMER_BIRTH_DATE = ? , CONSUMER_AGE = ? , CONSUMER_AGE_BUCKET = ? , EXIST_BANK_CUST_FLAG = ? , EXIST_BANK_AC_OPEN_DATE = ? , EXIST_BANK_ACCOUNT_NO = ? , BANK_RELATION_TYPE = ? , MOBILE_NO = ? , LIFE_STAGE_STATUS = ? , EXIST_BANK_NAME = ? , EXIST_BANK_IFSC_CODE = ? , EXIST_BANK_BRANCH_NAME = ? , EXIST_BANK_ACCOUNT_TYPE = ? WHERE FHR_ID = ? ",
                    [SalesChannel,CustomerSegmentSelect,Title,FName,MiddleName,LName,CommonService.formatDobToDb(Dob),Age,temp,rdValue,AccountOpeningDate,BankAccountNo,Relationship,MobileNo,CurrentLifeStage,"",ifscCode,branchName,accountType,factFinderService.getFHRId()],
//                    [SalesChannel,CustomerSegmentSelect,Title,FName,MiddleName,LName,CommonService.formatDobToDb(Dob),Age,temp,rdValue,AccountOpeningDate,BankAccountNo,Relationship,MobileNo,CurrentLifeStage,"",ifscCode,branchName,accountType,factFinderService.getFHRId()],
                    function(tx,res){
                        console.log("Data updated successfully! " +res);
                        factFinderService.setFHRPage1Data(SalesChannel,CustomerSegmentSelect,Title,FName,MiddleName,LName,MobileNo,rdValue,CommonService.formatDobToDb(Dob),AccountOpeningDate,BankAccountNo,Relationship,accountType,ifscCode,branchName,CurrentLifeStage);
                        dfd.resolve("success");
                    },
                    function(tx,err){
                        console.log("Error in doUpdateAgentDetails().transaction(): " + err.message);
                        dfd.resolve(null);
                    });
                },
                function(err){
                    console.log("DataBase Error in InsertToMylead_SelfLead1: " + err.message);
                    dfd.resolve(null);
                },null
                );
            }

        }catch(ex){
            console.log("Exception in InsertToMylead_SelfLead1(): " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    };
    this.checkIfFHRExist=function(){
                var dfd = $q.defer();
                CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"SELECT * from LP_FHR_FFNA where FHR_ID = '"+factFinderService.getFHRId()+"'",[],
                    function(tx,res){
                            dfd.resolve(res);
                    },
                    function(tx,err){
                        console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                    );
                },
                function(err){
                    console.log("Error in fetchErrDescMasterData(): " + err.message);
                    dfd.resolve(null);
                    },null
                );
                return dfd.promise;
         };
    this.getFHRData=function(){
            debug("FHR ID: "+factFinderService.getFHRId());
            var dfd = $q.defer();
            CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT * from LP_FHR_FFNA where FHR_ID = '"+factFinderService.getFHRId()+"'",[],
                function(tx,res){
                        dfd.resolve(res);
                },
                function(tx,err){
                    console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
                    dfd.resolve(null);
                }
                );
            },
            function(err){
                console.log("Error in fetchErrDescMasterData(): " + err.message);
                dfd.resolve(null);
                },null
            );
            return dfd.promise;
     };
     this.getFHRMainData=function(){
                 debug("FHR ID: "+factFinderService.getFHRId());
                 var dfd = $q.defer();
                 CommonService.transaction(db,
                 function(tx){
                     CommonService.executeSql(tx,"SELECT * from LP_FHR_MAIN where FHR_ID = '"+factFinderService.getFHRId()+"'",[],
                     function(tx,res){
                             dfd.resolve(res);
                     },
                     function(tx,err){
                         console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
                         dfd.resolve(null);
                     }
                     );
                 },
                 function(err){
                     console.log("Error in fetchErrDescMasterData(): " + err.message);
                     dfd.resolve(null);
                     },null
                 );
                 return dfd.promise;
          };
    this.getLeadData=function(){
            var dfd = $q.defer();
            CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT * from LP_MYLEAD where IPAD_LEAD_ID = '"+factFinderService.getleadId()+"'",[],
                function(tx,res){
                        console.log("Lead DATA : "+JSON.stringify(res));

                        dfd.resolve(res);
                },
                function(tx,err){
                    console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
                    dfd.resolve(null);
                }
                );
            },
            function(err){
                console.log("Error in fetchErrDescMasterData(): " + err.message);
                dfd.resolve(null);
                },null
            );
            return dfd.promise;
     };
     this.insertIntoOpportunity=function(){
            var dfd = $q.defer();
            CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"insert into LP_MYOPPORTUNITY (LEAD_ID,FHR_ID,AGENT_CD,PROPOSER_NAME,OPPORTUNITY_ID,LAST_UPDATED_DATE) VALUES (?,?,?,?,?,?)",
                                    [factFinderService.getleadId(),factFinderService.getFHRId(),factFinderService.getAgentCD(),factFinderService.getleadName(),factFinderService.getOppId(),CommonService.getCurrDate()],
                        function(tx,res){
                        console.log("inserted into LP_MYOPPORTUNITY");
                        dfd.resolve(res);
                },
                function(tx,err){
                    console.log("Error in insertintoOpportunity.transaction(): " + err.message);
                    dfd.resolve(null);
                }
                );
            },
            function(err){
                console.log("Error in fetchErrDescMasterData(): " + err.message);
                dfd.resolve(null);
                },null
            );
            return dfd.promise;
     };
    this.insertIntoFHRMain=function(){
            var dfd = $q.defer();
            CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"insert into LP_FHR_MAIN (LEAD_ID,FHR_ID,AGENT_CD) VALUES (?,?,?)",
                                    [factFinderService.getleadId(),factFinderService.getFHRId(),factFinderService.getAgentCD()],
                        function(tx,res){
                        console.log("inserted into LP_FHR_MAIN");
                        dfd.resolve(res);
                },
                function(tx,err){
                    console.log("Error in insertintoOpportunity.transaction(): " + err.message);
                    dfd.resolve(null);
                }
                );
            },
            function(err){
                console.log("Error in fetchErrDescMasterData(): " + err.message);
                dfd.resolve(null);
                },null
            );
            return dfd.promise;
     };
    this.getMasterData=function(){
        var dfd = $q.defer();
        CommonService.transaction(db,
        function(tx){
            CommonService.executeSql(tx,"SELECT LOV_CD,LOV_VALUE FROM LP_LEAD_LOV_MASTER WHERE LOV_TYPE=? ORDER BY LOV_VALUE ASC",['Income group'],
            function(tx,res){
                if(!!res && res.rows.length>0){
                    var IncGrpArrList = [];
                    console.log("res.rows.length: " + res.rows.length);
                    for(var i=0;i<res.rows.length;i++){
                        IncGrpObj = {};
                        IncGrpObj.LOV_VALUE = res.rows.item(i).LOV_VALUE;
                        IncGrpObj.LOV_CD = res.rows.item(i).LOV_CD;
                        IncGrpArrList.push(IncGrpObj);
                    }
                    dfd.resolve(IncGrpArrList);
                }
                else{
                    console.log("lp_error_desc_master is empty");
                    dfd.resolve(null);
                }
            },
            function(tx,err){
                console.log("Error in fetchErrDescMasterData().transaction(): " + err.message);
                dfd.resolve(null);
            }
            );
        },
        function(err){
            console.log("Error in fetchErrDescMasterData(): " + err.message);
            dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };
	 this.UpdateFFPage1Status = function(){
	 	var dfd = $q.defer();
	 	try{
	 		CommonService.transaction(db,
	 		function(tx){
	 			console.log("update LP_FHR_MAIN set IS_SCREEN1_DONE = 'Y'  where FHR_ID= ? ");
	 			CommonService.executeSql(tx,"update LP_FHR_MAIN set IS_SCREEN1_DONE = 'Y' where FHR_ID= ? ",[factFinderService.getFHRId()],
	 			function(tx,res){
	 				debug(" updated LP_FHR_FFNA1 Status");
	 				dfd.resolve("success");
	 			},
	 			function(tx,err){
	 				navigator.notification.alert("Error!!",null,"Fact Finder","OK");
	 				debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
	 				dfd.resolve(null);
	 			});
	 		},
	 		function(err){
	 			debug("Error in LP_FHR_FFNA(): " + err.message);
	 			dfd.resolve(null);
	 		},null
	 		);
	 	}catch(ex){
	 		debug("Exception in LP_FHR_FFNA(): " + ex.message);
	 		dfd.resolve(null);
	 	}
	 return dfd.promise;
	 };
    this.getCurrentDate=function() {
        var todayTime = new Date();
        todayTime.setDate(todayTime.getDate()+1);
        var tempMonth=todayTime.getMonth()+1;
        var month = tempMonth>9?tempMonth:("0"+tempMonth);
        var day = todayTime .getDate();
        var year = todayTime .getFullYear();
        return (year + "-" + month + "-" + day);

    };
}]);
