ffaModule.controller('FFTimelineCtrl',['FFTimelineService', '$state', function(FFTimelineService, $state){
	"use strict";
	debug("in FFTimelineCtrl");
	this.timelineObj = [
		{'name': 'Mandatory', 'name2': 'Fields', 'number': '1', 'type': 'mndFields', 'isTabComplete': FFTimelineService.isMndFieldsTabComplete},
		{'name': 'Need Identification', 'name2': '& Prioritization', 'number': '2', 'type': 'needId', 'isTabComplete': FFTimelineService.isNeedIdTabComplete},
		{'name': 'Prefered Payout Option', 'name2': '& Risk Assessment', 'number': '3', 'type': 'prefPayout', 'isTabComplete': FFTimelineService.isPrefPayoutTabComplete},
	 	{'name': 'Product', 'name2': 'Recommendations', 'number': '4', 'type': 'prodRec', 'isTabComplete': FFTimelineService.isProdRecTabComplete},
	 	{'name': 'Fact Finder', 'name2': 'Output', 'number': '5', 'type': 'ffOutput', 'isTabComplete': FFTimelineService.isFFOutputTabComplete},
	];
	debug("this.timelineObj: " + JSON.stringify(this.timelineObj));
	this.activeTab = FFTimelineService.getActiveTab();

	this.gotoMndFieldsTab = function(){
		if(!!FFTimelineService.isMndFieldsTabComplete)
			$state.go(FACT_FINDER_STATE);
	};

	this.gotoNeedIdTab = function(){
		if(!!FFTimelineService.isNeedIdTabComplete)
			$state.go(FACT_FINDER_STATE1);
	};

	this.gotoPrefPayoutTab = function(){
		if(!!FFTimelineService.isPrefPayoutTabComplete)
			$state.go(FACT_FINDER_STATE2);
	};

	this.gotoProdRecTab = function(){
		if(!!FFTimelineService.isProdRecTabComplete)
			$state.go(FACT_FINDER_STATE3);
	};

	this.gotoFFOutputTab = function(){
		if(!!FFTimelineService.isFFOutputTabComplete)
			$state.go("ffPageOutput");
	};



	this.onTabClick = function(type){
		debug("type: " + type);
		if(type=="mndFields")
			this.gotoMndFieldsTab();
		else if(type=="needId")
			this.gotoNeedIdTab();
		else if(type=="prefPayout")
			this.gotoPrefPayoutTab();
		else if(type=="prodRec")
			this.gotoProdRecTab();
		else if(type=="ffOutput")
			this.gotoFFOutputTab();
	};

}]);

ffaModule.service('FFTimelineService', [function(){
	"use strict";
	debug("in FFTimelineService");

	this.activeTab = "mndFields";
	this.isMndFieldsTabComplete = false;
	this.isNeedIdTabComplete = false;
	this.isPrefPayoutTabComplete = false;
	this.isProdRecTabComplete = false;
	this.isFFOutputTabComplete = false;

	this.getActiveTab = function(){
		return this.activeTab;
	};
	this.setActiveTab = function(activeTab){
		this.activeTab = activeTab;
	};
	this.reset=function(){
		this.isMndFieldsTabComplete = false;
		this.isNeedIdTabComplete = false;
		this.isPrefPayoutTabComplete = false;
		this.isProdRecTabComplete = false;
		this.isFFOutputTabComplete = false;
	};
}]);

ffaModule.directive('ffTimeline',[function() {
	"use strict";
	debug('in ffTimeline');
	return {
		restrict: 'E',
		controller: "FFTimelineCtrl",
		controllerAs: "ft",
		bindToController: true,
		templateUrl: "factFinder/ffTimeline.html"
	};
}]);

