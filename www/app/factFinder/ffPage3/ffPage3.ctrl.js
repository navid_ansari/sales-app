ffaModule3.controller('ffPage3Ctrl',['$state','ffPage3Service','factFinderService', 'CommonService','FFTimelineService', function($state,ffPage3Service,factFinderService, CommonService,FFTimelineService){
	"use strict";
	CommonService.hideLoading();
                                     
    
    /* Header Hight Calculator */
    setTimeout(function(){
               var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
               console.log("getHeight Test : " + factFinderAnalysisCalHeight);
               $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    });
    /* End Header Hight Calculator */

                                     
	FFTimelineService.setActiveTab("prefPayout");
	var page3Data=factFinderService.getFHRPage3Data();
	this.rdInvest1=null;
	var context=this;
	context.click = false;
	this.EquityFund=page3Data.EquityFund == null ? 0 :parseInt(page3Data.EquityFund);
	this.BalancedFund=page3Data.BalancedFund == null ? 0 :parseInt(page3Data.BalancedFund);
	this.DebtFund=page3Data.DebtFund == null ? 0 :parseInt(page3Data.DebtFund);
	this.Total=page3Data.Total == null ? 0 :parseInt(page3Data.Total);
	this.invest=page3Data.invest;
	this.rdInvesting=page3Data.rdInvesting;
	this.payoption=page3Data.payoption;
	var fundError=null;
	this.fundError="";
	this.Backbtn3 = function(){
		$state.go(FACT_FINDER_STATE1);
	};
	this.openMenu = function(){
		CommonService.openMenu();
	};
	this.Changed=function(){
           this.EquityFund="";
           this.BalancedFund="";
           this.DebtFund="";
           this.Total="";
           this.fundError="";

        }
	this.checkEquityVal=function(){
		var equityVal=this.EquityFund;
		var balancedFund=this.BalancedFund;
		var debtFund=this.DebtFund;
		if(equityVal==null)
			equityVal=0;
		if(balancedFund==null)
			balancedFund=0;
		if(debtFund==null)
			debtFund=0;
		var tot=equityVal+balancedFund+debtFund;
		if(equityVal>100)
		{
			this.EquityFund=0;
			tot=equityVal+balancedFund+debtFund;
			this.fundError="Invalid Total";
		}
		else if(tot!==100)
		{
			this.fundError="Invalid Total";
		}
		else if(tot==100)
		{
			this.fundError="";
		}
		else if(tot<100)
		{
			this.fundError="Invalid Total";
		}
		else if(equityVal==null)
		{
			var tot=balancedFund+debtFund;
			this.fundError="";
		}
		else if(balancedFund==null)
		{
			var tot=equityVal+debtFund;
			this.fundError="";
		}
		else if(debtFund==null)
		{
			var tot=balancedFund+equityVal;
			this.fundError="";
		}
		else
		{
			this.fundError="";
		}
	this.Total=tot;
	};
	this.checkBalanceVal=function(){
		var equityVal=this.EquityFund;
		var balancedFund=this.BalancedFund;
		var debtFund=this.DebtFund;
		if(equityVal==null)
			equityVal=0;
		if(balancedFund==null)
			balancedFund=0;
		if(debtFund==null)
			debtFund=0;
		var tot=equityVal+balancedFund+debtFund;
		if(balancedFund>100)
		{
			this.balancedFund=0;
			tot=equityVal+balancedFund+debtFund;
			this.fundError="Invalid Total";
		}
		else if(tot!==100)
		{
			this.fundError="Invalid Total";
		}
		else if(tot==100)
		{
			this.fundError="";
		}
		else if(equityVal==null)
		{
			var tot=balancedFund+debtFund;
			this.fundError="";
		}
		else if(balancedFund==null)
		{
			var tot=equityVal+debtFund;
			this.fundError="";
		}
		else if(debtFund==null)
		{
			var tot=balancedFund+equityVal;
			this.fundError="";
		}
		else
		{
			this.fundError="";
		}
	this.Total=tot;

	};
	this.checkDebtVal=function(){
		var equityVal=this.EquityFund;
		var balancedFund=this.BalancedFund;
		var debtFund=this.DebtFund;
		if(equityVal==null)
			equityVal=0;
		if(balancedFund==null)
			balancedFund=0;
		if(debtFund==null)
			debtFund=0;
		var tot=equityVal+balancedFund+debtFund;
		if(debtFund>100)
		{
			this.debtFund=0;
			tot=equityVal+balancedFund+debtFund;
			this.fundError="Invalid Total";
		}
		else if(tot==100)
		{
			this.fundError="";
		}
		else if(tot!==100)
		{
			this.fundError="Invalid Total";
		}
		else if(equityVal==null)
		{
			var tot=balancedFund+debtFund;
			this.fundError="";
		}
		else if(balancedFund==null)
		{
			var tot=equityVal+debtFund;
			this.fundError="";
		}
		else if(debtFund==null)
		{
			var tot=balancedFund+equityVal;
			this.fundError="";
		}
		else
		{
			this.fundError="";
		}
	this.Total=tot;
	};
this.ContSavePage5 = function(){
	context.click = true;
	console.log("Total"+this.Total);
	if(((!this.Total)||(this.Total!==100)) && this.rdInvesting=="Y")
	{
		navigator.notification.alert("Total should be equal to 100%",null,"Fact Finder","OK");
	}
	else
	{

		if(this.payoption=="" || this.rdInvesting=="" || this.rdInvesting==undefined || this.invest=="Select" || this.invest==undefined ||  this.invest == "")
		{
			navigator.notification.alert("Please fill all mandatory data!!",null,"Fact Finder","OK");
		}
		else{
			if(this.EquityFund==null)
				this.EquityFund=0;
			if(this.BalancedFund==null)
				this.BalancedFund=0;
			if(this.DebtFund==null)
				this.DebtFund=0;
			if(this.rdInvesting=="Y")
			{
				ffPage3Service.getDataPage5(this.payoption,this.invest,this.rdInvesting,this.EquityFund,this.BalancedFund,this.DebtFund,this.Total).then(
				function(res){
				if(res=="success"){
					fundError="";
					ffPage3Service.UpdateFFPage3Status().then(
					function(res){
					   FFTimelineService.isPrefPayoutTabComplete = true;
					   $state.go(FACT_FINDER_STATE3);
					});
				}
				});
			}
			else
			{
			ffPage3Service.getDataPage5(this.payoption,this.invest,this.rdInvesting,"0","0","0","0").then(
			function(res){
				if(res=="success"){
					fundError="";
					ffPage3Service.UpdateFFPage3Status().then(
					function(res){
					   FFTimelineService.isPrefPayoutTabComplete = true;
					   $state.go(FACT_FINDER_STATE3);
					});
				}
			});
			}
		}
	}
	};
}]);

ffaModule3.service('ffPage3Service',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){
		this.getDataPage5 = function(payoption,rdInvest1,rdInvesting,EquityFund,BalancedFund,DebtFund,Total){
			var dfd = $q.defer();
			console.log("EquityFund"+EquityFund);
			console.log("BalancedFund"+BalancedFund);
			console.log("DebtFund"+DebtFund);
			console.log("DebtFund"+DebtFund);
			try{
				CommonService.transaction(db,
				function(tx){
					console.log("update LP_FHR_FFNA set PREF_PAYOUT_OPTION = '"+ payoption +"' , RISK_ASMNT_EQUITY_PCT = '"+rdInvest1+"' , CMFRT_ULIP_INVEST_FLAG = '"+rdInvesting+"' , ULIP_ALOC_EQUITY_PCT = '"+EquityFund +"' , ULIP_ALOC_BALANCED_PCT= '"+ BalancedFund+"' , ULIP_ALOC_DEBT_PCT= '"+ DebtFund +"'  where FHR_ID= ? ");
					CommonService.executeSql(tx,"update LP_FHR_FFNA set PREF_PAYOUT_OPTION = ? , RISK_ASMNT_EQUITY_PCT =? , CMFRT_ULIP_INVEST_FLAG = ? , ULIP_ALOC_EQUITY_PCT = ? , ULIP_ALOC_BALANCED_PCT= ? , ULIP_ALOC_DEBT_PCT= ?  where FHR_ID= ? ",[payoption,rdInvest1,rdInvesting,EquityFund,BalancedFund,DebtFund,factFinderService.getFHRId()],
//					CommonService.executeSql(tx,"insert or replace into LP_FHR_FFNA (PREF_PAYOUT_OPTION ,RISK_ASMNT_EQUITY_PCT  , CMFRT_ULIP_INVEST_FLAG , ULIP_ALOC_EQUITY_PCT, ULIP_ALOC_BALANCED_PCT, ULIP_ALOC_DEBT_PCT, FHR_ID , AGENT_CD) VALUES (?,?,?,?,?,?,?,?) ",
//					[payoption , rdInvest1, rdInvesting , EquityFund , BalancedFund , DebtFund,factFinderService.getFHRId(),"123"],
					function(tx,res){
						factFinderService.setFHRPage3Data(payoption , rdInvest1, rdInvesting , EquityFund , BalancedFund , DebtFund,Total)
						debug("successfully updated LP_FHR_FFNA");
						dfd.resolve("success");
					},
					function(tx,err){
						navigator.notification.alert("Error!!",null,"Fact Finder","OK");
						debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
						dfd.resolve(null);
					});
				},
				function(err){
					debug("Error in LP_FHR_FFNA(): " + err.message);
					dfd.resolve(null);
				},null
				);
			}catch(ex){
				debug("Exception in LP_FHR_FFNA(): " + ex.message);
				dfd.resolve(null);
			}
		return dfd.promise;
		};
	 this.UpdateFFPage3Status = function(){
		var dfd = $q.defer();
		try{
			CommonService.transaction(db,
			function(tx){
				console.log("update LP_FHR_MAIN set IS_SCREEN3_DONE = 'Y'  where FHR_ID= ? ");
				CommonService.executeSql(tx,"update LP_FHR_MAIN set IS_SCREEN3_DONE = 'Y' where FHR_ID= ? ",[factFinderService.getFHRId()],
				function(tx,res){
					debug(" updated LP_FHR_FFNA3 Status");
					dfd.resolve("success");
				},
				function(tx,err){
					navigator.notification.alert("Error!!",null,"Fact Finder","OK");
					debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
					dfd.resolve(null);
				});
			},
			function(err){
				debug("Error in LP_FHR_FFNA(): " + err.message);
				dfd.resolve(null);
			},null
			);
		}catch(ex){
			debug("Exception in LP_FHR_FFNA(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};
}]);
