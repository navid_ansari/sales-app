ffaModule5.controller('ffPage5Ctrl',['$state','SavePage5Data','factFinderService', 'CommonService',function($state,SavePage5Data,factFinderService,CommonService){
	"use strict";
	CommonService.hideLoading();
                                     
    /* Header Hight Calculator */
    setTimeout(function(){
               var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
               console.log("getHeight Test : " + factFinderAnalysisCalHeight);
               $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    });
    /* End Header Hight Calculator */
                                     
                                     
    var self=this;


        this.ContSavePage5 = function(){
            SavePage5Data.getDataPage7(self.rdInvest).then(
                  function(res){

                           $state.go("ffPageOutput");



                 });
        };
        this.BackPage5= function(){
                   $state.go(FACT_FINDER_STATE3);
                          };

}]);

ffaModule5.service('SavePage5Data',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){
var self = this;
 this.getDataPage7 = function(rdInvest){
		var dfd = $q.defer();
		console.log("getFHRId : "+factFinderService.getFHRId());
		console.log("Selection : "+rdInvest);
		var flagAgree="";
		var flagDisAgree="";
		var flagPartial="";
		if(rdInvest=="The above recommendation is based on the information provided by me and as documented in this form. I have explained about the features of the products / and / funds / and believe it would be suitable for me based on my insurance needs and financial objectives")
		{
		    flagAgree="Y";
		    flagDisAgree="N";
		    flagPartial="N";
		}
		else if(rdInvest=="I do not agree with the above recommendation and have opted for alternate products for my need.")
		{
            flagAgree="N";
            flagDisAgree="Y";
            flagPartial="N";
		}
		else
		{
		    flagAgree="N";
        	flagDisAgree="N";
        	flagPartial="Y";
		}
		try{
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"update LP_FHR_FFNA set  CUST_CNT_AGREE = '"+flagAgree +"', CUST_CNT_NOT_AGREE ='"+ flagDisAgree +"', CUST_CNT_PARTIAL_AGREE ='"+flagPartial+"' where FHR_ID=?",[factFinderService.getFHRId()],
						function(tx,res){

							dfd.resolve("success");
						},
						function(tx,err){

							debug("Error in LP_FHR_FFNA().transaction(): " + err.message);
							dfd.resolve(null);
						}
					);
				},
				function(err){
					debug("Error in LP_FHR_FFNA(): " + err.message);
					dfd.resolve(null);
				},null
			);
		}catch(ex){
			debug("Exception in LP_FHR_FFNA(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};


}]);
