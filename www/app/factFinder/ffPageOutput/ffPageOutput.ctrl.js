ffaModuleOutput.controller('ffPageOutputCtrl',['$state','SavePageOutputData','factFinderService', 'CommonService','FFOutputFile','docId','FFTimelineService',function($state,SavePageOutputData,factFinderService,CommonService,FFOutputFile,docId,FFTimelineService){
	CommonService.hideLoading();
                                               
    /* Header Hight Calculator */
    setTimeout(function(){
        var factFinderAnalysisCalHeight = $(".fact-finderanalysis .fixed-bar").height();
        console.log("getHeight Test : " + factFinderAnalysisCalHeight);
        $(".fact-finderanalysis .custom-position").css({top:factFinderAnalysisCalHeight + "px"});
    });
    /* End Header Hight Calculator */
                                               
                                               
	FFTimelineService.setActiveTab("ffOutput");
	this.isSigned=SavePageOutputData.isSigned;
	this.doSign = function(){
		SavePageOutputData.getSign();

	};
	this.openMenu = function(){
		CommonService.openMenu();
	};
	this.doSave=function(){
		SavePageOutputData.saveData().then(
			function(res){
				alert("Data Saved Successfully!!");
			}
		);
	};
	factFinderService.setDocId(docId);
	debug("Get Doc Id"+docId);
	this.gotoLeadStatusPage = function(){
		SavePageOutputData.callProceed();
	};

}]);

ffaModuleOutput.service('SavePageOutputData',['$q', '$http', '$state','CommonService','factFinderService', function($q,$http,$state,CommonService,factFinderService){

	var self = this;
	var flag="N";
	this.FF_SIGNED=null;
	this.getAssetFile = function() {
        var dfd = $q.defer();
        debug("inside getFileAssets")
	    cordova.exec(
            function(fileContent){
                debug("fileContent : " + (!!fileContent)?(fileContent.length):null);
	            dfd.resolve(fileContent);
	        },
            function(errMsg){
    	        debug("inside cordova error :::"+errMsg);
	            dfd.resolve(null);
    	    },
			"PluginHandler","getDataFromFileInAssets",["www/templates/FF/FFOutput.html"]
       	);
        return dfd.promise;
    };
    this.sendDataToServer=function(mainObjData){
   		var dfd = $q.defer();
		CommonService.ajaxCall(FACTFINDER_SYNC_URL, AJAX_TYPE, TYPE_JSON, mainObjData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
		debug("FactFinder sync resp: " + JSON.stringify(ajaxResp));
		if(!!ajaxResp && !!ajaxResp.RS){
			dfd.resolve(ajaxResp);
		}
		else
			dfd.resolve(null);
		},
		function(data, status, headers, config, statusText){
			dfd.resolve(null);
		});
		return dfd.promise;
    };
	this.getSign=function(){
//		if(this.FF_SIGNED == false || this.FF_SIGNED == null){
			CommonService.doSign("ffCustSign");
           // debug("Is Signed",this.isSigned);
//		}
//		else{
//            alert("You have already signed!!");
//        }
	};
	this.callProceed=function(){
		if(this.FF_SIGNED == true ){
			var leadId = factFinderService.getleadId();
			var fhrId = factFinderService.getFHRId();
			var pglId = factFinderService.getPgl_id();
			this.FF_SIGNED=null;
			self.isSigned=null;
			console.log("FHR ID before LEADDASH :"+fhrId);
			$state.go("leadDashboard",{"FHR_ID": fhrId, "LEAD_ID": leadId, "PGL_ID": pglId});
		}
		else{
            navigator.notification.alert("Please Sign before proceed!!",null,"Application","OK");			
		}
	};
	this.saveData=function(){
    		var dfd = $q.defer();
    		this.saveFFReportFile().then(
    			function(res)
    			{
    				dfd.resolve(res);
    			});
    		return dfd.promise;
    	};
   	this.getFHRData = function(FHR_ID){ //Drastty
		try{
     		var dfd = $q.defer();
        	var FHRArray = [];
        	if(!FHR_ID)
              FHR_ID = factFinderService.getFHRId();

        	var FHRObj = {
        	FFNA : {},
        	MAIN : {}
        	};
        	db.transaction(
	            function(tx){/*factFinderService.getFHRId()*/
	                var query1 = "Select * from LP_FHR_FFNA where FHR_ID = '"+FHR_ID+"'";
	                var query2 = "Select * from LP_FHR_MAIN where FHR_ID = '"+FHR_ID+"'";
	                tx.executeSql(query1,[],
	                    function(tx,results){
	                       	if(results.rows.length > 0){
	                       	console.log("Result1 : " + JSON.stringify(results.rows.item(0)));
	                       	FHRObj.FFNA = CommonService.resultSetToObject(results);
	                       	debug("FFNA OBJ"+JSON.stringify(FHRObj.FFNA));
                            // dfd.resolve(results.rows.item(0));
	                       		tx.executeSql(query2,[],
										function(tx,results){
											if(results.rows.length > 0){
												FHRObj.MAIN = CommonService.resultSetToObject(results);
												console.log("Result2 : " + JSON.stringify(FHRObj));
												dfd.resolve(FHRObj);
											}
										},
										function(tx,err){

										}
									);
	                       	}
	                       	else {
	                           debug("No record of FHR ");
	                           dfd.resolve(null);
	                        }
	                    },
	                    function(tx,err){
	                    	debug("Error: " + err.message);
	                    	dfd.resolve(null);
	                    }
	            	);
	            },function(err){
	            	debug("Error: " + err.message);
	            	dfd.resolve(null);
	            },null
            );
          	return dfd.promise;
       	}
       	catch(ex){
    		debug("Exception in getFHRData(): " + ex.message);
    	}
  	};
  		this.getDocId = function(){
    		try{
         		var dfd = $q.defer();
            	db.transaction(
    	            function(tx){
    	                var query = "Select * from LP_DOC_PROOF_MASTER where CUSTOMER_CATEGORY = 'PR' AND MPDOC_CODE= 'FD' AND MPPROOF_CODE='FF'";
    	                tx.executeSql(query,[],
    	                    function(tx,results){
    	                       	if(results.rows.length > 0){
    	                       		console.log("Doc Id : " + JSON.stringify(results.rows.item(0).DOC_ID));
    	                       		dfd.resolve(results.rows.item(0).DOC_ID);
    	                       }
    	                       	else {
    	                           debug("No record of Doc ");
    	                           dfd.resolve(null);
    	                        }
    	                    },
    	                    function(tx,err){
    	                    	debug("Error: " + err.message);
    	                    	dfd.resolve(null);
    	                    }
    	            	);
    	            },function(err){
    	            	debug("Error: " + err.message);
    	            	dfd.resolve(null);
    	            },null
                );
              	return dfd.promise;
           	}
           	catch(ex){
        		debug("Exception in DocId(): " + ex.message);
        	}
      	};
	this.getRecommendedProduct = function(FHR_ID){
		try{
     		var dfd = $q.defer();
        	var FHRArray = [];
        	var FHRObj = {};
        	if(!FHR_ID)
        		FHR_ID = factFinderService.getFHRId();
        	db.transaction(
	            function(tx){

	                var query = "Select * from LP_FHR_PRODUCT_RCMND Where FHR_ID = '"+FHR_ID+"'";
	                console.debug("LP_FHR_PRODUCT_RCMND: "+query);
	                tx.executeSql(query,[],
	                    function(tx,results){
	                       	if(results.rows.length > 0){
	                            console.log("Result : " + JSON.stringify(results.rows.item(0)));
	                          	dfd.resolve(results.rows.item(0));
	                       	}
	                       	else {
	                           debug("No record of FHR ");
	                           dfd.resolve(null);
	                        }
	                    },
	                    function(tx,err){
	                    	debug("Error: " + err.message);
	                    	dfd.resolve(null);
	                    }
	            	);
	            },function(err){
	            	debug("Error: " + err.message);
	            	dfd.resolve(null);
	            },null
            );
          	return dfd.promise;
       	}
       	catch(ex){
    		debug("Exception in getFHRData(): " + ex.message);
    	}
  	};
    this.generateFFOutput = function(){
    	var dfd = $q.defer();
    	this.getAssetFile().then(
    		function(fileData){
		    	self.getFHRData().then(
		    		function(FHRObj){
		    			debug("FHROBJ: " +JSON.stringify(FHRObj));
		    			var FHRArray = FHRObj.FFNA;
						self.HtmlFormOutput = fileData;
		                var dobDate=FHRArray.CONSUMER_BIRTH_DATE;
		                dobDate=dobDate.substr(0,10);
		                var tdate;
		                if(FHRArray.EXIST_BANK_AC_OPEN_DATE == null)
		                	tdate="";
		                else{
		                   	tdate=FHRArray.EXIST_BANK_AC_OPEN_DATE .substr(0,10);
		                    debug("tdate"+tdate);
		                }
		                var middleName=FHRArray.CONSUMER_MIDDLE_NAME ==null ? "" : FHRArray.CONSUMER_MIDDLE_NAME;
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||COUSTMER_NAME||",FHRArray.CONSUMER_FIRST_NAME+" "+middleName+" "+FHRArray.CONSUMER_LAST_NAME);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||DATE_OF_BIRTH||",dobDate);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||EXITING_IND_BANK_CUST||",FHRArray.EXIST_BANK_CUST_FLAG);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||INDBANK_ACCNO||",FHRArray.EXIST_BANK_ACCOUNT_NO == "null" ? "" : FHRArray.EXIST_BANK_ACCOUNT_NO);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ACC_OPENING_DATE||",FHRArray.EXIST_BANK_AC_OPEN_DATE == "null" ? "" : tdate);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||MOBILE_NO||",FHRArray.MOBILE_NO);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||TYPE_OF_RELATIONSHIP||",FHRArray.BANK_RELATION_TYPE =="null" ? "" : FHRArray.BANK_RELATION_TYPE);
		               // self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PROPOSAL_NO||",FHRArray.PROPOSAL_FORM_NO ==null ? "" : FHRArray.PROPOSAL_FORM_NO);
						if(!!FHRArray.CONSUMER_AGE_BUCKET){
							FHRArray.CONSUMER_AGE_BUCKET = CommonService.replaceAll(FHRArray.CONSUMER_AGE_BUCKET, "&", "&amp;");
							FHRArray.CONSUMER_AGE_BUCKET = CommonService.replaceAll(FHRArray.CONSUMER_AGE_BUCKET, "<", "&lt;");
							FHRArray.CONSUMER_AGE_BUCKET = CommonService.replaceAll(FHRArray.CONSUMER_AGE_BUCKET, ">", "&gt;");
							FHRArray.CONSUMER_AGE_BUCKET = CommonService.replaceAll(FHRArray.CONSUMER_AGE_BUCKET, '"', "&quot;");
						}
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||AGE_RANGE||",FHRArray.CONSUMER_AGE_BUCKET);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||CURRENT_LIFESTAGE||",FHRArray.LIFE_STAGE_STATUS);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||AGE_LS_IDENTIFIED||",FHRArray.CONSUMER_AGE);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||OCCUPATION||",FHRArray.OCCUPATION_DESC =="Other" ? FHRArray.OCCUPATION_OTHER : FHRArray.OCCUPATION_DESC);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||BUSINESS_TYPE||",FHRArray.OCCU_ORG_TYPE_DESC =="Other" ? FHRArray.OCCU_ORG_TYPE_OTHERS : FHRArray.OCCU_ORG_TYPE_DESC);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ANNUAL_INCOME_BUCKET||",FHRArray.ANNUAL_INCOME_BUCKET);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PROTECTION_RANK||",FHRArray.INVEST_GOAL_RANK_1);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PLAN_CHILD_RANK||",FHRArray.INVEST_GOAL_RANK_2);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||SAVING_RANK||",FHRArray.INVEST_GOAL_RANK_3);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||RETIRE_PLAN_RANK||",FHRArray.INVEST_GOAL_RANK_4);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PROTECTION_EXIST_INVEST_DETAILS||",FHRArray.EXIST_INVEST_DETLS_1==null ? "":FHRArray.EXIST_INVEST_DETLS_1);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PLAN_CHILD_EXIST_INVEST_DETAILS||",FHRArray.EXIST_INVEST_DETLS_2==null ? "":FHRArray.EXIST_INVEST_DETLS_2);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||SAVINGS_EXIST_INVEST_DETAILS||",FHRArray.EXIST_INVEST_DETLS_3==null ? "":FHRArray.EXIST_INVEST_DETLS_3);
		                self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||RETIRE_PLAN_INVEST_DETAILS||",FHRArray.EXIST_INVEST_DETLS_4==null ? "":FHRArray.EXIST_INVEST_DETLS_4);
						var value;
						if(FHRArray.INVEST_GOAL_RANK_1=="1")
							value="Protection against death";
						else if(FHRArray.INVEST_GOAL_RANK_2=="1")
							value="Planning for your child’s education";
						else if(FHRArray.INVEST_GOAL_RANK_3=="1")
							value="Saving to meet long-term goals";
						else
							value="Planning for regular income after retirement";
						var i1=FHRArray.EXIST_INVEST_DETLS_1==null ? 0:FHRArray.EXIST_INVEST_DETLS_1;
						var i2=FHRArray.EXIST_INVEST_DETLS_2==null ? 0:FHRArray.EXIST_INVEST_DETLS_2;
						var i3=FHRArray.EXIST_INVEST_DETLS_3==null ? 0:FHRArray.EXIST_INVEST_DETLS_3;
						var i4=FHRArray.EXIST_INVEST_DETLS_4==null ? 0:FHRArray.EXIST_INVEST_DETLS_4;
						var totalInvest=parseInt(i1)+parseInt(i2)+parseInt(i3)+parseInt(i4);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||INVEST TOTAL||",totalInvest);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||UNFULFILLED_NEED||",value);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ADVENTUROUR_OR_RISKAVERSE||",FHRArray.RISK_ASMNT_EQUITY_PCT);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||UNIT_LINK_INVEST_CHOICE||",FHRArray.CMFRT_ULIP_INVEST_FLAG);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ULIP_EQUITY_FUND_PERCT||",FHRArray.ULIP_ALOC_EQUITY_PCT ==null ? "":FHRArray.ULIP_ALOC_EQUITY_PCT);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ULIP_BAL_FUND_PERCT||",FHRArray.ULIP_ALOC_BALANCED_PCT ==null ? "":FHRArray.ULIP_ALOC_BALANCED_PCT);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ULIP_DEBT_FUND_PERCT||",FHRArray.ULIP_ALOC_DEBT_PCT ==null ? "":FHRArray.ULIP_ALOC_DEBT_PCT);
						if((FHRArray.ULIP_ALOC_EQUITY_PCT == "0") && (FHRArray.ULIP_ALOC_DEBT_PCT =="0") && (FHRArray.ULIP_ALOC_BALANCED_PCT == "0"))
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||TOTAL||","0");
						else
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||TOTAL||","100");
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||PREFERED_PAYOUT_OPTION||",FHRArray.PREF_PAYOUT_OPTION);
						if(FHRArray.RISK_ASMNT_EQUITY_PCT=="Equity 0% and Debt 100")
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||ADVENTUROUR||","RISK AVERSE");
						var page4DataProduct=factFinderService.getFHRPage4Data1();
						var page4DataFund=factFinderService.getFHRPage4Data2();
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||EQUITY_DEBT_PERCENTAGE||",FHRArray.RISK_ASMNT_EQUITY_PCT);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||RECOMENDED_PRODUCT||",page4DataProduct.PRODUCT_NAME);
						self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||RECOMENDED_FUND||",page4DataFund.FUND_DESCRIPTION);
						if(FHRArray.CUST_CNT_AGREE=="Y")
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||CUSTOMER_CONSENT||","The above recommendation is based on the information provided by me and as documented in this form. I have explained about the features of the products / and / funds / and believe it would be suitable for me based on my insurance needs and financial objectives");
						else if(FHRArray.CUST_CNT_NOT_AGREE=="Y"){
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||CUSTOMER_CONSENT||","I do not agree with the above recommendation and have opted for alternate products for my need.");
							flag="Y";
						}

						else if(FHRArray.CUST_CNT_PARTIAL_AGREE=="Y")
							self.HtmlFormOutput = CommonService.replaceAll(self.HtmlFormOutput,"||CUSTOMER_CONSENT||","I agree with the product recommendation made above but DO NOT agree with the choice of fund ( only for ULIP products.)");
						debug("Self HTML "+self.HtmlFormOutput);
						factFinderService.ffData = self.HtmlFormOutput;
						dfd.resolve(self.HtmlFormOutput);
		    		}
		    	);
			}
		);
		return dfd.promise;
    };

    this.getFFData = function(){
    	return factFinderService.ffData;
    };

	this.saveCustSign = function(sign){
		debug("saveCustSignature: " + ((!!sign)?(sign.length):null));
		if(!!factFinderService.ffData){
			factFinderService.ffData = CommonService.replaceAll(factFinderService.ffData,'||CUSTOMER_SIGN||','<img src="' + sign + '" width="150px" height="50px"/>');
			factFinderService.ffData = factFinderService.ffData.replace(/"hideCustSign"/g,"\"showCustSign\"");
			//this.FF_SIGNED = true;
			this.FF_SIGNED = true;
			self.isSigned="Y";
			this.saveFFReportFile().then(
				function(res)
				{
					self.updateOpportunityTime();

				}
				);
		}
	};

	this.isFFSigned = function(){
		return this.FF_SIGNED;
	};

	this.saveFFReportFile = function(){
		var dfd = $q.defer();
		CommonService.saveFile(factFinderService.getFHRId(),".html"/*File Type*/,"/FromClient/FF/HTML/",factFinderService.ffData,"N"/*isBase64*/).then(
			function(res){

				cordova.exec(
					function(fileName){
						debug("fileName: " + fileName);
						self.saveDocumentData().then(
							function(res){
								if(res=="success"){
									dfd.resolve("success");
								}
							}
						);
						//CommonService.openFile(fileName);
					},
					function(errMsg){
						debug(errMsg);
						dfd.resolve(null);
					},
					"PluginHandler","getDecryptedFileName",[(factFinderService.getFHRId() + ".html"), "/FromClient/FF/HTML/"]
				);
			}
		);
		return dfd.promise;
	};
	this.updateOpportunityTime = function(){
		var dfd = $q.defer();
		try{
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"update LP_MYOPPORTUNITY set  RECO_PRODUCT_DEVIATION_FLAG = '"+ flag +"', FHR_SIGNED_TIMESTAMP = '"+CommonService.getCurrDate() +"' where FHR_ID= ? ",[factFinderService.getFHRId()],
						function(tx,res){

							dfd.resolve("success");
						},
						function(tx,err){

							debug("Error in LP_MYOPPORTUNITY().transaction(): " + err.message);
							dfd.resolve(null);
						}
					);
				},
				function(err){
					debug("Error in LP_MYOPPORTUNITY(): " + err.message);
					dfd.resolve(null);
				},null
			);
		}catch(ex){
			debug("Exception in LP_MYOPPORTUNITY(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};
 this.saveDocumentData = function(){


		console.log("factFinderService.getDocId() : "+factFinderService.getDocId());
		var dfd = $q.defer();
		try{
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"insert or replace into LP_DOCUMENT_UPLOAD (DOC_ID, AGENT_CD, DOC_CAT, DOC_CAT_ID , POLICY_NO , DOC_NAME, DOC_TIMESTAMP, DOC_PAGE_NO, IS_FILE_SYNCED, IS_DATA_SYNCED) VALUES (?,?,?,?,?,?,?,?,?,?) ",
					 [factFinderService.getDocId(), factFinderService.getAgentCD() ,"FHR", factFinderService.getFHRId(),null, factFinderService.getFHRId()+".html", CommonService.getCurrDate(), "0", "N", "N"],
					 function(tx,res){
							debug("successfully updated LP_DOCUMENT_UPLOAD");
							//factFinderService.setFHRPage4Data(recommendedProducts,recommendedFundsOptions,productName ,fundDescription,rdinvestval,product_pgl_id);
							dfd.resolve("success");
						},
						function(tx,err){
						//alert("error");
							debug("Error in LP_DOCUMENT_UPLOAD().transaction(): " + err.message);
							dfd.resolve(null);
						}
					);
				},
				function(err){
					debug("Error in LP_DOCUMENT_UPLOAD(): " + err.message);
					dfd.resolve(null);
				},null
			);
		}catch(ex){
			debug("Exception in LP_DOCUMENT_UPLOAD(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};
}]);
