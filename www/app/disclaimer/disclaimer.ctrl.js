disclaimerModule.controller('DisclaimerCtrl', ['$state', 'CommonService', 'ProductList', function($state, CommonService, ProductList){
	CommonService.hideLoading();

	var dc = this;
	this.productList = ProductList;
	this.appName = CommonService.getAppName();

    /* Header Hight Calculator */
    setTimeout(function(){
        var getHeight = $(".fixed-bar").height();
        $(".custom-position").css({top:getHeight + "px"});
    });
    /* End Header Hight Calculator */



	this.openMenu = function(){
		CommonService.openMenu();
	};
	CommonService.hideLoading();
}]);
disclaimerModule.service('DisclaimerService', ['CommonService', '$q', function(CommonService, $q){

	this.getProductList = function(agentCode){
		var dfd = $q.defer();

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select * from lp_product_master where business_type_list like '%" + BUSINESS_TYPE + "%' and isactive=?", ['Y'],
					function(tx,res){
						if(!!res && res.rows.length>0){
							var productList = [];
							for(var i=0;i<res.rows.length;i++){
								productList[i] = "Tata AIA Life Insurance " + res.rows.item(i).PRODUCT_NAME + " - UIN : " + res.rows.item(i).PRODUCT_UINNO + " (" + res.rows.item(i).PRODUCT_NAME.replace('V2','') + ")";
							}
							dfd.resolve(productList);
						}
						else
							dfd.resolve(null);
					}
				);
			}
		);

		return dfd.promise;
	};

}]);
