appExistingInsModule.controller('existInfoCtrl',['existInfoService','appExistingService',function(existInfoService,appExistingService){
    debug("exist controller loaded");

    this.existArr = [
        {'type':'tataPolicy','name':'Tata AIA'},
        {'type':'nonTataPolicy','name':'Non Tata AIA'}
    ]

    this.activeTab = existInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        appExistingService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.existArr));
}]);

appExistingInsModule.service('existInfoService',[function(){
    debug("exist service loaded");

    this.activeTab = "tataPolicy";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

appExistingInsModule.directive('exTimeline',[function() {
        "use strict";
        debug('in exist');
        return {
            restrict: 'E',
            controller: "existInfoCtrl",
            controllerAs: "exc",
            bindToController: true,
            templateUrl: "applicationForm/existingIns/existingTimeline.html"
        };
}]);
