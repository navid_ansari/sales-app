//alert("js");
tataPolicyModule.controller('tataPolicyCtrl',['$q','$state','LoginService', 'ApplicationFormDataService','appExistingInsService','getExistInsRec','getExistPropRec','existInfoService','CommonService','ExistingAppData','DeDupeData',function($q, $state, LoginService, ApplicationFormDataService, appExistingInsService,getExistInsRec,getExistPropRec,existInfoService,CommonService, ExistingAppData,DeDupeData){
debug("inside tataPolicyCtrl");
var tata = this;
this.click  = false;
existInfoService.setActiveTab("tataPolicy");
this.isSelf = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED;
tata.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */

/*if(ApplicationFormDataService.applicationFormBean.appExistingInsbean!=undefined)
    this.appExistingInsbean = ApplicationFormDataService.applicationFormBean.appExistingInsbean;
else*/
    this.appExistingInsbean={};

/*if(ApplicationFormDataService.applicationFormBean.TataObj!=undefined)
    this.TataObj = ApplicationFormDataService.applicationFormBean.TataObj;
else*/
    this.TataObj={
     policyNo:{},
     sumAssured:{}
    };

/*if(ApplicationFormDataService.applicationFormBean.appExistingPropbean!=undefined)
    this.appExistingPropbean = ApplicationFormDataService.applicationFormBean.appExistingPropbean;
else*/
    this.appExistingPropbean = {};

/*if(ApplicationFormDataService.applicationFormBean.PropTataObj!=undefined)
    this.PropTataObj = ApplicationFormDataService.applicationFormBean.PropTataObj;
else*/
    this.PropTataObj = {
        PropPolicyNo:{},
        PropSumAssured:{}
    };


tata.getExistsPolRecord = getExistInsRec;
this.getTataPolData = function(){
  var dfd = $q.defer();
CommonService.hideLoading();
var i = 0;
if(tata.getExistsPolRecord!=undefined && tata.getExistsPolRecord.length!=0){
    console.log("Insured Radio button is::"+tata.getExistsPolRecord[0].EXIST_INS_POL_FLAG+"Actual Radio button is::"+tata.appExistingInsbean.radioCh);
    tata.appExistingInsbean.radioCh = tata.getExistsPolRecord[0].EXIST_INS_POL_FLAG;
        console.log("0th record is::"+tata.getExistsPolRecord[i].POLICY_NO);

            if(tata.getExistsPolRecord[i]!=undefined){
                tata.TataObj.policyNo.polno1 = tata.getExistsPolRecord[i].POLICY_NO || null;
                tata.TataObj.sumAssured.sa1 = parseInt(tata.getExistsPolRecord[i].SUM_ASSURED) || null;
            }
            if(tata.getExistsPolRecord[i+1]!=undefined){
                tata.TataObj.policyNo.polno2 = tata.getExistsPolRecord[i+1].POLICY_NO || null;
                tata.TataObj.sumAssured.sa2 = parseInt(tata.getExistsPolRecord[i+1].SUM_ASSURED) || null;
            }
            if(tata.getExistsPolRecord[i+2]!=undefined){
                tata.TataObj.policyNo.polno3 = tata.getExistsPolRecord[i+2].POLICY_NO || null;
                tata.TataObj.sumAssured.sa3 = parseInt(tata.getExistsPolRecord[i+2].SUM_ASSURED) || null;
            }
            dfd.resolve(tata.TataObj);
    }
    else
      dfd.resolve(tata.TataObj);
    return dfd.promise;
}
this.getTataPolData().then(function (rs) {
  tata.setDeDupeData().then(function (resp) {
      CommonService.hideLoading();
  });
});


this.setDeDupeData = function () {
  var dfd = $q.defer();
      try{
        if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && !!DeDupeData.RESULT_APPDATA.POLICY_DETAILS && ExistingAppData.applicationMainData.IS_TATA_DET != 'Y'){
        		CommonService.showLoading("Loading Existing Data..");
            var pol = DeDupeData.RESULT_APPDATA.POLICY_DETAILS;
            var policyDetails = (!!pol) ? pol.split(",") : ["","",""];
            var policyArr1 = (!!policyDetails && policyDetails[0]) ? policyDetails[0].split("*") : [""];
            var policyArr2 = (!!policyDetails && policyDetails[1]) ? policyDetails[1].split("*") : [""];
            var policyArr3 = (!!policyDetails && policyDetails[2]) ? policyDetails[2].split("*") : [""];
            if(!!policyDetails && policyDetails.length>0 && !!policyArr1 && policyArr1.length>0){
              tata.appExistingInsbean.radioCh = "Y"; // Data present, Tata Flag Y
              tata.TataObj.policyNo.polno1 = (!!policyArr1[0]) ? policyArr1[0] : null;
              tata.TataObj.sumAssured.sa1 = (!!policyArr1[1]) ? parseInt(policyArr1[1]) : null;
              if(!!policyDetails && policyDetails.length>1 && !!policyArr2 && policyArr2.length>1){
                tata.TataObj.policyNo.polno2 = (!!policyArr2[0]) ? policyArr2[0] : null;
                tata.TataObj.sumAssured.sa2 = (!!policyArr2[1]) ? parseInt(policyArr2[1]) : null;
                if(!!policyDetails && policyDetails.length>2 && !!policyArr3 && policyArr3.length>1){
                  tata.TataObj.policyNo.polno3 = (!!policyArr3[0]) ? policyArr3[0] : null;
                  tata.TataObj.sumAssured.sa3 = (!!policyArr3[1]) ? parseInt(policyArr3[1]) : null;
                }
              }
            }
            dfd.resolve(tata.TataObj);
        }
        else
        {
          debug("Already data saved");
          dfd.resolve(tata.TataObj);
        }
      }catch(e){
        debug("Exception in deDupe - TATA POL");
        CommonService.hideLoading();
        dfd.resolve(tata.TataObj);
      }
      return dfd.promise;
}



this.getExistsPropPolRecord = getExistPropRec;
this.getTataPropPolData = function(){
CommonService.hideLoading();
var i=0;
if(tata.getExistsPropPolRecord!=undefined && tata.getExistsPropPolRecord.length!=0){
   // console.log("Radio button is::"+tata.getExistsPropPolRecord[0].EXIST_INS_POL_FLAG);
    tata.appExistingPropbean.PropradioCh = tata.getExistsPropPolRecord[0].EXIST_INS_POL_FLAG;

    console.log("Prop Pol no is::"+JSON.stringify(tata.getExistsPropPolRecord.length));

            if(tata.getExistsPropPolRecord[i]!=undefined){
                tata.PropTataObj.PropPolicyNo.polno4 = tata.getExistsPropPolRecord[i].POLICY_NO || null;
                tata.PropTataObj.PropSumAssured.sa4 = parseInt(tata.getExistsPropPolRecord[i].SUM_ASSURED) || null;
            }
            if(tata.getExistsPropPolRecord[i+1]!=undefined){
                tata.PropTataObj.PropPolicyNo.polno5 = tata.getExistsPropPolRecord[i+1].POLICY_NO || null;
                tata.PropTataObj.PropSumAssured.sa5 = parseInt(tata.getExistsPropPolRecord[i+1].SUM_ASSURED) || null;
            }
            if(tata.getExistsPropPolRecord[i+2]!=undefined){
                tata.PropTataObj.PropPolicyNo.polno6 = tata.getExistsPropPolRecord[i+2].POLICY_NO || null;
                tata.PropTataObj.PropSumAssured.sa6 = parseInt(tata.getExistsPropPolRecord[i+2].SUM_ASSURED) || null;
            }
        }
}
if(tata.isSelf!='Y')
this.getTataPropPolData();
else{}

this.deleteFunction = function(){
    appExistingInsService.deleteInsRec().then(
        function(res){
        console.log("res:::"+res);
            if(!!res && res){
                console.log("res1:::"+res);
                appExistingInsService.insertInsDet(tata,tata.TataObj,tata.appExistingInsbean, tata.appExistingPropbean);
            }
        }
    )
}

this.gotoNonTataPolpg = function(form)
{
	CommonService.showLoading();
tata.click=true;
    if(form.$invalid !=true){
        console.log("inside gotoPage");
        ApplicationFormDataService.applicationFormBean.appExistingInsbean = tata.appExistingInsbean;
        ApplicationFormDataService.applicationFormBean.TataObj = tata.TataObj;
        ApplicationFormDataService.applicationFormBean.appExistingPropbean = tata.appExistingPropbean;
        ApplicationFormDataService.applicationFormBean.PropTataObj = tata.PropTataObj;
        console.log("Data is::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.appExistingInsbean)+"Tata obj data is::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.appExistingInsbean));
        tata.deleteFunction();
    if(tata.isSelf!='Y'){
        appExistingInsService.deleteInsRec().then(
                function(res){
                console.log("res:::"+res);
                    if(!!res && res){
                        console.log("res1:::"+res);
                        appExistingInsService.insertPropDet(tata.PropTataObj,tata.appExistingPropbean, tata.appExistingInsbean);
                    }
                }
            );
        }
    }
    else
        navigator.notification.alert("Please complete the details of form.",function(){CommonService.hideLoading();},"Application","OK");
}


this.onChangeRadio = function(){
    tata.TataObj.policyNo.polno1 = "";
    tata.TataObj.sumAssured.sa1 = "";
    tata.TataObj.policyNo.polno2 = "";
    tata.TataObj.sumAssured.sa2 = "";
    tata.TataObj.policyNo.polno3 = "";
    tata.TataObj.sumAssured.sa3 = "";

}

this.onChangePropRadio = function(){
    tata.PropTataObj.PropPolicyNo.polno4 = "";
    tata.PropTataObj.PropSumAssured.sa4 = "";
    tata.PropTataObj.PropPolicyNo.polno5 = "";
    tata.PropTataObj.PropSumAssured.sa5 = "";
    tata.PropTataObj.PropPolicyNo.polno6 = "";
    tata.PropTataObj.PropSumAssured.sa6 = "";

}
CommonService.hideLoading();
}]);


appExistingInsModule.service('appExistingInsService',['$q','LoginService', 'CommonService', 'ApplicationFormDataService','$state','existInfoService',function($q, LoginService, CommonService, ApplicationFormDataService,$state,existInfoService){

var tataSrv = this;
tataSrv.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var INSBEAN;
var PROPBEAN;

this.insertInsDet = function(tata,tataObj,existingInsbean, propBean){
//var dfd = $q.defer();
var polList = [];
var saList = [];
 INSBEAN = existingInsbean;
 PROPBEAN = propBean;
console.log("inserting record");

    if(existingInsbean.radioCh != "Y"){
    console.log("Value of radio is::"+existingInsbean.radioCh);
                CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":tataSrv.AGENT_CD,"CUST_TYPE":"C01","INS_SEQ_NO":"1","TATA_OTHER_FLAG":"T","POLICY_NO":null,"SUM_ASSURED":null,"EXIST_INS_POL_FLAG":existingInsbean.radioCh},true).then(
                    function(res){
                        if(!!res)
                            {
                                console.log("Record inserted successfully"+JSON.stringify(res));
                                if(tataObj.isSelf!='Y')
                                {
                                updateInsPropRecord(INSBEAN, PROPBEAN);
                                existInfoService.setActiveTab("nonTataPolicy");
                                $state.go("applicationForm.existingIns.nonTataPolicy");
                                }
                            }
                        },
                    function(error)
                        {
                            console.log("Db Error"+error.message);
                        }
                    );
     }
     else{
     var j = 1;
        for(var i=0;i<3;i++){
            console.log("value of i is::"+i);
                (function(i){
                 polList = Object.keys(tataObj.policyNo);
                 saList = Object.keys(tataObj.sumAssured);
                    console.log(i+"abc is::"+JSON.stringify(saList)+"Policy No is::"+tataObj.policyNo[polList[i]]);
                        if(tataObj.policyNo[polList[i]]!=undefined && tataObj.policyNo[polList[i]]!="" && tataObj.sumAssured[saList[i]]!=undefined && tataObj.sumAssured[saList[i]]!=""){
                            console.log("Policy no value is::"+j);
                            var params = {"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":tataSrv.AGENT_CD,"CUST_TYPE":"C01","INS_SEQ_NO":j,"TATA_OTHER_FLAG":"T","POLICY_NO":tataObj.policyNo[polList[i]],"SUM_ASSURED":tataObj.sumAssured[saList[i]],"EXIST_INS_POL_FLAG":existingInsbean.radioCh};
                            j=j+1;
                            CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",params,true).then(
                                function(res){
                                    if(!!res){
                                        console.log("Record with policy inserted successfully"+JSON.stringify(res));

                                    }
                                },
                                function(error){
                                    console.log("Database error is"+error.message);
                                }
                            );
                        }
                        if(i==2 && tata.isSelf=='Y'){
                           existInfoService.setActiveTab("nonTataPolicy");
                           $state.go("applicationForm.existingIns.nonTataPolicy");
                           }
                        else
                            console.log("else ******"+tataObj.isSelf);
                    }
                )(i);
         }
         updateInsPropRecord(INSBEAN, PROPBEAN);
     }
     //call function
 }


this.selectTataInsRecords = function(){
console.log("inside insured selectRecords");
var dfd = $q.defer();
    var whereClauseObj = {};
        whereClauseObj.AGENT_CD = tataSrv.AGENT_CD;
        whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        whereClauseObj.CUST_TYPE = "C01";
        whereClauseObj.TATA_OTHER_FLAG = "T";
           // if(existIns.appExistingInsForm.radioCh=="Y"){
                CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj,"INS_SEQ_NO asc").then(
                        function(res){
                            var getExistInsRec =[];
                            console.log("Length of Insured rec::"+res.rows.length+"result is::"+JSON.stringify(res));
                            if(!!res && res.rows.length>0){
                                //getExistInsRec.push({"POLICY_NO":"","SUM_ASSURED":""});
                                for(var i=0;i<res.rows.length;i++){
                                    var RecDet = {};
                                    RecDet.POLICY_NO = res.rows.item(i).POLICY_NO;
                                    RecDet.SUM_ASSURED = res.rows.item(i).SUM_ASSURED;
                                    RecDet.EXIST_INS_POL_FLAG = res.rows.item(i).EXIST_INS_POL_FLAG;
                                    console.log("Record of Select is"+JSON.stringify(RecDet));
                                    getExistInsRec.push(RecDet);
                                }
                            dfd.resolve(getExistInsRec);
                            console.log("Rec is::"+JSON.stringify(getExistInsRec));
                        }
                        else
                            dfd.resolve(null);
                }
            );

            return dfd.promise;
         //}
    }

var updateInsPropRecord = function(INSBEAN, PROPBEAN){
var dfd = $q.defer();
var EXT_FLAG = 'N';
if((!!INSBEAN && INSBEAN.radioCh == 'Y') || (!!PROPBEAN && PROPBEAN.PropradioCh == 'Y')){
    EXT_FLAG = 'Y';
}

var whereClauseObj = {};
    whereClauseObj.AGENT_CD = tataSrv.AGENT_CD;
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    CommonService.updateRecords(db,'LP_APPLICATION_MAIN',{'IS_SCREEN4_COMPLETED':'Y','TATAAIA_INS_FLAG':EXT_FLAG,'IS_TATA_DET':'Y'},whereClauseObj).then(
        function(res){
            console.log("Record updated successfully");
            dfd.resolve("success");
        }

    );
    return dfd.promise;
}


//Proposer Insert Function
this.insertPropDet = function(ProtataObj,PropexistingInsbean, insBean){
var PropPolList = [];
var PropSaList = [];
INSBEAN = insBean;
 PROPBEAN = PropexistingInsbean;
console.log("inserting record");

    if(PropexistingInsbean.PropradioCh != "Y"){
    console.log("Value of radio is::"+PropexistingInsbean.PropradioCh);
                CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":tataSrv.AGENT_CD,"CUST_TYPE":"C02","INS_SEQ_NO":"1","TATA_OTHER_FLAG":"T","POLICY_NO":null,"SUM_ASSURED":null,"EXIST_INS_POL_FLAG":PropexistingInsbean.PropradioCh},true).then(
                    function(res){
                        if(!!res)
                            {
                                updateInsPropRecord(INSBEAN, PROPBEAN);
                                console.log("Record inserted successfully"+JSON.stringify(res));
                                existInfoService.setActiveTab("nonTataPolicy");
                                $state.go("applicationForm.existingIns.nonTataPolicy");
                            }
                        },
                    function(error)
                        {
                            console.log("Db Error"+error.message);
                        }
                    );
     }


     else{
        for(var i=0;i<3;i++){
            console.log("value of i is::"+i);
                (function(i){
                 PropPolList = Object.keys(ProtataObj.PropPolicyNo);
                 PropSaList = Object.keys(ProtataObj.PropSumAssured);
                    console.log("abc is::"+JSON.stringify(PropSaList)+"Policy No is::"+ProtataObj.PropPolicyNo[PropPolList[i]]);
                        if(ProtataObj.PropPolicyNo[PropPolList[i]]!=undefined && ProtataObj.PropPolicyNo[PropPolList[i]]!="" && ProtataObj.PropSumAssured[PropSaList[i]]!=undefined && ProtataObj.PropSumAssured[PropSaList[i]]!=""){
                            console.log("Policy no value is::");
                            CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":tataSrv.AGENT_CD,"CUST_TYPE":"C02","INS_SEQ_NO":i+1,"TATA_OTHER_FLAG":"T","POLICY_NO":ProtataObj.PropPolicyNo[PropPolList[i]],"SUM_ASSURED":ProtataObj.PropSumAssured[PropSaList[i]],"EXIST_INS_POL_FLAG":PropexistingInsbean.PropradioCh},true).then(
                                function(res){
                                    if(!!res){
                                        console.log("Proposer Record with policy inserted successfully"+JSON.stringify(res));

                                    }
                                },
                                function(error){
                                    console.log("Database error is"+error.message);
                                }
                            );
                        }
                        if(i==2){
                           existInfoService.setActiveTab("nonTataPolicy");
                           $state.go("applicationForm.existingIns.nonTataPolicy");
                           }
                    }
                )(i);
         }
         updateInsPropRecord(INSBEAN, PROPBEAN);
     }

 }

this.selectTataPropRecords = function(){
console.log("inside selectRecords");
var dfd = $q.defer();
    var whereClauseObj = {};
        whereClauseObj.AGENT_CD = tataSrv.AGENT_CD;
        whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        whereClauseObj.CUST_TYPE = "C02";
        whereClauseObj.TATA_OTHER_FLAG = "T";
           // if(existIns.appExistingInsForm.radioCh=="Y"){
                CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj,"INS_SEQ_NO asc").then(
                        function(res){
                            var getExistPropRec =[];
                            console.log("Length::"+res.rows.length);
                            if(!!res && res.rows.length>0){
                                //getExistInsRec.push({"POLICY_NO":"","SUM_ASSURED":""});
                                for(var i=0;i<res.rows.length;i++){
                                    var RecPropDet = {};
                                    RecPropDet.POLICY_NO = res.rows.item(i).POLICY_NO;
                                    RecPropDet.SUM_ASSURED = res.rows.item(i).SUM_ASSURED;
                                    RecPropDet.EXIST_INS_POL_FLAG = res.rows.item(i).EXIST_INS_POL_FLAG;
                                    console.log("Record of Select is"+JSON.stringify(RecPropDet));
                                    getExistPropRec.push(RecPropDet);
                                }
                            dfd.resolve(getExistPropRec);
                        }
                        else
                            dfd.resolve(null);
                }
            );

            return dfd.promise;
         //}
    }

this.deleteInsRec = function(){
var dfd = $q.defer();
console.log("Inside delete function");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = tataSrv.AGENT_CD;
    whereClauseObj.TATA_OTHER_FLAG = "T";
    CommonService.deleteRecords(db,"LP_APP_EXIST_INS_SCRN_D",whereClauseObj).then(
        function(res){
            console.log("Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;
}

}]);
