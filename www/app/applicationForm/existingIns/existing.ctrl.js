appExistingInsModule.controller('ExistingInsCtrl',['$state','CommonService',function($state,CommonService){

var existIns = this;

//timeline changes..
CommonService.hideLoading();

}]);

appExistingInsModule.service('appExistingService',['$state','existInfoService','LoadApplicationScreenData','ApplicationFormDataService','LoginService',function($state,existInfoService,LoadApplicationScreenData,ApplicationFormDataService,LoginService){
    this.gotoPage = function(pageName){
		if(existInfoService.getActiveTab()!==pageName){
			CommonService.showLoading();
		}
        existInfoService.setActiveTab(pageName);
        switch(pageName){
            case 'tataPolicy':
                        $state.go('applicationForm.existingIns.tataPolicy');
                        break;

            case 'nonTataPolicy':
            LoadApplicationScreenData.loadApplicationFlags(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                            function(appData){
                                                debug("appData.applicationFlags.IS_TATA_DET :"+appData.applicationFlags.IS_TATA_DET);
                                                if(appData!=undefined && !!appData.applicationFlags && Object.keys(appData.applicationFlags).length !=0 && appData.applicationFlags.IS_TATA_DET == 'Y')//if(payData != {})
                                                    $state.go('applicationForm.existingIns.nonTataPolicy');
                                                else
                                                    navigator.notification.alert("Please complete Tata Policy Details",function(){CommonService.hideLoading();},"Application","OK");
                                            }
                                        )

                        break;

            default:
                        $state.go('applicationForm.existingIns.tataPolicy');
            }
        }

}]);
