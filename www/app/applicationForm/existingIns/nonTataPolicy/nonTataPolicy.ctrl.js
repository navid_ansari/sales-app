//alert("another js");
nonTataPolicyModule.controller('nonTataPolicyCtrl',['$state','LoginService', 'ApplicationFormDataService','appNonTataExistingInsService','getNonExistInsRec','getNonExistPropRec','CommonService',function($state, LoginService, ApplicationFormDataService,appNonTataExistingInsService,getNonExistInsRec,getNonExistPropRec,CommonService){
debug("inside nonTataPolicyCtrl");
var nonTata = this;
this.click  = false;
this.isSelf = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED;

this.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */

/*if(ApplicationFormDataService.applicationFormBean.appNonTataExistingInsbean!=undefined)
    nonTata.appNonTataExistingInsbean = ApplicationFormDataService.applicationFormBean.appNonTataExistingInsbean;
else*/
    nonTata.appNonTataExistingInsbean={};

/*if(ApplicationFormDataService.applicationFormBean.RepeatIns!=undefined)
    nonTata.RepeatIns = ApplicationFormDataService.applicationFormBean.RepeatIns;
else*/
    nonTata.RepeatIns=[{
    }];

/*if(ApplicationFormDataService.applicationFormBean.appNonTataExistingPropbean!=undefined)
    nonTata.appNonTataExistingPropbean = ApplicationFormDataService.applicationFormBean.appNonTataExistingPropbean;
else*/
    nonTata.appNonTataExistingPropbean={};

/*if(ApplicationFormDataService.applicationFormBean.RepeatProp!=undefined)
    nonTata.RepeatProp = ApplicationFormDataService.applicationFormBean.RepeatProp;
else*/
    nonTata.RepeatProp=[{
    }];


//nonTata.RepeatIns.insuranceType = "Select ";
//console.log("nonTata.RepeatIns[0].insuranceType"+nonTata.RepeatIns.insuranceType);
//Add new Insured
this.addNewInsured = function(){
console.log("inside addNewInsured");
    if(nonTata.RepeatIns.length < 5){
        var newItemIns = nonTata.RepeatIns.length+1;
        nonTata.RepeatIns.push({'Inscnt':'0'+newItemIns});
    }
    else
        navigator.notification.alert("You cannont add more than 5",function(){CommonService.hideLoading();},"Application","OK");
}

this.deleteInsBlock = function(index){
    if(nonTata.RepeatIns.length>1)
        nonTata.RepeatIns.splice(index,1);
}


//Add new Proposer
this.addNewProposer = function(){
console.log("123456");
    if(this.RepeatProp.length < 5){
        var newItem = nonTata.RepeatProp.length+1;
        console.log("new Item is::"+newItem);
        nonTata.RepeatProp.push({'name':'sda'+newItem});
        console.log("Element added");
        }
        else
           navigator.notification.alert("You cannont add more than 5",function(){CommonService.hideLoading();},"Application","OK");
}

this.deletePropBlock = function(index){
    if(nonTata.RepeatProp.length>1)
        nonTata.RepeatProp.splice(index,1);
}

//Function to populate insured data
console.log("getNonExistInsRec ::"+JSON.stringify(getNonExistInsRec));
this.getNonTataData = getNonExistInsRec;

this.getInsRecord = function(){
if(nonTata.getNonTataData!=undefined && nonTata.RepeatIns!=undefined && nonTata.RepeatIns!=""){
nonTata.appNonTataExistingInsbean.radioCh2 = nonTata.getNonTataData[0].EXIST_INS_POL_FLAG;
    for(var i=0; i<nonTata.getNonTataData.length;i++){
        (function(i){
            if(i!=0)
                {
                    var newItemIns = nonTata.RepeatIns.length+1;
                    nonTata.RepeatIns.push({'Inscnt':'0'+newItemIns});
                }
                console.log("Start i:"+i);
                nonTata.RepeatIns[i].insuranceType = nonTata.getNonTataData[i].TYPE_OF_INSURANCE || null;
                nonTata.RepeatIns[i].compName = nonTata.getNonTataData[i].INSURANCE_PROVIDER || null;
                nonTata.RepeatIns[i].baseSA = parseInt(nonTata.getNonTataData[i].SUM_ASSURED) || null;
                nonTata.RepeatIns[i].annualPremium = parseInt(nonTata.getNonTataData[i].ANNUAL_PREMIUM) || null;
                nonTata.RepeatIns[i].NonTatadecision = nonTata.getNonTataData[i].DECISION || null;
                nonTata.RepeatIns[i].insOtherDet = nonTata.getNonTataData[i].DECISION_DETAILS || null;
                console.log("End i:"+i);
            }(i));
        }

    }
}
this.getInsRecord();

this.setInsRec = function(){
if(nonTata.getNonTataData!=undefined && nonTata.RepeatIns!=undefined && nonTata.RepeatIns!=""){
       nonTata.RepeatIns=[{}];
    }
}


//Function to populate Proposer data
this.getNonPropTataData = getNonExistPropRec;
this.getPropRecord = function(){
if(nonTata.getNonPropTataData!=undefined && nonTata.RepeatProp!=undefined && nonTata.RepeatProp!=""){
console.log("@radio value is::"+nonTata.getNonPropTataData[0].EXIST_INS_POL_FLAG);
nonTata.appNonTataExistingPropbean.PropradioCh2 = nonTata.getNonPropTataData[0].EXIST_INS_POL_FLAG;
    for(var i=0;i<nonTata.getNonPropTataData.length;i++){
        (function(i){
            if(i!=0)
            {
                var newItem = nonTata.RepeatProp.length+1;
                nonTata.RepeatProp.push({'Inscnt':'0'+newItem});
            }
                nonTata.RepeatProp[i].PropInsuranceType = nonTata.getNonPropTataData[i].TYPE_OF_INSURANCE || null;
                nonTata.RepeatProp[i].PropCompName = nonTata.getNonPropTataData[i].INSURANCE_PROVIDER || null;
                nonTata.RepeatProp[i].PropBaseSA = parseInt(nonTata.getNonPropTataData[i].SUM_ASSURED) || null;
                nonTata.RepeatProp[i].PropAnnualPremium = parseInt(nonTata.getNonPropTataData[i].ANNUAL_PREMIUM) || null;
                nonTata.RepeatProp[i].PropNonTatadecision = nonTata.getNonPropTataData[i].DECISION || null;
                nonTata.RepeatProp[i].PropInsOtherDet = nonTata.getNonPropTataData[i].DECISION_DETAILS || null;
            }(i));
        }
    }
}

if(nonTata.isSelf!='Y')
this.getPropRecord();

this.setPropRec = function(){
if(nonTata.getNonPropTataData!=undefined && nonTata.RepeatProp!=undefined && nonTata.RepeatProp!=""){
    nonTata.RepeatProp=[{}];
    }
}

this.deleteNonTataFunction = function(){
    appNonTataExistingInsService.deleteInsPropRec().then(
        function(res){
        console.log("res:::"+res);
            if(!!res && res){
                console.log("res1:::"+res);
                appNonTataExistingInsService.insertNonTataInsPol(nonTata.appNonTataExistingInsbean,nonTata,nonTata.appNonTataExistingPropbean);
            }
        }
    );
}

//Save data
this.saveExistingPoldata = function(form1,form2)
{
	CommonService.showLoading();
nonTata.click=true;
    if(form1.$valid ==true &&(form2.$valid==true || nonTata.isSelf=='Y')){
        ApplicationFormDataService.applicationFormBean.appNonTataExistingInsbean = nonTata.appNonTataExistingInsbean;
        ApplicationFormDataService.applicationFormBean.RepeatIns = nonTata.RepeatIns;
        ApplicationFormDataService.applicationFormBean.appNonTataExistingPropbean = nonTata.appNonTataExistingPropbean;
        ApplicationFormDataService.applicationFormBean.RepeatProp = nonTata.RepeatProp;
        console.log("Data on 2nd page is::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.nonTataObj));
        nonTata.deleteNonTataFunction();
    if(nonTata.isSelf!='Y'){
        appNonTataExistingInsService.deleteInsPropRec().then(
                function(res){
                console.log("res:::"+res);
                    if(!!res && res){
                        console.log("res1:::"+res);
                        appNonTataExistingInsService.insertNonTataPropPol(nonTata.appNonTataExistingPropbean,nonTata, nonTata.appNonTataExistingInsbean);
                    }
                }
            );
        }
    }
    else
        navigator.notification.alert("Please fill the Complete details.",function(){CommonService.hideLoading();},"Application","OK");
}

this.changeDecision = function(index){
    nonTata.RepeatIns[index].insOtherDet = "";
}

this.changePropDecision = function(index){
    nonTata.RepeatProp[index].PropInsOtherDet = "";
}

CommonService.hideLoading();
}]);


//Non tata policy Service
nonTataPolicyModule.service('appNonTataExistingInsService',['$state','$q','CommonService','LoginService', 'ApplicationFormDataService','AppTimeService',function($state,$q,CommonService, LoginService, ApplicationFormDataService,AppTimeService){

var nonTataSrv = this;
nonTataSrv.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var INSBEAN;
var PROPBEAN;

this.insertNonTataInsPol = function(NonTataexistingInsbean, nonTata, propBean){
console.log("inside insertPersonalInsPol insured");
    INSBEAN = NonTataexistingInsbean;
    PROPBEAN = propBean;
    if(NonTataexistingInsbean.radioCh2!="Y"){
        console.log("Radio button value is"+NonTataexistingInsbean.radioCh2);
            CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":nonTataSrv.AGENT_CD,"CUST_TYPE":"C01","INS_SEQ_NO":"1","TATA_OTHER_FLAG":"O","EXIST_INS_POL_FLAG":NonTataexistingInsbean.radioCh2},true).then(
                function(res)
                {
                    if(!!res)
                    {
                        console.log("Personal policy record inserted successfully"+JSON.stringify(res));
                        if(nonTata.isSelf=='Y'){
                        updateNonTataInsPropRecord(INSBEAN, PROPBEAN);
                        MAIN_TAB = 'familyDetails';
                        //timeline changes..
                        CommonService.showLoading();
                        AppTimeService.isExistPolicyCompleted = true;
                        AppTimeService.setActiveTab("familyDetails");
                        //$rootScope.val = 40;
                        AppTimeService.setProgressValue(40);
                        $state.go("applicationForm.familyDetails.familySection1");
                        }
                    }
                },
                function(error)
                {
                    console.log("Db2 Error"+error.message);
                }
            );
    }
    else
    {
    var j = 1;
        for(var i=0;i<5;i++){
            console.log("i is"+i);
            //try{
            (function(i){

                        try{
                            if(nonTata.RepeatIns[i]!=undefined && nonTata.RepeatIns[i]!=""){
                            console.log("Insurance list is::"+nonTata.RepeatIns[i].insuranceType+"comp name::"+nonTata.RepeatIns[i].compName+"SA::"+nonTata.RepeatIns[i].baseSA+"Annual Premium"+nonTata.RepeatIns[i].annualPremium+"Decision::"+nonTata.RepeatIns[i].NonTatadecision+"Other Detail::"+nonTata.RepeatIns[i].insOtherDet);
                            //console.log("Repeat Insurance is::"+JSON.stringify(nonTata.RepeatIns[i]));
                            if(nonTata.RepeatIns[i].insuranceType!=undefined && nonTata.RepeatIns[i].insuranceType!="" && nonTata.RepeatIns[i].compName!=undefined && nonTata.RepeatIns[i].compName!=""
                                && nonTata.RepeatIns[i].baseSA!=undefined && nonTata.RepeatIns[i].baseSA!="" && nonTata.RepeatIns[i].annualPremium!=undefined && nonTata.RepeatIns[i].annualPremium!=""
                                 && nonTata.RepeatIns[i].NonTatadecision!=undefined && nonTata.RepeatIns[i].NonTatadecision!="" /*&& nonTata.RepeatIns[i].insOtherDet!=undefined && nonTata.RepeatIns[i].insOtherDet!=""*/){
                                 console.log("Value of j is::"+j);
                                 var params = {"APPLICATION_ID": ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":nonTataSrv.AGENT_CD,"CUST_TYPE":"C01","INS_SEQ_NO":j,"TATA_OTHER_FLAG":"O","TYPE_OF_INSURANCE":nonTata.RepeatIns[i].insuranceType,"INSURANCE_PROVIDER":nonTata.RepeatIns[i].compName,"SUM_ASSURED":nonTata.RepeatIns[i].baseSA,"ANNUAL_PREMIUM":nonTata.RepeatIns[i].annualPremium,"DECISION":nonTata.RepeatIns[i].NonTatadecision,"DECISION_DETAILS":nonTata.RepeatIns[i].insOtherDet,"EXIST_INS_POL_FLAG":NonTataexistingInsbean.radioCh2};
                                 j = j+1;
                                    CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",params,true).then(
                                        function(res)
                                        {
                                        console.log("inside res");
                                            if(!!res)
                                            {
                                                console.log("Selected Personal policy record inserted successfully"+JSON.stringify(res));

                                            }
                                        },
                                        function(error)
                                        {
                                            console.log("Db2 Error"+error.message);
                                        }
                                    );
                                }

                            }
                            if(i==4 && nonTata.isSelf=='Y')
                            {
                            updateNonTataInsPropRecord(INSBEAN, PROPBEAN);
                            MAIN_TAB = 'familyDetails';
                            //timeline changes..
                            CommonService.showLoading();
                            AppTimeService.isExistPolicyCompleted = true;
                            AppTimeService.setActiveTab("familyDetails");
                            $state.go("applicationForm.familyDetails.familySection1");
                            }
                            else
                            console.log("inside else::"+nonTata.isSelf);

                        }catch(e){console.log("Error is::"+e.message);}
                    }
            )(i);
        }


    }
}

this.selectNontataInsRec = function(){
var dfd = $q.defer();
var whereClauseObj={};
    whereClauseObj.AGENT_CD = nonTataSrv.AGENT_CD;
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.CUST_TYPE="C01";
    whereClauseObj.TATA_OTHER_FLAG="O";
        CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj,"INS_SEQ_NO ASC").then(
            function(res){
            console.log("Inside for"+res.rows.length+"App_ID::"+ApplicationFormDataService.applicationFormBean.APPLICATION_ID+"AGENT is::"+nonTataSrv.AGENT_CD);
            var getNonExistInsRec = [];
                if(!!res && res.rows.length>0){
                //getNonExistInsRec.push({"TYPE_OF_INSURANCE":"Select","INSURANCE_PROVIDER":"","SUM_ASSURED":"","ANNUAL_PREMIUM":"","DECISION":"","DECISION_DETAILS":""});
                    for(var i=0;i<res.rows.length;i++){
                        var NonInsRecDet = {};
                        NonInsRecDet.TYPE_OF_INSURANCE = res.rows.item(i).TYPE_OF_INSURANCE;
                        NonInsRecDet.INSURANCE_PROVIDER = res.rows.item(i).INSURANCE_PROVIDER;
                        NonInsRecDet.SUM_ASSURED = res.rows.item(i).SUM_ASSURED;
                        NonInsRecDet.ANNUAL_PREMIUM = res.rows.item(i).ANNUAL_PREMIUM;
                        NonInsRecDet.DECISION = res.rows.item(i).DECISION;
                        NonInsRecDet.DECISION_DETAILS = res.rows.item(i).DECISION_DETAILS;
                        NonInsRecDet.EXIST_INS_POL_FLAG = res.rows.item(i).EXIST_INS_POL_FLAG;
                        console.log("Non Insurance Data is::"+JSON.stringify(NonInsRecDet));
                        getNonExistInsRec.push(NonInsRecDet);
                    }
                    dfd.resolve(getNonExistInsRec);
                    console.log("Records::"+JSON.stringify(getNonExistInsRec));
                }
                else
                    dfd.resolve(null);
            }

        );

     return dfd.promise;

}

var updateNonTataInsPropRecord = function(INSBEAN, PROPBEAN){
var dfd = $q.defer();
console.log("INSBEAN: "+JSON.stringify(INSBEAN));
var EXT_FLAG = 'N';
//appNonTataExistingInsbean.radioCh2
//appNonTataExistingPropbean.PropradioCh2
if((!!INSBEAN && INSBEAN.radioCh2 == 'Y') || (!!PROPBEAN && PROPBEAN.PropradioCh2 == 'Y')){
    EXT_FLAG = 'Y';
}

var whereClauseObj = {};
    whereClauseObj.AGENT_CD = nonTataSrv.AGENT_CD;
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    CommonService.updateRecords(db,'LP_APPLICATION_MAIN',{'IS_SCREEN4_COMPLETED':'Y','OTH_COMPANY_INS_FLAG':EXT_FLAG,'IS_NON_TATA_DET':'Y'},whereClauseObj).then(
        function(res){
            console.log("Record of screen5 updated successfully");
            dfd.resolve(res);
        }

    );
    return dfd.promise;
}


//Proposer Data
//Proposer Insert function
this.insertNonTataPropPol = function(NonTataexistingPropbean,nonTata, InsBean){
console.log("inside insertPersonalInsPol proposer");
    INSBEAN = InsBean;
    PROPBEAN = NonTataexistingPropbean;
//var PropInsList=[],PropCompName=[], PropBaseSA=[], PropAnnPremium=[],PropDec=[], PropDetOther=[];
    if(NonTataexistingPropbean.PropradioCh2!="Y"){
        console.log("Radio button value is"+NonTataexistingPropbean.PropradioCh2);
            CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID": ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":nonTataSrv.AGENT_CD,"CUST_TYPE":"C02","INS_SEQ_NO":"1","TATA_OTHER_FLAG":"O","EXIST_INS_POL_FLAG":NonTataexistingPropbean.PropradioCh2},true).then(
                function(res)
                {
                    if(!!res)
                    {
                        console.log("Proposal Personal policy record inserted successfully"+JSON.stringify(res));
                        updateNonTataInsPropRecord(INSBEAN, PROPBEAN);
                        MAIN_TAB = 'familyDetails';
                        //timeline changes..
                        CommonService.showLoading();
                        AppTimeService.isExistPolicyCompleted = true;
                        AppTimeService.setActiveTab("familyDetails");
                        $state.go("applicationForm.familyDetails.familySection1");
                    }
                },
                function(error)
                {
                    console.log("Db2 Error"+error.message);
                }
            );
    }
    else
    {
        for(var i=0;i<5;i++){
            console.log("i is"+i);
            //try{
            (function(i){
                        try{
                            if(nonTata.RepeatProp[i]!=undefined && nonTata.RepeatProp[i]!=""){
                                console.log("Proposer Insurance list is::"+nonTata.RepeatProp[i].PropInsuranceType);
                                    if(nonTata.RepeatProp[i].PropInsuranceType!=undefined && nonTata.RepeatProp[i].PropInsuranceType!="" && nonTata.RepeatProp[i].PropCompName!=undefined && nonTata.RepeatProp[i].PropCompName!=""
                                        && nonTata.RepeatProp[i].PropBaseSA!=undefined && nonTata.RepeatProp[i].PropBaseSA!="" && nonTata.RepeatProp[i].PropAnnualPremium!=undefined && nonTata.RepeatProp[i].PropAnnualPremium!=""
                                        && nonTata.RepeatProp[i].PropNonTatadecision!=undefined && nonTata.RepeatProp[i].PropNonTatadecision!=""){
                                            CommonService.insertOrReplaceRecord(db,"LP_APP_EXIST_INS_SCRN_D",{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":nonTataSrv.AGENT_CD,"CUST_TYPE":"C02","INS_SEQ_NO":i+1,"TATA_OTHER_FLAG":"O","TYPE_OF_INSURANCE":nonTata.RepeatProp[i].PropInsuranceType,"INSURANCE_PROVIDER":nonTata.RepeatProp[i].PropCompName,"SUM_ASSURED":nonTata.RepeatProp[i].PropBaseSA,"ANNUAL_PREMIUM":nonTata.RepeatProp[i].PropAnnualPremium,"DECISION":nonTata.RepeatProp[i].PropNonTatadecision,"DECISION_DETAILS":nonTata.RepeatProp[i].PropInsOtherDet,"EXIST_INS_POL_FLAG":NonTataexistingPropbean.PropradioCh2},true).then(
                                                function(res)
                                                {
                                                    if(!!res)
                                                    {
                                                        console.log("Selected Proposal Personal policy record inserted successfully"+JSON.stringify(res));

                                                    }
                                                },
                                                function(error)
                                                {
                                                    console.log("Db2 Error"+error.message);
                                                }
                                            );
                                        }
                                    }
                                    if(i==4)
                                    {
                                    updateNonTataInsPropRecord(INSBEAN, PROPBEAN);
                                    MAIN_TAB = 'familyDetails';
                                    //timeline changes..
                                    CommonService.showLoading();
                                    AppTimeService.isExistPolicyCompleted = true;
                                    AppTimeService.setActiveTab("familyDetails");
                                    $state.go("applicationForm.familyDetails.familySection1");
                                    }

                        }catch(e){console.log("Error is::"+e.message);}

                    }
            )(i);

        }


    }
}

//Proposer Select function
this.selectNontataPropRec = function(){
var dfd = $q.defer();
var whereClauseObj={};
    whereClauseObj.AGENT_CD = nonTataSrv.AGENT_CD;
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.CUST_TYPE="C02";
    whereClauseObj.TATA_OTHER_FLAG="O";
        CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj,"INS_SEQ_NO ASC").then(
            function(res){
            console.log("Length of rec::"+res.rows.length);
            var getNonExistPropRec = [];
                if(!!res && res.rows.length>0){
                //getNonExistInsRec.push({"TYPE_OF_INSURANCE":"Select","INSURANCE_PROVIDER":"","SUM_ASSURED":"","ANNUAL_PREMIUM":"","DECISION":"","DECISION_DETAILS":""});
                    for(var i=0;i<res.rows.length;i++){
                        var NonPropRecDet = {};
                        NonPropRecDet.TYPE_OF_INSURANCE = res.rows.item(i).TYPE_OF_INSURANCE;
                        NonPropRecDet.INSURANCE_PROVIDER = res.rows.item(i).INSURANCE_PROVIDER;
                        NonPropRecDet.SUM_ASSURED = res.rows.item(i).SUM_ASSURED;
                        NonPropRecDet.ANNUAL_PREMIUM = res.rows.item(i).ANNUAL_PREMIUM;
                        NonPropRecDet.DECISION = res.rows.item(i).DECISION;
                        NonPropRecDet.DECISION_DETAILS = res.rows.item(i).DECISION_DETAILS;
                        NonPropRecDet.EXIST_INS_POL_FLAG = res.rows.item(i).EXIST_INS_POL_FLAG;
                        console.log("Non Insurance Data is::"+JSON.stringify(NonPropRecDet));
                        getNonExistPropRec.push(NonPropRecDet);
                    }
                    dfd.resolve(getNonExistPropRec);
                    console.log("Records::"+JSON.stringify(getNonExistPropRec));
                }
                else
                    dfd.resolve(null);
            }

        );

     return dfd.promise;

}

this.deleteInsPropRec = function(){
var dfd = $q.defer();
console.log("Inside Prop delete function");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = nonTataSrv.AGENT_CD;
    whereClauseObj.TATA_OTHER_FLAG = "O";
    CommonService.deleteRecords(db,"LP_APP_EXIST_INS_SCRN_D",whereClauseObj).then(
        function(res){
            console.log("Proposer Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;
}

}]);
