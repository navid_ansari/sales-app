smrIndsQuesModule.controller('smrIndsQuesCtrl',['$state','SmrIndQuesService', function($state,SmrIndQuesService){
    var smric = this;
    this.smrIndsQuesData = {};


    this.onGenerateOutput = function(){
        console.log("smrIndsQuesData : " + JSON.stringify(this.smrIndsQuesData));

        cordova.exec(
            function(fileData){
                console.log("fileData: " + fileData);
                Object.assign(SmrIndQuesService.smrIndsQuesData,smric.smrIndsQuesData)
                if(!!fileData)
                    SmrIndQuesService.generateOutput(fileData);
                else
                    window.alert("Template file not found");
            },
            function(errMsg){
                debug(errMsg);
            },
            "PluginHandler","getDataFromFileInAssets",["ToClient/ReflQuesTemplates/filename.txt"]
        );
    };

    this.setNull = function(reasonNo){
        console.log("reasonNo : " + reasonNo);
        switch(reasonNo){
            case 'A' :
                this.smrIndsQuesData.ANS_Aa = null;
                this.smrIndsQuesData.ANS_Ab = null;
                this.smrIndsQuesData.ANS_Ac = null;
                break;
            case 'F' :
                this.smrIndsQuesData.ANS_FReason1 = null;
                this.smrIndsQuesData.ANS_FReason2 = null;
                break;
            case '4a' :
                this.smrIndsQuesData.ANS_4_ReasonY = null;
                break;
            case '4b' :
                this.smrIndsQuesData.ANS_4_ReasonN = null;
                break;
            case '8A' :
                this.smrIndsQuesData.ANS_8A_Reason = null;
                break;
            case '8BR' :
                this.smrIndsQuesData.ANS_8B_ReasonR = null;
                break;
            case '8BNR' :
                this.smrIndsQuesData.ANS_8B_ReasonNR = null;
                break;
            case '9a' :
                this.smrIndsQuesData.ANS_9a_Reason1 = null;
                this.smrIndsQuesData.ANS_9a_Reason2 = null;
                break;
            case '9b' :
                this.smrIndsQuesData.ANS_9b_Reason1 = null;
                this.smrIndsQuesData.ANS_9b_Reason2 = null;
                break;
        }
    };
    this.getTotalIncomeP = function(){
        this.smrIndsQuesData.ANS_7a5 = (this.smrIndsQuesData.ANS_7a1||0) + (this.smrIndsQuesData.ANS_7a2||0) + (this.smrIndsQuesData.ANS_7a3||0) + (this.smrIndsQuesData.ANS_7a4||0);
    }
    this.getTotalIncomeI = function(){
        this.smrIndsQuesData.ANS_7b5 = (this.smrIndsQuesData.ANS_7b1||0) + (this.smrIndsQuesData.ANS_7b2||0) + (this.smrIndsQuesData.ANS_7b3||0) + (this.smrIndsQuesData.ANS_7b4||0);
    }
}]);

smrIndsQuesModule.service('SmrIndQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var pos = this;
    this.HtmlFormOutput = "";
    this.smrIndsQuesData = {};
    this.generateOutput = function(fileData){
        debug("this.smrIndsQuesData : " + JSON.stringify(this.smrIndsQuesData));
        this.HtmlFormOutput = fileData;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA||",this.smrIndsQuesData.ANS_A ? this.smrIndsQuesData.ANS_A : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESAA||",this.smrIndsQuesData.ANS_Aa ? this.smrIndsQuesData.ANS_Aa : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESAB||",this.smrIndsQuesData.ANS_Ab ? this.smrIndsQuesData.ANS_Ab : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESAC||",this.smrIndsQuesData.ANS_Ac ? this.smrIndsQuesData.ANS_Ac : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESEA||",this.smrIndsQuesData.ANS_Ea ? this.smrIndsQuesData.ANS_Ea : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESEB||",this.smrIndsQuesData.ANS_Eb ? this.smrIndsQuesData.ANS_Eb : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESF||",this.smrIndsQuesData.ANS_F ? (this.smrIndsQuesData.ANS_F=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESFREASON1||",this.smrIndsQuesData.ANS_FReason1 ? this.smrIndsQuesData.ANS_FReason1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESFREASON2||",this.smrIndsQuesData.ANS_FReason2 ? this.smrIndsQuesData.ANS_FReason2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1||",this.smrIndsQuesData.ANS_1 ? (this.smrIndsQuesData.ANS_1=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2||",this.smrIndsQuesData.ANS_2 ? this.smrIndsQuesData.ANS_2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3||",this.smrIndsQuesData.ANS_3 ? this.smrIndsQuesData.ANS_3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4||",this.smrIndsQuesData.ANS_4 ? (this.smrIndsQuesData.ANS_4=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4REASONY||",this.smrIndsQuesData.ANS_4_ReasonY ? this.smrIndsQuesData.ANS_4_ReasonY : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4REASONN||",this.smrIndsQuesData.ANS_4_ReasonN ? this.smrIndsQuesData.ANS_4_ReasonN : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5A||",this.smrIndsQuesData.ANS_5a ? this.smrIndsQuesData.ANS_5a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5B||",this.smrIndsQuesData.ANS_5b ? this.smrIndsQuesData.ANS_5b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",this.smrIndsQuesData.ANS_6 ? this.smrIndsQuesData.ANS_6 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7A1||",this.smrIndsQuesData.ANS_7a1 ? this.smrIndsQuesData.ANS_7a1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7A2||",this.smrIndsQuesData.ANS_7a2 ? this.smrIndsQuesData.ANS_7a2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7A3||",this.smrIndsQuesData.ANS_7a3 ? this.smrIndsQuesData.ANS_7a3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7A4||",this.smrIndsQuesData.ANS_7a4 ? this.smrIndsQuesData.ANS_7a4 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7A5||",this.smrIndsQuesData.ANS_7a5 ? this.smrIndsQuesData.ANS_7a5 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7B1||",this.smrIndsQuesData.ANS_7b1 ? this.smrIndsQuesData.ANS_7b1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7B2||",this.smrIndsQuesData.ANS_7b2 ? this.smrIndsQuesData.ANS_7b2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7B3||",this.smrIndsQuesData.ANS_7b3 ? this.smrIndsQuesData.ANS_7c3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7B4||",this.smrIndsQuesData.ANS_7b4 ? this.smrIndsQuesData.ANS_7d4 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7B5||",this.smrIndsQuesData.ANS_7b5 ? this.smrIndsQuesData.ANS_7e5 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8A||",this.smrIndsQuesData.ANS_8A ? this.smrIndsQuesData.ANS_8A : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8AREASON||",this.smrIndsQuesData.ANS_8A_Reason ? this.smrIndsQuesData.ANS8A_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8BR||",this.smrIndsQuesData.ANS_8BR ? (this.smrIndsQuesData.ANS_8BR=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8BRREASON||",this.smrIndsQuesData.ANS_8B_ReasonR ? this.smrIndsQuesData.ANS_8B_ReasonR : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8BNR||",this.smrIndsQuesData.ANS_8BNR ? (this.smrIndsQuesData.ANS_8BNR=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8BNRREASON||",this.smrIndsQuesData.ANS_8B_ReasonNR ? this.smrIndsQuesData.ANS_8B_ReasonNR : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9A||",this.smrIndsQuesData.ANS_9a ? this.smrIndsQuesData.ANS_9a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9AREASON1||",this.smrIndsQuesData.ANS_9a_Reason1 ? this.smrIndsQuesData.ANS_9a_Reason1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9AREASON2||",this.smrIndsQuesData.ANS_9a_Reason2 ? this.smrIndsQuesData.ANS_9a_Reason2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9B||",this.smrIndsQuesData.ANS_9b ? this.smrIndsQuesData.ANS_9b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9BREASON1||",this.smrIndsQuesData.ANS_9b_Reason1 ? this.smrIndsQuesData.ANS_9b_Reason1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9BREASON2||",this.smrIndsQuesData.ANS_9b_Reason2 ? this.smrIndsQuesData.ANS_9b_Reason2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9C||",this.smrIndsQuesData.ANS_9c ? this.smrIndsQuesData.ANS_9c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9DI||",this.smrIndsQuesData.ANS_9di ? this.smrIndsQuesData.ANS_9di : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9DII||",this.smrIndsQuesData.ANS_9dii ? this.smrIndsQuesData.ANS_9dii : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES10||",this.smrIndsQuesData.ANS_10 ? this.smrIndsQuesData.ANS_10 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES11A||",this.smrIndsQuesData.ANS_11a ? this.smrIndsQuesData.ANS_11a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES11B||",this.smrIndsQuesData.ANS_11b ? this.smrIndsQuesData.ANS_11b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES11C||",this.smrIndsQuesData.ANS_11c ? this.smrIndsQuesData.ANS_11c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES11D||",this.smrIndsQuesData.ANS_11d ? this.smrIndsQuesData.ANS_11d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES11E||",this.smrIndsQuesData.ANS_11e ? this.smrIndsQuesData.ANS_11e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12||",this.smrIndsQuesData.ANS_12 ? this.smrIndsQuesData.ANS_12 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES13||",this.smrIndsQuesData.ANS_13 ? (this.smrIndsQuesData.ANS_13=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES14||",this.smrIndsQuesData.ANS_14 ? (this.smrIndsQuesData.ANS_14=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));


        debug("this.HtmlFormOutput : " + this.HtmlFormOutput);
    }
}]);

