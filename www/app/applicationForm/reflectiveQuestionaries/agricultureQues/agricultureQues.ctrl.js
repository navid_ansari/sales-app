agricultureQuesModule.controller('agricultureQuesCtrl',['$q','$state','AgriQuesService','LoadData','$stateParams','ViewReportService','ReflectiveQuesService','CommonService','ExistingAppData','rqInfoService','TermData', function($q,$state,AgriQuesService,LoadData,$stateParams,ViewReportService,ReflectiveQuesService,CommonService,ExistingAppData,rqInfoService,TermData){
    debug("LoadData : " + JSON.stringify(LoadData));
    debug("TermData : " + JSON.stringify(TermData));
    var agric = this;
    this.genOpClick = false;
    this.agriQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;

    rqInfoService.setActiveTab(LoadData.url);

    Object.assign(AgriQuesService.LoadData,agric.LoadData);
    Object.assign(AgriQuesService.ExistingAppData,ExistingAppData);

    this.mmList = [
        {"MM_DISPLAY": "Select an Option", "MM_VALUE": null},
        {"MM_DISPLAY": "Acres", "MM_VALUE": "Acres"},
        {"MM_DISPLAY": "Hectares", "MM_VALUE": "Hectares"},
        {"MM_DISPLAY": "Bigha", "MM_VALUE": "Bigha"},
        {"MM_DISPLAY": "Sq meters", "MM_VALUE": "Sq meters"},
        {"MM_DISPLAY": "Sq foot", "MM_VALUE": "Sq foot"},
        {"MM_DISPLAY": "Sq km", "MM_VALUE": "Sq km"},
        {"MM_DISPLAY": "Others", "MM_VALUE": "Others"}

    ];
    this.fmList = [
        {"FM_DISPLAY": "Select an Option", "FM_VALUE": null},
        {"FM_DISPLAY": "Tractor", "FM_VALUE": "Tractor"},
        {"FM_DISPLAY": "Farm Truck", "FM_VALUE": "Farm Truck"},
        {"FM_DISPLAY": "Bun Rake", "FM_VALUE": "Bun Rake"},
        {"FM_DISPLAY": "Cotton Picker", "FM_VALUE": "Cotton Picker"},
        {"FM_DISPLAY": "Mower", "FM_VALUE": "Mower"},
        {"FM_DISPLAY": "Grain Dryer", "FM_VALUE": "Grain Dryer"},
        {"FM_DISPLAY": "Reaper", "FM_VALUE": "Reaper"},
        {"FM_DISPLAY": "Others", "FM_VALUE": "Others"}
    ];
    this.lsList = [
        {"LS_DISPLAY": "Select an Option", "LS_VALUE": null},
        {"LS_DISPLAY": "Cattle", "LS_VALUE": "Cattle"},
        {"LS_DISPLAY": "Sheep", "LS_VALUE": "Sheep"},
        {"LS_DISPLAY": "Poultry", "LS_VALUE": "Poultry"},
        {"LS_DISPLAY": "Others", "LS_VALUE": "Others"},
    ];
    this.frequencyList = [
        {"FR_DISPLAY": "Select an Option", "FR_VALUE": null},
        {"FR_DISPLAY": "Yearly", "FR_VALUE": "Yearly"},
        {"FR_DISPLAY": "Half-Yearly", "FR_VALUE": "Half-Yearly"},
        {"FR_DISPLAY": "Quartely", "FR_VALUE": "Quartely"},
        {"FR_DISPLAY": "Monthly", "FR_VALUE": "Monthly"},
    ];

    this.mmOptionSelected = this.mmList[0];
    this.fmOptionSelected = this.fmList[0];
    this.lsOptionSelected = this.lsList[0];
    this.frequencyOptionSelected = this.frequencyList[0];

    this.mmChg = function(){
        debug("this.mmOptionSelected: " + JSON.stringify(this.mmOptionSelected));
        this.agriQuesData.ANS_A1em = this.mmOptionSelected.MM_VALUE;
        if(this.fmOptionSelected.MM_VALUE != 'Others')
            this.agriQuesData.ANS_A1em_Reason = null;
    };
    this.fmChg = function(){
        debug("this.fmOptionSelected: " + JSON.stringify(this.fmOptionSelected));
        this.agriQuesData.ANS_C12a = this.fmOptionSelected.FM_VALUE;
        if(this.fmOptionSelected.FM_VALUE != 'Others')
            this.agriQuesData.ANS_C12a_Reason = null;
    };
    this.lsChg = function(){
        debug("this.lsOptionSelected: " + JSON.stringify(this.lsOptionSelected));
        this.agriQuesData.ANS_C12b = this.lsOptionSelected.LS_VALUE;
        if(this.lsOptionSelected.LS_VALUE != 'Others')
            this.agriQuesData.ANS_C12b_Reason = null;
    };
    this.frequencyChg = function(){
        debug("this.frequencyOptionSelected: " + JSON.stringify(this.frequencyOptionSelected));
        this.agriQuesData.ANS_D2d = this.frequencyOptionSelected.FR_VALUE;
    };

    this.onGenerateOutput = function(agriQuesForm){
        var dfd = $q.defer();
        this.genOpClick = true;
        if(agriQuesForm.$invalid == false){
            debug("agriQuesData : " + JSON.stringify(this.agriQuesData));
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    debug("agriQuesData 1: " + JSON.stringify(AgriQuesService.agriQuesData));
                    debug("agriQuesData: " + JSON.stringify(agric.agriQuesData));

                    Object.assign(AgriQuesService.agriQuesData,agric.agriQuesData)
                    if(!!fileData)
                        AgriQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.agricultureQues", "REPORT_PARAMS": agric.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Agriculture_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
        return dfd.promise;
    };

    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.agricultureQues", "REPORT_PARAMS": agric.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };
    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };
    this.setNull = function(reasonNo){
        debug("reasonNo : " + reasonNo);
        switch(reasonNo){
            case 'A1fj' :
                if(!this.agriQuesData.ANS_A1fj){
                    this.agriQuesData.ANS_A1g1 = null;
                    this.agriQuesData.ANS_A1g2 = null;
                    this.agriQuesData.ANS_A1g3 = null;
                    this.agriQuesData.ANS_A1g4 = null;
                }
                break;
            case 'A1fn' :
                if(!this.agriQuesData.ANS_A1fn)
                    this.agriQuesData.ANS_A1h = null;
                break;
            case 'C5' :
                this.agriQuesData.ANS_C5Reason = null;
                break;

        }
    };
    CommonService.hideLoading();
}]);

agricultureQuesModule.service('AgriQuesService',['$q','$filter','$state','CommonService',function($q, $filter,$state, CommonService){
    var agris = this;
    this.HtmlFormOutput = "";
    this.agriQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.agriQuesData : " + JSON.stringify(this.agriQuesData));
        this.HtmlFormOutput = fileData;
        var insName = agris.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + agris.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",agris.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||AGE||",CommonService.getAge(agris.ExistingAppData.applicationPersonalInfo.Insured.BIRTH_DATE));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1A||",this.agriQuesData.ANS_A1a ? this.agriQuesData.ANS_A1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1B||",this.agriQuesData.ANS_A1b ? this.agriQuesData.ANS_A1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1C||",this.agriQuesData.ANS_A1c ? this.agriQuesData.ANS_A1c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1D||",this.agriQuesData.ANS_A1d ? this.agriQuesData.ANS_A1d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1E||",this.agriQuesData.ANS_A1e ? this.agriQuesData.ANS_A1e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1EM||",this.agriQuesData.ANS_A1em ? this.agriQuesData.ANS_A1em : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1EMR||",this.agriQuesData.ANS_A1em_Reason ? this.agriQuesData.ANS_A1em_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1FS||",this.agriQuesData.ANS_A1fs ? 'Yes' : 'No');
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1FJ||",this.agriQuesData.ANS_A1fj ? 'Yes' : 'No');
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1FN||",this.agriQuesData.ANS_A1fn ? 'Yes' : 'No');
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1G1||",this.agriQuesData.ANS_A1g1 ? this.agriQuesData.ANS_A1g1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1G2||",this.agriQuesData.ANS_A1g2 ? this.agriQuesData.ANS_A1g2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1G3||",this.agriQuesData.ANS_A1g3 ? this.agriQuesData.ANS_A1g3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1G4||",this.agriQuesData.ANS_A1g4 ? this.agriQuesData.ANS_A1g4 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1H||",this.agriQuesData.ANS_A1h ? this.agriQuesData.ANS_A1h : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1A||",this.agriQuesData.ANS_B1a ? this.agriQuesData.ANS_B1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1B||",this.agriQuesData.ANS_B1b ? this.agriQuesData.ANS_B1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1C||",this.agriQuesData.ANS_B1c ? this.agriQuesData.ANS_B1c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1D||",this.agriQuesData.ANS_B1d ? this.agriQuesData.ANS_B1d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1E||",this.agriQuesData.ANS_B1e ? this.agriQuesData.ANS_B1e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1F||",this.agriQuesData.ANS_B1f ? this.agriQuesData.ANS_B1f : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2A||",this.agriQuesData.ANS_B2a ? this.agriQuesData.ANS_B2a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2B||",this.agriQuesData.ANS_B2b ? this.agriQuesData.ANS_B2b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2C||",this.agriQuesData.ANS_B2c ? this.agriQuesData.ANS_B2c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2D||",this.agriQuesData.ANS_B2d ? this.agriQuesData.ANS_B2d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2E||",this.agriQuesData.ANS_B2e ? this.agriQuesData.ANS_B2e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2F||",this.agriQuesData.ANS_B2f ? this.agriQuesData.ANS_B2f : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3A||",this.agriQuesData.ANS_B3a ? this.agriQuesData.ANS_B3a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3B||",this.agriQuesData.ANS_B3b ? this.agriQuesData.ANS_B3b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3C||",this.agriQuesData.ANS_B3c ? this.agriQuesData.ANS_B3c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3D||",this.agriQuesData.ANS_B3d ? this.agriQuesData.ANS_B3d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3E||",this.agriQuesData.ANS_B3e ? this.agriQuesData.ANS_B3e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3F||",this.agriQuesData.ANS_B3f ? this.agriQuesData.ANS_B3f : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4A||",this.agriQuesData.ANS_B4a ? this.agriQuesData.ANS_B4a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4B||",this.agriQuesData.ANS_B4b ? this.agriQuesData.ANS_B4b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4C||",this.agriQuesData.ANS_B4c ? this.agriQuesData.ANS_B4c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4D||",this.agriQuesData.ANS_B4d ? this.agriQuesData.ANS_B4d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4E||",this.agriQuesData.ANS_B4e ? this.agriQuesData.ANS_B4e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4F||",this.agriQuesData.ANS_B4f ? this.agriQuesData.ANS_B4f : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1||",this.agriQuesData.ANS_C1 ? this.agriQuesData.ANS_C1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC2||",this.agriQuesData.ANS_C2 ? this.agriQuesData.ANS_C2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC3||",this.agriQuesData.ANS_C3 ? this.agriQuesData.ANS_C3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC4||",this.agriQuesData.ANS_C4 ? this.agriQuesData.ANS_C4 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC5||",this.agriQuesData.ANS_C5 ? (this.agriQuesData.ANS_C5=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC5REASON||",this.agriQuesData.ANS_C5Reason ? this.agriQuesData.ANS_C5Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC6||",this.agriQuesData.ANS_C6 ? this.agriQuesData.ANS_C6 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC7||",this.agriQuesData.ANS_C7 ? this.agriQuesData.ANS_C7 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC8A||",this.agriQuesData.ANS_C8a ? (this.agriQuesData.ANS_C8a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC8B||",this.agriQuesData.ANS_C8b ? (this.agriQuesData.ANS_C8b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC8C||",this.agriQuesData.ANS_C8c ? (this.agriQuesData.ANS_C8c=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC9||",this.agriQuesData.ANS_C9 ? this.agriQuesData.ANS_C9 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC10||",this.agriQuesData.ANS_C10 ? (this.agriQuesData.ANS_C10=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC11||",this.agriQuesData.ANS_C11 ? this.agriQuesData.ANS_C11 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC12A||",this.agriQuesData.ANS_C12a ? (this.agriQuesData.ANS_C12a=='Others' ? this.agriQuesData.ANS_C12a_Reason : this.agriQuesData.ANS_C12a) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC12B||",this.agriQuesData.ANS_C12b ? (this.agriQuesData.ANS_C12b=='Others' ? this.agriQuesData.ANS_C12b_Reason : this.agriQuesData.ANS_C12b) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD1A||",this.agriQuesData.ANS_D1a ? this.agriQuesData.ANS_D1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD1B||",this.agriQuesData.ANS_D1b ? this.agriQuesData.ANS_D1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD1C||",this.agriQuesData.ANS_D1c ? ($filter('date')(this.agriQuesData.ANS_D1c, "dd-MM-yyyy")) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2A||",this.agriQuesData.ANS_D2a ? this.agriQuesData.ANS_D2a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2B||",this.agriQuesData.ANS_D2b ? this.agriQuesData.ANS_D2b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2CY||",this.agriQuesData.ANS_D2cy ? this.agriQuesData.ANS_D2cy : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2CM||",this.agriQuesData.ANS_D2cm ? this.agriQuesData.ANS_D2cm : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2D||",this.agriQuesData.ANS_D2d ? this.agriQuesData.ANS_D2d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESD2E||",this.agriQuesData.ANS_D2e ? this.agriQuesData.ANS_D2e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE1A||",this.agriQuesData.ANS_E1a ? this.agriQuesData.ANS_E1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE1B||",this.agriQuesData.ANS_E1b ? this.agriQuesData.ANS_E1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE2A||",this.agriQuesData.ANS_E2a ? this.agriQuesData.ANS_E2a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE2B||",this.agriQuesData.ANS_E2b ? this.agriQuesData.ANS_E2b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE3A||",this.agriQuesData.ANS_E3a ? this.agriQuesData.ANS_E3a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESE3B||",this.agriQuesData.ANS_E3b ? this.agriQuesData.ANS_E3b : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
