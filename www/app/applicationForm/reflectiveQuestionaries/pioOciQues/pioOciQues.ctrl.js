pioOciQuesModule.controller('pioOciQuesCtrl',['$state','PioOciQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,PioOciQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var poc = this;
    this.genOpClick = false;
    this.LoadData = LoadData;
    this.poQuesData = {};

    rqInfoService.setActiveTab(LoadData.url);

    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;

    Object.assign(PioOciQuesService.LoadData,this.LoadData);
    Object.assign(PioOciQuesService.ExistingAppData,ExistingAppData);

    this.onGenerateOutput = function(poQuesForm){
        this.genOpClick = true;
        if(poQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("poQuesData : " + JSON.stringify(this.poQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(PioOciQuesService.poQuesData,poc.poQuesData)
                    if(!!fileData)
                        PioOciQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }

                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.pioOciQues", "REPORT_PARAMS": poc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/PIO_OCI_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.pioOciQues", "REPORT_PARAMS": poc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };

    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };

    this.setNull = function(reasonNo){
        debug("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '6' :
                this.poQuesData.ANS_6a = null;
                this.poQuesData.ANS_6b = null;
                this.poQuesData.ANS_6c = null;
                break;
            case '10' :
                this.poQuesData.ANS_10_Reason = null;
                break;
            case '12' :
                if(!!this.poQuesData.ANS_12)
                    this.poQuesData.ANS_12_Reason = null;
                break;
            case '15' :
                    this.poQuesData.ANS_15_Reason = null;
                    break;
        }
    };
    CommonService.hideLoading();
}]);

pioOciQuesModule.service('PioOciQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var pos = this;
    this.HtmlFormOutput = "";
    this.poQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.poQuesData : " + JSON.stringify(this.poQuesData));
        this.HtmlFormOutput = fileData;
        var insProfCkeck = pos.LoadData.custType == 'Insured' ? true : false;
        if(insProfCkeck){
            var insName = pos.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + pos.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",pos.ExistingAppData.applicationMainData.POLICY_NO);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSDOB||",pos.ExistingAppData.applicationPersonalInfo.Insured.BIRTH_DATE.substring(0,10));
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES2B||",this.poQuesData.ANS_2b ? this.poQuesData.ANS_2b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNATIONALITY||",pos.ExistingAppData.applicationPersonalInfo.Insured.NATIONALITY);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSRESCOUNTRY||",this.poQuesData.ANS_4 ? this.poQuesData.ANS_4 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES5||",this.poQuesData.ANS_5 ? $filter('date')(this.poQuesData.ANS_5, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6||",this.poQuesData.ANS_6 ? (this.poQuesData.ANS_6=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6A||",this.poQuesData.ANS_6a ? this.poQuesData.ANS_6a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6B||",this.poQuesData.ANS_6b ? $filter('date')(this.poQuesData.ANS_6b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6C||",this.poQuesData.ANS_6c ? this.poQuesData.ANS_6c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7A||",this.poQuesData.ANS_7a ? this.poQuesData.ANS_7a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7B||",this.poQuesData.ANS_7b ? $filter('date')(this.poQuesData.ANS_7b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7C||",this.poQuesData.ANS_7c ? this.poQuesData.ANS_7c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES8||",this.poQuesData.ANS_8 ? this.poQuesData.ANS_8 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES9||",this.poQuesData.ANS_9 ? this.poQuesData.ANS_9 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES10||",this.poQuesData.ANS_10 ? this.poQuesData.ANS_10 : this.poQuesData.ANS_10_Reason);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11A||",this.poQuesData.ANS_11a ? $filter('date')(this.poQuesData.ANS_11a, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11B||",this.poQuesData.ANS_11b ? $filter('date')(this.poQuesData.ANS_11b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12||",this.poQuesData.ANS_12 ? this.poQuesData.ANS_12 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12REASON||",this.poQuesData.ANS_12_Reason ? this.poQuesData.ANS_12_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13A||",this.poQuesData.ANS_13a ? this.poQuesData.ANS_13a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13B||",this.poQuesData.ANS_13b ? this.poQuesData.ANS_13b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13C||",this.poQuesData.ANS_13c ? this.poQuesData.ANS_13c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13D||",this.poQuesData.ANS_13d ? this.poQuesData.ANS_13d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13E||",this.poQuesData.ANS_13e ? this.poQuesData.ANS_13e : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13F||",this.poQuesData.ANS_13f ? this.poQuesData.ANS_13f : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14A||",this.poQuesData.ANS_14a ? this.poQuesData.ANS_14a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14B||",this.poQuesData.ANS_14b ? this.poQuesData.ANS_14b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14C||",this.poQuesData.ANS_14c ? this.poQuesData.ANS_14c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14D||",this.poQuesData.ANS_14d ? this.poQuesData.ANS_14d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15||",this.poQuesData.ANS_15 ? (this.poQuesData.ANS_15=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15REASON||",this.poQuesData.ANS_15_Reason ? this.poQuesData.ANS_15_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));

            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPDOB||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES2B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNATIONALITY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPRESCOUNTRY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES5||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES8||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES9||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10REASON||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13D||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13E||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13F||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14D||","");
        }else{
            var proName = pos.ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME + " " + pos.ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",pos.ExistingAppData.applicationMainData.POLICY_NO);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||",proName);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPDOB||",pos.ExistingAppData.applicationPersonalInfo.Proposer.BIRTH_DATE.substring(0,10));
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES2B||",this.poQuesData.ANS_2b ? this.poQuesData.ANS_2b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNATIONALITY||",pos.ExistingAppData.applicationPersonalInfo.Proposer.NATIONALITY);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPRESCOUNTRY||",this.poQuesData.ANS_4 ? this.poQuesData.ANS_4 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES5||",this.poQuesData.ANS_5 ? $filter('date')(this.poQuesData.ANS_5, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6||",this.poQuesData.ANS_6 ? (this.poQuesData.ANS_6=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6A||",this.poQuesData.ANS_6a ? this.poQuesData.ANS_6a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6B||",this.poQuesData.ANS_6b ? $filter('date')(this.poQuesData.ANS_6b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6C||",this.poQuesData.ANS_6c ? this.poQuesData.ANS_6c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7A||",this.poQuesData.ANS_7a ? this.poQuesData.ANS_7a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7B||",this.poQuesData.ANS_7b ? $filter('date')(this.poQuesData.ANS_7b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7C||",this.poQuesData.ANS_7c ? this.poQuesData.ANS_7c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES8||",this.poQuesData.ANS_8 ? this.poQuesData.ANS_8 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES9||",this.poQuesData.ANS_9 ? this.poQuesData.ANS_9 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10||",this.poQuesData.ANS_10 ? this.poQuesData.ANS_10 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10REASON||",this.poQuesData.ANS_10_Reason ? this.poQuesData.ANS_10_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11A||",this.poQuesData.ANS_11a ? $filter('date')(this.poQuesData.ANS_11a, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11B||",this.poQuesData.ANS_11b ? $filter('date')(this.poQuesData.ANS_11b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12||",this.poQuesData.ANS_12 ? this.poQuesData.ANS_12 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12REASON||",this.poQuesData.ANS_12_Reason ? this.poQuesData.ANS_12_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13A||",this.poQuesData.ANS_13a ? this.poQuesData.ANS_13a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13B||",this.poQuesData.ANS_13b ? this.poQuesData.ANS_13b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13C||",this.poQuesData.ANS_13c ? this.poQuesData.ANS_13c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13D||",this.poQuesData.ANS_13d ? this.poQuesData.ANS_13d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13E||",this.poQuesData.ANS_13e ? this.poQuesData.ANS_13e : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13F||",this.poQuesData.ANS_13f ? this.poQuesData.ANS_13f : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14A||",this.poQuesData.ANS_14a ? this.poQuesData.ANS_14a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14B||",this.poQuesData.ANS_14b ? this.poQuesData.ANS_14b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14C||",this.poQuesData.ANS_14c ? this.poQuesData.ANS_14c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14D||",this.poQuesData.ANS_14d ? this.poQuesData.ANS_14d : "");
//            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15||",this.poQuesData.ANS_15 ? this.poQuesData.ANS_15 : "");
			this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15||",this.poQuesData.ANS_15 ? (this.poQuesData.ANS_15=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15REASON||",this.poQuesData.ANS_15_Reason ? this.poQuesData.ANS_15_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));


            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSDOB||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES2B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNATIONALITY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSRESCOUNTRY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES5||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES8||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES9||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES10||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13D||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13E||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13F||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14D||","");
        }


        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
