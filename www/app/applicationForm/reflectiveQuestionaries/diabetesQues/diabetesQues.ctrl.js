diabetesQuesModule.controller('diabetesQuesCtrl',['$state','DiaQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService', 'TermData', function($state, DiaQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService, TermData){
    debug("TermData : " + JSON.stringify(TermData));
    var dc = this;
    this.genOpClick = false;
    this.diabetesQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;
    this.freeTxtRegex = FREE_TEXT_REGEX;
    rqInfoService.setActiveTab(LoadData.url);

    Object.assign(DiaQuesService.LoadData,this.LoadData);
    Object.assign(DiaQuesService.ExistingAppData,ExistingAppData);

    this.noOfInjectionsList = [
        {"NUMBER_DISPLAY": "Select an Option", "NUMBER_VALUE": null},
        {"NUMBER_DISPLAY": "1","NUMBER_VALUE": "1"},
        {"NUMBER_DISPLAY": "2","NUMBER_VALUE": "2"},
        {"NUMBER_DISPLAY": "3","NUMBER_VALUE": "3"},
        {"NUMBER_DISPLAY": "4","NUMBER_VALUE": "4"}
    ];
    this.unitsPerDayList = [
        {"RANGE_DISPLAY": "Select an Option", "RANGE_VALUE": null},
        {"RANGE_DISPLAY": "0-30","RANGE_VALUE": "0-30"},
        {"RANGE_DISPLAY": "31-50","RANGE_VALUE": "31-50"},
        {"RANGE_DISPLAY": ">50","RANGE_VALUE": ">50"}
    ];

    this.noOfInjeOptionSelected = this.noOfInjectionsList[0];
    this.unitsPerDaySelected = this.unitsPerDayList[0];

    this.onNoOfInjeChg = function(){
        debug("this.noOfInjeOptionSelected: " + JSON.stringify(this.noOfInjeOptionSelected));
        this.diabetesQuesData.ANS_5a = this.noOfInjeOptionSelected.NUMBER_VALUE;
    };
    this.onUnitsPerDayChg = function(){
        debug("this.unitsPerDaySelected: " + JSON.stringify(this.unitsPerDaySelected));
        this.diabetesQuesData.ANS_5b = this.unitsPerDaySelected.RANGE_VALUE;
    };

    this.onGenerateOutput = function(diabetesQuesForm){
        this.genOpClick = true;
        if(diabetesQuesForm.$invalid == false){
            debug("diabetesQuesData : " + JSON.stringify(this.diabetesQuesData));
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                        Object.assign(DiaQuesService.dqData,dc.diabetesQuesData)
                    if(!!fileData)
                        DiaQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", "&amp;");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", "&lt;");
            									  HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", "&gt;");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.diabetesQues", "REPORT_PARAMS": dc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Diabetes_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.diabetesQues", "REPORT_PARAMS": dc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };
    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };
    this.setNull = function(reasonNo){
        debug("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '4' :
                this.diabetesQuesData.ANS_4_Reason = null;
                break;
            case '5' :
                this.noOfInjeOptionSelected = this.noOfInjectionsList[0];
                this.unitsPerDaySelected = this.unitsPerDayList[0];
                break;
            case '6' :
                this.diabetesQuesData.ANS_6_Reason = null;
                break;
            case '7' :
                this.diabetesQuesData.ANS_7_Reason = null;
                break;
            case '9' :
                this.diabetesQuesData.ANS_9_Reason = null;
                break;
            case '10' :
                this.diabetesQuesData.ANS_10_Reason = null;
                break;
        }
        //debug("diabetesQuesData : " + JSON.stringify(this.diabetesQuesData));
    };
    CommonService.hideLoading();
}]);


diabetesQuesModule.service('DiaQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var ds = this;
    this.HtmlFormOutput = "";
    this.dqData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.dqData : " + JSON.stringify(this.dqData));
        this.HtmlFormOutput = fileData;
        var insName = ds.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + ds.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",ds.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1||",this.dqData.ANS_1 ? ($filter('date')(this.dqData.ANS_1, "dd-MM-yyyy")) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2||",this.dqData.ANS_2 ? this.dqData.ANS_2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3||",this.dqData.ANS_3 ? ($filter('date')(this.dqData.ANS_3, "dd-MM-yyyy")) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4||",this.dqData.ANS_4 ? (this.dqData.ANS_4=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4REASON||",this.dqData.ANS_4_Reason ? this.dqData.ANS_4_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5||",this.dqData.ANS_5 ? (this.dqData.ANS_5=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5A||",this.dqData.ANS_5a ? this.dqData.ANS_5a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5B||",this.dqData.ANS_5b ? this.dqData.ANS_5b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",this.dqData.ANS_6 ? (this.dqData.ANS_6=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6REASON||",this.dqData.ANS_6_Reason ? this.dqData.ANS_6_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7||",this.dqData.ANS_7 ? (this.dqData.ANS_7=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7REASON||",this.dqData.ANS_7_Reason ? this.dqData.ANS_7_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8A||",this.dqData.ANS_8a ? (this.dqData.ANS_8a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8B||",this.dqData.ANS_8b ? (this.dqData.ANS_8b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8C||",this.dqData.ANS_8c ? (this.dqData.ANS_8c=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8D||",this.dqData.ANS_8d ? (this.dqData.ANS_8d=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8E||",this.dqData.ANS_8e ? (this.dqData.ANS_8e=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8F||",this.dqData.ANS_8f ? (this.dqData.ANS_8f=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8G||",this.dqData.ANS_8g ? (this.dqData.ANS_8g=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9||",this.dqData.ANS_9 ? (this.dqData.ANS_9=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9REASON||",this.dqData.ANS_9_Reason ? this.dqData.ANS_9_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES10||",this.dqData.ANS_10 ? (this.dqData.ANS_10=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES10REASON||",this.dqData.ANS_10_Reason ? this.dqData.ANS_10_Reason : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);

        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
