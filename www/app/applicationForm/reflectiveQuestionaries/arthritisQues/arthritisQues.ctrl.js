arthritisQuesModule.controller('arthritisQuesCtrl',['$state','ArthriQuesService', function($state,ArthriQuesService){
    var arthric = this;
    this.arthritisQuesData = {};

    this.arthriConditionList = [
        {"CONDITION_DISPLAY": "Select an Option", "CONDITION_VALUE": null},
        {"CONDITION_DISPLAY": "Mild","CONDITION_VALUE": "Mild"},
        {"CONDITION_DISPLAY": "Moderate","CONDITION_VALUE": "Moderate"},
        {"CONDITION_DISPLAY": "Severe","CONDITION_VALUE": "Severe"},
        {"CONDITION_DISPLAY": "Improving","CONDITION_VALUE": "Improving"},
        {"CONDITION_DISPLAY": "Progressively","CONDITION_VALUE": "Progressively"},
        {"CONDITION_DISPLAY": "Worsening","CONDITION_VALUE": "Worsening"}
    ];

    this.arthriConditionOptionSelected = this.arthriConditionList[0];

    this.arthriConditionChg = function(){
        debug("this.arthriConditionOptionSelected: " + JSON.stringify(this.arthriConditionOptionSelected));
        this.arthritisQuesData.ANS_3 = this.arthriConditionOptionSelected.CONDITION_VALUE;
    };

    this.onGenerateOutput = function(){
        console.log("arthritisQuesData : " + JSON.stringify(this.arthritisQuesData));

        cordova.exec(
            function(fileData){
                console.log("fileData: " + fileData);
                Object.assign(ArthriQuesService.arthriData,arthric.arthritisQuesData)
                if(!!fileData)
                    ArthriQuesService.generateOutput(fileData);
                else
                    navigator.notification.alert("Template file not found",null,"Application","OK");                    
            },
            function(errMsg){
                debug(errMsg);
            },
            "PluginHandler","getDataFromFileInAssets",["ToClient/ReflQuesTemplates/Asthma.html"]
        );
    };

    this.setNull = function(reasonNo){
        console.log("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '4b' :
                this.arthritisQuesData.ANS_4b_Reason = null;
                break;
            case '4c' :
                this.arthritisQuesData.ANS_4c_Reason = null;
                break;
            case '5a' :
                this.arthritisQuesData.ANS_5a_Reason = null;
                break;
            case '5b' :
                this.arthritisQuesData.ANS_5b_Reason = null;
                break;
        }
    };
}]);

arthritisQuesModule.service('ArthriQuesService',['$q','$filter','CommonService',function($q, $filter, CommonService){
    var arthris = this;
    this.HtmlFormOutput = "";
    this.arthriData = {};
    this.generateOutput = function(fileData){
        debug("this.arthriData : " + JSON.stringify(this.arthriData));
        this.HtmlFormOutput = fileData;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1||",this.arthriData.ANS_1 ? this.arthriData.ANS_1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2||",this.arthriData.ANS_2 ? this.arthriData.ANS_2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3||",this.arthriData.ANS_3 ? this.arthriData.ANS_3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4A||",this.arthriData.ANS_4a ? this.arthriData.ANS_4a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5B||",this.arthriData.ANS_4b ? (this.arthriData.ANS_4b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4BREASON||",this.arthriData.ANS_4b_Reason ? this.arthriData.ANS_4b_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5C||",this.arthriData.ANS_4c ? (this.arthriData.ANS_4c=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4CREASON||",this.arthriData.ANS_4c_Reason ? this.arthriData.ANS_4c_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4D||",this.arthriData.ANS_4d ? this.arthriData.ANS_4d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4E||",this.arthriData.ANS_4e ? this.arthriData.ANS_4e : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5A||",this.arthriData.ANS_5a ? (this.arthriData.ANS_5a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5AREASON||",this.arthriData.ANS_5a_Reason ? this.arthriData.ANS_5a_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5B||",this.arthriData.ANS_5b ? (this.arthriData.ANS_5b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5BREASON||",this.arthriData.ANS_5b_Reason ? this.arthriData.ANS_5b_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",this.arthriData.ANS_6 ? this.arthriData.ANS_6 : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput);
    }
}]);
