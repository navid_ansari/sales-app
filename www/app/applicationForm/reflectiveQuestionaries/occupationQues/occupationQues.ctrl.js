occupationQuesModule.controller('occupationQuesCtrl',['$state','CommonService','LoadData','ExistingAppData','$stateParams','OCCUQuesService','ReflectiveQuesService','ViewReportService','rqInfoService','TermData',function($state,CommonService,LoadData,ExistingAppData,$stateParams,OCCUQuesService,ReflectiveQuesService,ViewReportService,rqInfoService,TermData){
    debug("occupationQuesCtrl controller loaded");
    var occQ = this;
    this.click = false;
    this.occuQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.TermData = TermData;
    this.LoadData.refFlag = true;
    rqInfoService.setActiveTab(LoadData.url);
    var totPercent = 0;

    Object.assign(OCCUQuesService.LoadData,this.LoadData);
    Object.assign(OCCUQuesService.ExistingAppData,ExistingAppData);


this.onGenerateOutput = function(occQuesForm){
        this.click = true;
        totPercent = (parseInt(this.occuQuesData.per1) + parseInt(this.occuQuesData.per2) + parseInt(this.occuQuesData.per3) + parseInt(this.occuQuesData.per4));
        if(occQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("occuQuesData : " + JSON.stringify(this.occuQuesData));
            if(totPercent != 100)
                navigator.notification.alert("Total Percentage should be 100");
            else{
                cordova.exec(
                    function(fileData){
                        debug("fileData: " + fileData.length);
                        Object.assign(OCCUQuesService.occuQuesData,occQ.occuQuesData)
                        if(!!fileData)
                            OCCUQuesService.generateOutput(fileData).then(
                                function(HtmlFormOutput){
                                  if(!!HtmlFormOutput){
                                    HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
                                    HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
                                    HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                                 }
                                    ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                        function(resp){
                                            if(!!resp){
                                                ViewReportService.fetchSavedReport("APP",fileName).then(
                                                    function(res){
                                                        $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.occupationQues", "REPORT_PARAMS": occQ.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                    }
                                                );
                                            }
                                        }
                                    );
                                }
                            );
                        else
                            navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                    },
                    function(errMsg){
                        debug(errMsg);
                    },
                    "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Occupation_Questionnaire.html"]
                );
            }
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };

    this.reGenerate = function(){
            this.isDone = false;
    };

    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.occupationQues", "REPORT_PARAMS": occQ.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };


    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };
	CommonService.hideLoading();
}]);


occupationQuesModule.service('OCCUQuesService',['$q','$state','$filter','CommonService',function($q,$state,$filter,CommonService){
var oc = this;
    this.HtmlFormOutput = "";
    this.occuQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.occuQuesData : " + JSON.stringify(this.occuQuesData));
        this.HtmlFormOutput = fileData;
        var insName = oc.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME+""+oc.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",oc.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1||",oc.occuQuesData.currOccupation ? oc.occuQuesData.currOccupation : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2||",oc.occuQuesData.duties ? oc.occuQuesData.duties : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3||",oc.occuQuesData.occuPeriod ? oc.occuQuesData.occuPeriod : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4a||",oc.occuQuesData.per1 ? parseInt(oc.occuQuesData.per1) : "0");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4b||",oc.occuQuesData.per2 ? parseInt(oc.occuQuesData.per2) : "0");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4c||",oc.occuQuesData.per3 ? parseInt(oc.occuQuesData.per3) : "0");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4d||",oc.occuQuesData.per4 ? parseInt(oc.occuQuesData.per4) : "0");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5a||",oc.occuQuesData.inOutdoor ? (oc.occuQuesData.inOutdoor == 'Indoor' ? 'Indoor':'Outdoor') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5b||",oc.occuQuesData.operMachine ? (oc.occuQuesData.operMachine == 'Operation of Machine' ? 'Operation of Machine':'No Operation of Machine') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5c||",oc.occuQuesData.workHeight ? (oc.occuQuesData.workHeight == 'Work at heights' ? 'Work at heights':'No Work at heights') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5d||",oc.occuQuesData.workOffShore ? (oc.occuQuesData.workOffShore == 'Work offshore or underground' ? 'Work offshore or underground':'No Work offshore or underground') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5e||",oc.occuQuesData.duty ? oc.occuQuesData.duty : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",oc.occuQuesData.jobTravel ? (oc.occuQuesData.jobTravel == 'Y' ? 'Yes': "No") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6Sub||",oc.occuQuesData.kilPerDay ? parseInt(oc.occuQuesData.kilPerDay) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7||",oc.occuQuesData.hazardousQ7 ? (oc.occuQuesData.hazardousQ7 == 'Y' ? 'Yes': "No") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7Sub||",oc.occuQuesData.que7Yes ? oc.occuQuesData.que7Yes : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
        }


}])
