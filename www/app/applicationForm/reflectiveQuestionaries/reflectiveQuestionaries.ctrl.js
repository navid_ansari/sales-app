reflectiveQuesModule.controller('reflectiveQuesCtrl',['$state','LoadReflService','CommonService','ReflectiveQuesService', function($state,LoadReflService,CommonService,ReflectiveQuesService){

    console.log("ReflectiveQuesList : " + JSON.stringify(LoadReflService.ReflectiveQuesList));
	var rqc = this;

	//timeline changes..

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



	this.reflectiveQuesTabs = LoadReflService.ReflectiveQuesList; //[{title : "Agriculture Questionaries", url : "agriQues"},{title : "Arthritis Questionaries", url : "arthriQues"}];

	this.currentTab = this.currentTab ? this.currentTab : LoadReflService.ReflectiveQuesList[0].url;
    ReflectiveQuesService.ReflectiveQuesList = [];
    debug("ReflectiveQuesService1 : " + ReflectiveQuesService.ReflectiveQuesList.length + "\t LoadReflService : " + LoadReflService.ReflectiveQuesList.length);
    Object.assign(ReflectiveQuesService.ReflectiveQuesList,LoadReflService.ReflectiveQuesList);
    debug("ReflectiveQuesService2 : " + ReflectiveQuesService.ReflectiveQuesList.length + "\t LoadReflService : " + LoadReflService.ReflectiveQuesList.length);
	this.isActiveTab = function (tabUrl) {
        return tabUrl == this.currentTab;
    };

	this.gotoPage = function(reflectiveQues){
	    ReflectiveQuesService.gotoPage(reflectiveQues);
	}
    CommonService.hideLoading();
}]);

reflectiveQuesModule.service('ReflectiveQuesService',['$q','$state', 'CommonService','AppTimeService','GenerateOutputService','rqInfoService', function($q,$state, CommonService,AppTimeService,GenerateOutputService,rqInfoService){
    var self = this;
    this.ReflectiveQuesList = [];

    this.gotoPage = function(reflectiveQues){
        debug("reflective quest >>"+reflectiveQues.docId);

		var earlierActivePage = rqInfoService.getActiveTab();
        rqInfoService.setActiveTab(reflectiveQues.docId);

        switch(reflectiveQues.docId){
          //  case 'U20001':
          case '1020010413' :
          case '2020010412' :
//                CommonService.showLoading("Loading...");
                //rqc.activeTab = "agricultureQues";
                console.log("agricultureQues")
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                $state.go('applicationForm.reflectiveQuestionaries.agricultureQues',{"LoadData" : reflectiveQues});
                break;
            /*case 'arthritisQues':
                rqc.activeTab = "arthritisQues";
                console.log("arthritisQues")
                $state.go('applicationForm.reflectiveQuestionaries.arthritisQues',{"LoadData" : reflectiveQues});
                break;
            case 'asthmaQues':
                rqc.activeTab = "asthmaQues";
                console.log("asthmaQues")
                $state.go('applicationForm.reflectiveQuestionaries.asthmaQues',{"LoadData" : reflectiveQues});
                break;
            case 'ccrQues':
                rqc.activeTab = "ccrQues";
                console.log("ccrQues")
                $state.go('applicationForm.reflectiveQuestionaries.ccrQues',{"LoadData" : reflectiveQues});
                break;*/
            //case 'U07701':
            case '1030010293':
            case '2030010273':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "diabetesQues";
                $state.go('applicationForm.reflectiveQuestionaries.diabetesQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U03001':
            case '1020010239':
            case '2020010229':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "houseWifeQues";
                $state.go('applicationForm.reflectiveQuestionaries.houseWifeQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U08201':
            case '1030010296':
            case '2030010276':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "hypertensionQues";
                console.log("hypertensionQues")
                $state.go('applicationForm.reflectiveQuestionaries.hypertensionQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U03402':
            case '1020010414':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "juvenileQues";
                console.log("juvenileQues")
                $state.go('applicationForm.reflectiveQuestionaries.juvenileQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U15301':
            case '7024010377':
            case '4024010226':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "mwpQues";
                console.log("mwpQues")
                $state.go('applicationForm.reflectiveQuestionaries.mwpQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U08403':
            case '1031010309':
            case '1031010308':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "nriQues";
                $state.go('applicationForm.reflectiveQuestionaries.nriQues',{"LoadData" : reflectiveQues});
                break;
            //case 'U20201':
            case '1031010406':
            case '2031010405':
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                //rqc.activeTab = "pioOciQues";
                $state.go('applicationForm.reflectiveQuestionaries.pioOciQues',{"LoadData" : reflectiveQues});
                break;

            //case 'U19801':
            case '1031010420':
            case '2031010419':
//                CommonService.showLoading("Loading...");
                //rqc.activeTab = "pioOciQues";
				if(earlierActivePage!==reflectiveQues.docId){
					CommonService.showLoading();
				}
                $state.go('applicationForm.reflectiveQuestionaries.fatcaQues',{"LoadData" : reflectiveQues});
                break;
            /*case 'smrIndsQues':
                CommonService.showLoading("Loading...");
                rqc.activeTab = "smrIndsQues";
                $state.go('applicationForm.reflectiveQuestionaries.smrIndsQues',{"LoadData" : reflectiveQues});
                break;*/
                case '1020010248':
                case '2020010238':
					if(earlierActivePage!==reflectiveQues.docId){
						CommonService.showLoading();
					}
                    $state.go('applicationForm.reflectiveQuestionaries.occupationQues',{"LoadData" : reflectiveQues});
            default:
                self.activeTab = "agricultureQues";
                console.log("default agricultureQues")
                //$state.go('applicationForm.personalInfo.personalDetails');
        }
    }

    this.getDocPresentData = function(LoadData){
        debug("LoadData : " + JSON.stringify(LoadData));
        var LoadData = LoadData;
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT * FROM LP_DOCUMENT_UPLOAD where DOC_ID = ? and DOC_CAT_ID = ? and AGENT_CD = ?",[LoadData.docId,LoadData.appId,LoadData.agentCd],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("LP_DOCUMENT_UPLOAD record found");
                            LoadData.isDone = true;
                            dfd.resolve(LoadData);
                        }
                        else{
                            debug("LP_DOCUMENT_UPLOAD record not found");
                            LoadData.isDone = false;
                            dfd.resolve(LoadData);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.saveQuesReportFile = function(fileName,fileData){
        var dfd = $q.defer();
        CommonService.saveFile(fileName,".html","/FromClient/APP/HTML/",fileData,"N").then(
            function(resp){
                debug("resp : " + resp);
                if(!!resp){
                    dfd.resolve(resp);
                }
            }
        );
        return dfd.promise;
    };

    this.getDocDescription = function(DOC_ID){
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select * from LP_DOC_PROOF_MASTER where DOC_ID = ?",[DOC_ID],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("DOC_DESC : " + JSON.stringify(res.rows.item(0)));
                            dfd.resolve(res.rows.item(0));
                        }
                        else{
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.onNext = function(loadData){
		CommonService.showLoading();
        debug("ReflectiveQuesService3 : " + this.ReflectiveQuesList.length);
        debug("loadData : " + JSON.stringify(loadData));
        debug("LoadReflService : " + JSON.stringify(this.ReflectiveQuesList));
        var refLen = this.ReflectiveQuesList.length;
        var nextPageIndex = loadData.seqNo + 1;
        var nextPage = null;
        debug("nextPageIndex : " + nextPageIndex + " refLen : " + refLen);
        if(nextPageIndex < refLen){
            nextPage = this.ReflectiveQuesList[nextPageIndex];
            debug("nextPage : " + JSON.stringify(nextPage));
            //rqInfoService.setActiveTab(reflectiveQues.url);
            self.gotoPage(nextPage);
        }
        else{
            debug("reflective done");
            self.getAllFlags(this.ReflectiveQuesList).then(
                function(flagData){
                    debug("flagData : " + JSON.stringify(flagData));
                    for(var i=0;i<flagData.length;i++){
                        if(flagData[i] == false){
                            navigator.notification.alert("Please fill all questionnaire",function(){CommonService.hideLoading();},"Questionnaire","OK");
                            return;
                        }
                    }
                    var whereClauseObj = {};
                    whereClauseObj.APPLICATION_ID = self.ReflectiveQuesList[0].appId;
                    whereClauseObj.AGENT_CD = self.ReflectiveQuesList[0].agentCd;
                    CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN10_COMPLETED":'Y'},whereClauseObj).then(
                        function(res){
                            console.log("Screen updated successfully :IS_SCREEN10_COMPLETED");
                         MAIN_TAB = 'genOutput';
                         //timeline changes..
                         AppTimeService.setActiveTab("genOutput");
                         AppTimeService.isReflQuestCompleted = true;
                         //$rootScope.val = 100;
                         AppTimeService.setProgressValue(100);
                          GenerateOutputService.generateApplicationOutput(self.ReflectiveQuesList[0].appFormServ);
                        }
                    );
                }
            );


        }

    };

    this.getAllFlags = function(ReflectiveQuesList){
        var promises = [];
        for(var i=0;i<ReflectiveQuesList.length;i++){
            promises.push(self.getDocPresentFlags(ReflectiveQuesList[i]));
        }
        return $q.all(promises);
    }

    this.getDocPresentFlags = function(LoadData){
        debug("LoadData : " + JSON.stringify(LoadData));
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT * FROM LP_DOCUMENT_UPLOAD where DOC_ID = ? and DOC_CAT_ID = ? and AGENT_CD = ?",[LoadData.docId,LoadData.appId,LoadData.agentCd],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("getDocPresentFlags LP_DOCUMENT_UPLOAD record found");
                            dfd.resolve(true);
                        }
                        else{
                            debug("getDocPresentFlags LP_DOCUMENT_UPLOAD record not found");
                            dfd.resolve(false);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(false);
                    }
                );
            },
            function(err){
                dfd.resolve(false);
            },null
        );
        return dfd.promise;
    };

}]);
