fatcaQuesModule.controller('fatcaQuesCtrl',['$q','CommonService','LoadData','rqInfoService','FatcaQuesService','ApplicationFormDataService','LoadApplicationScreenData','BirthCountryList','ExistingAppData','ReflectiveQuesService','ViewReportService','ExistingAppData','$stateParams','$state','$filter','ExistingAppMainData','uploadDocService', function($q,CommonService,LoadData,rqInfoService,FatcaQuesService,ApplicationFormDataService,LoadApplicationScreenData,BirthCountryList,ExistingAppData,ReflectiveQuesService,ViewReportService,ExistingAppData,$stateParams,$state,$filter,ExistingAppMainData,uploadDocService){
    debug("LoadApplicationScreenData : " + JSON.stringify(LoadApplicationScreenData));
    debug("ExistingAppData : " + JSON.stringify(ExistingAppData));
    debug("ExistingAppMainData : " + JSON.stringify(ExistingAppMainData));
    debug("ApplicationFormDataService : " + JSON.stringify(ApplicationFormDataService));
    debug("LoadData : " + JSON.stringify(LoadData));
        console.log("Current Tab :",JSON.stringify(LoadData.custType));
    rqInfoService.setActiveTab(LoadData.url);

 var fatc=this;
 fatc.pinRegex = FATCA_ADD_PIN;
 fatc.nameRegex = FULLNAME_REGEX_SP;
 fatc.addressRegex = FATCA_ADD_REGEX;
 fatc.stRegex = ALPHA_NUM_WITHSPACE_REG;
 fatc.isSelf=true;
 this.LoadData = LoadData;
 this.isDone = LoadData.isDone;
 this.LoadData.refFlag = true;
 this.birthCountryList = BirthCountryList;
 Object.assign(FatcaQuesService.LoadData,fatc.LoadData);
 Object.assign(FatcaQuesService.ExistingAppData,ExistingAppData);
 var OCCUPATION_CLASS=null;
 this.fatcQuesData ={};
 this.fatcQuesData.ANS_B1a = this.birthCountryList[0];
 this.fatcQuesData.ANS_B2a = this.birthCountryList[0];
 this.fatcQuesData.ANS_B3a = this.birthCountryList[0];
 this.fatcQuesData.ANS_COB = this.birthCountryList[0];
 this.fatcQuesData.ANS_b1d = this.birthCountryList[0];
 this.fatcQuesData.ANS_b2d = this.birthCountryList[0];
 this.fatcQuesData.ANS_b3d = this.birthCountryList[0];
 this.addressDetBean = {};
// fatc.fatcQuesData.ANS_b2a=null;

 this.row2values = {};

 this.row3values = {};

 fatc.today = new Date();
 ApplicationFormDataService.applicationFormBean.addressDetBean = this.addressDetBean;
 fatc.fatcQuesData.ANS_TAXRES='Y';
 FatcaQuesService.getOccType().then(function(res){
                debug("Occ type value "+JSON.stringify(res.OCC_TYPE));
                fatc.OccList=res.OCC_TYPE;
                fatc.IdentyList=res.ID_TYPE;

//                  this.OCCUPATION_CLASS=ExistingAppData.applicationPersonalInfo;
//                  this.IDENTITY_PROOF=ExistingAppData.applicationPersonalInfo;
                  debug("OCCUPATION_CLASS : " + JSON.stringify(ExistingAppData.applicationPersonalInfo));
//                  debug("IDENTITY_PROOF : " + this.IDENTITY_PROOF);
//                  debug("IDENTITY_PROOF_NO : " + ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF_NO);
    if(!!ExistingAppData.applicationPersonalInfo.Insured && LoadData.custType=='Insured'){
                  fatc.fatcQuesData.ANS_Idno=ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF_NO;
    }else{
              fatc.fatcQuesData.ANS_Idno=ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF_NO;
    }

                  fatc.fatcQuesData.currdate= fatc.today;
                  fatc.fatcQuesData.ANS_B1e = fatc.today;

                   if(!!ExistingAppData.applicationPersonalInfo.Insured && !!ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS && LoadData.custType=='Insured'){
                           if(ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS=='Salaried'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[0];
                                fatc.isDisabled_occ = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS=='Self Employed / Business Owner'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[1];
                                fatc.isDisabled_occ = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS=='Professional'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[2];
                                fatc.isDisabled_occ = true;
                            }else{
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[3];
                                fatc.isDisabled_occ = false;
                            }
                    }
                   if(!!ExistingAppData.applicationPersonalInfo.Insured && ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF && LoadData.custType=='Insured'){
                            if(ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF=='Passport'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[0];
                                fatc.isDisabled_idtype = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF=='Voter ID Card'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[1];
                                fatc.isDisabled_idtype = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Insured.IDENTITY_PROOF=='PAN Card'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[2];
                                fatc.isDisabled_idtype = true;
                            }else{
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[7];
                                fatc.isDisabled_idtype = false;
                                 fatc.isSelf=false;
                            }
                    }
					if(!!ExistingAppData.applicationPersonalInfo.Proposer){
					if(!!ExistingAppData.applicationPersonalInfo && !!ExistingAppData.applicationPersonalInfo.Proposer.OCCUPATION_CLASS && LoadData.custType=='Proposer'){
                           if(ExistingAppData.applicationPersonalInfo.Proposer.OCCUPATION_CLASS=='Salaried'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[0];
                                fatc.isDisabled_occ = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Proposer.OCCUPATION_CLASS=='Self Employed / Business Owner'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[1];
                                fatc.isDisabled_occ = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Proposer.OCCUPATION_CLASS=='Professional'){
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[2];
                                fatc.isDisabled_occ = true;
                            }else{
                                fatc.fatcQuesData.ANS_OCC = fatc.OccList[3];
                                fatc.isDisabled_occ = false;
                            }
                    }
                   if(!!ExistingAppData.applicationPersonalInfo.Proposer && ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF && LoadData.custType=='Proposer'){
                           if(ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF=='Passport'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[0];
                                fatc.isDisabled_idtype = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF=='Voter ID Card'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[1];
                                fatc.isDisabled_idtype = true;
                            }else if(ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF=='PAN Card'){
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[2];
                                fatc.isDisabled_idtype = true;
                            }else{
                                fatc.fatcQuesData.ANS_IDTYPE = fatc.IdentyList[7];
                                fatc.isDisabled_idtype = false;
                                 fatc.isSelf=false;
                            }
                    }
					}
                });

  this.validitydate1=function(validitydate){

  var date1=(validitydate.getFullYear()+","+(validitydate.getMonth()+1)+","+validitydate.getDate());
  var date2=(fatc.today.getFullYear()+","+(fatc.today.getMonth()+1)+","+fatc.today.getDate());

         if((new Date(date1)-new Date(date2)) < 0){
                     navigator.notification.alert('Validity date can not be less than current date ',function(){CommonService.hideLoading();},"Application","OK");
                     fatc.fatcQuesData.ANS_B1e = fatc.today;
                  }
  }

 this.validitydate2=function(validitydate){
   var date1=(validitydate.getFullYear()+","+(validitydate.getMonth()+1)+","+validitydate.getDate());
    var date2=(fatc.today.getFullYear()+","+(fatc.today.getMonth()+1)+","+fatc.today.getDate());

          if((new Date(date1)-new Date(date2)) < 0){
                      navigator.notification.alert('Validity date can not be less than current date ',function(){CommonService.hideLoading();},"Application","OK");
                      fatc.fatcQuesData.ANS_B2e = null;
                     }
   }

 this.validitydate3=function(validitydate){
     var date1=(validitydate.getFullYear()+","+(validitydate.getMonth()+1)+","+validitydate.getDate());
      var date2=(fatc.today.getFullYear()+","+(fatc.today.getMonth()+1)+","+fatc.today.getDate());

            if((new Date(date1)-new Date(date2)) < 0){
                        navigator.notification.alert('Validity date can not be less than current date ',function(){CommonService.hideLoading();},"Application","OK");
                        fatc.fatcQuesData.ANS_B3e = null;
                       }
     }

 this.onGenerateOutput = function(fatcaQuesForm){

          var dfd = $q.defer();
          this.genOpClick = true;
          var condrow2 = true;
          var condrow3 = true;
          console.log("row2 val "+condrow2+" row3 val "+condrow3);

          if(fatcaQuesForm.$invalid == false && (condrow2==true) && (condrow3==true)){
              var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
              cordova.exec(
                  function(fileData){
                      debug("fileData: " + fileData.length);
                      FatcaQuesService.fatcQuesData=fatc.fatcQuesData;
                      debug("FatcaQuesService fatcQuesData: " + JSON.stringify(FatcaQuesService.fatcQuesData));
                      Object.assign(fatc.fatcQuesData)
                      if(!!fileData)
                          FatcaQuesService.generateOutput(fileData).then(
                          function(HtmlFormOutput){
                              debug("Uki 1788");
                                  ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                      function(resp){
                                          if(!!resp){
                                              ViewReportService.fetchSavedReport("APP",fileName).then(
                                                  function(res){
                                                        var whereClauseObj = {};
                                                        whereClauseObj.APPLICATION_ID = fatc.LoadData.appId;
                                                        whereClauseObj.AGENT_CD = fatc.LoadData.agentCd ;

                                                        uploadDocService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj,true).then(
                                                               function(inserted){
                                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.fatcaQues", "REPORT_PARAMS": fatc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});

                                                                }
                                                        );
                                                      //$state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.fatcaQues", "REPORT_PARAMS": fatc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                  }
                                              );
                                          }
                                      }
                                  );
                              }
                          );
                      else
                          navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                      },
                      function(errMsg){
                          debug(errMsg);
                      },
                      "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Fatca_Questionnaire.html"]
              );
              }else{
                  navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
              }
              return dfd.promise;
     };

    this.reGenerate = function(){
         this.isDone = false;
     };
    this.showPreview = function(){
         var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
         ViewReportService.fetchSavedReport("APP",fileName).then(
             function(res){
                 debug("Fatca Report : " + res);
                 $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.fatcaQues", "REPORT_PARAMS": fatc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
             }
         );
     };
    this.onNext = function(){
         ReflectiveQuesService.onNext(this.LoadData);
     };
     CommonService.hideLoading();
 }]);

 fatcaQuesModule.service('FatcaQuesService',['$q','$filter','$state','CommonService','ApplicationFormDataService','uploadDocService',function($q, $filter,$state, CommonService,ApplicationFormDataService,uploadDocService){
    var fatc = this;
    this.HtmlFormOutput = "";
    this.fatcQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
//    this.ExistingAppMainData = {};
    this.ApplicationFormDataService = {};


    this.generateOutput = function(fileData){
            var dfd = $q.defer();
            debug("this.fatcQuesData : " + JSON.stringify(this.fatcQuesData));
            debug("cust type : " + JSON.stringify(fatc.LoadData.custType));
            debug("fatc.ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST " + JSON.stringify(fatc.ExistingAppData.applicationPersonalInfo));
            if(this.fatcQuesData.ANS_b2d.COUNTRY_NAME=="Select "){
            this.fatcQuesData.ANS_b2d.COUNTRY_NAME="";
            }if(this.fatcQuesData.ANS_b3d.COUNTRY_NAME=="Select "){
            this.fatcQuesData.ANS_b3d.COUNTRY_NAME="";
            }if(this.fatcQuesData.ANS_B1a.COUNTRY_NAME=="Select "){
            this.fatcQuesData.ANS_B1a.COUNTRY_NAME="";
            }if(this.fatcQuesData.ANS_B2a.COUNTRY_NAME=="Select "){
            this.fatcQuesData.ANS_B2a.COUNTRY_NAME="";
            }if(this.fatcQuesData.ANS_B3a.COUNTRY_NAME=="Select "){
            this.fatcQuesData.ANS_B3a.COUNTRY_NAME="";
            }
            this.HtmlFormOutput = fileData;
            if(fatc.LoadData.custType=="Insured"){
                var insName = fatc.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + fatc.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
                var custType="C01";
            }else{
                var insName = fatc.ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME + " " + fatc.ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
                var custType="C02";
            }
            var place = fatc.ExistingAppData.applicationMainData.APPLICATION_PLACE ;
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PLACE||",place);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",fatc.ExistingAppData.applicationMainData.POLICY_NO);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESFN||",this.fatcQuesData.ANS_fn ? this.fatcQuesData.ANS_fn : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESPOB||",this.fatcQuesData.ANS_pob ? this.fatcQuesData.ANS_pob : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESCOB||",this.fatcQuesData.ANS_COB.COUNTRY_NAME ? this.fatcQuesData.ANS_COB.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESOCC||",this.fatcQuesData.ANS_OCC ? this.fatcQuesData.ANS_OCC : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESIDTYPE||",this.fatcQuesData.ANS_IDTYPE ? this.fatcQuesData.ANS_IDTYPE : "");
            if(!!this.fatcQuesData.IDENTITY_PROOF_OTHER){
                 var otherType = "-" + this.fatcQuesData.IDENTITY_PROOF_OTHER;
                 this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||OTHER_QUESTTYPE||",otherType);
            }else
                this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||OTHER_QUESTTYPE||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESIDNO||",this.fatcQuesData.ANS_Idno ? this.fatcQuesData.ANS_Idno : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESTAX||",this.fatcQuesData.ANS_TAXRES ? (this.fatcQuesData.ANS_TAXRES=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1A||",this.fatcQuesData.ANS_B1a.COUNTRY_NAME ? this.fatcQuesData.ANS_B1a.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1BLDG||",this.fatcQuesData.ANS_B1b_BLDG ? this.fatcQuesData.ANS_B1b_BLDG : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1STRT||",this.fatcQuesData.ANS_B1b_STRT ? this.fatcQuesData.ANS_B1b_STRT : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1CITY||",this.fatcQuesData.ANS_B1b_CITY ? this.fatcQuesData.ANS_B1b_CITY : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1STATE||",this.fatcQuesData.ANS_B1b_STATE ? this.fatcQuesData.ANS_B1b_STATE : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1PIN||",this.fatcQuesData.ANS_B1b_PIN ? this.fatcQuesData.ANS_B1b_PIN : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1C||",this.fatcQuesData.ANS_B1c ? this.fatcQuesData.ANS_B1c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1D||",this.fatcQuesData.ANS_b1d.COUNTRY_NAME ? this.fatcQuesData.ANS_b1d.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1E||",!!this.fatcQuesData.ANS_B1e ?( this.fatcQuesData.ANS_B1e.getDate()+"-"+(this.fatcQuesData.ANS_B1e.getMonth()+1)+"-"+this.fatcQuesData.ANS_B1e.getFullYear() ): "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2A||",this.fatcQuesData.ANS_B2a.COUNTRY_NAME ? this.fatcQuesData.ANS_B2a.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2BLDG||",this.fatcQuesData.ANS_B2b_BLDG ? this.fatcQuesData.ANS_B2b_BLDG : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2STRT||",this.fatcQuesData.ANS_B2b_STRT ? this.fatcQuesData.ANS_B2b_STRT : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2CITY||",this.fatcQuesData.ANS_B2b_CITY ? this.fatcQuesData.ANS_B2b_CITY : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2STATE||",this.fatcQuesData.ANS_B2b_STATE ? this.fatcQuesData.ANS_B2b_STATE : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2PIN||",this.fatcQuesData.ANS_B2b_PIN ? this.fatcQuesData.ANS_B2b_PIN : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2C||",this.fatcQuesData.ANS_B2c ? this.fatcQuesData.ANS_B2c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2D||",this.fatcQuesData.ANS_b2d.COUNTRY_NAME ? this.fatcQuesData.ANS_b2d.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2E||",!!this.fatcQuesData.ANS_B2e ? ( this.fatcQuesData.ANS_B2e.getDate()+"-"+(this.fatcQuesData.ANS_B2e.getMonth()+1)+"-"+this.fatcQuesData.ANS_B2e.getFullYear() ) : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3A||",this.fatcQuesData.ANS_B3a.COUNTRY_NAME ? this.fatcQuesData.ANS_B3a.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3BLDG||",this.fatcQuesData.ANS_B3b_BLDG ? this.fatcQuesData.ANS_B3b_BLDG : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3STRT||",this.fatcQuesData.ANS_B3b_STRT ? this.fatcQuesData.ANS_B3b_STRT : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3CITY||",this.fatcQuesData.ANS_B3b_CITY ? this.fatcQuesData.ANS_B3b_CITY : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3STATE||",this.fatcQuesData.ANS_B3b_STATE ? this.fatcQuesData.ANS_B3b_STATE : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3PIN||",this.fatcQuesData.ANS_B3b_PIN ? this.fatcQuesData.ANS_B3b_PIN : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3C||",this.fatcQuesData.ANS_B3c ? this.fatcQuesData.ANS_B3c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3D||",this.fatcQuesData.ANS_b3d.COUNTRY_NAME ? this.fatcQuesData.ANS_b3d.COUNTRY_NAME : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3E||",!!this.fatcQuesData.ANS_B3e ? ( this.fatcQuesData.ANS_B3e.getDate()+"-"+(this.fatcQuesData.ANS_B3e.getMonth()+1)+"-"+this.fatcQuesData.ANS_B3e.getFullYear() ) : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||CURRDATE||",this.fatcQuesData.currdate ? ( this.fatcQuesData.currdate.getDate()+"-"+(this.fatcQuesData.currdate.getMonth()+1)+"-"+this.fatcQuesData.currdate.getFullYear() ) : "");

            debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);

            ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
                        Reflex :{
                            Insured:{},
                            Proposer:{}
                        },
                        NonReflex :{
                            Insured:{},
                            Proposer:{}
                        }
                    };

            var updateClauseObj = {};
            updateClauseObj.FATCA_FATHER_NAME = this.fatcQuesData.ANS_fn;
            updateClauseObj.FATCA_BIRTH_PLACE = this.fatcQuesData.ANS_pob;
            updateClauseObj.FATCA_BIRTH_COUNTRY = this.fatcQuesData.ANS_COB.COUNTRY_NAME;
            updateClauseObj.FATCA_BIRTH_COUNTRY_CODE = this.fatcQuesData.ANS_COB.COUNTRY_CODE;
            updateClauseObj.FATCA_OCC_TYPE = this.fatcQuesData.ANS_OCC;
            updateClauseObj.FATCA_ID_TYPE = this.fatcQuesData.ANS_IDTYPE;
            updateClauseObj.FATCA_ID_NO = this.fatcQuesData.ANS_Idno;

            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = fatc.LoadData.appId;
            whereClauseObj.AGENT_CD = fatc.LoadData.agentCd;
            whereClauseObj.CUST_TYPE = custType;

            CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', updateClauseObj,whereClauseObj).then(
                        function(res){
                        console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(updateClauseObj));
            });

            var paramDoc1 = {"APPLICATION_ID":fatc.LoadData.appId, "AGENT_CD":fatc.LoadData.agentCd, "SEQ_NO":"1", "COUNTRY_TAX":this.fatcQuesData.ANS_B1a.COUNTRY_NAME, "ADDRESS_TAX_BLDG":this.fatcQuesData.ANS_B1b_BLDG, "ADDRESS_TAX_STREET":this.fatcQuesData.ANS_B1b_STRT, "ADDRESS_TAX_CITY":this.fatcQuesData.ANS_B1b_CITY, "ADDRESS_TAX_STATE":this.fatcQuesData.ANS_B1b_STATE,"ADDRESS_TAX_PINCODE":this.fatcQuesData.ANS_B1b_PIN, "TIN":this.fatcQuesData.ANS_B1c,"TIN_COUNTRY":this.fatcQuesData.ANS_b1d.COUNTRY_NAME,"TIN_COUNTRY_CODE":this.fatcQuesData.ANS_b1d.COUNTRY_CODE,"DOC_VALIDITY":CommonService.formatDobToDb(this.fatcQuesData.ANS_B1e),"MODIFIED_DATE":CommonService.getCurrDate()};
            var paramDoc2 = {};
            var paramDoc3 = {};
            var paramDocMain = [];

            var docInsStatus = {firstDoc : false, secondDoc : false, thirdDoc : false};

            paramDocMain.push(paramDoc1);
            if(!!this.fatcQuesData.ANS_B2a &&  !!this.fatcQuesData.ANS_B2b_BLDG && !!this.fatcQuesData.ANS_B2c && !!this.fatcQuesData.ANS_b2d.COUNTRY_NAME ){
                paramDoc2 = {"APPLICATION_ID":fatc.LoadData.appId, "AGENT_CD":fatc.LoadData.agentCd, "SEQ_NO":"2", "COUNTRY_TAX":this.fatcQuesData.ANS_B2a.COUNTRY_NAME, "ADDRESS_TAX_BLDG": this.fatcQuesData.ANS_B2b_BLDG, "ADDRESS_TAX_STREET":this.fatcQuesData.ANS_B2b_STRT, "ADDRESS_TAX_CITY":this.fatcQuesData.ANS_B2b_CITY, "ADDRESS_TAX_STATE":this.fatcQuesData.ANS_B2b_STATE,"ADDRESS_TAX_PINCODE":this.fatcQuesData.ANS_B2b_PIN, "TIN":this.fatcQuesData.ANS_B2c,"TIN_COUNTRY":this.fatcQuesData.ANS_b2d.COUNTRY_NAME,"TIN_COUNTRY_CODE":this.fatcQuesData.ANS_b2d.COUNTRY_CODE,"DOC_VALIDITY":CommonService.formatDobToDb(this.fatcQuesData.ANS_B2e),"MODIFIED_DATE":CommonService.getCurrDate()};
                paramDocMain.push(paramDoc2);
            }

            if(!!this.fatcQuesData.ANS_B3a && !!this.fatcQuesData.ANS_B3b_BLDG && !!this.fatcQuesData.ANS_B3c && !!this.fatcQuesData.ANS_b3d.COUNTRY_NAME ){
                paramDoc3 = {"APPLICATION_ID":fatc.LoadData.appId, "AGENT_CD":fatc.LoadData.agentCd, "SEQ_NO":"3", "COUNTRY_TAX":this.fatcQuesData.ANS_B3a.COUNTRY_NAME, "ADDRESS_TAX_BLDG": this.fatcQuesData.ANS_B3b_BLDG, "ADDRESS_TAX_STREET":this.fatcQuesData.ANS_B3b_STRT, "ADDRESS_TAX_CITY":this.fatcQuesData.ANS_B3b_CITY, "ADDRESS_TAX_STATE":this.fatcQuesData.ANS_B3b_STATE,"ADDRESS_TAX_PINCODE":this.fatcQuesData.ANS_B3b_PIN, "TIN":this.fatcQuesData.ANS_B3c,"TIN_COUNTRY":this.fatcQuesData.ANS_b3d.COUNTRY_NAME,"TIN_COUNTRY_CODE":this.fatcQuesData.ANS_b3d.COUNTRY_CODE,"DOC_VALIDITY":CommonService.formatDobToDb(this.fatcQuesData.ANS_B3e),"MODIFIED_DATE":CommonService.getCurrDate()};
                paramDocMain.push(paramDoc3);
            }
            debug("2222 asa");
            debug(JSON.stringify(paramDocMain));
            //Fatca First Row insertion - Mandatory
            fatc.UpdateLP_APP_FATCA_DOC_Data(paramDocMain).then(function(res){
                    if(!!res){
                           // First Doc Bean DOC LIST Insertiong
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(fatc.ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
                           debug("RETURNED OBJECT "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

                           if(fatc.LoadData.custType=='Insured'){
                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca1={};

                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca1.DOC_ID="1031010445";
                                console.log("doc id value1 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                           }else{
                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca1 = {};

                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca1.DOC_ID="2031010442";
                                console.log("doc id value1 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                           }

                            // Second Doc Bean DOC LIST Inserting
                             debug(JSON.stringify(paramDoc2));
                            if(Object.keys(paramDoc2).length > 0){
                                debug("RETURNED OBJECT DOC 2"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

                                if(fatc.LoadData.custType=='Insured'){
                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca2={};

                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca2.DOC_ID="1031010446";
                                     console.log("doc id value2 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                                }else{
                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca2 = {};

                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca2.DOC_ID="2031010443";
                                     console.log("doc id value2 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                                }
                            }
                            debug(JSON.stringify(paramDoc3));
                            // Third Doc Bean DOC LIST Inserting
                            if(Object.keys(paramDoc3).length>0){
                                debug("RETURNED OBJECT DOC 3"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

                                if(fatc.LoadData.custType=='Insured'){
                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca3={};

                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca3.DOC_ID="1031010447";
                                     console.log("doc id value3 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                                }else{
                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca3 = {};

                                     ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca3.DOC_ID="2031010444";
                                     console.log("doc id value3 !!"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                                }
                            }
                            dfd.resolve(fatc.HtmlFormOutput);
                    }else{
                        debug("Doc insertion failed.");
                        dfd.resolve(null);
                    }
            });
            return dfd.promise;
    };

    this.UpdateLP_APP_FATCA_DOC_Data = function(parameterList){
            debug(parameterList);
            var paramsLength = parameterList.length;
            var columnLength = parameterList[0].length;      // Because first document is mandatory
            var tableName = "LP_APP_FATCA_DOC";

            var dfd = $q.defer();
            var insertQuery = "insert or replace into " + tableName + " ( ";

            var columnNames = Object.keys(parameterList[0]);
            debug(columnNames);
            insertQuery += columnNames[0];
            var questionList = "?";
            for(var i=1;i<columnNames.length;i++){
              insertQuery += ", " + columnNames[i];
              questionList += ", ?";
            }

            var valuesStr = "";
            for(var i=0;i<paramsLength;i++){
                  valuesStr += "(";
                  for(var j=0;j<columnNames.length;j++){
//                        var values = Object.values(parameterList[i]);
                        var values = Object.keys(parameterList[i]).map(function (key) {
                            return parameterList[i][key];
                        });

                        if(j>0)
                          valuesStr += ", "

                        valuesStr += "'" + values[j] + "'";
                  }
                  valuesStr += ")";
                  if(i!=paramsLength-1 )
                        valuesStr += ",";
            }
            console.log(valuesStr);

            insertQuery += " ) values " + valuesStr + " ";
            debug(insertQuery);
            CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,insertQuery,[],
                        function(tx,res){
                            debug("inserted record successfully!");
                            dfd.resolve(true);
                        },
                        function(tx,err){
                            dfd.resolve();
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );

           return dfd.promise;
    };


//GET OCCUPATION AND ID TYPE
    this.getOccType = function(){
    console.log("In service");
    var dfd =$q.defer();
    CommonService.selectRecords(db, 'LP_LOOKUP_MASTER', true, null, null, null).then(
                function(res){
                    if(!!res && res.rows.length>0){
                        fatc.occtype=[];
                        fatc.idtype=[];
                        fatc.data={};
                         console.log("result val "+JSON.stringify(res));
                            for(var i=0;i<res.rows.length;i++){
                            console.log("result val "+JSON.stringify(res.rows.item(i)));
                            if(res.rows.item(i).LOOKUP_TYPE=='FATCAOcc'){

                            fatc.occtype.push(res.rows.item(i).LOOKUP_TYPE_DESC);
                            }else if(res.rows.item(i).LOOKUP_TYPE=='FATCAId'){
                            fatc.idtype.push(res.rows.item(i).LOOKUP_TYPE_DESC);
                            }
                            }
                            fatc.data.OCC_TYPE=fatc.occtype;
                            fatc.data.ID_TYPE=fatc.idtype;
                            console.log("data val "+JSON.stringify(fatc.data));
                            dfd.resolve(fatc.data);
                    }else{
                            dfd.resolve(null);
                        }
                        });
        return dfd.promise;
      };
}]);
