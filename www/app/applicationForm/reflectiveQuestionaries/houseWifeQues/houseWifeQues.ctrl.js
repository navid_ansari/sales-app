houseWifeQuesModule.controller('houseWifeQuesCtrl',['$state','HwQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,HwQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var hwc = this;
    this.genOpClick = false;
    this.hwQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;

    rqInfoService.setActiveTab(LoadData.url);

    Object.assign(HwQuesService.LoadData,this.LoadData);
    Object.assign(HwQuesService.ExistingAppData,ExistingAppData);

    this.educationList = [
        {"EDU_DISPLAY": "Select an Option", "EDU_VALUE": null},
        {"EDU_DISPLAY": "Graduate", "EDU_VALUE": "Graduate"},
        {"EDU_DISPLAY": "Post Graduate", "EDU_VALUE": "Post Graduate"},
        {"EDU_DISPLAY": "Professional(eg. CA/MBA)", "EDU_VALUE": "Professional"},
        {"EDU_DISPLAY": "Others", "EDU_VALUE": "Others"}
    ];

    this.insPurposeList = [
        {"IP_DISPLAY": "Select an Option", "IP_VALUE": null},
        {"IP_DISPLAY": "Protection for Dependents", "IP_VALUE": "Protection for Dependents"},
        {"IP_DISPLAY": "Protection for Mortgage Loan", "IP_VALUE": "Protection for Mortgage Loan"},
        {"IP_DISPLAY": "Protection for Estate Duties", "IP_VALUE": "Protection for Estate Duties"},
        {"IP_DISPLAY": "Protection for Education Fund", "IP_VALUE": "Protection for Education Fund"},
        {"IP_DISPLAY": "Others", "IP_VALUE": "Others"}
    ];

    this.educationOptionSelected = this.educationList[0];
    this.insPurposeOptionSelected = this.insPurposeList[0];

    this.onEducationChg = function(){
        debug("this.educationOptionSelected: " + JSON.stringify(this.educationOptionSelected));
        this.hwQuesData.ANS_A1 = this.educationOptionSelected.EDU_VALUE;
        this.hwQuesData.ANS_A1_Reason = this.educationOptionSelected.EDU_VALUE!='Others'?null:this.hwQuesData.ANS_A1_Reason;
    };
    this.onInsPurposeChg = function(){
        debug("this.insPurposeOptionSelected: " + JSON.stringify(this.insPurposeOptionSelected));
        this.hwQuesData.ANS_A2 = this.insPurposeOptionSelected.IP_VALUE;
        this.hwQuesData.ANS_A2_Reason = this.insPurposeOptionSelected.IP_VALUE!='Others'?null:this.hwQuesData.ANS_A2_Reason;
    };
    this.onGenerateOutput = function(hwQuesForm){
        this.genOpClick = true;
        if(hwQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("hwQuesData : " + JSON.stringify(this.hwQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(HwQuesService.hwqData,hwc.hwQuesData)
                    if(!!fileData)
                        HwQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", "&amp;");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", "&lt;");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", "&gt;");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.houseWifeQues", "REPORT_PARAMS": hwc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Housewife_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.houseWifeQues", "REPORT_PARAMS": hwc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };
    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };
    CommonService.hideLoading();
}]);

houseWifeQuesModule.service('HwQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var hws = this;
    this.HtmlFormOutput = "";
    this.hwqData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.hwqData : " + JSON.stringify(this.hwqData));
        this.HtmlFormOutput = fileData;
        var insName = hws.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + hws.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",hws.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1||",this.hwqData.ANS_A1 ? (this.hwqData.ANS_A1=='Others' ? this.hwqData.ANS_A1_Reason : this.hwqData.ANS_A1) : "");
        //this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1REASON||",this.hwqData.ANS_A1_Reason ? this.hwqData.ANS_A1_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA2||",this.hwqData.ANS_A2 ? (this.hwqData.ANS_A2=='Others' ? this.hwqData.ANS_A2_Reason : this.hwqData.ANS_A2) : "");
        //this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA2REASON||",this.hwqData.ANS_A2_Reason ? this.hwqData.ANS_A2_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA3a||",this.hwqData.ANS_A3a ? this.hwqData.ANS_A3a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA3b||",this.hwqData.ANS_A3b ? this.hwqData.ANS_A3b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1||",this.hwqData.ANS_B1 ? this.hwqData.ANS_B1 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2||",this.hwqData.ANS_B2 ? this.hwqData.ANS_B2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3||",this.hwqData.ANS_B3 ? this.hwqData.ANS_B3 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4a||",this.hwqData.ANS_B4a ? this.hwqData.ANS_B4a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB4b||",this.hwqData.ANS_B4b ? this.hwqData.ANS_B4b : "");

        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };
}]);
