reflectiveQuesModule.controller('reflInfoCtrl',['rqInfoService','LoadReflService','ReflectiveQuesService',function(rqInfoService,LoadReflService,ReflectiveQuesService){

    this.reflexTabArr = ReflectiveQuesService.ReflectiveQuesList;//LoadReflService.ReflectiveQuesList;
    debug("Refelxive question list");
    debug(this.reflexTabArr);

    this.activeTab = rqInfoService.getActiveTab();

    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        ReflectiveQuesService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.reflexTabArr));
}]);

reflectiveQuesModule.service('rqInfoService',[function(){
    debug("reflex service loaded");

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

reflectiveQuesModule.directive('rqTimeline',[function() {
        "use strict";
        debug('in rqTimeline');
        return {
            restrict: 'E',
            controller: "reflInfoCtrl",
            controllerAs: "rqtc",
            bindToController: true,
            templateUrl: "applicationForm/reflectiveQuestionaries/reflectiveQuestionariesTimeline.html"
        };
}]);