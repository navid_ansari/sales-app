mwpQuesModule.controller('mwpQuesCtrl',['$state','MWPQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,MWPQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var mwpc = this;
    this.genOpClick = false;
    this.mwpQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;

    rqInfoService.setActiveTab(LoadData.url);

    Object.assign(MWPQuesService.LoadData,this.LoadData);
    Object.assign(MWPQuesService.ExistingAppData,ExistingAppData);

    this.onGenerateOutput = function(mwpQuesForm){
        this.genOpClick = true;
        if(mwpQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("mwpQuesData : " + JSON.stringify(this.mwpQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(MWPQuesService.mwpQuesData,mwpc.mwpQuesData)
                    if(!!fileData)
                        MWPQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.mwpQues", "REPORT_PARAMS": mwpc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/MWP_Addendum_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.mwpQues", "REPORT_PARAMS": mwpc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };

    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };
	CommonService.hideLoading();
}]);

mwpQuesModule.service('MWPQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var mwps = this;
    this.HtmlFormOutput = "";
    this.mwpQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.mwpQuesData : " + JSON.stringify(this.mwpQuesData));
        this.HtmlFormOutput = fileData;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",mwps.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||FIRSTNAME||",mwps.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||MIDDLENAME||",(!!mwps.ExistingAppData.applicationPersonalInfo.Insured.MIDDLE_NAME ? mwps.ExistingAppData.applicationPersonalInfo.Insured.MIDDLE_NAME : ""));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||LASTNAME||",mwps.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME);
        //this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||","Ramesh Joshi");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES21A||",this.mwpQuesData.ANS_21a ? this.mwpQuesData.ANS_21a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES21B||",this.mwpQuesData.ANS_21b ? this.mwpQuesData.ANS_21b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES21C||",this.mwpQuesData.ANS_21c ? this.mwpQuesData.ANS_21c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES22A||",this.mwpQuesData.ANS_22a ? this.mwpQuesData.ANS_22a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES22B||",this.mwpQuesData.ANS_22b ? this.mwpQuesData.ANS_22b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES22C||",this.mwpQuesData.ANS_22c ? this.mwpQuesData.ANS_22c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES23A||",this.mwpQuesData.ANS_23a ? this.mwpQuesData.ANS_23a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES23B||",this.mwpQuesData.ANS_23b ? this.mwpQuesData.ANS_23b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES23C||",this.mwpQuesData.ANS_23c ? this.mwpQuesData.ANS_23c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES41A||",this.mwpQuesData.ANS_41a ? this.mwpQuesData.ANS_41a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES41B||",this.mwpQuesData.ANS_41b ? this.mwpQuesData.ANS_41b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES41C||",this.mwpQuesData.ANS_41c ? this.mwpQuesData.ANS_41c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES42A||",this.mwpQuesData.ANS_42a ? this.mwpQuesData.ANS_42a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES42B||",this.mwpQuesData.ANS_42b ? this.mwpQuesData.ANS_42b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES42C||",this.mwpQuesData.ANS_42c ? this.mwpQuesData.ANS_42c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES43A||",this.mwpQuesData.ANS_43a ? this.mwpQuesData.ANS_43a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES43B||",this.mwpQuesData.ANS_43b ? this.mwpQuesData.ANS_43b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES43C||",this.mwpQuesData.ANS_43c ? this.mwpQuesData.ANS_43c : "");

        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
