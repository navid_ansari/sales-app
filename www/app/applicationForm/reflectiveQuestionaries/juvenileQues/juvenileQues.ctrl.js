juvenileQuesModule.controller('juvenileQuesCtrl',['$state','JVQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,JVQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var jvc = this;
    this.genOpClick = false;
    this.jvQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;

    rqInfoService.setActiveTab(LoadData.url);

    Object.assign(JVQuesService.LoadData,this.LoadData);
    Object.assign(JVQuesService.ExistingAppData,ExistingAppData);


    this.onGenerateOutput = function(jvQuesForm){
        this.genOpClick = true;
        if(jvQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("jvQuesData : " + JSON.stringify(this.jvQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(JVQuesService.jvQuesData,jvc.jvQuesData)
                    if(!!fileData)
                        JVQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.juvenileQues", "REPORT_PARAMS": jvc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Juvenile_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.juvenileQues", "REPORT_PARAMS": jvc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };

    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };

    this.setNull = function(reasonNo){
        debug("reasonNo : " + reasonNo);
        switch(reasonNo){
            case 'A'  :
                this.jvQuesData.ANS_A1 = null;
                this.jvQuesData.ANS_A2 = null;
                break;
            case 'B3AF':
                this.jvQuesData.ANS_B3a1 = null;
                this.jvQuesData.ANS_B3b1 = null;
                this.jvQuesData.ANS_B3c1 = null;
                break;
            case 'B3AM':
                this.jvQuesData.ANS_B3a2 = null;
                this.jvQuesData.ANS_B3b2 = null;
                this.jvQuesData.ANS_B3c2 = null;
                break;
            case 'C1' :
                this.jvQuesData.ANS_C1_Reason = null;
                this.jvQuesData.ANS_C1_Reason1a = null;
                this.jvQuesData.ANS_C1_Reason1b = null;
                this.jvQuesData.ANS_C1_Reason1c = null;
                this.jvQuesData.ANS_C1_Reason2a = null;
                this.jvQuesData.ANS_C1_Reason2b = null;
                this.jvQuesData.ANS_C1_Reason2c = null;
                this.jvQuesData.ANS_C1_Reason3a = null;
                this.jvQuesData.ANS_C1_Reason3b = null;
                this.jvQuesData.ANS_C1_Reason3c = null;
                this.jvQuesData.ANS_C1_Reason4a = null;
                this.jvQuesData.ANS_C1_Reason4b = null;
                this.jvQuesData.ANS_C1_Reason4c = null;
                break;
            case 'C1R' :
                this.jvQuesData.ANS_C1_Reason1a = null;
                this.jvQuesData.ANS_C1_Reason1b = null;
                this.jvQuesData.ANS_C1_Reason1c = null;
                this.jvQuesData.ANS_C1_Reason2a = null;
                this.jvQuesData.ANS_C1_Reason2b = null;
                this.jvQuesData.ANS_C1_Reason2c = null;
                this.jvQuesData.ANS_C1_Reason3a = null;
                this.jvQuesData.ANS_C1_Reason3b = null;
                this.jvQuesData.ANS_C1_Reason3c = null;
                this.jvQuesData.ANS_C1_Reason4a = null;
                this.jvQuesData.ANS_C1_Reason4b = null;
                this.jvQuesData.ANS_C1_Reason4c = null;
                break;
        }
    };
	CommonService.hideLoading();
}]);

juvenileQuesModule.service('JVQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var jvs = this;
    this.HtmlFormOutput = "";
    this.jvQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.jvQuesData : " + JSON.stringify(this.jvQuesData));
        this.HtmlFormOutput = fileData;
        var insName = jvs.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + jvs.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
		var proName = jvs.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + jvs.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
		if(!!jvs.ExistingAppData.applicationPersonalInfo.Proposer)
        	proName = jvs.ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME + " " + jvs.ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",jvs.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||",proName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA1||",this.jvQuesData.ANS_A1 ? this.jvQuesData.ANS_A1 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESA2||",this.jvQuesData.ANS_A2 ? this.jvQuesData.ANS_A2 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1A||",this.jvQuesData.ANS_B1a ? this.jvQuesData.ANS_B1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB1B||",this.jvQuesData.ANS_B1b ? this.jvQuesData.ANS_B1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2A||",this.jvQuesData.ANS_B2a ? this.jvQuesData.ANS_B2a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB2B||",this.jvQuesData.ANS_B2b ? this.jvQuesData.ANS_B2b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3A1||",this.jvQuesData.ANS_B3a1 ? this.jvQuesData.ANS_B3a1 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3A2||",this.jvQuesData.ANS_B3a2 ? this.jvQuesData.ANS_B3a2 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3B1||",this.jvQuesData.ANS_B3b1 ? this.jvQuesData.ANS_B3b1 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3B2||",this.jvQuesData.ANS_B3b2 ? this.jvQuesData.ANS_B3b2 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3C1||",this.jvQuesData.ANS_B3c1 ? this.jvQuesData.ANS_B3c1 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESB3C2||",this.jvQuesData.ANS_B3c2 ? this.jvQuesData.ANS_B3c2 : "NA");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1||",this.jvQuesData.ANS_C1 ? (this.jvQuesData.ANS_C1=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON||",this.jvQuesData.ANS_C1_Reason ? this.jvQuesData.ANS_C1_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON1A||",this.jvQuesData.ANS_C1_Reason1a ? this.jvQuesData.ANS_C1_Reason1a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON2A||",this.jvQuesData.ANS_C1_Reason2a ? this.jvQuesData.ANS_C1_Reason2a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON3A||",this.jvQuesData.ANS_C1_Reason3a ? this.jvQuesData.ANS_C1_Reason3a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON4A||",this.jvQuesData.ANS_C1_Reason4a ? this.jvQuesData.ANS_C1_Reason4a : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON1B||",this.jvQuesData.ANS_C1_Reason1b ? this.jvQuesData.ANS_C1_Reason1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON2B||",this.jvQuesData.ANS_C1_Reason2b ? this.jvQuesData.ANS_C1_Reason2b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON3B||",this.jvQuesData.ANS_C1_Reason3b ? this.jvQuesData.ANS_C1_Reason3b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON4B||",this.jvQuesData.ANS_C1_Reason4b ? this.jvQuesData.ANS_C1_Reason4b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON1C||",this.jvQuesData.ANS_C1_Reason1c ? this.jvQuesData.ANS_C1_Reason1c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON2C||",this.jvQuesData.ANS_C1_Reason2c ? this.jvQuesData.ANS_C1_Reason2c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON3C||",this.jvQuesData.ANS_C1_Reason3c ? this.jvQuesData.ANS_C1_Reason3c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUESC1REASON4C||",this.jvQuesData.ANS_C1_Reason4c ? this.jvQuesData.ANS_C1_Reason4c : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
