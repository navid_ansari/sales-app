asthmaQuesModule.controller('asthmaQuesCtrl',['$state','AstmaQuesService', function($state,AstmaQuesService){
    var astmc = this;
    this.asthmaQuesData = {};

    this.noOfMedicineList = [
        {"MED_DISPLAY": "Select an Option", "MED_VALUE": null},
        {"MED_DISPLAY": "1","MED_VALUE": "1"},
        {"MED_DISPLAY": "2","MED_VALUE": "2"},
        {"MED_DISPLAY": "3","MED_VALUE": "3"},
        {"MED_DISPLAY": "4","MED_VALUE": "4"},
        {"MED_DISPLAY": "5","MED_VALUE": "5"},
        {"MED_DISPLAY": ">5","MED_VALUE": ">5"}
    ];

    this.noOfHospiAdmitList = [
        {"ADMIT_DISPLAY": "Select an Option", "ADMIT_VALUE": null},
        {"ADMIT_DISPLAY": "1","ADMIT_VALUE": "1"},
        {"ADMIT_DISPLAY": "2","ADMIT_VALUE": "2"},
        {"ADMIT_DISPLAY": "3","ADMIT_VALUE": "3"},
        {"ADMIT_DISPLAY": "4","ADMIT_VALUE": "4"},
        {"ADMIT_DISPLAY": "5","ADMIT_VALUE": "5"},
        {"ADMIT_DISPLAY": ">5","ADMIT_VALUE": ">5"}
    ];

    this.noOfMedOptionSelected = this.noOfMedicineList[0];
    this.noOfHospiAdmitOptionSelected = this.noOfHospiAdmitList[0];

    this.onNoOfMedChg = function(){
        debug("this.noOfMedOptionSelected: " + JSON.stringify(this.noOfMedOptionSelected));
        this.asthmaQuesData.ANS_5_Reason = this.noOfMedOptionSelected.MED_VALUE;
    };
    this.onNoOfHospiAdmitChg = function(){
        debug("this.noOfHospiAdmitOptionSelected: " + JSON.stringify(this.noOfHospiAdmitOptionSelected));
        this.asthmaQuesData.ANS_6_Reason = this.noOfHospiAdmitOptionSelected.ADMIT_VALUE;
    };

    this.onGenerateOutput = function(){
        console.log("asthmaQuesData : " + JSON.stringify(this.asthmaQuesData));

        cordova.exec(
            function(fileData){
                console.log("fileData: " + fileData);
                Object.assign(AstmaQuesService.astmqData,astmc.asthmaQuesData)
                if(!!fileData)
                    AstmaQuesService.generateOutput(fileData);
                else
                    navigator.notification.alert("Template file not found",null,"Application","OK");
            },
            function(errMsg){
                debug(errMsg);
            },
            "PluginHandler","getDataFromFileInAssets",["ToClient/ReflQuesTemplates/Asthma.html"]
        );
    };
    this.setNull = function(reasonNo){
        console.log("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '4' :
                this.asthmaQuesData.ANS_4_Reason = null;
                break;
            case '5' :
                this.noOfMedOptionSelected = this.noOfMedicineList[0];
                break;
            case '6' :
                this.noOfHospiAdmitOptionSelected = this.noOfHospiAdmitList[0];
                break;
            case '8' :
                this.asthmaQuesData.ANS_8_Reason = null;
                break;
            case '10' :
                this.asthmaQuesData.ANS_10_Reason = null;
                break;
        }
        //console.log("asthmaQuesData : " + JSON.stringify(this.asthmaQuesData));
    };
}]);

asthmaQuesModule.service('AstmaQuesService',['$q','$filter','CommonService',function($q, $filter, CommonService){
    var astms = this;
    this.HtmlFormOutput = "";
    this.astmqData = {};
    this.generateOutput = function(fileData){
        debug("this.astmqData : " + JSON.stringify(this.astmqData));
        this.HtmlFormOutput = fileData;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||","");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1||",this.astmqData.ANS_1 ? ($filter('date')(this.astmqData.ANS_1, "dd-MM-yyyy")) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2||",this.astmqData.ANS_2 ? this.astmqData.ANS_2 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3||",this.astmqData.ANS_3 ? ($filter('date')(this.astmqData.ANS_3, "dd-MM-yyyy")) : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4||",this.astmqData.ANS_4 ? (this.astmqData.ANS_4=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4REASON||",this.astmqData.ANS_4_Reason ? this.astmqData.ANS_4_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5A||",this.astmqData.ANS_5a ? (this.astmqData.ANS_5a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5B||",this.astmqData.ANS_5b ? (this.astmqData.ANS_5b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5C||",this.astmqData.ANS_5c ? (this.astmqData.ANS_5c=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5D||",this.astmqData.ANS_5d ? (this.astmqData.ANS_5d=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5E||",this.astmqData.ANS_5e ? (this.astmqData.ANS_5e=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5F||",this.astmqData.ANS_5f ? (this.astmqData.ANS_5f=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5REASON||",this.astmqData.ANS_5_Reason ? this.astmqData.ANS_5_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",this.astmqData.ANS_6 ? (this.astmqData.ANS_6=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6REASON||",this.astmqData.ANS_6_Reason ? this.astmqData.ANS_6_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES7||",this.astmqData.ANS_7 ? this.astmqData.ANS_7 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8||",this.astmqData.ANS_8 ? (this.astmqData.ANS_8=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES8REASON||",this.astmqData.ANS_8_Reason ? this.astmqData.ANS_8_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES9||",this.astmqData.ANS_9 ? this.astmqData.ANS_9 : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES10||",this.astmqData.ANS_10 ? (this.astmqData.ANS_10=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES10REASON||",this.astmqData.ANS_10_Reason ? this.astmqData.ANS_10_Reason : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput);
    }
}]);
