nriQuesModule.controller('nriQuesCtrl',['$state','NRIQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,NRIQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var nric = this;
    this.genOpClick = false;
    this.nriQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;
    rqInfoService.setActiveTab(LoadData.url);
    var accountTypeData = ['Current','Saving','NRO'];
    this.accountTypeData = accountTypeData;
    this.freeTxtRegex = FREE_TEXT_REGEX;
    Object.assign(NRIQuesService.LoadData,this.LoadData);
    Object.assign(NRIQuesService.ExistingAppData,ExistingAppData);


    this.onGenerateOutput = function(nriQuesForm){
        this.genOpClick = true;
        if(nriQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("nriQuesData : " + JSON.stringify(this.nriQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(NRIQuesService.nriQuesData,nric.nriQuesData)
                    if(!!fileData)
                        NRIQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.nriQues", "REPORT_PARAMS": nric.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/NRI_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };
    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.nriQues", "REPORT_PARAMS": nric.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };


    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };

    this.setNull = function(reasonNo){
        console.log("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '5' :
                this.nriQuesData.ANS_6a = null;
                this.nriQuesData.ANS_6b = null;
                break;
            case '9' :
                this.nriQuesData.ANS_9_Reason = null;
                break;
            case '12' :
                if(!!this.nriQuesData.ANS_12)
                    this.nriQuesData.ANS_12_Reason = null;
                break;
            case '15' :
                    this.nriQuesData.ANS_15_Reason = null;
                    break;
        }
    };
    CommonService.hideLoading();
}]);

nriQuesModule.service('NRIQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var nris = this;
    this.HtmlFormOutput = "";
    this.nriQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.nriQuesData : " + JSON.stringify(this.nriQuesData));
        this.HtmlFormOutput = fileData;
        var insProfCkeck = nris.LoadData.custType == 'Insured' ? true : false;
        if(insProfCkeck){
            var insName = nris.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + nris.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",nris.ExistingAppData.applicationMainData.POLICY_NO);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSDOB||",nris.ExistingAppData.applicationPersonalInfo.Insured.BIRTH_DATE.substring(0,10));
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNATIONALITY||",nris.ExistingAppData.applicationPersonalInfo.Insured.NATIONALITY);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4A||",this.nriQuesData.ANS_4a ? this.nriQuesData.ANS_4a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4B||",this.nriQuesData.ANS_4b ? $filter('date')(this.nriQuesData.ANS_4b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4C||",this.nriQuesData.ANS_4c ? this.nriQuesData.ANS_4c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES5||",this.nriQuesData.ANS_5 ? this.nriQuesData.ANS_5 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6A||",this.nriQuesData.ANS_6a ? this.nriQuesData.ANS_6a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6B||",this.nriQuesData.ANS_6b ? this.nriQuesData.ANS_6b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7||",this.nriQuesData.ANS_7 ? this.nriQuesData.ANS_7 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES8||",this.nriQuesData.ANS_8 ? this.nriQuesData.ANS_8 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES9||",this.nriQuesData.ANS_9 ? this.nriQuesData.ANS_9 : this.nriQuesData.ANS_9_Reason);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES10||",this.nriQuesData.ANS_10 ? $filter('date')(this.nriQuesData.ANS_10, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11||",this.nriQuesData.ANS_11 ? $filter('date')(this.nriQuesData.ANS_11, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12||",this.nriQuesData.ANS_12 ? this.nriQuesData.ANS_12 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12REASON||",this.nriQuesData.ANS_12_Reason ? this.nriQuesData.ANS_12_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13A||",this.nriQuesData.ANS_13a ? this.nriQuesData.ANS_13a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13B||",this.nriQuesData.ANS_13b ? this.nriQuesData.ANS_13b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13C||",this.nriQuesData.ANS_13c ? this.nriQuesData.ANS_13c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13D||",this.nriQuesData.ANS_13d ? this.nriQuesData.ANS_13d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13E||",this.nriQuesData.ANS_13e ? this.nriQuesData.ANS_13e : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13F||",this.nriQuesData.ANS_13f ? this.nriQuesData.ANS_13f : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14A||",this.nriQuesData.ANS_14a ? this.nriQuesData.ANS_14a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14B||",this.nriQuesData.ANS_14b ? this.nriQuesData.ANS_14b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14C||",this.nriQuesData.ANS_14c ? this.nriQuesData.ANS_14c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14D||",this.nriQuesData.ANS_14d ? this.nriQuesData.ANS_14d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15||",this.nriQuesData.ANS_15 ? (this.nriQuesData.ANS_15=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15REASON||",this.nriQuesData.ANS_15_Reason ? this.nriQuesData.ANS_15_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));


            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPDOB||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNATIONALITY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES5||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES8||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES9||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13D||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13E||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13F||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14D||","");
        }else{
            var proName = nris.ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME + " " + nris.ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",nris.ExistingAppData.applicationMainData.POLICY_NO);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||",proName);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPDOB||",nris.ExistingAppData.applicationPersonalInfo.Proposer.BIRTH_DATE.substring(0,10));
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNATIONALITY||",nris.ExistingAppData.applicationPersonalInfo.Proposer.NATIONALITY);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4A||",this.nriQuesData.ANS_4a ? this.nriQuesData.ANS_4a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4B||",this.nriQuesData.ANS_4b ? $filter('date')(this.nriQuesData.ANS_4b, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4C||",this.nriQuesData.ANS_4c ? this.nriQuesData.ANS_4c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES5||",this.nriQuesData.ANS_5 ? this.nriQuesData.ANS_5 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6A||",this.nriQuesData.ANS_6a ? this.nriQuesData.ANS_6a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES6B||",this.nriQuesData.ANS_6b ? this.nriQuesData.ANS_6b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES7||",this.nriQuesData.ANS_7 ? this.nriQuesData.ANS_7 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES8||",this.nriQuesData.ANS_8 ? this.nriQuesData.ANS_8 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES9||",this.nriQuesData.ANS_9 ? this.nriQuesData.ANS_9 : this.nriQuesData.ANS_9_Reason);
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES10||",this.nriQuesData.ANS_10 ? $filter('date')(this.nriQuesData.ANS_10, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES11||",this.nriQuesData.ANS_11 ? $filter('date')(this.nriQuesData.ANS_11, "dd-MM-yyyy") : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12||",this.nriQuesData.ANS_12 ? this.nriQuesData.ANS_12 : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES12REASON||",this.nriQuesData.ANS_12_Reason ? this.nriQuesData.ANS_12_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13A||",this.nriQuesData.ANS_13a ? this.nriQuesData.ANS_13a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13B||",this.nriQuesData.ANS_13b ? this.nriQuesData.ANS_13b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13C||",this.nriQuesData.ANS_13c ? this.nriQuesData.ANS_13c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13D||",this.nriQuesData.ANS_13d ? this.nriQuesData.ANS_13d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13E||",this.nriQuesData.ANS_13e ? this.nriQuesData.ANS_13e : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES13F||",this.nriQuesData.ANS_13f ? this.nriQuesData.ANS_13f : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14A||",this.nriQuesData.ANS_14a ? this.nriQuesData.ANS_14a : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14B||",this.nriQuesData.ANS_14b ? this.nriQuesData.ANS_14b : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14C||",this.nriQuesData.ANS_14c ? this.nriQuesData.ANS_14c : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES14D||",this.nriQuesData.ANS_14d ? this.nriQuesData.ANS_14d : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15||",this.nriQuesData.ANS_15 ? (this.nriQuesData.ANS_15=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES15REASON||",this.nriQuesData.ANS_15_Reason ? this.nriQuesData.ANS_15_Reason : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));


            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSDOB||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNATIONALITY||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES5||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES6B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES7||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES8||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES9||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES10||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES11||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13D||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13E||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES13F||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14A||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14B||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14C||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES14D||","");
        }


        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
