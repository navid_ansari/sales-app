hypertensionQuesModule.controller('hypertensionQuesCtrl',['$state','HTQuesService','LoadData','ReflectiveQuesService','ViewReportService','$stateParams','CommonService','ExistingAppData','rqInfoService','TermData', function($state,HTQuesService,LoadData,ReflectiveQuesService,ViewReportService,$stateParams,CommonService,ExistingAppData,rqInfoService,TermData){
    var htc = this;
    this.genOpClick = false;
    this.htQuesData = {};
    this.LoadData = LoadData;
    this.isDone = LoadData.isDone;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;
    rqInfoService.setActiveTab(LoadData.url);


    Object.assign(HTQuesService.LoadData,this.LoadData);
    Object.assign(HTQuesService.ExistingAppData,ExistingAppData);

    this.onGenerateOutput = function(htQuesForm){
        this.genOpClick = true;
        if(htQuesForm.$invalid == false){
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            debug("htQuesData : " + JSON.stringify(this.htQuesData));
            cordova.exec(
                function(fileData){
                    debug("fileData: " + fileData.length);
                    Object.assign(HTQuesService.htQuesData,htc.htQuesData)
                    if(!!fileData)
                        HTQuesService.generateOutput(fileData).then(
                            function(HtmlFormOutput){
                              if(!!HtmlFormOutput){
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " & ", " &amp; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " < ", " &lt; ");
                                HtmlFormOutput = CommonService.replaceAll(HtmlFormOutput, " > ", " &gt; ");
                             }
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.hypertensionQues", "REPORT_PARAMS": htc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",function(){CommonService.hideLoading();},"Application","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Hypertension_Questionnaire.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',function(){CommonService.hideLoading();},"Application","OK");
        }
    };

    this.reGenerate = function(){
        this.isDone = false;
    };
    this.showPreview = function(){
        var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
        ViewReportService.fetchSavedReport("APP",fileName).then(
            function(res){
                $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "applicationForm.reflectiveQuestionaries.hypertensionQues", "REPORT_PARAMS": htc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
            }
        );
    };

    this.onNext = function(){
        ReflectiveQuesService.onNext(this.LoadData);
    };

    this.setNull = function(reasonNo){
        debug("reasonNo : " + reasonNo);
        switch(reasonNo){
            case '1e' :
                this.htQuesData.ANS_4b_Reason = null;
                break;
            case '2a' :
                this.htQuesData.ANS_2b = null;
                break;
            case '3a' :
                this.htQuesData.ANS_3a_Reason = null;
                break;
            case '3b' :
                this.htQuesData.ANS_3b_Reason = null;
                break;
            case '3c' :
                this.htQuesData.ANS_3c_Reason = null;
                break;
            case '3di' :
                this.htQuesData.ANS_3di_Reason = null;
                this.htQuesData.ANS_3dii_Reason = null;
                break;
            case '5' :
                this.htQuesData.ANS_5_Reason = null;
                break;
            case '6' :
                this.htQuesData.ANS_6_Reason = null;
                break;
        }
    };
    CommonService.hideLoading();
}]);

hypertensionQuesModule.service('HTQuesService',['$q','$state','$filter','CommonService',function($q,$state, $filter, CommonService){
    var hts = this;
    this.HtmlFormOutput = "";
    this.htQuesData = {};
    this.LoadData = {};
    this.ExistingAppData = {};
    this.generateOutput = function(fileData){
        var dfd = $q.defer();
        debug("this.htQuesData : " + JSON.stringify(this.htQuesData));
        this.HtmlFormOutput = fileData;
        var insName = hts.ExistingAppData.applicationPersonalInfo.Insured.FIRST_NAME + " " + hts.ExistingAppData.applicationPersonalInfo.Insured.LAST_NAME;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",insName);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",hts.ExistingAppData.applicationMainData.POLICY_NO);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1A||",this.htQuesData.ANS_1a ? $filter('date')(this.htQuesData.ANS_1a, "dd-MM-yyyy") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1B||",this.htQuesData.ANS_1b ? this.htQuesData.ANS_1b : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1C||",this.htQuesData.ANS_1c ? this.htQuesData.ANS_1c : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1D||",this.htQuesData.ANS_1d ? this.htQuesData.ANS_1d : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1E||",this.htQuesData.ANS_1e ? (this.htQuesData.ANS_1e=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES1EREASON||",this.htQuesData.ANS_1e_Reason ? this.htQuesData.ANS_1e_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2A||",this.htQuesData.ANS_2a ? (this.htQuesData.ANS_2a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2B||",this.htQuesData.ANS_2b ? this.htQuesData.ANS_2b : "");
        //this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2A||",this.htQuesData.ANS_2a ? (this.htQuesData.ANS_2a=='Y' ? 'Yes' : 'No') : "");
        //this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES2B||",this.htQuesData.ANS_2b ? this.htQuesData.ANS_2b : "");


        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3A||",this.htQuesData.ANS_3a ? (this.htQuesData.ANS_3a=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3AReason||",this.htQuesData.ANS_3a_Reason ? $filter('date')(this.htQuesData.ANS_3a_Reason, "dd-MM-yyyy") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3B||",this.htQuesData.ANS_3b ? (this.htQuesData.ANS_3b=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3BReason||",this.htQuesData.ANS_3b_Reason ? this.htQuesData.ANS_3b_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3C||",this.htQuesData.ANS_3c ? (this.htQuesData.ANS_3c=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3CReason||",this.htQuesData.ANS_3c_Reason ? this.htQuesData.ANS_3c_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3DI||",this.htQuesData.ANS_3di ? (this.htQuesData.ANS_3di=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3DIReason||",this.htQuesData.ANS_3di_Reason ? this.htQuesData.ANS_3di_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES3DIIReason||",this.htQuesData.ANS_3dii_Reason ? this.htQuesData.ANS_3dii_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4AI||",this.htQuesData.ANS_4ai ? this.htQuesData.ANS_4ai : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4AII||",this.htQuesData.ANS_4aii ? $filter('date')(this.htQuesData.ANS_4aii, "dd-MM-yyyy") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4BI||",this.htQuesData.ANS_4bi ? this.htQuesData.ANS_4bi : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4BII||",this.htQuesData.ANS_4bii ? $filter('date')(this.htQuesData.ANS_4bii, "dd-MM-yyyy") : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES4BIII||",this.htQuesData.ANS_4biii ? (this.htQuesData.ANS_4biii=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5||",this.htQuesData.ANS_5 ? (this.htQuesData.ANS_5=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES5Reason||",this.htQuesData.ANS_5_Reason ? this.htQuesData.ANS_5_Reason : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6||",this.htQuesData.ANS_6 ? (this.htQuesData.ANS_6=='Y' ? 'Yes' : 'No') : "");
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||QUES6Reason||",this.htQuesData.ANS_6_Reason ? this.htQuesData.ANS_6_Reason : "");

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);

        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

}]);
