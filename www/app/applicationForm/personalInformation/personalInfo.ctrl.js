personalInfoModule.controller('PersonalInfoCtrl',['$stateParams','CommonService','persInfoService','AppTimeService', function($stateParams,CommonService,persInfoService,AppTimeService){

var personal = this;

//timeline changes..
CommonService.hideLoading();

persInfoService.setActiveTab("personalDetails");
//first time set active first tab
AppTimeService.setActiveTab("lifeStage");

console.log("$stateParams PERSON DETAILS CONTROLLER:"+JSON.stringify($stateParams));

}]);


personalInfoModule.service('PersonalInfoService',['$q', '$state', 'CommonService','LoginService', 'ApplicationFormDataService','$stateParams','LoadApplicationScreenData','persInfoService', function($q, $state, CommonService, LoginService, ApplicationFormDataService,$stateParams,LoadApplicationScreenData,persInfoService){


this.gotoPage = function(pageName){
	if(persInfoService.getActiveTab(pageName)!==pageName){
		CommonService.showLoading();
	}
    debug("INSIDE PERSONAL PAGE "+pageName);
    persInfoService.setActiveTab(pageName);

    if(!!ApplicationFormDataService && !!ApplicationFormDataService.applicationFormBean.APPLICATION_ID && !$stateParams.APPLICATION_ID)
    {
        debug("inside setting persnl APPLICATION_ID")
        $stateParams.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    }
    debug("<<<< PERSONAL INFO DETAILS SERVICES STATEPARAMS >>>"+JSON.stringify($stateParams));

    LoadApplicationScreenData.loadApplicationFlags($stateParams.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(function(ApplicationFlagsList){

    var params = $stateParams;
    var FlagList = ApplicationFlagsList.applicationFlags;

    console.log(JSON.stringify(FlagList)+"gotoPage : subpage ::"+JSON.stringify(params));

    switch(pageName){
        case 'personalDetails':
            $state.go('applicationForm.personalInfo.personalDetails',{"LEAD_ID":params.LEAD_ID, "OPP_ID":params.OPP_ID,"APPLICATION_ID":params.APPLICATION_ID, "AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "SIS_ID":params.SIS_ID, "FHR_ID":params.FHR_ID, "POLICY_NO":params.POLICY_NO});
        break;

        case 'contactDetails':
            if(!!FlagList && (FlagList.IS_PERSONAL_DET=='Y' || FlagList.IS_PERSONAL_DET == 'NA')){
                $state.go('applicationForm.personalInfo.contactDetails');
            }
            else
                navigator.notification.alert("Please complete Personal details",function(){CommonService.hideLoading();},"Application","OK");
        break;

        case 'addressDetails':
            if(!!FlagList && (FlagList.IS_CONTACT_DET=='Y' || FlagList.IS_CONTACT_DET == 'NA')){
                $state.go('applicationForm.personalInfo.addressDetails');
            }
            else
                navigator.notification.alert("Please complete Contact details",function(){CommonService.hideLoading();},"Application","OK");
        break;

        case 'eduQualDetails':
            if(!!FlagList && (FlagList.IS_ADDR_DET=='Y' || FlagList.IS_ADDR_DET == 'NA')){
             $state.go('applicationForm.personalInfo.eduQualDetails');
            }
            else
                navigator.notification.alert("Please complete Address details",function(){CommonService.hideLoading();},"Application","OK");
        break;

        case 'otherDetails':
             if(!!FlagList && (FlagList.IS_EDUQUAL_DET=='Y' || FlagList.IS_EDUQUAL_DET == 'NA')){
                $state.go('applicationForm.personalInfo.otherDetails');
             }
             else
                navigator.notification.alert("Please complete Education details",function(){CommonService.hideLoading();},"Application","OK");
        break;

        default:
             $state.go('applicationForm.personalInfo.personalDetails',{"LEAD_ID":params.LEAD_ID, "OPP_ID":params.OPP_ID,"APPLICATION_ID":params.APPLICATION_ID, "AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "SIS_ID":params.SIS_ID, "FHR_ID":params.FHR_ID, "POLICY_NO":params.POLICY_NO});

        }
    });
};

this.insertApplicationMainData = function(TableName, appMainData, flag){
var dfd = $q.defer();
console.log("appMainData :"+JSON.stringify(appMainData));
CommonService.insertOrReplaceRecord(db, TableName, appMainData, flag).then(
				                function(res){
									console.log(TableName+" success !!");
									dfd.resolve(res);

				                }
				            );
	return dfd.promise;
}

this.getAppMainData = function(applicationFormBean, sisFormData){
var dfd = $q.defer();
var appMainData = {};
var personalDetailsData = applicationFormBean.personalDetBean;

console.log("applicationFormBean :"+JSON.stringify(applicationFormBean));
 appMainData.APPLICATION_ID = applicationFormBean.APPLICATION_ID;
 appMainData.POLICY_NO = (!!applicationFormBean.POLICY_NO) ? applicationFormBean.POLICY_NO : null
 appMainData.COMBO_ID = ((!!ApplicationFormDataService.ComboFormBean && !!ApplicationFormDataService.ComboFormBean.COMBO_ID)?ApplicationFormDataService.ComboFormBean.COMBO_ID:null);
 appMainData.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
 appMainData.SIS_ID = sisFormData.sisMainData.SIS_ID;
 appMainData.PLAN_CODE = sisFormData.sisFormBData.PLAN_CODE;
 appMainData.PLAN_NAME = sisFormData.sisFormBData.PLAN_NAME;
 appMainData.POLICY_TERM = sisFormData.sisFormBData.POLICY_TERM;
 appMainData.PREMIUM_PAY_TERM = sisFormData.sisFormBData.PREMIUM_PAY_TERM;
 appMainData.PREMIUM_PAY_MODE = sisFormData.sisFormBData.PREMIUM_PAY_MODE;
 appMainData.PREMIUM_PAY_MODE_DESC = sisFormData.sisFormBData.PREMIUM_PAY_MODE_DESC;
 appMainData.ANNUAL_PREMIUM_AMOUNT = sisFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
 appMainData.MODEL_PREMIUM_AMOUNT = sisFormData.sisMainData.MODAL_PREMIUM;
 appMainData.SERVICE_TAX = sisFormData.sisMainData.SERVICE_TAX;
 appMainData.SUM_ASSURED = sisFormData.sisFormBData.SUM_ASSURED;
 appMainData.SUB_OFFC_CODE = LoginService.lgnSrvObj.userinfo.SUBOFFICECODE;
 appMainData.OFFICE_CODE = LoginService.lgnSrvObj.userinfo.OFFICE_CODE;
 appMainData.CHANNEL_CODE = LoginService.lgnSrvObj.userinfo.CHANNEL_NAME;
 if(!!applicationFormBean && !!applicationFormBean.RECO_PRODUCT_DEVIATION_FLAG)
 	appMainData.RECO_PRODUCT_DEVIATION_FLAG = applicationFormBean.RECO_PRODUCT_DEVIATION_FLAG;
 else
 	appMainData.RECO_PRODUCT_DEVIATION_FLAG = 'S';

	appMainData.DISCOUNTED_MODAL_PREMIUM = sisFormData.sisMainData.DISCOUNTED_MODAL_PREMIUM;
	appMainData.DISCOUNTED_SERVICE_TAX = sisFormData.sisMainData.DISCOUNTED_SERVICE_TAX;


 if(personalDetailsData.EIA_RECEIVE_FLAG!=undefined){
 appMainData.EAI_RECEIVE_FLAG = (personalDetailsData.EIA_RECEIVE_FLAG ? 'Y':'N');
    if(personalDetailsData.EIA_RECEIVE_FLAG!=undefined && personalDetailsData.EIA_RECEIVE_FLAG)
        appMainData.EAI_NO = personalDetailsData.EIA_NO;
        }
 if(personalDetailsData.EIA_NO!=undefined)
 	appMainData.EAI_NO = personalDetailsData.EIA_NO;

 	dfd.resolve(appMainData);
 	return dfd.promise;

}

this.getPersonalDetailsData = function(/*personalDetailsData*/applicationFormBean, sisFormData){
		console.log(JSON.stringify(sisFormData)+"in getPersonalDetailsData"+JSON.stringify(applicationFormBean.DOCS_LIST));
		var dfd = $q.defer();
		var personalDetailsData = applicationFormBean.personalDetBean;
		var personalData = {
            Insured :{},
            Proposer :{}
		};
		console.log("isSameAsProposer :"+sisFormData.sisFormAData.PROPOSER_IS_INSURED);
					personalData.Insured.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
					personalData.Proposer.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
					personalData.Insured.APPLICATION_ID = applicationFormBean.APPLICATION_ID;
        			personalData.Proposer.APPLICATION_ID = applicationFormBean.APPLICATION_ID;
					personalData.Insured.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
					personalData.Proposer.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
					personalData.Insured.CUST_TYPE = 'C01';
					personalData.Proposer.CUST_TYPE = 'C02';
		            personalData.Insured.TITLE = sisFormData.sisFormAData.INSURED_TITLE || null;
        			personalData.Insured.FIRST_NAME = sisFormData.sisFormAData.INSURED_FIRST_NAME || null;
        			personalData.Insured.MIDDLE_NAME = sisFormData.sisFormAData.INSURED_MIDDLE || null;
        			personalData.Insured.LAST_NAME = sisFormData.sisFormAData.INSURED_LAST_NAME || null;
        			if(!!sisFormData.sisFormAData.INSURED_GENDER)
        			{	if(sisFormData.sisFormAData.INSURED_GENDER == "M")
        					personalData.Insured.GENDER = "Male";
        				else if(sisFormData.sisFormAData.INSURED_GENDER == "F")
        					personalData.Insured.GENDER = "Female";
        				else
        					personalData.Insured.GENDER = null;
        			}
        			else
        				personalData.Insured.GENDER = null;

        			personalData.Insured.GENDER_CODE = sisFormData.sisFormAData.INSURED_GENDER || null;
        			personalData.Insured.BIRTH_DATE = sisFormData.sisFormAData.INSURED_DOB || null;
                    personalData.Insured.AADHAR_CARD_EXISTS = personalDetailsData.INSURED_EKYC_OPT || null;
                    if(personalDetailsData.INSURED_EKYC_OPT == 'Y')
                        personalData.Insured.AADHAR_CARD_NO = personalDetailsData.INSURED_UAN_NO || null;
                    personalData.Insured.PAN_CARD_EXISTS = (personalDetailsData.INSURED_PAN_CARD_EXISTS ? 'N':'Y');
                    if(!personalDetailsData.INSURED_PAN_CARD_EXISTS)
                        personalData.Insured.PAN_CARD_NO = personalDetailsData.INSURED_PAN_CARD_NO;
                    personalData.Insured.IDENTIFICATION_MARK = personalDetailsData.INSURED_IDENTIFICATION_MARK;
                    personalData.Insured.DOMINANT_HAND = personalDetailsData.INSURED_DOMINANT_HAND || null;
                    personalData.Insured.MARTIAL_STATUS = personalDetailsData.INSURED_MARITAL_STATUS.MARITAL_STATUS || null;
                    personalData.Insured.MARTIAL_STATUS_CODE = personalDetailsData.INSURED_MARITAL_STATUS.MARITAL_CD || null;
                    if(!!personalDetailsData.INSURED_AGE_PROOF.DOC_ID)
                    	personalData.Insured.AGE_PROOF = personalDetailsData.INSURED_AGE_PROOF.MPPROOF_DESC || null;
                    personalData.Insured.AGE_PROOF_DOC_ID = personalDetailsData.INSURED_AGE_PROOF.DOC_ID || null;
                    /*if(personalDetailsData.INSURED_AGE_PROOF.MPPROOF_DESC == "Others")
                   	 	personalData.Insured.AGE_PROOF_OTHER = personalDetailsData.INSURED_AGE_PROOF_OTHER || null;*/

                   	//Added by Amruta
                   	personalData.Insured.AGE_PROOF_DOB = CommonService.formatDobToDb(personalDetailsData.INSURED_AGE_PROOF_DOB) || null;
                   	console.log("INSURED_AGE_PROOF.MPPROOF_CODE::"+personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE);
                   	if(personalDetailsData.INSURED_AGE_PROOF.MPPROOF_CODE == 'DL')
                   		personalData.Insured.DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.INSURED_ISSUANCE_DATE) || null;
                   	if(personalDetailsData.INSURED_AGE_PROOF.MPPROOF_CODE == 'PP' || personalDetailsData.INSURED_AGE_PROOF.MPPROOF_CODE == 'DL')
                   		personalData.Insured.DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.INSURED_EXPIRY_DATE) || null;
					if(personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
						personalData.Insured.ID_DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.INSURED_IDENTITY_ISSUANCE_DATE) || null;
					if(personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PP' || personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
	                	personalData.Insured.ID_DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.INSURED_IDENTITY_EXPIRY_DATE) || null;
					if(personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
						personalData.Insured.DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.INSURED_IDENTITY_ISSUANCE_DATE) || null;
					if(personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PP' || personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
                    	personalData.Insured.DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.INSURED_IDENTITY_EXPIRY_DATE) || null;

                   	if(!!personalDetailsData.INSURED_IDENTITY_PROOF.DOC_ID)
                   		personalData.Insured.IDENTITY_PROOF = personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_DESC || null;
                   	personalData.Insured.IDENTITY_PROOF_DOC_ID = personalDetailsData.INSURED_IDENTITY_PROOF.DOC_ID || null;
                   	personalData.Insured.IDENTITY_PROOF_NO = personalDetailsData.INSURED_IDENTITY_PROOF_NO || null;
                   	if(personalDetailsData.INSURED_IDENTITY_PROOF.MPPROOF_DESC == "Others")
                        personalData.Insured.IDENTITY_PROOF_OTHER = personalDetailsData.INSURED_IDENTITY_PROOF_OTHER || null;

                    /*personalData.Proposer.TITLE = sisFormData.sisFormAData.INSURED_TITLE || null;
                    personalData.Proposer.FIRST_NAME = sisFormData.sisFormAData.INSURED_FIRST_NAME || null;
                    personalData.Proposer.MIDDLE_NAME = sisFormData.sisFormAData.INSURED_MIDDLE || null;
                    personalData.Proposer.LAST_NAME = sisFormData.sisFormAData.INSURED_LAST_NAME || null;
                    personalData.Proposer.GENDER = sisFormData.sisFormAData.INSURED_GENDER || null;
                    personalData.Proposer.INSURED_DOB = sisFormData.sisFormAData.INSURED_DOB || null;*/
					if(!!sisFormData.sisFormAData.COMPANY_CODE)
						personalData.Insured.COMPANY_CODE = sisFormData.sisFormAData.COMPANY_CODE || null;
                    if(sisFormData.sisFormAData.PROPOSER_IS_INSURED!="Y"){
					 personalData.Proposer.TITLE = sisFormData.sisFormAData.PROPOSER_TITLE || null;
					 personalData.Proposer.FIRST_NAME = sisFormData.sisFormAData.PROPOSER_FIRST_NAME || null;
					 personalData.Proposer.MIDDLE_NAME = sisFormData.sisFormAData.PROPOSER_MIDDLE_NAME || null;
					 personalData.Proposer.LAST_NAME = sisFormData.sisFormAData.PROPOSER_LAST_NAME || null;
					 	if(!!sisFormData.sisFormAData.PROPOSER_GENDER)
						{	if(sisFormData.sisFormAData.PROPOSER_GENDER == "M")
								personalData.Proposer.GENDER = "Male";
							else if(sisFormData.sisFormAData.PROPOSER_GENDER == "F")
								personalData.Proposer.GENDER = "Female";
							else
								personalData.Proposer.GENDER = null;
						}
						else
							personalData.Proposer.GENDER = null;

					 personalData.Proposer.GENDER_CODE = sisFormData.sisFormAData.PROPOSER_GENDER || null;
					 personalData.Proposer.BIRTH_DATE = sisFormData.sisFormAData.PROPOSER_DOB || null;

                    personalData.Proposer.AADHAR_CARD_EXISTS = personalDetailsData.PROPOSER_EKYC_OPT || null;
					if(personalDetailsData.PROPOSER_EKYC_OPT == 'Y')
						personalData.Proposer.AADHAR_CARD_NO = personalDetailsData.PROPOSER_UAN_NO || null;
					personalData.Proposer.PAN_CARD_EXISTS = (personalDetailsData.PROPOSER_PAN_CARD_EXISTS ? 'N':'Y');
					if(!personalDetailsData.PROPOSER_PAN_CARD_EXISTS)
						personalData.Proposer.PAN_CARD_NO = personalDetailsData.PROPOSER_PAN_CARD_NO;
					personalData.Proposer.IDENTIFICATION_MARK = personalDetailsData.PROPOSER_IDENTIFICATION_MARK;
					personalData.Proposer.DOMINANT_HAND = personalDetailsData.PROPOSER_DOMINANT_HAND || null;
					personalData.Proposer.MARTIAL_STATUS = personalDetailsData.PROPOSER_MARITAL_STATUS.MARITAL_STATUS || null;
					personalData.Proposer.MARTIAL_STATUS_CODE = personalDetailsData.PROPOSER_MARITAL_STATUS.MARITAL_CD || null;
					if(!!personalDetailsData.PROPOSER_AGE_PROOF.DOC_ID)
						personalData.Proposer.AGE_PROOF = personalDetailsData.PROPOSER_AGE_PROOF.MPPROOF_DESC || null;
					personalData.Proposer.AGE_PROOF_DOC_ID = personalDetailsData.PROPOSER_AGE_PROOF.DOC_ID || null;
					//Added by Amruta
					personalData.Proposer.AGE_PROOF_DOB = CommonService.formatDobToDb(personalDetailsData.PROPOSER_AGE_PROOF_DOB) || null;
					if(personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
						personalData.Proposer.ID_DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.PROPOSER_IDENTITY_ISSUANCE_DATE) || null;
                    if(personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'DL' || personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PP')
                    	personalData.Proposer.ID_DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.PROPOSER_IDENTITY_EXPIRY_DATE) || null;
					if(personalDetailsData.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'DL')
						personalData.Proposer.DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.PROPOSER_ISSUANCE_DATE) || null;
					if(personalDetailsData.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'DL' || personalDetailsData.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'PP')
						personalData.Proposer.DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.PROPOSER_EXPIRY_DATE) || null;
					if(personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
						personalData.Proposer.DATE_OF_ISSUANCE = CommonService.formatDobToDb(personalDetailsData.PROPOSER_IDENTITY_ISSUANCE_DATE) || null;
                    if(personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'DL' || personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PP')
                    	personalData.Proposer.DATE_OF_EXPIRY = CommonService.formatDobToDb(personalDetailsData.PROPOSER_IDENTITY_EXPIRY_DATE) || null;

					/*if(personalDetailsData.PROPOSER_AGE_PROOF.MPPROOF_DESC == "Others")
						personalData.Proposer.AGE_PROOF_OTHER = personalDetailsData.PROPOSER_AGE_PROOF_OTHER || null;*/
					if(!!personalDetailsData.PROPOSER_IDENTITY_PROOF.DOC_ID)
						personalData.Proposer.IDENTITY_PROOF = personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_DESC || null;
					personalData.Proposer.IDENTITY_PROOF_DOC_ID = personalDetailsData.PROPOSER_IDENTITY_PROOF.DOC_ID || null;
					personalData.Proposer.IDENTITY_PROOF_NO = personalDetailsData.PROPOSER_IDENTITY_PROOF_NO || null;
					if(personalDetailsData.PROPOSER_IDENTITY_PROOF.MPPROOF_DESC == "Others")
						personalData.Proposer.IDENTITY_PROOF_OTHER = personalDetailsData.PROPOSER_IDENTITY_PROOF_OTHER || null;

					if(!!sisFormData.sisFormAData.COMPANY_CODE)
						personalData.Proposer.COMPANY_CODE = sisFormData.sisFormAData.COMPANY_CODE || null;
}

					if(sisFormData.sisFormAData.PROPOSER_IS_INSURED=="Y"){
							//Self
							personalData.Insured.RELATIONSHIP_DESC = personalDetailsData.RELATIONSHIP.RELATIONSHIP_DESC || null;
							personalData.Insured.RELATIONSHIP_ID = personalDetailsData.RELATIONSHIP.NBFE_CODE || null;
			 			}
					else{
							//Non - Self
							personalData.Proposer.RELATIONSHIP_DESC = personalDetailsData.RELATIONSHIP.RELATIONSHIP_DESC || null;
						 	personalData.Proposer.RELATIONSHIP_ID = personalDetailsData.RELATIONSHIP.NBFE_CODE || null;
						}

			dfd.resolve(personalData);
		return dfd.promise;
};

this.getAppContactDetailsData = function(contactDetBean){

console.log("in getAppContactDetailsData"+JSON.stringify(contactDetBean));
		var dfd = $q.defer();
		var contactData = {
            Insured :{},
            Proposer :{}
		};
		contactData.Insured.MOBILE_NO = contactDetBean.INSURED_MOBILE_NO || null;
		contactData.Insured.RESI_STD_CODE = contactDetBean.INSURED_RESI_STD_CODE || null;
		contactData.Insured.RESI_LANDLINE_NO = contactDetBean.INSURED_RESI_LANDLINE_NO || null;
		contactData.Insured.OFF_STD_CODE = contactDetBean.INSURED_OFF_STD_CODE || null;
		contactData.Insured.OFF_LANDLINE_NO = contactDetBean.INSURED_OFF_LANDLINE_NO || null;
		contactData.Insured.EMAIL_ID = contactDetBean.INSURED_EMAIL_ID || null;
	if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED !='Y'){
		contactData.Proposer.CUST_TYPE = 'C02';
		contactData.Proposer.MOBILE_NO = contactDetBean.PROPOSER_MOBILE_NO || null;
		contactData.Proposer.RESI_STD_CODE = contactDetBean.PROPOSER_RESI_STD_CODE || null;
		contactData.Proposer.RESI_LANDLINE_NO = contactDetBean.PROPOSER_RESI_LANDLINE_NO || null;
		contactData.Proposer.OFF_STD_CODE = contactDetBean.PROPOSER_OFF_STD_CODE || null;
		contactData.Proposer.OFF_LANDLINE_NO = contactDetBean.PROPOSER_OFF_LANDLINE_NO || null;
		contactData.Proposer.EMAIL_ID = contactDetBean.PROPOSER_EMAIL_ID || null;
		}
		dfd.resolve(contactData);
		return dfd.promise;
}

this.getAddressDetailsData = function(addressDetBean, applicationFormBean){

var dfd = $q.defer();
		var addressData = {
            Insured :{},
            Proposer :{}
		};
	console.log("in getAddrDet:"+JSON.stringify(applicationFormBean.DOCS_LIST));
	addressData.Insured.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
	addressData.Insured.RESIDENT_STATUS = addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_STATUS || null;
	addressData.Insured.RESIDENT_STATUS_CODE = addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD || null;
	addressData.Insured.NATIONALITY = 'INDIA';
	addressData.Insured.NATIONALITY_CODE = 'IN';
	addressData.Insured.CURR_RESIDENT_COUNTRY = 'INDIA';
	addressData.Insured.CURR_RESIDENT_COUNTRY_CODE = 'IN';
	addressData.Insured.BIRTH_COUNTRY = addressDetBean.INSURED_BIRTH_COUNTRY.COUNTRY_NAME;
if(!!addressDetBean.AddSameProposer)
	{
	addressData.Insured.Insured_ProposerAddress_Flag = addressDetBean.AddSameProposer ? "Y" :"N";
	addressData.Proposer.Insured_ProposerAddress_Flag = addressDetBean.AddSameProposer ? "Y" :"N";
	}
	else{
	    addressData.Insured.Insured_ProposerAddress_Flag = "N";
    	addressData.Proposer.Insured_ProposerAddress_Flag = "N";
	}

	/*if(addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'NRI')
		addressData.Insured.NATIONALITY = 'IN';*/
	if(addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'FN' || addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'OCI')
		{
			addressData.Insured.NATIONALITY = addressDetBean.INSURED_NATIONALITY.COUNTRY_NAME || null;
			addressData.Insured.NATIONALITY_CODE = addressDetBean.INSURED_NATIONALITY.COUNTRY_CODE || null;
		}
	if(addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'NRI' || addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'FN' || addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == 'OCI')
		{
			addressData.Insured.CURR_RESIDENT_COUNTRY = addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_NAME || null;
			addressData.Insured.CURR_RESIDENT_COUNTRY_CODE = addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE || null;
		}
	addressData.Insured.IS_CURRADD_PERMADD_SAME = (addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME ? 'Y':'N');
	addressData.Insured.CURR_ADD_LINE1 = addressDetBean.INSURED_CURR_ADDR1 || null;
	addressData.Insured.CURR_ADD_LINE2 = addressDetBean.INSURED_CURR_ADDR2 || null;
	addressData.Insured.CURR_ADD_LINE3 = addressDetBean.INSURED_CURR_ADDR3 || null;
	addressData.Insured.CURR_DISTRICT_LANDMARK = addressDetBean.INSURED_CURR_LANDMARK || null;
	if(addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN')
		{
			addressData.Insured.CURR_STATE = addressDetBean.INSURED_CURR_STATE_OTHER;
			addressData.Insured.CURR_STATE_CODE = 'OT';
			addressData.Insured.CURR_CITY = addressDetBean.INSURED_CURR_CITY_OTHER;
			addressData.Insured.CURR_CITY_CODE = 'OT';
		}
		else
		{
			addressData.Insured.CURR_STATE = addressDetBean.INSURED_CURR_STATE.STATE_DESC;
			addressData.Insured.CURR_STATE_CODE = addressDetBean.INSURED_CURR_STATE.STATE_CODE;
           	addressData.Insured.CURR_CITY = addressDetBean.INSURED_CURR_CITY.CITY_DESC;
           	addressData.Insured.CURR_CITY_CODE = addressDetBean.INSURED_CURR_CITY.CITY_CODE;
           	if(!!addressDetBean.INSURED_CURR_OtherCityName){addressData.Insured.CURR_CITY = addressDetBean.INSURED_CURR_OtherCityName }
		}
	addressData.Insured.CURR_PIN = addressDetBean.INSURED_CURR_PINCODE;
	//Permanent ADD
	addressData.Insured.PERM_ADD_LINE1 = addressDetBean.INSURED_PERM_ADDR1 || null;
	addressData.Insured.PERM_ADD_LINE2 = addressDetBean.INSURED_PERM_ADDR2 || null;
	addressData.Insured.PERM_ADD_LINE3 = addressDetBean.INSURED_PERM_ADDR3 || null;
	addressData.Insured.PERM_DISTRICT_LANDMARK = addressDetBean.INSURED_PERM_LANDMARK || null;
	if(addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN')
		{
			addressData.Insured.PERM_STATE = addressDetBean.INSURED_PERM_STATE_OTHER;
			addressData.Insured.PERM_STATE_CODE = 'OT';
			addressData.Insured.PERM_CITY = addressDetBean.INSURED_PERM_CITY_OTHER;
			addressData.Insured.PERM_CITY_CODE = 'OT';
		}
		else
		{
			addressData.Insured.PERM_STATE = addressDetBean.INSURED_PERM_STATE.STATE_DESC;
			addressData.Insured.PERM_STATE_CODE = addressDetBean.INSURED_PERM_STATE.STATE_CODE;
			addressData.Insured.PERM_CITY = addressDetBean.INSURED_PERM_CITY.CITY_DESC;
			addressData.Insured.PERM_CITY_CODE = addressDetBean.INSURED_PERM_CITY.CITY_CODE;
				if(!!addressDetBean.INSURED_PERM_OtherCityName){addressData.Insured.PERM_CITY = addressDetBean.INSURED_PERM_OtherCityName }
		}
	addressData.Insured.PERM_PIN = addressDetBean.INSURED_PERM_PINCODE;
	addressData.Insured.COMMUNICATE_ADD_FLAG = addressDetBean.INSURED_COMMUNICATE_ADD_FLAG;
  if(addressDetBean.INSURED_ADDRESS_PROOF.MPPROOF_DESC=="Select " || addressDetBean.INSURED_ADDRESS_PROOF.MPPROOF_DESC=="Select")
    addressData.Insured.ADDRESS_PROOF = null;
  else
	 addressData.Insured.ADDRESS_PROOF = addressDetBean.INSURED_ADDRESS_PROOF.MPPROOF_DESC;
	addressData.Insured.ADDRESS_PROOF_DOC_ID = addressDetBean.INSURED_ADDRESS_PROOF.DOC_ID;
	if(addressDetBean.INSURED_ADDRESS_PROOF.MPPROOF_DESC == 'Others')
		addressData.Insured.ADDRESS_PROOF_OTHER = addressDetBean.INSURED_OTHER_ADDRESS_PROOF;
	if(!!addressDetBean.INSURED_BILL_DATE)
		addressData.Insured.DATE_OF_BILL = CommonService.formatDobToDb(addressDetBean.INSURED_BILL_DATE) || null;
	else
		addressData.Insured.DATE_OF_BILL = null;

	//Proposer DET
	if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
	addressData.Proposer.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
	addressData.Proposer.RESIDENT_STATUS = addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_STATUS || null;
    	addressData.Proposer.RESIDENT_STATUS_CODE = addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD || null;
    	addressData.Proposer.NATIONALITY = 'INDIA';
    	addressData.Proposer.NATIONALITY_CODE = 'IN';
    	addressData.Proposer.CURR_RESIDENT_COUNTRY = 'INDIA';
    	addressData.Proposer.CURR_RESIDENT_COUNTRY_CODE = 'IN';
    	addressData.Proposer.BIRTH_COUNTRY = addressDetBean.PROPOSER_BIRTH_COUNTRY.COUNTRY_NAME;
    	/*if(addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'NRI')
    		addressData.Proposer.NATIONALITY = 'IN';*/
    	if(addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'FN' || addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'OCI')
    		{
    			addressData.Proposer.NATIONALITY = addressDetBean.PROPOSER_NATIONALITY.COUNTRY_NAME || null;
    			addressData.Proposer.NATIONALITY_CODE = addressDetBean.PROPOSER_NATIONALITY.COUNTRY_CODE || null;
    		}
    	if(addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'NRI' || addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'FN' || addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == 'OCI')
    		{
    			addressData.Proposer.CURR_RESIDENT_COUNTRY = addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_NAME || null;
    			addressData.Proposer.CURR_RESIDENT_COUNTRY_CODE = addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE || null;
    		}

    	addressData.Proposer.IS_CURRADD_PERMADD_SAME = (addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME ? 'Y':'N');
    	addressData.Proposer.CURR_ADD_LINE1 = addressDetBean.PROPOSER_CURR_ADDR1 || null;
    	addressData.Proposer.CURR_ADD_LINE2 = addressDetBean.PROPOSER_CURR_ADDR2 || null;
    	addressData.Proposer.CURR_ADD_LINE3 = addressDetBean.PROPOSER_CURR_ADDR3 || null;
    	addressData.Proposer.CURR_DISTRICT_LANDMARK = addressDetBean.PROPOSER_CURR_LANDMARK || null;
    	if(addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN')
    		{
    			addressData.Proposer.CURR_STATE = addressDetBean.PROPOSER_CURR_STATE_OTHER;
    			addressData.Proposer.CURR_STATE_CODE = 'OT';
    			addressData.Proposer.CURR_CITY = addressDetBean.PROPOSER_CURR_CITY_OTHER;
    			addressData.Proposer.CURR_CITY_CODE = 'OT';
    		}
    		else
    		{
    			addressData.Proposer.CURR_STATE = addressDetBean.PROPOSER_CURR_STATE.STATE_DESC;
    			addressData.Proposer.CURR_STATE_CODE = addressDetBean.PROPOSER_CURR_STATE.STATE_CODE;
               	addressData.Proposer.CURR_CITY = addressDetBean.PROPOSER_CURR_CITY.CITY_DESC;
               	addressData.Proposer.CURR_CITY_CODE = addressDetBean.PROPOSER_CURR_CITY.CITY_CODE;
               	if(!!addressDetBean.PROPOSER_CURR_OtherCityName){addressData.Proposer.CURR_CITY = addressDetBean.PROPOSER_CURR_OtherCityName }
    		}
    	addressData.Proposer.CURR_PIN = addressDetBean.PROPOSER_CURR_PINCODE;
    	//Permanent ADD
    	addressData.Proposer.PERM_ADD_LINE1 = addressDetBean.PROPOSER_PERM_ADDR1 || null;
    	addressData.Proposer.PERM_ADD_LINE2 = addressDetBean.PROPOSER_PERM_ADDR2 || null;
    	addressData.Proposer.PERM_ADD_LINE3 = addressDetBean.PROPOSER_PERM_ADDR3 || null;
    	addressData.Proposer.PERM_DISTRICT_LANDMARK = addressDetBean.PROPOSER_PERM_LANDMARK || null;
    	if(addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN')
    		{
    			addressData.Proposer.PERM_STATE = addressDetBean.PROPOSER_PERM_STATE_OTHER;
    			addressData.Proposer.PERM_STATE_CODE = 'OT';
    			addressData.Proposer.PERM_CITY = addressDetBean.PROPOSER_PERM_CITY_OTHER;
    			addressData.Proposer.PERM_CITY_CODE = 'OT';
    		}
    		else
    		{
    			addressData.Proposer.PERM_STATE = addressDetBean.PROPOSER_PERM_STATE.STATE_DESC;
    			addressData.Proposer.PERM_STATE_CODE = addressDetBean.PROPOSER_PERM_STATE.STATE_CODE;
    			addressData.Proposer.PERM_CITY = addressDetBean.PROPOSER_PERM_CITY.CITY_DESC;
    			addressData.Proposer.PERM_CITY_CODE = addressDetBean.PROPOSER_PERM_CITY.CITY_CODE;
    			if(!!addressDetBean.PROPOSER_PERM_OtherCityName){addressData.Proposer.PERM_CITY  = addressDetBean.PROPOSER_PERM_OtherCityName }
    		}
    	addressData.Proposer.PERM_PIN = addressDetBean.PROPOSER_PERM_PINCODE;
    	addressData.Proposer.COMMUNICATE_ADD_FLAG = addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG;
      if(addressDetBean.PROPOSER_ADDRESS_PROOF.MPPROOF_DESC=="Select " || addressDetBean.PROPOSER_ADDRESS_PROOF.MPPROOF_DESC=="Select")
        addressData.Proposer.ADDRESS_PROOF = null;
      else
        addressData.Proposer.ADDRESS_PROOF = addressDetBean.PROPOSER_ADDRESS_PROOF.MPPROOF_DESC;
    	addressData.Proposer.ADDRESS_PROOF_DOC_ID = addressDetBean.PROPOSER_ADDRESS_PROOF.DOC_ID;
    	if(addressDetBean.PROPOSER_ADDRESS_PROOF.MPPROOF_DESC == 'Others')
    		addressData.Proposer.ADDRESS_PROOF_OTHER = addressDetBean.PROPOSER_OTHER_ADDRESS_PROOF;

    	if(!!addressDetBean.PROPOSER_BILL_DATE)
    		addressData.Proposer.DATE_OF_BILL = CommonService.formatDobToDb(addressDetBean.PROPOSER_BILL_DATE) || null;
    	else
    		addressData.Proposer.DATE_OF_BILL = null;
	}
	dfd.resolve(addressData);
		return dfd.promise;
}

this.getEduQualDetails = function(eduQualDetBean, applicationFormBean,sisFormData){
debug("SISForm data is::"+JSON.stringify(sisFormData));
var InsuredAge , Age;
var dfd = $q.defer();
		var eduQualData = {
            Insured :{},
            Proposer :{}
		};
		eduQualData.Insured.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
		console.log(JSON.stringify(applicationFormBean.DOCS_LIST)+"getEduQualDetails :ApplicationFormDataService::"+JSON.stringify(eduQualDetBean.INSURED_INCOME_PROOF));
		eduQualData.Insured.FATCA = eduQualDetBean.INSURED_FATCA || null;
		eduQualData.Insured.EDUCATION_QUALIFICATION = eduQualDetBean.INSURED_EDUCATION_QUALIFICATION.LOOKUP_TYPE_DESC || null;
		eduQualData.Insured.EDUCATION_QUALIFICATION_CODE = eduQualDetBean.INSURED_EDUCATION_QUALIFICATION.NBFE_CODE || null;
		eduQualData.Insured.OTHER_EDUCATION_DETS = eduQualDetBean.INSURED_OTHER_EDUCATION_DETS || null;
		eduQualData.Insured.OCCUPATION_CLASS = eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS || null;
		eduQualData.Insured.OCCUPATION_CLASS_NBFE_CODE = eduQualDetBean.INSURED_OCCUPATION_CLASS.NBFE_CODE || null;
		eduQualData.Insured.DESIGNATION = eduQualDetBean.INSURED_DESIGNATION.DESIGNATION || null;
		console.log("Insured designation :::"+JSON.stringify(eduQualDetBean.INSURED_DESIGNATION.DESIGNATION));

		/*if(eduQualDetBean.INSURED_WORK_NATURE!='Select ' && eduQualDetBean.INSURED_WORK_NATURE!=undefined)
			eduQualData.Insured.OCCU_CODE = eduQualDetBean.INSURED_WORK_NATURE.OCCUPATION_CODE || null;*/

		if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Others')
			eduQualData.Insured.OCCUPATION_CLASS_OTHER = eduQualDetBean.INSURED_OCCUPATION_CLASS_OTHER || null;
		if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Student')
			eduQualData.Insured.STUDENT_STANDARD = eduQualDetBean.INSURED_STUDENT_STANDARD || null;
		if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Salaried'){
            eduQualData.Insured.COMPANY_CODE = eduQualDetBean.INSURED_COMPANY_NAME.COMPANY_CODE || null;
            if(eduQualDetBean.INSURED_COMPANY_NAME.COMPANY_CODE == "WZ99")
            	eduQualData.Insured.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.INSURED_EMPLOYER_NAME || null;
            else
            	eduQualData.Insured.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.INSURED_COMPANY_NAME.COMPANY_NAME || null;
        }
        else{
            eduQualData.Insured.COMPANY_CODE = null;
            eduQualData.Insured.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.INSURED_EMPLOYER_NAME || null;
        }
        if(eduQualDetBean.INSURED_DESIGNATION.DESIGNATION == 'Others')
        	eduQualData.Insured.DESIGNATION_OTHER = eduQualDetBean.INSURED_DESIGNATION_OTHER || null;

		eduQualData.Insured.EMPLOYER_SCH_BUSI_ADDRESS = eduQualDetBean.INSURED_EMPLOYER_ADDRESS || null;
		if(!!eduQualDetBean.INSURED_OCCUPATION && eduQualDetBean.INSURED_OCCUPATION.OCCU_GROUP!="Select Type")
			eduQualData.Insured.OCCUPATION = eduQualDetBean.INSURED_OCCUPATION.OCCU_GROUP || null;
		else
			eduQualData.Insured.OCCUPATION = null;

		if(!!sisFormData.sisFormAData.INSURED_DOB && sisFormData.sisFormAData.INSURED_DOB != undefined)
		InsuredAge = sisFormData.sisFormAData.INSURED_DOB;
		Age = CommonService.getAge(InsuredAge);
		debug("Age of Insured in PersonalInfo ctrl is::"+Age);

		if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Housewife')
			eduQualData.Insured.OCCUPATION_CD = 'Z64';
		else if(Age < 18 && eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Student/Juvenile')
        	eduQualData.Insured.OCCUPATION_CD = 'Z69';
		else if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Student/Juvenile')
			eduQualData.Insured.OCCUPATION_CD = 'O19';
		else if(eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS == 'Retired')
			eduQualData.Insured.OCCUPATION_CD = 'Z87';
		else
			eduQualData.Insured.OCCUPATION_CD = eduQualDetBean.INSURED_WORK_NATURE.OCCUPATION_CODE || ApplicationFormDataService.SISFormData.sisFormAData.INSURED_OCCU_CODE || null;

		console.log("Insured OCCU_CD is::"+JSON.stringify(eduQualData.Insured.OCCUPATION_CD));

		eduQualData.Insured.OCCUPATION_OTHERS = eduQualDetBean.INSURED_OCCUPATION_OTHER || null;
		if(!!eduQualDetBean.INSURED_INDUSTRY && eduQualDetBean.INSURED_INDUSTRY.APP_INDUSTRY!="Select Industry")
			eduQualData.Insured.OCCUPATION_INDUSTRY = eduQualDetBean.INSURED_INDUSTRY.APP_INDUSTRY || null;
		else
			eduQualData.Insured.OCCUPATION_INDUSTRY = null;

		eduQualData.Insured.OCCUP_INDUSTRY_OTHERS = eduQualDetBean.INSURED_INDUSTRY_OTHER || null;
		if(!!eduQualDetBean.INSURED_WORK_NATURE && eduQualDetBean.INSURED_WORK_NATURE.APP_NATURE_OF_WORK!="Select Nature Of Work"){
			eduQualData.Insured.OCCU_WORK_NATURE = eduQualDetBean.INSURED_WORK_NATURE.APP_NATURE_OF_WORK || null;
			eduQualData.Insured.OCCU_TERM_FLAG = eduQualDetBean.INSURED_WORK_NATURE.OCCU_TERM_FLAG || null;
        }
		else
			eduQualData.Insured.OCCU_WORK_NATURE = null;

		eduQualData.Insured.OCCU_WORK_NATURE_OTHERS = eduQualDetBean.INSURED_WORK_NATURE_OTHER || null;
		eduQualData.Insured.ANNUAL_INCOME = eduQualDetBean.INSURED_ANNUAL_INCOME || null;
		eduQualData.Insured.INCOME_PROOF_DOC_ID = eduQualDetBean.INSURED_INCOME_PROOF.DOC_ID || null;
		if(!!eduQualData.Insured.INCOME_PROOF_DOC_ID)
			{
				if(!!eduQualDetBean.INSURED_INCOME_PROOF.MPPROOF_DESC)
					eduQualData.Insured.INCOME_PROOF = eduQualDetBean.INSURED_INCOME_PROOF.MPPROOF_DESC || null;
      }
		else
			eduQualData.Insured.INCOME_PROOF = null;
		if(eduQualDetBean.INSURED_INCOME_PROOF.MPPROOF_DESC == 'Others')
			eduQualData.Insured.INCOME_PROOF_OTHER = eduQualDetBean.INSURED_INCOME_PROOF_OTHER || null;

		if(!!eduQualDetBean.INSURED_QUESTIONNAIRE)
			eduQualData.Insured.OCCU_QUES_SNO = eduQualDetBean.INSURED_QUESTIONNAIRE.SNO || null;
		console.log("Questionarrie SNO::"+JSON.stringify(eduQualData.Insured.OCCU_QUES_SNO));

		//Proposer
		if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
		eduQualData.Proposer.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
		eduQualData.Proposer.FATCA = eduQualDetBean.PROPOSER_FATCA || null;
		eduQualData.Proposer.EDUCATION_QUALIFICATION = eduQualDetBean.PROPOSER_EDUCATION_QUALIFICATION.LOOKUP_TYPE_DESC || null;
		eduQualData.Proposer.EDUCATION_QUALIFICATION_CODE = eduQualDetBean.PROPOSER_EDUCATION_QUALIFICATION.NBFE_CODE || null;
		eduQualData.Proposer.OTHER_EDUCATION_DETS = eduQualDetBean.PROPOSER_OTHER_EDUCATION_DETS || null;
		eduQualData.Proposer.OCCUPATION_CLASS = eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS || null;
		eduQualData.Proposer.OCCUPATION_CLASS_NBFE_CODE = eduQualDetBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE || null;
		eduQualData.Proposer.DESIGNATION = eduQualDetBean.PROPOSER_DESIGNATION.DESIGNATION || null;

		/*if(eduQualDetBean.PROPOSER_WORK_NATURE != 'Select ' && eduQualDetBean.PROPOSER_WORK_NATURE!=undefined)
			eduQualData.Proposer.OCCU_CODE = eduQualDetBean.PROPOSER_WORK_NATURE.OCCUPATION_CODE || null;*/

		if(!!eduQualDetBean.PROPOSER_QUESTIONNAIRE)
			eduQualData.Proposer.OCCU_QUES_SNO = eduQualDetBean.PROPOSER_QUESTIONNAIRE.SNO || null;
		console.log("Questionarrie SNO::"+JSON.stringify(eduQualData.Proposer.OCCU_QUES_SNO));

		if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Others')
			eduQualData.Proposer.OCCUPATION_CLASS_OTHER = eduQualDetBean.PROPOSER_OCCUPATION_CLASS_OTHER || null;

		if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Student')
			eduQualData.Proposer.STUDENT_STANDARD = eduQualDetBean.PROPOSER_STUDENT_STANDARD || null;
		if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Salaried'){
		    eduQualData.Proposer.COMPANY_CODE = eduQualDetBean.PROPOSER_COMPANY_NAME.COMPANY_CODE || null;
		    if(eduQualDetBean.PROPOSER_COMPANY_NAME.COMPANY_CODE == "WZ99")
		    	eduQualData.Proposer.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.PROPOSER_EMPLOYER_NAME || null;
		    else
		    	eduQualData.Proposer.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.PROPOSER_COMPANY_NAME.COMPANY_NAME || null;
		}
		else{
		    eduQualData.Proposer.COMPANY_CODE = null;
		    eduQualData.Proposer.EMPLOYER_SCH_BUSI_NAME = eduQualDetBean.PROPOSER_EMPLOYER_NAME || null;
		}
		if(eduQualDetBean.PROPOSER_DESIGNATION.DESIGNATION == 'Others')
           eduQualData.Proposer.DESIGNATION_OTHER = eduQualDetBean.PROPOSER_DESIGNATION_OTHER || null;

		eduQualData.Proposer.EMPLOYER_SCH_BUSI_ADDRESS = eduQualDetBean.PROPOSER_EMPLOYER_ADDRESS || null;
		if(eduQualDetBean.PROPOSER_OCCUPATION.OCCU_GROUP!="Select Type")
			eduQualData.Proposer.OCCUPATION = eduQualDetBean.PROPOSER_OCCUPATION.OCCU_GROUP || null;
		else
			eduQualData.Proposer.OCCUPATION = null;

		if(!!sisFormData.sisFormAData.PROPOSER_DOB && sisFormData.sisFormAData.PROPOSER_DOB != undefined)
		ProposerAge = sisFormData.sisFormAData.PROPOSER_DOB;
		PropAge = CommonService.getAge(ProposerAge);
		debug("Age of Insured in PersonalInfo ctrl is::"+PropAge);

		if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Housewife')
			eduQualData.Proposer.OCCUPATION_CD = 'Z64';
		else if(PropAge < 18 && eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Student/Juvenile')
            eduQualData.Proposer.OCCUPATION_CD = 'Z69';
		else if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Student/Juvenile')
			eduQualData.Proposer.OCCUPATION_CD = 'O19';
		else if(eduQualDetBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS == 'Retired')
			eduQualData.Proposer.OCCUPATION_CD = 'Z87';
		else
			eduQualData.Proposer.OCCUPATION_CD = eduQualDetBean.PROPOSER_WORK_NATURE.OCCUPATION_CODE || ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_OCCU_CODE || null;

		console.log("Proposer_Occupation_Code is::"+JSON.stringify(eduQualData.Proposer.OCCUPATION_CD));

		eduQualData.Proposer.OCCUPATION_OTHERS = eduQualDetBean.PROPOSER_OCCUPATION_OTHER || null;
		if(eduQualDetBean.PROPOSER_INDUSTRY.APP_INDUSTRY!="Select Industry")
			eduQualData.Proposer.OCCUPATION_INDUSTRY = eduQualDetBean.PROPOSER_INDUSTRY.APP_INDUSTRY || null;
		else
			eduQualData.Proposer.OCCUPATION_INDUSTRY = null;

		eduQualData.Proposer.OCCUP_INDUSTRY_OTHERS = eduQualDetBean.PROPOSER_INDUSTRY_OTHER || null;
		if(eduQualDetBean.PROPOSER_WORK_NATURE.APP_NATURE_OF_WORK!="Select Nature Of Work")
			eduQualData.Proposer.OCCU_WORK_NATURE = eduQualDetBean.PROPOSER_WORK_NATURE.APP_NATURE_OF_WORK || null;
		else
			eduQualData.Proposer.OCCU_WORK_NATURE = null;

		eduQualData.Proposer.OCCU_WORK_NATURE_OTHERS = eduQualDetBean.PROPOSER_WORK_NATURE_OTHER || null;
		eduQualData.Proposer.ANNUAL_INCOME = eduQualDetBean.PROPOSER_ANNUAL_INCOME || null;
		eduQualData.Proposer.INCOME_PROOF_DOC_ID = eduQualDetBean.PROPOSER_INCOME_PROOF.DOC_ID || null;
		if(!!eduQualData.Proposer.INCOME_PROOF_DOC_ID)
			eduQualData.Proposer.INCOME_PROOF = eduQualDetBean.PROPOSER_INCOME_PROOF.MPPROOF_DESC || null;
		else
			eduQualData.Proposer.INCOME_PROOF = null;
		if(eduQualDetBean.PROPOSER_INCOME_PROOF.MPPROOF_DESC == 'Others')
			eduQualData.Proposer.INCOME_PROOF_OTHER = eduQualDetBean.PROPOSER_INCOME_PROOF_OTHER || null;
		}
		dfd.resolve(eduQualData);

	return dfd.promise;
}
this.getIncomeProofData = function(incmPrfBean, eduQualDetBean,SISFormData){
    var dfd = $q.defer();
    var incomeProofBean = {};
    incomeProofBean = SISFormData.sisFormFData;
    debug("incmPrfBean : " + JSON.stringify(incmPrfBean));
    incomeProofBean.EDUCATION_QUALIFICATION = eduQualDetBean.INSURED_EDUCATION_QUALIFICATION.LOOKUP_TYPE_DESC || null;
    incomeProofBean.EDUCATION_QUALIFICATION_CODE = eduQualDetBean.INSURED_EDUCATION_QUALIFICATION.NBFE_CODE || null;
    incomeProofBean.ELIGIBILITY_FLAG_OCC = eduQualDetBean.INSURED_WORK_NATURE.OCCU_TERM_FLAG || null;
    if(incomeProofBean.OCCUPATION_CLASS_NBFE_CODE==2){
    	incomeProofBean.OCCUPATION_CLASS = eduQualDetBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS;
    	incomeProofBean.OCCUPATION_CLASS_NBFE_CODE = eduQualDetBean.INSURED_OCCUPATION_CLASS.NBFE_CODE;
    }
    incomeProofBean.INCOME_PROOF = eduQualDetBean.INSURED_INCOME_PROOF.MPPROOF_DESC || null;
    incomeProofBean.INCOME_PROOF_DOC_ID = eduQualDetBean.INSURED_INCOME_PROOF.DOC_ID || null;
    incomeProofBean.S_FRM16_YEAR1 = incmPrfBean.S_FRM16_YEAR1.YEAR_VALUE || null;
    incomeProofBean.S_FRM16_YEAR2 = incmPrfBean.S_FRM16_YEAR2.YEAR_VALUE || null;
    incomeProofBean.S_FRM16_YEAR3 = incmPrfBean.S_FRM16_YEAR3.YEAR_VALUE || null;
    incomeProofBean.S_FRM16_SAL1 = incmPrfBean.S_FRM16_SAL1;
    incomeProofBean.S_FRM16_SAL2 = incmPrfBean.S_FRM16_SAL2;
    incomeProofBean.S_FRM16_SAL3 = incmPrfBean.S_FRM16_SAL3;
    if(!!incmPrfBean.S_EMP_ISSDT)
        incomeProofBean.S_EMP_ISSDT = CommonService.formatDobToDb(new Date(incmPrfBean.S_EMP_ISSDT)) || null;
    else
        incomeProofBean.S_EMP_ISSDT = null;
    incomeProofBean.S_EMP_SAL = incmPrfBean.S_EMP_SAL || null;
    incomeProofBean.S_BNK_MONTH_YEAR1 = incmPrfBean.S_BNK_MONTH_YEAR1.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_MONTH_YEAR2 = incmPrfBean.S_BNK_MONTH_YEAR2.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_MONTH_YEAR3 = incmPrfBean.S_BNK_MONTH_YEAR3.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_MONTH_YEAR4 = incmPrfBean.S_BNK_MONTH_YEAR4.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_MONTH_YEAR5 = incmPrfBean.S_BNK_MONTH_YEAR5.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_MONTH_YEAR6 = incmPrfBean.S_BNK_MONTH_YEAR6.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_BNK_SAL1 = incmPrfBean.S_BNK_SAL1;
    incomeProofBean.S_BNK_SAL2 = incmPrfBean.S_BNK_SAL2;
    incomeProofBean.S_BNK_SAL3 = incmPrfBean.S_BNK_SAL3;
    incomeProofBean.S_BNK_SAL4 = incmPrfBean.S_BNK_SAL4;
    incomeProofBean.S_BNK_SAL5 = incmPrfBean.S_BNK_SAL5;
    incomeProofBean.S_BNK_SAL6 = incmPrfBean.S_BNK_SAL6;
    incomeProofBean.S_SAL_MONTH_YEAR1 = incmPrfBean.S_SAL_MONTH_YEAR1.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_SAL_MONTH_YEAR2 = incmPrfBean.S_SAL_MONTH_YEAR2.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_SAL_MONTH_YEAR3 = incmPrfBean.S_SAL_MONTH_YEAR3.MONTH_YEAR_VALUE || null;
    incomeProofBean.S_SAL_SAL1 = incmPrfBean.S_SAL_SAL1;
    incomeProofBean.S_SAL_SAL2 = incmPrfBean.S_SAL_SAL2;
    incomeProofBean.S_SAL_SAL3 = incmPrfBean.S_SAL_SAL3;
    incomeProofBean.S_COI_YEAR1 = incmPrfBean.S_COI_YEAR1.YEAR_VALUE || null;
    incomeProofBean.S_COI_YEAR2 = incmPrfBean.S_COI_YEAR2.YEAR_VALUE || null;
    //incomeProofBean.S_COI_YEAR3 = incmPrfBean.S_COI_YEAR3.YEAR_VALUE || null;
    incomeProofBean.S_COI_SAL1 = incmPrfBean.S_COI_SAL1;
    incomeProofBean.S_COI_SAL2 = incmPrfBean.S_COI_SAL2;
    //incomeProofBean.S_COI_SAL3 = incmPrfBean.S_COI_SAL3;
    incomeProofBean.SE_FRM16_YEAR1 = incmPrfBean.SE_FRM16_YEAR1.YEAR_VALUE || null;
    incomeProofBean.SE_FRM16_YEAR2 = incmPrfBean.SE_FRM16_YEAR2.YEAR_VALUE || null;
    incomeProofBean.SE_FRM16_YEAR3 = incmPrfBean.SE_FRM16_YEAR3.YEAR_VALUE || null;
    incomeProofBean.SE_FRM16_SAL1 = incmPrfBean.SE_FRM16_SAL1;
    incomeProofBean.SE_FRM16_SAL2 = incmPrfBean.SE_FRM16_SAL2;
    incomeProofBean.SE_FRM16_SAL3 = incmPrfBean.SE_FRM16_SAL3;
    incomeProofBean.SE_COI_YEAR1 = incmPrfBean.SE_COI_YEAR1.YEAR_VALUE || null;
    incomeProofBean.SE_COI_YEAR2 = incmPrfBean.SE_COI_YEAR2.YEAR_VALUE || null;
    //incomeProofBean.SE_COI_YEAR3 = incmPrfBean.SE_COI_YEAR3.YEAR_VALUE || null;
    incomeProofBean.SE_COI_PROFIT1 = incmPrfBean.SE_COI_PROFIT1;
    incomeProofBean.SE_COI_PROFIT2 = incmPrfBean.SE_COI_PROFIT2;
    //incomeProofBean.SE_COI_PROFIT3 = incmPrfBean.SE_COI_PROFIT3;
    incomeProofBean.SE_COI_SAL1 = incmPrfBean.SE_COI_SAL1;
    incomeProofBean.SE_COI_SAL2 = incmPrfBean.SE_COI_SAL2;
    //incomeProofBean.SE_COI_SAL3 = incmPrfBean.SE_COI_SAL3;
    incomeProofBean.SE_OTH_DOC = incmPrfBean.SE_OTH_DOC || null;
    incomeProofBean.SE_OTH_PROFIT = incmPrfBean.SE_OTH_PROFIT;
    incomeProofBean.UE_HOUSE_PROP = incmPrfBean.UE_HOUSE_PROP;
    incomeProofBean.UE_RENT = incmPrfBean.UE_RENT;
    incomeProofBean.UE_GAIN_DIV = incmPrfBean.UE_GAIN_DIV;
    incomeProofBean.UE_AGRI = incmPrfBean.UE_AGRI;
    incomeProofBean.UE_IOTH = incmPrfBean.UE_IOTH;
    incomeProofBean.UE_GIFT = incmPrfBean.UE_GIFT;
    incomeProofBean.UE_OTH = incmPrfBean.UE_OTH;
    var code = 0;
    if(!!incomeProofBean.EDUCATION_QUALIFICATION_CODE)
        code = parseInt(incomeProofBean.EDUCATION_QUALIFICATION_CODE);

    if(incomeProofBean.RESIDENT_STATUS_CODE == 'RI' && [1,2,6,7].indexOf(code) != -1){
        incomeProofBean.ELIGIBILITY_FLAG_EDUCATION = 'N';
    }else if(incomeProofBean.RESIDENT_STATUS_CODE != 'RI' && [1,2,3,6,7].indexOf(code) != -1){
        incomeProofBean.ELIGIBILITY_FLAG_EDUCATION = 'N';
    }else {
        incomeProofBean.ELIGIBILITY_FLAG_EDUCATION = 'Y';
    }
    dfd.resolve(incomeProofBean);
    return dfd.promise;
};

this.getInsuranceObjectiveData = function(otherDetailsBean){
var dfd = $q.defer();
var appMainData = {};
debug("Application Place :"+otherDetailsBean.APPLICATION_PLACE);

appMainData.APPLICATION_PLACE = otherDetailsBean.APPLICATION_PLACE;

if(otherDetailsBean.INS_OBJ_RISK)
appMainData.INS_OBJ_RISK = "Risk";
if(otherDetailsBean.INS_OBJ_SAVINGS)
appMainData.INS_OBJ_SAVINGS = "Saving";
if(otherDetailsBean.INS_OBJ_CHILD_EDU)
appMainData.INS_OBJ_CHILD_EDU = "Child Education";
if(otherDetailsBean.INS_OBJ_MARRIAGE)
appMainData.INS_OBJ_MARRIAGE = "Marriage";
if(otherDetailsBean.INS_OBJ_RETIREMENT)
appMainData.INS_OBJ_RETIREMENT = "Retirement";
if(otherDetailsBean.INS_OBJ_LEGACY_PLAN)
appMainData.INS_OBJ_LEGACY_PLAN = "Legacy Plan";
if(otherDetailsBean.INS_OBJ_OTHER)
	appMainData.INS_OBJ_OTHER_DETAILS = otherDetailsBean.INS_OBJ_OTHER_DETAILS;

dfd.resolve(appMainData);

return dfd.promise;
}

this.getOtherDetails = function(otherDetailsBean, applicationFormBean){

var dfd = $q.defer();
		var otherData = {
            Insured :{},
            Proposer :{}
		};
		otherData.Insured.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);
		otherData.Proposer.DOCS_LIST = JSON.stringify(applicationFormBean.DOCS_LIST);

		//Insured
		otherData.Insured.HUSB_FATH_FLAG = otherDetailsBean.INSURED_HUSB_FATH_FLAG || null;
		otherData.Insured.HUSB_FATH_FIRST_NAME = otherDetailsBean.INSURED_HUSB_FATH_FIRST_NAME || null;
		otherData.Insured.HUSB_FATH_LAST_NAME = otherDetailsBean.INSURED_HUSB_FATH_LAST_NAME || null;
		otherData.Insured.PEP_SELP_FLAG = otherDetailsBean.INSURED_PEP_SELP_FLAG || null;
		if(otherDetailsBean.INSURED_PEP_SELP_FLAG == 'Y')
			otherData.Insured.PEP_DETAILS = otherDetailsBean.INSURED_PEP_DETAILS || null;
		otherData.Insured.PEP_FAMILY_FLAG = otherDetailsBean.INSURED_PEP_FAMILY_FLAG || null;
		if(otherDetailsBean.INSURED_PEP_FAMILY_FLAG == 'Y')
			otherData.Insured.PEP_FAMILY_DETAILS = otherDetailsBean.INSURED_PEP_FAMILY_DETAILS || null;
		if(!!otherDetailsBean.INSURED_FUND_PROOF && !!otherDetailsBean.INSURED_FUND_PROOF.DOC_ID)
			otherData.Insured.PROOF_OF_SOURCE_OF_INCOME_DOC_ID = otherDetailsBean.INSURED_FUND_PROOF.DOC_ID || null;
		else
			otherData.Insured.PROOF_OF_SOURCE_OF_INCOME_DOC_ID = null;
		if(!!otherDetailsBean.INSURED_FUND_PROOF && otherDetailsBean.INSURED_FUND_PROOF.MPPROOF_DESC != 'Select ')
			otherData.Insured.PROOF_OF_SOURCE_OF_INCOME = otherDetailsBean.INSURED_FUND_PROOF.MPPROOF_DESC || null;
		else
			otherData.Insured.PROOF_OF_SOURCE_OF_INCOME = null;
		otherData.Insured.AML_BANK_STATEMENT_FLAG = otherDetailsBean.INSURED_BANK_STATEMENT || null;
		if(!!otherDetailsBean.INSURED_BANK_STATEMENT && otherDetailsBean.INSURED_BANK_STATEMENT=='Y'){
			otherData.Insured.AML_BANK_STATEMENT = "Bank Statement with closing balance > current policy premium";
			otherData.Insured.AML_BANK_STATEMENT_DOC_ID = "1039010598";
		}else {
			otherData.Insured.AML_BANK_STATEMENT = null;
			otherData.Insured.AML_BANK_STATEMENT_DOC_ID = null;
		}
		//Proposer
		if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED !='Y'){
		otherData.Proposer.HUSB_FATH_FLAG = otherDetailsBean.PROPOSER_HUSB_FATH_FLAG || null;
		otherData.Proposer.HUSB_FATH_FIRST_NAME = otherDetailsBean.PROPOSER_HUSB_FATH_FIRST_NAME || null;
		otherData.Proposer.HUSB_FATH_LAST_NAME = otherDetailsBean.PROPOSER_HUSB_FATH_LAST_NAME || null;
		otherData.Proposer.PEP_SELP_FLAG = otherDetailsBean.PROPOSER_PEP_SELP_FLAG || null;
		if(otherDetailsBean.PROPOSER_PEP_SELP_FLAG == 'Y')
			otherData.Proposer.PEP_DETAILS = otherDetailsBean.PROPOSER_PEP_DETAILS || null;
		otherData.Proposer.PEP_FAMILY_FLAG = otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG || null;
		if(otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG == 'Y')
			otherData.Proposer.PEP_FAMILY_DETAILS = otherDetailsBean.PROPOSER_PEP_FAMILY_DETAILS || null;
			}
		if(!!otherDetailsBean.PROPOSER_FUND_PROOF && !!otherDetailsBean.PROPOSER_FUND_PROOF.DOC_ID)
			otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME_DOC_ID = otherDetailsBean.PROPOSER_FUND_PROOF.DOC_ID || null;
		else
			otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME_DOC_ID = null;
		if(!!otherDetailsBean.PROPOSER_FUND_PROOF && otherDetailsBean.PROPOSER_FUND_PROOF.MPPROOF_DESC != 'Select ')
			otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME = otherDetailsBean.PROPOSER_FUND_PROOF.MPPROOF_DESC || null;
		else
			otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME = null;
		otherData.Proposer.AML_BANK_STATEMENT_FLAG = otherDetailsBean.PROPOSER_BANK_STATEMENT || null;
		if(!!otherDetailsBean.PROPOSER_BANK_STATEMENT && otherDetailsBean.PROPOSER_BANK_STATEMENT=='Y'){
			otherData.Proposer.AML_BANK_STATEMENT = "Bank Statement with closing balance > current policy premium";
			otherData.Proposer.AML_BANK_STATEMENT_DOC_ID = "2039010598";
		}else {
			otherData.Proposer.AML_BANK_STATEMENT = null;
			otherData.Proposer.AML_BANK_STATEMENT_DOC_ID = null;
		}
		dfd.resolve(otherData);

	return dfd.promise;
}
}]);
