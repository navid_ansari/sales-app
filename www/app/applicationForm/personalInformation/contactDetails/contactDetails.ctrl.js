contactDetailsModule.controller('ContactDetailsCtrl',['$q','$state','CommonService', 'ApplicationFormDataService', 'PersonalInfoService','ExistingAppData','persInfoService','DeDupeData', function($q, $state,CommonService, ApplicationFormDataService, PersonalInfoService, ExistingAppData,persInfoService, DeDupeData){

var contact = this;
this.contactDetBean = {};
console.log("ApplicationFormDataService "+JSON.stringify(ApplicationFormDataService));
    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


ApplicationFormDataService.applicationFormBean.contactDetBean = this.contactDetBean;
ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
contact.click = false;
this.mobRegex = MOBILE_REGEX;
this.emailRegex = EMAIL_REGEX;
this.numRegex = NUM_REG;
this.pinRegex = PINCODE_REG;
this.contactDetBean.insuredDOB = /*CommonService.formatDobToDb(*/ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;//);
this.contactDetBean.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
console.log("ExistingAppData :contact:"+JSON.stringify(ExistingAppData)+"sdfg :: "+this.insuredDOB);

this.setExistingContactDetails = function(){
      var dfd = $q.defer();
      var contactData = ExistingAppData.applicationPersonalInfo;
      CommonService.hideLoading();
      if(!! ApplicationFormDataService.SISFormData.sisFormAData.INSURED_EMAIL)
      {
        console.log("Ins EMAIL ::"+ApplicationFormDataService.SISFormData.sisFormAData.INSURED_EMAIL);
      	contact.contactDetBean.INSURED_EMAIL_ID = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_EMAIL;
      	if(!!ApplicationFormDataService.SISFormData.sisFormAData.INSURED_MOBILE)
      	    contact.contactDetBean.INSURED_MOBILE_NO = parseInt(ApplicationFormDataService.SISFormData.sisFormAData.INSURED_MOBILE);
      }

      if(!! ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_EMAIL && contact.contactDetBean.isSelf!=true)
      {
        contact.contactDetBean.PROPOSER_EMAIL_ID = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_EMAIL;
        if(!!ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_MOBILE)
        	    contact.contactDetBean.PROPOSER_MOBILE_NO = parseInt(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_MOBILE);
      }

      if(ExistingAppData!=undefined && ExistingAppData.applicationMainData!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.applicationPersonalInfo.Insured).length!=0)
      {
      		ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
      		if(contactData.Insured.MOBILE_NO!=undefined && contactData.Insured.MOBILE_NO!="")
      		  contact.contactDetBean.INSURED_MOBILE_NO = parseInt(contactData.Insured.MOBILE_NO);
      		if(contactData.Insured.RESI_STD_CODE!=undefined && contactData.Insured.RESI_STD_CODE!="")
      		  contact.contactDetBean.INSURED_RESI_STD_CODE = parseInt(contactData.Insured.RESI_STD_CODE);
      		if(contactData.Insured.RESI_LANDLINE_NO!=undefined && contactData.Insured.RESI_LANDLINE_NO!="")
      		  contact.contactDetBean.INSURED_RESI_LANDLINE_NO = parseInt(contactData.Insured.RESI_LANDLINE_NO);
      		if(contactData.Insured.OFF_STD_CODE!=undefined && contactData.Insured.OFF_STD_CODE!="")
      		  contact.contactDetBean.INSURED_OFF_STD_CODE = parseInt(contactData.Insured.OFF_STD_CODE);
      		if(contactData.Insured.OFF_LANDLINE_NO!=undefined && contactData.Insured.OFF_LANDLINE_NO!="")
      		  contact.contactDetBean.INSURED_OFF_LANDLINE_NO = parseInt(contactData.Insured.OFF_LANDLINE_NO);
      		if(!! contactData.Insured.EMAIL_ID)
      			{
              debug("Setting EmailId")
      			     contact.contactDetBean.INSURED_EMAIL_ID = contactData.Insured.EMAIL_ID;
      			}
      		if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
          		if(contactData.Proposer.MOBILE_NO!=undefined && contactData.Proposer.MOBILE_NO!="")
          		  contact.contactDetBean.PROPOSER_MOBILE_NO = parseInt(contactData.Proposer.MOBILE_NO);
          		if(contactData.Proposer.RESI_STD_CODE!=undefined && contactData.Proposer.RESI_STD_CODE!="")
          		  contact.contactDetBean.PROPOSER_RESI_STD_CODE = parseInt(contactData.Proposer.RESI_STD_CODE);
          		if(contactData.Proposer.RESI_LANDLINE_NO!=undefined && contactData.Proposer.RESI_LANDLINE_NO!="")
          		  contact.contactDetBean.PROPOSER_RESI_LANDLINE_NO = parseInt(contactData.Proposer.RESI_LANDLINE_NO);
          		if(contactData.Proposer.OFF_STD_CODE!=undefined && contactData.Proposer.OFF_STD_CODE!="")
          		  contact.contactDetBean.PROPOSER_OFF_STD_CODE = parseInt(contactData.Proposer.OFF_STD_CODE);
          		if(contactData.Proposer.OFF_LANDLINE_NO!=undefined && contactData.Proposer.OFF_LANDLINE_NO!="")
          		  contact.contactDetBean.PROPOSER_OFF_LANDLINE_NO = parseInt(contactData.Proposer.OFF_LANDLINE_NO);
          		if(!! contactData.Proposer.EMAIL_ID)
          			contact.contactDetBean.PROPOSER_EMAIL_ID = contactData.Proposer.EMAIL_ID;
      		}
      		ApplicationFormDataService.applicationFormBean.contactDetBean = contact.contactDetBean;
      }
      else
      	console.log("no data")

      	dfd.resolve(contact.contactDetBean);
      	return dfd.promise;
}

this.setExistingContactDetails().then(function(){
  contact.setDeDupeData(DeDupeData).then(function (resp) {
      CommonService.hideLoading();
  });
  ApplicationFormDataService.applicationFormBean.contactDetBean = contact.contactDetBean;
}
);

this.setDeDupeData = function(DeDupeData){
  var dfd = $q.defer();
  try{
    if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && ExistingAppData.applicationMainData.IS_CONTACT_DET != 'Y'){
      CommonService.showLoading("Loading Existing Data..");
      var insContact1 = DeDupeData.RESULT_APPDATA.INSURED_PHONE_1;
      var insContact2 = DeDupeData.RESULT_APPDATA.INSURED_PHONE_2;
      var insContArr1 = (!!insContact1) ? insContact1.split(/[\s,-]+/) : [""];
      var insContArr2 = (!!insContact2) ? insContact2.split(/[\s,-]+/) : [""];

      if(!contact.contactDetBean.INSURED_RESI_STD_CODE)
        contact.contactDetBean.INSURED_RESI_STD_CODE = (!!insContArr1 && !!insContArr1[0]) ? parseInt(insContArr1[0]) : null;
      if(!contact.contactDetBean.INSURED_RESI_LANDLINE_NO)
      {
        if((!!insContArr1 && !!insContArr1[1]))
          contact.contactDetBean.INSURED_RESI_LANDLINE_NO =  (!!insContArr1 && !!insContArr1[1]) ? parseInt(insContArr1[1]) : null;
        else
          {
            contact.contactDetBean.INSURED_RESI_STD_CODE = null;
            contact.contactDetBean.INSURED_RESI_LANDLINE_NO = (!!insContArr1 && !!insContArr1[0]) ? parseInt(insContArr1[0]) : null;
          }
      }
      if(!contact.contactDetBean.INSURED_OFF_STD_CODE)
        contact.contactDetBean.INSURED_OFF_STD_CODE = (!!insContArr2 && !!insContArr2[0]) ? parseInt(insContArr2[0]) : null;
      if(!contact.contactDetBean.INSURED_OFF_LANDLINE_NO)
        {
          if((!!insContArr2 && !!insContArr2[1]))
            contact.contactDetBean.INSURED_OFF_LANDLINE_NO = (!!insContArr2 && !!insContArr2[1]) ? parseInt(insContArr2[1]) : null;
          else {
            contact.contactDetBean.INSURED_OFF_STD_CODE = null;
            contact.contactDetBean.INSURED_OFF_LANDLINE_NO = (!!insContArr2 && !!insContArr2[0]) ? parseInt(insContArr2[0]) : null;
          }
        }

      if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
          var propContact1 = DeDupeData.RESULT_APPDATA.OWNER_PHONE_1;
          var propContact2 = DeDupeData.RESULT_APPDATA.OWNER_PHONE_2;
          var prContArr1 = (!!propContact1) ? propContact1.split(/[\s,-]+/) : [""];
          var prContArr2 = (!!propContact2) ? propContact2.split(/[\s,-]+/) : [""];

          if(!contact.contactDetBean.PROPOSER_RESI_STD_CODE)
            contact.contactDetBean.PROPOSER_RESI_STD_CODE = (!!prContArr1 && !!prContArr1[0]) ? parseInt(prContArr1[0]) : null;
          if(!contact.contactDetBean.PROPOSER_RESI_LANDLINE_NO)
            {
              if((!!prContArr1 && !!prContArr1[1]))
                contact.contactDetBean.PROPOSER_RESI_LANDLINE_NO = (!!prContArr1 && !!prContArr1[1]) ? parseInt(prContArr1[1]) : null;
              else
              {
                contact.contactDetBean.PROPOSER_RESI_STD_CODE = null;
                contact.contactDetBean.PROPOSER_RESI_LANDLINE_NO = (!!prContArr1 && !!prContArr1[0]) ? parseInt(prContArr1[0]) : null;
              }
            }
          if(!contact.contactDetBean.PROPOSER_OFF_STD_CODE)
            contact.contactDetBean.PROPOSER_OFF_STD_CODE = (!!prContArr2 && !!prContArr2[0]) ? parseInt(prContArr2[0]) : null;
          if(!contact.contactDetBean.PROPOSER_OFF_LANDLINE_NO)
            {
              if((!!prContArr2 && !!prContArr2[1]))
                contact.contactDetBean.PROPOSER_OFF_LANDLINE_NO = (!!prContArr2 && !!prContArr2[1]) ? parseInt(prContArr2[1]) : null;
              else
              {
                contact.contactDetBean.PROPOSER_OFF_STD_CODE = null;
                contact.contactDetBean.PROPOSER_OFF_LANDLINE_NO = (!!prContArr2 && !!prContArr2[0]) ? parseInt(prContArr2[0]) : null;
              }

            }
      }
      dfd.resolve(contact.contactDetBean);
    }
    else
      {
        debug("Already data saved");
        dfd.resolve(contact.contactDetBean);
      }
  }catch(e){
    debug("Exception in deDupe - Contact");
    CommonService.hideLoading();
    dfd.resolve(contact.contactDetBean);
  }
    return dfd.promise;
}

this.onNext = function(contactForm){
	CommonService.showLoading();
contact.click = true;
if(contactForm.$invalid!=true){
ApplicationFormDataService.applicationFormBean.contactDetBean = contact.contactDetBean;
PersonalInfoService.getAppContactDetailsData(contact.contactDetBean).then(function(ContactData){
var whereClauseObj = {};
        	whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

var whereClauseObj1 = {};
        	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj1.CUST_TYPE = 'C01';
var whereClauseObj2 = {};
        	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj2.CUST_TYPE = 'C02';
    console.log("DATA to be inserted :"+JSON.stringify(ContactData));
    CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Insured, whereClauseObj1).then(
				                function(res){
				                console.log("LP_APP_CONTACT_SCRN_A update success !!"+JSON.stringify(whereClauseObj1));
				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_CONTACT_DET" : 'Y'}, whereClauseObj).then(
                                                					function(res){
                                                						console.log("LP_APP_CONTACT_SCRN_A IS_CONTACT_DET update success !!"+JSON.stringify(whereClauseObj2));
                                                					});
				                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
				                CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Proposer, whereClauseObj2).then(
                                				                function(res){
                                				                persInfoService.setActiveTab("addressDetails");
                                				                $state.go('applicationForm.personalInfo.addressDetails');
                                									console.log("LP_APP_CONTACT_SCRN_A update success !!"+JSON.stringify(whereClauseObj2));
                                								});
                                else{
                                    persInfoService.setActiveTab("addressDetails");
                                    $state.go('applicationForm.personalInfo.addressDetails');
                                }
								});

});
}
else
    navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");

}

CommonService.hideLoading();
}]);
