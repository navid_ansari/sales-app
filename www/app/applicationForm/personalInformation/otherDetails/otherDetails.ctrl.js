otherDetailsModule.controller('OtherDetailsCtrl',['$q','$state', '$filter','CommonService','ApplicationFormDataService','PersonalInfoService','ExistingAppData','AppTimeService','qLifeStyleInfoService','PscData', 'IncomeSourceProofList', 'amlData', 'otherDetailService','DeDupeData', function($q, $state, $filter, CommonService, ApplicationFormDataService,PersonalInfoService, ExistingAppData,AppTimeService,qLifeStyleInfoService,PscData, IncomeSourceProofList, amlData, otherDetailService, DeDupeData){

var otherDet  = this;

otherDet.fundProodList = IncomeSourceProofList;

debug("<< PscData= >>"+JSON.stringify(PscData));
debug("<< amlData= >>"+JSON.stringify(amlData));
//this.rdOnlinePsc = "N";
otherDet.PSC_COND = false;
otherDet.PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
otherDet.PSC_COND = (otherDet.PGL_ID == "185" || otherDet.PGL_ID == "191");
otherDet.isTerm = false;
debug("<< otherDet.PSC_COND= >>"+otherDet.PSC_COND);
if(!!PscData){
	if(PscData.DIGITAL_PSC_FLAG=='M')
		this.rdOnlinePsc = "N";
	else
		this.rdOnlinePsc = "Y";

	this.PscTiming=PscData.PREFERRED_CALL_TIME;
	this.PscLanguage=PscData.PREFERRED_LANGUAGE_CD;
	//debug("<<PscTiming"+PscData.PREFERRED_CALL_TIME);
	//debug("<<PscTiming"+PscData.PREFERRED_LANGUAGE_CD);
}
otherDet.isTerm = (!!ApplicationFormDataService.ComboFormBean && !!ApplicationFormDataService.ComboFormBean.COMBO_ID && ApplicationFormDataService.ComboFormBean.COMBO_ID!="");
debug("otherDet.isTerm :"+otherDet.isTerm)
if(/*otherDet.PGL_ID == "222" || */(otherDet.PGL_ID == "224" || otherDet.PGL_ID == "225") || (!!ApplicationFormDataService.ComboFormBean && !!ApplicationFormDataService.ComboFormBean.COMBO_ID && ApplicationFormDataService.ComboFormBean.COMBO_ID!="")){
	this.rdOnlinePsc = 'N';
}
/*else if(BUSINESS_TYPE == 'iWealth')
	{
		otherDet.PSC_COND = true;
		this.rdOnlinePsc = 'N';
		debug("<< otherDet.PSC_COND= >>"+otherDet.PSC_COND);
	}*/

this.otherDetailsBean = {};

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */







ApplicationFormDataService.applicationFormBean.otherDetailsBean = this.otherDetailsBean;
ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
debug("otherDet.PSC_COND :"+otherDet.PSC_COND);
otherDet.nameRegex = FULLNAME_REGEX_SP;
otherDet.appPlaceRegex = FULLNAME_REGEX;
otherDet.freeTxt = FREE_TEXT_REGEX;
otherDet.click = false;
otherDet.insObjValidate = false;
otherDet.otherDetailsBean.INS_OBJ_RISK = false;
otherDet.otherDetailsBean.INS_OBJ_SAVINGS = false;
otherDet.otherDetailsBean.INS_OBJ_CHILD_EDU = false;
otherDet.otherDetailsBean.INS_OBJ_MARRIAGE = false;
otherDet.otherDetailsBean.INS_OBJ_RETIREMENT = false;
otherDet.otherDetailsBean.INS_OBJ_OTHER = false;

this.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)
console.log("ExistingAppData otherDet :"+JSON.stringify(ExistingAppData)+"inside OtherDetailsCtrl ::"+JSON.stringify(ApplicationFormDataService));
this.insGender = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_GENDER;
this.proGender = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_GENDER;
var age = CommonService.getAge(ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB);
console.log(otherDet.insGender+" :: "+"Insured age :"+age);
if(this.insGender == 'M')
    otherDet.otherDetailsBean.INSURED_HUSB_FATH_FLAG = 'F';

if(this.proGender == 'M' && otherDet.isSelf!=true)
    otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_FLAG = 'F';

if(this.insGender == 'F' && parseInt(age)<18)
    otherDet.otherDetailsBean.INSURED_HUSB_FATH_FLAG = 'F';
/************AML**************/
	otherDet.otherDetailsBean.INSURED_FUND_PROOF = otherDet.fundProodList[0];
	otherDet.otherDetailsBean.PROPOSER_FUND_PROOF = otherDet.fundProodList[0];
	if(otherDet.isSelf){
		otherDet.insFundProofReq = ((!!amlData) && (amlData.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y')) ? true : false;
		otherDet.proFundProofReq = false;
		otherDet.insBnkStmtValidate = ((!!amlData) && (amlData.BANK_STMT_REQ == 'Y')) ? true : false;
		otherDet.proBnkStmtValidate = false;
	}
	else {
		otherDet.insFundProofReq = false;
		otherDet.proFundProofReq = ((!!amlData) && (amlData.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y'))  ? true : false;
		otherDet.insBnkStmtValidate = false;
		otherDet.proBnkStmtValidate = ((!!amlData) && (amlData.BANK_STMT_REQ == 'Y')) ? true : false;
	}
	debug("otherDet.insFundProofReq : " + otherDet.insFundProofReq);
	debug("otherDet.proFundProofReq : " + otherDet.proFundProofReq);
	debug("otherDet.insBnkStmtValidate : " + otherDet.insBnkStmtValidate);
	debug("otherDet.proBnkStmtValidate : " + otherDet.proBnkStmtValidate);
/************AML**************/
this.onChangePSCOption = function(){

	console.log(otherDet.rdOnlinePsc);
	if(otherDet.rdOnlinePsc == 'Y')
		navigator.notification.alert("PSC is available in English and Hindi only.",function(){CommonService.hideLoading();} ,"Application","OK");

}


this.onChangeInsObjective = function(){

	if(otherDet.otherDetailsBean.INS_OBJ_RISK || otherDet.otherDetailsBean.INS_OBJ_SAVINGS || otherDet.otherDetailsBean.INS_OBJ_CHILD_EDU || otherDet.otherDetailsBean.INS_OBJ_MARRIAGE || otherDet.otherDetailsBean.INS_OBJ_RETIREMENT || otherDet.otherDetailsBean.INS_OBJ_LEGACY_PLAN || otherDet.otherDetailsBean.INS_OBJ_OTHER)
	{
    	otherDet.insObjValidate = true;
	}
	else
		otherDet.insObjValidate = false;

};
this.onInsPolExpChg = function(otherDetForm){
	if((!!otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG && otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG == 'Y') || (!!otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG && otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG == 'Y')){
		otherDetailService.getAmlData(ExistingAppData, true).then(
			function(res){
				debug("res : " + JSON.stringify(res));
				if(otherDet.isSelf){
					otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
					otherDet.proFundProofReq = false;
					otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					otherDet.proBnkStmtValidate = false;
				}
				else {
					otherDet.insFundProofReq = false;
					otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
					otherDet.insBnkStmtValidate = false;
					otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
				}
				if(otherDet.insFundProofReq)
					otherDetForm.insFundProof.$setValidity('selectRequired', false);
				else
					otherDetForm.insFundProof.$setValidity('selectRequired', true);
				if(otherDet.proFundProofReq)
					otherDetForm.proFundProof.$setValidity('selectRequired', false);
				else
					otherDetForm.proFundProof.$setValidity('selectRequired', true);
				debug("insFundProofReq1 : " + otherDet.insFundProofReq);
				debug("proFundProofReq1 : " + otherDet.proFundProofReq);
				debug("insBnkStmtValidate1 : " + otherDet.insBnkStmtValidate);
				debug("proBnkStmtValidate1 : " + otherDet.proBnkStmtValidate);
			}
		);
	}else if(!!otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG && otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG == 'N'){
		otherDetailService.getAmlData(ExistingAppData, false).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq){
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					}
					else{
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					}
					if(otherDet.proFundProofReq){
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					}
					else{
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					}
					debug("insFundProofReq2 : " + otherDet.insFundProofReq);
					debug("proFundProofReq2 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate2 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate2 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res getAmlData(): null");
				}
			}
		);
	}
};

this.onProPolExpChg = function(otherDetForm){
	if((!!otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG == 'Y') || (!!otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG == 'Y')){
		otherDetailService.getAmlData(ExistingAppData, true).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq3 : " + otherDet.insFundProofReq);
					debug("proFundProofReq3 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate3 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate3 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onProPolExpChg getAmlData(): null");
				}
			}
		);
	}else if(!!otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG == 'N'){
		otherDetailService.getAmlData(ExistingAppData, false).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq4 : " + otherDet.insFundProofReq);
					debug("proFundProofReq4 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate4 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate4 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onProPolExpChg getAmlData(): null");
				}
			}
		);
	}
};

this.onInsFamPolExpChg = function(otherDetForm){
	if((!!otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG && otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG == 'Y') || (!!otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG && otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG == 'Y')){
		otherDetailService.getAmlData(ExistingAppData, true).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq5 : " + otherDet.insFundProofReq);
					debug("proFundProofReq5 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate5 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate5 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onInsFamPolExpChg getAmlData(): null");
				}
			}
		);
	}else if(!!otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG && otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG == 'N'){
		otherDetailService.getAmlData(ExistingAppData, false).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq6 : " + otherDet.insFundProofReq);
					debug("proFundProofReq6 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate6 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate6 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onInsFamPolExpChg getAmlData(): null");
				}
			}
		);
	}
};

this.onProFamPolExpChg = function(otherDetForm){
	if((!!otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG == 'Y') || (!!otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG == 'Y')){
		otherDetailService.getAmlData(ExistingAppData, true).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq7 : " + otherDet.insFundProofReq);
					debug("proFundProofReq7 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate7 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate7 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onProFamPolExpChg getAmlData(): null");
				}
			}
		);
	}else if(!!otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG && otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG == 'N'){
		otherDetailService.getAmlData(ExistingAppData, false).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						otherDet.insFundProofReq = false;
						otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					if(otherDet.insFundProofReq)
						otherDetForm.insFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.insFundProof.$setValidity('selectRequired', true);
					if(otherDet.proFundProofReq)
						otherDetForm.proFundProof.$setValidity('selectRequired', false);
					else
						otherDetForm.proFundProof.$setValidity('selectRequired', true);
					debug("insFundProofReq8 : " + otherDet.insFundProofReq);
					debug("proFundProofReq8 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate8 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate8 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onProFamPolExpChg getAmlData(): null");
				}
			}
		);
	}
};

this.onInsFundProofChg = function(){
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.aml = {};
	if(!!otherDet.otherDetailsBean.INSURED_FUND_PROOF.DOC_ID){
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.aml.DOC_ID = otherDet.otherDetailsBean.INSURED_FUND_PROOF.DOC_ID;
		navigator.notification.alert("You will not be allowed to submit the application without uploading the Proof of Source of Income.",function(){CommonService.hideLoading();},"Application","OK");
	}else {
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.aml.DOC_ID = null;
	}
	if(!!otherDet.otherDetailsBean.INSURED_FUND_PROOF.DOC_ID && otherDet.otherDetailsBean.INSURED_FUND_PROOF.STANDARD_FLAG=='Y'){
		otherDet.insBnkStmtValidate = false;
	}else{
		otherDetailService.getAmlData(ExistingAppData, (otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG == 'Y' || otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG == 'Y')).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						//otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						//otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						//otherDet.insFundProofReq = false;
						//otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					debug("insFundProofReq9 : " + otherDet.insFundProofReq);
					debug("proFundProofReq9 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate9 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate9 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onInsFundProofChg getAmlData(): null");
				}
			}
		);
	}
};
this.onProFundProofChg = function(){
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.aml = {};
	if(!!otherDet.otherDetailsBean.PROPOSER_FUND_PROOF.DOC_ID){
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.aml.DOC_ID = otherDet.otherDetailsBean.PROPOSER_FUND_PROOF.DOC_ID;
		navigator.notification.alert("You will not be allowed to submit the application without uploading the Proof of Source of Income.",function(){CommonService.hideLoading();},"Application","OK");
	}else {
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.aml.DOC_ID = null;
	}
	if(!!otherDet.otherDetailsBean.PROPOSER_FUND_PROOF.DOC_ID && otherDet.otherDetailsBean.PROPOSER_FUND_PROOF.STANDARD_FLAG=='Y'){
		otherDet.proBnkStmtValidate = false;
	}else{
		otherDetailService.getAmlData(ExistingAppData, (otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG == 'Y' || otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG == 'Y')).then(
			function(res){
				if(!!res){
					debug("res : " + JSON.stringify(res));
					if(otherDet.isSelf){
						//otherDet.insFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						//otherDet.proFundProofReq = false;
						otherDet.insBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
						otherDet.proBnkStmtValidate = false;
					}
					else {
						//otherDet.insFundProofReq = false;
						//otherDet.proFundProofReq = res.PROOF_OF_SOURCE_OF_INCOME_REQ == 'Y' ? true : false;
						otherDet.insBnkStmtValidate = false;
						otherDet.proBnkStmtValidate = res.BANK_STMT_REQ == 'Y' ? true : false;
					}
					debug("insFundProofReq10 : " + otherDet.insFundProofReq);
					debug("proFundProofReq10 : " + otherDet.proFundProofReq);
					debug("insBnkStmtValidate10 : " + otherDet.insBnkStmtValidate);
					debug("proBnkStmtValidate10 : " + otherDet.proBnkStmtValidate);
				}
				else{
					debug("res onInsFundProofChg getAmlData(): null");
				}
			}
		);
	}
};

this.onInsBankStmtChg = function(){
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.amlBank = {};
	if(!!otherDet.otherDetailsBean.INSURED_BANK_STATEMENT && otherDet.otherDetailsBean.INSURED_BANK_STATEMENT == 'Y'){
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.amlBank.DOC_ID = '1039010598';
		navigator.notification.alert("You will not be allowed to submit the application without uploading the Bank Statement with closing balance > current policy premium.",function(){CommonService.hideLoading();},"Application","OK");
	}else {
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.amlBank.DOC_ID = null;
	}
};

this.onProBankStmtChg = function(){
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.amlBank = {};
	if(!!otherDet.otherDetailsBean.PROPOSER_BANK_STATEMENT && otherDet.otherDetailsBean.PROPOSER_BANK_STATEMENT == 'Y'){
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.amlBank.DOC_ID = '2039010597';
		navigator.notification.alert("You will not be allowed to submit the application without uploading the Bank Statement with closing balance > current policy premium.",function(){CommonService.hideLoading();},"Application","OK");
	}else {
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.amlBank.DOC_ID = null;
	}
};

this.setExistingOtherDetails = function(){
var dfd = $q.defer();
var otherData = ExistingAppData.applicationPersonalInfo;
var appMain = ExistingAppData.applicationMainData;
	if(ExistingAppData!=undefined && ExistingAppData.applicationMainData!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.applicationPersonalInfo.Insured).length!=0)
	{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
			if(otherData.Insured.HUSB_FATH_FLAG)
			otherDet.otherDetailsBean.INSURED_HUSB_FATH_FLAG = otherData.Insured.HUSB_FATH_FLAG;
			otherDet.otherDetailsBean.INSURED_HUSB_FATH_FIRST_NAME = otherData.Insured.HUSB_FATH_FIRST_NAME;
			otherDet.otherDetailsBean.INSURED_HUSB_FATH_LAST_NAME = otherData.Insured.HUSB_FATH_LAST_NAME;
			otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG = otherData.Insured.PEP_SELP_FLAG;
			if(otherData.Insured.PEP_SELP_FLAG == 'Y')
				otherDet.otherDetailsBean.INSURED_PEP_DETAILS = otherData.Insured.PEP_DETAILS;
			otherDet.otherDetailsBean.INSURED_PEP_FAMILY_FLAG = otherData.Insured.PEP_FAMILY_FLAG;
			if(otherData.Insured.PEP_FAMILY_FLAG == 'Y')
				otherDet.otherDetailsBean.INSURED_PEP_FAMILY_DETAILS = otherData.Insured.PEP_FAMILY_DETAILS;
			if(!!otherData.Insured.PROOF_OF_SOURCE_OF_INCOME_DOC_ID){
				otherDet.otherDetailsBean.INSURED_FUND_PROOF = $filter('filter')(otherDet.fundProodList, {"DOC_ID": otherData.Insured.PROOF_OF_SOURCE_OF_INCOME_DOC_ID}, true)[0];
			}
			otherDet.otherDetailsBean.INSURED_BANK_STATEMENT = otherData.Insured.AML_BANK_STATEMENT_FLAG;

			//Proposer
			if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
			if(otherData.Proposer.HUSB_FATH_FLAG)
			otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_FLAG = otherData.Proposer.HUSB_FATH_FLAG;
			otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_FIRST_NAME = otherData.Proposer.HUSB_FATH_FIRST_NAME;
			otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_LAST_NAME = otherData.Proposer.HUSB_FATH_LAST_NAME;
			otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG = otherData.Proposer.PEP_SELP_FLAG;
			if(otherData.Proposer.PEP_SELP_FLAG == 'Y')
				otherDet.otherDetailsBean.PROPOSER_PEP_DETAILS = otherData.Proposer.PEP_DETAILS;
			otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_FLAG = otherData.Proposer.PEP_FAMILY_FLAG;
			if(otherData.Proposer.PEP_FAMILY_FLAG == 'Y')
				otherDet.otherDetailsBean.PROPOSER_PEP_FAMILY_DETAILS = otherData.Proposer.PEP_FAMILY_DETAILS;
			if(!!otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME_DOC_ID){
				otherDet.otherDetailsBean.PROPOSER_FUND_PROOF = $filter('filter')(otherDet.fundProodList, {"DOC_ID": otherData.Proposer.PROOF_OF_SOURCE_OF_INCOME_DOC_ID}, true)[0];
			}
			otherDet.otherDetailsBean.PROPOSER_BANK_STATEMENT = otherData.Proposer.AML_BANK_STATEMENT_FLAG;
			}
			otherDet.otherDetailsBean.APPLICATION_PLACE = appMain.APPLICATION_PLACE;

			if(appMain.INS_OBJ_RISK == "Risk")
				otherDet.otherDetailsBean.INS_OBJ_RISK = true;
			else
				otherDet.otherDetailsBean.INS_OBJ_RISK = false;
			if(appMain.INS_OBJ_SAVINGS == "Saving")
            	otherDet.otherDetailsBean.INS_OBJ_SAVINGS = true;
            else
            	otherDet.otherDetailsBean.INS_OBJ_SAVINGS = false
            if(appMain.INS_OBJ_CHILD_EDU == "Child Education")
            	otherDet.otherDetailsBean.INS_OBJ_CHILD_EDU = true;
            else
            	otherDet.otherDetailsBean.INS_OBJ_CHILD_EDU = false;
            if(appMain.INS_OBJ_MARRIAGE == "Marriage")
            	otherDet.otherDetailsBean.INS_OBJ_MARRIAGE = true;
            else
            	otherDet.otherDetailsBean.INS_OBJ_MARRIAGE = false;
            if(appMain.INS_OBJ_RETIREMENT == "Retirement")
            	otherDet.otherDetailsBean.INS_OBJ_RETIREMENT = false;
            else
            	otherDet.otherDetailsBean.INS_OBJ_RETIREMENT = false;
            if(appMain.INS_OBJ_LEGACY_PLAN == "Legacy Plan")
            	otherDet.otherDetailsBean.INS_OBJ_LEGACY_PLAN = true;
            else
            	otherDet.otherDetailsBean.INS_OBJ_LEGACY_PLAN = false;

            if(appMain.INS_OBJ_OTHER_DETAILS != undefined && appMain.INS_OBJ_OTHER_DETAILS != "")
            {
            	otherDet.otherDetailsBean.INS_OBJ_OTHER = true;
            	otherDet.otherDetailsBean.INS_OBJ_OTHER_DETAILS = appMain.INS_OBJ_OTHER_DETAILS;
            }
			else{
				otherDet.otherDetailsBean.INS_OBJ_OTHER = false;
				otherDet.otherDetailsBean.INS_OBJ_OTHER_DETAILS = null;
			}
		dfd.resolve(otherDet.otherDetailsBean);
	}
	return dfd.promise;
}

this.setDeDupeData = function () {
	var dfd = $q.defer();
		try{
			if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && ExistingAppData.applicationMainData.IS_OTHER_DET != 'Y'){
						CommonService.showLoading("Loading Existing Data..");
						if(!!DeDupeData.RESULT_APPDATA.INSURED_FATHER_NAME)
							var nameArr = DeDupeData.RESULT_APPDATA.INSURED_FATHER_NAME.split(' ');
						otherDet.otherDetailsBean.INSURED_HUSB_FATH_FIRST_NAME = (!!nameArr && !!nameArr[0]) ? nameArr[0] : null;
						otherDet.otherDetailsBean.INSURED_HUSB_FATH_LAST_NAME = (!!nameArr && !!nameArr[1]) ? nameArr[1] : null;
						otherDet.otherDetailsBean.INSURED_PEP_SELP_FLAG = (!!DeDupeData.RESULT_APPDATA.INSURED_PIP) ? DeDupeData.RESULT_APPDATA.INSURED_PIP : null;
						if(!otherDet.isSelf)
						{
							if(!!DeDupeData.RESULT_APPDATA.OWNER_FATHER_NAME)
								var nameArr = DeDupeData.RESULT_APPDATA.OWNER_FATHER_NAME.split(' ');
							otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_FIRST_NAME = (!!nameArr && !!nameArr[0]) ? nameArr[0] : null;
							otherDet.otherDetailsBean.PROPOSER_HUSB_FATH_LAST_NAME = (!!nameArr && !!nameArr[1]) ? nameArr[1] : null;
							otherDet.otherDetailsBean.PROPOSER_PEP_SELP_FLAG = (!!DeDupeData.RESULT_APPDATA.OWNER_PIP) ? DeDupeData.RESULT_APPDATA.OWNER_PIP : null;
						}
						dfd.resolve(otherDet.otherDetailsBean);
			}
			else
			{
				debug("Already data saved");
				dfd.resolve(otherDet.otherDetailsBean);
			}
		}catch(e){
			debug("Exception in deDupe - Education");
			CommonService.hideLoading();
			dfd.resolve(otherDet.otherDetailsBean);
		}
	return dfd.promise;
}

otherDet.setExistingOtherDetails().then(function(){
	otherDet.setDeDupeData().then(function (resp) {
			CommonService.hideLoading();
	});
if(otherDet.otherDetailsBean.INS_OBJ_RISK || otherDet.otherDetailsBean.INS_OBJ_SAVINGS || otherDet.otherDetailsBean.INS_OBJ_CHILD_EDU || otherDet.otherDetailsBean.INS_OBJ_MARRIAGE || otherDet.otherDetailsBean.INS_OBJ_RETIREMENT || otherDet.otherDetailsBean.INS_OBJ_LEGACY_PLAN || otherDet.otherDetailsBean.INS_OBJ_OTHER)
	{
		otherDet.insObjValidate = true; //Valid
	}
	else
		otherDet.insObjValidate = false; //Invalid

});

this.insertPSCData=function(query,parameters){
	var dfd = $q.defer();
	db.transaction(function(tx){
		tx.executeSql(query,parameters,
		function(success){
			debug("success fully inserted PSC data");
			dfd.resolve(true);
		},
		function(tx, err){
			debug("fail to insert PSC data"+err.message);
			dfd.resolve(false);
		}
		)
	});
	return dfd.promise;
}

this.onNext = function(otherForm){
	CommonService.showLoading();
otherDet.click = true;

console.log("Psc PrefLanguage:" +this.PscTiming);
console.log("Psc Call Time"+this.PscLanguage);

if(otherForm.$invalid!=true && otherDet.insObjValidate == true){
	if((otherDet.isSelf && otherDet.insBnkStmtValidate && otherDet.otherDetailsBean.INSURED_BANK_STATEMENT == 'N') || (!otherDet.isSelf && otherDet.proBnkStmtValidate && otherDet.otherDetailsBean.PROPOSER_BANK_STATEMENT == 'N')){
		navigator.notification.alert("Bank statement with closing balance > current policy premium required",function(){CommonService.hideLoading();} ,"Application","OK");
	}else{
		ApplicationFormDataService.applicationFormBean.otherDetailsBean = otherDet.otherDetailsBean;
			PersonalInfoService.getOtherDetails(otherDet.otherDetailsBean, ApplicationFormDataService.applicationFormBean).then(function(OtherData){
					PersonalInfoService.getInsuranceObjectiveData(otherDet.otherDetailsBean).then(function(InsObj){
					var whereClauseObj = {};
								whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
								whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

					var whereClauseObj1 = {};
								whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
								whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
								whereClauseObj1.CUST_TYPE = 'C01';
					var whereClauseObj2 = {};
								whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
								whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
								whereClauseObj2.CUST_TYPE = 'C02';
					var whereClauseObj3 = {};
								whereClauseObj3.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
								whereClauseObj3.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

						console.log("Ins Objectve :"+JSON.stringify(InsObj)+"DATA to be inserted :"+JSON.stringify(OtherData));
						CommonService.updateRecords(db, 'LP_APPLICATION_MAIN', InsObj, whereClauseObj3).then(
														function(res){
														console.log("LP_APPLICATION_MAIN OtherData update success !!"+JSON.stringify(whereClauseObj3));


														});
						CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', OtherData.Insured, whereClauseObj1).then(
										   function(res){
													console.log("LP_APP_CONTACT_SCRN_A OtherData update success !!"+JSON.stringify(whereClauseObj1));
													CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_OTHER_DET" : 'Y',"IS_SCREEN1_COMPLETED" :"Y"}, whereClauseObj).then(
																		function(res){
																			//timeline changes..
																			//$rootScope.val = 10;
																			/*var query = "insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L(APPLICATION_ID,AGENT_CD,POLICY_NO,SIS_ID,FHR_ID,PREFERRED_LANGUAGE_CD,PREFERRED_CALL_TIME,DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE) values(?,?,?,?,?,?,?,?,?)";
																			var params = "";
																			debug("SIS ID >> "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
																			debug("VALUE SELECTED "+otherDet.rdOnlinePsc);
																			//if(BUSINESS_TYPE == 'iWealth'){
																				//params = [ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD,null,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID,ApplicationFormDataService.applicationFormBean.FF_FHR_ID,null,null,null,null];
																			//}
																			//else{
																					if(otherDet.rdOnlinePsc == "N"){
																						params = [ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD,null,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID,ApplicationFormDataService.applicationFormBean.FF_FHR_ID,otherDet.PscLanguage,otherDet.PscTiming,"M","Y"];
																					}else{
																						params = [ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD,null,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID,ApplicationFormDataService.applicationFormBean.FF_FHR_ID,null,null,"D","P"];
																					}
																			//}
																			debug("<<<< PARAMETERS >>>"+params);*/

																			/*otherDet.insertPSCData(query,params).then(
																				function(ifInserted){
																					if(ifInserted){*/
																						AppTimeService.setProgressValue(10);
																						CommonService.showLoading();
																						AppTimeService.isLifeStageCompleted = true;
																						AppTimeService.setActiveTab("lifeStyle");
																						qLifeStyleInfoService.setActiveTab("lifeStyleIns");

																						$state.go('applicationForm.questionnairels.lifeStyleIns');
																						console.log("LP_APP_CONTACT_SCRN_A IS_OTHER_DET update success !!"+JSON.stringify(whereClauseObj2));
																					/*}
																				}
																			);*/
																		});
													if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
													CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', OtherData.Proposer, whereClauseObj2).then(
																					function(res){
																						console.log("LP_APP_CONTACT_SCRN_A OtherData update success !!"+JSON.stringify(whereClauseObj2));
																					});

													});
					});
			});
	}

}
else{
	navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
}
}
CommonService.hideLoading();
}]);
//PSC
otherDetailsModule.service("otherDetailService",['ApplicationFormDataService','$q',function(ApplicationFormDataService,$q){
	this.getPscData=function(){
		var dfd = $q.defer();
		var query="select PREFERRED_LANGUAGE_CD,PREFERRED_CALL_TIME,DIGITAL_PSC_FLAG from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID=? and AGENT_CD=?";
		var parameters=[ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD];
		var pscObj={};

		db.transaction(function(tx){
			tx.executeSql(query,parameters,
			function(tx,success){
				if(success.rows.length!=0){
				debug("success fully selected PSC data"+JSON.stringify(success.rows.item(0)));
				pscObj.PREFERRED_LANGUAGE_CD=success.rows.item(0).PREFERRED_LANGUAGE_CD;
				pscObj.PREFERRED_CALL_TIME=success.rows.item(0).PREFERRED_CALL_TIME;
				pscObj.DIGITAL_PSC_FLAG=success.rows.item(0).DIGITAL_PSC_FLAG;
				dfd.resolve(pscObj);
				}
				else
					dfd.resolve(null);
			},
			function(tx,err){
				debug("fail to insert PSC data"+err.message);
				dfd.resolve(null);
			}
			)
		})
		return dfd.promise;
	};

	this.getAmlData = function(ExistingAppData, polExpossed){
		dfd = $q.defer();
		var amlObj = {};
		var resStatusCd = ExistingAppData.applicationPersonalInfo.Insured.RESIDENT_STATUS_CODE;
		var premium = ApplicationFormDataService.SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
		var riskType = resStatusCd == 'RI' ? "LR" : 'HR';
		if(!!polExpossed)
			riskType = "HR";
		//debug("resStatusCd : " + resStatusCd + "\npremium: " + premium + "\nriskType : " + riskType);
		var query="select * from LP_AML_GRID where ? between cast(min_premium as int) and cast(max_premium as int) and risk_type = ? and resi_status_code = ?";
		var parameters=[parseInt(premium),riskType,resStatusCd];
		debug("query : " + query);
		debug("parameters : " + JSON.stringify(parameters));
		db.transaction(function(tx){
			tx.executeSql(query,parameters,
			function(tx,success){
				if(success.rows.length!=0){
					debug("success fully selected AML data : " + JSON.stringify(success.rows.item(0)));
					amlObj.BANK_STMT_REQ = success.rows.item(0).BANK_STMT_REQ;
					amlObj.PROOF_OF_SOURCE_OF_INCOME_REQ = success.rows.item(0).PROOF_OF_SOURCE_OF_INCOME_REQ;
					dfd.resolve(amlObj);
				}
				else
					dfd.resolve(null);
			},
			function(tx,err){
				debug("fail to select AML data " + err.message);
				dfd.resolve(null);
			}
			)
		})
		return dfd.promise;
	};
}])
