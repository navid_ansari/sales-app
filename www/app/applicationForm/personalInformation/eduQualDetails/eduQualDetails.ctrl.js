eduQualDetailsModule.controller('EduQualDetailsCtrl',['$q','$state','$filter', 'CommonService','ApplicationFormDataService', 'PersonalInfoService','ExistingAppData', 'EduQualList', 'OccClassList', 'OrgTypeList','DesignationList', 'OccIndustryList', 'OccNatureList', 'IncomeProofList','FFNAData','persInfoService','CompanyNameList','FatcaList','LoadAppProofList','SISTermData','FHRData','OccuQuesList','OccupDocList','DeDupeData',function($q, $state, $filter, CommonService, ApplicationFormDataService, PersonalInfoService, ExistingAppData, EduQualList, OccClassList, OrgTypeList,DesignationList, OccIndustryList, OccNatureList, IncomeProofList,FFNAData,persInfoService,CompanyNameList,FatcaList,LoadAppProofList,SISTermData,FHRData,OccuQuesList,OccupDocList,DeDupeData){
debug("SIS Data : " + JSON.stringify(ApplicationFormDataService.SISFormData));
this.PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
this.disableOccClass = false;
this.disabledQue = false;
this.disabledProQue = false;

if(!!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE && !!ApplicationFormDataService.SISFormData.sisFormFData.NATIONALITY){
	this.RESIDENT_STATUS_CODE = ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE;
	this.NATIONALITY = ApplicationFormDataService.SISFormData.sisFormFData.NATIONALITY;
	this.sisDataPresent = true;
}else {
	this.RESIDENT_STATUS_CODE = null;
	this.NATIONALITY = null;
	this.sisDataPresent = true;
}

/*console.log("Occu class in controller is::"+ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE);
if(!!ApplicationFormDataService.SISFormData.sisFormFData && ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE == '2'){
    console.log("disableOccClass value is::"+JSON.stringify(this.disableOccClass));
    this.disableOccClass = true;
}
*/

this.SUM_ASSURED = ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED;
debug("this.PGL_ID : " + this.PGL_ID + "\tthis.SUM_ASSURED : " + this.SUM_ASSURED);
debug("SISTermData : " + JSON.stringify(SISTermData));
debug("ApplicationFormDataService.SISFormData : " + JSON.stringify(ApplicationFormDataService.SISFormData));

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


var isSelf = (ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y');
var AnnualPrem = ApplicationFormDataService.SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
var fatf;
if(ApplicationFormDataService.applicationFormBean.addressDetBean !=undefined){
   if(this.InsuredCode == 'IN')
      fatf = ApplicationFormDataService.applicationFormBean.addressDetBean.InsFATFFlag || 'N';
   else
      fatf = ApplicationFormDataService.applicationFormBean.addressDetBean.ProFATFFlag || 'N';
}
else
   fatf = 'N';
this.incomeCond = (((this.InsuredCode == 'PR') || isSelf) && (parseInt(AnnualPrem)>99999 || fatf == 'Y'));
this.irsCond = (this.PGL_ID == '191' || this.PGL_ID == '185' || this.PGL_ID == SR_PGL_ID || this.PGL_ID == SRP_PGL_ID);

this.isTataEmp = (ApplicationFormDataService.SISFormData.sisFormAData.TATA_FLAG=='Y') || null;

this.eduQualDetailsBean = {};
this.incomeProofBean = {};
ApplicationFormDataService.applicationFormBean.eduQualDetailsBean = this.eduQualDetailsBean;
//console.log("ExistingAppData eduQ ::"+JSON.stringify(ExistingAppData)+"in EduQualDetailsCtrl::"+ApplicationFormDataService.applicationFormBean);
ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
   Insured:{},
   Proposer:{}
},
NonReflex :{
   Insured:{},
   Proposer:{}
}

};
var eduQ = this;
eduQ.click = false;
eduQ.STAND_REQ = false;
this.numRegex = NUM_REG;
this.freeTxt = FREE_TEXT_REGEX;
this.eduQualList = EduQualList;
this.occClassList = OccClassList;
this.companyNameList = CompanyNameList;
this.orgTypeList = OrgTypeList;
this.insOccIndustryList = OccIndustryList;
this.proOccIndustryList = OccIndustryList;
this.insOccNatureList = OccNatureList;
this.proOccNatureList = OccNatureList;
this.insDesignationList = DesignationList;
this.proDesignationList = DesignationList;
this.insIncomeProofList = IncomeProofList;
this.proIncomeProofList = IncomeProofList;
this.insOccuQueList = OccuQuesList;
this.proOccuQueList = OccuQuesList;
var fiscalYearData = CommonService.getFiscalYearData();
var monthYearData = CommonService.get6MonthYearData();
this.fiscalYearList = [
	// {"YEAR_NAME": "Select ", "YEAR_VALUE":null},
	{"YEAR_NAME": fiscalYearData[0], "YEAR_VALUE":fiscalYearData[0]},
	{"YEAR_NAME": fiscalYearData[1], "YEAR_VALUE":fiscalYearData[1]},
	{"YEAR_NAME": fiscalYearData[2], "YEAR_VALUE":fiscalYearData[2]}
];
debug("fiscalYearData : " + JSON.stringify(fiscalYearData));
debug("monthYearData : " + JSON.stringify(monthYearData));
this.monthYearList = [
	// {"MONTH_YEAR_NAME": "Select ", "MONTH_YEAR_VALUE":null},
	{"MONTH_YEAR_NAME": monthYearData[0].MONTH + "-" + monthYearData[0].YEAR, "MONTH_YEAR_VALUE":monthYearData[0].MONTH + "-" + monthYearData[0].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[1].MONTH + "-" + monthYearData[1].YEAR, "MONTH_YEAR_VALUE":monthYearData[1].MONTH + "-" + monthYearData[1].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[2].MONTH + "-" + monthYearData[2].YEAR, "MONTH_YEAR_VALUE":monthYearData[2].MONTH + "-" + monthYearData[2].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[3].MONTH + "-" + monthYearData[3].YEAR, "MONTH_YEAR_VALUE":monthYearData[3].MONTH + "-" + monthYearData[3].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[4].MONTH + "-" + monthYearData[4].YEAR, "MONTH_YEAR_VALUE":monthYearData[4].MONTH + "-" + monthYearData[4].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[5].MONTH + "-" + monthYearData[5].YEAR, "MONTH_YEAR_VALUE":monthYearData[5].MONTH + "-" + monthYearData[5].YEAR},
];
this.monthYear3List = [
	// {"MONTH_YEAR_NAME": "Select ", "MONTH_YEAR_VALUE":null},
	{"MONTH_YEAR_NAME": monthYearData[0].MONTH + "-" + monthYearData[0].YEAR, "MONTH_YEAR_VALUE":monthYearData[0].MONTH + "-" + monthYearData[0].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[1].MONTH + "-" + monthYearData[1].YEAR, "MONTH_YEAR_VALUE":monthYearData[1].MONTH + "-" + monthYearData[1].YEAR},
	{"MONTH_YEAR_NAME": monthYearData[2].MONTH + "-" + monthYearData[2].YEAR, "MONTH_YEAR_VALUE":monthYearData[2].MONTH + "-" + monthYearData[2].YEAR}
];
this.InsuredCode = 'IN';
this.ProposerCode = 'PR';
this.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
this.eduQualDetailsBean.INSURED_EDUCATION_QUALIFICATION = this.eduQualList[0];
this.eduQualDetailsBean.INSURED_OCCUPATION_CLASS = this.occClassList[0];
this.eduQualDetailsBean.INSURED_COMPANY_NAME = this.companyNameList[0];
this.eduQualDetailsBean.INSURED_OCCUPATION = this.orgTypeList[0];
if(!!this.insOccIndustryList)
    this.eduQualDetailsBean.INSURED_INDUSTRY = this.insOccIndustryList[0];
    console.log("eduQualDetailsBean.INSURED_INDUSTRY::"+JSON.stringify(this.eduQualDetailsBean.INSURED_INDUSTRY));
if(!!this.insOccNatureList)
    this.eduQualDetailsBean.INSURED_WORK_NATURE = this.insOccNatureList[0];
this.eduQualDetailsBean.INSURED_INCOME_PROOF = this.insIncomeProofList[0];
// Income Proof SR & SRP
this.incomeProofBean.S_FRM16_YEAR1 = this.fiscalYearList[0];
this.incomeProofBean.S_FRM16_YEAR2 = this.fiscalYearList[1];
this.incomeProofBean.S_FRM16_YEAR3 = this.fiscalYearList[2];
this.incomeProofBean.S_BNK_MONTH_YEAR1 = this.monthYearList[0];
this.incomeProofBean.S_BNK_MONTH_YEAR2 = this.monthYearList[1];
this.incomeProofBean.S_BNK_MONTH_YEAR3 = this.monthYearList[2];
this.incomeProofBean.S_BNK_MONTH_YEAR4 = this.monthYearList[3];
this.incomeProofBean.S_BNK_MONTH_YEAR5 = this.monthYearList[4];
this.incomeProofBean.S_BNK_MONTH_YEAR6 = this.monthYearList[5];
this.incomeProofBean.S_SAL_MONTH_YEAR1 = this.monthYear3List[0];
this.incomeProofBean.S_SAL_MONTH_YEAR2 = this.monthYear3List[1];
this.incomeProofBean.S_SAL_MONTH_YEAR3 = this.monthYear3List[2];
this.incomeProofBean.S_COI_YEAR1 = this.fiscalYearList[0];
this.incomeProofBean.S_COI_YEAR2 = this.fiscalYearList[1];
//this.incomeProofBean.S_COI_YEAR3 = this.fiscalYearList[2];
this.incomeProofBean.SE_FRM16_YEAR1 = this.fiscalYearList[0];
this.incomeProofBean.SE_FRM16_YEAR2 = this.fiscalYearList[1];
this.incomeProofBean.SE_FRM16_YEAR3 = this.fiscalYearList[2];
this.incomeProofBean.SE_COI_YEAR1 = this.fiscalYearList[0];
this.incomeProofBean.SE_COI_YEAR2 = this.fiscalYearList[1];
//this.incomeProofBean.SE_COI_YEAR3 = this.fiscalYearList[2];
if(!!this.insDesignationList)
    this.eduQualDetailsBean.INSURED_DESIGNATION = this.insDesignationList[0];

this.eduQualDetailsBean.PROPOSER_EDUCATION_QUALIFICATION = this.eduQualList[0];
this.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS = this.occClassList[0];
this.eduQualDetailsBean.PROPOSER_COMPANY_NAME = this.companyNameList[0];
this.eduQualDetailsBean.PROPOSER_OCCUPATION = this.orgTypeList[0];
debug("Proposer Occu Group::"+JSON.stringify(this.eduQualDetailsBean.PROPOSER_OCCUPATION));

if(!!this.proOccIndustryList){
    this.eduQualDetailsBean.PROPOSER_INDUSTRY = this.proOccIndustryList[0];
    debug("eduQualDetailsBean.PROPOSER_INDUSTRY::"+JSON.stringify(this.eduQualDetailsBean.PROPOSER_INDUSTRY));
    }

if(!!this.proOccNatureList)
    this.eduQualDetailsBean.PROPOSER_WORK_NATURE = this.proOccNatureList[0];
this.eduQualDetailsBean.PROPOSER_INCOME_PROOF = this.proIncomeProofList[0];
if(!!this.proDesignationList)
    this.eduQualDetailsBean.PROPOSER_DESIGNATION = this.proDesignationList[0];

if(!!this.insOccuQueList)
    this.eduQualDetailsBean.INSURED_QUESTIONNAIRE = this.insOccuQueList[0];
if(!!this.proOccuQueList)
    this.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE = this.proOccuQueList[0];

debug("this.orgTypeList :"+JSON.stringify(this.orgTypeList));

this.onInsFatcaChange = function(){
    debug("FatcaList : " + JSON.stringify(FatcaList));
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.fatca = {};
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca = {};
    if(eduQ.eduQualDetailsBean.INSURED_FATCA == "Y"){
        navigator.notification.alert('FATCA & CRS-Self Certification Form to be mandatorily completed',function(){CommonService.hideLoading();},'Message','Ok');
        if(FatcaList.Insured.isDigital == "Y"){
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.fatca.DOC_ID = FatcaList.Insured.fatca;
         }
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca.DOC_ID = FatcaList.Insured.fatca;
    }else{
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.fatca = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.fatca = null;
    }
}
this.onProFatcaChange = function(){
    debug("FatcaList : " + JSON.stringify(FatcaList));
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.fatca = {};
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca = {};
    if(eduQ.eduQualDetailsBean.PROPOSER_FATCA == "Y"){
        navigator.notification.alert('FATCA & CRS-Self Certification Form to be mandatorily completed',function(){CommonService.hideLoading();},'Message','Ok');
        if(FatcaList.Proposer.isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.fatca.DOC_ID = FatcaList.Proposer.fatca;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca.DOC_ID = FatcaList.Proposer.fatca;
    }else{
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.fatca = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.fatca = null;
    }
}
//insurance occ changes
this.onInsOccClassChange = function(){
	console.log("onInsOccClassChange :"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS));
	var TERM_FLAG = (eduQ.PGL_ID == SR_PGL_ID || eduQ.PGL_ID == SRP_PGL_ID) ? 'Y' : 'N';
	debug("eduQ.PGL_ID : " + eduQ.PGL_ID + "\tTERM_FLAG : " + TERM_FLAG);
	if(!!eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS || TERM_FLAG=='Y'){
	    if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS != undefined){
    		LoadAppProofList.getIncomeProofList("IN",eduQ.STAND_REQ,eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS, TERM_FLAG).then(
    			function(IncomeProofList){
    				eduQ.insIncomeProofList = IncomeProofList;
    				console.log("eduQ.insIncomeProofList::"+JSON.stringify(IncomeProofList));
    				if(!!eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID && TERM_FLAG=='Y'){

    				}else {
    					eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF = IncomeProofList[0];
    				}
    				console.log("eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF::"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF));
    			}
    		);
    	}else{
    	    LoadAppProofList.getIncomeProofList().then(
    	        function(IncomeProofList){
                    eduQ.insIncomeProofList = IncomeProofList;
                    console.log("eduQ.insIncomeProofList else::"+JSON.stringify(IncomeProofList));
                    if(!!eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID && TERM_FLAG=='Y'){

                    }else {
                        eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF = IncomeProofList[0];
                    }
                    console.log("eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF else::"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF));
                }
    	    );
    	}
    }
   if(!!eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS){
        LoadAppProofList.getDesignationList(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE).then(
            function(DesignationList){
                eduQ.insDesignationList = DesignationList;
                console.log("eduQ.designationList::"+JSON.stringify(DesignationList));
                if(!!eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID && TERM_FLAG=='Y')
                {/*Skip for SR on load*/}
                else
                    eduQ.eduQualDetailsBean.INSURED_DESIGNATION = DesignationList[0];
            }
        );
   }
   if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS != undefined){
       if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPPROOF_CODE != undefined)
          eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPPROOF_CODE,'IN','class',TERM_FLAG=='Y');
       else
          {
             ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuclass = null;
             ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuclass = null;
          }
   //}

    //Added for Student ID Record
   /*ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec = {};
   if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '6' || (parseInt(Age) > 5 && parseInt(Age) < 18))
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec.DOC_ID = StudentIDList.Insured.studIDRec;

    else
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec = null;*/

	if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE != '1'){
		eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME = eduQ.companyNameList[0];
	}
	/*eduQ.eduQualForm.ocmpanyName.$validate();
	eduQ.eduQualForm.orgType.$validate();
	eduQ.eduQualForm.industry.$validate();
	eduQ.eduQualForm.nature.$validate();
	eduQ.eduQualForm.empName.$validate();
	eduQ.eduQualForm.empAddr.$validate();*/


	if(!!eduQ.eduQualForm && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '1' && !eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME.COMPANY_CODE){
		eduQ.eduQualForm.ocmpanyName.$setValidity('selectRequired', false);
	}
	if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE != '1' && eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME.COMPANY_CODE!=undefined){
    		eduQ.eduQualForm.ocmpanyName.$setValidity('selectRequired', true);
    }
    if(!!eduQ.eduQualForm && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '8'){
        eduQ.eduQualForm.ocmpanyName.$setValidity('selectRequired', true);
        eduQ.eduQualForm.nature.$setValidity('orgTypeRequired', true);
    }
	if(!!eduQ.eduQualForm && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '4' || eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '6')
		{
			eduQ.eduQualForm.orgType.$setValidity('orgTypeRequired', true);
			eduQ.eduQualForm.industry.$setValidity('orgTypeRequired', true);
			eduQ.eduQualForm.nature.$setValidity('orgTypeRequired', true);
			if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE == '4')
			{
				eduQ.eduQualForm.empName.$setValidity('required', true);
				eduQ.eduQualForm.empAddr.$setValidity('required', true);
			}
			else
			{
				eduQ.eduQualForm.empName.$setValidity('required', false);
				eduQ.eduQualForm.empAddr.$setValidity('required', false);
			}
		}
		else if(!!eduQ.eduQualForm)
		{
			console.log("1 - OCC:"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_OCCUPATION));
			if(eduQ.eduQualDetailsBean.INSURED_OCCUPATION.OCCU_GROUP == undefined || eduQ.eduQualDetailsBean.INSURED_OCCUPATION.OCCU_GROUP=="Select ")
				eduQ.eduQualForm.orgType.$setValidity('orgTypeRequired', false);
			if(eduQ.eduQualDetailsBean.INSURED_INDUSTRY.APP_INDUSTRY == undefined || eduQ.eduQualDetailsBean.INSURED_INDUSTRY.APP_INDUSTRY=="Select Industry")
				eduQ.eduQualForm.industry.$setValidity('orgTypeRequired', false);
			if(eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.OCCUPATION_CODE  == undefined || eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.OCCUPATION_CODE =="Select Nature Of Work")
				eduQ.eduQualForm.nature.$setValidity('orgTypeRequired', false);
		}
	debug(JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
	}
}

this.onInsCompanyNameChange = function(){
   debug("onInsCompanyNameChange : " + eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME.COMPANY_CODE);
   if(eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME.COMPANY_CODE != "WZ99"){
      eduQ.eduQualDetailsBean.INSURED_EMPLOYER_NAME = null;
   }
}
this.onProCompanyNameChange = function(){
   debug("onProCompanyNameChange : " + eduQ.eduQualDetailsBean.PROPOSER_COMPANY_NAME.COMPANY_CODE);
   if(eduQ.eduQualDetailsBean.PROPOSER_COMPANY_NAME.COMPANY_CODE != "WZ99"){
      eduQ.eduQualDetailsBean.PROPOSER_EMPLOYER_NAME = null;
   }
}
this.onProOccClassChange = function(){
console.log("onProOccClassChange :"+JSON.stringify(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS));

    if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS!=undefined){
       LoadAppProofList.getIncomeProofList("PR",eduQ.STAND_REQ,eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS,eduQ.STAND_REQ).then(
          function(IncomeProofList){
             eduQ.proIncomeProofList = IncomeProofList;
             console.log("eduQ.proIncomeProofList::"+JSON.stringify(IncomeProofList));
             eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF = IncomeProofList[0];
             console.log("eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF::"+JSON.stringify(eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF));
          }
       );
    }
    if(!!eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS){
        LoadAppProofList.getDesignationList(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE,eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS).then(
            function(DesignationList){
                eduQ.proDesignationList = DesignationList;
                console.log("eduQ.designationList::"+JSON.stringify(DesignationList));
                eduQ.eduQualDetailsBean.PROPOSER_DESIGNATION = DesignationList[0];
            }
        );
   }
if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPPROOF_CODE != undefined)
   eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPPROOF_CODE,'PR','class');
else
	{
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuclass = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuclass = null;
	}

			/*if(eduQ.isSelf == false){
				eduQ.eduQualForm.PropCmpanyName.$validate();
				eduQ.eduQualForm.proOrgType.$validate();
				eduQ.eduQualForm.proIndustry.$validate();
				eduQ.eduQualForm.proNatureOfWork.$validate();
				eduQ.eduQualForm.proEmpName.$validate();
				eduQ.eduQualForm.proEmpAddr.$validate();
			}*/
if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE == '1' && eduQ.eduQualDetailsBean.PROPOSER_COMPANY_NAME.COMPANY_CODE==undefined){
    eduQ.eduQualForm.PropCmpanyName.$setValidity('selectRequired', false);
}
if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE != '1'){
			eduQ.eduQualForm.PropCmpanyName.$setValidity('selectRequired', true);
	}
if(!!eduQ.eduQualForm && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE == '8')
    {
        eduQ.eduQualForm.ocmpanyName.$setValidity('selectRequired', true);
        eduQ.eduQualForm.nature.$setValidity('orgTypeRequired', true);
    }
if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE == '4' || eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE == '6')
    {
        eduQ.eduQualForm.proOrgType.$setValidity('selectRequired', true);
        eduQ.eduQualForm.proIndustry.$setValidity('selectRequired', true);
        eduQ.eduQualForm.proNatureOfWork.$setValidity('selectRequired', true);
        if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE == '4')
        {
           eduQ.eduQualForm.proEmpName.$setValidity('required', true);
            eduQ.eduQualForm.proEmpAddr.$setValidity('required', true);
        }
        else
        {
           eduQ.eduQualForm.proEmpName.$setValidity('required', false);
            eduQ.eduQualForm.proEmpAddr.$setValidity('required', false);
        }
    }
    else
    {
			if(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.OCCU_GROUP == undefined || eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.OCCU_GROUP == "Select ")
				eduQ.eduQualForm.proOrgType.$setValidity('selectRequired', false);
			if(eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY.APP_INDUSTRY == undefined || eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY.APP_INDUSTRY == "Select Industry")
				eduQ.eduQualForm.proIndustry.$setValidity('selectRequired', false);
			if(eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.OCCUPATION_CODE  == undefined || eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.OCCUPATION_CODE  == "Select Nature Of Work")
				eduQ.eduQualForm.proNatureOfWork.$setValidity('selectRequired', false);
    }

}

this.onInsChangeOccup = function(dntShowMsg){
debug("eduQ.eduQualDetailsBean.INSURED_OCCUPATION :"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_OCCUPATION));
if(!!eduQ.eduQualDetailsBean && !!eduQ.eduQualDetailsBean.INSURED_OCCUPATION && eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPPROOF_CODE != undefined)
   eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPPROOF_CODE,'IN','org', dntShowMsg);
else
   {
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.orgtype = null;
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.orgtype = null;
      //ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = null;
        //ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;

   }
}
this.onProChangeOccup = function(){

if(!!eduQ.eduQualDetailsBean && !!eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPPROOF_CODE != undefined)
   eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPPROOF_CODE,'PR','org');
else
   {
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.orgtype = null;
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.orgtype = null;
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = null;
      ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;
   }
}

this.onInsIndustryChng = function(IndustrySelect,setExisting){
var dfd = $q.defer();
var Ind;
if(!!eduQ.eduQualDetailsBean.INSURED_INDUSTRY)
Ind = eduQ.eduQualDetailsBean.INSURED_INDUSTRY.APP_INDUSTRY;
eduQ.setExisting = null;

if(!!setExisting)
    eduQ.setExisting = setExisting;

if(!!IndustrySelect)
    Ind = IndustrySelect;

if(!!eduQ.eduQualDetailsBean.INSURED_INDUSTRY){
debug("Ind ::"+Ind);
    LoadAppProofList.getOccNatureList(Ind,setExisting).then(
        function(occNatureList){
            eduQ.insOccNatureList = occNatureList;
            console.log(JSON.stringify(occNatureList[0])+"eduQ.insOccNatureList::"+JSON.stringify(occNatureList));
            if(setExisting == true)
            {/*skip default set value*/}
            else
                eduQ.eduQualDetailsBean.INSURED_WORK_NATURE = occNatureList[0];
            dfd.resolve(occNatureList);
            }
        );
   }
   return dfd.promise;
}

this.onProIndustryChng = function(IndustrySelect,setExisting){
var dfd = $q.defer();
var Ind;
eduQ.ProsetExisting = null;

if(!!setExisting)
eduQ.ProsetExisting = setExisting;

if(!!eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY)
Ind = eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY.APP_INDUSTRY;

if(!!IndustrySelect)
    Ind = IndustrySelect;

if(!!eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY){
    LoadAppProofList.getOccNatureList(Ind,setExisting).then(
        function(occNatureList){
            eduQ.proOccNatureList = occNatureList;
            console.log(occNatureList[0]+"eduQ.proOccNatureList::"+JSON.stringify(occNatureList));
            if(setExisting == true)
            {/*skip default set value*/}
            else
                eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE = occNatureList[0];
            dfd.resolve(occNatureList);
            }
        );
   }
   return dfd.promise;
}

this.onInsDesignation = function(){
debug("eduQ.eduQualDetailsBean.INSURED_DESIGNATION :"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_DESIGNATION));
}

this.onInsChangeNature = function(){
var dfd = $q.defer();
if(eduQ.setExisting == true && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE!=undefined)
{/*skip default set value*/
    eduQ.setExisting = null;
}
else{
        console.log("Insured MPDOC Code::"+eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE+"Insured MPPROOF CODE::"+eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE);
        if(!!eduQ.eduQualDetailsBean.INSURED_WORK_NATURE && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE != undefined)
            {
                navigator.notification.alert("You will not be allowed to submit the application without providing the Questionnaire",function(){CommonService.hideLoading();},"Application","Ok");
                eduQ.disabledQue = true;
            }
        else
           {
                eduQ.disabledQue = false;
                eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE = OccuQuesList[0];
                debug("eduQ.disabledQue : "+eduQ.disabledQue);
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nature = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nature = null;
           }

       if(!!eduQ.eduQualDetailsBean.INSURED_WORK_NATURE && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE != undefined){
            LoadAppProofList.getOccuQuesionnarie(eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.APP_SNO).then(
                function(OccuQuesList){
                    eduQ.insOccuQueList = OccuQuesList;
                    console.log(JSON.stringify(OccuQuesList[0])+"eduQ.insOccuQueList::"+JSON.stringify(OccuQuesList));
                    eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE = OccuQuesList[0];
                    dfd.resolve(OccuQuesList);
                }
            );
            return dfd.promise;
       }
   }
}

this.onProChangeNature = function(){
var dfd = $q.defer();
if(eduQ.ProsetExisting == true && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPPROOF_CODE != undefined)
{/*skip default set value*/
    eduQ.ProsetExisting = null;
}
else{
        if(!!eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPPROOF_CODE != undefined)
            {
                navigator.notification.alert("You will not be allowed to submit the application without providing the Questionnaire",function(){CommonService.hideLoading();},"Application","Ok");
                eduQ.disabledProQue = true;
            }

        else
           {
                eduQ.disabledProQue = false;
                eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE = OccuQuesList[0];
                debug("eduQ.disabledQue : "+eduQ.disabledQue);
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nature = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nature = null;
           }

       if(!!eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPPROOF_CODE != undefined){
            LoadAppProofList.getOccuQuesionnarie(eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.APP_SNO).then(
                function(OccuQuesList){
                    eduQ.proOccuQueList = OccuQuesList;
                    console.log(JSON.stringify(OccuQuesList[0])+"eduQ.proOccuQueList::"+JSON.stringify(OccuQuesList));
                    eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE = OccuQuesList[0];
                    dfd.resolve(OccuQuesList);
                }
            );
            return dfd.promise;
       }
   }
}

this.onInschngQues = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = {};

    if(!!eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE && eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE.MPDOC_CODE!=undefined && eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE.MPPROOF_CODE!=undefined)
    {
        eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE.MPPROOF_CODE,'IN','nature');
    }
    else
    {
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;
    }
}

this.onPropchngQues = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = {};

    if(!!eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE && eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE.MPDOC_CODE!=undefined && eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE.MPPROOF_CODE!=undefined)
    {
        eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE.MPPROOF_CODE,'PR','nature');
    }
    else
    {
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;
    }
}

this.onInsChangeIncProof = function(dntShowMsg){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.incproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof = {};

if(!!eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF && eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.isDigital == "Y" && eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.isDigital!=null)
   ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.incproof.DOC_ID = eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID;
else
   ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.DOC_ID = eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID;
if(!!dntShowMsg){
	/*Skip msg*/
}
else
	navigator.notification.alert("You will not be allowed to submit the application without uploading the respective income proof.",null,"Application","OK");
	if(eduQ.PGL_ID==SR_PGL_ID || eduQ.PGL_ID==SRP_PGL_ID){
		eduQ.incomeProofBean.S_FRM16_SAL1 = null;
		eduQ.incomeProofBean.S_FRM16_SAL2 = null;
		eduQ.incomeProofBean.S_FRM16_SAL3 = null;
		eduQ.incomeProofBean.S_EMP_ISSDT = null;
		eduQ.incomeProofBean.S_EMP_SAL = null;
		eduQ.incomeProofBean.S_BNK_SAL1 = null;
		eduQ.incomeProofBean.S_BNK_SAL2 = null;
		eduQ.incomeProofBean.S_BNK_SAL3 = null;
		eduQ.incomeProofBean.S_BNK_SAL4 = null;
		eduQ.incomeProofBean.S_BNK_SAL5 = null;
		eduQ.incomeProofBean.S_BNK_SAL6 = null;
		eduQ.incomeProofBean.S_SAL_SAL1 = null;
		eduQ.incomeProofBean.S_SAL_SAL2 = null;
		eduQ.incomeProofBean.S_SAL_SAL3 = null;
		eduQ.incomeProofBean.S_COI_SAL1 = null;
		eduQ.incomeProofBean.S_COI_SAL2 = null;
		//eduQ.incomeProofBean.S_COI_SAL3 = null;
		eduQ.incomeProofBean.SE_FRM16_SAL1 = null;
		eduQ.incomeProofBean.SE_FRM16_SAL2 = null;
		eduQ.incomeProofBean.SE_FRM16_SAL3 = null;
		eduQ.incomeProofBean.SE_COI_PROFIT1 = null;
		eduQ.incomeProofBean.SE_COI_PROFIT2 = null;
		//eduQ.incomeProofBean.SE_COI_PROFIT3 = null;
		eduQ.incomeProofBean.SE_COI_SAL1 = null;
		eduQ.incomeProofBean.SE_COI_SAL2 = null;
		//eduQ.incomeProofBean.SE_COI_SAL3 = null;
		eduQ.incomeProofBean.SE_OTH_DOC = null;
		eduQ.incomeProofBean.SE_OTH_PROFIT = null;
	}
}

this.onProChangeIncProof = function(){
console.log("inside onProChangeIncProof");
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.incproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof = {};

if(!!eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF && eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF.isDigital =="Y" && eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF.isDigital!=null)
   ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.incproof.DOC_ID = eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF.DOC_ID;
else
   ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof.DOC_ID = eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF.DOC_ID;
navigator.notification.alert("You will not be allowed to submit the application without uploading the respective income proof.",function(){CommonService.hideLoading();},"Application","OK");
}

this.showQuestionnaireMessage = function(MPDOC_CODE, MPPROOF_CODE, CUST_TYPE, type, dntShowMsg){
var dfd = $q.defer();
var ct = "Insured";
if(CUST_TYPE == 'IN')
   ct = "Insured";
else
   ct = "Proposer";
      var whereClauseObj = {};
         whereClauseObj.MPDOC_CODE = MPDOC_CODE;
         whereClauseObj.MPPROOF_CODE = MPPROOF_CODE;
         whereClauseObj.CUSTOMER_CATEGORY = CUST_TYPE;
         debug("MPDOC_CODE :"+MPDOC_CODE+" : MPPROOF_CODE:"+MPPROOF_CODE);
CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",true,"MPPROOF_DESC,DOC_ID,DIGITAL_HTML_FLAG",whereClauseObj, "").then(
         function(res){

            if(!!res && res.rows.length>0){
            console.log("showQuestionnaireMessage : LP_DOC_PROOF_MASTER :" + JSON.stringify(res.rows.item(0)));
            var r = res.rows.item(0);
            if(MPDOC_CODE == 'OQ' && MPPROOF_CODE == 'OC'){
                                debug(ct+"GOT OC :OQ"+r.DIGITAL_HTML_FLAG);
                                if(r.DIGITAL_HTML_FLAG == "Y")
                                {
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuQuest = {};
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuQuest = null;
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuQuest.DOC_ID = r.DOC_ID;


                                }
                                else
                                {
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuQuest = {};
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuQuest = null;
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuQuest.DOC_ID = r.DOC_ID;

                                }
                                if(type == 'class')
                                {
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuclass = null;
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuclass = null;
                                }
                                if(type == 'org')
                                {
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].orgtype = null;
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].orgtype = null;
                                }
                                if(type == 'nature')
                                {
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].nature = null;
                                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].nature = null;
                                }

            }else{
                  if(type == 'class')
                     {  if(r.DIGITAL_HTML_FLAG == "Y")
                        {
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuclass = {};
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuclass = null
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuclass.DOC_ID = r.DOC_ID;
                        }
                        else
                        {
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].occuclass = null;
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuclass = {};
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].occuclass.DOC_ID = r.DOC_ID;
                        }
                     }
                  if(type == 'org')
                  {
                      if(r.DIGITAL_HTML_FLAG == "Y")
                      {
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].orgtype = {};
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].orgtype = null;
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].orgtype.DOC_ID = r.DOC_ID;
                      }
                      else
                      {
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].orgtype = {};
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].orgtype = null;
                         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].orgtype.DOC_ID = r.DOC_ID;
                      }
                  }
                  if(type == 'nature')
                  {
                        debug("inside nature :"+r.DOC_ID);
                        if(r.DIGITAL_HTML_FLAG == "Y")
                        {
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].nature = {};
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].nature = null;
                           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].nature.DOC_ID = r.DOC_ID;
                        }
                        else
                        {
                            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex[ct].nature = null;
                            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].nature = {};
                            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[ct].nature.DOC_ID = r.DOC_ID;
                        }
                  }
            }
               if(!!dntShowMsg){
                  dfd.resolve("S");
               }
               else{
                  navigator.notification.alert("You will not be allowed to submit the application without uploading the " + res.rows.item(0).MPPROOF_DESC + " Questionnaire", function(){
					  CommonService.hideLoading();
                     dfd.resolve("S");
				 }, "Occupation Details", "OK");
               }
            }
            else{
               dfd.resolve("S");
            }
         }
      );
      return dfd.promise;
}

this.setExistingEduQualData = function(){
var dfd = $q.defer();
var existingData = ExistingAppData.applicationPersonalInfo;
var existingIncomeProofData = ApplicationFormDataService.SISFormData.sisFormFData;
persInfoService.setActiveTab("eduQualDetails");
if(ExistingAppData!=undefined && ExistingAppData.applicationMainData!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.applicationPersonalInfo.Insured).length!=0)
{
console.log("inside setExistingEduQualData " + existingData.Insured.COMPANY_CODE);
//Insured

ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
//console.log("Flag is::"+eduQ.STAND_REQ+"IncomeProofFlag::"+ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG);
if(isSelf)
{
   if(!!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured && !!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof && !!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG && ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG=='Y')
      eduQ.STAND_REQ = true;
   else
      eduQ.STAND_REQ = false;
}
else
{
   if(!!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer && !!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof && !!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof.FLAG && ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof.FLAG=='Y')
         eduQ.STAND_REQ = true;
      else
         eduQ.STAND_REQ = false;
}

eduQ.eduQualDetailsBean.INSURED_FATCA = existingData.Insured.FATCA;
eduQ.eduQualDetailsBean.INSURED_EDUCATION_QUALIFICATION = $filter('filter')(eduQ.eduQualList, {"NBFE_CODE": existingData.Insured.EDUCATION_QUALIFICATION_CODE}, true)[0];
eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": existingData.Insured.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
eduQ.eduQualDetailsBean.INSURED_COMPANY_NAME = $filter('filter')(eduQ.companyNameList, {"COMPANY_CODE": existingData.Insured.COMPANY_CODE}, true)[0];
if(!!existingData.Insured.OCCUPATION)
   eduQ.eduQualDetailsBean.INSURED_OCCUPATION = $filter('filter')(eduQ.orgTypeList, {"OCCU_GROUP": existingData.Insured.OCCUPATION}, true)[0];
console.log("Occ Ind is::"+existingData.Insured.OCCUPATION_INDUSTRY);
if(existingData.Insured.OCCUPATION_INDUSTRY !=undefined && existingData.Insured.OCCUPATION_INDUSTRY!="")
   {
	   CommonService.showLoading();
        eduQ.eduQualDetailsBean.INSURED_INDUSTRY = $filter('filter')(eduQ.insOccIndustryList, {"APP_INDUSTRY": existingData.Insured.OCCUPATION_INDUSTRY}, true)[0];
        console.log("value of Industry set as::"+eduQ.eduQualDetailsBean.INSURED_INDUSTRY);
            eduQ.onInsIndustryChng(existingData.Insured.OCCUPATION_INDUSTRY,true).then(function(insOccu){
                 if(existingData.Insured.OCCU_WORK_NATURE!=undefined && existingData.Insured.OCCU_WORK_NATURE!=""){
                    eduQ.eduQualDetailsBean.INSURED_WORK_NATURE = $filter('filter')(eduQ.insOccNatureList, {"APP_NATURE_OF_WORK": existingData.Insured.OCCU_WORK_NATURE}, true)[0];
					CommonService.hideLoading();
				}
				else{
					CommonService.hideLoading();
				}
                });
   }
   else{
	   CommonService.hideLoading();
   }


if(!!existingData.Insured.OCCU_QUES_SNO)
    eduQ.eduQualDetailsBean.INSURED_QUESTIONNAIRE = $filter('filter')(eduQ.insOccuQueList,{"SNO":existingData.Insured.OCCU_QUES_SNO},true)[0];

eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF = $filter('filter')(eduQ.insIncomeProofList, {"DOC_ID": existingData.Insured.INCOME_PROOF_DOC_ID}, true)[0];
if(existingData.Insured.DESIGNATION!=undefined && existingData.Insured.DESIGNATION!="")
   eduQ.eduQualDetailsBean.INSURED_DESIGNATION = $filter('filter')(eduQ.insDesignationList, {"DESIGNATION": existingData.Insured.DESIGNATION}, true)[0];
console.log("Designation is::::"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_DESIGNATION));
/*LoadAppProofList.getIncomeProofList("IN",eduQ.STAND_REQ,eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.OCCU_CLASS).then(
			function(IncomeProofList){
				eduQ.insIncomeProofList = IncomeProofList;
				console.log("eduQ.insIncomeProofList after coming back::"+JSON.stringify(IncomeProofList));
				eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF = IncomeProofList[0];
				console.log("eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF::"+JSON.stringify(eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF));
			}
		);*/
//Proposer
if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
eduQ.eduQualDetailsBean.PROPOSER_EDUCATION_QUALIFICATION = $filter('filter')(eduQ.eduQualList, {"NBFE_CODE": existingData.Proposer.EDUCATION_QUALIFICATION_CODE}, true)[0];
eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": existingData.Proposer.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
eduQ.eduQualDetailsBean.PROPOSER_COMPANY_NAME = $filter('filter')(eduQ.companyNameList, {"COMPANY_CODE": existingData.Proposer.COMPANY_CODE}, true)[0];
if(existingData.Proposer.OCCUPATION !=undefined && existingData.Proposer.OCCUPATION!="")
   eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION = $filter('filter')(eduQ.orgTypeList, {"OCCU_GROUP": existingData.Proposer.OCCUPATION}, true)[0];
if(existingData.Proposer.OCCUPATION_INDUSTRY !=undefined && existingData.Proposer.OCCUPATION_INDUSTRY!="")
   {
	   	CommonService.showLoading();
        eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY = $filter('filter')(eduQ.proOccIndustryList, {"APP_INDUSTRY": existingData.Proposer.OCCUPATION_INDUSTRY}, true)[0];
        eduQ.onProIndustryChng(existingData.Proposer.OCCUPATION_INDUSTRY, true).then(function(proOccu){
                         if(existingData.Proposer.OCCU_WORK_NATURE!=undefined && existingData.Proposer.OCCU_WORK_NATURE!=""){
                            eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE = $filter('filter')(eduQ.proOccNatureList, {"APP_NATURE_OF_WORK": existingData.Proposer.OCCU_WORK_NATURE}, true)[0];
							CommonService.hideLoading();
						}
						else{
							CommonService.hideLoading();
						}
                        });
   }
   else{
	   CommonService.hideLoading();
   }
if(existingData.Proposer.OCCU_WORK_NATURE!=undefined && existingData.Proposer.OCCU_WORK_NATURE!="")
    eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE = $filter('filter')(eduQ.proOccNatureList, {"APP_NATURE_OF_WORK": existingData.Proposer.OCCU_WORK_NATURE}, true)[0];

if(!!existingData.Proposer.OCCU_QUES_SNO)
    eduQ.eduQualDetailsBean.PROPOSER_QUESTIONNAIRE = $filter('filter')(eduQ.proOccuQueList,{"SNO":existingData.Proposer.OCCU_QUES_SNO},true)[0];

    eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF = $filter('filter')(eduQ.proIncomeProofList, {"DOC_ID": existingData.Proposer.INCOME_PROOF_DOC_ID}, true)[0];
if(existingData.Proposer.DESIGNATION!=undefined && existingData.Proposer.DESIGNATION!="")
   eduQ.eduQualDetailsBean.PROPOSER_DESIGNATION = $filter('filter')(eduQ.proDesignationList, {"DESIGNATION": existingData.Proposer.DESIGNATION}, true)[0];
/*LoadAppProofList.getIncomeProofList("PR",eduQ.STAND_REQ,eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.OCCU_CLASS,eduQ.STAND_REQ).then(
		function(IncomeProofList){
			eduQ.proIncomeProofList = IncomeProofList;
			console.log("eduQ.proIncomeProofList::"+JSON.stringify(IncomeProofList));
			eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF = IncomeProofList[0];
			console.log("eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF::"+JSON.stringify(eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF));
		}
	);*/
}

      if(existingData.Insured.EDUCATION_QUALIFICATION == 'Others')
         eduQ.eduQualDetailsBean.INSURED_OTHER_EDUCATION_DETS = existingData.Insured.OTHER_EDUCATION_DETS;

      if(existingData.Insured.OCCUPATION_CLASS == 'Others')
         eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS_OTHER = existingData.Insured.OCCUPATION_CLASS_OTHER;
      if(existingData.Insured.OCCUPATION_CLASS == 'Student')
         eduQ.eduQualDetailsBean.INSURED_STUDENT_STANDARD = existingData.Insured.STUDENT_STANDARD;
      eduQ.eduQualDetailsBean.INSURED_EMPLOYER_NAME = existingData.Insured.EMPLOYER_SCH_BUSI_NAME;
      eduQ.eduQualDetailsBean.INSURED_EMPLOYER_ADDRESS = existingData.Insured.EMPLOYER_SCH_BUSI_ADDRESS;

      if(existingData.Insured.OCCUPATION == 'Others')
         eduQ.eduQualDetailsBean.INSURED_OCCUPATION_OTHER = existingData.Insured.OCCUPATION_OTHERS;

      eduQ.eduQualDetailsBean.INSURED_INDUSTRY_OTHER = existingData.Insured.OCCUP_INDUSTRY_OTHERS;
      eduQ.eduQualDetailsBean.INSURED_WORK_NATURE_OTHER = existingData.Insured.OCCU_WORK_NATURE_OTHERS;
      if(!!existingData.Insured.ANNUAL_INCOME)
         eduQ.eduQualDetailsBean.INSURED_ANNUAL_INCOME = parseInt(existingData.Insured.ANNUAL_INCOME);
      if(existingData.Insured.INCOME_PROOF == 'Others')
         eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF_OTHER = existingData.Insured.INCOME_PROOF_OTHER;
      if(existingData.Insured.DESIGNATION == 'Others')
         eduQ.eduQualDetailsBean.INSURED_DESIGNATION_OTHER = existingData.Insured.DESIGNATION_OTHER;

      //Proposer
      if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
      eduQ.eduQualDetailsBean.PROPOSER_FATCA = existingData.Proposer.FATCA;
      if(existingData.Proposer.EDUCATION_QUALIFICATION == 'Others')
                 eduQ.eduQualDetailsBean.PROPOSER_OTHER_EDUCATION_DETS = existingData.Proposer.OTHER_EDUCATION_DETS;

              if(existingData.Proposer.OCCUPATION_CLASS == 'Others')
                 eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS_OTHER = existingData.Proposer.OCCUPATION_CLASS_OTHER;
              if(existingData.Proposer.OCCUPATION_CLASS == 'Student')
                 eduQ.eduQualDetailsBean.PROPOSER_STUDENT_STANDARD = existingData.Proposer.STUDENT_STANDARD;
              eduQ.eduQualDetailsBean.PROPOSER_EMPLOYER_NAME = existingData.Proposer.EMPLOYER_SCH_BUSI_NAME;
              eduQ.eduQualDetailsBean.PROPOSER_EMPLOYER_ADDRESS = existingData.Proposer.EMPLOYER_SCH_BUSI_ADDRESS;

              if(existingData.Proposer.OCCUPATION == 'Others')
                 eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_OTHER = existingData.Proposer.OCCUPATION_OTHERS;

              eduQ.eduQualDetailsBean.PROPOSER_INDUSTRY_OTHER = existingData.Proposer.OCCUP_INDUSTRY_OTHERS;
              eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE_OTHER = existingData.Proposer.OCCU_WORK_NATURE_OTHERS;
              if(!!existingData.Proposer.ANNUAL_INCOME)
                 eduQ.eduQualDetailsBean.PROPOSER_ANNUAL_INCOME = parseInt(existingData.Proposer.ANNUAL_INCOME);
              if(existingData.Proposer.INCOME_PROOF == 'Others')
                 eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF_OTHER = existingData.Proposer.INCOME_PROOF_OTHER;
              if(existingData.Proposer.DESIGNATION == 'Others')
                 eduQ.eduQualDetailsBean.PROPOSER_DESIGNATION_OTHER = existingData.Proposer.DESIGNATION_OTHER;
   }
   dfd.resolve(eduQ.eduQualDetailsBean);
   }

   if(!existingData.Insured.ANNUAL_INCOME || existingData.Insured.ANNUAL_INCOME == "")
   {  //Loading FFN data

      if(!!FFNAData && !!FFNAData.FHRMain && !!FFNAData.FFNA && !!FFNAData.FFNA.FHR_ID)
      {  if(!!FFNAData.FFNA.ANNUAL_INCOME && FFNAData.FFNA.ANNUAL_INCOME!="" && eduQ.isSelf)
            eduQ.eduQualDetailsBean.INSURED_ANNUAL_INCOME = parseInt(FFNAData.FFNA.ANNUAL_INCOME);
         else if(!!FFNAData.FFNA.ANNUAL_INCOME && FFNAData.FFNA.ANNUAL_INCOME!="" && eduQ.isSelf == false)
            eduQ.eduQualDetailsBean.PROPOSER_ANNUAL_INCOME = parseInt(FFNAData.FFNA.ANNUAL_INCOME);
      }
	  if(!!FHRData && !!FHRData.FFHR && !!FHRData.FFHR.FHR_ID)
	  {
		  if(!!FHRData.FFHR.ANN_INCOME_TOTAL && FHRData.FFHR.ANN_INCOME_TOTAL!="" && eduQ.isSelf)
	            eduQ.eduQualDetailsBean.INSURED_ANNUAL_INCOME = parseInt(FHRData.FFHR.ANN_INCOME_TOTAL);
	         else if(!!FHRData.FFHR.ANN_INCOME_TOTAL && FHRData.FFHR.ANN_INCOME_TOTAL!="" && eduQ.isSelf == false)
	            eduQ.eduQualDetailsBean.PROPOSER_ANNUAL_INCOME = parseInt(FHRData.FFHR.ANN_INCOME_TOTAL);
	  }


	}
	if(!existingData.Insured.EDUCATION_QUALIFICATION_CODE || existingData.Insured.EDUCATION_QUALIFICATION_CODE == "")
	{
		if(!!SISTermData && !!SISTermData.OPPORTUNITY_ID)
		{
			eduQ.eduQualDetailsBean.INSURED_EDUCATION_QUALIFICATION = $filter('filter')(eduQ.eduQualList, {"NBFE_CODE": SISTermData.EDUCATION_QUALIFICATION_CODE}, true)[0];
			eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": SISTermData.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
		}
	}
	/* For SR & SRP set Occ Class*/
	if(!existingData.Insured.OCCUPATION_CLASS_NBFE_CODE){
		if((ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SR_PGL_ID ||ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SRP_PGL_ID) && (!!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE) && (!!ApplicationFormDataService.SISFormData.sisFormFData && ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE != '2')){
			eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
			eduQ.disableOccClass = true;
			eduQ.onInsOccClassChange();
		}
	}else {
		if((ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SR_PGL_ID ||ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SRP_PGL_ID) && (!!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE)){
			eduQ.disableOccClass = true;
			eduQ.onInsOccClassChange();
		}
	}
	debug("existingIncomeProofData : " + JSON.stringify(existingIncomeProofData));
	if(!!existingIncomeProofData){
		if(!!existingIncomeProofData.S_FRM16_SAL1 || existingIncomeProofData.S_FRM16_SAL1==0)
			eduQ.incomeProofBean.S_FRM16_SAL1 = parseInt(existingIncomeProofData.S_FRM16_SAL1);
		if(!!existingIncomeProofData.S_FRM16_SAL2 || existingIncomeProofData.S_FRM16_SAL2==0)
			eduQ.incomeProofBean.S_FRM16_SAL2 = parseInt(existingIncomeProofData.S_FRM16_SAL2);
		if(!!existingIncomeProofData.S_FRM16_SAL3 || existingIncomeProofData.S_FRM16_SAL3==0)
			eduQ.incomeProofBean.S_FRM16_SAL3 = parseInt(existingIncomeProofData.S_FRM16_SAL3);
		if(!!existingIncomeProofData.S_EMP_ISSDT)
			eduQ.incomeProofBean.S_EMP_ISSDT = CommonService.formatDobFromDb(existingIncomeProofData.S_EMP_ISSDT);
		if(!!existingIncomeProofData.S_EMP_SAL || existingIncomeProofData.S_EMP_SAL==0)
			eduQ.incomeProofBean.S_EMP_SAL = parseInt(existingIncomeProofData.S_EMP_SAL);
		if(!!existingIncomeProofData.S_BNK_SAL1 || existingIncomeProofData.S_BNK_SAL1==0)
			eduQ.incomeProofBean.S_BNK_SAL1 = parseInt(existingIncomeProofData.S_BNK_SAL1);
		if(!!existingIncomeProofData.S_BNK_SAL2 || existingIncomeProofData.S_BNK_SAL2==0)
			eduQ.incomeProofBean.S_BNK_SAL2 = parseInt(existingIncomeProofData.S_BNK_SAL2);
		if(!!existingIncomeProofData.S_BNK_SAL3 || existingIncomeProofData.S_BNK_SAL3==0)
			eduQ.incomeProofBean.S_BNK_SAL3 = parseInt(existingIncomeProofData.S_BNK_SAL3);
		if(!!existingIncomeProofData.S_BNK_SAL4 || existingIncomeProofData.S_BNK_SAL4==0)
			eduQ.incomeProofBean.S_BNK_SAL4 = parseInt(existingIncomeProofData.S_BNK_SAL4);
		if(!!existingIncomeProofData.S_BNK_SAL5 || existingIncomeProofData.S_BNK_SAL5==0)
			eduQ.incomeProofBean.S_BNK_SAL5 = parseInt(existingIncomeProofData.S_BNK_SAL5);
		if(!!existingIncomeProofData.S_BNK_SAL6 || existingIncomeProofData.S_BNK_SAL6==0)
			eduQ.incomeProofBean.S_BNK_SAL6 = parseInt(existingIncomeProofData.S_BNK_SAL6);
		if(!!existingIncomeProofData.S_SAL_SAL1 || existingIncomeProofData.S_SAL_SAL1==0)
			eduQ.incomeProofBean.S_SAL_SAL1 = parseInt(existingIncomeProofData.S_SAL_SAL1);
		if(!!existingIncomeProofData.S_SAL_SAL2 || existingIncomeProofData.S_SAL_SAL2==0)
			eduQ.incomeProofBean.S_SAL_SAL2 = parseInt(existingIncomeProofData.S_SAL_SAL2);
		if(!!existingIncomeProofData.S_SAL_SAL3 || existingIncomeProofData.S_SAL_SAL3==0)
			eduQ.incomeProofBean.S_SAL_SAL3 = parseInt(existingIncomeProofData.S_SAL_SAL3);
		if(!!existingIncomeProofData.S_COI_SAL1 || existingIncomeProofData.S_COI_SAL1==0)
			eduQ.incomeProofBean.S_COI_SAL1 = parseInt(existingIncomeProofData.S_COI_SAL1);
		if(!!existingIncomeProofData.S_COI_SAL2 || existingIncomeProofData.S_COI_SAL2==0)
			eduQ.incomeProofBean.S_COI_SAL2 = parseInt(existingIncomeProofData.S_COI_SAL2);
		if(!!existingIncomeProofData.S_COI_SAL3 || existingIncomeProofData.S_COI_SAL3==0)
			eduQ.incomeProofBean.S_COI_SAL3 = parseInt(existingIncomeProofData.S_COI_SAL3);
		if(!!existingIncomeProofData.SE_FRM16_SAL1 || existingIncomeProofData.SE_FRM16_SAL1 ==0)
			eduQ.incomeProofBean.SE_FRM16_SAL1 = parseInt(existingIncomeProofData.SE_FRM16_SAL1);
		if(!!existingIncomeProofData.SE_FRM16_SAL2 || existingIncomeProofData.SE_FRM16_SAL2 ==0)
			eduQ.incomeProofBean.SE_FRM16_SAL2 = parseInt(existingIncomeProofData.SE_FRM16_SAL2);
		if(!!existingIncomeProofData.SE_FRM16_SAL3 || existingIncomeProofData.SE_FRM16_SAL3 ==0)
			eduQ.incomeProofBean.SE_FRM16_SAL3 = parseInt(existingIncomeProofData.SE_FRM16_SAL3);
		if(!!existingIncomeProofData.SE_COI_PROFIT1 || existingIncomeProofData.SE_COI_PROFIT1==0)
			eduQ.incomeProofBean.SE_COI_PROFIT1 = parseInt(existingIncomeProofData.SE_COI_PROFIT1);
		if(!!existingIncomeProofData.SE_COI_PROFIT2 || existingIncomeProofData.SE_COI_PROFIT2==0)
			eduQ.incomeProofBean.SE_COI_PROFIT2 = parseInt(existingIncomeProofData.SE_COI_PROFIT2);
		if(!!existingIncomeProofData.SE_COI_PROFIT3 || existingIncomeProofData.SE_COI_PROFIT3==0)
			eduQ.incomeProofBean.SE_COI_PROFIT3 = parseInt(existingIncomeProofData.SE_COI_PROFIT3);
		if(!!existingIncomeProofData.SE_COI_SAL1 || existingIncomeProofData.SE_COI_SAL1==0)
			eduQ.incomeProofBean.SE_COI_SAL1 = parseInt(existingIncomeProofData.SE_COI_SAL1);
		if(!!existingIncomeProofData.SE_COI_SAL2 || existingIncomeProofData.SE_COI_SAL2==0)
			eduQ.incomeProofBean.SE_COI_SAL2 = parseInt(existingIncomeProofData.SE_COI_SAL2);
		if(!!existingIncomeProofData.SE_COI_SAL3 || existingIncomeProofData.SE_COI_SAL3==0)
			eduQ.incomeProofBean.SE_COI_SAL3 = parseInt(existingIncomeProofData.SE_COI_SAL3);
		if(!!existingIncomeProofData.SE_OTH_DOC || existingIncomeProofData.SE_OTH_DOC==0)
			eduQ.incomeProofBean.SE_OTH_DOC = existingIncomeProofData.SE_OTH_DOC;
		if(!!existingIncomeProofData.SE_OTH_PROFIT || existingIncomeProofData.SE_OTH_PROFIT==0)
			eduQ.incomeProofBean.SE_OTH_PROFIT = parseInt(existingIncomeProofData.SE_OTH_PROFIT);
		if(!!existingIncomeProofData.UE_HOUSE_PROP || existingIncomeProofData.UE_HOUSE_PROP==0)
			eduQ.incomeProofBean.UE_HOUSE_PROP = parseInt(existingIncomeProofData.UE_HOUSE_PROP);
		if(!!existingIncomeProofData.UE_RENT || existingIncomeProofData.UE_RENT==0)
			eduQ.incomeProofBean.UE_RENT = parseInt(existingIncomeProofData.UE_RENT);
		if(!!existingIncomeProofData.UE_GAIN_DIV || existingIncomeProofData.UE_GAIN_DIV==0)
			eduQ.incomeProofBean.UE_GAIN_DIV = parseInt(existingIncomeProofData.UE_GAIN_DIV);
		if(!!existingIncomeProofData.UE_AGRI || existingIncomeProofData.UE_AGRI==0)
			eduQ.incomeProofBean.UE_AGRI = parseInt(existingIncomeProofData.UE_AGRI);
		if(!!existingIncomeProofData.UE_IOTH || existingIncomeProofData.UE_IOTH==0)
			eduQ.incomeProofBean.UE_IOTH = parseInt(existingIncomeProofData.UE_IOTH);
		if(!!existingIncomeProofData.UE_GIFT || existingIncomeProofData.UE_GIFT==0)
			eduQ.incomeProofBean.UE_GIFT = parseInt(existingIncomeProofData.UE_GIFT);
		if(!!existingIncomeProofData.UE_OTH || existingIncomeProofData.UE_OTH==0)
			eduQ.incomeProofBean.UE_OTH = parseInt(existingIncomeProofData.UE_OTH);
	}

return dfd.promise;

}

this.setDeDupeData = function (){
var dfd = $q.defer();
		try{
			if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && ExistingAppData.applicationMainData.IS_EDUQUAL_DET != 'Y'){
						CommonService.showLoading("Loading Existing Data..");
						if(!eduQ.eduQualDetailsBean.INSURED_EDUCATION_QUALIFICATION.NBFE_CODE)
							eduQ.eduQualDetailsBean.INSURED_EDUCATION_QUALIFICATION = $filter('filter')(eduQ.eduQualList, {"NBFE_CODE": DeDupeData.RESULT_APPDATA.INSURED_QUALIFICATION}, true)[0];
						if(!eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.NBFE_CODE)
							{
								var occuClass = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": DeDupeData.RESULT_APPDATA.INSURED_OCCUPATION}, true)[0];
								if(!!occuClass && !!occuClass.NBFE_CODE){
									eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS = occuClass;
									eduQ.onInsOccClassChange(true);
								}
							}
						if(!eduQ.eduQualDetailsBean.INSURED_ANNUAL_INCOME && !!DeDupeData.RESULT_APPDATA.INSURED_ANNUALINCOME)
							eduQ.eduQualDetailsBean.INSURED_ANNUAL_INCOME = parseInt(DeDupeData.RESULT_APPDATA.INSURED_ANNUALINCOME);
						if(!eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF.DOC_ID && !!DeDupeData.RESULT_APPDATA.INSUERD_INCOMEPROOF)
							{
								var incPrf = $filter('filter')(eduQ.insIncomeProofList, {"NBFE_PROOF_CODE": DeDupeData.RESULT_APPDATA.INSUERD_INCOMEPROOF, "CUSTOMER_CATEGORY": 'IN'}, true)[0];
								if(!!incPrf && !!incPrf.DOC_ID){
									eduQ.eduQualDetailsBean.INSURED_INCOME_PROOF = incPrf;
									eduQ.onInsChangeIncProof(true);
								}
							}
						if(!eduQ.eduQualDetailsBean.INSURED_EMPLOYER_NAME)
							eduQ.eduQualDetailsBean.INSURED_EMPLOYER_NAME = DeDupeData.RESULT_APPDATA.INSURED_EMPLOYEEDETAIS;


							if(!eduQ.isSelf){
									if(!eduQ.eduQualDetailsBean.PROPOSER_EDUCATION_QUALIFICATION.NBFE_CODE)
										eduQ.eduQualDetailsBean.PROPOSER_EDUCATION_QUALIFICATION = $filter('filter')(eduQ.eduQualList, {"NBFE_CODE": DeDupeData.RESULT_APPDATA.OWNER_QUALIFICATION}, true)[0];
									if(!eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.NBFE_CODE)
										{
											var occuClass = $filter('filter')(eduQ.occClassList, {"NBFE_CODE": DeDupeData.RESULT_APPDATA.OWNER_OCCUPATION}, true)[0];
												if(!!occuClass && !!occuClass.NBFE_CODE)
												{
													eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS = occuClass;
													eduQ.onProOccClassChange(true);
												}
										}
									if(!eduQ.eduQualDetailsBean.PROPOSER_ANNUAL_INCOME && !!DeDupeData.RESULT_APPDATA.OWNER_ANNUALINCOME)
										eduQ.eduQualDetailsBean.PROPOSER_ANNUAL_INCOME = parseInt(DeDupeData.RESULT_APPDATA.OWNER_ANNUALINCOME);
									if(!eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF.DOC_ID && !!DeDupeData.RESULT_APPDATA.INSUERD_INCOMEPROOF)
										{
											var inc = $filter('filter')(eduQ.proIncomeProofList, {"NBFE_PROOF_CODE": DeDupeData.RESULT_APPDATA.INSUERD_INCOMEPROOF, "CUSTOMER_CATEGORY": 'IN'}, true)[0];
											if(!!inc && !!inc.DOC_ID)
												{
													eduQ.eduQualDetailsBean.PROPOSER_INCOME_PROOF = inc;
													eduQ.onProChangeIncProof(true);
												}
										}
									if(!eduQ.eduQualDetailsBean.PROPOSER_EMPLOYER_NAME)
										eduQ.eduQualDetailsBean.PROPOSER_EMPLOYER_NAME = DeDupeData.RESULT_APPDATA.OWNER_EMPLOYEEDETAIS;

										dfd.resolve(eduQ.eduQualDetailsBean);
							}
							else
								dfd.resolve(eduQ.eduQualDetailsBean);
			}
			else
	    {
	      debug("Already data saved");
	      dfd.resolve(eduQ.eduQualDetailsBean);
	    }

		}catch(e){
			debug("Exception in deDupe - Education");
			CommonService.hideLoading();
			dfd.resolve(eduQ.eduQualDetailsBean);
		}
    return dfd.promise;
}

eduQ.setExistingEduQualData().then(function(){
		eduQ.setDeDupeData().then(function (resp) {
	      CommonService.hideLoading();
	  });
		ApplicationFormDataService.applicationFormBean.eduQualDetailsBean = eduQ.eduQualDetailsBean;

});

   this.updateInsOccClassDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!!eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPPROOF_CODE != undefined){
         eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_OCCUPATION_CLASS.MPPROOF_CODE,'IN','class', true).then(
            function(res){
               dfd.resolve(res);
            }
         );
      }
      else{
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuclass = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuclass = null;
         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateProOccClassDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!isSelf){
         if(!!eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPPROOF_CODE != undefined){
            eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION_CLASS.MPPROOF_CODE,'PR','class', true).then(
               function(res){
                  dfd.resolve(res);
               }
            );
         }
         else{
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuclass = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuclass = null;
            dfd.resolve("S");
         }
      }
      else{
         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateInsOccDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!!eduQ.eduQualDetailsBean.INSURED_OCCUPATION && eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPPROOF_CODE != undefined){
         eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_OCCUPATION.MPPROOF_CODE,'IN','org', true).then(
            function(res){
               dfd.resolve(res);
            }
         );
      }
      else{
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.orgtype = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.orgtype = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;

         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateProOccDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!isSelf){
         if(!!eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPPROOF_CODE != undefined){
            eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_OCCUPATION.MPPROOF_CODE,'PR','org', true).then(
               function(res){
                  dfd.resolve(res);
               }
            );
         }
         else{
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.orgtype = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.orgtype = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;

            dfd.resolve("S");
         }
      }
      else{
         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateInsNatDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!!eduQ.eduQualDetailsBean.INSURED_WORK_NATURE && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE != undefined){
         eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPDOC_CODE, eduQ.eduQualDetailsBean.INSURED_WORK_NATURE.MPPROOF_CODE,'IN','nature', true).then(
            function(res){
               dfd.resolve(res);
            }
         );
      }
      else{
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nature = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nature = null;
         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateProNatDocsListIncaseOfBkp = function(){
      var dfd = $q.defer();
      if(!isSelf){
         if(!!eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPDOC_CODE != undefined && eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPPROOF_CODE != undefined){
            eduQ.showQuestionnaireMessage(eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPDOC_CODE, eduQ.eduQualDetailsBean.PROPOSER_WORK_NATURE.MPPROOF_CODE,'PR','nature', true).then(
               function(res){
                  dfd.resolve(res);
               }
            );
         }
         else{
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nature = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nature = null;
            dfd.resolve("S");
         }
      }
      else{
         dfd.resolve("S");
      }
      return dfd.promise;
   };

   this.updateDocsListIncaseOfBackup = function(){
      var dfd = $q.defer();

      this.updateInsOccClassDocsListIncaseOfBkp().then(
         function(insOccClassRes){
            return eduQ.updateProOccClassDocsListIncaseOfBkp();
         }
      )
      .then(
         function(proOccClassRes){
            return eduQ.updateInsOccDocsListIncaseOfBkp();
         }
      )
      .then(
         function(insOccRes){
            return eduQ.updateProOccDocsListIncaseOfBkp();
         }
      )
      .then(
         function(proOccRes){
            return eduQ.updateInsNatDocsListIncaseOfBkp();
         }
      )
      .then(
         function(insNatRes){
            return eduQ.updateProNatDocsListIncaseOfBkp();
         }
      )
      .then(
         function(res){
            dfd.resolve(res)
         }
      );

      return dfd.promise;
   };

   	this.onNext = function(){
   		eduQ.click = true;
   		if(eduQ.eduQualForm.$invalid != true){
   			debug("EDU DOCS_LIST BEFORE: " + JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
   			//this.updateDocsListIncaseOfBackup().then(
   				//function(updDocsListRes){
			debug("EDU DOCS_LIST AFTER: " + JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
			ApplicationFormDataService.applicationFormBean.eduQualDetailsBean = eduQ.eduQualDetailsBean;
			PersonalInfoService.getEduQualDetails(eduQ.eduQualDetailsBean, ApplicationFormDataService.applicationFormBean,ApplicationFormDataService.SISFormData).then(
				function(EduQualData){
					var whereClauseObj = {};
					whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
					whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

					var whereClauseObj1 = {};
					whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
					whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
					whereClauseObj1.CUST_TYPE = 'C01';

					var whereClauseObj2 = {};
					whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
					whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
					whereClauseObj2.CUST_TYPE = 'C02';
					console.log("DATA to be inserted :"+JSON.stringify(EduQualData.Insured.INCOME_PROOF));

					CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', EduQualData.Insured, whereClauseObj1).then(
						function(res){
							console.log("LP_APP_CONTACT_SCRN_A EduQualData update success !!"+JSON.stringify(whereClauseObj1));
							CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_EDUQUAL_DET" : 'Y'}, whereClauseObj).then(
								function(res){
									console.log("LP_APP_CONTACT_SCRN_A IS_EDUQUAL_DET update success !!"+JSON.stringify(whereClauseObj2));
									if((eduQ.PGL_ID==SR_PGL_ID || eduQ.PGL_ID==SRP_PGL_ID) && eduQ.sisDataPresent){
										PersonalInfoService.getIncomeProofData(eduQ.incomeProofBean,eduQ.eduQualDetailsBean,ApplicationFormDataService.SISFormData).then(
											function(incomeProofBean){
												debug("incomeProofBean : " + JSON.stringify(incomeProofBean));
												if(!!incomeProofBean){
													incomeProofBean.ISSYNCED = 'N';
													var whereClauseObj3 = {};
													whereClauseObj3.AGENT_CD = ApplicationFormDataService.SISFormData.sisMainData.AGENT_CD;
													whereClauseObj3.SIS_ID = ApplicationFormDataService.SISFormData.sisMainData.SIS_ID;
													debug("whereClauseObj3 : " + JSON.stringify(whereClauseObj3));
													CommonService.updateRecords(db, 'LP_TERM_VALIDATION', incomeProofBean, whereClauseObj3).then(
														function(res){
															console.log("LP_TERM_VALIDATION EduQualData update success !!"+JSON.stringify(res));
															if(!!res){
																ApplicationFormDataService.SISFormData.sisFormFData = incomeProofBean;
															}
														}
													);
												}
											}
										);
									}
								}
							);
							if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
								CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', EduQualData.Proposer, whereClauseObj2).then(
									function(res){
										persInfoService.setActiveTab("otherDetails");
										$state.go('applicationForm.personalInfo.otherDetails');
										console.log("LP_APP_CONTACT_SCRN_A EduQualData update success !!"+JSON.stringify(whereClauseObj2));
									}
								);
							}
							else{
								persInfoService.setActiveTab("otherDetails");
								$state.go('applicationForm.personalInfo.otherDetails');
							}
						}
					);
				}
			);
   				//}
   			//);
   		}
   		else
   			navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
   	}
	//CommonService.hideLoading();
}]);
