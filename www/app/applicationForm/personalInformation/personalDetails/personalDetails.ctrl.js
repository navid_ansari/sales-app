personalDetailsModule.controller('PersonalDetailsCtrl',['$q','$state','$filter', 'CommonService','LoginService','LoadApplicationScreenData','ApplicationFormDataService','personalDetailsService','ExistingAppData', 'SISFormData','LoadAppProofList','PersonalInfoService', 'Form60List', 'PANList', 'UANList', 'InsAgeProofList','ProAgeProofList','IDProofList','RelDescList','Profile','ProfileFlag','AppTimeService','LeadData', 'EKYCData','EKYCOldData','persInfoService', 'TdDocId','validationService','NOCparentsList','DeDupeData','DeDupeService', function($q, $state, $filter, CommonService, LoginService, LoadApplicationScreenData, ApplicationFormDataService,personalDetailsService,ExistingAppData, SISFormData, LoadAppProofList,PersonalInfoService, Form60List, PANList, UANList, InsAgeProofList, ProAgeProofList, IDProofList, RelDescList,Profile,ProfileFlag,AppTimeService, LeadData, EKYCData, EKYCOldData, persInfoService, TdDocId,validationService,NOCparentsList, DeDupeData, DeDupeService){

console.log("ApplicationFormDataService "+JSON.stringify(ApplicationFormDataService));
console.log("ApplicationFormDataService for cust_type1 "+JSON.stringify(SISFormData));
console.log("sisVernaculardata for custType :" + JSON.stringify(SISFormData.sisFormAData_eKyc.INSURANCE_BUYING_FOR));
console.log("ApplicationFormDataService for IDP "+JSON.stringify(IDProofList));

var pDet = this;
this.dvErrMsg = "";
this.personalDetBean = {};

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     console.log("Height : " + appOutputGetHeight);
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */
    if(!EKYCOldData || (!EKYCOldData.Insured && !EKYCOldData.Proposer) && (!EKYCOldData.Insured.OPP_ID && !EKYCOldData.Proposer.OPP_ID))
    {
      debug("Old EKYC data not found");
    }
    else {
      if(!EKYCData || (!EKYCData.Insured.EKYC_ID && !EKYCData.Proposer.EKYC_ID))
        {
          debug("Old EKYC data found");
          EKYCData = EKYCOldData;
        }
        else
          debug("USE NEW EKYC data");
    }

//start active tab
persInfoService.setActiveTab("personalDetails");

this.buyForCode = SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE;
debug("Insured Marital status: " + JSON.stringify(this.buyForCode));

ApplicationFormDataService.applicationFormBean.personalDetBean = this.personalDetBean;
this.click = false;

this.INS_DOB = SISFormData.sisFormAData.INSURED_DOB;
this.Age = CommonService.getAge(this.INS_DOB);

pDet.insEKYCFlag = false;
pDet.proEKYCFlag = false;

this.nameRegex = NAME_REGEX;
this.middleNameRegex = FULLNAME_REGEX;
this.panRegex = PAN_REG;
this.numRegex = NUM_REG;
this.freeTxtRegex = FREE_TEXT_REGEX;
ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
    Term : {},
	Insured:{},
	Proposer:{}
},
NonReflex :{
    Term :{},
	Insured:{},
	Proposer:{}
}

};
pDet.isSelf = ((SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)
pDet.PhotoFlag = ProfileFlag
debug((!pDet.PhotoFlag.Insured || pDet.PhotoFlag.Insured  != 'Y')+"pDet.PhotoFlag :"+JSON.stringify(pDet.PhotoFlag));
personalDetailsService.ProfileDocId = Profile;

this.InsDOBCondition = true;
this.ProDOBCondition = true;
if(!!SISFormData.sisFormAData.INSURED_DOB)
{
  debug("INS DOB:"+SISFormData.sisFormAData.INSURED_DOB);
  var dob = SISFormData.sisFormAData.INSURED_DOB;
  dob = dob.substring(0,5);
  debug("INS DOB: db:"+dob +"Cond :"+(dob == '01-01' || dob == '01-07'));
  pDet.InsDOBCondition = ((dob == '01-01' || dob == '01-07') ? false:true);
}

/*this.boolValue = false;
this.InsuredAge = SISFormData.sisFormAData.INSURED_DOB;
this.InsAge = CommonService.getAge(this.InsuredAge);
debug("Insured Age is::"+this.InsAge);
if(pDet.InsAge < 18){
	pDet.boolValue = true;
	debug("boolValue is::"+pDet.boolValue);
	}
else
	pDet.boolValue = false;*/

if(!!SISFormData.sisFormAData.PROPOSER_DOB)
{
  debug("PRO DOB:"+SISFormData.sisFormAData.PROPOSER_DOB);
  var dob = SISFormData.sisFormAData.PROPOSER_DOB;
  dob = dob.substring(0,5);
  debug("PRO DOB: db:"+dob +"Cond :"+(dob == '01-01' || dob == '01-07'));
  pDet.ProDOBCondition = ((dob == '01-01' || dob == '01-07') ? false:true);
}

this.titleList = [
		{"TITLE_DISPLAY": "Select ", "TITLE": null},
		{"TITLE_DISPLAY": "Mr.","TITLE": "Mr."},
		{"TITLE_DISPLAY": "Mrs.","TITLE": "Mrs."},
		{"TITLE_DISPLAY": "Ms.","TITLE": "Ms."},
		{"TITLE_DISPLAY": "Miss.","TITLE": "Miss."},
		{"TITLE_DISPLAY": "Dr.","TITLE": "Dr."}
	];
this.maritalStatusList = [
		{"MARITAL_STATUS":"Select ", "MARITAL_CD": null},
		{"MARITAL_STATUS":"Single", "MARITAL_CD":"S"},
		{"MARITAL_STATUS":"Married", "MARITAL_CD":"M"},
		{"MARITAL_STATUS":"Divorced", "MARITAL_CD":"D"},
		{"MARITAL_STATUS":"Widow", "MARITAL_CD":"W"},
		{"MARITAL_STATUS":"Widower", "MARITAL_CD":"W"}
];

//Added by Sneha 10/02/2017
this.onInsChangeExpDate = function(){
	debug("inside onInsChangeExpDate"+pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_DESC);
	debug(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_DESC);
	if(pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_DESC == pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_DESC){
		pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.INSURED_EXPIRY_DATE;
	}
}

this.onInsChangeIssuDate = function(){
	debug("inside onInsChangeIssuDate");
	pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE = pDet.personalDetBean.INSURED_ISSUANCE_DATE;
}

this.onInsChangeIdExpDate = function(){
	debug("inside onInsChangeIdExpDate");
	pDet.personalDetBean.INSURED_EXPIRY_DATE = pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE;
}

this.onInsChangeIdIssuDate = function(){
	debug("inside onInsChangeIdIssuDate");
	pDet.personalDetBean.INSURED_ISSUANCE_DATE = pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE;
}

this.onProChangeExpDate = function(){
	debug("inside onProChangeExpDate");
	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.PROPOSER_EXPIRY_DATE;
}

this.onProChangeIssuDate = function(){
	debug("inside onProChangeIssuDate");
	pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE = pDet.personalDetBean.PROPOSER_ISSUANCE_DATE;
}

this.onProChangeIdExpDate = function(){
	debug("inside onProChangeIdExpDate");
	pDet.personalDetBean.PROPOSER_EXPIRY_DATE = pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE;
}

this.onProChangeIdIssuDate = function(){
	debug("inside onProChangeIdIssuDate");
	pDet.personalDetBean.PROPOSER_ISSUANCE_DATE = pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE;
}

//Added by Amruta for Date of Expiry Validation
this.validateInsExpiryDate = function(){
   console.log("inside validateInsExpiryDate func");
if(!!pDet.personalDetBean.INSURED_EXPIRY_DATE){
  validationService.validateExpiryDate(pDet.personalDetBean.INSURED_EXPIRY_DATE).then(
    function(valid){
     console.log((valid.Flag)+"value of Valid::"+JSON.stringify(valid));
		if(!!valid)
		{
			if(valid.Flag==false)
			{
                 navigator.notification.alert(valid.msg,function(){
					 CommonService.hideLoading();
                     console.log("not valid::");
                 },"Application","OK");
                 //pDet.personalDetBean.INSURED_EXPIRY_DATE = "";
			}
		}
	});
  }
}

this.validateInsIDExpiryDate = function(){
if(!!pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE){
validationService.validateExpiryDate(pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE).then(function(valid){
    		if(!!valid)
    		{
    			if(valid.Flag==false)
    			{
    				//pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = valid.param;
    			}
    		}

    	});
        }
}

this.validatePropExpiryDate = function(){
console.log("inside validatePropExpiryDate func");
if(!!pDet.personalDetBean.PROPOSER_EXPIRY_DATE){
validationService.validateExpiryDate(pDet.personalDetBean.PROPOSER_EXPIRY_DATE).then(function(valid){
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.PROPOSER_EXPIRY_DATE = valid.param;
			}
		}
	});
    }
}

this.validatePropIDExpiryDate = function(){
console.log("inside validatePropIDExpiryDate func");
if(!!pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE){
validationService.validateExpiryDate(pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE).then(function(valid){
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = valid.param;
			}
		}

	});
    }
}

this.validateInsIssuanceDate = function(){
console.log("inside validateInsIssuanceDate::");
if(!!pDet.personalDetBean.INSURED_ISSUANCE_DATE){
validationService.validateIssuanceDate(pDet.personalDetBean.INSURED_ISSUANCE_DATE).then(function(valid){
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.INSURED_ISSUANCE_DATE = valid.param;
			}
		}

	});
    }
}

this.validateInsIDIssuanceDate = function(){
if(!!pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE){
validationService.validateIssuanceDate(pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE).then(function(valid){
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE = valid.param;
			}
		}

	});
    }
}

this.validatePropIssuanceDate = function(){
if(!!pDet.personalDetBean.PROPOSER_ISSUANCE_DATE){
validationService.validateIssuanceDate(pDet.personalDetBean.PROPOSER_ISSUANCE_DATE).then(function(valid){
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.PROPOSER_ISSUANCE_DATE = valid.param;
			}
		}

	});
    }
}

this.validatePropIDIssuanceDate = function(){
if(!!pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE){
validationService.validateIssuanceDate(pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE).then(function(valid){
console.log("valid in validatePropIDIssuanceDate::"+JSON.stringify(valid));
		if(!!valid)
		{
			if(valid.Flag==false)
			{
				//pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE = valid.param;
			}
		}

	});
}
}

this.initialisingData = function(){
var dfd = $q.defer();
//Set Insured List
pDet.personalDetBean.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
pDet.insAgeProofList = InsAgeProofList;
pDet.insIDProofList = IDProofList;
pDet.insRelDescList = RelDescList;
console.log("RelDescList::"+JSON.stringify(RelDescList));

console.log("EKYCData:here"+JSON.stringify(EKYCData));

if(!!EKYCData && !!EKYCData.Insured && !!EKYCData.Insured.CONSUMER_AADHAR_NO)
	pDet.insEKYCFlag = ((EKYCData.Insured.EKYC_FLAG == 'Y') ? true:false);

if(!!EKYCData && !!EKYCData.Proposer && !!EKYCData.Proposer.CONSUMER_AADHAR_NO)
	pDet.proEKYCFlag = ((EKYCData.Proposer.EKYC_FLAG == 'Y') ? true:false);

debug("proEKYCFlag :"+pDet.proEKYCFlag);
debug("ProDOBCondition :"+pDet.ProDOBCondition);
pDet.personalDetBean.INSURED_TITLE = pDet.titleList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean== undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.INSURED_MARITAL_STATUS==undefined)
	pDet.personalDetBean.INSURED_MARITAL_STATUS = pDet.maritalStatusList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.INSURED_AGE_PROOF == undefined)
	pDet.personalDetBean.INSURED_AGE_PROOF = pDet.insAgeProofList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.INSURED_IDENTITY_PROOF == undefined)
	pDet.personalDetBean.INSURED_IDENTITY_PROOF = pDet.insIDProofList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.RELATIONSHIP == undefined)
	pDet.personalDetBean.RELATIONSHIP = pDet.insRelDescList[0];

//Proposer List
pDet.proAgeProofList = ProAgeProofList;
pDet.proIDProofList = IDProofList;
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.PROPOSER_TITLE == undefined)
	pDet.personalDetBean.PROPOSER_TITLE = pDet.titleList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.PROPOSER_MARITAL_STATUS == undefined)
	pDet.personalDetBean.PROPOSER_MARITAL_STATUS = pDet.maritalStatusList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.PROPOSER_AGE_PROOF == undefined)
	pDet.personalDetBean.PROPOSER_AGE_PROOF = pDet.proAgeProofList[0];
if(ApplicationFormDataService.applicationFormBean.personalDetBean==undefined || ApplicationFormDataService.applicationFormBean.personalDetBean.PROPOSER_IDENTITY_PROOF == undefined)
	pDet.personalDetBean.PROPOSER_IDENTITY_PROOF = pDet.proIDProofList[0];


dfd.resolve(pDet.personalDetBean);

return dfd.promise;

}


this.setSisFormData = function(){
var dfd = $q.defer();
	console.log("inside setSISFormData");
	pDet.personalDetBean.INSURED_TITLE = $filter('filter')(pDet.titleList, {"TITLE": SISFormData.sisFormAData.INSURED_TITLE}, true)[0];
	/*.TITLE = SISFormData.sisFormAData.INSURED_TITLE;
	pDet.personalDetBean.INSURED_TITLE.TITLE_DISPLAY = SISFormData.sisFormAData.INSURED_TITLE;*/
	pDet.personalDetBean.INSURED_FIRST_NAME = SISFormData.sisFormAData.INSURED_FIRST_NAME;
	pDet.personalDetBean.INSURED_MIDDLE_NAME = SISFormData.sisFormAData.INSURED_MIDDLE;
	pDet.personalDetBean.INSURED_LAST_NAME = SISFormData.sisFormAData.INSURED_LAST_NAME;
	pDet.personalDetBean.INSURED_GENDER = SISFormData.sisFormAData.INSURED_GENDER;
	var dob = CommonService.formatDobFromDb(SISFormData.sisFormAData.INSURED_DOB);
	console.log(JSON.stringify(pDet.personalDetBean.INSURED_TITLE)+"dob::"+dob);
	pDet.personalDetBean.INSURED_DOB = new Date(dob);
	pDet.personalDetBean.INSURED_AGE_PROOF = $filter('filter')(pDet.insAgeProofList, {"DOC_ID": SISFormData.sisFormAData.AGE_PROOF_DOC_ID}, true)[0];
  if(!!pDet.personalDetBean.INSURED_AGE_PROOF && !!pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE && pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'AD')
    pDet.personalDetBean.INSURED_EKYC_OPT = 'Y';
  if(pDet.isSelf == false){
	debug("SISFormData.sisFormAData.PROPOSER_TITLE :"+SISFormData.sisFormAData.PROPOSER_TITLE);
		pDet.personalDetBean.PROPOSER_TITLE = $filter('filter')(pDet.titleList, {"TITLE": SISFormData.sisFormAData.PROPOSER_TITLE}, true)[0];
		/*.TITLE = SISFormData.sisFormAData.PROPOSER_TITLE;
		pDet.personalDetBean.PROPOSER_TITLE.TITLE_DISPLAY = SISFormData.sisFormAData.PROPOSER_TITLE;*/
		pDet.personalDetBean.PROPOSER_FIRST_NAME = SISFormData.sisFormAData.PROPOSER_FIRST_NAME;
		pDet.personalDetBean.PROPOSER_MIDDLE_NAME = SISFormData.sisFormAData.PROPOSER_MIDDLE_NAME;
		pDet.personalDetBean.PROPOSER_LAST_NAME = SISFormData.sisFormAData.PROPOSER_LAST_NAME;
		pDet.personalDetBean.PROPOSER_GENDER = SISFormData.sisFormAData.PROPOSER_GENDER;
		pDet.personalDetBean.PROPOSER_AGE_PROOF = $filter('filter')(pDet.proAgeProofList, {"DOC_ID": SISFormData.sisFormAData.AGE_PROOF_DOC_ID_PR}, true)[0];
    if(!!pDet.personalDetBean.PROPOSER_AGE_PROOF && !!pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE && pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'AD')
      pDet.personalDetBean.PROPOSER_EKYC_OPT = 'Y';
    var dob = CommonService.formatDobFromDb(SISFormData.sisFormAData.PROPOSER_DOB);
			pDet.personalDetBean.PROPOSER_DOB = new Date(dob);
	}
	dfd.resolve(pDet.personalDetBean);

    return dfd.promise;
}

this.setExisitingFormData = function(){
var dfd = $q.defer();
var MainData = ExistingAppData.applicationMainData;
var existingData = ExistingAppData.applicationPersonalInfo;


if(ExistingAppData!=undefined && ExistingAppData.applicationMainData!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.applicationPersonalInfo.Insured).length!=0)
	{
		ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
			if(MainData.EAI_RECEIVE_FLAG == 'Y')
		 pDet.personalDetBean.EIA_RECEIVE_FLAG = true;
		 else
		 pDet.personalDetBean.EIA_RECEIVE_FLAG = false;
		 if(MainData.EAI_NO!=undefined && MainData.EAI_NO!="")
		 pDet.personalDetBean.EIA_NO = parseInt(MainData.EAI_NO);

		 debug("existingData.Insured.AADHAR_CARD_EXISTS: " + existingData.Insured.AADHAR_CARD_EXISTS);
		pDet.personalDetBean.INSURED_EKYC_OPT = existingData.Insured.AADHAR_CARD_EXISTS;
		if(existingData.Insured.AADHAR_CARD_EXISTS == 'Y')
			pDet.personalDetBean.INSURED_UAN_NO = existingData.Insured.AADHAR_CARD_NO;
		pDet.personalDetBean.INSURED_PAN_CARD_EXISTS = ((existingData.Insured.PAN_CARD_EXISTS == 'Y' )? false:true);
		if(existingData.Insured.PAN_CARD_EXISTS)
		   pDet.personalDetBean.INSURED_PAN_CARD_NO = existingData.Insured.PAN_CARD_NO;
		pDet.personalDetBean.INSURED_IDENTIFICATION_MARK = existingData.Insured.IDENTIFICATION_MARK;
		pDet.personalDetBean.INSURED_DOMINANT_HAND = existingData.Insured.DOMINANT_HAND;
		if(existingData.Insured.MARTIAL_STATUS !=undefined)
			pDet.personalDetBean.INSURED_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList, {"MARITAL_CD": existingData.Insured.MARTIAL_STATUS_CODE}, true)[0];

		if(existingData.Insured.AGE_PROOF != undefined)
			{
        pDet.personalDetBean.INSURED_AGE_PROOF = $filter('filter')(pDet.insAgeProofList, {"DOC_ID": existingData.Insured.AGE_PROOF_DOC_ID}, true)[0];

      }

		pDet.personalDetBean.INSURED_AGE_PROOF_DOB = CommonService.formatDobFromDb(existingData.Insured.AGE_PROOF_DOB);

		if(existingData.Insured.DATE_OF_EXPIRY!=null || existingData.Insured.DATE_OF_EXPIRY!=undefined)
			pDet.personalDetBean.INSURED_EXPIRY_DATE = CommonService.formatDobFromDb(existingData.Insured.DATE_OF_EXPIRY);

		if(existingData.Insured.ID_DATE_OF_EXPIRY!=null || existingData.Insured.ID_DATE_OF_EXPIRY!=undefined)
			pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = CommonService.formatDobFromDb(existingData.Insured.ID_DATE_OF_EXPIRY);

		if(existingData.Insured.DATE_OF_ISSUANCE!=null || existingData.Insured.DATE_OF_ISSUANCE!=undefined)
			pDet.personalDetBean.INSURED_ISSUANCE_DATE = CommonService.formatDobFromDb(existingData.Insured.DATE_OF_ISSUANCE);

		if(existingData.Insured.ID_DATE_OF_ISSUANCE!=null || existingData.Insured.ID_DATE_OF_ISSUANCE!=undefined)
			pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE = CommonService.formatDobFromDb(existingData.Insured.ID_DATE_OF_ISSUANCE);

		if(existingData.Insured.IDENTITY_PROOF != undefined)
			pDet.personalDetBean.INSURED_IDENTITY_PROOF = $filter('filter')(pDet.insIDProofList, {"DOC_ID": existingData.Insured.IDENTITY_PROOF_DOC_ID}, true)[0];


		pDet.personalDetBean.INSURED_IDENTITY_PROOF_NO = existingData.Insured.IDENTITY_PROOF_NO;
		if(existingData.Insured.IDENTITY_PROOF_OTHER != undefined)
			pDet.personalDetBean.INSURED_IDENTITY_PROOF_OTHER = existingData.Insured.IDENTITY_PROOF_OTHER;
			//Proposer
			if(SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
			console.log("JSON  :  :"+JSON.stringify(existingData.Proposer));
			pDet.personalDetBean.PROPOSER_EKYC_OPT = existingData.Proposer.AADHAR_CARD_EXISTS;
					if(existingData.Proposer.AADHAR_CARD_EXISTS == 'Y')
						pDet.personalDetBean.PROPOSER_UAN_NO = existingData.Proposer.AADHAR_CARD_NO;
					pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS = ((existingData.Proposer.PAN_CARD_EXISTS == 'Y' )? false:true);
					if(existingData.Proposer.PAN_CARD_EXISTS)
					   pDet.personalDetBean.PROPOSER_PAN_CARD_NO = existingData.Proposer.PAN_CARD_NO;
					pDet.personalDetBean.PROPOSER_IDENTIFICATION_MARK = existingData.Proposer.IDENTIFICATION_MARK;
					pDet.personalDetBean.PROPOSER_DOMINANT_HAND = existingData.Proposer.DOMINANT_HAND;
					if(existingData.Proposer.MARTIAL_STATUS !=undefined)
						pDet.personalDetBean.PROPOSER_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList, {"MARITAL_CD": existingData.Proposer.MARTIAL_STATUS_CODE}, true)[0];

					if(existingData.Proposer.AGE_PROOF != undefined)
						{
              pDet.personalDetBean.PROPOSER_AGE_PROOF = $filter('filter')(pDet.proAgeProofList, {"DOC_ID": existingData.Proposer.AGE_PROOF_DOC_ID}, true)[0];

            }

					pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB = CommonService.formatDobFromDb(existingData.Proposer.AGE_PROOF_DOB);
					if(existingData.Proposer.DATE_OF_EXPIRY!=undefined || existingData.Proposer.DATE_OF_EXPIRY!=null)
						pDet.personalDetBean.PROPOSER_EXPIRY_DATE = CommonService.formatDobFromDb(existingData.Proposer.DATE_OF_EXPIRY);

					if(existingData.Proposer.ID_DATE_OF_EXPIRY!=undefined || existingData.Proposer.ID_DATE_OF_EXPIRY!=null)
						pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = CommonService.formatDobFromDb(existingData.Proposer.ID_DATE_OF_EXPIRY);

					if(existingData.Proposer.DATE_OF_ISSUANCE!=undefined || existingData.Proposer.DATE_OF_ISSUANCE!=null)
						pDet.personalDetBean.PROPOSER_ISSUANCE_DATE = CommonService.formatDobFromDb(existingData.Proposer.DATE_OF_ISSUANCE);

					if(existingData.Proposer.ID_DATE_OF_ISSUANCE!=undefined || existingData.Proposer.ID_DATE_OF_ISSUANCE!=null)
						pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE = CommonService.formatDobFromDb(existingData.Proposer.ID_DATE_OF_ISSUANCE);


					if(existingData.Proposer.IDENTITY_PROOF != undefined)
						pDet.personalDetBean.PROPOSER_IDENTITY_PROOF = $filter('filter')(pDet.proIDProofList, {"DOC_ID": existingData.Proposer.IDENTITY_PROOF_DOC_ID}, true)[0];


					pDet.personalDetBean.PROPOSER_IDENTITY_PROOF_NO = existingData.Proposer.IDENTITY_PROOF_NO;
					if(existingData.Proposer.IDENTITY_PROOF_OTHER != undefined)
						pDet.personalDetBean.PROPOSER_IDENTITY_PROOF_OTHER = existingData.Proposer.IDENTITY_PROOF_OTHER;
			}
						if(SISFormData.sisFormAData.PROPOSER_IS_INSURED=="Y")
						{
							//Self
							pDet.personalDetBean.RELATIONSHIP = $filter('filter')(pDet.insRelDescList, {"NBFE_CODE": "20"})[0];

						}
						else
						{
							//Non - Self
							pDet.personalDetBean.RELATIONSHIP = $filter('filter')(pDet.insRelDescList, {"NBFE_CODE": existingData.Proposer.RELATIONSHIP_ID}, true)[0];
						}
	}
	else
	    {
	    console.log("INSURANCE_BUYING_FOR_CODE is::"+SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE);
	    if(SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE == '75')
	    {
	    	pDet.personalDetBean.INSURED_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList,{"MARITAL_CD":"M"})[0];
	    	pDet.personalDetBean.PROPOSER_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList,{"MARITAL_CD":"M"})[0];
	    	console.log("PROPOSER_MARITAL_STATUS:::"+JSON.stringify(pDet.personalDetBean.PROPOSER_MARITAL_STATUS));
	    }
         if(SISFormData.sisFormAData.PROPOSER_IS_INSURED=="Y")
         {
           //Self
           pDet.personalDetBean.RELATIONSHIP = $filter('filter')(pDet.insRelDescList, {"NBFE_CODE": "20"})[0];

         }
         else
         {
           //Non - Self
           pDet.personalDetBean.RELATIONSHIP = pDet.insRelDescList[0];
         }
	       if(!!LeadData &&  !!LeadData.MARITAL_STATUS_VAL && LeadData.MARITAL_STATUS_VAL!="" && SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE != '75')
            {
                if(pDet.isSelf)
				{
					pDet.personalDetBean.INSURED_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList, {"MARITAL_CD": LeadData.MARITAL_STATUS_VAL}, true)[0];

				}
                else
                {
                    pDet.personalDetBean.PROPOSER_MARITAL_STATUS = $filter('filter')(pDet.maritalStatusList, {"MARITAL_CD": LeadData.MARITAL_STATUS_VAL}, true)[0];
                    console.log("Lead Proposer Marital Status:::"+JSON.stringify(pDet.personalDetBean.PROPOSER_MARITAL_STATUS));
            	}
            }
            if(!pDet.personalDetBean.INSURED_EKYC_OPT)
              pDet.personalDetBean.INSURED_EKYC_OPT = EKYCData.Insured.EKYC_FLAG;
            console.log(pDet.personalDetBean.INSURED_EKYC_OPT+" :: "+EKYCData.Insured.CONSUMER_AADHAR_NO)
			if(EKYCData.Insured.EKYC_FLAG == 'Y')
				pDet.personalDetBean.INSURED_UAN_NO = EKYCData.Insured.CONSUMER_AADHAR_NO;
			if(!pDet.isSelf){
        if(!pDet.personalDetBean.PROPOSER_EKYC_OPT)
				    pDet.personalDetBean.PROPOSER_EKYC_OPT = EKYCData.Proposer.EKYC_FLAG;
	            console.log(pDet.personalDetBean.PROPOSER_EKYC_OPT+" :: "+EKYCData.Proposer.CONSUMER_AADHAR_NO)
				if(EKYCData.Proposer.EKYC_FLAG == 'Y')
					pDet.personalDetBean.PROPOSER_UAN_NO = EKYCData.Proposer.CONSUMER_AADHAR_NO;
			}
	    }
		/*console.log("Immunization Record List is::"+JSON.stringify(ImmunizationList));
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization = {};
		if(parseInt(pDet.Age) >= 0 && parseInt(pDet.Age) <= 5)
	    	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization.DOC_ID = ImmunizationList.Insured.immunization;
	   	else
	   		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization = null;*/


	dfd.resolve(pDet.personalDetBean);

        return dfd.promise;
}

this.initialisingData().then(function(Data){

console.log("initialisingData complete !!");
pDet.setSisFormData().then(
	function(){
		pDet.setExisitingFormData().then(function(){
			CommonService.hideLoading();
			ApplicationFormDataService.applicationFormBean.personalDetBean = pDet.personalDetBean;
        	ApplicationFormDataService.SISFormData = SISFormData;
		});

	});
});

this.onChangeEiaFlag = function(personalForm){
console.log(pDet.personalDetBean.EIA_NO+"inside onChangeEiaFlag"+pDet.personalDetBean.EIA_RECEIVE_FLAG);
	if(pDet.personalDetBean.EIA_RECEIVE_FLAG && (pDet.personalDetBean.EIA_NO == undefined || pDet.personalDetBean.EIA_NO == "" || pDet.personalDetBean.EIA_NO == NaN))
	{
		console.log("onChangeEiaFlag :invalid")
		personalForm.eIANo.$setValidity('eiaRequired', false);
	}
	else
		personalForm.eIANo.$setValidity('eiaRequired', true);

}

this.onRelationshipChnge = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.parentNOC = {};
	if(pDet.personalDetBean.RELATIONSHIP.NBFE_CODE == '29'){
	console.log("inside onRelationshipChnge::"+JSON.stringify(pDet.personalDetBean.RELATIONSHIP.NBFE_CODE));
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.parentNOC.DOC_ID = NOCparentsList.Proposer.parentNOC;
	}
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.parentNOC = null;
	//}
}

this.isSameAsInsIDProof = function(){

if(pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.INSURED_IDENTITY_PROOF_NO = pDet.personalDetBean.INSURED_PAN_CARD_NO;
	}
else
	{
		debug("GOT Ins KYC"+pDet.personalDetBean.INSURED_PAN_CARD_EXISTS);
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = {};
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = {};
		if(pDet.personalDetBean.INSURED_PAN_CARD_EXISTS  == false || pDet.personalDetBean.INSURED_PAN_CARD_EXISTS == undefined)
		{
			if(PANList.Insured.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan.DOC_ID = PANList.Insured.pan;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan.DOC_ID = PANList.Insured.pan;
		}
		else
		{
			debug("KYC NULL :1");
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = null;
		}

	}
	if(pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
	{
		pDet.personalDetBean.INSURED_IDENTITY_PROOF_NO = pDet.personalDetBean.INSURED_UAN_NO;
	}
	else
	{
		debug("GOT Ins KYC:AC")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan = {};
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan = {};
		if(pDet.personalDetBean.INSURED_EKYC_OPT == 'Y' && pDet.insEKYCFlag == false)
		{
			if(UANList.Insured.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan.DOC_ID = UANList.Insured.uan;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan.DOC_ID = UANList.Insured.uan;
		}
		else
		{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan = null;
		}
	}

}

this.isSameAsProIDProof = function(){

if(pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.PROPOSER_IDENTITY_PROOF_NO = pDet.personalDetBean.PROPOSER_PAN_CARD_NO;
	}
else
	{
		debug("GOT pr KYC")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = {};
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = {};
		if(pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS  == false)
		{
			if(PANList.Proposer.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan.DOC_ID = PANList.Proposer.pan;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan.DOC_ID = PANList.Proposer.pan;
		}
		else
			{
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = null;
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = null;
			}
	}

if(pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
	{
		pDet.personalDetBean.PROPOSER_IDENTITY_PROOF_NO = pDet.personalDetBean.PROPOSER_UAN_NO;
	}
	else
	{
	debug("GOT Ins KYC:AC")
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan = {};
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan = {};
		if(pDet.personalDetBean.PROPOSER_EKYC_OPT == 'Y' && pDet.proEKYCFlag == false)
    		{
    			if(UANList.Proposer.isDigital == "Y")
					ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan.DOC_ID = UANList.Proposer.uan;
				else
					ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan.DOC_ID = UANList.Proposer.uan;
    		}
    		else
    		{
    			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan = null;
    		}

	}
}

this.resetInsPAN = function(showMsg){
                                                        if(showMsg)
                                                        pDet.personalDetBean.INSURED_PAN_CARD_EXISTS = false;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.form60 = {};
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = {};
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.form60 = {};
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = {};

                                                        if(showMsg)
                                                        {/*Do not reset */}
                                                        else
                                                        pDet.personalDetBean.INSURED_PAN_CARD_NO = null;
                                                        if(pDet.personalDetBean.INSURED_PAN_CARD_EXISTS)
                                                        {debug("KYC NULL :2")
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = null;
                                                        if(showMsg == undefined)
                                                        {
                                                        navigator.notification.alert("You will not be allowed to submit the application without uploading a Signed Form 60/61.",function(){CommonService.hideLoading();},"Application","OK");
                                                        if(Form60List.Insured.isDigital == "Y")
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.form60.DOC_ID = Form60List.Insured.form60;
                                                        else
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.form60.DOC_ID = Form60List.Insured.form60;
                                                        }
                                                        else
                                                        {debug("KYC NULL :3")
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.form60 = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.form60 = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = null;
                                                        }

                                                        }
                                                        else
                                                        {
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.form60 = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.form60 = null;
                                                        if(showMsg == undefined){
                                                        if(PANList.Insured.isDigital == "Y")
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan.DOC_ID = PANList.Insured.pan;
                                                        else
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan.DOC_ID = PANList.Insured.pan;
                                                        }
                                                        else
                                                        {debug("KYC NULL :4")
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.form60 = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.pan = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.form60 = null;
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.pan = null;
                                                        }
                                                        }
}
this.resetProPAN = function(showMsg){
if(showMsg)
	pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS = false;
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.form60 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.form60 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = {};

	if(showMsg)
		{/*Do not reset */}
	else
		pDet.personalDetBean.PROPOSER_PAN_CARD_NO = null;

	if(pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS)
	{
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = null;
		if(showMsg == undefined){
			navigator.notification.alert("You will not be allowed to submit the application without uploading a Signed Form 60/61.",function(){CommonService.hideLoading();},"Application","OK");
			if(Form60List.Proposer.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.form60.DOC_ID = Form60List.Proposer.form60;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.form60.DOC_ID = Form60List.Proposer.form60;
		}
		else
		{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.form60 = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.form60 = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = null;
		}

	}
	else
	{
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.form60 = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.form60 = null;
		if(showMsg == undefined){
			if(PANList.Proposer.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan.DOC_ID = PANList.Proposer.pan;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan.DOC_ID = PANList.Proposer.pan;
		}
		else
		{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.form60 = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.pan = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.form60 = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.pan = null;
		}
	}
}

this.resetInsUAN = function(showMsg){
if(showMsg)
	pDet.personalDetBean.INSURED_EKYC_OPT = 'Y';
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan = {};
		if(pDet.personalDetBean.INSURED_EKYC_OPT == 'Y' && pDet.insEKYCFlag == false)
		{
			if(showMsg == undefined){
			if(UANList.Insured.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan.DOC_ID = UANList.Insured.uan;
            else
            	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan.DOC_ID = UANList.Insured.uan;
			}
			else
			{
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan = null;
			}
		}
		else
		{
			pDet.personalDetBean.INSURED_UAN_NO = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.uan = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.uan = null;
		}



}

this.resetProUAN = function(showMsg){
if(showMsg)
	pDet.personalDetBean.PROPOSER_EKYC_OPT = 'Y';
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan = {};
	if(pDet.personalDetBean.PROPOSER_EKYC_OPT == 'Y' && pDet.proEKYCFlag == false)
		{
			if(showMsg == undefined){
				if(UANList.Insured.isDigital == "Y")
					ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan.DOC_ID = UANList.Proposer.uan;
				else
					ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan.DOC_ID = UANList.Proposer.uan;
			}
			else
			{
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan = null;
			}
		}
		else
		{
			pDet.personalDetBean.PROPOSER_UAN_NO = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.uan = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.uan = null;
		}
}

this.onInsChangeAgeProof = function(){
console.log("pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE :"+pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE);
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.ageproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ageproof = {};

if(pDet.personalDetBean.INSURED_AGE_PROOF.isDigital == "Y")
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.ageproof.DOC_ID = pDet.personalDetBean.INSURED_AGE_PROOF.DOC_ID;
else
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ageproof.DOC_ID = pDet.personalDetBean.INSURED_AGE_PROOF.DOC_ID;
navigator.notification.alert("You will not be allowed to submit the application without uploading the respective age proof.",function(){CommonService.hideLoading();},"Application","OK");
if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'PC' || pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.INSURED_PAN_CARD_EXISTS = false;
		pDet.resetInsPAN(true);
	}
if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'AD' || pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
{
	pDet.personalDetBean.INSURED_EKYC_OPT = 'Y';
	pDet.resetInsUAN(true);
}

}
this.onProChangeAgeProof = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.ageproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ageproof = {};

if(pDet.personalDetBean.PROPOSER_AGE_PROOF.isDigital == "Y")
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.ageproof.DOC_ID = pDet.personalDetBean.PROPOSER_AGE_PROOF.DOC_ID;
else
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ageproof.DOC_ID = pDet.personalDetBean.PROPOSER_AGE_PROOF.DOC_ID;
navigator.notification.alert("You will not be allowed to submit the application without uploading the respective age proof.",function(){CommonService.hideLoading();},"Application","OK");
if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'PC' || pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS = false;
		pDet.resetProPAN(true);
	}
if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'AD' || pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
{
	pDet.personalDetBean.PROPOSER_EKYC_OPT = 'Y';
	pDet.resetProUAN(true);
}
}
this.onInsChangeIDProof = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.idproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.idproof = {};
pDet.isSameAsInsIDProof();
navigator.notification.alert("You will not be allowed to submit the application without uploading the respective identity proof.",function(){CommonService.hideLoading();},"Application","OK");
if(pDet.personalDetBean.INSURED_IDENTITY_PROOF.isDigital == "Y")
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.idproof.DOC_ID = pDet.personalDetBean.INSURED_IDENTITY_PROOF.DOC_ID;
else
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.idproof.DOC_ID = pDet.personalDetBean.INSURED_IDENTITY_PROOF.DOC_ID;
if(pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_DESC!='Others')
	pDet.personalDetBean.INSURED_IDENTITY_PROOF_OTHER = null;

if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'PC' || pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.INSURED_PAN_CARD_EXISTS = false;
		pDet.resetInsPAN(true);
	}
if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'AD' || pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
{
	pDet.personalDetBean.INSURED_EKYC_OPT = 'Y';
	pDet.resetInsUAN(true);
}
console.log("InsAgeProof::"+pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE+"InsIdentityProof::"+pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE);
if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'DL' && pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
{
	pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.INSURED_EXPIRY_DATE;
	pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE = pDet.personalDetBean.INSURED_ISSUANCE_DATE;
}
else
{
	//Commented by Sneha 09/02/2017
//	if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'PP' && pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PP')
//    	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.PROPOSER_EXPIRY_DATE;
	if(pDet.personalDetBean.INSURED_AGE_PROOF.MPPROOF_CODE == 'PP' && pDet.personalDetBean.INSURED_IDENTITY_PROOF.MPPROOF_CODE == 'PP')
    	pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.INSURED_EXPIRY_DATE;
    else
//    	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = "";
		pDet.personalDetBean.INSURED_IDENTITY_EXPIRY_DATE = "";

	pDet.personalDetBean.INSURED_IDENTITY_ISSUANCE_DATE = "";
}
}

this.onProChangeIDProof = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.idproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.idproof = {};
pDet.isSameAsProIDProof();
navigator.notification.alert("You will not be allowed to submit the application without uploading the respective identity proof.",function(){CommonService.hideLoading();},"Application","OK");
if(pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.isDigital == "Y")
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.idproof.DOC_ID = pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.DOC_ID;
else
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.idproof.DOC_ID = pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.DOC_ID;

if(pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_DESC!='Others')
	pDet.personalDetBean.PROPOSER_IDENTITY_PROOF_OTHER = null;

if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'PC' || pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PC')
	{
		pDet.personalDetBean.PROPOSER_PAN_CARD_EXISTS = false;
		pDet.resetProPAN(true);
	}
if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'AD' || pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'AC')
{
	pDet.personalDetBean.PROPOSER_EKYC_OPT = 'Y';
	pDet.resetProUAN(true);
}
if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'DL' && pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'DL')
{
	console.log("1 - PropExpiryDate::"+pDet.personalDetBean.PROPOSER_EXPIRY_DATE+"PropIdentityExpiryDate::"+pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE);
	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.PROPOSER_EXPIRY_DATE;
	console.log("PropExpiryDate::"+pDet.personalDetBean.PROPOSER_EXPIRY_DATE+"PropIdentityExpiryDate::"+pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE);
	pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE = pDet.personalDetBean.PROPOSER_ISSUANCE_DATE;
}
else
{
	console.log("inside else:::");
	if(pDet.personalDetBean.PROPOSER_AGE_PROOF.MPPROOF_CODE == 'PP' && pDet.personalDetBean.PROPOSER_IDENTITY_PROOF.MPPROOF_CODE == 'PP')
    	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = pDet.personalDetBean.PROPOSER_EXPIRY_DATE;
    else
    	pDet.personalDetBean.PROPOSER_IDENTITY_EXPIRY_DATE = "";

    pDet.personalDetBean.PROPOSER_IDENTITY_ISSUANCE_DATE = "";
}
}

this.onNext = function(personalForm){
CommonService.showLoading();
personalDetailsService.checkExistingPhoto().then(function(ProfileFlag){

	//if(pDet.boolValue == true){
		//ProfileFlag.Insured = 'Y';
		debug("Inside if::"+ProfileFlag.Insured);
	//}
	debug("ProfileFlag :"+JSON.stringify(ProfileFlag));
	pDet.click = true;
	if(pDet.isSelf)
	    propCond = true;
	else
	    propCond = (ProfileFlag.Proposer == 'Y');

	if(ApplicationFormDataService.SISFormData.sisFormAData.TATA_FLAG=='Y'){
		if(!!pDet.isSelf){
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.tataDisc = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.tataDisc.DOC_ID = TdDocId;
		}
		else{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.tataDisc = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.tataDisc.DOC_ID = TdDocId;
		}
	}

	if(!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ageproof){
		if(pDet.personalDetBean.INSURED_AGE_PROOF.isDigital == "Y"){
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.ageproof = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.ageproof.DOC_ID = pDet.personalDetBean.INSURED_AGE_PROOF.DOC_ID;
		}
		else{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ageproof = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ageproof.DOC_ID = pDet.personalDetBean.INSURED_AGE_PROOF.DOC_ID;
		}
	}

	if(!pDet.isSelf){
		if(pDet.personalDetBean.PROPOSER_AGE_PROOF.isDigital == "Y"){
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.ageproof = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.ageproof.DOC_ID = pDet.personalDetBean.PROPOSER_AGE_PROOF.DOC_ID;
		}
		else{
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ageproof = {};
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ageproof.DOC_ID = pDet.personalDetBean.PROPOSER_AGE_PROOF.DOC_ID;
		}
	}

	debug("ProfileFlag :propCond: "+propCond+"pDet.boolValue::"+pDet.boolValue);
	if(personalForm.$invalid == false && ProfileFlag.Insured == 'Y' && propCond){
	var whereClauseOpp = {};
		whereClauseOpp.OPPORTUNITY_ID = ApplicationFormDataService.applicationFormBean.OPP_ID;
		//whereClauseOpp.FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
	    whereClauseOpp.AGENT_CD = pDet.personalDetBean.AGENT_CD;
	var whereClauseObj = {};
		whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
		whereClauseObj.AGENT_CD = pDet.personalDetBean.AGENT_CD;
	var whereClauseObj1 = {};
		whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
		whereClauseObj1.AGENT_CD = pDet.personalDetBean.AGENT_CD;
		whereClauseObj1.CUST_TYPE = 'C01';
	var whereClauseObj2 = {};
		whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
		whereClauseObj2.AGENT_CD = pDet.personalDetBean.AGENT_CD;
		whereClauseObj2.CUST_TYPE = 'C02';

		ApplicationFormDataService.applicationFormBean.personalDetBean = pDet.personalDetBean;
		ApplicationFormDataService.SISFormData = SISFormData;
		//console.log("ApplicationFormDataService.applicationFormBean :"+JSON.stringify(ApplicationFormDataService.applicationFormBean));
		LoadApplicationScreenData.loadOppFlags(ApplicationFormDataService.applicationFormBean.OPP_ID, pDet.personalDetBean.AGENT_CD, 'opp').then(function(oppData){
			if(!!oppData && !!oppData.oppFlags && !!oppData.oppFlags.RECO_PRODUCT_DEVIATION_FLAG)
			{
				ApplicationFormDataService.applicationFormBean.RECO_PRODUCT_DEVIATION_FLAG = oppData.oppFlags.RECO_PRODUCT_DEVIATION_FLAG;

			}
			else
				ApplicationFormDataService.applicationFormBean.RECO_PRODUCT_DEVIATION_FLAG = 'S';

			PersonalInfoService.getAppMainData(ApplicationFormDataService.applicationFormBean,SISFormData).then(
			function(MainData){

				PersonalInfoService.getPersonalDetailsData(ApplicationFormDataService.applicationFormBean, SISFormData).then(
				function(ContactData){

						console.log("Main Data::"+JSON.stringify(MainData)+" \n personalData :"+ContactData);

							CommonService.selectRecords(db,"LP_APP_CONTACT_SCRN_A",false,"*",whereClauseObj1, "").then(
								function(res){
									console.log("record exisit LP_APP_CONTACT_SCRN_A:" + res.rows.length);
									if(!!res && res.rows.length>0){
											delete ContactData.Insured.APPLICATION_ID;
											delete ContactData.Insured.CUST_TYPE;
											delete ContactData.Insured.AGENT_CD;
											delete ContactData.Proposer.APPLICATION_ID;
											delete ContactData.Proposer.CUST_TYPE;
											delete ContactData.Proposer.AGENT_CD;
												CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"EAI_RECEIVE_FLAG":(pDet.personalDetBean.EIA_RECEIVE_FLAG ? 'Y':'N'),"EAI_NO":pDet.personalDetBean.EIA_NO,"COMBO_ID":((!!ApplicationFormDataService.ComboFormBean && !!ApplicationFormDataService.ComboFormBean.COMBO_ID)?ApplicationFormDataService.ComboFormBean.COMBO_ID:null),"RECO_PRODUCT_DEVIATION_FLAG":ApplicationFormDataService.applicationFormBean.RECO_PRODUCT_DEVIATION_FLAG}, whereClauseObj).then(
													function(res){
														console.log("LP_APP_CONTACT_SCRN_A ContactData update success !!"+JSON.stringify(whereClauseObj2));
													});
												CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Insured, whereClauseObj1).then(
												function(res){
												console.log("LP_APP_CONTACT_SCRN_A ContactData update success !!"+JSON.stringify(whereClauseObj1));

								                console.log("Proposer DATA ::: "+JSON.stringify(ContactData.Proposer));
												if(SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
												CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Proposer, whereClauseObj2).then(
																				function(res){
																					pDet.gotoContactScreen(ContactData, whereClauseObj);
																						console.log("LP_APP_CONTACT_SCRN_A ContactData update Pro success !!"+JSON.stringify(whereClauseObj2));
																				});
												else{
								                    pDet.gotoContactScreen(ContactData, whereClauseObj);
												}

												});
									}
									else
									{
										if(!!ApplicationFormDataService.applicationFormBean.OPP_ID)
										CommonService.updateRecords(db, 'LP_MYOPPORTUNITY', {"APPLICATION_ID" : ApplicationFormDataService.applicationFormBean.APPLICATION_ID}, whereClauseOpp).then(function(res){

				                        													console.log("LP_MYOPPORTUNITY ContactData update success !!"+JSON.stringify(whereClauseOpp));
				                        												});
				                        else
				                        	navigator.notification.alert("Opportunity not found",null,"Application","OK");

										CommonService.insertOrReplaceRecord(db, 'LP_APPLICATION_MAIN', MainData, false).then(
																function(res){
																	console.log("LP_APPLICATION_MAIN success !!"+JSON.stringify(res));
																}
															);

										CommonService.insertOrReplaceRecord(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Insured,true).then(
																function(res){
																	console.log("Contact Success Insured");

																	if(SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
																		CommonService.insertOrReplaceRecord(db, 'LP_APP_CONTACT_SCRN_A', ContactData.Proposer, true).then(function(res){
																						    pDet.gotoContactScreen(ContactData, whereClauseObj);
																							console.log("Contact Success Proposer");

																						});
																	else{
																		    pDet.gotoContactScreen(ContactData, whereClauseObj);
																		}
																});
									}
								});
				});
			});
		});

	}
	else
	{
      navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
	}
	});
}

this.gotoContactScreen = function(ContactData, whereClauseObj){
    var POLICY_NO = ApplicationFormDataService.applicationFormBean.POLICY_NO;
    var APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;

    CommonService.showLoading("Checking existing policy data...");
    if(!!DeDupeData && !!DeDupeData.RESP && (DeDupeData.RESP == 'S' || DeDupeData.RESP == 'F'))
    {
      if(!!whereClauseObj)
        CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_PERSONAL_DET" : 'Y'}, whereClauseObj).then(function(res){
          console.log("LP_APP_CONTACT_SCRN_A IS_PERSONAL_DET update success !!"+JSON.stringify(whereClauseObj));
        });
      CommonService.showLoading("Loading...");
      persInfoService.setActiveTab("contactDetails");
      $state.go('applicationForm.personalInfo.contactDetails');
    }
    else
    {
      if(CommonService.checkConnection() == "online"){
        debug("<<<<<<<<<< CALLING DE DUPE >>>>>>>>>>>>>");
          DeDupeService.createDeDupeRequest(SISFormData, ContactData, POLICY_NO, APP_ID).then(function(dupeRequest) {
              DeDupeService.callDeDupeService(dupeRequest).then(function(resp) {
                if(!!whereClauseObj)
                  CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_PERSONAL_DET" : 'Y'}, whereClauseObj).then(function(res){
                    console.log("LP_APP_CONTACT_SCRN_A IS_PERSONAL_DET update success !!"+JSON.stringify(whereClauseObj));
                  });
                  CommonService.showLoading("Loading...");
                  persInfoService.setActiveTab("contactDetails");
                  $state.go('applicationForm.personalInfo.contactDetails');
              });
          });
      }
        else
        {
           CommonService.hideLoading();
           navigator.notification.alert("Please be Online or Check your internet connection.",null,"Application Form","OK");
        }
    }

}


    this.cameraClicked = function(CUST_TYPE){
        debug("in cameraClicked : " + CUST_TYPE);
        if(CUST_TYPE == 'PR' && pDet.isSelf){
            //Proposer Photo not allowed for Self
            debug("Proposer Photo not allowed for Self")
        }
        else{
            personalDetailsService.openCamera(CUST_TYPE, "Profile").then(function(resp){
                if(!!resp && resp)
                {
                    if(CUST_TYPE == 'IN')
                        ProfileFlag.Insured = 'Y';
                    else if(CUST_TYPE == 'PR')
                        ProfileFlag.Proposer = 'Y';
                }
            });
        }
    };

    this.gallaryClicked = function(CUST_TYPE){
        debug("in gallaryClicked : " + CUST_TYPE);
        if(CUST_TYPE == 'PR' && pDet.isSelf){
            //Proposer Photo not allowed for Self
            debug("Proposer Photo not allowed for Self")
        }
        else{
            personalDetailsService.openCamera(CUST_TYPE, "GalleryProfile").then(function(resp){
                if(!!resp && resp)
                {
                    if(CUST_TYPE == 'IN')
                        ProfileFlag.Insured = 'Y';
                    else if(CUST_TYPE == 'PR')
                        ProfileFlag.Proposer = 'Y';
                }
            });
        }
    };

    this.gallaryClicked = function(CUST_TYPE){
        debug("in gallaryClicked : " + CUST_TYPE);
        if(CUST_TYPE == 'PR' && pDet.isSelf){
            //Proposer Photo not allowed for Self
            debug("Proposer Photo not allowed for Self")
        }
        else{
            personalDetailsService.openCamera(CUST_TYPE, "GalleryProfile").then(function(resp){
                if(!!resp && resp)
                {
                    if(CUST_TYPE == 'IN')
                        ProfileFlag.Insured = 'Y';
                    else if(CUST_TYPE == 'PR')
                        ProfileFlag.Proposer = 'Y';
                }
            });
        }
    };

    //view images in Native Gridview
   this.viewimage = function(CUST_TYPE){
       //check for existing image
       debug("CUST_TYPE : " + CUST_TYPE);
       personalDetailsService.getDocNameFromDocID(CUST_TYPE).then(
           function(docData){
               if(!!docData){
                   debug("exists : " + docData.rows.item(0).DOC_NAME);
                   var docName = docData.rows.item(0).DOC_NAME;
                   if(!!docName){
                       if(docName.indexOf(".pdf") == -1)
                           personalDetailsService.callNativeViewImage("viewimageProfile", CUST_TYPE);
                       else{
                           debug("cannot view pdf");
                           navigator.notification.alert("Cannot View PDF",function(){CommonService.hideLoading();},"Document Upload","OK");
                       }
                   }
               }
               else{
                  console.log("not exists");
                   debug("not exist");
                   navigator.notification.alert("No Image found",function(){CommonService.hideLoading();},"Document Upload","OK");
               }
           }
       );

  };

this.onChangePropDOB = function()
{
var PropAgeProofDob = CommonService.formatDobToDb(pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB);
var PropDOb = CommonService.formatDobToDb(pDet.personalDetBean.PROPOSER_DOB);
	if(!!pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB)
	{
		 if(pDet.isSelf == false && PropAgeProofDob != PropDOb)
		{
			console.log("inside non-self case Proposer tab"+pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB);
            //commented not working in ios 2-1-17
			pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB = "";
			navigator.notification.alert("Please provide alternate Age Proof document due to DOB mismatch",function(){CommonService.hideLoading();},"Application","OK");
		}
	}
	else
    {
        pDet.personalDetBean.PROPOSER_AGE_PROOF_DOB = "";
    }

}

CommonService.hideLoading();
}]);


personalDetailsModule.service('personalDetailsService',['$q','CommonService','LoadApplicationScreenData','ApplicationFormDataService','LoginService', function($q, CommonService, LoadApplicationScreenData,ApplicationFormDataService,LoginService){

var pDetSrv = this;
var imagePath = "/FromClient/APP/IMAGES/PHOTOS/";
pDetSrv.ProfileDocId = {};

this.profilePicDocID = function(){
debug("profilePicDocID 1");
var dfd = $q.defer();
var Profile = {};
var whereClauseObj = {};
		whereClauseObj.ISACTIVE = '1';
		whereClauseObj.MPPROOF_CODE = 'RP';
		whereClauseObj.MPDOC_CODE = 'PH';
		whereClauseObj.CUSTOMER_CATEGORY = 'IN';
		debug("profilePicDocID 2");
	CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, null).then(
				function(res){
					console.log("LP_DOC_PROOF_MASTER PROFILE:IN" + res.rows.length);
					if(!!res && res.rows.length>0){
						Profile.Insured = CommonService.resultSetToObject(res);
						pDetSrv.ProfileDocId = Profile;
						//Proposer
						whereClauseObj.CUSTOMER_CATEGORY = 'PR';
						CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, null).then(
								function(res){
									console.log("LP_DOC_PROOF_MASTER PROFILE:PR" + res.rows.length);
									if(!!res && res.rows.length>0){
										Profile.Proposer = CommonService.resultSetToObject(res);
										pDetSrv.ProfileDocId = Profile;
										dfd.resolve(Profile);
									}
									else
										dfd.resolve(Profile);
								}
							);
					}
					else
						dfd.resolve(Profile);
				});

		return dfd.promise;

}

this.openCamera = function(CUST_TYPE, source){
var dfd = $q.defer();
debug("Service openCamera : " + CUST_TYPE + "\t source : " + source);
var DocID;
var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
if(CUST_TYPE == 'IN')
	DocID = pDetSrv.ProfileDocId.Insured.DOC_ID;
else
	DocID = pDetSrv.ProfileDocId.Proposer.DOC_ID;
var fileName = AGENT_CD+"_"+APP_ID+"_"+DocID;

	 cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
                if(success.code == "1"){
                    //read profile image..
                    pDetSrv.insertProfileDocUpload(success, fileName, DocID,CUST_TYPE).then(function(resp){

                        if(!!resp && resp!="")
                            dfd.resolve(true);
                        else
                            dfd.resolve(false);
                    });

                }
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
                 dfd.resolve(false);
            },//function error..
            "PluginHandler",'camera',[imagePath,fileName,source]);
return dfd.promise;
}

this.insertProfileDocUpload = function(success, fileName, DocID,CUST_TYPE){
var dfd = $q.defer();
var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
var POLICY_NO = ApplicationFormDataService.applicationFormBean.POLICY_NO;
var doc_cap = CommonService.getRandomNumber();
var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID,DOC_CAP_REF_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
var parameterList = [DocID,LoginService.lgnSrvObj.userinfo.AGENT_CD,"PHOTOGRAPH",APP_ID,POLICY_NO,fileName+".jpg",CommonService.getCurrDate(),"0","N","N",doc_cap,null];
debug("query : " + query);
debug("parameterList : " + JSON.stringify(parameterList));
CommonService.transaction(db,
	function(tx){
		CommonService.executeSql(tx, query, parameterList,
			function(tx,res){
				console.log("Profile saved"+DocID);
				dfd.resolve("S");
			},
			function(tx,err){
				dfd.resolve(null);
			}
		);
	},
	function(err){
		dfd.resolve(null);
	});
return dfd.promise;
}


this.checkExistingPhoto = function(){
debug("checkExistingPhoto :"+JSON.stringify(pDetSrv.ProfileDocId));
var dfd = $q.defer();
var APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var isProfileDone = {};
var query = "(select DOC_ID from LP_DOC_PROOF_MASTER where MPPROOF_CODE = 'RP' AND MPDOC_CODE = 'PH' AND CUSTOMER_CATEGORY = ?)";
var sql = "select * from LP_DOCUMENT_UPLOAD where DOC_CAT = 'PHOTOGRAPH' AND DOC_CAT_ID = '"+APP_ID+"' AND AGENT_CD = '"+AGENT_CD+"' AND DOC_ID = "+query+"";
debug("photo existing is "+sql);
var params = ['IN'];

CommonService.transaction(db,
	function(tx){
		CommonService.executeSql(tx, sql, params,
			function(tx,res){
			    console.log(sql+"DOC_UPLOAD:IN" + res.rows.length);
                isProfileDone.Insured = null;
                isProfileDone.Proposer = null;
                if(!!res && res.rows.length>0){
                    isProfileDone.Insured = 'Y';
                params = ['PR'];
                CommonService.executeSql(tx, sql, params,
                			function(tx,res){
                                if(!!res && res.rows.length>0){
                                    isProfileDone.Proposer = 'Y';
                                    dfd.resolve(isProfileDone);
                                }
                                else
                                    dfd.resolve(isProfileDone);
                			},
                			function(tx,err){
                				dfd.resolve(null);
                			});
                }
                else
                {
                params = ['PR'];
				CommonService.executeSql(tx, sql, params,
							function(tx,res){
								if(!!res && res.rows.length>0){
									isProfileDone.Proposer = 'Y';
									dfd.resolve(isProfileDone);
								}
								else
									dfd.resolve(isProfileDone);
							},
							function(tx,err){
								dfd.resolve(null);
							});


                }
                    //dfd.resolve(isProfileDone);
			},
			function(tx,err){
				debug("error in query");
				dfd.resolve(null);
			}
		);
	},
	function(err){
		debug("error in transaction");
		dfd.resolve(null);
	});

return dfd.promise;

}

this.getDocNameFromDocID = function(CUST_TYPE){
    var dfd = $q.defer();
    debug("CUST_TYPE : " + CUST_TYPE);
    var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    var APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    var DOC_ID;
    if(CUST_TYPE == 'IN')
    	DOC_ID = pDetSrv.ProfileDocId.Insured.DOC_ID;
    else
    	DOC_ID = pDetSrv.ProfileDocId.Proposer.DOC_ID;
    var query = "select DOC_NAME from LP_DOCUMENT_UPLOAD where DOC_ID = ? and AGENT_CD = ? and DOC_CAT_ID = ?"
    var parameters = [DOC_ID,AGENT_CD,APPLICATION_ID];
    debug("paramaterss   --->"+parameters);
    if(!!DOC_ID){
        CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,query,parameters,
                        function(tx,res){
                            debug("DOC EXIST DATA res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                dfd.resolve(res);
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            //CommonService.hideLoading();
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
        );
    }else{
        //for testing...
        dfd.resolve(null);
    }
    return dfd.promise;
};

this.callNativeViewImage = function(source, CUST_TYPE){
    var DOC_ID;
    var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    var APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    if(CUST_TYPE == 'IN')
    	DOC_ID = pDetSrv.ProfileDocId.Insured.DOC_ID;
    else
    	DOC_ID = pDetSrv.ProfileDocId.Proposer.DOC_ID;
    var fileName = AGENT_CD + "_" + APPLICATION_ID + "_" + DOC_ID;
    debug("File name : " + fileName);

    //call native class...
    cordova.exec(
        function(success){
            debug("success -->"+JSON.stringify(success));
            if(success.code == 1){

            }else{
                debug("code from camera  "+success.code);
            }
        },//function success
        function(error){
            debug("error -->"+JSON.stringify(error));
        },//function error..
        "PluginHandler",
        'camera',
        [imagePath,fileName,source]
    );
};

}]);
