personalDetailsModule.service('validationService',['$q','$state',function($q,$state){

	this.validateExpiryDate = function(EXPIRY_DATE){
		console.log("inside validateInsExpiryDate service");
		var valid = {};
		valid.Flag = true;
		valid.param = null;
		valid.msg = "";

		var PassportDate = new Date(EXPIRY_DATE);
		var currDate = new Date();
		PassportDate.setMonth(PassportDate.getMonth()+1);
		currDate.setMonth(currDate.getMonth()+1);
		console.log("PassportMonth is::"+PassportDate.getMonth()+"CurrentMonth is::"+currDate.getMonth());

		if(!!EXPIRY_DATE) {
			console.log("inside 1st if");
			if(PassportDate.getFullYear() > currDate.getFullYear()) {
				console.log("Passport Year::"+PassportDate.getFullYear()+"CurrDateYear::"+PassportDate.getFullYear());
				valid = true;
			}
			else if(PassportDate.getFullYear() == currDate.getFullYear()) {
				console.log("PassportDate.getMonth::"+PassportDate.getMonth()+"currDate.getMonth::"+currDate.getMonth());
				if(PassportDate.getMonth() > currDate.getMonth()) {
					valid = true;
				}
				else if(PassportDate.getMonth() == currDate.getMonth()) {
					if(PassportDate.getDate() > currDate.getDate()) {
						valid = true;
					}
					else {
						console.log("inside year");
						valid = false;
					}
				}
				else if(PassportDate.getMonth() < currDate.getMonth()) {
                   	valid= false;
				}
			}
			else {
				console.log("inside year");
				valid = false;
			}
		}
		else {
			console.log("inside 1st else::::");
			valid = false;
		}
		return valid;
	};


	this.validateIssuanceDate = function(ISSUANCE_DATE){
		console.log("inside validateInsIssuanceDate::");
		var valid = {};
		valid.flag = true;
		valid.param = null;
		valid.msg = "";
		var IssuanceDate = new Date(ISSUANCE_DATE);
		var currDate = new Date();
		IssuanceDate.setMonth(IssuanceDate.getMonth()+1);
		currDate.setMonth(currDate.getMonth()+1);
		currDate.setDate(currDate.getDate()-1);
		var NoOfMonths = (currDate.getFullYear()*12 + currDate.getMonth()) - (IssuanceDate.getFullYear()*12 + IssuanceDate.getMonth());

		console.log("Issuemonth::"+IssuanceDate.getMonth()+"CurrMonth::"+currDate.getMonth()+"IssueYear::"+IssuanceDate.getFullYear()+"CurrYear::"+currDate.getFullYear());
		if(!!ISSUANCE_DATE) {
			if(IssuanceDate.getFullYear() > currDate.getFullYear()) {
				valid.flag = false;
				valid.msg = "Date of issuance cannot be a future date.";
				return valid;
			}
			else if(IssuanceDate.getFullYear() <= currDate.getFullYear()) {
				console.log("NoOfMonths in Year::"+NoOfMonths+"IssuanceDate:"+IssuanceDate);
				if(NoOfMonths > 11) {
					valid.flag = true;
					valid.param = "";
				}
				else if(NoOfMonths == 11) {
					if(IssuanceDate.getDate() < currDate.getDate()) {
						console.log("IssuanceDate.getDate::"+IssuanceDate.getDate()+"currDate.getDate()::"+currDate.getDate());
						valid.flag = true;
						valid="";
					}
					else if(currDate.getDate() == IssuanceDate.getDate()) {
						console.log("Equality Date Check::");
						valid.flag = true;
						valid="";
					}
					else {
						valid.flag = false;
						valid.msg = "Please submit alternative Age Proof document as Driving Licence with age less than 1 year will not be considered as Age Proof document.";
					}
				}
				else {
					console.log("123");
					valid.flag = false;
					valid.msg = "Please submit alternative Age Proof document as Driving Licence with age less than 1 year will not be considered as Age Proof document.";
				}
			}
			else {
				console.log("*******");
				valid.flag = false;
				valid.msg = "Please submit alternative Age Proof document as Driving Licence with age less than 1 year will not be considered as Age Proof document.";
			}
		}
		else {
			valid.flag = false;
		}
		return valid;
	};

	this.validateBillDate = function(BILL_DATE){
		var dfd = $q.defer();
		var valid={};
		valid.param = null;
		valid.Flag = true;
		valid.mesg = "";

		var BillDate = new Date(BILL_DATE);
		var currDate = new Date();
		BillDate.setMonth(BillDate.getMonth());
		currDate.setMonth(currDate.getMonth());
		var NoOfMonths = (currDate.getFullYear()*12 + currDate.getMonth()) - (BillDate.getFullYear()*12 + BillDate.getMonth());
		console.log("No of Months::"+NoOfMonths);

		if(!!BILL_DATE) {
			console.log("BillDate:"+BILL_DATE);
			if(BillDate.getFullYear() == currDate.getFullYear()) {
				console.log("BillDate.getFullYear()***"+BillDate.getFullYear()+"currDate.getFullYear()***"+currDate.getFullYear())
				if(BillDate.getMonth() <= currDate.getMonth()) {
					if(BillDate.getDate() <= currDate.getDate()) {
						console.log("1");
						if(NoOfMonths <= 2) {
							console.log("2");
							valid = true;
						}
						else {
							console.log("3");
							valid = false;
						}
					}
					else {
					console.log("**");
						valid = false;
					}
				}
				else {
				console.log("4");
					valid = false;
				}
			}
			else if(parseInt(BillDate.getFullYear()) < parseInt(currDate.getFullYear())) {
				console.log("NoofMonths::**"+NoOfMonths);
				if(NoOfMonths <= 2) {
					valid = true;
				}
				else {
					console.log("5");
					valid = false;
				}
			}
			else {
				console.log("6 BillDateYear::"+BillDate.getFullYear()+"CurrDateYear::"+currDate.getFullYear()+"BillDateMonth::"+BillDate.getMonth()+"currMonth::"+currDate.getMonth());
				valid = false;
			}
		}
		else {
			console.log("7::"+BILL_DATE);
			valid = false;
		}
		return valid;
	};
}]);

