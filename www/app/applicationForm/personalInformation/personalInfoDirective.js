personalInfoModule.controller('persInfoCtrl',['persInfoService','PersonalInfoService',function(persInfoService,PersonalInfoService){
    debug("personl controller loaded");

    this.perSideTabArr = [
        {'type':'personalDetails','name':'Personal Details'},
        {'type':'contactDetails','name':'Contact Details'},
        {'type':'addressDetails','name':'Address Details'},
        {'type':'eduQualDetails','name':'Education Qual. & Occupation Details'},
        {'type':'otherDetails','name':'Other Details'}
    ]

    this.activeTab = persInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        PersonalInfoService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.perSideTabArr));
}]);

personalInfoModule.service('persInfoService',[function(){
    debug("personal service loaded");

    this.activeTab = "personalDetails";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

personalInfoModule.directive('piTimeline',[function() {
        "use strict";
        debug('in piTimeline');
        return {
            restrict: 'E',
            controller: "persInfoCtrl",
            controllerAs: "pstc",
            bindToController: true,
            templateUrl: "applicationForm/personalInformation/personalInfoTimeline.html"
        };
}]);
