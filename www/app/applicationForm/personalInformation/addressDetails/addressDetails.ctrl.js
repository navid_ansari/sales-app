addressDetailsModule.controller('AddressDetailsCtrl', ['$q','$state','$filter','CommonService','ApplicationFormDataService','PersonalInfoService','ExistingAppData', 'NRIList','OCIList', 'LoadAppProofList', 'NationalityList', 'ResidentCountryList', 'BirthCountryList', 'StateList','CityList','AddrProofList', 'LeadData','EKYCData','EKYCOldData','persInfoService', 'SISTermData','validationService','FHRData','DeDupeData',function($q, $state, $filter, CommonService, ApplicationFormDataService,PersonalInfoService, ExistingAppData, NRIList,OCIList, LoadAppProofList, NationalityList, ResidentCountryList, BirthCountryList, StateList, CityList, AddrProofList, LeadData, EKYCData, EKYCOldData, persInfoService, SISTermData,validationService,FHRData, DeDupeData){

var addrDet = this;
this.addressDetBean = {};

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


ApplicationFormDataService.applicationFormBean.addressDetBean = this.addressDetBean;
addrDet.click = false;
addrDet.PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
addrDet.insEKYCFlag = false;
addrDet.proEKYCFlag = false;
addrDet.disableResStatus = false;
addrDet.disableCountryOfRes = false;
addrDet.disableState = false;
addrDet.disableCity = false;

this.residentStatusList = [
    {"RESIDENT_STATUS": "Select ", "RESIDENT_CD":null},
    {"RESIDENT_STATUS": "Resident Indian", "RESIDENT_CD":"RI"},
    {"RESIDENT_STATUS": "Non Resident Indian", "RESIDENT_CD":"NRI"},
    {"RESIDENT_STATUS": "Foreign National", "RESIDENT_CD":"FN"},
    {"RESIDENT_STATUS": "Overseas Citizen of India", "RESIDENT_CD":"OCI"}

];
ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
addrDet.pinRegex = PINCODE_REG;
addrDet.freeTxtRegex = FREE_TEXT_REGEX;
this.nationalityList = NationalityList;
this.residentCountryList = ResidentCountryList;
this.birthCountryList = BirthCountryList;
this.insStateList = StateList;
this.insPStateList = StateList;
this.insCityList = CityList;
this.insPCityList = CityList;
this.insAddrProofList = AddrProofList;
addrDet.InsFATFFlag = 'N';
addrDet.ProFATFFlag = 'N';
this.proStateList = StateList;
this.proPStateList = StateList;
this.proCityList = CityList;
this.proPCityList = CityList;

this.proAddrProofList = AddrProofList;
this.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
console.log("SISTermData :"+JSON.stringify(SISTermData)+"\n Existing DATA:"+JSON.stringify(ExistingAppData.applicationPersonalInfo)+"inside AddressDetailsCtrl"+JSON.stringify(ApplicationFormDataService));

var countryArray = $filter('filter')(this.birthCountryList, { "COUNTRY_NAME": "INDIA"  }, true)[0];
debug("India Country "+JSON.stringify(countryArray));
var indiaPos = this.birthCountryList.indexOf(countryArray );
debug("India Country Pos"+indiaPos);

if(!EKYCOldData || (!EKYCOldData.Insured && !EKYCOldData.Proposer) && (!EKYCOldData.Insured.OPP_ID && !EKYCOldData.Proposer.OPP_ID))
    {
      debug("Old EKYC data not found");
    }
    else {
      if(!EKYCData || (!EKYCData.Insured.EKYC_ID && !EKYCData.Proposer.EKYC_ID))
        {
          debug("Old EKYC data found");
          EKYCData = EKYCOldData;
        }
        else
          debug("USE NEW EKYC data");
  }

//Insured
this.addressDetBean.INSURED_RESIDENT_STATUS = this.residentStatusList[0];
this.addressDetBean.INSURED_NATIONALITY = this.nationalityList[0];
this.addressDetBean.INSURED_RESIDENT_COUNTRY = this.residentCountryList[0];
this.addressDetBean.INSURED_BIRTH_COUNTRY = this.birthCountryList[indiaPos];
this.addressDetBean.INSURED_CURR_STATE = this.insStateList[0];
this.addressDetBean.INSURED_PERM_STATE = this.insPStateList[0];
this.addressDetBean.INSURED_CURR_CITY = this.insCityList[0];
this.addressDetBean.INSURED_PERM_CITY = this.insPCityList[0];
this.addressDetBean.INSURED_ADDRESS_PROOF = this.insAddrProofList[0];
//Proposer
this.addressDetBean.PROPOSER_RESIDENT_STATUS = this.residentStatusList[0];
this.addressDetBean.PROPOSER_NATIONALITY = this.nationalityList[0];
this.addressDetBean.PROPOSER_RESIDENT_COUNTRY = this.residentCountryList[0];

if(addrDet.isSelf)
    this.addressDetBean.PROPOSER_BIRTH_COUNTRY = this.birthCountryList[0];
else
    this.addressDetBean.PROPOSER_BIRTH_COUNTRY = this.birthCountryList[indiaPos];

this.addressDetBean.PROPOSER_CURR_STATE = this.proStateList[0];
this.addressDetBean.PROPOSER_PERM_STATE = this.proPStateList[0];
this.addressDetBean.PROPOSER_CURR_CITY = this.proCityList[0];
this.addressDetBean.PROPOSER_PERM_CITY = this.proPCityList[0];
this.addressDetBean.PROPOSER_ADDRESS_PROOF = this.proAddrProofList[0];

debug("insPStateList is "+JSON.stringify(this.insPStateList));
debug("insStateList is "+JSON.stringify(this.insStateList));
debug("insCityList is "+JSON.stringify(this.insCityList));
debug("insPCityList  is "+JSON.stringify(this.insPCityList));
//Added by Amruta for Date of bill Validation
this.validateBillDate = function(){
validationService.validateBillDate(addrDet.addressDetBean.INSURED_BILL_DATE).then(function(valid){
        if(!!valid){
        console.log("valid value is::"+JSON.stringify(!!valid));
            if(valid.Flag == false){
                console.log("valid is::"+JSON.stringify(valid.Flag));
                //addrDet.addressDetBean.INSURED_BILL_DATE = valid.param;
            }
        }
    });
}

this.validatePropBillDate = function(){
validationService.validateBillDate(addrDet.addressDetBean.PROPOSER_BILL_DATE).then(function(valid){
        if(!!valid){
            if(valid.Flag == false){
                //addrDet.addressDetBean.PROPOSER_BILL_DATE = valid.param;
            }
        }

    });

}

console.log("EKYCData:here"+JSON.stringify(EKYCData));

if(!!EKYCData && !!EKYCData.Insured && !!EKYCData.Insured.CONSUMER_AADHAR_NO)
	addrDet.insEKYCFlag = ((EKYCData.Insured.EKYC_FLAG == 'Y') ? true:false);

if(!!EKYCData && !!EKYCData.Proposer && !!EKYCData.Proposer.CONSUMER_AADHAR_NO)
	addrDet.proEKYCFlag = ((EKYCData.Proposer.EKYC_FLAG == 'Y') ? true:false);


this.onInsuredChangeState = function(SCODE){
    var dfd = $q.defer();
    var stateCode;
    if(SCODE !=undefined)
        stateCode = SCODE;
    else
        stateCode = addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE;
    LoadAppProofList.getCityList(stateCode).then(
        function(CityList){
            addrDet.insCityList = CityList;
            console.log("Ins appCity::"+JSON.stringify(CityList));
            addrDet.addressDetBean.INSURED_CURR_CITY = CityList[0];
            if(addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME)
               {
                addrDet.addressDetBean.INSURED_PERM_STATE = addrDet.addressDetBean.INSURED_CURR_STATE;
                   addrDet.onInsuredChangePState(addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE).then(
                        function(PCityList){
                        console.log("setting on change");
                        addrDet.addressDetBean.INSURED_PERM_CITY = addrDet.addressDetBean.INSURED_CURR_CITY;
                        }
                    );
                }
            dfd.resolve(addrDet.insCityList);

        }
    );

    return dfd.promise;
};

this.onProposerChangeState = function(SCODE){
var dfd = $q.defer();
var stateCode;
if(SCODE!=undefined)
    stateCode = SCODE;
else
    stateCode = addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE;

         console.log("Proposer Curr State: " + JSON.stringify(addrDet.addressDetBean.PROPOSER_CURR_STATE));
            LoadAppProofList.getCityList(stateCode).then(
                function(CityList){
                    addrDet.proCityList = CityList;
                    console.log("Pro appCity::"+JSON.stringify(CityList));
                    addrDet.addressDetBean.PROPOSER_CURR_CITY = CityList[0];
                    if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME)
                       {
                        addrDet.addressDetBean.PROPOSER_PERM_STATE = addrDet.addressDetBean.PROPOSER_CURR_STATE;
                           addrDet.onProposerChangePState(addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE).then(
                                function(PCityList){
                                console.log("setting on change");
                                addrDet.addressDetBean.PROPOSER_PERM_CITY = addrDet.addressDetBean.PROPOSER_CURR_CITY;
                                }
                            );
                        }
                    dfd.resolve(addrDet.proCityList);

                }
            );
if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
          this.copyProposerAddressToInsured();
            }

    return dfd.promise;
}

this.onInsuredChangeCity = function(){

  if(addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME!=undefined && addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME)
       addrDet.addressDetBean.INSURED_PERM_CITY = addrDet.addressDetBean.INSURED_CURR_CITY;

}

this.onProposerChangeCity = function(){

  console.log("addrDet.addressDetBean.isPermanent::"+addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME);
      if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME)
           addrDet.addressDetBean.PROPOSER_PERM_CITY = addrDet.addressDetBean.PROPOSER_CURR_CITY;

      if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
                   this.copyProposerAddressToInsured();
                   }
}

this.onProposerChangePCity = function(){
 if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
                   this.copyProposerAddressToInsured();
                   }
}

this.onInsuredChangePState = function(SCODE){
    var dfd = $q.defer();
    var pStateCode;
   if(SCODE!=undefined)
     pStateCode = SCODE;
   else
     pStateCode = addrDet.addressDetBean.INSURED_PERM_STATE.STATE_CODE;

    console.log("Pstate: " + JSON.stringify(addrDet.addressDetBean.INSURED_PERM_STATE));
    LoadAppProofList.getCityList(pStateCode).then(
        function(PCityList){
            addrDet.insPCityList = PCityList;
            console.log("Ins PCity::"+JSON.stringify(PCityList));
            addrDet.addressDetBean.INSURED_PERM_CITY = PCityList[0];
            dfd.resolve(addrDet.insPCityList);
        }
    );

    return dfd.promise;
};

this.onProposerChangePState = function(SCODE){
var dfd = $q.defer();
var pStateCode;
if(SCODE!=undefined)
pStateCode = SCODE;
else
pStateCode = addrDet.addressDetBean.PROPOSER_PERM_STATE.STATE_CODE;

     console.log("Pstate: " + JSON.stringify(addrDet.addressDetBean.PROPOSER_PERM_STATE));
        LoadAppProofList.getCityList(pStateCode).then(
            function(PCityList){
                addrDet.proPCityList = PCityList;
                console.log("Pr PCity::"+JSON.stringify(PCityList));
                addrDet.addressDetBean.PROPOSER_PERM_CITY = PCityList[0];
                dfd.resolve(addrDet.proPCityList);
            }
        );

if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
                   this.copyProposerAddressToInsured();
                   }

    return dfd.promise;

}

this.setEKYCState = function(CUST_TYPE){
//toUpperCase
        if(CUST_TYPE == 'IN'){
            var STATE = EKYCData.Insured.STATE;
            var CITY = EKYCData.Insured.CITY;
            STATE = STATE.toUpperCase();
            CITY = CITY.toUpperCase();
            var STATE_SELECT = $filter('filter')(addrDet.insStateList, {"STATE_DESC": STATE}, true)[0];
            if(!!STATE_SELECT)
            {
                addrDet.addressDetBean.INSURED_CURR_STATE = STATE_SELECT;
                console.log("CITY :"+CITY+" : STATE_SELECT :"+JSON.stringify(STATE_SELECT));
                addrDet.onInsuredChangeState(STATE_SELECT.STATE_CODE).then(function(){
					var cityList = $filter('filter')(addrDet.insCityList, {"CITY_DESC": CITY}, true);
					var CITY_SELECT = (!!cityList && cityList.length>0)?cityList[0]:$filter('filter')(addrDet.insCityList, {"CITY_CODE": "OT"}, true)[0];
					console.log("CITY_SELECT :"+JSON.stringify(CITY_SELECT));
					if(!!CITY_SELECT)
						addrDet.addressDetBean.INSURED_CURR_CITY = CITY_SELECT;
                });
            }
        }
        else if(CUST_TYPE == 'PR'){

            var STATE = EKYCData.Proposer.STATE;
            var CITY = EKYCData.Proposer.CITY;
            STATE = STATE.toUpperCase();
            CITY = CITY.toUpperCase();
            var STATE_SELECT = $filter('filter')(addrDet.proStateList, {"STATE_DESC": STATE}, true)[0];
            if(!!STATE_SELECT)
            {
                addrDet.addressDetBean.PROPOSER_CURR_STATE = STATE_SELECT;
                console.log("CITY :"+CITY+" : STATE_SELECT :"+JSON.stringify(STATE_SELECT));
                addrDet.onProposerChangeState(STATE_SELECT.STATE_CODE).then(function(){
					var cityList = $filter('filter')(addrDet.proCityList, {"CITY_DESC": CITY}, true);
					var CITY_SELECT = (!!cityList && cityList.length>0)?cityList[0]:$filter('filter')(addrDet.proCityList, {"CITY_CODE": "OT"}, true)[0];
					console.log("CITY_SELECT :"+JSON.stringify(CITY_SELECT));
					if(!!CITY_SELECT)
						addrDet.addressDetBean.PROPOSER_CURR_CITY = CITY_SELECT;
                });
            }

        }
}

this.ekycAddressLogic = function(ekycAddr) {
	var addressLine1 = "";
	var addressLine2 = "";
	var addressLine3 = "";
	var landmark = "";
	var carryForward = "";

	var areaDone = false;
	var distSubDistDone = false;

	var i = 0;

	addressLine1 += ((!!ekycAddr.BUILDING)?(ekycAddr.BUILDING + " "):"");
	addressLine1 += ((!!ekycAddr.STREET)?(ekycAddr.STREET + " "):"");

	if(!addressLine1 || addressLine1.length<45){
		addressLine1 += ekycAddr.AREA || "";
		areaDone = true;
	}
    if(!addressLine1 || addressLine1.length < 45){
          addressLine1 += ekycAddr.DISTRICT || "";
          addressLine1 += ekycAddr.SUBDISTRICT || "";
          distSubDistDone = true;
    }
	if(!!addressLine1 && addressLine1.length>45){
		var addr1SplitArr = addressLine1.split(" ");
		var _addressLine1 = "";
		carryForward = "";
		for(i=0;i<addr1SplitArr.length;i++){
			if((!carryForward) && ((_addressLine1 + addr1SplitArr[i]).length < 45)) {
				_addressLine1 += addr1SplitArr[i] + " ";
			}
			else {
				carryForward += addr1SplitArr[i] + " ";
			}
		}
		addressLine1 = (!!_addressLine1)?(_addressLine1.trim()):"";
	}
	else{
		carryForward = "";
	}


	addressLine2 += carryForward + " ";

	if(!addressLine2 || addressLine2.length<45){
		if(!areaDone)
			addressLine2 += ekycAddr.AREA || "";
		if(!addressLine2){
			addressLine2 += ekycAddr.DISTRICT || "";
			addressLine2 += ekycAddr.SUBDISTRICT || "";
			addressLine2 = addressLine2 || null;
			distSubDistDone = true;
		}
	}
	if(!!addressLine2 && addressLine2.length>45){
		var addr2SplitArr = addressLine2.split(" ");
		var _addressLine2 = "";
		carryForward = "";
		for(i=0;i<addr2SplitArr.length;i++){
			if((!carryForward) && ((_addressLine2 + addr2SplitArr[i]).length < 45)) {
				_addressLine2 += addr2SplitArr[i] + " ";
			}
			else {
				carryForward += addr2SplitArr[i] + " ";
			}
		}
		addressLine2 = (!!_addressLine2)?(_addressLine2.trim()):null;
	}
	else{
		carryForward = "";
	}

	addressLine3 += carryForward + " ";
	if(!addressLine3 || addressLine3.length<45){
		if(!distSubDistDone){
			addressLine3 += ekycAddr.DISTRICT || "";
			addressLine3 += ekycAddr.SUBDISTRICT || "";
			addressLine3 = addressLine3 || null;
		}
	}
	if(!!addressLine3 && addressLine3.length>45){
		var addr3SplitArr = addressLine3.split(" ");
		var _addressLine3 = "";
		carryForward = "";
		for(i=0;i<addr3SplitArr.length;i++){
			if((!carryForward) && ((_addressLine3 + addr3SplitArr[i]).length < 45)) {
				_addressLine3 += addr3SplitArr[i] + " ";
			}
			else {
				carryForward += addr3SplitArr[i] + " ";
			}
		}
		addressLine3 = (!!_addressLine3)?(_addressLine3.trim()):null;
	}
	else{
		carryForward = "";
	}


	landmark += carryForward + " ";
	if(!landmark || landmark.length<45){
		landmark += ekycAddr.LANDMARK || "";
	}
	if(!!landmark && landmark.length>45){
		var landmarkSplitArr = landmark.split(" ");
		var _landmark = "";
		carryForward = "";
		for(i=0;i<landmarkSplitArr.length;i++){
			if((!carryForward) && ((_landmark + landmarkSplitArr[i]).length < 45)) {
				_landmark += landmarkSplitArr[i] + " ";
			}
			else {
				carryForward += landmarkSplitArr[i] + " ";
			}
		}
		landmark = (!!_landmark)?(_landmark.trim()):null;
	}

	return {"addressLine1": addressLine1, "addressLine2": addressLine2, "addressLine3": addressLine3, "landmark": landmark, "carryForward": carryForward, "pincode": ekycAddr.PIN};
};

this.setEKYCAddress = function(CUST_TYPE){
	var type = (CUST_TYPE=="IN"?"Insured":"Proposer");
	var ekycAddr = this.ekycAddressLogic(EKYCData[type]);
	if(CUST_TYPE=="IN"){
		addrDet.addressDetBean.INSURED_CURR_ADDR1 = ((!!ekycAddr.addressLine1)?(ekycAddr.addressLine1.trim() || null):null);
		debug("addrDet.addressDetBean.INSURED_CURR_ADDR1: " + addrDet.addressDetBean.INSURED_CURR_ADDR1);
		addrDet.addressDetBean.INSURED_CURR_ADDR2 = ((!!ekycAddr.addressLine2)?(ekycAddr.addressLine2.trim() || null):null);
		debug("addrDet.addressDetBean.INSURED_CURR_ADDR2: " + addrDet.addressDetBean.INSURED_CURR_ADDR2);
		addrDet.addressDetBean.INSURED_CURR_ADDR3 = ((!!ekycAddr.addressLine3)?(ekycAddr.addressLine3.trim() || null):null);
		debug("addrDet.addressDetBean.INSURED_CURR_ADDR3: " + addrDet.addressDetBean.INSURED_CURR_ADDR3);
		addrDet.addressDetBean.INSURED_CURR_LANDMARK = ((!!ekycAddr.landmark)?(ekycAddr.landmark.trim() || null):null);
		debug("addrDet.addressDetBean.INSURED_CURR_LANDMARK: " + addrDet.addressDetBean.INSURED_CURR_LANDMARK);
		addrDet.addressDetBean.INSURED_CURR_PINCODE = (!!ekycAddr.pincode?(parseInt(ekycAddr.pincode)):null);
		debug("addrDet.addressDetBean.INSURED_CURR_PINCODE: " + addrDet.addressDetBean.INSURED_CURR_PINCODE);
	}
	else if(CUST_TYPE=="PR"){
		addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = ((!!ekycAddr.addressLine1)?(ekycAddr.addressLine1.trim() || null):null);
		debug("addrDet.addressDetBean.PROPOSER_CURR_ADDR1: " + addrDet.addressDetBean.PROPOSER_CURR_ADDR1);
		addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = ((!!ekycAddr.addressLine2)?(ekycAddr.addressLine2.trim() || null):null);
		debug("addrDet.addressDetBean.PROPOSER_CURR_ADDR2: " + addrDet.addressDetBean.PROPOSER_CURR_ADDR2);
		addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = ((!!ekycAddr.addressLine3)?(ekycAddr.addressLine3.trim() || null):null);
		debug("addrDet.addressDetBean.PROPOSER_CURR_ADDR3: " + addrDet.addressDetBean.PROPOSER_CURR_ADDR3);
		addrDet.addressDetBean.PROPOSER_CURR_LANDMARK = ((!!ekycAddr.landmark)?(ekycAddr.landmark.trim() || null):null);
		debug("addrDet.addressDetBean.PROPOSER_DISTRICT_LANDMARK: " + addrDet.addressDetBean.PROPOSER_CURR_LANDMARK);
		addrDet.addressDetBean.PROPOSER_CURR_PINCODE = (!!ekycAddr.pincode?(parseInt(ekycAddr.pincode)):null);
		debug("addrDet.addressDetBean.PROPOSER_CURR_PINCODE: " + addrDet.addressDetBean.PROPOSER_CURR_PINCODE);
	}
	debug("Excess address: " + ekycAddr.carryForward);
}

this.setEKYCAddress_OLD = function(CUST_TYPE){

var ADDRESS_LINE1 = "";
var _LINE1 = "";
var ADDRESS_LINE2 = "";
var _LINE2 = "";
var ADDRESS_LINE3 = "";
var _LINE3 = "";
var ADDRESS_LMARK = "";

    if(CUST_TYPE == 'IN'){
        ADDRESS_LINE1 = ((!!EKYCData.Insured.BUILDING)?(EKYCData.Insured.BUILDING + " "):"") +  (((!!EKYCData.Insured.STREET)?EKYCData.Insured.STREET:"") + "");

         if(ADDRESS_LINE1 == undefined || ADDRESS_LINE1 == " ")
           ADDRESS_LINE1 = EKYCData.Insured.AREA || "";
         else
           ADDRESS_LINE2 = EKYCData.Insured.AREA || "" /*+ " " +EKYCData.Insured.SUBDISTRICT;*/;

          if(!!EKYCData.Insured.SUBDISTRICT)
    		    ADDRESS_LINE2 = ADDRESS_LINE2 + ((!!EKYCData.Insured.AREA)?(EKYCData.Insured.AREA + " "):"") + (EKYCData.Insured.SUBDISTRICT || "");
          if(!!EKYCData.Insured.DISTRICT)
		        ADDRESS_LINES3 = ADDRESS_LINE3 + " " + EKYCData.Insured.DISTRICT;

          var ADDR_SIZE = ADDRESS_LINE1.length;
           console.log("ADDR_SIZE1 :"+ADDR_SIZE);
        if(ADDR_SIZE > 45)
        {
          _LINE1 = ADDRESS_LINE1.substring(45, ADDR_SIZE);
          ADDRESS_LINE2 = _LINE1 + " " + ADDRESS_LINE2;
          ADDR_SIZE = ADDRESS_LINE2.length;
          console.log("ADDR_SIZE2 :"+ADDR_SIZE);
          addrDet.addressDetBean.INSURED_CURR_ADDR1 = ADDRESS_LINE1.substring(0, 45);
            if(ADDR_SIZE > 45)
            {
              _LINE2 = ADDRESS_LINE2.substring(45, ADDR_SIZE);
              ADDRESS_LINE3 = _LINE2 + " " + ADDRESS_LINE3;
              ADDR_SIZE = ADDRESS_LINE3.length;
              console.log("ADDR_SIZE3 :"+ADDR_SIZE);
              addrDet.addressDetBean.INSURED_CURR_ADDR2 = ADDRESS_LINE2.substring(0, 45);
              if(ADDR_SIZE > 45)
                addrDet.addressDetBean.INSURED_CURR_ADDR3 = ADDRESS_LINE3.substring(0, 45);
              else
                addrDet.addressDetBean.INSURED_CURR_ADDR3 = ADDRESS_LINE3;
            }
            else
                addrDet.addressDetBean.INSURED_CURR_ADDR2 = ADDRESS_LINE2;

        }
        else
            addrDet.addressDetBean.INSURED_CURR_ADDR1 = ADDRESS_LINE1;


        addrDet.addressDetBean.INSURED_CURR_LANDMARK = EKYCData.Insured.LANDMARK;
        if(!!EKYCData.Insured.PIN && EKYCData.Insured.PIN != "")
            addrDet.addressDetBean.INSURED_CURR_PINCODE = parseInt(EKYCData.Insured.PIN);
    }
    else if(CUST_TYPE == 'PR'){

        ADDRESS_LINE1 = ((!!EKYCData.Proposer.BUILDING)?(EKYCData.Proposer.BUILDING + " "):"") + (((!!EKYCData.Proposer.STREET)?EKYCData.Proposer.STREET:"") + "");

          if(ADDRESS_LINE1 == undefined || ADDRESS_LINE1 == " ")
            ADDRESS_LINE1 = EKYCData.Proposer.AREA || "";
          else
            ADDRESS_LINE2 = EKYCData.Proposer.AREA || "" /*+ " " +EKYCData.Insured.SUBDISTRICT;*/;

     /* ADDRESS_LINE1 = EKYCData.Proposer.BUILDING + " " +EKYCData.Proposer.STREET;
      ADDRESS_LINE2 = EKYCData.Proposer.AREA || "";*/ /*+ " " +EKYCData.Insured.SUBDISTRICT;*/
      if(!!EKYCData.Proposer.SUBDISTRICT)
        ADDRESS_LINE2 = ADDRESS_LINE2 + ((!!EKYCData.Proposer.AREA)?(EKYCData.Proposer.AREA + " "):"") + (EKYCData.Proposer.SUBDISTRICT || "");
      if(!!EKYCData.Proposer.DISTRICT)
        ADDRESS_LINES3 = ADDRESS_LINES + " "+EKYCData.Proposer.DISTRICT;

      var ADDR_SIZE = ADDRESS_LINE1.length;
       console.log("ADDR_SIZE1 :"+ADDR_SIZE);
    if(ADDR_SIZE > 45)
    {
      _LINE1 = ADDRESS_LINE1.substring(45, ADDR_SIZE);
      ADDRESS_LINE2 = _LINE1 + " " + ADDRESS_LINE2;
      ADDR_SIZE = ADDRESS_LINE2.length;
      console.log("ADDR_SIZE2 :"+ADDR_SIZE);
      addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = ADDRESS_LINE1.substring(0, 45);
        if(ADDR_SIZE > 45)
        {
          _LINE2 = ADDRESS_LINE2.substring(45, ADDR_SIZE);
          ADDRESS_LINE3 = _LINE2 + " " + ADDRESS_LINE3;
          ADDR_SIZE = ADDRESS_LINE3.length;
          console.log("ADDR_SIZE3 :"+ADDR_SIZE);
            addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = ADDRESS_LINE2.substring(0, 45);
            if(ADDR_SIZE > 45)
              addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = ADDRESS_LINE3.substring(0, 45);
            else
              addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = ADDRESS_LINE3;
        }
        else
            addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = ADDRESS_LINE2;

    }
    else
        addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = ADDRESS_LINE1;


                addrDet.addressDetBean.PROPOSER_CURR_LANDMARK = EKYCData.Proposer.LANDMARK;
                if(!!EKYCData.Proposer.PIN && EKYCData.Proposer.PIN != "")
                    addrDet.addressDetBean.PROPOSER_CURR_PINCODE = parseInt(EKYCData.Proposer.PIN);

    }
}
this.onInsResidentChange = function(addressForm, isDisabled){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci = {};

if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_STATUS!='Select ' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_NAME!='Select ' && !isDisabled)
 {
        addressForm.state.$setValidity('selectRequired', true);
        addressForm.city.$setValidity('selectRequired', true);
        addressForm.permState.$setValidity('selectRequired', true);
        addressForm.permCity.$setValidity('selectRequired', true);
 }

if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!=undefined && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD !='RI' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!="OCI")
    {
        if(NRIList.Insured.isDigital!= undefined && NRIList.Insured.isDigital == 'Y')
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = NRIList.Insured.nri;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = NRIList.Insured.nri;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = null;
        if(!isDisabled)
            navigator.notification.alert("You will not be allowed to submit the application without uploading the filled NRI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
    }
    else if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == "OCI")
        {
            if(OCIList.Insured.isDigital != undefined && OCIList.Insured.isDigital == 'Y')
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = OCIList.Insured.oci;
            else
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = OCIList.Insured.oci;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = null;
            if(!isDisabled)
                navigator.notification.alert("You will not be allowed to submit the application without uploading the filled OCI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
        }
        else
            {
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = null;
            }

 LoadAppProofList.setFATFFlag(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD, addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY).then(
 function(FATFFlag){
  console.log("Insured FATFFlag :"+FATFFlag/*+"::AppAddrComm:"+addrDet.addressDetBean.AppAddrComm*/);
    addrDet.InsFATFFlag = FATFFlag;
     LoadAppProofList.setAddressType(FATFFlag,addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG,ApplicationFormDataService.SISFormData).then(
         function(addrType){
            console.log("IN addrType:"+addrType);
            LoadAppProofList.getAddrProofList('IN',addrType).then(
            function(AddrProofList){
            console.log("IN:AddrProofList:"+JSON.stringify(AddrProofList));
                addrDet.insAddrProofList = AddrProofList;
                addrDet.addressDetBean.INSURED_ADDRESS_PROOF = addrDet.insAddrProofList[0];
                });
            });
         });



}

//ng-change = "addrDet.addressDetBean.AddSameProposer && addrDet.copyProposerAddressToInsured()"
this.copyProposerAddressToInsured = function(){
console.log("inside copyProposerAddressToInsured");
  if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){

      if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME){addrDet.addressDetBean.PROPOSER_PERM_OtherCityName = addrDet.addressDetBean.PROPOSER_CURR_OtherCityName}
      addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME = addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME;

        if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN'){
             console.log("inside copyProposerAddressToInsured !=RI ");
             addrDet.addressDetBean.INSURED_CURR_STATE_OTHER = addrDet.addressDetBean.PROPOSER_CURR_STATE_OTHER;
             addrDet.addressDetBean.INSURED_CURR_CITY_OTHER = addrDet.addressDetBean.PROPOSER_CURR_CITY_OTHER;
             addrDet.addressDetBean.INSURED_PERM_STATE_OTHER = addrDet.addressDetBean.PROPOSER_PERM_STATE_OTHER;
             addrDet.addressDetBean.INSURED_PERM_CITY_OTHER = addrDet.addressDetBean.PROPOSER_PERM_CITY_OTHER;
             addrDet.addressDetBean.INSURED_NATIONALITY = $filter('filter')(addrDet.nationalityList, {"COUNTRY_CODE": addrDet.addressDetBean.PROPOSER_NATIONALITY.COUNTRY_CODE}, true)[0];
             addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = $filter('filter')(addrDet.residentCountryList, {"COUNTRY_CODE": addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE}, true)[0];
        }
        else{
        console.log("inside copyProposerAddressToInsured == RI ");
        addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE}, true)[0];
        addrDet.addressDetBean.INSURED_PERM_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": addrDet.addressDetBean.PROPOSER_PERM_STATE.STATE_CODE}, true)[0];

        addrDet.onInsuredChangePState(addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE).then(
            function(PCityList){
                console.log("setting PCity")
                addrDet.addressDetBean.INSURED_CURR_CITY = addrDet.addressDetBean.PROPOSER_CURR_CITY;
            }
        );

        addrDet.onInsuredChangePState(addrDet.addressDetBean.PROPOSER_PERM_STATE.STATE_CODE).then(
                function(PCityList){
                    console.log("setting PCity")
                    addrDet.addressDetBean.INSURED_PERM_CITY = addrDet.addressDetBean.PROPOSER_PERM_CITY;
                }
         );

          if(!!addrDet.addressDetBean.PROPOSER_CURR_OtherCityName){addrDet.addressDetBean.INSURED_CURR_OtherCityName = addrDet.addressDetBean.PROPOSER_CURR_OtherCityName}
          if(!!addrDet.addressDetBean.PROPOSER_PERM_OtherCityName){addrDet.addressDetBean.INSURED_PERM_OtherCityName = addrDet.addressDetBean.PROPOSER_PERM_OtherCityName}


        }
        addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD}, true)[0];
        addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG ;
        addrDet.addressDetBean.INSURED_CURR_ADDR1 = addrDet.addressDetBean.PROPOSER_CURR_ADDR1;
        addrDet.addressDetBean.INSURED_CURR_ADDR2 = addrDet.addressDetBean.PROPOSER_CURR_ADDR2;
        addrDet.addressDetBean.INSURED_CURR_ADDR3 = addrDet.addressDetBean.PROPOSER_CURR_ADDR3;
        addrDet.addressDetBean.INSURED_CURR_LANDMARK = addrDet.addressDetBean.PROPOSER_CURR_LANDMARK;
        addrDet.addressDetBean.INSURED_CURR_PINCODE = addrDet.addressDetBean.PROPOSER_CURR_PINCODE;

        addrDet.addressDetBean.INSURED_PERM_ADDR1 = addrDet.addressDetBean.PROPOSER_PERM_ADDR1
        addrDet.addressDetBean.INSURED_PERM_ADDR2 = addrDet.addressDetBean.PROPOSER_PERM_ADDR2
        addrDet.addressDetBean.INSURED_PERM_ADDR3 = addrDet.addressDetBean.PROPOSER_PERM_ADDR3 ;
        addrDet.addressDetBean.INSURED_PERM_LANDMARK = addrDet.addressDetBean.PROPOSER_PERM_LANDMARK;
        addrDet.addressDetBean.INSURED_PERM_PINCODE = addrDet.addressDetBean.PROPOSER_PERM_PINCODE;

  }
  else{
        console.log("inside copyProposerAddressToInsured  Default values");
        addrDet.addressDetBean.INSURED_RESIDENT_STATUS = addrDet.residentStatusList[0];
        addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = false;
        addrDet.addressDetBean.INSURED_CURR_ADDR1 = null;
        addrDet.addressDetBean.INSURED_CURR_ADDR2 = null;
        addrDet.addressDetBean.INSURED_CURR_ADDR3 = null;
        addrDet.addressDetBean.INSURED_CURR_LANDMARK = null;
        addrDet.addressDetBean.INSURED_CURR_STATE = this.insPStateList[0];
        addrDet.addressDetBean.INSURED_CURR_CITY = this.insPCityList[0];
        addrDet.addressDetBean.INSURED_CURR_STATE_OTHER = null;
        addrDet.addressDetBean.INSURED_CURR_CITY_OTHER = null;
        addrDet.addressDetBean.INSURED_CURR_PINCODE = null;
        addrDet.addressDetBean.INSURED_CURR_OtherCityName = null;
        addrDet.addressDetBean.INSURED_NATIONALITY = this.nationalityList[0];;
        addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = this.residentCountryList[0];

        addrDet.addressDetBean.INSURED_PERM_ADDR1 = null;
        addrDet.addressDetBean.INSURED_PERM_ADDR2 = null;
        addrDet.addressDetBean.INSURED_PERM_ADDR3 = null;
        addrDet.addressDetBean.INSURED_PERM_LANDMARK = null;
        addrDet.addressDetBean.INSURED_PERM_STATE = this.insPStateList[0];
        addrDet.addressDetBean.INSURED_PERM_CITY = this.insPCityList[0];
        addrDet.addressDetBean.INSURED_PERM_STATE_OTHER = null;
        addrDet.addressDetBean.INSURED_PERM_CITY_OTHER = null;
        addrDet.addressDetBean.INSURED_PERM_PINCODE = null;
        addrDet.addressDetBean.INSURED_PERM_OtherCityName = null;
        addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME = false;
        if(!!ApplicationFormDataService.SISFormData && !!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE){
          addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE}, true)[0];
        }
        addrDet.setEKYCAddress('IN');
        addrDet.setEKYCState('IN');
}
}

this.setExistingAddressData = function(){
var dfd = $q.defer();
var existingData = ExistingAppData.applicationPersonalInfo;
//CommonService.showLoading("Loading...");

persInfoService.setActiveTab("addressDetails");
if(ExistingAppData!=undefined && ExistingAppData.applicationMainData!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.applicationPersonalInfo.Insured).length!=0)
{
    debug("In:if: "+existingData.Insured.RESIDENT_STATUS_CODE);
    //Re-set values of Other City Name Start
    if(existingData.Insured.CURR_CITY_CODE == "OT" || existingData.Insured.PERM_CITY_CODE == "OT")
       {
        addrDet.addressDetBean.INSURED_CURR_OtherCityName = existingData.Insured.CURR_CITY;
        addrDet.addressDetBean.INSURED_PERM_OtherCityName =existingData.Insured.PERM_CITY;
        }

if(!addrDet.isSelf){
if(existingData.Insured.Insured_ProposerAddress_Flag == "Y" || existingData.Proposer.Insured_ProposerAddress_Flag == "Y"){
addrDet.addressDetBean.AddSameProposer = true;
addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME = (existingData.Proposer.IS_CURRADD_PERMADD_SAME == "Y")? true :false;
}
if(existingData.Proposer.CURR_CITY_CODE == "OT" || existingData.Proposer.PERM_CITY_CODE == "OT"){
   addrDet.addressDetBean.PROPOSER_CURR_OtherCityName = (!!LeadData && !!LeadData.IPAD_LEAD_ID) ? (!!LeadData.Other_CityName ?  LeadData.Other_CityName: existingData.Proposer.CURR_CITY) : "";
   addrDet.addressDetBean.PROPOSER_PERM_OtherCityName =existingData.Proposer.PERM_CITY;
    }
}


    //Re-set values of Other City Name END
    ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
    addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": existingData.Insured.RESIDENT_STATUS_CODE}, true)[0];
    addrDet.addressDetBean.INSURED_NATIONALITY = $filter('filter')(addrDet.nationalityList, {"COUNTRY_CODE": existingData.Insured.NATIONALITY_CODE}, true)[0];
    addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = $filter('filter')(addrDet.residentCountryList, {"COUNTRY_CODE": existingData.Insured.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
    if(!!existingData.Insured.BIRTH_COUNTRY)
    	addrDet.addressDetBean.INSURED_BIRTH_COUNTRY = $filter('filter')(addrDet.birthCountryList, {"COUNTRY_NAME": existingData.Insured.BIRTH_COUNTRY}, true)[0];
    if(addrDet.insEKYCFlag && !existingData.Insured.CURR_STATE_CODE)
    {
        debug(addrDet.insEKYCFlag+" not found state data :"+EKYCData.Insured.STATE)
        addrDet.setEKYCState('IN');
        if(addrDet.isSelf)
          addrDet.addressDetBean.INSURED_CURR_OtherCityName = (!!LeadData && !!LeadData.IPAD_LEAD_ID) ? (!!LeadData.Other_CityName ?  LeadData.Other_CityName: "Others") : "";
    }
    else
    {
        debug(addrDet.insEKYCFlag+"found state data :"+existingData.Insured.CURR_STATE_CODE)
        addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": existingData.Insured.CURR_STATE_CODE}, true)[0];

        addrDet.onInsuredChangeState(existingData.Insured.CURR_STATE_CODE).then(function(){
            addrDet.addressDetBean.INSURED_CURR_CITY = $filter('filter')(addrDet.insCityList, {"CITY_CODE": existingData.Insured.CURR_CITY_CODE}, true)[0];
        });

    }
    if(existingData.Insured.IS_CURRADD_PERMADD_SAME != 'Y' && existingData.Insured.Insured_ProposerAddress_Flag != "Y"){
    addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME = false;
    addrDet.addressDetBean.INSURED_PERM_STATE = $filter('filter')(addrDet.insPStateList, {"STATE_CODE": existingData.Insured.PERM_STATE_CODE},true)[0];
    addrDet.onInsuredChangePState(existingData.Insured.PERM_STATE_CODE).then(function(){
         addrDet.addressDetBean.INSURED_PERM_CITY = $filter('filter')(addrDet.insPCityList, {"CITY_CODE": existingData.Insured.PERM_CITY_CODE}, true)[0];

        });
    }
    else if(existingData.Insured.IS_CURRADD_PERMADD_SAME == 'Y' && existingData.Insured.Insured_ProposerAddress_Flag != "Y"){
        addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME = true;
        }


    if(addrDet.insEKYCFlag && !existingData.Insured.CURR_ADD_LINE1)
    {
        debug(addrDet.insEKYCFlag+" not found Addr data :"+EKYCData.Insured.STREET);
        addrDet.setEKYCAddress('IN');
    }
    else
    {
        addrDet.addressDetBean.INSURED_CURR_ADDR1 = existingData.Insured.CURR_ADD_LINE1;
        addrDet.addressDetBean.INSURED_CURR_ADDR2 = existingData.Insured.CURR_ADD_LINE2;
        addrDet.addressDetBean.INSURED_CURR_ADDR3 = existingData.Insured.CURR_ADD_LINE3;
        addrDet.addressDetBean.INSURED_CURR_LANDMARK = existingData.Insured.CURR_DISTRICT_LANDMARK;
        if(!!existingData.Insured.CURR_PIN)
            addrDet.addressDetBean.INSURED_CURR_PINCODE = parseInt(existingData.Insured.CURR_PIN);
    }

    //Permanent ADD
    addrDet.addressDetBean.INSURED_PERM_ADDR1 = existingData.Insured.PERM_ADD_LINE1;
    addrDet.addressDetBean.INSURED_PERM_ADDR2 = existingData.Insured.PERM_ADD_LINE2;
    addrDet.addressDetBean.INSURED_PERM_ADDR3 = existingData.Insured.PERM_ADD_LINE3;
    addrDet.addressDetBean.INSURED_PERM_LANDMARK = existingData.Insured.PERM_DISTRICT_LANDMARK;
    if(!!existingData.Insured.PERM_PIN)
        addrDet.addressDetBean.INSURED_PERM_PINCODE = parseInt(existingData.Insured.PERM_PIN);
    addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = existingData.Insured.COMMUNICATE_ADD_FLAG;
    if(existingData.Insured.ADDRESS_PROOF == 'Others')
        addrDet.addressDetBean.INSURED_OTHER_ADDRESS_PROOF = existingData.Insured.ADDRESS_PROOF_OTHER;
    if(existingData.Insured.RESIDENT_STATUS_CODE!='RI' && existingData.Insured.CURR_RESIDENT_COUNTRY_CODE!='IN')
    		{   addrDet.addressDetBean.INSURED_CURR_STATE_OTHER = existingData.Insured.CURR_STATE;
                addrDet.addressDetBean.INSURED_CURR_CITY_OTHER = existingData.Insured.CURR_CITY;
    			addrDet.addressDetBean.INSURED_PERM_STATE_OTHER = existingData.Insured.PERM_STATE;
                addrDet.addressDetBean.INSURED_PERM_CITY_OTHER = existingData.Insured.PERM_CITY;
    		}
    if(existingData.Insured.DATE_OF_BILL!=null || existingData.Insured.DATE_OF_BILL!=undefined)
        addrDet.addressDetBean.INSURED_BILL_DATE = CommonService.formatDobFromDb(existingData.Insured.DATE_OF_BILL);
    //Proposer
    if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
    //Proposer
        addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": existingData.Proposer.RESIDENT_STATUS_CODE}, true)[0];
        addrDet.addressDetBean.PROPOSER_NATIONALITY = $filter('filter')(addrDet.nationalityList, {"COUNTRY_CODE": existingData.Proposer.NATIONALITY_CODE}, true)[0];
        addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY = $filter('filter')(addrDet.residentCountryList, {"COUNTRY_CODE": existingData.Proposer.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
        if(!!existingData.Proposer.BIRTH_COUNTRY)
        	addrDet.addressDetBean.PROPOSER_BIRTH_COUNTRY = $filter('filter')(addrDet.birthCountryList, {"COUNTRY_NAME": existingData.Proposer.BIRTH_COUNTRY}, true)[0];
        addrDet.addressDetBean.PROPOSER_CURR_STATE = $filter('filter')(addrDet.proStateList, {"STATE_CODE": existingData.Proposer.CURR_STATE_CODE}, true)[0];
         console.log("State to set :"+existingData.Proposer.CURR_STATE_CODE);
        if(addrDet.proEKYCFlag && !existingData.Proposer.CURR_STATE_CODE)
        {
            debug(addrDet.proEKYCFlag+" not found state data :"+EKYCData.Proposer.STATE)
            addrDet.setEKYCState('PR');
            addrDet.addressDetBean.PROPOSER_CURR_OtherCityName = (!!LeadData && !!LeadData.IPAD_LEAD_ID) ? (!!LeadData.Other_CityName ?  LeadData.Other_CityName: "Others") : "";
        }
        else{
            addrDet.onProposerChangeState(existingData.Proposer.CURR_STATE_CODE).then(function(cityList){

             addrDet.addressDetBean.PROPOSER_CURR_CITY = $filter('filter')(cityList, {"CITY_CODE": existingData.Proposer.CURR_CITY_CODE}, true)[0];
            //addrDet.addressDetBean.PROPOSER_PERM_CITY = $filter('filter')(cityList, {"CITY_CODE": existingData.Proposer.PERM_CITY_CODE})[0];
            });
        }
        if(existingData.Proposer.IS_CURRADD_PERMADD_SAME != 'Y'){
        addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME = false;
        addrDet.addressDetBean.PROPOSER_PERM_STATE = $filter('filter')(addrDet.proPStateList, {"STATE_CODE": existingData.Proposer.PERM_STATE_CODE}, true)[0];
        addrDet.onProposerChangePState(existingData.Proposer.PERM_STATE_CODE).then(function(){
                addrDet.addressDetBean.PROPOSER_PERM_CITY = $filter('filter')(addrDet.proPCityList, {"CITY_CODE": existingData.Proposer.PERM_CITY_CODE}, true)[0];

               });

        }
        else if(existingData.Proposer.IS_CURRADD_PERMADD_SAME == 'Y'){
        addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME = true;
        }

     if(addrDet.proEKYCFlag && !existingData.Proposer.CURR_ADD_LINE1)
        {
            debug(addrDet.proEKYCFlag+" not found Addr data :"+EKYCData.Proposer.STREET);
            addrDet.setEKYCAddress('PR');
        }
        else
        {
            addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = existingData.Proposer.CURR_ADD_LINE1;
            addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = existingData.Proposer.CURR_ADD_LINE2;
            addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = existingData.Proposer.CURR_ADD_LINE3;
            addrDet.addressDetBean.PROPOSER_CURR_LANDMARK = existingData.Proposer.CURR_DISTRICT_LANDMARK;
            if(!!existingData.Proposer.CURR_PIN)
                addrDet.addressDetBean.PROPOSER_CURR_PINCODE = parseInt(existingData.Proposer.CURR_PIN);
        }
    //Permanent ADD
    addrDet.addressDetBean.PROPOSER_PERM_ADDR1 = existingData.Proposer.PERM_ADD_LINE1;
    addrDet.addressDetBean.PROPOSER_PERM_ADDR2 = existingData.Proposer.PERM_ADD_LINE2;
    addrDet.addressDetBean.PROPOSER_PERM_ADDR3 = existingData.Proposer.PERM_ADD_LINE3;
    addrDet.addressDetBean.PROPOSER_PERM_LANDMARK = existingData.Proposer.PERM_DISTRICT_LANDMARK;
    if(!!existingData.Proposer.PERM_PIN)
        addrDet.addressDetBean.PROPOSER_PERM_PINCODE = parseInt(existingData.Proposer.PERM_PIN);
    addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG = existingData.Proposer.COMMUNICATE_ADD_FLAG;
    if(existingData.Proposer.ADDRESS_PROOF == 'Others')
        addrDet.addressDetBean.PROPOSER_OTHER_ADDRESS_PROOF = existingData.Proposer.ADDRESS_PROOF_OTHER;
    if(existingData.Proposer.RESIDENT_STATUS_CODE!='RI' && existingData.Proposer.CURR_RESIDENT_COUNTRY_CODE!='IN')
            {
                addrDet.addressDetBean.PROPOSER_CURR_STATE_OTHER = existingData.Proposer.CURR_STATE;
                addrDet.addressDetBean.PROPOSER_CURR_CITY_OTHER = existingData.Proposer.CURR_CITY;
                addrDet.addressDetBean.PROPOSER_PERM_STATE_OTHER = existingData.Proposer.PERM_STATE;
                addrDet.addressDetBean.PROPOSER_PERM_CITY_OTHER = existingData.Proposer.PERM_CITY;
            }

    if(existingData.Proposer.DATE_OF_BILL!=null || existingData.Proposer.DATE_OF_BILL!=undefined)
        addrDet.addressDetBean.PROPOSER_BILL_DATE = CommonService.formatDobFromDb(existingData.Proposer.DATE_OF_BILL);
      }
            dfd.resolve(addrDet.addressDetBean);
}

if(!existingData.Insured.RESIDENT_STATUS_CODE || existingData.Insured.RESIDENT_STATUS_CODE == "")
{
    debug("ApplicationFormDataService.SISFormData : " + JSON.stringify(ApplicationFormDataService.SISFormData));
    if((ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SR_PGL_ID ||ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SRP_PGL_ID) && (!!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE)){
        debug("Succ");
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE){
            addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE}, true)[0];
            addrDet.disableResStatus = true;
            addrDet.onInsResidentChange(null,true);
        }
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.CURR_RESIDENT_COUNTRY_CODE){
            addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = $filter('filter')(addrDet.residentCountryList, {"COUNTRY_CODE": ApplicationFormDataService.SISFormData.sisFormFData.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
            addrDet.disableCountryOfRes = true;
        }
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.CURR_STATE_CODE){
            addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": ApplicationFormDataService.SISFormData.sisFormFData.CURR_STATE_CODE}, true)[0];
            addrDet.disableState = true;
            addrDet.onInsuredChangeState(ApplicationFormDataService.SISFormData.sisFormFData.CURR_STATE_CODE).then(function(insCityList){
                addrDet.addressDetBean.INSURED_CURR_CITY = $filter('filter')(insCityList, {"CITY_CODE": ApplicationFormDataService.SISFormData.sisFormFData.CURR_CITY_CODE})[0];
                addrDet.disableCity = true;
                if(addrDet.addressDetBean.INSURED_CURR_CITY.CITY_DESC =='Others')
                    addrDet.addressDetBean.INSURED_CURR_OtherCityName = ApplicationFormDataService.SISFormData.sisFormFData.OTHER_CITY;
            });
        }
    }
   if(!!SISTermData && !!SISTermData.OPPORTUNITY_ID)
   {
        addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": SISTermData.RESIDENT_STATUS_CODE}, true)[0];
        addrDet.onInsResidentChange(null,true);
        if(!!SISTermData.NATIONALITY_CODE)
            addrDet.addressDetBean.INSURED_NATIONALITY = $filter('filter')(addrDet.nationalityList, {"COUNTRY_CODE": SISTermData.NATIONALITY_CODE}, true)[0];
        if(!!SISTermData.CURR_RESIDENT_COUNTRY_CODE)
            addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = $filter('filter')(addrDet.residentCountryList, {"COUNTRY_CODE": SISTermData.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
        if(addrDet.isSelf)
        {   console.log("inside setting SIS data");
			if(addrDet.insEKYCFlag!=true){
	            addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": SISTermData.CURR_STATE_CODE}, true)[0];
	            addrDet.onInsuredChangeState(SISTermData.CURR_STATE_CODE).then(function(insCityList){

	                 addrDet.addressDetBean.INSURED_CURR_CITY = $filter('filter')(insCityList, {"CITY_CODE": SISTermData.CURR_CITY_CODE})[0];

	            });
			}
        }
        else
        {
			if(addrDet.proEKYCFlag!=true){
            addrDet.addressDetBean.PROPOSER_CURR_STATE = $filter('filter')(addrDet.proStateList, {"STATE_CODE": SISTermData.CURR_STATE_CODE}, true)[0];
            console.log("State to set :"+SISTermData.CURR_STATE_CODE);
               addrDet.onProposerChangeState(SISTermData.CURR_STATE_CODE).then(function(proCityList){

                    debug("addrDet.addressDetBean.PROPOSER_CURR_STATE :"+JSON.stringify(addrDet.addressDetBean.PROPOSER_CURR_STATE));
                    addrDet.addressDetBean.PROPOSER_CURR_CITY = $filter('filter')(proCityList, {"CITY_CODE": SISTermData.CURR_CITY_CODE}, true)[0];

                });
			}
        }
   }
	else{ // Populate FHR data
		if(!!FHRData && !!FHRData.FFHR && !!FHRData.FFHR.FHR_ID)
		{
			addrDet.addressDetBean.INSURED_RESIDENT_STATUS = $filter('filter')(addrDet.residentStatusList, {"RESIDENT_CD": FHRData.FFHR.RESIDENTIAL_STATUS}, true)[0];

	        if(addrDet.isSelf)
	        {   console.log("inside setting FHR data");
				if(addrDet.insEKYCFlag!=true && !!FHRData.FFHR.STATE_CODE){
		            addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": FHRData.FFHR.STATE_CODE}, true)[0];
		            addrDet.onInsuredChangeState(FHRData.FFHR.STATE_CODE).then(function(insCityList){

		                 addrDet.addressDetBean.INSURED_CURR_CITY = $filter('filter')(insCityList, {"CITY_CODE": FHRData.FFHR.CITY_CODE})[0];

		            });
				}
	        }
	        else
	        {
				if(addrDet.proEKYCFlag!=true && !!FHRData.FFHR.STATE_CODE){
	            addrDet.addressDetBean.PROPOSER_CURR_STATE = $filter('filter')(addrDet.proStateList, {"STATE_CODE": FHRData.FFHR.STATE_CODE}, true)[0];
	            console.log("State to set :"+FHRData.FFHR.STATE_CODE);
	               addrDet.onProposerChangeState(FHRData.FFHR.STATE_CODE).then(function(proCityList){

	                    debug("addrDet.addressDetBean.PROPOSER_CURR_STATE :"+JSON.stringify(addrDet.addressDetBean.PROPOSER_CURR_STATE));
	                    addrDet.addressDetBean.PROPOSER_CURR_CITY = $filter('filter')(proCityList, {"CITY_CODE": FHRData.FFHR.CITY_CODE}, true)[0];

	                });
				}
	        }
		}

	}
   //Populate Lead Data
   if(!!LeadData && !!LeadData.IPAD_LEAD_ID)
        {
        debug(addrDet.insEKYCFlag+" :: "+addrDet.proEKYCFlag+" :: "+LeadData.CURNT_CITY+"Populating Lead Data "+LeadData.CURNT_STATE)

        if(addrDet.isSelf && addrDet.insEKYCFlag == false)
            {
                addrDet.addressDetBean.INSURED_CURR_ADDR1 = LeadData.CURNT_ADDRESS1;
                addrDet.addressDetBean.INSURED_CURR_ADDR2 = LeadData.CURNT_ADDRESS2;
                addrDet.addressDetBean.INSURED_CURR_ADDR3 = LeadData.CURNT_ADDRESS3;
                addrDet.addressDetBean.INSURED_CURR_LANDMARK = LeadData.CURNT_DISTRICT_LANDMARK;
                if(LeadData.CURNT_CITY == "OT") {
                       addrDet.addressDetBean.INSURED_CURR_OtherCityName = LeadData.Other_CityName;
                }

                if(!!LeadData.CURNT_ZIP_CODE)
                    addrDet.addressDetBean.INSURED_CURR_PINCODE = parseInt(LeadData.CURNT_ZIP_CODE);
                if((SISTermData==undefined || SISTermData.OPPORTUNITY_ID==undefined) && (FHRData.FFHR == undefined || FHRData.FFHR.FHR_ID == undefined) && addrDet.insEKYCFlag!=true && (ApplicationFormDataService.SISFormData.sisFormFData==undefined || ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE==undefined)){
                    console.log("inside setting LEAD data");
                    addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": LeadData.CURNT_STATE}, true)[0];
                        addrDet.onInsuredChangeState(LeadData.CURNT_STATE).then(function(insCityList){

                         addrDet.addressDetBean.INSURED_CURR_CITY = $filter('filter')(insCityList, {"CITY_CODE": LeadData.CURNT_CITY})[0];

                        });
                }
            }
            else
            {
            if(addrDet.isSelf == false && addrDet.proEKYCFlag == false)
            {
                addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = LeadData.CURNT_ADDRESS1;
                addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = LeadData.CURNT_ADDRESS2;
                addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = LeadData.CURNT_ADDRESS3;
                addrDet.addressDetBean.PROPOSER_CURR_LANDMARK = LeadData.CURNT_DISTRICT_LANDMARK;

                if(LeadData.CURNT_CITY == "OT") {
                   addrDet.addressDetBean.PROPOSER_CURR_OtherCityName = LeadData.Other_CityName;
                   }

                if(!!LeadData.CURNT_ZIP_CODE)
                    addrDet.addressDetBean.PROPOSER_CURR_PINCODE = parseInt(LeadData.CURNT_ZIP_CODE);
               if(((SISTermData==undefined || SISTermData.OPPORTUNITY_ID==undefined) && (FHRData.FFHR == undefined || FHRData.FFHR.FHR_ID == undefined)) && addrDet.proEKYCFlag!=true){

                addrDet.addressDetBean.PROPOSER_CURR_STATE = $filter('filter')(addrDet.proStateList, {"STATE_CODE": LeadData.CURNT_STATE}, true)[0];
                console.log("State to set :"+LeadData.CURNT_STATE);
                   addrDet.onProposerChangeState(LeadData.CURNT_STATE).then(function(proCityList){

                        debug("addrDet.addressDetBean.PROPOSER_CURR_STATE :"+JSON.stringify(addrDet.addressDetBean.PROPOSER_CURR_STATE));
                        addrDet.addressDetBean.PROPOSER_CURR_CITY = $filter('filter')(proCityList, {"CITY_CODE": LeadData.CURNT_CITY}, true)[0];

                    });
            }
                }
            }
        }
        else
            debug("in else LEAD Data :"+JSON.stringify(LeadData));
}
else {
    if((ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SR_PGL_ID ||ApplicationFormDataService.SISFormData.sisMainData.PGL_ID==SRP_PGL_ID) && (!!ApplicationFormDataService.SISFormData.sisFormFData && !!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE)){
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.RESIDENT_STATUS_CODE){
            addrDet.disableResStatus = true;
            addrDet.onInsResidentChange(null,true);
        }
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.CURR_RESIDENT_COUNTRY_CODE){
            addrDet.disableCountryOfRes = true;
        }
        if(!!ApplicationFormDataService.SISFormData.sisFormFData.CURR_STATE_CODE){
            addrDet.disableState = true;
            addrDet.disableCity = true;
        }
    }
}

if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
      this.copyProposerAddressToInsured();
 }

return dfd.promise;
}


// this.setExistingAddressData().then(
// function(){
//   console.log("ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID ::"+ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID);
//   LoadAppProofList.getCurrAddressProofType(ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID).then(function(docProofMaster){
//   console.log("docProofMaster :IN:"+JSON.stringify(docProofMaster));
//   if(!!docProofMaster && docProofMaster.length!=0){
//     LoadAppProofList.getAddrProofList('IN',docProofMaster[0].MPDOC_CODE).then(
//                 function(AddrProofList){
//                 console.log("IN:AddrProofList:"+JSON.stringify(AddrProofList));
//                     addrDet.insAddrProofList = AddrProofList;
//                     addrDet.addressDetBean.INSURED_ADDRESS_PROOF = $filter('filter')(addrDet.insAddrProofList, {"DOC_ID": ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID}, true)[0];
//                     CommonService.hideLoading();
//                     });
//                 }
//                 else{
//                     CommonService.hideLoading();
//                 }
//     if(ExistingAppData.applicationPersonalInfo.Proposer!=undefined && ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID!=undefined){
//     	CommonService.showLoading();
//     LoadAppProofList.getCurrAddressProofType(ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID).then(function(prdocProofMaster){
//     console.log("docProofMaster :PR:"+JSON.stringify(prdocProofMaster));
//     if(!!prdocProofMaster && prdocProofMaster.length!=0){
//       LoadAppProofList.getAddrProofList('PR',prdocProofMaster[0].MPDOC_CODE).then(
//                      function(AddrProofList){
//                      console.log("PR:AddrProofList:"+JSON.stringify(AddrProofList));
//                          addrDet.proAddrProofList = AddrProofList;
//                          addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF = $filter('filter')(addrDet.proAddrProofList, {"DOC_ID": ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID}, true)[0];
//                          CommonService.hideLoading();
//                          });
//       }else{
//       	CommonService.hideLoading();
//     }
//     });
//   }
//   else{
//       CommonService.hideLoading();
//   }
// });

//CommonService.hideLoading();
ApplicationFormDataService.applicationFormBean.addressDetBean = addrDet.addressDetBean;

this.setDeDupeData = function() {
  var dfd = $q.defer();
  debug("inside Addr Dedupe");
  try{
    if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && ExistingAppData.applicationMainData.IS_ADDR_DET != 'Y'){
      CommonService.showLoading("Loading Existing Data..");
      if(!addrDet.addressDetBean.INSURED_CURR_ADDR1)
        addrDet.addressDetBean.INSURED_CURR_ADDR1 = (!!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS1 && !!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS1) ? DeDupeData.RESULT_APPDATA.INSURED_ADDRESS1 : null;
      if(!addrDet.addressDetBean.INSURED_CURR_ADDR2)
        addrDet.addressDetBean.INSURED_CURR_ADDR2 = (!!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS2 && !!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS2) ? DeDupeData.RESULT_APPDATA.INSURED_ADDRESS2 : null;
      if(!addrDet.addressDetBean.INSURED_CURR_ADDR3)
        addrDet.addressDetBean.INSURED_CURR_ADDR3 = (!!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS3 && !!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS3) ? DeDupeData.RESULT_APPDATA.INSURED_ADDRESS3 : null;
      if(!addrDet.addressDetBean.INSURED_CURR_LANDMARK)
        addrDet.addressDetBean.INSURED_CURR_LANDMARK = (!!DeDupeData.RESULT_APPDATA.INSURED_LANDMARK && !!DeDupeData.RESULT_APPDATA.INSURED_LANDMARK) ? DeDupeData.RESULT_APPDATA.INSURED_LANDMARK : null;
      if(!addrDet.addressDetBean.INSURED_CURR_PINCODE && !!DeDupeData.RESULT_APPDATA.INSURED_CONTACT_ZIP && DeDupeData.RESULT_APPDATA.INSURED_CONTACT_ZIP)
          addrDet.addressDetBean.INSURED_CURR_PINCODE = parseInt(DeDupeData.RESULT_APPDATA.INSURED_CONTACT_ZIP);
      if(!addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG)
        addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = (!!DeDupeData.RESULT_APPDATA.INSURED_COMM_ADDRESS && !!DeDupeData.RESULT_APPDATA.INSURED_COMM_ADDRESS) ? DeDupeData.RESULT_APPDATA.INSURED_COMM_ADDRESS : null;

      var STATE = (!!DeDupeData.RESULT_APPDATA.INSURED_STATE && !!DeDupeData.RESULT_APPDATA.INSURED_STATE) ? DeDupeData.RESULT_APPDATA.INSURED_STATE : null;
      var CITY = (!!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS4 && !!DeDupeData.RESULT_APPDATA.INSURED_ADDRESS4) ? DeDupeData.RESULT_APPDATA.INSURED_ADDRESS4 : null;

      if(!!STATE && (!addrDet.addressDetBean.INSURED_CURR_STATE || !addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE)){

        if(!addrDet.addressDetBean.INSURED_CURR_STATE || !addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE)
          addrDet.addressDetBean.INSURED_CURR_STATE = $filter('filter')(addrDet.insStateList, { "STATE_DESC": STATE.toUpperCase()}, true)[0];
        addrDet.onInsuredChangeState(addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE).then(
            function(cityList){
              var cityList = $filter('filter')(addrDet.insCityList, {"CITY_DESC": CITY}, true);
              var CITY_SELECT = (!!cityList && cityList.length>0)?cityList[0]:$filter('filter')(addrDet.insCityList, {"CITY_CODE": "OT"}, true)[0];
              //console.log("CITY_SELECT :"+JSON.stringify(CITY_SELECT));
              if(!!CITY_SELECT)
                addrDet.addressDetBean.INSURED_CURR_CITY = CITY_SELECT;
            }
        );
      }

      var addPrf = $filter('filter')(addrDet.insAddrProofList, {"NBFE_PROOF_CODE": DeDupeData.RESULT_APPDATA.INSURED_ADDRESS_PROOF,"CUSTOMER_CATEGORY": 'IN'}, true)[0];
      if((!addrDet.addressDetBean.INSURED_ADDRESS_PROOF || !addrDet.addressDetBean.INSURED_ADDRESS_PROOF.DOC_ID) && !!addPrf && !!addPrf.NBFE_PROOF_CODE)
        addrDet.addressDetBean.INSURED_ADDRESS_PROOF = addPrf;
      if(!addrDet.isSelf){
        if(!addrDet.addressDetBean.PROPOSER_CURR_ADDR1)
          addrDet.addressDetBean.PROPOSER_CURR_ADDR1 = (!!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS1 && !!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS1) ? DeDupeData.RESULT_APPDATA.OWNER_ADDRESS1 : null;
        if(!addrDet.addressDetBean.PROPOSER_CURR_ADDR2)
          addrDet.addressDetBean.PROPOSER_CURR_ADDR2 = (!!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS2 && !!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS2) ? DeDupeData.RESULT_APPDATA.OWNER_ADDRESS2 : null;
        if(!addrDet.addressDetBean.PROPOSER_CURR_ADDR3)
          addrDet.addressDetBean.PROPOSER_CURR_ADDR3 = (!!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS3 && !!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS3) ? DeDupeData.RESULT_APPDATA.OWNER_ADDRESS3 : null;
        if(!addrDet.addressDetBean.PROPOSER_CURR_LANDMARK)
          addrDet.addressDetBean.PROPOSER_CURR_LANDMARK = (!!DeDupeData.RESULT_APPDATA.OWNER_LANDMARK && !!DeDupeData.RESULT_APPDATA.OWNER_LANDMARK) ? DeDupeData.RESULT_APPDATA.OWNER_LANDMARK : null;
        if(!addrDet.addressDetBean.PROPOSER_CURR_PINCODE && !!DeDupeData.RESULT_APPDATA.OWNER_CONTACT_ZIP && DeDupeData.RESULT_APPDATA.OWNER_CONTACT_ZIP)
            addrDet.addressDetBean.PROPOSER_CURR_PINCODE = parseInt(DeDupeData.RESULT_APPDATA.OWNER_CONTACT_ZIP);
        if(!addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG)
          addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG = (!!DeDupeData.RESULT_APPDATA.OWNER_COMM_ADDRESS && !!DeDupeData.RESULT_APPDATA.OWNER_COMM_ADDRESS) ? DeDupeData.RESULT_APPDATA.OWNER_COMM_ADDRESS : null;

        var STATE = (!!DeDupeData.RESULT_APPDATA.OWNER_STATE && !!DeDupeData.RESULT_APPDATA.OWNER_STATE) ? DeDupeData.RESULT_APPDATA.OWNER_STATE : null;
        var CITY = (!!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS4 && !!DeDupeData.RESULT_APPDATA.OWNER_ADDRESS4) ? DeDupeData.RESULT_APPDATA.OWNER_ADDRESS4 : null;

        if(!!STATE && (!addrDet.addressDetBean.PROPOSER_CURR_STATE || !addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE)){
          addrDet.addressDetBean.PROPOSER_CURR_STATE = $filter('filter')(addrDet.proStateList, { "STATE_DESC": STATE.toUpperCase()}, true)[0];
          addrDet.onProposerChangeState(addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE).then(
              function(cityList){
                var cityList = $filter('filter')(addrDet.proCityList, {"CITY_DESC": CITY}, true);
                var CITY_SELECT = (!!cityList && cityList.length>0)?cityList[0]:$filter('filter')(addrDet.proCityList, {"CITY_CODE": "OT"}, true)[0];
                //console.log("CITY_SELECT :"+JSON.stringify(CITY_SELECT));
                if(!!CITY_SELECT)
                  addrDet.addressDetBean.PROPOSER_CURR_CITY = CITY_SELECT;
              }
          );
        }
        var prAddrPrf = $filter('filter')(addrDet.proAddrProofList, {"NBFE_PROOF_CODE": DeDupeData.RESULT_APPDATA.OWNER_ADDRESS_PROOF,"CUSTOMER_CATEGORY": 'PR'}, true)[0];
        if((!addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF || !addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF.NBFE_PROOF_CODE) && !!prAddrPrf && !!prAddrPrf.NBFE_PROOF_CODE)
          addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF = prAddrPrf;
        dfd.resolve(addrDet.addressDetBean);
      }
      else
        dfd.resolve(addrDet.addressDetBean);

    }
    else
      {
        debug("Already data saved");
        dfd.resolve(addrDet.addressDetBean);
      }
  }catch(e){
    debug("Exception in deDupe - Address");
    CommonService.hideLoading();
    dfd.resolve(addrDet.addressDetBean);
  }
    return dfd.promise;
}

this.setExistingAddressData().then(
  function(){
      console.log("ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID ::"+ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID);
      LoadAppProofList.getCurrAddressProofType(ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID).then(function(docProofMaster){
          console.log("docProofMaster :IN:"+JSON.stringify(docProofMaster));
          if(!!docProofMaster && docProofMaster.length!=0){
              LoadAppProofList.getAddrProofList('IN',docProofMaster[0].MPDOC_CODE).then(
                      function(AddrProofList){
                                console.log("IN:AddrProofList:"+JSON.stringify(AddrProofList));
                                addrDet.insAddrProofList = AddrProofList;
                                addrDet.addressDetBean.INSURED_ADDRESS_PROOF = $filter('filter')(addrDet.insAddrProofList, {"DOC_ID": ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID}, true)[0];
                                if(addrDet.isSelf)//call here if Self Case
                                  addrDet.setDeDupeData().then(function (resp) {
                                      CommonService.hideLoading();
                                  });
                          });
          }
          else if(addrDet.isSelf){
            addrDet.setDeDupeData().then(function (resp) {
                CommonService.hideLoading();
            });
          }
          if(ExistingAppData.applicationPersonalInfo.Proposer!=undefined && ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID!=undefined)
          {    LoadAppProofList.getCurrAddressProofType(ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID).then(function(prdocProofMaster){
                  console.log("docProofMaster :PR:"+JSON.stringify(prdocProofMaster));
                  if(!!prdocProofMaster && prdocProofMaster.length!=0){
                    LoadAppProofList.getAddrProofList('PR',prdocProofMaster[0].MPDOC_CODE).then(
                               function(AddrProofList){
                                       console.log("PR:AddrProofList:"+JSON.stringify(AddrProofList));
                                       addrDet.proAddrProofList = AddrProofList;
                                       addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF = $filter('filter')(addrDet.proAddrProofList, {"DOC_ID": ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID}, true)[0];
                                       addrDet.setDeDupeData().then(function (resp) {
                                           CommonService.hideLoading();
                                       });
                                   });
                  }
                  else{
                    addrDet.setDeDupeData().then(function (resp) {
                        CommonService.hideLoading();
                    });
                  }
              });
          }
          else{
            addrDet.setDeDupeData().then(function (resp) {
                CommonService.hideLoading();
            });
          }
      });

      ApplicationFormDataService.applicationFormBean.addressDetBean = addrDet.addressDetBean;
  });

/*
this.setStateOnFilter = function(STATE_CODE, STATE_LIST){

debug("STATE to set :"+STATE_CODE);
    var stateData = $filter('filter')(STATE_LIST, {"STATE_CODE": STATE_CODE},true);
    debug(i+"STATE got set :"+JSON.stringify(stateData))
            if(!!stateData)
            for(var i = 0; i<stateData.length; i++)
            {
                (function(i){
                    debug("i :"+i+"STATE got set :"+JSON.stringify(stateData[i]));
                                if(stateData[i].STATE_CODE == existingData.Insured.CURR_STATE_CODE)
                                    addrDet.addressDetBean.INSURED_CURR_STATE = stateData[i];

                }(i));
            }

}
*/
this.onInsResidentChange = function(addressForm){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci = {};

if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_STATUS!='Select ' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_NAME!='Select ')
 {
        addressForm.state.$setValidity('selectRequired', true);
        addressForm.city.$setValidity('selectRequired', true);
        addressForm.permState.$setValidity('selectRequired', true);
        addressForm.permCity.$setValidity('selectRequired', true);
 }

/*if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD =='NRI' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_STATUS!='Select ')
{
    //addrDet.addressDetBean.INSURED_BIRTH_COUNTRY = this.birthCountryList[0];
    addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY = this.residentCountryList[0];
    console.log("Insured Birth Country::"+JSON.stringify(addrDet.addressDetBean.INSURED_BIRTH_COUNTRY));
}*/

if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!=undefined && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD !='RI' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!="OCI")
    {
        if(NRIList.Insured.isDigital!= undefined && NRIList.Insured.isDigital == 'Y')
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = NRIList.Insured.nri;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = NRIList.Insured.nri;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = null;
        navigator.notification.alert("You will not be allowed to submit the application without uploading the filled NRI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
    }
    else if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD == "OCI")
        {
            if(OCIList.Insured.isDigital != undefined && OCIList.Insured.isDigital == 'Y')
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = OCIList.Insured.oci;
            else
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = OCIList.Insured.oci;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = null;
            navigator.notification.alert("You will not be allowed to submit the application without uploading the filled OCI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
        }
        else
            {
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.nri.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.nri.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.oci.DOC_ID = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.oci.DOC_ID = null;
            }

 LoadAppProofList.setFATFFlag(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD, addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY).then(
 function(FATFFlag){
  console.log("Insured FATFFlag :"+FATFFlag/*+"::AppAddrComm:"+addrDet.addressDetBean.AppAddrComm*/);
    addrDet.InsFATFFlag = FATFFlag;
     LoadAppProofList.setAddressType(FATFFlag,addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG,ApplicationFormDataService.SISFormData).then(
         function(addrType){
            console.log("IN addrType:"+addrType);
            LoadAppProofList.getAddrProofList('IN',addrType).then(
            function(AddrProofList){
            console.log("IN:AddrProofList:"+JSON.stringify(AddrProofList));
                addrDet.insAddrProofList = AddrProofList;
                addrDet.addressDetBean.INSURED_ADDRESS_PROOF = addrDet.insAddrProofList[0];
                });
            });
         });



}

this.onProResidentChange = function(addressForm){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.oci = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nri = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.oci = {};
console.log("Checkbox"+addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME);
/*if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD =='NRI' && addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_STATUS!='Select ')
{
    //addrDet.addressDetBean.PROPOSER_BIRTH_COUNTRY = this.birthCountryList[0];
    addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY = this.residentCountryList[0];
    console.log("PROPOSER Birth Country::"+JSON.stringify(addrDet.addressDetBean.PROPOSER_BIRTH_COUNTRY));
}*/

if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_STATUS!='Select ' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_NAME!='Select ')
  {
    addressForm.proState.$setValidity('selectRequired', true);
    addressForm.proCity.$setValidity('selectRequired', true);
    addressForm.proPermState.$setValidity('selectRequired', true);
    addressForm.proPermCity.$setValidity('selectRequired', true);

  }
  else if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD=='RI'){
     if(addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_DESC=='Select ' || addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_DESC==undefined){
          addressForm.proState.$setValidity('selectRequired', false);
          addressForm.proCity.$setValidity('selectRequired', false);
      }

    if((addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME == false || addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME == undefined) && (addrDet.addressDetBean.PROPOSER_PERM_STATE.STATE_DESC=='Select ' || addrDet.addressDetBean.PROPOSER_PERM_STATE.STATE_DESC==undefined)){
        addressForm.proPermState.$setValidity('selectRequired', false);
        if(addrDet.addressDetBean.PROPOSER_PERM_CITY.CITY_DESC=='Select ')
            addressForm.proPermCity.$setValidity('selectRequired', false);
    }
  }

if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!=undefined && addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD !='RI' && addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!="OCI")
   {
       //ApplicationFormDataService.applicationFormBean.DOCS_LIST.Proposer.nri = NRIList.Proposer.nri;
       if(NRIList.Proposer.isDigital!= undefined && NRIList.Proposer.isDigital == 'Y')
                   ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nri.DOC_ID = NRIList.Proposer.nri;
               else
                   ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nri.DOC_ID = NRIList.Proposer.nri;
               ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.oci.DOC_ID = null;
               ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.oci.DOC_ID = null;
       navigator.notification.alert("You will not be allowed to submit the application without uploading the filled NRI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
   }
   else if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD == "OCI")
           {
               if(OCIList.Proposer.isDigital != undefined && OCIList.Proposer.isDigital == 'Y')
                   ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.oci.DOC_ID = OCIList.Proposer.oci;
               else
                   ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.oci.DOC_ID = OCIList.Proposer.oci;

               ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nri.DOC_ID = null;
               ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nri.DOC_ID = null;
               navigator.notification.alert("You will not be allowed to submit the application without uploading the filled OCI Questionnaire.",function(){CommonService.hideLoading();},"Application","OK");
           }
   else
        {
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.nri.DOC_ID = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.nri.DOC_ID = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.oci.DOC_ID = null;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.oci.DOC_ID = null;
        }

 LoadAppProofList.setFATFFlag(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD, addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY).then(
    function(FATFFlag){
     console.log("Proposer FATFFlag :"+FATFFlag);
       addrDet.ProFATFFlag = FATFFlag;
        LoadAppProofList.setAddressType(FATFFlag,addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG,ApplicationFormDataService.SISFormData).then(
            function(addrType){
               console.log("PR addrType:"+addrType);
               LoadAppProofList.getAddrProofList('PR',addrType).then(
               function(AddrProofList){
               console.log("PR:AddrProofList:"+JSON.stringify(AddrProofList));
                   addrDet.proAddrProofList = AddrProofList;
                   addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF = addrDet.proAddrProofList[0];
                   });
               });
            });

if(addrDet.addressDetBean.AddSameProposer && !!addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
    if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME)
    this.copyProposerCurrAddressToPerm();
    else
    this.copyProposerAddressToInsured();
 }
 else{
  if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME)
     this.copyProposerCurrAddressToPerm();
 }

}

this.onInsCountryOfResidenceChange = function(addressForm){

if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_STATUS!='Select ' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_NAME!='Select ')
  {
    addressForm.state.$setValidity('selectRequired', true);
    addressForm.city.$setValidity('selectRequired', true);
    addressForm.permState.$setValidity('selectRequired', true);
    addressForm.permCity.$setValidity('selectRequired', true);

  }

if(addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE !='IN')
    {
        navigator.notification.alert("You will not be allowed to submit the application without uploading an income proof.",function(){CommonService.hideLoading();},"Application","OK");
        //ApplicationFormDataService.applicationFormBean.DOCS_LIST.Insured.nri = NRIList.Insured.nri;
    }
    else
      {// ApplicationFormDataService.applicationFormBean.DOCS_LIST.Insured.nri = null;
      }

        LoadAppProofList.setFATFFlag(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD, addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY).then(
             function(FATFFlag){
              console.log("Insured FATFFlag :"+FATFFlag);
                addrDet.InsFATFFlag = FATFFlag;

             }
             );

}

this.onProCountryOfResidenceChange = function(addressForm){

if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_STATUS!='Select ' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_NAME!='Select ')
  {
    addressForm.proState.$setValidity('selectRequired', true);
    addressForm.proCity.$setValidity('selectRequired', true);
    addressForm.proPermState.$setValidity('selectRequired', true);
    addressForm.proPermCity.$setValidity('selectRequired', true);

  }
if(addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE !='IN')
    {
        navigator.notification.alert("You will not be allowed to submit the application without uploading an income proof.",function(){CommonService.hideLoading();},"Application","OK");
        //ApplicationFormDataService.applicationFormBean.DOCS_LIST.Proposer.nri = NRIList.Proposer.nri;
    }
    else
        {
        //ApplicationFormDataService.applicationFormBean.DOCS_LIST.Proposer.nri = null;
        }

 LoadAppProofList.setFATFFlag(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD, addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY).then(
             function(FATFFlag){
              console.log("Proposer FATFFlag :"+FATFFlag);
                addrDet.ProFATFFlag = FATFFlag;

             }
             );

}

this.onInsChangeAddressProof = function(){
if(addrDet.addressDetBean.INSURED_ADDRESS_PROOF.MPPROOF_CODE == 'AC' && ExistingAppData.applicationPersonalInfo.Insured.AADHAR_CARD_EXISTS != "Y")
{
addrDet.addressDetBean.INSURED_ADDRESS_PROOF = this.insAddrProofList[0];
navigator.notification.alert("Please enter Aadhar Card No in Personal Details.",function(){CommonService.hideLoading();},"Application","OK");
}
else{
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.addrproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.addrproof = {};

if(addrDet.addressDetBean.INSURED_ADDRESS_PROOF.isDigital == 'Y')
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.addrproof.DOC_ID = addrDet.addressDetBean.INSURED_ADDRESS_PROOF.DOC_ID;
else
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.addrproof.DOC_ID = addrDet.addressDetBean.INSURED_ADDRESS_PROOF.DOC_ID;
    navigator.notification.alert("You will not be allowed to submit the application without uploading the respective address proof.",function(){CommonService.hideLoading();},"Application","OK");
}

}

this.onProChangeAddressProof = function(){
if(addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF.MPPROOF_CODE == 'AC' && ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_EXISTS != "Y")
{
addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF = this.proAddrProofList[0];
navigator.notification.alert("Please enter Aadhar Card No in Personal Details.",function(){CommonService.hideLoading();},"Application","OK");
}
else
{
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.addrproof = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.addrproof = {};

if(addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF.isDigital == 'Y')
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.addrproof.DOC_ID = addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF.DOC_ID;
else
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.addrproof.DOC_ID = addrDet.addressDetBean.PROPOSER_ADDRESS_PROOF.DOC_ID;
    navigator.notification.alert("You will not be allowed to submit the application without uploading the respective address proof.",function(){CommonService.hideLoading();},"Application","OK");
}
}


this.copyInsuredCurrAddressToPerm = function(){
debug("copyInsuredCurrAddressToPerm ....");
    if(addrDet.addressDetBean.INSURED_IS_CURRADD_PERMADD_SAME)
    {
        if(addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE!='IN' && !!addrDet.addressDetBean.INSURED_RESIDENT_STATUS.RESIDENT_CD && !!addrDet.addressDetBean.INSURED_RESIDENT_COUNTRY.COUNTRY_CODE){
        debug("setting Other City");
        addrDet.addressDetBean.INSURED_PERM_STATE_OTHER = addrDet.addressDetBean.INSURED_CURR_STATE_OTHER;
        addrDet.addressDetBean.INSURED_PERM_CITY_OTHER = addrDet.addressDetBean.INSURED_CURR_CITY_OTHER;

        }
        else{
        debug("setting CityLIST");
         console.log("State to set :"+JSON.stringify(addrDet.addressDetBean.INSURED_CURR_STATE));
         addrDet.addressDetBean.INSURED_PERM_STATE = $filter('filter')(addrDet.insStateList, {"STATE_CODE": addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE}, true)[0];

            if(!!addrDet.addressDetBean.INSURED_CURR_OtherCityName){addrDet.addressDetBean.INSURED_PERM_OtherCityName = addrDet.addressDetBean.INSURED_CURR_OtherCityName}

            addrDet.onInsuredChangePState(addrDet.addressDetBean.INSURED_CURR_STATE.STATE_CODE).then(
                function(PCityList){
                    console.log("setting PCity")
                    addrDet.addressDetBean.INSURED_PERM_CITY = addrDet.addressDetBean.INSURED_CURR_CITY;
                }
            );
        }
        addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = 'P';
        addrDet.addressDetBean.INSURED_PERM_ADDR1 = addrDet.addressDetBean.INSURED_CURR_ADDR1;
        addrDet.addressDetBean.INSURED_PERM_ADDR2 = addrDet.addressDetBean.INSURED_CURR_ADDR2;
        addrDet.addressDetBean.INSURED_PERM_ADDR3 = addrDet.addressDetBean.INSURED_CURR_ADDR3;
        addrDet.addressDetBean.INSURED_PERM_LANDMARK = addrDet.addressDetBean.INSURED_CURR_LANDMARK;
        addrDet.addressDetBean.INSURED_PERM_PINCODE = addrDet.addressDetBean.INSURED_CURR_PINCODE;

    }
    else
    {
        addrDet.addressDetBean.INSURED_COMMUNICATE_ADD_FLAG = null;
        addrDet.addressDetBean.INSURED_PERM_ADDR1 = null;
        addrDet.addressDetBean.INSURED_PERM_ADDR2 = null;
        addrDet.addressDetBean.INSURED_PERM_ADDR3 = null;
        addrDet.addressDetBean.INSURED_PERM_LANDMARK = null;
        addrDet.addressDetBean.INSURED_PERM_STATE = this.insPStateList[0];
        addrDet.addressDetBean.INSURED_PERM_CITY = this.insPCityList[0];
        addrDet.addressDetBean.INSURED_PERM_STATE_OTHER = null;
        addrDet.addressDetBean.INSURED_PERM_CITY_OTHER = null;
        addrDet.addressDetBean.INSURED_PERM_PINCODE = null;
        addrDet.addressDetBean.INSURED_PERM_OtherCityName = null;
    }


};

this.copyProposerCurrAddressToPerm = function(){
console.log("inside copyProposerCurrAddressToPerm")
if(addrDet.addressDetBean.PROPOSER_IS_CURRADD_PERMADD_SAME)
    {
    console.log("ADDR :1");
    if(addrDet.addressDetBean.PROPOSER_RESIDENT_STATUS.RESIDENT_CD!='RI' && addrDet.addressDetBean.PROPOSER_RESIDENT_COUNTRY.COUNTRY_CODE!='IN'){
            console.log("ADDR :2");
             addrDet.addressDetBean.PROPOSER_PERM_STATE_OTHER = addrDet.addressDetBean.PROPOSER_CURR_STATE_OTHER;
             addrDet.addressDetBean.PROPOSER_PERM_CITY_OTHER = addrDet.addressDetBean.PROPOSER_CURR_CITY_OTHER;
        }
        else
        {
        console.log("ADDR :3");
        console.log("State to set :"+JSON.stringify(addrDet.addressDetBean.PROPOSER_CURR_STATE));
        addrDet.addressDetBean.PROPOSER_PERM_STATE = $filter('filter')(addrDet.proStateList, {"STATE_CODE": addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE}, true)[0];
        if(!!addrDet.addressDetBean.PROPOSER_CURR_OtherCityName){addrDet.addressDetBean.PROPOSER_PERM_OtherCityName = addrDet.addressDetBean.PROPOSER_CURR_OtherCityName}

        addrDet.onProposerChangePState(addrDet.addressDetBean.PROPOSER_CURR_STATE.STATE_CODE).then(
            function(PCityList){
                console.log("setting PCity")
                addrDet.addressDetBean.PROPOSER_PERM_CITY = addrDet.addressDetBean.PROPOSER_CURR_CITY;
            }
        );

        }
        addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG = 'P';
        addrDet.addressDetBean.PROPOSER_PERM_ADDR1 = addrDet.addressDetBean.PROPOSER_CURR_ADDR1;
        addrDet.addressDetBean.PROPOSER_PERM_ADDR2 = addrDet.addressDetBean.PROPOSER_CURR_ADDR2;
        addrDet.addressDetBean.PROPOSER_PERM_ADDR3 = addrDet.addressDetBean.PROPOSER_CURR_ADDR3;
        addrDet.addressDetBean.PROPOSER_PERM_LANDMARK = addrDet.addressDetBean.PROPOSER_CURR_LANDMARK;

        addrDet.addressDetBean.PROPOSER_PERM_PINCODE = addrDet.addressDetBean.PROPOSER_CURR_PINCODE;
        if(addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
        this.copyProposerAddressToInsured();
        }

    }
    else
    {console.log("ADDR :4");
        addrDet.addressDetBean.PROPOSER_COMMUNICATE_ADD_FLAG = false;
        addrDet.addressDetBean.PROPOSER_PERM_ADDR1 = null;
        addrDet.addressDetBean.PROPOSER_PERM_ADDR2 = null;
        addrDet.addressDetBean.PROPOSER_PERM_ADDR3 = null;
        addrDet.addressDetBean.PROPOSER_PERM_LANDMARK = null;
        addrDet.addressDetBean.PROPOSER_PERM_STATE = this.insPStateList[0];
        addrDet.addressDetBean.PROPOSER_PERM_CITY = this.insPCityList[0];
        addrDet.addressDetBean.PROPOSER_PERM_STATE_OTHER = null;
        addrDet.addressDetBean.PROPOSER_PERM_CITY_OTHER = null;
        addrDet.addressDetBean.PROPOSER_PERM_PINCODE = null;
        addrDet.addressDetBean.PROPOSER_PERM_OtherCityName =null;
        if(addrDet.addressDetBean.AddSameProposer && !addrDet.isSelf){
        this.copyProposerAddressToInsured();
        }
    }

}



this.onNext = function(addressForm){
	CommonService.showLoading();
addrDet.click = true;
if(addressForm.$invalid != true){
ApplicationFormDataService.applicationFormBean.addressDetBean = addrDet.addressDetBean;
PersonalInfoService.getAddressDetailsData(addrDet.addressDetBean,ApplicationFormDataService.applicationFormBean).then(function(AddressData){
var whereClauseObj = {};
        	whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

var whereClauseObj1 = {};
        	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj1.CUST_TYPE = 'C01';
var whereClauseObj2 = {};
        	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj2.CUST_TYPE = 'C02';
    console.log("DATA to be inserted :"+JSON.stringify(AddressData));
    CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', AddressData.Insured, whereClauseObj1).then(
				                function(res){
				                console.log("LP_APP_CONTACT_SCRN_A AddressData update success !!"+JSON.stringify(whereClauseObj1));
				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_ADDR_DET" : 'Y'}, whereClauseObj).then(
                                                    function(res){
                                                        console.log("LP_APP_CONTACT_SCRN_A IS_ADDR_DET update success !!"+JSON.stringify(whereClauseObj2));
                                                    });
				                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
				                CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', AddressData.Proposer, whereClauseObj2).then(
                                				                function(res){
                                                                persInfoService.setActiveTab("eduQualDetails");
                                				                $state.go('applicationForm.personalInfo.eduQualDetails');
                                									console.log("LP_APP_CONTACT_SCRN_A AddressData update success !!"+JSON.stringify(whereClauseObj2));
                                								});
                                else{
                                    persInfoService.setActiveTab("eduQualDetails");
                                    $state.go('applicationForm.personalInfo.eduQualDetails');
                                }
								});

});
}
else
    navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
}
//CommonService.hideLoading();
}]);
