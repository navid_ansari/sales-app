policyNoModule.controller('PolicyNoCtrl', ['$q','$state','CommonService', 'PolicyNoData','TermPolicyNoData','PolicyNoService','LoginService','ApplicationFormDataService','UWRuleService','ExistingAppData','MedicalFlags','FinancialQuest','AppTimeService','TermSisData','TotInsPremium','TotProPremium','EKYCData','EKYCOldData','ImmunizationList','StudentIDList', function($q, $state, CommonService, PolicyNoData, TermPolicyNoData, PolicyNoService, LoginService,ApplicationFormDataService, UWRuleService, ExistingAppData, MedicalFlags, FinancialQuest,AppTimeService,TermSisData,TotInsPremium,TotProPremium,EKYCData, EKYCOldData,ImmunizationList,StudentIDList){
    var pol = this;


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */

    if(!EKYCOldData || (!EKYCOldData.Insured && !EKYCOldData.Proposer) && (!EKYCOldData.Insured.OPP_ID && !EKYCOldData.Proposer.OPP_ID))
    {
      debug("Old EKYC data not found");
    }
    else {
      if(!EKYCData || (!EKYCData.Insured.EKYC_ID && !EKYCData.Proposer.EKYC_ID))
        {
          debug("Old EKYC data found");
          EKYCData = EKYCOldData;
        }
        else
          debug("USE NEW EKYC data");
    }

    if(!EKYCOldData || (!EKYCOldData.Insured && !EKYCOldData.Proposer) && (!EKYCOldData.Insured.OPP_ID && !EKYCOldData.Proposer.OPP_ID))
    {
      debug("Old EKYC data not found");
    }
    else {
      if(!EKYCData || (!EKYCData.Insured.EKYC_ID && !EKYCData.Proposer.EKYC_ID))
        {
          debug("Old EKYC data found");
          EKYCData = EKYCOldData;
        }
        else
          debug("USE NEW EKYC data");
    }

    UWRuleService.ImmunizationList = ImmunizationList;
    debug("UWRuleService.ImmunizationList::"+JSON.stringify(ImmunizationList));
    UWRuleService.StudentIDList = StudentIDList;
    pol.POLICY_NO = PolicyNoData.POLICY_NO;
    pol.REF_POLICY_NO = '';
    pol.COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
    pol.freeTxt = FREE_TEXT_REGEX;
    pol.startPopup = false;
    if(!!ApplicationFormDataService.SISFormData.sisFormFData)
        pol.sisDataPresent = true;
    else
        pol.sisDataPresent = false;
    console.log(JSON.stringify(TermPolicyNoData));
    if(!!TermPolicyNoData && !!TermPolicyNoData.REF_POLICY_NO)
        {
            pol.REF_POLICY_NO = TermPolicyNoData.REF_POLICY_NO;
            ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID = TermPolicyNoData.REF_APPLICATION_ID;
            ApplicationFormDataService.ComboFormBean.REF_POLICY_NO = TermPolicyNoData.REF_POLICY_NO;
        }
    //timeline changes..
    CommonService.hideLoading();

    ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
    Reflex :{
    	Insured:{},
    	Proposer:{}
    },
    NonReflex :{
    	Insured:{},
    	Proposer:{}
    }

    };
    debug("TotInsPremium :"+JSON.stringify(TotInsPremium)+"TotProPremium :"+JSON.stringify(TotProPremium));
    debug(JSON.stringify(MedicalFlags) +"PolicyNoData.POLICY_NO :"+PolicyNoData.POLICY_NO)
    var sisData = ApplicationFormDataService.SISFormData;
    var appId = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    pol.isSelf = ((sisData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)
    if(!!sisData.sisFormFData && !!sisData.sisFormFData.ELIGIBILITY_JUSTIFICATION)
        pol.ELIGIBILITY_JUSTIFICATION = sisData.sisFormFData.ELIGIBILITY_JUSTIFICATION;
    if(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST!=undefined)
        ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
    var UWReq = {
    "APP_ID":appId,
    "AGENT_CD":LoginService.lgnSrvObj.userinfo.AGENT_CD,
    "PLAN_CODE":sisData.sisFormBData.PLAN_CODE,
    "RIDER_CODE":null,
    "POLICY_NO":PolicyNoData.POLICY_NO,
    "SIS_ID":sisData.sisMainData.SIS_ID,
    "INS_AGE":CommonService.getAge(sisData.sisFormAData.INSURED_DOB),
    "PRO_AGE":((sisData.sisFormAData.PROPOSER_DOB !=undefined) ? CommonService.getAge(sisData.sisFormAData.PROPOSER_DOB) : null),
    "IS_PROP":sisData.sisFormAData.PROPOSER_IS_INSURED,
    "PGL_ID":sisData.sisMainData.PGL_ID,
    "COMBO_ID":ApplicationFormDataService.ComboFormBean.COMBO_ID,
    "SAR":sisData.sisMainData.SAR,
    "OCCU_CLASS":ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS
    };
    this.onNext = function(){
        debug("BEFORE DOCS_LIST::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        CommonService.showLoading("Loading...");
      /*  if(CommonService.checkConnection() == "online"){*/
          CommonService.showLoading("Checking Existing policies...");
          if(!!PolicyNoData.POLICY_NO && !ExistingAppData.applicationMainData.POLICY_NO)
            ExistingAppData.applicationMainData.POLICY_NO = PolicyNoData.POLICY_NO;
          PolicyNoService.saveDeDupeData(sisData, ExistingAppData).then(function(SARValues){
            CommonService.showLoading("Loading...");
              console.log("success DeDupeData: "+JSON.stringify(SARValues));
              if(!!SARValues.FSAR)
                UWReq.FSAR = SARValues.FSAR;
              else
                UWReq.FSAR = 0;

              if(!!SARValues.FINPREM)
                UWReq.FINPREM = SARValues.FINPREM;
              else
                UWReq.FINPREM = 0;

            pol.insertMedicalDC().then(
                function(){
                    pol.insertTermMedicalDC(pol.COMBO_ID).then(function(){
                        UWRuleService.getFinGridData(UWReq,pol.sisDataPresent).then(
                            function(FinGridData){
                              CommonService.hideLoading();
                                debug("FinGridData : " + JSON.stringify(FinGridData));
                                pol.FinGridData = FinGridData;
                                if(!!FinGridData){
                                    if((UWReq.PGL_ID==SR_PGL_ID || UWReq.PGL_ID==SRP_PGL_ID) && !!pol.sisDataPresent){
                                        if(!!FinGridData.buttonIndex && FinGridData.buttonIndex==0){
                                            return FinGridData;
                                        }else if (!!FinGridData.buttonIndex && FinGridData.buttonIndex==1) {
                                            pol.startPopup = true;
                                        }
                                        else if (!!FinGridData.buttonIndex && FinGridData.buttonIndex==2) {
                                            debug("User Quit");
                                            $state.go("dashboard.home");
                                        }else {
											CommonService.hideLoading();
                                            debug("something went wrong");
                                        }
                                    }else {
                                        return FinGridData;
                                    }
                                }
                            }
                        ).then(
                            function(FinGridData){
                                CommonService.hideLoading();
                                debug("pol.startPopup : " + pol.startPopup);
                                if(!!FinGridData){
                                    if((UWReq.PGL_ID==SR_PGL_ID || UWReq.PGL_ID==SRP_PGL_ID) && !!pol.sisDataPresent){
                                        UWRuleService.generateTermValidationOP(UWReq).then(
                                            function(res){
                                                if(!!res && res=="S"){
                                                    pol.continuePolNoGeneration(FinGridData, UWReq);
                                                }else {
                                                    debug("could not generate output");
                                                }
                                            }
                                        );
                                    }else {
                                        pol.continuePolNoGeneration(FinGridData, UWReq);
                                    }

                                }else {
                                    debug("FinGridData not available");
                                }
                            }
                        );
                    });
                }
            );
          });
      /*  }
        else
        {
           CommonService.hideLoading();
           navigator.notification.alert("Please be Online or Check your internet connection.",null,"Policy No. Generation","OK");
        }*/
    }

    this.continuePolNoGeneration = function(FinGridData, UWReq) {
        debug("FinGridData in pol no updation : " + JSON.stringify(FinGridData));
        pol.updateUWData(FinGridData).then(
            function(data){
                debug("Final DOCS_LIST::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                if(data!=undefined)
                    pol.updateDocsList().then(function(){
                        var ANN_PREM = sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
                        debug("FINPREM : "+UWReq.FINPREM+"ANN_PREM -1 :"+ANN_PREM);
                        var TOT_ANN_PREM = parseInt(ANN_PREM);
                        var INC_ID = ExistingAppData.applicationPersonalInfo.Insured.INCOME_PROOF_DOC_ID;
                        var ADD_ID = ExistingAppData.applicationPersonalInfo.Insured.ADDRESS_PROOF_DOC_ID;
                        var EKYC_FLAG = null;
                        if(!!EKYCData && !!EKYCData.Insured && !!EKYCData.Insured.EKYC_FLAG)
                            EKYC_FLAG = EKYCData.Insured.EKYC_FLAG;
                        if(sisData.sisFormAData.PROPOSER_IS_INSURED == 'Y')
                            TOT_ANN_PREM = TOT_ANN_PREM + parseInt(TotInsPremium.Ins) + parseInt(UWReq.FINPREM);
                        else{
                            TOT_ANN_PREM = TOT_ANN_PREM + parseInt(TotProPremium.Pro) + parseInt(UWReq.FINPREM);
                            INC_ID = ExistingAppData.applicationPersonalInfo.Proposer.INCOME_PROOF_DOC_ID;
                            ADD_ID = ExistingAppData.applicationPersonalInfo.Proposer.ADDRESS_PROOF_DOC_ID;
                            if(!!EKYCData && !!EKYCData.Proposer && !!EKYCData.Proposer.EKYC_FLAG)
                                EKYC_FLAG = EKYCData.Proposer.EKYC_FLAG;
                          }
                          console.log("ANN_PREM:"+ANN_PREM+"TOtal :"+TOT_ANN_PREM+"IncomeProof::"+FinGridData.incTyp+"Flag is::"+ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG);
                        if(TOT_ANN_PREM > 99999 && (INC_ID == null || INC_ID == '')){
                            CommonService.hideLoading();
                            if(FinGridData.incTyp == "standard")
                                navigator.notification.alert("Please select Standard Income Proof documents",function(){
                                debug("Please select Standard Income Proof documents");
                                $state.go("applicationForm.personalInfo.eduQualDetails");
                                },"Message","OK");
                            else
                                navigator.notification.alert("Please select Income Proof document",function(){
                                    debug("Please select Standard Income Proof documents");
                                    $state.go("applicationForm.personalInfo.eduQualDetails");
                                },"Message","OK");
                        }
                        else if(TOT_ANN_PREM > 10000 && (ADD_ID == null || ADD_ID == '') && EKYC_FLAG!='Y'){
                            CommonService.hideLoading();
                            if(FinGridData.incTyp == "standard")
                                navigator.notification.alert("Please select Address Proof and Standard Income Proof documents",function(){
                                    debug("Please select Standard Income Proof documents");
                                    $state.go("applicationForm.personalInfo.addressDetails");
                                },"Message","OK");
                            else
                                navigator.notification.alert("Please select Address Proof document",function(){
                                    debug("Please select Standard Income Proof documents");
                                    $state.go("applicationForm.personalInfo.addressDetails");
                                },"Message","OK");
                            }
                        else if(FinGridData.incTyp == "standard"){
                            CommonService.hideLoading();
                            navigator.notification.alert("Please select Standard Income Proof documents",function(){
                                debug("Please select Standard Income Proof documents");
                                $state.go("applicationForm.personalInfo.eduQualDetails");
                            },"Message","OK");
                        }
                        else{
                            //timeline changes..
                            CommonService.showLoading();
                            AppTimeService.setActiveTab("medDiag");
                            AppTimeService.isPolNoGenCompleted = true;
                            //$rootScope.val = 80;
                            AppTimeService.setProgressValue(80);
                            $state.go("applicationForm.medicalDiagnosis.insMedDiagnosis");
                        }
                    });
                }
        );
    }

    this.onSaveJstfication = function(justifcationForm){
        if(justifcationForm.$invalid === false){
            var sisFormFData = ApplicationFormDataService.SISFormData.sisFormFData;
            sisFormFData.POLICY_NO = PolicyNoData.POLICY_NO;
            sisFormFData.ELIGIBILITY_JUSTIFICATION = pol.ELIGIBILITY_JUSTIFICATION;
            sisFormFData.ELIGIBILITY_FLAG = 'N';
            sisFormFData.APPROVAL_FLAG = 'N';
            sisFormFData.ISSYNCED = 'N';
            var whereClauseObj = {};
            whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
            //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
            //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
            whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
            CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                function(res){
                    console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                    if(!!res){
                        ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                        pol.startPopup = false;
                        UWRuleService.generateTermValidationOP(UWReq).then(
                            function(res){
                                if(!!res && res=="S"){
                                    pol.continuePolNoGeneration(pol.FinGridData, UWReq);
                                }else {
                                    debug("could not generate output");
                                }
                            }
                        );
                    }
                }
            );
        }else
			     navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
    }

    this.closePopUp = function(){
        pol.startPopup = false;
    }

    this.updateUWData = function(FinGridData){
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.finQnFlg = {};
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.finQnFlg = {};
    if(ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.incproof ==undefined)
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.incproof = {};
    if(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof ==undefined)
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof = {};

    if(pol.isSelf == false)
    {
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.finQnFlg = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.finQnFlg = {};
        if(ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.incproof ==undefined)
           ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.incproof = {};
        if(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof ==undefined)
           ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof = {};
    }
    var dfd = $q.defer();
        if(!!FinGridData){
        debug("UWRule Success");
        if(FinGridData.finQnFlg == 'Y')
            {
                if(FinancialQuest.DIGITAL_HTML_FLAG == 'Y')
                {   if(pol.isSelf)
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.finQnFlg.DOC_ID = FinancialQuest.DOC_ID;
                    else
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.finQnFlg.DOC_ID = FinancialQuest.DOC_ID;
                }
                else
                {
                    if(pol.isSelf)
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.finQnFlg.DOC_ID = FinancialQuest.DOC_ID;
                    else
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.finQnFlg.DOC_ID = FinancialQuest.DOC_ID;
                }
            }
            else
            {
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.finQnFlg = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.finQnFlg = null;
                if(pol.isSelf == false)
                {
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.finQnFlg = null;
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.finQnFlg = null;
                }
            }
        if(FinGridData.incTyp!=undefined && FinGridData.incTyp=="standard")
            {
                if(pol.isSelf)
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG = 'Y';
                else
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof.FLAG = 'Y';
            }
        else
            {
            if(pol.isSelf)
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.incproof.FLAG = null;
            else
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.incproof.FLAG = null;


            }
        dfd.resolve(ApplicationFormDataService.applicationFormBean.DOCS_LIST);
    }
    else{
        debug("UWRule Failed");
            dfd.resolve(null);
    }

        return dfd.promise;
    }

    this.updateDocsList = function(){
    var dfd = $q.defer();
    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

    var whereClauseObj1 = {};
            	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
            	whereClauseObj1.CUST_TYPE = 'C01';
    var whereClauseObj2 = {};
            	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
            	whereClauseObj2.CUST_TYPE = 'C02';

    CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj1).then(
    				                function(res){
    				                console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST update success !!"+JSON.stringify(whereClauseObj1));

    				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN8_COMPLETED" :"Y"}, whereClauseObj).then(
    													function(res){
    													dfd.resolve(res);
    														console.log("IS_SCREEN8_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
    													});
                                    if(sisData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
                                    CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj2).then(
                                                                    function(res){
                                                                        console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST proposer update success !!"+JSON.stringify(whereClauseObj2));
                                                                    });


    								});
    	return dfd.promise;
    }

    this.insertMedicalDC = function(){
    var dfd = $q.defer();
        if(sisData.sisFormDData!=undefined && sisData.sisFormDData[0].RIDER_CODE!=undefined)
            {
                //MedicalFlags
                if((sisData.sisFormDData[0].RIDER_CODE == 'WPPN1V1' || sisData.sisFormDData[0].RIDER_CODE == 'WOPPULV1') && MedicalFlags!=undefined && MedicalFlags.PAYOR_MED_FLAG == 'Y')
                PolicyNoService.getMedicalRecord(ExistingAppData.applicationPersonalInfo, pol.POLICY_NO, true).then(function(Data){

                                debug("if:prop rider:DATA :"+JSON.stringify(Data));
                                PolicyNoService.insertMedicalDC(Data).then(
                                    function(res){
                                        dfd.resolve(res);
                                    }
                                );
                                });
                else
                    PolicyNoService.getMedicalRecord(ExistingAppData.applicationPersonalInfo, pol.POLICY_NO, false).then(function(Data){

                                    debug("if :else:DATA :"+JSON.stringify(Data));
                                    PolicyNoService.insertMedicalDC(Data).then(
                                        function(res){
                                            dfd.resolve(res);
                                        }
                                    );
                                    });
            }
            else
                PolicyNoService.getMedicalRecord(ExistingAppData.applicationPersonalInfo, pol.POLICY_NO, false).then(function(Data){

                debug("ELSE :DATA :"+JSON.stringify(Data));
                PolicyNoService.insertMedicalDC(Data).then(
                         function(res){
                             dfd.resolve(res);
                         }
                     );
                });
        return dfd.promise;
    }

this.insertTermMedicalDC = function(combo_id){
        var dfd = $q.defer();
        if(!!combo_id && combo_id!=''){
            console.log("calling for combo policy");
            PolicyNoService.getMedicalRecord(ExistingAppData.applicationPersonalInfo, pol.REF_POLICY_NO, false, ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID).then(function(Data){

            debug("insertTermMedicalDC :DATA :"+JSON.stringify(Data));
            PolicyNoService.insertMedicalDC(Data).then(
                     function(res){
                         dfd.resolve(res);
                     }
                 );
            });
        }
        else
            dfd.resolve(true);

        return dfd.promise;
     }
     CommonService.hideLoading();
}]);


policyNoModule.service('PolicyNoService', ['$q','CommonService','LoginService', 'ApplicationFormDataService','LoadApplicationScreenData', 'ComboPolicyNoService','InsMedDiagnosisService', function($q, CommonService, LoginService, ApplicationFormDataService, LoadApplicationScreenData, ComboPolicyNoService, InsMedDiagnosisService){


    var PolNoFunc=this;
    var APP_ID;
    var FHR_ID;
    var SIS_ID;
    var PGL_ID;
    var AGENT_CD;
    var whereClauseObj = {};

    this.saveDeDupeData = function(sisData, ExistingAppData){
      console.log("inside saveDeDupeData ");
      var dfd = $q.defer();
      var SARValues = {};
      CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(function (DeDupeData) {
        if(!!DeDupeData && !!DeDupeData.RESP && DeDupeData.RESP == 'S'){
          var MSAR = 0;
          var FSAR = 0;
          var FINPREM = 0;

            if(!!DeDupeData && !!DeDupeData.RESP){
                if(!!DeDupeData.RESULT_MEDSAR)
                  MSAR = DeDupeData.RESULT_MEDSAR;
                if(!!DeDupeData.RESULT_FINSAR)
                  FSAR = DeDupeData.RESULT_FINSAR;
                if(!!DeDupeData.RESULT_TPREM)
                  FINPREM = DeDupeData.RESULT_TPREM;
            }
            InsMedDiagnosisService.saveMedSARData(ExistingAppData, MSAR, FSAR, FINPREM).then(function(res) {
              //update SAR values
              SARValues.MSAR = MSAR;
              SARValues.FSAR = FSAR;
              SARValues.FINPREM = FINPREM;
                console.log("DE-DUPE :Success : MSAR :"+MSAR+" : FSAR :"+FSAR + " : FINPREM :"+FINPREM);
                dfd.resolve(SARValues);
            });
        }
        else
        if(CommonService.checkConnection() == "online"){
            InsMedDiagnosisService.getMedicalSAR(sisData, ExistingAppData).then(function(medData){
              var MSAR = 0;
              var FSAR = 0;
              var FINPREM = 0;

                if(!!medData && !!medData.RESP){
                    if(!!medData.RESP.MEDCOV)
                      MSAR = medData.RESP.MEDCOV;
                    if(!!medData.RESP.FINCOV)
                      FSAR = medData.RESP.FINCOV;
                    if(!!medData.RESP.AnnualPremium)
                      FINPREM = medData.RESP.AnnualPremium;
                }
                InsMedDiagnosisService.saveMedSARData(ExistingAppData, MSAR, FSAR, FINPREM).then(function(res) {
                  //update SAR values
                  SARValues.MSAR = MSAR;
                  SARValues.FSAR = FSAR;
                  SARValues.FINPREM = FINPREM;
                    console.log("DE-DUPE :Success : MSAR :"+MSAR+" : FSAR :"+FSAR + " : FINPREM :"+FINPREM);
                    dfd.resolve(SARValues);
                });

            });
          }
          else
          {
             CommonService.hideLoading();
             navigator.notification.alert("Please be Online or Check your internet connection.",null,"Policy No. Generation","OK");
          }
      });
      return dfd.promise;
    }

    this.addExistInsPremium = function(APP_ID, AGENT_CD){
      var dfd = $q.defer();
      var TOT_PREM = {};
      TOT_PREM.Ins = 0;
      LoadApplicationScreenData.loadExistingData(APP_ID, AGENT_CD).then(function(ExistInsData){
          debug("inside loadExistingData:"+JSON.stringify(ExistInsData))
          if(!!ExistInsData && !!ExistInsData.Insured && !!ExistInsData.Insured.length>0)
          {
              for (var i = 0; i < ExistInsData.Insured.length; i++) {
                if(!!ExistInsData.Insured[i].ANNUAL_PREMIUM)
                  TOT_PREM.Ins = TOT_PREM.Ins + parseInt(ExistInsData.Insured[i].ANNUAL_PREMIUM);
              }
              dfd.resolve(TOT_PREM);
          }
          else
            dfd.resolve(TOT_PREM);
      });
      return dfd.promise;
    }

    this.addExistProPremium = function(APP_ID, AGENT_CD){
      var dfd = $q.defer();
      var TOT_PREM = {};
      TOT_PREM.Pro = 0;
      LoadApplicationScreenData.loadExistingData(APP_ID, AGENT_CD).then(function(ExistInsData){
          if(!!ExistInsData && !!ExistInsData.Proposer && !!ExistInsData.Proposer.length>0)
          {
              for (var i = 0; i < ExistInsData.Proposer.length; i++) {
                if(!!ExistInsData.Proposer[i].ANNUAL_PREMIUM)
                  TOT_PREM.Pro = TOT_PREM.Pro + parseInt(ExistInsData.Proposer[i].ANNUAL_PREMIUM);
              }
              dfd.resolve(TOT_PREM);
          }
          else
            dfd.resolve(TOT_PREM);
      });
      return dfd.promise;
    }
    this.isPolicyNoAssigned = function(){

        APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
        SIS_ID = ApplicationFormDataService.SISFormData.sisMainData.SIS_ID;
        PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
        AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        whereClauseObj = {};
        whereClauseObj.APPLICATION_ID = APP_ID;
        whereClauseObj.AGENT_CD = AGENT_CD;

    debug("isPolicyNoAssigned 1")
        var dfd = $q.defer();
        var policyBean = {};
        LoadApplicationScreenData.loadApplicationFlags(APP_ID, AGENT_CD).then(function(AppMain){
            debug("isPolicyNoAssigned 2"+JSON.stringify(AppMain))
            if(AppMain.applicationFlags.POLICY_NO == undefined || AppMain.applicationFlags.POLICY_NO =="")
            {
               PolNoFunc.getProdType().then(function(pol){
                    policyBean.PROD_TYPE = pol.PROD_TYPE;
                    debug("pol.PROD_TYPE :"+pol.PROD_TYPE);
                    PolNoFunc.checkPolicyNo(pol.PROD_TYPE).then(function(polBean){
                    if(polBean!=undefined && polBean.POLICY_NO!=undefined && polBean.PROD_TYPE!=undefined)
                    {
                        policyBean.POLICY_NO = polBean.POLICY_NO;
                        policyBean.PROD_TYPE = polBean.PROD_TYPE;
                        ApplicationFormDataService.applicationFormBean.POLICY_NO = policyBean.POLICY_NO;
                         PolNoFunc.AssignPolicyNo(policyBean).then(function(){
                            dfd.resolve(policyBean);
                         });
                    }
                    });

               });
            }
            else
            {
                policyBean.POLICY_NO = AppMain.applicationFlags.POLICY_NO;
                ApplicationFormDataService.applicationFormBean.POLICY_NO = AppMain.applicationFlags.POLICY_NO;
                var plNo = AppMain.applicationFlags.POLICY_NO;
                policyBean.PROD_TYPE = plNo.substring(0,1);
                var whereClauseObj4 = {};
                               whereClauseObj4.DOC_CAT_ID = APP_ID;
                               whereClauseObj4.AGENT_CD = AGENT_CD;
                CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj4).then(
                            function(res){
                                debug("All LP_DOCUMENT_UPLOAD: update success !!"+JSON.stringify(whereClauseObj4));

                        });
                     dfd.resolve(policyBean);
            }

        });

        return dfd.promise
    }

    this.getProdType=function(){
    var dfd = $q.defer();
    var policyBean = {}
    var whereClauseObj={};
        whereClauseObj.PGL_ID = PGL_ID;
        CommonService.selectRecords(sisDB,"LP_PGL_PLAN_GROUP_LK",false,'PGL_PRODUCT_TYPE',whereClauseObj).then(
            function(res){
                policyBean.PROD_TYPE = res.rows.item(0).PGL_PRODUCT_TYPE;
                dfd.resolve(policyBean);
            }
        );
        return dfd.promise;
    }

    this.checkPolicyNo=function(prdType){
        var dfd = $q.defer();
        var policyBean = {};
        var whereClauseObj={};
                whereClauseObj.POLICY_TYPE = prdType;
                whereClauseObj.POLICY_FINAL_STATUS = 'F';
                debug(JSON.stringify(whereClauseObj))
        CommonService.selectRecords(db,"LP_POLICY_ASSIGNMENT",false,'POLICY_NO',whereClauseObj).then(
                    function(res){
                        debug("LP_POLICY_ASSIGNMENT :"+res.rows.length);
                       if(!!res && res.rows.length>0){
                            debug(res.rows.item(0).POLICY_NO)
                            policyBean.POLICY_NO = res.rows.item(0).POLICY_NO;
                            policyBean.PROD_TYPE = prdType;
                            dfd.resolve(policyBean);
                       }
                       else
                        {
                            navigator.notification.alert(POLICYNO_MSG,function(){CommonService.hideLoading();},"Application","OK");
                            dfd.resolve(null);
                        }
                    }
                );
        return dfd.promise;

    }

    this.AssignPolicyNo=function(policyBean){
    var dfd = $q.defer();
            APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
            SIS_ID = ApplicationFormDataService.SISFormData.sisMainData.SIS_ID;
            PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
            AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

         var whereClauseObj={};
                        whereClauseObj.POLICY_NO = policyBean.POLICY_NO;
                        whereClauseObj.POLICY_TYPE = policyBean.PROD_TYPE;
                        whereClauseObj.POLICY_FINAL_STATUS = 'F';
          var whereClauseObj1 = {};
                        whereClauseObj1.APPLICATION_ID = APP_ID;
                        whereClauseObj1.AGENT_CD = AGENT_CD;
         var whereClauseObj2 = {};
                         whereClauseObj2.DOC_CAT_ID = SIS_ID;
                         whereClauseObj2.AGENT_CD = AGENT_CD;
                        // whereClauseObj2.DOC_CAT = "SIS";
         var whereClauseObj3 = {};
                       whereClauseObj3.DOC_CAT_ID = FHR_ID;
                       whereClauseObj3.AGENT_CD = AGENT_CD;
                       //whereClauseObj3.DOC_CAT = "FHR";
         var whereClauseObj4 = {};
                        whereClauseObj4.DOC_CAT_ID = APP_ID;
                        whereClauseObj4.AGENT_CD = AGENT_CD;

        CommonService.updateRecords(db, 'LP_POLICY_ASSIGNMENT',{"POLICY_FINAL_STATUS" :"U"}, whereClauseObj).then(
                        function(res){
                            debug("POLICY_FINAL_STATUS update success !!"+JSON.stringify(whereClauseObj));


                        CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                function(res){
                                    debug("LP_APPLICATION_MAIN update success !!"+JSON.stringify(whereClauseObj1));
                                });
                                //PSC changes
                        CommonService.updateRecords(db, 'LP_APP_SOURCER_IMPSNT_SCRN_L',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                                        function(res){
                                                            debug("LP_APP_SOURCER_IMPSNT_SCRN_L update success !!"+JSON.stringify(whereClauseObj1));
                                                        });
                        CommonService.updateRecords(db, 'LP_APP_PAY_DTLS_SCRN_G',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                                        function(res){
                                                            debug("LP_APP_PAY_DTLS_SCRN_G update success !!"+JSON.stringify(whereClauseObj1));
                                                        });
                        CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                function(res){
                                    debug("LP_MYOPPORTUNITY update success !!"+JSON.stringify(whereClauseObj1));
                                });
                        CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj2).then(
                                    function(res){
                                    debug("SIS HTML update success !!"+JSON.stringify(whereClauseObj2));
                                });
                         CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj3).then(
                                                            function(res){
                                                            debug("FHR HTML update success !!"+JSON.stringify(whereClauseObj3));
                                                        });
                        CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj4).then(
                                    function(res){
                                        debug("All LP_DOCUMENT_UPLOAD: update success !!"+JSON.stringify(whereClauseObj4));
                                    dfd.resolve(res);
                                });

                        });
        return dfd.promise;
    }

    this.getMedicalRecord = function(AppData, polNo, flag, appId){
        var dfd = $q.defer();
        var medicalDC = {
        Insured : {},
        Proposer : {}
        };
         medicalDC.Insured.APPLICATION_ID = AppData.Insured.APPLICATION_ID;//ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        if(!!appId)
            medicalDC.Insured.APPLICATION_ID = appId;//ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        medicalDC.Insured.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        medicalDC.Insured.POLICY_NO = polNo;
        medicalDC.Insured.CUST_TYPE = 'C01';
        medicalDC.Insured.MG_MEDICAL_FLAG = 'N';
        medicalDC.Insured.RELATIONSHIP_ID = AppData.Insured.RELATIONSHIP_ID;
        //proposer
        if(flag){
            medicalDC.Proposer.APPLICATION_ID = AppData.Insured.APPLICATION_ID;//ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            if(!!appId)
                medicalDC.Insured.APPLICATION_ID = appId;
            medicalDC.Proposer.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            medicalDC.Proposer.POLICY_NO = polNo;
            medicalDC.Proposer.CUST_TYPE = 'C02';
            medicalDC.Insured.MG_MEDICAL_FLAG = 'N';
            medicalDC.Proposer.RELATIONSHIP_ID = AppData.Insured.RELATIONSHIP_ID;
        }
        dfd.resolve(medicalDC);
        return dfd.promise;
    }
    this.insertMedicalDC = function(Data){
    var dfd = $q.defer();
    debug("inside service :insertMedicalDC");
    CommonService.insertOrReplaceRecord(db, 'LP_APP_SAR_MED_SCREEN_K', Data.Insured, true).then(
                function(res){
                    debug("LP_APP_SAR_MED_SCREEN_K Insured success !!"+JSON.stringify(res));

                    dfd.resolve(res);

                    if(Data.Proposer!=undefined && Data.Proposer.APPLICATION_ID!=undefined)
                        CommonService.insertOrReplaceRecord(db, 'LP_APP_SAR_MED_SCREEN_K', Data.Proposer, true).then(
                                        function(res){
                                            debug("LP_APP_SAR_MED_SCREEN_K Proposer success !!"+JSON.stringify(res));

                                        }
                                    );

                }
            );
            return dfd.promise;
    }


    CommonService.hideLoading();
}]);

//COMBO service
policyNoModule.service('ComboPolicyNoService',['$q','$state','LoginService','CommonService','LoadApplicationScreenData','SisFormService', function($q,$state,LoginService,CommonService,LoadApplicationScreenData,SisFormService){

    var combo = this;
    var APP_ID;
    var FHR_ID;
    var SIS_ID;
    var PGL_ID;
    var AGENT_CD;
    var whereClauseObj = {};

    var policyBean = {};

    //Check policy no already generated for TERM
    this.loadComboDtls = function(COMBO_ID){
     var dfd = $q.defer();
        AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        console.log("Here loadComboDtls");
        LoadApplicationScreenData.loadOppFlags(COMBO_ID, AGENT_CD, 'combo','P').then(function(oppData){
            console.log("oppdata >>"+JSON.stringify(oppData));
            if(!oppData.oppFlags.REF_APPLICATION_ID || !oppData.oppFlags.REF_POLICY_NO)
            {
                console.log("loadPolicy IF")
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                SIS_ID = oppData.oppFlags.REF_SIS_ID;
                FHR_ID = oppData.oppFlags.REF_FHRID;
                if(!!oppData.oppFlags.REF_APPLICATION_ID)
                {
                    APP_ID = oppData.oppFlags.REF_APPLICATION_ID;
                    policyBean.REF_APPLICATION_ID = oppData.oppFlags.REF_APPLICATION_ID;
                }
                else
                {
                    APP_ID = CommonService.getRandomNumber();
                    policyBean.REF_APPLICATION_ID = APP_ID;
                }

                SisFormService.loadSavedSISData(AGENT_CD, SIS_ID).then(function(sisData){
                      PGL_ID = sisData.sisMainData.PGL_ID;
                      combo.checkPolicyNo('C').then(function(polBean){
                          if(polBean!=undefined && polBean.POLICY_NO!=undefined && polBean.PROD_TYPE!=undefined)
                          {
                              policyBean.REF_POLICY_NO = polBean.POLICY_NO;
                              policyBean.PROD_TYPE = polBean.PROD_TYPE;
                              policyBean.COMBO_ID = COMBO_ID;
                              combo.AssignPolicyNo(policyBean).then(function(){
                                  dfd.resolve(policyBean);
                              });
                          }
                      });
                });
            }
            else
            {
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                SIS_ID = oppData.oppFlags.REF_SIS_ID;
                FHR_ID = oppData.oppFlags.REF_FHRID;
                console.log(FHR_ID+"loadPolicy ELSE"+SIS_ID);
                policyBean.REF_APPLICATION_ID = oppData.oppFlags.REF_APPLICATION_ID;
                policyBean.REF_POLICY_NO = oppData.oppFlags.REF_POLICY_NO;
                policyBean.PROD_TYPE = 'C'; //hardcoded for For Term
                policyBean.COMBO_ID = COMBO_ID;
                console.log("policyBean:"+JSON.stringify(policyBean));
                 combo.AssignPolicyNo(policyBean).then(function(){
                      dfd.resolve(policyBean);
                  });
               // dfd.resolve(policyBean);
            }
        });
        return dfd.promise;
    }

    //Get one policty with status 'F'
    this.checkPolicyNo=function(prdType){
            var dfd = $q.defer();
            var policyBean = {};
            var whereClauseObj={};
                    whereClauseObj.POLICY_TYPE = prdType;
                    whereClauseObj.POLICY_FINAL_STATUS = 'F';
                    debug(JSON.stringify(whereClauseObj))
            CommonService.selectRecords(db,"LP_POLICY_ASSIGNMENT",false,'POLICY_NO',whereClauseObj).then(
                        function(res){
                            debug("LP_POLICY_ASSIGNMENT :"+res.rows.length);
                           if(!!res && res.rows.length>0){
                                debug(res.rows.item(0).POLICY_NO)
                                policyBean.POLICY_NO = res.rows.item(0).POLICY_NO;
                                policyBean.PROD_TYPE = prdType;
                                dfd.resolve(policyBean);
                           }
                           else
                            {
                                navigator.notification.alert(POLICYNO_MSG,function(){CommonService.hideLoading();},"Application","OK");
                                dfd.resolve(null);
                            }
                        }
                    );
            return dfd.promise;

        }

        //Assign term policy no
    this.AssignPolicyNo=function(policyBean){
            var dfd = $q.defer();

            var whereClauseObj={};
                            whereClauseObj.POLICY_NO = policyBean.REF_POLICY_NO;
                            whereClauseObj.POLICY_TYPE = 'C';
                            whereClauseObj.POLICY_FINAL_STATUS = 'F';
            var whereClauseObj1 = {};
                            whereClauseObj1.COMBO_ID = policyBean.COMBO_ID;
                            whereClauseObj1.AGENT_CD = AGENT_CD;
                            whereClauseObj1.RECO_PRODUCT_DEVIATION_FLAG = 'P';
            var whereClauseObj4 = {};
                            whereClauseObj4.COMBO_ID = policyBean.COMBO_ID;
                            whereClauseObj4.AGENT_CD = AGENT_CD;
                            whereClauseObj4.RECO_PRODUCT_DEVIATION_FLAG = 'T';
            var whereClauseObj2 = {};
                             whereClauseObj2.DOC_CAT_ID = SIS_ID;
                             whereClauseObj2.AGENT_CD = AGENT_CD;
                             //whereClauseObj2.DOC_CAT = "SIS";
            var whereClauseObj3 = {};
                           whereClauseObj3.DOC_CAT_ID = FHR_ID;
                           whereClauseObj3.AGENT_CD = AGENT_CD;
                           whereClauseObj3.DOC_CAT = "FHR";

                    CommonService.updateRecords(db, 'LP_POLICY_ASSIGNMENT',{"POLICY_FINAL_STATUS" :"U"}, whereClauseObj).then(
                    function(res){
                        debug("TERM POLICY_FINAL_STATUS update success !!"+JSON.stringify(whereClauseObj));

                    CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"REF_POLICY_NO" :policyBean.REF_POLICY_NO, "REF_APPLICATION_ID":policyBean.REF_APPLICATION_ID}, whereClauseObj1).then(
                            function(res){
                                debug("TERM LP_MYOPPORTUNITY update success !!"+JSON.stringify(whereClauseObj1));
                            });
                    CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"POLICY_NO" :policyBean.REF_POLICY_NO, "APPLICATION_ID":policyBean.REF_APPLICATION_ID}, whereClauseObj4).then(
                            function(res){
                                debug("TERM LP_MYOPPORTUNITY update success - 2 !!"+JSON.stringify(whereClauseObj1));
                            });
                    CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.REF_POLICY_NO}, whereClauseObj2).then(
                                function(res){
                                debug("TERM SIS HTML update success !!"+JSON.stringify(whereClauseObj2));
                            });
                     CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.REF_POLICY_NO}, whereClauseObj3).then(
                                                        function(res){
                                                        debug("TERM FHR HTML update success !!"+JSON.stringify(whereClauseObj3));
                                                        dfd.resolve(true);
                                                    });
                });
            return dfd.promise;
        }
}])


//Underwritting Service

policyNoModule.service('UWRuleService',['$q','$state','LoginService','CommonService','LoadAppProofList','ApplicationFormDataService', function($q,$state,LoginService,CommonService,LoadAppProofList, ApplicationFormDataService){
    var pngs = this;
    this.ImmunizationList = {};
    this.StudentIDList = {};
    pngs.HtmlFormOutput = "";
    this.getFinGridData = function(UWReq, sisDataPresent){
            var dfd = $q.defer();
            var finGridData = {};
            debug("UWReq : " + JSON.stringify(UWReq));
            if((UWReq.PGL_ID == SR_PGL_ID || UWReq.PGL_ID == SRP_PGL_ID) && !!sisDataPresent){
                debug("sisFormFData : " + JSON.stringify(ApplicationFormDataService.SISFormData.sisFormFData));
                pngs.getRiskAggregationTerm(UWReq, ApplicationFormDataService.SISFormData.sisFormFData).then(
                    function(finGridData){
                        dfd.resolve(finGridData);
                    }
                );
            }else {
                pngs.getRiskAggregation(UWReq).then(
                    function(riskAggregarion){
                        console.log("riskAggregarion : " + riskAggregarion);
                        finGridData.riskAggregarion = riskAggregarion;
                        pngs.getSumAtRisk(riskAggregarion,UWReq).then(
                            function(sumAtRisk){
                            console.log("sumAtRisk ; " + sumAtRisk);
                            finGridData.sumAtRisk = sumAtRisk;
                         pngs.getInsuranceIncome(sumAtRisk,UWReq).then(
                            function(Insurance_Income){
                                console.log("Insurance_Income : " +Insurance_Income);
                                finGridData.Insurance_Income = Insurance_Income;
                                pngs.compareSar(UWReq,sumAtRisk,Insurance_Income).then(
                                    function(boolValue){
                                        console.log("boolValue::"+boolValue);
                                        finGridData.boolValue = boolValue;
                                            if(boolValue == true){
                                               window.alert("Equal Insurance on Spouse/Parent required for STP eligibility.");
                                            }
                                        pngs.juvenileImmunizationDocList(sumAtRisk).then(
                                            function(docListVal){
                                                console.log("docListVal:"+docListVal);
                                                finGridData.docListVal = docListVal;
                                                    if(docListVal == true)
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization.DOC_ID = pngs.ImmunizationList.Insured.immunization;
                                                    else
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization = null;
                                            pngs.juvenileStudentIDDocList(UWReq,sumAtRisk).then(
                                                function(studIDValue){
                                                console.log("studIDValue:"+studIDValue);
                                                    if(studIDValue == true)
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec.DOC_ID = pngs.StudentIDList.Insured.studIDRec;
                                                    else
                                                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec = null;

                                                         pngs.getFinancialLimit(sumAtRisk,UWReq).then(
                                                            function(financialLimit){
                                                                console.log("financialLimit : " +financialLimit);
                                                                finGridData.financialLimit = financialLimit;
                                                                pngs.getHumanLiveValue(UWReq).then(
                                                                    function(humanLiveValue){
                                                                        console.log("humanLiveValue : " + humanLiveValue + " finGridData.financialLimit : " + finGridData.financialLimit);
                                                                        finGridData.humanLiveValue = humanLiveValue;
                                                                        if(!UWReq.IS_TERM && parseInt(finGridData.financialLimit) > parseInt(humanLiveValue)){
                                                                            CommonService.hideLoading();
                                                                            window.alert("Applicant not financially eligible for the cover opted, kindly reduce cover amount.\nFinancial Limit : "+finGridData.financialLimit+" is greater than \nHuman Live Value : "+finGridData.humanLiveValue);
                                                                            debug("Applicant not financially eligible for the cover opted, kindly reduce cover amount")
                                                                            dfd.resolve(null);
                                                                        }else{
                                                                            pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                                                                function(mndDocFlags){
                                                                                    console.log("mndDocFlags.finQnFlg : " + mndDocFlags.finQnFlg + " mndDocFlags.standrdFlag : " + mndDocFlags.standrdFlag);
                                                                                    finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                                                    finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                                                    pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                                                        function(incTyp){
                                                                                            console.log("in  cTyp : " + incTyp);
                                                                                            finGridData.incTyp = incTyp;
                                                                                            pngs.saveFinGridData(finGridData,UWReq).then(
                                                                                                function(FinGridData){
                                                                                                    debug("Underwritting Rule Executed...")
                                                                                                    dfd.resolve(FinGridData);
                                                                                                }
                                                                                            );
                                                                                            //dfd.resolve(finGridData);
                                                                                        }
                                                                                    );
                                                                                }
                                                                            );
                                                                        }
                                                                    }
                                                                );
                                                            }
                                                        );
                                                });
                                         });
                                    });
                                });
                            }
                        );
                    }
                );
            }
            return dfd.promise;
        };

    this.getRiskAggregation = function(UWReq){
        var dfd = $q.defer();
        var sumAssuared = 0;
        var riskAggregarion = 0;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT SUM_ASSURED FROM LP_APPLICATION_MAIN WHERE APPLICATION_ID=? AND AGENT_CD=?",[UWReq.APP_ID,UWReq.AGENT_CD],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            sumAssuared = res.rows.item(0).SUM_ASSURED;
                            console.log("sumAssuared : " + sumAssuared);
                            pngs.getRiskAggregationFctor(UWReq).then(
                                function(raMulfFactor){
                                    console.log("raMulfFactor : " + raMulfFactor);
                                    if(raMulfFactor !== undefined){
                                        riskAggregarion = parseFloat(sumAssuared) * parseFloat(raMulfFactor);
                                        if(UWReq.PGL_ID == "226"){
                                            debug("GIP SAR : " + UWReq.SAR);
                                            riskAggregarion = !!UWReq.SAR ? parseInt(UWReq.SAR) : 0
                                        }
                                        dfd.resolve(Math.round(riskAggregarion));
                                    }else{
                                        if(UWReq.PGL_ID == "226"){
                                            debug("GIP SAR2 : " + UWReq.SAR);
                                            riskAggregarion = !!UWReq.SAR ? parseInt(UWReq.SAR) : 0
                                        }
                                        dfd.resolve(Math.round(riskAggregarion));
                                    }
                                }
                            );
                        }
                    },
                    function(tx,err){
                        console.log("Error in getRiskAggregation().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getRiskAggregation(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };
    this.getRiskAggregationFctor = function(UWReq){
        var dfd = $q.defer();
        var raMulfFactor = 0;
        var AGE = (UWReq.IS_PROP=='Y' ? UWReq.INS_AGE : UWReq.PRO_AGE);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select RISK_AGRT_MULTI from LP_PLAN_RISK_AGG where (PLAN_CODE = ?) AND (CAST(MIN_AGE AS int)<=?) AND (CAST(MAX_AGE AS int) >= ?)",[UWReq.PLAN_CODE,AGE,AGE],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                           raMulfFactor = res.rows.item(0).RISK_AGRT_MULTI;
                           console.log("raMulfFactor : " + raMulfFactor);
                           dfd.resolve(raMulfFactor);
                        }else
                            dfd.resolve(raMulfFactor);
                    },
                    function(tx,err){
                        console.log("Error in getRiskAggregationFctor().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getRiskAggregationFctor(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getSumAtRisk = function(riskAggregarion,UWReq){
        var dfd = $q.defer();
        var riderCode = null;
        var riderSA = 0;
        var sumAtRisk = 0;

        //dfd.resolve(riskAggregarion + 0);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select RIDER_CODE,RIDER_COVERAGE from LP_SIS_SCREEN_D where (SIS_ID = ?) AND (AGENT_CD = ?)",[UWReq.SIS_ID,UWReq.AGENT_CD],
                    function(tx,res){
                        console.log("res.rows.length : " + res.rows.length);
                        if(res.rows.length>0){
                            riderCode = res.rows.item(0).RIDER_CODE;
                            riderSA = res.rows.item(0).RIDER_COVERAGE;
                            console.log("riderCode : " + riderCode + " riderSA : " + riderSA);
                            CommonService.executeSql(tx,"select * from LP_PLAN_MASTER where PLAN_CODE = ? AND RIDER_TYPE IN(?,?)",[riderCode,'CL','T'],
                            //CommonService.executeSql(tx,"select * from LP_PLAN_MASTER",[],
                                function(tx,res){
                                    console.log("LP_PLAN_MASTER Length : " + res.rows.length);
                                    if(res.rows.length>0){
                                        sumAtRisk = parseInt(riskAggregarion) + parseInt(riderSA);
                                        dfd.resolve(sumAtRisk);
                                    }else
                                        dfd.resolve(parseInt(riskAggregarion));
                                },
                                function(tx,err){
                                    console.log("Error in getSumAtRisk().transaction(): " + err.message);
                                    dfd.resolve(null);
                                }
                            );

                        }else
                            dfd.resolve(parseInt(riskAggregarion));
                    },
                    function(tx,err){
                        console.log("Error in getSumAtRisk().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getSumAtRisk(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getFinancialLimit = function(sumAtRisk,UWReq){
        var dfd = $q.defer();
        var TotSumAssured_T=0;
        var TotSumAssured_O=0;
        var DecisionDetails1='Standard';
        var DecisionDetails2='With Revised or Extra Premium';
        var financialLimit = 0;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT SUM(SUM_ASSURED) AS TOTSA_T FROM LP_APP_EXIST_INS_SCRN_D WHERE APPLICATION_ID=? AND CUST_TYPE=? AND TATA_OTHER_FLAG=? AND AGENT_CD=?",[UWReq.APP_ID,'C01','T',UWReq.AGENT_CD],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){

                            TotSumAssured_T = ((!!res.rows.item(0).TOTSA_T) ? res.rows.item(0).TOTSA_T : 0);
                            console.log("TotSumAssured_T : " + TotSumAssured_T);
                            CommonService.executeSql(tx,"SELECT SUM(SUM_ASSURED) AS TOTSA_O FROM LP_APP_EXIST_INS_SCRN_D WHERE APPLICATION_ID=? AND CUST_TYPE=? AND TATA_OTHER_FLAG=? AND DECISION in(?,?) AND AGENT_CD=?",[UWReq.APP_ID,'C01','O',DecisionDetails1,DecisionDetails2,UWReq.AGENT_CD],
                                function(tx,res){
                                    console.log("res.rows.length: " + res.rows.length);
                                    if(res.rows.length>0){
                                        TotSumAssured_O = ((!!res.rows.item(0).TOTSA_O) ? res.rows.item(0).TOTSA_O : 0);
                                        console.log("TotSumAssured_O :" + TotSumAssured_O);
                                        if(!!UWReq.FSAR && UWReq.FSAR!="0" && parseInt(UWReq.FSAR)!=0)
                                          {
                                            debug(UWReq.FSAR+"alpha ID MATCH"+parseInt(UWReq.FSAR));
                                            financialLimit = parseInt(sumAtRisk) + parseInt(UWReq.FSAR) + parseInt(TotSumAssured_O);
                                          }
                                        else
                                          {
                                            debug("NO MATCH");
                                            financialLimit = parseInt(sumAtRisk) + parseInt(TotSumAssured_T) + parseInt(TotSumAssured_O);
                                          }
                                        dfd.resolve(parseInt(financialLimit));
                                    }
                                    else {
                                      if(!!UWReq.FSAR && UWReq.FSAR!="0" && UWReq.FSAR!=0)
                                          financialLimit = parseInt(UWReq.FSAR)
                                      dfd.resolve(parseInt(financialLimit));
                                    }
                                },
                                function(tx,err){
                                    console.log("Error in getFinancialLimit().transaction(): " + err.message);
                                    dfd.resolve(null);
                                }
                            );
                        }
                        else {
                          if(!!UWReq.FSAR && UWReq.FSAR!="0" && UWReq.FSAR!=0)
                              financialLimit = parseInt(UWReq.FSAR)
                          dfd.resolve(parseInt(financialLimit));
                        }
                    },
                    function(tx,err){
                        console.log("Error in getFinancialLimit().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getFinancialLimit(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getInsuranceIncome = function(sumAtRisk,UWReq){
        var dfd = $q.defer();
        var Insurance_Income = 0;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT (INSURANCE_DETAILS) AS Ins_Income FROM LP_APP_FH_SCRN_E WHERE APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?",[UWReq.APP_ID,UWReq.AGENT_CD,'C11'],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            console.log("Insurance Income is :" + res.rows.item(0).Ins_Income);
                            Insurance_Income = res.rows.item(0).Ins_Income;
                            dfd.resolve(parseInt(Insurance_Income));
                        }
                        dfd.resolve(parseInt(Insurance_Income));
                    },
                    function(tx,err){
                        console.log("Error in getFinancialLimit().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getFinancialLimit(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    }

    this.compareSar = function(UWReq,sumAtRisk,Insurance_Income){
    console.log("SumAtRisk Value is::"+sumAtRisk+"Insurance_Income is::"+Insurance_Income+"BusinessType::"+BUSINESS_TYPE+"OCCU_CLASS::"+UWReq.OCCU_CLASS);
    var dfd = $q.defer();
    var boolValue = false;
    if(UWReq.OCCU_CLASS == 'Housewife'){
         if(BUSINESS_TYPE == "GoodSolution" || BUSINESS_TYPE == "LifePlaner" || BUSINESS_TYPE == "iWealth"){
            if(sumAtRisk > 1000000){
                    if(sumAtRisk > Insurance_Income)
                        boolValue = true;
                    else
                        boolValue = false;

                    dfd.resolve(boolValue);
                }
                else
                    {
                        boolValue = false;
                        dfd.resolve(boolValue);
                    }

            }
            else{
                if(sumAtRisk > 2500000){
                    if(sumAtRisk > Insurance_Income)
                        boolValue = true;
                    else
                        boolValue =false;

                    dfd.resolve(boolValue);
                }
                else
                   {
                        boolValue = false;
                         dfd.resolve(boolValue);
                   }
            }

        }
        else
            {
                boolValue = false;
                dfd.resolve(boolValue);
            }
         return dfd.promise;
    }

    //Added for Immunization List
    this.juvenileImmunizationDocList = function(SumAtRisk){
       console.log("Immunization Record List is::"+JSON.stringify(this.ImmunizationList)+"SumAtRisk is::"+SumAtRisk);
       pngs.INS_DOB = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
       pngs.Age = CommonService.getAge(pngs.INS_DOB);
       debug("Insured Age on PolNo Page is::"+JSON.stringify(pngs.Age));
       var dfd = $q.defer();
       var docListVal = false;
       ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.immunization = {};

            if(SumAtRisk > 5000000)
            {
                if(parseInt(pngs.Age) >= 0 && parseInt(pngs.Age) <= 5)
                {
                    docListVal = true;
                    dfd.resolve(docListVal);
                    console.log("Inside StudentImmunization List::"+docListVal);
                }
                else
                {
                    docListVal = false;
                    dfd.resolve(docListVal);
                    console.log("Inside else::"+docListVal);
                }

            }
            else
            {
                docListVal = false;
                dfd.resolve(docListVal);
            }
        return dfd.promise;
    }
    //Added for Student List
    this.juvenileStudentIDDocList = function(UWReq,sumAtRisk){
    var dfd = $q.defer();
    var studIDValue = false;
    pngs.INS_DOB = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
    pngs.Age = CommonService.getAge(pngs.INS_DOB);
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.studIDRec = {};
        if(sumAtRisk > 5000000){
            if(UWReq.OCCU_CLASS == 'Student/Juvenile' && (parseInt(pngs.Age) > 5 && parseInt(pngs.Age) < 18))
            {
                studIDValue = true;
                dfd.resolve(studIDValue);
                console.log("Inside StudentID Proof::"+studIDValue);
            }
            else
            {
                studIDValue = false;
                dfd.resolve(studIDValue);
            }
        }
        else
        {
            studIDValue = false;
            dfd.resolve(studIDValue);
        }
        return dfd.promise;
    }

    this.getHumanLiveValue = function(UWReq){
        var dfd = $q.defer();
        var annualIncome = 0;
        var hvl = 0;
        var propAge = (UWReq.IS_PROP=='Y' ? UWReq.INS_AGE : UWReq.PRO_AGE);
        var custType = (UWReq.IS_PROP=='Y' ? 'C01' : 'C02');
        var pglId = UWReq.PGL_ID;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT ANNUAL_INCOME FROM LP_APP_CONTACT_SCRN_A WHERE APPLICATION_ID=? AND CUST_TYPE=? AND AGENT_CD=?",[UWReq.APP_ID,custType,UWReq.AGENT_CD],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                          if(!!res.rows.item(0).ANNUAL_INCOME)
                            annualIncome = res.rows.item(0).ANNUAL_INCOME;
                            console.log("annualIncome : " + annualIncome);
                            pngs.getIncmMultpleFctor(propAge, pglId).then(
                                function(mulfFactor){
                                    console.log("multFactor : " + mulfFactor);
                                    if(mulfFactor !== undefined){
                                        hvl = parseFloat(annualIncome) * parseFloat(mulfFactor);
                                        dfd.resolve(Math.round(hvl));
                                    }else{
                                        dfd.resolve(Math.round(hvl));
                                    }
                                }
                            );
                        }
                    },
                    function(tx,err){
                        console.log("Error in getHumanLiveValue().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getHumanLiveValue(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getIncmMultpleFctor = function(propAge, pglId){
        var dfd = $q.defer();
        var multFactor = 0;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT ANN_INCOME_MULTI FROM LP_FHR_FIN_ELIGIBILITY_GRID WHERE CAST(CUSTOMER_MIN_AGE AS int)<=? AND CAST(CUSTOMER_MAX_AGE as int)>=? AND PRODUCT_PGL_LIST like '%" + pglId + "%'",[propAge,propAge],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            multFactor = res.rows.item(0).ANN_INCOME_MULTI;
                            console.log("multFactor : " + multFactor);
                            dfd.resolve(parseFloat(multFactor));
                        }else{
                            dfd.resolve(parseFloat(multFactor));
                        }
                    },
                    function(tx,err){
                        console.log("Error in getIncmMultpleFctor().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getIncmMultpleFctor(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getmandatoryDocFetch = function(financialLimit,UWReq){
        console.log("BUSINESS_TYPE::"+BUSINESS_TYPE);
        var dfd = $q.defer();
        var mndDocFlags = {};
        var propAge = (UWReq.IS_PROP=='Y' ? UWReq.INS_AGE : UWReq.PRO_AGE);
        mndDocFlags.finQnFlg = null;
        mndDocFlags.standrdFlag = null;
        if(BUSINESS_TYPE=="IndusSolution")
            mndDocFlags.IsIbl = 'Y'
        else
            mndDocFlags.IsIbl = 'N'
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT FIN_QN_FLAG,INCOMEPROOF_STD_FLAG FROM LP_FHR_FINANCIAL_REQ_GRID WHERE (CAST(CUSTOMER_MIN_AGE as int)<=?) AND (CAST(CUSTOMER_MAX_AGE as int)>=?) AND (CAST(SUM_ASSURED_MIN as int)<=?) AND (CAST(SUM_ASSURED_MAX as int)>=?) AND IS_IBL = ?",[propAge,propAge,financialLimit,financialLimit,mndDocFlags.IsIbl],
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length+"propAge::"+propAge+"financialLimit::"+financialLimit+"mndDocFlags.IsIbl::"+mndDocFlags.IsIbl);
                        if(res.rows.length>0){
                            if(!!UWReq.COMBO_ID)
                                mndDocFlags.finQnFlg = 'Y';
                            else
                                mndDocFlags.finQnFlg = res.rows.item(0).FIN_QN_FLAG;
                            mndDocFlags.standrdFlag = res.rows.item(0).INCOMEPROOF_STD_FLAG;
                            console.log("mndDocFlags.finQnFlg : " + mndDocFlags.finQnFlg + " mndDocFlags.standrdFlag : " + mndDocFlags.standrdFlag);
                            dfd.resolve(mndDocFlags);
                        }
                    },
                    function(tx,err){
                        console.log("Error in getmandatoryDocFetch().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getmandatoryDocFetch(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getMasterValues = function(standrdFlag,UWReq){
        var dfd = $q.defer();
        var standardCondition="";
        var argsList = [];
        var custType = (UWReq.IS_PROP=='Y' ? 'C01' : 'C02');
        if(standrdFlag.toLowerCase() === "Y".toLowerCase()){
            standardCondition = "STANDARD_FLAG=? AND ";
            argsList = ['Y','Income Proof','1'];
        }
        else
            argsList = ['Income Proof','1'];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE " + standardCondition + "MPDOC_DESCRIPTION=? AND ISACTIVE=?",argsList,
                    function(tx,res){
                        console.log("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            var docIdArr=[];
                            for(var i=0;i<res.rows.length;i++)
                                docIdArr[i]=res.rows.item(i).DOC_ID;
                            CommonService.executeSql(tx,"SELECT INCOME_PROOF_DOC_ID FROM LP_APP_CONTACT_SCRN_A WHERE APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?",[UWReq.APP_ID,UWReq.AGENT_CD,custType],
                                function(tx,res){
                                    console.log("res.rows.length: " + res.rows.length);
                                    if(res.rows.length>0){
                                        var ipDocId = res.rows.item(0).INCOME_PROOF_DOC_ID
                                        console.log("Income Proof DocId ::" + ipDocId);
                                        if((docIdArr.indexOf(ipDocId))>=0){
                                               dfd.resolve(null);
                                        }else{
                                            if(standrdFlag.toLowerCase()==="Y".toLowerCase()){
                                                //incTyp = "standard";
                                                dfd.resolve("standard");
                                            }
                                            else
                                                //incTyp = "surrogate";
                                                dfd.resolve("surrogate");

                                        }
                                    }else
                                        //incTyp = "standard";
                                        dfd.resolve("standard");
                                },
                                function(tx,err){
                                    console.log("Error in getMasterValues().transaction(): " + err.message);
                                    dfd.resolve(null);
                                }
                            );
                        }
                    },
                    function(tx,err){
                        console.log("Error in getMasterValues().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in getMasterValues(): " + err.message);
                dfd.resolve(null);
            }
        );

        return dfd.promise;
    };

    this.saveFinGridData = function(FinGridData,UWReq){
        var dfd = $q.defer();
        console.log("FinGridData : " + JSON.stringify(FinGridData));
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"update LP_APP_SAR_MED_SCREEN_K set RISK_AGRT_VALUE = ?,SUM_AT_RISK_VALUE = ?,FINANCIAL_LIMIT_VALUE = ?,HUMAN_LIFE_VALUE = ? where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[FinGridData.riskAggregarion,FinGridData.sumAtRisk,FinGridData.financialLimit,FinGridData.humanLiveValue,UWReq.APP_ID,UWReq.AGENT_CD,'C01'],
                    function(tx,res){
                        console.log("LP_APP_SAR_MED_SCREEN_K inserted: " + FinGridData.riskAggregarion);
                        dfd.resolve(FinGridData);
                    },
                    function(tx,err){
                        console.log("Error in saveFinGridData().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                console.log("Error in saveFinGridData(): " + err.message);
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };

    this.getRiskAggregationTerm = function(UWReq, sisFormFData){
        var dfd = $q.defer();
        var finGridData = {};
        var planCode = UWReq.PLAN_CODE;
        var option = (planCode.indexOf("N1") != -1 ? 1 : (planCode.indexOf("N2") != -1 ? 2 : (planCode.indexOf("N3") != -1 ? 3 : 4)));
        var polTerm = parseInt(!!ApplicationFormDataService.SISFormData.sisFormBData.POLICY_TERM ? ApplicationFormDataService.SISFormData.sisFormBData.POLICY_TERM : 0);
        var sumAssuared = parseInt(!!ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED ? ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED : 0);
        var riskAggregarion = 0;
        if(option==1){
            riskAggregarion = sumAssuared;
        }else if (option==2) {
            riskAggregarion = 2 * sumAssuared;
        }else if (option==3) {
            riskAggregarion = (((sumAssuared * 5)/100) * (polTerm > 20 ? 20 : polTerm)) + sumAssuared;
        }else if (option==4) {
            riskAggregarion = (((sumAssuared * 5)/100) * (polTerm > 20 ? 20 : polTerm)) + (2 * sumAssuared);
        }
        finGridData.riskAggregarion = riskAggregarion;
        pngs.getSumAtRisk(riskAggregarion, UWReq).then(
            function(sumAtRisk) {
                debug("sumAtRisk : " + sumAtRisk);
                finGridData.sumAtRisk = sumAtRisk;
                return pngs.getFinancialLimit(sumAtRisk, UWReq).then(
                    function(financialLimit){
                        debug("financialLimit : " + financialLimit);
                        finGridData.financialLimit = financialLimit;
                        return finGridData;
                    }
                );
            }
        ).then(
            function(finGridData){
                if(!!finGridData){
                    pngs.validateTermProduct(UWReq, sisFormFData, finGridData).then(
                        function(finGridData){
                            debug("finGridData : " + JSON.stringify(finGridData));
                            dfd.resolve(finGridData);
                        }
                    );
                }
            }
        );
        return dfd.promise;
    }

    this.validateTermProduct = function(UWReq, sisFormFData, finGridData){
        var dfd = $q.defer();
        var propAge = (UWReq.IS_PROP=='Y' ? UWReq.INS_AGE : UWReq.PRO_AGE);
        var pglId = UWReq.PGL_ID;
        pngs.getIncmMultpleFctor(propAge, pglId).then(
            function(mulfFactor){
                debug("mulfFactor : " + mulfFactor);
                if(!!mulfFactor){
                    /* Salaried Form16*/
                     if(sisFormFData.OCCUPATION_CLASS_NBFE_CODE == 1 && sisFormFData.INCOME_PROOF_DOC_ID=="1016010050"){
                        var avgAnnSal = Math.round((parseInt(!!sisFormFData.S_FRM16_SAL1 ? sisFormFData.S_FRM16_SAL1 : 0) + parseInt(!!sisFormFData.S_FRM16_SAL2 ? sisFormFData.S_FRM16_SAL2 : 0) + parseInt(!!sisFormFData.S_FRM16_SAL3 ? sisFormFData.S_FRM16_SAL3 : 0)) / 3);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 300000;  //Initially same min & max Threshold for all flow
                        //sisFormFData.GROUP_EMPLOYEE == 'TATA Group Employee' ? 200000 : 300000;
                        var maxAmountAllowedNonRI = 1200000;
                        //sisFormFData.GROUP_EMPLOYEE == 'TATA Group Employee' ? 700000 : 1200000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(sisFormFData.OCCUPATION_CLASS_NBFE_CODE == 1 && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010056" || sisFormFData.INCOME_PROOF_DOC_ID=="1016010432" || sisFormFData.INCOME_PROOF_DOC_ID=="1016010471")){
                        var avgAnnSal = Math.round(parseInt(!!sisFormFData.S_EMP_SAL ? sisFormFData.S_EMP_SAL : 0));
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 300000;
                        var maxAmountAllowedNonRI = 1200000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(sisFormFData.OCCUPATION_CLASS_NBFE_CODE == 1 && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010441")){
                        var avgAnnSal = Math.round(((parseInt(!!sisFormFData.S_BNK_SAL1 ? sisFormFData.S_BNK_SAL1 : 0) + parseInt(!!sisFormFData.S_BNK_SAL2 ? sisFormFData.S_BNK_SAL2 : 0) + parseInt(!!sisFormFData.S_BNK_SAL3 ? sisFormFData.S_BNK_SAL3 : 0) + parseInt(!!sisFormFData.S_BNK_SAL4 ? sisFormFData.S_BNK_SAL4 : 0) + parseInt(!!sisFormFData.S_BNK_SAL5 ? sisFormFData.S_BNK_SAL5 : 0) + parseInt(!!sisFormFData.S_BNK_SAL6 ? sisFormFData.S_BNK_SAL6 : 0))/6)*12);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 300000;
                        var maxAmountAllowedNonRI = 1200000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(sisFormFData.OCCUPATION_CLASS_NBFE_CODE == 1 && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010052")){
                        var avgAnnSal = Math.round(((parseInt(!!sisFormFData.S_SAL_SAL1 ? sisFormFData.S_SAL_SAL1 : 0) + parseInt(!!sisFormFData.S_SAL_SAL2 ? sisFormFData.S_SAL_SAL2 : 0) + parseInt(!!sisFormFData.S_SAL_SAL3 ? sisFormFData.S_SAL_SAL3 : 0))/3)*12);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 300000;
                        var maxAmountAllowedNonRI = 1200000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(sisFormFData.OCCUPATION_CLASS_NBFE_CODE == 1 && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010472" || sisFormFData.INCOME_PROOF_DOC_ID=="1016010473")){
                        var avgAnnSal = Math.round((parseInt(!!sisFormFData.S_COI_SAL1 ? sisFormFData.S_COI_SAL1 : 0) + parseInt(!!sisFormFData.S_COI_SAL2 ? sisFormFData.S_COI_SAL2 : 0))/2);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 300000;
                        var maxAmountAllowedNonRI = 1200000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(([2,3,7,8].indexOf(parseInt(!!sisFormFData.OCCUPATION_CLASS_NBFE_CODE ? sisFormFData.OCCUPATION_CLASS_NBFE_CODE : 0)) != -1) && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010049")){
                        var avgAnnSal = Math.round((parseInt(!!sisFormFData.SE_FRM16_SAL1 ? sisFormFData.SE_FRM16_SAL1 : 0) + parseInt(!!sisFormFData.SE_FRM16_SAL2 ? sisFormFData.SE_FRM16_SAL2 : 0) + parseInt(!!sisFormFData.SE_FRM16_SAL3 ? sisFormFData.SE_FRM16_SAL3 : 0))/3);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 500000;
                        var maxAmountAllowedNonRI = 2000000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(([2,3,7,8].indexOf(parseInt(!!sisFormFData.OCCUPATION_CLASS_NBFE_CODE ? sisFormFData.OCCUPATION_CLASS_NBFE_CODE : 0)) != -1) && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010472" || sisFormFData.INCOME_PROOF_DOC_ID=="1016010473")){
                        var avgAnnSal = Math.round((parseInt(!!sisFormFData.SE_COI_PROFIT1 ? sisFormFData.SE_COI_PROFIT1 : 0) + parseInt(!!sisFormFData.SE_COI_PROFIT2 ? sisFormFData.SE_COI_PROFIT2 : 0) + parseInt(!!sisFormFData.SE_COI_SAL1 ? sisFormFData.SE_COI_SAL1 : 0) + parseInt(!!sisFormFData.SE_COI_SAL2 ? sisFormFData.SE_COI_SAL2 : 0))/2);
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 500000;
                        var maxAmountAllowedNonRI = 2000000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                    else if(([2,3,7,8].indexOf(parseInt(!!sisFormFData.OCCUPATION_CLASS_NBFE_CODE ? sisFormFData.OCCUPATION_CLASS_NBFE_CODE : 0)) != -1) && (sisFormFData.INCOME_PROOF_DOC_ID=="1016010315")){
                        var avgAnnSal = Math.round(parseInt(sisFormFData.SE_OTH_PROFIT));
                        var maxSaAllowed = Math.round(parseFloat(avgAnnSal) * parseFloat(mulfFactor));
                        var currSa = finGridData.financialLimit;
                        var maxAmountAllowedRI = 500000;
                        var maxAmountAllowedNonRI = 2000000;
                        pngs.validateEachIncomeProof(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq).then(
                            function(res){
                                debug("res : " + res);
                                pngs.chackFinalEligibility(ApplicationFormDataService.SISFormData.sisFormFData).then(
                                    function(buttonIndex){
                                        debug("buttonIndex : " + buttonIndex);
                                        finGridData.buttonIndex = buttonIndex;
                                        finGridData.humanLiveValue = maxSaAllowed;
                                        pngs.getmandatoryDocFetch(finGridData.financialLimit,UWReq).then(
                                            function(mndDocFlags){
                                                debug("finQnFlg :" + mndDocFlags.finQnFlg + "standrdFlag :" + mndDocFlags.standrdFlag);
                                                finGridData.finQnFlg = mndDocFlags.finQnFlg;
                                                finGridData.standrdFlag = mndDocFlags.standrdFlag;
                                                pngs.getMasterValues(mndDocFlags.standrdFlag,UWReq).then(
                                                    function(incTyp){
                                                        debug("incTyp : " + incTyp);
                                                        finGridData.incTyp = incTyp;
                                                        pngs.saveFinGridData(finGridData,UWReq).then(
                                                            function(FinGridData){
                                                                debug("Underwritting Rule Executed...")
                                                                dfd.resolve(FinGridData);
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }else{
                        debug("Income Proof Incorrect.");
                        dfd.resolve(null);
                    }//main if
                }
            }
        );
        return dfd.promise;
    };

    this.validateEachIncomeProof = function(avgAnnSal, maxSaAllowed, currSa, maxAmountAllowedRI, maxAmountAllowedNonRI, sisFormFData, UWReq){
        var dfd = $q.defer();
        debug("avgAnnSal : " + avgAnnSal + "\tmaxSaAllowed : " + maxSaAllowed + "\tcurrSar : " + currSa + "\tmaxAmountAllowedRI : " + maxAmountAllowedRI + "\tmaxAmountAllowedNonRI : " + maxAmountAllowedNonRI);
        if(sisFormFData.RESIDENT_STATUS_CODE == "RI"){
            debug("RI");
            // For IBL Flow Date of joining flow
            /*if(sisFormFData.GROUP_EMPLOYEE == 'IBL Employee' && !!sisFormFData.IBL_JOINING_DATE){
                var monthCnt = parseInt(CommonService.getNoOfMonths(sisFormFData.IBL_JOINING_DATE));
                debug("monthCnt1 : " + monthCnt);
                if(monthCnt<=6){
                    if(parseInt(avgAnnSal) >= parseInt(500000)){
                        debug("11");
                        sisFormFData.ELIGIBILITY_FLAG_CITY = 'Y';
                    }
                    else{
                        debug("12");
                        sisFormFData.ELIGIBILITY_FLAG_CITY = 'N';
                    }
                }
                else
                    sisFormFData.ELIGIBILITY_FLAG_CITY = 'Y';
            }*/
            if(parseInt(avgAnnSal) < parseInt(maxAmountAllowedRI)){
                debug("1");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'N';
                sisFormFData.ELIGIBILITY_CATEGORY = 'Income';
                sisFormFData.ELIGIBILITY_REASON = 'Income for Resident Indian';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;

                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }else if(parseInt(avgAnnSal) >= parseInt(maxAmountAllowedRI) && parseInt(maxSaAllowed) < parseInt(currSa)){
                debug("2");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'N';
                sisFormFData.ELIGIBILITY_CATEGORY = 'Income';
                sisFormFData.ELIGIBILITY_REASON = 'Cover not justified';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;
                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }else {
                debug("3");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'Y';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;
                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }
        }else {
            debug("NOT RI");
            if(parseInt(avgAnnSal) < parseInt(maxAmountAllowedNonRI)){
                debug("1");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'N';
                sisFormFData.ELIGIBILITY_CATEGORY = 'Income';
                sisFormFData.ELIGIBILITY_REASON = 'Income for Non Resident Indian';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;
                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }
            else if(parseInt(avgAnnSal) >= parseInt(maxAmountAllowedNonRI) && parseInt(maxSaAllowed) < parseInt(currSa)){
                debug("2");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'N';
                sisFormFData.ELIGIBILITY_CATEGORY = 'Income';
                sisFormFData.ELIGIBILITY_REASON = 'Cover not justified';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;
                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }
            else {
                debug("3");
                sisFormFData.POLICY_NO = UWReq.POLICY_NO;
                sisFormFData.ELIGIBILITY_FLAG_INCOME = 'Y';
                sisFormFData.ISSYNCED = 'N';
                //NEW FIELDS Added - TEMPLATE
                sisFormFData.SAR = currSa;
                sisFormFData.AVG_INCOME = avgAnnSal;
                sisFormFData.ELIGIBILITY_VALUE = maxSaAllowed;
                var whereClauseObj = {};
                whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                //whereClauseObj.LEAD_ID = sisFormFData.LEAD_ID;
                //whereClauseObj.FHR_ID = sisFormFData.FHR_ID;
                whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                    function(res){
                        console.log("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                        if(!!res){
                            ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                            dfd.resolve("S");
                        }
                    }
                );
            }
        }
        return dfd.promise;
    };

    this.chackFinalEligibility = function(sisFormFData){
        var dfd = $q.defer();
        var reasonCat = "";
        var reason = "";
        if(!!sisFormFData && (sisFormFData.ELIGIBILITY_FLAG_CITY=='N' || sisFormFData.ELIGIBILITY_FLAG_COUNTRY=='N' || sisFormFData.ELIGIBILITY_FLAG_OCCCLASS == 'N' || sisFormFData.ELIGIBILITY_FLAG_EDUCATION=='N' || sisFormFData.ELIGIBILITY_FLAG_INCOME=='N' || sisFormFData.ELIGIBILITY_FLAG_OCC=='N')){
            if(sisFormFData.ELIGIBILITY_FLAG_INCOME=='N'){
                reasonCat = sisFormFData.ELIGIBILITY_CATEGORY;
                reason = sisFormFData.ELIGIBILITY_REASON;
            }
            if(sisFormFData.ELIGIBILITY_FLAG_CITY=='N'){
                reasonCat += !!reasonCat ? ', City' : 'City';
                reason += !!reason ? ', City criteria not met' : 'City criteria not met';
            }
            if(sisFormFData.ELIGIBILITY_FLAG_COUNTRY=='N'){
                reasonCat += !!reasonCat ? ', Country' : 'Country';
                reason += !!reason? ', Country criteria not met' : 'Country criteria not met';
            }
            if(sisFormFData.ELIGIBILITY_FLAG_OCCCLASS=='N'){
                reasonCat += !!reasonCat ? ', Occupation Class' : 'Occupation Class';
                reason += !!reason ? ', Occupation class criteria not met' : 'Occupation class criteria not met';
            }
            if(sisFormFData.ELIGIBILITY_FLAG_EDUCATION=='N'){
                reasonCat += !!reasonCat ? ', Education' : 'Education';
                reason += !!reason ? ', Education criteria not met' : 'Education criteria not met';
            }
            if(sisFormFData.ELIGIBILITY_FLAG_OCC=='N'){
                reasonCat += !!reasonCat ? ', Occupation' : 'Occupation';
                reason += !!reason ? ', Occupation criteria not met' : 'Occupation criteria not met';
            }
            debug("reasonCat : " + reasonCat);
            debug("reason : " + reason);
            navigator.notification.confirm(reason,
                function(buttonIndex){
                    sisFormFData.ELIGIBILITY_FLAG = 'N';
                    sisFormFData.ISSYNCED = 'N';
                    sisFormFData.ELIGIBILITY_CATEGORY = reasonCat;
                    sisFormFData.ELIGIBILITY_REASON = reason;
                    var whereClauseObj = {};
                    whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
                    whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
                    CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                        function(res){
                            debug("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                            if(!!res){
                                ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                                dfd.resolve(buttonIndex);
                            }
                        }
                    );
                },
                /*'MSG'*/"Insured do not qualify for SR/SR+. Either quit or provide an approval from your superviser.",
                'Continue, Quit'
            );
        }else {
            sisFormFData.ELIGIBILITY_FLAG = 'Y';
            sisFormFData.ISSYNCED = 'N';
            sisFormFData.ELIGIBILITY_CATEGORY = null;
            sisFormFData.ELIGIBILITY_REASON = null;
            var whereClauseObj = {};
            whereClauseObj.AGENT_CD = sisFormFData.AGENT_CD;
            whereClauseObj.SIS_ID = sisFormFData.SIS_ID;
            CommonService.updateRecords(db, 'LP_TERM_VALIDATION', sisFormFData, whereClauseObj).then(
                function(res){
                    debug("LP_TERM_VALIDATION update success !!"+JSON.stringify(res));
                    if(!!res){
                        ApplicationFormDataService.SISFormData.sisFormFData = sisFormFData;
                        dfd.resolve("0");
                    }
                }
            );
        }
        return dfd.promise;
    };

    this.generateTermValidationOP = function(UWReq){
        var dfd = $q.defer();
        var whereClauseObj = {};
        whereClauseObj.AGENT_CD = ApplicationFormDataService.SISFormData.sisMainData.AGENT_CD;
        //whereClauseObj.LEAD_ID = ApplicationFormDataService.SISFormData.sisMainData.LEAD_ID;
        //whereClauseObj.FHR_ID = ApplicationFormDataService.SISFormData.sisMainData.FHR_ID;
        whereClauseObj.SIS_ID = ApplicationFormDataService.SISFormData.sisMainData.SIS_ID;
        CommonService.selectRecords(db,"LP_TERM_VALIDATION",false,null,whereClauseObj).then(
            function(res){
                if(!!res && res.rows.length>0){
                    var sisFormFData = CommonService.resultSetToObject(res);
                    debug("sisFormFData in generate op : " + JSON.stringify(sisFormFData));
                    pngs.getAppHTMLTemplate().then(
                        function(HtmlFormOutput){
                            if(!!HtmlFormOutput){ // SIS FORM A Info - for insured name - Term template
                                pngs.printTermValidationData(sisFormFData, ApplicationFormDataService.SISFormData).then(
                                    function(res){
                                        if(!!res){
                                            pngs.saveTermValidationOutput(UWReq).then(
                                                function(res){
                                                    if(!!res){
                                                        dfd.resolve("S");
                                                    }else {
                                                        debug("could not save TermValidationOutput");
                                                    }
                                                }
                                            );
                                        }else {
                                            debug("could not replace data")
                                        }
                                    }
                                );
                            }else {
                                debug("HtmlFormOutput not found");
                            }
                        }
                    );
                }
            }
        );
        return dfd.promise;
    };

    this.getAppHTMLTemplate = function(){
        var dfd = $q.defer();
        APP_PATH = "www/templates/APP/TermValidationOutput.html";
        cordova.exec(
            function(fileData){
                debug("fileData: " + ((!!fileData)?(fileData.length):fileData));
                if(!!fileData){
                    pngs.HtmlFormOutput = fileData;
                    dfd.resolve(pngs.HtmlFormOutput);
                }
                else
                    window.alert("Template file not found");
            },
            function(errMsg){
                debug(errMsg);
            },
            "PluginHandler","getDataFromFileInAssets",[APP_PATH]
        );
        return dfd.promise;
    };

    this.printTermValidationData = function(sisFormFData, sisData){
        var dfd = $q.defer();

        //New Template fields added
        var planCode = sisData.sisFormBData.PLAN_CODE;
        debug("planCode : " + planCode);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||POLICY_NO||',sisFormFData.POLICY_NO);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||INS_NAME||', sisData.sisFormAData.INSURED_FIRST_NAME + ' ' + sisData.sisFormAData.INSURED_LAST_NAME);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||PLAN_NAME_OPT||', sisData.sisFormBData.PLAN_NAME + "(" + (planCode.indexOf('N1')!=-1 ? "Option 1" : (planCode.indexOf('N2')!=-1 ? "Option 2" : (planCode.indexOf('N3')!=-1 ? "Option 3" : "Option 4"))) + ")");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SUM_ASSURED||', sisData.sisFormBData.SUM_ASSURED);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SAR||', (!!sisFormFData.SAR) ? sisFormFData.SAR : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||UE_HOUSE_PROP||', (!!sisFormFData.UE_HOUSE_PROP) ? sisFormFData.UE_HOUSE_PROP : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||UE_RENT||', (!!sisFormFData.UE_RENT) ? sisFormFData.UE_HOUSE_PROP : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||UE_GAIN_DIV||', (!!sisFormFData.UE_GAIN_DIV) ? sisFormFData.UE_GAIN_DIV : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||UE_AGRI||', (!!sisFormFData.UE_AGRI) ? sisFormFData.UE_AGRI : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||AVG_INCOME||', (!!sisFormFData.AVG_INCOME) ? sisFormFData.AVG_INCOME : "");
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ELIGIBILITY_VALUE||', (!!sisFormFData.ELIGIBILITY_VALUE) ? sisFormFData.ELIGIBILITY_VALUE : "");


        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||GROUP_EMPLOYEE||',sisFormFData.GROUP_EMPLOYEE);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||NATIONALITY||',sisFormFData.NATIONALITY);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||RESIDENT_STATUS||',sisFormFData.RESIDENT_STATUS);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||CURR_RESIDENT_COUNTRY||',sisFormFData.CURR_RESIDENT_COUNTRY);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||CURR_CITY||',sisFormFData.CURR_CITY);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||EDUCATION_QUALIFICATION||',sisFormFData.EDUCATION_QUALIFICATION);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||OCCUPATION_CLASS||',sisFormFData.OCCUPATION_CLASS);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||INCOME_PROOF||',sisFormFData.INCOME_PROOF);
        if(!!sisFormFData.S_FRM16_SAL1 || sisFormFData.S_FRM16_SAL1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||FORM16||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR1||',sisFormFData.S_FRM16_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR2||',sisFormFData.S_FRM16_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR3||',sisFormFData.S_FRM16_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL1||',sisFormFData.S_FRM16_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL2||',sisFormFData.S_FRM16_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL3||',sisFormFData.S_FRM16_SAL3);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||FORM16||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_FRM16_SAL3||',"");
        }

        if(!!sisFormFData.S_EMP_SAL || sisFormFData.S_EMP_SAL==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||APPEMP||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_EMP_ISSDT||',!!sisFormFData.S_EMP_ISSDT ? sisFormFData.S_EMP_ISSDT.substring(0,10) : "");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_EMP_SAL||',sisFormFData.S_EMP_SAL);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||APPEMP||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_EMP_ISSDT||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_EMP_SAL||',"");
        }

        if(!!sisFormFData.S_BNK_SAL1 || sisFormFData.S_BNK_SAL1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||BNKSAL||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR1||',sisFormFData.S_BNK_MONTH_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR2||',sisFormFData.S_BNK_MONTH_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR3||',sisFormFData.S_BNK_MONTH_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR4||',sisFormFData.S_BNK_MONTH_YEAR4);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR5||',sisFormFData.S_BNK_MONTH_YEAR5);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR6||',sisFormFData.S_BNK_MONTH_YEAR6);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL1||',sisFormFData.S_BNK_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL2||',sisFormFData.S_BNK_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL3||',sisFormFData.S_BNK_SAL3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL4||',sisFormFData.S_BNK_SAL4);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL5||',sisFormFData.S_BNK_SAL5);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL6||',sisFormFData.S_BNK_SAL6);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||BNKSAL||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR4||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR5||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_MONTH_YEAR6||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL4||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL5||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_BNK_SAL6||',"");
        }

        if(!!sisFormFData.S_SAL_SAL1 || sisFormFData.S_SAL_SAL1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SAL3M||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR1||',sisFormFData.S_SAL_MONTH_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR2||',sisFormFData.S_SAL_MONTH_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR3||',sisFormFData.S_SAL_MONTH_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL1||',sisFormFData.S_SAL_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL2||',sisFormFData.S_SAL_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL3||',sisFormFData.S_SAL_SAL3);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SAL3M||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_MONTH_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_SAL_SAL3||',"");
        }

        if(!!sisFormFData.S_COI_SAL1 || sisFormFData.S_COI_SAL1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ITRCOI||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR1||',sisFormFData.S_COI_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR2||',sisFormFData.S_COI_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR3||',sisFormFData.S_COI_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL1||',sisFormFData.S_COI_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL2||',sisFormFData.S_COI_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL3||',sisFormFData.S_COI_SAL3);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ITRCOI||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||S_COI_SAL3||',"");
        }

        if(!!sisFormFData.SE_FRM16_SAL1 || sisFormFData.SE_FRM16_SAL1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||FORM16A||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR1||',sisFormFData.SE_FRM16_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR2||',sisFormFData.SE_FRM16_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR3||',sisFormFData.SE_FRM16_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL1||',sisFormFData.SE_FRM16_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL2||',sisFormFData.SE_FRM16_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL3||',sisFormFData.SE_FRM16_SAL3);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||FORM16A||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_FRM16_SAL3||',"");
        }

        if(!!sisFormFData.SE_COI_PROFIT1 || sisFormFData.SE_COI_PROFIT1==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||COIBA||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR1||',sisFormFData.SE_COI_YEAR1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR2||',sisFormFData.SE_COI_YEAR2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR3||',sisFormFData.SE_COI_YEAR3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT1||',sisFormFData.SE_COI_PROFIT1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT2||',sisFormFData.SE_COI_PROFIT2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT3||',sisFormFData.SE_COI_PROFIT3);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL1||',sisFormFData.SE_COI_SAL1);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL2||',sisFormFData.SE_COI_SAL2);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL3||',sisFormFData.SE_COI_SAL3);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||COIBA||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR1||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_YEAR3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_PROFIT3||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL1||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL2||',"");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_COI_SAL3||',"");
        }

        if(!!sisFormFData.SE_OTH_PROFIT || sisFormFData.SE_OTH_PROFIT==0){
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||OTHERINCM||','');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_OTH_DOC||',sisFormFData.SE_OTH_DOC);
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_OTH_PROFIT||',sisFormFData.SE_OTH_PROFIT);
        }else {
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||OTHERINCM||','none');
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_OTH_DOC||',"NA");
            pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||SE_OTH_PROFIT||',"");
        }


        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ELIGIBILITY_FLAG||',sisFormFData.ELIGIBILITY_FLAG);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||DEVIATION_APPROVAL||',sisFormFData.ELIGIBILITY_FLAG==='Y' ? 'No' : 'Yes');
        var eligibleValue = !!sisFormFData.ELIGIBILITY_VALUE ? Math.round(sisFormFData.ELIGIBILITY_VALUE) : 0;
        var sar = !!sisFormFData.SAR ? Math.round(sisFormFData.SAR) : 0;
        debug("eligibleValue - sar : " + eligibleValue - sar);
        pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ADDITIONAL_COVER||',(eligibleValue - sar) >= 0 ? eligibleValue - sar : "Not Eligible for the applied cover");
        //pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ELIGIBILITY_CATEGORY||',sisFormFData.ELIGIBILITY_CATEGORY || "");
        //pngs.HtmlFormOutput = CommonService.replaceAll(pngs.HtmlFormOutput,'||ELIGIBILITY_REASON||',sisFormFData.ELIGIBILITY_REASON || "");
        dfd.resolve(true);
        return dfd.promise;
    };

    this.saveTermValidationOutput = function(UWReq){
        var dfd = $q.defer();
        var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        var APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        var POLICY_NO = UWReq.POLICY_NO;
        debug("pngs.HtmlFormOutput : " + pngs.HtmlFormOutput.length);
        var whereClauseObj = {};
        whereClauseObj.CUSTOMER_CATEGORY = 'IN';
        whereClauseObj.MPPROOF_CODE = 'TE';
        whereClauseObj.MPDOC_CODE = 'OT';
        CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,'DOC_ID',whereClauseObj).then(
            function(res){
                if(!!res && res.rows.length>0){
                    var DOC_ID = res.rows.item(0).DOC_ID;
                    var fileName = AGENT_CD + "_" +APP_ID + "_" + DOC_ID;
                    debug("DOC_ID : " + DOC_ID);
                    debug("fileName : " + fileName);
                    CommonService.saveFile(fileName,".html","/FromClient/APP/HTML/",pngs.HtmlFormOutput,"N").then(
                        function(saveFileRes){
                            if(!!saveFileRes && saveFileRes=="success"){
                                var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED) VALUES(?,?,?,?,?,?,?,?,?,?)";
                                var parameterList = [DOC_ID,AGENT_CD,"FORM",APP_ID,POLICY_NO,(fileName + ".html"),CommonService.getCurrDate(),"0","N","N"];
                                CommonService.transaction(db,
                                    function(tx){
                                        CommonService.executeSql(tx, query, parameterList,
                                            function(tx,res){
                                                debug("TermValidationOutput saved");
                                                dfd.resolve("S");
                                            },
                                            function(tx,err){
                                                dfd.resolve(null);
                                            }
                                        );
                                    },
                                    function(err){
                                        dfd.resolve(null);
                                    }
                                );
                            }
                        }
                    );
                }else {
                    dfd.resolve(null);
                }
            }
        );
        return dfd.promise;
    };

}]);
