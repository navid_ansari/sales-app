genOutputModule.controller('GenerateTermOutputCtrl', ['$q','$state','CommonService', 'LoginService', 'GenerateTermOutputService','ApplicationFormDataService','AppTermOutputService','ApplicationFlagsList','LoadApplicationScreenData', 'AppSyncService','onlinePaymentService','sisData','PaymentData','AppTimeService','ComboService', function($q, $state, CommonService, LoginService, GenerateTermOutputService, ApplicationFormDataService, AppTermOutputService, ApplicationFlagsList, LoadApplicationScreenData, AppSyncService,onlinePaymentService,sisData, PaymentData,AppTimeService,ComboService){


	var genT = this;
	this.reportPopup = true;
	//$rootScope.val = 100;
	AppTimeService.setProgressValue(100);

	//timeline changes..
	CommonService.hideLoading();

    /* Header Hight Calculator */
    setTimeout(function(){
        var appOutputGetHeight = $(".customer-details .fixed-bar").height();
        $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    });
    /* End Header Hight Calculator */


	this.isInsuredSigned = ApplicationFlagsList.applicationFlags.ISINSURED_SIGNED || 'N';
	//ganesh
	this.isProposerSigned = ApplicationFlagsList.applicationFlags.ISPROPOSER_SIGNED == 'Y' ? true : false;
	//ganesh
	this.isDataSynced = (!!ApplicationFlagsList.applicationFlags.ISSYNCED && (ApplicationFlagsList.applicationFlags.ISSYNCED == 'Y')) ? true : false;

	this.isOnlinePay = (!!PaymentData && !!PaymentData.PAY_METHOD_ONLINE_FLAG && PaymentData.PAY_METHOD_ONLINE_FLAG == 'Y') ? true : false;

	this.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
	//changes home
	var APP_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;
    var SIS_ID = ApplicationFormDataService.ComboFormBean.REF_SIS_ID;
	//For term Payment change
	var BASE_APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;

    debug("COMBO BEAN TERM OP ::"+JSON.stringify(ApplicationFormDataService.ComboFormBean));
    var FHR_ID = ApplicationFormDataService.ComboFormBean.REF_FHRID;
  var LEAD_ID = ApplicationFormDataService.applicationFormBean.LEAD_ID;
  var OPP_ID = ApplicationFormDataService.ComboFormBean.REF_OPP_ID;
  var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
	var EMPFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
	var POLICY_NO = ApplicationFormDataService.ComboFormBean.REF_POLICY_NO;
	var InsDOB = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
	//changes home
	//var REF_POLNO = POLICY_NO;
	//var REF_APP_ID = ApplicationFormDataService.ComboFormBean.APPLICATION_ID;
	var Age = CommonService.getAge(InsDOB)
	this.juvSignCond = (parseInt(Age) >=18 && genT.isInsuredSigned == 'Y') || (parseInt(Age) <18);

    //7th july 2016
    var PGL_ID = sisData.sisMainData.PGL_ID;
	var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
	genT.COMBO_ID = COMBO_ID;

    if(!!COMBO_ID)
    	genT.COMBO_PRIM_FLAG = 'T';

	LoadApplicationScreenData.loadOppFlags(COMBO_ID, AGENT_CD, 'combo', 'P').then(function(oppData){
				console.log("update Term values "+JSON.stringify(oppData))
    			OPP_ID = oppData.oppFlags.OPP_ID;
    			POLICY_NO = oppData.oppFlags.REF_POLICY_NO;
    });

    this.ShowInsuredSign = true;
    if(PGL_ID == '185' || PGL_ID == '191'){
        debug(SIS_ID+"PGL ID -->"+PGL_ID);
        this.ShowInsuredSign = false;
    }
    else
    	debug(SIS_ID+"PGL ID :"+PGL_ID);
    //7th july 2016
    LoadApplicationScreenData.loadApplicationFlags(APP_ID,AGENT_CD).then(function(ApplicationFlagsList){

				genT.isInsuredSigned = ApplicationFlagsList.applicationFlags.ISINSURED_SIGNED;
				//ganesh
        genT.isProposerSigned = ApplicationFlagsList.applicationFlags.ISPROPOSER_SIGNED == 'Y' ? true : false;
        //ganesh
      	genT.isDataSynced = (!!ApplicationFlagsList.applicationFlags.ISSYNCED && (ApplicationFlagsList.applicationFlags.ISSYNCED=='Y')) ? true : false;
    		genT.juvSignCond = ((parseInt(Age) >=18 && genT.isInsuredSigned == 'Y') || (parseInt(Age) <18));
    		debug("juvSignCond  ::"+genT.juvSignCond);
    	});


    LoadApplicationScreenData.loadPaymentData(APP_ID, AGENT_CD).then(function(PaymentData){
        console.log(genT.COMBO_PRIM_FLAG+"PAYMENTDATA : "+JSON.stringify(PaymentData));
        genT.isOnlinePay = (!!PaymentData && !!PaymentData.PAY_METHOD_ONLINE_FLAG && PaymentData.PAY_METHOD_ONLINE_FLAG == 'Y') ? true : false;
        genT.isOnlinePay = true;
        console.log("PAYMENTDATA :FLAG: "+genT.isOnlinePay);
    });

	this.saveLASign = function(){

		CommonService.doSign("appTermInsSign");
	};

	this.savePROSign = function(){
		if(genT.juvSignCond == false)
			navigator.notification.alert("Please complete Insured Signature.",null,"App Form","OK");
		else
    		CommonService.doSign("appTermProSign");
    };

	this.saveAppReportFile = function(){
		AppTermOutputService.saveAppReportFile().then(
			function(res){
				if(!!res){
				    //7th july 2016
				    if(PGL_ID == '185' || PGL_ID == '191'){
				        AppTermOutputService.updateProAndInsFlag().then(
				            function(inserted){
				                genT.isProposerSigned = "Y";
				            }
				        );
				    }
				    //7th july 2016
					navigator.notification.alert("Application saved successfully.",null,"App Form","OK");
			    }
			}
		);
	};
	this.syncData = function(){
		//IRS block
		debug("PGL_ID: " + PGL_ID);
		if(PGL_ID != "185" && PGL_ID != "191" && PGL_ID!=FG_PGL_ID && ((BUSINESS_TYPE=='iWealth' && PGL_ID != SIP_PGL_ID) || (BUSINESS_TYPE!='iWealth'))){
		navigator.notification.confirm("Do you want to Sync Application Data ?",
        			function(buttonIndex){
        	        	if(buttonIndex=="1"){
        	        		CommonService.showLoading("Loading...");
											if(EMPFlag == 'E')
													navigator.notification.alert("Note: Final submission for this policy will not be done, since data entry is done in Training Role.",null,"Trainee","OK");

        	        		if(!!ApplicationFormDataService.ComboFormBean.COMBO_ID)
								LoadApplicationScreenData.loadOppFlags(ApplicationFormDataService.ComboFormBean.COMBO_ID, AGENT_CD, 'combo', 'P').then(function(oppData){
									AppSyncService.startAppDataSync(oppData.oppFlags.FHR_ID,oppData.oppFlags.LEAD_ID, oppData.oppFlags.OPPORTUNITY_ID, oppData.oppFlags.SIS_ID, oppData.oppFlags.APPLICATION_ID, null, oppData.oppFlags.POLICY_NO,null,'T').then(function(res){

                                        console.log("inside complete Base syncAppData ::"+JSON.stringify(res));
                                       if(!!res && res!=false)
                                        AppSyncService.startAppDataSync(FHR_ID,LEAD_ID, OPP_ID, SIS_ID, APP_ID, null, POLICY_NO,null,'T').then(function(Tres){
                                            /*CommonService.hideLoading();
                                            console.log("inside complete TERM syncAppData ::"+JSON.stringify(res));*/
                                                if(!!Tres && Tres!=false)
                                                genT.updateLPAppMain(oppData.oppFlags.APPLICATION_ID, AGENT_CD).then(function(){
                                                    genT.updateLPAppMain(APP_ID, AGENT_CD).then(function(){
                                                        CommonService.hideLoading();
																												LoadApplicationScreenData.loadApplicationFlags(oppData.oppFlags.APPLICATION_ID, AGENT_CD).then(function(ApplicationFlagsList){
																														//drastty
																														debug("ISSYNCED :::"+ApplicationFlagsList.applicationFlags.ISSYNCED);
																										        genT.isDataSynced = (!!ApplicationFlagsList.applicationFlags.ISSYNCED && (ApplicationFlagsList.applicationFlags.ISSYNCED=='Y')) ? true : false;
																														debug("genT.isDataSynced :AFTER-2:"+genT.isDataSynced);
																										    	});
                                                        navigator.notification.alert(APP_DATA_SYN_MSG,function(){CommonService.hideLoading();},"Application Sync","OK");

                                                    });

                                                });
                                            });
                                    });
								});
							else
        	        		AppSyncService.startAppDataSync(FHR_ID,LEAD_ID, OPP_ID, SIS_ID, APP_ID, null, POLICY_NO).then(function(res){
                            		CommonService.hideLoading();
                            				console.log("inside complete syncAppData ::"+JSON.stringify(res));
                            		});


        				}
        			},
        	        'Confirm Data Sync',
        	        'Yes, No'
        	    );
			}
			else{
				if(PGL_ID==FG_PGL_ID || (PGL_ID==SIP_PGL_ID && (BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner')))
					navigator.notification.alert("The sale of this product via tabsales has been discontinued.",null,"App Form","OK");
				else
					navigator.notification.alert("Please buy this product online - through WebSales.",null,"App Form","OK");
			}
	}
    this.updateLPAppMain = function(APP_ID, AGENT_CD){
    var dfd = $q.defer();
		var issynced = 'Y';
		if(EMPFlag == 'E')
			issynced = 'E';
          CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":APP_ID,"AGENT_CD":AGENT_CD}).then(
                function(res){
                    console.log("ISSYNCED update success !!"+APP_ID);
                    dfd.resolve(true);
                });
    return dfd.promise;
    }

	this.olGenPaymentData = function(){
		console.log("inside callpopup");
		if(!!BASE_APP_ID){
			/*onlinePaymentService.sendOTData(APP_ID).then(function(res){
				onlinePaymentService.pay(res);
				console.log("result is::"+JSON.stringify(res));
			});*/
			console.log("result is::"+BASE_APP_ID);
			onlinePaymentService.checkPayDtls(BASE_APP_ID);
		}
	}

	this.calOlGenPop = function(){
		if(PGL_ID != "185" && PGL_ID != "191" && PGL_ID!=FG_PGL_ID && ((BUSINESS_TYPE=='iWealth' && PGL_ID != SIP_PGL_ID) || (BUSINESS_TYPE!='iWealth'))){
			if(!!BASE_APP_ID){
				onlinePaymentService.getProposerData(BASE_APP_ID).then(function(CustData){
					//console.log("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
					genT.CustName = CustData[0].FNAME;
					genT.MobNo = CustData[0].MOBILENO;
					genT.Email = CustData[0].EMAIL;
					var newBasePAmt = CustData[0].AMOUNT;
					var newBaseTermAmt = CustData[0].TERM_AMOUNT;
					genT.TransAmt = parseInt(newBasePAmt)+parseInt(newBaseTermAmt);
				});
			}
			this.reportPopup = !this.reportPopup;
		}
		else{
			if(PGL_ID==FG_PGL_ID || (PGL_ID==SIP_PGL_ID && (BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner')))
				navigator.notification.alert("The sale of this product via tabsales has been discontinued.",null,"App Form","OK");
			else
				navigator.notification.alert("Please buy this product online - through WebSales.",null,"App Form","OK");
		}
	};

	this.closeBlackOverlay = function(){
		this.reportPopup = !this.reportPopup;
	};

	this.exit = function(){
			$state.go('leadDashboard',{"LEAD_ID": ApplicationFormDataService.applicationFormBean.LEAD_ID, "FHR_ID": ApplicationFormDataService.applicationFormBean.FHR_ID, "SIS_ID": ApplicationFormDataService.SISFormData.sisMainData.SIS_ID, "APP_ID": ApplicationFormDataService.applicationFormBean.APPLICATION_ID, "OPP_ID": ApplicationFormDataService.applicationFormBean.OPP_ID, 'POLICY_NO': ApplicationFormDataService.applicationFormBean.POLICY_NO});
	};

}]);


genOutputModule.service('AppTermOutputService', ['$q','$state', 'CommonService', 'LoginService', 'ApplicationFormDataService','LoadApplicationScreenData','AppOutputService', function($q, $state, CommonService, LoginService, ApplicationFormDataService, LoadApplicationScreenData, AppOutputService){
var appOt = this;
debug("Loading APPOutput Service");
    var AGENT_CD;
	var APP_ID ;
	var isSelf;
	var POLICY_NO;
	appOt.INSURED_SIGN = null;
        //5th july 2016 update flag and prop and ins sign on save click..
	    this.updateProAndInsFlag = function(){
	        var dfd = $q.defer();
	        AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            APP_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;

            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = APP_ID;
            whereClauseObj.AGENT_CD = AGENT_CD;

            var whereClauseOpp = {};
            whereClauseOpp.FHR_ID = ApplicationFormDataService.ComboFormBean.REF_FHRID;
            whereClauseOpp.AGENT_CD = AGENT_CD;

	        CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISINSURED_SIGNED" : 'Y', "ISPROPOSER_SIGNED" : 'Y', "APPLICATION_DATE":CommonService.getCurrDate()}, whereClauseObj).then(
                function(res){
                    console.log("ISINSURED_SIGNED update success !!"+JSON.stringify(whereClauseObj));

                    CommonService.updateRecords(db, 'LP_MYOPPORTUNITY', {"IS_CUST_DET" : 'Y'}, whereClauseOpp).then(
                        function(res){
                            console.log("LP_MYOPPORTUNITY IS_CUST_DET update success !!"+JSON.stringify(whereClauseOpp));
                            dfd.resolve(true);
                        });

                }
            );

            return dfd.promise;
	    }

	    /* Vernacular dec*/

	    this.vernacularDetails = function(){

	    	navigator.notification.confirm("Is the signature of Proposer/Life Assured in Vernacular?",
            			function(buttonIndex){
            			console.log("buttonIndex :"+buttonIndex);
            				if(buttonIndex=="1"){
            					CommonService.showLoading("Loading...");
								$state.go('applicationForm.appVernacular',{"COMBO_ID": ApplicationFormDataService.ComboFormBean.COMBO_ID});
            				}
            				else
            				{
            				        ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_NAME||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_ADDR||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDTYPE||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDNO||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||AGENT_LANG||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||VERNAC_FIRST||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||VERNAC_MIDDLE||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||VERNAC_LAST||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_NAME||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_SIGN||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDTYPE||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDNO||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||PROP_SIGN||',"");
                                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||LIFE_SIGN||',"");

									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_NAME||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_ADDR||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_IDTYPE||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_IDNO||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||AGENT_LANG||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||VERNAC_FIRST||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||VERNAC_MIDDLE||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||VERNAC_LAST||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_NAME||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_SIGN||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDTYPE||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDNO||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||PROP_SIGN||',"");
									ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||LIFE_SIGN||',"");
            						AppOutputService.saveAppReportFile().then(function(){
										   appOt.saveAppReportFile().then(function(){
										   	debug("Vernac not selected");
										 });
									});
            						CommonService.hideLoading();
                                    $state.reload();
            				}
            			},
            			'Vernacular Declaration',
            			'Yes, No'
            		);

	    }

	    //5th july 2016
		this.replaceVernacDetails = function(sign, VernDet){
			var isSelf = false;
			if(!!ApplicationFormDataService && !!ApplicationFormDataService.SISFormData){
				debug("setting isSelf");
				isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
			}
			debug("vernacularDetails: " + isSelf);
			console.log("inside replaceVernacDetails " + isSelf);
			console.log(JSON.stringify(VernDet));
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_NAME||',VernDet.witnessName);
                if(!!VernDet.idProofType && !!VernDet.idProofType.MPPROOF_DESC){
                    VernDet.idProofType.MPPROOF_DESC = CommonService.replaceAll(VernDet.idProofType.MPPROOF_DESC, "&", "&amp;");
                    VernDet.idProofType.MPPROOF_DESC = CommonService.replaceAll(VernDet.idProofType.MPPROOF_DESC, "<", "&lt;");
                    VernDet.idProofType.MPPROOF_DESC = CommonService.replaceAll(VernDet.idProofType.MPPROOF_DESC, ">", "&gt;");
                    VernDet.idProofType.MPPROOF_DESC = CommonService.replaceAll(VernDet.idProofType.MPPROOF_DESC, '"', "&quot;");
                }
                 if(VernDet.idProofType.MPPROOF_DESC =='Others'){

                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDTYPE||',VernDet.Other_PROOF_TYPE);
                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDTYPE||',VernDet.Other_PROOF_TYPE);
                    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDTYPE||',VernDet.Other_PROOF_TYPE);
                }else{
                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDTYPE||',VernDet.idProofType.MPPROOF_DESC);
                    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDTYPE||',VernDet.idProofType.MPPROOF_DESC);
                    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDTYPE||',VernDet.idProofType.MPPROOF_DESC);
                }
                //ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDTYPE||',VernDet.idProofType.MPPROOF_DESC);
                 ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_IDNO||',VernDet.idProofNo);
						if(!!VernDet.addr){
							VernDet.addr = CommonService.replaceAll(VernDet.addr, "&", "&amp;");
							VernDet.addr = CommonService.replaceAll(VernDet.addr, "<", "&lt;");
							VernDet.addr = CommonService.replaceAll(VernDet.addr, ">", "&gt;");
							VernDet.addr = CommonService.replaceAll(VernDet.addr, '"', "&quot;");
						}
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||DEC_ADDR||',VernDet.addr);
						if(!!VernDet.expLang){
							VernDet.expLang = CommonService.replaceAll(VernDet.expLang, "&", "&amp;");
							VernDet.expLang = CommonService.replaceAll(VernDet.expLang, "<", "&lt;");
							VernDet.expLang = CommonService.replaceAll(VernDet.expLang, ">", "&gt;");
							VernDet.expLang = CommonService.replaceAll(VernDet.expLang, '"', "&quot;");
						}
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||AGENT_LANG||',VernDet.expLang);
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_NAME||',VernDet.witnessName);
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_SIGN||',sign);
            //ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDTYPE||',VernDet.idProofType);
            ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||WITNESS_IDNO||',VernDet.idProofNo);

            ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_NAME||',VernDet.witnessName);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_IDTYPE||',VernDet.idProofType);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_IDNO||',VernDet.idProofNo);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||DEC_ADDR||',VernDet.addr);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||AGENT_LANG||',VernDet.expLang);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_NAME||',VernDet.witnessName);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_SIGN||',sign);
						//ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDTYPE||',VernDet.idProofType);
						ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||WITNESS_IDNO||',VernDet.idProofNo);
						if(!!appOt.PROPOSER_SIGN)
							{
							    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||PROP_SIGN||',appOt.PROPOSER_SIGN); //appOt.PROPOSER_SIGN
							    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||PROP_SIGN||',appOt.PROPOSER_SIGN); //appOt.PROPOSER_SIGN
							}
						else
							{
							    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||PROP_SIGN||',""); //appOt.PROPOSER_SIGN
							    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||PROP_SIGN||',""); //appOt.PROPOSER_SIGN
							}

						if(!!appOt.INSURED_SIGN)
							{
							    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||LIFE_SIGN||',appOt.INSURED_SIGN); //appOt.INSURED_SIGN
							    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||LIFE_SIGN||',appOt.INSURED_SIGN); //appOt.INSURED_SIGN
							}
						else
							{
							    ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||LIFE_SIGN||',""); //appOt.INSURED_SIGN
							    ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||LIFE_SIGN||',""); //appOt.INSURED_SIGN
							}

			if(!!isSelf && isSelf){
				CommonService.selectRecords(db, "lp_app_contact_scrn_a", true, "FIRST_NAME, MIDDLE_NAME, LAST_NAME", {"application_id": ApplicationFormDataService.applicationFormBean.APPLICATION_ID, "cust_type": "C01", "agent_cd": LoginService.lgnSrvObj.userinfo.AGENT_CD}).then(
					function(res){
                        ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,"||VERNAC_FIRST||", res.rows.item(0).FIRST_NAME);
                        ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,"||VERNAC_MIDDLE||", res.rows.item(0).MIDDLE_NAME);
                        ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,"||VERNAC_LAST||", res.rows.item(0).LAST_NAME);

						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_FIRST||", res.rows.item(0).FIRST_NAME);
						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_MIDDLE||", res.rows.item(0).MIDDLE_NAME);
						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_LAST||", res.rows.item(0).LAST_NAME);
						 AppOutputService.saveAppReportFile().then(function(){

                               appOt.saveAppReportFile().then(function(){
                                 $state.go("applicationForm.appOutputTerm");
                             });
                        });
					}
				);
			}
			else{
				CommonService.selectRecords(db, "lp_app_contact_scrn_a", true, "FIRST_NAME, MIDDLE_NAME, LAST_NAME", {"application_id": ApplicationFormDataService.applicationFormBean.APPLICATION_ID, "cust_type": "C02", "agent_cd": LoginService.lgnSrvObj.userinfo.AGENT_CD}).then(
					function(res){
						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_FIRST||", res.rows.item(0).FIRST_NAME);
						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_MIDDLE||", res.rows.item(0).MIDDLE_NAME);
						 ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,"||VERNAC_LAST||", res.rows.item(0).LAST_NAME);
						AppOutputService.saveAppReportFile().then(function(){

                              appOt.saveAppReportFile().then(function(){
                                $state.go("applicationForm.appOutputTerm");
                            });
						});
					}
				);
			}



		}

		this.saveLifeAssSign = function(sign){
		CommonService.showLoading("Loading...");
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            	APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            	POLICY_NO = ApplicationFormDataService.applicationFormBean.POLICY_NO;
            	isSelf = false;
				appOt.INSURED_SIGN = sign;
            	if(!!ApplicationFormDataService && !!ApplicationFormDataService.SISFormData){
            	    debug("setting isSelf");
            	    isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
                }
                else
                	isSelf = false

                var whereClauseObj = {};
            		whereClauseObj.APPLICATION_ID = APP_ID;
            		whereClauseObj.AGENT_CD = AGENT_CD;
            	var whereClauseOpp = {};
            		whereClauseOpp.FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
            		whereClauseOpp.AGENT_CD = AGENT_CD;

        		console.log(isSelf+"LA Sign :"+APP_ID);

		if(ApplicationFormDataService.APP_TERM_OUTPUT!=undefined){
			if(isSelf!= undefined && isSelf)
			{	appOt.PROPOSER_SIGN = sign;
                ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||PROPSIGN||',sign);
                ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||LIFESIGN||',sign);

                ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||PROPSIGN||',sign);
                ApplicationFormDataService.APP_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_OUTPUT,'||LIFESIGN||',sign);

                var fileName = AGENT_CD+"_"+APP_ID+"_C01";
				var saveSign = sign.replace("data:image/jpeg;base64,","");
				var cap_id = CommonService.getRandomNumber();
                CommonService.saveFile(fileName,".jpg","/FromClient/APP/SIGNATURE/",saveSign,"N").then(function(){
                                        console.log("CUST Sign Success");
                                                appOt.getSignDocID().then(function(SignDocID){
                                                var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                                                var parameterList = [SignDocID.Insured,AGENT_CD,"APP",APP_ID,POLICY_NO,fileName+".jpg",CommonService.getCurrDate(),"0","N","N",cap_id];
                                                CommonService.transaction(db, function(tx){
                                                    CommonService.executeSql(tx, query, parameterList, function(tx,res){
																appOt.insertTermSignRecord(SignDocID,cap_id);
                                                               debug("Success InsSign Doc");
                                                    }, function(tx, err){console.log("Error 1");});
                                                    },function(err){});
                                            });
                                        });
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISINSURED_SIGNED" : 'Y', "ISPROPOSER_SIGNED" : 'Y', "APPLICATION_DATE":CommonService.getCurrDate()}, whereClauseObj).then(
                        function(res){
                        	appOt.updateTermFlags();

                        	AppOutputService.saveAppReportFile().then(function(){
								console.log("SAVED BASE FILE !!");
								appOt.saveAppReportFile().then(function(){
										console.log("SAVED TERM FILE !!");
										CommonService.hideLoading();
//										//Vernacular
//										debug("calling .vernacularDetails - term 2");
//										appOt.vernacularDetails();
									});
							});

                            console.log("ISINSURED_SIGNED update success !!"+JSON.stringify(whereClauseObj));

                            CommonService.updateRecords(db, 'LP_MYOPPORTUNITY', {"IS_CUST_DET" : 'Y'}, whereClauseOpp).then(
                                function(res){

                                    console.log("LP_MYOPPORTUNITY IS_CUST_DET update success !!"+JSON.stringify(whereClauseOpp));
                                });

                        });
	        }
	        else{
                ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||LIFESIGN||',sign);
                var fileName = AGENT_CD+"_"+APP_ID+"_C01";
				var saveSign = sign.replace("data:image/jpeg;base64,","");
                            CommonService.saveFile(fileName,".jpg","/FromClient/APP/SIGNATURE/",saveSign,"N").then(function(){
                                                    console.log("CUST Sign Success");
                                                         appOt.getSignDocID().then(function(SignDocID){
                                                            var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED) VALUES(?,?,?,?,?,?,?,?,?,?)";
                                                            var parameterList = [SignDocID.Insured,AGENT_CD,"APP",APP_ID,POLICY_NO,fileName+".jpg",CommonService.getCurrDate(),"0","N","N"];
                                                            CommonService.transaction(db, function(tx){
                                                                CommonService.executeSql(tx, query, parameterList, function(tx,res){
                                                                           CommonService.hideLoading();
                                                                           debug("Success InsSign Doc")

                                                                }, function(tx, err){console.log("Error 1");});
                                                                },function(err){});
                                                        });
                                                    });
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISINSURED_SIGNED" : 'Y', "APPLICATION_DATE":CommonService.getCurrDate()}, whereClauseObj).then(
                    function(res){

                        console.log("TERM ISINSURED_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                        appOt.saveAppReportFile();
                });
            }
		//	this.saveAppReportFile();
		}
		else
			console.log("LIFESIGN : nt found");
		};

		this.insertTermSignRecord = function(SignDocID, ref_cap_id){
			var dfd = $q.defer();
			var REF_APP_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;
			var REF_POLICY_NO = ApplicationFormDataService.ComboFormBean.REF_POLICY_NO;
				console.log("REF_APP_ID:"+REF_APP_ID+" REF_POLICY_NO :"+REF_POLICY_NO);
			AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			var fileName = AGENT_CD+"_"+REF_APP_ID+"_C01";
        	var cap_id = CommonService.getRandomNumber();
		 	var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID,DOC_CAP_REF_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			var parameterList = [SignDocID.Insured,AGENT_CD,"APP",REF_APP_ID,REF_POLICY_NO,fileName+".jpg",CommonService.getCurrDate(),"0","Y","N",cap_id,ref_cap_id];

				CommonService.transaction(db, function(tx){
						CommonService.executeSql(tx, query, parameterList, function(tx,res){
									  dfd.resolve(res);
									  debug("Success TermInsSign Doc");

						   }, function(tx, err){console.log("Error 1");});
				},function(err){});

			return dfd.promise;
		}

		this.updateTermFlags = function(){
			var whereClauseObj = {};
				whereClauseObj.APPLICATION_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;
				whereClauseObj.AGENT_CD = AGENT_CD;

			var whereClauseOpp = {};
				whereClauseOpp.OPPORTUNITY_ID = ApplicationFormDataService.ComboFormBean.REF_OPP_ID;
				whereClauseOpp.AGENT_CD = AGENT_CD;

			CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISINSURED_SIGNED" : 'Y', "ISPROPOSER_SIGNED" : 'Y', "APPLICATION_DATE":CommonService.getCurrDate()}, whereClauseObj).then(
									function(res){

										appOt.saveAppReportFile().then(function(){

											CommonService.hideLoading();
											//Vernacular
											debug("calling .vernacularDetails - term 3");
											appOt.vernacularDetails();
										});
										console.log("ISINSURED_SIGNED TERM update success !!"+JSON.stringify(whereClauseObj));

										CommonService.updateRecords(db, 'LP_MYOPPORTUNITY', {"IS_CUST_DET" : 'Y'}, whereClauseOpp).then(
											function(res){

												console.log("LP_MYOPPORTUNITY TERM IS_CUST_DET update success !!"+JSON.stringify(whereClauseOpp));
											});

									});
		}

		this.saveProposerSign = function(sign){
		CommonService.showLoading("Loading...");
	    	console.log("Proposer Sign :"+sign);
	    	/*CommonService.saveFile(APP_ID,".html","/FromClient/APP/HTML/",ApplicationFormDataService.APP_TERM_OUTPUT,"N").then(function(){

	    	});*/
            /* wherecluase added by Ganesh */
	    	AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            POLICY_NO = ApplicationFormDataService.applicationFormBean.POLICY_NO;
            isSelf = false;
			appOt.PROPOSER_SIGN = sign;
            if(!!ApplicationFormDataService && !!ApplicationFormDataService.SISFormData){
                debug("setting isSelf");
                isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
            }
            else
                isSelf = false

            var whereClauseObj = {};
                whereClauseObj.APPLICATION_ID = APP_ID;
                whereClauseObj.AGENT_CD = AGENT_CD;
            var whereClauseOpp = {};
                whereClauseOpp.FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
                whereClauseOpp.AGENT_CD = AGENT_CD;
            console.log(isSelf+"PROP Sign :"+sign);


	    	ApplicationFormDataService.APP_TERM_OUTPUT = CommonService.replaceAll(ApplicationFormDataService.APP_TERM_OUTPUT,'||PROPSIGN||',sign);
	    	var fileName = AGENT_CD+"_"+APP_ID+"_C02";
			var saveSign = sign.replace("data:image/jpeg;base64,","");
            			CommonService.saveFile(fileName,".jpg","/FromClient/APP/SIGNATURE/",saveSign,"N").then(function(){
                                				console.log("CUST Sign Success");
                                                    appOt.getSignDocID().then(function(SignDocID){
                                                        var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED) VALUES(?,?,?,?,?,?,?,?,?,?)";
                                                        var parameterList = [SignDocID.Proposer,AGENT_CD,"APP",APP_ID,POLICY_NO,fileName+".jpg",CommonService.getCurrDate(),"0","N","N"];
                                                        CommonService.transaction(db, function(tx){
                                                            CommonService.executeSql(tx, query, parameterList, function(tx,res){

                                                                       debug("Success InsSign Doc")

                                                            }, function(tx, err){console.log("Error 1");});
                                                            },function(err){});
                                                    });
                                				});
	    	if(ApplicationFormDataService.APP_TERM_OUTPUT!=undefined){
	    		CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISPROPOSER_SIGNED" : 'Y'}, whereClauseObj).then(
						function(res){
							console.log("ISPROPOSER_SIGNED update success !!"+JSON.stringify(whereClauseObj));
							appOt.saveAppReportFile().then(function(){

								CommonService.hideLoading();
								//Vernacular
								debug("calling .vernacularDetails - term 1");
								appOt.vernacularDetails();
							 });
							CommonService.updateRecords(db, 'LP_MYOPPORTUNITY', {"IS_CUST_DET" : 'Y'}, whereClauseOpp).then(
													function(res){

														console.log("LP_MYOPPORTUNITY IS_CUST_DET update success : in proposer!!"+JSON.stringify(whereClauseOpp));
													});
						});
			}
	    	else
	    		console.log("PROPSIGN : nt found");
	    	};

		this.saveAppReportFile = function(){
		AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
		//changes home
		APP_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;
		POLICY_NO = ApplicationFormDataService.ComboFormBean.REF_POLICY_NO;

		console.log("inside saveAppReport");
		    AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
		    //changes home
            APP_ID = ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID;

			var dfd = $q.defer();
			CommonService.saveFile(APP_ID,".html","/FromClient/APP/HTML/",ApplicationFormDataService.APP_TERM_OUTPUT,"N").then(
				function(saveFileRes){
					if(!!saveFileRes && saveFileRes=="success"){
						var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED) VALUES((SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE CUSTOMER_CATEGORY=? AND MPPROOF_CODE=? AND MPDOC_CODE=?),?,?,?,?,?,?,?,?,?)";
						var parameterList = ["IN","CA","AF",AGENT_CD,"APP",APP_ID,POLICY_NO,(APP_ID+".html"),CommonService.getCurrDate(),"0","N","N"];

						CommonService.transaction(db,
							function(tx){
								CommonService.executeSql(tx, query, parameterList,
									function(tx,res){
										console.log("TERM App HTML saved")
										dfd.resolve("S");
									},
									function(tx,err){
										dfd.resolve(null);
									}
								);
							},
							function(err){
								dfd.resolve(null);
							}
						);
					}
					else{
						dfd.resolve(null);
					}
				}
			);
			return dfd.promise;
		};

	this.getSignDocID = function(){
    var dfd = $q.defer();
    var SignDocID = {};
	var sql = "SELECT * FROM LP_DOC_PROOF_MASTER WHERE MPDOC_CODE=?";
    var params = ["SZ"];
    CommonService.transaction(db, function(tx){
                                    CommonService.executeSql(tx, sql, params, function(tx,res){
                                                if(res.rows.length!=0)
                                                {
                                                    for(var i = 0; i<res.rows.length;i++)
                                                    {
                                                        if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
                                                            SignDocID.Insured = res.rows.item(i).DOC_ID;
                                                        if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
                                                            SignDocID.Proposer = res.rows.item(i).DOC_ID;
                                                        if(i == res.rows.length-1)
                                                            dfd.resolve(SignDocID);
                                                    }
                                                }

                					}, function(tx, err){console.log("Error 1");});
                					},function(err){});
       return dfd.promise;
	}


}]);

genOutputModule.service('GenerateTermOutputService', ['$q','$state', 'CommonService', 'LoginService','LoadApplicationScreenData','SisFormService', function($q, $state, CommonService, LoginService, LoadApplicationScreenData, SisFormService){

var genOp = this;
var APP_ID ;
var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
var SIS_ID ;
var PGL_ID ;
var POLICY_NO;
var sisData ;
var isSelf ;
var whereClauseObj = {};
var COMBO_ID = null;
genOp.AppOutput = null;
genOp.HtmlFormOutput = null;
var APP_PATH;


this.generateTermApplicationOutput = function(appData){
var AppDataSrv = {}
AppDataSrv.applicationFormBean = {};
AppDataSrv.SISFormData = {};

CommonService.showLoading("Generating Term Application output...");
 LoadApplicationScreenData.loadOppFlags(appData.applicationFormBean.OPP_ID, AGENT_CD, 'opp').then(function(oppData){
				console.log("ABC: Herere123 oppData:"+JSON.stringify(oppData));
			 SisFormService.loadSavedSISData(AGENT_CD, oppData.oppFlags.REF_SIS_ID).then(function(sisData){
					console.log("ABC: sisData:"+JSON.stringify(sisData));
			 		AppDataSrv.SISFormData.sisMainData = sisData.sisMainData;
			 		AppDataSrv.SISFormData.sisFormAData = sisData.sisFormAData;
					AppDataSrv.applicationFormBean.APPLICATION_ID = oppData.oppFlags.REF_APPLICATION_ID;
					AppDataSrv.applicationFormBean.COMBO_ID = appData.ComboFormBean.COMBO_ID//'8999999999';
					AppDataSrv.applicationFormBean.POLICY_NO = oppData.oppFlags.REF_POLICY_NO;
					AppDataSrv.applicationFormBean.SIS_ID = oppData.oppFlags.REF_SIS_ID;
					AppDataSrv.applicationFormBean.PGL = sisData.sisMainData.PGL_ID;
					genOp.generateApplicationOutput(AppDataSrv, appData);
			 });
		});

}

this.generateApplicationOutput = function(AppDataSrv, _AppDataSrv){

APP_ID = AppDataSrv.applicationFormBean.APPLICATION_ID;
POLICY_NO = AppDataSrv.applicationFormBean.POLICY_NO;
SIS_ID = AppDataSrv.SISFormData.sisMainData.SIS_ID;
PGL_ID = AppDataSrv.SISFormData.sisMainData.PGL_ID;
APP_PATH = "www/templates/APP/standard-proposal-withborders.html";
    //7th july 2016
    if(PGL_ID=="191" || PGL_ID=="185"){
        debug("inside application output control >>>>"+PGL_ID);
	    APP_PATH = "www/templates/APP/standard-proposal-irs.html";
	    debug("app path is "+APP_PATH);
	}
	//Added by Amruta for VCP
	//Change to VCP PGLID
	/*else if(PGL_ID=="222"){
	debug("AppForm for VCP::"+PGL_ID);
	APP_PATH = "www/templates/APP/standard-proposal-vcp.html";
	debug("app path is "+APP_PATH);
	}*/
	//7th july 2016

sisData = AppDataSrv.SISFormData;
isSelf = ((AppDataSrv.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
	whereClauseObj.APPLICATION_ID = APP_ID;
	whereClauseObj.AGENT_CD = AGENT_CD;
	whereClauseObj.CUST_TYPE = 'C01';
var fileData;
console.log("inside generateApplicationOutput"+isSelf);
CommonService.showLoading("Generating Term Application output...");
genOp.getAppHTMLTemplate().then(
	function(fileData){
		genOp.printUserInfoData(fileData).then(function(){
		    genOp.SISMainData().then(function(){
                //console.log("Final O/P :"+fileData);
                    genOp.printContactScrnA().then(function(){
                        genOp.printNomineeDetails().then(function(){
                            genOp.printAppointeeDetails().then(function(){
                                genOp.printFundSelectionDetails().then(function(){
                                    genOp.printPaymentModeDetails().then(function(){
                                        genOp.printPaymentDataDetails().then(function(){
                                            genOp.printNEFTDetails().then(function(){
                                                genOp.printExtInsurance().then(function(){
														genOp.printLSData().then(function(){
															genOp.printLSSubQuestion().then(function(){
															console.log("return printLSSubQuestion :")
																genOp.getHDQuestionnaire1_8().then(function(){
																		console.log("inside 1_8")
																			genOp.getHDQuestionnaire9().then(function(){
																				genOp.getFemaleQuestionnaire().then(function(){
																					console.log("inside getFemaleQuestionnaire")
																					genOp.getFH_History().then(function(){
																					console.log("inside FH")
																						genOp.HDSubQuestionDetails().then(function(){
																						console.log("calling loadProfileImage");
																						    genOp.loadProfileImage(_AppDataSrv.applicationFormBean.APPLICATION_ID).then(function(){

                                                                                                genOp.AppOutput = genOp.HtmlFormOutput;

                                                                                                AppDataSrv.APP_TERM_OUTPUT = genOp.AppOutput;
                                                                                                CommonService.hideLoading();
                                                                                                if(!!AppDataSrv.applicationFormBean.COMBO_ID && AppDataSrv.applicationFormBean.COMBO_ID == '')
                                                                                                {
																									MAIN_TAB = 'genOutput';
																									$state.go("applicationForm.appOutput");
                                                                                               	}
                                                                                                else
                                                                                                {
																									if(!!_AppDataSrv){
																										_AppDataSrv.APP_TERM_OUTPUT = genOp.AppOutput;
																										console.log("applicationForm.appOutputTerm");
																										MAIN_TAB = 'genOutput';
																										$state.go("applicationForm.appOutputTerm");
																										CommonService.hideLoading();
																										//$state.reload();
																									}
																									else
																									{
																										MAIN_TAB = 'genOutput';
																										$state.go("applicationForm.appOutput");
																									}
																								}

																						    });
																					})

																				})
																			})
																		})
																	})
															})
													})


                                                })

                                            })

                                        })

                                    })

                                })

                            })

                        })

                    })
		    });

		});
	}
);

}

this.getAppHTMLTemplate = function(){
var dfd = $q.defer();
cordova.exec(
                function(fileData){
                    console.log("fileData: " + fileData);
                    if(!!fileData)
                        {
							genOp.HtmlFormOutput = fileData;
							dfd.resolve(genOp.HtmlFormOutput);
                        }
                    else
                        window.alert("Template file not found");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",[APP_PATH]
            );
return dfd.promise;
}



this.loadProfileImage = function(appID){
debug("inside loadProfileImage"+appID);
var dfd = $q.defer();
var imagePath = "/FromClient/APP/IMAGES/PHOTOS/";
genOp.PROFILE = {};
var APP_ID = appID
var query = "(select DOC_ID from LP_DOC_PROOF_MASTER where MPPROOF_CODE = 'RP' AND MPDOC_CODE = 'PH' AND CUSTOMER_CATEGORY = ?)";
var sql = "select * from LP_DOCUMENT_UPLOAD where DOC_CAT = 'PHOTOGRAPH' AND DOC_CAT_ID = '"+APP_ID+"' AND AGENT_CD = '"+AGENT_CD+"' AND DOC_ID = "+query+"";

CommonService.transaction(db, function(tx){
        genOp.PROFILE.INS = null;
        genOp.PROFILE.PRO = null;
	CommonService.executeSql(tx, sql, ['IN'],
                			function(tx,res){
                			debug("Profile Imag Leng :"+res.rows.length);
                                if(!!res && res.rows.length>0){
									var fileName = AGENT_CD+"_"+APP_ID+"_"+res.rows.item(0).DOC_ID;

									CommonService.readOriginalFile( fileName+ ".jpg", imagePath).then(
                                        			function(res){
                                        				if(!!res){
                                    							genOp.PROFILE.INS = res;
                                    							//debug("PROFILE :INS"+res);
                                                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_PHOTO||',"data:image/jpg;base64,"+res);
                                                                if(isSelf)
                                                                {
                                                                	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_PHOTO||',"");
                                        				    		dfd.resolve(genOp.HtmlFormOutput);
                                        				    	}
                                        				}
                                        				else{
                                        					dfd.resolve(null);
                                        				}
                                        			}
                                        		);
                                }
                                else
                                    dfd.resolve(genOp.HtmlFormOutput);
                    if(isSelf != true)
					    CommonService.executeSql(tx, sql, ['PR'],
												function(tx,res){
												debug("Profile Imag Leng :Proposer:"+res.rows.length);
													if(!!res && res.rows.length>0){
														var fileName = AGENT_CD+"_"+APP_ID+"_"+res.rows.item(0).DOC_ID;
                                                        genOp.PROFILE.PRO = null;
														CommonService.readOriginalFile(fileName+ ".jpg", imagePath).then(
																		function(res){
																			if(!!res){
																					genOp.PROFILE.PRO = res;
																					//debug("PROFILE :PRO"+res);
																					genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_PHOTO||',"data:image/jpg;base64,"+res);
																			    dfd.resolve(genOp.HtmlFormOutput);
																			}
																			else{
																			    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_PHOTO||',"");
																				dfd.resolve(genOp.HtmlFormOutput);
																			}
																		}
																	);
													}
													else
													   dfd.resolve(genOp.HtmlFormOutput);
												},
												function(tx,err){
													dfd.resolve(null);
												});
                			},
                			function(tx,err){
                				dfd.resolve(null);
                			});

		}, function(error){});

return dfd.promise;

}


this.printUserInfoData = function(fileData){
console.log("printUserInfoData :"+APP_ID+" :"+POLICY_NO+": "+ AGENT_CD);
var dfd = $q.defer();
var sql = "SELECT UI.OFFICE_CODE AS OFFICE_CODE,UI.SUBOFFICECODE AS SUBOFFICECODE,UI.CHANNEL_NAME AS CHANNEL_NAME,UI.USER_CITY_BRANCH AS USER_CITY_BRANCH,UI.OFFICE_NAME,UI.OFFICE_ADDRESS1 AS OFFICE_ADDRESS1,UI.OFFICE_ADDRESS2 AS OFFICE_ADDRESS2,UI.OFFICE_STATE AS OFFICE_STATE,UI.AGENT_NAME AS AGENT_NAME,UI.MOBILENO AS MOBILENO,UI.IRDA_LIC_NO AS IRDA_LIC_NO,AM.EAI_NO AS EAI_NO,AM.EAI_RECEIVE_FLAG AS EFLAG,AM.MODEL_PREMIUM_AMOUNT AS MODAL,AM.SERVICE_TAX AS SERTAX FROM LP_APPLICATION_MAIN AS AM INNER JOIN LP_USERINFO AS UI ON AM.AGENT_CD==UI.AGENT_CD WHERE AM.APPLICATION_ID=? AND AM.AGENT_CD=?";

CommonService.transaction(db,function(tx){
					CommonService.executeSql(tx, sql,[APP_ID, AGENT_CD], function(tx,res){
						console.log("Success "+JSON.stringify(res));
						if(!!res && res.rows.length>0){
                                genOp.HtmlFormOutput = fileData;

                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROPNO||',POLICY_NO);

                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FOSCAMPAIGNCODE||',"");

                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BANKRELATIONSHIP||',"");

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NewPlaceHolder||',"");


                           /*
                           genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGENT_NAME||',"");
                           	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGNT_IDTYPE||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGENT_IDENTNO||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGENT_LANG||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||WITNESS_NAME||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||WITNESS_SIGN||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||WITNESS_IDTYPE||',"");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||WITNESS_IDNO||',"");
                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SIGN||',"");
                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SIGN||',"");
							*/
							if(PGL_ID=="191" || PGL_ID=="185"){
							 /***** iRaksha  *****/

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||CHANNEL||","WEBSALES");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||SERVICEBRANCHNAME||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||AGENTNAME||","Online WebSales");

							 if(BUSINESS_TYPE == "IndusSolution"){
							    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||AGENTCODE||","004603875");
							 }else
							    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||AGENTCODE||","004587542");

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||OFFCODE||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||SUBOFFCODE||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||BRANCHCODE||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||OFFICENAME||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDRESSLINE1||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDRESSLINE2||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||CONTACTDETAILS||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LICENSEDETAILS||","");
							 }
							 if(sisData.sisFormDData!=undefined && (sisData.sisFormDData[0].RIDER_MODAL_PREMIUM!=undefined && (sisData.sisFormDData[0].RIDER_MODAL_PREMIUM!="")))
							 {
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PAYING_PREMIUM||",parseInt(res.rows.item(0).MODAL)+parseInt(sisData.sisFormDData[0].RIDER_MODAL_PREMIUM));
							 }
							 else
							 {
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PAYING_PREMIUM||",parseInt(res.rows.item(0).MODAL));
							 }
							 //genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PAYING_SERVICETAX||",res.rows.item(0).SERTAX);
							 if(BUSINESS_TYPE != "IndusSolution"){
							    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||OFFCODE||",res.rows.item(0).OFFICE_CODE);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||SUBOFFCODE||",res.rows.item(0).SUBOFFICECODE);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||CHANNEL||",res.rows.item(0).CHANNEL_NAME);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||SERVICEBRANCHNAME||",res.rows.item(0).USER_CITY_BRANCH);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||BRANCHCODE||",res.rows.item(0).OFFICE_CODE);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||OFFICENAME||",res.rows.item(0).OFFICE_NAME);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDRESSLINE1||",res.rows.item(0).OFFICE_ADDRESS1);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDRESSLINE2||",res.rows.item(0).OFFICE_ADDRESS2);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||AGENTCODE||",AGENT_CD);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||AGENTNAME||",res.rows.item(0).AGENT_NAME);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||CONTACTDETAILS||",res.rows.item(0).MOBILENO);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LICENSEDETAILS||",res.rows.item(0).IRDA_LIC_NO);

							 }
							 if(res.rows.item(0).EAI_NO!=undefined && res.rows.item(0).EAI_NO!="")
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||EIANO||","*eIA No. "+res.rows.item(0).EAI_NO);
							 else
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||EIANO||","");

							 if(res.rows.item(0).EFLAG=="Y")
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||eaiFlag||","display:block;");
							 else
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||eaiFlag||","display:none;");
							dfd.resolve(genOp.HtmlFormOutput);
						}else{
						    debug("data not found for term");
						}
					}, function(tx, err){console.log("Error 1");});
					}, function(err){}, function(){});

return dfd.promise;
}

this.SISMainData = function(){
console.log("inside SISMainData");
var dfd = $q.defer();
var TBLsql1 = "SELECT SB.PLAN_NAME,SB.SUM_ASSURED,SB.POLICY_TERM,SB.PREMIUM_PAY_TERM,AM.MODEL_PREMIUM_AMOUNT,AM.SERVICE_TAX,SB.PREMIUM_PAY_MODE_DESC FROM LP_SIS_SCREEN_B AS SB INNER JOIN LP_APPLICATION_MAIN AS AM ON SB.SIS_ID==AM.SIS_ID WHERE APPLICATION_ID=? AND AM.AGENT_CD=?";
var TBLsql2 = "SELECT RD.RIDER_NAME AS RDNM,RD.RIDER_COVERAGE AS SA,RD.RIDER_TERM AS RTERM,RD.RIDER_PAY_TERM AS RPAYTERM,RD.RIDER_MODAL_PREMIUM RMODAL,RD.RIDER_PAY_MODE_DESC AS RMODE FROM LP_SIS_SCREEN_D AS RD INNER JOIN LP_APPLICATION_MAIN AS AM ON RD.SIS_ID==AM.SIS_ID WHERE APPLICATION_ID=? AND AM.AGENT_CD=?";

CommonService.transaction(db,function(tx){
					CommonService.executeSql(tx, TBLsql1, [APP_ID, AGENT_CD], function(tx,res){
						console.log("Success ");
						if(!!res && res.rows.length>0){
                            var planName = res.rows.item(0).PLAN_NAME;
                           	debug("NewPlanName :"+planName);
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_BASERIDER||",planName);
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_SA||",res.rows.item(0).SUM_ASSURED);
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_POLTERM||",res.rows.item(0).POLICY_TERM);
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PPT||",res.rows.item(0).PREMIUM_PAY_TERM);

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PREMIUM||",res.rows.item(0).MODEL_PREMIUM_AMOUNT);
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PAYMODE||",res.rows.item(0).PREMIUM_PAY_MODE_DESC);
						}
					}, function(tx, err){console.log("Error 1");});

					CommonService.executeSql(tx, TBLsql2, [APP_ID, AGENT_CD], function(tx,res){
							console.log("Success ");
							if(!!res && res.rows.length>0){
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_BASERIDER1||",res.rows.item(0).RDNM);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_SA1||",res.rows.item(0).SA);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_POLTERM1||",res.rows.item(0).RTERM);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PPT1||",res.rows.item(0).RPAYTERM);

                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PREMIUM1||",res.rows.item(0).RMODAL);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PAYMODE1||",res.rows.item(0).RMODE);
                                    dfd.resolve(genOp.HtmlFormOutput);
                                 }
                                 else
                                 {
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_BASERIDER1||","");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_SA1||","");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_POLTERM1||","");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PPT1||","");

                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PREMIUM1||","");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TBL_PAYMODE1||","");
                                    dfd.resolve(genOp.HtmlFormOutput);
                                 }
						}, function(tx, err){console.log("Error 1");});

					}, function(err){});
return dfd.promise;
}

this.printContactScrnA = function(){
console.log("inside ContactA");
var dfd = $q.defer();//FATCA
var contactSql = "SELECT IDENTITY_PROOF_NO,OCCUPATION_CLASS_OTHER,OTHER_EDUCATION_DETS,OCCUPATION_OTHERS,OCCUP_INDUSTRY_OTHERS,INCOME_PROOF_OTHER,IDENTITY_PROOF_OTHER,ADDRESS_PROOF_OTHER,AGE_PROOF_OTHER,TITLE,FIRST_NAME,MIDDLE_NAME,LAST_NAME,HUSB_FATH_FIRST_NAME,HUSB_FATH_LAST_NAME,MAIDEN_NAME,GENDER,BIRTH_DATE,IDENTIFICATION_MARK,DOMINANT_HAND,MARTIAL_STATUS,AGE_PROOF,NATIONALITY,EDUCATION_QUALIFICATION,CURR_ADD_LINE1,CURR_ADD_LINE2,CURR_ADD_LINE3,CURR_DISTRICT_LANDMARK,CURR_CITY,CURR_STATE,CURR_PIN,PERM_ADD_LINE1,PERM_ADD_LINE2,PERM_ADD_LINE3,PERM_DISTRICT_LANDMARK,PERM_CITY,PERM_STATE,PERM_PIN,COMMUNICATE_ADD_FLAG,RESI_STD_CODE,RESI_LANDLINE_NO,OFF_STD_CODE,OFF_LANDLINE_NO,MOBILE_NO,EMAIL_ID,OCCUPATION_CLASS,EMPLOYER_SCH_BUSI_NAME,EMPLOYER_SCH_BUSI_ADDRESS,OCCUPATION,OCCUPATION_OTHERS,OCCUPATION_INDUSTRY,OCCUP_INDUSTRY_OTHERS,OCCU_WORK_NATURE,OCCU_WORK_NATURE_OTHERS,ANNUAL_INCOME,IDENTITY_PROOF,IDENTITY_PROOF_OTHER,INCOME_PROOF,INCOME_PROOF_OTHER,AADHAR_CARD_EXISTS,AADHAR_CARD_NO,PAN_CARD_EXISTS,PAN_CARD_NO,PEP_SELP_FLAG,PEP_FAMILY_FLAG,PEP_DETAILS,PEP_FAMILY_DETAILS,RELATIONSHIP_DESC,ADDRESS_PROOF,ADDRESS_PROOF_OTHER,CURR_RESIDENT_COUNTRY,RESIDENT_STATUS,FATCA,BIRTH_COUNTRY FROM LP_APP_CONTACT_SCRN_A WHERE APPLICATION_ID=? AND CUST_TYPE=? AND AGENT_CD=?";

CommonService.transaction(db,function(tx){
					CommonService.executeSql(tx, contactSql, [APP_ID, 'C01', AGENT_CD], function(tx,res){
                    		console.log("Success ");
                    		if(!!res && res.rows.length>0){
							//FATCA
							 if(res.rows.item(0).FATCA!=undefined && res.rows.item(0).FATCA!="")
								genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||FATCA_INS||",(res.rows.item(0).FATCA == 'Y')?"Yes":"No");
							 else
								genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||FATCA_INS||","");

							 if(res.rows.item(0).FATCA=="Y")
								genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||fatcaInsFlag||","display:block;");
							 else
								genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||fatcaInsFlag||","display:none;");

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||TITLE||",res.rows.item(0).TITLE);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FIRST||",res.rows.item(0).FIRST_NAME);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_MIDDLE||",res.rows.item(0).MIDDLE_NAME);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_LAST||",res.rows.item(0).LAST_NAME);

							debug("|| genOp.isSelf: " + isSelf);
							 /*if(!!isSelf){
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_FIRST||",res.rows.item(0).FIRST_NAME);
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_MIDDLE||",res.rows.item(0).MIDDLE_NAME);
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_LAST||",res.rows.item(0).LAST_NAME);
							 }*/

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FATHER_FIRST||",res.rows.item(0).HUSB_FATH_FIRST_NAME);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FATHER_LAST||",res.rows.item(0).HUSB_FATH_LAST_NAME);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_MAIDEN||",res.rows.item(0).MAIDEN_NAME);

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_GENDER||",res.rows.item(0).GENDER);
							 var dob=res.rows.item(0).BIRTH_DATE;
							 dob=dob.substring(0,10);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_DOB||",dob);
							 if(!!res.rows.item(0).IDENTIFICATION_MARK){
   								res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, "&", "&amp;");
   								res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, "<", "&lt;");
   								res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, ">", "&gt;");
   								res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, '"', "&quot;");
   							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_IDENTMARK||",res.rows.item(0).IDENTIFICATION_MARK);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_DOMINANTHAND||",res.rows.item(0).DOMINANT_HAND);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_MARITALSTATUS||",res.rows.item(0).MARTIAL_STATUS);
							 if(!!res.rows.item(0).AGE_PROOF){
  								res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, "&", "&amp;");
  								res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, "<", "&lt;");
  								res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, ">", "&gt;");
  								res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, '"', "&quot;");
  							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_AGEPROOF||",res.rows.item(0).AGE_PROOF);
							  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_NATIONALITY||",res.rows.item(0).RESIDENT_STATUS);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FNATIONALITY||",res.rows.item(0).NATIONALITY);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_COUNTRYNAME||",res.rows.item(0).CURR_RESIDENT_COUNTRY);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_COUNTRY_OF_BIRTH||",res.rows.item(0).BIRTH_COUNTRY);
							 if(res.rows.item(0).RESIDENT_STATUS=="Resident Indian")
							 {
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showFNationality||","display:none;");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showCountryName||","display:none;");
							 }
							 else
							 {
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showFNationality||","display:block;");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showCountryName||","display:block;");
							 }

							 //changed by Amruta
							 tx.executeSql("Select  LOOKUP_TYPE_PRINT from LP_APP_SUB_QUESTION_LOOKUP where LOOKUP_TYPE_DESC=?",[res.rows.item(0).EDUCATION_QUALIFICATION],function(tx,r){
								 if(res.rows.item(0).EDUCATION_QUALIFICATION == "Others"){
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_EDUQUALHIGHEST||",res.rows.item(0).OTHER_EDUCATION_DETS);
								 }else{
								 console.log("Educational Qual is::");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_EDUQUALHIGHEST||",r.rows.item(0).LOOKUP_TYPE_PRINT);
								 console.log("Education is:::"+r.rows.item(0).LOOKUP_TYPE_PRINT);
								 }

							 },function(tx,error){
								console.log("sadf"+error.message);
							});

							 var currAdd=res.rows.item(0).CURR_ADD_LINE1;
							 if(res.rows.item(0).CURR_ADD_LINE2!=null)
							 currAdd =currAdd+" "+res.rows.item(0).CURR_ADD_LINE2;
							 if(res.rows.item(0).CURR_ADD_LINE3!=null)
							 currAdd=currAdd+" "+res.rows.item(0).CURR_ADD_LINE3;
							 if(!!currAdd){
  								currAdd = CommonService.replaceAll(currAdd, "&", "&amp;");
  								currAdd = CommonService.replaceAll(currAdd, "<", "&lt;");
  								currAdd = CommonService.replaceAll(currAdd, ">", "&gt;");
  								currAdd = CommonService.replaceAll(currAdd, '"', "&quot;");
  							}

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_CURRADDR||",currAdd);
							 if(!!res.rows.item(0).CURR_DISTRICT_LANDMARK){
									 res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, "&", "&amp;");
									 res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, "<", "&lt;");
									 res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, ">", "&gt;");
									 res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, '"', "&quot;");
								 }
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_LANDMARK||",res.rows.item(0).CURR_DISTRICT_LANDMARK);
							 if(!!res.rows.item(0).CURR_CITY){
    								res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, "&", "&amp;");
    								res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, "<", "&lt;");
    								res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, ">", "&gt;");
    								res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, '"', "&quot;");
    							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_CITY||",res.rows.item(0).CURR_CITY);
							 if(!!res.rows.item(0).CURR_STATE){
    								res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, "&", "&amp;");
    								res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, "<", "&lt;");
    								res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, ">", "&gt;");
    								res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, '"', "&quot;");
    							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_STATE||",res.rows.item(0).CURR_STATE);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PINCODE||",res.rows.item(0).CURR_PIN);
							 var permAdd=res.rows.item(0).PERM_ADD_LINE1;
							 if(res.rows.item(0).PERM_ADD_LINE2!=null)
							 permAdd =permAdd+" "+res.rows.item(0).PERM_ADD_LINE2;
							 if(res.rows.item(0).PERM_ADD_LINE3!=null)
							 permAdd=permAdd+" "+res.rows.item(0).PERM_ADD_LINE3;
							 if(!!permAdd){
   								permAdd = CommonService.replaceAll(permAdd, "&", "&amp;");
   								permAdd = CommonService.replaceAll(permAdd, "<", "&lt;");
   								permAdd = CommonService.replaceAll(permAdd, ">", "&gt;");
   								permAdd = CommonService.replaceAll(permAdd, '"', "&quot;");
   							}
							 /*var permAdd=res.rows.item(0).PERM_ADD_LINE1+" "+res.rows.item(0).PERM_ADD_LINE2+" "+res.rows.item(0).PERM_ADD_LINE3;*/
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PERMADDR||",permAdd);
							 if(!!res.rows.item(0).PERM_DISTRICT_LANDMARK){
    								res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, "&", "&amp;");
    								res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, "<", "&lt;");
    								res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, ">", "&gt;");
    								res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, '"', "&quot;");
    							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PERM_LANDMARK||",res.rows.item(0).PERM_DISTRICT_LANDMARK);
							 if(!!res.rows.item(0).PERM_CITY){
 								res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, "&", "&amp;");
 								res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, "<", "&lt;");
 								res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, ">", "&gt;");
 								res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, '"', "&quot;");
 							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PERM_CITY||",res.rows.item(0).PERM_CITY);
							 if(!!res.rows.item(0).PERM_STATE){
  								res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, "&", "&amp;");
  								res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, "<", "&lt;");
  								res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, ">", "&gt;");
  								res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, '"', "&quot;");
  							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PERM_STATE||",res.rows.item(0).PERM_STATE);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PM_PINCODE||",res.rows.item(0).PERM_PIN);
							 if(res.rows.item(0).COMMUNICATE_ADD_FLAG=="P")
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ADDRCOMM||","Permanent");
							 else
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ADDRCOMM||","Current");

							 var ResNo=res.rows.item(0).RESI_STD_CODE+"-"+res.rows.item(0).RESI_LANDLINE_NO;
							 if(res.rows.item(0).RESI_STD_CODE!=undefined && res.rows.item(0).RESI_STD_CODE!="")
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_RESIDENCE_NO||",ResNo);
							 else
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_RESIDENCE_NO||","");

							 var LandNo=res.rows.item(0).OFF_STD_CODE+"-"+res.rows.item(0).OFF_LANDLINE_NO;
							 if(res.rows.item(0).OFF_STD_CODE!=undefined && res.rows.item(0).OFF_STD_CODE!="")
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OFFICENO||",LandNo);
							 else
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OFFICENO||","");
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_MOBNO||",res.rows.item(0).MOBILE_NO);
							 if(!!res.rows.item(0).EMAIL_ID){
   								res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, "&", "&amp;");
   								res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, "<", "&lt;");
   								res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, ">", "&gt;");
   								res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, '"', "&quot;");
   							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_EMAIL||",res.rows.item(0).EMAIL_ID);
							 //changed by Ganesh
							 if(res.rows.item(0).OCCUPATION_CLASS == "Others"){
								 if(!!res.rows.item(0).OCCUPATION_CLASS_OTHER){
 	   								res.rows.item(0).OCCUPATION_CLASS_OTHER = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS_OTHER, "&", "&amp;");
 	   								res.rows.item(0).OCCUPATION_CLASS_OTHER = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS_OTHER, "<", "&lt;");
 	   								res.rows.item(0).OCCUPATION_CLASS_OTHER = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS_OTHER, ">", "&gt;");
 	   								res.rows.item(0).OCCUPATION_CLASS_OTHER = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS_OTHER, '"', "&quot;");
 	   							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCCLASS||",res.rows.item(0).OCCUPATION_CLASS_OTHER);
							 }else{
								 if(!!res.rows.item(0).OCCUPATION_CLASS){
 	   								res.rows.item(0).OCCUPATION_CLASS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS, "&", "&amp;");
 	   								res.rows.item(0).OCCUPATION_CLASS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS, "<", "&lt;");
 	   								res.rows.item(0).OCCUPATION_CLASS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS, ">", "&gt;");
 	   								res.rows.item(0).OCCUPATION_CLASS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_CLASS, '"', "&quot;");
 	   							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCCLASS||",res.rows.item(0).OCCUPATION_CLASS);
							 }
							 if(!!res.rows.item(0).EMPLOYER_SCH_BUSI_NAME){
 								res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, "&", "&amp;");
 								res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, "<", "&lt;");
 								res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, ">", "&gt;");
 								res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, '"', "&quot;");
 							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_EMPLOYERBSC||",res.rows.item(0).EMPLOYER_SCH_BUSI_NAME);
							 if(!!res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS){
  								res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, "&", "&amp;");
  								res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, "<", "&lt;");
  								res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, ">", "&gt;");
  								res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, '"', "&quot;");
  							}
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ADDREMPLOYERBSC||",res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS);
							 //changed by Ganesh
							 if(res.rows.item(0).OCCUPATION=="Others")
								{
									if(!!res.rows.item(0).OCCUPATION_OTHERS){
		 								res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, "&", "&amp;");
		 								res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, "<", "&lt;");
		 								res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, ">", "&gt;");
		 								res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, '"', "&quot;");
		 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCUPATION||",res.rows.item(0).OCCUPATION_OTHERS);
								}
							 else
							 {
							 if(res.rows.item(0).OCCUPATION!="" && res.rows.item(0).OCCUPATION!=null)
							 tx.executeSql("select OCCU_GROUP_PRINT from LP_APP_OCCUPATION_GROUP WHERE OCCU_GROUP=?",[res.rows.item(0).OCCUPATION],function(tx, res){
							 if(!!res && res.rows.length!=0)
									{
										if(!!res.rows.item(0).OCCU_GROUP_PRINT){
											res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, "&", "&amp;");
											res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, "<", "&lt;");
											res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, ">", "&gt;");
											res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, '"', "&quot;");
										}
										genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCUPATION||",res.rows.item(0).OCCU_GROUP_PRINT);
									}
									else
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCUPATION||","");
											},function(tx,error){
											console.log("sadf"+error.message);
											});
							 else
							 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_OCCUPATION||","");
							 }
							 //changed by Ganesh
							 if(res.rows.item(0).OCCUPATION_INDUSTRY=="Others")
								{
									if(!!res.rows.item(0).OCCUP_INDUSTRY_OTHERS){
										res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, "&", "&amp;");
										res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, "<", "&lt;");
										res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, ">", "&gt;");
										res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, '"', "&quot;");
									}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_INDUSTRY||",res.rows.item(0).OCCUP_INDUSTRY_OTHERS);
								}
							 else
								{
									if(!!res.rows.item(0).OCCUPATION_INDUSTRY){
	 									res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, "&", "&amp;");
	 									res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, "<", "&lt;");
	 									res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, ">", "&gt;");
	 									res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, '"', "&quot;");
	 								}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_INDUSTRY||",res.rows.item(0).OCCUPATION_INDUSTRY);
								}
							 //changed by Ganesh
							 if(res.rows.item(0).OCCU_WORK_NATURE=="Others")
								{
									if(!!res.rows.item(0).OCCU_WORK_NATURE_OTHERS){
	 									res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, "&", "&amp;");
	 									res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, "<", "&lt;");
	 									res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, ">", "&gt;");
	 									res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, '"', "&quot;");
	 								}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_NATUREWORK||",res.rows.item(0).OCCU_WORK_NATURE_OTHERS);
								}
							 else
								{
									if(!!res.rows.item(0).OCCU_WORK_NATURE){
	 									res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, "&", "&amp;");
	 									res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, "<", "&lt;");
	 									res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, ">", "&gt;");
	 									res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, '"', "&quot;");
	 								}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_NATUREWORK||",res.rows.item(0).OCCU_WORK_NATURE);
								}

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ANNINCOME||",res.rows.item(0).ANNUAL_INCOME);
							 //changed by Ganesh
							 if(res.rows.item(0).IDENTITY_PROOF=="Others")
								{
									if(!!res.rows.item(0).IDENTITY_PROOF_OTHER){
		 								res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, "&", "&amp;");
		 								res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, "<", "&lt;");
		 								res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, ">", "&gt;");
		 								res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, '"', "&quot;");
		 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_IDENTPROOF||",res.rows.item(0).IDENTITY_PROOF_OTHER);
								}
							 else
								{
									if(!!res.rows.item(0).IDENTITY_PROOF){
		 								res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, "&", "&amp;");
		 								res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, "<", "&lt;");
		 								res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, ">", "&gt;");
		 								res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, '"', "&quot;");
		 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_IDENTPROOF||",res.rows.item(0).IDENTITY_PROOF);
								}
							 //changed by Ganesh
							 if(res.rows.item(0).INCOME_PROOF=="Others")
								{
									if(!!res.rows.item(0).INCOME_PROOF_OTHER){
		 								res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, "&", "&amp;");
		 								res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, "<", "&lt;");
		 								res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, ">", "&gt;");
		 								res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, '"', "&quot;");
		 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_INCOMEPROOF||",res.rows.item(0).INCOME_PROOF_OTHER);
								}
							 else
								{
									if(!!res.rows.item(0).INCOME_PROOF){
	 	 								res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, "&", "&amp;");
	 	 								res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, "<", "&lt;");
	 	 								res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, ">", "&gt;");
	 	 								res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, '"', "&quot;");
	 	 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_INCOMEPROOF||",res.rows.item(0).INCOME_PROOF);
								}
							 //changed by Ganesh
							 if(res.rows.item(0).ADDRESS_PROOF=="Others")
								{
									if(!!res.rows.item(0).ADDRESS_PROOF_OTHER){
	  	 								res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, "&", "&amp;");
	  	 								res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, "<", "&lt;");
	  	 								res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, ">", "&gt;");
	  	 								res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, '"', "&quot;");
	  	 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ADDRPROOF||",res.rows.item(0).ADDRESS_PROOF_OTHER);
								}
							 else
								{
									if(!!res.rows.item(0).ADDRESS_PROOF){
	   	 								res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, "&", "&amp;");
	   	 								res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, "<", "&lt;");
	   	 								res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, ">", "&gt;");
	   	 								res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, '"', "&quot;");
	   	 							}
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_ADDRPROOF||",res.rows.item(0).ADDRESS_PROOF);
								}

							 if(res.rows.item(0).AADHAR_CARD_EXISTS=="Y"){
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_AADHAARNO||",res.rows.item(0).AADHAR_CARD_NO);
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_AADHAAR_FLAG||","");
							 }
							 else{
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_AADHAARNO||","");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_AADHAAR_FLAG||","No");
							 }
							 if(res.rows.item(0).PAN_CARD_EXISTS=="Y"){
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PANNO||",res.rows.item(0).PAN_CARD_NO);
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PAN_FLAG||","");
							 }
							 else{
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PANNO||","");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PAN_FLAG||","No");
							 }

							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PEP||",res.rows.item(0).PEP_SELP_FLAG);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FMPEP||",res.rows.item(0).PEP_FAMILY_FLAG);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_PEP_DET||",res.rows.item(0).PEP_DETAILS);
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_FMPEP_DET||",res.rows.item(0).PEP_FAMILY_DETAILS);

							 if(!!res.rows.item(0).IDENTITY_PROOF_NO){
   								res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, "&", "&amp;");
   								res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, "<", "&lt;");
   								res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, ">", "&gt;");
   								res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, '"', "&quot;");
   							}
							 //Added by Ganesh
							 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||LIFE_IDENTPROOF_NO||"," "+res.rows.item(0).IDENTITY_PROOF_NO);
                            dfd.resolve(genOp.HtmlFormOutput);
                        }
                        },function(tx, err){});
					CommonService.executeSql(tx, contactSql, [APP_ID, 'C02', AGENT_CD], function(tx,res){
						console.log("Success ");
						if(!!res && res.rows.length>0){
						//FATCA
                         if(res.rows.item(0).FATCA!=undefined && res.rows.item(0).FATCA!="")
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||FATCA_PRO||",(res.rows.item(0).FATCA == 'Y')?"Yes":"No");
                         else
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||FATCA_PRO||","");

                         if(res.rows.item(0).FATCA=="Y")
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||fatcaProFlag||","display:block;");
                         else
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||fatcaProFlag||","display:none;");
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_TITLE||",res.rows.item(0).TITLE);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_FIRST||",res.rows.item(0).FIRST_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_MIDDLE||",res.rows.item(0).MIDDLE_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||VERNAC_LAST||",res.rows.item(0).LAST_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FIRST||",res.rows.item(0).FIRST_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MIDDLE||",res.rows.item(0).MIDDLE_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_LAST||",res.rows.item(0).LAST_NAME);

					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FATHER_FIRST||",res.rows.item(0).HUSB_FATH_FIRST_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FATHER_LAST||",res.rows.item(0).HUSB_FATH_LAST_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MAIDEN||",res.rows.item(0).MAIDEN_NAME);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_GENDER||",res.rows.item(0).GENDER);
					 var dob=res.rows.item(0).BIRTH_DATE;
					 dob=dob.substring(0,10);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_DOB||",dob);
					 if(!!res.rows.item(0).IDENTIFICATION_MARK){
  						res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, "&", "&amp;");
  						res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, "<", "&lt;");
  						res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, ">", "&gt;");
  						res.rows.item(0).IDENTIFICATION_MARK = CommonService.replaceAll(res.rows.item(0).IDENTIFICATION_MARK, '"', "&quot;");
  					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTMARK||",res.rows.item(0).IDENTIFICATION_MARK);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_DOMINANTHAND||",res.rows.item(0).DOMINANT_HAND);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MARITALSTATUS||",res.rows.item(0).MARTIAL_STATUS);
					 if(!!res.rows.item(0).AGE_PROOF){
 						res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, "&", "&amp;");
 						res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, "<", "&lt;");
 						res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, ">", "&gt;");
 						res.rows.item(0).AGE_PROOF = CommonService.replaceAll(res.rows.item(0).AGE_PROOF, '"', "&quot;");
 					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AGEPROOF||",res.rows.item(0).AGE_PROOF);
					  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_NATIONALITY||",res.rows.item(0).RESIDENT_STATUS);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FNATIONALITY||",res.rows.item(0).NATIONALITY);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_COUNTRYNAME||",res.rows.item(0).CURR_RESIDENT_COUNTRY);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_COUNTRY_OF_BIRTH||",res.rows.item(0).BIRTH_COUNTRY || "");
					 if(res.rows.item(0).RESIDENT_STATUS=="Resident Indian")
					 {
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showFNationalityProp||","display:none;");
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showCountryNameProp||","display:none;");
					 }
					 else
					 {
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showFNationalityProp||","display:block;");
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showCountryNameProp||","display:block;");
					 }

					 if(res.rows.item(0).EDUCATION_QUALIFICATION == "Others"){
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EDUQUALHIGHEST||",res.rows.item(0).OTHER_EDUCATION_DETS);
					 }else{
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EDUQUALHIGHEST||",res.rows.item(0).EDUCATION_QUALIFICATION);
					 }


					 var currAdd=res.rows.item(0).CURR_ADD_LINE1;
					 if(res.rows.item(0).CURR_ADD_LINE2!=null)
					 currAdd =currAdd+" "+res.rows.item(0).CURR_ADD_LINE2;
					 if(res.rows.item(0).CURR_ADD_LINE3!=null)
					 currAdd=currAdd+" "+res.rows.item(0).CURR_ADD_LINE3;
					 if(!!currAdd){
 						currAdd = CommonService.replaceAll(currAdd, "&", "&amp;");
 						currAdd = CommonService.replaceAll(currAdd, "<", "&lt;");
 						currAdd = CommonService.replaceAll(currAdd, ">", "&gt;");
 						currAdd = CommonService.replaceAll(currAdd, '"', "&quot;");
 					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_CURRADDR||",currAdd);
					 if(!!res.rows.item(0).CURR_DISTRICT_LANDMARK){
                     						res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, "&", "&amp;");
                     						res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, "<", "&lt;");
                     						res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, ">", "&gt;");
                     						res.rows.item(0).CURR_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).CURR_DISTRICT_LANDMARK, '"', "&quot;");
                     					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_LANDMARK||",res.rows.item(0).CURR_DISTRICT_LANDMARK);
					 if(!!res.rows.item(0).CURR_CITY){
                     						res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, "&", "&amp;");
                     						res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, "<", "&lt;");
                     						res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, ">", "&gt;");
                     						res.rows.item(0).CURR_CITY = CommonService.replaceAll(res.rows.item(0).CURR_CITY, '"', "&quot;");
                     					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_CITY||",res.rows.item(0).CURR_CITY);
					 if(!!res.rows.item(0).CURR_STATE){
                     						res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, "&", "&amp;");
                     						res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, "<", "&lt;");
                     						res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, ">", "&gt;");
                     						res.rows.item(0).CURR_STATE = CommonService.replaceAll(res.rows.item(0).CURR_STATE, '"', "&quot;");
                     					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_STATE||",res.rows.item(0).CURR_STATE);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PINCODE||",res.rows.item(0).CURR_PIN);

					 var permAdd=res.rows.item(0).PERM_ADD_LINE1;
					 if(res.rows.item(0).PERM_ADD_LINE2!=null)
					 permAdd =permAdd+" "+res.rows.item(0).PERM_ADD_LINE2;
					 if(res.rows.item(0).PERM_ADD_LINE3!=null)
					 permAdd=permAdd+" "+res.rows.item(0).PERM_ADD_LINE3;
					 if(!!permAdd){
 						permAdd = CommonService.replaceAll(permAdd, "&", "&amp;");
 						permAdd = CommonService.replaceAll(permAdd, "<", "&lt;");
 						permAdd = CommonService.replaceAll(permAdd, ">", "&gt;");
 						permAdd = CommonService.replaceAll(permAdd, '"', "&quot;");
 					}
					/*var permAdd=res.rows.item(0).PERM_ADD_LINE1+" "+res.rows.item(0).PERM_ADD_LINE2+" "+res.rows.item(0).PERM_ADD_LINE3;*/
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERMADDR||",permAdd);
					 if(!!res.rows.item(0).PERM_DISTRICT_LANDMARK){
  						res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, "&", "&amp;");
  						res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, "<", "&lt;");
  						res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, ">", "&gt;");
  						res.rows.item(0).PERM_DISTRICT_LANDMARK = CommonService.replaceAll(res.rows.item(0).PERM_DISTRICT_LANDMARK, '"', "&quot;");
  					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_LANDMARK||",res.rows.item(0).PERM_DISTRICT_LANDMARK);
					 if(!!res.rows.item(0).PERM_CITY){
  						res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, "&", "&amp;");
  						res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, "<", "&lt;");
  						res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, ">", "&gt;");
  						res.rows.item(0).PERM_CITY = CommonService.replaceAll(res.rows.item(0).PERM_CITY, '"', "&quot;");
  					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_CITY||",res.rows.item(0).PERM_CITY);
					 if(!!res.rows.item(0).PERM_STATE){
					 	res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, "&", "&amp;");
					 	res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, "<", "&lt;");
					 	res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, ">", "&gt;");
					 	res.rows.item(0).PERM_STATE = CommonService.replaceAll(res.rows.item(0).PERM_STATE, '"', "&quot;");
					 }
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_STATE||",res.rows.item(0).PERM_STATE);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PM_PINCODE||",res.rows.item(0).PERM_PIN);
					 if(res.rows.item(0).COMMUNICATE_ADD_FLAG=="P")
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRCOMM||","Permanent");
					 else
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRCOMM||","Current");

					 var ResNo=res.rows.item(0).RESI_STD_CODE+"-"+res.rows.item(0).RESI_LANDLINE_NO;
					 if(!!res.rows.item(0).RESI_STD_CODE && res.rows.item(0).RESI_LANDLINE_NO)
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_RESIDENCE_NO||",ResNo);
					 else
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_RESIDENCE_NO||","");

					 var LandNo=res.rows.item(0).OFF_STD_CODE+"-"+res.rows.item(0).OFF_LANDLINE_NO
					 if(!!res.rows.item(0).OFF_STD_CODE && !!res.rows.item(0).OFF_LANDLINE_NO)
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OFFICENO||",LandNo);
					 else
					 	 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OFFICENO||","");

					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MOBNO||",res.rows.item(0).MOBILE_NO);
					 if(!!res.rows.item(0).EMAIL_ID){
   						res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, "&", "&amp;");
   						res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, "<", "&lt;");
   						res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, ">", "&gt;");
   						res.rows.item(0).EMAIL_ID = CommonService.replaceAll(res.rows.item(0).EMAIL_ID, '"', "&quot;");
   					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EMAIL||",res.rows.item(0).EMAIL_ID);

					 if(res.rows.item(0).OCCUPATION_CLASS == "Others"){
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCCLASS||",res.rows.item(0).OCCUPATION_CLASS_OTHER);
					 }else{
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCCLASS||",res.rows.item(0).OCCUPATION_CLASS);
					 }
					 if(!!res.rows.item(0).EMPLOYER_SCH_BUSI_NAME){
 						res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, "&", "&amp;");
 						res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, "<", "&lt;");
 						res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, ">", "&gt;");
 						res.rows.item(0).EMPLOYER_SCH_BUSI_NAME = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_NAME, '"', "&quot;");
 					}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EMPLOYERBSC||",res.rows.item(0).EMPLOYER_SCH_BUSI_NAME);
					 if(!!res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS){
    						res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, "&", "&amp;");
    						res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, "<", "&lt;");
    						res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, ">", "&gt;");
    						res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS = CommonService.replaceAll(res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS, '"', "&quot;");
    			 }
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDREMPLOYERBSC||",res.rows.item(0).EMPLOYER_SCH_BUSI_ADDRESS);

					 if(res.rows.item(0).OCCUPATION=="Others")
					 {
						 if(!!res.rows.item(0).OCCUPATION_OTHERS){
 	   						res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, "&", "&amp;");
 	   						res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, "<", "&lt;");
 	   						res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, ">", "&gt;");
 	   						res.rows.item(0).OCCUPATION_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUPATION_OTHERS, '"', "&quot;");
 	   					}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCUPATION||",res.rows.item(0).OCCUPATION_OTHERS);
				 	 }
					 else
					 {
					 //genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCUPATION||",res.rows.item(0).OCCUPATION);
					 if(res.rows.item(0).OCCUPATION!="" && res.rows.item(0).OCCUPATION!=null)
					 tx.executeSql("select OCCU_GROUP_PRINT from LP_APP_OCCUPATION_GROUP WHERE OCCU_GROUP=?",[res.rows.item(0).OCCUPATION],function(tx, res){
									 if(!!res.rows.item(0).OCCU_GROUP_PRINT){
										 res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, "&", "&amp;");
										 res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, "<", "&lt;");
										 res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, ">", "&gt;");
										 res.rows.item(0).OCCU_GROUP_PRINT = CommonService.replaceAll(res.rows.item(0).OCCU_GROUP_PRINT, '"', "&quot;");
									 }
									 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCUPATION||",res.rows.item(0).OCCU_GROUP_PRINT);
								   },function(tx,error){
								   console.log("sadf"+error.message);
								   });
					 else
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCUPATION||","");
					 }

					 if(res.rows.item(0).OCCUPATION_INDUSTRY=="Others")
					 {
						 if(!!res.rows.item(0).OCCUP_INDUSTRY_OTHERS){
 							res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, "&", "&amp;");
 							res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, "<", "&lt;");
 							res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, ">", "&gt;");
 							res.rows.item(0).OCCUP_INDUSTRY_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCUP_INDUSTRY_OTHERS, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INDUSTRY||",res.rows.item(0).OCCUP_INDUSTRY_OTHERS);
				 	}
					 else
					 {
						 if(!!res.rows.item(0).OCCUPATION_INDUSTRY){
 							res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, "&", "&amp;");
 							res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, "<", "&lt;");
 							res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, ">", "&gt;");
 							res.rows.item(0).OCCUPATION_INDUSTRY = CommonService.replaceAll(res.rows.item(0).OCCUPATION_INDUSTRY, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INDUSTRY||",res.rows.item(0).OCCUPATION_INDUSTRY);
				 	}

					 if(res.rows.item(0).OCCU_WORK_NATURE=="Others")
					 {
						 if(!!res.rows.item(0).OCCU_WORK_NATURE_OTHERS){
 							res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, "&", "&amp;");
 							res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, "<", "&lt;");
 							res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, ">", "&gt;");
 							res.rows.item(0).OCCU_WORK_NATURE_OTHERS = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE_OTHERS, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_NATUREWORK||",res.rows.item(0).OCCU_WORK_NATURE_OTHERS);
				 	 }
					 else
					 {
						 if(!!res.rows.item(0).OCCU_WORK_NATURE){
 							res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, "&", "&amp;");
 							res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, "<", "&lt;");
 							res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, ">", "&gt;");
 							res.rows.item(0).OCCU_WORK_NATURE = CommonService.replaceAll(res.rows.item(0).OCCU_WORK_NATURE, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_NATUREWORK||",res.rows.item(0).OCCU_WORK_NATURE);
				 	 }

					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ANNINCOME||",res.rows.item(0).ANNUAL_INCOME);

					 if(res.rows.item(0).IDENTITY_PROOF=="Others")
					 {
						 if(!!res.rows.item(0).IDENTITY_PROOF_OTHER){
 							res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, "&", "&amp;");
 							res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, "<", "&lt;");
 							res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, ">", "&gt;");
 							res.rows.item(0).IDENTITY_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_OTHER, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTPROOF||",res.rows.item(0).IDENTITY_PROOF_OTHER);
				 	 }
					 else
					 {
						 if(!!res.rows.item(0).IDENTITY_PROOF){
 							res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, "&", "&amp;");
 							res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, "<", "&lt;");
 							res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, ">", "&gt;");
 							res.rows.item(0).IDENTITY_PROOF = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTPROOF||",res.rows.item(0).IDENTITY_PROOF);
				 	 }

					 if(res.rows.item(0).INCOME_PROOF=="Others")
					 {
						 if(!!res.rows.item(0).INCOME_PROOF_OTHER){
 							res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, "&", "&amp;");
 							res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, "<", "&lt;");
 							res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, ">", "&gt;");
 							res.rows.item(0).INCOME_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF_OTHER, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INCOMEPROOF||",res.rows.item(0).INCOME_PROOF_OTHER);
				 	 }
					 else
					 {
						 if(!!res.rows.item(0).INCOME_PROOF){
 							res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, "&", "&amp;");
 							res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, "<", "&lt;");
 							res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, ">", "&gt;");
 							res.rows.item(0).INCOME_PROOF = CommonService.replaceAll(res.rows.item(0).INCOME_PROOF, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INCOMEPROOF||",res.rows.item(0).INCOME_PROOF);
				 	 }

					 if(res.rows.item(0).ADDRESS_PROOF=="Others")
					 {
						 if(!!res.rows.item(0).ADDRESS_PROOF_OTHER){
 							res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, "&", "&amp;");
 							res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, "<", "&lt;");
 							res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, ">", "&gt;");
 							res.rows.item(0).ADDRESS_PROOF_OTHER = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF_OTHER, '"', "&quot;");
 						}
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRPROOF||",res.rows.item(0).ADDRESS_PROOF_OTHER);
				   }
					 else
					 {
						 if(!!res.rows.item(0).ADDRESS_PROOF){
						 res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, "&", "&amp;");
						 res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, "<", "&lt;");
						 res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, ">", "&gt;");
						 res.rows.item(0).ADDRESS_PROOF = CommonService.replaceAll(res.rows.item(0).ADDRESS_PROOF, '"', "&quot;");
					 	 }
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRPROOF||",res.rows.item(0).ADDRESS_PROOF);
				 	 }

					 if(res.rows.item(0).AADHAR_CARD_EXISTS=="Y"){
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAARNO||",res.rows.item(0).AADHAR_CARD_NO);
						 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAAR_FLAG||","");
					 }
					 else{
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAARNO||","");
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAAR_FLAG||","No");
					 }
					 if(res.rows.item(0).PAN_CARD_EXISTS=="Y"){
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PANNO||",res.rows.item(0).PAN_CARD_NO);
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PAN_FLAG||","");
					 }
					 else
					 {
					 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PANNO||","");
					  	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PAN_FLAG||","No");
					 }
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PEP||",res.rows.item(0).PEP_SELP_FLAG);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FMPEP||",res.rows.item(0).PEP_FAMILY_FLAG);

					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PEP_DET||",res.rows.item(0).PEP_DETAILS);
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FMPEP_DET||",res.rows.item(0).PEP_FAMILY_DETAILS);
					   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDDETAILOFPROP||",res.rows.item(0).RELATIONSHIP_DESC);
						 if(!!res.rows.item(0).IDENTITY_PROOF_NO){
 							res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, "&", "&amp;");
 							res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, "<", "&lt;");
 							res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, ">", "&gt;");
 							res.rows.item(0).IDENTITY_PROOF_NO = CommonService.replaceAll(res.rows.item(0).IDENTITY_PROOF_NO, '"', "&quot;");
 						}
					 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTPROOF_NO||"," "+res.rows.item(0).IDENTITY_PROOF_NO);

						}
						else
						{
						    //FATCA
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||FATCA_PRO||",'');
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,"||fatcaProFlag||","display:none;");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||ADDDETAILOFPROP||","Self");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_TITLE||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FIRST||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MIDDLE||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_LAST||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FATHER_FIRST||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FATHER_LAST||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MAIDEN||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_GENDER||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_DOB||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTMARK||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_DOMINANTHAND||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MARITALSTATUS||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AGEPROOF||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_NATIONALITY||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FNATIONALITY||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_COUNTRYNAME||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_COUNTRY_OF_BIRTH||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showFNationalityProp||","display:none;");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||showCountryNameProp||","display:none;");
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EDUQUALHIGHEST||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_CURRADDR||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_LANDMARK||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_CITY||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_STATE||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PINCODE||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERMADDR||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_LANDMARK||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_CITY||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PERM_STATE||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PM_PINCODE||",'');
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRCOMM||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_RESIDENCE_NO||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OFFICENO||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_MOBNO||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EMAIL||",'');

								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCCLASS||",'');


							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_EMPLOYERBSC||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDREMPLOYERBSC||",'');

									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_OCCUPATION||","");

								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INDUSTRY||",'');
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_NATUREWORK||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ANNINCOME||",'');
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTPROOF||",'');

								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_INCOMEPROOF||",'');
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRPROOF||",'');

								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_ADDRPROOF||",'');

								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAARNO||",'');
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_AADHAAR_FLAG||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PANNO||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PAN_FLAG||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_panFlag||","display:none;");

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PEP||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FMPEP||",'');

							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_PEP_DET||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_FMPEP_DET||",'');
							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||PROP_IDENTPROOF_NO||",'');
						dfd.resolve(genOp.HtmlFormOutput);
						}
					}, function(tx, err){console.log("Error 1");});
					}, function(err){});
return dfd.promise;
}

this.printNomineeDetails = function(){
console.log("inside Nominee");
var dfd = $q.defer();
var nomSql = "SELECT FIRST_NAME,MIDDLE_NAME,LAST_NAME,BIRTH_DATE,GENDER,RELATIONSHIP_DESC,PERCENTAGE FROM LP_APP_NOM_CONTACT_SCRN_F WHERE APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?";
//genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,);
debug("isSelf :"+isSelf);
if(isSelf == false && PGL_ID != "212" && PGL_ID!="221"){

        for(var i=1;i<=5;i++){

            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_FNAME"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_MNAME"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_LNAME"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_DOB"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_GENDER"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_RELATION"+i+"||","");
            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_PERCENT"+i+"||","");

        }
      		dfd.resolve(genOp.HtmlFormOutput);
      		//getAppointeeDetails();
    }
    else
    {
		CommonService.transaction(db,function(tx){
        					CommonService.executeSql(tx, nomSql,[APP_ID, AGENT_CD, 'C05'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
                                     var noOfNomm = res.rows.length;
                                     var noOfNommEmpty = 5 - parseInt(noOfNomm);

                                     for(var i=0;i<noOfNomm;i++){
                                     var j=i+1;
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NOMINEE_FNAME'+j+'||',res.rows.item(i).FIRST_NAME);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_MNAME"+j+"||",res.rows.item(i).MIDDLE_NAME);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_LNAME"+j+"||",res.rows.item(i).LAST_NAME);
                                     var dob=res.rows.item(i).BIRTH_DATE;
                                     dob=dob.substring(0,10);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_DOB"+j+"||",dob);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_GENDER"+j+"||",res.rows.item(i).GENDER);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_RELATION"+j+"||",res.rows.item(i).RELATIONSHIP_DESC);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_PERCENT"+j+"||",res.rows.item(i).PERCENTAGE);
                                     }
                                     for(var k=noOfNomm+1;k<=5;k++){
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NOMINEE_FNAME'+k+'||',"");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_MNAME"+k+"||","");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_LNAME"+k+"||","");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_DOB"+k+"||","");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_GENDER"+k+"||","");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_RELATION"+k+"||","");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_PERCENT"+k+"||","");
                                     }
                                     dfd.resolve(genOp.HtmlFormOutput);
                                     }else{
                                         for(var i=1;i<=5;i++){
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NOMINEE_FNAME'+i+'||',"");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_MNAME"+i+"||","");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_LNAME"+i+"||","");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_DOB"+i+"||","");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_GENDER"+i+"||","");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_RELATION"+i+"||","");
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,"||NOMINEE_PERCENT"+i+"||","");
                                         }
                                         dfd.resolve(genOp.HtmlFormOutput);
                                     }

        					}, function(tx, err){console.log("Error 1");});
        					}, function(err){});

    }
return dfd.promise;
}

this.printAppointeeDetails = function(){
console.log("inside Appointee");
var dfd = $q.defer();
var appointeeSql = "SELECT FIRST_NAME, MIDDLE_NAME, LAST_NAME, BIRTH_DATE, RELATIONSHIP_DESC, GENDER from LP_APP_NOM_CONTACT_SCRN_F where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?";

CommonService.transaction(db,function(tx){
        					CommonService.executeSql(tx, appointeeSql,[APP_ID, AGENT_CD, "C98"], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_FIRST||',res.rows.item(0).FIRST_NAME);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_MIDDLE||',res.rows.item(0).MIDDLE_NAME);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_LAST||',res.rows.item(0).LAST_NAME);
								   var dob=res.rows.item(0).BIRTH_DATE;
								   dob=dob.substring(0,10);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_DOB||',dob);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_GNDR||',res.rows.item(0).GENDER);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_RLN||',res.rows.item(0).RELATIONSHIP_DESC);
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_PRCNT||',"100");
							    dfd.resolve(genOp.HtmlFormOutput);
							   }
							   else
							   {
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_FIRST||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_MIDDLE||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_LAST||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_DOB||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_GNDR||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_RLN||',"");
								   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APNTE_PRCNT||',"");
                                dfd.resolve(genOp.HtmlFormOutput);
							   }
        					}, function(tx, err){console.log("Error 1");});
        					}, function(err){});
return dfd.promise;
}

var j;

this.printFundSelectionDetails = function(){
console.log("inside Funds");
var dfd = $q.defer();
/*var sql1 = "select fd.FUND_DESCRIPTION AS DESC,fd.FUND_SFIN_NUMBER AS SFIN,app.OTHER_DETAILS AS OTH from LP_APP_FUND_SCRN_I AS app JOIN LP_FUND_MASTER AS fd on (fd.FUND_CODE=app.FUND_CODE_FROM) where app.APPLICATION_ID=?";
var sql2 = "select fd.FUND_DESCRIPTION AS DESC,fd.FUND_SFIN_NUMBER AS SFIN from LP_APP_FUND_SCRN_I AS app JOIN LP_FUND_MASTER AS fd on (fd.FUND_CODE=app.FUND_CODE_TO) where app.APPLICATION_ID=?";*/

var sql1 = "select fd.FUND_DESCRIPTION AS DESC,fd.FUND_SFIN_NUMBER AS SFIN from LP_SIS_SCREEN_C AS sis JOIN LP_FUND_MASTER AS fd on (fd.FUND_CODE=sis.FUND_CODE_FROM) where sis.SIS_ID=?";
var sql2 = "select fd.FUND_DESCRIPTION AS DESC,fd.FUND_SFIN_NUMBER AS SFIN from LP_SIS_SCREEN_C AS sis JOIN LP_FUND_MASTER AS fd on (fd.FUND_CODE=sis.FUND_CODE_TO) where sis.SIS_ID=?";


 CommonService.transaction(db, function(tx){
                   j = 0;
                   for(var i=0;i<10;i++){
                   (function(i){

                    var sql = "select fd.FUND_DESCRIPTION AS DESC,fd.FUND_SFIN_NUMBER AS SFIN,sis.FUND"+(i+1)+"_CODE AS CODE,sis.FUND"+(i+1)+"_PERCENT AS PERC from LP_SIS_SCREEN_C AS sis JOIN LP_FUND_MASTER AS fd on (fd.FUND_CODE=sis.FUND"+(i+1)+"_CODE) where sis.SIS_ID='"+SIS_ID+"'";
                    console.log(sql);
                    CommonService.executeSql(tx, sql,[], function(tx,res){
						console.log("Success "+res.rows.length);
						if(!!res && res.rows.length>0){
						console.log("FUNDS data :"+JSON.stringify(res.rows.item(0)));
						 var r = res.rows.item(0);
							 j=j+1;
						  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NAME_OF_FUND_SELECTION'+j+'||',r.DESC+" "+r.SFIN);
						  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FUND_ALLOCATION'+j+'||',r.PERC);

						}
					}, function(tx, err){console.log("Error 1");});

                    }(i));
                   // dfd.resolve(genOp.HtmlFormOutput);
                   }

					CommonService.executeSql(tx, sql1,[SIS_ID], function(tx,res){
								console.log("Success ");
								if(!!res && res.rows.length>0){
 									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PORTFOLIO_STRATEGY||',"Smart");
                               		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEBTORIENTEDFUND||',res.rows.item(0).DESC+" "+res.rows.item(0).SFIN);
                                 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FUND_OTHERDETAILS||',"");
                                 //dfd.resolve(genOp.HtmlFormOutput);
                                 }
                                 else
                                 {
									 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PORTFOLIO_STRATEGY||',"");
									 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEBTORIENTEDFUND||',"");
									 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FUND_OTHERDETAILS||',"");
                                 //dfd.resolve(genOp.HtmlFormOutput);
                                 }
					}, function(tx, err){console.log("Error 1");});

             		CommonService.executeSql(tx, sql2,[SIS_ID], function(tx,res){
                        						console.log("Success ");
												console.log("Smart :TO:"+res.rows.length);
												 if(res.rows.length!=0)
													genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EQUITYORIENTEDFUND||',res.rows.item(0).DESC+" "+res.rows.item(0).SFIN);
												 else
												 {
													genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EQUITYORIENTEDFUND||',"");
												 }
                                                    //dfd.resolve(genOp.HtmlFormOutput);

                        					}, function(tx, err){console.log("Error 1");});

                   }, function(error){
                   console.log("DB error : "+error.message);
                   },function(){
                   var cnt = 6-j;
                   console.log("Here is d count :"+cnt+"--"+j);

                   for(var k=j+1;k<=6;k++)
                   {
                   console.log("K :: "+k);
                   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||NAME_OF_FUND_SELECTION'+k+'||',"");
                   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FUND_ALLOCATION'+k+'||',"");
                    if(k == 6)
                        dfd.resolve(genOp.HtmlFormOutput);
                   }
					if(j == 6)
                       dfd.resolve(genOp.HtmlFormOutput);
                   });
   return dfd.promise;
}

this.printPaymentModeDetails = function(){
console.log("inside PayMode");
var dfd = $q.defer();
var paySql = "SELECT MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,PREMIUM_PAY_MODE,PAY_METHOD_CHEQ_FLAG,PAY_METHOD_DD_FLAG,PAY_METHOD_ECS_FLAG,PAY_METHOD_SI_FLAG,PAY_METHOD_CC_FLAG,PAY_METHOD_ONLINE_FLAG,PAY_METHOD_CASH_FLAG from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
//var genOp.payMethod;
CommonService.transaction(db,function(tx){
        					CommonService.executeSql(tx, paySql,[APP_ID, AGENT_CD], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
                                                console.log("Payment Data  :"+JSON.stringify(res.rows.item(0)));
								 if(res.rows.item(0).PREMIUM_PAY_MODE=="M")
								  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"Monthly");
								 if(res.rows.item(0).PREMIUM_PAY_MODE=="S")
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"Semi-Annual");
								 if(res.rows.item(0).PREMIUM_PAY_MODE=="A")
								  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"Annual");
								 if(res.rows.item(0).PREMIUM_PAY_MODE=="O")
								  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"Single-Pay");
								 if(res.rows.item(0).PREMIUM_PAY_MODE=="Q")
								  genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"Quarterly");

							if(res.rows.item(0).MONTHS_INITIAL_DEPOSIT==null || res.rows.item(0).MONTHS_INITIAL_DEPOSIT=="")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_MONTHINIDEPOSIT||',"");
                               else
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_MONTHINIDEPOSIT||',res.rows.item(0).MONTHS_INITIAL_DEPOSIT);
                               if(res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE=="M")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"Monthly");
                               if(res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE=="S")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"Semi-Annual");
                               if(res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE=="A")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"Annual");
                               if(res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE=="Q")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"Quarterly");
                               if(res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE==null || res.rows.item(0).FIRST_ANIV_CHANGE_PAY_MODE=="")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"");

								 if(res.rows.item(0).PAY_METHOD_CHEQ_FLAG=="Y")
								 {
								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Cheque");
								 	genOp.payMethod = "Cheque";
								 }
								 if(res.rows.item(0).PAY_METHOD_ONLINE_FLAG=="Y")
								 {
									 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Online Payment");
								 	genOp.payMethod = "Online Payment";
								 }
								 if(res.rows.item(0).PAY_METHOD_CASH_FLAG=="Y")
								 {

								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Cash");
								 	genOp.payMethod = "Cash";
								 }
								 if(res.rows.item(0).PAY_METHOD_DD_FLAG=="Y")
								 {
								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Demand Draft");
								 	genOp.payMethod = "Demand Draft";
								 }
								 if(res.rows.item(0).PAY_METHOD_CC_FLAG=="Y")
								 {
								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Credit Card");
								 	genOp.payMethod = "Credit Card";
								 }
								 if(res.rows.item(0).PAY_METHOD_ECS_FLAG=="Y")
								 {
								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"ECS");
								 	genOp.payMethod = "ECS";
								 }
								 if(res.rows.item(0).PAY_METHOD_SI_FLAG=="Y")
								 {
								 	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"Standing Instructions");
								 	genOp.payMethod = "Standing Instructions";
								 }
                                	dfd.resolve(genOp.HtmlFormOutput);
        						}
        						else
        							{
        								genOp.payMethod = "NA";
        								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODE||',"");
        								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_MONTHINIDEPOSIT||',"");
        								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_MODECHANGE||',"");
        								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PREMIUM_PAYING_MODE||',"");
        								dfd.resolve(genOp.HtmlFormOutput);
        							}
        					}, function(tx, err){console.log("Error 1");});
        					}, function(err){});
return dfd.promise;
}

this.printPaymentDataDetails = function(){
var dfd = $q.defer();
console.log("Select pay method ::"+genOp.payMethod);
var cashSql = "SELECT CASH_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var OnlSql = "SELECT TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var chqSql = "SELECT CHEQUE_NO,CHEQ_BANK_NAME,CHEQ_BANK_BRANCH_NAME,CHEQ_AMOUNT,CHEQ_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var ddSql = "SELECT DEMAND_DRAFT_NO,DD_BANK_NAME,DD_BANK_BRANCH_NAME,DD_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var ecsSql = "SELECT ECS_BANK_NAME,ECS_BANK_BRANCH_NAME,ECS_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var siSql = "SELECT SI_BANK_NAME,SI_BANK_BRANCH_NAME,SI_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PAYMENT_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
var ecsSql = "SELECT CC_AMOUNT,CREDIT_CARD_NO,CC_EXP_DATE,TRANSACTION_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_PAYMENT_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,CC_CARD_TYPE,TRANSACTION_DATE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";

CommonService.transaction(db,function(tx){
							if(genOp.payMethod == "NA")
							{
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',"");

								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',"");
								 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',"");

								 dfd.resolve(genOp.HtmlFormOutput);
							}
                           if(genOp.payMethod == "Cash")
        					CommonService.executeSql(tx, cashSql,[APP_ID, AGENT_CD], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',"");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',"");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',"");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).CASH_AMOUNT);

                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);

                                     dfd.resolve(genOp.HtmlFormOutput);

        						}

        					}, function(tx, err){console.log("Error 1");});
        					if(genOp.payMethod == "Online Payment")
        					CommonService.executeSql(tx, OnlSql,[APP_ID, AGENT_CD], function(tx,res){
                                console.log("Success ");
                                if(!!res && res.rows.length>0){
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',"");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',"");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',"");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);

                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);

                                            dfd.resolve(genOp.HtmlFormOutput);

                                }
                            }, function(tx, err){console.log("Error 1");});
                            if(genOp.payMethod == "Cheque")
                            CommonService.executeSql(tx, chqSql,[APP_ID, AGENT_CD], function(tx,res){
                                console.log("Success ");
                                if(!!res && res.rows.length>0){
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',res.rows.item(0).CHEQUE_NO);
								 if(!!res.rows.item(0).CHEQ_BANK_NAME){
	 								res.rows.item(0).CHEQ_BANK_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_NAME, "&", "&amp;");
	 								res.rows.item(0).CHEQ_BANK_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_NAME, "<", "&lt;");
	 								res.rows.item(0).CHEQ_BANK_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_NAME, ">", "&gt;");
	 								res.rows.item(0).CHEQ_BANK_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_NAME, '"', "&quot;");
	 							}
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',res.rows.item(0).CHEQ_BANK_NAME);
								 if(!!res.rows.item(0).CHEQ_BANK_BRANCH_NAME){
								   res.rows.item(0).CHEQ_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_BRANCH_NAME, "&", "&amp;");
								   res.rows.item(0).CHEQ_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_BRANCH_NAME, "<", "&lt;");
								   res.rows.item(0).CHEQ_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_BRANCH_NAME, ">", "&gt;");
								   res.rows.item(0).CHEQ_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).CHEQ_BANK_BRANCH_NAME, '"', "&quot;");
							   }
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',res.rows.item(0).CHEQ_BANK_BRANCH_NAME);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).CHEQ_AMOUNT);
                                 var dob=res.rows.item(0).CHEQ_DATE;
                                 dob=dob.substring(0,10);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',dob);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);
                                        dfd.resolve(genOp.HtmlFormOutput);

                                }
                            }, function(tx, err){console.log("Error 1");});
                            if(genOp.payMethod == "Demand Draft")
                            CommonService.executeSql(tx, ddSql,[APP_ID, AGENT_CD], function(tx,res){
                                console.log("Success ");
                                if(!!res && res.rows.length>0){
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',res.rows.item(0).DEMAND_DRAFT_NO);
									if(!!res.rows.item(0).DD_BANK_NAME){
	   								   res.rows.item(0).DD_BANK_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_NAME, "&", "&amp;");
	   								   res.rows.item(0).DD_BANK_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_NAME, "<", "&lt;");
	   								   res.rows.item(0).DD_BANK_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_NAME, ">", "&gt;");
	   								   res.rows.item(0).DD_BANK_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_NAME, '"', "&quot;");
	   							   }
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',res.rows.item(0).DD_BANK_NAME);
									if(!!res.rows.item(0).DD_BANK_BRANCH_NAME){
	   								   res.rows.item(0).DD_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_BRANCH_NAME, "&", "&amp;");
	   								   res.rows.item(0).DD_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_BRANCH_NAME, "<", "&lt;");
	   								   res.rows.item(0).DD_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_BRANCH_NAME, ">", "&gt;");
	   								   res.rows.item(0).DD_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).DD_BANK_BRANCH_NAME, '"', "&quot;");
	   							   }
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',res.rows.item(0).DD_BANK_BRANCH_NAME);
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).DD_AMOUNT);
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
                                    /*genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_PREMIUM||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);*/
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);

                                        dfd.resolve(genOp.HtmlFormOutput);
                                }
                            }, function(tx, err){console.log("Error 1");});
                            if(genOp.payMethod == "ECS")
                            CommonService.executeSql(tx, ecsSql,[APP_ID, AGENT_CD], function(tx,res){
                                console.log("Success ");
                                if(!!res && res.rows.length>0){
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',"");
								if(!!res.rows.item(0).ECS_BANK_NAME){
									res.rows.item(0).ECS_BANK_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_NAME, "&", "&amp;");
									res.rows.item(0).ECS_BANK_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_NAME, "<", "&lt;");
									res.rows.item(0).ECS_BANK_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_NAME, ">", "&gt;");
									res.rows.item(0).ECS_BANK_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_NAME, '"', "&quot;");
								}
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',res.rows.item(0).ECS_BANK_NAME);
								if(!!res.rows.item(0).ECS_BANK_BRANCH_NAME){
									res.rows.item(0).ECS_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_BRANCH_NAME, "&", "&amp;");
									res.rows.item(0).ECS_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_BRANCH_NAME, "<", "&lt;");
									res.rows.item(0).ECS_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_BRANCH_NAME, ">", "&gt;");
									res.rows.item(0).ECS_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).ECS_BANK_BRANCH_NAME, '"', "&quot;");
								}
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',res.rows.item(0).ECS_BANK_BRANCH_NAME);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).ECS_AMOUNT);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
                                 /*genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_PREMIUM||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);*/
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);
                                    dfd.resolve(genOp.HtmlFormOutput);
                                }
                            }, function(tx, err){console.log("Error 1");});
                            if(genOp.payMethod == "Standing Instructions")
                            CommonService.executeSql(tx, siSql,[APP_ID, AGENT_CD], function(tx,res){
                                                            console.log("Success ");
                                                            if(!!res && res.rows.length>0){
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CHEQUENO||',"");
															if(!!res.rows.item(0).SI_BANK_NAME){
							 									res.rows.item(0).SI_BANK_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_NAME, "&", "&amp;");
							 									res.rows.item(0).SI_BANK_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_NAME, "<", "&lt;");
							 									res.rows.item(0).SI_BANK_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_NAME, ">", "&gt;");
							 									res.rows.item(0).SI_BANK_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_NAME, '"', "&quot;");
							 								}
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||ISSUEBANK||',res.rows.item(0).SI_BANK_NAME);
															if(!!res.rows.item(0).SI_BANK_BRANCH_NAME){
 							 									res.rows.item(0).SI_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_BRANCH_NAME, "&", "&amp;");
 							 									res.rows.item(0).SI_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_BRANCH_NAME, "<", "&lt;");
 							 									res.rows.item(0).SI_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_BRANCH_NAME, ">", "&gt;");
 							 									res.rows.item(0).SI_BANK_BRANCH_NAME = CommonService.replaceAll(res.rows.item(0).SI_BANK_BRANCH_NAME, '"', "&quot;");
 							 								}
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BRANCH||',res.rows.item(0).SI_BANK_BRANCH_NAME);
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).SI_AMOUNT);
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYMENT_DATE||',"");
                                                            /* genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_PREMIUM||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);*/
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_SERVICETAX||',res.rows.item(0).TOTAL_SERVICE_TAX);
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PAYING_TOTPAYMENT||',res.rows.item(0).TOTAL_PREMIUM_AMOUNT);
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AMOUNT||',res.rows.item(0).TOTAL_PAYMENT_AMOUNT);
                                                                dfd.resolve(genOp.HtmlFormOutput);
                                                            }
                                                        }, function(tx, err){console.log("Error 1");});
        					}, function(err){});
return dfd.promise;
}

this.printNEFTDetails = function(){
console.log("inside NEFT");
var dfd = $q.defer();
var neftSql = "SELECT NEFT_ACCOUNT_HOLDER_NAME,NEFT_BANK_ACCOUNT_NO,NEFT_BANK_NAME,NEFT_BANK_BRANCH_NAME,NEFT_BANK_ACCOUNT_TYPE, NEFT_BANK_IFSC_CODE from LP_APP_PAY_NEFT_SCRN_H where APPLICATION_ID=? AND AGENT_CD=?";

CommonService.transaction(db,function(tx){
        					CommonService.executeSql(tx, neftSql,[APP_ID, AGENT_CD], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCHOLDER||',res.rows.item(0).NEFT_ACCOUNT_HOLDER_NAME);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCNO||',res.rows.item(0).NEFT_BANK_ACCOUNT_NO);
                                 var bnkBranch=res.rows.item(0).NEFT_BANK_NAME+", "+res.rows.item(0).NEFT_BANK_BRANCH_NAME;
								if(!!bnkBranch){
									bnkBranch = CommonService.replaceAll(bnkBranch, "&", "&amp;");
									bnkBranch = CommonService.replaceAll(bnkBranch, "<", "&lt;");
									bnkBranch = CommonService.replaceAll(bnkBranch, ">", "&gt;");
									bnkBranch = CommonService.replaceAll(bnkBranch, '"', "&quot;");
								}
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_NAMEBRANCH||',bnkBranch);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCTYPE||',res.rows.item(0).NEFT_BANK_ACCOUNT_TYPE);
                                 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_IFSCCODE||',res.rows.item(0).NEFT_BANK_IFSC_CODE);
                                    dfd.resolve(genOp.HtmlFormOutput);
        						}
        						else
        						{
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCHOLDER||',"");
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCNO||',"");
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_NAMEBRANCH||',"");
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_ACCTYPE||',"");
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||MANDBANK_IFSCCODE||',"");
										dfd.resolve(genOp.HtmlFormOutput);
        						}
        					}, function(tx, err){console.log("Error 1");});
        					}, function(err){});
return dfd.promise;
}

this.printExtInsurance = function(){
console.log("inside Exist Ins");
var dfd = $q.defer();
var sql1 = "SELECT INS_OBJ_RISK,INS_OBJ_SAVINGS,INS_OBJ_CHILD_EDU,INS_OBJ_MARRIAGE,INS_OBJ_RETIREMENT,INS_OBJ_LEGACY_PLAN from LP_APPLICATION_MAIN where APPLICATION_ID=? AND AGENT_CD=?";
var sql2 = "SELECT TATAAIA_INS_FLAG,OTH_COMPANY_INS_FLAG from LP_APPLICATION_MAIN where APPLICATION_ID=? AND AGENT_CD=?";
var sql3 = "SELECT POLICY_NO,SUM_ASSURED from LP_APP_EXIST_INS_SCRN_D where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND TATA_OTHER_FLAG=? AND EXIST_INS_POL_FLAG=?";
var sql4 = "SELECT POLICY_NO,SUM_ASSURED from LP_APP_EXIST_INS_SCRN_D where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND TATA_OTHER_FLAG=? AND EXIST_INS_POL_FLAG=?";
var sql5 = "SELECT INSURANCE_PROVIDER,TYPE_OF_INSURANCE,ANNUAL_PREMIUM,SUM_ASSURED,DECISION,DECISION_DETAILS from LP_APP_EXIST_INS_SCRN_D where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND EXIST_INS_POL_FLAG=? AND TATA_OTHER_FLAG=?";
var sql6 = "SELECT INSURANCE_PROVIDER,TYPE_OF_INSURANCE,ANNUAL_PREMIUM,SUM_ASSURED,DECISION from LP_APP_EXIST_INS_SCRN_D where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND EXIST_INS_POL_FLAG=? AND TATA_OTHER_FLAG=?";

CommonService.transaction(db,function(tx){
        					CommonService.executeSql(tx, sql1,[APP_ID, AGENT_CD], function(tx,res){
        						console.log("Success ");
                            if(!!res && res.rows.length>0){
                              genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE1||',res.rows.item(0).INS_OBJ_RISK);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE2||',res.rows.item(0).INS_OBJ_SAVINGS);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE3||',res.rows.item(0).INS_OBJ_CHILD_EDU);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE4||',res.rows.item(0).INS_OBJ_MARRIAGE);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE5||',res.rows.item(0).INS_OBJ_RETIREMENT);
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE6||',res.rows.item(0).INS_OBJ_LEGACY_PLAN);
                                dfd.resolve(genOp.HtmlFormOutput);
                             }
                             else
                             {
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE1||',"");
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE2||',"");
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE3||',"");
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE4||',"");
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE5||',"");
                                genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PURPOSE6||',"");
                                dfd.resolve(genOp.HtmlFormOutput);
                             }
        					}, function(tx, err){console.log("Error 1");});

        					CommonService.executeSql(tx, sql2,[APP_ID, AGENT_CD], function(tx,res){
                                        console.log("Success ");
                                        if(!!res && res.rows.length>0){
                                         if(res.rows.item(0).TATAAIA_INS_FLAG=="Y")
                                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DOHAVEEXISTPOLICY||',"Yes");
                                             else
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DOHAVEEXISTPOLICY||',"No");

                                             if(res.rows.item(0).OTH_COMPANY_INS_FLAG=="Y")
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CURR_HOLD_INSURANCE||',"Yes");
                                             else
                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||CURR_HOLD_INSURANCE||',"No");
                                            dfd.resolve(genOp.HtmlFormOutput);
                                        }
                                    }, function(tx, err){console.log("Error 1");});
                            CommonService.executeSql(tx, sql3,[APP_ID, AGENT_CD,"C01","T","Y"], function(tx,res){
                                    console.log("Success ");
                                    if(!!res && res.rows.length>0){
                                     for(var i=0;i<res.rows.length;i++)
                                     {//alert("result leng:"+res.rows.length);

                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO'+(i+1)+'||',res.rows.item(i).POLICY_NO);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA'+(i+1)+'||',res.rows.item(i).SUM_ASSURED);

                                     }
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO2||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA2||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO3||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA3||',"");
                                        dfd.resolve(genOp.HtmlFormOutput);
                                     }
                                     else
                                     {
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO1||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA1||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO2||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA2||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_POLNO3||',"");
                                       genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_LIFE_SA3||',"");
                                        dfd.resolve(genOp.HtmlFormOutput);
                                     }
                                }, function(tx, err){console.log("Error 1");});
                            CommonService.executeSql(tx, sql4,[APP_ID, AGENT_CD,"C02","T","Y"], function(tx,res){
                                                    console.log("Success ");
                                                    if(!!res && res.rows.length>0)
                                                    {
                                                        for(var i=0;i<res.rows.length;i++)
                                                        {

                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO'+(i+1)+'||',res.rows.item(i).POLICY_NO);
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA'+(i+1)+'||',res.rows.item(i).SUM_ASSURED);

                                                        }
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO2||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA2||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO3||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA3||',"");

                                                        dfd.resolve(genOp.HtmlFormOutput);
                                                    }
                                                    else
                                                    {
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO1||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA1||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO2||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA2||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_POLNO3||',"");
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||EXISTPOLICY_PROP_SA3||',"");
                                                        dfd.resolve(genOp.HtmlFormOutput);
                                                    }
                                                }, function(tx, err){console.log("Error 1");});
                             CommonService.executeSql(tx, sql5,[APP_ID, AGENT_CD,"C01","Y","O"], function(tx,res){
                                                    console.log("Success ");
                                                    if(!!res && res.rows.length>0)
                                                    {
                                                     var noOfIns=res.rows.length;
                                                     for(var i=0;i<res.rows.length;i++)
                                                     {
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_INS_TYPE'+(i+1)+'||',res.rows.item(i).TYPE_OF_INSURANCE);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_CNAME'+(i+1)+'||',res.rows.item(i).INSURANCE_PROVIDER);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SA'+(i+1)+'||',res.rows.item(i).SUM_ASSURED);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_ANNPREM'+(i+1)+'||',res.rows.item(i).ANNUAL_PREMIUM);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_DCSN'+(i+1)+'||',res.rows.item(i).DECISION);
                                                     if(res.rows.item(i).DECISION_DETAILS==null || res.rows.item(i).DECISION_DETAILS=="")
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DECISION_OTHER_THAN_STD_DET||',"");
                                                     else
                                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DECISION_OTHER_THAN_STD_DET||',res.rows.item(i).DECISION_DETAILS);

                                                     }

                                                     for(var j=noOfIns;j<3;j++)
                                                     {
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_INS_TYPE'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_CNAME'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SA'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_ANNPREM'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_DCSN'+(j+1)+'||',"");

                                                     }
                                                        dfd.resolve(genOp.HtmlFormOutput);

                                                     }
                                                     else
                                                     {
                                                         for(var i=0;i<3;i++)
                                                         {
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_INS_TYPE'+(i+1)+'||',"");
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_CNAME'+(i+1)+'||',"");
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SA'+(i+1)+'||',"");
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_ANNPREM'+(i+1)+'||',"");
                                                             genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_DCSN'+(i+1)+'||',"");
                                                         }
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DECISION_OTHER_THAN_STD_DET||',"");
                                                        dfd.resolve(genOp.HtmlFormOutput);
                                                     }
                                                }, function(tx, err){console.log("Error 1");});
                            CommonService.executeSql(tx, sql6,[APP_ID, AGENT_CD,"C02","Y","O"], function(tx,res){
                                                console.log("Success ");
                                                 if(!!res && res.rows.length>0)
                                                 {
                                                     var noOfIns=res.rows.length;
                                                     for(var i=0;i<res.rows.length;i++)
                                                     {
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_INS_TYPE'+(i+1)+'||',res.rows.item(i).TYPE_OF_INSURANCE);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_CNAME'+(i+1)+'||',res.rows.item(i).INSURANCE_PROVIDER);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SA'+(i+1)+'||',res.rows.item(i).SUM_ASSURED);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_ANNPREM'+(i+1)+'||',res.rows.item(i).ANNUAL_PREMIUM);
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_DCSN'+(i+1)+'||',res.rows.item(i).DECISION);
                                                     }

                                                     for(var j=noOfIns;j<3;j++)
                                                     {
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_INS_TYPE'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_CNAME'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SA'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_ANNPREM'+(j+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_DCSN'+(j+1)+'||',"");

                                                     }
                                                     dfd.resolve(genOp.HtmlFormOutput);
                                                 }
                                                 else
                                                 {
                                                     for(var i=0;i<3;i++)
                                                     {
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_INS_TYPE'+(i+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_CNAME'+(i+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SA'+(i+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_ANNPREM'+(i+1)+'||',"");
                                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_DCSN'+(i+1)+'||',"");
                                                     }
                                                     dfd.resolve(genOp.HtmlFormOutput);
                                                 }
                                            }, function(tx, err){console.log("Error 1");});
        					}, function(err){});
return dfd.promise;
}

this.getLSAnswer = function(i, tx, sql){
CommonService.executeSql(tx, sql,[APP_ID, AGENT_CD, 'C01'], function(tx,res){
        						debug("Success getLSAnswer");
        						if(!!res && res.rows.length>0){
									  if(res.rows.item(0).ANSWAR_FLAG=="Y")
										genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_QUEST'+(i+1)+'||',"Yes");
									  else
										genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_QUEST'+(i+1)+'||',"No");
                                }
                                 else
                                   genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_QUEST'+(i+1)+'||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql,[APP_ID, AGENT_CD, 'C02'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
        						//ganesh removed d,
								debug("in prop LS");
								  if(res.rows.item(0).ANSWAR_FLAG=="Y")
								  	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_QUEST'+(i+1)+'||',"Yes");
								  else
								  	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_QUEST'+(i+1)+'||',"No");
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_QUEST'+(i+1)+'||',"");
        					}, function(tx, err){console.log("Error 1");});

}

this.printLSData = function(){
var dfd = $q.defer();
CommonService.transaction(db,function(tx){
									for(var i=0;i<8;i++){
										var LSSql = "select * from LP_APP_LS_ANS_SCRN_B where QUESTION_SEQUENCE='"+(i+1)+"' AND APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?";
										genOp.getLSAnswer(i,tx,LSSql);
									}
									dfd.resolve("success");

        					}, function(err){});
return dfd.promise;
}

this.printLSSubQuestion = function(){
console.log("inside printLSSubQuestion :: ");
var dfd = $q.defer();
var sql1 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql2 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql3 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql4 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql5 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql6 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql7 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql8 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql9 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql10 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql11 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
var sql12 = "select ANSWAR_DTLS1,ANSWAR_DTLS2,ANSWAR_DTLS3,ANSWAR_DTLS4,ANSWAR_DTLS5,ANSWAR_DTLS6 from LP_APP_LS_ANS_SCRN_B where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND QUESTION_ID=?";
CommonService.transaction(db,function(tx){

CommonService.executeSql(tx, sql1,[APP_ID, AGENT_CD, 'C01', '6'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_CONVICTCRIMINAL_DET||',"Criminal Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_CONVICTCRIMINAL_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql2,[APP_ID, AGENT_CD, 'C02', '6'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_CONVICTCRIMINAL_DET||',"Criminal Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_CONVICTCRIMINAL_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql3,[APP_ID, AGENT_CD, 'C01', '8'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_LIVETRAVELOUTSIDE_DET||',"Travel Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_LIVETRAVELOUTSIDE_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql4,[APP_ID, AGENT_CD, 'C02', '8'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_LIVETRAVELOUTSIDE_DET||',"Travel Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_LIVETRAVELOUTSIDE_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql5,[APP_ID, AGENT_CD, 'C01', '10'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_ALCOHOL_DET||',"Alcohol Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_ALCOHOL_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql6,[APP_ID, AGENT_CD, 'C02', '10'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
																		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_ALCOHOL_DET||',"Alcohol Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_ALCOHOL_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql7,[APP_ID, AGENT_CD, 'C01', '12'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SMOKE_DET||',"Smoking Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_SMOKE_DET||',"");
        					}, function(tx, err){console.log("Error 1");});

CommonService.executeSql(tx, sql8,[APP_ID, AGENT_CD, 'C02', '12'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SMOKE_DET||',"Smoking Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_SMOKE_DET||',"");
        					}, function(tx, err){console.log("Error 1");});

CommonService.executeSql(tx, sql9,[APP_ID, AGENT_CD, 'C01', '14'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_TOBACCO_DET||',"Tobacco Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_TOBACCO_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql10,[APP_ID, AGENT_CD, 'C02', '14'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_TOBACCO_DET||',"Tobacco Details :"+ans);
        						}
        						else
        							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_TOBACCO_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql11,[APP_ID, AGENT_CD, 'C01', '16'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
									var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                    genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_DRUGS_DET||',"Drug Details :"+ans);
        						}
								else
									genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_DRUGS_DET||',"");
        					}, function(tx, err){console.log("Error 1");});
CommonService.executeSql(tx, sql12,[APP_ID, AGENT_CD, 'C02', '16'], function(tx,res){
        						console.log("Success ");
        						if(!!res && res.rows.length>0){
        							var result=res.rows.item(0);
                                    var ans=result.ANSWAR_DTLS1+" "+result.ANSWAR_DTLS2+" "+result.ANSWAR_DTLS3;
																		if(!!ans){
																			ans = CommonService.replaceAll(ans, "&", "&amp;");
																		  ans = CommonService.replaceAll(ans, "<", "&lt;");
																		  ans = CommonService.replaceAll(ans, ">", "&gt;");
																		  ans = CommonService.replaceAll(ans, '"', "&quot;");
																		}
                                     genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_DRUGS_DET||',"Drug Details :"+ans);
										dfd.resolve(genOp.HtmlFormOutput);
								}
        						else
                                    {
										genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_DRUGS_DET||',"");
										dfd.resolve(genOp.HtmlFormOutput);
        							}
        					}, function(tx, err){console.log("Error 1");});
        					},function(err){console.log("Error 1");})

  return dfd.promise;
}

this.getHDAnswer1_8 = function(j,tx,sql){
if(j==18){
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C01"],function(tx,res){
							if(res.rows.length!=0)
                            {
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_WT||',res.rows.item(0).ANSWAR_DTLS1);
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HT||',res.rows.item(0).ANSWAR_DTLS2);
                            }
							},function(tx,err){console.log("Error::");});
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C02"],function(tx,res){
							if(res.rows.length!=0)
                            {
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HT||',res.rows.item(0).ANSWAR_DTLS1);
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_WT||',res.rows.item(0).ANSWAR_DTLS2);
                            }
                            else{
                            genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HT||',"");
                                                           genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_WT||',"");
                            }
							},function(tx,err){console.log("Error::");});
}
else{
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C01"],function(tx,res){
console.log("length of que17::"+res.rows.length);
							if(res.rows.length!=0)
                            {
                               if(res.rows.item(0).ANSWAR_FLAG=="Y")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+j+'||',"Yes");
                               else
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+j+'||',"No");
                            }
                            else
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+j+'||',"");
							},function(tx,err){console.log("Error::");});
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C02"],function(tx,res){
							if(res.rows.length!=0)
                            {
                               if(res.rows.item(0).ANSWAR_FLAG=="Y")
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+j+'||',"Yes");
                               else
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+j+'||',"No");
                            }
                            else
                               genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+j+'||',"");
							},function(tx,err){console.log("Error::");});
}
}

this.getHDQuestionnaire1_8 = function(){
var dfd = $q.defer();
CommonService.transaction(db,function(tx){
                                                            // tx.executeSql("",[],function(){},function(){});
			var j=16;
			for(var i=0;i<8;i++)
			{
				if(i==1)
				{//alert("here"+(j+1));
					sql1="SELECT ANSWAR_DTLS1,ANSWAR_DTLS2 from LP_APP_HD_ANS_SCRN_C where QUESTION_ID='"+(j+1)+"' AND APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?";
					j=j+1;
					genOp.getHDAnswer1_8(j,tx,sql1);
				}
				else
				{
				sql1="SELECT ANSWAR_FLAG from LP_APP_HD_ANS_SCRN_C where QUESTION_ID='"+(j+1)+"' AND APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?";
				j=j+1;
				genOp.getHDAnswer1_8(j,tx,sql1);
				}
			}
			dfd.resolve("success");
 }, function(err){});
return dfd.promise;

}

this.getHDAnswer9 = function(i,tx,sql){
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C01"],function(tx,res){
						if(res.rows.length!=0 && res.rows.item(0).ANSWAR_FLAG!="" && res.rows.item(0).ANSWAR_FLAG!=null)
                        {
                        	if(res.rows.item(0).ANSWAR_FLAG=="Y")
                        		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"Yes");
                        	else
                           		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"No");
                        }
                        else
                        	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"");
						},function(tx,err){console.log("Error::")});
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C02"],function(tx,res){
						if(res.rows.length!=0 && res.rows.item(0).ANSWAR_FLAG!="" && res.rows.item(0).ANSWAR_FLAG!=null)
                        {
                        	if(res.rows.item(0).ANSWAR_FLAG=="Y")
                        		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"Yes");
                        	else
                           		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"No");
                        }
                        else
                        	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"");
						},function(tx,err){console.log("Error::")});
}

this.getHDAnswer12 = function(i,tx,sql){
console.log("Valuof i is::"+i);
CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C01"],function(tx,res){
						console.log("res.rows.length::"+res.rows.length);
						if(!!res && res.rows.length>0)
						{
							if(i==48)
              {
              	if(res.rows.item(0).ANSWAR_DTLS1!=undefined && res.rows.item(0).ANSWAR_DTLS1!="")
                  {
                  	var det = res.rows.item(0).ANSWAR_DTLS1;
                      det = det.substring(0,10);
                      genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_INS||',det+" ");
                  }
              	else
                  	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_INS||',"");
              }
							if(i == 52)
							{
										var PhyDet = res.rows.item(0).ANSWAR_DTLS1+", "+res.rows.item(0).ANSWAR_DTLS2;
										console.log("Physical details::"+res.rows.item(0).ANSWAR_DTLS1+"Second is::"+res.rows.item(0).ANSWAR_DTLS2+"PhyDet::"+PhyDet);
										if(!!res.rows.item(0).ANSWAR_DTLS1 && res.rows.item(0).ANSWAR_DTLS2)
										{
											if(!!PhyDet){
												PhyDet = CommonService.replaceAll(PhyDet, "&", "&amp;");
												PhyDet = CommonService.replaceAll(PhyDet, "<", "&lt;");
												PhyDet = CommonService.replaceAll(PhyDet, ">", "&gt;");
												PhyDet = CommonService.replaceAll(PhyDet, '"', "&quot;");
											}
											genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANDETAILS||',PhyDet);
										}
										else
											genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANDETAILS||',"NA");
										if(!!res.rows.item(0).ANSWAR_DTLS3)
											genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANTELNO||',res.rows.item(0).ANSWAR_DTLS3);
										else
											genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANTELNO||',"NA");
							}

							else
							{
							console.log("AnswerFlag::"+res.rows.item(0).ANSWAR_FLAG);
								if(res.rows.item(0).ANSWAR_FLAG=="Y")
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"Yes");
								else
								genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"No");

							}
						}
						else
						{
							if(i==48)
              genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_INS||',"");
              else
              genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||LIFE_HD_QUEST'+i+'||',"");
						}
						},function(tx,err){console.log("Error::");});

CommonService.executeSql(tx,sql,[APP_ID,AGENT_CD,"C02"],function(tx,res){
						if(!!res && res.rows.length>0){
							if(i==48)
							{
              	if(res.rows.item(0).ANSWAR_DTLS1!=undefined && res.rows.item(0).ANSWAR_DTLS1!="")
                  {
                  	var det = res.rows.item(0).ANSWAR_DTLS1;
                      det = det.substring(0,10);
                      genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_PRO||',det+" ");
                  }
              	else
                  	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_PRO||',"");
              }//genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE||',res.rows.item(0).ANSWAR_DTLS1);
            else
								{
            			if(res.rows.item(0).ANSWAR_FLAG=="Y")
                		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"Yes");
                	else
                		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"No");
               	}
						}
						else
						{
							if(i==48)
              	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DELIVERYDATE_PRO||',"");
              else
              	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PROP_HD_QUEST'+i+'||',"");
						}
						},function(tx,err){console.log("Error::");});

}

this.getHDQuestionnaire9 = function(){
var dfd = $q.defer();
CommonService.transaction(db,function(tx){
						for(var i=25;i<=46;i++)
						{
						var sql="SELECT ANSWAR_FLAG from LP_APP_HD_ANS_SCRN_C where QUESTION_ID='"+i+"' AND APPLICATION_ID=? AND AGENT_CD=? 								 AND CUST_TYPE=?";
                                 genOp.getHDAnswer9(i,tx,sql);
						}
						dfd.resolve(genOp.HtmlFormOutput);
						},function(err){console.log("Error::");
						});
		return dfd.promise;
}

this.getFemaleQuestionnaire = function(){
var dfd = $q.defer();
CommonService.transaction(db,function(tx){
                         //FATCA
                      /* if(PGL_ID!="222")
                       {
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANDETAILS||',"");
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||PHYSIANTELNO||',"");
                       }*/

						for(var i=47;i<=57;i++)
						{
							/*if(i==48)
							{
								var sql="SELECT ANSWAR_DTLS1 from LP_APP_HD_ANS_SCRN_C where QUESTION_ID='"+i+"' AND APPLICATION_ID=? AND 									     AGENT_CD=? AND CUST_TYPE=?";
                                genOp.getHDAnswer12(i,tx,sql);
							}
							else
                            {*/
                            	var sql="SELECT * from LP_APP_HD_ANS_SCRN_C where QUESTION_ID='"+i+"' AND APPLICATION_ID=? AND 										 AGENT_CD=? AND CUST_TYPE=?";
                            	console.log("Before func called::"+i);
                                genOp.getHDAnswer12(i,tx,sql);

                            //}
						}
						dfd.resolve(genOp.HtmlFormOutput);
						},function(err){console.log("Error::")
						});
		return dfd.promise;
}

this.getFH_History = function(){
var dfd = $q.defer();
var sql1 = "select FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,BIRTH_DATE,OCCUPATION_DESC,ANNUAL_INCOME,INSURANCE_DETAILS from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql2 = "select FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,BIRTH_DATE,OCCUPATION_DESC,ANNUAL_INCOME,INSURANCE_DETAILS from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql3 = "select FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,BIRTH_DATE,OCCUPATION_DESC,ANNUAL_INCOME,INSURANCE_DETAILS from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql4 = "select ALIVE_OR_DECEASED,HEALTH_STATUS_DECEASED_REASON,DEATH_AGE from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql5 = "select ALIVE_OR_DECEASED,HEALTH_STATUS_DECEASED_REASON,DEATH_AGE from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql6 = "select ALIVE_OR_DECEASED,HEALTH_STATUS_DECEASED_REASON,DEATH_AGE from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql7 = "select ALIVE_OR_DECEASED,HEALTH_STATUS_DECEASED_REASON,DEATH_AGE from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql8 = "select ALIVE_OR_DECEASED,HEALTH_STATUS_DECEASED_REASON,DEATH_AGE from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID=?";
var sql9 = "select INSURANCE_BACK_DATE,APPLICATION_DATE,APPLICATION_PLACE from LP_APPLICATION_MAIN where APPLICATION_ID=? AND AGENT_CD=?";
CommonService.transaction(db,function(tx){
			CommonService.executeSql(tx,sql1,[APP_ID,AGENT_CD,"C11"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{
            							var name;
										if(res.rows.item(0).MIDDLE_NAME == undefined)
										name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).LAST_NAME;
										else
										name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).MIDDLE_NAME+" "+res.rows.item(0).LAST_NAME;
            							//var name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).MIDDLE_NAME+" "+res.rows.item(0).LAST_NAME;
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERNAME||',name);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERGENDER||',res.rows.item(0).GENDER);
                                    	var dob=res.rows.item(0).BIRTH_DATE;
                                    	dob=dob.substring(0,10);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERDOB||',dob);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHEROCC||',res.rows.item(0).OCCUPATION_DESC);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERANNINC||',res.rows.item(0).ANNUAL_INCOME);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERINSDETAIL||',res.rows.item(0).INSURANCE_DETAILS);
                                    	//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERNAME||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERGENDER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERDOB||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHEROCC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERANNINC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FATHERINSDETAIL||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});
            CommonService.executeSql(tx,sql2,[APP_ID,AGENT_CD,"C12"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{	var name;
            							if(res.rows.item(0).MIDDLE_NAME == undefined)
											name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).LAST_NAME;
										else
											name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).MIDDLE_NAME+" "+res.rows.item(0).LAST_NAME;
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDNAME||',name);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDGENDER||',res.rows.item(0).GENDER);
                                    	var dob=res.rows.item(0).BIRTH_DATE;
                                    	dob=dob.substring(0,10);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDDOB||',dob);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDOCC||',res.rows.item(0).OCCUPATION_DESC);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDANNINC||',res.rows.item(0).ANNUAL_INCOME);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDINSDETAIL||',res.rows.item(0).INSURANCE_DETAILS);
                                    	//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDNAME||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDGENDER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDDOB||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDOCC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDANNINC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDINSDETAIL||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::")});
            CommonService.executeSql(tx,sql3,[APP_ID,AGENT_CD,"C13"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{
            							var name;
										if(res.rows.item(0).MIDDLE_NAME == undefined)
										name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).LAST_NAME;
										else
										name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).MIDDLE_NAME+" "+res.rows.item(0).LAST_NAME;
            							//var name=res.rows.item(0).FIRST_NAME+" "+res.rows.item(0).MIDDLE_NAME+" "+res.rows.item(0).LAST_NAME;
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDNAME||',name);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDGENDER||',res.rows.item(0).GENDER);
                                    	var dob=res.rows.item(0).BIRTH_DATE;
                                    	dob=dob.substring(0,10);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDDOB||',dob);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDOCC||',res.rows.item(0).OCCUPATION_DESC);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDANNINC||',res.rows.item(0).ANNUAL_INCOME);
                                    	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||FIRSTCHILDINSDETAIL||',res.rows.item(0).INSURANCE_DETAILS);
                                    	//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDNAME||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDGENDER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDDOB||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDOCC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDANNINC||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SECCHILDINSDETAIL||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});
            CommonService.executeSql(tx,sql4,[APP_ID,AGENT_CD,"96"],function(tx, res){

            						if(!!res && res.rows.length>0)
            						{console.log("REL : 96"+JSON.stringify(res.rows.item(0)))
            						     if(res.rows.item(0).ALIVE_OR_DECEASED=="L")
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_FATHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         else
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_FATHER||',"NA");

                                         if(res.rows.item(0).ALIVE_OR_DECEASED=="D")
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_FATHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_FATHER||',res.rows.item(0).DEATH_AGE);
                                         }
                                         else
                                         {
											 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_FATHER||',"NA");
											 genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_FATHER||',"NA");
                                         }
                                         //dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_FATHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_FATHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_FATHER||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});
            CommonService.executeSql(tx,sql5,[APP_ID,AGENT_CD,"97"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{console.log("REL : 97"+JSON.stringify(res.rows.item(0)))
            						     if(res.rows.item(0).ALIVE_OR_DECEASED=="L")
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_MOTHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         else
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_MOTHER||',"NA");

                                         if(res.rows.item(0).ALIVE_OR_DECEASED=="D")
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_MOTHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_MOTHER||',res.rows.item(0).DEATH_AGE);
                                         }
                                         else
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_MOTHER||',"NA");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_MOTHER||',"NA");
                                         }
                                         //dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_MOTHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_MOTHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_MOTHER||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});
            CommonService.executeSql(tx,sql6,[APP_ID,AGENT_CD,"99"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{console.log("REL : 99"+JSON.stringify(res.rows.item(0)))
            						     if(res.rows.item(0).ALIVE_OR_DECEASED=="L")
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_BROTHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         else
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_BROTHER||',"NA");

                                         if(res.rows.item(0).ALIVE_OR_DECEASED=="D")
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_BROTHER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_BROTHER||',res.rows.item(0).DEATH_AGE);
                                         }
                                         else
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_BROTHER||',"NA");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_BROTHER||',"NA");
                                         }
                                         //dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_BROTHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_BROTHER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_BROTHER||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::")});
            CommonService.executeSql(tx,sql7,[APP_ID,AGENT_CD,"98"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{console.log("REL : 98"+JSON.stringify(res.rows.item(0)))
            						     if(res.rows.item(0).ALIVE_OR_DECEASED=="L")
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SISTER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         else
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SISTER||',"NA");

                                         if(res.rows.item(0).ALIVE_OR_DECEASED=="D")
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SISTER||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SISTER||',res.rows.item(0).DEATH_AGE);
                                         }
                                         else
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SISTER||',"NA");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SISTER||',"NA");
                                         }
                                         //dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SISTER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SISTER||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SISTER||',"NA");
            							//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});
            CommonService.executeSql(tx,sql8,[APP_ID,AGENT_CD,"95"],function(tx, res){
            						if(!!res && res.rows.length>0)
            						{console.log("REL : 95"+JSON.stringify(res.rows.item(0)))
            						     if(res.rows.item(0).ALIVE_OR_DECEASED=="L")
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SPOUSE||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         else
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SPOUSE||',"NA");

                                         if(res.rows.item(0).ALIVE_OR_DECEASED=="D")
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SPOUSE||',res.rows.item(0).HEALTH_STATUS_DECEASED_REASON);
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SPOUSE||',res.rows.item(0).DEATH_AGE);
                                         }
                                         else
                                         {
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SPOUSE||',"NA");
                                         genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SPOUSE||',"NA");
                                         }
                                        // dfd.resolve(genOp.HtmlFormOutput);
            						}
            						else
            						{
            							genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||STATUS_SPOUSE||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||DEATH_SPOUSE||',"NA");
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||AGE_DEATH_SPOUSE||',"NA");
										//dfd.resolve(genOp.HtmlFormOutput);
            						}
            						},function(error){console.log("Error::");});

            CommonService.executeSql(tx,sql9,[APP_ID,AGENT_CD],function(tx,res){
            						if(res.rows.length!=0)
                                    {
                                    	if(res.rows.item(0).INSURANCE_BACK_DATE!=null && res.rows.item(0).INSURANCE_BACK_DATE!="")
                                        {
                                        var dt=res.rows.item(0).INSURANCE_BACK_DATE;
                                        dt=dt.substring(0,10);
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BACKDATE||',dt);
                                        }
                                        else
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||BACKDATE||',"");

                                       /* if(res.rows.item(0).APPLICATION_DATE!=null && res.rows.item(0).APPLICATION_DATE!="")
                                        {*/
											var appDt = CommonService.getCurrDate();
											appDt=appDt.substring(0,10);
											genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APPDATE||',appDt);
                                       /* }
                                        else
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APPDATE||',"");*/
																				if(!!res.rows.item(0).APPLICATION_PLACE){
																					res.rows.item(0).APPLICATION_PLACE = CommonService.replaceAll(res.rows.item(0).APPLICATION_PLACE, "&", "&amp;");
																					res.rows.item(0).APPLICATION_PLACE = CommonService.replaceAll(res.rows.item(0).APPLICATION_PLACE, "<", "&lt;");
																					res.rows.item(0).APPLICATION_PLACE = CommonService.replaceAll(res.rows.item(0).APPLICATION_PLACE, ">", "&gt;");
																					res.rows.item(0).APPLICATION_PLACE = CommonService.replaceAll(res.rows.item(0).APPLICATION_PLACE, '"', "&quot;");
																				}
                                        genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||PLACE||',res.rows.item(0).APPLICATION_PLACE);
										dfd.resolve(genOp.HtmlFormOutput);

//											CommonService.getCurrDateUTC().then(function(responce){
//												debug("getCurrDateUTC responce : "+CommonService.getCurrDate())
//												if(!!responce){
//													genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||APPDATE||',responce);
//												}
//												dfd.resolve(genOp.HtmlFormOutput);
//
//											});
                                    }
                                    else
                                    	dfd.resolve(genOp.HtmlFormOutput);
            						},function(err){console.log("Error::");});
						},function(err){console.log("Error::getFH_History");
						});

		return dfd.promise;
}

this.HDSubQuestionDetails = function(){
var dfd = $q.defer();
var sql1 = "select QUESTION_SEQUENCE,ANSWAR_DTLS1 from LP_APP_HD_ANS_SCRN_C13 where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND SUB_QUESTION_ID IN ('1','2') ORDER BY SUB_QUESTION_ID ASC";
var sql2 = "select QUESTION_SEQUENCE,ANSWAR_DTLS1 from LP_APP_HD_ANS_SCRN_C13 where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND SUB_QUESTION_ID NOT IN ('1','2','25','26','27') ORDER BY SUB_QUESTION_ID ASC";
var sql3 = "select APPC.QUESTION_SEQUENCE AS SEQ,APPC.ANSWAR_DTLS1 AS DTLS,APPSUB.SUB_QUES_PARAM AS PARAM from LP_APP_HD_ANS_SCRN_C13  AS APPC JOIN LP_APP_SUB_QUESTION_MASTER AS APPSUB ON (APPC.SUB_QUESTION_ID=APPSUB.SUB_QUESTION_ID) where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? ORDER BY APPC.SUB_QUESTION_ID ASC";
var sql4 = "select QUESTION_SEQUENCE,ANSWAR_DTLS1 from LP_APP_HD_ANS_SCRN_C13 where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND SUB_QUESTION_ID IN ('1','2') ORDER BY SUB_QUESTION_ID ASC";
var sql5 = "select QUESTION_SEQUENCE,ANSWAR_DTLS1 from LP_APP_HD_ANS_SCRN_C13 where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND SUB_QUESTION_ID NOT IN ('1','2','25','26','27') ORDER BY SUB_QUESTION_ID ASC";
var sql6 = "select APPC.QUESTION_SEQUENCE AS SEQ,APPC.ANSWAR_DTLS1 AS DTLS,APPSUB.SUB_QUES_PARAM AS PARAM from LP_APP_HD_ANS_SCRN_C13  AS APPC JOIN LP_APP_SUB_QUESTION_MASTER AS APPSUB ON (APPC.SUB_QUESTION_ID=APPSUB.SUB_QUESTION_ID) where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? ORDER BY APPC.SUB_QUESTION_ID ASC";
var sql7 = "select APPC.QUESTION_SEQUENCE AS SEQ,APPC.ANSWAR_DTLS1 AS DTLS,APPSUB.SUB_QUES_PARAM AS PARAM from LP_APP_HD_ANS_SCRN_C13  AS APPC JOIN LP_APP_SUB_QUESTION_MASTER AS APPSUB ON (APPC.SUB_QUESTION_ID=APPSUB.SUB_QUESTION_ID) where APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=? AND APPSUB.SUB_QUESTION_ID IN('25','26','27') ORDER BY APPC.SUB_QUESTION_ID ASC"
var InsDet = "";
var ProDet = "";
var InsDetVCP = "";
var ProDetVCP = "";

CommonService.transaction(db,function(tx){

CommonService.executeSql(tx,sql1,[APP_ID,AGENT_CD,"C01"],function(tx,res){
						if(res.rows.length!=0)
                        {
                        	InsDet=InsDet+""+res.rows.item(0).QUESTION_SEQUENCE+" "+res.rows.item(0).ANSWAR_DTLS1+", "+res.rows.item(1).ANSWAR_DTLS1+" </br>";
                        	CommonService.executeSql(tx,sql2,[APP_ID,AGENT_CD,"C01"],function(tx,res){
								if(res.rows.length!=0)
                            	{
                            	for(var i=0;i<res.rows.length;i++)
                            	InsDet=InsDet+""+res.rows.item(i).QUESTION_SEQUENCE+" "+res.rows.item(i).ANSWAR_DTLS1+" </br>";
                            	}
                        		},function(err){console.log("Error::");});

                        		dfd.resolve(genOp.HtmlFormOutput);
						}
						else
						{
							CommonService.executeSql(tx,sql3,[APP_ID,AGENT_CD,"C01"],function(tx,res){
								if(res.rows.length!=0)
                                {
                                for(var i=0;i<res.rows.length;i++)
                                {
                                if(res.rows.item(i).PARAM!=null)
                                InsDet=InsDet+""+res.rows.item(i).SEQ+" "+res.rows.item(i).PARAM+" - "+res.rows.item(i).DTLS+" </br>";
                                else
                                InsDet=InsDet+""+res.rows.item(i).SEQ+" "+res.rows.item(i).DTLS+" </br>";
                                }
                                }
								},function(err){console.log("Error::");});
								dfd.resolve(genOp.HtmlFormOutput);
						}
						},function(err){console.log("Error::");});
CommonService.executeSql(tx,sql4,[APP_ID,AGENT_CD,"C02"],function(tx,res){
						if(res.rows.length!=0)
                        {
                        	ProDet=ProDet+""+res.rows.item(0).QUESTION_SEQUENCE+" "+res.rows.item(0).ANSWAR_DTLS1+", "+res.rows.item(1).ANSWAR_DTLS1+" </br>";
                        	CommonService.executeSql(tx,sql5,[APP_ID,AGENT_CD,"C02"],function(tx,res){
                        		if(res.rows.length!=0)
                            	{
                            	for(var i=0;i<res.rows.length;i++)
                            	ProDet=ProDet+""+res.rows.item(i).QUESTION_SEQUENCE+" "+res.rows.item(i).ANSWAR_DTLS1+" </br>";
                            	}
                        		},function(err){console.log("Error");});

                        	dfd.resolve(genOp.HtmlFormOutput);
                        }
                        else
                        {
                        	CommonService.executeSql(tx,sql6,[APP_ID,AGENT_CD,"C02"],function(tx,res){
                        		if(res.rows.length!=0)
                                {
                                for(var i=0;i<res.rows.length;i++)
                                {
                                if(res.rows.item(i).PARAM!=null)
                                ProDet=ProDet+""+res.rows.item(i).SEQ+" "+res.rows.item(i).PARAM+" - "+res.rows.item(i).DTLS+" </br>";
                                else
                                ProDet=ProDet+""+res.rows.item(i).SEQ+" "+res.rows.item(i).DTLS+" </br>";
                                }
                                }
                        		},function(err){console.log("Error");});
                        		dfd.resolve(genOp.HtmlFormOutput);
                        }
						},function(err){console.log("Error");});
CommonService.executeSql(tx,sql7,[APP_ID,AGENT_CD,"C01"],function(tx,res){
							if(res.rows.length!=0){
							console.log(" InsDetVCP :"+res.rows.length);
							for(var i=0;i<res.rows.length;i++){
							InsDetVCP=InsDetVCP+""+res.rows.item(i).SEQ+" "+res.rows.item(i).DTLS+" <br/>";
							console.log("value of InsDetVCP :"+InsDetVCP);
							}
							}
						},function(err){console.log("Error");});
CommonService.executeSql(tx,sql7,[APP_ID,AGENT_CD,"C02"],function(tx,res){
							if(res.rows.length!=0){
							console.log(" ProDetVCP :"+res.rows.length);
							for(var i=0;i<res.rows.length;i++){
							ProDetVCP=ProDetVCP+""+res.rows.item(i).SEQ+" "+res.rows.item(i).DTLS+" <br/>";
							console.log("value of ProDetVCP :"+ProDetVCP);
							}
							}
						},function(err){console.log("Error");});

						},function(err){console.log("Error::")
						},function(){
						//Change to VCP PGLID
						/*if(PGL_ID=="222"){
						console.log("ID is::"+PGL_ID+"Length ::"+InsDetVCP.length);
						   if(InsDetVCP.length!=0)
							genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_INS||',"Insured : "+InsDetVCP);
						   else
							genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_INS||',InsDetVCP);

						   if(ProDetVCP.length!=0)
							genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_PRO||',"Proposer : "+ProDetVCP);
						   else
							genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_PRO||',ProDetVCP);
                        }
                        else //FATCA
                       {
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_INS||',"");
                            genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_PRO||',"");
                       }*/
							console.log("ID is::"+PGL_ID+"Length ::"+InsDetVCP.length);
                            if(InsDetVCP.length!=0)
                                genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_INS||',"Insured : "+InsDetVCP);
                            else
                                genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_INS||',InsDetVCP);

                            if(ProDetVCP.length!=0)
                                genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_PRO||',"Proposer : "+ProDetVCP);
                            else
                                genOp.HtmlFormOutput=CommonService.replaceAll(genOp.HtmlFormOutput,'||VCP_DET_PRO||',ProDetVCP);

																if(InsDet.length!=0)
																	{
																		if(!!InsDet){
																			InsDet = CommonService.replaceAll(InsDet, "&", "&amp;");
																			InsDet = CommonService.replaceAll(InsDet, "<", "&lt;");
																			InsDet = CommonService.replaceAll(InsDet, ">", "&gt;");
																			InsDet = CommonService.replaceAll(InsDet, '"', "&quot;");
																		}
																		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SUB_DET_INS||',"Insured : "+InsDet);
																	}
																else
																	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SUB_DET_INS||',InsDet);

																if(ProDet.length!=0)
																	{
																		if(!!ProDet){
																			ProDet = CommonService.replaceAll(ProDet, "&", "&amp;");
																			ProDet = CommonService.replaceAll(ProDet, "<", "&lt;");
																			ProDet = CommonService.replaceAll(ProDet, ">", "&gt;");
																			ProDet = CommonService.replaceAll(ProDet, '"', "&quot;");
																		}
																		genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SUB_DET_PRO||',"Proposer : "+ProDet);
																}
																else
																	genOp.HtmlFormOutput = CommonService.replaceAll(genOp.HtmlFormOutput,'||SUB_DET_PRO||',ProDet);

					});

	return dfd.promise;
}
}]);
