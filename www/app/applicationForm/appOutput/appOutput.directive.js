genOutputModule.directive('viewAppOutput',['ApplicationFormDataService', function (ApplicationFormDataService) {
	"use strict";
    return {
		template: function(tElement, tAttrs) {
					var appData = ApplicationFormDataService.APP_OUTPUT;
					//console.log("inside directive :: " + ((!!appData)?(appData.length):appData));
				    return appData;
				}
	};
}]);
