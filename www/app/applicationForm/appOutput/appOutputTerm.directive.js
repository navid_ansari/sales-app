genOutputModule.directive('viewTermAppOutput',['ApplicationFormDataService', function (ApplicationFormDataService) {
	"use strict";
    return {
		template: function(tElement, tAttrs) {
					var appData = ApplicationFormDataService.APP_TERM_OUTPUT;
					//console.log("inside directive ::"+appData);
				    return appData;
				}
	};
}]);