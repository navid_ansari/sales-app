appVernacularModule.controller('VernacularCtrl',['$state','$stateParams','SISFormData','CommonService','VernacularService','LoadAppProofList','IDProofList','$filter', function($state, $stateParams,SISFormData,CommonService,VernacularService,LoadAppProofList,IDProofList, $filter){

var vern = this;
vern.click = false;
vern.witnessBean = {};

console.log("inside VernacularCtrl");
console.log("data of list:" + JSON.stringify(IDProofList));
console.log("VernacularService for sisdata "+JSON.stringify(SISFormData));
console.log("VernacularService for custType :" + JSON.stringify(SISFormData.sisFormAData_eKyc.INSURANCE_BUYING_FOR));
CommonService.hideLoading();
                                                 
    /* Header Hight Calculator */
    setTimeout(function(){
        var appOutputGetHeight = $(".customer-details .fixed-bar").height();
        $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    });
    /* End Header Hight Calculator */
                                                 
this.verProofList = IDProofList;
vern.witnessBean.idProofType=this.verProofList[0];
console.log("setting data"+ JSON.stringify(vern.witnessBean.MPPROOF_DESC));
vern.alphaNumSpaceRegex = ALPHA_NUM_WITHSPACE_REG;
vern.alphaNumRegex = ALPHA_WITHSPACE_REG;
vern.nameRegex = VERNAC_WITNESS_NAME_REGEX;

VernacularService.COMBO_ID = $stateParams.COMBO_ID;
VernacularService.VernDet = vern.witnessBean;

//Auto-cap function
this.onIDNumChange = function(){
	this.witnessBean.idProofNo = $filter('uppercase')(this.witnessBean.idProofNo);
};
 if(SISFormData.sisFormAData_eKyc.INSURANCE_BUYING_FOR =='Self'){
            console.log("In Insured");
           vern.customertype='IN';
     }else{
          console.log("In Proposer");
          vern.customertype='PR';
     }
this.ExpLanguageList = [
		        {"EXP_LANGUAGE": "Select ", "LANGUAGE": null},
        		{"EXP_LANGUAGE": "Adi","LANGUAGE": "Adi"},
        		{"EXP_LANGUAGE": "Angami","LANGUAGE": "Angami"},
        		{"EXP_LANGUAGE": "Ao","LANGUAGE": "Ao"},
        		{"EXP_LANGUAGE": "Assamese","LANGUAGE": "Assamese"},
        		{"EXP_LANGUAGE": "Bengali","LANGUAGE": "Bengali"},
        		{"EXP_LANGUAGE": "Bhili/Bhilodi","LANGUAGE": "Bhili/Bhilodi"},
        		{"EXP_LANGUAGE": "Bodo","LANGUAGE": "Bodo"},
        		{"EXP_LANGUAGE": "Coorgi/Kodagu","LANGUAGE": "Coorgi/Kodagu"},
        		{"EXP_LANGUAGE": "Dimasa","LANGUAGE": "Dimasa"},
        		{"EXP_LANGUAGE": "Dogri","LANGUAGE": "Dogri"},
        		{"EXP_LANGUAGE": "Garo","LANGUAGE": "Garo"},
        		{"EXP_LANGUAGE": "Gondi","LANGUAGE": "Gondi"},
        		{"EXP_LANGUAGE": "Gujarati","LANGUAGE": "Gujarati"},
        		{"EXP_LANGUAGE": "Halabi","LANGUAGE": "Halabi"},
        		{"EXP_LANGUAGE": "Hindi","LANGUAGE": "Hindi"},
        		{"EXP_LANGUAGE": "Ho","LANGUAGE": "Ho"},
        		{"EXP_LANGUAGE": "Kannada","LANGUAGE": "Kannada"},
        		{"EXP_LANGUAGE": "Karbi/Mikir","LANGUAGE": "Karbi/Mikir"},
        		{"EXP_LANGUAGE": "Kashmiri","LANGUAGE": "Kashmiri"},
        		{"EXP_LANGUAGE": "Khandeshi","LANGUAGE": "Khandeshi"},
        		{"EXP_LANGUAGE": "Kharia","LANGUAGE": "Kharia"},
        		{"EXP_LANGUAGE": "Khasi","LANGUAGE": "Khasi"},
        		{"EXP_LANGUAGE": "Khond/Kondh","LANGUAGE": "Khond/Kondh"},
        		{"EXP_LANGUAGE": "Kisan","LANGUAGE": "Kisan"},
        		{"EXP_LANGUAGE": "Kolami","LANGUAGE": "Kolami"},
        		{"EXP_LANGUAGE": "Konkani","LANGUAGE": "Konkani"},
        		{"EXP_LANGUAGE": "Konyak","LANGUAGE": "Konyak"},
        		{"EXP_LANGUAGE": "Korku","LANGUAGE": "Korku"},
        		{"EXP_LANGUAGE": "Koya","LANGUAGE": "Koya"},
        		{"EXP_LANGUAGE": "Kui","LANGUAGE": "Kui"},
        		{"EXP_LANGUAGE": "Kurukh","LANGUAGE": "Kurukh"},
        		{"EXP_LANGUAGE": "Ladakhi","LANGUAGE": "Ladakhi"},
        		{"EXP_LANGUAGE": "Lotha","LANGUAGE": "Lotha"},
        		{"EXP_LANGUAGE": "Lushai/Mizo","LANGUAGE": "Lushai/Mizo"},
        		{"EXP_LANGUAGE": "Maithili","LANGUAGE": "Maithili"},
        		{"EXP_LANGUAGE": "Malayalam","LANGUAGE": "Malayalam"},
        		{"EXP_LANGUAGE": "Malto","LANGUAGE": "Malto"},
        		{"EXP_LANGUAGE": "Marathi","LANGUAGE": "Marathi"},
        		{"EXP_LANGUAGE": "Meitei/Manipuri","LANGUAGE": "Meitei/Manipuri"},
        		{"EXP_LANGUAGE": "Miri/Mishing","LANGUAGE": "Miri/Mishing"},
        		{"EXP_LANGUAGE": "Munda","LANGUAGE": "Munda"},
        		{"EXP_LANGUAGE": "Mundari","LANGUAGE": "Mundari"},
        		{"EXP_LANGUAGE": "Nepali","LANGUAGE": "Nepali"},
        		{"EXP_LANGUAGE": "Nissi/Dafla","LANGUAGE": "Nissi/Dafla"},
        		{"EXP_LANGUAGE": "Odia","LANGUAGE": "Odia"},
        		{"EXP_LANGUAGE": "Phom","LANGUAGE": "Phom"},
        		{"EXP_LANGUAGE": "Punjabi","LANGUAGE": "Punjabi"},
        		{"EXP_LANGUAGE": "Rabha","LANGUAGE": "Rabha"},
        		{"EXP_LANGUAGE": "Santali","LANGUAGE": "Santali"},
        		{"EXP_LANGUAGE": "Savara","LANGUAGE": "Savara"},
        		{"EXP_LANGUAGE": "Sema","LANGUAGE": "Sema"},
        		{"EXP_LANGUAGE": "Sindhi","LANGUAGE": "Sindhi"},
        		{"EXP_LANGUAGE": "Tamil","LANGUAGE": "Tamil"},
        		{"EXP_LANGUAGE": "Tangkhul","LANGUAGE": "Tangkhul"},
        		{"EXP_LANGUAGE": "Telugu","LANGUAGE": "Telugu"},
        		{"EXP_LANGUAGE": "Thado","LANGUAGE": "Thado"},
        		{"EXP_LANGUAGE": "Tripuri","LANGUAGE": "Tripuri"},
        		{"EXP_LANGUAGE": "Tulu","LANGUAGE": "Tulu"},
        		{"EXP_LANGUAGE": "Urdu","LANGUAGE": "Urdu"}
	];
	    console.log("Value : " + JSON.stringify(this.ExpLanguageList[0]));
        this.expLang = this.ExpLanguageList[0];
        vern.witnessBean.expLang=this.expLang.LANGUAGE;


this.saveWitnessSign = function(){

    CommonService.doSign("appVernacSign");

};

//this.onProofChange = function(){
//    //vern.witnessBean.idProofType=this.verProofList[0];
//    if(vern.idProofType.MPPROOF_DESC =='Others'){
//     vern.witnessBean.idProofType=vern.Other_PROOF_TYPE;
//     console.log("onOther: " + vern.Other_PROOF_TYPE);
//     console.log("onOther2: " + vern.witnessBean.idProofType);
//    }else{
//   vern.witnessBean.idProofType=vern.idProofType.MPPROOF_DESC;
//   console.log("In nonothers section: "+ vern.witnessBean.idProofType);
//   }
//   console.log("onLangChange: " + vern.witnessBean.idProofType);
//	};
this.onLangChange = function(){
    vern.witnessBean.expLang = vern.expLang.LANGUAGE;
    console.log("onLangChange: " + vern.witnessBean.expLang);

	};

this.onNext = function(vernacForm){
vern.click = true;

    if(vernacForm.$invalid == false){
        console.log("vern.witnessBean value :"+ vern.witnessBean.idProofType);
        console.log("vern.witnessBean :"+JSON.stringify(vern.witnessBean));
        VernacularService.VernDet = vern.witnessBean;

            vern.saveWitnessSign();
    }
}

}]);



appVernacularModule.service('VernacularService',['$state', 'CommonService','AppOutputService','AppTermOutputService', function($state, CommonService, AppOutputService,AppTermOutputService){


var vServ = this;
this.VernDet = {};
this.COMBO_ID;

this.saveWitnessSignature = function(sign){
	CommonService.showLoading("Saving Signature...");
    console.log(vServ.COMBO_ID+"VernDet :"+JSON.stringify(vServ.VernDet));
    if(!!vServ.COMBO_ID)
        AppTermOutputService.replaceVernacDetails(sign, vServ.VernDet);
    else
        AppOutputService.replaceVernacDetails(sign, vServ.VernDet);

}

}]);
