//alert("family1");
familySection1Module.controller('familySection1Ctrl',['$state','CommonService','ApplicationFormDataService','appFamilyDetService','ExistingAppData','$q','$filter','getFamilyMemList','getFamilyRecord','getProdFlagData','getBackDate','famDetInfoService','LoginService',function($state,CommonService,ApplicationFormDataService,appFamilyDetService,ExistingAppData,$q,$filter,getFamilyMemList,getFamilyRecord,getProdFlagData,getBackDate,famDetInfoService,LoginService){

//alert("familySection1");
var famDet = this;
this.click = false;

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


famDet.appFamilybean={};
famDet.FamilyArr=[{
    }];
this.INS_DOB = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
this.Age = CommonService.getAge(this.INS_DOB);
this.OCC = ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS;
this.maritalStatus = ApplicationFormDataService.applicationFormBean.personalDetBean.INSURED_MARITAL_STATUS;
debug("Insured Marital status: " + JSON.stringify(this.maritalStatus));

this.famCondition = (parseInt(famDet.Age)<18 || famDet.OCC=='Student' || famDet.OCC=='Housewife');
console.log("famcondition in Family Section 1::"+famDet.Age+"Occupation::"+famDet.OCC+"this.famCondition:::"+this.famCondition);

famDetInfoService.setActiveTab("familySection1");
CommonService.hideLoading();
console.log("Load family details::"+JSON.stringify(getFamilyMemList));
famDet.loadFamilyDetails = getFamilyMemList;
console.log("FAmily List is::"+JSON.stringify(getFamilyMemList)+"0th value::"+JSON.stringify(famDet.loadFamilyDetails[0]));
//famDet.FamilyArr[0].familyMember = famDet.loadFamilyDetails[0];

this.addMembers = function(){
    if(famDet.FamilyArr.length<5){
        var newItem = famDet.FamilyArr.length+1;
        famDet.FamilyArr.push({'Inscnt':'0'+newItem});
        console.log("Element added");
        //famDet.FamilyArr[length+1].familyMember = famDet.loadFamilyDetails[0];
    }
    else
        navigator.notification.alert("You cannot add more than 5",function(){CommonService.hideLoading();},"Application","OK");
}

this.deleteBlock = function(index){
     if(this.FamilyArr.length > 1)
        this.FamilyArr.splice(index,1);
 };

famDet.getFamilyDetails = getFamilyRecord;
this.getSelectedRecord = function(){
console.log("getFamilyRecord length :"+famDet.getFamilyDetails.length);
    if(famDet.getFamilyDetails!=undefined && famDet.FamilyArr!=undefined && famDet.FamilyArr!=""){
        for(var i=0;i<famDet.getFamilyDetails.length;i++){
        //var i = famDet.getFamilyDetails[i].RELATIONSHIP_ID;
        //console.log("First i is::"+i);
            (function(i){
                if(i!=0)
                    {
                        var newItem = famDet.FamilyArr.length+1;
                        famDet.FamilyArr.push({'Inscnt':'0'+newItem});

                    }
                        famDet.familyMemberSelected = $filter('filter')(famDet.loadFamilyDetails, {"NBFE_CODE": famDet.getFamilyDetails[i].RELATIONSHIP_ID})[0];
                        console.log("ith start value::"+i+"this.familyMemberSelected::"+JSON.stringify($filter('filter')(famDet.loadFamilyDetails, {"NBFE_CODE": famDet.getFamilyDetails[i].RELATIONSHIP_ID})[0]));
                        famDet.FamilyArr[i].familyMember = famDet.familyMemberSelected;
                        famDet.FamilyArr[i].rdFamilyStatus = famDet.getFamilyDetails[i].ALIVE_OR_DECEASED || null;
                        famDet.FamilyArr[i].CurrAge = parseInt(famDet.getFamilyDetails[i].DEATH_AGE) || null;
                        famDet.FamilyArr[i].status = famDet.getFamilyDetails[i].HEALTH_STATUS_DECEASED_REASON || null;
                        console.log("ith end value::"+i);
                        console.log("Value::::"+JSON.stringify(famDet.FamilyArr));
                }(i)

             );
        }
    }
}
famDet.getSelectedRecord();

this.disableOption = function(index){
console.log("function"+index);
var selectedItem = famDet.FamilyArr[index].familyMember.NBFE_CODE;
for(var i=0;i<famDet.FamilyArr.length;i++){
   if(i!=index){
    if(famDet.FamilyArr[i].familyMember.NBFE_CODE == famDet.FamilyArr[index].familyMember.NBFE_CODE/* && famDet.FamilyArr[i].familyMember.NBFE_CODE != 99 && famDet.FamilyArr[i].familyMember.NBFE_CODE != 98*/){
        navigator.notification.alert("Member is already selected",function(){CommonService.hideLoading();},"Application","OK");
        famDet.FamilyArr[index].familyMember = famDet.FamilyArr[0];
        console.log("value:::"+JSON.stringify(famDet.FamilyArr[0])+"vsgx::"+JSON.stringify(famDet.FamilyArr[index].familyMember));
        }
      }
   }
}

var today = new Date();
famDet.getFlagData = getProdFlagData;
console.log("flag::"+JSON.stringify(getProdFlagData));
if(famDet.getFlagData!=undefined && famDet.getFlagData.length!=0 && famDet.getFlagData[0].ISBACKDATE=='Y')
{
    famDet.flag = 'Y';
    famDet.appFamilybean.appBackDateFrom = today;

}
else
    famDet.flag='N';

var diff = famDet.getFlagData[0].NOOFMONTH;

this.monthDiff = function(d1, d2) {
    var months;
    months = (d1.getFullYear() - d2.getFullYear()) * 12;
    //alert(months);
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months+2;

}

famDet.errFlag = false;
this.validateDate = function(){
if(famDet.flag == 'Y'){
console.log("inside validate func");
var startdate = new Date(famDet.appFamilybean.appBackDateFrom);
    startdate.setMonth(startdate.getMonth()+1);

    var chkdate = CommonService.formatDobToDb(famDet.appFamilybean.appBackDateTo);
    var chkFutureDate = CommonService.getAge(chkdate);

var enddate = new Date(famDet.appFamilybean.appBackDateTo);
    enddate.setMonth(enddate.getMonth()+1);

    debug("startdate :"+startdate+"\n  enddate: "+enddate);

    var currDate = new Date();
    currDate.setMonth(currDate.getMonth()+1);
    var BackNoMnth=(startdate.getFullYear()*12 + startdate.getMonth()) - (enddate.getFullYear()*12 + enddate.getMonth());
    debug("Got diff :"+BackNoMnth);
      if(chkFutureDate <0){
          navigator.notification.alert("Back date cannot be future date",null,"Application","OK");
          famDet.errFlag = true;
          famDet.appFamilybean.appBackDateTo = "";
      }
      else{
        if(currDate.getMonth()>=1 && currDate.getMonth()<=3)
            {
                debug("back date :1:"+currDate.getMonth());
                var finYearDate=(new Date().getFullYear()-1)+"-04-01";
                var finYear=new Date().getFullYear()-1;
                debug("finYearDate :"+finYearDate+"\nfinYear :"+finYear)
            }
        else
            {
                debug("back date :2:"+currDate.getMonth());
                var finYearDate=(new Date().getFullYear())+"-04-01";
                var finYear=new Date().getFullYear();
                debug("ELSE : finYearDate :"+finYearDate+"\nfinYear :"+finYear)
            }
         if(famDet.appFamilybean.appBackDateTo!=undefined && famDet.appFamilybean.appBackDateTo!="")
            {
                debug("enddate.getMonth():"+enddate.getMonth()+"\n"+enddate.getFullYear()+"back date :3:"+BackNoMnth);
                if(parseInt(BackNoMnth)>diff)
                    {
                        navigator.notification.alert("Back date cannot be more than "+diff+" months.",function(){CommonService.hideLoading();},"Application","OK");
                        famDet.errFlag = true;
                        famDet.appFamilybean.appBackDateTo = "";
                    }

                 else
                    if(startdate.getFullYear()==parseInt(finYear) && (enddate.getMonth())<4)
                    {
                        navigator.notification.alert("Back date should within same finanacial year.",function(){CommonService.hideLoading();},"Application","OK");
                        famDet.errFlag  = true;
                        famDet.appFamilybean.appBackDateTo = "";
                    }
                    else
                    famDet.errFlag = false;
            }
            else
                debug("back date :4:"+famDet.appFamilybean.appBackDateTo);
      }
        }
else{}
}

famDet.getSelectedBackDate = getBackDate;
console.log("returend back date is::"+JSON.stringify(famDet.getBackDate));
famDet.returnBackdate = function(){
if(famDet.appFamilybean!=undefined && famDet.appFamilybean!="" && famDet.getSelectedBackDate!=undefined && famDet.getSelectedBackDate!="")
famDet.appFamilybean.appBackDateTo = famDet.getSelectedBackDate[0].INSURANCE_BACK_DATE;
}
famDet.returnBackdate();

this.deleteFunction = function(){
    appFamilyDetService.deleteFamRec().then(
        function(res){
            if(!!res && res && famDet.famCondition!=false){
                console.log("result is::"+res+"famDet.famCondition in if::"+famDet.famCondition);
                appFamilyDetService.insertMemDetails(famDet).then(function(){
                    appFamilyDetService.updateScreenRec(famDet,famDet.appFamilybean).then(function(){
                        $state.go("applicationForm.familyDetails.familySection2");
                    });
                });
            }
            else{
                console.log("result in else::"+res+"famDet.famCondition in else::"+famDet.famCondition);
                appFamilyDetService.insertMemDetails(famDet).then(function(){
                    appFamilyDetService.updateScreenRec(famDet,famDet.appFamilybean).then(function(){
						famDet.updateFlag();
                        $state.go("applicationForm.nomAppDetails.nomineeDetails");
                    });
                });
            }
         }
    );
}

this.updateFlag = function(){
	var whereClauseObj = {};
     whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
     whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN5_COMPLETED":'Y'},whereClauseObj).then(
		function(updated){
			debug("SCREEN 5 COMPLETED WHEN MOVE TO NOM APPT");
		}
	);

}

this.gotoFamilySection2 = function(form){
    CommonService.showLoading();
console.log("Inside gotoFamilySection2::"+famDet.getFamilyDetails.length+"famDet.famCondition::"+famDet.famCondition);
this.click = true;

    if(form.$invalid !=true && famDet.errFlag==false)
    {
        if(famDet.getFamilyDetails!=undefined && famDet.FamilyArr!=undefined && famDet.FamilyArr!="")
        {
            if(famDet.getFamilyDetails.length!=5)
            {
                console.log("inside family details");
                navigator.notification.confirm("Do you want to continue without other member details?",
                                    function(buttonIndex){
                                        if(buttonIndex=="2")
                                        {
                                            if(famDet.getFamilyDetails.length<5 )
                                            {
                                                navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                                CommonService.hideLoading();
                                                var newItem = famDet.FamilyArr.length+1;
                                                famDet.FamilyArr.push({'Inscnt':'0'+newItem});
                                                famDet.getFamilyDetails.length++;
                                                console.log("Element added"+famDet.getFamilyDetails.length);
                                            }
                                            else
                                            {
                                                console.log("inside if else");
                                                navigator.notification.alert("You cannot add more than 5",function(){CommonService.hideLoading();},"Application","OK");
                                                ApplicationFormDataService.applicationFormBean.appFamilybean = famDet.appFamilybean;
                                                ApplicationFormDataService.applicationFormBean.FamilyArr = famDet.FamilyArr;
                                                famDet.deleteFunction();
                                            }
                                        }
                                        else if(famDet.famCondition!=true)
                                        {
                                            console.log("inside famCondition if::"+famDet.famCondition);
                                            famDet.deleteFunction();
                                        }
                                        else
                                        {
                                            console.log("inside popup else");
                                            ApplicationFormDataService.applicationFormBean.appFamilybean = famDet.appFamilybean;
                                            ApplicationFormDataService.applicationFormBean.FamilyArr = famDet.FamilyArr;
                                            famDet.deleteFunction();
                                        }
                                    },
                                    'Confirm Family Details',
                                    ['Yes', 'No']
                );
                //changes 30-12-2016 converted string to array for yes,no
            }
            else
            {
                ApplicationFormDataService.applicationFormBean.appFamilybean = famDet.appFamilybean;
                ApplicationFormDataService.applicationFormBean.FamilyArr = famDet.FamilyArr;
                famDet.deleteFunction();
            }

        }

    }
    else
        navigator.notification.alert("Please fill the complete form details.",function(){CommonService.hideLoading();},"Application","OK");
}
CommonService.hideLoading();
}]);

//Service
familySection1Module.service('appFamilyDetService',['$state','LoginService', 'CommonService','ApplicationFormDataService','$q','famDetInfoService','LoadApplicationScreenData','AppTimeService','nomService',function($state,LoginService, CommonService,ApplicationFormDataService,$q,famDetInfoService,LoadApplicationScreenData,AppTimeService,nomService){

var famDetServ = this;
this.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

this.loadFamilyMemList = function(){
famDetServ.PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
console.log(ApplicationFormDataService.SISFormData.sisMainData.SIS_ID+" : Inside function : "+famDetServ.PGL_ID);
var dfd = $q.defer();
var whereClauseObj ={};
    whereClauseObj.ISACTIVE = 'Y';
try{
    CommonService.selectRecords(db,'LP_FAMILY_HISTORY_RELATION',false,'nbfe_code,relationship_desc',whereClauseObj,'nbfe_code asc').then(

        function(res){
        try{
        console.log("Result is::"+JSON.stringify(res));
            var getFamilyMemList=[];
            console.log("Length is::"+res.rows.length);
            if(!!res && res.rows.length>0){
                //getFamilyMemList.push({"RELATIONSHIP_DESC":"Select ","NBFE_CODE":null});
                for(var i=0;i<res.rows.length;i++){
                    var RecFamilyDet = {};
                    RecFamilyDet.NBFE_CODE = res.rows.item(i).NBFE_CODE;
                    RecFamilyDet.RELATIONSHIP_DESC = res.rows.item(i).RELATIONSHIP_DESC;
                    console.log("Family Member Record is::"+JSON.stringify(RecFamilyDet));
                    getFamilyMemList.push(RecFamilyDet);
                }
                dfd.resolve(getFamilyMemList);
            }
            else
            dfd.resolve(null);
            }catch(error){console.log("Error found is::"+error.message);}
        }

    );
    return dfd.promise;
    }catch(e){console.log("Error Message::"+e.message);}
}

this.insertMemDetails = function(famDet){
var dfd = $q.defer();
    for(var i=0;i<5;i++){
        (function(i){
        if(famDet.FamilyArr[i]!=undefined && famDet.FamilyArr[i]!=""){
        console.log("Inside if::");
                if(famDet.FamilyArr[i].familyMember!=undefined && famDet.FamilyArr[i].familyMember!=""&& !!famDet.FamilyArr[i].familyMember.NBFE_CODE && famDet.FamilyArr[i].rdFamilyStatus!=undefined && famDet.FamilyArr[i].rdFamilyStatus!="" && famDet.FamilyArr[i].CurrAge!=undefined && famDet.FamilyArr[i].CurrAge!="" && famDet.FamilyArr[i].status!=undefined && famDet.FamilyArr[i].status!=""){
                    CommonService.insertOrReplaceRecord(db,'LP_APP_FH_SCRN_E',{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":famDetServ.AGENT_CD,"RELATIONSHIP_ID":famDet.FamilyArr[i].familyMember.NBFE_CODE,"ALIVE_OR_DECEASED":famDet.FamilyArr[i].rdFamilyStatus,"DEATH_AGE":famDet.FamilyArr[i].CurrAge,"HEALTH_STATUS_DECEASED_REASON":famDet.FamilyArr[i].status},true).then(
                        function(res){
                            if(!!res){
                                console.log("Family member record inserted successfully"+JSON.stringify(famDet.FamilyArr[i].familyMember));
                                dfd.resolve(res);
                                famDetInfoService.setActiveTab("familySection2");
                                //$state.go("applicationForm.familyDetails.familySection2");
                            }
                            else
                                dfd.resolve(null);
                        }

                    );

                }
            }
        }(i));
    }
    return dfd.promise;
}

this.selectFamilyMem = function(){
var dfd = $q.defer();
var whereClauseObj ={};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famDetServ.AGENT_CD;
    //whereClauseObj.RELATIONSHIP_ID = "";
    CommonService.selectRecords(db,"LP_APP_FH_SCRN_E",false,"*",whereClauseObj,"RELATIONSHIP_ID ASC").then(
        function(res){
        var getFamilyRecord = [];
        console.log("FamilyRecord length is::"+res.rows.length);
            if(!!res && res.rows.length>0){
                //getFamilyRecord.push({"RELATIONSHIP_ID":"Select ","ALIVE_OR_DECEASED":"","DEATH_AGE":"","HEALTH_STATUS_DECEASED_REASON":""})
                for(var i=0;i<res.rows.length;i++){
                (function(i){
                 if(res.rows.item(i).RELATIONSHIP_ID!="C11" && res.rows.item(i).RELATIONSHIP_ID!="C12" && res.rows.item(i).RELATIONSHIP_ID!="C13" && i<6)
                                {
                                    var RecFamDetails = {};
                                    RecFamDetails.RELATIONSHIP_ID = res.rows.item(i).RELATIONSHIP_ID;
                                    RecFamDetails.ALIVE_OR_DECEASED = res.rows.item(i).ALIVE_OR_DECEASED;
                                    RecFamDetails.DEATH_AGE = res.rows.item(i).DEATH_AGE;
                                    RecFamDetails.HEALTH_STATUS_DECEASED_REASON = res.rows.item(i).HEALTH_STATUS_DECEASED_REASON;
                                    console.log("Family record::"+JSON.stringify(RecFamDetails));
                                    getFamilyRecord.push(RecFamDetails);
                                 }
                }(i));

                }
                dfd.resolve(getFamilyRecord);
                console.log("getFamilyRecord::"+JSON.stringify(getFamilyRecord));
            }
            else
                dfd.resolve(getFamilyRecord);
        }
    );

    return dfd.promise;
}

this.updateScreenRec = function(famDet,appFamilybean){
console.log("famDetServ.famCondition in updateScreen ::"+famDet.famCondition);
famDetServ.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
var dfd = $q.defer();
var AppBackDate = null;
if(famDet.appFamilybean.appBackDateTo!=undefined && famDet.appFamilybean.appBackDateTo!="")
 AppBackDate = CommonService.formatDobToDb(new Date(famDet.appFamilybean.appBackDateTo));
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famDetServ.AGENT_CD;
    try{
    CommonService.updateRecords(db,'LP_APPLICATION_MAIN',{"INSURANCE_BACK_DATE":AppBackDate,"IS_FAM_SEC1":'Y'},whereClauseObj).then(
        function(res){
            console.log("LP_APPLICATION_MAIN updated::IS_FAM_SEC1"+AppBackDate+"Case is::"+famDetServ.isSelf);
            if(famDetServ.isSelf){
            console.log("FamilyCondtion is::"+famDet.famCondition);
                   if(famDet.famCondition!=true){
                        //timeline changes
                        CommonService.showLoading();
                        AppTimeService.setActiveTab("nomAppDetails");
                        AppTimeService.isFamDetCompleted = true;
                        //$rootScope.val = 50;
                        AppTimeService.setProgressValue(50);
                        //$state.go("applicationForm.nomAppDetails.nomineeDetails");
                        dfd.resolve(res);
                    }
                    else if(famDet.famCondition == true){
                        console.log("123*****");
                        CommonService.hideLoading();
                        famDetInfoService.setActiveTab("familySection2");
                        AppTimeService.isFamDetCompleted = true;
                        //$rootScope.val = 50;
                        AppTimeService.setProgressValue(40);
                        //$state.go("applicationForm.nomAppDetails.nomineeDetails");
                        dfd.resolve(res);
                        }
            }
            else
            {
                if(famDet.famCondition==true){

                    console.log("123*****");
                    CommonService.hideLoading();
                    famDetInfoService.setActiveTab("familySection2");
                    AppTimeService.isFamDetCompleted = true;
                    //$rootScope.val = 50;
                    AppTimeService.setProgressValue(40);
                    //$state.go("applicationForm.nomAppDetails.nomineeDetails");
                    dfd.resolve(res);
                }
                else{
                    CommonService.showLoading();
                    AppTimeService.setActiveTab("paymentDetails");
                    AppTimeService.isFamDetCompleted = true;
                    //$rootScope.val = 60;
                    AppTimeService.setProgressValue(60);
                    famDetServ.skipNomineeAppointee();
                    dfd.resolve(res);
                }
            }
        }

    );
    }catch(e){console.log("Error Message is::"+e.message);}
    return dfd.promise;
}

this.chkBackDate = function(){
famDetServ.PGL_ID = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
var dfd = $q.defer();
var whereClauseObj={};
    whereClauseObj.PNL_PGL_ID = famDetServ.PGL_ID;
    CommonService.selectRecords(sisDB,"LP_PNL_PLAN_LK",false,'isbackdate,noofmonth',whereClauseObj).then(
        function(res){
        var getProdFlagData=[];
        console.log("SIS db len::"+res.rows.length);
            if(!!res && res.rows.length>0){
                var ProdData = {};
                ProdData.ISBACKDATE = res.rows.item(0).ISBACKDATE;
                ProdData.NOOFMONTH = res.rows.item(0).NOOFMONTH;
                console.log("ProdData is::"+JSON.stringify(ProdData));
                console.log("ProdData is::"+JSON.stringify(ProdData));
                getProdFlagData.push(ProdData);
                dfd.resolve(getProdFlagData);
            }
            dfd.resolve(null);
        }
    );
    return dfd.promise;
}

this.getProposerAsNominee = function(){
var dfd = $q.defer();

    var nomObj = {};
    nomObj.appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    nomObj.username = famDetServ.AGENT_CD;
    nomObj.nomCustID = 'C05';
    nomObj.FirstName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_FIRST_NAME;
    nomObj.LastName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_LAST_NAME;
    nomObj.MiddleName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_MIDDLE_NAME;
    var dob = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_DOB;
    nomObj.dob = CommonService.formatDobFromDb(dob);
    var Relation = {};
    LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
        function(AppData){
                if(!!AppData.applicationPersonalInfo.Insured && !!AppData.applicationPersonalInfo.Insured.RELATIONSHIP_ID)
                {debug("in insured relation");
                    Relation.NOM_RELATIONSHIP_DESC = AppData.applicationPersonalInfo.Insured.RELATIONSHIP_DESC;
                    Relation.NOM_NBFE_CODE = AppData.applicationPersonalInfo.Insured.RELATIONSHIP_ID
                    nomObj.Relation = Relation;
                }
                else
                {debug("in proposer relation");
                    Relation.NOM_RELATIONSHIP_DESC = AppData.applicationPersonalInfo.Proposer.RELATIONSHIP_DESC;
                    Relation.NOM_NBFE_CODE = AppData.applicationPersonalInfo.Proposer.RELATIONSHIP_ID
                    nomObj.Relation = Relation;
                }
                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_GENDER == 'M')
                    nomObj.Gen = 'Male';
                else
                    nomObj.Gen = 'Female';

                nomObj.Percent = 100;

                dfd.resolve(nomObj);
        }
    );


return dfd.promise;

}

this.skipNomineeAppointee = function(){

    debug("inside skipNomineeAppointee");

    famDetServ.getProposerAsNominee().then(function(nomObj){

    debug("getProposerAsNominee completed!");

       nomService.nomDataInsert(0, null, nomObj).then(function(resp){

            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            whereClauseObj.AGENT_CD = famDetServ.AGENT_CD;
            if(resp)
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN6_COMPLETED" :"NA"}, whereClauseObj).then(
                                    function(res){
                                        debug("IS_SCREEN6_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
                                        $state.go("applicationForm.paymentDetails.bankdetails");
                                    }
                                );



        });

    });

}

this.selectBackdate = function(){
var dfd = $q.defer();
var whereClauseObj= {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famDetServ.AGENT_CD;
    CommonService.selectRecords(db,"LP_APPLICATION_MAIN",true,"insurance_back_date",whereClauseObj).then(
        function(res){
        var getBackDate=[];
        console.log("Back date result::"+res.rows.length);
            if(!!res && res.rows.length>0 ){
                var backDateObj = {};
                if(res.rows.item(0).INSURANCE_BACK_DATE!=undefined && res.rows.item(0).INSURANCE_BACK_DATE!="")
                backDateObj.INSURANCE_BACK_DATE = CommonService.formatDobFromDb(res.rows.item(0).INSURANCE_BACK_DATE);
                console.log("BackDate is::"+JSON.stringify(backDateObj));
                getBackDate.push(backDateObj);
                dfd.resolve(getBackDate);
            }
            dfd.resolve(null);
        }
    );
    return dfd.promise;
}

this.deleteFamRec = function(){
var dfd = $q.defer();
console.log("Inside family delete function");
var sql = "Delete from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID NOT IN('C11','C12','C13') OR RELATIONSHIP_ID is NULL"
    CommonService.transaction(db,
        function(tx){
        console.log("inside deleteFamRec")
            CommonService.executeSql(tx,sql,[ApplicationFormDataService.applicationFormBean.APPLICATION_ID,famDetServ.AGENT_CD],
                function(tx,res){
                    console.log("Family record deleted successfully...");
                    dfd.resolve(true);
                },
                function(tx,err)
                {
                    console.log("Error"+err.message);
                }
            );
        },function(error){
            console.log("Error msg is::"+error.message);
        }
    )
return dfd.promise;
}


}]);
