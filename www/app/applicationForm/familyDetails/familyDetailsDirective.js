appfamilyDetailsModule.controller('famDetInfoCtrl',['famDetInfoService','famDetService',function(famDetInfoService,famDetService){
    debug("exist controller loaded");

    this.famDetArr = [
        {'type':'familySection1','name':'Section 1'},
        {'type':'familySection2','name':'Section 2'}
    ]

    this.activeTab = famDetInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        famDetService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.famDetArr));
}]);

appfamilyDetailsModule.service('famDetInfoService',[function(){
    debug("exist service loaded");

    this.activeTab = "familySection1";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

appfamilyDetailsModule.directive('fdTimeline',[function() {
        "use strict";
        debug('in exist');
        return {
            restrict: 'E',
            controller: "famDetInfoCtrl",
            controllerAs: "fdc",
            bindToController: true,
            templateUrl: "applicationForm/familyDetails/familyDetailsTimeline.html"
        };
}]);