appfamilyDetailsModule.controller('familyCtrl',['$state','CommonService',function($state,CommonService){

var family = this;

//timeline changes..
CommonService.hideLoading();

}]);

appfamilyDetailsModule.service('famDetService',['$state','famDetInfoService','LoadApplicationScreenData','ApplicationFormDataService','LoginService',function($state,famDetInfoService,LoadApplicationScreenData,ApplicationFormDataService,LoginService){
    this.gotoPage = function(pageName){
		if(famDetInfoService.getActiveTab(pageName)!==pageName){
			CommonService.showLoading();
		}
        famDetInfoService.setActiveTab(pageName);
        switch(pageName){
            case 'familySection1':
                    $state.go('applicationForm.familyDetails.familySection1');
                    break;

            case 'familySection2':
                 LoadApplicationScreenData.loadApplicationFlags(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                function(appData){
                                    debug("appData.applicationFlags.IS_FAM_SEC1 :"+appData.applicationFlags.IS_FAM_SEC1);
                                    if(appData!=undefined && !!appData.applicationFlags && Object.keys(appData.applicationFlags).length !=0 && appData.applicationFlags.IS_FAM_SEC1 == 'Y')//if(payData != {})
                                        $state.go('applicationForm.familyDetails.familySection2');
                                    else
                                        navigator.notification.alert("Please complete Family Section 1",function(){CommonService.hideLoading();},"Application","OK");
                                }
                            )

                break;

            default:
                $state.go('applicationForm.familyDetails.familySection1');
                break;
        }
    };
}]);
