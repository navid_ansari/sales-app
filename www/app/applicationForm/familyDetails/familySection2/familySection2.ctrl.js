
familySection2Module.controller('familySection2Ctrl',['$state','CommonService','ApplicationFormDataService','appFamilyChildDetService','ExistingAppData','$q','getMemberRec','getChildRec','getSecondChildRec',function($state,CommonService,ApplicationFormDataService,appFamilyChildDetService,ExistingAppData,$q,getMemberRec,getChildRec,getSecondChildRec){
console.log("familysection2 ctrl");

var famChildDet = this;
famChildDet.click = false;
this.INS_DOB = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
this.Age = CommonService.getAge(this.INS_DOB);
this.OCC = ExistingAppData.applicationPersonalInfo.Insured.OCCUPATION_CLASS;
console.log(this.OCC+"Age is::"+this.Age);


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


this.famCondition = (parseInt(famChildDet.Age)<18 || famChildDet.OCC=='Student' || famChildDet.OCC=='Housewife')
console.log("famCondition is::"+this.famCondition);
famChildDet.appFamilybean2 = {};
famChildDet.FamilyChildArr=[{}];

this.ChildId = [{"RELATIONSHIP_ID":"C12"},{"RELATIONSHIP_ID":"C13"}];
famChildDet.appFamilybean2.rdFamilyGender = "M";

this.addNewChild = function(){
    if(famChildDet.FamilyChildArr.length<2)
    {
        var newChild = famChildDet.FamilyChildArr.length+1;
        famChildDet.FamilyChildArr.push({'InsCnt':'0'+newChild});
        console.log("child added");
    }
    else
        navigator.notification.alert("You cannot add more than 2 child.",function(){CommonService.hideLoading();},"Application","OK");
}

this.deleteChildBlock = function(index){
    if(famChildDet.FamilyChildArr.length>1)
        {
            famChildDet.getSecondChildRec = null;
            famChildDet.FamilyChildArr.splice(index,1);
        }
}

famChildDet.getSelectedRec = getMemberRec;
this.selectedRec = function(){
console.log("Father details::"+famChildDet.getSelectedRec);
if(famChildDet.getSelectedRec!=undefined && famChildDet.getSelectedRec!=""){
    famChildDet.appFamilybean2.Fname = famChildDet.getSelectedRec[0].FIRST_NAME;
    famChildDet.appFamilybean2.Mname = famChildDet.getSelectedRec[0].MIDDLE_NAME;
    famChildDet.appFamilybean2.Lname = famChildDet.getSelectedRec[0].LAST_NAME;
    famChildDet.appFamilybean2.rdFamilyGender = famChildDet.getSelectedRec[0].GENDER;
    famChildDet.appFamilybean2.Bdate = famChildDet.getSelectedRec[0].BIRTH_DATE;
    famChildDet.appFamilybean2.MemOccu = famChildDet.getSelectedRec[0].OCCUPATION_DESC;
    if(famChildDet.getSelectedRec[0].ANNUAL_INCOME!=undefined && famChildDet.getSelectedRec[0].ANNUAL_INCOME!=null)
    famChildDet.appFamilybean2.MemAnnualInc = famChildDet.getSelectedRec[0].ANNUAL_INCOME;
    if(famChildDet.getSelectedRec[0].INSURANCE_DETAILS!=undefined && famChildDet.getSelectedRec[0].INSURANCE_DETAILS!=null)
    famChildDet.appFamilybean2.MemInsDet = famChildDet.getSelectedRec[0].INSURANCE_DETAILS;
    }
}
famChildDet.selectedRec();

famChildDet.getSelectedChildRec = getChildRec;
this.selectedChildRec = function(){
if(famChildDet.getSelectedChildRec!=undefined && famChildDet.getSelectedChildRec!="" && famChildDet.FamilyChildArr!=undefined && famChildDet.FamilyChildArr!=""){
    famChildDet.FamilyChildArr[0].childFname = famChildDet.getSelectedChildRec[0].FIRST_NAME;
    famChildDet.FamilyChildArr[0].childMname = famChildDet.getSelectedChildRec[0].MIDDLE_NAME;
    famChildDet.FamilyChildArr[0].childLname = famChildDet.getSelectedChildRec[0].LAST_NAME;
    famChildDet.FamilyChildArr[0].rdFamilyGender1 = famChildDet.getSelectedChildRec[0].GENDER;
    famChildDet.FamilyChildArr[0].childBdate = famChildDet.getSelectedChildRec[0].BIRTH_DATE;
    famChildDet.FamilyChildArr[0].childOccu = famChildDet.getSelectedChildRec[0].OCCUPATION_DESC;
    famChildDet.FamilyChildArr[0].childAnnuInc = parseInt(famChildDet.getSelectedChildRec[0].ANNUAL_INCOME);
    famChildDet.FamilyChildArr[0].childInsdet = parseInt(famChildDet.getSelectedChildRec[0].INSURANCE_DETAILS);
    }
}
famChildDet.selectedChildRec();

this.getSecondChildRec = getSecondChildRec;
this.getSecondChildSelectedRec = function(){
if(famChildDet.getSecondChildRec!=undefined && famChildDet.getSecondChildRec!="" && famChildDet.FamilyChildArr!=undefined && famChildDet.FamilyChildArr!=""){
     var newChild = famChildDet.FamilyChildArr.length+1;
     famChildDet.FamilyChildArr.push({'InsCnt':'0'+newChild});
     famChildDet.FamilyChildArr[1].childFname = famChildDet.getSecondChildRec[0].FIRST_NAME;
     famChildDet.FamilyChildArr[1].childMname = famChildDet.getSecondChildRec[0].MIDDLE_NAME;
     famChildDet.FamilyChildArr[1].childLname = famChildDet.getSecondChildRec[0].LAST_NAME;
     famChildDet.FamilyChildArr[1].rdFamilyGender1 = famChildDet.getSecondChildRec[0].GENDER;
     famChildDet.FamilyChildArr[1].childBdate = famChildDet.getSecondChildRec[0].BIRTH_DATE;
     famChildDet.FamilyChildArr[1].childOccu = famChildDet.getSecondChildRec[0].OCCUPATION_DESC;
     famChildDet.FamilyChildArr[1].childAnnuInc = parseInt(famChildDet.getSecondChildRec[0].ANNUAL_INCOME);
     famChildDet.FamilyChildArr[1].childInsdet = parseInt(famChildDet.getSecondChildRec[0].INSURANCE_DETAILS);
    }
}
famChildDet.getSecondChildSelectedRec();

this.deleteFun = function(){
    appFamilyChildDetService.deleteFatherRec().then(
        function(res){
            if(!!res && res){
                console.log("result is::"+res);
                appFamilyChildDetService.insertFamDetails(famChildDet).then(function(){
                    appFamilyChildDetService.insertChildDetails(famChildDet).then(function(){
                        appFamilyChildDetService.updateScreen().then(function(){
                            $state.go("applicationForm.nomAppDetails.nomineeDetails");
                        });
                    });

                });
            }
        }
    );
}

this.gotoNextPg = function(form){
    CommonService.showLoading();
famChildDet.click = true;
//Condition to allow only one block....
var ChildCondition = famChildDet.FamilyChildArr[0].childFname!=undefined && famChildDet.FamilyChildArr[0].childFname!="" && famChildDet.FamilyChildArr[0].childLname!=undefined && famChildDet.FamilyChildArr[0].childLname!=""
                     && famChildDet.FamilyChildArr[0].rdFamilyGender1!=undefined && famChildDet.FamilyChildArr[0].rdFamilyGender1!="" && famChildDet.FamilyChildArr[0].childBdate!=undefined && famChildDet.FamilyChildArr[0].childBdate!=""
                     && famChildDet.FamilyChildArr[0].childOccu!=undefined && famChildDet.FamilyChildArr[0].childOccu!="" && famChildDet.FamilyChildArr[0].childAnnuInc!=undefined && famChildDet.FamilyChildArr[0].childAnnuInc!=""
                     && famChildDet.FamilyChildArr[0].childInsdet!=undefined && famChildDet.FamilyChildArr[0].childInsdet!="";

var FamilyCondition = famChildDet.appFamilybean2.Fname!=undefined && famChildDet.appFamilybean2.Fname!="" && famChildDet.appFamilybean2.Lname!=undefined && famChildDet.appFamilybean2.Lname!="" &&
                      famChildDet.appFamilybean2.rdFamilyGender!=undefined && famChildDet.appFamilybean2.rdFamilyGender!="" && famChildDet.appFamilybean2.Bdate!=undefined && famChildDet.appFamilybean2.Bdate!="" &&
                      famChildDet.appFamilybean2.MemOccu!="" && famChildDet.appFamilybean2.MemOccu!=undefined && famChildDet.appFamilybean2.MemAnnualInc!=undefined && famChildDet.appFamilybean2.MemAnnualInc!="" &&
                      famChildDet.appFamilybean2.MemInsDet!=undefined && famChildDet.appFamilybean2.MemInsDet!="";
console.log("Family data is filled..."+form.$invalid);
    if(form.$invalid!=true && famChildDet.famCondition!=true)
    {
        console.log("Inside main if:::"+form.$invalid+"Condition is::"+famChildDet.famCondition);
        famChildDet.deleteFun();
    }
    else
    {
        if(famChildDet.famCondition) {
        console.log("Condition is:::"+famChildDet.famCondition+"Records are::"+famChildDet.getSelectedRec+"And childRec are::"+ChildCondition);
            if(FamilyCondition!="" && FamilyCondition!=undefined)
            {


                console.log("FirstChildRec::"+famChildDet.getSelectedChildRec+"SecondChild::"+famChildDet.getSecondChildRec);
                            if(famChildDet.getSelectedChildRec != undefined || (!!famChildDet.FamilyChildArr && famChildDet.FamilyChildArr.length == 1 && !!famChildDet.FamilyChildArr[0].childFname && !!famChildDet.FamilyChildArr[0].childLname))
                            {
                                if(famChildDet.getSecondChildRec!=undefined)
                                {
                                    CommonService.showLoading();
                                    famChildDet.deleteFun();
                                }
                                else
                                {
                                    console.log("inside secondChild popup"+famChildDet.FamilyChildArr.length);
                                    if(famChildDet.FamilyChildArr.length == 1 && famChildDet.FamilyChildArr.length<3)
                                    navigator.notification.confirm("Do you want to continue without another child detalis?",
                                                        function(buttonIndex){
                                                            if(buttonIndex=="2"){
                                                            navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                                            CommonService.hideLoading();
                                                            if(famChildDet.FamilyChildArr.length<2)
                                                                {
                                                                    var newChild = famChildDet.FamilyChildArr.length+1;
                                                                    famChildDet.FamilyChildArr.push({'InsCnt':'0'+newChild});
                                                                    console.log("child added");
                                                                }
                                                                else
                                                                {
                                                                    navigator.notification.alert("You cannot add more than 2 child.",function(){CommonService.hideLoading();},"Application","OK");
                                                                    CommonService.showLoading();
                                                                    famChildDet.deleteFun();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                console.log("form invalid inside second child popup::"+form.$invalid);
                                                                if(form.$invalid!=true)
                                                                    {
                                                                        CommonService.showLoading();
                                                                        famChildDet.deleteFun();
                                                                    }
                                                                else
                                                                        navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                                            }
                                                        },
                                                        'Confirm Family Details',
                                                        'Yes, No'
                                                    );
                                    else
                                    {
                                        console.log("form invalid is::"+form.$invalid);
                                        if(form.$invalid!=true)
                                        {
                                            CommonService.showLoading();
                                            famChildDet.deleteFun();
                                        }
                                        else
                                            navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                    }
                                }
                            }
                             else
                             {
                                console.log("inside firstChild popup");
                                /*if(form.$invalid!=true)
                                {*/
                                    if(famChildDet.FamilyChildArr.length ==1 && (famChildDet.FamilyChildArr[1] == undefined || famChildDet.FamilyChildArr[1].childFname == undefined))
                                    navigator.notification.confirm("Do you want to continue without first child detalis?",
                                        function(buttonIndex){
                                            if(buttonIndex=="2"){
                                            navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                            CommonService.hideLoading();
                                            if(famChildDet.FamilyChildArr.length<2)
                                                {
                                                    console.log("Length is::"+famChildDet.FamilyChildArr.length);
                                                    /*var newChild = famChildDet.FamilyChildArr.length+1;
                                                    famChildDet.FamilyChildArr.push({'InsCnt':'0'+newChild});*/
                                                    console.log("child added");
                                                }
                                                else
                                                {
                                                    console.log("form invalid is inside first child::"+form.$invalid);
                                                    if(form.$invalid!=true)
                                                    {
                                                        navigator.notification.alert("You cannot add more than 2 child.",function(){CommonService.hideLoading();},"Application","OK");
                                                        CommonService.showLoading();
                                                        famChildDet.deleteFun();
                                                    }
                                                    else
                                                        navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                                }
                                            }
                                            else
                                            {
                                                if(famChildDet.FamilyChildArr[0].childFname == undefined)
                                                {
                                                    CommonService.showLoading();
                                                    famChildDet.deleteFun();
                                                }
                                                else
                                                    navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");
                                            }
                                        },
                                        'Confirm Family Details',
                                        'Yes, No'
                                    );
                                    else
                                    {
                                        CommonService.showLoading();
                                        famChildDet.deleteFun();
                                    }
                                /*}
                                else
                                    navigator.notification.alert("Please complete the details",function(){CommonService.hideLoading();},"Application","OK");*/
                             }
            }
            else
            {

                navigator.notification.alert("Father/Husband data is mandatory.",function(){CommonService.hideLoading();},"Application","OK");
                console.log("After filling data::::"+FamilyCondition+"insie childrec::"+ChildCondition+"famChildDet.FamilyChildArr[0].childAnnuInc::"+famChildDet.FamilyChildArr[0].childAnnuInc);
            }
           /* else
            {
                console.log("inside else:::");
                famChildDet.deleteFun();
            }*/
        }
        else
            navigator.notification.alert("Please fill the complete details.",function(){CommonService.hideLoading();},"Application","OK");
    }
}
CommonService.hideLoading();
}]);

familySection2Module.service('appFamilyChildDetService',['$state','ApplicationFormDataService','LoginService','CommonService','$q','nomService','LoadApplicationScreenData','AppTimeService',function($state,ApplicationFormDataService, LoginService, CommonService,$q, nomService, LoadApplicationScreenData,AppTimeService){

var famSrv = this;
famSrv.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;


this.insertFamDetails = function(famChildDet){
var dfd = $q.defer();
var dob = CommonService.formatDobToDb(new Date(famChildDet.appFamilybean2.Bdate));
console.log("Date of Birth::"+dob+"Original format"+JSON.stringify(famChildDet.appFamilybean2));
    if(!!famChildDet.appFamilybean2.Fname && !!famChildDet.appFamilybean2.Lname &&
        !!famChildDet.appFamilybean2.rdFamilyGender && !!dob &&
        !!famChildDet.appFamilybean2.MemOccu && !!famChildDet.appFamilybean2.MemAnnualInc+"" &&
        !!famChildDet.appFamilybean2.MemInsDet+""){
        CommonService.insertOrReplaceRecord(db,'LP_APP_FH_SCRN_E',{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":famSrv.AGENT_CD,"RELATIONSHIP_ID":"C11","FIRST_NAME":famChildDet.appFamilybean2.Fname,"MIDDLE_NAME":famChildDet.appFamilybean2.Mname,"LAST_NAME":famChildDet.appFamilybean2.Lname,"GENDER":famChildDet.appFamilybean2.rdFamilyGender,"BIRTH_DATE":dob,"OCCUPATION_DESC":famChildDet.appFamilybean2.MemOccu,"ANNUAL_INCOME":famChildDet.appFamilybean2.MemAnnualInc+"","INSURANCE_DETAILS":famChildDet.appFamilybean2.MemInsDet+""},true).then(
            function(res){
                if(!!res){
                    console.log("Member inserted");
                    dfd.resolve(res);
                }
                dfd.resolve(null);
            }

        );
    }
    else
	{
		console.log("Member not inserted");
		dfd.resolve(null);
	}

    return dfd.promise;
}

this.insertChildDetails = function(famChildDet){
var dfd = $q.defer();
console.log("famChildDet.FamilyChildArr.length:"+famChildDet.FamilyChildArr.length);
    for(var i=0;i<famChildDet.FamilyChildArr.length;i++){
        (function(i){

        console.log("Child Bdate is::"+JSON.stringify(famChildDet.FamilyChildArr[i]));
            if(famChildDet.FamilyChildArr!=undefined && famChildDet.FamilyChildArr!=""){

            var childDOB = CommonService.formatDobToDb(new Date(famChildDet.FamilyChildArr[i].childBdate));

                if(!!famChildDet.FamilyChildArr[i].childFname && !!famChildDet.FamilyChildArr[i].childLname
                    && !!famChildDet.FamilyChildArr[i].rdFamilyGender1 && !!famChildDet.FamilyChildArr[i].childBdate
                    && !!famChildDet.FamilyChildArr[i].childOccu && !!famChildDet.FamilyChildArr[i].childAnnuInc+""
                    && !!famChildDet.FamilyChildArr[i].childInsdet+""){
                        CommonService.insertOrReplaceRecord(db,'LP_APP_FH_SCRN_E',{"APPLICATION_ID":ApplicationFormDataService.applicationFormBean.APPLICATION_ID,"AGENT_CD":famSrv.AGENT_CD,"RELATIONSHIP_ID":famChildDet.ChildId[i].RELATIONSHIP_ID,"FIRST_NAME":famChildDet.FamilyChildArr[i].childFname,"MIDDLE_NAME":famChildDet.FamilyChildArr[i].childMname,"LAST_NAME":famChildDet.FamilyChildArr[i].childLname,"GENDER":famChildDet.FamilyChildArr[i].rdFamilyGender1,"BIRTH_DATE":childDOB,"OCCUPATION_DESC":famChildDet.FamilyChildArr[i].childOccu,"ANNUAL_INCOME":famChildDet.FamilyChildArr[i].childAnnuInc+"","INSURANCE_DETAILS":famChildDet.FamilyChildArr[i].childInsdet+""},true).then(
                            function(result){
                                if(!!result){
                                    console.log("Child inserted"+i+"famChildDet.ChildId[i].RELATIONSHIP_ID::"+famChildDet.ChildId[i].RELATIONSHIP_ID);
                                    dfd.resolve(result);
                                }
                                dfd.resolve(null);
                            }

                        );


                    }
                    else{
							debug("Condition failed");
							dfd.resolve(null);
					}

                }
            }(i)
        );
    }
    return dfd.promise;
}


this.getFamilyMemRecords = function(){
var dfd = $q.defer();
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famSrv.AGENT_CD;
    whereClauseObj.RELATIONSHIP_ID="C11";
    CommonService.selectRecords(db,'LP_APP_FH_SCRN_E',false,"*",whereClauseObj,"RELATIONSHIP_ID ASC").then(
        function(res){
            var getMemberRec = [];
            console.log("Family Mem Record length::"+JSON.stringify(res));
                if(!!res && res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                    var MemberRec = {};
                    MemberRec.FIRST_NAME = res.rows.item(i).FIRST_NAME;
                    MemberRec.MIDDLE_NAME = res.rows.item(i).MIDDLE_NAME;
                    MemberRec.LAST_NAME = res.rows.item(i).LAST_NAME;
                    MemberRec.GENDER = res.rows.item(i).GENDER;
                    MemberRec.BIRTH_DATE = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                    MemberRec.OCCUPATION_DESC = res.rows.item(i).OCCUPATION_DESC;
                    MemberRec.ANNUAL_INCOME = res.rows.item(i).ANNUAL_INCOME;
                    MemberRec.INSURANCE_DETAILS = res.rows.item(i).INSURANCE_DETAILS;
                    console.log("Array Record::"+JSON.stringify(MemberRec));
                    getMemberRec.push(MemberRec);
                }
                dfd.resolve(getMemberRec);
                console.log("getMemberRec::"+JSON.stringify(getMemberRec));
             }
             dfd.resolve(null);
        }

    );
    return dfd.promise;
}

this.getChildMemRecords = function(){
var dfd = $q.defer();
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famSrv.AGENT_CD;
    whereClauseObj.RELATIONSHIP_ID = "C12";
    CommonService.selectRecords(db,'LP_APP_FH_SCRN_E',false,"*",whereClauseObj,"RELATIONSHIP_ID ASC").then(
        function(res){
            var getChildRec = [];
            console.log("First Record length::"+JSON.stringify(res));
                if(!!res && res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                    var ChildRecord = {};
                    ChildRecord.FIRST_NAME = res.rows.item(i).FIRST_NAME;
                    ChildRecord.MIDDLE_NAME = res.rows.item(i).MIDDLE_NAME;
                    ChildRecord.LAST_NAME = res.rows.item(i).LAST_NAME;
                    ChildRecord.GENDER = res.rows.item(i).GENDER;
                    ChildRecord.BIRTH_DATE = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                    ChildRecord.OCCUPATION_DESC = res.rows.item(i).OCCUPATION_DESC;
                    ChildRecord.ANNUAL_INCOME = res.rows.item(i).ANNUAL_INCOME;
                    ChildRecord.INSURANCE_DETAILS = res.rows.item(i).INSURANCE_DETAILS;
                    console.log("Array Record::"+JSON.stringify(ChildRecord));
                    getChildRec.push(ChildRecord);
                }
                dfd.resolve(getChildRec);
                console.log("getMemberRec::"+JSON.stringify(getChildRec));
             }
             dfd.resolve(null);
        }

    );
    return dfd.promise;
}

this.getSecondChildMemRecords = function(){
var dfd = $q.defer();
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = famSrv.AGENT_CD;
    whereClauseObj.RELATIONSHIP_ID = "C13";
    CommonService.selectRecords(db,'LP_APP_FH_SCRN_E',false,"*",whereClauseObj,"RELATIONSHIP_ID ASC").then(
        function(res){
            var getSecondChildRec = [];
            console.log("Second Record length::"+JSON.stringify(res));
                if(!!res && res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                    var SecChildRecord = {};
                    SecChildRecord.FIRST_NAME = res.rows.item(i).FIRST_NAME;
                    SecChildRecord.MIDDLE_NAME = res.rows.item(i).MIDDLE_NAME;
                    SecChildRecord.LAST_NAME = res.rows.item(i).LAST_NAME;
                    SecChildRecord.GENDER = res.rows.item(i).GENDER;
                    SecChildRecord.BIRTH_DATE = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                    SecChildRecord.OCCUPATION_DESC = res.rows.item(i).OCCUPATION_DESC;
                    SecChildRecord.ANNUAL_INCOME = res.rows.item(i).ANNUAL_INCOME;
                    SecChildRecord.INSURANCE_DETAILS = res.rows.item(i).INSURANCE_DETAILS;
                    console.log("Array Record::"+JSON.stringify(SecChildRecord));
                    getSecondChildRec.push(SecChildRecord);
                }
                dfd.resolve(getSecondChildRec);
                console.log("getSecondChildRec::"+JSON.stringify(getSecondChildRec));
             }
             dfd.resolve(null);
        }

    );
    return dfd.promise;
}

this.updateScreen = function(){
var dfd = $q.defer();
famSrv.isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false);
var whereClauseObj = {};
     whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
     whereClauseObj.AGENT_CD = famSrv.AGENT_CD;
    CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN5_COMPLETED":'Y', "IS_FAM_SEC2":'Y'},whereClauseObj).then(
        function(res){
            console.log("Screen updated successfully :IS_FAM_SEC2"+famSrv.isSelf);
            if(famSrv.isSelf){
                //timeline changes
                CommonService.showLoading();
                AppTimeService.setActiveTab("nomAppDetails");
                AppTimeService.isFamDetCompleted = true;
                //$rootScope.val = 50;
                AppTimeService.setProgressValue(50);
                //$state.go("applicationForm.nomAppDetails.nomineeDetails");
                dfd.resolve(res);
            }
            else
            {
                CommonService.showLoading();
                AppTimeService.setActiveTab("paymentDetails");
                AppTimeService.isFamDetCompleted = true;
                //$rootScope.val = 60;
                AppTimeService.setProgressValue(60);
                famSrv.skipNomineeAppointee();
                dfd.resolve(res);
            }


        }
    );
    return dfd.promise;
}

this.getProposerAsNominee = function(){
var dfd = $q.defer();

    var nomObj = {};
    nomObj.appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    nomObj.username = famSrv.AGENT_CD;
    nomObj.nomCustID = 'C05';
    nomObj.FirstName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_FIRST_NAME;
    nomObj.LastName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_LAST_NAME;
    nomObj.MiddleName = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_MIDDLE_NAME;
    var dob = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_DOB;
    nomObj.dob = CommonService.formatDobFromDb(dob);
    var Relation = {};
    LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
        function(AppData){
                if(!!AppData.applicationPersonalInfo.Insured && !!AppData.applicationPersonalInfo.Insured.RELATIONSHIP_ID)
                {debug("in insured relation");
                    Relation.NOM_RELATIONSHIP_DESC = AppData.applicationPersonalInfo.Insured.RELATIONSHIP_DESC;
                    Relation.NOM_NBFE_CODE = AppData.applicationPersonalInfo.Insured.RELATIONSHIP_ID
                    nomObj.Relation = Relation;
                }
                else
                {debug("in proposer relation");
                    Relation.NOM_RELATIONSHIP_DESC = AppData.applicationPersonalInfo.Proposer.RELATIONSHIP_DESC;
                    Relation.NOM_NBFE_CODE = AppData.applicationPersonalInfo.Proposer.RELATIONSHIP_ID
                    nomObj.Relation = Relation;
                }
                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_GENDER == 'M')
                    nomObj.Gen = 'Male';
                else
                    nomObj.Gen = 'Female';

                nomObj.Percent = 100;

                dfd.resolve(nomObj);
        }
    );


return dfd.promise;

}

this.skipNomineeAppointee = function(){

    debug("inside skipNomineeAppointee");

    famSrv.getProposerAsNominee().then(function(nomObj){

    debug("getProposerAsNominee completed!");

       nomService.nomDataInsert(0, null, nomObj).then(function(resp){

            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            whereClauseObj.AGENT_CD = famSrv.AGENT_CD;
            if(resp)
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN6_COMPLETED" :"NA"}, whereClauseObj).then(
                                    function(res){
                                        debug("IS_SCREEN6_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
                                        $state.go("applicationForm.paymentDetails.bankdetails");
                                    }
                                );



        });

    });

}

this.deleteFatherRec = function(){
var dfd = $q.defer();
console.log("Inside family2 delete function");
var sql1 = "Delete from LP_APP_FH_SCRN_E where APPLICATION_ID=? AND AGENT_CD=? AND RELATIONSHIP_ID IN('C11','C12','C13')"
    CommonService.transaction(db,
        function(tx){
        console.log("inside deleteFatherRec")
            CommonService.executeSql(tx,sql1,[ApplicationFormDataService.applicationFormBean.APPLICATION_ID,famSrv.AGENT_CD],
                function(tx,res){
                    console.log("Family section2 record deleted successfully...");
                    dfd.resolve(true);
                },
                function(tx,err)
                {
                    console.log("Error"+err.message);
                }
            );
        },function(error){
            console.log("Error msg is::"+error.message);
        }
    )
return dfd.promise;
}

}]);
