// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var applicationTabs = angular.module('TabsApp', [])

.controller('TabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Payment Options',
        url: 'paymentOption'
        }, {
        title: 'Bank details',
        url: 'bankDetails'
        }];

    $scope.currentTab = 'paymentOption';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])


/*Nominee Detsils Tabs*/
.controller('nomineeTabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Nominee',
        url: 'nomineeDetails'
        }, {
        title: 'Appointee',
        url: 'appointeeDetails'
        }];

    $scope.currentTab = 'nomineeDetails';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])


/*Personal Info Tabs*/
.controller('personalinfoTabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Personal Details',
        url: 'personalDetails'
        }, {
        title: 'Contact Details',
        url: 'contactDetails'
        }, {
        title: 'Address Details',
        url: 'addressDetsils'
        }, {
        title: 'Education & Qualification Details',
        url: 'eduQualDetails'
        }, {
        title: 'Other Details',
        url: 'otherDetails'
        }];

    $scope.currentTab = 'personalDetails';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])

/*Existing Policy Tabs*/
.controller('existingpolicyTabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Tata Aia',
        url: 'tataAiaPolicy'
        }, {
        title: 'Non Tata Aia',
        url: 'nontataAiaPolicy'
        }];

    $scope.currentTab = 'tataAiaPolicy';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])

/*Family Details Tabs*/
.controller('familydetailsTabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Section 1',
        url: 'familydetailsSectionOne'
        }, {
        title: 'Section 2',
        url: 'familydetailsSectionTwo'
        }];

    $scope.currentTab = 'familydetailsSectionOne';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])

/*Questions Tabs*/
.controller('questionTabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
        title: 'Lifestyle',
        url: 'lifestyleQuestion'
        }, {
        title: 'Health',
        url: 'healthQuestion'
        }];

    $scope.currentTab = 'lifestyleQuestion';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}])



