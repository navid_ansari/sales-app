renewalPaymentModule.controller('renewalPayCtrl',['$state','renPayService','bankDetService','ExistingAppMainData','ApplicationFormDataService','LoginService','$filter','amount','payMode','sisData','existingData','AppTimeService','CommonService','existingDataTermPlan','sisDataTermPlan','bankDataFHR',function($state,renPayService,bankDetService,ExistingAppMainData,ApplicationFormDataService,LoginService,$filter,amount,payMode,sisData,existingData,AppTimeService,CommonService,existingDataTermPlan,sisDataTermPlan,bankDataFHR){
    debug("controller loaded");

    debug("renewal "+JSON.stringify(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST));
    var self = this;
    self.RenPay = {};
    self.termPlan = {};
    debug("bankDataFHR in renewal payment : "+JSON.stringify(bankDataFHR));

        renPayService.getExistingRenewalData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
            function(RenewalPayData){
            if(RenewalPayData)
            {
            debug("RenewalPayData is::"+JSON.stringify(RenewalPayData));
                    self.RenPay.BankClick = RenewalPayData.BankClick;
                    self.RenPay.AccNo = parseInt(RenewalPayData.AccNo);
                    self.RenPay.reenterAccNo = parseInt(RenewalPayData.AccNo);
                    self.RenPay.IfscCode = RenewalPayData.IfscCode;
                    self.RenPay.ReenterIfscCode = RenewalPayData.IfscCode;
                    self.RenPay.BranchName = RenewalPayData.BranchName;
                    self.RenPay.AccClick = RenewalPayData.AccClick;

                    self.termPlan.BankClick = RenewalPayData.BankClick;
                    self.termPlan.AccNo = parseInt(RenewalPayData.AccNo);
                    self.termPlan.reenterAccNo = parseInt(RenewalPayData.AccNo);
                    self.termPlan.IfscCode = RenewalPayData.IfscCode;
                    self.termPlan.ReenterIfscCode = RenewalPayData.IfscCode;
                    self.termPlan.BranchName = RenewalPayData.BranchName;
                    self.RenPay.AccClick = RenewalPayData.AccClick;
            }
            else
            {
        renPayService.getNeftBankPayDetails(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                function(BankDetails)
                {
                    if(BankDetails)
                    {
                    debug("BankDetails are:::"+JSON.stringify(BankDetails));
                        self.RenPay.BankClick = BankDetails.bankName;
                        self.RenPay.AccNo = parseInt(BankDetails.AccNo);
                        self.RenPay.reenterAccNo = parseInt(BankDetails.AccNo);
                        self.RenPay.IfscCode = BankDetails.IfscCode;
                        self.RenPay.ReenterIfscCode = BankDetails.IfscCode;
                        self.RenPay.BranchName = BankDetails.branchname;
                        self.RenPay.AccClick = BankDetails.AccType;

                        self.termPlan.BankClick = BankDetails.bankName;
                        self.termPlan.AccNo = parseInt(BankDetails.AccNo);
                        self.termPlan.reenterAccNo = parseInt(BankDetails.AccNo);
                        self.termPlan.IfscCode = BankDetails.IfscCode;
                        self.termPlan.ReenterIfscCode = BankDetails.IfscCode;
                        self.termPlan.BranchName = BankDetails.branchname;
                        self.RenPay.AccClick = BankDetails.AccType;
                    }
                }
            );
        }
    }
);
   /* if(!!bankDataFHR){
            self.RenPay.BankClick = bankDataFHR.bankName;
            self.RenPay.AccNo = parseInt(bankDataFHR.AccNo);
            self.RenPay.reenterAccNo = parseInt(bankDataFHR.AccNo);
            self.RenPay.IfscCode = bankDataFHR.IfscCode;
            self.RenPay.ReenterIfscCode = bankDataFHR.IfscCode;
            self.RenPay.BranchName = bankDataFHR.branchname;
            self.RenPay.AccClick = bankDataFHR.AccType;

            self.termPlan.BankClick = bankDataFHR.bankName;
            self.termPlan.AccNo = parseInt(bankDataFHR.AccNo);
            self.termPlan.reenterAccNo = parseInt(bankDataFHR.AccNo);
            self.termPlan.IfscCode = bankDataFHR.IfscCode;
            self.termPlan.ReenterIfscCode = bankDataFHR.IfscCode;
            self.termPlan.BranchName = bankDataFHR.branchname;
            self.RenPay.AccClick = bankDataFHR.AccType;
        }*/
    debug("termPlan amount object:"+sisDataTermPlan);


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */


    self.click = false;

    this.nameRegex = USERNAME_REGEX;
    self.ifscRegex = IFSC_REG;
    //self.termPlan is object which holds the data for existing term plan.


    self.COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;

    debug("Amount is "+amount+"-->payMode  "+payMode);

    debug("data after parse int >>"+self.RenPay.amount);


    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

     //set initial all data.
     debug("Account Type:"+renPayService.getAccTypes());
    self.AccTypes = renPayService.getAccTypes();
    self.RenData = renPayService.locRenewalData();
     self.RenPay.RenClick = self.RenData[1];
    debug("existing RenData data is "+JSON.stringify(self.RenData));

    //set bank data..
    bankDetService.getBankNames().then(
        function(bankData){
            self.BankData = bankData;
        }
    );
    self.RenPay.RenewalCheck = "Y";
	if(sisData.sisFormBData.PREMIUM_PAY_MODE=="O")
		self.RenPay.RenewalCheck = "N";
    if(existingData){
        debug("existing data is "+JSON.stringify(existingData));
        if(existingData.RenewalCheck == "N"){
            debug("data is null only no selected");
            self.RenPay = existingData;
        }else
            self.RenPay = existingData;
    }else{
        debug("no data in database");
    }



    if(existingDataTermPlan){
        debug("existingDataTermPlan data is "+JSON.stringify(existingDataTermPlan));
        if(existingDataTermPlan.RenewalCheckTermPlan == "N"){
            debug("data is null only no selected");
            self.termPlan = existingDataTermPlan;
        }else
            self.termPlan = existingDataTermPlan;
    }else{
        debug("no data in database");
    }


    self.RenPay.amount = parseInt(amount);




    if(!!sisDataTermPlan)
        self.termPlan.amount = parseInt(sisDataTermPlan.MODAL_PREMIUM) + parseInt(sisDataTermPlan.SERVICE_TAX);

   // debug("termPlan amount :"+self.termPlan.amount+" sisDataTermPlan MODAL_PREMIUM "+sisDataTermPlan.MODAL_PREMIUM)

    //create blank object to add docids..
    ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
        Reflex :{
            Insured:{},
            Term:{},
            Proposer:{}
        },
        NonReflex :{
            Insured:{},
            Term:{},
            Proposer:{}
        }
    };
    //29th june..
    if(!!ExistingAppMainData && !!ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST && Object.keys(ExistingAppMainData.applicationPersonalInfo.Insured).length!=0){

        ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
        debug("existing data "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    }else{
        debug("existing object is blank");
    }



    debug("existing RenPay data is "+JSON.stringify(self.RenPay));
    debug("existing existingData data is "+JSON.stringify(existingData));

    self.renewalChange = function(){
        debug(sisData.sisFormBData.PREMIUM_PAY_MODE);
        if(!!sisData.sisFormBData.PREMIUM_PAY_MODE){
            var paym = sisData.sisFormBData.PREMIUM_PAY_MODE;
            debug("PAY MODE IN SIS "+paym);
            if(!!paym){
                if(paym == "O"){
                    debug("single pay");
                    self.RenPay.RenewalCheck = "N";
                    navigator.notification.alert("Not Allowed as Single pay was selected",function(){CommonService.hideLoading();},"Application","OK");
                }
            }
        }
    }

    self.renewalChangeTermPlan = function(){
        //on select of renewal of change of term plan
        debug("Json object on "+JSON.stringify(sisData.sisFormBData));
        debug(sisData.sisFormBData.PREMIUM_PAY_MODE);
        if(!!sisData.sisFormBData.PREMIUM_PAY_MODE){
            var paym = sisData.sisFormBData.PREMIUM_PAY_MODE;
            debug("PAY MODE IN SIS "+paym);
            if(!!paym){
                if(paym == "O"){
                    debug("single pay");
                    self.RenPay.RenewalCheckTermPlan = "N";
                    navigator.notification.alert("Not Allowed as Single pay was selected",function(){CommonService.hideLoading();},"Application","OK");
                }
            }
        }
    }

     self.SaveClick = function(RenPayForm){
		CommonService.showLoading();
        self.click = true;
        debug("RenPayForm data "+RenPayForm.$invalid);
         debug("SaveClick  ApplicationFormDataService.applicationFormBean.DOCS_LIST "+self.termPlan.RenClickTermPlan+" APPLICATION RENEWAL PAY "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        if(RenPayForm.$invalid != true){
            var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            var appid =ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            self.RenPay.APPLICATION_ID = appid;
            self.RenPay.AGENT_CD = agentCD;
            self.termPlan.AGENT_CD= agentCD;
            if(ApplicationFormDataService.applicationFormBean.POLICY_NO){
                self.RenPay.POLICY_NO = ApplicationFormDataService.applicationFormBean.POLICY_NO;
            }else{
                self.RenPay.POLICY_NO = "";
            }
            // self.RenPay.POLICY_NO  = (ApplicationFormDataService.applicationFormBean.POLICY_NO)?ApplicationFormDataService.applicationFormBean.POLICY_NO:"";

            if(payMode){
                debug("value set is"+payMode);
                self.RenPay.payMode = payMode;
                 self.termPlan.payMode=payMode;
            }else{
                debug("sisdata set is"+sisData.sisFormBData.PREMIUM_PAY_MODE);
                self.RenPay.payMode = sisData.sisFormBData.PREMIUM_PAY_MODE;
                self.termPlan.payMode=sisData.sisFormBData.PREMIUM_PAY_MODE;
            }

            debug("data is "+JSON.stringify(self.RenPay));

            //changes 29th june..
            //check if renewal check is yes or no..
            var empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
            if(empFlag != "E"){
                if(!!self.COMBO_ID){
                     self.termPlan.COMBO_ID=self.COMBO_ID;
                    //Check if the COMBO_ID Present then check for user selected type then based on that perform further operations.
                        self.validateTermPlanAndSaveData();

                }else{
                    //when term plan is not visible.
                    if(self.RenPay.RenewalCheck == "N"){
                        renPayService.InsertNoFlagData(self.RenPay).then(
                            function(inserted){
                                 self.ChangeStateToPolicyNoGen();
                            }
                        );
                    }else{
                        renPayService.insert(self.RenPay).then(
                            function(inserted){
                                if(inserted){
                                     self.ChangeStateToPolicyNoGen();
                                }
                            }
                        );
                    }
                }
            }else{
                navigator.notification.alert("You are not allowed to do payment.",function() {
                    self.ChangeStateToPolicyNoGen();
                },"Application","OK");
            }


        }else{
            navigator.notification.alert('Please fill all fields with valid data',function(){CommonService.hideLoading();},"Application","OK");
        }
    };

    /**
    *function to validate selection type of term plan nd base plane
    *base on selection first base plan data is save and then term plan data is stored in db
    **/
    self.validateTermPlanAndSaveData = function(){

        debug(" termPlan combo id: "+self.COMBO_ID);
        debug("Term plan object : "+JSON.stringify(self.termPlan));
        if(self.RenPay.RenewalCheck == "N" && self.termPlan.RenewalCheckTermPlan == "Y"){
            renPayService.InsertNoFlagData(self.RenPay).then(
                function(inserted){
                    // add insert Yes into term plan
                    renPayService.insertTermPlan(self.termPlan).then(
                        function(res){
                             self.ChangeStateToPolicyNoGen();
                        }
                    );
                }
            );
        }else if(self.RenPay.RenewalCheck == "N" && self.termPlan.RenewalCheckTermPlan == "N"){

            renPayService.InsertNoFlagData(self.RenPay).then(
                function(inserted){
                    // add insert No into term plan

                  renPayService.InsertNoFlagDataTermData(self.termPlan).then(
                        function(res){

                             self.ChangeStateToPolicyNoGen();
                        }
                    );
                }
            );
        }else if(self.RenPay.RenewalCheck == "Y" && self.termPlan.RenewalCheckTermPlan == "N"){
            renPayService.insert(self.RenPay).then(
                function(inserted){
                    // add insert No into term plan
                    renPayService.InsertNoFlagDataTermData(self.termPlan).then(
                        function(res){
                             self.ChangeStateToPolicyNoGen();
                        }
                    );
                }
            );
        }else if(self.RenPay.RenewalCheck == "Y" && self.termPlan.RenewalCheckTermPlan == "Y") {
            renPayService.insert(self.RenPay).then(
                function(inserted){
                    if(inserted){
                        renPayService.insertTermPlan(self.termPlan).then(
                            function(res){
                                self.ChangeStateToPolicyNoGen();
                            }
                        );
                    }
                }
            );
        }
    };


    self.ChangeStateToPolicyNoGen = function(){
            renPayService.updateDocListAndScreenFlag(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj);
            debug("move to page");

            AppTimeService.setActiveTab("policyNoGen");
            AppTimeService.isPayDetCompleted = true;

            AppTimeService.setProgressValue(70);
            $state.go('applicationForm.policyNoGen');
    };

    self.onChangeRenData = function(){

 debug("selection "+self.RenPay.RenClick);
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.renewalPay = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.renewalPay = {};

        renPayService.getRenewalData().then(
            function(data){
                debug("json is "+JSON.stringify(data));
                var renobj = $filter('filter')(data,{ MPPROOF_CODE:self.RenPay.RenClick.MPPROOF_CODE })[0];
                debug("ren obj "+JSON.stringify(renobj));
                navigator.notification.alert(renobj.MPPROOF_DESC +" form needs to be submitted",function(){CommonService.hideLoading();},"Application","OK");
                //if(renobj.DIGITAL_HTML_FLAG){
                    if(renobj.DIGITAL_HTML_FLAG == "Y")
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.renewalPay.DOC_ID = renobj.DOC_ID;
                    else
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.renewalPay.DOC_ID = renobj.DOC_ID;
                //}
            }
        );
    };


    self.onChangeRenDataTermPlan = function(){
        debug("selection "+self.termPlan.RenClickTermPlan+" APPLICATION RENEWAL PAY "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term.renewalPay = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.renewalPay = {};

        renPayService.getRenewalData().then(
            function(data){
                debug("onChangeRenDataTermPlan json is "+JSON.stringify(data));
                var renobj = $filter('filter')(data,{ MPPROOF_CODE:self.termPlan.RenClickTermPlan.MPPROOF_CODE })[0];
                debug("onChangeRenDataTermPlan ren obj "+JSON.stringify(renobj));
                navigator.notification.alert(renobj.MPPROOF_DESC +" form needs to be submitted",function(){CommonService.hideLoading();},"Application","OK");
                //if(renobj.DIGITAL_HTML_FLAG){
                    if(renobj.DIGITAL_HTML_FLAG == "Y")
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term.renewalPay.DOC_ID = renobj.DOC_ID;
                    else
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.renewalPay.DOC_ID = renobj.DOC_ID;
                //}

                        debug("On Chnage APPLICATION RENEWAL PAY "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

            }
        );
    };
    CommonService.hideLoading();
}]);

renewalPaymentModule.service('renPayService',['$state','bankDetService','$q','CommonService','$filter',function($state,bankDetService,$q,CommonService,$filter){

    var self = this;

    self.locRenewalData = function(){
        var renArr = ["NACH","Standing Instruction"];
        var renID = ["RN","RS"];
        var renData = [];
        for(var i=0;i<renArr.length;i++){
            var renObj = {};
            renObj.MPPROOF_DESC = renArr[i];
            renObj.MPPROOF_CODE = renID[i];
            renData.push(renObj);
        }
        return renData;
    }

    self.insert = function(dataToInsert){
        var dfd = $q.defer();
        //debug("data selection "+dataToInsert.RenClick.MPPROOF_DESC);
        debug("data selection "+JSON.stringify(dataToInsert));
        var query = "";
        var parameters = "";
        switch(dataToInsert.RenClick.MPPROOF_DESC){
            case "Standing Instruction" :
                    query = "insert or replace into LP_APP_PAY_NEFT_SCRN_I(APPLICATION_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_SI_BANK_ACCOUNT_NO,RYP_SI_BANK_ACCOUNT_TYPE,RYP_SI_BANK_NAME,RYP_SI_BANK_IFSC_CODE,RYP_SI_BANK_BRANCH_NAME,RYP_SI_AMOUNT) values(?,?,?,?,?,?,?,?,?,?,?)";
                    parameters = [dataToInsert.APPLICATION_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,"Y","Y",dataToInsert.AccNo,dataToInsert.AccClick,dataToInsert.BankClick.bankName,dataToInsert.IfscCode,dataToInsert.BranchName,dataToInsert.amount];
                break;
            case "NACH" :
                    query = "insert or replace into LP_APP_PAY_NEFT_SCRN_I(APPLICATION_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_NACH_FLAG,RYP_NACH_BANK_ACCOUNT_NO,RYP_NACH_BANK_ACCOUNT_TYPE,RYP_NACH_BANK_NAME,RYP_NACH_BANK_IFSC_CODE,RYP_NACH_BANK_BRANCH_NAME,RYP_NACH_AMOUNT) values(?,?,?,?,?,?,?,?,?,?,?)";
                    parameters = [dataToInsert.APPLICATION_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,"Y","Y",dataToInsert.AccNo,dataToInsert.AccClick,dataToInsert.BankClick.bankName,dataToInsert.IfscCode,dataToInsert.BranchName,dataToInsert.amount];
                break;
            default :
                break;
        }
        debug("param --"+parameters);
        self.insertData(query,parameters).then(
            function(inserted){
                if(inserted){
                    debug("successfully inserted");
                    dfd.resolve(true);
                }else{
                    debug("error in insertion");
                    dfd.resolve(false);
                }
            }
        );

        return dfd.promise;
    };

        //29th june..
    self.InsertNoFlagData = function(dataToInsert){
        var dfd = $q.defer();
        var query = "insert or replace into LP_APP_PAY_NEFT_SCRN_I(APPLICATION_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_AUTOPAY_NACH_FLAG) values(?,?,?,?,?,?)";
        var parameters = [dataToInsert.APPLICATION_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,dataToInsert.RenewalCheck,"N","N"];

        debug("param --"+parameters);
        self.insertData(query,parameters).then(
            function(inserted){
                if(inserted){
                    debug("successfully inserted no flag data");
                    dfd.resolve(true);
                }else{
                    debug("error in insertion no flag data");
                    dfd.resolve(false);
                }
            }
        );

        return dfd.promise;
    }



    //common query to insert
    self.insertData = function(query,params){
        var dfd = $q.defer();
        debug("insert data --"+params+"--Query is "+query);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,params,
                                            function(tx,res){
                                                debug("query executed successfully for pay options");
                                                //inserted = true;
                                                dfd.resolve(true);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for pay options"+err);
                                                debug(JSON.stringify(err));
                                                dfd.resolve(false);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error in payoption"+err.message);
                dfd.resolve(false);
            },
        null);
        return dfd.promise;
    };



    self.getRenewalData = function(){
        var dfd = $q.defer();
        var renewalArr = [];
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select DOC_ID,MPPROOF_DESC,DIGITAL_HTML_FLAG,MPPROOF_CODE from LP_DOC_PROOF_MASTER where MPDOC_CODE=? AND ISACTIVE='1'",["SI"],
                                function(tx,res){
                                    debug("payment arr res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            var renObj = {};
                                            renObj.DOC_ID = res.rows.item(i).DOC_ID;
                                            renObj.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
                                            renObj.DIGITAL_HTML_FLAG = res.rows.item(i).DIGITAL_HTML_FLAG;
                                            renObj.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                            renewalArr.push(renObj);
                                        }
                                        dfd.resolve(renewalArr);
                                    }
                                    else
                                        dfd.resolve(renewalArr);
                                },
                                function(tx,err){
                                    dfd.resolve(renewalArr);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(renewalArr);
                        },null
                    );
        return dfd.promise;
    };

    self.getAccTypes = function(){
        debug("getAccTypes");
        var accTypes = ["Current","Saving","NRO"];
        return accTypes;
    };

    self.getExistingRenewalData = function(APPLICATION_ID,AGENT_CD){
        var dfd = $q.defer();

        var query = "select RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_AUTOPAY_NACH_FLAG from LP_APP_PAY_NEFT_SCRN_I where APPLICATION_ID = ? and AGENT_CD= ?";
        var parameters = [APPLICATION_ID,AGENT_CD];

        debug("the parameters "+JSON.stringify(parameters));

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,res){
                                                debug("query executed successfully for pay options");
                                                //inserted = true;
                                                var renObj = {};
                                                debug("the result of flags "+JSON.stringify(res.rows.item(0)));
                                                if(!!res && res.rows.length > 0){
                                                        renObj.APPLICATION_ID = APPLICATION_ID;
                                                        renObj.AGENT_CD = AGENT_CD;

                                                    if(res.rows.item(0).RYP_AUTOPAY_FLAG){
                                                        if(res.rows.item(0).RYP_AUTOPAY_FLAG == "Y"){
                                                            renObj.RenewalCheck = "Y";
                                                        }else{
                                                            renObj.RenewalCheck = "N";
                                                        }

                                                        if(res.rows.item(0).RYP_AUTOPAY_SI_FLAG == "Y"){
                                                            renObj.RenClick = $filter('filter')(self.locRenewalData(), {MPPROOF_CODE : "RS"})[0];
                                                        }
                                                        if(res.rows.item(0).RYP_AUTOPAY_NACH_FLAG == "Y"){
                                                            renObj.RenClick = $filter('filter')(self.locRenewalData(), {MPPROOF_CODE : "RN"})[0];
                                                        }
                                                        debug("object on selection and passed to method"+JSON.stringify(renObj));
                                                        if(renObj.RenewalCheck == "Y"){
                                                            self.selectDataBaseOnRenewal(renObj).then(
                                                                function(data){
                                                                    dfd.resolve(data);
                                                                }
                                                            );
                                                        }else{
                                                            //nothing was selected..
                                                            debug("renewal selected was no");
                                                            dfd.resolve(renObj);
                                                        }
                                                    }else{
                                                        dfd.resolve(null);
                                                    }
                                                }else{
                                                    dfd.resolve(null);
                                                }
                                            },
                                            function(tx,err){
                                                debug("query execution failed for pay options"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error in payoption"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    }

    self.selectDataBaseOnRenewal = function(renObj){
        var dfd = $q.defer();
        var renewalObj = renObj;

         debug("RECEIVED FROM method"+JSON.stringify(renewalObj));
         debug("database selected value is "+renewalObj.RenClick.MPPROOF_DESC);
         var query = "";
         var parameters = "";
         //var renObj= {};
         switch(renewalObj.RenClick.MPPROOF_DESC){
            case "Standing Instruction":
                   query ="select PREMIUM_PAY_MODE,RYP_SI_BANK_ACCOUNT_NO,RYP_SI_BANK_ACCOUNT_TYPE,RYP_SI_BANK_NAME,RYP_SI_BANK_IFSC_CODE,RYP_SI_BANK_BRANCH_NAME,RYP_SI_AMOUNT from LP_APP_PAY_NEFT_SCRN_I where APPLICATION_ID=? and AGENT_CD = ?";
                   parameters = [renewalObj.APPLICATION_ID,renewalObj.AGENT_CD];
                   debug("paramaters select "+JSON.stringify(parameters));
                  self.getDisplayData(query,parameters).then(
                    function(data){
                        debug(data.rows.length+"response is "+JSON.stringify(data.rows.item(0)));
                        for(var i=0;i<data.rows.length;i++){
                            var resp = data.rows.item(i);
                            debug("response is "+JSON.stringify(resp));
                            (function(i){
                                renewalObj.payMode = resp.PREMIUM_PAY_MODE;
                                renewalObj.AccNo = parseInt(resp.RYP_SI_BANK_ACCOUNT_NO);
                                renewalObj.AccClick = resp.RYP_SI_BANK_ACCOUNT_TYPE;
                                bankDetService.getBankNames().then(
                                    function(names){
                                        renewalObj.BankClick = $filter('filter')(names,{bankName:resp.RYP_SI_BANK_NAME})[0];
                                        debug("Saved bank is::"+JSON.stringify(renewalObj.BankClick));
                                        renewalObj.IfscCode = resp.RYP_SI_BANK_IFSC_CODE;
                                        renewalObj.ReenterIfscCode = resp.RYP_SI_BANK_IFSC_CODE;
                                        renewalObj.BranchName = resp.RYP_SI_BANK_BRANCH_NAME;
                                        renewalObj.amount = parseInt(resp.RYP_SI_AMOUNT);
                                        debug("ren obj is"+JSON.stringify(renewalObj));
                                        if(i==data.rows.length-1)
                                            dfd.resolve(renewalObj);
                                    }
                                );
                            }(i));
                        }
                        if(data.rows.length == 0)
                            dfd.resolve(renewalObj);

                    }
                  );

                break;
            case "NACH":
                   query ="select PREMIUM_PAY_MODE,RYP_NACH_BANK_ACCOUNT_NO,RYP_NACH_BANK_ACCOUNT_TYPE,RYP_NACH_BANK_NAME,RYP_NACH_BANK_IFSC_CODE,RYP_NACH_BANK_BRANCH_NAME,RYP_NACH_AMOUNT from LP_APP_PAY_NEFT_SCRN_I where APPLICATION_ID=? and AGENT_CD = ?";
                   parameters = [renewalObj.APPLICATION_ID,renewalObj.AGENT_CD];
                   debug("paramaters select "+JSON.stringify(parameters));
                  self.getDisplayData(query,parameters).then(
                    function(data){
                        debug("response is "+JSON.stringify(data.rows.item(0)));
                        for(var i=0;i<data.rows.length;i++){
                            var resp = data.rows.item(i);

                            (function(i){
                                renewalObj.payMode = resp.PREMIUM_PAY_MODE;
                                renewalObj.AccNo = parseInt(resp.RYP_NACH_BANK_ACCOUNT_NO);
                                renewalObj.AccClick = resp.RYP_NACH_BANK_ACCOUNT_TYPE;
                                bankDetService.getBankNames().then(
                                    function(names){
                                        renewalObj.BankClick = $filter('filter')(names,{bankName:resp.RYP_NACH_BANK_NAME})[0];
                                    }
                                );
                                renewalObj.IfscCode = resp.RYP_NACH_BANK_IFSC_CODE;
                                renewalObj.ReenterIfscCode = resp.RYP_NACH_BANK_IFSC_CODE;
                                renewalObj.BranchName = resp.RYP_NACH_BANK_BRANCH_NAME;
                                renewalObj.amount = parseInt(resp.RYP_NACH_AMOUNT);
                                debug("ren obj is"+JSON.stringify(renewalObj));
                            }(i));
                        }
                        dfd.resolve(renewalObj);
                    }
                  );
                break;
            default:
               break;
         }

         return dfd.promise;
    };

    //common query for get data...
    self.getDisplayData = function(query,params){
        var dfd = $q.defer();

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,params,
                                            function(tx,res){
                                                debug("query executed successfully for appt");
                                                //inserted = true;
                                                //if(res.rows.length > 0)
                                                    dfd.resolve(res);
                                               // else
                                                    //dfd.resolve(null);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };

    //update flag and insert doclists...
    this.updateDocListAndScreenFlag = function(docslist,whereClause){
        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(docslist)}, whereClause).then(
            function(res){
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN7_COMPLETED" :"Y"}, whereClause).then(
                    function(res){
                        debug("IS_SCREEN7_COMPLETED update success !!"+JSON.stringify(whereClause));
                    }
                );
            }
        );
    };

    this.getAmount = function(AGENT_CD,APPLICATION_ID){
        var dfd = $q.defer();
        var query = "select TOTAL_PAYMENT_AMOUNT from LP_APP_PAY_DTLS_SCRN_G where AGENT_CD =? and APPLICATION_ID = ?";
        var parameters = [AGENT_CD,APPLICATION_ID];
        var amount = 0;

        self.getDisplayData(query,parameters).then(
            function(data){
                if(!!data){
                    debug("AMOUNT IN GET AMOUNT FUNCTION >>"+data);
					if(!!data.rows.item(0))
                    	amount = parseInt(data.rows.item(0).TOTAL_PAYMENT_AMOUNT);
                    dfd.resolve(amount);
                }
            }
        )
        return dfd.promise;

    };

    /**** function related Term plan starts here ****/
    /**
    *function to save flag no term plan renuwal to the
    *Here dataToIneset is the input parameter which is consist of json object
    **/

    self.InsertNoFlagDataTermData = function(dataToInsert){
        var dfd = $q.defer();
        var query = "insert or replace into LP_COMBO_PAY_NEFT_SCRN_I(COMBO_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_AUTOPAY_NACH_FLAG) values(?,?,?,?,?,?)";
        var parameters = [dataToInsert.COMBO_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,dataToInsert.RenewalCheckTermPlan,"N","N"];
        debug("query --"+query);
        debug("param --"+parameters);
        self.insertTermData(query,parameters).then(
            function(inserted){
                if(inserted){
                    debug("successfully inserted no flag data");
                    dfd.resolve(true);
                }else{
                    debug("error in insertion no flag data");
                    dfd.resolve(false);
                }
            }
        );

        return dfd.promise;
    }

    /**
    *function to check the type of the plan selected. based on selection the query and parameters are redered.
    *Here dataToIneset is the input parameter which is consist of json object
    **/

    self.insertTermPlan = function(dataToInsert){
        var dfd = $q.defer();
        //debug("data selection "+dataToInsert.RenClick.MPPROOF_DESC);
        debug("data selection insert Term Plan"+JSON.stringify(dataToInsert));
        var query = "";
        var parameters = "";
        switch(dataToInsert.RenClickTermPlan.MPPROOF_DESC){
            case "Standing Instruction" :
                    query = "insert or replace into LP_COMBO_PAY_NEFT_SCRN_I(COMBO_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_SI_BANK_ACCOUNT_NO,RYP_SI_BANK_ACCOUNT_TYPE,RYP_SI_BANK_NAME,RYP_SI_BANK_IFSC_CODE,RYP_SI_BANK_BRANCH_NAME,RYP_SI_AMOUNT) values(?,?,?,?,?,?,?,?,?,?,?)";
                    parameters = [dataToInsert.COMBO_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,"Y","Y",dataToInsert.AccNo,dataToInsert.AccClick,dataToInsert.BankClick.bankName,dataToInsert.IfscCode,dataToInsert.BranchName,dataToInsert.amount];
                break;
            case "NACH" :
                    query = "insert or replace into LP_COMBO_PAY_NEFT_SCRN_I(COMBO_ID,AGENT_CD,PREMIUM_PAY_MODE,RYP_AUTOPAY_FLAG,RYP_AUTOPAY_NACH_FLAG,RYP_NACH_BANK_ACCOUNT_NO,RYP_NACH_BANK_ACCOUNT_TYPE,RYP_NACH_BANK_NAME,RYP_NACH_BANK_IFSC_CODE,RYP_NACH_BANK_BRANCH_NAME,RYP_NACH_AMOUNT) values(?,?,?,?,?,?,?,?,?,?,?)";
                    parameters = [dataToInsert.COMBO_ID,dataToInsert.AGENT_CD,dataToInsert.payMode,"Y","Y",dataToInsert.AccNo,dataToInsert.AccClick,dataToInsert.BankClick.bankName,dataToInsert.IfscCode,dataToInsert.BranchName,dataToInsert.amount];
                break;
            default :
                break;
        }
        debug("param --"+parameters);
        self.insertTermData(query,parameters).then(
            function(inserted){
                if(inserted){
                    debug("successfully inserted");
                    dfd.resolve(true);
                }else{
                    debug("error in insertion");
                    dfd.resolve(false);
                }
            }
        );

        return dfd.promise;
    };

/**
    *function to check the type of the plan selected. based on selection the query and parameters are redered and .
    *Here dataToIneset is the input parameter which is consist of json object
    **/
    self.insertTermData = function(query,params){
        var dfd = $q.defer();
        debug("insert data --"+params+"--Query is "+query);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,params,
                                            function(tx,res){
                                                debug("query executed successfully for pay options");
                                                //inserted = true;
                                                dfd.resolve(true);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for pay options"+err);
                                                dfd.resolve(false);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error in payoption"+err.message);
                dfd.resolve(false);
            },
        null);
        return dfd.promise;
    };

    /**
    *function to get the the existing saved data from the server
    *Here input values are COMBO_ID and Agent_CD
    **/


    self.getExistingRenewalDataTermPlan = function(COMBO_ID,AGENT_CD){
        var dfd = $q.defer();

        var query = "select RYP_AUTOPAY_FLAG,RYP_AUTOPAY_SI_FLAG,RYP_AUTOPAY_NACH_FLAG from LP_COMBO_PAY_NEFT_SCRN_I where COMBO_ID = ? and AGENT_CD= ?";
        var parameters = [COMBO_ID,AGENT_CD];
        debug("query "+JSON.stringify(query));
        debug("the parameters "+JSON.stringify(parameters));

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,res){
                                                debug("query executed successfully for pay options");
                                                debug("get Existing Renewal Data Term Plan "+JSON.stringify(res.rows.item(0)));
                                                //inserted = true;
                                                var renObj = {};
                                                debug("the result of flags "+JSON.stringify(res.rows.item(0)));
                                                if(!!res && res.rows.length > 0){
                                                        //dfd.resolve(res);
                                                       renObj.COMBO_ID = COMBO_ID;
                                                       renObj.AGENT_CD = AGENT_CD;

                                                    if(res.rows.item(0).RYP_AUTOPAY_FLAG){
                                                        if(res.rows.item(0).RYP_AUTOPAY_FLAG == "Y"){
                                                            renObj.RenewalCheckTermPlan = "Y";
                                                        }else{
                                                            renObj.RenewalCheckTermPlan = "N";
                                                        }

                                                        if(res.rows.item(0).RYP_AUTOPAY_SI_FLAG == "Y"){
                                                            renObj.RenClickTermPlan = $filter('filter')(self.locRenewalData(), {MPPROOF_CODE : "RS"})[0];
                                                        }
                                                        if(res.rows.item(0).RYP_AUTOPAY_NACH_FLAG == "Y"){
                                                            renObj.RenClickTermPlan = $filter('filter')(self.locRenewalData(), {MPPROOF_CODE : "RN"})[0];
                                                        }
                                                        debug("object on selection and passed to method Term plan"+JSON.stringify(renObj));
                                                        if(renObj.RenewalCheckTermPlan == "Y"){
                                                            self.selectDataBaseOnRenewalTermPlan(renObj).then(
                                                                function(data){
                                                                    dfd.resolve(data);
                                                                }
                                                            );
                                                        }else{
                                                            //nothing was selected..
                                                            debug("renewal selected was no");
                                                            dfd.resolve(renObj);
                                                        }
                                                    }else{
                                                        dfd.resolve(null);
                                                    }
                                                }else{
                                                    dfd.resolve(null);
                                                }
                                            },
                                            function(tx,err){
                                                debug("query execution failed for pay options"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error in payoption"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    }



    /**
    *function to get the the existing saved detail data such as IfscCode,ReenterIfscCode,BranchName,and AccNo and insert into object and return it.
    *Here input is renObj which is coming from getExistingRenewalDataTermPlan
    **/

    self.selectDataBaseOnRenewalTermPlan = function(renObj){
        var dfd = $q.defer();
        var renewalObj = renObj;

         debug("RECEIVED FROM method"+JSON.stringify(renewalObj));
         debug("database selected value is "+renewalObj.RenClickTermPlan.MPPROOF_DESC);
         var query = "";
         var parameters = "";
         //var renObj= {};
         switch(renewalObj.RenClickTermPlan.MPPROOF_DESC){
            case "Standing Instruction":
                   query ="select PREMIUM_PAY_MODE,RYP_SI_BANK_ACCOUNT_NO,RYP_SI_BANK_ACCOUNT_TYPE,RYP_SI_BANK_NAME,RYP_SI_BANK_IFSC_CODE,RYP_SI_BANK_BRANCH_NAME,RYP_SI_AMOUNT from LP_COMBO_PAY_NEFT_SCRN_I where COMBO_ID=? and AGENT_CD = ?";
                   parameters = [renewalObj.COMBO_ID,renewalObj.AGENT_CD];
                   debug("paramaters select "+JSON.stringify(parameters));
                  self.getDisplayData(query,parameters).then(
                    function(data){
                        debug("response is "+JSON.stringify(data.rows.item(0)));
                        for(var i=0;i<data.rows.length;i++){
                            var resp = data.rows.item(i);
                            debug("response is "+JSON.stringify(resp));
                            (function(i){
                                renewalObj.payMode = resp.PREMIUM_PAY_MODE;
                                renewalObj.AccNo = parseInt(resp.RYP_SI_BANK_ACCOUNT_NO);
                                renewalObj.AccClick = resp.RYP_SI_BANK_ACCOUNT_TYPE;
                                bankDetService.getBankNames().then(
                                    function(names){
                                        renewalObj.BankClick = $filter('filter')(names,{bankName:resp.RYP_SI_BANK_NAME})[0];
                                    }
                                );
                                renewalObj.IfscCode = resp.RYP_SI_BANK_IFSC_CODE;
                                renewalObj.ReenterIfscCode = resp.RYP_SI_BANK_IFSC_CODE;
                                renewalObj.BranchName = resp.RYP_SI_BANK_BRANCH_NAME;
                                renewalObj.amount = parseInt(resp.RYP_SI_AMOUNT);
                                debug("ren obj is term plan"+JSON.stringify(renewalObj));
                            }(i));
                        }

                        dfd.resolve(renewalObj);
                    }
                  );

                break;
            case "NACH":
                   query ="select PREMIUM_PAY_MODE,RYP_NACH_BANK_ACCOUNT_NO,RYP_NACH_BANK_ACCOUNT_TYPE,RYP_NACH_BANK_NAME,RYP_NACH_BANK_IFSC_CODE,RYP_NACH_BANK_BRANCH_NAME,RYP_NACH_AMOUNT from LP_COMBO_PAY_NEFT_SCRN_I where COMBO_ID=? and AGENT_CD = ?";
                   parameters = [renewalObj.COMBO_ID,renewalObj.AGENT_CD];
                   debug("paramaters select "+JSON.stringify(parameters));
                  self.getDisplayData(query,parameters).then(
                    function(data){
                        debug("response is "+JSON.stringify(data.rows.item(0)));
                        for(var i=0;i<data.rows.length;i++){
                            var resp = data.rows.item(i);

                            (function(i){
                                renewalObj.payMode = resp.PREMIUM_PAY_MODE;
                                renewalObj.AccNo = parseInt(resp.RYP_NACH_BANK_ACCOUNT_NO);
                                renewalObj.AccClick = resp.RYP_NACH_BANK_ACCOUNT_TYPE;
                                bankDetService.getBankNames().then(
                                    function(names){
                                        renewalObj.BankClick = $filter('filter')(names,{bankName:resp.RYP_NACH_BANK_NAME})[0];
                                    }
                                );
                                renewalObj.IfscCode = resp.RYP_NACH_BANK_IFSC_CODE;
                                renewalObj.ReenterIfscCode = resp.RYP_NACH_BANK_IFSC_CODE;
                                renewalObj.BranchName = resp.RYP_NACH_BANK_BRANCH_NAME;
                                renewalObj.amount = parseInt(resp.RYP_NACH_AMOUNT);
                                debug("ren obj is term plan"+JSON.stringify(renewalObj));
                            }(i));
                        }
                        dfd.resolve(renewalObj);
                    }
                  );
                break;
            default:
               break;
         }

         return dfd.promise;
    };




     /**
    * Function to get the data (REF_SIS_IS) from the LP_MYOPPORTUNITY based on the COMBO_ID
    * Input parameters are COMBO_id
    **/

    self.getDataFrom_LP_MYOPPORTUNITY = function(COMBO_ID){
        var dfd = $q.defer();
       query ="select REF_SIS_ID from LP_MYOPPORTUNITY where COMBO_ID=?";
        parameters = [COMBO_ID];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,res){
                                                debug("COMBO_ID "+COMBO_ID+"query executed successfully for appt "+res);
                                                debug(res);
                                                //inserted = true;
                                                if(res.rows.length > 0){

                                                // self.setTermPlanAmount(res.rows.item(0));
                                                    debug("getDataFrom_LP_MYOPPORTUNITY Reponce "+JSON.stringify(res.rows.item(0).REF_SIS_ID));
                                                     var REF_SIS_ID = res.rows.item(0).REF_SIS_ID;



                                                       self.getTermDataFrom_LP_SIS_MAIN(REF_SIS_ID).then(
                                                             function(data){
                                                                debug("get TermDataFrom_LP_SIS_MAIN renewalPayment"+JSON.stringify(data));
                                                                debug("term plan data object : "+data);
                                                                dfd.resolve(data);
                                                            }
                                                        );


                                                }else{
                                                     dfd.resolve(null);
                                                    //self.setTerm_REF_SIS_ID(null);
                                                }

                                               // else
                                                    //dfd.resolve(null);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };

    /**
    * Function to get the data (REF_SIS_IS) from the LP_SIS_MAIN based on the REF_SIS_ID
    * Input parameters is Referce SIS Id
    **/
    self.getTermDataFrom_LP_SIS_MAIN = function(REF_SIS_ID){

            var dfd = $q.defer();
            if(REF_SIS_ID){
                query ="select ANNUAL_PREMIUM_AMOUNT,MODAL_PREMIUM,SERVICE_TAX from LP_SIS_MAIN where SIS_ID=?";

                parameters = [REF_SIS_ID];
                debug("get Term Data From_ LP_SIS_MAIN query : "+query+"parameters : "+parameters);
                CommonService.transaction(db,
                function(tx){
                        CommonService.executeSql(tx,query,parameters,
                                    function(tx,res){
                                        debug("query executed successfully for appt");
                                        //inserted = true;
                                        if(res.rows.length>0){
                                            debug("Term Data From_ LP_SIS_MAIN Reponce res");
                                            debug(res.rows.item(0));
                                            dfd.resolve(res.rows.item(0));
                                        }else{
                                             dfd.resolve(null);
                                        }
                                    },
                                    function(tx,err){
                                        debug("query execution failed for appt"+err);
                                        dfd.resolve(null);
                                    },null

                        );

                    },
                    function(tx,err){
                        debug("transaction error"+err.message);
                        dfd.resolve(null);
                    },
                null);
            }else{
                  dfd.resolve(null);
            }

            return dfd.promise;

    };

self.getNeftBankPayDetails = function(APPLICATION_ID,AGENT_CD){
var dfd = $q.defer();
var bankObj = {};
CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"select NEFT_BANK_NAME,NEFT_BANK_BRANCH_NAME,NEFT_BANK_ACCOUNT_NO,NEFT_BANK_IFSC_CODE,NEFT_BANK_ACCOUNT_TYPE from LP_APP_PAY_NEFT_SCRN_H where APPLICATION_ID =? and AGENT_CD = ?",[APPLICATION_ID,AGENT_CD],
                        function(tx,res){
                            debug("exist band data res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                for(var i =0;i<res.rows.length;i++){
                                    (function(i){
                                        bankDetService.getBankNames().then(
                                            function(data){
                                                bankObj.bankName = $filter('filter')(data,{bankName : res.rows.item(0).NEFT_BANK_NAME})[0];
                                                bankObj.AccNo = res.rows.item(0).NEFT_BANK_ACCOUNT_NO;
                                                bankObj.IfscCode = res.rows.item(0).NEFT_BANK_IFSC_CODE;
                                                bankObj.branchname = res.rows.item(0).NEFT_BANK_BRANCH_NAME;
                                                bankObj.AccType = res.rows.item(0).NEFT_BANK_ACCOUNT_TYPE;
                                                dfd.resolve(bankObj);
                                                debug("bank details "+ bankObj.bankName);
                                            }
                                        );
                                    }(i))
                                }
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );

            return dfd.promise;
}

/**** function related Term plan ends here ****/

// function ends here

}]);
