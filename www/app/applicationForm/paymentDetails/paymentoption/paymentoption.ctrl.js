payoptionModule.controller('payOptionCtrl',['$state','paymentData','bankData','payData','sisData','paymentDetService','LoginService','loadData','ApplicationFormDataService','ExistingAppMainData','$filter','payDetInfoService','minDate','maxDate','sisDataTermPlan', 'CommonService', function($state,paymentData,bankData,payData,sisData,paymentDetService,LoginService,loadData,ApplicationFormDataService,ExistingAppMainData,$filter,payDetInfoService,minDate,maxDate,sisDataTermPlan, CommonService){
    var self = this;
    self.click = false;
    self.disableBank = false;
    self.disablSI = false;
    this.bankData = bankData;

    payDetInfoService.setActiveTab("paymentoptions");
    debug("pay options "+JSON.stringify(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST));


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



    self.PayOpt = {};
    self.PayOpt.appid = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    self.nameRegex = USERNAME_REGEX;
	self.ifscRegex = IFSC_REG;
    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

    this.maxDate = new Date(maxDate);
    this.minDate = new Date(minDate);
    self.termPlanAmountObj = {};
    self.termPlanAmountObj = sisDataTermPlan;
    debug("Payment Option termPlanAmountObj "+JSON.stringify(self.termPlanAmountObj));


    //create blank object to add docids..
    ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
        Reflex :{
            Insured:{},
            Proposer:{}
        },
        NonReflex :{
            Insured:{},
            Proposer:{}
        }
    };
    var FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
    debug("FHR ID IS ----> "+FHR_ID);

    //changes 29th june...
    if(!!ExistingAppMainData && !!ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST && Object.keys(ExistingAppMainData.applicationPersonalInfo.Insured).length!=0){

        ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
        debug("existing data "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    }else
        debug("existing object is blank");

    debug("data is "+JSON.stringify(sisData));

    //set initial values..
    self.PayOpt.agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    self.PayOpt.empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
    self.PayOpt.premPayMode = sisData.sisFormBData.PREMIUM_PAY_MODE;


    var filterVal = sisData.sisFormBData.PREMIUM_PAY_MODE;
    debug("filter val is "+filterVal);
    for(var i =0 ;i<payData.length;i++){
        if(payData[i].PAY_ID == filterVal){
            payData.splice(i,1);
        }
    }
    debug("filter data is "+JSON.stringify(payData));

    self.payModes = payData;
    //changes 7th nov..
    //set the service tax,amount, premium from bean object
    var comboID = ApplicationFormDataService.ComboFormBean.COMBO_ID;

    if(loadData){
        debug("load data is --"+JSON.stringify(loadData));
        self.PayOpt = loadData;
    }
    else{
        debug("No data in database");
        self.PayOpt.freq = payData[0];
    }
    var dataToSet = paymentDetService.initValues(sisData,self.termPlanAmountObj);

    debug("SIS DATA :"+JSON.stringify(dataToSet));
    if(dataToSet){
        self.PayOpt.serviceTax = dataToSet.servicetax;
        self.PayOpt.Premium = dataToSet.annPrem;

        if(!!loadData){
            loadData.Premium = dataToSet.annPrem;
            loadData.TotPremAmt  = dataToSet.payableAmt;
            loadData.amount  = dataToSet.payableAmt;
        }

        self.PayOpt.TotPremAmt = dataToSet.totPrem;
        self.PayOpt.amount = dataToSet.payableAmt;
        self.PayOpt.mode = dataToSet.mode;
    }

    //set all the Dropdown values..
    self.PayData = paymentData;

    if(!!self.PayOpt && !!self.PayOpt.payClick && !!self.PayOpt.payClick.payClick){
        if(self.PayOpt.payClick.payClick == "Standing Instruction" && BUSINESS_TYPE == "IndusSolution"){
            self.disableBank = true;
        }
    }

    var PGL_ID = sisData.sisMainData.PGL_ID;

    /**
    *
    *
    **/
    self.OnSaveClick = function(payOptForm){
         self.click = true;
        debug("SAVE TERM AND BASE DATA :: "+JSON.stringify(dataToSet));

    };

    //code to check if he is Ind exsisting bank customer
    //Otherwise disable SI
    paymentDetService.checkIFExistingBankCust(ApplicationFormDataService.applicationFormBean.FHR_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).
    then (function(ifExistCust){
            if(ifExistCust)
                self.disablSI = true;
            else
                self.disablSI = false;
            debug("disableSI Value is::"+self.disablSI);
    });


    //set all the amount from sis..
    self.Save = function(payOptForm){
        CommonService.showLoading();
        self.click = true;
        debug("SAVE TERM AND BASE DATA :: "+JSON.stringify(dataToSet));
         debug("self data object :: "+JSON.stringify(self.PayOpt));

        if(payOptForm.$invalid != true){

            var PayOptSave = {};
            Object.assign(PayOptSave,self.PayOpt);

            PayOptSave.appid = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            PayOptSave.agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            PayOptSave.empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
            if(sisData.sisFormBData.PREMIUM_PAY_MODE == "O")
                PayOptSave.premPayMode = "A";
            else
                PayOptSave.premPayMode = sisData.sisFormBData.PREMIUM_PAY_MODE;

            //change the value to save of service tax, premium , amount , mode to save to database...
            PayOptSave.TotPremAmt = dataToSet.totalBaseAmt;//prem + ST
            PayOptSave.serviceTax = dataToSet.baseTax;// st
            PayOptSave.amount = dataToSet.totalBaseAmt; // prem + st

            //PayOptSave.payClick = self.PayOpt.payClick;
            //PayOptSave.freq = self.PayOpt.freq;


            debug("SAVE TERM AND BASE DATA  PAY OOPT SAVE :: "+JSON.stringify(PayOptSave));

            if(ApplicationFormDataService.applicationFormBean.POLICY_NO){
               PayOptSave.policyNo = ApplicationFormDataService.applicationFormBean.POLICY_NO;
            }else{
                PayOptSave.policyNo = "";
            }


            debug("save called"+JSON.stringify(PayOptSave));
			//changes 25 aug
			if(PayOptSave.empFlag != 'E'){
				//insert all data to DB
	            paymentDetService.insertAllDataBaseOnPayModes(PayOptSave,false).then(
	                function(inserted){
	                    if(inserted){
	                        //if combo id exist insert for record with combo id
	                        if(comboID){
	                            //change the locals to term data for insertion

                                PayOptSave.appid = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                PayOptSave.TotPremAmt = dataToSet.totalTermAmt;
                                PayOptSave.serviceTax = dataToSet.termTax;
                                PayOptSave.amount = dataToSet.totalTermAmt;

                                paymentDetService.insertAllDataBaseOnPayModes(PayOptSave,true).then(
                                    function(inserted){
                                        if(inserted){
                                            paymentDetService.updateDocListAndScreenFlag(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj);
                                            //alert("move to page");
                                            debug("pay mode "+PayOptSave.freq.PAY_CODE);
                                            debug("amount captured "+PayOptSave.TotPremAmt);
                                            payDetInfoService.setActiveTab("renewalPayment");
                                            $state.go("applicationForm.paymentDetails.renewalPayment",{amount : PayOptSave.TotPremAmt,payMode : PayOptSave.freq.PAY_CODE});
                                        }
                                    }
                                )
	                        }else{
	                            paymentDetService.updateDocListAndScreenFlag(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj);
                                //alert("move to page");
                                debug("pay mode "+PayOptSave.freq.PAY_CODE);
                                debug("amount captured "+PayOptSave.TotPremAmt);
                                payDetInfoService.setActiveTab("renewalPayment");
                                $state.go("applicationForm.paymentDetails.renewalPayment",{amount : PayOptSave.TotPremAmt,payMode : PayOptSave.freq.PAY_CODE});
	                        }
	                    }else{
	                        debug("issue in db or employyee cannot pay or Employee Flag :"+PayOptSave.empFlag);
	                    }
	                }
	            );
			}else{
				navigator.notification.alert("You are not allowed to do payment.",function (){
					payDetInfoService.setActiveTab("renewalPayment");
					debug("pay mode "+PayOptSave.freq.PAY_CODE);
					debug("amount captured "+PayOptSave.TotPremAmt);
					$state.go("applicationForm.paymentDetails.renewalPayment",{amount : PayOptSave.TotPremAmt,payMode : PayOptSave.freq.PAY_CODE});
				},"Application","OK");
			}

        }else{
            navigator.notification.alert('Please fill all fields with valid data',function(){CommonService.hideLoading();},"Application","OK");
        }
    };

    //payment type change call
    self.onChangePayMode = function(){
        debug("doc clicked "+ JSON.stringify(self.PayOpt.payClick));

        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.paymentOption = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.paymentOption = {};

        paymentDetService.getDocRelatedData().then(
            function(data){
                if(self.PayOpt.payClick.code){
                    var payObj = $filter('filter')(data,{ MPPROOF_CODE : self.PayOpt.payClick.code})[0];
                    debug("selected all data "+JSON.stringify(payObj));
                    navigator.notification.alert("\"" + payObj.MPPROOF_DESC +"\" copy needs to be submitted",function(){CommonService.hideLoading();},"Application","OK");
                    if(payObj.DIGITAL_HTML_FLAG == "Y")
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.paymentOption.DOC_ID = payObj.DOC_ID;
                    else
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.paymentOption.DOC_ID = payObj.DOC_ID;
                }else{
                    debug("code is null");
                }
            }
        );

        // if existing indusland customer, prepopulate data
        //5 july 2016
        if(self.PayOpt.payClick.payClick == "Standing Instruction" && BUSINESS_TYPE == "IndusSolution"){
                paymentDetService.getFHRBankData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.applicationFormBean.FHR_ID).then(
                    function(BankDetails){
                        if(BankDetails){
                            debug("Neft DATA IS ---> "+JSON.stringify(BankDetails));
                            self.PayOpt.bankName = BankDetails.bankName;
                            self.PayOpt.AccNo = parseInt(BankDetails.AccNo);
                            self.PayOpt.IfscCode = BankDetails.IfscCode;
                            self.PayOpt.reenterIfscCode = BankDetails.IfscCode;
                            self.PayOpt.branchname = BankDetails.branchname;
                            self.disableBank = true;
                        }
                        else
                        {
                            var defaultBank = $filter('filter')(this.bankData,{"bankName":"INDUSIND BANK LTD"},true)[0];
                            debug("Default bank is::"+JSON.stringify(defaultBank));
                            var setBank = this.bankData.indexOf(defaultBank);
                            self.PayOpt.bankName = this.bankData[setBank];
                            self.PayOpt.AccNo = "";
                            self.PayOpt.IfscCode = "";
                            self.PayOpt.reenterIfscCode = "";
                            self.PayOpt.branchname = "";
                            self.disableBank = true;
                        }
                    }
                );
        }
        else
        {
            if(self.PayOpt.payClick.payClick == "Demand Draft" || self.PayOpt.payClick.payClick == "Cheque" ){
                            debug("inside payment option data::");
                            paymentDetService.getNeftBankDetail(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                function(BankDetails){
                                    if(BankDetails){
                                        debug("Neft DATA IS ---> "+JSON.stringify(BankDetails));
                                        self.PayOpt.bankName = BankDetails.bankName;
                                        self.PayOpt.AccNo = parseInt(BankDetails.AccNo);
                                        self.PayOpt.IfscCode = BankDetails.IfscCode;
                                        self.PayOpt.reenterIfscCode = BankDetails.IfscCode;
                                        self.PayOpt.branchname = BankDetails.branchname;
                                        self.disableBank = false;
                                    }
                                }

                            );

                        //}
                    //}
                //);
            }
        }
        //5 july 2016

    }
    CommonService.hideLoading();
}]);

payoptionModule.service('paymentDetService',['$state','CommonService','$q','bankDetService','$filter',function($state,CommonService,$q,bankDetService,$filter){
    var self = this;

    self.getModes = function(PGL_ID,PNL_CODE){
        var dfd = $q.defer();
        var query = "SELECT PNL_FREQUENCY FROM LP_PNL_PLAN_LK WHERE PNL_PGL_ID = ? and PNL_CODE = ?";
        var parameters = [PGL_ID,PNL_CODE];
        debug("parametes to get modes >> "+parameters);
        var arrOfPaytypes = [];

        CommonService.transaction(sisDB,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                        debug("modes of plan code res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            var modes = res.rows.item(0).PNL_FREQUENCY;
                            var modeArr = modes.split("");
                            debug("Modes Data >>"+modeArr);
                            if(!!modeArr){
                                var payObj = {};
                                payObj.PAY_ID = null;
                                payObj.PAY_CODE = "Select Methods";
                                arrOfPaytypes.push(payObj);

                                for(var i=0;i<modeArr.length;i++){
                                    var payObj = {};
                                    if(modeArr[i] == "A"){
                                        payObj.PAY_ID = "A";
                                        payObj.PAY_CODE = "Annual";
                                    }
                                    if(modeArr[i] == "S"){
                                        payObj.PAY_ID = "S";
                                        payObj.PAY_CODE = "Semi Annual";
                                    }
                                    if(modeArr[i] == "Q"){
                                        payObj.PAY_ID = "Q";
                                        payObj.PAY_CODE = "Quarterly";
                                    }
                                    if(modeArr[i] == "M"){
                                        payObj.PAY_ID = "M";
                                        payObj.PAY_CODE = "Monthly";
                                    }
                                    if(modeArr[i] == "O"){
                                        payObj.PAY_ID = "O";
                                        payObj.PAY_CODE = "Single Pay";
                                    }
                                    arrOfPaytypes.push(payObj);
                                    debug("the pushed arr is"+arrOfPaytypes);
                                }
                                dfd.resolve(arrOfPaytypes);
                            }else
                                dfd.resolve(arrOfPaytypes);
                        }
                        else
                            dfd.resolve(arrOfPaytypes);
                    },
                    function(tx,err){
                        dfd.resolve(arrOfPaytypes);
                    }
                );
            },
            function(err){
                dfd.resolve(arrOfPaytypes);
            },null
        );

        return dfd.promise;
    }

    //5 july 2016
    //check flag in FFNA table if bank details exist..
    self.checkIFExistingBankCust = function(FHR_ID,AGENT_CD){
        var dfd = $q.defer();
        var isExist = false;
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select EXIST_BANK_CUST_FLAG from LP_FHR_FFNA where FHR_ID =? and AGENT_CD = ?",[FHR_ID,AGENT_CD],
                    function(tx,res){
                        debug("Exist indusland Bank res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            var flag = res.rows.item(0).EXIST_BANK_CUST_FLAG;
                            debug("its existing bank  "+flag);
                            if(flag == "Y"){
                                //debug("its existing bank  "+flag);
                                isExist = true;
                            }
                            dfd.resolve(isExist);
                        }
                        else
                            dfd.resolve(isExist);
                    },
                    function(tx,err){
                        dfd.resolve(isExist);
                    }
                );
            },
            function(err){
                dfd.resolve(isExist);
            },null
        );

        return dfd.promise;
    }

    //get all data from FFNA...
    self.getBankDetailFromFHRFFNA = function(AGENT_CD,FHR_ID){
            var dfd = $q.defer();
            var bankObj = {};
            CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"select EXIST_BANK_ACCOUNT_NO,EXIST_BANK_ACCOUNT_TYPE,EXIST_BANK_NAME,EXIST_BANK_IFSC_CODE,EXIST_BANK_BRANCH_NAME from LP_FHR_FFNA where FHR_ID =? and AGENT_CD = ?",[FHR_ID,AGENT_CD],
                        function(tx,res){

                            debug("exist band data res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                for(var i =0;i<res.rows.length;i++){
                                    (function(i){

                                        bankDetService.getBankNames().then(
                                            function(data){
                                                bankObj.bankName = $filter('filter')(data,{bankName : res.rows.item(0).EXIST_BANK_NAME})[0];
                                                bankObj.AccNo = res.rows.item(0).EXIST_BANK_ACCOUNT_NO;
                                                bankObj.IfscCode = res.rows.item(0).EXIST_BANK_IFSC_CODE;
                                                bankObj.branchname = res.rows.item(0).EXIST_BANK_BRANCH_NAME;
                                                bankObj.AccType = res.rows.item(0).EXIST_BANK_ACCOUNT_TYPE;
                                                dfd.resolve(bankObj);
                                                debug("bank details "+ bankObj.bankName);
                                            }
                                        );
                                    }(i))
                                }
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );

            return dfd.promise;
        }
    //5 july 2016

    //get service tax and premium data..
    self.initValues = function(sisData,termPlanAmountObj){
         if(sisData.sisMainData){
            debug("SIS MAIN DATA "+JSON.stringify(sisData.sisMainData));
            debug("SIS MAIN DATA "+JSON.stringify(termPlanAmountObj));

            var dataToSet = {};
            var servicetax = parseInt(sisData.sisMainData.SERVICE_TAX);
            var annPrem = parseInt(sisData.sisMainData.MODAL_PREMIUM);
			if(sisData.sisFormAData.TATA_FLAG=='Y'){
				annPrem = parseInt(sisData.sisMainData.DISCOUNTED_MODAL_PREMIUM);
				servicetax = parseInt(sisData.sisMainData.DISCOUNTED_SERVICE_TAX);
			}
            var payMode = "";

            //for database purpose..
            var basePrem = annPrem ;//parseInt(sisData.sisMainData.MODAL_PREMIUM);
            var baseTax = servicetax;//parseInt(sisData.sisMainData.SERVICE_TAX);
            var termPrem = 0;
            var termTax = 0;

            debug("initial  servicetax "+servicetax);
            debug("initial  prem is "+annPrem);
            if(sisData.sisFormBData){
                payMode = sisData.sisFormBData.PREMIUM_PAY_MODE;
            }

            if(!!termPlanAmountObj){
                console.log("service tzzzz : "+parseInt(termPlanAmountObj.SERVICE_TAX));
                console.log("modal premium tzzzz : "+parseInt(termPlanAmountObj.MODAL_PREMIUM));
               // if(termPlanAmountObj.COMBO_ID){
                    servicetax = (!!termPlanAmountObj.SERVICE_TAX)?(parseInt(servicetax)+parseInt(termPlanAmountObj.SERVICE_TAX)):servicetax;
                    termTax = parseInt(termPlanAmountObj.SERVICE_TAX);

                    annPrem = (!!termPlanAmountObj.MODAL_PREMIUM)?(parseInt(annPrem)+parseInt(termPlanAmountObj.MODAL_PREMIUM)):annPrem;
                    termPrem = parseInt(termPlanAmountObj.MODAL_PREMIUM);
                //}

                debug("Service tax after adding term tax "+servicetax);
                debug("Premium after adding term premium "+annPrem);
            }


            if(sisData.sisFormDData){
                //changes due to rider premium issue 26/10/2016
                servicetax = servicetax + (parseInt(sisData.sisFormDData[0].RIDER_SERVICE_TAX) || 0);
                baseTax = baseTax + (parseInt(sisData.sisFormDData[0].RIDER_SERVICE_TAX) || 0);
                debug(" after adding servicetax "+servicetax);

                annPrem = annPrem + (parseInt(sisData.sisFormDData[0].RIDER_MODAL_PREMIUM) || 0);
                basePrem = basePrem + (parseInt(sisData.sisFormDData[0].RIDER_MODAL_PREMIUM) || 0);
                debug(" after adding prem is "+annPrem);

            }else{
                debug("sis form d not proper");
            }

            var totPrem = servicetax + annPrem;
            var payableAmt = totPrem;

            if(payMode == "M"){
                totPrem = totPrem * 3;
                payableAmt = payableAmt * 3;
            }

            dataToSet.mode = payMode;
            dataToSet.servicetax = servicetax;
            dataToSet.annPrem = annPrem;
            dataToSet.totPrem = totPrem;
            dataToSet.payableAmt = payableAmt;

            //base
            dataToSet.basePrem = basePrem;
            dataToSet.baseTax = baseTax;
            dataToSet.totalBaseAmt = parseInt(basePrem) +  parseInt(baseTax);

            //term
            dataToSet.termPrem = termPrem;
            dataToSet.termTax = termTax;
            dataToSet.totalTermAmt = termPrem + termTax;

            debug("BASE DATA >>"+basePrem+" << SERVICE TAX >>"+baseTax+ "<< TERM DATA >>"+termPrem+"<< SERVICE TAX TERM >> "+termTax);

            debug("ST   "+servicetax+"  premium   "+annPrem+"  tot prem amt  "+totPrem+"  amount   "+payableAmt);
            return dataToSet;
         }
         else{
            debug("sis data not proper");
         }
    };

    //get docs data..
    self.getDocRelatedData = function(){
        var dfd = $q.defer();
        var payMentArr = [];
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select DOC_ID,MPPROOF_DESC,DIGITAL_HTML_FLAG,MPPROOF_CODE from LP_DOC_PROOF_MASTER where MPDOC_CODE=? AND ISACTIVE='1'",["SI"],
                                function(tx,res){
                                    debug("payment arr res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            var payObj = {};
                                            payObj.DOC_ID = res.rows.item(i).DOC_ID;
                                            payObj.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
                                            payObj.DIGITAL_HTML_FLAG = res.rows.item(i).DIGITAL_HTML_FLAG;
                                            payObj.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                            payMentArr.push(payObj);
                                        }
                                        dfd.resolve(payMentArr);
                                    }
                                    else
                                        dfd.resolve(payMentArr);
                                },
                                function(tx,err){
                                    dfd.resolve(payMentArr);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(payMentArr);
                        },null
                    );
        return dfd.promise;
    };

    //get all payment data..
    self.getPaymentData = function(buss_type){
		//3rd october added Cash field as it was not in Glide.
        debug("inside pay method details");
        var paymentMethodArr = ['Cheque','Demand Draft','Online Payment','Standing Instruction'];
        var paymentCode=["QC","DC",null,"IS"];
        var index = paymentMethodArr.indexOf('Standing Instruction');
        debug("Index of SI::"+index);
		if(buss_type != "IndusSolution"){
		    paymentMethodArr.splice(index);
		    //paymentCode.pop("IS");
		    debug("paymethd array::"+paymentMethodArr);
		}
		if(buss_type != "IndusSolution"){
        			paymentMethodArr.push("Cash");
        			paymentCode.push(null);
        }
        var arrOfPayTypes = [];

        for(var i=0;i<paymentMethodArr.length;i++){
             var payObj = {};
             payObj.code = paymentCode[i];
             payObj.payClick = paymentMethodArr[i];
             arrOfPayTypes.push(payObj);
        }

        return arrOfPayTypes;
    };

    //get docid and set to table...
    self.getandInsetDocID = function (payCode){
        var query = "select DOC_ID from LP_DOC_PROOF_MASTER where CUSTOMER_CATEGORY='PR' AND ISACTIVE='1' AND MPDOC_CODE='SI' AND MPPROOF_CODE=?";

        self.getDisplayData(query,[payCode]).then(
                function(data){
                    if(data){
                        var query = "INSERT OR REPLACE INTO LP_DOCUMENT_UPLOAD(DOCID,AGENT_CD,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP) values (?,?,?,?,?,?,?)";
                        var parameters = [];
                        self.insertData(query,parameters).then(
                            function(inserted){
                                if(inserted){
                                    debug("success fully inserted into doc table");
                                }
                            }
                        );
                    }
                }
        );
    };

    //get all paying modess..
    self.getPayingModes = function(){
        debug("inside modes");
        var payModes = ["Select Methods","Annual","Monthly","Semi Annual","Quarterly"];
        var keys = [null,"A","M","S","Q"];
        var arrOfPaytypes = [];

        for(var i=0;i<payModes.length;i++){
           var payObj = {};
           payObj.PAY_ID = keys[i];
           payObj.PAY_CODE = payModes[i];
           arrOfPaytypes.push(payObj);
        }
        return arrOfPaytypes;
    };

    //save data to database based on modes
    self.insertAllDataBaseOnPayModes = function(data,isCombo){

        var dfd = $q.defer();
        //var data = Chqdata.PayOpt;
        var query = "";
        var parameters = [];
        var currDate = CommonService.getCurrDate();
        var mnthInit = 0;
        if(data.mode == "M")
            mnthInit = 3;

        var mode = data.payClick;
        debug("insertion for mode "+JSON.stringify(mode));
        var tableName = "LP_APP_PAY_DTLS_SCRN_G";
        var columnName = "APPLICATION_ID";

        debug("pay id "+data.freq.PAY_ID);
        if(isCombo){
            tableName = "LP_COMBO_PAY_DTLS_SCRN_G";
            columnName = "COMBO_ID";
        }

        debug("WHILE INSERTING :: "+JSON.stringify(data));
		//changes 25 aug
        //only allowed for employees other than E..
        //if(data.empFlag != "E"){
            //depending on pay click, creating query and parameters..
        switch(mode.payClick){
            case "Cheque":
                    {
                        var chqDate = CommonService.formatDobToDb(data.ChqDate);
                        //29th june..
                        debug("CHEQUE DATE IS "+CommonService.formatDobToDb(data.ChqDate));
                        query = "insert or replace into "+tableName+" ("+columnName+",AGENT_CD,POLICY_NO,PREMIUM_PAY_MODE,PAY_METHOD_CHEQ_FLAG,CHEQUE_NO,CHEQ_BANK_NAME,CHEQ_BANK_BRANCH_NAME,CHEQ_BANK_IFSC_CODE,CHEQ_AMOUNT,CHEQ_DATE,TOTAL_PAYMENT_AMOUNT,TRANSACTION_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,MODIFIED_DATE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        parameters = [data.appid,data.agentCD,data.policyNo,data.premPayMode,"Y",data.CheqNo,data.bankName.bankName,data.branchname,data.IfscCode,data.amount,CommonService.formatDobToDb(data.ChqDate),data.amount,currDate,data.TotPremAmt,data.serviceTax,mnthInit,data.freq.PAY_ID,currDate];
                    }
                break;
            case "Demand Draft":
                    {
                        query = "insert or replace into "+tableName+" ("+columnName+",AGENT_CD,POLICY_NO,PREMIUM_PAY_MODE,PAY_METHOD_DD_FLAG,DEMAND_DRAFT_NO,DD_BANK_NAME,DD_BANK_BRANCH_NAME,DD_BANK_IFSC_CODE,DD_AMOUNT,DD_IMAGE_CAPTURED,TOTAL_PAYMENT_AMOUNT,TRANSACTION_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,MODIFIED_DATE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        parameters = [data.appid,data.agentCD,data.policyNo,data.premPayMode,"Y",data.DDNo,data.bankName.bankName,data.branchname,data.IfscCode,data.amount,null,data.amount,currDate,data.TotPremAmt,data.serviceTax,mnthInit,data.freq.PAY_ID,currDate];
                    }
                break;
            case "Online Payment":
                    {
                        query = "insert or replace into "+tableName+" ("+columnName+",AGENT_CD,POLICY_NO,TOTAL_PAYMENT_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PREMIUM_AMOUNT,PAY_METHOD_ONLINE_FLAG,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,PREMIUM_PAY_MODE)values(?,?,?,?,?,?,?,?,?,?)";
                        parameters = [data.appid,data.agentCD,data.policyNo,data.amount,data.serviceTax,data.TotPremAmt,"Y",mnthInit,data.freq.PAY_ID,data.premPayMode];
                    }
                break;
            case "Cash" :
                    {
                        query = "insert or replace into "+tableName+" ("+columnName+",AGENT_CD,POLICY_NO,TOTAL_PAYMENT_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PREMIUM_AMOUNT,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,PREMIUM_PAY_MODE,PAY_METHOD_CASH_FLAG,CASH_AMOUNT,TRANSACTION_DATE)values(?,?,?,?,?,?,?,?,?,?,?,?)";
                        parameters = [data.appid,data.agentCD,data.policyNo,data.amount,data.serviceTax,data.TotPremAmt,data.mnthInit,data.freq.PAY_ID,data.premPayMode,"Y",data.amount,currDate];
                    }
                break;
            case "Standing Instruction" :
                    {
                        query = "insert or replace into "+tableName+" ("+columnName+",AGENT_CD,POLICY_NO,PREMIUM_PAY_MODE,PAY_METHOD_SI_FLAG,SI_BANK_ACCOUNT_NO,SI_BANK_NAME,SI_BANK_BRANCH_NAME,SI_BANK_IFSC_CODE,SI_AMOUNT,SI_AUTHORIZE_FORM,TOTAL_PAYMENT_AMOUNT,TRANSACTION_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,MODIFIED_DATE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        parameters = [data.appid,data.agentCD,data.policyNo,data.premPayMode,"Y",data.AccNo,data.bankName.bankName,data.branchname,data.IfscCode,data.amount,null,data.amount,currDate,data.TotPremAmt,data.serviceTax,data.mnthInit,data.freq.PAY_ID,currDate];
                    }
                break;
            default :
                break;
        }
        debug("param --"+parameters);
        self.insertData(query,parameters).then(
                function(inserted){
                    if(inserted){
                        debug("successfully inserted");
                        dfd.resolve(true);
                    }else{
                        debug("error in insertion");
                        dfd.resolve(false);
                    }
                }
            );
       //}else{
        //    dfd.resolve(false);
        //    navigator.notification.alert("You are not allowed to do payment.",function(){CommonService.hideLoading();},"Application","OK");
       //}

        return dfd.promise;
    };

    //common query to insert
    self.insertData = function(query,params){
        var dfd = $q.defer();
        debug("insert data --"+params+"--Query is "+query);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,params,
                                            function(tx,res){
                                                debug("query executed successfully for pay options");
                                                //inserted = true;
                                                dfd.resolve(true);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for pay options"+err);
                                                dfd.resolve(false);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error in payoption"+err.message);
                dfd.resolve(false);
            },
        null);
        return dfd.promise;
    };

    //common query for get data...
    self.getDisplayData = function(query,params){
        var dfd = $q.defer();

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,params,
                                            function(tx,res){
                                                debug("query executed successfully for appt");
                                                //inserted = true;
                                                if(res.rows.length > 0)
                                                    dfd.resolve(res);
                                                else
                                                    dfd.resolve(null);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };

    //get data from DB.
    self.getPayModeFlagFromDB = function(appid,agentCD){
        var dfd = $q.defer();
		//3rd oct added Cash
        var query = "select PAY_METHOD_CHEQ_FLAG,PAY_METHOD_SI_FLAG,PAY_METHOD_DD_FLAG,PAY_METHOD_ONLINE_FLAG,PAY_METHOD_CASH_FLAG from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
        var parameters = [appid,agentCD];

        var payDetails = "";

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                        if(!!res && res.rows.length>0){
                            var resp = res.rows.item(0);
                            debug("data is tssssss"+JSON.stringify(resp));
                            if(resp.PAY_METHOD_CHEQ_FLAG == "Y"){
                                payDetails = "Cheque";
                            }
                            else if(resp.PAY_METHOD_SI_FLAG == "Y"){
                                payDetails = "Standing Instruction";
                            }
                            else if(resp.PAY_METHOD_DD_FLAG == "Y"){
                                payDetails = "Demand Draft";
                            }
                            else if(resp.PAY_METHOD_ONLINE_FLAG == "Y"){
                                payDetails = "Online Payment";
                            } else if(resp.PAY_METHOD_CASH_FLAG == "Y"){
                                payDetails = "Cash";
							}
                            //payDetails = data;
                            debug("data from DB click ---->"+payDetails);
                            self.initializeUI(payDetails,appid,agentCD).then(
                                function(data){
                                    dfd.resolve(data);
                                }
                            );
                            //dfd.resolve(null);
                        }else{
                            //if no data...
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        debug("query execution failed for pay mode"+err);
                        dfd.resolve(null);
                    },null
                );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };

    //set data to UI
    self.initializeUI = function(payClick,appid,agentCD){
        var dfd = $q.defer();
        var query = "";
        var parameters = [appid,agentCD];
        var payDetails = {};
        payDetails.payClick = payClick.payClick;

        switch(payClick){
            case "Cheque" :
                query = "select CHEQUE_NO,CHEQ_BANK_NAME,CHEQ_BANK_BRANCH_NAME,CHEQ_BANK_IFSC_CODE,CHEQ_AMOUNT,CHEQ_DATE,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
                self.getDisplayData(query,parameters).then(
                    function(data){
                        if(data.rows.length > 0){
                            for(var i=0;i<data.rows.length;i++){
                                var resp = data.rows.item(i);
                                (function(i){
                                    payDetails.CheqNo = resp.CHEQUE_NO;
                                    payDetails.branchname = resp.CHEQ_BANK_BRANCH_NAME;
                                    payDetails.payClick = $filter('filter')(self.getPaymentData(BUSINESS_TYPE),{payClick : "Cheque"})[0];
                                    //number..
                                    payDetails.Premium = parseInt(resp.CHEQ_AMOUNT);
                                    payDetails.amount = parseInt(resp.CHEQ_AMOUNT);
                                    payDetails.IfscCode = resp.CHEQ_BANK_IFSC_CODE;
                                    payDetails.reenterIfscCode = resp.CHEQ_BANK_IFSC_CODE;
                                    //29th june..
                                    payDetails.ChqDate = CommonService.formatDobFromDb(resp.CHEQ_DATE);
                                    debug("cheque date is  :: "+payDetails.ChqDate);
                                    payDetails.TotPremAmt = parseInt(resp.TOTAL_PREMIUM_AMOUNT);
                                    payDetails.serviceTax = parseInt(resp.TOTAL_SERVICE_TAX);
                                    payDetails.mnthInit = resp.MONTHS_INITIAL_DEPOSIT;
                                    payDetails.freq = $filter('filter')(self.getPayingModes(),{PAY_ID : resp.FIRST_ANIV_CHANGE_PAY_MODE})[0];
                                    debug("FREQUENCY TO SET "+JSON.stringify(payDetails.freq)+">>>> and year is >>"+ resp.FIRST_ANIV_CHANGE_PAY_MODE);
                                    bankDetService.getBankNames().then(
                                        function(data){
                                            debug("pay option before"+ resp.CHEQ_BANK_NAME);
                                            payDetails.bankName = $filter('filter')(data,{bankName : resp.CHEQ_BANK_NAME})[0];
                                            debug("pay option "+ payDetails.bankName);
                                        }
                                    );
                                }(i))
                            }
                             dfd.resolve(payDetails);
                        }
                    }
                );
            break;
            case "Cash" :
                query ="select TOTAL_PAYMENT_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PREMIUM_AMOUNT,FIRST_ANIV_CHANGE_PAY_MODE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
                self.getDisplayData(query,parameters).then(
                    function(data){
                        if(data.rows.length > 0){
                                var resp = data.rows.item(0);
                                payDetails.payClick = $filter('filter')(self.getPaymentData(BUSINESS_TYPE),{payClick : "Cash"})[0];
                                payDetails.TotPremAmt = parseInt(resp.TOTAL_PREMIUM_AMOUNT);
                                //number
                                payDetails.Premium = parseInt(resp.TOTAL_PAYMENT_AMOUNT);
                                payDetails.amount = parseInt(resp.TOTAL_PAYMENT_AMOUNT);
                                payDetails.serviceTax = parseInt(resp.TOTAL_SERVICE_TAX);
                                payDetails.freq = $filter('filter')(self.getPayingModes(),{PAY_ID : resp.FIRST_ANIV_CHANGE_PAY_MODE})[0];
                                debug("FREQUENCY TO SET "+JSON.stringify(payDetails.freq)+">>>> and year is >>"+ resp.FIRST_ANIV_CHANGE_PAY_MODE);
                            }
                            dfd.resolve(payDetails);
                        }
                );
            break;
            case "Online Payment" :
                query = "select TOTAL_PAYMENT_AMOUNT,TOTAL_SERVICE_TAX,TOTAL_PREMIUM_AMOUNT,FIRST_ANIV_CHANGE_PAY_MODE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
                self.getDisplayData(query,parameters).then(
                    function(data){
                        if(data.rows.length > 0){
                                var resp = data.rows.item(0);
                                payDetails.payClick = $filter('filter')(self.getPaymentData(BUSINESS_TYPE),{payClick : "Online Payment"})[0];
                                payDetails.TotPremAmt = parseInt(resp.TOTAL_PREMIUM_AMOUNT);
                                payDetails.Premium = parseInt(resp.TOTAL_PAYMENT_AMOUNT);
                                payDetails.amount = parseInt(resp.TOTAL_PAYMENT_AMOUNT);
                                payDetails.serviceTax = parseInt(resp.TOTAL_SERVICE_TAX);
                                payDetails.freq = $filter('filter')(self.getPayingModes(),{PAY_ID : resp.FIRST_ANIV_CHANGE_PAY_MODE})[0];
                                debug("FREQUENCY TO SET "+JSON.stringify(payDetails.freq)+">>>> and year is >>"+ resp.FIRST_ANIV_CHANGE_PAY_MODE);
                        }
                        dfd.resolve(payDetails);
                    }
                );
            break;
            case "Standing Instruction" :
                query = "select SI_BANK_ACCOUNT_NO,SI_BANK_NAME,SI_BANK_BRANCH_NAME,SI_BANK_IFSC_CODE,SI_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE,SI_AUTHORIZE_FORM from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
                self.getDisplayData(query,parameters).then(
                    function(data){
                            if(data.rows.length > 0){
                                for(var i=0;i<data.rows.length;i++){
                                    var resp = data.rows.item(i);
                                    (function(i){
                                        payDetails.payClick = $filter('filter')(self.getPaymentData(BUSINESS_TYPE),{payClick : "Standing Instruction"})[0];
                                        payDetails.AccNo = parseInt(resp.SI_BANK_ACCOUNT_NO);
                                        payDetails.branchname = resp.SI_BANK_BRANCH_NAME;
                                        payDetails.IfscCode = resp.SI_BANK_IFSC_CODE;
                                        payDetails.reenterIfscCode = resp.SI_BANK_IFSC_CODE;
                                        payDetails.Premium = parseInt(resp.SI_AMOUNT);
                                        payDetails.amount = parseInt(resp.SI_AMOUNT);
                                        payDetails.TotPremAmt = parseInt(resp.TOTAL_PREMIUM_AMOUNT);
                                        //payDetails.amount = resp.TOTAL_PREMIUM_AMOUNT;
                                        payDetails.serviceTax = parseInt(resp.TOTAL_SERVICE_TAX);
                                        payDetails.freq = $filter('filter')(self.getPayingModes(),{PAY_ID : resp.FIRST_ANIV_CHANGE_PAY_MODE})[0];
                                        debug("FREQUENCY TO SET "+JSON.stringify(payDetails.freq)+">>>> and year is >>"+ resp.FIRST_ANIV_CHANGE_PAY_MODE);
                                        bankDetService.getBankNames().then(
                                            function(data){
                                                debug("pay option before"+ resp.SI_BANK_NAME);
                                                payDetails.bankName = $filter('filter')(data,{bankName : resp.SI_BANK_NAME})[0];
                                                debug("pay option "+ payDetails.bankName);
                                            }
                                        );
                                    }(i))
                                }
                            }//if loop
                            dfd.resolve(payDetails);
                        }
                );
                break;
            case "Demand Draft" :
                query = "select DEMAND_DRAFT_NO,DD_BANK_NAME,DD_BANK_BRANCH_NAME,DD_BANK_IFSC_CODE,DD_AMOUNT,TOTAL_PREMIUM_AMOUNT,TOTAL_SERVICE_TAX,MONTHS_INITIAL_DEPOSIT,FIRST_ANIV_CHANGE_PAY_MODE from LP_APP_PAY_DTLS_SCRN_G where APPLICATION_ID=? AND AGENT_CD=?";
                self.getDisplayData(query,parameters).then(
                    function(data){
                        if(data.rows.length > 0){
                            for(var i=0;i<data.rows.length;i++){
                                var resp = data.rows.item(i);
                                (function(i){
                                    payDetails.DDNo = resp.DEMAND_DRAFT_NO;
                                    payDetails.branchname = resp.DD_BANK_BRANCH_NAME;
                                    payDetails.payClick = $filter('filter')(self.getPaymentData(BUSINESS_TYPE),{payClick : "Demand Draft"})[0];
                                    payDetails.Premium = parseInt(resp.DD_AMOUNT);
                                    payDetails.amount = parseInt(resp.DD_AMOUNT);
                                    payDetails.IfscCode = resp.DD_BANK_IFSC_CODE;
                                    payDetails.reenterIfscCode = resp.DD_BANK_IFSC_CODE;
                                    payDetails.TotPremAmt = parseInt(resp.TOTAL_PREMIUM_AMOUNT);
                                    payDetails.serviceTax = parseInt(resp.TOTAL_SERVICE_TAX);
                                    payDetails.mnthInit = resp.MONTHS_INITIAL_DEPOSIT;
                                    payDetails.freq = $filter('filter')(self.getPayingModes(),{PAY_ID : resp.FIRST_ANIV_CHANGE_PAY_MODE})[0];
                                    debug("FREQUENCY TO SET "+JSON.stringify(payDetails.freq)+">>>> and year is >>"+ resp.FIRST_ANIV_CHANGE_PAY_MODE);
                                    bankDetService.getBankNames().then(
                                        function(data){
                                            debug("pay option before"+ resp.DD_BANK_NAME);
                                            payDetails.bankName = $filter('filter')(data,{bankName : resp.DD_BANK_NAME})[0];
                                            debug("pay option "+ payDetails.bankName);
                                        }
                                    );
                                }(i))
                            }

                        }
                        dfd.resolve(payDetails);
                    }
                );
                break;
            default :
                    dfd.resolve(null);
            break;
        }

        return dfd.promise;
    };

    //update flag and insert doclists...
    this.updateDocListAndScreenFlag = function(docslist,whereClause){
        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(docslist)}, whereClause).then(
            function(res){
                /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN7_COMPLETED" :"Y"}, whereClause).then(
                    function(res){
                        debug("IS_SCREEN7_COMPLETED update success !!"+JSON.stringify(whereClause));
                    }
                );*/
            }
        );
    };

    this.getMaxDate=function() {
        var todayTime = new Date();
        todayTime.setDate(todayTime.getDate()+2);
        var tempMonth=todayTime.getMonth()+1;
        var month = tempMonth>9?tempMonth:("0"+tempMonth);
        var tempDay = todayTime.getDate();
        var day =  tempDay>9?tempDay:("0"+tempDay);
        var year = todayTime .getFullYear();
        debug("MAX DATE "+(year + "-" + month + "-" + day));
        return (year + "-" + month + "-" + day);
    };

    this.getMinDate=function() {
        var todayTime = new Date();
        todayTime.setDate(todayTime.getDate()-16);
        var tempMonth=todayTime.getMonth()+1;
        var month = tempMonth>9?tempMonth:("0"+tempMonth);
        var tempDay = todayTime.getDate();
        var day =  tempDay>9?tempDay:("0"+tempDay);
        var year = todayTime .getFullYear();
        debug("MIN DATE "+(year + "-" + month + "-" + day));
        return (year + "-" + month + "-" + day);
    };


    /**
    * Function to get the data (REF_SIS_IS) from the LP_MYOPPORTUNITY based on the COMBO_ID
    * Input parameters are COMBO_id
    **/

    // self.getDataFrom_LP_MYOPPORTUNITY = function(COMBO_ID){
    //     var dfd = $q.defer();
    //    query ="select SIS_ID from LP_MYOPPORTUNITY where COMBO_ID=?";
    //     parameters = [COMBO_ID];
    //     CommonService.transaction(db,
    //         function(tx){
    //             CommonService.executeSql(tx,query,parameters,
    //                                         function(tx,res){
    //                                             debug("query executed successfully for appt");
    //                                             //inserted = true;
    //                                             //if(res.rows.length > 0)
    //                                                 dfd.resolve(res);
    //                                                 debug("getDataFrom_LP_MYOPPORTUNITY Reponce "+JSON.stringify(res.rows.item(0)));
    //                                            // else
    //                                                 //dfd.resolve(null);
    //                                         },
    //                                         function(tx,err){
    //                                             debug("query execution failed for appt"+err);
    //                                             dfd.resolve(null);
    //                                         },null
    //                                     );

    //         },
    //         function(tx,err){
    //             debug("transaction error"+err.message);
    //             dfd.resolve(null);
    //         },
    //     null);

    //     return dfd.promise;
    // };


      self.getDataFrom_LP_MYOPPORTUNITY = function(COMBO_ID){
        var dfd = $q.defer();
       query ="select REF_SIS_ID from LP_MYOPPORTUNITY where COMBO_ID=?";
        parameters = [COMBO_ID];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,res){
                                                debug("COMBO_ID "+COMBO_ID+"query executed successfully for appt "+res);
                                                debug(res);

                                                if(res.rows.length > 0){
                                                    debug("getDataFrom_LP_MYOPPORTUNITY Reponce "+JSON.stringify(res.rows.item(0).REF_SIS_ID));
                                                     var REF_SIS_ID = res.rows.item(0).REF_SIS_ID;
                                                       self.getTermDataFrom_LP_SIS_MAIN(REF_SIS_ID).then(
                                                             function(data){
                                                                debug("get TermDataFrom_LP_SIS_MAIN renewalPayment"+JSON.stringify(data));
                                                                debug("term plan data object : "+data);
                                                                dfd.resolve(data);
                                                            }
                                                        );
                                                }else{
                                                     dfd.resolve(null);
                                                }
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };



    /**
    * Function to get the data (REF_SIS_IS) from the LP_SIS_MAIN based on the REF_SIS_ID
    * Input parameters is Referce SIS Id
    **/
    self.getTermDataFrom_LP_SIS_MAIN = function(REF_SIS_ID){
        var dfd = $q.defer();
       query ="select ANNUAL_PREMIUM_AMOUNT,MODAL_PREMIUM,SERVICE_TAX from LP_SIS_MAIN where SIS_ID=?";
        parameters = [REF_SIS_ID];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,res){
                                                debug("query executed successfully for appt");
                                                //inserted = true;
                                                //if(res.rows.length > 0)
                                                if(!!res && res.rows.length>0)
                                                    dfd.resolve(CommonService.resultSetToObject(res));
                                                 else
                                                    dfd.resolve(null);
                                                    debug("get Term Data From_ LP_SIS_MAIN Reponce "+JSON.stringify(res.rows.item(0)));
                                               // else
                                                    //dfd.resolve(null);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };

self.getNeftBankDetail = function(APPLICATION_ID,AGENT_CD){
var dfd = $q.defer();
var bankObj = {};
CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"select NEFT_BANK_NAME,NEFT_BANK_BRANCH_NAME,NEFT_BANK_ACCOUNT_NO,NEFT_BANK_IFSC_CODE,NEFT_BANK_ACCOUNT_TYPE from LP_APP_PAY_NEFT_SCRN_H where APPLICATION_ID =? and AGENT_CD = ?",[APPLICATION_ID,AGENT_CD],
                        function(tx,res){
                            debug("exist band data res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                for(var i =0;i<res.rows.length;i++){
                                    (function(i){
                                        bankDetService.getBankNames().then(
                                            function(data){
                                                bankObj.bankName = $filter('filter')(data,{bankName : res.rows.item(0).NEFT_BANK_NAME})[0];
                                                bankObj.AccNo = res.rows.item(0).NEFT_BANK_ACCOUNT_NO;
                                                bankObj.IfscCode = res.rows.item(0).NEFT_BANK_IFSC_CODE;
                                                bankObj.branchname = res.rows.item(0).NEFT_BANK_BRANCH_NAME;
                                                bankObj.AccType = res.rows.item(0).NEFT_BANK_ACCOUNT_TYPE;
                                                dfd.resolve(bankObj);
                                                debug("bank details "+ bankObj.bankName);
                                            }
                                        );
                                    }(i))
                                }
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );

            return dfd.promise;
}

    this.getFHRBankData = function(AGENT_CD,FHR_ID){
         var dfd = $q.defer();
            self.checkIFExistingBankCust(FHR_ID,AGENT_CD).then(
                  function(ifExistCust){
                      if(ifExistCust){
                          self.getBankDetailFromFHRFFNA(AGENT_CD,FHR_ID).then(
                              function(FFNAData){
                                      debug("FFNA DATA IS ---> "+JSON.stringify(FFNAData));
                                     dfd.resolve(FFNAData);
                                  }

                          );
                    }else{
                          dfd.resolve(null);
                    }
                }
            );
             return dfd.promise;
        };



}]);

//check for empty field..
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

//amount calculation based on min of 100 and 0.1% of Total premium.
payoptionModule.directive('maxCalc', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.maxCalc, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max = scope.$eval(attr.maxCalc) || Infinity;
                debug("max value is "+max);
                var minValue = Math.min(100,0.1 * max);
                debug("min value is "+minValue);
                var compareValue = parseInt(max)+parseInt(minValue);
                debug("min value is "+(value > compareValue)+"test"+!isEmpty(value));
                if (!isEmpty(value) && (value > compareValue)) {
                    ctrl.$setValidity('maxCalc', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('maxCalc', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});
