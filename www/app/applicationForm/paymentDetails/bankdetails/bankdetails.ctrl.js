bankDetModule.controller('banDetCtrl',['$q','$state','bankData','NEFTData','LoginService','loadData','bankDetService','ApplicationFormDataService','ExistingAppMainData','$filter','payDetInfoService','sisData','bankDataFHR','DeDupeData','CommonService',function($q, $state,bankData,NEFTData,LoginService,loadData,bankDetService,ApplicationFormDataService,ExistingAppMainData,$filter,payDetInfoService,sisData,bankDataFHR, DeDupeData, CommonService){
    var self = this;
    self.click = false;
    self.BankDetails = {};

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */
    self.AccTypeNBFECode = {
      "11":"Current",
      "10":"Saving",
      "13":"NRO"
    };
    this.setDeDupeData = function () {
      var dfd = $q.defer();
      try{
        if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && (!loadData || !loadData.BankAccNum)){
      				CommonService.showLoading("Loading Existing Data..");
              var bnk = DeDupeData.RESULT_APPDATA.BANK_DETAILS;
              var bnkDetails = (!!bnk) ? bnk.split(",") : ["","",""];
              var bnkArr = (!!bnkDetails && bnkDetails[0]) ? bnkDetails[0].split("*") : [""];
              if((!self.BankDetails.Bank || !self.BankDetails.Bank.BANK_CODE) && !!bnkArr[2].trim())
                self.BankDetails.Bank = $filter('filter')(bankData,{"bankName" : bnkArr[2].trim()}, true)[0];
              if(!self.BankDetails.BankAccNum && !!bnkArr[1] && !!bnkArr[1].trim())
                self.BankDetails.BankAccNum = (!!bnkArr[1]) ? parseInt(bnkArr[1].trim()) : null;
              if(!self.BankDetails.IfscCode)
                self.BankDetails.IfscCode = (!!bnkArr[5]) ? bnkArr[5] : null;
              if(!self.BankDetails.BranchName)
                self.BankDetails.BranchName = (!!bnkArr[3]) ? bnkArr[3] : null;
              if(!self.BankDetails.AccType)
                self.BankDetails.AccType = (!!bnkArr[4] && !!self.AccTypeNBFECode[bnkArr[4]]) ? self.AccTypeNBFECode[bnkArr[4]] : null;
              dfd.resolve(self.BankDetails);
    	}
    	else
        {
          debug("Already data saved");
          dfd.resolve(self.BankDetails);
        }
      }catch(e){
          debug("Exception in deDupe - BANK DETAILS");
          CommonService.hideLoading();
          dfd.resolve(self.BankDetails);
      }

        return dfd.promise;
    }
    debug("bankDataFHR : "+bankDataFHR);
    this.BankData = bankData;
    if(!!bankDataFHR){
        self.BankDetails.Bank = bankDataFHR.bankName;
        self.BankDetails.BankAccNum = parseInt(bankDataFHR.AccNo);
        self.BankDetails.reenterAccNo = parseInt(bankDataFHR.AccNo);
        self.BankDetails.IfscCode = bankDataFHR.IfscCode;
        self.BankDetails.reenterIfscCode = bankDataFHR.IfscCode;
        self.BankDetails.BranchName = bankDataFHR.branchname;
        self.BankDetails.AccType = bankDataFHR.AccType;
    }else{
      self.BankDetails.Bank=this.BankData[0];
    }
    payDetInfoService.setActiveTab("bankdetails");
    debug("bank details "+JSON.stringify(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST));
    self.nameRegex = USERNAME_REGEX;
	self.ifscRegex = IFSC_REG;

    self.BankDetails.appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    self.BankDetails.agentCD = agentCD;
    self.BankDetails.empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;


    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

    //create blank object to add docids..
    ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
        Reflex :{
            Insured:{},
            Proposer:{}
        },
        NonReflex :{
            Insured:{},
            Proposer:{}
        }
    };

    //29th june..
    if(!!ExistingAppMainData && !!ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST && Object.keys(ExistingAppMainData.applicationPersonalInfo.Insured).length!=0){

         ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
         debug("existing data "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    }else{
        debug("no data in existing object");
    }

    if(!!loadData && !!loadData.BankAccNum){
        debug("load data is "+JSON.stringify(loadData));
        self.BankDetails = loadData;
    }else{
        debug("No data in Database"+JSON.stringify(loadData));
        self.BankDetails.AccHoldName = sisData.sisFormAData.PROPOSER_FIRST_NAME+" "+sisData.sisFormAData.PROPOSER_LAST_NAME;
        self.setDeDupeData().then(function (resp) {
    	      CommonService.hideLoading();
    	  });
    }

    var accountTypeData = ['Current','Saving','NRO'];


    this.accountTypeData = accountTypeData;
    this.neftDocData = NEFTData;

    self.save = function(bankdetForm){
		CommonService.showLoading();
    self.click = true;
        //please check if form is valid..
        if(bankdetForm.$invalid != true ){
            self.BankDetails.appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            self.BankDetails.agentCD = agentCD;
            self.BankDetails.empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
            debug("ssave clicked -- account --"+JSON.stringify(self.BankDetails));

			//changes 25 aug
           if(self.BankDetails.empFlag != "E"){
                bankDetService.saveBankDetails(self.BankDetails).then(
                    function(inserted){
                        if(inserted){
                            debug("move to new page");
                            bankDetService.updateDocListAndScreenFlag(whereClauseObj,ApplicationFormDataService.applicationFormBean.DOCS_LIST);
                            payDetInfoService.setActiveTab("paymentoptions");
                            $state.go("applicationForm.paymentDetails.paymentoption");
                        }
                        else{
                            debug("issue in query");
                        }
                    }
                );
           }else{
                navigator.notification.alert("You are not allowed to do payment.",function() {
					payDetInfoService.setActiveTab("paymentoptions");
                	$state.go("applicationForm.paymentDetails.paymentoption");
                },"Application","OK");
			}
        }else{
            navigator.notification.alert('Please fill all fields with valid data',function(){CommonService.hideLoading();},"Application","OK");
        }

    };

    self.onChangeNeftDoc = function(){
        debug("doc clicked "+ self.BankDetails.NeftDoc);

        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.bankDetail = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.bankDetail = {};

        bankDetService.NEFTDocData().then(
            function(data){
                var neftObj = $filter('filter')(data,{ DOC_ID : self.BankDetails.NeftDoc})[0];
                debug("selected all data "+JSON.stringify(neftObj));
                navigator.notification.alert(neftObj.MPPROOF_DESC +" needs to be submitted.",function(){CommonService.hideLoading();},"Application","OK");
                if(neftObj.DIGITAL_HTML_FLAG == "Y"){
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.bankDetail.DOC_ID = neftObj.DOC_ID;
                }else{
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.bankDetail.DOC_ID = neftObj.DOC_ID;
                }
            }
        );
    }
    CommonService.hideLoading();
}]);

bankDetModule.service('bankDetService',['$state','$q','CommonService','$filter',function($state,$q,CommonService,$filter){
    var self = this;

    //get all bank names
    this.getBankNames = function(){
        var dfd = $q.defer();
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select DISTINCT BANK_NAME,BANK_CODE, ACCOUNT_LENGTH from LP_BANK_MASTER where ISACTIVE='Y' ORDER BY BANK_NAME ASC",[],
                                function(tx,res){
                                    var bankArr = [];
                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                      var bankObj = {};
                                        bankObj.bankName = "Select bank name";
//                                        bankObj.bankCode = "";
//                                        bankObj.acclength = "";
                                        bankArr.push(bankObj);
                                        for(var i=0;i<res.rows.length;i++){
                                            var bankObj = {};
                                            bankObj.bankName = res.rows.item(i).BANK_NAME;
                                            bankObj.bankCode = res.rows.item(i).BANK_CODE;
                                            bankObj.acclength = res.rows.item(i).ACCOUNT_LENGTH;
                                            bankArr.push(bankObj);
                                        }
                                        dfd.resolve(bankArr);
                                    }
                                    else
                                        dfd.resolve(null);
                                },
                                function(tx,err){
                                    dfd.resolve(null);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(null);
                        },null
                    );
        return dfd.promise;
    };

    //get all neft document data..
    this.NEFTDocData = function(){
    var dfd = $q.defer();
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select DOC_ID,MPPROOF_DESC,DIGITAL_HTML_FLAG from LP_DOC_PROOF_MASTER where MPDOC_CODE=? AND ISACTIVE='1'",["NR"],
                                function(tx,res){
                                    var neftArr = [];
                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            var neftObj = {};
                                            neftObj.DOC_ID = res.rows.item(i).DOC_ID;
                                            neftObj.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
                                            neftObj.DIGITAL_HTML_FLAG = res.rows.item(i).DIGITAL_HTML_FLAG;
                                            neftArr.push(neftObj);
                                        }
                                        dfd.resolve(neftArr);
                                    }
                                    else
                                        dfd.resolve(null);
                                },
                                function(tx,err){
                                    dfd.resolve(null);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(null);
                        },null
                    );
        return dfd.promise;
    };

    //insert all bank related details..
    this.saveBankDetails = function(data){
         var dfd = $q.defer();
         var query = "insert or replace into LP_APP_PAY_NEFT_SCRN_H values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
         debug("saving bank details "+JSON.stringify(data));
         var parameters = [data.appID,data.agentCD,data.AccHoldName,data.BankAccNum,data.Bank.bankName,data.BranchName,data.AccType,data.IfscCode,CommonService.getCurrDate(),"Y",null,data.NeftDoc,data.otBank];
         debug("data params "+parameters);

         CommonService.transaction(db,
             function(tx){
                 CommonService.executeSql(tx,query,parameters,
                                             function(tx,res){
                                                 debug("query executed successfully for bank details");
                                                 //inserted = true;
                                                 dfd.resolve(true);
                                             },
                                             function(tx,err){
                                                 debug("query execution failed for bank details"+err);
                                                 dfd.resolve(false);
                                             },null
                                         );

             },
             function(tx,err){
                 debug("transaction error in bank details"+err.message);
                 dfd.resolve(false);
             },
         null);

         return dfd.promise;
    };

    //get all bankDetails
    this.loadBankDetails = function(appID,agentCD){
        var dfd = $q.defer();
        debug("load band details");
        var query = "select NEFT_ACCOUNT_HOLDER_NAME, NEFT_BANK_ACCOUNT_NO, NEFT_BANK_NAME, NEFT_BANK_BRANCH_NAME, NEFT_BANK_ACCOUNT_TYPE, NEFT_BANK_IFSC_CODE,NEFT_DOCUMENT,OTHER_BANK_NAME from LP_APP_PAY_NEFT_SCRN_H where APPLICATION_ID=? AND AGENT_CD=?";
        var parameters = [appID,agentCD];
        debug("data is param --->"+JSON.stringify(parameters));
        var bankDetail = {};

        CommonService.transaction(db,
             function(tx){
                 CommonService.executeSql(tx,query,parameters,
                                             function(tx,res){
                                                 debug("length is "+JSON.stringify(res));
                                                 if(res.rows.length > 0){

                                                    for(var i=0;i<res.rows.length;i++){
                                                        var response = res.rows.item(0);
                                                        debug("response is bank details "+JSON.stringify(response));
                                                        (function(i){
                                                            self.getBankNames().then(
                                                                function(data){
                                                                    bankDetail.Bank = $filter('filter')(data,{bankName : response.NEFT_BANK_NAME})[0];
                                                                    debug("bank details "+ bankDetail.bankName);
                                                                }
                                                            );
                                                            bankDetail.AccHoldName = response.NEFT_ACCOUNT_HOLDER_NAME;
                                                            bankDetail.BankAccNum = parseInt(response.NEFT_BANK_ACCOUNT_NO);
                                                            bankDetail.reenterAccNo = parseInt(response.NEFT_BANK_ACCOUNT_NO);
                                                            bankDetail.BranchName = response.NEFT_BANK_BRANCH_NAME;
                                                            bankDetail.AccType = response.NEFT_BANK_ACCOUNT_TYPE;
                                                            bankDetail.IfscCode = response.NEFT_BANK_IFSC_CODE;
                                                            bankDetail.reenterIfscCode = response.NEFT_BANK_IFSC_CODE;
                                                            bankDetail.NeftDoc = response.NEFT_DOCUMENT;
                                                            bankDetail.otBank = response.OTHER_BANK_NAME;
                                                        }(i))
                                                    }
                                                 }
                                                 dfd.resolve(bankDetail);
                                             },
                                             function(tx,err){
                                                 debug("query execution failed for bank details"+err);
                                                 dfd.resolve(null);
                                             },null
                                         );

             },
             function(tx,err){
                 debug("transaction error in bank details"+err.message);
                 dfd.resolve(null);
             },
         null);
        return dfd.promise;
    };

    this.updateDocListAndScreenFlag = function(whereClause,docslist){
        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(docslist)}, whereClause).then(
            function(res){
                /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN7_COMPLETED" :"Y"}, whereClause).then(
                    function(res){
                        debug("IS_SCREEN7_COMPLETED update success !!"+JSON.stringify(whereClause));
                    }
                );*/
            }
        );
    }


}]);

//directives for comparing password...
bankDetModule.directive('compareTo', function () {
           return {
                   require: "ngModel",
                   scope: {
                       otherModelValue: "=compareTo"
                   },
                   link: function(scope, element, attributes, ngModel) {

                       ngModel.$validators.compareTo = function(modelValue) {
                            //alert(scope.otherModelValue);
                           return modelValue == scope.otherModelValue;
                       };

                       scope.$watch("otherModelValue", function() {
                            //debug("watching");
                           ngModel.$validate();
                       });
                   }
               };
})
