paymentdetailsModule.controller('payDetCtrl',['CommonService',function(CommonService){
    //timeline changes..
    CommonService.hideLoading();

}]);


paymentdetailsModule.service('payDetService',['$state','payDetInfoService','LoadApplicationScreenData','ApplicationFormDataService','LoginService',function($state,payDetInfoService,LoadApplicationScreenData,ApplicationFormDataService,LoginService){

    this.gotoPage = function(pagename){
		if(payDetInfoService.getActiveTab()!==pagename){
			CommonService.showLoading();
		}
        payDetInfoService.setActiveTab(pagename);
        switch(pagename){
            case 'bankdetails':
                        $state.go('applicationForm.paymentDetails.bankdetails');
                        break;
            case 'paymentoptions':{
                            LoadApplicationScreenData.loadBankDetails(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                function(payData){
                                    if(payData!=undefined && Object.keys(payData).length !=0)//if(payData != {})
                                        $state.go('applicationForm.paymentDetails.paymentoption');
                                    else
                                        navigator.notification.alert("Please complete Bank Details",function(){CommonService.hideLoading();},"Application","OK");
                                }
                            )
                        }
                        break;
            case 'renewalPayment':{
                            LoadApplicationScreenData.loadPaymentData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                function(payData){
                                    if(payData!=undefined && Object.keys(payData).length !=0)
                                        $state.go('applicationForm.paymentDetails.renewalPayment');
                                    else
                                        navigator.notification.alert("Please complete Payment Options",function(){CommonService.hideLoading();},"Application","OK");
                                }
                            )
                        }
                        break;
            default:
                        $state.go('applicationForm.paymentDetails.bankdetails');
                        break;
        }
    }

}])
