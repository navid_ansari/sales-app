var paymentdetailsModule = angular.module("app.applicationModule.paymentdetailsModule",[
"app.applicationModule.paymentdetailsModule.bankdetailModule",
"app.applicationModule.paymentdetailsModule.paymentoptionModule",
"app.applicationModule.paymentdetailsModule.renewalPaymentModule"
]);
