paymentdetailsModule.controller('payDetInfoCtrl',['payDetInfoService','payDetService',function(payDetInfoService,payDetService){
    debug("exist controller loaded");

    this.payDetArr = [
        {'type':'bankdetails','name':'Bank Details'},
        {'type':'paymentoptions','name':'Payment Options'},
        {'type':'renewalPayment','name':'Renewal Payment'}
    ];

    this.activeTab = payDetInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        payDetService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.payDetArr));
}]);

paymentdetailsModule.service('payDetInfoService',[function(){
    debug("exist service loaded");

    this.activeTab = "bankdetails";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

paymentdetailsModule.directive('pydTimeline',[function() {
        "use strict";
        debug('in exist');
        return {
            restrict: 'E',
            controller: "payDetInfoCtrl",
            controllerAs: "pydc",
            bindToController: true,
            templateUrl: "applicationForm/paymentDetails/paymentDetailsTimeline.html"
        };
}]);