AppFormModule.service('DeDupeService',['$q', '$state','$stateParams','CommonService','LoginService', function($q, $state,$stateParams, CommonService, LoginService){

var dupe = this;
var APP_ID;
var POLICY_NO;

this.createDeDupeRequest = function (sisData, appData, policyNo, appId) {
  var dfd = $q.defer();
  var request = {};
  request.REQ = {};
  var aadharFlag = false;
  if(!!appData)
  {
      APP_ID = appId;
      POLICY_NO = policyNo;
  }

  if(!!appData && !!appData.Insured)
    aadharFlag = appData.Insured.AADHAR_CARD_EXISTS != 'Y' && (appData.Insured.AADHAR_CARD_NO == undefined || appData.Insured.AADHAR_CARD_NO == "") && appData.Insured.PAN_CARD_EXISTS != 'Y' && (appData.Insured.PAN_CARD_NO == undefined && appData.Insured.PAN_CARD_NO == "");
  //request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
  request.REQ.POLNUM = policyNo || "";
  request.REQ.ORGID = "W00001";//Constant
  request.REQ.SYSTEM = "TAB";//Constant
  //request.REQ.ACN = 'DUP';
  request.REQ.METHOD = "1"; //Constant
  request.REQ.OPT = "3";  //1 - Medical DeDupe , 2 - MED & FIN DeDupe, 3 - All Data DeDupe

  /*if(!!appData && !!appData.Insured)
    request.REQ.id = appData.Insured.IDENTITY_PROOF_NO || "";
  else
    request.REQ.id = "";*/

  var birthDate = "";
  if(!!sisData.sisFormAData.INSURED_DOB){
    var dob = "";
    dob = sisData.sisFormAData.INSURED_DOB;
    dob = dob.substring(0,10);
    dob = dob.replace("-","");
    dob = dob.replace("-","");
    debug("dob :"+dob);
    birthDate = dob.substring(4,8) + "" + dob.substring(2,4) + "" + dob.substring(0,2);
  }
  request.REQ.DOB = birthDate;
  request.REQ.SEX = sisData.sisFormAData.INSURED_GENDER;
  var name = sisData.sisFormAData.INSURED_FIRST_NAME;
  if(!!sisData.sisFormAData.INSURED_MIDDLE)
    name = name + " " + sisData.sisFormAData.INSURED_MIDDLE;

  name = name + " " + sisData.sisFormAData.INSURED_LAST_NAME;
  debug("name :"+name.toUpperCase());
  request.REQ.NAME = name.toUpperCase();
  request.REQ.RELID = 'C01';
  request.REQ.CURPLAN = CommonService.formatText(sisData.sisFormBData.PLAN_CODE, 10, ' '); //Format PlanCode to 10 digits
  if(!!appData && appData.Insured.AADHAR_CARD_EXISTS == 'Y' && !!appData.Insured.AADHAR_CARD_NO)
    request.REQ.ADHAR = appData.Insured.AADHAR_CARD_NO;
  else
    request.REQ.ADHAR = "";
  if(appData.Insured.PAN_CARD_EXISTS == 'Y' && !!appData.Insured.PAN_CARD_NO)
    request.REQ.PAN = appData.Insured.PAN_CARD_NO;
  else
    request.REQ.PAN = "";
    //New keys added
  if(!!sisData.sisFormAData.INSURED_MOBILE)
    request.REQ.MOBNO = sisData.sisFormAData.INSURED_MOBILE || "";
  else
    request.REQ.MOBNO = "";

  if(!!sisData.sisFormAData.INSURED_EMAIL)
  {
    var em = sisData.sisFormAData.INSURED_EMAIL || "";
    request.REQ.EMLID = em.toUpperCase();
  }
  else
    request.REQ.EMLID = "";

      dfd.resolve(request);

    return dfd.promise;
}
this.insertDupeResp = function (resp) {
  var dfd = $q.defer();
  var obj = {};
  var TableName = 'LP_DEDUPE_DATA';
  obj.APPLICATION_ID = APP_ID;
  obj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
  obj.POLICY_NO = POLICY_NO;
  if(!!resp && !!resp.RS && resp.RS.RESP == 'S'){
    if(!!resp.RS.RESULT_APPDATA)
      {
        resp.RS.RESULT_APPDATA = dupe.trimObj(resp.RS.RESULT_APPDATA);
        resp.RS.RESULT_APPDATA.INSURED_ANNUALINCOME = (!!resp.RS.RESULT_APPDATA.INSURED_ANNUALINCOME) ? parseInt(resp.RS.RESULT_APPDATA.INSURED_ANNUALINCOME) : null;
        resp.RS.RESULT_APPDATA.OWNER_ANNUALINCOME = (!!resp.RS.RESULT_APPDATA.OWNER_ANNUALINCOME) ? parseInt(resp.RS.RESULT_APPDATA.OWNER_ANNUALINCOME) : null;
      }
    obj.RESP = (!!resp.RS.RESP) ? resp.RS.RESP : null;
    obj.RESULT_APPDATA = (!!resp.RS.RESULT_APPDATA) ? JSON.stringify(resp.RS.RESULT_APPDATA) : null;
    obj.RESULT_MEDSAR = (!!resp.RS.RESULT_MEDSAR) ? parseInt(resp.RS.RESULT_MEDSAR) : null;
    obj.RESULT_FINSAR = (!!resp.RS.RESULT_FINSAR) ? parseInt(resp.RS.RESULT_FINSAR) : null;
    obj.RESULT_TPREM = (!!resp.RS.RESULT_TPREM) ? parseInt(resp.RS.RESULT_TPREM) : null;
  }
  else {
    obj.RESP = 'F';
    obj.RESULT_APPDATA = null;
    obj.RESULT_MEDSAR = null;
    obj.RESULT_FINSAR = null;
    obj.RESULT_TPREM = null;
  }
  debug("obj :: "+JSON.stringify(obj));
  CommonService.insertOrReplaceRecord(db, TableName, obj, true).then(
  				                function(res){
          									console.log(TableName+" success !!");
          									dfd.resolve(res);
  				                });
    return dfd.promise;
}
this.callDeDupeService = function (reqData, deDupeFlag){
    var dfd = $q.defer();
    function deDupeSucc(resp){
        debug("DE_DUPE Resp in Success: " + JSON.stringify(resp));
        if(!!resp && !!resp.RS && !!resp.RS.RESP && resp.RS.RESP == 'S')
        {
          debug("inserting DE_DUPE");
          dupe.insertDupeResp(resp).then(function(rs) {
            dfd.resolve(rs);
          });
        }
        else
        {
          if(!!resp && !!resp.RS && !!resp.RS.RESP && resp.RS.RESP == 'F') //insert No match data
            dupe.insertDupeResp(resp).then(function(rs) {
              dfd.resolve(null);
            });
          else
              dfd.resolve(null);

        }
    }

    function deDupeErr(data, status, headers, config, statusText) {
        debug("error in DEDUPE");
        dfd.resolve(null);
        CommonService.hideLoading();
    }
    try{
        var request = {};
        request = reqData;

        debug("DE-DUPE Request: " + JSON.stringify(request));
      //  debug("ID flag ::"+JSON.stringify(ExistingAppData.applicationPersonalInfo.Insured));
        if(deDupeFlag)
        {
            debug("No KYC document!! MSAR : 0");
            deDupeSucc(null);
        }
        else
          CommonService.ajaxCall(DUPLICATE_APP_URL/*DUPLICATE_DATA_URL*/, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, deDupeSucc, deDupeErr);
    }catch(ex){
        debug("exception in DEDUPE : " + ex.message);
        dfd.resolve(null);
    }
    return dfd.promise;
};



this.trimObj = function(obj) {
	try{
			if (!Array.isArray(obj) && typeof obj != 'object') return obj;
			return Object.keys(obj).reduce(function(acc, key) {
				acc[key.trim()] = typeof obj[key] == 'string'? obj[key].trim() : trimObj(obj[key]);
				return acc;
			}, Array.isArray(obj)? []:{});
	}catch(e){
		return obj;
	}
}

}]);
