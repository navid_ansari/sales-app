medicalDiagModule.controller('medDiaInfoCtrl',['medDiaInfoService','MedDiagnosisService',function(medDiaInfoService,MedDiagnosisService){
    debug("exist controller loaded");

    this.medDiaArr = [
        {'type':'insMedDiagnosis','name':'Insured','show':medDiaInfoService.Insshow},
        {'type':'propMedDiagnosis','name':'Proposer','show':medDiaInfoService.Proshow}
    ];

    this.activeTab = medDiaInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        MedDiagnosisService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.medDiaArr));
}]);

medicalDiagModule.service('medDiaInfoService',[function(){
    debug("exist service loaded");

    this.activeTab = "insMedDiagnosis";

    this.Insshow = false;
    this.Proshow = false;

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

medicalDiagModule.directive('mdTimeline',[function() {
        "use strict";
        debug('in exist');
        return {
            restrict: 'E',
            controller: "medDiaInfoCtrl",
            controllerAs: "medac",
            bindToController: true,
            templateUrl: "applicationForm/medicalDiagnosis/medicalDiagnosisTimeline.html"
        };
}]);