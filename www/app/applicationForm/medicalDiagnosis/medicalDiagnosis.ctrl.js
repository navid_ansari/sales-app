medicalDiagModule.controller('MedicalCtrl', ['$q','$state','CommonService','LoginService','ApplicationFormDataService','MedicalDCData','medDiaInfoService', function($q, $state, CommonService,LoginService,ApplicationFormDataService,MedicalDCData,medDiaInfoService){
    var med = this;
    this.isIns = false;
    this.isProp = false;
    debug("MedicalCtrl MedicalDCData : " + JSON.stringify(MedicalDCData));

    //timeline changes..

    if(!!MedicalDCData){
        debug("inside medical");
        var insObj = MedicalDCData.Insured;
        var propObj = MedicalDCData.Proposer;
        if(!!insObj && Object.keys(insObj).length>0){
            this.isIns = true;
            medDiaInfoService.Insshow = true;
        }
        if(!!propObj && Object.keys(propObj).length>0){
            debug("inside proposer");
            this.isProp = true;
            medDiaInfoService.Proshow = true;
        }else{
            debug("inside proposer ELSE");
            this.isProp = false;
            medDiaInfoService.Proshow = false;
        }
        debug(medDiaInfoService.Proshow+"isIns : " + this.isIns + " isProp : " + this.isProp);
    }
	CommonService.hideLoading();
}]);

medicalDiagModule.service('MedDiagnosisService',['$q','$state','$filter','CommonService','LoginService','ApplicationFormDataService','LoadReflService','GenerateOutputService','AppTimeService','medDiaInfoService',function($q,$state, $filter, CommonService,LoginService,ApplicationFormDataService,LoadReflService,GenerateOutputService,AppTimeService,medDiaInfoService){
    var self = this;

    this.gotoPage = function(pageName){
		if(medDiaInfoService.getActiveTab()!==pageName){
			CommonService.showLoading();
		}
        debug("pageName : " + pageName);
        medDiaInfoService.setActiveTab(pageName);
        switch(pageName){
            case "insMedDiagnosis" :
                $state.go('applicationForm.medicalDiagnosis.insMedDiagnosis');
                break;
            case "propMedDiagnosis" :
                $state.go('applicationForm.medicalDiagnosis.propMedDiagnosis',{"isInsCompleted" : false});
                break;
        }
    }

    this.determineNextPage = function(){
        var whereClauseObj1 = {};
        whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        whereClauseObj1.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        debug("Inside MedicalCtrl - to next page")
        var reflInsObj = ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured;
        var reflPropObj = ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer;
        debug("reflInsObj : " + JSON.stringify(reflInsObj))
        debug("reflPropObj : " + JSON.stringify(reflPropObj))
        LoadReflService.getInsReflectiveQuesList(reflInsObj,ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.POLICY_NO,ApplicationFormDataService).then(
            function(reflectiveQuesArr){
                LoadReflService.getPropReflectiveQuesList(reflectiveQuesArr,reflPropObj,ApplicationFormDataService.applicationFormBean.APPLICATION_ID,ApplicationFormDataService.applicationFormBean.POLICY_NO,ApplicationFormDataService).then(
                    function(reflectiveQuesArr){
                        console.log("reflectiveQuesArr : " + JSON.stringify(reflectiveQuesArr));
                        if(!!reflectiveQuesArr && reflectiveQuesArr.length>0){
                            LoadReflService.ReflectiveQuesList = reflectiveQuesArr;
                            LoadReflService.getStateName(reflectiveQuesArr).then(
                                function(stateName){
                                    debug("stateName : " + stateName);
                                    if(!!stateName){
                                        //timeline changes
                                        AppTimeService.setActiveTab("reflectiveQues");
                                        AppTimeService.isMedDiaCompleted = true;
                                        //$rootScope.val = 90;
                                        AppTimeService.setProgressValue(90);
                                        MAIN_TAB = 'reflectiveQues';
                                        $state.go(stateName,{"LoadData" : reflectiveQuesArr[0]});
                                    }else{
                                        debug("Reflective State Not Found ");
                                    }
                                }
                            )
                        }
                        else{
                         CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN10_COMPLETED":'NA'},whereClauseObj1).then(
                                function(res){
                                    console.log("Screen updated successfully :IS_SCREEN10_COMPLETED");
                                 MAIN_TAB = 'genOutput';
                                 //timeline changes..
                                 AppTimeService.setActiveTab("genOutput");
                                 AppTimeService.isMedDiaCompleted = true;
                                 //$rootScope.val = 100;
                                 AppTimeService.setProgressValue(100);
                                  GenerateOutputService.generateApplicationOutput(ApplicationFormDataService);
                                }
                            );

                        }
                    }
                );
            }
        )
    }
}]);
