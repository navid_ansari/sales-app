propMedDiagnosisModule.controller('PropMedDiagnosisCtrl', ['$q','$state','CommonService','LoginService','PropMedDiagnosisService','MedicalDCData','ExistingAppData','isInsCompleted','MedDiagnosisService','AppTimeService', function($q, $state, CommonService,LoginService,PropMedDiagnosisService,MedicalDCData,ExistingAppData,isInsCompleted,MedDiagnosisService,AppTimeService){
    var self = this;
    self.medicalBean = {};
    self.click = false;
    self.netStatus = CommonService.checkConnection();
    debug("MedicalDCData : " + JSON.stringify(MedicalDCData));
    debug("ExistingAppData : " + JSON.stringify(ExistingAppData));
    debug("isInsCompleted : " + JSON.stringify(isInsCompleted));
    self.ExistingAppData = ExistingAppData;
    self.medicalBean.MedicalDetailsArr = [];
    self.medicalBean.tpaList = null;
    self.medicalBean.tpaOptionSelected = null;
    self.medicalBean.dcList = null;
    self.medicalBean.dcOptionSelected = null;
    self.isInsCompleted = isInsCompleted;


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



    if(MedicalDCData!=undefined && MedicalDCData.Proposer!=undefined){
        if(MedicalDCData.Proposer.MG_MEDICAL_FLAG == 'O'){
            self.medicalBean.MED_FLAG = 'Y';
            PropMedDiagnosisService.getTestDataList(self.ExistingAppData).then(
                function(testData){
                    debug("testData : " + JSON.stringify(testData));
                    self.medicalBean.MedicalDetailsArr = testData;
                    PropMedDiagnosisService.getTpaList(self.ExistingAppData).then(
                        function(tpaList){
                            debug("tpaList : " + JSON.stringify(tpaList));
                            self.medicalBean.tpaList = tpaList;
                            self.medicalBean.tpaOptionSelected = tpaList[0];
                            debug("self.medicalBean.tpaOptionSelected : " + JSON.stringify(self.medicalBean.tpaOptionSelected));
                            PropMedDiagnosisService.loadDcList(self.medicalBean.tpaOptionSelected.MG_TPA_CODE,self.ExistingAppData).then(
                                function(dcList){
                                    debug("dcList : " + JSON.stringify(dcList));
                                    self.medicalBean.dcList = dcList;
                                    self.medicalBean.dcOptionSelected = dcList[0];
                                    PropMedDiagnosisService.loadMedData(self.ExistingAppData).then(
                                        function(res){
                                            debug("res :" + JSON.stringify(res));
                                            if(!!res){
                                                self.medicalBean.tpaOptionSelected.MG_TPA_CODE = res.MG_TPA_CODE;
                                                self.medicalBean.tpaOptionSelected.MG_TPA_NAME = res.MG_TPA_NAME;
                                                self.medicalBean.MG_PREFERED1_DATE = res.MG_PREFERED1_DATE;
                                                self.medicalBean.MG_PREFERED2_DATE = res.MG_PREFERED2_DATE;
                                                self.medicalBean.MG_PREFERED3_DATE = res.MG_PREFERED3_DATE;
                                                self.medicalBean.MG_DC_HOME_VISIT_FLAG = res.MG_DC_HOME_VISIT_FLAG;
                                                PropMedDiagnosisService.loadDcList(self.medicalBean.tpaOptionSelected.MG_TPA_CODE,self.ExistingAppData).then(
                                                    function(dcList){
                                                        debug("dcList : " + JSON.stringify(dcList));
                                                        self.medicalBean.dcList = dcList;
                                                        self.medicalBean.dcOptionSelected.MG_DC_NAME = res.MG_DC_NAME;
                                                        self.medicalBean.dcOptionSelected.MG_DC_CODE = res.MG_DC_CODE;
                                                    }
                                                );
                                            }
                                        }
                                    );
                                }
                            );
                        }
                    );
                }
            );
        }
        else if(MedicalDCData.Proposer.MG_MEDICAL_FLAG == 'OF'){
            self.medicalBean.MED_FLAG = 'N';
            PropMedDiagnosisService.getTestDataList(self.ExistingAppData).then(
                function(testData){
                    debug("testData : " + JSON.stringify(testData));
                    self.medicalBean.MedicalDetailsArr = testData;
                    PropMedDiagnosisService.getTpaList(self.ExistingAppData).then(
                        function(tpaList){
                            debug("tpaList : " + JSON.stringify(tpaList));
                            self.medicalBean.tpaList = tpaList;
                            self.medicalBean.tpaOptionSelected = tpaList[0];
                            debug("self.medicalBean.tpaOptionSelected : " + JSON.stringify(self.medicalBean.tpaOptionSelected));
                            PropMedDiagnosisService.loadDcList(self.medicalBean.tpaOptionSelected.MG_TPA_CODE,self.ExistingAppData).then(
                                function(dcList){
                                    debug("dcList : " + JSON.stringify(dcList));
                                    self.medicalBean.dcList = dcList;
                                    self.medicalBean.dcOptionSelected = dcList[0];
                                }
                            );
                        }
                    );
                }
            );
        }else{
                     debug("MED_FLAG N")
                 }
    }
    this.onChangeMedFlag = function(){
         self.netStatus = CommonService.checkConnection();
        if(self.medicalBean.MED_FLAG == 'Y'){
            if(self.netStatus == "online"){
                self.medicalBean.MG_MEDICAL_FLAG = 'O';
                CommonService.showLoading("Please Wait...");
                PropMedDiagnosisService.getMedicalSAR(self.ExistingAppData).then(function(medData){
                  var MSAR = 0;
                  if(!!medData && !!medData.RESP && !!medData.RESP.MEDCOV)
                    MSAR = medData.RESP.MEDCOV;

                    PropMedDiagnosisService.saveMedSARData(self.ExistingAppData,MSAR).then(function(res){

                        PropMedDiagnosisService.feathMedDiagData(self.ExistingAppData, MSAR).then(
                                            function(loadData){
                                                debug("resp in feathMedDiagData : " + JSON.stringify(loadData));
                                                CommonService.hideLoading();
                        						if(loadData=="N"){
                                                    self.medicalBean.MED_FLAG = 'N';
                                                    debug("feathMedDiagData failed");
                                                }
                                                else if(!!loadData){
                                                    self.medicalBean.MedicalDetailsArr = loadData.testData;
                                                    self.medicalBean.tpaList = loadData.tpaList;
                                                    self.medicalBean.tpaOptionSelected = loadData.tpaList[0];
                                                    debug("MedicalDetailsArr : " + JSON.stringify(self.medicalBean));
                                                }else{
                                                    navigator.notification.alert("Server not responding please try after some time",function(){CommonService.hideLoading();},"Application","OK");
                                                    self.medicalBean.MED_FLAG = 'N';
                                                    debug("feathMedDiagData failed");
                                                }
                                            }
                                        );
                    });
                });
            }
            else{
                self.medicalBean.MG_MEDICAL_FLAG = 'OF';
                debug("self.medicalBean.MED_FLAG : " + self.medicalBean.MED_FLAG);
                navigator.notification.alert("Please be online for Medical fixation",function(){CommonService.hideLoading();},"Application","OK");
                self.medicalBean.MED_FLAG = 'N';
                debug("self.medicalBean.MED_FLAG : " + self.medicalBean.MED_FLAG);
            }
        }
        else{
            self.medicalBean.MG_MEDICAL_FLAG = 'N';
            self.medicalBean.MedicalDetailsArr = null;
            self.medicalBean.tpaOptionSelected = null;
            self.medicalBean.dcOptionSelected = null;
            self.medicalBean.MG_PREFERED1_DATE = null;
            self.medicalBean.MG_PREFERED2_DATE = null;
            self.medicalBean.MG_PREFERED3_DATE = null;
            self.medicalBean.MG_DC_HOME_VISIT_FLAG = null;
        }
    }

    this.onSave = function(medicalForm){
        self.click = true;
        self.netStatus = CommonService.checkConnection();
        if(medicalForm.$invalid == false){
            if(self.medicalBean.MED_FLAG == 'Y'){
                if(self.netStatus == "online"){
                    CommonService.showLoading("Please Wait...");
                    PropMedDiagnosisService.saveMTRFDetails(self.medicalBean,self.ExistingAppData).then(
                        function(res){
                            debug("res onSave: " + JSON.stringify(res));
                            CommonService.hideLoading();
                            if(!!res){
                                self.medicalBean.isSaved = true;
                                PropMedDiagnosisService.onNext(self.medicalBean,self.ExistingAppData).then(
                                    function(res){
                                        debug("onNext res : " + JSON.stringify(res));
                                        if(!!res){
                                            if(isInsCompleted){
                                                PropMedDiagnosisService.updateScreen9Flag(self.ExistingAppData).then(
                                                    function(res){
                                                        debug("res : " + res);
                                                        //timeline changes..
                                                        //AppTimeService.isMedDiaCompleted = true;
                                                        MedDiagnosisService.determineNextPage();
                                                    }
                                                );
                                            }else{
                                                navigator.notification.alert("Please complete Insured Section",function(){CommonService.hideLoading();},"Application","OK");
                                            }
                                        }
                                    }
                                );
                                navigator.notification.alert("Saved Successfully",function(){CommonService.hideLoading();},"Application","OK");
                            }else{
                                debug("Not saved");
                                navigator.notification.alert("Server not responding please try after some time",function(){CommonService.hideLoading();},"Application","OK");
                            }
                        }
                    );
                }
                else{
                    navigator.notification.alert("Please be online to save data",function(){CommonService.hideLoading();},"Application","OK");
                }
            }else{
                self.medicalBean.isSaved = true;
                PropMedDiagnosisService.onNext(self.medicalBean,self.ExistingAppData).then(
                    function(res){
                        debug("onNext res : " + JSON.stringify(res));
                        if(!!res){
                            self.medicalBean.isSaved = true;
                            PropMedDiagnosisService.onNext(self.medicalBean,self.ExistingAppData).then(
                                function(res){
                                    debug("onNext res : " + JSON.stringify(res));
                                    if(!!res){
                                        if(isInsCompleted){
                                            PropMedDiagnosisService.updateScreen9Flag(self.ExistingAppData).then(
                                                function(res){
                                                    debug("res : " + res);
                                                    //timeline changes..
                                                    AppTimeService.isMedDiaCompleted = true;
                                                    MedDiagnosisService.determineNextPage();
                                                }
                                            );
                                        }else{
                                            navigator.notification.alert("Please complete Insured Section",function(){CommonService.hideLoading();},"Application","OK");
                                        }
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
        else{
            navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
        }
    };

    this.onTpaChg = function(tpaOptionSelected){
        debug("tpaOptionSelected : " + JSON.stringify(tpaOptionSelected));
        self.medicalBean.tpaOptionSelected.MG_TPA_NAME = tpaOptionSelected.MG_TPA_NAME;
        self.medicalBean.tpaOptionSelected.MG_TPA_CODE = tpaOptionSelected.MG_TPA_CODE;
        debug("self.medicalBean.tpaOptionSelected.MG_TPA_CODE : " + self.medicalBean.tpaOptionSelected.MG_TPA_CODE);
        PropMedDiagnosisService.loadDcList(self.medicalBean.tpaOptionSelected.MG_TPA_CODE,self.ExistingAppData).then(
            function(dcList){
                debug("dcList : " + JSON.stringify(dcList));
                self.medicalBean.dcList = dcList;
                self.medicalBean.dcOptionSelected = dcList[0];
            }
        );
    };

    this.onDcChg = function(){
        debug("dcOptionSelected : " + JSON.stringify(self.medicalBean.dcOptionSelected));
    };
    this.clearDate1 = function(){
        self.medicalBean.MG_PREFERED2_DATE = null;
        self.medicalBean.MG_PREFERED3_DATE = null;
    };
    this.clearDate2 = function(){
        self.medicalBean.MG_PREFERED3_DATE = null;
    };
    CommonService.hideLoading();
}]);

propMedDiagnosisModule.service('PropMedDiagnosisService',['$q','$state','$filter','CommonService','LoginService','ApplicationFormDataService',function($q,$state, $filter, CommonService,LoginService,ApplicationFormDataService){
    var self = this;

     this.getMedicalSAR = function(ExistingAppData){
            var dfd = $q.defer();
            function medSARSucc(resp){
                debug("medSAR Resp in Success: " + JSON.stringify(resp));
                if(!!resp)
                    dfd.resolve(resp)
                else
                    dfd.resolve(null);
            }

            function medSARErr(data, status, headers, config, statusText) {
                debug("error in medSAR");
                dfd.resolve(null);
            }
            try{
                var request = {};
                request.REQ = {};
                request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                request.REQ.polNum = ExistingAppData.applicationMainData.POLICY_NO;
                request.REQ.orgId = "W00001";
                request.REQ.ACN = 'DUP';
                request.REQ.id = ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF_NO || "";
                var birthDate = "";
                if(!!ExistingAppData.applicationPersonalInfo.Proposer.BIRTH_DATE){
                  var dob = "";
                  dob = ExistingAppData.applicationPersonalInfo.Proposer.BIRTH_DATE;
                  dob = dob.substring(0,10);
                  dob = dob.replace("-","");
                  dob = dob.replace("-","");
                  debug("dob :"+dob);
                  birthDate = dob.substring(4,8) + "" + dob.substring(2,4) + "" + dob.substring(0,2);
                }
                request.REQ.birthday = birthDate;
                request.REQ.sex = ExistingAppData.applicationPersonalInfo.Proposer.GENDER_CODE;
                var name = ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME;
                if(!!ExistingAppData.applicationPersonalInfo.Proposer.MIDDLE_NAME)
                  name = name + " " + ExistingAppData.applicationPersonalInfo.Proposer.MIDDLE_NAME;

                name = name + " " + ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
                debug("name :"+name.toUpperCase());
                request.REQ.name = name.toUpperCase();
                request.REQ.customerId = 'C02';
                if(ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_EXISTS == 'Y' && !!ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_NO)
                  request.REQ.aadharId = ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_NO;
                else
                  request.REQ.aadharId = "";
                if(ExistingAppData.applicationPersonalInfo.PAN_CARD_EXISTS == 'Y' && !!ExistingAppData.applicationPersonalInfo.Proposer.PAN_CARD_NO)
                  request.REQ.panId = ExistingAppData.applicationPersonalInfo.Proposer.PAN_CARD_NO;
                else
                  request.REQ.panId = "";

                debug("medSAR Request: " + JSON.stringify(request));
                debug("ID flag ::"+JSON.stringify(ExistingAppData.applicationPersonalInfo.Proposer));
                //For Proposer - No Duplication check
                if(request.REQ.customerId == 'C02' || (ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_EXISTS != 'Y' && (ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_NO == undefined || ExistingAppData.applicationPersonalInfo.Proposer.AADHAR_CARD_NO == "") && ExistingAppData.applicationPersonalInfo.PAN_CARD_EXISTS != 'Y' && (ExistingAppData.applicationPersonalInfo.Proposer.PAN_CARD_NO == undefined && ExistingAppData.applicationPersonalInfo.Proposer.PAN_CARD_NO == "")))
                {
                    debug("No KYC document or Proposer!! MSAR : 0");
                    medSARSucc(null);
                }
                else
                    CommonService.ajaxCall(DUPLICATE_DATA_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, medSARSucc, medSARErr);
            }catch(ex){
                debug("exception in featchMedGridEnquiry : " + ex.message);
                dfd.resolve(null);
            }
            return dfd.promise;
        };

        this.saveMedSARData = function(ExistingAppData, MSAR){
            var dfd = $q.defer();
            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = ExistingAppData.applicationMainData.APPLICATION_ID;
            whereClauseObj.AGENT_CD = ExistingAppData.applicationMainData.AGENT_CD;
            whereClauseObj.CUST_TYPE = 'C02';
            CommonService.updateRecords(db,"LP_APP_CONTACT_SCRN_A",{"MSAR": MSAR},whereClauseObj).then(
                function(res){
                    console.log("MSAR updated successfully :LP_APP_CONTACT_SCRN_A");
                    dfd.resolve(true);
                }
            );
            return dfd.promise;
        };

    this.feathMedDiagData = function(ExistingAppData, MSAR){
        var dfd = $q.defer();
        var loadData = {};
         TERM_FLAG = false;
                COMBO_ID =  ApplicationFormDataService.ComboFormBean.COMBO_ID;
                OPP_ID = ApplicationFormDataService.applicationFormBean.OPP_ID;
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                SIS_ID = ApplicationFormDataService.SISFormData.sisMainData.SIS_ID;
                REF_SIS_ID = ApplicationFormDataService.ComboFormBean.REF_SIS_ID;
                debug("COMBO DATTA "+JSON.stringify(ApplicationFormDataService.applicationFormBean));
                if(!!COMBO_ID){
                    LoadApplicationScreenData.loadOppFlags(OPP_ID, AGENT_CD, 'opp').then(function(oppData){
                    // SisFormService.loadSavedSISData(AGENT_CD, SIS_ID).then(function(sisData){
                       SisFormService.loadSavedSISData(AGENT_CD, REF_SIS_ID).then(function(sisTermData){
                            if(oppData.oppFlags.REF_NML_FLAG == 'Y' && oppData.oppFlags.REF_NML_AMT <= sisData.sisFormBData.SUM_ASSURED)
                                TERM_FLAG = false;
                            else
                                TERM_FLAG = true;

                        self.featchMedGridEnquiry(ExistingAppData, oppData, sisTermData, MSAR).then(
                                    function(resp){
                                        debug(MSAR+"featchMedGridEnquiry resp : " + JSON.stringify(resp));
                                        if(!!resp && resp.RES.RESPSTATUS==='S'){
                                            if(resp.RES.MEDICALCASEFLAG==='Y'){
                                                self.saveMedTestData(ExistingAppData,resp).then(
                                                    function(res){
                                                        debug("saveMedTestData res : " + JSON.stringify(res));
                                                        self.saveMedGridEnqResp(ExistingAppData,resp, oppData, sisTermData).then(
                                                            function(res){
                                                                debug("saveMedGridEnqResp res : " + JSON.stringify(res));
                                                                self.getTestDataList(ExistingAppData).then(
                                                                    function(testData){
                                                                        debug("getTestDataList : " + JSON.stringify(testData));
                                                                        loadData.testData = testData;
                                                                        self.getTpaList(ExistingAppData).then(
                                                                            function(tpaList){
                                                                                debug("tpaList : " + JSON.stringify(tpaList));
                                                                                loadData.tpaList = tpaList;
                                                                                dfd.resolve(loadData);
                                                                            }
                                                                        );
                                                                    }
                                                                );
                                                            }
                                                        );
                                                    }
                                                );
                                            }else if(resp.RES.MEDICALCASEFLAG==='N'){
                                                debug("Medical diagnosis not required");
                                                navigator.notification.alert("Medical diagnosis not required",function(){CommonService.hideLoading();},"Application","OK");
                                                dfd.resolve("N");
                                            }else{
                                                debug("something went wrong");
                                                dfd.resolve(null);
                                            }
                                        }
                                        else if(!!resp && resp.RES.RESPSTATUS==='F'){
                                            navigator.notification.alert("Error : " + resp.RES.ERRMSG,function(){CommonService.hideLoading();},"Application","OK");
                                            dfd.resolve(null);
                                        }
                                        else{
                                            debug("feathMedDiagData Failed - resp null");
                                            dfd.resolve(null);
                                        }
                                    }
                                );
                       });
                    });
                    }
                    else
                        self.featchMedGridEnquiry(ExistingAppData, null, null, MSAR).then(
                            function(resp){
                                debug("featchMedGridEnquiry resp : " + JSON.stringify(resp));
                                if(!!resp && resp.RES.RESPSTATUS==='S'){
                                    if(resp.RES.MEDICALCASEFLAG==='Y'){
                                        self.saveMedTestData(ExistingAppData,resp).then(
                                            function(res){
                                                debug("saveMedTestData res : " + JSON.stringify(res));
                                                self.saveMedGridEnqResp(ExistingAppData,resp).then(
                                                    function(res){
                                                        debug("saveMedGridEnqResp res : " + JSON.stringify(res));
                                                        self.getTestDataList(ExistingAppData).then(
                                                            function(testData){
                                                                debug("getTestDataList : " + JSON.stringify(testData));
                                                                loadData.testData = testData;
                                                                self.getTpaList(ExistingAppData).then(
                                                                    function(tpaList){
                                                                        debug("tpaList : " + JSON.stringify(tpaList));
                                                                        loadData.tpaList = tpaList;
                                                                        dfd.resolve(loadData);
                                                                    }
                                                                );
                                                            }
                                                        );
                                                    }
                                                );
                                            }
                                        );
                                    }else if(resp.RES.MEDICALCASEFLAG==='N'){
                                        debug("Medical diagnosis not required");
                                        navigator.notification.alert("Medical diagnosis not required",function(){CommonService.hideLoading();},"Application","OK");
                                        dfd.resolve("N");
                                    }else{
                                        debug("something went wrong")
                                    }
                                }
                                else if(!!resp && resp.RES.RESPSTATUS==='F'){
                                    navigator.notification.alert("Error : " + resp.RES.ERRMSG,function(){CommonService.hideLoading();},"Application","OK");
                                    dfd.resolve(null);
                                }
                                else{
                                    debug("feathMedDiagData Failed - resp null");
                                    dfd.resolve(null);
                                }
                            }
                        );
        return dfd.promise;
    };

    this.featchMedGridEnquiry = function(ExistingAppData, oppData, sisTermData, MSAR){
        var dfd = $q.defer();
        function medGridSucc(resp){
            debug(MSAR+"featchMedGridEnquiry Resp in Success: " + JSON.stringify(resp));
            if(!!resp)
                dfd.resolve(resp)
            else
                dfd.resolve(null);
        }

        function medGridErr(data, status, headers, config, statusText) {
            debug("error in fetchSpDetails");
            dfd.resolve(null);
        }
        try{
            var request = {};
            request.REQ = {};
            request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.PWD = LoginService.lgnSrvObj.password;
            request.REQ.DVID = LoginService.lgnSrvObj.dvid;
            request.REQ.BTI = BUSINESS_TYPE;
            request.REQ.ACN = 'VMD';
            request.REQ.MEDDETS = {};
            request.REQ.MEDDETS.PlanCode = ApplicationFormDataService.SISFormData.sisFormBData.PLAN_CODE;
            request.REQ.MEDDETS.AgentCode = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.MEDDETS.SumAssured = (ApplicationFormDataService.SISFormData.sisMainData.PGL_ID == "226") ? ApplicationFormDataService.SISFormData.sisMainData.SAR : ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED;
            request.REQ.MEDDETS.Age = CommonService.getAge(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_DOB) + "";
            request.REQ.MEDDETS.SourceChannel = "IBL";
			if(BUSINESS_TYPE!='IndusSolution'){
				request.REQ.MEDDETS.SourceChannel = LoginService.lgnSrvObj.userinfo.CHANNEL_NAME;
			}
            request.REQ.MEDDETS.Source = "Tabsale";
            request.REQ.MEDDETS.Sex = ExistingAppData.applicationPersonalInfo.Proposer.GENDER;
            request.REQ.MEDDETS.Qualification = ExistingAppData.applicationPersonalInfo.Proposer.EDUCATION_QUALIFICATION;
            request.REQ.MEDDETS.CustomerType = "NA";
            request.REQ.MEDDETS.CustomerCity = ExistingAppData.applicationPersonalInfo.Proposer.CURR_CITY;
            request.REQ.MEDDETS.BankRelationDuration = "10";
            request.REQ.MEDDETS.HomeVisit = "N";
            request.REQ.MEDDETS.Pincode = ExistingAppData.applicationPersonalInfo.Proposer.CURR_PIN;
            //DE-DUPE
            request.REQ.MEDDETS["Medical SAR"] = MSAR + "";
            debug("OPP DATA >>>>>> "+JSON.stringify(oppData));

            if(!!oppData && !!oppData.oppFlags.COMBO_ID && !!sisTermData && !!sisTermData.sisFormBData.SUM_ASSURED)
                if(oppData.oppFlags.REF_NML_FLAG == 'Y' && oppData.oppFlags.REF_NML_AMT >= sisTermData.sisFormBData.SUM_ASSURED)
                    {
                        //Nothing to do
                        debug(oppData.oppFlags.REF_NML_FLAG+"COMBO CONDITION PROPOSER>>>>>> REF_NML_AMT:"+oppData.oppFlags.REF_NML_AMT+"SUM_ASSURED:"+sisTermData.sisFormBData.SUM_ASSURED);
                    }
                    else
                    {
                        debug(oppData.oppFlags.REF_NML_FLAG+"COMBO CONDITION PROPOSER>>>>>> REF_NML_AMT:"+oppData.oppFlags.REF_NML_AMT+"SUM_ASSURED:"+sisTermData.sisFormBData.SUM_ASSURED);
                        request.REQ.MEDDETS.comboflag = 'Y';
                        request.REQ.MEDDETS.PlanCode1 = sisTermData.sisFormBData.PLAN_CODE;
                        request.REQ.MEDDETS.SumAssured1 = sisTermData.sisFormBData.SUM_ASSURED;
                    }
            debug("fetchSpDetails Request: " + JSON.stringify(request));
            CommonService.ajaxCall(MEDICAL_GRID_ENQUIRY_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, medGridSucc, medGridErr);
        }catch(ex){
            debug("exception in featchMedGridEnquiry : " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    };

    this.saveMedTestData = function(ExistingAppData,resp){
        var dfd = $q.defer();
        var testNameStr = "";
        var testDetailsStr = "";

        function update(testNameStr,testDetailsStr,ExistingAppData){
            debug("testNameStr : " + testNameStr + "testDetailsStr : " + testDetailsStr);
            debug("ExistingAppData : " + JSON.stringify(ExistingAppData));
            CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"update LP_APP_SAR_MED_SCREEN_K set MG_MEDICAL_TEST_LIST = ?,MG_MEDICAL_TEST_DETAILS = ? where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[testNameStr,testDetailsStr,ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'],
                        function(tx,res){
                            debug("res : " + JSON.stringify(res));
                            debug("LP_APP_SAR_MED_SCREEN_K Updated ");
                            dfd.resolve(true);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );
        }

        for(var i = 0; i<resp.RES.MEDICALTESTDETAILS.length;i++){
            testNameStr += resp.RES.MEDICALTESTDETAILS[i].TESTNAME + '$';
            testDetailsStr += resp.RES.MEDICALTESTDETAILS[i].TESTDETAILS + '$';
            debug("i : " + i);
            if(i==(resp.RES.MEDICALTESTDETAILS.length-1))
                update(testNameStr,testDetailsStr,ExistingAppData);
        }
        return dfd.promise;
    }

    this.saveMedGridEnqResp = function(ExistingAppData,resp, oppData, sisTermData){
        var dfd = $q.defer();
        debug("resp : " + JSON.stringify(resp));
        var mainDcArr = resp.RES.DC;
        debug("main arr created : " + mainDcArr.length);
        var promises = [];
        var seqNo = 0;
        var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        var APPLICATION_ID = ExistingAppData.applicationMainData.APPLICATION_ID;
        var POLICY_NO = ExistingAppData.applicationMainData.POLICY_NO;

        function getData(TPACODE,TPANAME,DC,i,j,seqNo,AGENT_CD,APPLICATION_ID,POLICY_NO){
            self.insert(TPACODE,TPANAME,DC,i,j,seqNo,AGENT_CD,APPLICATION_ID,POLICY_NO).then(
                function(tpaNo){
                    debug("inserted : " + tpaNo);
                    if(!!tpaNo)
                        if(tpaNo == mainDcArr.length)
                            dfd.resolve(resp);
                    else
                            dfd.resolve(null)
                }
            );
        }
        if(!!mainDcArr && mainDcArr.length>0){
        for(var i =0;i<mainDcArr.length;i++){
            var data = mainDcArr[i];
            var TPACODE = data.TPACODE;
            var DC = data.DC;
            var TPANAME = data.TPANAME;

            debug("first loop >>>>"+i);

            for(var j=0 ;j<DC.length;j++){
                debug("inner second loop >>>>"+j);
                var DCObj = DC[j];
                seqNo++;
                promises.push(getData(TPACODE,TPANAME,DCObj,i,j,seqNo,AGENT_CD,APPLICATION_ID,POLICY_NO));
            }
        }
        }else{
            navigator.notification.alert("There are no Digonostic center available, Medicals will be fixed by your relationship manager.",function(){CommonService.hideLoading();},"Application","OK");
            dfd.resolve(null);
        }
        return dfd.promise;
    }

    this.insert = function(TPACODE,TPANAME,DC,i,j,seqNo,AGENT_CD,APPLICATION_ID,POLICY_NO){
        var dfd = $q.defer();

        debug("data received is >>>>> "+TPACODE+"<<<<< NAME >>>"+TPANAME+"<<<<< I >>>"+i+"<<<<< J >>>>>"+j+"<<<<< seqNo >>>>"+seqNo);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"insert or replace into LP_APP_MED_DC_SCREEN_K values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[APPLICATION_ID,AGENT_CD,POLICY_NO,'C02',seqNo,TPACODE.trim(),TPANAME.trim(),DC.PROVIDERCODE.trim(),DC.PROVIDERNAME.trim(),DC.ADDLINE1.trim(),DC.CITY.trim(),DC.PINCODE.trim(),DC.HV.trim(),DC.PREFEREDDC.trim(),CommonService.getCurrDate()],
                    function(tx,res){
                        debug("TPA & DC insert : " + JSON.stringify(res))
                        dfd.resolve(i);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );

        return dfd.promise;
    };

    this.getTestDataList = function(ExistingAppData){
        var dfd = $q.defer();
        var testData = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select MG_MEDICAL_TEST_LIST, MG_MEDICAL_TEST_DETAILS from LP_APP_SAR_MED_SCREEN_K where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'],
                    function(tx,res){
                        debug("getTestDataList length: " + JSON.stringify(res));
                        if(!!res && res.rows.length>0){
                            var medTestList = (res.rows.item(0).MG_MEDICAL_TEST_LIST).split('$');
                            var medTestDetails = (res.rows.item(0).MG_MEDICAL_TEST_DETAILS).split('$');
                            for(var i=0;i<(medTestList.length-1);i++){
                                var obj = {};
                                obj.TESTNAME = medTestList[i];
                                obj.TESTDETAILS = medTestDetails[i];
                                testData.push(obj);
                            }
                            dfd.resolve(testData);
                        }else{
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    }

    this.getTpaList = function(ExistingAppData){
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT DISTINCT MG_TPA_CODE,MG_TPA_NAME FROM LP_APP_MED_DC_SCREEN_K where APPLICATION_ID = ? and AGENT_CD = ? and POLICY_NO = ?",[ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,ExistingAppData.applicationMainData.POLICY_NO],
                    function(tx,res){
                        var tpaList = [];
                        debug("tpa length : " + res.rows.length);
                        tpaList.push({"MG_TPA_NAME": "Select TPA", "MG_TPA_CODE": null});
                        if(!!res && res.rows.length>0){
                            for(var i=0;i<res.rows.length;i++){
                                var tpa = {};
                                tpa.MG_TPA_CODE = res.rows.item(i).MG_TPA_CODE;
                                tpa.MG_TPA_NAME = res.rows.item(i).MG_TPA_NAME;
                                tpaList.push(tpa);
                            }
                            dfd.resolve(tpaList);
                        }
                        else
                            dfd.resolve(tpaList);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.loadDcList = function(TPACODE,ExistingAppData){
        var dfd = $q.defer();
        debug("TPACODE : " + TPACODE);
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select MG_DC_CODE, MG_DC_NAME,MG_DC_ADDRESS,MG_DC_CITY,MG_DC_PIN, MG_DC_HOME_VISIT_FLAG, MG_PREFERED_FLAG from LP_APP_MED_DC_SCREEN_K where MG_TPA_CODE = ? and APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[TPACODE,ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'],
                    function(tx,res){
                        var dcList = [];
                        debug("dc length : " + res.rows.length);
                        dcList.push({"MG_DC_NAME": "Select DC", "MG_DC_CODE": null});
                        if(!!res && res.rows.length>0){
                            for(var i=0;i<res.rows.length;i++){
                                var dc = {};
                                dc.MG_DC_CODE = res.rows.item(i).MG_DC_CODE;
                                dc.MG_DC_NAME = res.rows.item(i).MG_DC_NAME;
                                dc.MG_DC_ADDRESS = res.rows.item(i).MG_DC_ADDRESS;
                                dc.MG_DC_CITY = res.rows.item(i).MG_DC_CITY;
                                dc.MG_DC_PIN = res.rows.item(i).MG_DC_PIN;
                                dc.MG_DC_HOME_VISIT_FLAG = res.rows.item(i).MG_DC_HOME_VISIT_FLAG;
                                dc.MG_PREFERED_FLAG = res.rows.item(i).MG_PREFERED_FLAG;
                                dcList.push(dc);
                            }
                            dfd.resolve(dcList);
                        }
                        else
                            dfd.resolve(dcList);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.saveMTRFDetails = function(medicalBean,ExistingAppData){
        var dfd = $q.defer();
        debug("medicalBean : " + JSON.stringify(medicalBean,ExistingAppData));
        self.fetchMTRFSavedData(medicalBean,ExistingAppData).then(
            function(resp){
                debug("resp : " + JSON.stringify(resp));
                if(!!resp && resp.RES.RESPSTATUS == 'S'){
                    self.updateSARTable(medicalBean,ExistingAppData).then(
                        function(res){
                            debug("res : " + JSON.stringify(res));
                            if(!!res){
                                dfd.resolve(true);
                            }else{
                                dfd.resolve(false);
                            }
                        }
                    );
                }else if(!!resp && resp.RES.RESPSTATUS == 'F'){
                    navigator.notification.alert("Error : " + resp.RES.MSG,function(){CommonService.hideLoading();},"Application","OK");
                    dfd.resolve(null);
                }else{
                    debug("resp Null");
                    dfd.resolve(null);
                }
            }
        )
        return dfd.promise;
    };

    this.fetchMTRFSavedData = function(medicalBean,ExistingAppData){
        var dfd = $q.defer();
        var today = new Date();
        var validDate = CommonService.formatDobToDb(new Date(today.setDate(today.getDate()+30))).substring(0,10) + "";
        debug("validDate : " + validDate);
        var insTstsDet = "";
        for(var i=0;i<medicalBean.MedicalDetailsArr.length;i++){
            insTstsDet += medicalBean.MedicalDetailsArr[i].TESTNAME + '$';
        }
        function mtrfSucc(resp){
            debug("fetchMTRFSavedData Resp in Success: " + JSON.stringify(resp));
            if(!!resp)
                dfd.resolve(resp)
            else
                dfd.resolve(null);
        }

        function mtrfErr(data, status, headers, config, statusText) {
            debug("error in fetchSpDetails");
            dfd.resolve(null);
        }
        try{
            var request = {};
            request.REQ = {};
            request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.PWD = LoginService.lgnSrvObj.password;
            request.REQ.DVID = LoginService.lgnSrvObj.dvid;
            request.REQ.BTI = BUSINESS_TYPE;
            request.REQ.ACN = 'SMD';
            request.REQ.MTRFDETS = {};
            request.REQ.MTRFDETS.POLICYNO = ExistingAppData.applicationMainData.POLICY_NO;
            request.REQ.MTRFDETS.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.MTRFDETS.HNIFLG = "N";
            request.REQ.MTRFDETS.APPCD = "NA";
            request.REQ.MTRFDETS.ZCD = "HO";
            request.REQ.MTRFDETS.RDR = (!!ApplicationFormDataService.SISFormData.sisFormDData) ? (!!ApplicationFormDataService.SISFormData.sisFormDData[0].RIDER_CODE ? 'Yes' : 'No') : 'No';
            request.REQ.MTRFDETS.RDRSA = ((!!ApplicationFormDataService.SISFormData.sisFormDData) ? (!!ApplicationFormDataService.SISFormData.sisFormDData[0].RIDER_COVERAGE ? ApplicationFormDataService.SISFormData.sisFormDData[0].RIDER_COVERAGE : "0"): "0") || "0";
            request.REQ.MTRFDETS.SA = ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED;
            request.REQ.MTRFDETS.PRDCD = ApplicationFormDataService.SISFormData.sisFormBData.PLAN_CODE;
            request.REQ.MTRFDETS.CHNL = "BANCA";
			if(BUSINESS_TYPE!='IndusSolution'){
				request.REQ.MTRFDETS.CHNL = LoginService.lgnSrvObj.userinfo.CHANNEL_NAME;
			}
            request.REQ.MTRFDETS.NRIFLG = (ExistingAppData.applicationPersonalInfo.Proposer.RESIDENT_STATUS_CODE == 'NRI') ? 'Y' : 'N';
            request.REQ.MTRFDETS.HIVCD = "None";
            request.REQ.MTRFDETS.IDPRF = ExistingAppData.applicationPersonalInfo.Proposer.IDENTITY_PROOF;
            request.REQ.MTRFDETS.MTRFTYP = "I";
            request.REQ.MTRFDETS.GENDT = CommonService.getCurrDate().substring(0,10);
            request.REQ.MTRFDETS.VLDTE = validDate;
            request.REQ.MTRFDETS.DPTCODE = "POS";
            request.REQ.MTRFDETS.OPRID = "Tabsale";
            request.REQ.MTRFDETS.CUSTNAME = ExistingAppData.applicationPersonalInfo.Proposer.FIRST_NAME + (!!ExistingAppData.applicationPersonalInfo.Proposer.MIDDLE_NAME ? " " + ExistingAppData.applicationPersonalInfo.Proposer.MIDDLE_NAME : "") + " " + ExistingAppData.applicationPersonalInfo.Proposer.LAST_NAME;
            request.REQ.MTRFDETS.SEX = ExistingAppData.applicationPersonalInfo.Proposer.GENDER;
            request.REQ.MTRFDETS.DOB = ExistingAppData.applicationPersonalInfo.Proposer.BIRTH_DATE.substring(0,10);
            request.REQ.MTRFDETS.MRTLSTATS = ExistingAppData.applicationPersonalInfo.Proposer.MARTIAL_STATUS;
            request.REQ.MTRFDETS.PHN = ExistingAppData.applicationPersonalInfo.Proposer.MOBILE_NO || "null";
            request.REQ.MTRFDETS.HMADD = ExistingAppData.applicationPersonalInfo.Proposer.CURR_ADD_LINE1 + " " + ExistingAppData.applicationPersonalInfo.Proposer.CURR_ADD_LINE2 + ExistingAppData.applicationPersonalInfo.Proposer.CURR_ADD_LINE3;
            request.REQ.MTRFDETS.INSTSTDET = insTstsDet;
            request.REQ.MTRFDETS.HMVSTFLG = medicalBean.MG_DC_HOME_VISIT_FLAG;
            request.REQ.MTRFDETS.OTHTSTDET = "NA";
            request.REQ.MTRFDETS.OTHTSTDESC = "NA";
            request.REQ.MTRFDETS.OTHHMVSTFLG = "NA";
            request.REQ.MTRFDETS.SCNDT = CommonService.getCurrDate().substring(0,10);
            request.REQ.MTRFDETS.CHRGBORNBY = "TATA-AIA";
            request.REQ.MTRFDETS.CHNDCRSN = "NA";
            //as per Vikas from sanjits's team
            request.REQ.MTRFDETS.SPCLAPRVL = "null";
            request.REQ.MTRFDETS.APPRVALDT = "null";
            request.REQ.MTRFDETS.DCCD = medicalBean.dcOptionSelected.MG_DC_CODE;
            request.REQ.MTRFDETS.PRFDT1 = (!!medicalBean.MG_PREFERED1_DATE) ?  (CommonService.formatDobToDb(medicalBean.MG_PREFERED1_DATE).substring(0,10)) : "null";
            request.REQ.MTRFDETS.PRFDT2 = (!!medicalBean.MG_PREFERED2_DATE) ?  (CommonService.formatDobToDb(medicalBean.MG_PREFERED2_DATE).substring(0,10)) : "null";
            request.REQ.MTRFDETS.PRFDT3 = (!!medicalBean.MG_PREFERED3_DATE) ?  (CommonService.formatDobToDb(medicalBean.MG_PREFERED3_DATE).substring(0,10)) : "null";
            request.REQ.MTRFDETS.EmailId = (!!ExistingAppData.applicationPersonalInfo.Proposer.EMAIL_ID) ? ExistingAppData.applicationPersonalInfo.Proposer.EMAIL_ID : "null";

            debug("fetchMTRFSavedData Request: " + JSON.stringify(request));
            CommonService.ajaxCall(MTRF_DETAILS_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, mtrfSucc, mtrfErr);
        }catch(ex){
            debug("exception in fetchMTRFSavedData : " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    }

    this.updateSARTable = function(medicalBean,ExistingAppData){
        var dfd = $q.defer();
        debug("updateSARTable medicalBean : " + JSON.stringify(medicalBean));
        var pDate2 = (!!medicalBean.MG_PREFERED2_DATE) ? CommonService.formatDobToDb(medicalBean.MG_PREFERED2_DATE) : null;
        var pDate3 = (!!medicalBean.MG_PREFERED3_DATE) ? CommonService.formatDobToDb(medicalBean.MG_PREFERED3_DATE) : null;
        var params = ['O','Y',medicalBean.tpaOptionSelected.MG_TPA_CODE,medicalBean.tpaOptionSelected.MG_TPA_NAME,medicalBean.dcOptionSelected.MG_DC_CODE,medicalBean.dcOptionSelected.MG_DC_NAME,medicalBean.dcOptionSelected.MG_DC_ADDRESS,medicalBean.dcOptionSelected.MG_DC_CITY,medicalBean.dcOptionSelected.MG_DC_PIN,medicalBean.dcOptionSelected.MG_DC_HOME_VISIT_FLAG,CommonService.formatDobToDb(medicalBean.MG_PREFERED1_DATE),pDate2,pDate3,null,CommonService.getCurrDate(),ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'];
        debug("params : " + JSON.stringify(params));
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"update LP_APP_SAR_MED_SCREEN_K set MG_MEDICAL_FLAG = ?,MG_SELECTED_DC_FLAG = ?,MG_TPA_CODE = ?,MG_TPA_NAME = ?,MG_DC_CODE = ?,MG_DC_NAME = ?,MG_DC_ADDRESS = ?,MG_DC_CITY = ?,MG_DC_PIN = ?,MG_DC_HOME_VISIT_FLAG = ?,MG_PREFERED1_DATE = ?,MG_PREFERED2_DATE = ?,MG_PREFERED3_DATE = ?,MG_MTRF_SRNO = ?,MODIFIED_DATE = ? where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",params,
                    function(tx,res){
                        debug("res : " + JSON.stringify(res));
                        debug("LP_APP_SAR_MED_SCREEN_K Updateded ");
                        dfd.resolve(true);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.loadMedData = function(ExistingAppData){
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select * from LP_APP_SAR_MED_SCREEN_K where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'],
                    function(tx,res){
                        debug("loadMedData length : " + JSON.stringify(res));
                        if(!!res && res.rows.length>0){
                            dfd.resolve(res.rows.item(0));
                        }
                        else
                            dfd.resolve(null);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.onNext = function(medicalBean,ExistingAppData){
        var dfd = $q.defer();
        if(medicalBean.MED_FLAG == 'N'){
            CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,"update LP_APP_SAR_MED_SCREEN_K set MG_MEDICAL_FLAG = ?, MODIFIED_DATE = ? where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",['OF',CommonService.getCurrDate(),ExistingAppData.applicationMainData.APPLICATION_ID,ExistingAppData.applicationMainData.AGENT_CD,'C02'],
                        function(tx,res){
                            debug("res : " + JSON.stringify(res));
                            debug("LP_APP_SAR_MED_SCREEN_K OF Flag Updateded ");
                            dfd.resolve(true);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );
        }else{
            if(medicalBean.isSaved)
                dfd.resolve(true);
            else{
                navigator.notification.alert("Please Save the data",function(){CommonService.hideLoading();},"Application","OK");
                dfd.resolve(false);
            }
        }

        return dfd.promise;
    };

    this.updateScreen9Flag = function(ExistingAppData){
        var dfd = $q.defer();
        var whereClauseObj = {};
        whereClauseObj.APPLICATION_ID = ExistingAppData.applicationMainData.APPLICATION_ID;
        whereClauseObj.AGENT_CD = ExistingAppData.applicationMainData.AGENT_CD;
        CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN9_COMPLETED":'Y'},whereClauseObj).then(
            function(res){
                console.log("Screen updated successfully :IS_SCREEN9_COMPLETED");
                dfd.resolve(true);
            }
        );
        return dfd.promise;
    };

}]);
