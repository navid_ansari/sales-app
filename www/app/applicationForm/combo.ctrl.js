AppFormModule.service('ComboService',['$q', '$state','$stateParams','CommonService','LoginService','LoadApplicationScreenData', 'SisFormService', function($q, $state,$stateParams, CommonService, LoginService, LoadApplicationScreenData, SisFormService){

var com = this;/*
var REF_APP_ID = '999999999';
var REF_POLNO = 'C999999999';*/

this.replicateTermData = function(COMBO_ID, OPP_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

    var dfd = $q.defer();
    var REF_SIS_ID;
    CommonService.showLoading("Saving Term Details...");
    LoadApplicationScreenData.loadOppFlags(COMBO_ID, AGENT_CD, 'combo','P').then(function(oppData){
            REF_SIS_ID = oppData.oppFlags.REF_SIS_ID;

            com.insertAppMainData(COMBO_ID, AGENT_CD, OPP_ID, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, REF_SIS_ID).then(function(flag){
                if(flag)
                com.insertContactScrnAData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                        if(flag)
                        com.insertLSData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                if(flag)
                                com.insertHDData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                        if(flag)
                                        com.insertHDSubData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                if(flag)
                                                com.insertExtInsData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                        if(flag)
                                                         com.insertFamilyData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                if(flag)
                                                               com.insertNomineeData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                        if(flag)
                                                                         com.insertPaymentData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                                if(flag)
                                                                                com.insertNEFTData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                                        if(flag)
                                                                                         com.insertRenewalPayData(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                                              if(flag)
                                                                                              com.insertTermProfilePic(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO).then(function(flag){
                                                                                                    console.log("Replication Done !!");
                                                                                                     dfd.resolve(true);
                                                                                              });
                                                                                        });
                                                                                   });

                                                                             });
                                                                   });
                                                             });
                                                    });
                                            });
                                   });
                            });
                    });
            });

    });
 return dfd.promise;
}



this.insertTermProfilePic = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){
var dfd = $q.defer();
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_DOCUMENT_UPLOAD', null, false, true, 'PHOTOGRAPH').then(
                    function(data){
                        if(!!data)
                        {
                            var fileName = data[0].AGENT_CD+"_"+REF_APP_ID+"_"+data[0].DOC_ID+".jpg";
                            var doc_cap = CommonService.getRandomNumber();
                            var param = {"DOC_ID":data[0].DOC_ID, "AGENT_CD":data[0].AGENT_CD, "DOC_CAT":data[0].DOC_CAT, "DOC_CAT_ID":REF_APP_ID, "POLICY_NO": REF_POLNO, "DOC_NAME":fileName, "DOC_TIMESTAMP":data[0].DOC_TIMESTAMP, "DOC_PAGE_NO":data[0].DOC_PAGE_NO, "IS_FILE_SYNCED":'Y', "IS_DATA_SYNCED":'N',"IS_PENDING":null, "DOC_CAP_ID":doc_cap, "DOC_CAP_REF_ID":data[0].DOC_CAP_ID};
                               CommonService.insertOrReplaceRecord(db, 'LP_DOCUMENT_UPLOAD', param, true).then(
                                           function(res){
                                               console.log("replicate: LP_DOCUMENT_UPLOAD success !!"+JSON.stringify(param));
                                               dfd.resolve(true);
                                           }
                                       );
                        }
                        else
                        {
                            navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                            dfd.resolve(false);
                        }
                    }
                );
    return dfd.promise;
}

this.insertAppMainData = function(COMBO_ID, AGENT_CD, OPP_ID, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, REF_SIS_ID){

    var dfd = $q.defer();
    console.log("inside insertAppMainData"+AGENT_CD+" : "+ REF_SIS_ID);

            SisFormService.loadSavedSISData(AGENT_CD, REF_SIS_ID).then(function(sisData){
                    console.log("sisData : "+JSON.stringify(sisData));
               com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APPLICATION_MAIN', null, false).then(
                               function(mainData){
                                   if(!!mainData)
                                       com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APPLICATION_MAIN').then(function(){
                                            com.replaceWithTermSIS(REF_SIS_ID, sisData, mainData).then(function(mainData){
                                               console.log("here :: mainData ::"+JSON.stringify(mainData[0]));
                                               CommonService.insertOrReplaceRecord(db, 'LP_APPLICATION_MAIN', mainData[0], true).then(
                                                                           function(res){
                                                                               console.log("replicate: LP_APPLICATION_MAIN success !!"+JSON.stringify(res));
                                                                               dfd.resolve(true);
                                                                           });
                                                });
                                       });
                                   else
                                   {
                                       navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                                       dfd.resolve(false);
                                   }
                               });
            });
       // });
    return dfd.promise;
}

this.deleteTableData = function(APP_ID, AGENT_CD, CUST_TYPE, tableName){
var dfd = $q.defer();
console.log("Inside deleteTableData");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = APP_ID;
    whereClauseObj.AGENT_CD = AGENT_CD;
    if(!!CUST_TYPE)
        whereClauseObj.CUST_TYPE = CUST_TYPE;
    CommonService.deleteRecords(db,tableName,whereClauseObj).then(
        function(res){
            console.log(tableName+" Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;
}

this.replaceWithTermSIS = function(REF_SIS_ID, sisData, mainData){

 var dfd = $q.defer();
    console.log("Before: replaceWithTermSIS: mainData"+JSON.stringify(mainData[0]));
    mainData[0].SIS_ID = REF_SIS_ID;
    mainData[0].PLAN_CODE = sisData.sisFormBData.PLAN_CODE;
    mainData[0].PLAN_NAME = sisData.sisFormBData.PLAN_NAME;
    mainData[0].POLICY_TERM = sisData.sisFormBData.POLICY_TERM;
    mainData[0].PREMIUM_PAY_TERM = sisData.sisFormBData.PREMIUM_PAY_TERM;
    mainData[0].PREMIUM_PAY_MODE = sisData.sisFormBData.PREMIUM_PAY_MODE;
    mainData[0].PREMIUM_PAY_MODE_DESC = sisData.sisFormBData.PREMIUM_PAY_MODE_DESC;
    mainData[0].ANNUAL_PREMIUM_AMOUNT = sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
    mainData[0].MODEL_PREMIUM_AMOUNT = sisData.sisMainData.MODAL_PREMIUM;
    mainData[0].SERVICE_TAX = sisData.sisMainData.SERVICE_TAX;
    mainData[0].SUM_ASSURED = sisData.sisFormBData.SUM_ASSURED;
    console.log("replaceWithTermSIS: mainData"+JSON.stringify(mainData[0]));
    console.log("sisData")
    dfd.resolve(mainData);
 return dfd.promise;

}

this.insertContactScrnAData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

    var dfd = $q.defer();
    console.log("inside insertContactScrnAData"+REF_APP_ID);
    com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_CONTACT_SCRN_A', 'C01', true).then(
				function(data){
				    if(!!data)
				    com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_CONTACT_SCRN_A').then(function(){
                        CommonService.insertOrReplaceRecord(db, 'LP_APP_CONTACT_SCRN_A', data[0], true).then(
                            function(res){
                                console.log("replicate: LP_APP_CONTACT_SCRN_A success !!"+JSON.stringify(res));
                                //Check for proposer
                               console.log("Here 1");
                               com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_CONTACT_SCRN_A', 'C02', true).then(
                                            function(data){
                                             console.log("Here 2");
                                                if(!!data)
                                                   CommonService.insertOrReplaceRecord(db, 'LP_APP_CONTACT_SCRN_A', data[0], true).then(
                                                               function(res){
                                                                   console.log("replicate: LP_APP_CONTACT_SCRN_A success !!"+JSON.stringify(res));
                                                                   dfd.resolve(true);
                                                               }
                                                           );
                                                else
                                                 {
                                                     dfd.resolve(true);
                                                 }
                                            });
                            });
                    });
				}
    );
   return dfd.promise;
}

this.insertLSData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertLSData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_LS_ANS_SCRN_B', /*'C01'*/null, /*true*/false).then(
    				function(data){
    				console.log("insertLSData length: "+data.length);
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_LS_ANS_SCRN_B').then(function(){
                                 for(var i = 0; i<data.length; i++)
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_LS_ANS_SCRN_B', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_LS_ANS_SCRN_B success C01!!"+JSON.stringify(res));
                                                    dfd.resolve(true);
                                                }
                                    );

                            });
                        }
                        else
                        {
                            navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                            dfd.resolve(false);
                        }
    				}
    			);
       return dfd.promise;

}


this.insertHDData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertHDData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_HD_ANS_SCRN_C', /*'C01'*/null, /*true*/false).then(
    				function(data){
    				console.log("length: "+data.length);
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_HD_ANS_SCRN_C').then(function(){
                                for(var i = 0; i<data.length; i++)
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_HD_ANS_SCRN_C', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_HD_ANS_SCRN_C success C01!!"+JSON.stringify(res));
                                                     dfd.resolve(true);
                                                }
                                            );
                            });
                        }
                        else
                        {
                            navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                            dfd.resolve(false);
                        }
    				}
    			);
       return dfd.promise;

}


this.insertHDSubData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertHDSubData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_HD_ANS_SCRN_C13', /*'C01'*/null, /*true*/false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
                        com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_HD_ANS_SCRN_C13').then(function(){
                            if(!!data)
                            {

                                    for(var i = 0; i<data.length; i++)
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_HD_ANS_SCRN_C13', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_HD_ANS_SCRN_C13 success C01!!"+JSON.stringify(res));
                                                     dfd.resolve(true);
                                                }
                                            );
                            }
                            else
                            {
                                 dfd.resolve(true);
                            }
                        });
    				}
    			);
       return dfd.promise;

}

this.insertExtInsData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertExtInsData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_EXIST_INS_SCRN_D', /*'C01'*/null, /*true*/false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_EXIST_INS_SCRN_D').then(function(){
                                for(var i = 0; i<data.length; i++)
                                CommonService.insertOrReplaceRecord(db, 'LP_APP_EXIST_INS_SCRN_D', data[i], true).then(
                                            function(res){
                                                console.log("replicate: LP_APP_EXIST_INS_SCRN_D success C01!!"+JSON.stringify(res));
                                                 dfd.resolve(true);
                                            }
                                        );
                            });
                        }
                        else
                         {
                             navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                             dfd.resolve(false);
                         }
    				}
    			);
       return dfd.promise;

} 


this.insertFamilyData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertFamilyData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_FH_SCRN_E', null, false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_FH_SCRN_E').then(function(){
                                for(var i = 0; i<data.length; i++)
                                CommonService.insertOrReplaceRecord(db, 'LP_APP_FH_SCRN_E', data[i], true).then(
                                            function(res){
                                                console.log("replicate: LP_APP_FH_SCRN_E success C01!!"+JSON.stringify(res));
                                                 dfd.resolve(true);
                                            }
                                        );
                            });
                        }
                        else
                            dfd.resolve(true);
    				}
    			);
       return dfd.promise;
}


this.insertNomineeData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertNomineeData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_COMBO_NOM_CONTACT_SCRN_F', null, false,false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_NOM_CONTACT_SCRN_F').then(function(){
                                for(var i = 0; i<data.length; i++)
                                {
                                    delete data[i].COMBO_ID;
                                    data[i].APPLICATION_ID = REF_APP_ID;
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_NOM_CONTACT_SCRN_F', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_NOM_CONTACT_SCRN_F success C01!!"+JSON.stringify(res));
                                                     dfd.resolve(true);
                                                }
                                            );
                                }
                            });
                        }
                        else
                         {
                             navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                             dfd.resolve(false);
                         }
    				}
    			);
       return dfd.promise;
}


this.insertNEFTData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
	 var EMPFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
        console.log("inside insertNEFTData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_APP_PAY_NEFT_SCRN_H', null, false).then(
    				function(data){
    				    console.log("data: "+JSON.stringify(data));
                        com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_PAY_NEFT_SCRN_H').then(function(){
                            if(!!data)
                            {
                                for(var i = 0; i<data.length; i++)
                                CommonService.insertOrReplaceRecord(db, 'LP_APP_PAY_NEFT_SCRN_H', data[i], true).then(
                                            function(res){
                                                console.log("replicate: LP_APP_PAY_NEFT_SCRN_H success C01!!"+JSON.stringify(res));
                                                 dfd.resolve(true);
                                            }
                                        );

                            }
                            else
                            {
								if(EMPFlag == 'E')
									dfd.resolve(true);
								else
	                            {
                                	navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                                	dfd.resolve(false);
								}
                            }
                        });
    				}
    			);
       return dfd.promise;
}


this.insertPaymentData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
	 var EMPFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
        console.log("inside insertPaymentData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_COMBO_PAY_DTLS_SCRN_G', null, false, false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
    				    if(!!data)
    				    {
                            com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_PAY_DTLS_SCRN_G').then(function(){
                                for(var i = 0; i<data.length; i++)
                                {
                                 delete data[i].COMBO_ID;
                                 data[i].APPLICATION_ID = REF_APP_ID;
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_PAY_DTLS_SCRN_G', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_PAY_DTLS_SCRN_G success C01!!"+JSON.stringify(res));
                                                     dfd.resolve(true);
                                                }
                                            );
                                }
                            });
                        }
                        else
                        {
							if(EMPFlag == 'E')
								dfd.resolve(true);
							else
                            {
								navigator.notification.alert("Invalid data",function(){CommonService.hideLoading();} ,"Application","OK");
                            	dfd.resolve(false);
							}
                        }
    				}
    			);
       return dfd.promise;
}


this.insertRenewalPayData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO){

     var dfd = $q.defer();
        console.log("inside insertRenewalPayData"+REF_APP_ID);
        com.getBasePlanData(COMBO_ID,AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, 'LP_COMBO_PAY_NEFT_SCRN_I', null, false, false).then(
    				function(data){
    				console.log("data: "+JSON.stringify(data));
                        com.deleteTableData(REF_APP_ID, AGENT_CD, null, 'LP_APP_PAY_NEFT_SCRN_I').then(function(){
                            if(!!data)
                            {
                                for(var i = 0; i<data.length; i++)
                                {
                                 delete data[i].COMBO_ID;
                                 data[i].APPLICATION_ID = REF_APP_ID;
                                    CommonService.insertOrReplaceRecord(db, 'LP_APP_PAY_NEFT_SCRN_I', data[i], true).then(
                                                function(res){
                                                    console.log("replicate: LP_APP_PAY_NEFT_SCRN_I success C01!!"+JSON.stringify(res));
                                                     dfd.resolve(true);
                                                }
                                            );
                                }
                            }
                            else
                                dfd.resolve(true);
                        });
    				}
    			);
       return dfd.promise;
}

this.getBasePlanData = function(COMBO_ID, AGENT_CD, BASE_APP_ID, BASE_POLNO, REF_APP_ID, REF_POLNO, TABLE_NAME, CUST_TYPE, flag, appIDFlag, doc_cat){
console.log("inside getBasePlanData");
    var dfd = $q.defer();
    var DataBean = {}
    var whereClauseObj = {};
    if(appIDFlag == false)
        whereClauseObj.COMBO_ID = COMBO_ID;
    else if(appIDFlag == true)
    {
        //Do nothing
    }
    else
        whereClauseObj.APPLICATION_ID = BASE_APP_ID;

    whereClauseObj.AGENT_CD = AGENT_CD;
    if(flag)
        whereClauseObj.CUST_TYPE = CUST_TYPE;
    if(!!doc_cat)
        {
            whereClauseObj.DOC_CAT = doc_cat;
            whereClauseObj.DOC_CAT_ID = BASE_APP_ID;
        }

    console.log(JSON.stringify(whereClauseObj));
    /*var param = "*";
    if(!!columnName)
        param = columnName;*/

    CommonService.selectRecords(db,TABLE_NAME,false,"*",whereClauseObj, "").then(
        function(res){

            if(!!res && res.rows.length>0){
                if(TABLE_NAME == 'LP_APP_EXIST_INS_SCRN_D')
                    DataBean = CommonService.termResultSetToObject(res, REF_APP_ID, REF_POLNO, false);//res.rows.item(0);
                else
                    DataBean = CommonService.termResultSetToObject(res, REF_APP_ID, REF_POLNO);//res.rows.item(0);
                if(!!DataBean && !(DataBean instanceof Array))
                   DataBean = [DataBean];
                /*if(!!_temp.APPLICATION_ID)
                    _temp.APPLICATION_ID = REF_APP_ID;
                if(!!_temp.POLICY_NO)
                    _temp.POLICY_NO = REF_POLNO;*/
                //DataBean = _temp;
                dfd.resolve(DataBean);
            }
            else
                dfd.resolve(null);
        }
    );

    return dfd.promise;
};

}]);
