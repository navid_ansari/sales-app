questModule.controller('qLifeStyleInfoCtrl',['qLifeStyleInfoService','QuestionnaireService',function(qLifeStyleInfoService,QuestionnaireService){
    debug("question side controller loaded");

    this.lifeStyleSideTabArr = [
        {'type':'lifeStyleIns','name':'Insured'},
        {'type':'lifeStylePro','name':'Proposer'},
    ];

    this.activeTab = qLifeStyleInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        QuestionnaireService.gotoLSPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.lifeStyleSideTabArr));
}]);

questModule.service('qLifeStyleInfoService',[function(){
    debug("questin lifestyle side service loaded");

    this.activeTab = "lifeStyleIns";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

questModule.directive('qlsTimeline',[function() {
        "use strict";
        debug('in questTimeline');
        return {
            restrict: 'E',
            controller: "qLifeStyleInfoCtrl",
            controllerAs: "qlstc",
            bindToController: true,
            templateUrl: "applicationForm/questionnaire/qLifeStyleTimeline.html"
        };
}]);