questModule.controller('qHealthInfoCtrl',['qHealthInfoService','QuestionnaireService',function(qHealthInfoService,QuestionnaireService){
    debug("question side controller loaded");

    this.healthSideTabArr = [
        {'type':'healthIns','name':'Insured'},
        {'type':'healthPro','name':'Proposer'},
    ]

    this.activeTab = qHealthInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        QuestionnaireService.gotoHealthPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.healthSideTabArr));
}]);

questModule.service('qHealthInfoService',[function(){
    debug("questin lifestyle side service loaded");

    this.activeTab = "healthIns";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

questModule.directive('qhTimeline',[function() {
        "use strict";
        debug('in questTimeline');
        return {
            restrict: 'E',
            controller: "qHealthInfoCtrl",
            controllerAs: "qhtc",
            bindToController: true,
            templateUrl: "applicationForm/questionnaire/qHealthTimeline.html"
        };
}]);