lifeStyleModule.controller('LifeStyleCtrl',['$q', '$state','$filter', 'CommonService','QuestionnaireService','ApplicationFormDataService','ExistingAppMainData', 'ExistingAppData','SubOccDocList','DefenceDocList','OccupDocList','TravelDocList', 'DrinksTypeList','SmokeTypeList','FrequencyList','AppTimeService','qLifeStyleInfoService', function($q, $state, $filter, CommonService,QuestionnaireService, ApplicationFormDataService, ExistingAppMainData, ExistingAppData,SubOccDocList, DefenceDocList, OccupDocList , TravelDocList, DrinksTypeList, SmokeTypeList, FrequencyList,AppTimeService,qLifeStyleInfoService){

var life = this;

qLifeStyleInfoService.setActiveTab("lifeStyleIns");

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
life.click = false;
this.lifeStyleBean = {
Quest1:{"ID":"1","SEQ":"1"},
Quest2:{"ID":"2","SEQ":"2",SUB1:{"ID":"3","SEQ":"9"},SUB2:{"ID":"4","SEQ":"10"}},
Quest3:{"ID":"5","SEQ":"3",SUB1:{"ID":"6","SEQ":"11"}},
Quest4:{"ID":"7","SEQ":"4",SUB1:{"ID":"8","SEQ":"12"}},
Quest5:{"ID":"9","SEQ":"5",SUB1:{"ID":"10","SEQ":"13"}},
Quest6:{"ID":"11","SEQ":"6",SUB1:{"ID":"12","SEQ":"14"}},
Quest7:{"ID":"13","SEQ":"7",SUB1:{"ID":"14","SEQ":"15"}},
Quest8:{"ID":"15","SEQ":"8",SUB1:{"ID":"16","SEQ":"16"}}
};

this.numRegex = NUM_REG;
this.freeTxtRegex = FREE_TEXT_REGEX;

this.Quest2List1 = [
{"DISPLAY":"Select ", "VALUE":null},
{"DISPLAY":"Hobbies", "VALUE":"Hobbies"},
{"DISPLAY":"Occupation", "VALUE":"Occupation"}
];
console.log("DOCS_LIST ::"+JSON.stringify(JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST))+"\n SubOccDocList ::"+JSON.stringify(SubOccDocList)+"\n Existing Data :LifeStyle :"+JSON.stringify(ExistingAppData));
this.Quest2List2 = [
{"DISPLAY":"Select ", "VALUE":null, "HOBBY_CD":null},
{"DISPLAY":"Climbing and mountaineering", "VALUE":"Climbing and mountaineering", "HOBBY_CD":"CM"},
{"DISPLAY":"Diving", "VALUE":"Diving", "HOBBY_CD":"DV"},
{"DISPLAY":"Gliding", "VALUE":"Gliding", "HOBBY_CD":"GD"},
{"DISPLAY":"Hand-Gliding", "VALUE":"Hand-Gliding", "HOBBY_CD":"HG"},
{"DISPLAY":"Hazardous Sports", "VALUE":"Hazardous Sports", "HOBBY_CD":"HS"},
{"DISPLAY":"Microlighting", "VALUE":"Microlighting", "HOBBY_CD":"ML"},
{"DISPLAY":"Parachuting", "VALUE":"Parachuting", "HOBBY_CD":"PC"},
{"DISPLAY":"Scuba Diving", "VALUE":"Scuba Diving", "HOBBY_CD":"SD"},
{"DISPLAY":"Yachting", "VALUE":"Yachting", "HOBBY_CD":"YC"}
];


this.drinksTypeList = DrinksTypeList;
this.smokeTypeList = SmokeTypeList;
this.frequencyList = FrequencyList;

this.lifeStyleBean.Quest2.SUB1.ANS = this.Quest2List1[0];
this.lifeStyleBean.Quest2.SUB2.ANS = this.Quest2List2[0];
this.lifeStyleBean.Quest5.SUB1.ANS1 = this.drinksTypeList[0];
this.lifeStyleBean.Quest5.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest6.SUB1.ANS1 = this.smokeTypeList[0];
this.lifeStyleBean.Quest6.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest7.SUB1.ANS1 = this.smokeTypeList[0];
this.lifeStyleBean.Quest7.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest8.SUB1.ANS3 = this.frequencyList[0];


this.setExistingLifeData = function(){
var dfd = $q.defer();
var existing = ExistingAppData.Insured;
ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
if(ExistingAppData!=undefined && ExistingAppData.Insured!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.Insured).length!=0)
{
	life.lifeStyleBean.Quest1.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"1","QUESTION_SEQUENCE": "1"}, true)[0].ANSWAR_FLAG;
	life.lifeStyleBean.Quest2.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"2","QUESTION_SEQUENCE": "2"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest3.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"5","QUESTION_SEQUENCE": "3"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest4.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"7","QUESTION_SEQUENCE": "4"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest5.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"9","QUESTION_SEQUENCE": "5"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest6.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"11","QUESTION_SEQUENCE": "6"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest7.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"13","QUESTION_SEQUENCE": "7"}, true)[0].ANSWAR_FLAG
	life.lifeStyleBean.Quest8.ANSFLAG = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID":"15","QUESTION_SEQUENCE": "8"}, true)[0].ANSWAR_FLAG

}
dfd.resolve(life.lifeStyleBean);

return dfd.promise;
}
this.setExistingLifeData().then(function(){
console.log("done setExistingLifeData");
if(life.lifeStyleBean.Quest2.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "3"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest2 :"+_temp.ANSWAR_DTLS1);
	life.lifeStyleBean.Quest2.SUB1.ANS = $filter('filter')(life.Quest2List1, {"VALUE": _temp.ANSWAR_DTLS1}, true)[0];
	if(_temp.ANSWAR_DTLS1 == "Hobbies")
	{	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "4"}, true)[0];
		life.lifeStyleBean.Quest2.SUB2.ANS = $filter('filter')(life.Quest2List2, {"VALUE": _temp.ANSWAR_DTLS1}, true)[0];
	}
}

if(life.lifeStyleBean.Quest3.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "6"}, true)[0];
	life.lifeStyleBean.Quest3.SUB1.ANS = _temp.ANSWAR_DTLS1;
}

if(life.lifeStyleBean.Quest4.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "8"}, true)[0];
	life.lifeStyleBean.Quest4.SUB1.ANS = _temp.ANSWAR_DTLS1;
}

if(life.lifeStyleBean.Quest5.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "10"}, true)[0];
	life.lifeStyleBean.Quest5.SUB1.ANS1 = $filter('filter')(life.drinksTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		life.lifeStyleBean.Quest5.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	life.lifeStyleBean.Quest5.SUB1.ANS3 = $filter('filter')(life.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}


if(life.lifeStyleBean.Quest6.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "12"}, true)[0];
	life.lifeStyleBean.Quest6.SUB1.ANS1 = $filter('filter')(life.smokeTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		life.lifeStyleBean.Quest6.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	life.lifeStyleBean.Quest6.SUB1.ANS3 = $filter('filter')(life.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}


if(life.lifeStyleBean.Quest7.ANSFLAG =='Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "14"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest7 :"+_temp.ANSWAR_DTLS1);
	life.lifeStyleBean.Quest7.SUB1.ANS1 = $filter('filter')(life.smokeTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		life.lifeStyleBean.Quest7.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	life.lifeStyleBean.Quest7.SUB1.ANS3 = $filter('filter')(life.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}

if(life.lifeStyleBean.Quest8.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Insured, {"QUESTION_ID": "16"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest8 :"+_temp.ANSWAR_DTLS1);
	life.lifeStyleBean.Quest8.SUB1.ANS1 = _temp.ANSWAR_DTLS1;
	if(!!_temp.ANSWAR_DTLS2)
		life.lifeStyleBean.Quest8.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	life.lifeStyleBean.Quest8.SUB1.ANS3 = $filter('filter')(life.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}

});

this.onChangeQuest1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest1 = {};
if(life.lifeStyleBean.Quest1.ANSFLAG == 'Y')
{
	life.lifeStyleBean.Quest1.DOC_ID = DefenceDocList.Insured.defence;
	if(DefenceDocList.Insured.isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest1.DOC_ID = DefenceDocList.Insured.defence;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest1.DOC_ID = DefenceDocList.Insured.defence;
//	alert("Defence Questionnaire need to be filled");
navigator.notification.alert("Defence Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","Ok");
}
else
	{
		life.lifeStyleBean.Quest1.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest1 = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest1 = null;
	}
};

this.onChangeQuest2 = function(){
if(life.lifeStyleBean.Quest2.ANSFLAG == 'N')
	{
		console.log("Changing Value")
		life.lifeStyleBean.Quest2.SUB1.ANS = life.Quest2List1[0];
		life.lifeStyleBean.Quest2.SUB2.ANS = life.Quest2List2[0];
		life.lifeStyleBean.Quest2.SUB1.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = null;
		if(!!ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest)
			{/* if occuQuest is not null */}
		else
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;
		life.lifeStyleBean.Quest2.SUB2.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest2Sub2 = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest2Sub2 = null;
	}

}
this.onChangeOccup = function(){

	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = {};
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = {};

	console.log("Doc_List inside lifeStyle::"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
		if(life.lifeStyleBean.Quest2.ANSFLAG == 'Y' && life.lifeStyleBean.Quest2.SUB1.ANS.VALUE == "Occupation")
		{
			life.lifeStyleBean.Quest2.SUB1.DOC_ID = OccupDocList.Insured.occup;
			if(OccupDocList.Insured.isDigital == "Y")
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest.DOC_ID = OccupDocList.Insured.occup;
			else
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest.DOC_ID = OccupDocList.Insured.occup;
	//		alert("Occupation Questionnaire need to be filled");
	navigator.notification.alert("Occupation Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","Ok");
			life.lifeStyleBean.Quest2.SUB2.ANS = life.Quest2List2[0];
		}
		else
		{
			life.lifeStyleBean.Quest2.SUB1.DOC_ID = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.occuQuest = null;
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.occuQuest = null;

		}
	//}

}

this.onChangeHobbies = function(){
//console.log("VALUE :"+life.lifeStyleBean.Quest2.SUB2.ANS.VALUE+"\n HOBBY_CD :"+life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD);
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest2Sub2 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest2Sub2 = {};

if(life.lifeStyleBean.Quest2.SUB2.ANS!=undefined && life.lifeStyleBean.Quest2.SUB2.ANS.VALUE!=undefined && life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD!=undefined)
{
	life.lifeStyleBean.Quest2.SUB2.DOC_ID = SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
	if(SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest2Sub2.DOC_ID = SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest2Sub2.DOC_ID = SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
//	alert(""+SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].MSG+" Questionnaire need to be filled")
navigator.notification.alert(""+SubOccDocList.Insured.subOcc[life.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].MSG+" Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","Ok");
}
else
{
	life.lifeStyleBean.Quest2.SUB2.DOC_ID = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest2Sub2 = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest2Sub2 = null;
}

}

this.onChangeTravel = function(){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest4 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest4 = {};

if(life.lifeStyleBean.Quest4.ANSFLAG == 'Y')
{
	life.lifeStyleBean.Quest4.SUB1.DOC_ID = TravelDocList.Insured.DOC_ID;
	if(TravelDocList.Insured.isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest4.DOC_ID = TravelDocList.Insured.DOC_ID;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest4.DOC_ID = TravelDocList.Insured.DOC_ID;
		navigator.notification.alert('Travel Questionnaire need to be filled',
                                        function(){CommonService.hideLoading();},
                                        'Message',
                                        'Ok'
                                        );

}
else
{
	life.lifeStyleBean.Quest4.SUB1.DOC_ID = null;
	life.lifeStyleBean.Quest4.SUB1.ANS = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.LSQuest4 = null;
  ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.LSQuest4 = null;
}

}


this.onChangeQuest5 = function(){
	if(life.lifeStyleBean.Quest5.ANSFLAG == 'Y')
	{}
	else
	{
		life.lifeStyleBean.Quest5.SUB1.ANS1 = life.drinksTypeList[0];
		life.lifeStyleBean.Quest5.SUB1.ANS3 = life.frequencyList[0];
		life.lifeStyleBean.Quest5.SUB1.ANS2 = null;
	}

}


this.onChangeQuest6 = function(){
	if(life.lifeStyleBean.Quest6.ANSFLAG == 'Y')
	{}
	else
	{
		life.lifeStyleBean.Quest6.SUB1.ANS1 = life.smokeTypeList[0];
		life.lifeStyleBean.Quest6.SUB1.ANS3 = life.frequencyList[0];
		life.lifeStyleBean.Quest6.SUB1.ANS2 = null;
	}

}

this.onChangeQuest7 = function(){
	if(life.lifeStyleBean.Quest7.ANSFLAG == 'Y')
	{}
	else
	{
		life.lifeStyleBean.Quest7.SUB1.ANS1 = life.smokeTypeList[0];
		life.lifeStyleBean.Quest7.SUB1.ANS3 = life.frequencyList[0];
		life.lifeStyleBean.Quest7.SUB1.ANS2 = null;
	}

}


this.onChangeQuest8 = function(){
	if(life.lifeStyleBean.Quest8.ANSFLAG == 'Y')
	{}
	else
	{
		life.lifeStyleBean.Quest8.SUB1.ANS1 = null;
		life.lifeStyleBean.Quest8.SUB1.ANS3 = life.frequencyList[0];
		life.lifeStyleBean.Quest8.SUB1.ANS2 = null;
	}

}

this.onNext = function(lifestyleForm){
    CommonService.showLoading();
life.click = true;
/**/
if(lifestyleForm.$invalid !=true){
QuestionnaireService.getLifeStyleQuest(life.lifeStyleBean,'C01').then(function(Data){
console.log("Record length:"+Data.length+"Details :Data: "+JSON.stringify(Data[0]));
	life.deleteLifeQuest('LP_APP_LS_ANS_SCRN_B').then(function(){
		for(var i=0;i<Data.length;i++)
        	(function(i){
        	CommonService.insertOrReplaceRecord(db, 'LP_APP_LS_ANS_SCRN_B', Data[i], true).then(
        					function(res){
        						console.log("LP_APP_LS_ANS_SCRN_B success Insured: "+"i:"+i+"!! : : "+JSON.stringify(res));
        						if(i == (Data.length-1)){
        						life.updateAppMainTables();
        						if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y'){
        						    qLifeStyleInfoService.setActiveTab("lifeStylePro");
        							$state.go("applicationForm.questionnairels.lifeStylePro");
        						}else{
        						    //timeline changes..
        						    CommonService.showLoading();
        						    AppTimeService.setActiveTab("health");
        						    AppTimeService.isLifeStyleCompleted = true;
        						    //$rootScope.val = 20;
        						    AppTimeService.setProgressValue(20);
        							$state.go("applicationForm.questionnairehl.healthIns");
        					    }
        						}
        					}
        				);
        	})(i);
	});
});
}
else
    navigator.notification.alert("Please complete your form",function(){CommonService.hideLoading();} ,"Application","OK");
}

this.deleteLifeQuest = function(tableName){
var dfd = $q.defer();
console.log("Inside deleteLifeQuest");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
    whereClauseObj.CUST_TYPE = 'C01';
    CommonService.deleteRecords(db,tableName,whereClauseObj).then(
        function(res){
            console.log(tableName+" Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;


}
this.updateAppMainTables = function(){
console.log("inside updateAppMainTables")
var whereClauseObj = {};
whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

var whereClauseObj1 = {};
        	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj1.CUST_TYPE = 'C01';
var whereClauseObj2 = {};
        	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj2.CUST_TYPE = 'C02';

CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj1).then(
				                function(res){
				                console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST update success !!"+JSON.stringify(whereClauseObj1));
				                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y')
				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN2_COMPLETED" :"Y"}, whereClauseObj).then(
													function(res){
														console.log("IS_SCREEN2_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
													});
				                if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
				                CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj2).then(
                                				                function(res){
                                									console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST proposer update success !!"+JSON.stringify(whereClauseObj2));
                                								});

								});

}

CommonService.hideLoading();
}]);
