lifeStyleModule.controller('LifeStyleProCtrl',['$q', '$state','$filter', 'CommonService','ApplicationFormDataService','QuestionnaireService', 'ExistingAppMainData', 'ExistingAppData','SubOccDocList','DefenceDocList','OccupDocList','TravelDocList', 'DrinksTypeList','SmokeTypeList','FrequencyList','AppTimeService','qHealthInfoService', function($q, $state, $filter, CommonService, ApplicationFormDataService, QuestionnaireService, ExistingAppMainData, ExistingAppData, SubOccDocList, DefenceDocList, OccupDocList , TravelDocList, DrinksTypeList, SmokeTypeList, FrequencyList,AppTimeService,qHealthInfoService){

var plife = this;

    /* Header Hight Calculator */
    setTimeout(function(){
        var appOutputGetHeight = $(".customer-details .fixed-bar").height();
        $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    });
    /* End Header Hight Calculator */



ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
plife.click = false;
this.lifeStyleBean = {
Quest1:{"ID":"1","SEQ":"1"},
Quest2:{"ID":"2","SEQ":"2",SUB1:{"ID":"3","SEQ":"9"},SUB2:{"ID":"4","SEQ":"10"}},
Quest3:{"ID":"5","SEQ":"3",SUB1:{"ID":"6","SEQ":"11"}},
Quest4:{"ID":"7","SEQ":"4",SUB1:{"ID":"8","SEQ":"12"}},
Quest5:{"ID":"9","SEQ":"5",SUB1:{"ID":"10","SEQ":"13"}},
Quest6:{"ID":"11","SEQ":"6",SUB1:{"ID":"12","SEQ":"14"}},
Quest7:{"ID":"13","SEQ":"7",SUB1:{"ID":"14","SEQ":"15"}},
Quest8:{"ID":"15","SEQ":"8",SUB1:{"ID":"16","SEQ":"16"}}
};

this.numRegex = NUM_REG;
this.freeTxtRegex = FREE_TEXT_REGEX;

this.Quest2List1 = [
{"DISPLAY":"Select ", "VALUE":null},
{"DISPLAY":"Hobbies", "VALUE":"Hobbies"},
{"DISPLAY":"Occupation", "VALUE":"Occupation"}
];
console.log("Existing Data :LifeStyle :"+JSON.stringify(ExistingAppData));
this.Quest2List2 = [
{"DISPLAY":"Select ", "VALUE":null, "HOBBY_CD":null},
{"DISPLAY":"Climbing and mountaineering", "VALUE":"Climbing and mountaineering", "HOBBY_CD":"CM"},
{"DISPLAY":"Diving", "VALUE":"Diving", "HOBBY_CD":"DV"},
{"DISPLAY":"Gliding", "VALUE":"Gliding", "HOBBY_CD":"GD"},
{"DISPLAY":"Hand-Gliding", "VALUE":"Hand-Gliding", "HOBBY_CD":"HG"},
{"DISPLAY":"Hazardous Sports", "VALUE":"Hazardous Sports", "HOBBY_CD":"HS"},
{"DISPLAY":"Microlighting", "VALUE":"Microlighting", "HOBBY_CD":"ML"},
{"DISPLAY":"Parachuting", "VALUE":"Parachuting", "HOBBY_CD":"PC"},
{"DISPLAY":"Scuba Diving", "VALUE":"Scuba Diving", "HOBBY_CD":"SD"},
{"DISPLAY":"Yachting", "VALUE":"Yachting", "HOBBY_CD":"YC"}
];


this.drinksTypeList = DrinksTypeList;
this.smokeTypeList = SmokeTypeList;
this.frequencyList = FrequencyList;

this.lifeStyleBean.Quest2.SUB1.ANS = this.Quest2List1[0];
this.lifeStyleBean.Quest2.SUB2.ANS = this.Quest2List2[0];
this.lifeStyleBean.Quest5.SUB1.ANS1 = this.drinksTypeList[0];
this.lifeStyleBean.Quest5.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest6.SUB1.ANS1 = this.smokeTypeList[0];
this.lifeStyleBean.Quest6.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest7.SUB1.ANS1 = this.smokeTypeList[0];
this.lifeStyleBean.Quest7.SUB1.ANS3 = this.frequencyList[0];
this.lifeStyleBean.Quest8.SUB1.ANS3 = this.frequencyList[0];


this.setExistingLifeData = function(){
var dfd = $q.defer();
var existing = ExistingAppData.Proposer;
ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
if(ExistingAppData!=undefined && ExistingAppData.Proposer!=undefined && Object.keys(ExistingAppData).length !=0 && Object.keys(ExistingAppData.Proposer).length!=0)
{
	plife.lifeStyleBean.Quest1.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"1","QUESTION_SEQUENCE": "1"}, true)[0].ANSWAR_FLAG;
	plife.lifeStyleBean.Quest2.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"2","QUESTION_SEQUENCE": "2"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest3.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"5","QUESTION_SEQUENCE": "3"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest4.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"7","QUESTION_SEQUENCE": "4"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest5.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"9","QUESTION_SEQUENCE": "5"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest6.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"11","QUESTION_SEQUENCE": "6"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest7.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"13","QUESTION_SEQUENCE": "7"}, true)[0].ANSWAR_FLAG
	plife.lifeStyleBean.Quest8.ANSFLAG = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID":"15","QUESTION_SEQUENCE": "8"}, true)[0].ANSWAR_FLAG

}
dfd.resolve(plife.lifeStyleBean);

return dfd.promise;
}
this.setExistingLifeData().then(function(){
console.log("done setExistingLifeData");
if(plife.lifeStyleBean.Quest2.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "3"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest2 :"+_temp.ANSWAR_DTLS1);
	plife.lifeStyleBean.Quest2.SUB1.ANS = $filter('filter')(plife.Quest2List1, {"VALUE": _temp.ANSWAR_DTLS1}, true)[0];
	if(_temp.ANSWAR_DTLS1 == "Hobbies")
	{	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "4"}, true)[0];
		plife.lifeStyleBean.Quest2.SUB2.ANS = $filter('filter')(plife.Quest2List2, {"VALUE": _temp.ANSWAR_DTLS1}, true)[0];
	}
}

if(plife.lifeStyleBean.Quest3.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "6"}, true)[0];
	plife.lifeStyleBean.Quest3.SUB1.ANS = _temp.ANSWAR_DTLS1;
}

if(plife.lifeStyleBean.Quest4.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "8"}, true)[0];
	plife.lifeStyleBean.Quest4.SUB1.ANS = _temp.ANSWAR_DTLS1;
}

if(plife.lifeStyleBean.Quest5.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "10"}, true)[0];
	plife.lifeStyleBean.Quest5.SUB1.ANS1 = $filter('filter')(plife.drinksTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		plife.lifeStyleBean.Quest5.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	plife.lifeStyleBean.Quest5.SUB1.ANS3 = $filter('filter')(plife.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}


if(plife.lifeStyleBean.Quest6.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "12"}, true)[0];
	plife.lifeStyleBean.Quest6.SUB1.ANS1 = $filter('filter')(plife.smokeTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		plife.lifeStyleBean.Quest6.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	plife.lifeStyleBean.Quest6.SUB1.ANS3 = $filter('filter')(plife.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}


if(plife.lifeStyleBean.Quest7.ANSFLAG =='Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "14"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest7 :"+_temp.ANSWAR_DTLS1);
	plife.lifeStyleBean.Quest7.SUB1.ANS1 = $filter('filter')(plife.smokeTypeList, {"NBFE_CODE": _temp.ANSWAR_DTLS4}, true)[0];
	if(!!_temp.ANSWAR_DTLS2)
		plife.lifeStyleBean.Quest7.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	plife.lifeStyleBean.Quest7.SUB1.ANS3 = $filter('filter')(plife.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}

if(plife.lifeStyleBean.Quest8.ANSFLAG == 'Y')
{
	var _temp = $filter('filter')(ExistingAppData.Proposer, {"QUESTION_ID": "16"}, true)[0];
	console.log(JSON.stringify(_temp)+"Quest8 :"+_temp.ANSWAR_DTLS1);
	plife.lifeStyleBean.Quest8.SUB1.ANS1 = _temp.ANSWAR_DTLS1;
	if(!!_temp.ANSWAR_DTLS2)
		plife.lifeStyleBean.Quest8.SUB1.ANS2 = parseInt(_temp.ANSWAR_DTLS2);
	plife.lifeStyleBean.Quest8.SUB1.ANS3 = $filter('filter')(plife.frequencyList, {"NBFE_CODE": _temp.ANSWAR_DTLS6}, true)[0];
}

});


this.onChangeQuest1 = function(){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest1 = {};

if(plife.lifeStyleBean.Quest1.ANSFLAG == 'Y')
{
	plife.lifeStyleBean.Quest1.DOC_ID = DefenceDocList.Proposer.defence;
	if(DefenceDocList.Proposer.isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest1.DOC_ID = DefenceDocList.Proposer.defence;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest1.DOC_ID = DefenceDocList.Proposer.defence;
    navigator.notification.alert("Defence Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","OK");
}
else
	{
		plife.lifeStyleBean.Quest1.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest1 = null;
       	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest1 = null;
	}
};

this.onChangeQuest2 = function(){
if(plife.lifeStyleBean.Quest2.ANSFLAG == 'N')
	{
		console.log("Changing Value")
		plife.lifeStyleBean.Quest2.SUB1.ANS = plife.Quest2List1[0];
		plife.lifeStyleBean.Quest2.SUB2.ANS = plife.Quest2List2[0];
		plife.lifeStyleBean.Quest2.SUB1.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = null;
		if(!!ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest)
			{/* if occuQuest is not null */}
		else
			ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;
		plife.lifeStyleBean.Quest2.SUB2.DOC_ID = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest2Sub2 = null;
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest2Sub2 = null;
	}

}

this.onChangeOccup = function(){

ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = {};

if(plife.lifeStyleBean.Quest2.ANSFLAG == 'Y' && plife.lifeStyleBean.Quest2.SUB1.ANS.VALUE == "Occupation")
{
	plife.lifeStyleBean.Quest2.SUB1.DOC_ID = OccupDocList.Proposer.occup;
	if(OccupDocList.Proposer.isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest.DOC_ID = OccupDocList.Proposer.occup;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest.DOC_ID = OccupDocList.Proposer.occup;
    navigator.notification.alert("Occupation Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","OK");
	plife.lifeStyleBean.Quest2.SUB2.ANS = plife.Quest2List2[0];
}
else
{
	plife.lifeStyleBean.Quest2.SUB1.DOC_ID = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.occuQuest = null;
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.occuQuest = null;
}

}

this.onChangeHobbies = function(){
//console.log("VALUE :"+plife.lifeStyleBean.Quest2.SUB2.ANS.VALUE+"\n HOBBY_CD :"+plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD);
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest2Sub2 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest2Sub2 = {};

if(plife.lifeStyleBean.Quest2.SUB2.ANS!=undefined && plife.lifeStyleBean.Quest2.SUB2.ANS.VALUE!=undefined && plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD!=undefined)
{
	plife.lifeStyleBean.Quest2.SUB2.DOC_ID = SubOccDocList.Proposer.subOcc[plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
	if(SubOccDocList.Proposer.subOcc[plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest2Sub2.DOC_ID = SubOccDocList.Proposer.subOcc[plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
	else
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest2Sub2.DOC_ID = SubOccDocList.Proposer.subOcc[plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].DOC_ID;
    navigator.notification.alert(""+SubOccDocList.Proposer.subOcc[plife.lifeStyleBean.Quest2.SUB2.ANS.HOBBY_CD].MSG+" Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","OK");
}
else
{
	plife.lifeStyleBean.Quest2.SUB2.DOC_ID = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest2Sub2 = null;
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest2Sub2 = null;
}

}

this.onChangeTravel = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest4 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest4 = {};

if(plife.lifeStyleBean.Quest4.ANSFLAG == 'Y')
{
	plife.lifeStyleBean.Quest4.SUB1.DOC_ID = TravelDocList.Proposer.DOC_ID;
	if(TravelDocList.Proposer.isDigital == "Y")
		ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest4.DOC_ID = TravelDocList.Proposer.DOC_ID;
	else
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest4.DOC_ID = TravelDocList.Proposer.DOC_ID;
    navigator.notification.alert("Travel Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","OK");
}
else
{
	plife.lifeStyleBean.Quest4.SUB1.DOC_ID = null;
	plife.lifeStyleBean.Quest4.SUB1.ANS = null;
	ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.LSQuest4 = null;
    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.LSQuest4 = null;
}

}

this.onNext = function(lifestyleProForm){
    CommonService.showLoading();
plife.click = true;
/**/
if(lifestyleProForm.$invalid != true){
QuestionnaireService.getLifeStyleQuest(plife.lifeStyleBean,'C02').then(function(Data){
console.log("Record length:"+Data.length+"Details :Data: "+JSON.stringify(Data[0]));
	plife.deleteLifeQuest('LP_APP_LS_ANS_SCRN_B').then(function(){
		for(var i=0;i<Data.length;i++)
        	(function(i){
        	CommonService.insertOrReplaceRecord(db, 'LP_APP_LS_ANS_SCRN_B', Data[i], true).then(
        													function(res){
        														console.log("LP_APP_LS_ANS_SCRN_B success "+"i:"+i+"!! : : "+JSON.stringify(res));
        														if(i == (Data.length-1)){
                                                                    console.log("inside last loop :")
                                                                    plife.updateAppMainTables();
                                                                    //timeline changes..
                                                                    CommonService.showLoading();
                                                                    AppTimeService.isLifeStyleCompleted = true;
                                                                    AppTimeService.setActiveTab("health");
                                                                    //$rootScope.val = 20;
                                                                    AppTimeService.setProgressValue(20);
                                                                    qHealthInfoService.setActiveTab("healthIns");
                                                                    $state.go("applicationForm.questionnairehl.healthIns");
                                                                }

        													}
        												);
        	})(i);
	});
});
}
else
	navigator.notification.alert("Please complete your form",function(){CommonService.hideLoading();} ,"Application","OK");

}

this.deleteLifeQuest = function(tableName){
var dfd = $q.defer();
console.log("Inside deleteLifeQuest");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
    whereClauseObj.CUST_TYPE = 'C02';
    CommonService.deleteRecords(db,tableName,whereClauseObj).then(
        function(res){
            console.log(tableName+" Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;


}

this.updateAppMainTables = function(){
console.log("inside updateAppMainTables")
var whereClauseObj = {};
whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

var whereClauseObj1 = {};
        	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj1.CUST_TYPE = 'C01';
var whereClauseObj2 = {};
        	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj2.CUST_TYPE = 'C02';

CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj1).then(
				                function(res){
				                console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST update success !!"+JSON.stringify(whereClauseObj));
				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN2_COMPLETED" :"Y"}, whereClauseObj).then(
													function(res){
														console.log("IS_SCREEN2_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
													});
				                CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj2).then(
                                				                function(res){
                                									console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST proposer update success !!"+JSON.stringify(whereClauseObj2));
                                								});
								});




}
CommonService.hideLoading();
}]);
