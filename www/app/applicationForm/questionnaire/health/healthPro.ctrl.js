healthModule.controller('HealthProCtrl', ['$q','$state','$filter', 'CommonService', 'LoginService', 'ApplicationFormDataService', 'QuestionnaireService', 'ExistingAppData','ExistingHealthMainData','ExistingHealthSubData','HealthDocList','AppTimeService', function($q, $state, $filter, CommonService, LoginService,ApplicationFormDataService, QuestionnaireService, ExistingAppData, ExistingHealthMainData, ExistingHealthSubData, HealthDocList,AppTimeService){

var phealth = this;

    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
Reflex :{
	Insured:{},
	Proposer:{}
},
NonReflex :{
	Insured:{},
	Proposer:{}
}

};
if(ExistingAppData.applicationPersonalInfo.Proposer!=undefined && ExistingAppData.applicationPersonalInfo.Proposer.GENDER_CODE)
    phealth.Gender = ExistingAppData.applicationPersonalInfo.Proposer.GENDER_CODE;
else
    phealth.Gender = null;

this.numRegex = NUM_REG;
this.nameRegex = FULLNAME_REGEX_SP;
this.htWtRegex = AGENT_CD_REGEX;

phealth.click = false;
phealth.planCode = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
phealth.SA = ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED;
console.log("phealth.planCode::"+phealth.planCode+"phealth.SA::"+phealth.SA);

this.healthMainBean = {
    VcpQuest1:{"ID":"52"},
    VcpQuest2:{"ID":"53"},
    VcpQuest3:{"ID":"54"},
    VcpQuest4:{"ID":"55"},
    Quest1:{"ID":"17"},
    Quest2:{"ID":"18"},
    Quest3:{"ID":"19"},
    Quest4:{"ID":"20","CODE":"PM"},
    Quest5:{"ID":"21","CODE":"MC"},
    Quest6:{"ID":"22"},
    Quest7:{"ID":"23","CODE":"TG"},
    Quest8:{"ID":"24","CODE":"AI"},
    Quest9A:{"ID":"25",SUB1:{"ID":"26"}},
    Quest9B:{"ID":"27",SUB1:{"ID":"28"}},
    Quest9C:{"ID":"29","CODE":"CP"},
    Quest9D:{"ID":"30"},
    Quest9E:{"ID":"31",SUB1:{"ID":"32"}},
    Quest9F:{"ID":"33",SUB1:{"ID":"34"}},
    Quest9G:{"ID":"35"},
    Quest9H:{"ID":"36",SUB1:{"ID":"37"}},
    Quest9I:{"ID":"38",SUB1:{"ID":"39"}},
    Quest9J:{"ID":"40",SUB1:{"ID":"41"}},
    Quest9K:{"ID":"42","CODE":"DD"},
    Quest9L:{"ID":"43"},
    Quest9M:{"ID":"44"},
    Quest10:{"ID":"45"},
    Quest11:{"ID":"46"},
    Quest12A:{"ID":"47","CODE":"PG",SUB1:{"ID":"48"}},
    Quest12B:{"ID":"49"},
    Quest12C:{"ID":"50"},
    Quest12D:{"ID":"51"}
};

this.healthSubBean = {
    subQuest1:{"ID":"1","SEQ":"2. "},
    subQuest2:{"ID":"2","SEQ":"2. "},
    subQuest3:{"ID":"3","SEQ":"8b. "},
    subQuest4:{"ID":"4","SEQ":"8e. "},
    subQuest5:{"ID":"5","SEQ":"8e. "},
    subQuest6:{"ID":"6","SEQ":"8h. "},
    subQuest7:{"ID":"7","SEQ":"8i. "},
    subQuest8:{"ID":"8","SEQ":"8j. "},
    subQuest9:{"ID":"9","SEQ":"11.a. "},
    subQuest10:{"ID":"10","SEQ":"8a. "},
    subQuest11:{"ID":"11","SEQ":"8a. "},
    subQuest12:{"ID":"12","SEQ":"8b. "},
    subQuest13:{"ID":"13","SEQ":"8d. "},
    subQuest14:{"ID":"14","SEQ":"8e. "},
    subQuest15:{"ID":"15","SEQ":"8g. "},
    subQuest16:{"ID":"16","SEQ":"8h. "},
    subQuest17:{"ID":"17","SEQ":"8j. "},
    subQuest18:{"ID":"18","SEQ":"8l. "},
    subQuest19:{"ID":"19","SEQ":"8m. "},
    subQuest20:{"ID":"20","SEQ":"9. "},
    subQuest21:{"ID":"21","SEQ":"10. "},
    subQuest22:{"ID":"22","SEQ":"11.b. "},
    subQuest23:{"ID":"23","SEQ":"11.c. "},
    subQuest24:{"ID":"24","SEQ":"11.d. "},
    VcpsubQuest25:{"ID":"25","SEQ":"2. "},
    VcpsubQuest26:{"ID":"26","SEQ":"3. "},
    VcpsubQuest27:{"ID":"27","SEQ":"4. "},
    subQuest28:{"ID":"28","SEQ":"1. "},
    subQuest29:{"ID":"29","SEQ":"5. "}
};

this.Quest9AList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Diabetes","VALUE":"Diabetes","CODE":"DB"},
{"DISPLAY":"High Blood Sugar","VALUE":"High Blood Sugar"},
{"DISPLAY":"Sugar in Urine","VALUE":"Sugar in Urine"}
];

this.Quest9BList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"High BP","VALUE":"High BP","CODE":"HT"},
{"DISPLAY":"Low BP","VALUE":"Low BP"},
{"DISPLAY":"Raised Cholesterol","VALUE":"Raised Cholesterol"}
];

this.Quest9EList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Anxiety and Depression","VALUE":"Anxiety and Depression","CODE":"HT"},
{"DISPLAY":"Epilepsy","VALUE":"Epilepsy","CODE":"EP"},
{"DISPLAY":"Nervous disorder","VALUE":"Nervous disorder"},
{"DISPLAY":"Any Other disorder","VALUE":"Any Other disorder"},
{"DISPLAY":"None of these", "VALUE":"None"}

];

this.Quest9FList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Asthma","VALUE":"Asthma","CODE":"AS"},
{"DISPLAY":"Bronchitis","VALUE":"Bronchitis","CODE":"RD"},
{"DISPLAY":"Blood Spitting","VALUE":"Blood Spitting"},
{"DISPLAY":"Tuberculosis", "VALUE":"Tuberculosis"},
{"DISPLAY":"Other Respiratory disorders", "VALUE":"Other Respiratory disorders"}
];

this.Quest9HList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Arthritis","VALUE":"Asthma","CODE":"AR"},
{"DISPLAY":"Recurrent Back Pain","VALUE":"Recurrent Back Pain","CODE":"BN"},
{"DISPLAY":"Slipped Disc/Disorder of Spine","VALUE":"Slipped Disc/Disorder of Spine"},
{"DISPLAY":"Joints/Limbs", "VALUE":"Joints/Limbs"},
{"DISPLAY":"Leprosy", "VALUE":"Leprosy"},
{"DISPLAY":"None of these","VALUE":"None"}
];

this.Quest9IList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Hepatitis B","VALUE":"Hepatitis B","CODE":"HL"},
{"DISPLAY":"Hepatitis C","VALUE":"Hepatitis C","CODE":"HL"},
{"DISPLAY":"HIV/AIDS or other sexually transmitted disease","VALUE":"HIV/AIDS or other sexually transmitted disease"}
];

this.Quest9JList1 = [
{"DISPLAY":"Select ","VALUE":null},
{"DISPLAY":"Hydrocele/fistula/Piles","VALUE":"Hydrocele/fistula/Piles"},
{"DISPLAY":"Kidney","VALUE":"Kidney","CODE":"KD"},
{"DISPLAY":"Prosate, Urinary System or Reproductive System","VALUE":"Prosate, Urinary System or Reproductive System"}
];

phealth.healthMainBean.Quest9A.SUB1.ANS = this.Quest9AList1[0];
phealth.healthMainBean.Quest9B.SUB1.ANS = this.Quest9BList1[0];
phealth.healthMainBean.Quest9E.SUB1.ANS = this.Quest9EList1[0];
phealth.healthMainBean.Quest9F.SUB1.ANS = this.Quest9FList1[0];
phealth.healthMainBean.Quest9H.SUB1.ANS = this.Quest9HList1[0];
phealth.healthMainBean.Quest9I.SUB1.ANS = this.Quest9IList1[0];
phealth.healthMainBean.Quest9J.SUB1.ANS = this.Quest9JList1[0];


console.log("QUEST :BEFORE:"+JSON.stringify(JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST))+"Existing MAIN :"+JSON.stringify(ExistingHealthMainData.Proposer));
//console.log("sub Records :"+ExistingHealthSubData.Proposer.length+"\n Existing SUB ::"+JSON.stringify(ExistingHealthSubData.Proposer));

this.onChangeQuest1 = function(){

    if(phealth.healthMainBean.Quest1.ANSFLAG == 'N')
    {}
    else
        phealth.healthSubBean.subQuest28.ANS = null;

}

this.onChangeQuest6 = function(){

    if(phealth.healthMainBean.Quest6.ANSFLAG == 'Y')
    {}
    else
        phealth.healthSubBean.subQuest29.ANS = null;

}

this.onChangeVCP2 = function(){

    if(phealth.healthMainBean.VcpQuest2.ANSFLAG == 'Y')
    {}
    else
        phealth.healthSubBean.VcpsubQuest25.ANS = null;

}

this.onChangeVCP3 = function(){

    if(phealth.healthMainBean.VcpQuest3.ANSFLAG == 'Y')
    {}
    else
        phealth.healthSubBean.VcpsubQuest26.ANS = null;

}

this.onChangeVCP4 = function(){

    if(phealth.healthMainBean.VcpQuest4.ANSFLAG == 'Y')
    {}
    else
        phealth.healthSubBean.VcpsubQuest27.ANS = null;

}

this.onChange9A = function(healthProForm){

if(phealth.healthMainBean.Quest9A.ANSFLAG == 'Y')
    healthProForm.Quest9ASub1.$setValidity('selectRequired', false);
else
     {
		 	 healthProForm.Quest9ASub1.$setValidity('selectRequired', true);
		 	 ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ASub1 = null;
       ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ASub1 = null;
       phealth.healthMainBean.Quest9A.SUB1.ANS = this.Quest9AList1[0];
       phealth.healthSubBean.subQuest10.ANS = null;
       phealth.healthSubBean.subQuest11.ANS = null;
 	}

}

this.onChange9B = function(healthProForm){

if(phealth.healthMainBean.Quest9B.ANSFLAG == 'Y')
    healthProForm.Quest9BSub1.$setValidity('selectRequired', false);
else
     {
         healthProForm.Quest9BSub1.$setValidity('selectRequired', true);
				 ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9BSub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9BSub1 = null;
         phealth.healthMainBean.Quest9B.SUB1.ANS = this.Quest9BList1[0];
         phealth.healthSubBean.subQuest12.ANS = null;
         phealth.healthSubBean.subQuest3.ANS = null;
     }

}

this.onChange9D = function(healthProForm){

if(phealth.healthMainBean.Quest9D.ANSFLAG == 'Y')
    healthProForm.Quest9DSub1.$setValidity('required', false);
else
     {
        healthProForm.Quest9DSub1.$setValidity('required', true);
        phealth.healthSubBean.subQuest13.ANS = null;
     }
}

this.onChange9E = function(healthProForm){

if(phealth.healthMainBean.Quest9E.ANSFLAG == 'Y')
    healthProForm.Quest9ESub1.$setValidity('selectRequired', false);
else
     {
        healthProForm.Quest9ESub1.$setValidity('selectRequired', true);
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ESub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ESub1 = null;
        phealth.healthMainBean.Quest9E.SUB1.ANS = this.Quest9EList1[0];
        phealth.healthSubBean.subQuest4.ANS = null;
        phealth.healthSubBean.subQuest5.ANS = null;
     }

}

this.onChange9F = function(healthProForm){

if(phealth.healthMainBean.Quest9F.ANSFLAG == 'Y')
    healthProForm.Quest9FSub1.$setValidity('selectRequired', false);
else
     {
        healthProForm.Quest9FSub1.$setValidity('selectRequired', true);
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9FSub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9FSub1 = null;
        phealth.healthMainBean.Quest9F.SUB1.ANS = this.Quest9FList1[0];
     }
}

this.onChange9G = function(healthProForm){

if(phealth.healthMainBean.Quest9G.ANSFLAG == 'Y')
    healthProForm.Quest9GSub1.$setValidity('required', false);
else
     {
        healthProForm.Quest9GSub1.$setValidity('required', true);
        phealth.healthSubBean.subQuest15.ANS = null;
     }
}

this.onChange9H = function(healthProForm){

if(phealth.healthMainBean.Quest9H.ANSFLAG == 'Y')
    healthProForm.Quest9HSub1.$setValidity('selectRequired', false);
else
     {
        healthProForm.Quest9HSub1.$setValidity('selectRequired', true);
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9HSub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9HSub1 = null;
        phealth.healthMainBean.Quest9H.SUB1.ANS = this.Quest9HList1[0];
        phealth.healthSubBean.subQuest6.ANS = null;
        phealth.healthSubBean.subQuest16.ANS = null;
     }
}

this.onChange9I = function(healthProForm){

if(phealth.healthMainBean.Quest9I.ANSFLAG == 'Y')
    healthProForm.Quest9ISub1.$setValidity('selectRequired', false);
else
     {
        healthProForm.Quest9ISub1.$setValidity('selectRequired', true);
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ISub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ISub1 = null;
        phealth.healthMainBean.Quest9I.SUB1.ANS = this.Quest9IList1[0];
        phealth.healthSubBean.subQuest7.ANS = null;
     }
}

this.onChange9J = function(healthProForm){

if(phealth.healthMainBean.Quest9J.ANSFLAG == 'Y')
    healthProForm.Quest9JSub1.$setValidity('selectRequired', false);
else
     {
        healthProForm.Quest9JSub1.$setValidity('selectRequired', true);
				ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9JSub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9JSub1 = null;
        phealth.healthMainBean.Quest9J.SUB1.ANS = this.Quest9JList1[0];
        phealth.healthSubBean.subQuest8.ANS = null;
        phealth.healthSubBean.subQuest17.ANS = null;
     }
}


this.onChangeQuest4 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest4 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest4 = {};

    if(phealth.healthMainBean.Quest4.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest4.CODE);
        //phealth.DOCS_LIST.Proposer.quest4 = HealthDocList.Proposer[phealth.healthMainBean.Quest4.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest4.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest4.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest4.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest4.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest4.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest4 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest4 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest4 = null;
    }
}

this.onChangeQuest5 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest5 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest5 = {};

    if(phealth.healthMainBean.Quest5.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest5.CODE);
       // phealth.DOCS_LIST.Proposer.quest5 = HealthDocList.Proposer[phealth.healthMainBean.Quest5.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest5.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest5.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest5.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest5.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest5.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest5 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest5 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest5 = null;
    }
}

this.onChangeQuest7 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest7 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest7 = {};

    if(phealth.healthMainBean.Quest7.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest7.CODE);
        //phealth.DOCS_LIST.Proposer.quest7 = HealthDocList.Proposer[phealth.healthMainBean.Quest7.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest7.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest7.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest7.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest7.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest7.CODE].DOC_ID;
    }
    else
    {
        // phealth.DOCS_LIST.Proposer.quest7 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest7 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest7 = null;
    }
}

this.onChangeQuest8 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest8 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest8 = {};

    if(phealth.healthMainBean.Quest8.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest8.CODE);
        //phealth.DOCS_LIST.Proposer.quest8 = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest8.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest8.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest8 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest8.DOC_ID = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest8.DOC_ID = null;
    }
}
this.onChangeQuest9C = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9C = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9C = {};

    if(phealth.healthMainBean.Quest9C.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9C.CODE);
        //phealth.DOCS_LIST.Proposer.quest8 = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9C.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9C.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9C.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9C.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9C.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest8 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9C = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9C = null;
    }
}
this.onChangeQuest9K = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9K = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9K = {};

    if(phealth.healthMainBean.Quest9K.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9K.CODE);
        //phealth.DOCS_LIST.Proposer.quest8 = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9K.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9K.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9K.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9K.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9K.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest8 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9K = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9K = null;
    }
}

this.onChangeQuest9L = function(){

    if(phealth.healthMainBean.Quest9L.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest18.ANS = null;
}

this.onChangeQuest9M = function(){

    if(phealth.healthMainBean.Quest9K.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest19.ANS = null;
}

this.onChangeQuest10 = function(){

    if(phealth.healthMainBean.Quest10.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest20.ANS = null;
}

this.onChangeQuest11 = function(){

    if(phealth.healthMainBean.Quest11.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest21.ANS = null;
}

this.onChangeQuest12B = function(){

    if(phealth.healthMainBean.Quest12B.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest22.ANS = null;
}

this.onChangeQuest12C = function(){

    if(phealth.healthMainBean.Quest12C.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest23.ANS = null;
}

this.onChangeQuest12D = function(){

    if(phealth.healthMainBean.Quest12D.ANSFLAG == 'Y')
    {
    }
    else
        phealth.healthSubBean.subQuest24.ANS = null;
}

this.onChangeQuest12A = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest12A = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest12A = {};

    if(phealth.healthMainBean.Quest12A.ANSFLAG == 'Y')
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest12A.CODE);
        //phealth.DOCS_LIST.Proposer.quest8 = HealthDocList.Proposer[phealth.healthMainBean.Quest8.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest12A.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest12A.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest12A.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest12A.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest12A.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest8 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest12A = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest12A = null;
         phealth.healthMainBean.Quest12A.SUB1.ANS = null;
    }
}


this.onChangeQuest9ASub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ASub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ASub1 = {};

    if(phealth.healthMainBean.Quest9A.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9A.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9A = HealthDocList.Proposer[phealth.healthMainBean.Quest9A.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9A.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ASub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9A.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ASub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9A.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9A = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ASub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ASub1 = null;
    }
}

this.onChangeQuest9BSub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9BSub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9BSub1 = {};

    if(phealth.healthMainBean.Quest9B.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9B.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9B = HealthDocList.Proposer[phealth.healthMainBean.Quest9B.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9B.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9BSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9B.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9BSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9B.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9B = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9BSub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9BSub1 = null;
    }
}

this.onChangeQuest9ESub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ESub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ESub1 = {};

    if(phealth.healthMainBean.Quest9E.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9E.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9E = HealthDocList.Proposer[phealth.healthMainBean.Quest9E.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9E.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ESub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9E.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ESub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9E.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9E = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ESub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ESub1 = null;
    }
}

this.onChangeQuest9FSub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9FSub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9FSub1 = {};

    if(phealth.healthMainBean.Quest9F.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9F.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9F = HealthDocList.Proposer[phealth.healthMainBean.Quest9F.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9F.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9FSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9F.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9FSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9F.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9F = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9FSub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9FSub1 = null;
    }
}

this.onChangeQuest9HSub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9HSub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9HSub1 = {};

    if(phealth.healthMainBean.Quest9H.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9H.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9H = HealthDocList.Proposer[phealth.healthMainBean.Quest9H.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9H.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9HSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9H.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9HSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9H.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9H = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9HSub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9HSub1 = null;
    }
}

this.onChangeQuest9ISub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ISub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9HSub1 = {};

    if(phealth.healthMainBean.Quest9I.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9I.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9I = HealthDocList.Proposer[phealth.healthMainBean.Quest9I.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9I.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ISub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9I.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ISub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9I.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9I = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9ISub1 = null;
         ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9ISub1 = null;
    }
}

this.onChangeQuest9JSub1 = function(){
ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9JSub1 = {};
ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9JSub1 = {};

    if(phealth.healthMainBean.Quest9J.SUB1.ANS.CODE != undefined)
    {
        phealth.showQuestionnaireMessage(phealth.healthMainBean.Quest9J.SUB1.ANS.CODE);
        //phealth.DOCS_LIST.Proposer.quest9J = HealthDocList.Proposer[phealth.healthMainBean.Quest9J.SUB1.ANS.CODE].DOC_ID;
        if(HealthDocList.Proposer[phealth.healthMainBean.Quest9J.SUB1.ANS.CODE].isDigital == "Y")
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9JSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9J.SUB1.ANS.CODE].DOC_ID;
        else
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9JSub1.DOC_ID = HealthDocList.Proposer[phealth.healthMainBean.Quest9J.SUB1.ANS.CODE].DOC_ID;
    }
    else
    {
         //phealth.DOCS_LIST.Proposer.quest9J = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Proposer.HDQuest9JSub1 = null;
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.HDQuest9JSub1 = null;
    }
}

this.showQuestionnaireMessage = function(CODE){
    console.log("CODE :"+CODE);
    navigator.notification.alert(""+HealthDocList.Proposer[CODE].MSG+" Questionnaire need to be filled",function(){CommonService.hideLoading();},"Application","OK");
}



this.setExistingMainData = function(){
var dfd = $q.defer();
try{
  ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppData.applicationPersonalInfo.Insured.DOCS_LIST);
  if(ExistingHealthMainData!=undefined && ExistingHealthMainData.Proposer!=undefined && Object.keys(ExistingHealthMainData).length !=0 && Object.keys(ExistingHealthMainData.Proposer).length!=0)
  {
  //Change to VCP PGLID
  if(phealth.planCode == '222' && phealth.SA < 2000000){
  phealth.healthMainBean.VcpQuest2.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"53"},true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.VcpQuest3.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"54"},true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.VcpQuest4.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"55"},true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest2.ANS1 = parseInt($filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"18"}, true)[0].ANSWAR_DTLS1);
  phealth.healthMainBean.Quest2.ANS2 = parseInt($filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"18"}, true)[0].ANSWAR_DTLS2);
  }
  else
  {
  //VCP change
  /** Change made by Ameya - Removed if condition because Physician details block should be available for all products  */
  //if(phealth.planCode=='222'){
  if(!!$filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"53"}, true)[0])
      phealth.healthMainBean.VcpQuest2.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"53"},true)[0].ANSWAR_FLAG;
  if(!!$filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"54"}, true)[0])
      phealth.healthMainBean.VcpQuest3.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"54"},true)[0].ANSWAR_FLAG;
  if(!!$filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"55"}, true)[0])
      phealth.healthMainBean.VcpQuest4.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"55"},true)[0].ANSWAR_FLAG;

  //}
  if(!!$filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"17"}, true)[0]){
  phealth.healthMainBean.Quest1.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"17"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest2.ANS1 = parseInt($filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"18"}, true)[0].ANSWAR_DTLS1);
  phealth.healthMainBean.Quest2.ANS2 = parseInt($filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"18"}, true)[0].ANSWAR_DTLS2);
  phealth.healthMainBean.Quest3.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"19"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest4.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"20"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest5.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"21"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest6.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"22"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest7.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"23"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest8.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"24"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9A.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"25"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9B.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"27"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9C.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"29"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9D.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"30"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9E.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"31"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9F.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"33"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9G.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"35"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9H.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"36"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9I.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"38"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9J.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"40"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9K.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"42"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9L.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"43"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest9M.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"44"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest10.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"45"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest11.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"46"}, true)[0].ANSWAR_FLAG;
  if(!!$filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"47"}, true)[0]){
  phealth.healthMainBean.Quest12A.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"47"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest12B.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"49"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest12C.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"50"}, true)[0].ANSWAR_FLAG;
  phealth.healthMainBean.Quest12D.ANSFLAG = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"51"}, true)[0].ANSWAR_FLAG;
  }

  //SUB
  if(phealth.healthMainBean.Quest9A.ANSFLAG =='Y')
     {
      var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"26"}, true)[0].ANSWAR_DTLS1;
      phealth.healthMainBean.Quest9A.SUB1.ANS = $filter('filter')(phealth.Quest9AList1, {"VALUE":ANS}, true)[0]
     }
  if(phealth.healthMainBean.Quest9B.ANSFLAG == 'Y')
      {
          var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"28"}, true)[0].ANSWAR_DTLS1;
          phealth.healthMainBean.Quest9B.SUB1.ANS = $filter('filter')(phealth.Quest9BList1, {"VALUE":ANS}, true)[0]
      }
  if(phealth.healthMainBean.Quest9E.ANSFLAG == 'Y')
       {
              var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"32"}, true)[0].ANSWAR_DTLS1;
              phealth.healthMainBean.Quest9E.SUB1.ANS = $filter('filter')(phealth.Quest9EList1, {"VALUE":ANS}, true)[0]
       }

  if(phealth.healthMainBean.Quest9F.ANSFLAG == 'Y')
      {
              var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"34"}, true)[0].ANSWAR_DTLS1;
              phealth.healthMainBean.Quest9F.SUB1.ANS = $filter('filter')(phealth.Quest9FList1, {"VALUE":ANS}, true)[0]
       }
  if(phealth.healthMainBean.Quest9H.ANSFLAG == 'Y')
      {
                  var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"37"}, true)[0].ANSWAR_DTLS1;
                  phealth.healthMainBean.Quest9H.SUB1.ANS = $filter('filter')(phealth.Quest9HList1, {"VALUE":ANS}, true)[0]
           }
  if(phealth.healthMainBean.Quest9I.ANSFLAG == 'Y')
   {
                  var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"39"}, true)[0].ANSWAR_DTLS1;
                  phealth.healthMainBean.Quest9I.SUB1.ANS = $filter('filter')(phealth.Quest9IList1, {"VALUE":ANS}, true)[0]
           }
  if(phealth.healthMainBean.Quest9J.ANSFLAG == 'Y')
  {
                  var ANS = $filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"41"}, true)[0].ANSWAR_DTLS1;
                  phealth.healthMainBean.Quest9J.SUB1.ANS = $filter('filter')(phealth.Quest9JList1, {"VALUE":ANS}, true)[0]
           }
  //Female
  if(phealth.healthMainBean.Quest12A.ANSFLAG == 'Y')
      phealth.healthMainBean.Quest12A.SUB1.ANS = CommonService.formatDobFromDb($filter('filter')(ExistingHealthMainData.Proposer, {"QUESTION_ID":"48"}, true)[0].ANSWAR_DTLS1);
  }
  }
  }
}catch(e){
    debug("Exception")
    CommonService.hideLoading();
}

dfd.resolve(phealth.healthMainBean);
return dfd.promise;
}

this.setExistingSubData = function(){
console.log("setExistingSubData Proposer!"+JSON.stringify(ExistingHealthSubData.Proposer))
/*for(var i=0; i<ExistingHealthSubData.Proposer.length; i++)
{
var _temp = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":(i+1)});
    if(_temp!=undefined && _temp[0]!=undefined)
        if(_temp[0].ANSWAR_DTLS1 !=undefined)
        phealth.healthSubBean["subQuest"+(i+1)].ANS = _temp[0].ANSWAR_DTLS1;
}*/
if(ExistingHealthSubData!=undefined && ExistingHealthSubData.Proposer!=undefined && Object.keys(ExistingHealthSubData).length !=0 && Object.keys(ExistingHealthSubData.Proposer).length!=0)
{
console.log("inside ExistingHealthSubData::"+phealth.healthSubBean.VcpsubQuest25.ANS);
if(phealth.healthMainBean.Quest3.ANSFLAG == 'Y')
   {
    phealth.healthSubBean.subQuest1.ANS = parseInt($filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"1"}, true)[0].ANSWAR_DTLS1);
    phealth.healthSubBean.subQuest2.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"2"}, true)[0].ANSWAR_DTLS1;
   }
if(phealth.healthMainBean.Quest9B.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9B.SUB1.ANS.VALUE == 'Raised Cholesterol')
    phealth.healthSubBean.subQuest3.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"3"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9E.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9E.SUB1.ANS.VALUE == 'Nervous disorder')
    phealth.healthSubBean.subQuest4.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"4"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9E.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9E.SUB1.ANS.VALUE == 'Any Other disorder')
    phealth.healthSubBean.subQuest5.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"5"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9H.ANSFLAG == 'Y'&& phealth.healthMainBean.Quest9H.SUB1.ANS.VALUE == 'Leprosy')
    phealth.healthSubBean.subQuest6.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"6"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9I.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9I.SUB1.ANS.VALUE == 'HIV/AIDS or other sexually transmitted disease')
    phealth.healthSubBean.subQuest7.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"7"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9J.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9J.SUB1.ANS.VALUE == 'Hydrocele/fistula/Piles')
    phealth.healthSubBean.subQuest8.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"8"}, true)[0].ANSWAR_DTLS1;
//phealth.healthSubBean.subQuest9.ANS =
if(phealth.healthMainBean.Quest9A.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9A.SUB1.ANS.VALUE == 'High Blood Sugar')
phealth.healthSubBean.subQuest10.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"10"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9A.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9A.SUB1.ANS.VALUE == 'Sugar in Urine')
    phealth.healthSubBean.subQuest11.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"11"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9B.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9B.SUB1.ANS.VALUE == 'Low BP')
    phealth.healthSubBean.subQuest12.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"12"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9D.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest13.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"13"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9E.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9E.SUB1.ANS.VALUE == 'None')
    phealth.healthSubBean.subQuest14.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"14"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9G.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9G.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest15.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"15"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9H.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9H.SUB1.ANS.VALUE == 'None')
    phealth.healthSubBean.subQuest16.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"16"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9J.ANSFLAG == 'Y' && phealth.healthMainBean.Quest9J.SUB1.ANS.VALUE == 'Prosate, Urinary System or Reproductive System')
    phealth.healthSubBean.subQuest17.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"17"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9L.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest18.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"18"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest9M.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest19.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"19"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest10.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest20.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"20"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest11.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest21.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"21"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest12B.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest22.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"22"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest12C.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest23.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"23"}, true)[0].ANSWAR_DTLS1;
if(phealth.healthMainBean.Quest12D.ANSFLAG == 'Y')
    phealth.healthSubBean.subQuest24.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"24"}, true)[0].ANSWAR_DTLS1;
//VCP change for subquestions.

    console.log("VCP QUEST ANSWERS");
    console.log(phealth.healthMainBean.VcpQuest2.ANSFLAG);
    console.log(phealth.healthMainBean.VcpQuest3.ANSFLAG);
    console.log(phealth.healthMainBean.VcpQuest4.ANSFLAG);

//if(phealth.planCode=='222'){
    if(phealth.healthMainBean.VcpQuest2.ANSFLAG == 'Y')
        phealth.healthSubBean.VcpsubQuest25.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"25"}, true)[0].ANSWAR_DTLS1;
    if(phealth.healthMainBean.VcpQuest3.ANSFLAG == 'Y')
        phealth.healthSubBean.VcpsubQuest26.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"26"}, true)[0].ANSWAR_DTLS1;
    if(phealth.healthMainBean.VcpQuest4.ANSFLAG == 'Y')
        phealth.healthSubBean.VcpsubQuest27.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"27"}, true)[0].ANSWAR_DTLS1;
		if(phealth.healthMainBean.Quest1.ANSFLAG == 'N')
		  phealth.healthSubBean.subQuest28.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"28"}, true)[0].ANSWAR_DTLS1;
		if(phealth.healthMainBean.Quest6.ANSFLAG == 'Y')
		  phealth.healthSubBean.subQuest29.ANS = $filter('filter')(ExistingHealthSubData.Proposer, {"SUB_QUESTION_ID":"29"}, true)[0].ANSWAR_DTLS1;
    console.log(phealth.healthSubBean.VcpsubQuest25.ANS);
    console.log(phealth.healthSubBean.VcpsubQuest26.ANS);
    console.log(phealth.healthSubBean.VcpsubQuest27.ANS);


//}

}


}

this.setExistingMainData().then(function(){

console.log("setExistingMainData Complete !")
phealth.setExistingSubData();

});

this.onNext = function(healthProForm){
	CommonService.showLoading();
phealth.click = true;
    console.log("QUEST :MAIN:"+JSON.stringify(phealth.healthMainBean));
    console.log("QUEST :SUB:"+JSON.stringify(phealth.healthSubBean));
    console.log("QUEST :AFTER:"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    if(healthProForm.$invalid !=true){
    QuestionnaireService.getHealthMainQuestData(phealth.healthMainBean, 'C02').then(function(MainData){

        console.log("RESULT SET MAIN:"+JSON.stringify(MainData));
        QuestionnaireService.getHealthSubQuestData(phealth.healthSubBean, 'C02').then(function(SubData){
            phealth.deleteHealthQuest('LP_APP_HD_ANS_SCRN_C').then(function(){
                phealth.deleteHealthQuest('LP_APP_HD_ANS_SCRN_C13').then(function(){
                    console.log("RESULT SET SUB:"+JSON.stringify(SubData));
                        for(var i=0;i<SubData.length;i++)
                                            (function(i){
                                            CommonService.insertOrReplaceRecord(db, 'LP_APP_HD_ANS_SCRN_C13', SubData[i], true).then(
                                                            function(res){
                                                                console.log("LP_APP_HD_ANS_SCRN_C13 success Proposer: "+"i:"+i+"!! : : "+JSON.stringify(res));

                                                            }
                                                        );
                                            })(i);
                        for(var i=0;i<MainData.length;i++)
                            (function(i){
                            CommonService.insertOrReplaceRecord(db, 'LP_APP_HD_ANS_SCRN_C', MainData[i], true).then(
                                            function(res){
                                                console.log("LP_APP_HD_ANS_SCRN_C success Proposer: "+"i:"+i+"!! : : "+JSON.stringify(res));
                                                if(i == (MainData.length-1)){
                                                phealth.updateAppMainTables();
                                                //Changed by Ganesh 07-07-2016
                                                /*if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED != 'Y')
                                                    $state.go("applicationForm.questionnairehl.healthPro");
                                                else*/
                                                    //timeline changes..
                                                    CommonService.showLoading();
                                                    AppTimeService.isHealthCompleted = true;
                                                    AppTimeService.setActiveTab("existingPolicy");
                                                    //$rootScope.val = 30;
                                                    AppTimeService.setProgressValue(30);
                                                    $state.go("applicationForm.existingIns.tataPolicy");
                                                }
                                            }
                                        );
                            })(i);
                });
            });
        });
    });
    }
    else
        navigator.notification.alert(INCOMPLETE_DATA_MSG,function(){CommonService.hideLoading();} ,"Application","OK");
}

this.deleteHealthQuest = function(tableName){
var dfd = $q.defer();
console.log("Inside deleteHealthQuest");
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    whereClauseObj.CUST_TYPE = 'C02';
    CommonService.deleteRecords(db,tableName,whereClauseObj).then(
        function(res){
            console.log(tableName+" Record deleted successfully");
            dfd.resolve(true);
        }

    );
    return dfd.promise;


}
this.updateAppMainTables = function(){
console.log("inside updateAppMainTables")
var whereClauseObj = {};
whereClauseObj.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
whereClauseObj.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;

var whereClauseObj1 = {};
        	whereClauseObj1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj1.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj1.CUST_TYPE = 'C01';
var whereClauseObj2 = {};
        	whereClauseObj2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        	whereClauseObj2.AGENT_CD = ApplicationFormDataService.applicationFormBean.personalDetBean.AGENT_CD;
        	whereClauseObj2.CUST_TYPE = 'C02';

CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj1).then(
				                function(res){
				                console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST update success !!"+JSON.stringify(whereClauseObj1));

				                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN3_COMPLETED" :"Y"}, whereClauseObj).then(
													function(res){
														console.log("IS_SCREEN3_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
													});
                      	CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST)}, whereClauseObj2).then(
                          function(res){
                              console.log("LP_APP_CONTACT_SCRN_A DOCS_LIST proposer update success !!"+JSON.stringify(whereClauseObj2));
                          });


								});

}

CommonService.hideLoading();
}]);
