questModule.controller('QuestionnaireCtrl', ['CommonService', function(CommonService){

//timeline changes..
CommonService.hideLoading();

}]);

questModule.service('QuestionnaireService', ['$q', 'CommonService', 'LoginService','ApplicationFormDataService','$state','qHealthInfoService','qLifeStyleInfoService', function($q, CommonService, LoginService, ApplicationFormDataService,$state,qHealthInfoService,qLifeStyleInfoService){

    this.gotoLSPage = function(pageName){
        if(qLifeStyleInfoService.getActiveTab()!==pageName){
            CommonService.showLoading();
        }
        console.log("QuestionnaireCtrl pageName >>> "+pageName);
        qLifeStyleInfoService.setActiveTab(pageName);
        var isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)

        switch(pageName){
        case 'lifeStyleIns':
            $state.go('applicationForm.questionnairels.lifeStyleIns');
        break;

        case 'lifeStylePro':
            if(!isSelf){
                $state.go('applicationForm.questionnairels.lifeStylePro');
            }
            else
                navigator.notification.alert("It's a Self Case",function(){CommonService.hideLoading();},"Application","OK");
        break;

        default:
            $state.go('applicationForm.questionnairels.lifeStyleIns');
        }
    };

    this.gotoHealthPage = function(pageName){
        if(qHealthInfoService.getActiveTab()!==pageName){
            CommonService.showLoading();
        }
        console.log("QuestionnaireCtrl pageName >>> "+pageName);
        qHealthInfoService.setActiveTab(pageName);
        var isSelf = ((ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)
        switch(pageName){
            case 'healthIns':
                $state.go('applicationForm.questionnairehl.healthIns');
            break;

            case 'healthPro':
                if(!isSelf){
                $state.go('applicationForm.questionnairehl.healthPro');
                }
                else
                    navigator.notification.alert("It's a Self Case",function(){CommonService.hideLoading();},"Application","OK");
            break;

            default:
                $state.go('applicationForm.questionnairehl.healthIns');
        }
    }

    this.getLifeStyleQuest = function(lifeStyleBean, CUST_TYPE){
var dfd = $q.defer();
var lifeStyleData = [];
for (var key in lifeStyleBean) {
   if (lifeStyleBean.hasOwnProperty(key)) {
      var obj = lifeStyleBean[key];
      console.log("My Results :: " + JSON.stringify(obj));
      var life = {};
      life.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
      life.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
      life.MODIFIED_DATE = CommonService.getCurrDate();
      life.CUST_TYPE = CUST_TYPE;
      life.QUESTION_ID = obj.ID;
      if(obj.SEQ!=undefined)
      	life.QUESTION_SEQUENCE = obj.SEQ;
      if(obj.ANSFLAG!=undefined)
      	{
      		life.ANSWAR_FLAG = obj.ANSFLAG;
      		if(obj.DOC_ID!=undefined)
      			life.DOC_ID = obj.DOC_ID;
      		if(obj.SEQ != "1" && obj.ANSFLAG == 'Y')
      		{
      			var subLife = {};
      			subLife.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
				subLife.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
				subLife.MODIFIED_DATE = CommonService.getCurrDate();
				subLife.CUST_TYPE = CUST_TYPE;

				subLife.ANSWAR_FLAG = null;
				for (var prop in obj) {
				console.log("FOR seq:: "+obj.SEQ);
                      console.log("prop :"+prop );
                      if(obj.SEQ == '2' && prop != "SUB2")
						{	if(obj[prop].ANS!=undefined){
							console.log("obj.SUB1.ID :"+obj.SUB1.ID);
							var subLife1 = {};
							subLife1.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
                            subLife1.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                            subLife1.MODIFIED_DATE = CommonService.getCurrDate();
                            subLife1.CUST_TYPE = CUST_TYPE;
                            subLife1.ANSWAR_FLAG = null;
							subLife1.QUESTION_ID = obj.SUB1.ID;
							if(obj.SUB1.SEQ!=undefined)
                              subLife1.QUESTION_SEQUENCE = obj.SUB1.SEQ;
                            if(obj.SUB1.ANS.VALUE == "Occupation")
                            	subLife1.DOC_ID = obj.SUB1.DOC_ID;
							subLife1.ANSWAR_DTLS1 = obj.SUB1.ANS.VALUE;
							lifeStyleData.push(subLife1);
							if(obj.SUB1.ANS.VALUE == "Hobbies" && obj[prop].ANS!= undefined)
							{var subLife2 = {};
								console.log("obj.SUB2.ID :"+obj.SUB2.ID);
								subLife2.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
								subLife2.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
								subLife2.MODIFIED_DATE = CommonService.getCurrDate();
								subLife2.CUST_TYPE = CUST_TYPE;
								subLife2.ANSWAR_FLAG = null;
								subLife2.QUESTION_ID = obj.SUB2.ID;
								if(obj.SUB2.SEQ!=undefined)
                                    subLife2.QUESTION_SEQUENCE = obj.SUB2.SEQ;
								subLife2.ANSWAR_DTLS1 = obj.SUB2.ANS.VALUE;
								if(obj.SUB2.DOC_ID!=undefined)
								subLife2.DOC_ID = obj.SUB2.DOC_ID;
								lifeStyleData.push(subLife2);
							}
						  }
                      	}
                      if(obj.SEQ == '3' || obj.SEQ == '4')
                      	if(obj[prop].ANS!=undefined)
							{	console.log("obj[prop].ANS :"+obj[prop].ANS);
								subLife.QUESTION_ID = obj[prop].ID;
								subLife.QUESTION_SEQUENCE = obj[prop].SEQ;
								subLife.ANSWAR_DTLS1 = obj[prop].ANS;
								lifeStyleData.push(subLife);
							}
					  if(obj.SEQ == '5' || obj.SEQ == '6' || obj.SEQ == '7')
					  	if(obj[prop].ANS1!=undefined)
							{
								subLife.QUESTION_ID = obj[prop].ID;
								subLife.QUESTION_SEQUENCE = obj[prop].SEQ;
								if(obj[prop].ANS1.NBFE_CODE !=undefined)
									subLife.ANSWAR_DTLS1 = obj[prop].ANS1.LOOKUP_TYPE_DESC;
								else
									subLife.ANSWAR_DTLS1 = null;
								subLife.ANSWAR_DTLS2 = obj[prop].ANS2;
								if(obj[prop].ANS3.NBFE_CODE!=undefined)
									subLife.ANSWAR_DTLS3 = obj[prop].ANS3.LOOKUP_TYPE_DESC;
								else
                                	subLife.ANSWAR_DTLS3 = null;
								subLife.ANSWAR_DTLS4 = obj[prop].ANS1.NBFE_CODE;
								subLife.ANSWAR_DTLS5 = obj[prop].ANS2;
								subLife.ANSWAR_DTLS6 = obj[prop].ANS3.NBFE_CODE;
								lifeStyleData.push(subLife);
							}
					  if(obj.SEQ == '8')
						if(obj[prop].ANS1!=undefined)
							{
								subLife.QUESTION_ID = obj[prop].ID;
								subLife.QUESTION_SEQUENCE = obj[prop].SEQ;
								if(obj[prop].ANS1!=undefined && obj[prop].ANS1!="")
									subLife.ANSWAR_DTLS1 = obj[prop].ANS1;
								else
									subLife.ANSWAR_DTLS1 = null;
								subLife.ANSWAR_DTLS2 = obj[prop].ANS2;
								if(obj[prop].ANS3.NBFE_CODE!=undefined)
									subLife.ANSWAR_DTLS3 = obj[prop].ANS3.LOOKUP_TYPE_DESC;
								else
									subLife.ANSWAR_DTLS3 = null;
								subLife.ANSWAR_DTLS4 = obj[prop].ANS1;
								subLife.ANSWAR_DTLS5 = obj[prop].ANS2;
								subLife.ANSWAR_DTLS6 = obj[prop].ANS3.NBFE_CODE;

								lifeStyleData.push(subLife);
							}

                   }


      		}
			lifeStyleData.push(life);
      	}

   }
}

dfd.resolve(lifeStyleData);
return dfd.promise;
}

this.getHealthMainQuestData = function(healthMainData, CUST_TYPE){
var dfd = $q.defer();
var planCode = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
var SA = ApplicationFormDataService.SISFormData.sisFormBData.SUM_ASSURED;
console.log("Health getHealthMainQuestData ::"+planCode+"SA is::"+SA);
var healthData = [];

for (var key in healthMainData) {
   if (healthMainData.hasOwnProperty(key)) {
		  var obj = healthMainData[key];
		  console.log("Health RES :: " + JSON.stringify(obj)+"obj.ANSFLAG::"+obj.ANSFLAG);
		  var health = {};
                health.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
                health.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                health.MODIFIED_DATE = CommonService.getCurrDate();
                health.CUST_TYPE = CUST_TYPE;
                health.QUESTION_ID = obj.ID;

                if(obj.ANSFLAG!=undefined || obj.ID == '18' || obj.ID == '52')
                {
                      console.log("inside ifElse");
      						if(obj.ANSFLAG!=undefined)
      							health.ANSWAR_FLAG = obj.ANSFLAG;
      						else
      							health.ANSWAR_FLAG = null;
      						if(obj.ID == "52")
      						{
      							console.log("ID is 52::");
      							health.ANSWAR_DTLS1 = obj.ANS1;
      							health.ANSWAR_DTLS2 = obj.ANS2;
      							health.ANSWAR_DTLS3 = obj.ANS3;
      						}
      						 if(obj.ID == "18")
      							{
      								health.ANSWAR_DTLS1 = obj.ANS1;
      								health.ANSWAR_DTLS2 = obj.ANS2;
      							}
      						if((obj.ID == "25" || obj.ID == "27" || obj.ID == "31" || obj.ID == "33" || obj.ID == "36" || obj.ID == "38" || obj.ID == "40") && obj.ANSFLAG!=undefined && obj.ANSFLAG == 'Y')
      							{

      								if(obj.SUB1.ANS!=undefined && obj.SUB1.ANS.VALUE!=undefined){
      								console.log("ID :here:"+obj.ID+"obj.SUB1.ANS.VALUE:"+obj.SUB1.ANS.VALUE);
      								var temp = {};
      								temp.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
      								temp.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
      								temp.MODIFIED_DATE = CommonService.getCurrDate();
      								temp.CUST_TYPE = CUST_TYPE;
      								temp.QUESTION_ID = obj.SUB1.ID;
      								temp.ANSWAR_DTLS1 = obj.SUB1.ANS.VALUE;
      								console.log("JSON :"+JSON.stringify(temp));
      								healthData.push(temp);
      								}

      							}
      							if(obj.ID == "47" && obj.ANSFLAG!=undefined && obj.ANSFLAG == 'Y')
      							{

      									if(obj.SUB1.ANS!=undefined){
      									console.log("ID :here:"+obj.ID);
      									var temp = {};
      									temp.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
      									temp.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
      									temp.MODIFIED_DATE = CommonService.getCurrDate();
      									temp.CUST_TYPE = CUST_TYPE;
      									temp.QUESTION_ID = obj.SUB1.ID;
      									temp.ANSWAR_DTLS1 = CommonService.formatDobToDb(obj.SUB1.ANS);
      									console.log("JSON :"+JSON.stringify(temp));
      									healthData.push(temp);
      									}

      							}

					         //console.log("JSON :"+JSON.stringify(health));
					     healthData.push(health);
				    }
      	}
   }
   dfd.resolve(healthData);
return dfd.promise;
}

this.getHealthSubQuestData = function(healthSubData, CUST_TYPE){
var dfd = $q.defer();

console.log("Health getHealthSubQuestData ::");
var healthData = [];

for (var key in healthSubData) {
   if (healthSubData.hasOwnProperty(key)) {
		  var obj = healthSubData[key];
		  console.log("Health SubRES :: " + JSON.stringify(obj));
		  	if(obj.ANS!=undefined){
		  				var health = {};
                          health.APPLICATION_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
                          health.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                          health.MODIFIED_DATE = CommonService.getCurrDate();
                          health.CUST_TYPE = CUST_TYPE;
                          health.SUB_QUESTION_ID = obj.ID;
                          health.ANSWAR_DTLS1 = obj.ANS;
                          health.QUESTION_SEQUENCE = obj.SEQ;
                          healthData.push(health);
                      }

		  }
	}
	dfd.resolve(healthData);
return dfd.promise;
}

}]);
