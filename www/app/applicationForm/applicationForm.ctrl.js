AppFormModule.controller('ApplicationCtrl', ['$q', '$stateParams','$state','CommonService', 'LoginService', 'ApplicationFormDataService','LoadAppProofList','LoadApplicationScreenData','GenerateOutputService','$timeout','LoadReflService','percent','AppTimeService', function($q, $stateParams, $state, CommonService, LoginService, ApplicationFormDataService, LoadAppProofList,LoadApplicationScreenData, GenerateOutputService, $timeout,LoadReflService,percent,AppTimeService){
    var appCtrl = this;


    var sign = "%";
    debug("PERCENT >>"+percent);
    //this.countTo = percent;
    //this.countFrom = 0;
    if(!!percent)
        AppTimeService.setProgressValue(percent);
    else
        AppTimeService.setProgressValue(0);

    debug("APP ID >>"+$stateParams.APPLICATION_ID);
    debug("AGENT CD >>"+$stateParams.AGENT_CD);

    if($stateParams!=undefined && $stateParams.APPLICATION_ID!=undefined){
        console.log("GOT APP ID ::"+$stateParams.APPLICATION_ID);
        appCtrl.APPLICATION_ID = $stateParams.APPLICATION_ID;
    }
}]);

AppFormModule.service('ApplicationFormDataService',['$q', '$state','$stateParams','CommonService','LoginService','LoadApplicationScreenData','LoadAppProofList','GenerateOutputService','AppTimeService','LoadReflService','GenerateTermOutputService', function($q, $state,$stateParams, CommonService, LoginService, LoadApplicationScreenData,LoadAppProofList, GenerateOutputService,AppTimeService,LoadReflService,GenerateTermOutputService){
    var appFormServ = this;
    appFormServ.applicationFormBean = {}; // Application Form Data Bean
    appFormServ.ComboFormBean = {}; // COMBO Data Bean

    this.appCompletedPercent = function(APPLICATION_ID,AGENT_CD){
        var dfd = $q.defer();
        var count = 0;
        LoadApplicationScreenData.loadApplicationFlags(APPLICATION_ID,AGENT_CD).then(
            function(appFLags){
                var flag = appFLags.applicationFlags;
                debug("<< COUNT CALCULATION >>"+JSON.stringify(flag));
                if(!!flag){
                    if(flag.IS_SCREEN1_COMPLETED == "Y"){
                        debug("life stage");
                        count += 10;
                    }
                    if(flag.IS_SCREEN2_COMPLETED == "Y"){
                        debug("life style");
                        count += 10;
                    }
                    if(flag.IS_SCREEN3_COMPLETED == "Y"){
                        debug("health");
                        count += 10;
                    }
                    if(flag.IS_SCREEN4_COMPLETED == "Y"){
                        debug("exist policy");
                        count += 10;
                    }
                    if(flag.IS_SCREEN5_COMPLETED == "Y"){
                        debug("family details");
                        count += 10;
                    }
                    if(flag.IS_SCREEN6_COMPLETED == "Y"){
                        debug("Nom Appt");
                        count += 10;
                    }
                    if(flag.IS_SCREEN7_COMPLETED == "Y"){
                        debug("pay details");
                        count += 10;
                    }
                    if(flag.IS_SCREEN8_COMPLETED == "Y"){
                        debug("pol no");
                        count += 10;
                    }
                    if(flag.IS_SCREEN9_COMPLETED == "Y" ){
                        debug("meddia");
                        count += 10;
                    }
                    if(flag.IS_SCREEN10_COMPLETED == "Y" || flag.IS_SCREEN10_COMPLETED == "NA"){
                        debug("reflex");
                        count += 10;
                    }
                    debug("count is >>"+count);
                    dfd.resolve(count);
                }
                else{
                    debug("count is >>"+count);
                    dfd.resolve(count);
                }
            }
        );

        return dfd.promise;
    }


    //tick marks on header..
    //timeline changes..
    this.setAllHeaderFlags = function(APPLICATION_ID,AGENT_CD){
        var dfd = $q.defer();

        LoadApplicationScreenData.loadApplicationFlags(APPLICATION_ID,AGENT_CD).then(
            function(appFLags){
                var flag = appFLags.applicationFlags;
                debug("<< ALL FLAGS DATA SET HERE >>"+JSON.stringify(flag));
                if(!!flag){
                    if(flag.IS_SCREEN1_COMPLETED == "Y"){
                        debug("life stage");
                        AppTimeService.isLifeStageCompleted = true;
                    }
                    if(flag.IS_SCREEN2_COMPLETED == "Y"){
                        debug("life style");
                        AppTimeService.isLifeStyleCompleted = true;
                    }
                    if(flag.IS_SCREEN3_COMPLETED == "Y"){
                        debug("health");
                        AppTimeService.isHealthCompleted = true;
                    }
                    if(flag.IS_SCREEN4_COMPLETED == "Y"){
                        debug("exist policy");
                        AppTimeService.isExistPolicyCompleted = true;
                    }
                    if(flag.IS_SCREEN5_COMPLETED == "Y"){
                        debug("family details");
                        AppTimeService.isFamDetCompleted = true;
                    }
                    if(flag.IS_SCREEN6_COMPLETED == "Y"){
                        debug("Nom Appt");
                        AppTimeService.isNomApptCompleted = true;
                    }
                    if(flag.IS_SCREEN7_COMPLETED == "Y"){
                        debug("pay details");
                        AppTimeService.isPayDetCompleted = true;
                    }
                    if(flag.IS_SCREEN8_COMPLETED == "Y"){
                        debug("pol no");
                        AppTimeService.isPolNoGenCompleted = true;
                    }
                    if(flag.IS_SCREEN9_COMPLETED == "Y"){
                        debug("med dia");
                        AppTimeService.isMedDiaCompleted = true;
                    }
                    if(flag.IS_SCREEN10_COMPLETED == "Y"){
                        debug("reflex");
                        AppTimeService.isReflQuestCompleted = true;
                    }
                    if(flag.IS_SCREEN11_COMPLETED == "Y"){
                        debug("out gen");
                        AppTimeService.isOutGenCompleted = true;
                    }
                    if(flag.IS_SCREEN12_COMPLETED == "Y" && !!flag.COMBO_ID){
                        debug("Term out gen");
                        AppTimeService.isTermOutGenCompleted = true;
                        AppTimeService.isTab = true;
                    }
                    else
                        {
                            debug("ELSE:Term out gen"+flag.COMBO_ID);
                            AppTimeService.isTermOutGenCompleted = false;
                            if(!!flag.COMBO_ID)
                                AppTimeService.isTab = true;
                            else
                                AppTimeService.isTab = false;
                        }
                }
                else{
                    debug("resetting flag");
                    if(!!flag && !!flag.COMBO_ID)
                       AppTimeService.isTab = true;
                    else
                       AppTimeService.isTab = false;

                    AppTimeService.isLifeStageCompleted = false;
                    AppTimeService.isLifeStyleCompleted = false;
                    AppTimeService.isHealthCompleted = false;
                    AppTimeService.isExistPolicyCompleted = false;
                    AppTimeService.isFamDetCompleted = false;
                    AppTimeService.isNomApptCompleted = false;
                    AppTimeService.isPayDetCompleted = false;
                    AppTimeService.isPolNoGenCompleted = false;
                    AppTimeService.isMedDiaCompleted = false;
                    AppTimeService.isReflQuestCompleted = false;
                    AppTimeService.isOutGenCompleted = false;
                    AppTimeService.isTermOutGenCompleted = false;
                }
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    }

    //go to clicked header page
    //timeline changes..
    this.gotoPage = function(pageName){
        if(AppTimeService.getActiveTab()!==pageName){
            CommonService.showLoading();
        }
        console.log(JSON.stringify($stateParams)+"gotoPage :pageName:"+pageName);
        if(!!appFormServ.applicationFormBean.APPLICATION_ID && !$stateParams.APPLICATION_ID)
        {
            debug("inside setting$stateParams.APPLICATION_ID")
            $stateParams.APPLICATION_ID = appFormServ.applicationFormBean.APPLICATION_ID;
        }
        LoadApplicationScreenData.loadApplicationFlags($stateParams.APPLICATION_ID, $stateParams.AGENT_CD).then(function(ApplicationFlagsList){
            var params = $stateParams;
            var FlagList = ApplicationFlagsList.applicationFlags;
            console.log(JSON.stringify(FlagList)+"gotoPage : params ::"+JSON.stringify(params));
            //timeline changes..
            AppTimeService.setActiveTab(pageName);
            switch(pageName){
                    case 'lifeStage':
                        $state.go('applicationForm.personalInfo.personalDetails',{"OPP_ID":params.OPP_ID,"APPLICATION_ID":params.APPLICATION_ID, "AGENT_CD": params.AGENT_CD, "SIS_ID":params.SIS_ID});
                        break;
                    case 'lifeStyle':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN1_COMPLETED == 'Y' || FlagList.IS_SCREEN1_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.questionnairels.lifeStyleIns');
                            }
                        else{
                            navigator.notification.alert("Please complete Personal Information",null,"Application","OK");
                            CommonService.hideLoading();
                        }
                        break;
                    case 'health':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN2_COMPLETED == 'Y' || FlagList.IS_SCREEN2_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.questionnairehl.healthIns');
                            }
                        else{
                            navigator.notification.alert("Please complete Personal Information",null,"Application","OK");
                            CommonService.hideLoading();
                        }
                        break;
                    case 'existingPolicy':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN3_COMPLETED == 'Y' || FlagList.IS_SCREEN3_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.existingIns.tataPolicy');
                            }
                        else{
                            navigator.notification.alert("Please complete Health Questionnaire",null,"Application","OK");
                             CommonService.hideLoading();
                            }
                        break;
                    case 'familyDetails':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN4_COMPLETED == "Y" || FlagList.IS_SCREEN4_COMPLETED == 'NA'))
                             {
                                $state.go('applicationForm.familyDetails.familySection1');
                             }
                        else{
                             navigator.notification.alert("Please complete Existing Policy Details",null,"Application","OK");
                             CommonService.hideLoading();
                            }
                        break;
                    case 'nomAppDetails':
                        this.isSelf = ((appFormServ.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y') ? true:false)
                        debug("SELF CASE "+this.isSelf);
                        if(isSelf){
                            if(FlagList!=undefined && (FlagList.IS_SCREEN5_COMPLETED == "Y" || FlagList.IS_SCREEN5_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.nomAppDetails.nomineeDetails');
                            }
                            else{
                                navigator.notification.alert("Please complete Family Details",null,"Application","OK");
                                CommonService.hideLoading();
                            }
                        }else{
                            navigator.notification.alert("Not Allowed For Non Self Case",null,"Application","OK");
                        }
                        break;
                    case 'paymentDetails':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN6_COMPLETED == "Y" || FlagList.IS_SCREEN6_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.paymentDetails.bankdetails');
                            }
                        else{
                             navigator.notification.alert("Please complete Nominee Details",null,"Application","OK");
                             CommonService.hideLoading();
                            }
                        break;
                    case 'policyNoGen':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN7_COMPLETED == "Y" || FlagList.IS_SCREEN7_COMPLETED == 'NA'))
                            {
                                $state.go('applicationForm.policyNoGen');
                            }
                        else{
                             navigator.notification.alert("Please complete Payment Details",null,"Application","OK");
                             CommonService.hideLoading();
                            }
                        break;
                    case 'medDiag':
                        if(FlagList!=undefined && (FlagList.IS_SCREEN8_COMPLETED == "Y" || FlagList.IS_SCREEN8_COMPLETED == 'NA'))
                        {
                            var whereClauseObj1 = {};
                            whereClauseObj1.APPLICATION_ID = appFormServ.applicationFormBean.APPLICATION_ID;
                            whereClauseObj1.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

                            CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN9_COMPLETED":'Y'},whereClauseObj1).then(
                                                                    function(res){
                                                                            $state.go("applicationForm.medicalDiagnosis.insMedDiagnosis");
                                                                            console.log("Screen updated successfully :IS_SCREEN9_COMPLETED");
                                                                    }
                            );

                        }
                        else{
                             navigator.notification.alert("Policy Number not generated.\n Please complete previous screens.",null,"Application","OK");
                             CommonService.hideLoading();
                            }
                            break;
                    case 'reflectiveQues':

                        var reflInsObj = appFormServ.applicationFormBean.DOCS_LIST.Reflex.Insured;//{'Health Ques' : {'DOC_ID' : '1030010293','stdFlag' : 'Y'},'HW Ques' : {'DOC_ID' : '1020010239','stdFlag' : 'N'}};
                        var reflPropObj = appFormServ.applicationFormBean.DOCS_LIST.Reflex.Proposer;//{'Health Ques' : {'DOC_ID' : '2030010273','stdFlag' : 'Y'},'HW Ques' : {'DOC_ID' : '2020010229','stdFlag' : 'N'}};
                        debug("reflInsObj : " + JSON.stringify(reflInsObj))
                        debug("reflPropObj : " + JSON.stringify(reflPropObj))
                        LoadReflService.getInsReflectiveQuesList(reflInsObj,appFormServ.applicationFormBean.APPLICATION_ID,appFormServ.applicationFormBean.POLICY_NO,appFormServ).then(
                            function(reflectiveQuesArr){
                                LoadReflService.getPropReflectiveQuesList(reflectiveQuesArr,reflPropObj,appFormServ.applicationFormBean.APPLICATION_ID,appFormServ.applicationFormBean.POLICY_NO,appFormServ).then(
                                    function(reflectiveQuesArr){
                                        console.log("reflectiveQuesArr : " + JSON.stringify(reflectiveQuesArr));
                                        if(!!reflectiveQuesArr && reflectiveQuesArr.length>0){
                                            LoadReflService.ReflectiveQuesList = reflectiveQuesArr;
                                            LoadReflService.getStateName(reflectiveQuesArr).then(
                                                function(stateName){
                                                    debug("stateName : " + stateName);
                                                    if(!!stateName){
                                                        $state.go(stateName,{"LoadData" : reflectiveQuesArr[0]});
                                                    }else{
                                                        CommonService.hideLoading();
                                                        debug("Reflective State Not Found ");
                                                    }
                                                }
                                            )
                                        }else{
                                                var whereClauseObj1 = {};
                                                whereClauseObj1.APPLICATION_ID = appFormServ.applicationFormBean.APPLICATION_ID;
                                                whereClauseObj1.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                                                CommonService.updateRecords(db,"LP_APPLICATION_MAIN",{"IS_SCREEN10_COMPLETED":'NA'},whereClauseObj1).then(
                                                    function(res){
                                                        console.log("Screen updated successfully :IS_SCREEN10_COMPLETED");
                                                    }
                                                );
                                            debug("No Ref Ques");
                                            CommonService.hideLoading();
                                            navigator.notification.alert("No questionnaire to fill",null,"Application","OK");
                                        }
                                    }
                                );
                            }
                        )
                        break;
                    case 'genOutput':
                            var data = FlagList;
                            var validateScreen = false;
                            if(!!data)
                               validateScreen = data.IS_SCREEN1_COMPLETED == 'Y' && data.IS_SCREEN2_COMPLETED == 'Y' && data.IS_SCREEN3_COMPLETED == 'Y' && data.IS_SCREEN4_COMPLETED == 'Y' && data.IS_SCREEN5_COMPLETED == 'Y' && (data.IS_SCREEN6_COMPLETED == 'Y' || data.IS_SCREEN6_COMPLETED == 'NA') && data.IS_SCREEN7_COMPLETED == 'Y' && data.IS_SCREEN8_COMPLETED == 'Y' && (data.IS_SCREEN9_COMPLETED == 'Y' || data.IS_SCREEN9_COMPLETED == 'NA') && (data.IS_SCREEN10_COMPLETED == 'Y' || data.IS_SCREEN10_COMPLETED == 'NA');

                             //console.log("validateScreen :"+validateScreen+"\n in genOutput :"+JSON.stringify(data));
                            if(validateScreen)
                            {
                                GenerateOutputService.generateApplicationOutput(appFormServ);
                            }
                            else{
                                 navigator.notification.alert("Please complete all mandatory screens !!",null,"Application","OK");
                                 CommonService.hideLoading();
                                }
                        break;
                     case 'genOutputTerm':
                        var data = FlagList;
                        var validateScreen = false;
                        if(!!data)
                           validateScreen = data.IS_SCREEN1_COMPLETED == 'Y' && data.IS_SCREEN2_COMPLETED == 'Y' && data.IS_SCREEN3_COMPLETED == 'Y' && data.IS_SCREEN4_COMPLETED == 'Y' && data.IS_SCREEN5_COMPLETED == 'Y' && (data.IS_SCREEN6_COMPLETED == 'Y' || data.IS_SCREEN6_COMPLETED == 'NA') && data.IS_SCREEN7_COMPLETED == 'Y' && data.IS_SCREEN8_COMPLETED == 'Y' && (data.IS_SCREEN9_COMPLETED == 'Y' || data.IS_SCREEN9_COMPLETED == 'NA') && (data.IS_SCREEN10_COMPLETED == 'Y' || data.IS_SCREEN10_COMPLETED == 'NA');

                         //console.log("validateScreen :"+validateScreen+"\n in genOutput :"+JSON.stringify(data));
                        if(validateScreen)
                        {
                            //GenerateTermOutputService.generateTermApplicationOutput(appFormServ);
                        }
                        else
                        {
                             navigator.notification.alert("Please complete all mandatory screens !!",null,"Application","OK");
                             CommonService.hideLoading();
                        }
                    break;
                    default:
                        $state.go('applicationForm.personalInfo.personalDetails',{"APPLICATION_ID": params.APPLICATION_ID, "AGENT_CD": params.AGENT_CD, "SIS_ID":params.SIS_ID});
        }
        });
    };

    this.continueToApplication = function(params){

        console.log(JSON.stringify(params)+"gotoApplication"+params.APPLICATION_ID+"params.REF_APPLICATION_ID"+params.REF_APPLICATION_ID);
        if(params.APPLICATION_ID == undefined || params.APPLICATION_ID == ""){
            appFormServ.applicationFormBean.APPLICATION_ID = CommonService.getRandomNumber();
            appFormServ.applicationFormBean.OPP_ID = params.OPP_ID;
            appFormServ.applicationFormBean.FHR_ID = params.FHR_ID;
            appFormServ.applicationFormBean.LEAD_ID = params.LEAD_ID;
            appFormServ.ComboFormBean.COMBO_ID = params.COMBO_ID;
            appFormServ.ComboFormBean.REF_SIS_ID = params.REF_SIS_ID;
            appFormServ.ComboFormBean.REF_FHRID = params.REF_FHRID;
            appFormServ.ComboFormBean.REF_OPP_ID = params.REF_OPP_ID; // Optional
            appFormServ.ComboFormBean.REF_POLICY_NO = params.REF_POLICY_NO;
            appFormServ.APP_OUTPUT = null;
            console.log("NEW APPLICATION_ID::"+appFormServ.applicationFormBean.APPLICATION_ID);
        }
        else{
            appFormServ.applicationFormBean.APPLICATION_ID = params.APPLICATION_ID;
            appFormServ.applicationFormBean.OPP_ID = params.OPP_ID;
            appFormServ.applicationFormBean.FHR_ID = params.FHR_ID;
            appFormServ.applicationFormBean.POLICY_NO = params.POLICY_NO;
            appFormServ.applicationFormBean.LEAD_ID = params.LEAD_ID;
            appFormServ.ComboFormBean.COMBO_ID = params.COMBO_ID;
            appFormServ.ComboFormBean.REF_SIS_ID = params.REF_SIS_ID;
            appFormServ.ComboFormBean.REF_FHRID = params.REF_FHRID;
            appFormServ.ComboFormBean.REF_APPLICATION_ID = params.REF_APPLICATION_ID;
            appFormServ.ComboFormBean.REF_OPP_ID = params.REF_OPP_ID; // Optional
            appFormServ.ComboFormBean.REF_POLICY_NO = params.REF_POLICY_NO;
            appFormServ.APP_OUTPUT = null;
            console.log("Existing APPLICATION_ID::"+appFormServ.applicationFormBean.APPLICATION_ID);
        }
        appFormServ.activeTab = MAIN_TAB;
        SUB_TAB = 'personalDetails';
        $state.go('applicationForm.personalInfo.personalDetails',{"LEAD_ID":params.LEAD_ID, "OPP_ID":params.OPP_ID,"APPLICATION_ID":params.APPLICATION_ID, "AGENT_CD": params.AGENT_CD, "SIS_ID":params.SIS_ID, "FHR_ID":params.FHR_ID, "POLICY_NO":params.POLICY_NO});
    };
}]);

AppFormModule.service('LoadAppProofList',['$q','CommonService', function($q, CommonService){
    var lpls = this;
    this.ReflectiveQuesList = [];
this.getNationalityList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
        		whereClauseObj.NATIONALITY_ALLOWED = 'Y';
        		whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
			function(res){
				var nationalityList = [];
				console.log("nationalityList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					nationalityList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null, "FATF_FLAG": null, "MPDOC_CODE": null, "MPPROOF_CODE": null});
					for(var i=0;i<res.rows.length;i++){
						var nationality = {};
						nationality.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						nationality.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
						nationality.FATF_FLAG = res.rows.item(i).FATF_FLAG;
						nationality.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						nationality.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						nationalityList.push(nationality);
					}
					dfd.resolve(nationalityList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
	};

this.getResidentCountryList = function(){
	var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
			function(res){
				var AppResidentCountryList = [];
				console.log("resiCountry :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppResidentCountryList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null,"NATIONALITY_ALLOWED": null, "FATF_FLAG": null, "MPDOC_CODE": null, "MPPROOF_CODE": ""});
					for(var i=0;i<res.rows.length;i++){
						var resident = {};
						resident.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						resident.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
						resident.NATIONALITY_ALLOWED = res.rows.item(i).NATIONALITY_ALLOWED;
						resident.FATF_FLAG = res.rows.item(i).FATF_FLAG;
						resident.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						resident.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						AppResidentCountryList.push(resident);
					}
					dfd.resolve(AppResidentCountryList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
};

this.getBirthCountryList = function(){
	var dfd = $q.defer();
	var whereClauseObj = {};
	whereClauseObj.ISACTIVE = 'Y';
	CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
		function(res){
			var AppBirthCountryList = [];
			console.log("birthCountry :" + res.rows.length);
			if(!!res && res.rows.length>0){
				AppBirthCountryList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null,"NATIONALITY_ALLOWED": null, "FATF_FLAG": null, "MPDOC_CODE": null, "MPPROOF_CODE": ""});
				for(var i=0;i<res.rows.length;i++){
					var birthCountry = {};
					birthCountry.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
					birthCountry.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
					birthCountry.NATIONALITY_ALLOWED = res.rows.item(i).NATIONALITY_ALLOWED;
					birthCountry.FATF_FLAG = res.rows.item(i).FATF_FLAG;
					birthCountry.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
					birthCountry.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
					AppBirthCountryList.push(birthCountry);
				}
				dfd.resolve(AppBirthCountryList);
			}
			else
				dfd.resolve(null);
		}
	);
	return dfd.promise;
};

this.getEduQualList = function(){
var dfd = $q.defer();
		var whereClauseObj = {};
			whereClauseObj.LOOKUP_TYPE = 'Education';
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var AppEduQualList = [];
				console.log("EduQualiftn :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppEduQualList.push({"LOOKUP_TYPE_DESC": "Select ", "LOOKUP_TYPE": null,"NBFE_CODE":null, "ISACTIVE": null});
					for(var i=0;i<res.rows.length;i++){
						var eduQual = {};
						eduQual.LOOKUP_TYPE = res.rows.item(i).LOOKUP_TYPE;
                        eduQual.NBFE_CODE = res.rows.item(i).NBFE_CODE;
                        eduQual.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
                        eduQual.ISACTIVE = res.rows.item(i).ISACTIVE;
						AppEduQualList.push(eduQual);
					}
					dfd.resolve(AppEduQualList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

this.getStateList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_STATE_MASTER",false,"*",whereClauseObj, "STATE_DESC ASC").then(
			function(res){
				var AppStateList = [];
				console.log("StateList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppStateList.push({"STATE_DESC": "Select ", "STATE_CODE":null, "COUNTRY_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var stateList = {};
						stateList.STATE_DESC = res.rows.item(i).STATE_DESC;
                        stateList.STATE_CODE = res.rows.item(i).STATE_CODE;
                        stateList.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						AppStateList.push(stateList);
					}
					dfd.resolve(AppStateList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

this.getCityList = function(stateCode){

var dfd = $q.defer();

console.log("inside getCityList"+stateCode+"----")
CommonService.transaction(db,
						function(tx){
						console.log("inside getCityList 2")
							CommonService.executeSql(tx,"SELECT * FROM LP_CITY_MASTER WHERE STATE_CODE IN (?,?) AND ISACTIVE=? ORDER BY CITY_DESC ASC",[stateCode,'0','Y'],
								function(tx,res){
										var AppCityList = [];
										console.log("CityList :" + res.rows.item(0).CITY_DESC);
										if(!!res && res.rows.length>0){
											AppCityList.push({"CITY_DESC": "Select ", "CITY_CODE":null, "STATE_CODE":null, "TOP_CITY_FLAG":null});
											for(var i=0;i<res.rows.length;i++){
												var cityList = {};
												cityList.CITY_DESC = res.rows.item(i).CITY_DESC;
												cityList.CITY_CODE = res.rows.item(i).CITY_CODE;
												cityList.STATE_CODE = res.rows.item(i).STATE_CODE;
												cityList.TOP_CITY_FLAG = res.rows.item(i).TOP_CITY_FLAG;
												AppCityList.push(cityList);
											}
											dfd.resolve(AppCityList);
										}
										else
											dfd.resolve(null);
									},
								function(tx,err){
									dfd.resolve(null);
								}
							);
						},
						function(err){
							dfd.resolve(null);
						},null
					);
		return dfd.promise;
};

this.getOccClassList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_OCCUPATION_CLASS",false,"*",whereClauseObj, "OCCU_CLASS ASC").then(
			function(res){
				var AppOccClassList = [];
				console.log("OccList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppOccClassList.push({"OCCU_CLASS": "Select ", "NBFE_CODE":null, "MPDOC_CODE":null,"OCCU_CLASS_PRINT":null,"MPPROOF_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var occClassList = {};
						    occClassList.OCCU_CLASS = res.rows.item(i).OCCU_CLASS;
							occClassList.OCCU_CLASS_PRINT = res.rows.item(i).OCCU_CLASS_PRINT;
							occClassList.NBFE_CODE = res.rows.item(i).NBFE_CODE;
							occClassList.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
							occClassList.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						AppOccClassList.push(occClassList);
					}
					dfd.resolve(AppOccClassList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

this.getCompanyNameList = function(){
    var dfd = $q.defer();
    CommonService.selectRecords(db,"LP_APP_COMPANY_MASTER",false,"*",null, "COMPANY_NAME ASC").then(
        function(res){
            var AppCompanyNameList = [];
            console.log("AppCompanyNameList :" + res.rows.length);
            if(!!res && res.rows.length>0){
                AppCompanyNameList.push({"COMPANY_NAME": "Select ", "COMPANY_CODE":null});
                for(var i=0;i<res.rows.length;i++){
                    var companyName = {};
                    companyName.COMPANY_NAME = res.rows.item(i).COMPANY_NAME;
                    companyName.COMPANY_CODE = res.rows.item(i).COMPANY_CODE;
                    AppCompanyNameList.push(companyName);
                }
                dfd.resolve(AppCompanyNameList);
            }
            else
                dfd.resolve(null);
        }
    );
	return dfd.promise;
};

this.getOrgTypeList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_OCCUPATION_GROUP",true,"*",whereClauseObj, "OCCU_GROUP ASC").then(
			function(res){
				var AppOrgTypeList = [];
				console.log("getOrgTypeList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppOrgTypeList.push({"OCCU_GROUP": "Select Type", "NBFE_CODE":null, "MPDOC_CODE":null,"OCCU_GROUP_PRINT":null,"MPPROOF_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var orgType = {};
						  orgType.OCCU_GROUP = res.rows.item(i).OCCU_GROUP;
                          orgType.OCCU_GROUP_PRINT = res.rows.item(i).OCCU_GROUP_PRINT;
                          orgType.NBFE_CODE = res.rows.item(i).NBFE_CODE;
                          orgType.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                          orgType.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						AppOrgTypeList.push(orgType);
					}
					dfd.resolve(AppOrgTypeList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

/*this.getOccIndustryList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_SIS_OCCUPATION",true,"APP_INDUSTRIES",whereClauseObj, "APP_INDUSTRIES ASC").then(
			function(res){
				var AppOccIndustryList = [];
				console.log("getOccIndustryList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppOccIndustryList.push({"APP_INDUSTRIES": "Select "});
					for(var i=0;i<res.rows.length;i++){
						var occIndustry = {};
						  occIndustry.APP_INDUSTRIES = res.rows.item(i).APP_INDUSTRIES;
						AppOccIndustryList.push(occIndustry);
					}
					dfd.resolve(AppOccIndustryList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};
*/

this.getOccIndustryList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_NATURE_OF_WORK",true,"APP_INDUSTRY",whereClauseObj, "APP_INDUSTRY ASC").then(
			function(res){
				var AppOccIndustryList = [];
				console.log("getOccIndustryList :" + res.rows.length);
				if(!!res && res.rows.length>0){
				console.log("OccupationInsudtry Result is ::"+JSON.stringify(res.rows.item(0)));
					AppOccIndustryList.push({"APP_INDUSTRY": "Select Industry","APP_SNO":0,"APP_NATURE_OF_WORK":null,"OCCUPATION_CODE":null,"MPDOC_CODE":null,"MPPROOF_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var occIndustry = {};
						  occIndustry.APP_SNO = res.rows.item(i).APP_SNO;
						  occIndustry.APP_INDUSTRY = res.rows.item(i).APP_INDUSTRY;
						  occIndustry.APP_NATURE_OF_WORK = res.rows.item(i).APP_NATURE_OF_WORK;
						  occIndustry.OCCUPATION_CODE = res.rows.item(i).OCCUPATION_CODE;
						  occIndustry.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						  occIndustry.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						  occIndustry.APP_PROMPT = res.rows.item(i).APP_PROMPT;
						AppOccIndustryList.push(occIndustry);
					}
					dfd.resolve(AppOccIndustryList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

/*this.getOccNatureList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_OCCUPATION_NATURE",true,"*",whereClauseObj, "OCCU_NATURE ASC").then(
			function(res){
				var AppOccNatureList = [];
				console.log("getOccNatureList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppOccNatureList.push({"OCCU_NATURE":"Select ","NBFE_CODE":null,"MPDOC_CODE":null,"MPPROOF_CODE":null,"OCCU_NATURE_PRINT":null});
					for(var i=0;i<res.rows.length;i++){
						var occNature = {};
						occNature.OCCU_NATURE = res.rows.item(i).OCCU_NATURE;
						occNature.OCCU_NATURE_PRINT = res.rows.item(i).OCCU_NATURE_PRINT;
						occNature.NBFE_CODE = res.rows.item(i).NBFE_CODE;
						occNature.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						occNature.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						AppOccNatureList.push(occNature);
					}
					dfd.resolve(AppOccNatureList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};*/

this.getOccNatureList = function(APP_INDUSTRY){
console.log("APP_INDUSTRY::"+APP_INDUSTRY);
var dfd = $q.defer();
var s1 = '';

    if(APP_INDUSTRY!=undefined && APP_INDUSTRY!='Select Industry')
        s1 = "AND APP_INDUSTRY ='"+APP_INDUSTRY+"'";
    /*if(CUST_TYPE != undefined)
        s2 = "AND CUST_TYPE like '"+CUST_TYPE+"'";*/

var sql = "select distinct APP_SNO, APP_NATURE_OF_WORK, OCCUPATION_CODE, MPDOC_CODE, MPPROOF_CODE, TERM_FLAG from LP_APP_NATURE_OF_WORK where ISACTIVE = 'Y'"+s1+" order by APP_NATURE_OF_WORK ASC";

		CommonService.transaction(db,
        		function(tx){
        		    CommonService.executeSql(tx,sql,[],
        				function(tx,res){
        				console.log("inside function tx getOccNatureList::"+s1);
                            var AppNatureOfWork = [];
                            console.log("getOccNatureList :" + res.rows.length);
                            if(!!res && res.rows.length>0){
                                    console.log("record LP_APP_NATURE_OF_WORK :"+JSON.stringify(res.rows.item(0)));
                                AppNatureOfWork.push({"APP_SNO":0,"APP_NATURE_OF_WORK":"Select Nature Of Work","OCCUPATION_CODE":null,"MPDOC_CODE":null, "MPPROOF_CODE":null,"FORMID":null,"APP_PROMPT":null});
                                for(var i=0;i<res.rows.length;i++){
                                    var NatureWork = {};
                                    NatureWork.APP_SNO = res.rows.item(i).APP_SNO;
                                    NatureWork.APP_NATURE_OF_WORK = res.rows.item(i).APP_NATURE_OF_WORK;
                                    NatureWork.OCCUPATION_CODE = res.rows.item(i).OCCUPATION_CODE;
                                    NatureWork.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                                    NatureWork.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                    NatureWork.FORMID = res.rows.item(i).FORMID;
                                    NatureWork.APP_PROMPT = res.rows.item(i).APP_PROMPT;
                                    NatureWork.OCCU_TERM_FLAG = res.rows.item(i).TERM_FLAG;
                                    AppNatureOfWork.push(NatureWork);
                                }
                                dfd.resolve(AppNatureOfWork);
                                //console.log("AppNatureOfWork is::"+JSON.stringify(AppNatureOfWork));
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err)
                        {
                            dfd.resolve(null);
                        }
        		    );
        		},
                function(err){
                dfd.resolve(null);
                });


		return dfd.promise;

};

//Added for Quessionarrie
this.getOccuQuesionnarie = function(APP_SNO){
var dfd = $q.defer();
var s1 = "";
    if(APP_SNO!= undefined && APP_SNO!=null)
        s1 = "AND APP_SNO = '"+APP_SNO+"'";

    var sql = "Select * from LP_APP_OCCU_QUESTIONNAIRE where ISACTIVE = 'Y' "+s1+" order by QUESTIONNAIRE_DESC asc";

    CommonService.transaction(db,
        function(tx){
            CommonService.executeSql(tx,sql,[],
                function(tx,res){
                var OccuQuesionnarieList =[];
                console.log("res length is::"+res.rows.length);
                    if(!!res && res.rows.length > 0){
                        OccuQuesionnarieList.push({"SNO":0,"APP_SNO":0,"MPDOC_CODE":null,"MPPROOF_CODE":null,"FORM_ID":null,"DIGITIZED_FLAG":null,"QUESTIONNAIRE_DESC":"Select Occupation Quesionnaire"});
                        for (var i=0; i < res.rows.length; i++){
                            var OccuQues = {};
                            OccuQues.SNO = res.rows.item(i).SNO;
                            OccuQues.APP_SNO = res.rows.item(i).APP_SNO;
                            OccuQues.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                            OccuQues.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                            OccuQues.FORM_ID = res.rows.item(i).FORM_ID;
                            OccuQues.DIGITIZED_FLAG = res.rows.item(i).DIGITIZED_FLAG;
                            OccuQues.QUESTIONNAIRE_DESC = res.rows.item(i).QUESTIONNAIRE_DESC;
                            OccuQuesionnarieList.push(OccuQues);
                        }
                        dfd.resolve(OccuQuesionnarieList);
                        //console.log("OccuQuesionnarieList is::"+JSON.stringify(OccuQuesionnarieList));
                    }
                    else
                        dfd.resolve(null);
                },
                function(err)
                {
                    dfd.resolve(null);
                }
            );
        },
        function(err)
        {
            dfd.resolve(null);
        }
    );

    return dfd.promise;
}


this.getDesignationList = function(NBFE_CODE){
console.log("Inside getDesignationList"+NBFE_CODE);
var dfd = $q.defer();
var s1 = '';

    if(NBFE_CODE!=undefined)
        s1 = " AND NBFE_CODE = '"+NBFE_CODE+"'";
    /*if(CUST_TYPE != undefined)
        s2 = "AND CUST_TYPE like '"+CUST_TYPE+"'";*/

var sql = "select * from LP_APP_DESIGNATION_LOOKUP where ISACTIVE = 'Y'"+s1+"";

		CommonService.transaction(db,
        		function(tx){
        		    CommonService.executeSql(tx,sql,[],
        				function(tx,res){
        				console.log("inside function tx getdesignationList::"+s1);
                            var AppDesignationList = [];
                            console.log("getdesignationList :" + res.rows.length);
                            if(!!res && res.rows.length>0){
                                    console.log("record LP_APP_DESIGNATION_LOOKUP :"+JSON.stringify(res.rows.item(0)));
                                AppDesignationList.push({"DESIGNATION":"Select Designation","APP_SNO":0,"MPDOC_CODE":null, "MPPROOF_CODE":null});
                                for(var i=0;i<res.rows.length;i++){
                                    var designation = {};
                                    designation.APP_SNO = res.rows.item(0).APP_SNO;
                                    designation.DESIGNATION = res.rows.item(i).DESIGNATION;
                                    designation.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                                    designation.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                    designation.NBFE_CODE = res.rows.item(i).NBFE_CODE;
                                    designation.OCCUPATION_CLASS = res.rows.item(i).OCCUPATION_CLASS_PRINT;
                                    AppDesignationList.push(designation);
                                }
                                dfd.resolve(AppDesignationList);
                                console.log("AppDesignationList is::"+JSON.stringify(AppDesignationList));
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err)
                        {
                            dfd.resolve(null);
                        }
        		    );
        		},
                function(err){
                dfd.resolve(null);
                });

		return dfd.promise;
}

this.getIncomeProofList = function(CUST_TYPE,STANDARD_FLAG,OCCU_CLASS, TERM_FLAG){
console.log("Occupation Class::"+OCCU_CLASS);
debug("TERM_FLAG:: " + TERM_FLAG);
var dfd = $q.defer();
var s1 = '';
var s2 = '';
var s3 = '';
var s4 = '';
    if(CUST_TYPE!=undefined)
        s1 = " AND CUSTOMER_CATEGORY = '"+CUST_TYPE+"'";
    if(STANDARD_FLAG!=undefined && STANDARD_FLAG == true)
        s2 = " AND STANDARD_FLAG = 'Y'";
    if(OCCU_CLASS!=undefined && OCCU_CLASS!="Select ")
        s3 = " AND OCCUPATION_CLASS like '%"+OCCU_CLASS+"%'";
    if(!!TERM_FLAG && TERM_FLAG=='Y')
        s4 = " AND TERM_FLAG = '" + TERM_FLAG + "'";

        var sql = "select * from LP_DOC_PROOF_MASTER where ISACTIVE = '1' and MPDOC_CODE = 'FP'" + s1 + s2 + s3 + s4 +"";
        debug("sql : " + sql);
		CommonService.transaction(db,
        		function(tx){
        		    CommonService.executeSql(tx,sql,[],
        				function(tx,res){
        				console.log("inside function tx::"+ s1 + s2 + s3 + s4);
                            var AppIncomeProofList = [];
                            console.log("getincomeProofList :" + res.rows.length);
                            if(!!res && res.rows.length>0){
                                    console.log("record LP_DOC_PROOF_MASTER :"+JSON.stringify(res.rows.item(0)));
                                AppIncomeProofList.push({"MPPROOF_DESC":"Select ","DOC_ID":null, "CUSTOMER_CATEGORY":"INPR"});
                                for(var i=0;i<res.rows.length;i++){
                                    var incomeProof = {};
                                    incomeProof.DOC_ID = res.rows.item(i).DOC_ID;
                                    incomeProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
                                    incomeProof.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                                    incomeProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                    incomeProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
                                    incomeProof.FORM_ID = res.rows.item(i).FORM_ID;
                                    incomeProof.NBFE_PROOF_CODE = res.rows.item(i).NBFE_PROOF_CODE;
                                    incomeProof.STANDARD_FLAG = res.rows.item(i).STANDARD_FLAG;
                                    incomeProof.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
                                    incomeProof.OCCUPATION_CLASS = res.rows.item(i).OCCUPATION_CLASS;
                                    AppIncomeProofList.push(incomeProof);
                                }
                                dfd.resolve(AppIncomeProofList);
                                console.log("AppIncomeProofList is::"+JSON.stringify(AppIncomeProofList));
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err)
                        {
                            dfd.resolve(null);
                        }
        		    );
        		},
                function(err){
                dfd.resolve(null);
                });

		return dfd.promise;

};
this.getIncomeSourceProofList = function(){
	var dfd = $q.defer();
	var sql = "select * from LP_DOC_PROOF_MASTER where ISACTIVE = '1' and MPDOC_CODE = 'AM'";
	debug("getIncomeSourceProofList sql : " + sql);
	CommonService.transaction(db,
		function(tx){
			CommonService.executeSql(tx,sql,[],
				function(tx,res){
					var AppIncomeSourceProofList = [];
					debug("getIncomeSourceProofList :" + res.rows.length);
					if(!!res && res.rows.length>0){
						AppIncomeSourceProofList.push({"MPPROOF_DESC":"Select ","DOC_ID":null, "CUSTOMER_CATEGORY":"INPR"});
						for(var i=0;i<res.rows.length;i++){
							var incomeSourceProof = {};
							incomeSourceProof.DOC_ID = res.rows.item(i).DOC_ID;
							incomeSourceProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
							incomeSourceProof.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
							incomeSourceProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
							incomeSourceProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
							incomeSourceProof.FORM_ID = res.rows.item(i).FORM_ID;
							incomeSourceProof.NBFE_PROOF_CODE = res.rows.item(i).NBFE_PROOF_CODE;
							incomeSourceProof.STANDARD_FLAG = res.rows.item(i).STANDARD_FLAG;
							incomeSourceProof.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							incomeSourceProof.OCCUPATION_CLASS = res.rows.item(i).OCCUPATION_CLASS;
							AppIncomeSourceProofList.push(incomeSourceProof);
						}
						dfd.resolve(AppIncomeSourceProofList);
					}
					else
						dfd.resolve(null);
				},
				function(tx,err){
					dfd.resolve(null);
				}
			);
		},
		function(err){
			dfd.resolve(null);
		});
	return dfd.promise;
};

this.getFatcaDocID = function(){
    var dfd = $q.defer();
    var FatcaDocList = {
        Insured:{},
        Proposer:{}
    };
    var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = "NQ";
	whereClauseObj.MPPROOF_CODE = "FI";

    CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("LP_DOC_PROOF_MASTER FATCA :" + res.rows.length);
            if(!!res && res.rows.length>0){
                for(var i=0;i<res.rows.length;i++){
                    if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
                        {
                            FatcaDocList.Insured.fatca = res.rows.item(i).DOC_ID;
                            FatcaDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
                        }
                    else
                        if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
                        {
                            FatcaDocList.Proposer.fatca = res.rows.item(i).DOC_ID;
                            FatcaDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
                        }
                }
                dfd.resolve(FatcaDocList);
            }
            else
                dfd.resolve(FatcaDocList);
        }
    );

    return dfd.promise;
}

this.getFatcaFileData = function(fatcaObj, APP_ID, AGENT_CD, CUST_TYPE){
var dfd = $q.defer();
var Filename = '';
if(CUST_TYPE == 'C01')
    Filename = AGENT_CD+"_"+APP_ID+"_"+fatcaObj.Insured.fatca;
if(CUST_TYPE == 'C02')
    Filename = AGENT_CD+"_"+APP_ID+"_"+fatcaObj.Proposer.fatca;


debug(Filename+"fatca"+JSON.stringify(fatcaObj));
	CommonService.readFile(Filename+ ".html", "/FromClient/APP/HTML/").then(
    			function(res){
    				if(!!res){

							debug("FATCA HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					debug("FATCA file not found");
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;
}

this.getIDProofList = function(CUST_TYPE,STANDARD_FLAG){

var dfd = $q.defer();
console.log("inside IDProofList");
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = '1';
        	whereClauseObj.MPDOC_CODE = 'IP';
        	if(CUST_TYPE!=undefined)
        		whereClauseObj.CUSTOMER_CATEGORY = CUST_TYPE;
			if(STANDARD_FLAG!=undefined)
				whereClauseObj.STANDARD_FLAG = STANDARD_FLAG;

		CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "MPPROOF_DESC ASC").then(
			function(res){
				var IDProofList = [];
				console.log("getIDProofList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					IDProofList.push({"MPPROOF_DESC":"Select ID Proof","DOC_ID":null, "CUSTOMER_CATEGORY" : "INPR"});
					for(var i=0;i<res.rows.length;i++){
						var idProof = {};
						idProof.DOC_ID = res.rows.item(i).DOC_ID;
						idProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
						idProof.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						idProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						idProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
						idProof.FORM_ID = res.rows.item(i).FORM_ID;
						idProof.NBFE_PROOF_CODE = res.rows.item(i).NBFE_PROOF_CODE;
						idProof.STANDARD_FLAG = res.rows.item(i).STANDARD_FLAG;
						idProof.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
						IDProofList.push(idProof);
					}
					dfd.resolve(IDProofList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

this.getAddrProofList = function(CUST_TYPE,ADDRTYPE){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = '1';
        	if(ADDRTYPE!=undefined && ADDRTYPE!='')
        		whereClauseObj.MPDOC_CODE = ADDRTYPE;
        	else
        		whereClauseObj.MPDOC_CODE = 'RP';
        	if(CUST_TYPE)
        		whereClauseObj.CUSTOMER_CATEGORY = CUST_TYPE;

		CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "MPPROOF_DESC ASC").then(
			function(res){
				var AppAddrProofList = [];
				console.log(JSON.stringify(whereClauseObj)+" getAddrProofList:" + res.rows.length);
				if(!!res && res.rows.length>0){
					AppAddrProofList.push({"MPPROOF_DESC":"Select ","DOC_ID":null,"CUSTOMER_CATEGORY":"INPR"});
					for(var i=0;i<res.rows.length;i++){
						var addrProof = {};
						addrProof.DOC_ID = res.rows.item(i).DOC_ID;
						addrProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
						addrProof.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						addrProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						addrProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
						addrProof.FORM_ID = res.rows.item(i).FORM_ID;
						addrProof.NBFE_PROOF_CODE = res.rows.item(i).NBFE_PROOF_CODE;
						addrProof.STANDARD_FLAG = res.rows.item(i).STANDARD_FLAG;
						addrProof.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
						AppAddrProofList.push(addrProof);
					}
					dfd.resolve(AppAddrProofList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

};

this.getRelDescList = function(isSelf){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_PROPOSER_RELATION",false,"*",whereClauseObj, "RELATIONSHIP_DESC ASC").then(
			function(res){
				var RelDescList = [];
				console.log("AppRelDescList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					RelDescList.push({"RELATIONSHIP_DESC":"Select ","NBFE_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var relDesc = {};
						if(isSelf){
							if(res.rows.item(i).NBFE_CODE == '20'){
								relDesc.RELATIONSHIP_DESC = res.rows.item(i).RELATIONSHIP_DESC;
								relDesc.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								RelDescList.push(relDesc);
							}
						}
						else
						{
							if(res.rows.item(i).NBFE_CODE != '20'){
								relDesc.RELATIONSHIP_DESC = res.rows.item(i).RELATIONSHIP_DESC;
								relDesc.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								RelDescList.push(relDesc);
							}
						}
					}
					dfd.resolve(RelDescList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;


};

this.getDrinkTypeList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';
        	whereClauseObj.LOOKUP_TYPE = 'DrinkCons';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var DrinkTypeList = [];
				console.log("DrinkTypeList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					DrinkTypeList.push({"LOOKUP_TYPE_DESC":"Select ","DISPLAY":"Select ","NBFE_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var drink = {};
								drink.DISPLAY = CommonService.toTitleCase(res.rows.item(i).LOOKUP_TYPE_DESC);
								drink.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
								drink.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								DrinkTypeList.push(drink);

					}
					dfd.resolve(DrinkTypeList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
};

this.getSmokeTypeList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';
        	whereClauseObj.LOOKUP_TYPE = 'SmokCons';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var SmokeTypeList = [];
				console.log("SmokeTypeList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					SmokeTypeList.push({"LOOKUP_TYPE_DESC":"Select ","DISPLAY":"Select ","NBFE_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var smoke = {};
								smoke.DISPLAY = CommonService.toTitleCase(res.rows.item(i).LOOKUP_TYPE_DESC);
								smoke.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
								smoke.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								SmokeTypeList.push(smoke);

					}
					dfd.resolve(SmokeTypeList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
};

this.getFrequencyList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';
        	whereClauseObj.LOOKUP_TYPE = 'Frequency';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var FrequencyList = [];
				console.log("FrequencyList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					FrequencyList.push({"LOOKUP_TYPE_DESC":"Select ","DISPLAY":"Select ","NBFE_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var freq = {};
								freq.DISPLAY = CommonService.toTitleCase(res.rows.item(i).LOOKUP_TYPE_DESC);
								freq.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
								freq.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								FrequencyList.push(freq);

					}
					dfd.resolve(FrequencyList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
};

//NOT USED
this.getQuantityList = function(){

var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = 'Y';
        	whereClauseObj.LOOKUP_TYPE = 'Quantity';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var QuantityList = [];
				console.log("FrequencyList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					QuantityList.push({"LOOKUP_TYPE_DESC":"Select ","DISPLAY":"Select ","NBFE_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var quant = {};
								quant.DISPLAY = CommonService.toTitleCase(res.rows.item(i).LOOKUP_TYPE_DESC);
								quant.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
								quant.NBFE_CODE = res.rows.item(i).NBFE_CODE;
								QuantityList.push(quant);

					}
					dfd.resolve(QuantityList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
};

this.getDefenceDocId = function(){
var dfd = $q.defer();
var DefenceDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'OQ';
	whereClauseObj.MPPROOF_CODE = 'DF';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER DefenceDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								DefenceDocList.Insured.defence = res.rows.item(i).DOC_ID;
								DefenceDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
								{
									DefenceDocList.Proposer.defence = res.rows.item(i).DOC_ID;
									DefenceDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
								}
					}
					dfd.resolve(DefenceDocList);
				}
				else
					dfd.resolve(DefenceDocList);
			});

return dfd.promise;
}

this.getOccupQuestDocId = function(){
var dfd = $q.defer();
var OccupDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'OQ';
	whereClauseObj.MPPROOF_CODE = 'OC';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER OccupDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								OccupDocList.Insured.occup = res.rows.item(i).DOC_ID;
								OccupDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
								{
									OccupDocList.Proposer.occup = res.rows.item(i).DOC_ID;
									OccupDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
								}
					}
					dfd.resolve(OccupDocList);
				}
				else
					dfd.resolve(OccupDocList);
			});

return dfd.promise;
}

this.getSubOccDocId = function(){
var dfd = $q.defer();
var SubOccDocList = {
Insured:{
subOcc :{}
},
Proposer:{
subOcc :{}
}
};
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'HQ';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER SubOccDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								SubOccDocList.Insured.subOcc[res.rows.item(i).MPPROOF_CODE] = {};
								SubOccDocList.Insured.subOcc[res.rows.item(i).MPPROOF_CODE].DOC_ID = res.rows.item(i).DOC_ID;
								SubOccDocList.Insured.subOcc[res.rows.item(i).MPPROOF_CODE].MSG = res.rows.item(i).MPPROOF_DESC;
								SubOccDocList.Insured.subOcc[res.rows.item(i).MPPROOF_CODE].isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								SubOccDocList.Proposer.subOcc[res.rows.item(i).MPPROOF_CODE] = {};
								SubOccDocList.Proposer.subOcc[res.rows.item(i).MPPROOF_CODE].DOC_ID = res.rows.item(i).DOC_ID;
								SubOccDocList.Proposer.subOcc[res.rows.item(i).MPPROOF_CODE].MSG = res.rows.item(i).MPPROOF_DESC;
								SubOccDocList.Proposer.subOcc[res.rows.item(i).MPPROOF_CODE].isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(SubOccDocList);
				}
				else
					dfd.resolve(SubOccDocList);
			});

return dfd.promise;
}

this.getTravelDocId = function(){
var dfd = $q.defer();
var TravelDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'TQ';
	whereClauseObj.MPPROOF_CODE = 'TV';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER TravelDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								TravelDocList.Insured.travel = res.rows.item(i).DOC_ID;
								TravelDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								TravelDocList.Proposer.travel = res.rows.item(i).DOC_ID;
								TravelDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(TravelDocList);
				}
				else
					dfd.resolve(TravelDocList);
			});

return dfd.promise;
}


this.getForm60DocID = function(){
var dfd = $q.defer();
var Form60DocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = 'CD';
	whereClauseObj.MPPROOF_CODE = 'FS';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER QUEST :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								Form60DocList.Insured.form60 = res.rows.item(i).DOC_ID;
								Form60DocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								Form60DocList.Proposer.form60 = res.rows.item(i).DOC_ID;
								Form60DocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(Form60DocList);
				}
				else
					dfd.resolve(Form60DocList);
			});

return dfd.promise;
}

this.getNRIDocID = function(){
var dfd = $q.defer();
var NRIDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = 'NQ';
	whereClauseObj.MPPROOF_CODE = 'NR';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER NRIDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
				console.log("NRIDocList ::"+JSON.stringify(res.rows.item(0)));
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								NRIDocList.Insured.nri = res.rows.item(i).DOC_ID;
								NRIDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								NRIDocList.Proposer.nri = res.rows.item(i).DOC_ID;
								NRIDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(NRIDocList);
				}
				else
					dfd.resolve(NRIDocList);
			});

return dfd.promise;
}


this.getOCIDocID = function(){
var dfd = $q.defer();
var OCIDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = 'NQ';
	whereClauseObj.MPPROOF_CODE = 'OC';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER OCIDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
				console.log("OCIDocList ::"+JSON.stringify(res.rows.item(0)));
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								OCIDocList.Insured.oci = res.rows.item(i).DOC_ID;
								OCIDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								OCIDocList.Proposer.oci = res.rows.item(i).DOC_ID;
								OCIDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(OCIDocList);
				}
				else
					dfd.resolve(OCIDocList);
			});

return dfd.promise;
}

this.getPANDocID = function(){
var dfd = $q.defer();
var PanDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = 'KY';
	whereClauseObj.MPPROOF_CODE = 'PC';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER PAN :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								PanDocList.Insured.pan = res.rows.item(i).DOC_ID;
								PanDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								PanDocList.Proposer.pan = res.rows.item(i).DOC_ID;
								PanDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
					}
					dfd.resolve(PanDocList);
				}
			});

return dfd.promise;
}

this.getStudentIDProof = function(){
var dfd = $q.defer();
var StudntIDList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
    whereClauseObj.MPDOC_CODE = 'OQ';
    whereClauseObj.MPPROOF_CODE = 'ST';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj,"").then(
    function(res){
        console.log("Length of Student_ID_Record::"+res.rows.length);
        if(!!res && res.rows.length > 0){
            for(var i=0; i<res.rows.length;i++){
                if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN'){
                    StudntIDList.Insured.studIDRec = res.rows.item(i).DOC_ID;
                    StudntIDList.Insured.isDigital = res.rows.item(i).isDigital;

                }
                else
                    if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR'){
                        StudntIDList.Proposer.studIDRec = res.rows.item(i).DOC_ID;
                        StudntIDList.Proposer.isDigital = res.rows.item(i).isDigital;
                    }
            }
        }
            dfd.resolve(StudntIDList);
    });

    return dfd.promise;
}

this.getImmunizationRecord = function(){
var dfd = $q.defer();
var ImmunizationRecList = {
    Insured:{},
    Proposer:{}
};

var whereClauseObj ={};
    whereClauseObj.MPDOC_CODE = 'CD';
    whereClauseObj.MPPROOF_CODE = 'IR';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj,"").then(
    function(res){
        console.log("Lenght of Immunization Record is::"+res.rows.length);
        if(!!res && res.rows.length > 0){
            for(var i=0;i<res.rows.length;i++){
                if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN'){
                    ImmunizationRecList.Insured.immunization = res.rows.item(i).DOC_ID;
                    ImmunizationRecList.Insured.isDigital = res.rows.item(i).isDigital
                }
                else
                    if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR'){
                        ImmunizationRecList.Proposer.immunization = res.rows.item(i).DOC_ID;
                        ImmunizationRecList.Proposer.isDigital = res.rows.item(i).isDigital;
                    }
            }
            dfd.resolve(ImmunizationRecList);
        }


    });
    return dfd.promise;
}

this.getParentsNOC = function(){
var dfd = $q.defer();
var parentsNOCList = {
    Insured:{},
    Proposer:{}
};

var whereClauseObj ={};
    whereClauseObj.MPDOC_CODE = 'RE';
    whereClauseObj.MPPROOF_CODE = 'NO';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj,"").then(
    function(res){
        console.log("Lenght of parentsNOCList Record is::"+res.rows.length);
        if(!!res && res.rows.length > 0){
            for(var i=0;i<res.rows.length;i++){
                if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN'){
                    parentsNOCList.Insured.parentNOC = res.rows.item(i).DOC_ID;
                    parentsNOCList.Insured.isDigital = res.rows.item(i).isDigital
                }
                else
                    if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR'){
                        parentsNOCList.Proposer.parentNOC = res.rows.item(i).DOC_ID;
                        parentsNOCList.Proposer.isDigital = res.rows.item(i).isDigital;
                    }
            }
            dfd.resolve(parentsNOCList);
        }


    });
    return dfd.promise;
}

this.getUANDocID = function(){
var dfd = $q.defer();
var UanDocList = {
Insured:{},
Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.MPDOC_CODE = 'KY';
	whereClauseObj.MPPROOF_CODE = 'AD';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER UAN :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								UanDocList.Insured.uan = res.rows.item(i).DOC_ID;
								UanDocList.Insured.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
								{
									UanDocList.Proposer.uan = res.rows.item(i).DOC_ID;
									UanDocList.Proposer.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
								}
					}
					dfd.resolve(UanDocList);
				}
			});

return dfd.promise;
}

this.getHealthDocIDList = function(){
var dfd = $q.defer();
var HealthDocList = {
	Insured:{},
	Proposer:{}
};
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'VQ';

CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_DOC_PROOF_MASTER HealthDocList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						if(res.rows.item(i).CUSTOMER_CATEGORY == 'IN')
							{
								HealthDocList.Insured[res.rows.item(i).MPPROOF_CODE] = {};
								HealthDocList.Insured[res.rows.item(i).MPPROOF_CODE].DOC_ID = res.rows.item(i).DOC_ID;
								HealthDocList.Insured[res.rows.item(i).MPPROOF_CODE].MSG = res.rows.item(i).MPPROOF_DESC;
								HealthDocList.Insured[res.rows.item(i).MPPROOF_CODE].isDigital = res.rows.item(i).DIGITAL_HTML_FLAG
							}
						else
							if(res.rows.item(i).CUSTOMER_CATEGORY == 'PR')
							{
								HealthDocList.Proposer[res.rows.item(i).MPPROOF_CODE] = {};
								HealthDocList.Proposer[res.rows.item(i).MPPROOF_CODE].DOC_ID = res.rows.item(i).DOC_ID;
								HealthDocList.Proposer[res.rows.item(i).MPPROOF_CODE].MSG = res.rows.item(i).MPPROOF_DESC;
								HealthDocList.Proposer[res.rows.item(i).MPPROOF_CODE].isDigital = res.rows.item(i).DIGITAL_HTML_FLAG
							}
					}
					dfd.resolve(HealthDocList);
				}
				else
					dfd.resolve(HealthDocList);
			});

return dfd.promise;
}

this.setAddressType = function(FATF_FLAG, ADDRCOMM,SISFormData){
var dfd = $q.defer();
var addrType = 'RP';
console.log("ADDRCOMM :"+ADDRCOMM+": FATF:"+FATF_FLAG+"SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT :"+JSON.stringify(SISFormData.sisMainData));
if((FATF_FLAG=='Y') || (parseInt(SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT) > 10000) || (ADDRCOMM!=undefined && ADDRCOMM!='' && ADDRCOMM == 'P'))
{
    addrType = 'PP';
}
dfd.resolve(addrType);

return dfd.promise;
};

this.setFATFFlag = function(APP_RESIDENT, APP_RESI_COUNTRY){
var dfd = $q.defer();
console.log("inside setFATFFlag :"+APP_RESIDENT+"::"+APP_RESI_COUNTRY);
var FATFFlag = 'N';
if((APP_RESIDENT == 'NRI' || APP_RESIDENT == 'FN' || APP_RESIDENT == 'OCI') && APP_RESI_COUNTRY.FATF_FLAG!=undefined && APP_RESI_COUNTRY.FATF_FLAG !="")
    FATFFlag = APP_RESI_COUNTRY.FATF_FLAG;

dfd.resolve(FATFFlag);

return dfd.promise;

};



this.getCurrAddressProofType = function(doc_id){
var dfd = $q.defer();
		var whereClauseObj = {};
        	whereClauseObj.ISACTIVE = '1';
        	whereClauseObj.DOC_ID = doc_id;

		CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",true,"*",whereClauseObj, "").then(
			function(res){
				var docProofMaster = [];
				console.log(JSON.stringify(whereClauseObj)+"LP_DOC_PROOF_MASTER :" + res.rows.length);
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						var docProof = {};
						  docProof.DOC_ID = res.rows.item(i).DOC_ID;
						  docProof.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						  docProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
						  docProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						  docProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
						  docProof.isDigital = res.rows.item(i).DIGITAL_HTML_FLAG;
						docProofMaster.push(docProof);
					}
					dfd.resolve(docProofMaster);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;

}
this.getPlanMasterDetails = function(planCode){
console.log("planCode ::"+planCode);
var dfd = $q.defer();

if(planCode!=undefined && planCode!=""){
		var whereClauseObj = {};
        	whereClauseObj.PLAN_CODE = planCode;
        var MedicalFlags = {};
		CommonService.selectRecords(db,"LP_PLAN_MASTER",true,"*",whereClauseObj, "").then(
			function(res){

				console.log(JSON.stringify(whereClauseObj)+"LP_PLAN_MASTER :" + res.rows.length);
				if(!!res && res.rows.length>0){
					MedicalFlags.PAYOR_MED_FLAG = res.rows.item(0).PAYOR_MED_FLAG;
					dfd.resolve(MedicalFlags);
				}
				else
					dfd.resolve(null);
			}
		);
}
else
	dfd.resolve(null);

	return dfd.promise;
}

this.getFinancialQuest = function(){
var dfd = $q.defer();
var FinancialQuest = [];
var whereClauseObj = {};
	whereClauseObj.ISACTIVE = '1';
	whereClauseObj.MPDOC_CODE = 'FQ';
	whereClauseObj.MPPROOF_CODE = 'FQ';
CommonService.selectRecords(db,"LP_DOC_PROOF_MASTER",true,"*",whereClauseObj, "").then(
			function(res){
				console.log(JSON.stringify(whereClauseObj)+"LP_DOC_PROOF_MASTER :" + res.rows.length);
				if(!!res && res.rows.length>0){
					FinancialQuest.DOC_ID = res.rows.item(0).DOC_ID;
					FinancialQuest.MPPROOF_DESC = res.rows.item(0).DOC_ID;
					FinancialQuest.DIGITAL_HTML_FLAG = res.rows.item(0).DIGITAL_HTML_FLAG;
					dfd.resolve(FinancialQuest);
				}
				else
					dfd.resolve(null);
			}
		);

return dfd.promise;
};

}]);

AppFormModule.service('LoadApplicationScreenData',['$q','CommonService','AppTimeService', function($q, CommonService,AppTimeService){

var lsd = this;

this.loadApplicationFlags = function(APPLICATION_ID, AGENT_CD){

    var dfd = $q.defer();
    var ApplicationData = {}
    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = APPLICATION_ID;
    whereClauseObj.AGENT_CD = AGENT_CD;
    console.log(JSON.stringify(whereClauseObj));

    CommonService.selectRecords(db,"LP_APPLICATION_MAIN",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("LP_APPLICATION_MAIN FLAGS :" + res.rows.length);
            if(!!res && res.rows.length>0){

                ApplicationData.applicationFlags = CommonService.resultSetToObject(res);

                dfd.resolve(ApplicationData);
            }
            else
                dfd.resolve(ApplicationData);
        }
    );

	return dfd.promise;

};


this.loadAgentImpData = function(APPLICATION_ID, AGENT_CD){

    var dfd = $q.defer();
    var AgentImpData = {}
    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = APPLICATION_ID;
    whereClauseObj.AGENT_CD = AGENT_CD;
    console.log(JSON.stringify(whereClauseObj));

    CommonService.selectRecords(db,"lp_app_sourcer_impsnt_scrn_l",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("lp_app_sourcer_impsnt_scrn_l :" + res.rows.length);
            if(!!res && res.rows.length>0){

                AgentImpData = CommonService.resultSetToObject(res);

                dfd.resolve(AgentImpData);
            }
            else
                dfd.resolve(null);
        }
    );

	return dfd.promise;

};

this.loadOppFlags = function(mainId, AGENT_CD, type, flag){

        var dfd = $q.defer();
        var OppData = {}
		var whereClauseObj = {};
                console.log("loadOppFlags:"+type);
				if(type == 'opp')
        		    whereClauseObj.OPPORTUNITY_ID = mainId;
        		else if(type == 'combo')
        		    {
        		        whereClauseObj.COMBO_ID = mainId;
        		        if(!!flag)
        		            whereClauseObj.RECO_PRODUCT_DEVIATION_FLAG = flag;
        		    }
        		    else if(type == 'app')
        		             whereClauseObj.APPLICATION_ID = mainId;
        		        else
        		             whereClauseObj.FHR_ID = mainId;
        		whereClauseObj.AGENT_CD = AGENT_CD;

		console.log(JSON.stringify(whereClauseObj));
		CommonService.selectRecords(db,"LP_MYOPPORTUNITY",false,"*",whereClauseObj, "").then(
			function(res){
				console.log(JSON.stringify(whereClauseObj)+"LP_MYOPPORTUNITY FLAGS :" + res.rows.length);
				if(!!res && res.rows.length>0){

                    OppData.oppFlags = CommonService.resultSetToObject(res);

                    dfd.resolve(OppData);
				}
				else
					dfd.resolve(OppData);
			}
		);

		return dfd.promise;
}

	//EKYC changes
	this.loadEKYCData = function(OPP_ID, AGENT_CD, isTermAttached){
		var dfd = $q.defer();
		var EKYCData = {
			Insured : {},
			Proposer : {}
		};
		var whereClauseObj = {};
		whereClauseObj.OPP_ID = OPP_ID;
		whereClauseObj.AGENT_CD = AGENT_CD;
		whereClauseObj.CUST_TYPE = 'C01';

		if(!!isTermAttached){
			debug("in term attached");
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx, "select a.opportunity_id from lp_myopportunity a, lp_myopportunity b where a.opportunity_id!=b.opportunity_id and a.combo_id=b.combo_id and b.opportunity_id=? and b.reco_product_deviation_flag=?", [OPP_ID, 'T'],
						function(tx, oppRes){
							if(!!oppRes && oppRes.rows.length>0){
								whereClauseObj.OPP_ID = oppRes.rows.item(0).OPPORTUNITY_ID;
							}
							CommonService.selectRecords(db,"LP_EKYC_DTLS",false,"*",whereClauseObj, "").then(
								function(res){
									console.log(JSON.stringify(whereClauseObj)+"LP_EKYC_DTLS INS:" + res.rows.length);
									if(!!res && res.rows.length>0){
										EKYCData.Insured = CommonService.resultSetToObject(res);
									}
									whereClauseObj.CUST_TYPE = 'C02';
									CommonService.selectRecords(db,"LP_EKYC_DTLS",false,"*",whereClauseObj, "").then(
										function(res){
											console.log("LP_EKYC_DTLS PRO:" + res.rows.length);
											if(!!res && res.rows.length>0){
												EKYCData.Proposer = CommonService.resultSetToObject(res);
												dfd.resolve(EKYCData);
											}
											else
												dfd.resolve(EKYCData);
										}
									);
								}
							);
						},
						function(tx, err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		else{
			CommonService.selectRecords(db,"LP_EKYC_DTLS",false,"*",whereClauseObj, "").then(
				function(res){
					console.log(JSON.stringify(whereClauseObj)+"LP_EKYC_DTLS INS:" + res.rows.length);
					if(!!res && res.rows.length>0){
						EKYCData.Insured = CommonService.resultSetToObject(res);
					}
					whereClauseObj.CUST_TYPE = 'C02';
					CommonService.selectRecords(db,"LP_EKYC_DTLS",false,"*",whereClauseObj, "").then(
						function(res){
							console.log("LP_EKYC_DTLS PRO:" + res.rows.length);
							if(!!res && res.rows.length>0){
								EKYCData.Proposer = CommonService.resultSetToObject(res);
								dfd.resolve(EKYCData);
							}
							else
								dfd.resolve(EKYCData);
						}
					);
				}
			);
		}
		return dfd.promise;
	};
  //LEAD EKYC changes
	this.loadLeadEKYCData = function(OPP_ID, AGENT_CD, isTermAttached,LEAD_ID, EKYC_ID, BUY_FOR_CD){
		var dfd = $q.defer();
		var EKYCData = {
			Insured : {},
			Proposer : {}
		};
		var whereClauseObj = {};
		whereClauseObj.LEAD_ID = LEAD_ID;
    if(!!EKYC_ID)
      whereClauseObj.EKYC_ID = EKYC_ID;
		whereClauseObj.AGENT_CD = AGENT_CD;
		//whereClauseObj.CUST_TYPE = 'C01';

		if(!!isTermAttached && !!OPP_ID){
			debug("in term attached");
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx, "select a.opportunity_id from lp_myopportunity a, lp_myopportunity b where a.opportunity_id!=b.opportunity_id and a.combo_id=b.combo_id and b.opportunity_id=? and b.reco_product_deviation_flag=?", [OPP_ID, 'T'],
						function(tx, oppRes){
							if(!!oppRes && oppRes.rows.length>0){
								//whereClauseObj.OPP_ID = oppRes.rows.item(0).OPPORTUNITY_ID;
                whereClauseObj.EKYC_ID = oppRes.rows.item(0).INS_EKYC_ID;
                if(BUY_FOR_CD == '200')
                  whereClauseObj.CUST_TYPE = 'C02';
							}
							CommonService.selectRecords(db,"LP_LEAD_EKYC_DTLS",false,"*",whereClauseObj, "").then(
								function(res){
									console.log(JSON.stringify(whereClauseObj)+"LP_LEAD_EKYC_DTLS INS:" + res.rows.length);
									if(!!res && res.rows.length>0){
										EKYCData.Insured = CommonService.resultSetToObject(res);
									}
									whereClauseObj.CUST_TYPE = 'C02';
                  if(!!oppRes && oppRes.rows.length>0)
                    whereClauseObj.EKYC_ID = oppRes.rows.item(0).PR_EKYC_ID;
									CommonService.selectRecords(db,"LP_LEAD_EKYC_DTLS",false,"*",whereClauseObj, "").then(
										function(res){
											console.log("LP_LEAD_EKYC_DTLS PRO:" + res.rows.length);
											if(!!res && res.rows.length>0){
												EKYCData.Proposer = CommonService.resultSetToObject(res);
                        if(BUY_FOR_CD == '200')
                          EKYCData.Insured.CUST_TYPE = 'C01';
												dfd.resolve(EKYCData);
											}
											else
												{
                          if(BUY_FOR_CD == '200')
                            EKYCData.Insured.CUST_TYPE = 'C01';
                          dfd.resolve(EKYCData);
                        }
										}
									);
								}
							);
						},
						function(tx, err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		else{
      lsd.loadOppFlags(OPP_ID, AGENT_CD,'opp').then(function(oppRes) {
          if(!!oppRes && !! oppRes.oppFlags && !!oppRes.oppFlags.OPPORTUNITY_ID){
            whereClauseObj.EKYC_ID = oppRes.oppFlags.INS_EKYC_ID;
            if(BUY_FOR_CD == '200')
              whereClauseObj.CUST_TYPE = 'C02'; //load proposer EKYC for self
            CommonService.selectRecords(db,"LP_LEAD_EKYC_DTLS",false,"*",whereClauseObj, "").then(
      				function(res){
      					console.log(JSON.stringify(whereClauseObj)+"LP_LEAD_EKYC_DTLS INS:" + res.rows.length);
      					if(!!res && res.rows.length>0){
      						EKYCData.Insured = CommonService.resultSetToObject(res);
      					}
      					whereClauseObj.CUST_TYPE = 'C02';
                whereClauseObj.EKYC_ID = oppRes.oppFlags.PR_EKYC_ID;
      					CommonService.selectRecords(db,"LP_LEAD_EKYC_DTLS",false,"*",whereClauseObj, "").then(
      						function(res){
      							console.log("LP_LEAD_EKYC_DTLS PRO:" + res.rows.length);
      							if(!!res && res.rows.length>0){
      								EKYCData.Proposer = CommonService.resultSetToObject(res);
                      if(BUY_FOR_CD == '200')
                        EKYCData.Insured.CUST_TYPE = 'C01';
      								dfd.resolve(EKYCData);
      							}
      							else
      								{
                        if(BUY_FOR_CD == '200')
                          EKYCData.Insured.CUST_TYPE = 'C01';
                        dfd.resolve(EKYCData);
                      }
      						}
      					);
      				}
      			);
        }
        else
          dfd.resolve(EKYCData);
      });
		}

		return dfd.promise;
	};


this.loadApplicationMain = function(APPLICATION_ID, AGENT_CD){

var dfd = $q.defer();
var ApplicationData = {}
		var whereClauseObj = {};
        		whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;

		CommonService.selectRecords(db,"LP_APPLICATION_MAIN",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_APPLICATION_MAIN :" + res.rows.length);
				if(!!res && res.rows.length>0){
                    ApplicationData.applicationMainData = CommonService.resultSetToObject(res);
                    ApplicationData.applicationPersonalInfo = {};
                    whereClauseObj.CUST_TYPE = 'C01';
                    CommonService.selectRecords(db,"LP_APP_CONTACT_SCRN_A",false,"*",whereClauseObj, "").then(
                    			function(res){

                    				if(!!res && res.rows.length>0){
                                        ApplicationData.applicationPersonalInfo.Insured = CommonService.resultSetToObject(res);
										console.log("LP_APP_CONTACT_SCRN_A INS:" + JSON.stringify(res.rows.item(0)));
										//dfd.resolve(ApplicationData);
										whereClauseObj.CUST_TYPE = 'C02';
										CommonService.selectRecords(db,"LP_APP_CONTACT_SCRN_A",false,"*",whereClauseObj, "").then(
												function(res){
													console.log("LP_APP_CONTACT_SCRN_A PRO:" + res.rows.length);
													if(!!res && res.rows.length>0){
														ApplicationData.applicationPersonalInfo.Proposer = CommonService.resultSetToObject(res);
														dfd.resolve(ApplicationData);
													}
													else
														dfd.resolve(ApplicationData);
												}
											);
                    				}
                    				else
                    					dfd.resolve(ApplicationData);
                    			}
                    		);
				}
				else
					dfd.resolve(ApplicationData);
			}
		);

		return dfd.promise;

};

this.loadLifeStyleData = function(APPLICATION_ID, AGENT_CD){

var dfd = $q.defer();
var lifeStyleInfo = {};
		var whereClauseObj = {};
        		whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;
        		whereClauseObj.CUST_TYPE = 'C01';
                            CommonService.selectRecords(db,"LP_APP_LS_ANS_SCRN_B",false,"*",whereClauseObj, "").then(
                            			function(res){
                            				console.log("LP_APP_LS_ANS_SCRN_B Ins:" + res.rows.length);
                            				if(!!res && res.rows.length>0){
                                                lifeStyleInfo.Insured = CommonService.resultSetToObject(res);
        										//dfd.resolve(lifeStyleInfo);
        										whereClauseObj.CUST_TYPE = 'C02';
        										CommonService.selectRecords(db,"LP_APP_LS_ANS_SCRN_B",false,"*",whereClauseObj, "").then(
        												function(res){
        													console.log("LP_APP_LS_ANS_SCRN_B PRO:" + res.rows.length);
        													if(!!res && res.rows.length>0){
        														lifeStyleInfo.Proposer = CommonService.resultSetToObject(res);
																dfd.resolve(lifeStyleInfo);
        													}
        													else
        														dfd.resolve(lifeStyleInfo);
        												}
        											);
                            				}
                            				else
                            					dfd.resolve(lifeStyleInfo);
                            			}
                            		);

		return dfd.promise;

};

this.loadHealthData = function(APPLICATION_ID, AGENT_CD){

var dfd = $q.defer();
var HealthInfo = {};
		var whereClauseObj = {};
        		whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;
        		whereClauseObj.CUST_TYPE = 'C01';
                            CommonService.selectRecords(db,"LP_APP_HD_ANS_SCRN_C",false,"*",whereClauseObj, "QUESTION_ID ASC").then(
                            			function(res){
                            				console.log("LP_APP_HD_ANS_SCRN_C Ins:" + res.rows.length);
                            				if(!!res && res.rows.length>0){
                                                HealthInfo.Insured = CommonService.resultSetToObject(res);
        										//dfd.resolve(lifeStyleInfo);
        										whereClauseObj.CUST_TYPE = 'C02';
        										CommonService.selectRecords(db,"LP_APP_HD_ANS_SCRN_C",false,"*",whereClauseObj, "QUESTION_ID ASC").then(
        												function(res){
        													console.log("LP_APP_HD_ANS_SCRN_C PRO:" + res.rows.length);
        													if(!!res && res.rows.length>0){
        														HealthInfo.Proposer = CommonService.resultSetToObject(res);
																dfd.resolve(HealthInfo);
        													}
        													else
        														dfd.resolve(HealthInfo);
        												}
        											);
                            				}
                            				else
                            					dfd.resolve(HealthInfo);
                            			}
                            		);

		return dfd.promise;

};

this.loadHealthSubData = function(APPLICATION_ID, AGENT_CD){

var dfd = $q.defer();
var HealthInfo = {};
		var whereClauseObj = {};
        		whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;
        		whereClauseObj.CUST_TYPE = 'C01';
                            CommonService.selectRecords(db,"LP_APP_HD_ANS_SCRN_C13",false,"*",whereClauseObj, "").then(
                            			function(res){
                            				console.log("LP_APP_HD_ANS_SCRN_C13 Ins:" + res.rows.length);
                            				if(!!res && res.rows.length>0){
                                                HealthInfo.Insured = CommonService.resultSetToObject(res);
                                                if(!!HealthInfo.Insured && !(HealthInfo.Insured instanceof Array))
                                                	HealthInfo.Insured = [HealthInfo.Insured];
        										//dfd.resolve(lifeStyleInfo);
        										whereClauseObj.CUST_TYPE = 'C02';
        										console.log("Proposer Data::"+whereClauseObj.CUST_TYPE);
        										CommonService.selectRecords(db,"LP_APP_HD_ANS_SCRN_C13",false,"*",whereClauseObj, "").then(
        												function(res){
        													console.log("LP_APP_HD_ANS_SCRN_C13 PRO:" + res.rows.length);
        													if(!!res && res.rows.length>0){
        														HealthInfo.Proposer = CommonService.resultSetToObject(res);
        														if(!!HealthInfo.Proposer && !(HealthInfo.Proposer instanceof Array))
                                                                     HealthInfo.Proposer = [HealthInfo.Proposer];
																dfd.resolve(HealthInfo);
																console.log("HealthInfo::"+JSON.stringify(HealthInfo));
        													}
        													else
        														dfd.resolve(HealthInfo);
        												}
        											);
                            				}
                            				else
                            					dfd.resolve(HealthInfo);
                            			}
                            		);

		return dfd.promise;

};

this.loadExistingData = function(APPLICATION_ID, AGENT_CD){

var dfd = $q.defer();
var ExistInsuranceData = {};
		var whereClauseObj = {};
        		whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;
        		whereClauseObj.CUST_TYPE = 'C01';
            whereClauseObj.TATA_OTHER_FLAG = 'O';
                            CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj, "INS_SEQ_NO ASC").then(
                            			function(res){
                            				console.log("LP_APP_EXIST_INS_SCRN_D Ins:" + res.rows.length);
                            				if(!!res && res.rows.length>0){
                                        ExistInsuranceData.Insured = CommonService.resultSetToObject(res);

                                        if(!!ExistInsuranceData.Insured && !(ExistInsuranceData.Insured instanceof Array))
                                          ExistInsuranceData.Insured = [ExistInsuranceData.Insured];
        										whereClauseObj.CUST_TYPE = 'C02';
        										CommonService.selectRecords(db,"LP_APP_EXIST_INS_SCRN_D",false,"*",whereClauseObj, "INS_SEQ_NO ASC").then(
        												function(res){
        													console.log("LP_APP_EXIST_INS_SCRN_D PRO:" + res.rows.length);
        													if(!!res && res.rows.length>0){
        														ExistInsuranceData.Proposer = CommonService.resultSetToObject(res);
                                    if(!!ExistInsuranceData.Proposer && !(ExistInsuranceData.Proposer instanceof Array))
                                      ExistInsuranceData.Proposer = [ExistInsuranceData.Proposer];
																      dfd.resolve(ExistInsuranceData);
        													}
        													else
        														dfd.resolve(ExistInsuranceData);
        												}
        											);
                            				}
                            				else
                            					dfd.resolve(ExistInsuranceData);
                            			}
                            		);

		return dfd.promise;

};

this.loadFHData = function(APPLICATION_ID, AGENT_CD){
var dfd = $q.defer();
var FHData = {};
	var whereClauseObj = {};
	whereClauseObj.APPLICATION_ID = APPLICATION_ID;
	whereClauseObj.AGENT_CD = AGENT_CD;
	CommonService.selectRecords(db,"LP_APP_FH_SCRN_E",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("LP_APP_FH_SCRN_E :" + res.rows.length);
            if(!!res && res.rows.length>0){
                FHData = CommonService.resultSetToObject(res);
                dfd.resolve(FHData);
            }
            else
                dfd.resolve(null);
        }
	);
	return dfd.promise;
};

this.loadPaymentData = function(APPLICATION_ID, AGENT_CD){
    var dfd = $q.defer();
    var PaymentData = {};
	var whereClauseObj = {};
	whereClauseObj.APPLICATION_ID = APPLICATION_ID;
	whereClauseObj.AGENT_CD = AGENT_CD;
	CommonService.selectRecords(db,"LP_APP_PAY_DTLS_SCRN_G",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("LP_APP_PAY_DTLS_SCRN_G :" + res.rows.length);
            if(!!res && res.rows.length>0){
                PaymentData = CommonService.resultSetToObject(res);
                dfd.resolve(PaymentData);
            }
            else
                dfd.resolve(PaymentData);
        }
	);
	return dfd.promise;
};

this.loadBankDetails = function(APPLICATION_ID, AGENT_CD){
    var dfd = $q.defer();
    var bankData = {};
	var whereClauseObj = {};
	whereClauseObj.APPLICATION_ID = APPLICATION_ID;
	whereClauseObj.AGENT_CD = AGENT_CD;
	CommonService.selectRecords(db,"LP_APP_PAY_NEFT_SCRN_H",false,"*",whereClauseObj, "").then(
        function(res){
            console.log("LP_APP_PAY_NEFT_SCRN_H :" + res.rows.length);
            if(!!res && res.rows.length>0){
                bankData = CommonService.resultSetToObject(res);
                dfd.resolve(bankData);
            }
            else
                dfd.resolve(bankData);
        }
	);
	return dfd.promise;
};

this.loadMedicalDCData = function(APPLICATION_ID, AGENT_CD){
        var dfd = $q.defer();
        var MedicalDCData = {
            Insured : {},
            Proposer : {}
        }
		var whereClauseObj = {};
        whereClauseObj.APPLICATION_ID = APPLICATION_ID;
        whereClauseObj.AGENT_CD = AGENT_CD;
        whereClauseObj.CUST_TYPE = 'C01';
		console.log(JSON.stringify(whereClauseObj));
		CommonService.selectRecords(db,"LP_APP_SAR_MED_SCREEN_K",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_APP_SAR_MED_SCREEN_K data :C01 :" + res.rows.length);
				if(!!res && res.rows.length>0){
                    MedicalDCData.Insured = CommonService.resultSetToObject(res);
                    whereClauseObj.CUST_TYPE = 'C02';
                    CommonService.selectRecords(db,"LP_APP_SAR_MED_SCREEN_K",false,"*",whereClauseObj, "").then(
                        function(res){
                            console.log("LP_APP_SAR_MED_SCREEN_K data :C02 :" + res.rows.length);
                            if(!!res && res.rows.length>0){
                                MedicalDCData.Proposer = CommonService.resultSetToObject(res);
                                dfd.resolve(MedicalDCData);
                            }
                            else
                                dfd.resolve(MedicalDCData);
                        }
                    );
				}
				else
					dfd.resolve(MedicalDCData);
			}
		);
	    return dfd.promise;
    }

this.loadFFNATableData = function(FHR_ID, AGENT_CD){
    var dfd = $q.defer();
	var FFNAData = {};
		var whereClauseObj = {};
        		whereClauseObj.FHR_ID = FHR_ID;
        		whereClauseObj.AGENT_CD = AGENT_CD;
				CommonService.selectRecords(db,"LP_FHR_FFNA",false,"*",whereClauseObj, "").then(
					function(res){
						console.log("LP_FHR_FFNA :" + res.rows.length);
						if(!!res && res.rows.length>0){
							FFNAData.FFNA = CommonService.resultSetToObject(res);
							CommonService.selectRecords(db,"LP_FHR_MAIN",false,"*",whereClauseObj, "").then(
									function(res){
										console.log("LP_FHR_MAIN :" + res.rows.length);
										if(!!res && res.rows.length>0){
											FFNAData.FHRMain = CommonService.resultSetToObject(res);
											dfd.resolve(FFNAData);
										}
										else
											dfd.resolve(FFNAData);
									}
								);
						}
						else
							dfd.resolve(FFNAData);
					}
				);

		return dfd.promise;

    }
	this.loadFHRTableData = function(FHR_ID, AGENT_CD){
	    var dfd = $q.defer();
		var FHRData = {};
			var whereClauseObj = {};
	        		whereClauseObj.FHR_ID = FHR_ID;
	        		whereClauseObj.AGENT_CD = AGENT_CD;
					CommonService.selectRecords(db,"LP_FHR_PERSONAL_INF_SCRN_A",false,"*",whereClauseObj, "").then(
						function(res){
							console.log("LP_FHR_PERSONAL_INF_SCRN_A :" + res.rows.length);
							if(!!res && res.rows.length>0){
								FHRData.FFHR = CommonService.resultSetToObject(res);
								CommonService.selectRecords(db,"LP_FHR_MAIN",false,"*",whereClauseObj, "").then(
										function(res){
											console.log("LP_FHR_MAIN :" + res.rows.length);
											if(!!res && res.rows.length>0){
												FHRData.FHRMain = CommonService.resultSetToObject(res);
												dfd.resolve(FHRData);
											}
											else
												dfd.resolve(FHRData);
										}
									);
							}
							else
								dfd.resolve(FHRData);
						}
					);

			return dfd.promise;

	    }
this.loadLeadTableData = function(LEAD_ID, AGENT_CD){
	debug("inside loadLeadTableData"+LEAD_ID)
        var dfd = $q.defer();
    	var LeadData = {};
    		var whereClauseObj = {};
            		whereClauseObj.IPAD_LEAD_ID = LEAD_ID;
            		whereClauseObj.AGENT_CD = AGENT_CD;
    				CommonService.selectRecords(db,"LP_MYLEAD",false,"*",whereClauseObj, "").then(
    					function(res){
    						console.log("LP_MYLEAD :" + res.rows.length);
    						if(!!res && res.rows.length>0){
    							LeadData = CommonService.resultSetToObject(res);
    							dfd.resolve(LeadData);
    						}
    						else
    							dfd.resolve(LeadData);
    					}
    				);

    		return dfd.promise;

        }

    this.loadSISTermData = function(OPP_ID,AGENT_CD){
        debug("inside LP_COMBO_ADD_DETAILS"+OPP_ID)
             var dfd = $q.defer();
            var sisTermData = {};
                var whereClauseObj = {};
                        whereClauseObj.OPPORTUNITY_ID = OPP_ID;
                        whereClauseObj.AGENT_CD = AGENT_CD;
                        CommonService.selectRecords(db,"LP_COMBO_ADD_DETAILS",false,"*",whereClauseObj, "").then(
                            function(res){
                                console.log("LP_COMBO_ADD_DETAILS :" + res.rows.length);
                                if(!!res && res.rows.length>0){
                                    sisTermData = CommonService.resultSetToObject(res);
                                    dfd.resolve(sisTermData);
                                }
                                else
                                    dfd.resolve(sisTermData);
                            }
                        );

                return dfd.promise;

             }

}]);

AppFormModule.service('LoadReflService',['$q','CommonService','LoginService','ReflectiveQuesService', function($q, CommonService,LoginService,ReflectiveQuesService){
    var lrs = this;
    this.ReflectiveQuesList = [];
    this.getInsReflectiveQuesList = function(reflInsObj,APPLICATION_ID,POLICY_NO,appFormServ){
        var agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        var promises = [];
        var i = 0;
        if(!!reflInsObj && Object.keys(reflInsObj).length>0){
            angular.forEach(reflInsObj, function(value, key) {
                var obj = reflInsObj[key];
                console.log("reflInsObj :: " + JSON.stringify(obj));
                if(!!obj && Object.keys(obj).length>0 && !!obj.DOC_ID){
                    promises.push(ReflectiveQuesService.getDocDescription(obj.DOC_ID).then(
                        function(res){
                            if(!!res){
                                var obj = {};
                                obj.title = res.MPPROOF_DESC;
                                obj.url = res.FORM_ID;
                                obj.docId = res.DOC_ID;
                                obj.agentCd = agentCd;
                                obj.custType = res.CATEGORY_DESCRIPTION;
                                obj.appId = APPLICATION_ID;
                                obj.polNo = POLICY_NO;
                                obj.seqNo  = i;
                                obj.appFormServ  = appFormServ;
                                i++;
                                debug("obj : " + JSON.stringify(obj));
                                return obj;
                            }
                        }
                    ));
                }else{
                    debug("No Doc Id's in reflInsObj inner");
                }
            });
        }else{
            debug("No Doc Id's in reflInsObj");
            //promises.push();
        }
        return $q.all(promises);
    };

    this.getPropReflectiveQuesList = function(reflectiveQuesArr,reflPropObj,APPLICATION_ID,POLICY_NO,appFormServ){
        var agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        var promises = reflectiveQuesArr;
        var i = reflectiveQuesArr.length;
        if(!!reflPropObj && Object.keys(reflPropObj).length>0){
            angular.forEach(reflPropObj, function(value, key) {
                var obj = reflPropObj[key];
                console.log("reflPropObj :: " + JSON.stringify(obj));
                if(!!obj && Object.keys(obj).length>0 && !!obj.DOC_ID){
                    promises.push(ReflectiveQuesService.getDocDescription(obj.DOC_ID).then(
                        function(res){
                            if(!!res){
                                var obj = {};
                                obj.title = res.MPPROOF_DESC;
                                obj.url = res.FORM_ID;
                                obj.docId = res.DOC_ID;
                                obj.agentCd = agentCd;
                                obj.custType = res.CATEGORY_DESCRIPTION;
                                obj.appId = APPLICATION_ID;
                                obj.polNo = POLICY_NO;
                                obj.seqNo  = i;
                                obj.appFormServ  = appFormServ;
                                i++;
                                debug("obj : " + JSON.stringify(obj));
                                return obj;
                            }
                        }
                    ));
                }else{
                    debug("No Doc Id's in reflPropObj inner");
                }
            });
        }else{
            debug("No Doc Id's in reflPropObj");
            //promises.push();
        }
        return $q.all(promises);
    };

    this.getStateName = function(reflectiveQuesArr){
        var dfd = $q.defer();
        debug("FORM_ID : " + reflectiveQuesArr[0].docId);
        var docId =  reflectiveQuesArr[0].docId;
        var stateName = 'applicationForm.reflectiveQuestionaries.';
        switch(docId){
            /*case 'U20001' :
                dfd.resolve(stateName + 'agricultureQues');
                break;*/
            case '1020010413' :
            case '2020010412' :
                dfd.resolve(stateName + 'agricultureQues');
                break;
            //case 'U07701' :
            case '1030010293':
            case '2030010273':
                dfd.resolve(stateName + 'diabetesQues');
                break;
            //case 'U03001' :
            case '1020010239':
            case '2020010229':
                dfd.resolve(stateName + 'houseWifeQues');
                break;
            //case 'U08201' :
            case '1030010296':
            case '2030010276':
                dfd.resolve(stateName + 'hypertensionQues');
                break;
            //case 'U03402' :.
            case '1020010414':
                dfd.resolve(stateName + 'juvenileQues');
                break;
            //case 'U15301' :
            case '7024010377':
            case '4024010226':
                dfd.resolve(stateName + 'mwpQues');
                break;
            case '1031010309':
            case '1031010308':
                dfd.resolve(stateName + 'nriQues');
                break;
            /*case 'U08403' :
                dfd.resolve(stateName + 'nriQues');
                break;*/
            //case 'U20201' :
            case '1031010406':
            case '2031010405':
                dfd.resolve(stateName + 'pioOciQues');
                break;
            //case 'U19801' :
            case '1031010420':
            case '2031010419':
                dfd.resolve(stateName + 'fatcaQues');
                break;
            //case 'U03001' :
            case '1020010248':
            case '2020010238':
                dfd.resolve(stateName + 'occupationQues');
                break;
            default :
                debug("Default case");
                dfd.resolve(null);
        }
        return dfd.promise;
    };
}]);
