AppFormModule.controller('AppTimeCtrl', ['$state', 'AppTimeService', 'ApplicationFormDataService', 'CommonService', '$rootScope', '$timeout', function($state, AppTimeService, ApplicationFormDataService, CommonService, $rootScope, $timeout){
	var ac = this;
	debug("TimeLine control loaded " + AppTimeService.isTab);

	this.appTimeArr = [
		{'name': 'lifeStage', 'timeLineName': 'Personal information', 'type': 'lifeStage', 'isCompleted': AppTimeService.isLifeStageCompleted, 'isTab': true},
		{'name': 'lifeStyle', 'timeLineName': 'Life Style', 'type': 'lifeStyle', 'isCompleted': AppTimeService.isLifeStyleCompleted, 'isTab': true},
		{'name': 'health', 'timeLineName': 'Health Questionnaire', 'type': 'health', 'isCompleted': AppTimeService.isHealthCompleted, 'isTab': true},
		{'name': 'existingPolicy', 'timeLineName': 'Existing policy', 'type': 'existingPolicy', 'isCompleted':AppTimeService.isExistPolicyCompleted, 'isTab': true},
		{'name': 'familyDetails', 'timeLineName': 'Family details', 'type': 'familyDetails', 'isCompleted': AppTimeService.isFamDetCompleted, 'isTab': true},
		{'name': 'nomAppDetails', 'timeLineName': 'Nominee / Appointee', 'type': 'nomAppDetails', 'isCompleted': AppTimeService.isNomApptCompleted, 'isTab':true},
		{'name': 'paymentDetails', 'timeLineName': 'Payment details', 'type': 'paymentDetails', 'isCompleted': AppTimeService.isPayDetCompleted, 'isTab': true},
		{'name': 'policyNoGen', 'timeLineName': 'Policy No Generation', 'type': 'policyNoGen', 'isCompleted': AppTimeService.isPolNoGenCompleted, 'isTab': true},
		{'name': 'medDiag', 'timeLineName': 'Medical Diagnosis', 'type': 'medDiag', 'isCompleted': AppTimeService.isMedDiaCompleted, 'isTab': true},
		{'name': 'reflectiveQues', 'timeLineName': 'Reflexive Questionnaire', 'type': 'reflectiveQues', 'isCompleted': AppTimeService.isReflQuestCompleted, 'isTab': true},
		{'name': 'genOutput', 'timeLineName': 'Generate Application', 'type': 'genOutput', 'isCompleted': AppTimeService.isOutGenCompleted, 'isTab': true}
	];
	var termTab = {'name': 'genOutputTerm', 'timeLineName': 'Term Application', 'type': 'genOutputTerm', 'isCompleted': AppTimeService.isTermOutGenCompleted, 'isTab': AppTimeService.isTab};
	debug(JSON.stringify(ApplicationFormDataService.ComboFormBean));
	if(!!ApplicationFormDataService.ComboFormBean && !!ApplicationFormDataService.ComboFormBean.COMBO_ID){
		this.appTimeArr.push(termTab);
	}
	this.activeTab = AppTimeService.getActiveTab();
	debug("active tab set");

	debug("this.appTimeArr: " + JSON.stringify(this.appTimeArr));

	ac.progressValue = AppTimeService.getProgressValue();

	this.gotoPage = function(pageName){
		debug("PAGE NAME >>>"+pageName);
		ApplicationFormDataService.gotoPage(pageName);
	};

	this.openMenu = function(){
		CommonService.openMenu();
	};

}]);

AppFormModule.service('AppTimeService',[function(){
	"use strict";
	debug("in app timeline service");

	this.isLifeStageCompleted = false;
	this.isLifeStyleCompleted = false;
	this.isHealthCompleted = false;
	this.isExistPolicyCompleted = false;
	this.isFamDetCompleted = false;
	this.isNomApptCompleted = false;
	this.isPayDetCompleted = false;
	this.isPolNoGenCompleted = false;
	this.isMedDiaCompleted = false;
	this.isReflQuestCompleted = false;
	this.isOutGenCompleted = false;
	this.isTermOutGenCompleted = false;
	this.isTab = true;

	this.activeTab = "lifeStage";
	this.progressValue = 0;

	this.getActiveTab = function(){
			return this.activeTab;
	};

	this.setActiveTab = function(activeTab){
		this.activeTab = activeTab;
	};

	//progress values..
	this.setProgressValue = function(progressValue){
		this.progressValue = progressValue;
	}

	this.getProgressValue = function(){
		return this.progressValue;
	}
}]);

AppFormModule.directive('appTimeline',[function() {
	"use strict";
	debug('in appTimeline');
	return {
		restrict: 'E',
		controller: "AppTimeCtrl",
		controllerAs: "aptc",
		bindToController: true,
		templateUrl: "applicationForm/AppHeader.html"
	};
}]);
