var AppFormModule = angular.module('app.applicationModule', [
    'app.applicationModule.personalInfoModule',
    'app.applicationModule.questionnaire',
    'app.applicationModule.existingInsModule',
    'app.applicationModule.familyDetailsModule',
    'app.applicationModule.nomAppModule',
    'app.applicationModule.paymentdetailsModule',
    'app.applicationModule.policyNoModule',
    'app.applicationModule.medicalDiagnosis',
    'app.applicationModule.reflectiveQuesModule',
    'app.applicationModule.appOutput']);
