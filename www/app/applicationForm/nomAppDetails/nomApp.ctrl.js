nomApptModule.controller('NomApptCtrl',['CommonService',function(CommonService){

//timeline changes..
CommonService.hideLoading();

}]);

nomApptModule.service('NomApptService',['CommonService','$q','ApplicationFormDataService','LoginService','nomAppInfoService','$state',function(CommonService,$q,ApplicationFormDataService,LoginService,nomAppInfoService,$state){
        var self = this;

        this.gotoPage = function(pageName){
			if(nomAppInfoService.getActiveTab()!==pageName){
				CommonService.showLoading();
			}
            nomAppInfoService.setActiveTab(pageName);
            switch(pageName){
                case 'nominee':
                    $state.go('applicationForm.nomAppDetails.nomineeDetails');
                    break;

                case 'appointee':
                    this.checkIfAppointeeInData().then(
                        function(data){
                            if(!!data && data.rows.length > 0){
                                debug("APPOINTEE EXIST")
                                $state.go('applicationForm.nomAppDetails.appointeeDetails');
                            }else{
                                navigator.notification.alert("Appointee not allowed for nominee greater than 18",function(){CommonService.hideLoading();},"Application","OK");
                            }
                        }
                    );
                    break;

                default:
                    $state.go('applicationForm.nomAppDetails.nomineeDetails');
            }
        };

        this.checkIfAppointeeInData = function(){
            var dfd =$q.defer();

            var appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

            var whereClauseObj = {};
            whereClauseObj.APPLICATION_ID = appID;
            whereClauseObj.AGENT_CD = agentCD;

            CommonService.selectRecords(db,"LP_APP_NOM_CONTACT_SCRN_F",true,"1",whereClauseObj,null).then(
                function(res){
                    debug("appointee exist is "+JSON.stringify(res));
                    dfd.resolve(res);
                }
            );

            return dfd.promise;
        };
}]);
