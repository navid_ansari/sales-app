nomApptModule.controller('nomAppInfoCtrl',['nomAppInfoService','NomApptService',function(nomAppInfoService,NomApptService){
    debug("exist controller loaded");

    this.nomAppArr = [
        {'type':'nominee','name':'Nominee'},
        {'type':'appointee','name':'Appointee'}
    ]

    this.activeTab = nomAppInfoService.getActiveTab();
    debug("active tab set");

    this.gotoPage = function(pageName){
        debug("pagename "+pageName);
        NomApptService.gotoPage(pageName);
    };

    debug("this.PI Side: " + JSON.stringify(this.nomAppArr));
}]);

nomApptModule.service('nomAppInfoService',[function(){
    debug("exist service loaded");

    this.activeTab = "nominee";

    this.getActiveTab = function(){
            return this.activeTab;
    };

    this.setActiveTab = function(activeTab){
        this.activeTab = activeTab;
    };
}]);

nomApptModule.directive('naTimeline',[function() {
        "use strict";
        debug('in exist');
        return {
            restrict: 'E',
            controller: "nomAppInfoCtrl",
            controllerAs: "nac",
            bindToController: true,
            templateUrl: "applicationForm/nomAppDetails/nomAppTimeline.html"
        };
}]);