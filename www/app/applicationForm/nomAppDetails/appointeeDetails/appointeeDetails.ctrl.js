ApptModule.controller('ApptCntrl',['$q','$state','apptRelationData','ApptTermExistingData','CommonService','NomApptService','ApptService','ApptExistingData','ApplicationFormDataService','LoginService','AppTimeService','nomService','DeDupeData',function($q, $state,apptRelationData,ApptTermExistingData,CommonService,NomApptService,ApptService,ApptExistingData,ApplicationFormDataService,LoginService,AppTimeService,nomService, DeDupeData){
    var self = this;
    self.apptRelationData = apptRelationData;
    self.COMBO_ID=ApplicationFormDataService.ComboFormBean.COMBO_ID;
    self.click = false;
    self.apptData = {};
    self.apptTermData={};
    self.nomineeAgeFlag=nomService.isNomineeBelow18();
    self.nomineeTermAgeFlag=nomService.isNomineeTermBelow18();
    console.log("nomineeAgeFlag : "+self.nomineeAgeFlag);
    console.log("nomineeTermAgeFlag : "+self.nomineeTermAgeFlag);
    var appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
    self.apptData.appID = appID;
    self.apptTermData.comboID = self.comboID;
    var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    self.apptData.username = agentCD;
    self.apptData.apptCustID = "C98";
    self.apptTermData.username = agentCD;
    self.apptTermData.apptCustID = "C98";
    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = appID;
    whereClauseObj.AGENT_CD = agentCD;


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */

    //Set DeDupe Data
    this.setDeDupeData = function () {
      var dfd = $q.defer();
      try{
        if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && !!DeDupeData.RESULT_APPDATA.APPOINTEE_DETAILS && (!ApptExistingData || !ApptExistingData.appID)){
            self.apptData = {};
            CommonService.showLoading("Loading Existing Data..");
            var appt = DeDupeData.RESULT_APPDATA.APPOINTEE_DETAILS;
            var apptDetails = (!!appt) ? appt.split(",") : [""];
            var aptDataObj = (!!apptDetails && apptDetails[0]) ? apptDetails[0].split("*") : [""];
            var name = (!!aptDataObj && !!aptDataObj[0]) ? aptDataObj[0].trim() : [""];
            var nameArr = name.split(" ");
            if(!!apptDetails && apptDetails.length>0 && !!aptDataObj && aptDataObj.length>0){
              self.apptData.FirstName = nameArr[0];
              if(nameArr.length>2){
                self.apptData.MiddleName = "";
                for(var i=1; i<(nameArr.length-1); i++){
                  self.apptData.MiddleName += nameArr[i];
                  if(i!=(nameArr.length-2))
                    self.apptData.MiddleName += " ";
                }
                self.apptData.LastName = nameArr[(nameArr.length - 1)];
              }
              if(!!aptDataObj && !!aptDataObj[1])
              {
                var db = aptDataObj[1].substring(6,8) +"-"+ aptDataObj[1].substring(4,6) +"-"+ aptDataObj[1].substring(0,4);
                self.apptData.dob = CommonService.formatDobFromDb(db);
                debug("Dupe: Appointee : Date of birth "+ self.apptData.dob);
              }
              self.apptData.seq_no = 1;
              self.apptData.Gen = (!!aptDataObj[2]) ? ((aptDataObj[2] == 'M') ? 'Male': 'Female') : null;
              self.apptData.gender_code = (!!aptDataObj[2]) ? aptDataObj[2] : null;
              debug(JSON.stringify(aptDataObj));
              dfd.resolve(self.apptData);
            }
            else
              dfd.resolve(self.apptData);
        }
        else
        {
          debug("Already data saved");
          dfd.resolve(self.apptData);
        }
      }catch(e){
        debug("Exception in deDupe - NOMINEE");
        CommonService.hideLoading();
        dfd.resolve(self.apptData);
      }
          return dfd.promise;
    }

    var whereClauseObjTerm = {};
        whereClauseObjTerm.COMBO_ID = self.comboID;
        whereClauseObjTerm.AGENT_CD = agentCD;

    debug("data from database  -->"+JSON.stringify(ApptExistingData));
    if(!!ApptExistingData && !!ApptExistingData.appID){
        debug("data from database  -->"+JSON.stringify(ApptExistingData));
        self.apptData = ApptExistingData;
        this.setDeDupeData().then(function (resp) {
            CommonService.hideLoading();
        });
    }
    else
        {
          debug("no data from database");
          this.setDeDupeData().then(function (resp) {
              CommonService.hideLoading();
          });
        }

    if(ApptTermExistingData){
        debug("data from database  -->"+JSON.stringify(ApptTermExistingData));
        self.apptTermData = ApptTermExistingData;
    }
    else
        debug("no data from database");


     //save to DB...
    this.saveApptData = function(ApptForm){
		CommonService.showLoading();
    self.click = true;
        if(ApptForm.$invalid != true){
            self.apptData.appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
            self.apptData.username = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            self.apptData.apptCustID = "C98";
            self.apptTermData.username = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            self.apptTermData.apptCustID = "C98";
            self.apptTermData.comboID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
            debug("appt data is "+JSON.stringify(self.apptData));
            if(self.nomineeAgeFlag){
                //insert appt data..
                    ApptService.insertApptData(self.apptData).then(
                    function(insertAppt){
                    if(insertAppt){
                         if(self.nomineeTermAgeFlag){
                            ApptService.insertApptTermData(self.apptTermData).then(
                            function(insertAppt){
                                 debug("appointee data inserted");
                                 debug("MOVE TO NEXT PAGE");
                                 //update is screen completed flag..
                                 ApptService.updateScreenFlag(whereClauseObj);
                                 //timeline changes..
                                 CommonService.showLoading();
                                 AppTimeService.setActiveTab("paymentDetails");
                                 AppTimeService.isNomApptCompleted = true;
                                 //$rootScope.val = 60;
                                 AppTimeService.setProgressValue(60);
                                 $state.go("applicationForm.paymentDetails.bankdetails");
                            });
                         }
                         else{
                             debug("appointee data inserted");
                             debug("MOVE TO NEXT PAGE");
                             //update is screen completed flag..
                             ApptService.updateScreenFlag(whereClauseObj);
                             //timeline changes..
                             CommonService.showLoading();
                             AppTimeService.setActiveTab("paymentDetails");
                             AppTimeService.isNomApptCompleted = true;
                             //$rootScope.val = 60;
                             AppTimeService.setProgressValue(60);
                             $state.go("applicationForm.paymentDetails.bankdetails");
                         }

                    }else{
                         debug("insertion failed for appointee");
                    }//else appt data inserion..
                    }//function end appointee..
                 );//then end appointee..
            }
            else{
                    //insert appt data..
                    ApptService.insertApptTermData(self.apptTermData).then(
                    function(insertAppt){
                    if(insertAppt){

                         debug("appointee data inserted");
                         debug("MOVE TO NEXT PAGE");
                         //update is screen completed flag..
                         ApptService.updateScreenFlag(whereClauseObj);
                         //timeline changes..
                         CommonService.showLoading();
                         AppTimeService.setActiveTab("paymentDetails");
                         AppTimeService.isNomApptCompleted = true;
                         //$rootScope.val = 60;
                         AppTimeService.setProgressValue(60);
                         $state.go("applicationForm.paymentDetails.bankdetails");
                    }else{
                         debug("insertion failed for appointee");
                    }//else appt data inserion..
                    }//function end appointee..
                    );//then end appointee..
            }
        }else{
            navigator.notification.alert("Please fill all fields with valid data.",function(){CommonService.hideLoading();},"Application","OK");
        }
    }
    CommonService.hideLoading();
}]);

ApptModule.service('ApptService',['$q','CommonService','nomService','NomApptService','$filter',function($q,CommonService,nomService,NomApptService,$filter){
    debug("service called ");
    var self = this;
    var apptRelationDataArr = [];

    //get all the appointee relationship data..
    self.getApptRelationData = function(){
        var dfd = $q.defer();
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select nbfe_code,relationship_desc,mpproof_code,mpdoc_code,nbfe_reldesc from LP_APPOINTEE_RELATION where isactive = ? order by nbfe_code",["Y"],
                                function(tx,res){

                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            var apptRelationObj = {};
                                            apptRelationObj.APPT_NBFE_CODE = res.rows.item(i).NBFE_CODE;
                                            apptRelationObj.APPT_RELATIONSHIP_DESC = res.rows.item(i).RELATIONSHIP_DESC;
                                            apptRelationObj.APPT_MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                            apptRelationObj.APPT_MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                                            apptRelationObj.APPT_NBFE_RELDESC = res.rows.item(i).NBFE_RELDESC;
                                            apptRelationDataArr.push(apptRelationObj);
                                        }
                                        dfd.resolve(apptRelationDataArr);
                                    }
                                    else
                                        dfd.resolve(apptRelationDataArr);
                                },
                                function(tx,err){
                                    dfd.resolve(apptRelationDataArr);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(apptRelationDataArr);
                        },null
                    );
        return dfd.promise;
    }

    //insert appointee data..
    self.insertApptData = function(apptData){

        var dfd = $q.defer();
        var query = "INSERT OR REPLACE INTO LP_APP_NOM_CONTACT_SCRN_F values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        debug("age of appointee is "+CommonService.formatDobToDb(apptData.dob));
        var parameters = [
        apptData.appID,
        apptData.username,
        apptData.apptCustID,
        null,
        apptData.FirstName,
        apptData.MiddleName,
        apptData.LastName,
        CommonService.formatDobToDb(apptData.dob),
        apptData.Relation.APPT_RELATIONSHIP_DESC,
        apptData.Relation.APPT_NBFE_CODE,
        CommonService.getCurrDate(),
        1,
        apptData.Gen,
        apptData.Gen.substring(0,1),
        null,
        null,null];
        debug("parameters "+parameters);

        CommonService.transaction(db,
                    function(tx){
                    CommonService.executeSql(tx,query,parameters,
                                                function(tx,res){
                                                    debug("query executed successfully for appt");
                                                    //inserted = true;
                                                    dfd.resolve(true);
                                                },
                                                function(tx,err){
                                                    debug("query execution failed for appt"+err);
                                                    dfd.resolve(false);
                                                },null
                                            );

                    },
                    function(tx,err){
                        debug("transaction error"+err.message);
                        dfd.resolve(false);
                    },
        null);

        return dfd.promise;
    }

    //insert appointee data for combo..
    self.insertApptTermData = function(apptTermData){

        var dfd = $q.defer();
        var query = "INSERT OR REPLACE INTO LP_COMBO_NOM_CONTACT_SCRN_F values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        debug("age of appointee is "+CommonService.formatDobToDb(apptTermData.dob));
        var parameters = [
        apptTermData.comboID,
        apptTermData.username,
        apptTermData.apptCustID,
        null,
        apptTermData.FirstName,
        apptTermData.MiddleName,
        apptTermData.LastName,
        CommonService.formatDobToDb(apptTermData.dob),
        apptTermData.Relation.APPT_RELATIONSHIP_DESC,
        apptTermData.Relation.APPT_NBFE_CODE,
        CommonService.getCurrDate(),
        1,
        apptTermData.Gen,
        apptTermData.Gen.substring(0,1),
        null,
        null];
        debug("parameters "+parameters);

        CommonService.transaction(db,
                    function(tx){
                    CommonService.executeSql(tx,query,parameters,
                                                function(tx,res){
                                                    debug("query executed successfully for appt");
                                                    //inserted = true;
                                                    dfd.resolve(true);
                                                },
                                                function(tx,err){
                                                    debug("query execution failed for appt"+err);
                                                    dfd.resolve(false);
                                                },null
                                            );

                    },
                    function(tx,err){
                        debug("transaction error"+err.message);
                        dfd.resolve(false);
                    },
        null);

        return dfd.promise;
    }

    //get all the existing data from db for appointee...
    self.existingApptData = function(apptID,agentCD){
        var dfd = $q.defer();
        var nomApptDBObj = {};
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select APPLICATION_ID,AGENT_CD,cust_type,first_name,middle_name,last_name,birth_date,relationship_desc,relationship_id,seq_no,gender,gender_code,percentage from LP_APP_NOM_CONTACT_SCRN_F where application_id=? and agent_cd = ? and cust_type=?",[apptID,agentCD,"C98"],
                                function(tx,res){
                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            (function(i){
                                                nomApptDBObj.appID = res.rows.item(i).APPLICATION_ID;
                                                nomApptDBObj.username = res.rows.item(i).AGENT_CD;
                                                nomApptDBObj.apptCustID = res.rows.item(i).CUST_TYPE;
                                                nomApptDBObj.FirstName = res.rows.item(i).FIRST_NAME;
                                                nomApptDBObj.MiddleName = res.rows.item(i).MIDDLE_NAME;
                                                nomApptDBObj.LastName = res.rows.item(i).LAST_NAME;
                                                nomApptDBObj.seq_no = res.rows.item(i).SEQ_NO;
                                                nomApptDBObj.Gen = res.rows.item(i).GENDER;
                                                nomApptDBObj.gender_code = res.rows.item(i).GENDER_CODE;
                                                nomApptDBObj.dob = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                                                self.getApptRelationData().then(
                                                   function(listOfAppt){
                                                       nomApptDBObj.Relation = $filter('filter')(listOfAppt, {"APPT_NBFE_CODE": res.rows.item(i).RELATIONSHIP_ID})[0];
                                                       debug("Relation data is "+nomApptDBObj.Relation);
                                                   }
                                                );
                                           }(i))


                                        }
                                        dfd.resolve(nomApptDBObj);
                                    }
                                    else
                                        dfd.resolve(nomApptDBObj);
                                },
                                function(tx,err){
                                    dfd.resolve(nomApptDBObj);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(nomApptDBObj);
                        },null
                    );
        return dfd.promise;
    };

    self.existingTermApptData = function(comboID,agentCD){
        var dfd = $q.defer();
        var nomApptDBObj = {};
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select COMBO_ID,AGENT_CD,cust_type,first_name,middle_name,last_name,birth_date,relationship_desc,relationship_id,seq_no,gender,gender_code,percentage from LP_COMBO_NOM_CONTACT_SCRN_F where COMBO_ID=? and agent_cd = ? and cust_type=?",[comboID,agentCD,"C98"],
                                function(tx,res){
                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            (function(i){
                                                nomApptDBObj.comboID = res.rows.item(i).COMBO_ID;
                                                nomApptDBObj.username = res.rows.item(i).AGENT_CD;
                                                nomApptDBObj.apptCustID = res.rows.item(i).CUST_TYPE;
                                                nomApptDBObj.FirstName = res.rows.item(i).FIRST_NAME;
                                                nomApptDBObj.MiddleName = res.rows.item(i).MIDDLE_NAME;
                                                nomApptDBObj.LastName = res.rows.item(i).LAST_NAME;
                                                nomApptDBObj.seq_no = res.rows.item(i).SEQ_NO;
                                                nomApptDBObj.Gen = res.rows.item(i).GENDER;
                                                nomApptDBObj.gender_code = res.rows.item(i).GENDER_CODE;
                                                nomApptDBObj.dob = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                                                self.getApptRelationData().then(
                                                   function(listOfAppt){
                                                       nomApptDBObj.Relation = $filter('filter')(listOfAppt, {"APPT_NBFE_CODE": res.rows.item(i).RELATIONSHIP_ID})[0];
                                                       debug("Relation data is "+nomApptDBObj.Relation);
                                                   }
                                                );
                                           }(i))


                                        }
                                        dfd.resolve(nomApptDBObj);
                                    }
                                    else
                                        dfd.resolve(nomApptDBObj);
                                },
                                function(tx,err){
                                    dfd.resolve(nomApptDBObj);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(nomApptDBObj);
                        },null
                    );
        return dfd.promise;
    };
    self.updateScreenFlag = function(whereClauseObj){
        CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN6_COMPLETED" :"Y"}, whereClauseObj).then(
             function(res){
                 debug("IS_SCREEN6_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
             }
        );
    }
}]);


/*
//insert Nominee and Appointee Data..
    self.insertNomApptData = function(apptData,NomData){
        var dfd =$q.defer();
        for(var i=0;i<NomData.length;i++){
            var nomObj = NomData[i];
            if(angular.equals(nomObj.Relation,"MWP")){
                nomService.getMWPDetails("MWP").then(
                    function(mwpDet){
                        nomService.nomDataInsert(i,mwpDet,nomObj).then(
                            function(inserted){
                                if(inserted){
                                    debug("nominee data inserted");
                                    self.insertApptData(apptData).then(
                                        function(insertApp){
                                            if(insertApp){
                                                debug("appointee data inserted");
                                                 //debug("MOVE TO NEXT PAGE");
                                                 dfd.resolve(true);
                                            }else{
                                                 dfd.resolve(false);
                                            }//else appt data insertion..
                                        }//function end appt
                                    );//then end appt
                                }else{
                                    dfd.resolve(false);
                                }//else nominee insertion..
                            }//function end nominee
                        );// then end nominee..
                    }//function end mwp
                );//then end MWP..
            }
            else{
                nomService.nomDataInsert(i,null,nomObj).then(
                    function(inserted){
                        if(inserted){
                            debug("nominee data inserted");
                            dfd.resolve(true);
                        }else{
                            dfd.resolve(false);
                        }//else nominee insertion.
                    }//function end nominee..
                );//then end nominee..
            }//else
        }//for
        return dfd.promise;
    }
*/
