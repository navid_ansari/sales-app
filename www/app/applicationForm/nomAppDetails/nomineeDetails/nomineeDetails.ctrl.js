nomModule.controller('NomCntrl',['$q','$state','nomRelationData','nomService','NomApptService','ApplicationFormDataService','LoginService','NomExistingData','ExistingAppMainData','CommonService','LoadApplicationScreenData','sisData','AppTimeService','nomAppInfoService','TermExistingData','DeDupeData',function($q, $state,nomRelationData,nomService,NomApptService,ApplicationFormDataService,LoginService,NomExistingData,ExistingAppMainData,CommonService,LoadApplicationScreenData,sisData,AppTimeService,nomAppInfoService,TermExistingData, DeDupeData){
        debug("nomineee  "+JSON.stringify(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST));
        var self = this;
        var ifIRaksha = false;

        self.comboID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
        debug("combo id in nominee section "+self.comboID);


    /* Header Hight Calculator */
    // setTimeout(function(){
    //     var appOutputGetHeight = $(".customer-details .fixed-bar").height();
    //     $(".customer-details .custom-position").css({top:appOutputGetHeight + "px"});
    // });
    /* End Header Hight Calculator */



        var pglID = sisData.sisMainData.PGL_ID;
        debug("IRAKSHA PRODUCT --->"+pglID);
        nomAppInfoService.setActiveTab("nominee");

        if(pglID == "185" || pglID == "191"){
            debug("IRAKSHA PRODUCT --->"+pglID);
            ifIRaksha = true;
        }

		this.maritalStatus = ApplicationFormDataService.applicationFormBean.personalDetBean.INSURED_MARITAL_STATUS;
		debug("Insured Marital status: " + JSON.stringify(this.maritalStatus));
        self.click = false;
        var appID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;
        var agentCD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

        debug("appid in nominee details "+appID);
        self.nomTermFlag="N";
        var whereClauseObj1 = {};
        whereClauseObj1.APPLICATION_ID = appID;
        whereClauseObj1.AGENT_CD = agentCD;

		//changes 27th jan
		var whereClauseObjDel = {};
        whereClauseObjDel.APPLICATION_ID = appID;
        whereClauseObjDel.AGENT_CD = agentCD;
		whereClauseObjDel.CUST_TYPE = "C05";

        var whereClauseObjTerm = {};
        whereClauseObjTerm.COMBO_ID = self.comboID;
        whereClauseObjTerm.AGENT_CD = agentCD;

        //create blank object to add docids..
        ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
            Reflex :{
                Term:{},
                Insured:{},
                Proposer:{}
            },
            NonReflex :{
                Term:{},
                Insured:{},
                Proposer:{}
            }
        };

        //check if object to add ids is blank...
        //else assign the object from previous page..
        //29th june..
        if(!!ExistingAppMainData && !!ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST && Object.keys(ExistingAppMainData.applicationPersonalInfo.Insured).length!=0)
        {
            ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
            debug("existing data "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        }
        else{
            debug("exisiting object is blank")
        }
        // Set DeDupe Data
        this.prepareDeDupeData = function (nm, seq) {
          var nomApptDBObj = {};
              try{
                  nomApptDBObj.appID = appID;
                  nomApptDBObj.username = agentCD;
                  nomApptDBObj.nomCustID = "C05";
                  var name = nm[0].trim();
                  var nameArr = name.split(" ");
                  nomApptDBObj.FirstName = nameArr[0];
                  if(nameArr.length>2){
                    nomApptDBObj.MiddleName = "";
                    for(var i=1; i<(nameArr.length-1); i++){
                      nomApptDBObj.MiddleName += nameArr[i];
                      if(i!=(nameArr.length-2))
                        nomApptDBObj.MiddleName += " ";
                    }
                    nomApptDBObj.LastName = nameArr[(nameArr.length - 1)];
                  }
                  var db = nm[1].substring(6,8) +"-"+ nm[1].substring(4,6) +"-"+ nm[1].substring(0,4);
                  nomApptDBObj.dob = CommonService.formatDobFromDb(db);
                  debug("Dupe : Date of birth "+nomApptDBObj.dob);
                  nomApptDBObj.seq_no = seq;
                  nomApptDBObj.Gen = (!!nm[2]) ? ((nm[2] == 'M') ? 'Male': 'Female') : null;
                  nomApptDBObj.gender_code = (!!nm[2]) ? nm[2] : null;
                  var per = parseInt(nm[4])/100;
                  nomApptDBObj.Percent = per;

                  return nomApptDBObj;
              }catch(e){
                  debug("Exception in deDupe - NOMINEE");
                  CommonService.hideLoading();
                  nomApptDBObj = {};
                  return nomApptDBObj;
              }
        }
        this.setDeDupeData = function () {
          var dfd = $q.defer();
          try{
            if(!!DeDupeData && !!DeDupeData.RESULT_APPDATA && DeDupeData.RESP == 'S' && !!DeDupeData.RESULT_APPDATA.NOMINEE_DETAILS && NomExistingData.length <=0){
              self.NomineeArr = [];
                CommonService.showLoading("Loading Existing Data..");
                var nominee = DeDupeData.RESULT_APPDATA.NOMINEE_DETAILS;
                var nomDetails = (!!nominee) ? nominee.split(",") : [""];
                var nomDataObj = [];
                for(var k = 0; k< nomDetails.length; k++)
                {
                  nomDataObj[k] = {};
                  nomDataObj[k].Data = (!!nomDetails && nomDetails[k]) ? nomDetails[k].split("*") : [""];
                }

                debug(JSON.stringify(nomDataObj));
                if(!!nomDetails && nomDetails.length>0 && !!nomDataObj && nomDataObj.length>0){
                  for(var m = 0; m<nomDataObj.length; m++)
                    {
                      if(nomDataObj[m].Data.length>1){
                        var Nominee = {};
                        Nominee = self.prepareDeDupeData(nomDataObj[m].Data, m+1);
                        self.NomineeArr.push(Nominee);
                      }
                   }
                }
                dfd.resolve(self.NomineeArr);
            }
            else
            {
              debug("Already data saved");
              dfd.resolve(self.NomineeArr);
            }
          }catch(e){
            debug("Exception in deDupe - NOMINEE");
            CommonService.hideLoading();
            dfd.resolve(self.NomineeArr);
          }
              return dfd.promise;
        }
        //get value from DB
        //else assign default values..
        if(NomExistingData.length > 0){
            debug("data is "+JSON.stringify(NomExistingData));
            this.NomineeArr = NomExistingData;
            self.nomTermFlag=NomExistingData[0].nomTermFlag;
        }
        else{
            debug("no data found in database");
            var Nominee = {};
            Nominee.nomID = 1;
            Nominee.appID = appID;
            Nominee.username = agentCD;
            Nominee.nomCustID = "C05";
            Nominee.apptCustID = "C98";
            this.NomineeArr = [Nominee];
            this.setDeDupeData().then(function (resp) {
        	      CommonService.hideLoading();
        	  });
        }

        //Term Attach
         if(TermExistingData.length > 0){
            debug("data is "+JSON.stringify(NomExistingData));
            this.TermNomineeArr = TermExistingData;
        }
        else{
            //self.nomTermFlag="Y";
            var TNominee = {};
            TNominee.nomID = 1;
            TNominee.comboID = self.comboID; // Term AppID
            TNominee.username = agentCD;
            TNominee.nomCustID = "C05";
            TNominee.apptCustID = "C98";
            this.TermNomineeArr = [TNominee];
        }
        //clear the percentage error message...
        //Base
        this.percentCalled = function(){
            self.totalErr = "";
        }
        //Term..
        this.TpercentCalled = function(){
            self.totalTErr = "";
        }

        //get all the nominee relation dropdown...
        this.nomRelationData = nomRelationData;

        //insert data or move to appointee...
        this.save = function(nomForm){
			CommonService.showLoading();
            self.click = true;
            if(nomForm.$invalid != true){
                var totalPercent = 0;
                //add the percent of all the nominees...
                for(var cnt=0;cnt<this.NomineeArr.length;cnt++){
                    var objnom = this.NomineeArr[cnt];
                    var percentToAdd = objnom.Percent;
                    totalPercent = parseInt(totalPercent) + parseInt(percentToAdd);
                }
                //all percentage should be equal 100..
                //First delete records and then insert records...
                if(totalPercent == 100)
                    self.dataDelete();
                else{
                    self.totalErr = "Total percentage should be 100%";
					CommonService.hideLoading();
                    //navigator.notification.alert("Total percentage should be 100%",null,"Application","OK");
                }
            }else{
                navigator.notification.alert("Please fill all fields with valid data",function(){CommonService.hideLoading();},"Application","OK");
            }
        };

        //saving the term plan data...
        this.savingTermData = function(){
            debug("Term FLag"+self.nomTermFlag);
            if(self.nomTermFlag=="Y"){
                self.dataTermNormDelete();
            }
            else{

                var totalPercent=0;
                for(var cnt=0;cnt<this.TermNomineeArr.length;cnt++){
                    var objTnom = this.TermNomineeArr[cnt];
                    var percentToAdd = objTnom.Percent;
                    totalPercent = parseInt(totalPercent) + parseInt(percentToAdd);
                    debug("Term Count "+cnt);
                }
                debug("Term Nominee Percent");
                debug(totalPercent);
                //all percentage should be equal 100..
                //First delete records and then insert records...
                if(totalPercent == 100)
                    self.dataTermNormDelete();
                else{
                    self.totalTErr = "Total percentage should be 100%";
                    //navigator.notification.alert("Total percentage should be 100%",function(){CommonService.hideLoading();},"Application","OK");
                }
            }

        }

        //delete record and then call insert query...
		//changes 27th jan
        this.dataDelete = function(){
             CommonService.deleteRecords(db, 'LP_APP_NOM_CONTACT_SCRN_F', whereClauseObjDel).then(
                function(deleted){
                     debug("deleted all records");
                     self.dumpNomData();
                }
             );
        }

        //term plan delete..CHANGES
		//changes 27th jan
        this.dataTermNormDelete = function(){
            CommonService.deleteRecords(db, 'LP_COMBO_NOM_CONTACT_SCRN_F', whereClauseObjDel).then(
                function(deleted){
                     debug("deleted all records");
                     self.dumpTNomData();
                }
            );
        }

        //save to DB..
        this.dumpNomData = function(){
            // self.NomineeArr.push({'nomTermFlag':self.nomTermFlag});
            nomService.insertOnlyNomData(self.NomineeArr,self.nomTermFlag).then(
                function(inserted){
                    debug("after nom data insertion"+inserted);
                    if(inserted){
                        //get the age if below 18...
                        nomService.getApptAgeFlag(self.NomineeArr).then(
                            function(belowAge){
                                if(belowAge){
                                    nomService.setNomineeBelow18(true);
                                    if(!!self.comboID){
                                        debug("Combo Exist Insert into term");
                                        self.savingTermData();
                                    }else{
                                        debug("appointee set flag "+belowAge);
                                        nomAppInfoService.setActiveTab("appointee");
                                        $state.go("applicationForm.nomAppDetails.appointeeDetails");
                                    }
                                }else{
                                    nomService.setNomineeBelow18(false);
                                    nomService.setNomineeTermBelow18(false);
                                    if(!!self.comboID){
                                        debug("Combo Exist Insert into term");
                                        self.savingTermData();
                                    }else{
										debug("DOCSLIST in NOMINIEE "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
                                        nomService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj1);
                                        //timeline changes..
                                        CommonService.showLoading();
                                        AppTimeService.setActiveTab("paymentDetails");
                                        AppTimeService.isNomApptCompleted = true;
                                        //$rootScope.val = 60;
                                        AppTimeService.setProgressValue(60);
                                        $state.go("applicationForm.paymentDetails.bankdetails");
                                    }

                                }
                            }
                        );
                    }
                }
            );
        }

        this.dumpTNomData = function(){
            debug("Term FLag"+self.nomTermFlag);
            if(self.nomTermFlag=="Y"){

            self.TermNomineeArr = self.NomineeArr;
            debug(self.TermNomineeArr);
            for(var i=0; i<self.TermNomineeArr.length; i++){
                self.TermNomineeArr[i]['comboID'] = self.comboID;

            }
            debug(self.TermNomineeArr);


               nomService.insertTermNomData(self.NomineeArr).then(
                       function(inserted){
                           debug("after term nom data insertion"+inserted);
                           if(inserted){
                               nomService.getApptAgeFlag(self.NomineeArr).then(
                                   function(belowAge){
                                       console.log("Nominee Age"+ nomService.isNomineeBelow18());
                                       console.log("TermAge Age"+belowAge);
                                       if(nomService.isNomineeBelow18()){
                                            if(belowAge){
                                                nomService.setNomineeTermBelow18(true);
                                                debug("term term set flag "+nomService.setNomineeTermBelow18());
                                            }
                                            else{
                                                nomService.setNomineeTermBelow18(false);
                                            }
                                           nomAppInfoService.setActiveTab("appointee");
                                           $state.go("applicationForm.nomAppDetails.appointeeDetails");
                                       }
                                       else if(belowAge){
                                            nomService.setNomineeTermBelow18(true);
                                           debug("term term set flag "+nomService.setNomineeTermBelow18());
                                           nomAppInfoService.setActiveTab("appointee");
                                           $state.go("applicationForm.nomAppDetails.appointeeDetails");
                                       }else{
                                               nomService.setNomineeBelow18(false);
                                               nomService.setNomineeTermBelow18(false);
                                               nomService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj1);
                                               //timeline changes..
                                               CommonService.showLoading();
                                               AppTimeService.setActiveTab("paymentDetails");
                                               AppTimeService.isNomApptCompleted = true;
                                               //$rootScope.val = 60;
                                               AppTimeService.setProgressValue(60);
                                               $state.go("applicationForm.paymentDetails.bankdetails");

                                       }
                                   }
                               )
                           }
                       }
                   )
            }
            else{
                nomService.insertTermNomData(self.TermNomineeArr).then(
                    function(inserted){
                        debug("after term nom data insertion"+inserted);
                        if(inserted){
                            nomService.getApptAgeFlag(self.TermNomineeArr).then(
                                function(belowAge){

                                    console.log("Nominee Age"+nomService.isNomineeBelow18());
                                    console.log("TermAge Age"+belowAge);
                                    if(nomService.isNomineeBelow18()){
                                        if(belowAge){
                                                nomService.setNomineeTermBelow18(true);
                                                debug("term term set flag "+nomService.isNomineeTermBelow18());
                                        }
                                        else{
                                                nomService.setNomineeTermBelow18(false);
                                        }
                                        nomAppInfoService.setActiveTab("appointee");
                                       $state.go("applicationForm.nomAppDetails.appointeeDetails");
                                    }
                                    else if(belowAge){
                                        nomService.setNomineeTermBelow18(true);
                                        debug("term term set flag "+nomService.isNomineeTermBelow18());
                                        nomAppInfoService.setActiveTab("appointee");
                                        $state.go("applicationForm.nomAppDetails.appointeeDetails");
                                    }else{
                                        nomService.setNomineeBelow18(false);
                                        nomService.setNomineeTermBelow18(false);
                                        nomService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj1);
                                       //timeline changes..
                                       CommonService.showLoading();
                                       AppTimeService.setActiveTab("paymentDetails");
                                       AppTimeService.isNomApptCompleted = true;
                                       //$rootScope.val = 60;
                                       AppTimeService.setProgressValue(60);
                                       $state.go("applicationForm.paymentDetails.bankdetails");
                                    }
                                }
                            )
                        }
                    }
                )
            }
        }



        //delete item..
        this.deleteItem = function(index){
             if(this.NomineeArr.length > 1)
                this.NomineeArr.splice(index,1);
        };

        //delete item term..
        this.deleteTermItem = function(index){
             if(this.TermNomineeArr.length > 1)
                this.TermNomineeArr.splice(index,1);
        };

        //relation ship change data set..
        this.RelationChange = function(cnt){

             var relationData = self.NomineeArr[cnt];
             debug("CNT "+JSON.stringify(relationData));

             debug("Count is "+cnt);

             var Gender = relationData.Gen;
             debug("gender selected is "+Gender);
             ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term = {};
             ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term = {};

            if((Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Mother") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Daughter") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Mother") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Daughter") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Sister") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="MWP")){
                debug("CHECK TO FEMALE");
                relationData.Gen = "Female";
            }
            else if((Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Father") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Son") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Father") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand son") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Brother")){
                debug("CHECK TO MALE");
                relationData.Gen = "Male";
            }
            else
                debug("ITS SPOUSE BOTH ALLOWED");

              ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation = {};
              ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation = {};

             if(!!relationData.Relation.NOM_MPPROOF_CODE){
                debug("MWP selected");
                nomService.getMWPDetails(relationData).then(
                    function(data){
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term.NomRelation = {};
                        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.NomRelation = {};
                        navigator.notification.alert(data.MPPROOF_DESC + " Questionaire need to be required",function(){CommonService.hideLoading();},"Application","OK");
                         if(!self.comboID){
                            if(data.DIGITAL_HTML_FLAG == "Y"){
                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation.DOC_ID = data.DOC_ID;
								}
                            else{
                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation.DOC_ID = data.DOC_ID;
								}
                         }
                         else{
                            if(data.DIGITAL_HTML_FLAG == "Y"){
                                 ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term.NomRelation.DOC_ID = data.DOC_ID;
								 }
                            else{
                                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.NomRelation.DOC_ID = data.DOC_ID;
								}
                         }
						 debug("DOCSLIST in NOMINIEE in page show "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

                    }
                );
             }
             else{
                debug("MWP no selected");
                if(!self.comboID){
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation = null;
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation = null;
                 }
                 else{
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Term.NomRelation = null;
                    ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.NomRelation = null;
                 }

             }
        };


        //relation ship change data set for TermPlan..
        this.RelationChangeTerm = function(cnt){

             var relationData = self.TermNomineeArr[cnt];
             debug("CNT "+JSON.stringify(relationData));

             debug("Count is "+cnt);

             var Gender = relationData.Gen;
             debug("gender selected is "+Gender);

            if((Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Mother") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Daughter") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Mother") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Daughter") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Sister") ||
                (Gender=="Male" && relationData.Relation.NOM_RELATIONSHIP_DESC=="MWP")){
                debug("CHECK TO FEMALE");
                relationData.Gen = "Female";
            }
            else if((Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Father") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Son") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand Father") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Grand son") ||
                (Gender=="Female" && relationData.Relation.NOM_RELATIONSHIP_DESC=="Brother")){
                debug("CHECK TO MALE");
                relationData.Gen = "Male";
            }
            else
                debug("ITS SPOUSE BOTH ALLOWED");

             ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation = {};
             ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation = {};

             if(!!relationData.Relation.NOM_MPPROOF_CODE){
                debug("MWP selected");
                nomService.getMWPDetails(relationData).then(
                    function(data){
						console.log("Data is "+JSON.stringify(data));
                        navigator.notification.alert(data.MPPROOF_DESC + " Questionaire need to be required",function(){CommonService.hideLoading();},"Application","OK");
                        if(data.DIGITAL_HTML_FLAG == "Y"){
                            ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation.DOC_ID = data.DOC_ID;
							debug("DOCSLIST in NOMINIEE "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation));
							}
                        else{
                            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation.DOC_ID = data.DOC_ID;
							debug("DOCSLIST in NOMINIEE "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation));
							}
                    }
                );
             }
             else{
                debug("MWP no selected");
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.Reflex.Insured.NomRelation = null;
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.NomRelation = null;
             }
        };
        //add new item..
        this.AddMore = function(){
        self.click = true;
            if(ifIRaksha){
                navigator.notification.alert("IRAKSHA ::   you cannot add more than one nominee",function(){CommonService.hideLoading();},"Application","OK");
            }else{
                if(this.NomineeArr.length < 5){
                    var newItemNo = this.NomineeArr.length+1;
                    this.NomineeArr.push(
                    {
                        'nomID':newItemNo,
                         'appID' :appID,
                         'username' :agentCD,
                         'nomCustID' :"C05",
                         'apptCustID' :"C98"
                    });
                }else{
                    navigator.notification.alert("cannot add more than 5 Nominee",function(){CommonService.hideLoading();},"Application","OK");
                }
            }
        };

        //add new term item..
        this.AddMoreTerm = function(){
        self.click = true;
            if(ifIRaksha){
                navigator.notification.alert("IRAKSHA ::   you cannot add more than one nominee for term product",function(){CommonService.hideLoading();},"Application","OK");
            }else{
                if(this.TermNomineeArr.length < 5){
                    var newItemNo = this.TermNomineeArr.length+1;
                    this.TermNomineeArr.push(
                    {
                        'nomID':newItemNo,
                         'comboID' : self.comboID,
                         'username' :agentCD,
                         'nomCustID' :"C05",
                         'apptCustID' :"C98"
                    });
                }else{
                    navigator.notification.alert("cannot add more than 5 Nominee Term Product",function(){CommonService.hideLoading();},"Application","OK");
                }
            }
        };
    CommonService.hideLoading();
}]);


nomModule.service('nomService',['$q','$rootScope','CommonService','NomApptService','$state','$filter',function($q,$rootScope,CommonService,NomApptService,$state,$filter){

    var self = this;
    var nomineeAgeFlag=false;
    var nomineeTermAgeFlag=false;
    //update docid and is screen completion flag..
    this.updateFlagAndObject = function(data,whereClauseObj1){
        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(data)}, whereClauseObj1).then(
            function(res){
                CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN6_COMPLETED" :"Y"}, whereClauseObj1).then(
                    function(res){
                        debug("IS_SCREEN6_COMPLETED update success !!"+JSON.stringify(whereClauseObj1));
                    }
                );
            }
        );
    }

    //get data for nominee from nominee table which are active
    this.getNomRelationData = function(){
        var dfd = $q.defer();
        CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,"select nbfe_code,relationship_desc,mpproof_code,mpdoc_code from LP_NOMINEE_RELATION where isactive = ? order by nbfe_code",["Y"],
                                function(tx,res){
                                    var nomRelationDataArr = [];
                                    debug("nom drop down res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        for(var i=0;i<res.rows.length;i++){
                                            var nomRelationObj = {};
                                            nomRelationObj.NOM_NBFE_CODE = res.rows.item(i).NBFE_CODE;
                                            nomRelationObj.NOM_RELATIONSHIP_DESC = res.rows.item(i).RELATIONSHIP_DESC;
                                            nomRelationObj.NOM_MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
                                            nomRelationObj.NOM_MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
                                            nomRelationDataArr.push(nomRelationObj);
                                        }
                                        dfd.resolve(nomRelationDataArr);
                                    }
                                    else
                                        dfd.resolve(null);
                                },
                                function(tx,err){
                                    dfd.resolve(null);
                                }
                            );
                        },
                        function(err){
                            dfd.resolve(null);
                        },null
                    );
        return dfd.promise;
    };

    //get the docid and isdigitalflag for MWP from database.
    self.getMWPDetails = function(MWP){
        var dfd = $q.defer();
        debug(MWP.Relation.NOM_MPDOC_CODE+"MWP details"+MWP.Relation.NOM_MPPROOF_CODE);
        var MWPDetailObj = {};
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select DOC_ID,MPPROOF_DESC,DIGITAL_HTML_FLAG from LP_DOC_PROOF_MASTER where MPDOC_CODE =? and MPPROOF_CODE = ? and CUSTOMER_CATEGORY=?",[MWP.Relation.NOM_MPPROOF_CODE,MWP.Relation.NOM_MPDOC_CODE,'NM'],
                    function(tx,res){
                        debug("MWP details res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            MWPDetailObj.DOC_ID = res.rows.item(0).DOC_ID;
                            MWPDetailObj.MPPROOF_DESC = res.rows.item(0).MPPROOF_DESC;
                            MWPDetailObj.DIGITAL_HTML_FLAG = res.rows.item(0).DIGITAL_HTML_FLAG;
                            dfd.resolve(MWPDetailObj);
                        }
                        else
                            dfd.resolve(MWPDetailObj);
                    },
                    function(tx,err){
                        dfd.resolve(MWPDetailObj);
                    }
                );
            },
            function(err){
                dfd.resolve(MWPDetailObj);
            },null
        );
        return dfd.promise;
    };

    //insert the nominee details after all the validation with or without MWP...
    this.nomDataInsert = function(cnt,data,nomObj){
        var dfd = $q.defer();
        debug("inserting nom data started"+JSON.stringify(nomObj));
        var docuID = null;
        var digHtml = null;

        var query = "INSERT OR REPLACE INTO LP_APP_NOM_CONTACT_SCRN_F values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        var parameters = [
        nomObj.appID,
        nomObj.username,
        nomObj.nomCustID,
        null,
        nomObj.FirstName,
        nomObj.MiddleName,
        nomObj.LastName,
        CommonService.formatDobToDb(nomObj.dob),
        nomObj.Relation.NOM_RELATIONSHIP_DESC,
        nomObj.Relation.NOM_NBFE_CODE,
        CommonService.getCurrDate(),
        cnt+1,
        nomObj.Gen,
        nomObj.Gen.substring(0,1),
        nomObj.Percent,
        null,
        nomObj.nomTermFlag];
        debug("parameters "+parameters);

        debug("the insert DOB "+CommonService.formatDobToDb(nomObj.dob));
        debug("Flag"+nomObj.nomTermFlag);
        CommonService.transaction(db,
                    function(tx){
                    CommonService.executeSql(tx,query,parameters,
                                                function(tx,res){
                                                    debug("query executed successfully");
                                                    //inserted = true;
                                                    dfd.resolve(true);
                                                },
                                                function(tx,err){
                                                    debug("query execution failed"+err);
                                                    dfd.resolve(false);
                                                },null
                                            );

                    },
                    function(tx,err){
                        debug("transaction error"+err.message);
                        dfd.resolve(false);
                    },
        null);

        return dfd.promise;
    };


//insert the term details after all the validation with or without MWP...
    this.termDataInsert = function(cnt,data,termObj){
        var dfd = $q.defer();
        debug("inserting term data started"+JSON.stringify(termObj));
        var docuID = null;
        var digHtml = null;

        var query = "INSERT OR REPLACE INTO LP_COMBO_NOM_CONTACT_SCRN_F values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        var parameters = [
        termObj.comboID,
        termObj.username,
        termObj.nomCustID,
        null,
        termObj.FirstName,
        termObj.MiddleName,
        termObj.LastName,
        CommonService.formatDobToDb(termObj.dob),
        termObj.Relation.NOM_RELATIONSHIP_DESC,
        termObj.Relation.NOM_NBFE_CODE,
        CommonService.getCurrDate(),
        cnt+1,
        termObj.Gen,
        termObj.Gen.substring(0,1),
        termObj.Percent,
        null];
        debug("parameters "+parameters);

        debug("the insert DOB "+CommonService.formatDobToDb(termObj.dob));

        CommonService.transaction(db,
                    function(tx){
                    CommonService.executeSql(tx,query,parameters,
                                                function(tx,res){
                                                    debug("query executed successfully");
                                                    //inserted = true;
                                                    dfd.resolve(true);
                                                },
                                                function(tx,err){
                                                    debug("query execution failed"+err);
                                                    dfd.resolve(false);
                                                },null
                                            );

                    },
                    function(tx,err){
                        debug("transaction error"+err.message);
                        dfd.resolve(false);
                    },
        null);

        return dfd.promise;
    };

    //insert nom data outer function..
    this.insertOnlyNomData = function(nomArr,flag){
        //var flag = false;
        var dfd = $q.defer();
		var row = 0;
        for(var cnt = 0 ; cnt < nomArr.length; cnt++){
            var nomObj = nomArr[cnt];
            nomObj.nomTermFlag=flag;
            self.nomDataInsert(cnt,null,nomObj).then(
                function(inserted){
                    //debug("inserted nom data in nom only function    "+ cnt);
                    //debug("length is    "+(nomArr.length - 1);
                    if(row == nomArr.length-1){
                        dfd.resolve(true);
                    }
					row++;
                }
            );
        }

        return dfd.promise;
    }
    //insert term data outer function..
    this.insertTermNomData = function(termArr){
            //var flag = false;
            var dfd = $q.defer();
            var rows = 0;
            for(var cnt = 0 ; cnt < termArr.length; cnt++){
                var termObj = termArr[cnt];
                self.termDataInsert(cnt,null,termObj).then(
                    function(inserted){
                        //debug("inserted nom data in nom only function    "+ cnt);
                        //debug("length is    "+(nomArr.length - 1);
                        if(rows == termArr.length-1){
                            dfd.resolve(true);
                        }
                        rows++;
                    }
                );
            }

            return dfd.promise;
        }

    //check if any of the nominee is below 18..
    this.getApptAgeFlag = function(nomDataArr){
        var dfd = $q.defer();
        var flag = false;
        if(nomDataArr !== null){
            for(var cnt =0 ; cnt < nomDataArr.length; cnt++){
                var nomObj = nomDataArr[cnt];
                var age = CommonService.getAge(CommonService.formatDobToDb(nomObj.dob));
                debug("age is "+age);
                if(age < 18){
                    flag = true;
                }
            }
            dfd.resolve(flag);
        }
        return dfd.promise;
    };

    //get the existing data..
    this.existingData = function(appID,agentCD){
       var dfd = $q.defer();
       var nomApptDBData = [];
       CommonService.transaction(db,
                       function(tx){
                           CommonService.executeSql(tx,"select application_id,agent_cd,cust_type,first_name,middle_name,last_name,birth_date,relationship_desc,relationship_id,seq_no,gender,gender_code,percentage,same_nominee_for_term from LP_APP_NOM_CONTACT_SCRN_F where application_id=? and agent_cd = ? and cust_type=?",[appID,agentCD,"C05"],
                               function(tx,res){
                                   debug("select nom data res.rows.length: " + res.rows.length);
                                   if(!!res && res.rows.length>0){
                                       for(var i=0;i<res.rows.length;i++){

                                           (function(i){
                                              var nomApptDBObj = {};
                                              nomApptDBObj.appID = res.rows.item(i).APPLICATION_ID;
                                              nomApptDBObj.username = res.rows.item(i).AGENT_CD;
                                              //nomApptDBObj.nomCustID = "C09";
                                              nomApptDBObj.nomCustID = res.rows.item(i).CUST_TYPE;
                                              nomApptDBObj.FirstName = res.rows.item(i).FIRST_NAME;
                                              nomApptDBObj.MiddleName = res.rows.item(i).MIDDLE_NAME;
                                              nomApptDBObj.LastName = res.rows.item(i).LAST_NAME;
                                              nomApptDBObj.dob = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                                              debug("Date of birth "+nomApptDBObj.dob);
                                              nomApptDBObj.seq_no = res.rows.item(i).SEQ_NO;
                                              nomApptDBObj.Gen = res.rows.item(i).GENDER;
                                              nomApptDBObj.gender_code = res.rows.item(i).GENDER_CODE;
                                              nomApptDBObj.Percent = parseInt(res.rows.item(i).PERCENTAGE);
                                              nomApptDBObj.nomTermFlag = res.rows.item(i).SAME_NOMINEE_FOR_TERM;

                                              self.getNomRelationData().then(
                                                   function(listOfNom){
                                                       nomApptDBObj.Relation = $filter('filter')(listOfNom, {"NOM_NBFE_CODE": res.rows.item(i).RELATIONSHIP_ID})[0];
                                                   }
                                              );
                                              nomApptDBData.push(nomApptDBObj);
                                           }(i))

                                           //nomApptDBObj.Relation.Relationship_DESC = res.rows.item(i).RELATIONSHIP_DESC;
                                           //nomApptDBObj.Relation.Relationship_ID = res.rows.item(i).RELATIONSHIP_ID;


                                       }
                                       debug("data from select query : ");
                                       debug(nomApptDBData);
                                       dfd.resolve(nomApptDBData);
                                   }
                                   else
                                       dfd.resolve(nomApptDBData);
                               },
                               function(tx,err){
                                   dfd.resolve(nomApptDBData);
                               }
                           );
                       },
                       function(err){
                           dfd.resolve(nomApptDBData);
                       },null
                   );
       return dfd.promise;
    };


    //get the existing data Term..
    this.existingDataTerm = function(comboID,agentCD){
       var dfd = $q.defer();
       var nomApptTermDBData = [];
       CommonService.transaction(db,
                       function(tx){
                           CommonService.executeSql(tx,"select combo_id,agent_cd,cust_type,first_name,middle_name,last_name,birth_date,relationship_desc,relationship_id,seq_no,gender,gender_code,percentage from LP_COMBO_NOM_CONTACT_SCRN_F where combo_id=? and agent_cd = ? and cust_type=?",[comboID,agentCD,"C05"],
                               function(tx,res){
                                   debug("select nom data res.rows.length: " + res.rows.length);
                                   if(!!res && res.rows.length>0){
                                       for(var i=0;i<res.rows.length;i++){

                                           (function(i){
                                              var nomApptTermDBObj = {};
                                              nomApptTermDBObj.comboID = res.rows.item(i).COMBO_ID;
                                              nomApptTermDBObj.username = res.rows.item(i).AGENT_CD;
                                              //nomApptDBObj.nomCustID = "C09";
                                              nomApptTermDBObj.nomCustID = res.rows.item(i).CUST_TYPE;
                                              nomApptTermDBObj.FirstName = res.rows.item(i).FIRST_NAME;
                                              nomApptTermDBObj.MiddleName = res.rows.item(i).MIDDLE_NAME;
                                              nomApptTermDBObj.LastName = res.rows.item(i).LAST_NAME;
                                              nomApptTermDBObj.dob = CommonService.formatDobFromDb(res.rows.item(i).BIRTH_DATE);
                                              debug("Date of birth "+nomApptTermDBObj.dob);
                                              nomApptTermDBObj.seq_no = res.rows.item(i).SEQ_NO;
                                              nomApptTermDBObj.Gen = res.rows.item(i).GENDER;
                                              nomApptTermDBObj.gender_code = res.rows.item(i).GENDER_CODE;
                                              nomApptTermDBObj.Percent = parseInt(res.rows.item(i).PERCENTAGE);

                                              self.getNomRelationData().then(
                                                   function(listOfNom){
                                                       nomApptTermDBObj.Relation = $filter('filter')(listOfNom, {"NOM_NBFE_CODE": res.rows.item(i).RELATIONSHIP_ID})[0];
                                                   }
                                              );
                                              nomApptTermDBData.push(nomApptTermDBObj);
                                           }(i))

                                           //nomApptDBObj.Relation.Relationship_DESC = res.rows.item(i).RELATIONSHIP_DESC;
                                           //nomApptDBObj.Relation.Relationship_ID = res.rows.item(i).RELATIONSHIP_ID;


                                       }
                                       dfd.resolve(nomApptTermDBData);
                                   }
                                   else
                                       dfd.resolve(nomApptTermDBData);
                               },
                               function(tx,err){
                                   dfd.resolve(nomApptTermDBData);
                               }
                           );
                       },
                       function(err){
                           dfd.resolve(nomApptTermDBData);
                       },null
                   );
       return dfd.promise;
    };

    this.isNomineeBelow18=function(){
        return nomineeAgeFlag;
    }
    this.setNomineeBelow18=function(nomineeAge){
        nomineeAgeFlag=nomineeAge;
    }
    this.isNomineeTermBelow18=function(){
        return nomineeTermAgeFlag;
    }
    this.setNomineeTermBelow18=function(nomineeTermAge){
        nomineeTermAgeFlag=nomineeTermAge;
    }


}]);
