uploadDocModule.controller('uploadDocCtrl',['$state','paramData','ExistingAppMainData','ApplicationFormDataService','uploadDocService','CommonService','AppSyncService','$q','LoginService','appData','LoadApplicationScreenData',function($state,paramData,ExistingAppMainData,ApplicationFormDataService,uploadDocService,CommonService,AppSyncService,$q,LoginService,appData, LoadApplicationScreenData){
    var self = this;

    debug("the params data "+JSON.stringify(paramData));
    debug("existing data "+JSON.stringify(JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST)));
    CommonService.showLoading("Loading");
    console.log("Business type"+BUSINESS_TYPE);


    /* Header Hight Calculator */
    setTimeout(function(){
        var uploadDocGetHeight = $(".upload-doc .fixed-bar").height();
        $(".upload-doc .custom-position").css({top:uploadDocGetHeight + "px"});
    });
    /* End Header Hight Calculator */

	this.hideForiOS = false;
	if(!!ionic && !!ionic.Platform)
		this.hideForiOS = ionic.Platform.isIOS() || false;

    self.termclick = "";
    self.propclick = "";
    self.insclick = "active";
    var imagePath = "/FromClient/APP/IMAGES/";
    var pdfExtension = ".pdf";
    var jpegExtension = ".jpg";
    var user = "Insured";
    var filteredArray = [];

    var APP_ID = paramData.APP_ID;
    var AGENT_CD = paramData.AGENT_CD;
    var POLICY_NO = paramData.POLICY_NO;
    var EMPLOYEE_FLAG = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
    var COMBO_ID = paramData.COMBO_ID;
    debug(EMPLOYEE_FLAG+"COMBO_ID is upload document task "+COMBO_ID);

    var refernceApplicationID = null;
	var referenceSISId = null;
    var RefPolicyNo = null;

    if(!!appData){
        referenceSISId = appData.REF_SIS_ID || null;
        refernceApplicationID = appData.REF_APPLICATION_ID || null;
        RefPolicyNo = appData.REF_POLICY_NO || null;
    }

    debug("refernce Application ID "+refernceApplicationID);
    debug("refernce RefPolicyNo  "+RefPolicyNo);

    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = APP_ID;
    whereClauseObj.AGENT_CD = AGENT_CD;

     //side menu open
     this.openMenu = function(){
         CommonService.openMenu();
     };

    //create blank object to add docids..
    ApplicationFormDataService.applicationFormBean.DOCS_LIST = {
        Reflex :{
            Insured:{},
            Proposer:{}
        },
        NonReflex :{
            Insured:{},
            Proposer:{}
        }
    };

    //assign db object to script object..
    if(!!ExistingAppMainData && !!ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST && Object.keys(ExistingAppMainData.applicationPersonalInfo.Insured).length!=0){
        ApplicationFormDataService.applicationFormBean.DOCS_LIST = JSON.parse(ExistingAppMainData.applicationPersonalInfo.Insured.DOCS_LIST);
        debug("RETURNED OBJECT "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));

        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.CustDecForm = {};
        /* ContactProof added for IndusSolution on 2nd Feb*/
        if(BUSINESS_TYPE=="IndusSolution"){
            console.log("Business type in Insured" + BUSINESS_TYPE);
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ContactProof = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.ContactProof.DOC_ID = "1021010498";
         }
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other1 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other2 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other3 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other4 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other5 = {};
        /*New 5 document added - insured*/
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other6 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other7 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other8 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other9 = {};
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other10 = {};


        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.CustDecForm.DOC_ID = "1014010197";

        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other1.DOC_ID="1021010347";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other2.DOC_ID="1021010348";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other3.DOC_ID="1021010349";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other4.DOC_ID="1021010350";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other5.DOC_ID="1021010351";
        /*New 5 document added - insured*/
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other6.DOC_ID="1021010421";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other7.DOC_ID="1021010422";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other8.DOC_ID="1021010423";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other9.DOC_ID="1021010424";
        ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Insured.other10.DOC_ID="1021010425";

        if(Object.keys(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer).length == 0){
            debug("proposer is disabled");
            self.disableProp = false;
        }else{

            self.disableProp = true;
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.CustDecForm = {};
            /* ContactProof added for IndusSolution on 2nd Feb*/
            if(BUSINESS_TYPE=="IndusSolution"){
                console.log("BusinessTypein proposer" + BUSINESS_TYPE);
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ContactProof = {};
                ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.ContactProof.DOC_ID = "2021010497";
             }
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other1 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other2 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other3 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other4 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other5 = {};
            /*New 5 document added - proposer*/
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other6 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other7 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other8 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other9 = {};
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other10 = {};

            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.CustDecForm.DOC_ID = "2014010196";

            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other1.DOC_ID="2021010352";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other2.DOC_ID="2021010353";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other3.DOC_ID="2021010354";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other4.DOC_ID="2021010355";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other5.DOC_ID="2021010356";

            /*New 5 document added - proposer*/
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other6.DOC_ID="2021010426";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other7.DOC_ID="2021010427";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other8.DOC_ID="2021010428";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other9.DOC_ID="2021010429";
            ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Proposer.other10.DOC_ID="2021010430";
        }


         debug("Term is disabled"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term));
         var termdata = ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term;

         if(!!termdata && Object.keys(termdata).length>0){
             var nomReldata =  ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.NomRelation;
             var renPayData = ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term.renewalPay;

             if((!!nomReldata && Object.keys(nomReldata).length>0) || (!!renPayData && Object.keys(renPayData).length>0))
                 self.disableTerm = true;
             else
                 self.disableTerm = false;
         }else{
             self.disableTerm = false;
             debug("Term is disabled"+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex.Term));
         }
     }else{
            debug("existing object");
     }

    debug("APPLICATION DOC LIST IS "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    //check for insured or proposer click...
    self.navUser = function(pageName){
        debug("page to nav "+ pageName);
        self.DocUploadArr = [];
        switch(pageName){
            case "Proposer":
                self.propclick = "active";
                self.insclick = "";
                self.termclick = "";
                self.getDocDataBasedOnUser("Proposer");
            break;
            case "Insured":
                self.insclick = "active";
                self.propclick = "";
                 self.termclick = "";
                self.getDocDataBasedOnUser("Insured");
            break;
            case "Term":
                self.insclick = "";
                self.propclick = "";
                 self.termclick = "active";
                self.getDocDataBasedOnUser("Term");
            break;
            default:
                self.propclick = "";
                self.termclick = "";
                self.getDocDataBasedOnUser("Insured");
            break;
        }
     };

    //get all document  Data for insured or proposer....
    self.getDocDataBasedOnUser = function(user){
        filteredArray = [];
       // var APP_ID =(!!COMBO_ID)?refernceApplicationID:paramData.APP_ID;
       var APP_ID = "";
        if(user != "Term")
            APP_ID = paramData.APP_ID;
        else
            APP_ID = refernceApplicationID;
        //var APP_ID ="2424";

        uploadDocService.getAllDocRelatedData(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[user],paramData.AGENT_CD,APP_ID).then(
             function(data){
                debug("data returned is "+JSON.stringify(data));
                //filter the array to remove null values for ng-repeat duplicate issue..
                angular.forEach(data, function(item) {
                    if (!!item)
                        filteredArray.push(item);
                    else
                        debug("item is null");
                })
                self.docArr = filteredArray;
                CommonService.hideLoading();
             }//outer function
        );//then
    };

    //initially show insured data..
    self.getDocDataBasedOnUser("Insured");

    //Document click..
    self.nativeDocClicked = function(index){
        debug("index  clicked is "+index);
        debug(filteredArray);
        debug("selected docid >>>"+filteredArray[index].selected);
                debug("selected docid >>>"+filteredArray[index].selected);

        if(!!filteredArray[index].selected){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
            var DOC_ID = filteredArray[index].DOC_ID;
            self.callNative("Document",index,filteredArray[index].selected,pdfExtension,DOC_ID);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };

    //Camera click..
    self.nativeCamClicked = function(index){
        debug("index  clicked is "+index);
          debug(filteredArray);
        debug("selected docid >>>"+filteredArray[index].selected);
        if(!!filteredArray[index].selected){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
             var DOC_ID = filteredArray[index].DOC_ID;

            self.callNative("Camera",index,filteredArray[index].selected,jpegExtension,DOC_ID);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };

    //Gallery click..
    self.nativeGallClicked = function(index){
        debug("index  clicked is "+index);
        debug("selected docid >>>"+filteredArray[index].selected);
        if(!!filteredArray[index].selected){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
            var DOC_ID = filteredArray[index].DOC_ID;
            //self.callNative("Document",index,filteredArray[index].selected,pdfExtension,DOC_ID);
            self.callNative("Gallery",index,filteredArray[index].selected,jpegExtension,DOC_ID);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };

    //call native code depending on the source..
    self.callNative = function(source,index,DOC_ID,extension,DOC_ID_NEW){
        //var documentID = filteredArray[index].DOC_ID;
        var fileName = AGENT_CD + "_" + APP_ID + "_" + DOC_ID;
        debug("File name "+fileName);

        //call native class...
        cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
                if(success.code == 1){
                    if(!!success.count){
                        debug("result from the camera class "+JSON.stringify(success));
                          debug("call native refernceApplicationID :  "+refernceApplicationID);

                        uploadDocService.insertCapturedDocs(DOC_ID,fileName,success.count,AGENT_CD,POLICY_NO,APP_ID,extension,COMBO_ID,refernceApplicationID,DOC_ID_NEW,self.disableTerm,RefPolicyNo,self.termclick).then(
                            function(ifInsertedCnt){
                                debug("inserted Flag "+JSON.stringify(ifInsertedCnt));
                                if(ifInsertedCnt){
                                    //debug("all data inserted successfully "+JSON.stringify(filteredArray[index]));
                                    filteredArray[index].inDatabase = true;
                                }//if loop
                            }//function
                        );//service function
                    }
                }else{
                    debug("code from camera  "+success.code);
                }
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
            },//function error..
            "PluginHandler",
            'camera',
            [imagePath,fileName,source]
        );
    }

    self.callNativeViewImage = function(source,index,DOC_ID,extension,DOC_ID_NEW){
        //var documentID = filteredArray[index].DOC_ID;
        var fileName = AGENT_CD + "_" + APP_ID + "_" + DOC_ID;
        debug("File name "+fileName);

        //call native class...
        cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
            },//function error..
            "PluginHandler",
            'camera',
            [imagePath,fileName,source]
        );
    }

    //upload data and files
    self.submitt = function(){
        self.uploadDataAndFile();
    };


     //view images in Native Gridview
    self.viewimage = function(index){
        //check for existing image
        uploadDocService.getDocNameFromDocID(filteredArray[index].selected,AGENT_CD,APP_ID).then(
            function(docData){
                if(!!docData){
                    console.log("exists"+docData.rows.item(0).DOC_NAME);
                    debug("exist");
                    var docName = docData.rows.item(0).DOC_NAME;
                    if(!!docName){
                        if(docName.indexOf(pdfExtension) == -1)
                            self.callNativeViewImage("viewimage",index,filteredArray[index].selected,jpegExtension);
                        else{
                            debug("cannot view pdf");
                            navigator.notification.alert("Cannot View PDF",null,"Document Upload","OK");
                        }
                    }
                }
                else{
                   console.log("not exists");
                    debug("not exist");
                    navigator.notification.alert("No Image found",null,"Document Upload","OK");
                }
            }
        );

   };

    self.dropDownChange = function(DOC_ID,description,index){
        debug("dropdown change DOC ID >>>"+DOC_ID);
        debug("description is >>>>"+description);
        if(self.insclick === "active"){
            debug("insured selected ");
            user = "Insured";
        }else{
            debug("Proposer Selected");
            user = "Proposer";
        }
        debug("user selected is "+user);
        var docName = self.getNameOfKey(description);
        debug("data is "+docName);
        debug("BEFORE UPDATE DOCLIST "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        if(docName != ""){
            if(!!ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[user][docName].DOC_ID)
                {
					var DOCIDTOCHANGE = ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[user][docName].DOC_ID;
					uploadDocService.deleteAllImagesOfDOCID(APP_ID,AGENT_CD,DOCIDTOCHANGE,POLICY_NO).then(
						function(ifDeleted){
							if(ifDeleted){
								ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex[user][docName].DOC_ID = DOC_ID;
								filteredArray[index].inDatabase = false;
								//27-12-2016
								uploadDocService.updateDocsList(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj).then(
								    function(updated){
								        console.log("updated");
								    }
								);
							}
						}
					)
			    }
        }
        debug("BEFORE UPDATE DOCLIST "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
    }

    self.getNameOfKey = function(val){
        var key = "";
        switch(val){
            case "Income Proof":
                key = "incproof";
            break;
            case "Permanent Address Proof":
                key = "addrproof";
            break;
            case "Residential Address Proof":
                key = "addrproof";
            break;
            default :
                key = "";
            break;
        }
        return key;
    }

    //start document sync here after data sync check..
    self.uploadDataAndFile = function(){
		CommonService.updateRecords(db, "LP_DOCUMENT_UPLOAD", {"POLICY_NO": POLICY_NO}, {"DOC_CAT_ID": paramData.SIS_ID, "DOC_CAT": "SIS", "AGENT_CD": AGENT_CD, "DOC_PAGE_NO": "0"}).then(
			function(updateRes){
				uploadDocService.checkIfDataSync(AGENT_CD,APP_ID).then(
		            function(isSynced){
		                debug("sync flag >> "+isSynced);
               // self.startDocumentSync();
		                if(CommonService.checkConnection() == "online"){
		                    if(isSynced){
		                        self.startDocumentSync();
		                    }else{
		                    if(!!paramData.COMBO_ID)    //FhrId,leadId, OppId, sisId, AppId, custEmailId, polNo
                            LoadApplicationScreenData.loadOppFlags(paramData.COMBO_ID, AGENT_CD, 'combo', 'T').then(function(oppData){
                                AppSyncService.startAppDataSync(paramData.FHR_ID, paramData.LEAD_ID,paramData.OPP_ID, paramData.SIS_ID, paramData.APP_ID, null, paramData.POLICY_NO, true,'T').then(function(res){

                                                                            console.log("inside complete Base syncAppData ::"+JSON.stringify(res));
                                                                        if(!!res && res!=false)
                                                                        AppSyncService.startAppDataSync(oppData.oppFlags.FHR_ID,oppData.oppFlags.LEAD_ID, oppData.oppFlags.OPPORTUNITY_ID, oppData.oppFlags.SIS_ID, oppData.oppFlags.APPLICATION_ID, null, oppData.oppFlags.POLICY_NO,true,'T').then(function(Tres){
                                                                                                                     if(!!Tres && Tres!=false)
                                                                                                                     self.updateLPAppMain(paramData.APP_ID, AGENT_CD).then(function(){
                                                                                                                        self.updateLPAppMain(oppData.oppFlags.APPLICATION_ID, AGENT_CD).then(function(){
                                                                                                                            CommonService.hideLoading();
                                                                                                                            console.log("inside complete TERM syncAppData ::"+JSON.stringify(res));
                                                                                                                            self.startDocumentSync();

                                                                                                                        });

                                                                                                                    });
                                                                                                            });
                                                                    });
                            });
                        else //FhrId,leadId, OppId, sisId, AppId, custEmailId, polNo
		                        AppSyncService.startAppDataSync(paramData.FHR_ID, paramData.LEAD_ID,paramData.OPP_ID, paramData.SIS_ID, paramData.APP_ID, null, paramData.POLICY_NO, true).then(function(resp){
		                            debug("Callback resp:"+resp);
		                            if(resp)
		                                self.startDocumentSync();
		                        });

		                    }
		                }
		                else
		                    navigator.notification.alert(OFFLINE_SYNC_MSG,null,"DocUpload","OK");
		            }
		        );
			}
		);
    }

    self.updateLPAppMain = function(APP_ID, AGENT_CD){
      var issynced = 'Y';
      if(EMPLOYEE_FLAG == 'E')
        issynced = 'E';
        var dfd = $q.defer();
              CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISSYNCED" : issynced}, {"APPLICATION_ID":APP_ID,"AGENT_CD":AGENT_CD}).then(
                    function(res){
                        console.log("ISSYNCED update success !!"+APP_ID);
                        dfd.resolve(true);
                    });
        return dfd.promise;
    }

    //actuall uploading code here..
    self.startDocumentSync = function(){
        self.getAllFlags().then(
            function(data){
                debug("data is inserted "+JSON.stringify(data));
                for(var i=0;i<data.length;i++){
                    if(data[i] == false){
                        navigator.notification.alert("Please upload all documents",null,"DocUpload","OK");
                        CommonService.hideLoading();
                        return;
                    }
                }
                debug("Move to new page");

                self.uplodDocDataWithFiles();
            }
        );
    }

    //upload all document data first..
    //after doc data upload, upload all files..
    //after file upload update database and flag..
    self.uplodDocDataWithFiles = function(){
        uploadDocService.syncFormData(paramData.AGENT_CD,paramData.APP_ID,LoginService.lgnSrvObj.password,localStorage.DVID,paramData.SIS_ID,refernceApplicationID, referenceSISId).then(
            function(result){
				//changes for result check 27/10/2016
				if(!!result){
					debug("result is "+JSON.stringify(result));
	                debug("<<<<<< UPDATION ENDED >>>>>>");
	                debug("<<<<<< DATA SYNCING ENDED >>>>>>>");
                uploadDocService.getFileData(paramData.AGENT_CD,paramData.APP_ID,paramData.SIS_ID,refernceApplicationID).then(
	                    function(res){
	                        debug("<<<<< GET ALL THE FILES FROM DB ENDED >>>>>>>");
	                        if(!!res && res.rows.length > 0){
	                            uploadDocService.synFileData(res,paramData.AGENT_CD,paramData.APP_ID,LoginService.lgnSrvObj.password,localStorage.DVID,imagePath).then(
	                                function(data){
										debug("the data after syncing all files >>>"+JSON.stringify(data));
										//changes 29th august..for server response false..
										if(!!data){
											for(var j=0;j<data.length;j++){
												if(data[j] == false){
													navigator.notification.alert(REQ_FAIL_MSG,null,"DocUpload","OK");
													CommonService.hideLoading();
													return;
												}
											}
										}
										else{
											CommonService.hideLoading();
											return;
										}
										self.enableAgentDetails();
	                                }
	                            );
	                        }else{
								self.alreadyUploadAlert();
	                        }
	                    }
	                );
				}else{
					navigator.notification.alert(REQ_FAIL_MSG,null,"DocUpload","OK");
					CommonService.hideLoading();
				}
            }
        );
    }

	self.enableAgentDetails = function(){
		uploadDocService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj).then(
			   function(inserted){
				   CommonService.hideLoading();
				   navigator.notification.alert("Document Uploaded SuccessFully",null,"DocUpload","OK");
				   self.disabled = true;
				   debug("Exist PT FLAG >> "+paramData.fromPT);
				   if(paramData.fromPT)
					   $state.go("dashboard.home");
				   else
					   $state.go('leadDashboard',{"APP_ID":paramData.APP_ID,"SIS_ID":paramData.SIS_ID,"LEAD_ID":paramData.LEAD_ID,"FHR_ID":paramData.FHR_ID,"POLICY_NO":paramData.POLICY_NO,"PGL_ID":paramData.PGL_ID});
			   }
		);
	}

	self.alreadyUploadAlert = function(){
		uploadDocService.updateFlagAndObject(ApplicationFormDataService.applicationFormBean.DOCS_LIST,whereClauseObj).then(
			function(inserted){
				navigator.notification.alert("Document already uploaded",null,"DocUpload","OK");
				CommonService.hideLoading();
				debug("Exist PT FLAG >> "+paramData.fromPT);
				if(paramData.fromPT)
					$state.go("dashboard.home");
				else
					$state.go('leadDashboard',{"APP_ID":paramData.APP_ID,"SIS_ID":paramData.SIS_ID,"LEAD_ID":paramData.LEAD_ID,"FHR_ID":paramData.FHR_ID,"POLICY_NO":paramData.POLICY_NO,"PGL_ID":paramData.PGL_ID});
			}
		)
	}

    //check if all document picture has takenn...
    self.getAllFlags = function(){
        var promises = [];
        debug("Doc list from get all flags "+JSON.stringify(ApplicationFormDataService.applicationFormBean.DOCS_LIST));
        angular.forEach(ApplicationFormDataService.applicationFormBean.DOCS_LIST.NonReflex,function(value,key){
            angular.forEach(value,function(value,key){
                debug("OUTER KET IS "+key);
                if(key.indexOf("other") == -1){
                    angular.forEach(value,function(value,key){
                        debug("inner inner key is >>"+key+"<<inner value is >>"+value);
                        if(key == "DOC_ID"){
                            debug("checking for docid if present in DB -->"+value);
                            if(!!value){
                                var app_id = (self.termclick == "active")?refernceApplicationID:APP_ID;//1788
                                promises.push(self.checkSubmittFlag(value,AGENT_CD,app_id));
                            }
                        }
                    });
                }else{
                    debug("IT IS OTHER DOCUMENT DONT CHECK");
                }
            });
        });

        return $q.all(promises);
    };

    //check if all document is in database..
    self.checkSubmittFlag = function(value,agentCD,appID){
        var dfd = $q.defer();
        uploadDocService.checkIfDocPresentInDB(value,agentCD,appID).then(
            function(exist){
                if(exist){
                    debug("exist");
                    dfd.resolve(true);
                }
                else{
                    debug("not exist");
                    dfd.resolve(false);
                }
            }
        );
        return dfd.promise;
    };
}]);

uploadDocModule.service('uploadDocService',['$state','$q','CommonService','ApplicationFormDataService',function($state,$q,CommonService,ApplicationFormDataService){
    var self = this;
    var MWP_DOC_ID = "7024010377";
    var RS_DOC_ID = "2028010408";
    var RN_DOC_ID = "2028010409";

    //documents based on ins or prop..
    this.getAllDocRelatedData = function(data,agentCD,appID){
         var docNames = [];
         var objData = data;
         //-1 because start with 0 as index made ++..
         var index = -1;

         function getDoc(value){
            var dfd = $q.defer();
            if(!!value.DOC_ID){
                index++;
                debug("inside inner >>>"+index);
                self.getDocumentData(value,index,agentCD,appID).then(
                    function(result){
                        debug("RESULT IS "+JSON.stringify(result));
                        if(!!result)
                            dfd.resolve(result);
                    }
                );
            }else{
                debug("doc id is null");
                dfd.resolve(null);
            }

            return dfd.promise;
         }
         angular.forEach(objData, function(value,key){
            debug("key to add is "+ key +" and the value is "+JSON.stringify(value));
            if(!!value){
                var dataVal = getDoc(value);
                debug("data is not null >> "+dataVal);
                if(!!dataVal)
                    docNames.push(dataVal);
            }else{
                debug("data is null");
            }
         })
         return $q.all(docNames);
    };

    //query to get doc details based on docid
    this.getDocumentData = function(data,index,agentCD,appID){
        var dfd = $q.defer();
        debug("INSERTING FOR INDEX  >>>"+index);
        var query = "SELECT CUSTOMER_CATEGORY,MPDOC_CODE,MPDOC_DESCRIPTION,MPPROOF_DESC,MPPROOF_CODE FROM LP_DOC_PROOF_MASTER WHERE ISACTIVE=? AND DOC_ID = ?";
        var parameters = ['1', data.DOC_ID];

        CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,query,parameters,
                        function(tx,res){
                            debug("Document Id is : " + JSON.stringify(data));
                            debug("res Id is : " + JSON.stringify(res.rows.item(0)));
                            debug("nom drop down res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                var docObj = {};
                                docObj.docArr = [];
                                docObj.DOC_ID = data.DOC_ID;
                                docObj.CUSTOMER_CATEGORY = res.rows.item(0).CUSTOMER_CATEGORY;
                                docObj.MPDOC_CODE = res.rows.item(0).MPDOC_CODE;
                                docObj.MPDOC_DESCRIPTION = res.rows.item(0).MPDOC_DESCRIPTION;
                                docObj.MPPROOF_CODE = res.rows.item(0).MPPROOF_CODE;
                                //docObj.MPPROOF_DESC = res.rows.item(0).MPPROOF_DESC;
                                console.log("Printing DOCOBJ" + JSON.stringify(docObj));
                                if(data.Flag)
                                docObj.FLAG = data.Flag;
                                docObj.seqNo = index;
                                debug("agent cd "+agentCD);
                                debug("app id "+appID);
                                docObj.selected = data.DOC_ID;
                                docObj.inDatabase = false;

                                self.getDropDownData(docObj).then(
                                    function(docArr){
                                        if(!!docArr){
                                            docObj.docArr = docArr;
                                            debug("<<< ARRAY FILLED FOR DROPDOWN >>>");
                                            for(var i=0;i<docArr.length;i++){
                                                (function(i){
                                                    var documentId = docArr[i].DOC_ID;
                                                    self.checkIfDocPresentInDB(documentId,agentCD,appID).then(
                                                        function(ifPresent){
                                                            if(ifPresent){
                                                                debug("doc id present in database >>>"+documentId);
                                                                docObj.selected = documentId;
                                                                docObj.inDatabase = true;
                                                                dfd.resolve(docObj);
                                                            }
                                                        }
                                                    );
                                                }(i))
                                            }
                                            dfd.resolve(docObj);
                                        }else{
                                            dfd.resolve(null);
                                        }
                                    }
                                );
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
        );
        return dfd.promise;
    };

    //check doc in database...
    this.checkIfDocPresentInDB = function(DOC_ID,AGENT_CD,APPLICATION_ID){
        debug("check in DOB");
        var dfd = $q.defer();
        var query = "select DOC_ID from LP_DOCUMENT_UPLOAD where DOC_ID = ? and AGENT_CD = ? and DOC_CAT_ID = ?"
        var parameters = [DOC_ID,AGENT_CD,APPLICATION_ID];
        debug("paramaterss   --->"+parameters);
        if(!!DOC_ID){
            CommonService.transaction(db,
                    function(tx){
                        CommonService.executeSql(tx,query,parameters,
                            function(tx,res){
                                debug("DOC EXIST DATA res.rows.length: " + res.rows.length);
                                if(!!res && res.rows.length>0){
                                    //var docObj = {};
                                    //docObj.DOC_ID = res.rows.item(0).DOC_ID;
                                    //docObj.MPPROOF_DESC = res.rows.item(0).MPPROOF_DESC;
                                    //debug("data is "+JSON.stringify(docObj));
                                    dfd.resolve(true);
                                }
                                else
                                    dfd.resolve(false);
                            },
                            function(tx,err){
                                //CommonService.hideLoading();
                                dfd.resolve(false);
                            }
                        );
                    },
                    function(err){
                        dfd.resolve(false);
                    },null
            );
        }else{
            //for testing...
            dfd.resolve(false);
        }
        return dfd.promise;
    };


    this.getDocInfoFormDb = function(DOC_ID,AGENT_CD,APPLICATION_ID){
            debug("check in DOB");
            var dfd = $q.defer();
            var query = "select DOC_ID,DOC_NAME from LP_DOCUMENT_UPLOAD where DOC_ID = ? and AGENT_CD = ? and DOC_CAT_ID = ?"
            var parameters = [DOC_ID,AGENT_CD,APPLICATION_ID];
            debug("paramaterss   --->"+parameters);
             var docObj = {};
            if(!!DOC_ID){
                CommonService.transaction(db,
                        function(tx){
                            CommonService.executeSql(tx,query,parameters,
                                function(tx,res){
                                    debug("DOC EXIST DATA res.rows.length: " + res.rows.length);
                                    if(!!res && res.rows.length>0){
                                        docObj.STATUS = true;
                                        docObj.DOC_ID = res.rows.item(0).DOC_ID;
                                        docObj.DOC_NAME = res.rows.item(0).DOC_NAME;
                                        debug("data is "+JSON.stringify(docObj));
                                        dfd.resolve(docObj);
                                    }
                                    else{
                                        docObj.STATUS = false;
                                        dfd.resolve(docObj);
                                    }
                                },
                                function(tx,err){
                                    //CommonService.hideLoading();
                                    docObj.STATUS = false;
                                    dfd.resolve(docObj);
                                }
                            );
                        },
                        function(err){
                            docObj.STATUS = false;
                            dfd.resolve(docObj);
                        },null
                );
            }else{
                //for testing....
                docObj.STATUS = false;
                dfd.resolve(docObj);
            }
            return dfd.promise;
        };



    this.getDocNameFromDocID = function(DOC_ID,AGENT_CD,APPLICATION_ID){
        var dfd = $q.defer();
        var query = "select DOC_NAME from LP_DOCUMENT_UPLOAD where DOC_ID = ? and AGENT_CD = ? and DOC_CAT_ID = ?"
        var parameters = [DOC_ID,AGENT_CD,APPLICATION_ID];
        debug("paramaterss   --->"+parameters);
        if(!!DOC_ID){
            CommonService.transaction(db,
                    function(tx){
                        CommonService.executeSql(tx,query,parameters,
                            function(tx,res){
                                debug("DOC EXIST DATA res.rows.length: " + res.rows.length);
                                if(!!res && res.rows.length>0){
                                    //var docObj = {};
                                    //docObj.DOC_ID = res.rows.item(0).DOC_ID;
                                    //docObj.MPPROOF_DESC = res.rows.item(0).MPPROOF_DESC;
                                    //debug("data is "+JSON.stringify(docObj));
                                    dfd.resolve(res);
                                }
                                else
                                    dfd.resolve(null);
                            },
                            function(tx,err){
                                //CommonService.hideLoading();
                                dfd.resolve(null);
                            }
                        );
                    },
                    function(err){
                        dfd.resolve(null);
                    },null
            );
        }else{
            //for testing...
            dfd.resolve(null);
        }
        return dfd.promise;
    }

    //get the dropdown data w.r.t to Document..
    this.getDropDownData = function(docObj){
       var dfd = $q.defer();
       var final_query = "";
       var parameters = "";

       if(docObj.MPDOC_CODE == "PP" || docObj.MPDOC_CODE == "RP" || docObj.MPDOC_CODE == "FP"){
            debug("FOR ADDRESS PROOF AND INCOME PROOF");
            final_query = "SELECT MPPROOF_DESC,DOC_ID FROM LP_DOC_PROOF_MASTER WHERE ISACTIVE=? AND MPDOC_CODE=? AND CUSTOMER_CATEGORY=?";
            parameters = ['1',docObj.MPDOC_CODE,docObj.CUSTOMER_CATEGORY];
       }else{
            final_query = "SELECT MPPROOF_DESC,DOC_ID FROM LP_DOC_PROOF_MASTER WHERE ISACTIVE=? AND MPDOC_CODE=? AND CUSTOMER_CATEGORY=? AND MPPROOF_CODE=?";
            parameters = ['1',docObj.MPDOC_CODE,docObj.CUSTOMER_CATEGORY,docObj.MPPROOF_CODE];
       }

       if(docObj.FLAG != undefined && docObj.FLAG == "Y"){
            final_query = final_query + " AND STANDARD_FLAG = 'Y'";
       }
       debug("query after flag check "+final_query);

       var docArrObj = [];
       //docArrObj.docID = docObj.DOC_ID;
       //var docTypeArr = [];

       CommonService.transaction(db,
               function(tx){
                   CommonService.executeSql(tx,final_query,parameters,
                       function(tx,res){
                           debug("nom drop down res.rows.length: " + res.rows.length);
                           if(!!res && res.rows.length>0){
                               for(var i=0;i<res.rows.length;i++){
                                    docTypeObj = {};
                                    docTypeObj.DOC_ID = res.rows.item(i).DOC_ID;
                                    docTypeObj.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
                                    docArrObj.push(docTypeObj);
                                    //docTypeArr.push(res.rows.item(i).MPPROOF_DESC);
                               }
                               //docArrObj.docArr = docTypeArr;
                               dfd.resolve(docArrObj);
                           }
                           else
                               dfd.resolve(null);
                       },
                       function(tx,err){
                           dfd.resolve(null);
                       }
                   );
               },
               function(err){
                   dfd.resolve(null);
               },null
       );
       return dfd.promise;
    };

    //inserting into Database..
    this.insertCapturedDocs = function(docID,fileName,count,agentCD,policyNo,appID,extension,comboId,refernceApplicationID,DOC_ID_NEW,disableTerm,RefPolicyNo,insclick){
        var inserted = [];
        debug("count in insert capture "+count);
        var dfd = $q.defer();
        var test = "xyz";
        var COMBO_ID = comboId;
        var row = 0;
        var IsFlagSync = "N";
        //check if docid already present in DB
        self.checkIfDocPresentInDB(docID,agentCD,appID).then(
            function(ifExist){
                debug("Exist DOCID in DB >> "+docID);
                //if yes, then delete all Documents for Particular DOCID..
                //if no, then insert into DB
                if(ifExist){
                    self.deleteAllImagesOfDOCID(appID,agentCD,docID,policyNo).then(
                        function(deleted){
                            if(deleted){
                                debug("<<<< DELETED ALL DOCIDS >>>>");
                                debug("<<<< INSERTION STARTED >>>>");
                                checkTermPlanConditions();
                            }
                        }
                    );
                }else{
                    checkTermPlanConditions();
                }


            /**
            *function to validate if COMBO_ID is there or not
            *if COMBO_ID is available then it should call the insert the data twice at first it save app id
            *if the SI (Standing Instruction ) is been applied for the both i.e base plan and term plan it shuld only upldate doc once
            **/
            function checkTermPlanConditions(){
                //DOC_ID
                 if(!!COMBO_ID){
                        debug("disableTerm  "+disableTerm +" MPDOC_DESCRIPTION  "+DOC_ID_NEW +"docID : "+docID+" insclick" +insclick +" RS_DOC_ID >> "+RS_DOC_ID+"<< MWP_DOC_ID >>"+MWP_DOC_ID+" << RN_DOC_ID >> "+RN_DOC_ID);

                        if(disableTerm && (RS_DOC_ID==docID ||  MWP_DOC_ID == docID || RN_DOC_ID == docID)){

                            if(insclick == "active"){//1798

                                 insertSingleDataDataAfterDeletion(refernceApplicationID,RefPolicyNo);
                            }else{

                              insertSingleDataDataAfterDeletion(appID,policyNo);
                            }

                        }else{
                             insertDataAfterDeletionComboId();
                        }
                }else{
                    insertDataAfterDeletion();
                }
            }

        }
    );

    function insertDataAfterDeletionComboId(){
            for(var i=1;i<=count;i++){
                debug("loop  "+i);

                var douCapId = CommonService.getRandomNumber();
                var douCapRefId = null;

                debug("refernceApplicationID " +refernceApplicationID);

                IsFlagSync = "N";
                self.insertDocToDb(docID,fileName,i,agentCD,policyNo,appID,extension,douCapId,douCapRefId,IsFlagSync).then(
                    function(insert){
                        if(insert.status){
                            appID = refernceApplicationID;// application id will be REF_APPLICATION_ID
                            douCapRefId = insert.docCapID;//set cap id to cap refernce id
                            douCapId = CommonService.getRandomNumber();//set cap id as random number
                            policyNo = RefPolicyNo;
                            IsFlagSync = "Y";
                            var fileName = agentCD + "_" + appID + "_" + docID;


                            debug("File name "+fileName);
                            debug("loop inside "+insert.count);
                             self.insertDocToDb(docID,fileName,insert.count,agentCD,policyNo,appID,extension,douCapId,douCapRefId,IsFlagSync).then(
                                function(insert){
                                    if(insert.status){
                                        row++;
                                        debug("inserted succesfully for record "+row);
                                        if(row == count){
                                            debug("<<<< INSERTION COMPLETED >>>>");
                                            dfd.resolve(true);
                                        }
                                    }
                                }
                            );

                        }
                    }
                );
            }
        }


    function insertSingleDataDataAfterDeletion(appID,policyNo){
            debug("*******  appID "+appID+ " ********* policyNo"+policyNo);
           IsFlagSync = "N";
            for(var i=1;i<=count;i++){
                debug("loop  "+i);

                var douCapId = CommonService.getRandomNumber();
                var douCapRefId = null;
                self.insertDocToDb(docID,fileName,i,agentCD,policyNo,appID,extension,douCapId,douCapRefId,IsFlagSync).then(
                    function(insert){
                        if(insert.status){
                            row++;
                            debug("inserted succesfully for record "+row);
                            if(row == count){
                                debug("<<<< INSERTION COMPLETED >>>>");
                                dfd.resolve(true);
                            }
                        }

                    }//function
                );//service function
            }//loop
    }


    function insertDataAfterDeletion(){
            for(var i=1;i<=count;i++){
                debug("loop  "+i);

                var douCapId = CommonService.getRandomNumber();
                var douCapRefId = null;
                self.insertDocToDb(docID,fileName,i,agentCD,policyNo,appID,extension,douCapId,douCapRefId,IsFlagSync).then(
                    function(insert){
                        if(insert.status){
                            row++;
                            debug("inserted succesfully for record "+row);
                            if(row == count){
                                debug("<<<< INSERTION COMPLETED >>>>");
                                dfd.resolve(true);
                            }
                        }

                    }//function
                );//service function
            }//loop
        }
        return dfd.promise;
    };





    /**
    *
    **/
    this.insertDocToDb = function(docID,fileName,count,agentCD,policyNo,appID,extension,docCapID,docCapRefId,IsFlagSync){
        var dfd = $q.defer();
        debug("count in insert capture "+fileName);
        var query = "insert or replace into LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,IS_PENDING,DOC_CAP_ID,DOC_CAP_REF_ID) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        debug("extension is >>> "+extension);
        var parameters = [docID,agentCD,"FORM",appID,policyNo,fileName+"_"+count+extension,CommonService.getCurrDate(),count,IsFlagSync,"N","N",docCapID,docCapRefId];
        debug("parameters ---->"+parameters);
        var responceObject = {};
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                        debug("result is "+JSON.stringify(res));
                        if(res){
                            debug("success in insert capture "+JSON.stringify(res));

                            responceObject.status = true;
                            responceObject.count = count;
                            responceObject.docCapID = docCapID;

                            dfd.resolve(responceObject);
                        }else{
                            debug("error in insert capture ");
                             responceObject.status = false;
                              dfd.resolve(responceObject);
                        }
                    },
                    function(tx,err){
                        debug("error in insert capture "+err);
                        responceObject.status = false;
                         dfd.resolve(responceObject);
                    }
                );//execute sql
            },//function
            function(err){
                 debug("error in insert capture transaction "+JSON.stringify(err));
                 dfd.resolve(false);
            },
        null);//transaction
        return dfd.promise;
    };


    /**
    * Function to get the data (REF_SIS_Is) from the LP_MYOPPORTUNITY based on the COMBO_ID
    * Input parameters are COMBO_id
    **/

    self.getAppIDFrom_LP_MYOPPORTUNITY = function(COMBO_ID){
        var dfd = $q.defer();
       query ="select REF_APPLICATION_ID,REF_POLICY_NO,REF_SIS_ID from LP_MYOPPORTUNITY where COMBO_ID = ? ";
        parameters = [COMBO_ID];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                                            function(tx,resp1){
                                                debug("query executed successfully for appt");
                                                //inserted = true;
                                                //if(res.rows.length > 0)

                                                if(resp1 && resp1.rows.length>0){

                                                    var resp = resp1.rows.item(0);

                                                    debug("data is tssssss"+JSON.stringify(resp));

                                                    var appID = (!!resp) ? resp1.rows.item(0):null;

                                                    debug("getDataFrom_ REF_APPLICATION_ID Reponce "+appID);


                                                    dfd.resolve(appID);



                                                }else{
                                                   dfd.resolve(null);
                                                }

                                                 // else
                                                    //dfd.resolve(null);
                                            },
                                            function(tx,err){
                                                debug("query execution failed for appt"+err);
                                                dfd.resolve(null);
                                            },null
                                        );

            },
            function(tx,err){
                debug("transaction error"+err.message);
                dfd.resolve(null);
            },
        null);

        return dfd.promise;
    };





    //delete all the imaged for particular DOCID
    this.deleteAllImagesOfDOCID = function(APP_ID,AGENT_CD,DOC_ID,POLICY_NO){
        debug("<<<<< DELETING FOR DOCID >>>>"+DOC_ID);
        var dfd = $q.defer();
        var query = "delete from LP_DOCUMENT_UPLOAD where AGENT_CD=? and DOC_ID=? and DOC_CAT=? and POLICY_NO = ?";
        var parameters = [AGENT_CD,DOC_ID,'FORM',POLICY_NO];

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                        debug("Deleted : " + res.rows.length);
                        if(!!res){
                            debug("<<<<< DELETED SUCCESS FOR DOCID >>>>"+DOC_ID);
                            dfd.resolve(true);
                        }
                        else{
                            debug("<<<<< DELETED FAILED FOR DOCID >>>>"+DOC_ID);
                            dfd.resolve(false);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(false);
                    }
                );
            },
            function(err){
                dfd.resolve(false);
            },null
        );
        return dfd.promise;
    };

    //check if data is synced..
    this.checkIfDataSync = function(AGENT_CD,APP_ID){
        var dfd = $q.defer();
        var query = "SELECT ISSYNCED FROM LP_APPLICATION_MAIN where APPLICATION_ID=? and AGENT_CD=?";
        var parameters = [APP_ID, AGENT_CD];
        debug("params >>"+parameters);

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                    debug("LP_APPLICATION_MAIN "+res.rows.length)
                        if(!!res && res.rows.length>0){
                            if(res.rows.item(0).ISSYNCED == "Y" || res.rows.item(0).ISSYNCED == "E"){
                                dfd.resolve(true);
                            }else{
                                dfd.resolve(false);
                            }
                        }

                    }
                )
            }
        );

        return dfd.promise;
    };

    //get all the data and pass to method to send it to server...
    this.syncFormData = function(AGENT_CD,APP_ID,PWD,DVID,SIS_ID,REFAPPID, REFSISID){
        CommonService.showLoading("Document Sync in Progress...");
        debug("<<<<<< DATA SYNCING STARTED >>>>>>>");
        var dfd = $q.defer();
        //var docArr = [];
        var query = "select DOC_ID AS DOCID,DOC_CAT AS CAT,DOC_CAT_ID AS CATID, POLICY_NO AS POLNO,DOC_NAME AS DOCNM, DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG,DOC_CAP_ID AS DOCCID,DOC_CAP_REF_ID AS DOCRID from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT = ? and (IS_DATA_SYNCED = ? OR IS_DATA_SYNCED is null) and DOC_CAT_ID in (?, ?, ?, ?) and DOC_ID not in ('6033010334')"
        var parameters = [AGENT_CD,"FORM","N",APP_ID,SIS_ID,REFAPPID, REFSISID];
        debug("paramaterss   --->"+parameters);

        CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,query,parameters,
                        function(tx,res){
                            debug("sync data res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                 var docData = {};
                                 var request = {};
                                 request.AC = AGENT_CD;
                                 request.PWD = PWD;
                                 request.DVID = DVID;
                                 request.ACN = "FDS";
                                 request.DOCUPLOAD = [];
                                 docData.REQ = request;

                                 for(var i=0;i<res.rows.length;i++){
                                    docData.REQ.DOCUPLOAD.push(res.rows.item(i));

                                    if(i == (res.rows.length - 1)){
                                        debug("data to send is "+DOCUMENT_UPLOAD_URL+"?Key =" + JSON.stringify(docData.REQ));
                                        CommonService.APP_AJAX_CALL(DOCUMENT_UPLOAD_URL,AJAX_TYPE,TYPE_JSON,docData,AJAX_ASYNC,AJAX_TIMEOUT,function(ajaxResponse){
                                            debug("response is "+JSON.stringify(ajaxResponse));
                                            debug("<<<<<< DATA SYNCING ENDED >>>>>>>");
                                            dfd.resolve(self.updateSyncedData(ajaxResponse));
                                        },function(data, status, headers, config, statusText){
                                            //debug("error in response"+JSON.stringify(data));
                                            debug("<<<<<< DATA SYNCING ENDED >>>>>>>");
                                            CommonService.hideLoading();
                                            dfd.resolve(null);
                                        })
                                    }
                                 }
                            }
                            else
                                dfd.resolve("S");
                        },
                        function(tx,err){
                            dfd.resolve(null);
                            CommonService.hideLoading();
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                    CommonService.hideLoading();
                },null
        );

        return dfd.promise;
    };

    this.updateSyncedData = function(resp){
        debug("<<<<<< UPDATION STARTED >>>>>>");
        var promises = [];
        var query = "update LP_DOCUMENT_UPLOAD set IS_DATA_SYNCED='Y' where DOC_CAT_ID=? AND DOC_CAT =? AND DOC_ID = ?";
        var response = resp.RS;

        if(!!resp && !!response && !!response.DOCUPLOAD && response.DOCUPLOAD.length > 0){
            for(var i=0;i<response.DOCUPLOAD.length;i++){
                var data = response.DOCUPLOAD[i];
                var flag = data.RESP;
                if(flag === "S"){
                    var parameters = [data.CATID,"FORM",data.DOCID];
                    promises.push(self.innerUpdateData(query,parameters));
                }else{
                    debug("failed to update for doc id "+data.DOC_ID);
                    CommonService.hideLoading();
                }
            }
        }

        return $q.all(promises);
    };

    //common query for insertion...
    this.innerUpdateData = function(query,params){
        var dfd = $q.defer();
        CommonService.transaction(db,
        function(tx){
                CommonService.executeSql(tx,query,params,
                    function(tx,res){
                        debug("update res.rows.length: " + res.rows.length);
                        if(!!res){
                            debug("updated success ");
                            dfd.resolve(true);
                        }
                        else{
                            debug("updated failed ");
                            dfd.resolve(false);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(false);
                        CommonService.hideLoading();
                    }
                );
            },
            function(err){
                dfd.resolve(false);
            },null
        );
        return dfd.promise;
    };

    this.getFileData = function(AGENT_CD,APP_ID,SIS_ID,REFAPPID){
        debug("<<<<< GET ALL THE FILES FROM DB STARTED >>>>>>>");
        var dfd = $q.defer();
        var query = "SELECT DOC_NAME,DOC_CAT_ID,POLICY_NO, DOC_ID, DOC_PAGE_NO,DOC_CAT, DOC_TIMESTAMP,DOC_CAP_ID,DOC_CAP_REF_ID from LP_DOCUMENT_UPLOAD where AGENT_CD = ? and DOC_CAT = ? and IS_DATA_SYNCED = ? and (IS_FILE_SYNCED = ? OR IS_FILE_SYNCED is null) and DOC_CAT_ID in (?, ?, ?) and DOC_ID not in ('6033010334')"
        var parameters = [AGENT_CD,"FORM","Y","N",APP_ID,SIS_ID,REFAPPID];

        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameters,
                    function(tx,res){
                        debug("getFileData res.rows.length: " + res.rows.length);
                        dfd.resolve(res);
                    },
                    function(tx,err){
                        debug("getFileData >> error in tx "+JSON.stringify(err));
                        CommonService.hideLoading();
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                debug("getFileData >> transaction "+JSON.stringify(err));
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.synFileData = function(res,AGENT_CD,APP_ID,PWD,DVID,imagePath){
        var docFileData = [];
        debug("BASED ON FILE IMAGE/HTML CALL FILE UPLOAD STARTED");
        for(var i=0;i<res.rows.length;i++){
            var data = res.rows.item(i);
            if(data.DOC_NAME.indexOf("jpg") == -1 && data.DOC_NAME.indexOf("pdf") == -1){
                debug("HTML SERVLET STARTED");
                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.mimeType = "application/octet-stream";
                options.headers={'contentType':'multipart/form-data'};
                options.httpMethod = "POST";
                var params = {};
                options.fileName = data.DOC_NAME;
                params.AC = AGENT_CD;
                params.DVID = DVID;
                params.PWD = PWD;
                params.ACN = "SCFD";
                params.CATID = data.DOC_CAT_ID;
                params.CAT = data.DOC_CAT;
                params.DOCID = data.DOC_ID;

                params.DOCCID = data.DOC_CAP_ID;
                params.DOCRID = data.DOC_CAP_REF_ID;

                params.ISASUB = null;
                params.FILETYPE = "HTML";
                params.COMBO = null;

                debug("PARAMTERS >>> "+JSON.stringify(params));
                options.params = params;
                docFileData.push(self.uploadJPGImageFileData(AGENT_CD,APP_ID,PWD,DVID,"/FromClient/APP/HTML/",options,data.DOC_NAME,"HTML"));
            }
            else{
                debug("IMAGE SERVLET CALL STARTED OR PDF SERVLET CALL STARTED");
                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.mimeType = "image/jpeg";
                options.headers={'contentType':'multipart/form-data'};
                options.httpMethod = "POST";
                var params = {};
                options.fileName = data.DOC_NAME;
                params.AC = AGENT_CD;
                params.DVID = DVID;
                params.PWD = PWD;
                params.ACN = "SID";
                params.FRM = data.DOC_CAT_ID;
                params.POL = data.POLICY_NO;
                params.DOC = data.DOC_ID;
                params.PAG = data.DOC_PAGE_NO;
                params.TST = data.DOC_TIMESTAMP;
                params.DOC_NAME = data.DOC_NAME;

                params.DOCCID = data.DOC_CAP_ID;
                params.DOCRID = data.DOC_CAP_REF_ID;

                debug("PARAMTERS >>> "+JSON.stringify(params));
                options.params = params;

                docFileData.push(self.uploadJPGImageFileData(AGENT_CD,APP_ID,PWD,DVID,"/FromClient/APP/IMAGES/",options,data.DOC_NAME,"IMG"));
            }
        }
        return $q.all(docFileData);
    };

    //File upload data to server...
    this.uploadJPGImageFileData = function(AGENT_CD,APP_ID,PWD,DVID,imagePath,options,name,flag){
        var dfd = $q.defer();
        debug("flag is -----> "+flag);
        function sendFile(file,options){
            var innerDfd = $q.defer();
            if(flag == "HTML"){
                debug("INSIDE HTML");
                self.uploadFile(file,HTML_SYNC_URL,options,flag).then(
                    function(uploaded){
                        debug("uploaded");
						//changes 27-10-2016
                        innerDfd.resolve(uploaded);
                    }
                );
            }
            else{
                debug("INSIDE IMAGE");
                self.uploadFile(file,DOCUMENT_FILE_UPLOAD_URL,options,flag).then(
                    function(uploaded){
                        debug("uploaded");
						//changes 27-10-2016
                        innerDfd.resolve(uploaded);
                    }
                );
            }
            return innerDfd.promise;
        }

        if(flag == "HTML"){
            //servlet call the decrypted url...
            CommonService.getTempFileURI(imagePath, name).then(
                function(file){
                    debug("OPTIONS is >>"+JSON.stringify(options));
                    sendFile(file,options).then(
                        function(resp){
                            dfd.resolve(resp);
                        }
                    );
                }
            );
        }else{
            CommonService.getTempNormalFileURI(imagePath, name).then(
                function(file){
                    debug("OPTIONS is >>"+JSON.stringify(options));
                    sendFile(file,options).then(
                        function(resp){
                            dfd.resolve(resp);
                        }
                    );
                }
            );
        }

        return dfd.promise;
    };

    //HTTP url code for file upload...
    this.uploadFile = function(file,name,options,flag){
        var dfd = $q.defer();

        var ft = new FileTransfer();
        debug("file: " + file);
        debug("SERVLET TO CALL IS "+name);

        ft.upload(file, name,function(succ){
            debug("Code = " + succ.responseCode);
            debug("Response = " + succ.response);
            debug("Sent = " + succ.bytesSent);
            if(!!succ.response && succ.response != "{}"){
                if(JSON.parse(succ.response).RS.RESP==="S"){
                    self.updateSyncFileData(JSON.parse(succ.response),options,flag).then(
                        function(insert){
                            dfd.resolve(insert);
                        }
                    );
                }
                else{
                    dfd.resolve(false);
                }
            }else{
                dfd.resolve(false);
            }
        },
        function(err){
            debug("FT upload error: " + JSON.stringify(err));
            dfd.resolve(false);
            CommonService.hideLoading();
        },options);

        return dfd.promise;
    };

    this.updateSyncFileData = function(resp,data,flagcheck){
        debug("<<<<<< UPDATION FILE SYNC STARTED >>>>>>");
        var dfd = $q.defer();
        var query = "UPDATE LP_DOCUMENT_UPLOAD set IS_FILE_SYNCED='Y' where DOC_CAT_ID=? AND DOC_CAT =? AND DOC_ID = ?";
        var response = resp.RS;
        debug("response to parse >>>"+JSON.stringify(response));
        debug("OPTIONS PARAMS >>>"+JSON.stringify(data));
        if(!!response){
            var flag = response.RESP;
            var param = data.params;
            if(flag === "S"){
                var parametes = [];
                if(flagcheck == "HTML"){
                    parameters = [param.CATID,"FORM",param.DOCID];
                }else{
                    parameters = [param.FRM,"FORM",param.DOC];
                }
                debug("parameters >>> "+JSON.stringify(parameters));
                self.innerUpdateData(query,parameters).then(
                    function(inserted){
                        debug("data inserted");
                        dfd.resolve(true);
                    }
                );
            }else{
                debug("failed to update for doc id "+data.DOC_ID);
                dfd.resolve(false);
                CommonService.hideLoading();
            }
        }
        return dfd.promise;
    };

    this.updateFlagAndObject = function(data,whereClauseObj, isFatca){
        var dfd = $q.defer();

        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(data)}, whereClauseObj).then(
            function(res){
				if(!isFatca){
	                CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"IS_DOC_DET" :"Y"}, whereClauseObj).then(
	                    function(res){
	                        debug("IS_DOC_DET update success !!"+JSON.stringify(whereClauseObj));
	                        dfd.resolve(true);
	                    }
	                );
				}
				else{
					dfd.resolve(true);
				}
            }
        );

        return dfd.promise;
    };

    //27-12-2016
    this.updateDocsList = function(data,whereClauseObj){
        var dfd = $q.defer();
        CommonService.updateRecords(db, 'LP_APP_CONTACT_SCRN_A', {"DOCS_LIST": JSON.stringify(data)}, whereClauseObj).then(
            function(res){
                dfd.resolve(true);
            }
        )
        return dfd.promise;
    };



}]);

/*
    this.getComboRelatedData = function(COMBO_ID,AGENT_CD){
        var dfd = $q.defer();

        var query = "SELECT LAM.APPLICATION_ID as APPLICATION_ID,LAM.POLICY_NO as POLICY_NO FROM LP_APPLICATION_MAIN as LAM, LP_COMBO_DTLS as LCD where LCD.APPLICATION_ID = LAM.APPLICATION_ID and LCD.COMBO_ID = ?";
        var parameters = [COMBO_ID,AGENT_CD];
        var comboArr = [];

        CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,query,parameters,
                        function(tx,res){
                            debug("Combo Data res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                for(var i=0;i<res.rows.length;i++){
                                    var comboObj = {};
                                    comboObj.APPLICATION_ID = res.rows.item(i).APPLICATION_ID;
                                    comboObj.POLICY_NO = res.rows.item(i).POLICY_NO;
                                    comboArr.push(comboObj);
                                }
                                dfd.resolve(comboArr);
                            }
                            else
                                dfd.resolve(null);
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
        );

        return dfd.promise;
    }
*/
