loginModule.controller('LoginCtrl', ['$state', 'LoginService', 'CommonService', 'ForgotPasswordService', 'UserData', function($state, LoginService, CommonService, ForgotPasswordService, UserData){
	"use strict";
	var lc = this;

	this.version = VERSION_NO;

	this.appName = CommonService.getAppName();

	this.usernameRegex = USERNAME_REGEX;

	this.username = null;
	this.password = null;

	BUSINESS_TYPE = (!!localStorage.BUSINESS_TYPE && localStorage.BUSINESS_TYPE!="null")?(localStorage.BUSINESS_TYPE):(!!UserData?(UserData.BUSSINESS_TYPE_ID):null);
	localStorage.BUSINESS_TYPE = BUSINESS_TYPE;
	localStorage.DVID = (!!localStorage.DVID && localStorage.DVID!="null")?(localStorage.DVID):(!!UserData?(UserData.DEVICE_ID):"NA");
	localStorage.EMPFLG = (!!localStorage.EMPFLG && localStorage.EMPFLG!="null")?(localStorage.EMPFLG):(!!UserData?(UserData.EMPLOYEE_FLAG):null);

	var checkLocalStorage = CommonService.checkLoginLocalStorage();
	debug("after differ checkLocalStorage: " + JSON.stringify(checkLocalStorage));
	if(checkLocalStorage){
		this.username = checkLocalStorage[0].username;
		debug("set user name and pass: ");
	}
	else{
		CommonService.removeLocalStorageForLogin();
		debug("removeLocalStorageForLogin: ");
	}


	this.ntidLogin = CommonService.isNTIDLogin();
	debug("this.ntidLogin: " + this.ntidLogin + " " + localStorage.EMPFLG);

	LoginService.lgnSrvObj.ntidLogin = this.ntidLogin;


	// hide forgot password link if it is FTL (localStorage.EMPFLG is undefined or null) or (the Business type in (iWealth, IndusSolution)) or (the Employee Flag in (Y, E, R, S))
	this.hideForgotPasswordLink = (!localStorage.EMPFLG) || this.ntidLogin;

	this.doLogin = function(){
		CommonService.showLoading();
		var loginType = CommonService.checkConnection();
		if(loginType == "offline"){
			LoginService.doLogin(this.username, this.password, this.ntidLogin);
			CommonService.setLocalStorageForLogin(this.username, this.password);
			debug("Set the user id and password when use offline" );
		}
		else{
			CommonService.checkSslPinning().then(function(response){
				if(response=="CONNECTION_SECURE"){
					CommonService.compairCurrentDateWithUTC().then(function(response){
						debug("diffHours checkIfValidate: ");
						debug(response);
						if(!!response){
							if(response.convertedTime){
								LoginService.doLogin(lc.username, lc.password, lc.ntidLogin);
								CommonService.setLocalStorageForLogin(lc.username, lc.password);
								debug("Set the user id and password when use online (convertedTime)" );
							}
							else{
								var message = "Your TAB date/time is not in sync with system time. \nTAB date/time: " + response.CurrDate + "\nSystem date/time: " + response.utcTime + "\n \n Please change your TAB date/time.";
								navigator.notification.alert(message, function(){
									CommonService.hideLoading();
								}, "Alert", "OK");
							}
						}
						else{
							LoginService.doLogin(lc.username, lc.password, lc.ntidLogin);
							CommonService.setLocalStorageForLogin(lc.username, lc.password);
							debug("Set the user id and password when use online." );
						}
					});
				}
				else if (response == "CONNECTION_NOT_SECURE"){
					navigator.notification.alert("CONNECTION NOT SECURE.",function(){CommonService.hideLoading();},"Login","OK");
				}
				else if (response == "CONNECTION_FAILED") {
					navigator.notification.alert("CONNECTION FAILED",function(){CommonService.hideLoading();},"Login","OK");
				}
				else{
					navigator.notification.alert("CONNECTION NOT SECURE.",function(){CommonService.hideLoading();},"Login","OK");
				}
			});
		}
	};

	this.forgotPassword = function(){
		CommonService.showLoading();
		ForgotPasswordService.getSecurityQuestionData(this.username).then(
			function(res){
				if(!!res)
					LoginService.forgotPassword(lc.username, lc.password, localStorage.DVID, res);
				else {
					navigator.notification.alert("Please enter the correct username.",function(){
						CommonService.hideLoading();
					},"Forgot Password","OK");
				}
			}
		);
	};

	this.resetLogin = function() {
		this.username = null;
		this.password = null;
	};

	this.logout= function() {
		debug("in logout");
		$state.go(LOGIN_STATE);
	};

	CommonService.hideLoading();

}]);

loginModule.service('LoginService',['$q', '$http', '$state','CommonService', '$filter', '$rootScope', function($q, $http, $state, CommonService, $filter, $rootScope){
	"use strict";
	var ls = this;
	this.lgnSrvObj = {};
	this.lgnSrvObj.username = null;
	this.lgnSrvObj.password = null;
	this.lgnSrvObj.dvid = null;
	this.lgnSrvObj.loginType = null;
	this.lgnSrvObj.userinfo = null;
	this.lgnSrvObj.errorDescObj = null;

	this.forgotPassword = function(username, password, dvid, SecQuesData){
		this.lgnSrvObj.username = username || null;
		this.lgnSrvObj.password = password || null;
		this.lgnSrvObj.dvid = dvid || null;
		$state.go(FORGOT_PASSWORD_STATE, {"SecQuesData": SecQuesData});
	};

	this.fetchErrDescMasterData = function(language){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_error_desc_master', true, null, {"language":language},null).then(
			function(res){
				if(!!res && res.rows.length>0){
					var errDescObj = {};
					for(var i=0;i<res.rows.length;i++){
						errDescObj[res.rows.item(i).ERROR_CODE] = res.rows.item(i).ERROR_DESCRIPTION;
					}
					dfd.resolve(errDescObj);
				}
				else{
					debug("lp_error_desc_master is empty");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.doLogin = function(username, password, ntidLogin) {
		CommonService.showLoading("Authenticating...");

		ls.lgnSrvObj.loginType = CommonService.checkConnection();
		debug("ls.lgnSrvObj.loginType: " + JSON.stringify(navigator) + "\t" + ls.lgnSrvObj.loginType + "\t" + navigator.onLine);
		ls.loadUserInfo(username).then(
			function(res){
				if(!!res){
					ls.lgnSrvObj.userinfo = res;
					ls.lgnSrvObj.dvid = res.DEVICE_ID;
					debug("ls.lgnSrvObj.userinfo: " + JSON.stringify(ls.lgnSrvObj.userinfo));
					debug("ls.lgnSrvObj.dvid: " + JSON.stringify(ls.lgnSrvObj.dvid));
				}
				debug("Doing login: " + ls.lgnSrvObj.loginType + "\t" + username + "\t" + password);
				ls.fetchErrDescMasterData(LANGUAGE).then(
					function(errDescObj){
						ls.lgnSrvObj.errorDescObj = errDescObj;
					}
				);
				ls.valNdAuthInput(username, password, ls.lgnSrvObj.userinfo, ls.lgnSrvObj.loginType).then(
					function(valRes){
						if(!!valRes){
							if(valRes == "ONL_FTL")
								ls.doOnlineLogin("FTL", username, username, password, localStorage.DVID || "NA", device.uuid, ntidLogin, BUSINESS_TYPE);
							else if(valRes == "ONL_UL"){
								ls.doOnlineLogin("UL", ls.lgnSrvObj.userinfo.LOGIN_ID, ls.lgnSrvObj.userinfo.AGENT_CD, password, ls.lgnSrvObj.userinfo.DEVICE_ID, device.uuid, ntidLogin, BUSINESS_TYPE, VERSION_NO);
							}
							else if(valRes == "OFL_UL")
								ls.doOfflineLogin(ls.lgnSrvObj.userinfo.AGENT_CD, password, ls.lgnSrvObj.userinfo);
							else{
								debug("This has been dealt with.");
							}
						}
						else{
							CommonService.hideLoading();
							navigator.notification.alert("Could not validate the user.",null,"Login","OK");
						}
					}
				);
			}
		);
	};

	this.loadUserInfo = function(username){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_userinfo', true, null, {"login_id":username},null).then(
			function(res){
				if(!!res && res.rows.length > 0)
					dfd.resolve(res.rows.item(0));
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.valNdAuthInput = function(username, password, userinfo, loginType){
		var dfd = $q.defer();
		if((!username) || username.trim()===""){
			CommonService.hideLoading();
			navigator.notification.alert("Username cannot be blank.",null,"Login","OK");
			dfd.resolve("dealt_with");
		}
		else if((!password) || password.trim()===""){
			CommonService.hideLoading();
			navigator.notification.alert("Password cannot be blank.",null,"Login","OK");
			dfd.resolve("dealt_with");
		}
		else{
			if(!userinfo){
				// FTL
				if(!loginType || loginType === "offline"){
					debug("Please connect to internet for first time login " + loginType);
					CommonService.hideLoading();
					navigator.notification.alert("If you are a first time user please connect to the internet, else please enter the correct username.",null,"Login","OK");
					dfd.resolve("dealt_with");
				}
				else{
					// online login
					debug("FTL");
					dfd.resolve("ONL_FTL");
				}
			}
			else{
				// UL
				if(userinfo.LOGIN_ID != username.trim()){
					CommonService.hideLoading();
					navigator.notification.alert("This device is registered to some other user.",null,"Login","OK");
					dfd.resolve("dealt_with");
				}
				else if ((loginType == "offline") && (!userinfo.PASSWORD)){
					CommonService.hideLoading();
					navigator.notification.alert("Please connect to the internet to complete the online login/ change password process.",null,"Login","OK");
					dfd.resolve("dealt_with");
				}
				else if((loginType == "offline") && (userinfo.PASSWORD != password)){
					CommonService.hideLoading();
					navigator.notification.alert("Invalid password.",null,"Login","OK");
					ls.invalidLogin(userinfo.AGENT_CD, userinfo.DEVICE_ID);
					dfd.resolve("dealt_with");
				}
				else{
					// normal login
					if(loginType == "offline"){
						// offline login
						debug("calling doOfflineLogin");
						dfd.resolve("OFL_UL");
					}
					else{
						// online login
						debug("calling doOnlineLogin");
						dfd.resolve("ONL_UL");
					}
				}
			}
		}
		return dfd.promise;
	};

	this.doOnlineLogin = function(type,loginId,username,password,dvid,imei,ntidLogin,bti,version){
		ls.lgnSrvObj.username = username;
		ls.lgnSrvObj.password = password;
		if(type === "FTL"){
			this.firstTimeLogin(loginId,password,dvid,imei,ntidLogin,bti);
		}
		else if(type==="RPL"){
			console.log("All values: ",type,loginId,username,password,dvid,imei,ntidLogin,bti,version);
			debug("In RPD loop");
			this.refreshPolicyData(loginId,username,password,dvid,ntidLogin,bti,version).then(
				function(refreshresponse){
				debug("Response :",refreshresponse);
				if(!!refreshresponse && refreshresponse=="success")
					navigator.notification.alert("Your data has been Refreshed Successfully",null,"Policy Tracker","OK");
					$state.go("dashboard.home");
					setTimeout(function(){
						if(!!localStorage.getItem("TabStateValue")){
							var stateVal="dashboard.policyTracker."+localStorage.getItem("TabStateValue");
							$state.go(stateVal);
							console.log("In App login ",localStorage.getItem("TabStateValue"));
						}
					}, 1500);
					CommonService.hideLoading();
				}
			);
		}
		else if(type === "UL"){
			this.usualLogin(loginId,username,password,dvid,ntidLogin,bti,version).then(
				function(loginResp){
					if(!!loginResp && loginResp=="success")
						$state.go(DASHBOARD_HOME_STATE);
				}
			);
		}
		else{
			debug("Unknown login type");
		}
	};

	this.firstTimeLogin = function(loginId,password,dvid,imei,ntidLogin,bti){
		var ftlResp, username;
		ls.preLoginSubmit(loginId||null).then(
			function(btiResp){
				debug("pre login submit");
				if(!bti && !!btiResp){
					localStorage.BUSINESS_TYPE = btiResp;
					BUSINESS_TYPE = btiResp;
					debug("BTR BUSINESS_TYPE: " + BUSINESS_TYPE);
					debug("BTR localStorage.BUSINESS_TYPE: " + localStorage.BUSINESS_TYPE);
					return btiResp;
				}
				else{
					return bti;
				}
			}
		)
		.then(
			function(btiResp){
				debug("going in ftl " + btiResp);
				if(!!btiResp){
					bti = btiResp;
					return ls.ftlHandshake(loginId,password,dvid,ntidLogin,bti).then(
						function(_ftlResp){
							ftlResp = _ftlResp;
							dvid = localStorage.DVID;
							if(!!ftlResp && ftlResp!="SKIP"){
								debug("ftlResp: " + JSON.stringify(ftlResp));
								username = ftlResp.RS.AC;
								ls.lgnSrvObj.username = ftlResp.RS.AC;
								CommonService.showLoading("Downloading data...please wait...");
								return ls.updateCatalogUserMaster_AC(username);
							}
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(updtCatRes){
				if(!!updtCatRes && updtCatRes!="SKIP"){
					debug("updtCatRes:: " + JSON.stringify(updtCatRes));
					if(BUSINESS_TYPE!="IndusSolution" && !!ftlResp.RS.BDF && ftlResp.RS.BDF=="Y"){
						return ls.doRestoration(username, password, dvid);
					}
					else {
						return "S";
					}
				}
			}
		)
		.then(
			function(restorationRes){
				debug("restorationRes: " + restorationRes);
				debug("ftlResp: " + JSON.stringify(ftlResp) + " " + !!ftlResp);
				if(BUSINESS_TYPE!="IndusSolution" && restorationRes!="SKIP" && (!!ftlResp && !!ftlResp.RS && ftlResp.RS.BDF=="Y")){
					debug("updtCatRes:: " + JSON.stringify(restorationRes));
					if(!restorationRes || restorationRes=="F"){
						return ls.showBackupFailedMessage().then(
							function(res){
								return ls.doTableDataDownload(username, password, ftlResp.RS.mandatoryDataList, ftlResp.RS.DVID);
							}
						);
					}
					else {
						return ls.doTableDataDownload(username,password,ftlResp.RS.mandatoryDataList,ftlResp.RS.DVID);
					}
				}
				else {
					if(!!ftlResp && !!ftlResp.RS){
						return ls.doTableDataDownload(username,password,ftlResp.RS.mandatoryDataList,ftlResp.RS.DVID);
					}
					else{
						return null;
					}
				}
			}
		)
		.then(
			function(tblDtDwnldResp){
				if(!!tblDtDwnldResp && tblDtDwnldResp!="SKIP") {
					debug("tblDtDwnldResp:: " + JSON.stringify(tblDtDwnldResp));
					return ls.doDocumentDownload(username,password,ftlResp.RS.mandatoryDocumentList,ftlResp.RS.DVID);
				}
				else {
					return tblDtDwnldResp;
				}
			}
		)
		.then(
			function(docDwnldResp){
				if(!!docDwnldResp && docDwnldResp!="SKIP"){
					debug("docDwnldResp:: " + JSON.stringify(docDwnldResp));
					return ls.storePolicies(username,ftlResp.RS.fetchPolicyDetails);
				}
				else {
					return docDwnldResp;
				}
			}
		)
		.then(
			function(storePoliciesRes){
				if(!!storePoliciesRes && storePoliciesRes!="SKIP"){
					debug("storePoliciesRes:: " + JSON.stringify(storePoliciesRes));
					return ls.confirmPolicySync(username,password,dvid,storePoliciesRes);
				}
				else {
					return storePoliciesRes;
				}
			}
		)
		.then(
			function(cpsResp){
				if(!!cpsResp && cpsResp!="SKIP"){
					debug("CPS Done:: " + JSON.stringify(cpsResp));
					debug("ftlResp.RS.EMPFLG:: " + ftlResp.RS.EMPFLG);
					if(!!ftlResp.RS.EMPFLG && ftlResp.RS.EMPFLG != "N" && ftlResp.RS.EMPFLG != "L"){
						return ls.doFetchAgentDetails(username,password,ftlResp.RS.DVID);
					}
					else{
						// Change state and go to change password page
						debug("changing state");
						localStorage.EMPFLG = ftlResp.RS.EMPFLG;
						$state.go(FTL_CP_STATE);
						return "SKIP";
					}
				}
				else {
					return cpsResp;
				}
			}
		)
		.then(
			function(fadRes){
				debug("fadRes:: " + JSON.stringify(fadRes));
				if(!!fadRes && fadRes!="SKIP" && fadRes.RS.RESP==="S"){
					return ls.doUpdateAgentDetails(password,fadRes);
				}
				else{
					return fadRes;
				}
			}
		).then(
			function(updAgntDtlsResp){
				if(!!updAgntDtlsResp && updAgntDtlsResp!="SKIP"){
					debug("updAgntDtlsResp not success: " + JSON.stringify(updAgntDtlsResp));
					localStorage.EMPFLG = ftlResp.RS.EMPFLG;
					$state.go(DASHBOARD_HOME_STATE);
				}
				else{
					if(updAgntDtlsResp!="SKIP" && !!ftlResp && ftlResp.RS.RESP=="S"){
						//ftlResp.RS.RESP=="S" condition added as this block should show error if FTL login is successful but some other part of the FTL process fails, FTL login errors are handled separately
						navigator.notification.alert("Could not complete login. Please try again later.",function(){CommonService.hideLoading();},"Login","OK");
					}
				}
			}
		);
	};

	 this.refreshPolicyData = function(loginId, username, password, dvid, ntidLogin, bti, version){
		var dfd = $q.defer();
		var policyThreshold, loginResp;
		ls.getConfigMasterValue('Policy Thresh-hold Agent').then(
			function(_policyThreshold){
				policyThreshold = (!!_policyThreshold)?_policyThreshold:"4";
				debug("policyThreshold: " + JSON.stringify(policyThreshold) + "  " + ntidLogin + "  " + bti);
				return ls.getPolicyCountAndType(username);
			}
		)
		.then(
			function(polCountResp){
				var polType = "(C,U)";
				var polCount = "0";
				debug("polCountResp: " + JSON.stringify(polCountResp));
				if(!!polCountResp){
					if(polCountResp.C_COUNT===0 && polCountResp.U_COUNT===0){
						polCount = policyThreshold;
					}
					else if(polCountResp.C_COUNT < polCountResp.U_COUNT){
						polType = "C";
						polCount = (parseInt(policyThreshold) - parseInt(polCountResp.C_COUNT)) + "";
					}
					else if(polCountResp.C_COUNT > polCountResp.U_COUNT){
						polType = "U";
						polCount = (parseInt(policyThreshold) - parseInt(polCountResp.U_COUNT)) + "";
					}
				}
				return ls.refreshHandshake(username, password, dvid, polCount, polType, version, bti);
			}
		)
		.then(
			function(_loginResp){
				loginResp = _loginResp;
				debug("loginResp: " + JSON.stringify(loginResp));
				if(!!loginResp && loginResp.RS.RESP=="S"){
					if(loginResp.RS.EC == 21){
						CommonService.hideLoading();
						cordova.exec(null,null,"PluginHandler","updateCheck",[]);
						return "SKIP";
					}
					else{
						CommonService.showLoading("Downloading data...please wait...");
						return ls.resetInvalidLoginCount(username);
					}
				}
			}
		).then(
			function(docDataResp){
				if(!!docDataResp && docDataResp!="SKIP"){
					return ls.storePolicies(username,loginResp.RS.fetchPolicyDetails);
				}
				else{
					return docDataResp;
				}
			}
		)
		.then(
			function(cpsResp){
				if(!!cpsResp && cpsResp!="SKIP"){
					debug("CPS Done");
					return ls.storePendingPolicies(username,loginResp.RS.policyPendingList);
				}
				else{
					return cpsResp;
				}
			}
		)
		.then(
			function(polPendResp){
				if(!!polPendResp && polPendResp!="SKIP"){
					var errFlag = false;
					angular.forEach(polPendResp,function(polPendRespRec){
						if(!polPendRespRec)
							errFlag = true;
					});
					if(errFlag){
						debug("Should not go to optional downloads");
						navigator.notification.alert("Could not complete Refresh. Please try again later.",function(){CommonService.hideLoading();},"Refresh","OK");
						dfd.resolve(null);
					}
					else{
						debug("Policy Pending insert done");
						debug("Should now go to optional downloads");
						dfd.resolve("success");
					}
				}
				else{
					if(polPendResp!="SKIP"){
						navigator.notification.alert("Could not complete Refresh. Please try again later.",function(){CommonService.hideLoading();},"Refresh","OK");
					}
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.usualLogin = function(loginId, username, password, dvid, ntidLogin, bti, version){
		var dfd = $q.defer();
		var policyThreshold, loginResp;
		ls.getConfigMasterValue('Policy Thresh-hold Agent').then(
			function(_policyThreshold){
				policyThreshold = (!!_policyThreshold)?_policyThreshold:"4";
				debug("policyThreshold: " + JSON.stringify(policyThreshold) + "  " + ntidLogin + "  " + bti);
				return ls.getPolicyCountAndType(username);
			}
		)
		.then(
			function(polCountResp){
				var polType = "(C,U)";
				var polCount = "0";
				debug("polCountResp: " + JSON.stringify(polCountResp));
				if(!!polCountResp){
					if(polCountResp.C_COUNT===0 && polCountResp.U_COUNT===0){
						polCount = policyThreshold;
					}
					else if(polCountResp.C_COUNT < polCountResp.U_COUNT){
						polType = "C";
						polCount = (parseInt(policyThreshold) - parseInt(polCountResp.C_COUNT)) + "";
					}
					else if(polCountResp.C_COUNT > polCountResp.U_COUNT){
						polType = "U";
						polCount = (parseInt(policyThreshold) - parseInt(polCountResp.U_COUNT)) + "";
					}
				}
				return ls.ulHandshake(username, password, dvid, polCount, polType, version, bti);
			}
		).then(
			function(_loginResp){
				loginResp = _loginResp;
				debug("loginResp: " + JSON.stringify(loginResp));
				if(!!loginResp && loginResp.RS.RESP=="S"){
					if(loginResp.RS.EC == 21){
						CommonService.hideLoading();
						cordova.exec(null,null,"PluginHandler","updateCheck",[]);
						return "SKIP";
					}
					else{
						CommonService.showLoading("Downloading data...please wait...");
						return ls.resetInvalidLoginCount(username);
					}
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(invLgnRes){
				if(!!invLgnRes && invLgnRes=="success"){
					return ls.updateUserInfo(username,password,loginResp.RS.PWED,loginResp.RS.AVGR);
				}
				else{
					return invLgnRes;
				}
			}
		)
		.then(
			function(updateUserInfoResp){
				if(!!updateUserInfoResp && updateUserInfoResp=="success"){
					return ls.updateLatestAgentInfo(username, password, dvid, loginResp.RS.AGNTV);
				}
				else{
					return updateUserInfoResp;
				}
			}
		).
		then(
			function(updateLatestAgentInfoResp){
				if(!!updateLatestAgentInfoResp && updateLatestAgentInfoResp=="success"){
					return ls.doPhotographDownload(username,password,dvid,loginResp.RS.PHRF);
				}
				else{
					return updateLatestAgentInfoResp;
				}
			}
		)
		.then(
			function(photoDownloadResp){
				if(!!photoDownloadResp && photoDownloadResp!="SKIP"){
					return ls.doTableDataDownload(username,password,loginResp.RS.mandatoryDataList,dvid);
				}
				else{
					return photoDownloadResp;
				}
			}
		)
		.then(
			function(tableDataResp){
				if(!!tableDataResp && tableDataResp!="SKIP"){
					return ls.doDocumentDownload(username,password,loginResp.RS.mandatoryDocumentList,dvid);
				}
				else{
					return tableDataResp;
				}
			}
		)
		.then(
			function(docDataResp){
				if(!!docDataResp && docDataResp!="SKIP"){
					return ls.storePolicies(username,loginResp.RS.fetchPolicyDetails);
				}
				else{
					return docDataResp;
				}
			}
		)
		.then(
			function(storePoliciesRes){
				if(!!storePoliciesRes && storePoliciesRes!="SKIP"){
					debug("Stored Policies");
					return ls.confirmPolicySync(username,password,dvid,storePoliciesRes);
				}
				else{
					return storePoliciesRes;
				}
			}
		)
		.then(
			function(cpsResp){
				if(!!cpsResp && cpsResp!="SKIP"){
					debug("CPS Done");
					return ls.storePendingPolicies(username,loginResp.RS.policyPendingList);
				}
				else{
					return cpsResp;
				}
			}
		)
		.then(
			function(polPendResp){
				if(!!polPendResp && polPendResp!="SKIP"){
					var errFlag = false;
					angular.forEach(polPendResp,function(polPendRespRec){
						if(!polPendRespRec)
							errFlag = true;
					});
					if(errFlag){
						debug("Should not go to optional downloads");
						navigator.notification.alert("Could not complete login. Please try again later.",function(){CommonService.hideLoading();},"Login","OK");
						dfd.resolve(null);
					}
					else{
						debug("Policy Pending insert done");
						debug("Should now go to optional downloads");
						dfd.resolve("success");
					}
				}
				else{
					if(polPendResp!="SKIP" && !!loginResp && loginResp.RS.RESP=="S"){
						//loginResp.RS.RESP=="S" condition added as this block should show error if UL login is successful but some other part of the UL process fails, UL login errors are handled separately
						navigator.notification.alert("Could not complete login. Please try again later.",function(){
							CommonService.hideLoading();
							dfd.resolve(null);
						},"Login","OK");
					}
				}
			}
		);
		return dfd.promise;
	};

	this.showBackupFailedMessage = function(){
		var dfd = $q.defer();
		navigator.notification.alert("This backup could not be restored, please try again later from the side menu.",function(){
			dfd.resolve("S");
		},"Login","OK");
		return dfd.promise;
	};

	this.isAgentUpdateRequired = function(username,latestVersion){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_userinfo', true, ['version_no'], {'agent_cd': username}, null).then(
			function(res){
				if(!!res && res.rows.length>0){
					if(res.rows.item(0).VERSION_NO < latestVersion)
						dfd.resolve(true);
					else
						dfd.resolve(false);
				}
				else
					dfd.resolve(false);
			}
		);
		return dfd.promise;
	};

	this.updateLatestAgentInfo = function(username,password,dvid,latestVersion){
		var dfd = $q.defer();
		latestVersion = (latestVersion || 1);
		ls.isAgentUpdateRequired(username,latestVersion).then(
			function(isAgentUpdateReqResp){
				if(isAgentUpdateReqResp){
					return ls.doFetchAgentDetails(username,password,dvid);
				}
				else
					dfd.resolve("success");
			}
		)
		.then(
			function(fadResp){
				if(!!fadResp && fadResp.RS.RESP==="S"){
					return ls.doUpdateAgentDetails(password,fadResp);
				}
				else{
					dfd.resolve(null);
				}
			}
		)
		.then(
			function(updateAgentDetailsResp){
				if(!!updateAgentDetailsResp && updateAgentDetailsResp=="success")
					dfd.resolve(updateAgentDetailsResp);
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.doPhotographDownload = function(username,password,dvid,isNewPhotoAvailable){
		var dfd = $q.defer();
		if(!!isNewPhotoAvailable && isNewPhotoAvailable=="Y"){
			ls.fetchPhoto(username,password,dvid).then(
				function(fetchPhotoResp){
					if(!!fetchPhotoResp){
						return CommonService.saveFile(username,IMG_TYPE_JPG,REL_PATH_PROFILE,fetchPhotoResp.RS.PHDET);
					}
					else
						dfd.resolve(null);
				}
			)
			.then(
				function(saveFileResp){
					if(!!saveFileResp && saveFileResp=="success"){
						return ls.confirmPhotoDownload(username,password,dvid);
					}
					else
						dfd.resolve(null);
				}
			)
			.then(
				function(confirmPhotoDownloadResp){
					debug("confirmPhotoDownloadResp: " + confirmPhotoDownloadResp);
					dfd.resolve(confirmPhotoDownloadResp);
				}
			);
		}
		else{
			dfd.resolve("success");
		}
		return dfd.promise;
	};

	this.fetchPhoto = function(username,password,dvid){
		var dfd = $q.defer();

		function gppSucc(response){
			debug("Resolving fetchPhoto()");
			dfd.resolve(response);
		}

		function gppErr(data, status, headers, config, statusText){
			dfd.resolve(null);
		}

		var photoReq = {};
		photoReq.REQ = {};
		photoReq.REQ.AC  = username;
		photoReq.REQ.DVID = dvid;
		photoReq.REQ.PWD = password;
		photoReq.REQ.ACN = "GPP";
		CommonService.ajaxCall(PHOTO_SERVLET_URL, AJAX_TYPE, TYPE_JSON, photoReq, AJAX_ASYNC, AJAX_TIMEOUT, gppSucc, gppErr);
		return dfd.promise;
	};

	this.confirmPhotoDownload = function(username,password,dvid){
		var dfd = $q.defer();
		function cppsSucc(response){
			debug("Resolving confirmPhotoDownload()");
			dfd.resolve(response);
		}
		function cppsErr(data, status, headers, config, statusText){
			dfd.resolve(null);
		}
		var photoReq = {};
		photoReq.REQ = {};
		photoReq.REQ.AC  = username;
		photoReq.REQ.DVID = dvid;
		photoReq.REQ.PWD = password;
		photoReq.REQ.ACN = "CPPS";
		CommonService.ajaxCall(PHOTO_SERVLET_URL, AJAX_TYPE, TYPE_JSON, photoReq, AJAX_ASYNC, AJAX_TIMEOUT, cppsSucc, cppsErr);
		return dfd.promise;
	};

	this.continueFTL = function(){
		debug("ls.lgnSrvObj.username: " + ls.lgnSrvObj.username);
		debug("ls.lgnSrvObj.password: " + ls.lgnSrvObj.password);
		debug("ls.lgnSrvObj.dvid: " + ls.lgnSrvObj.dvid);
		ls.doFetchAgentDetails(ls.lgnSrvObj.username,ls.lgnSrvObj.password,ls.lgnSrvObj.dvid).then(
			function(res){
				if(!!res && res.RS.RESP==="S"){
					var password = null;
					if(res.RS.FADDET[0].EMPLOYEE_FLAG != 'N'){
						password = ls.lgnSrvObj.password;
						debug("res.RS.FADDET.EMPLOYEE_FLAG: " + res.RS.FADDET[0].EMPLOYEE_FLAG);
					}
					return ls.doUpdateAgentDetails(password, res);
				}
				else
					return null;
			}
		)
		.then(
			function(resp){
				if(!!resp && resp=="success"){
					navigator.notification.alert("New password set successfully.",function(){
						$state.go(LOGIN_STATE);
					},"Login","OK");
				}
				else{
					navigator.notification.alert("Failed to set the new password.",function(){
						CommonService.hideLoading();
					},"Login","OK");
				}
			}
		);
	};

	this.ftlHandshake = function(lgd, password, dvid, ntidLogin, bti){
		var dfd = $q.defer();
		function ftlSucc(response){
			if(!!response && !!response.RS){
				switch(parseInt(response.RS.EC || 1000/*any number for default case*/)){
					case 0:	localStorage.DVID = response.RS.DVID;
							ls.lgnSrvObj.dvid = response.RS.DVID;
							dfd.resolve(response);
							break;
					case 1: CommonService.hideLoading();
							navigator.notification.alert("LGN_F_1: Device is already registered.\nPlease contact the help desk.",null,"Login","OK");
							dfd.resolve(null);
							break;
					case 2: CommonService.hideLoading();
							navigator.notification.alert("LGN_F_2: Your account is LOCKED or INACTIVE.\nPlease contact the help desk.",null,"Login","OK");
							dfd.resolve(null);
							break;
					case 3: CommonService.hideLoading();
							navigator.notification.alert("LGN_F_3: Agent's information is unavailable.\nPlease contact the help desk.",null,"Login","OK");
							dfd.resolve(null);
							break;
					case 4: CommonService.hideLoading();
							navigator.notification.alert("LGN_F_4: Invalid Credentials.\nPlease contact the help desk.",null,"Login","OK");
							dfd.resolve(null);
							break;
					default:CommonService.hideLoading();
							navigator.notification.alert("LGN_F: Login Failed.\nPlease contact the help desk.",null,"Login","OK");
							dfd.resolve(null);
				}
			}
			else{
				navigator.notification.alert("Login Failed.\nPlease contact helpdesk.",function(){
					CommonService.hideLoading();
					dfd.resolve(null);
				},"Login","OK");
			}
		}
		function ftlErr(data, status, headers, config, statusText) {
			navigator.notification.alert("Could not complete first time online login. Please check your network connection or try again after some time.",function(){
				CommonService.hideLoading();
				dfd.resolve(null);
			},"Login","OK");
		}

		ls.getConfigMasterValue('Policy Thresh-hold Agent').then(
			function(policyThreshold){
				policyThreshold = (!!policyThreshold)?policyThreshold:"4";
				var loginDetails = {};
				loginDetails.REQ = {};
				loginDetails.REQ.LGD = lgd;
				loginDetails.REQ.PWD = password;
				loginDetails.REQ.ACN = "FTL";
				loginDetails.REQ.DVID = dvid || "NA";
				loginDetails.REQ.IMEI = device.uuid;
				loginDetails.REQ.BTI = bti;
				loginDetails.REQ.AID = APP_NAME;
				debug("ftl ntidLogin: " + ntidLogin);

				loginDetails.REQ.PLC = policyThreshold;
				loginDetails.REQ.PLT = "(C,U)";

				loginDetails.REQ.EXD = CommonService.getUserExtraDetails(loginDetails.REQ.ACN);

				debug("JSONObject: " + DOMAIN_URL + "LoginServlet?Key=" + JSON.stringify(loginDetails));
				CommonService.ajaxCall(LOGIN_SERVLET_URL, AJAX_TYPE, TYPE_JSON, loginDetails, AJAX_ASYNC, AJAX_TIMEOUT, ftlSucc, ftlErr);
			}
		);
		return dfd.promise;
	};

	this.ulHandshake = function(username, password, dvid, policyCount, policyType, ipv, bti){
		var dfd = $q.defer();
		function ulSucc(response){
			debug("Online UL Success");
			if(!!response && !!response.RS && response.RS!={}){
				if(!!response.RS.RESP && response.RS.RESP=="S" && response.RS.EC=="0"){
					dfd.resolve(response);
				}
				else{
					if(!!ls.lgnSrvObj.errorDescObj && ls.lgnSrvObj.errorDescObj!={}){
						navigator.notification.alert("LGN_U_" + response.RS.EC + ": " + ls.lgnSrvObj.errorDescObj[response.RS.EC],function(){
							CommonService.hideLoading();
							dfd.resolve(null);
						},"Login","OK");
					}
					else{
						navigator.notification.alert("Could not complete online login.\n Please contact the helpdesk.",function(){
							CommonService.hideLoading();
							dfd.resolve(null);
						},"Login","OK");
					}
					if(!!response && response.RS.EC=="4"){
						ls.invalidLogin(username, dvid).then(
							function(invLgnRes){
								dfd.resolve(null);
							}
						);
					}
				}
			}
			else{
				CommonService.hideLoading();
				navigator.notification.alert("Login Failed.\nPlease contact the helpdesk.",null,"Login","OK");
				dfd.resolve(null);
			}
		}

		function ulErr(data, status, headers, config, statusText) {
			CommonService.hideLoading();
			navigator.notification.alert("Could not complete online login. Please check your network connection or try again after some time.",null,"Login","OK");
			dfd.resolve(null);
		}

		var loginDetails = {};
		loginDetails.REQ = {};
		loginDetails.REQ.AC = username;
		loginDetails.REQ.LGD = username;
		loginDetails.REQ.PWD = password;
		loginDetails.REQ.ACN = "UL";
		loginDetails.REQ.DVID = dvid;
		loginDetails.REQ.PC = policyCount;
		loginDetails.REQ.PT = policyType;
		loginDetails.REQ.IPV = ipv;
		loginDetails.REQ.BTI = bti;
		loginDetails.REQ.AID = APP_NAME;
		loginDetails.REQ.EXD = CommonService.getUserExtraDetails(loginDetails.REQ.ACN);
		debug("JSONObject: " + DOMAIN_URL + "LoginServlet?Key=" +JSON.stringify(loginDetails));
		CommonService.ajaxCall(LOGIN_SERVLET_URL, AJAX_TYPE, TYPE_JSON, loginDetails, AJAX_ASYNC, AJAX_TIMEOUT, ulSucc, ulErr);
		return dfd.promise;
	};

	//refresh handshake
	this.refreshHandshake = function(username, password, dvid, policyCount, policyType, ipv, bti){
		var dfd = $q.defer();
		function refSucc(response){
			debug("Online UL Success");
			if(!!response && !!response.RS){
				if(!!response.RS.RESP && response.RS.RESP=="S" && response.RS.EC=="0"){
					dfd.resolve(response);
				}
				else{
					if(!!ls.lgnSrvObj.errorDescObj && ls.lgnSrvObj.errorDescObj!={}){
						CommonService.hideLoading();
						navigator.notification.alert("Refresh" + response.RS.EC + ": " + ls.lgnSrvObj.errorDescObj[response.RS.EC],null,"Refresh","OK");
					}
					else{
						CommonService.hideLoading();
						navigator.notification.alert("Could not complete Refresh",null,"Refresh","OK");
					}
					if(!!response && response.RS.EC=="4")
						ls.invalidLogin(username, dvid);
					dfd.resolve(null);
				}
			}
			else{
				CommonService.hideLoading();
				navigator.notification.alert("Refresh Failed..",null,"Refresh","OK");
				dfd.resolve(null);
			}
		}

		function refError(data, status, headers, config, statusText) {
			CommonService.hideLoading();
			navigator.notification.alert("Could not complete online Refresh. Please check your network connection or try again after some time.",null,"Refresh","OK");
			dfd.resolve(null);
		}

		var refreshdata = {};
		refreshdata.REQ = {};
		refreshdata.REQ.AC = username;
		refreshdata.REQ.LGD = username;
		refreshdata.REQ.PWD = password;
		refreshdata.REQ.ACN = "RPL";
		refreshdata.REQ.DVID = dvid;
		refreshdata.REQ.PC = policyCount;
		refreshdata.REQ.PT = policyType;
		refreshdata.REQ.IPV = ipv;
		refreshdata.REQ.BTI = bti;
		debug("JSONObject: " + DOMAIN_URL + "FetchPolicyServlet?Key=" + JSON.stringify(refreshdata));
		CommonService.ajaxCall(REFRESH_DASHBOARD_DATA, AJAX_TYPE, TYPE_JSON, refreshdata, AJAX_ASYNC, AJAX_TIMEOUT, refSucc, refError);
		return dfd.promise;
	};

	this.updateUserInfo = function(username,password,pwed,avgr){
		var dfd = $q.defer();
		var updateClauseObj = {'password': password, 'lastlogin_date': CommonService.getCurrDate(), 'password_expiry_date': pwed, 'force_change_password_flag': 'N', 'avgrating': avgr, "lock_status": "U"};
		var whereClauseObj = {'agent_cd': username};
		CommonService.updateRecords(db, 'lp_userinfo', updateClauseObj, whereClauseObj).then(
			function(res){
				debug("successfully updated lp_userinfo");
				dfd.resolve("success");
			}
		);
		return dfd.promise;
	};

	this.doUpdateAgentDetails = function(password,resp){
		var dfd = $q.defer();
		resp.RS.FADDET[0].PASSWORD = password;
		resp.RS.FADDET[0].LASTLOGIN_DATE = CommonService.getCurrDate();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"PRAGMA table_info(lp_userinfo)",[],
					function(tx,result){
						/* doing this as incase there are columns in resp.RS.FADDET[0] (server's userinfo table) which are not available in sqlite client table */
						var insertRecord = {};
						for(var i=0;i<result.rows.length;i++){
							if(result.rows.item(i).name == "BUSSINESS_TYPE_ID"){
								insertRecord[result.rows.item(i).name] = (resp.RS.FADDET[0]).APP_BUSINESS_TYPE_ID;
							}
							else{
								insertRecord[result.rows.item(i).name] = (resp.RS.FADDET[0])[result.rows.item(i).name];
							}
						}
						CommonService.insertOrReplaceRecord(db, 'lp_userinfo', insertRecord, true).then(
							function(res){
								ls.lgnSrvObj.userinfo = insertRecord;
								debug("successfully updated agent info " + password);
								dfd.resolve("success");
							}
						);
					}
				);
			},
			function(err){
				debug("Error: " + err.message);
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.doFetchAgentDetails = function(username,password,dvid){
		var dfd = $q.defer();
		function fadSucc(resp){
			dfd.resolve(resp);
		}
		function fadErr(data, status, headers, config, statusText) {
			// this isn't happening:
			debug("Error in fetchAgentDetails: " + data);
			debug(status + " - " + statusText);
			CommonService.hideLoading();
			navigator.notification.alert("Could not process the request. Please try again after some time.",null,"Login","OK");
			dfd.resolve(null);
		}
		var request = {};
		request.REQ = {};
		request.REQ.AC = username;
		request.REQ.PWD = password;
		request.REQ.DVID = dvid;
		request.REQ.ACN = "FAD";
		CommonService.ajaxCall(USER_INFO_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, fadSucc, fadErr);
		return dfd.promise;
	};

	this.getTableData = function(username,password,compVerMapRec,dvid){
		var dfd = $q.defer();

		function ftdSucc(resp){
			debug("ftdSucc resp: " + resp);
			resp.CNAME = compVerMapRec.cname;
			resp.CID = compVerMapRec.cid;
			resp.NVER = compVerMapRec.nver;
			resp.AC = username;
			resp.PWD = password;
			resp.DVID = dvid;
			debug("Resolving: " + compVerMapRec.cname);
			dfd.resolve(resp);
		}

		function ftdErr(data, status, headers, config, statusText) {
			debug("error in getTableData");
			dfd.resolve(null);
		}

		if((!!compVerMapRec) && ((parseInt(compVerMapRec.cver)<parseInt(compVerMapRec.nver)) || (!!compVerMapRec.force && compVerMapRec.force==="Y"))){
			var request = {};
			request.REQ = {};
			request.REQ.AC = username;
			request.REQ.DVID = dvid;
			request.REQ.PWD = password;
			request.REQ.ACN = "FTD";
			request.REQ.CVER = compVerMapRec.cver + "";
			if(!!compVerMapRec.force && compVerMapRec.force==="Y"){
				request.REQ.CVER = "0";
			}
			request.REQ.COMPID = compVerMapRec.cid + "";
			request.REQ.NVER = compVerMapRec.nver + "";
			debug("ftdSucc req: " + JSON.stringify(request));
			CommonService.ajaxCall(CATALOG_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, ftdSucc, ftdErr);
		}
		else{
			dfd.resolve("success");
		}
		return dfd.promise;
	};

	this.insertOrReplaceTableData = function(tableName, ftdResponse){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, "PRAGMA table_info(" + tableName + ")", [],
					function(tx,result){
						/* doing this as incase there are columns in ftdResponseRec which are not available in sqlite client table */
						var insertList = [];
						var k = 0;
						angular.forEach(ftdResponse.RS.FTDDET, function(ftdResponseRec){
							insertList[k] = {};
							for(var i=0; i<result.rows.length; i++){
								insertList[k][result.rows.item(i).name] = ftdResponseRec[result.rows.item(i).name];
							}
							k++;
						});
						if(!!tableName && !!insertList && insertList.length>0){
							dfd.resolve(CommonService.bulkInsert(tableName, insertList));
						}
						else{
							dfd.resolve(null);
						}
					}
				);
			},
			function(err){
				debug("Error: " + err.message);
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.updateCatalogUserMaster = function(username,cid,nver,cname){
		var dfd = $q.defer();
		var insertRecord = { 'component_id': cid, 'agent_cd': username, 'current_version_no': nver, 'fetch_date': CommonService.getCurrDate(),'component_name': cname };
		CommonService.insertOrReplaceRecord(db, 'lp_catalog_user_master', insertRecord, true).then(
			function(res){
				debug("lp_catalog_user_master updated for table: " + cname);
				dfd.resolve({"CID":cid,"NVER":nver,"CNAME":cname,"SUCCESS":"Y"});
			}
		);
		return dfd.promise;
	};

	this.storeTableData = function(ftdResponseRec){
		var dfd = $q.defer();
		if(!!ftdResponseRec && !!ftdResponseRec.RS && !!ftdResponseRec.RS.FTDDET){
			ls.insertOrReplaceTableData(ftdResponseRec.CNAME,ftdResponseRec).then(
				function(insRes){
					if(!!insRes){
						return ls.updateCatalogUserMaster(ftdResponseRec.AC,ftdResponseRec.CID,ftdResponseRec.NVER,ftdResponseRec.CNAME);
					}
					else
						return null;
				}
			)
			.then(
				function(updCatUsrMstResp){
					if(!!updCatUsrMstResp){
						return ls.confirmDataDownload(ftdResponseRec.AC,ftdResponseRec.PWD,ftdResponseRec.CID,ftdResponseRec.NVER,ftdResponseRec.CNAME,ftdResponseRec.DVID);
					}
					else
						return null;
				}
			)
			.then(
				function(cnfTblDataResp){
					dfd.resolve(cnfTblDataResp || null);
				}
			);
		}
		else{
			dfd.resolve("success");
		}
		return dfd.promise;
	};

	this.confirmDataDownload = function(username,password,compid,nver,cname,dvid){
		var dfd = $q.defer();
		var request = {};
		request.REQ = {};
		request.REQ.AC = username;
		request.REQ.DVID = dvid;
		request.REQ.PWD = password;
		request.REQ.ACN = "CTD";
		request.REQ.COMPID = compid + "";
		request.REQ.NVER = nver + "";
		request.REQ.AID = APP_NAME;
		function ctdSucc(resp){
			debug("Confirmed table download for: " + cname);
			dfd.resolve(resp);
		}
		function ctdErr(data, status, headers, config, statusText) {
			debug("Confirmation failed for: " + cname);
			dfd.resolve(null);
		}
		CommonService.ajaxCall(CATALOG_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, ctdSucc, ctdErr);
		return dfd.promise;
	};

	this.confirmDocDownload = function(username,password,did,nver,dname,dvid){
		var dfd = $q.defer();
		var request = {};
		request.REQ = {};
		request.REQ.AC = username;
		request.REQ.DVID = dvid;
		request.REQ.PWD = password;
		request.REQ.ACN = "CDD";
		request.REQ.DOCID = did + "";
		request.REQ.NVER = nver + "";
		request.REQ.AID = APP_NAME;
		function cddSucc(resp){
			debug("Confirmed table download for: " + dname + " " + did + " " + nver);
			dfd.resolve(resp);
		}
		function cddErr(data, status, headers, config, statusText) {
			debug("Error confirming for document: " + dname);
			dfd.resolve(null);
		}
		CommonService.ajaxCall(DOCUMENT_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, cddSucc, cddErr);
		return dfd.promise;
	};

	this.getDocData = function(username,password,docVerMap,dvid){
		debug("in getDocData");
		var promises = [];
		angular.forEach(docVerMap, function(docVerMapRec) {
			function fdSucc(resp){
				debug("downloading doc: " + docVerMapRec.did);
				resp.DNAME = docVerMapRec.dname;
				resp.DID = docVerMapRec.did;
				resp.DPATH = docVerMapRec.dpath;
				resp.DTYPE = docVerMapRec.dtype;
				resp.NVER = docVerMapRec.nver;
				resp.PGLID = docVerMapRec.pglid;
				resp.AC = username;
				resp.PWD = password;
				resp.DVID = dvid;
			}

			function fdErr(data, status, headers, config, statusText) {
				debug("error in getDataDoc");
			}

			if((!!docVerMapRec) && (parseInt(docVerMapRec.cver)<parseInt(docVerMapRec.nver))){
				var request = {};
				request.REQ = {};
				request.REQ.AC = username;
				request.REQ.DVID = dvid;
				request.REQ.PWD = password;
				request.REQ.ACN = "FD";
				request.REQ.DOCID = docVerMapRec.did + "";
				request.REQ.NVER = docVerMapRec.nver + "";
				promises.push(CommonService.ajaxCall(DOCUMENT_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, fdSucc, fdErr));
			}
			else{
				promises.push(null);
			}
		});
		return $q.all(promises);
	};

	this.getFilteredTableList = function(tableDataList){
		var dfd = $q.defer();
		if(!!tableDataList){
			var tableNameParList = ["table"];
			var quesString = "?";
			for(var i=0; i<tableDataList.length; i++){
				tableNameParList.push(!!(tableDataList[i].NM)?(tableDataList[i].NM.toUpperCase()):"");
				quesString += ", ?";
			}
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, "select upper(tbl_name) as TABLE_NAME from sqlite_master where type=? and upper(tbl_name) in (" + quesString + ")", tableNameParList,
					function(tx, res){
						if(!!res && res.rows.length>0){
							var tableObjList = [];
							for(i=0; i<res.rows.length; i++){
								var tableName = ($filter("filter")(tableDataList, {"NM": res.rows.item(i).TABLE_NAME}));
								if(!!tableName){
									tableObjList.push(tableName[0]);
								}
							}
							dfd.resolve(tableObjList);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx, err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);

		return dfd.promise;
	};

	this.getComponentDetails = function(username, tableDataList){
		var dfd = $q.defer();

		var quesString = "?";
		var compIds = [];

		angular.forEach(tableDataList, function(tableDataRec){
			compIds.push(tableDataRec.ID);
		});

		for(var i=1; i<tableDataList.length; i++){
			quesString += ", ?";
		}

		compIds.push(username);

		debug("quesString: " + quesString);
		debug("compIds: " + compIds);

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, "select component_id, current_version_no, component_name from lp_catalog_user_master where component_id in (" + quesString + ") and agent_cd=?", compIds,
					function(tx, res){
						var compVerMap = [];
						if((!!res) && (res.rows.length>0)){
							var nverMap = {};
							angular.forEach(tableDataList, function(tableDataRec){
								nverMap[tableDataRec.ID] = tableDataRec.VER;
								var flag = true;
								for(var i=0; i<res.rows.length; i++){
									if(tableDataRec.ID == res.rows.item(i).COMPONENT_ID){
										compVerMap.push({ "cver":res.rows.item(i).CURRENT_VERSION_NO, "cid": res.rows.item(i).COMPONENT_ID, "cname": res.rows.item(i).COMPONENT_NAME, "nver": nverMap[res.rows.item(i).COMPONENT_ID], "force": tableDataRec.FCFLG||null });
										flag = false;
									}
								}
								if(flag){
									compVerMap.push({"cver": "0", "cid": tableDataRec.ID, "cname": tableDataRec.NM, "nver": tableDataRec.VER, "force": tableDataRec.FCFLG||null});
								}
							});
						}
						else{
							angular.forEach(tableDataList,function(tableDataRec){
								compVerMap.push({"cver": "0", "cid": tableDataRec.ID, "cname": tableDataRec.NM, "nver": tableDataRec.VER, "force": tableDataRec.FCFLG||null});
							});
						}
						dfd.resolve(compVerMap);
						debug("compVerMap.length: " + JSON.stringify(compVerMap) + compVerMap.length);
					},
					function(tx, err){
						dfd.resolve(null);
					}
				);
			}
		);
		return dfd.promise;
	};

	this.fetchAndStoreTable = function(username, password, dvid, compVerMapRec){
		debug("compVerMapRec: " + JSON.stringify(compVerMapRec));
		return ls.getTableData(username, password, compVerMapRec, dvid).then(
			function(getTableDataResp){
				return ls.storeTableData(getTableDataResp).then(
					function(storeTableDataResp){
						debug("storeTableDataResp: " + getTableDataResp.CNAME + " :: " + JSON.stringify(storeTableDataResp));
						return storeTableDataResp || null;
					}
				);
			},
			function(rejectMsg){
				return rejectMsg;
			}
		);
	};

	this.downloadTables = function(username, password, dvid, compVerMap){
		var promises = [];
		angular.forEach(compVerMap,function(compVerMapRec){
			promises.push(ls.fetchAndStoreTable(username, password, dvid, compVerMapRec));
		});
		return $q.all(promises);
	};

	this.doTableDataDownload = function(username, password, tableDataList, dvid){
		var dfd = $q.defer();
		if((!!tableDataList) && (tableDataList.length>0)){
			this.getFilteredTableList(tableDataList).then(
				function(tableDataListRes){
					tableDataList = JSON.parse(JSON.stringify(tableDataListRes));
					var compVerMap = [];
					debug("here");
					ls.getComponentDetails(username, tableDataList).then(
						function(compDtlsRes){
							compVerMap = compDtlsRes;
							ls.downloadTables(username, password, dvid, compVerMap).then(
								function(downloadTablesResp){
									var errFlag = false;
									angular.forEach(downloadTablesResp,function(downloadTablesRespRec){
										if(!downloadTablesRespRec){
											errFlag = true;
										}
									});
									if(errFlag){
										debug("resolving downloadTablesResp: null");
										dfd.resolve(null);
									}
									else{
										debug("resolving downloadTablesResp: " + downloadTablesResp);
										dfd.resolve(downloadTablesResp);
									}
								}
							);
						}
					);
				}
			);
		}
		else{
			debug("No data available for download");
			dfd.resolve("success");
		}
		return dfd.promise;
	};

	this.updateDocFetchUserMaster = function(username,did,nver,dname,dpath,dtype,pglId){
		var dfd = $q.defer();
		var insertRecord = { 'agent_cd': username, 'doc_id': did, 'fetch_date': CommonService.getCurrDate(), 'version_no': nver, 'doc_name': dname, 'doc_type': dtype, 'doc_path': dpath, 'issynced': 'N', 'pgl_id': pglId};
		CommonService.insertOrReplaceRecord(db, 'lp_doc_fetch_user_master', insertRecord, true).then(
			function(res){
				dfd.resolve({"DID":did,"NVER":nver,"DNAME":dname,"DPATH":dpath,"DTYPE":dtype,"SUCCESS":"Y"});
			}
		);
		return dfd.promise;
	};

	this.storeDocData = function(fdResponse){
		var promises = [];
		angular.forEach(fdResponse, function(fdResponseRec){
			if(!!fdResponseRec){
				promises.push(
					CommonService.saveFile(fdResponseRec.data.DNAME,((fdResponseRec.data.DTYPE==".pps")?".pdf":fdResponseRec.data.DTYPE),fdResponseRec.data.DPATH,fdResponseRec.data.RS.DOCDET).then(
						function(saveFileResp){
							if(!!saveFileResp){
								return ls.updateDocFetchUserMaster(fdResponseRec.data.AC,fdResponseRec.data.CID,fdResponseRec.data.NVER,fdResponseRec.data.DNAME,fdResponseRec.data.DPATH,fdResponseRec.data.DTYPE,fdResponseRec.data.PGLID).then(
									function(resp){
										debug("updateDocFetchUserMaster done: " + JSON.stringify(resp));
										if(!!resp)
											return ls.confirmDocDownload(fdResponseRec.data.AC,fdResponseRec.data.PWD,fdResponseRec.data.DID,fdResponseRec.data.NVER,fdResponseRec.data.DNAME,fdResponseRec.data.DVID);
										else
											return null;
									}
								);
							}
							else{
								return null;
							}
						}
					)
				);
			}
			else{
				promises.push("success");
			}
		});
		return $q.all(promises);
	};

	this.doDocumentDownload = function(username,password,documentList,dvid){
		var dfd = $q.defer();
		try{
			if((!!documentList) && (documentList.length>0)){
				var quesString = "?";
				var docIds = [];
				angular.forEach(documentList,function(documentListRec){
					docIds.push(documentListRec.DCID);
				});
				for(var i=1;i<documentList.length;i++)
					quesString += ",?";
				docIds.push(username);
				debug("quesString: " + quesString);
				debug("docIds: " + docIds);
				CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"select doc_id,doc_name,version_no,doc_path,doc_type from lp_doc_fetch_user_master where doc_id in (" + quesString + ") and agent_cd=?",docIds,
							function(tx,res){
								var docVerMap = [];
								if(!!res && res.rows.length>0){
									var nverMap = {};
									angular.forEach(documentList,function(documentListRec){
										nverMap[documentListRec.DCID] = documentListRec.VER;
									});
									for(var i=0;i<res.rows.length;i++){
										docVerMap.push({"dver":res.rows.item(i).VERSION_NO,"did": res.rows.item(i).DOC_ID,"dname":res.rows.item(i).DOC_NAME,"nver": nverMap[res.rows.item(i).DOC_ID], "dtype": res.rows.item(i).DOC_TYPE, "dpath": res.rows.item(0).DOC_PATH});
									}
								}
								else{
									debug("in else");
									angular.forEach(documentList,function(documentListRec){
										docVerMap.push({"cver":"0","did": documentListRec.DCID,"dname":documentListRec.DCNM,"nver": documentListRec.VER, "dtype": documentListRec.DCTP, "dpath": documentListRec.DCPATH, 'pglid': documentListRec.PGLID});
									});
								}
								ls.getDocData(username,password,docVerMap,dvid).then(
									function(getDocDataResp){
										debug("returned from getDocData()");
										ls.storeDocData(getDocDataResp).then(
											function(storeDocDataResp){
												if(!!storeDocDataResp){
													var errFlag = false;
													angular.forEach(storeDocDataResp,function(storeDocDataRespRec){
														if(!storeDocDataRespRec)
															errFlag = true;
													});
													if(errFlag){
														debug("resolving storeDocDataResp: null");
														dfd.resolve(null);
													}
													else{
														debug("resolving storeDocDataResp: " + storeDocDataResp);
														dfd.resolve(storeDocDataResp);
													}
												}
												else
													dfd.resolve(null);
											}
										);
									}
								);
							},
							function(tx,err){
								debug("Error in doDocumentDownload().transaction(): " + err.message);
								dfd.resolve(null);
							}
						);
					},
					function(err){
						debug("Error in doDocumentDownload(): " + err.message);
						dfd.resolve(null);
					},null
				);
			}
			else{
				debug("No documents available for download");
				dfd.resolve("success");
			}
		}catch(ex){
			debug("Exception in doDocumentDownload(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.storePolicies = function(username, fetchPolicyDetails){
		var promises = [];
		function insertPolicies(policyDetail){
		debug("Policy details :"+JSON.stringify(policyDetail));
			var dfd = $q.defer();
			var polType = "C";
			if(!!policyDetail.PLN && policyDetail.PLN.charAt(0) == "U"){
				polType = "U";
			}
			var insertRecord = { 'agent_cd': username, 'policy_no': policyDetail.PLN, 'allocation_date': policyDetail.PLAD, 'policy_type': polType, 'policy_final_status': policyDetail.PLFS, 'policy_status_cd': policyDetail.PLCD, 'uw_issue_date': null, 'policy_preuw_status': policyDetail.PPUWS,'assign_to':policyDetail.ASGNT};
			CommonService.insertOrReplaceRecord(db, 'lp_policy_assignment', insertRecord, true).then(
				function(res){
					var _polObj = {};
					_polObj.PLN = policyDetail.PLN;
					_polObj.LRD = CommonService.getCurrDate();
					dfd.resolve(_polObj);
				}
			);
			return dfd.promise;
		}
		if(!!fetchPolicyDetails){
			angular.forEach(fetchPolicyDetails,function(policyDetail){
				promises.push(insertPolicies(policyDetail));
			});
		}
		return $q.all(promises).then(
			function(storePoliciesPromise){
				debug("storePoliciesPromise: " + JSON.stringify(storePoliciesPromise));
				return storePoliciesPromise;
			}
		);
	};

	this.confirmPolicySync = function(username,password,dvid,storePoliciesRes){
		var dfd = $q.defer();
		function cpsSucc(resp){
			dfd.resolve("success");
		}
		function cpsErr(data, status, headers, config, statusText) {
			dfd.resolve(null);
		}

		if(!!storePoliciesRes && storePoliciesRes!={}){
			var cnfPolSync = {};
			cnfPolSync.REQ = {};
			cnfPolSync.REQ.AC = username;
			cnfPolSync.REQ.DVID = dvid;
			cnfPolSync.REQ.PWD = password;
			cnfPolSync.REQ.ACN = "CPS";
			cnfPolSync.REQ.PolicyResp = storePoliciesRes;
			CommonService.ajaxCall(POLICY_SERVLET_URL, AJAX_TYPE, TYPE_JSON, cnfPolSync, AJAX_ASYNC, AJAX_TIMEOUT, cpsSucc, cpsErr);
		}
		else{
			dfd.resolve("success");
		}
		return dfd.promise;
	};

	this.storePendingPolicies = function(username,pendingPoliciesList){
		var promises = [];
		function insertPendingPolicies(policyDetail){
			var dfd = $q.defer();
			var insertRecord = { 'policy_no': policyDetail.PLN, 'pending_code': policyDetail.PENC, 'pending_seq_no': policyDetail.PENSQ, 'pending_details': policyDetail.PENDT, 'pending_category': policyDetail.PENCT, 'pending_sub_category': policyDetail.PENSCT, 'pending_type': policyDetail.PENT, 'pending_status': policyDetail.PENST};
			CommonService.insertOrReplaceRecord(db, 'lp_policy_pending', insertRecord, true).then(
				function(res){
					var _polObj = {};
					_polObj.PLN = policyDetail.PLN;
					_polObj.LRD = CommonService.getCurrDate();
					dfd.resolve(_polObj);
				}
			);
			return dfd.promise;
		}
		if(!!pendingPoliciesList){
			angular.forEach(pendingPoliciesList,function(policyDetail){
				promises.push(insertPendingPolicies(policyDetail));
			});
		}
		return $q.all(promises);
	};

	this.getPolicyCountAndType = function(username){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select sum(case when policy_type = ? then 1 else 0 end) C_COUNT, sum(case when policy_type = ? then 1 else 0 end) U_COUNT from lp_policy_assignment where agent_cd=? and policy_final_status=?",['C','U',username,'F'],
					function(tx,res){
						try{
							if(res.rows.length > 0){
								res.rows.item(0).C_COUNT = res.rows.item(0).C_COUNT || 0;
								res.rows.item(0).U_COUNT = res.rows.item(0).U_COUNT || 0;
								dfd.resolve(res.rows.item(0));
							}
							else
								dfd.resolve({C_COUNT: 0, U_COUNT: 0});
						}catch(ex){
							debug("ex: " + ex.message);
							dfd.resolve({C_COUNT: 0, U_COUNT: 0});
						}
					},null
				);
			}
		);
		return dfd.promise;
	};

	this.getConfigMasterValue = function(configName){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_configuration_master', false, 'config_value', {'config_name': configName}, null).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug("getConfigMasterValue CONFIG_VALUE: " + res.rows.item(0).CONFIG_VALUE);
					dfd.resolve(res.rows.item(0).CONFIG_VALUE);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.validateOfflineLoginRequest = function(username,password,userinfo,configValueRes){
		debug("validateOfflineLoginRequest: " + JSON.stringify(userinfo));
		var returnObj = {};
		returnObj.SUCC = false;
		returnObj.INVALID_LOGIN = false;
		returnObj.MESSAGE = "";
		debug("userinfo.password: " + userinfo.PASSWORD);
		debug("password: " + password);
		if(password != userinfo.PASSWORD){
			debug("(password != userinfo.password): " + (password != userinfo.PASSWORD));
			returnObj.INVALID_LOGIN = true;
			returnObj.MESSAGE = "Invalid Password";
		}
		else if(userinfo.LOCK_STATUS=="L"){
			returnObj.MESSAGE = "The device is locked.\nPlease contact the help desk.";
		}
		else if(userinfo.ISACTIVE!="A"){
			returnObj.MESSAGE = "The account is inactive.\nPlease contact the help desk.";
		}
		else if(userinfo.IS_STOLEN=="Y"){
			returnObj.MESSAGE = "The device is registered as stolen.\nPlease contact the help desk.";
		}
		else if((userinfo.APP_BUSINESS_TYPE_ID=="LifePlaner" || userinfo.APP_BUSINESS_TYPE_ID=="GoodSolution") && (userinfo.EMPLOYEE_FLAG==="N" || userinfo.EMPLOYEE_FLAG==="L") && ((!!userinfo.PASSWORD_EXPIRY_DATE) && (CommonService.compareDates(CommonService.getCurrDate(),userinfo.PASSWORD_EXPIRY_DATE)==="more"))){
			returnObj.MESSAGE = "Your password has expired.\nPlease contact the help desk.";
		}
		else if((Math.floor( new Date((CommonService.getDateObj(CommonService.getCurrDate()).getSYYYY+"-"+CommonService.getDateObj(CommonService.getCurrDate()).getSMM+"-"+CommonService.getDateObj(CommonService.getCurrDate()).getSDD)).getTime() / (3600*24*1000)) -  Math.floor( new Date((CommonService.getDateObj(userinfo.LASTLOGIN_DATE).getSYYYY+"-"+CommonService.getDateObj(userinfo.LASTLOGIN_DATE).getSMM+"-"+CommonService.getDateObj(userinfo.LASTLOGIN_DATE).getSDD)).getTime() / (3600*24*1000)))>configValueRes){
			// this block is to check the Mandatory Online Login condition
			returnObj.MESSAGE = "Mandatory - Online login required at least once now.";
		}
		else if(userinfo.FORCE_CHANGE_PASSWORD_FLAG==="Y"){
			returnObj.MESSAGE = "Please connect to the internet for mandatory Online Login process";
		}
		else{
			returnObj.SUCC = true;
		}
		return returnObj;
	};

	this.doOfflineLogin = function(username,password,userinfo){
		ls.lgnSrvObj.username = username;
		ls.lgnSrvObj.password = password;
		debug("doOfflineLogin: " + JSON.stringify(userinfo));
		ls.getConfigMasterValue('Mandatory Online Login').then(
			function(configValueRes){
				var validationRes = ls.validateOfflineLoginRequest(username,password,userinfo,configValueRes);
				if(!!validationRes){
					if(validationRes.SUCC){
						// Login successful ... go to profile
						debug("Going to profile page");
						$state.go(DASHBOARD_HOME_STATE);
					}
					else{
						if(validationRes.INVALID_LOGIN){
							// increment invalid login count
							debug("Invalid login");
							ls.invalidLogin(username, userinfo.DEVICE_ID);
						}
						CommonService.hideLoading();
						debug("Offline login validation failed: " + validationRes.MESSAGE);
						navigator.notification.alert(validationRes.MESSAGE,null,"Login","OK");
					}
				}
				else{
					CommonService.hideLoading();
					debug("Offline login failed.");
					navigator.notification.alert("Offline login failed.\nPlease contact the help desk.",null,"Login","OK");
				}
			}
		);
	};

	this.resetInvalidLoginCount = function(username){
		var dfd = $q.defer();
		CommonService.updateRecords(db, 'lp_userinfo', {'invalid_login_count': '0', 'lock_status': 'U'}, {'agent_cd': username}).then(
			function(res){
				debug("invalid_login_count and lock_status reset");
				if(!!res)
					dfd.resolve("success");
				else
					dfd.resovlve(null);
			}
		);
		return dfd.promise;
	};

	this.getInvalidLoginCount = function(username){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_userinfo', false, 'invalid_login_count', {'agent_cd': username}, null).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug("invalid login count: " + res.rows.item(0).INVALID_LOGIN_COUNT);
					dfd.resolve({
						"INVALID_LOGIN_COUNT": res.rows.item(0).INVALID_LOGIN_COUNT,
						"LOCK_STATUS": res.rows.item(0).LOCK_STATUS
					});
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.insInvLgnAudRec = function(username,dvid,invLgnTimestamp){
		var dfd = $q.defer();
		var insertRecord = {'agent_cd': username, 'device_id': dvid, 'login_time': invLgnTimestamp, 'issynced': 'N'};
		CommonService.insertOrReplaceRecord(db, 'lp_invalid_login_audit', insertRecord, null).then(
			function(res){
				debug("inserted into invalidLoginAudit table successfully");
				dfd.resolve("success");
			}
		);
		return dfd.promise;
	};

	this.incrementInvalidLoginCount = function(invLgnCnt,lockStatus,username){
		var dfd = $q.defer();
		CommonService.updateRecords(db, 'lp_userinfo', {'invalid_login_count': invLgnCnt, 'lock_status': lockStatus}, {'agent_cd': username}).then(
			function(res){
				debug("updated lp_userinfo(inv lgn cnt) table successfully");
				dfd.resolve("success");
			}
		);
		return dfd.promise;
	};

	this.invalidLogin = function(username,dvid){
		var dfd = $q.defer();
		var resMsg = "unlocked";
		ls.getInvalidLoginCount(username).then(
			function(invLgnCntResp){
				if(!!invLgnCntResp){
					var INC_INV_CNT = parseInt(invLgnCntResp.INVALID_LOGIN_COUNT) + 1;
					if(invLgnCntResp.LOCK_STATUS == "U"){
						ls.insInvLgnAudRec(username,dvid,CommonService.getCurrDate()).then(
							function(insInvLgnAudRecResp){
								var lockStatus = invLgnCntResp.LOCK_STATUS;
								if(INC_INV_CNT>=5)
									lockStatus = "L";
								ls.incrementInvalidLoginCount(INC_INV_CNT,lockStatus,username).then(
									function(incInvLgnCntResp){
										if(INC_INV_CNT>=5){
											CommonService.hideLoading();
											navigator.notification.alert("Offline account locked.\n Please login online.",null,"Login","OK");
											resMsg = "locked";
										}
										dfd.resolve(resMsg);
									}
								);
							}
						);
					}
					else{
						var lockStatus = invLgnCntResp.LOCK_STATUS;
						if(INC_INV_CNT>=5)
							lockStatus = "L";
						ls.incrementInvalidLoginCount(INC_INV_CNT,lockStatus,username).then(
							function(incInvLgnCntResp){
								if(INC_INV_CNT>=5){
									CommonService.hideLoading();
									navigator.notification.alert("Offline account locked.\n Please login online.",null,"Login","OK");
									resMsg = "locked";
								}
								dfd.resolve(resMsg);
							}
						);
					}
				}
			}
		);
		return dfd.promise;
	};

	this.updateCatalogUserMaster_AC = function(username){
		var dfd = $q.defer();
		var updateClause = {"agent_cd": username};
		CommonService.updateRecords(db, "lp_catalog_user_master", updateClause).then(
			function(res){
				dfd.resolve(res);
				debug("updateCatalogUserMaster_AC: " + JSON.stringify(res));
			}
		);
		return dfd.promise;
	};

	this.preLoginSubmit = function(loginId){
		var dfd = $q.defer();
		if(!!loginId){
			loginId = loginId.toUpperCase();
			var req = {"REQ": {"LGD": loginId, "ACN": "BTR"}};
			debug("BTR REQ: " + JSON.stringify(req));
			CommonService.ajaxCall(BUSINESS_TYPE_SERVLET, AJAX_TYPE, TYPE_JSON, req, AJAX_ASYNC, AJAX_TIMEOUT,
				function(resp){
					if(!!resp && !!resp.RS && resp.RS.RESP == "S"){
						dfd.resolve(resp.RS.BTI);
					}
					else{
						dfd.resolve(null);
					}
				},
				function(data, status, headers, config, statusText){
					dfd.resolve(null);
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.doRestoration = function(username, password, dvid){
		var dfd = $q.defer();
		ls.showBackupMessage().then(
			function(res){
				if(!!res && res=="Y"){
					return ls.startRestoration(username, password, dvid);
				}
				else{
					dfd.resolve("S");
				}
			}
		)
		.then(
			function(restorationRes){
				debug("doRestoration restorationRes: " + restorationRes);
				dfd.resolve(restorationRes);
			}
		);
		return dfd.promise;
	};

	this.showBackupMessage = function(){
		var dfd = $q.defer();
		navigator.notification.confirm("Backup available, do you want to restore now? This may take a while, else you can opt No and restore from the side menu later.",
			function(buttonIndex){
				if(buttonIndex=="1"){
					dfd.resolve("Y");
				}
				else{
					dfd.resolve("N");
				}
			},
			'Confirm Policy Submission',
			['Yes', 'No']
		);
		return dfd.promise;
	};

	this.startRestoration = function(username, password, dvid){
		var dfd = $q.defer();
		ls.dataRestoration(username, password, dvid).then(
			function(dataRestoreRes){
				debug("dataRestoreRes: " + JSON.stringify(dataRestoreRes));
				if(!!dataRestoreRes && dataRestoreRes=="S"){
					return ls.fileRestoration(username, password, dvid);
				}
				else {
					return dataRestoreRes;
				}
			}
		).then(
			function(fileRestoreRes){
				debug("fileRestoreRes: " + fileRestoreRes);
				if(!!fileRestoreRes && fileRestoreRes=="S"){
					debug("ls.restoreTableData.tables: " + JSON.stringify(ls.restoreTableData.tables));
					return ls.saveTableData(ls.restoreTableData.tables);
				}
				else
					return fileRestoreRes;
			}
		)
		.then(
			function(saveTableDataRes){
				debug("saveTableDataRes: " + saveTableDataRes);
				if(!!saveTableDataRes && saveTableDataRes=="S"){
					return ls.updateDocsList().then(
						function(updateRes){
							if(!!updateRes){
								return ls.syncComplete(username, password, dvid, "Y", "F");
							}
							else{
								return "F";
							}
						}
					);
				}
				else {
					return saveTableDataRes;
				}
			}
		)
		.then(
			function(syncCompleteRes){
				debug("File Sync Complete Res: " + syncCompleteRes);
				dfd.resolve(syncCompleteRes || null);
			}
		);
		return dfd.promise;
	};

	this.updateDocsList = function(){
		var promises = [];
		debug("ls.newDocsListArr: " + JSON.stringify(ls.newDocsListArr));
		var keys = Object.keys(ls.newDocsListArr);
		angular.forEach(keys, function(key){
			promises.push(CommonService.updateRecords(db, "LP_APP_CONTACT_SCRN_A", {"DOCS_LIST": JSON.stringify(ls.newDocsListArr[key])}, {"APPLICATION_ID": key}));
		});
		return $q.all(promises).then(
			function(res){
				if(!!res && res instanceof Array){
					var flag = "S";
					for(var i=0; i<res.length; i++){
						if(!res)
							res = "F";
					}
					return flag;
				}
				else
					return (res || null);
			}
		);
	};

	this.dataRestoration = function(username, password, dvid){
		var dfd = $q.defer();
		ls.getLpDocProofMaster().then(
			function(res){
				ls.lpDocProofMaster = res;
				return res;
			}
		)
		.then(
			function(lpDocProofMaster){
				if(!!lpDocProofMaster){
					return ls.checkDataResFlag().then(
						function(dataResFlag){
							if(!!dataResFlag && dataResFlag=="Y")
								return "S";
							else
								return ls.getBackupTableList();
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(backupTableList){
				debug("backupTableList: " + JSON.stringify(backupTableList));
				var promises = [];
				if(!!backupTableList && backupTableList instanceof Array){
					angular.forEach(backupTableList, function(bckTableName){
						var tableName = bckTableName.TABLE_NAME;
						promises.push(ls.fetchTableData(username, password, dvid, tableName).then(
							function(fetchTableDataRes){
								if(!!fetchTableDataRes){
									return ls.saveTableDataTemp(tableName, fetchTableDataRes);
								}
								else{
									return "F";
								}
							}
						));
					});
				}
				else{
					promises.push(backupTableList || null);
				}
				return $q.all(promises);
			}
		)
		.then(
			function(saveTableRes){
				debug("saveTableRes: " + JSON.stringify(saveTableRes));
				if(!!saveTableRes && saveTableRes instanceof Array){
					var flag = false;
					angular.forEach(saveTableRes, function(rec){
						if(!rec || rec=="F")
							flag = true;
					});
					if(flag)
						return ls.rollbackRestoredTables();
					else
						return ls.syncComplete(username, password, dvid, "Y", "D");
				}
				else{
					return ls.rollbackRestoredTables();
				}
			}
		).
		then(
			function(result){
				if(!!result && result=="S"){
					dfd.resolve("S");
				}
				else{
					dfd.resolve("F");
				}
			}
		);
		return dfd.promise;
	};

	this.checkDataResFlag = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var query = "select * from lp_bkp_status where data_sync is null or data_sync=?";
				CommonService.executeSql(tx, query, ['N'],
					function(tx,res){
						if(!!res && res.rows.length>0)
							dfd.resolve("N");
						else
							dfd.resolve("Y");
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};


	this.getBackupTableList = function(){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_bkp_status", true, "table_name").then(
			function(res){
				res = CommonService.resultSetToObject(res);
				if(!!res && !(res instanceof Array))
					res = [res];
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.fetchTableData = function(username, password, dvid, tableName){
		var dfd = $q.defer();
		if(!!tableName){
			var req = {REQ: {AC: username, PWD: password, DVID: dvid, ACN: "RESD", TABLE_NAME: tableName.toUpperCase()}};
			debug("req: " + JSON.stringify(req));
			CommonService.ajaxCall(DATA_RESTORE_SERVLET, AJAX_TYPE, TYPE_JSON, req, AJAX_ASYNC, AJAX_TIMEOUT,
				function(resp){
					debug("resp: " + JSON.stringify(resp));
					if(!!resp && !!resp.RS && resp.RS.RESP=="S"){
						debug("fetchTableData resp.RS.RESP: " + resp.RS.RESP);
						dfd.resolve(resp.RS[tableName.toUpperCase()]);
					}
					else{
						debug("fetchTableData resp.RS.RESP: null");
						dfd.resolve(null);
					}
				},
				function(data, status, headers, config, statusText){
					dfd.resolve(null);
				}
			);
		}
		else {
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.saveTableDataTemp = function(tableName, tableData){
		try{
			ls.restoreTableData.tables = ls.restoreTableData.tables || {};
			ls.restoreTableData.tables[tableName.toUpperCase()] = tableData;
			return "S";
		}
		catch(ex){
			debug("Exception in saveTableDataTemp: " + ex.message);
			return "F";
		}
	};

	this.saveTableData = function(tables){
		var promises = [];
		if(!!tables){
			var tableNameList = Object.keys(tables);
			debug("tableNameList: " + JSON.stringify(tableNameList));
			angular.forEach(tableNameList, function(tableName){
				promises.push(ls.restoreTableData(tableName, tables[tableName]));
			});
		}
		else{
			promises.push(null);
		}
		return $q.all(promises).then(
			function(res){
				debug("restoreTableData res: " + JSON.stringify(res));
				if(!!res && res instanceof Array && res.length>0){
					var flag = false;
					angular.forEach(res, function(resRec){
						if(!resRec || resRec=="F")
							flag = true;
					});
					if(flag)
						return "F";
					else
						return "S";
				}
				else {
					return "F";
				}
			}
		);
	};

	this.getLpDocProofMaster = function(){
		var dfd = $q.defer();

		CommonService.selectRecords(db, "lp_doc_proof_master", true, null, {"isactive": "1"}).then(
			function(res){
				if(!!res && res.rows.length>0){
					res = CommonService.resultSetToObject(res);
					if(!!res && res instanceof Array){
						dfd.resolve(res);
					}
					else{
						dfd.resolve([res]);
					}
				}
				else{
					dfd.resolve(null);
				}
			}
		);

		return dfd.promise;
	};

	this.isReflexive = function(docId){
		debug("ls.lpDocProofMaster: " + ((!!ls.lpDocProofMaster)?ls.lpDocProofMaster.length:"null"));
		var reflexiveMaster = ($filter('filter')(ls.lpDocProofMaster, {"DOC_ID": docId}));
		if(!!reflexiveMaster && reflexiveMaster.length>0){
			return (reflexiveMaster[0].DIGITAL_HTML_FLAG=="Y");
		}
		else {
			return "N";
		}
	};

	this.insertRestoreDataInTable = function(tableName, insRec){
		var dfd = $q.defer();
		var i = 0;
		var _obj = {};
		var skip = false;
		ls.newDocsListArr = ls.newDocsListArr || {};
		if(insRec.hasOwnProperty("APPLICATION_ID") && (!(ls.newDocsListArr.hasOwnProperty(insRec.APPLICATION_ID))))
			ls.newDocsListArr[insRec.APPLICATION_ID] = {"Reflex": null, "NonReflex": null};

		var _CUST_TYPE = (insRec.CUST_TYPE == INSURED_CUST_TYPE)?"Insured":"Proposer";

		var renewalCond = ((tableName.toUpperCase()=="LP_APP_PAY_NEFT_SCRN_I") && (!!insRec && ((insRec.RYP_AUTOPAY_SI_FLAG=="Y") || (insRec.RYP_AUTOPAY_NACH_FLAG=="Y"))));
		var paymentCond = ((tableName.toUpperCase()=="LP_APP_PAY_DTLS_SCRN_G") && (!!insRec && ((insRec.PAY_METHOD_DD_FLAG=="Y") || (insRec.PAY_METHOD_SI_FLAG=="Y") || (insRec.PAY_METHOD_CHEQ_FLAG=="Y"))));
		var neftCond = ((tableName.toUpperCase()=="LP_APP_PAY_NEFT_SCRN_H") && (!!insRec && !!insRec.NEFT_DOCUMENT));
		var nomCond = ((tableName.toUpperCase()=="LP_APP_NOM_CONTACT_SCRN_F") && (!!insRec && !!insRec.DOC_ID));

		var rfxString = null;
		if((tableName.toUpperCase()=="LP_APPLICATION_MAIN")){
			if(insRec.APP_SUBMITTED_FLAG!='Y'){
				insRec.IS_SCREEN3A_COMPLETED = null;
				for(i=1; i<=12; i++)
					insRec["IS_SCREEN" + i + "_COMPLETED"] = null;
				insRec.IS_PHOTO_COMPLETED = null;
				insRec.IS_MANDATORY_DOCS_COMPLETED = null;
				insRec.ISINSURED_SIGNED = null;
				insRec.ISPROPOSER_SIGNED = null;
				insRec.ISAGENT_SIGNED = null;
				insRec.APPLICATION_DATE = null;
				insRec.COMPLETION_TIMESTAMP = null;
				insRec.ISSYNCED = null;
				insRec.IS_OUTPUT_GENERATED = null;
				insRec.IS_PDF_SAVED = null;
			}
		}
		else if((tableName.toUpperCase()=="LP_DOCUMENT_UPLOAD")){
			if(insRec.DOC_CAT!="SIS" && insRec.DOC_CAT!="FHR"){
				var appMainRec = ($filter('filter')(ls.restoreTableData.tables.LP_APPLICATION_MAIN, {"APPLICATION_ID": insRec.DOC_CAT_ID}))[0];
				if((!!appMainRec) && (appMainRec.APP_SUBMITTED_FLAG!='Y')){
					insRec.IS_DATA_SYNCED = null;
					insRec.IS_FILE_SYNCED = null;
				}
			}
		}
		else if((tableName.toUpperCase()=="LP_APP_CONTACT_SCRN_A") && insRec.hasOwnProperty("DOCS_LIST") && !!insRec.DOCS_LIST){
			if(!!insRec && (insRec.RELATIONSHIP_ID=='20' && insRec.CUST_TYPE=='C02')){
				insRec = null;
				skip = true;
			}
			else if(!!insRec){
				skip = false;
				var orgDocsListArr = insRec.DOCS_LIST.split(",");
				var catArr = ["ageproof", "nri", "incproof", "addrproof", "occuclass", "occuQuest", "nature", "idproof", "form60"/* or CustDecForm*/, "pan", "uan"];
				for(i=0; i<orgDocsListArr.length; i++){
					var category = catArr[i];
					rfxString = (ls.isReflexive(orgDocsListArr[i]))?"Reflex":"NonReflex";
					ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] || {"Insured": null, "Proposer": null};
					_obj = {};
					_obj[category] = null;
					ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] || _obj;
					ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][category] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][category] || {};
					ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][category].DOC_ID = (orgDocsListArr[i]=="null")?null:orgDocsListArr[i];
				}
				if(insRec.RELATIONSHIP_ID=='20'){
					ls.newDocsListArr[insRec.APPLICATION_ID].Reflex.Proposer = {};
					ls.newDocsListArr[insRec.APPLICATION_ID].NonReflex.Proposer = {};
				}
			}
			else{
				skip = false;
			}
		}
		else if(((tableName.toUpperCase()=="LP_APP_LS_ANS_SCRN_B") || (tableName.toUpperCase()=="LP_APP_HD_ANS_SCRN_C")) && !!insRec.DOC_ID && (!!insRec.ANSWAR_FLAG && insRec.ANSWAR_FLAG=="Y")){
			rfxString = (ls.isReflexive(insRec.DOC_ID))?"Reflex":"NonReflex";
			var docQIdMap = {}; //{"QUESTION_ID": value}
			if(tableName.toUpperCase()=="LP_APP_LS_ANS_SCRN_B"){
				docQIdMap = {"1": "LSQuest1", "3": ((insRec.CUST_TYPE.trim() == INSURED_CUST_TYPE)?"occuQuest":"LSQuest2Sub1"), "4": "LSQuest2Sub2", "7": "LSQuest4"};
			}
			else if(tableName.toUpperCase()=="LP_APP_HD_ANS_SCRN_C"){
				docQIdMap = {"20": "HDQuest4", "21": "HDQuest5", "23": "HDQuest7", "24": "HDQuest8", "29": "HDQuest9C", "42": "HDQuest9K", "47": "HDQuest12A", "26": "HDQuest9ASub1", "28": "HDQuest9BSub1", "31": "HDQuest9ESub1", "33": "HDQuest9FSub1", "36": "HDQuest9HSub1", "38": "HDQuest9ISub1", "40": "HDQuest9JSub1"};
			}

			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] || {"Insured": null, "Proposer": null};

			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] || {};
			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docQIdMap[insRec.QUESTION_ID]] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docQIdMap[insRec.QUESTION_ID]] || {"DOC_ID": null};
			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docQIdMap[insRec.QUESTION_ID]].DOC_ID = insRec.DOC_ID;
		}
		else if(renewalCond || paymentCond || neftCond || nomCond){
			var type = null;
			var mpDocCode = "SI";
			var docCat = null;
			if(tableName.toUpperCase()=="LP_APP_PAY_NEFT_SCRN_I"){
				docCat = "renewalPay";
				if(insRec.RYP_AUTOPAY_SI_FLAG=="Y")
					type = "RS";
				else if(insRec.RYP_AUTOPAY_NACH_FLAG=="Y")
					type = "RN";
				_CUST_TYPE = "Insured"; //"IN"
			}
			else if(tableName.toUpperCase()=="LP_APP_PAY_DTLS_SCRN_G"){
				docCat = "paymentOption";
				if(insRec.PAY_METHOD_DD_FLAG=="Y")
					type = "DC";
				else if(insRec.PAY_METHOD_SI_FLAG=="Y")
					type = "IS";
				else if(insRec.PAY_METHOD_CHEQ_FLAG=="Y")
					type = "QC";
				_CUST_TYPE = "Insured"; //"IN"
			}
			var docId = null;
			if(tableName.toUpperCase()=="LP_APP_PAY_NEFT_SCRN_H"){
				docCat = "bankDetail";
				docId = (($filter('filter')(ls.lpDocProofMaster, {"MPPROOF_DESC": insRec.NEFT_DOCUMENT})[0]).DOC_ID);
				insRec.NEFT_DOCUMENT = docId;
				_CUST_TYPE = "Insured"; //"IN"
			}
			else if(tableName.toUpperCase()=="LP_APP_NOM_CONTACT_SCRN_F"){
				docCat = "NomRelation";
				docId = insRec.DOC_ID;
				_CUST_TYPE = "Insured"; //"IN"
			}
			else{
				_obj = {"MPPROOF_CODE": type, "MPDOC_CODE": mpDocCode};
				debug("_obj: " + JSON.stringify(_obj));
				docId = (($filter('filter')(ls.lpDocProofMaster, {"MPPROOF_CODE": type, "MPDOC_CODE": mpDocCode})[0]).DOC_ID);
			}
			rfxString = (ls.isReflexive(docId))?"Reflex":"NonReflex";

			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString] || {"Insured": null, "Proposer": null};

			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE] || {"renewalPay": null};
			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docCat] = ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docCat] || {"DOC_ID": null};
			ls.newDocsListArr[insRec.APPLICATION_ID][rfxString][_CUST_TYPE][docCat].DOC_ID = docId;
		}

		if(!!insRec){
			CommonService.insertOrReplaceRecord(db, tableName, insRec).then(
				function(res){
					debug("insert record res: " + JSON.stringify(res));
					if(!!res)
						dfd.resolve("S");
					else{
						debug("tableName: FAIL: " + JSON.stringify(insRec));
						dfd.resolve("F");
					}
				}
			);
		}
		else{
			if(!!skip){
				dfd.resolve("S");
			}
			else {
				dfd.resolve("F");
			}
		}
		return dfd.promise;
	};

	this.restoreTableData = function(tableName, tableData){
		var promises = [];
		if(!!tableData && tableData instanceof Array){
			angular.forEach(tableData, function(insRec){
				promises.push(ls.insertRestoreDataInTable(tableName, insRec));
			});
		}
		else{
			promises.push("S");
		}
		return $q.all(promises);
	};

	this.syncComplete = function(username, password, dvid, sFlag, type){
		var dfd = $q.defer();
		var req = {REQ: {AC: username, PWD: password, DVID: dvid}};
		if(type==="D"){
			req.REQ.ACN = "RES_DS_COMP";
			req.REQ.DS_FLAG = sFlag||"N";
		}
		else{
			req.REQ.ACN = "RES_FS_COMP";
			req.REQ.FS_FLAG = sFlag||"N";
		}
		debug("req: " + JSON.stringify(req));
		CommonService.ajaxCall(SYNC_COMPLETE_SERVLET, AJAX_TYPE, TYPE_JSON, req, AJAX_ASYNC, AJAX_TIMEOUT,
			function(resp){
				debug("syncComplete resp: " + JSON.stringify(resp));
				if(!!resp && !!resp.RS)
					dfd.resolve(resp.RS.RESP);
				else
					dfd.resolve("F");
			},
			function(data, status, headers, config, statusText){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.rollbackRestoredTables = function(){
		try{
			delete ls.restoreTableData.tables;
			return "S";
		}
		catch(ex){
			debug("Exception in rollbackRestoredTables: " + ex.message);
			return "F";
		}
	};

	this.fileRestoration = function(username, password, dvid){
		var dfd = $q.defer();
		var fileDataArray = ls.fetchFilesToBeRestored(username, ls.restoreTableData.tables);
		debug("fileDataArray: " + JSON.stringify(fileDataArray));
		if(!!fileDataArray && fileDataArray instanceof Array){
			if(fileDataArray.length>0){
				ls.saveAllFiles(username, password, dvid, fileDataArray).then(
					function(res){
						dfd.resolve(res);
					}
				);
			}
			else{
				dfd.resolve("S");
			}
		}
		return dfd.promise;
	};

	this.fetchFilesToBeRestored = function(username, tables){
		if(!!tables){
			var lpDocumentUploadTable = tables.LP_DOCUMENT_UPLOAD;
			var fileDataArray = $filter('filter')(lpDocumentUploadTable, function(value){
				return ((value.AGENT_CD==username) && (!value.IS_FILE_SYNCED || value.IS_FILE_SYNCED==="N"));
			});
			return fileDataArray;
		}
		else{
			return null;
		}
	};

	this.saveAllFiles = function(username, password, dvid, fileDataArray){
		var promises = [];
		angular.forEach(fileDataArray, function(fileData){
			promises.push(ls.fetchFileAndStoreIt(username, password, dvid, fileData));
		});
		return $q.all(promises).then(
			function(res){
				debug("promises: " + JSON.stringify(res));
				if(!!res && res instanceof Array && res.length>0){
					var flag = false;
					angular.forEach(res, function(resRec){
						if(!resRec || resRec=="F")
							flag = true;
					});
					if(flag)
						return "F";
					else
						return "S";
				}
				else {
					return "F";
				}
			}
		);
	};

	this.fetchFileAndStoreIt = function(username, password, dvid, fileData){
		var dfd = $q.defer();
		var req = {REQ: {AC: username, PWD: password, DVID: dvid, ACN: "RESF", DOCID: fileData.DOC_ID, DCID: fileData.DOC_CAT_ID, DCAT: fileData.DOC_CAT, DPNO: fileData.DOC_PAGE_NO, DCNM: fileData.DOC_NAME}};
		CommonService.ajaxCall(FILE_RESTORE_SERVLET, AJAX_TYPE, TYPE_JSON, req, AJAX_ASYNC, AJAX_TIMEOUT,
			function(resp){
				if(!!resp && !!resp.RS && resp.RS.RESP=="S"){
					var ext = fileData.DOC_NAME.split(".")[1];
					var filePath = "";
					if(fileData.DOC_CAT=="SIS" && ext=="pdf"){
						filePath = "/FromClient/SIS/PDF/";
					}
					else if(fileData.DOC_CAT=="SIS" && ext=="html"){
						filePath = "/FromClient/SIS/HTML/";
					}
					else if(fileData.DOC_CAT=="APP" && ext=="pdf"){
						filePath = "/FromClient/APP/PDF/";
					}
					else if(fileData.DOC_CAT=="APP" && ext=="html"){
						filePath = "/FromClient/APP/HTML/";
					}
					else if(fileData.DOC_CAT=="APP" && ext=="jpg"){
						filePath = "/FromClient/APP/SIGNATURE/";
					}
					else if(fileData.DOC_CAT=="FORM" && ext=="jpg"){
						filePath = "/FromClient/APP/IMAGES/";
					}
					else if(fileData.DOC_CAT=="PHOTOGRAPH" && ext=="jpg"){
						filePath = "/FromClient/APP/IMAGES/PHOTOS/";
					}
					if(!!resp.RS.FILE_DATA){
						if(fileData.DOC_CAT=="PHOTOGRAPH" || fileData.DOC_CAT=="FORM"){
							CommonService.saveNonEncryptedFile(fileData.DOC_NAME,filePath,resp.RS.FILE_DATA,"Y").then(
								function(saveNonEncryptedFileRes){
									if(!!saveNonEncryptedFileRes && saveNonEncryptedFileRes=="success"){
										dfd.resolve("S");
									}
									else{
										dfd.resolve("F");
									}
								}
							);
						}
						else{
							CommonService.saveFile(fileData.DOC_NAME.split(".")[0], "."+ext,filePath,resp.RS.FILE_DATA,"Y").then(
								function(saveEncryptedFileRes){
									if(!!saveEncryptedFileRes && saveEncryptedFileRes=="success"){
										dfd.resolve("S");
									}
									else{
										dfd.resolve("F");
									}
								}
							);
						}
					}
					else{
						debug("blob is null");
						dfd.resolve("S");
					}
				}
				else
					dfd.resolve("F");
			},
			function(data, status, headers, config, statusText){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
