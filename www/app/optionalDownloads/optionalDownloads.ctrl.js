optionalDownloadsModule.controller('OptionalDownloadsCtrl',['$state','OptionalDownloadsList','OptionalDownloadsService','LoginService','CommonService',function($state, OptionalDownloadsList, OptionalDownloadsService, LoginService,CommonService){
	"use strict";
	CommonService.hideLoading();
	debug("optionalDownloadsModule");
	var odc = this;

	if(OptionalDownloadsList!=undefined && OptionalDownloadsList!=""){
		debug("productDocsList: " + JSON.stringify(OptionalDownloadsList.ProductDocsList));
		debug("resourceDocsList: " + JSON.stringify(OptionalDownloadsList.ResourceDocsList));
		this.productDocsList = OptionalDownloadsList.ProductDocsList;
		this.resourceDocsList = OptionalDownloadsList.ResourceDocsList;
	}

	this.onChange = function(type, doc){
		debug("in on change download docs");
		OptionalDownloadsService.onChange(type, doc);
	};

	this.gotoProfile = function(){
		$state.go(PROFILE_STATE);
	};

	this.optDwnldBtnClick = function(){
		OptionalDownloadsService.optDwnldBtnClick(this.productDocsList, this.resourceDocsList);
	};

}]);
optionalDownloadsModule.service('OptionalDownloadsService',['$q','$state', 'LoginService', 'CommonService', function($q,$state,LoginService,CommonService){
	"use strict";
	var ods = this;
	this.docsDownloadList = [];

	this.refreshDocsDownloadList = function(downloadResp, productDocsList, resourceDocsList){
		angular.forEach(downloadResp,function(downloadRespRec){
			debug(JSON.stringify(downloadRespRec)+"in refreshDocsDownloadList::"+JSON.stringify(downloadRespRec));
			if(!!downloadRespRec && !!downloadRespRec.RS && downloadRespRec.RS.RESP=="S"){
				ods.removeDocFromList(productDocsList,"DCID",downloadRespRec.RS.DOCID);
				ods.removeDocFromList(resourceDocsList,"DCID",downloadRespRec.RS.DOCID);
			}
			else{
				navigator.notification.alert("Could not download one or more document(s). Please try again after some time.",null,"Optional Downloads","OK");
			}
		});
		this.docsDownloadList = [];
		debug("docsDownloadList::"+this.docsDownloadList);
	};


	this.optDwnldBtnClick = function(productDocsList, resourceDocsList){
		debug("ProductList::"+JSON.stringify(productDocsList)+"ResourceList::"+JSON.stringify(resourceDocsList));
		if(!!this.docsDownloadList && this.docsDownloadList.length>0){
			CommonService.showLoading();
			this.downloadDocuments(this.docsDownloadList).then(
				function(downloadResp){
					debug("downloadResp: " + JSON.stringify(downloadResp));
					var flag = true;
					angular.forEach(downloadResp,function(downloadRespRec){
						if(!downloadRespRec)
							flag = false;
					});

					if(!!flag){
						ods.refreshDocsDownloadList(downloadResp, productDocsList, resourceDocsList);
						CommonService.hideLoading();
						navigator.notification.alert("Document(s) downloaded successfully.",null,"Optional Downloads","OK");
					}
					else{
						CommonService.hideLoading();
						navigator.notification.alert("Could not download the document(s).\nPlease try again later.",null,"Optional Downloads","OK");
					}
				}
			);
		}
		else{
			CommonService.hideLoading();
			navigator.notification.alert("Please select at least one document to download.",null,"Optional Downloads","OK");
		}
	};

	this.downloadDocuments = function(){
		debug("in downloadDocuments : " + this.docsDownloadList.length);
		var dfd = $q.defer();
		try{
			LoginService.doDocumentDownload(LoginService.lgnSrvObj.userinfo.AGENT_CD,LoginService.lgnSrvObj.password,this.docsDownloadList,LoginService.lgnSrvObj.dvid).then(
				function(docDownloadResp){
					dfd.resolve(docDownloadResp);
					debug("docDownloadResp::"+docDownloadResp);
				}
			);
		}catch(ex){
			debug("Exception in downloadDocuments(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	/*this.downloadSelectedDocs = function(){
		var dfd = $q.defer();
		debug("docsDownloadList"+this.docsDownloadList);
		this.downloadDocuments(this.docsDownloadList).then(
			function(res){
				dfd.resolve(res);
			}
		);
		debug("document list::"+this.docsDownloadList);
		return dfd.promise;
	};*/

	this.removeDocFromList = function(array, property, value) {
    	array.forEach(function(result, index) {
        	if(result[property] === value) {
          		//Remove from array
          		array.splice(index, 1);
        	}
      	});
    };

	this.onChange = function(type, doc){
		var cnt = 0;
		debug("Type: " + type + "\t checked: " + doc.checked);
		if(doc.checked){
			this.docsDownloadList.push(doc);
			cnt++;
			debug("Count is::"+cnt);
		}
		else
			this.removeDocFromList(this.docsDownloadList,"DCID",doc.DCID);
		debug("this.docsDownloadList: " + JSON.stringify(this.docsDownloadList));
	};

	this.fetchOptDocs = function(username, password, dvid, bti){
		var dfd = $q.defer();
		function golSucc(response){
			debug("Resolving fetchOptDocs()"+JSON.stringify(response));
			dfd.resolve(response);
		}
		function golErr(data, status, headers, config, statusText){
			dfd.resolve(null);
		}
		try{
			var optDocsReq = {};
			optDocsReq.REQ = {};
			optDocsReq.REQ.AC  = username;
			optDocsReq.REQ.DVID = dvid;
			optDocsReq.REQ.PWD = password;
			optDocsReq.REQ.BTI = bti;
			optDocsReq.REQ.ACN = "GOL";
			optDocsReq.REQ.AID = APP_NAME;
			debug("JSONObject: https://lpu.tataaia.com/LifePlaner/DocumentServlet?Key=" + JSON.stringify(optDocsReq));
			CommonService.ajaxCall(DOCUMENT_SERVLET_URL, AJAX_TYPE, TYPE_JSON, optDocsReq, AJAX_ASYNC, AJAX_TIMEOUT, golSucc, golErr);
		}catch(ex){
			debug("Exception in fetchOptDocs(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.parseOptDocs = function(fetchOptDocsResp){
		var dfd = $q.defer();
		var optDocsList = {};
		optDocsList.ProductDocsList = [];
		optDocsList.ResourceDocsList = [];
		if(!!fetchOptDocsResp && fetchOptDocsResp.RS.RESP=="S"){
		debug("fetchOptDocsResp::"+JSON.stringify(fetchOptDocsResp));
			angular.forEach(fetchOptDocsResp.RS.optionalDocumentList,function(optDocRec){
				var optDoc = {};
				optDoc.DCID = optDocRec.DCID;
				optDoc.DCNM = optDocRec.DCNM;
				optDoc.DCTP = optDocRec.DCTP;
				optDoc.DCPATH = optDocRec.DCPATH;
				optDoc.VER = optDocRec.VER;
				optDoc.PGLID = optDocRec.PGLID;
				optDoc.checked = false;
				debug("optDoc::"+JSON.stringify(optDoc));
				if(optDocRec.DCPATH.toLowerCase() == REL_PATH_PRODUCTS.toLowerCase())
					optDocsList.ProductDocsList.push(optDoc);
				else
					optDocsList.ResourceDocsList.push(optDoc);
			});
			dfd.resolve(optDocsList);
			debug("optDocsList::"+JSON.stringify(optDocsList));
		}
		else
			dfd.resolve(null);
		return dfd.promise;
	};

	this.getOptionalDocuments = function(username, password, dvid, bti){
		var dfd = $q.defer();
		this.fetchOptDocs(username, password, dvid, bti).then(
			function(fetchOptDocsResp){
				ods.parseOptDocs(fetchOptDocsResp).then(
					function(optDocsList){
						dfd.resolve(optDocsList);
					}
				);
			}
		);
		return dfd.promise;
	};

}]);
