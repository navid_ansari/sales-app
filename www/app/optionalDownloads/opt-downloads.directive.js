optionalDownloadsModule.directive('optDownloadBtn',function(){
	"use strict";
	return {
		restrict: 'E',
		templateUrl: 'optionalDownloads/opt-download-btn.html'
	};
});


optionalDownloadsModule.directive('productDocsList',function(){
	"use strict";
	return {
		restrict: 'E',
		templateUrl: 'optionalDownloads/products.html'
	};
});

optionalDownloadsModule.directive('resourcesDocsList',function(){
	"use strict";
	return {
		restrict: 'E',
		templateUrl: 'optionalDownloads/resources.html'
	};
});
