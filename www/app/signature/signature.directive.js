signatureModule.directive('signDirective',['SignatureService', function(SignatureService){
	"use strict";
	return {
        restrict: "A",
        link: function (scope, element, attr, ctrl) {
        	debug("in signDirective");    		
    		SignatureService.initSignaturePad(element[0]);
   	    }
   	};
}]);