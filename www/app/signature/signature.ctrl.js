signatureModule.controller('SignatureCtrl',['$uibModalInstance', 'SignatureService', 'Action', function($uibModalInstance, SignatureService, Action){
	var sc = this;
	debug("in SignatureCtrl " + Action);
	SignatureService.action = Action;
	this.clearCanvas = function(){
		SignatureService.clearSignaturePad();
		SignatureService.initSignaturePad();
	};

	this.hideSign = function() {
		$uibModalInstance.close();
	};

	this.saveSign = function(){
		SignatureService.saveSign($uibModalInstance);
	};

}]);


signatureModule.service('SignatureService', ['SisOutputService','SavePageOutputData','AppOutputService','AppTermOutputService','FinalSubmissionService','$state','$stateParams', 'SisVernacularService', 'VernacularService', 'SisFormService', 'FhrRecommendationsService', function(SisOutputService, SavePageOutputData, AppOutputService, AppTermOutputService, FinalSubmissionService, $state, $stateParams, SisVernacularService, VernacularService, SisFormService, FhrRecommendationsService){
	var sc = this;
	this.signaturePad = null;
	this.canvas = null;
	this.action = null;
	this.initSignaturePad = function(canvas){
		debug("singature pad initializing");
		if(!!canvas)
			this.canvas = canvas;
		this.signaturePad = new SignaturePad(this.canvas,{
	    	minWidth: 0.8,
	    	maxWidth: 2.8,
	    	backgroundColor: "rgba(255, 255, 255, 1)",
			penColor: "rgb(0, 0, 0)"
		});
		//debug("singature pad initialized " + JSON.stringify(this.signaturePad));
	};
	this.clearSignaturePad = function(){
		this.signaturePad.clear();
	};

	this.saveSign = function(uibModalInstance){
		navigator.notification.confirm("Is this your final signature?",
			function(buttonIndex){
				if(buttonIndex=="1"){
					var sign = sc.signaturePad.toDataURL("image/jpeg");
					sc.saveSignDelegate(sign);
					debug("sign: " + sign.length);
					sc.clearSignaturePad();
					uibModalInstance.close();
					if((sc.action != "sisPropSign") && (sc.action != "sisDeclarantSign") && (sc.action != "appSelfInsSign") && (sc.action != "appProSign") && (sc.action != "fhrRecommDeviaSign"))
						$state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
				}
			},
			'Confirm Signature',
			'Yes, No'
		);
	};

	this.saveSignDelegate = function(sign){
		debug("saveSignDelegate: " + this.action);
		debug("sign: " + sign.length);
		if(this.action == "sisPropSign"){
			if((SisFormService.sisData.sisMainData.PGL_ID!=IRS_PGL_ID) && (SisFormService.sisData.sisMainData.PGL_ID!=ITRP_PGL_ID)) {
				navigator.notification.confirm("Is the signature of proposer in vernacular?",
					function(buttonIndex){
						if(buttonIndex=="1"){
							SisOutputService.saveProposerSignature(sign, true);
						}
						else{
							SisOutputService.saveProposerSignature(sign, false);
						}
					},
					'Vernacular','Yes, No'
				);
			}
			else{
				SisOutputService.saveProposerSignature(sign, false);
			}
		}
		else if(this.action == "sisDeclarantSign")
			SisVernacularService.saveDeclarantSignature(sign);
		else if(this.action == "ffCustSign")
			SavePageOutputData.saveCustSign(sign);
		else if(this.action == "appInsSign"|| this.action == "appSelfInsSign") // Drastty
			AppOutputService.saveLifeAssSign(sign);
		else if(this.action == "appProSign")
			AppOutputService.saveProposerSign(sign);
		else if(this.action == "appTermInsSign") // Term Sign
			AppTermOutputService.saveLifeAssSign(sign);
		else if(this.action == "appTermProSign") // Term Sign
			AppTermOutputService.saveProposerSign(sign);
		else if(this.action == "agentSign")
			FinalSubmissionService.saveAgentSignature(sign);
		else if(this.action == "appVernacSign")
			VernacularService.saveWitnessSignature(sign);
		else if(this.action == "fhrRecommDeviaSign")
			FhrRecommendationsService.saveRecommDeviaSignature(sign);			

	};

}]);
