initiatePSCModule.service('InitiatePSCService', ['$q','$state','CommonService','LoginService','SisFormService','ComboPolicyNoService', function($q, $state, CommonService, LoginService, SisFormService, ComboPolicyNoService){

this.PSCModuleData = {};
var psc = this;

  this.loadPSCModuleData = function (params) {
    var dfd = $q.defer();
    CommonService.showLoading("Loading ...");
    debug("params :: "+JSON.stringify(params));
    psc.PSCModuleData.OPP_ID = params.OPP_ID;
    psc.PSCModuleData.LEAD_ID = params.LEAD_ID;
    psc.PSCModuleData.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
    psc.PSCModuleData.SIS_ID = params.SIS_ID;
    psc.PSCModuleData.APPLICATION_ID = params.APPLICATION_ID;
    psc.PSCModuleData.FHR_ID = params.FHR_ID;
    psc.loadTableData(null, params.LEAD_ID, psc.PSCModuleData.AGENT_CD, "LP_MYLEAD").then(function (leadData) {
          psc.PSCModuleData.leadData = leadData;
      psc.loadTableData(params.OPP_ID, params.LEAD_ID, psc.PSCModuleData.AGENT_CD, "LP_MYOPPORTUNITY").then(function (oppData) {
          psc.PSCModuleData.oppData = oppData;
            if(!!psc.PSCModuleData && !!psc.PSCModuleData.oppData)
              {
                psc.PSCModuleData.POLICY_NO = psc.PSCModuleData.oppData.POLICY_NO;
                psc.PSCModuleData.COMBO_ID = psc.PSCModuleData.oppData.COMBO_ID;
              }
                SisFormService.loadSavedSISData(psc.PSCModuleData.AGENT_CD, params.SIS_ID).then(function (sisData) {
                  psc.PSCModuleData.sisData = sisData;
                  psc.PSCModuleData.PGL_ID = sisData.sisMainData.PGL_ID;
                  debug("PSCModuleData :: "+JSON.stringify(psc.PSCModuleData));
                  CommonService.hideLoading();
                  $state.go("pscPage1", params);
              });
          });
    });
      return dfd.promise;
  }

  this.loadTableData = function(oppId, leadId, agentCd, tableName){
		var dfd = $q.defer();
		var oppData = {};
		var params =  {"agent_cd": agentCd, "lead_id": leadId};
		if(!!oppId)
			params = {"agent_cd": agentCd, "lead_id": leadId, "opportunity_id": oppId};
    if(tableName == 'LP_MYLEAD')
      params =  {"agent_cd": agentCd, "ipad_lead_id": leadId};

		CommonService.selectRecords(db, tableName, true, null,params).then(
			function(res){
				debug("Opp :: res.rows.length: " + res.rows.length);
				if(!!res && res.rows.length>0){
					oppData = CommonService.resultSetToObject(res);
					dfd.resolve(oppData);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

  this.isPolicyNoAssigned = function(){

      whereClauseObj = {};
      whereClauseObj.OPP_ID = psc.PSCModuleData.OPP_ID;
      whereClauseObj.LEAD_ID = psc.PSCModuleData.LEAD_ID;
      whereClauseObj.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
      CommonService.showLoading("Loading ...");
      debug("isPolicyNoAssigned 1"+JSON.stringify(psc.PSCModuleData));
      var dfd = $q.defer();
      var policyBean = {};
      /*LoadApplicationScreenData.loadApplicationFlags(APP_ID, AGENT_CD).then(function(AppMain){
          debug("isPolicyNoAssigned 2"+JSON.stringify(AppMain))*/
          psc.loadTableData(psc.PSCModuleData.OPP_ID, psc.PSCModuleData.LEAD_ID, psc.PSCModuleData.AGENT_CD, "LP_MYOPPORTUNITY").then(function (oppData){
              psc.PSCModuleData.oppData = oppData;
          if(psc.PSCModuleData.oppData.POLICY_NO == undefined || psc.PSCModuleData.oppData.POLICY_NO =="")
          {
             psc.getProdType().then(function(pol){
                  policyBean.PROD_TYPE = pol.PROD_TYPE;
                  debug("pol.PROD_TYPE :"+pol.PROD_TYPE);
                  psc.checkPolicyNo(pol.PROD_TYPE).then(function(polBean){
                  if(polBean!=undefined && polBean.POLICY_NO!=undefined && polBean.PROD_TYPE!=undefined)
                  {
                      policyBean.POLICY_NO = polBean.POLICY_NO;
                      policyBean.PROD_TYPE = polBean.PROD_TYPE;
                      psc.PSCModuleData.POLICY_NO = policyBean.POLICY_NO;
                      psc.PSCModuleData.APPLICATION_ID = CommonService.getRandomNumber();
                       psc.AssignPolicyNo(policyBean).then(function(){
                          dfd.resolve(policyBean);
                       });
                  }
                  });

             });
          }
          else
          {
              policyBean.POLICY_NO = psc.PSCModuleData.oppData.POLICY_NO;
              psc.PSCModuleData.POLICY_NO = psc.PSCModuleData.oppData.POLICY_NO;
              psc.PSCModuleData.APPLICATION_ID = psc.PSCModuleData.oppData.APPLICATION_ID;
              var plNo = psc.PSCModuleData.oppData.POLICY_NO;
              policyBean.PROD_TYPE = plNo.substring(0,1);
              /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"IS_SCREEN8_COMPLETED" :"Y"}, whereClauseObj).then(
                                  function(res){
                                      debug("IS_SCREEN8_COMPLETED update success !!"+JSON.stringify(whereClauseObj));
                                  });*/
                   dfd.resolve(policyBean);
          }
        });

      //});

      return dfd.promise
  }

  this.getProdType=function(){
  var dfd = $q.defer();
  var policyBean = {}
  var whereClauseObj={};
      whereClauseObj.PGL_ID = psc.PSCModuleData.PGL_ID;
      CommonService.selectRecords(sisDB,"LP_PGL_PLAN_GROUP_LK",false,'PGL_PRODUCT_TYPE',whereClauseObj).then(
          function(res){
              policyBean.PROD_TYPE = res.rows.item(0).PGL_PRODUCT_TYPE;
              dfd.resolve(policyBean);
          }
      );
      return dfd.promise;
  }

  this.checkPolicyNo=function(prdType){
      var dfd = $q.defer();
      var policyBean = {};
      var whereClauseObj={};
              whereClauseObj.POLICY_TYPE = prdType;
              whereClauseObj.POLICY_FINAL_STATUS = 'F';
              debug(JSON.stringify(whereClauseObj))
      CommonService.selectRecords(db,"LP_POLICY_ASSIGNMENT",false,'POLICY_NO',whereClauseObj).then(
                  function(res){
                      debug("LP_POLICY_ASSIGNMENT :"+res.rows.length);
                     if(!!res && res.rows.length>0){
                          debug(res.rows.item(0).POLICY_NO)
                          policyBean.POLICY_NO = res.rows.item(0).POLICY_NO;
                          policyBean.PROD_TYPE = prdType;
                          dfd.resolve(policyBean);
                     }
                     else
                      {
                          CommonService.hideLoading();
                          navigator.notification.alert(POLICYNO_MSG,null,"Application","OK");
                          dfd.resolve(null);
                      }
                  }
              );
      return dfd.promise;

  }
  this.AssignPolicyNo=function(policyBean){
    var dfd = $q.defer();
          var OPP_ID = psc.PSCModuleData.OPP_ID;
          var FHR_ID = psc.PSCModuleData.FHR_ID;
          var SIS_ID = psc.PSCModuleData.SIS_ID;
          var PGL_ID = psc.PSCModuleData.PGL_ID;
          var AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
          debug("inside AssignPolicyNo: "+JSON.stringify(psc.PSCModuleData));
       var whereClauseObj={};
                      whereClauseObj.POLICY_NO = policyBean.POLICY_NO;
                      whereClauseObj.POLICY_TYPE = policyBean.PROD_TYPE;
                      whereClauseObj.POLICY_FINAL_STATUS = 'F';
        var whereClauseObj1 = {};
                      whereClauseObj1.OPPORTUNITY_ID = OPP_ID;
                      whereClauseObj1.AGENT_CD = AGENT_CD;
       var whereClauseObj2 = {};
                       whereClauseObj2.DOC_CAT_ID = SIS_ID;
                       whereClauseObj2.AGENT_CD = AGENT_CD;
                      // whereClauseObj2.DOC_CAT = "SIS";
       var whereClauseObj3 = {};
                     whereClauseObj3.DOC_CAT_ID = FHR_ID;
                     whereClauseObj3.AGENT_CD = AGENT_CD;
                     //whereClauseObj3.DOC_CAT = "FHR";
       var whereClauseObj4 = {};
                      whereClauseObj4.DOC_CAT_ID = SIS_ID;
                      whereClauseObj4.AGENT_CD = AGENT_CD;

      CommonService.updateRecords(db, 'LP_POLICY_ASSIGNMENT',{"POLICY_FINAL_STATUS" :"U"}, whereClauseObj).then(
                      function(res){
                          debug("POLICY_FINAL_STATUS update success !!"+JSON.stringify(whereClauseObj));


                      /*CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                              function(res){
                                  debug("LP_APPLICATION_MAIN update success !!"+JSON.stringify(whereClauseObj1));
                              });
                              //PSC changes
                      CommonService.updateRecords(db, 'LP_APP_SOURCER_IMPSNT_SCRN_L',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                                      function(res){
                                                          debug("LP_APP_SOURCER_IMPSNT_SCRN_L update success !!"+JSON.stringify(whereClauseObj1));
                                                      });
                      CommonService.updateRecords(db, 'LP_APP_PAY_DTLS_SCRN_G',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj1).then(
                                                      function(res){
                                                          debug("LP_APP_PAY_DTLS_SCRN_G update success !!"+JSON.stringify(whereClauseObj1));
                                                      });*/
                      CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"POLICY_NO" :policyBean.POLICY_NO, "APPLICATION_ID": psc.PSCModuleData.APPLICATION_ID}, whereClauseObj1).then(
                              function(res){
                                  debug("LP_MYOPPORTUNITY update success !!"+JSON.stringify(whereClauseObj1));
                              });
                      CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj2).then(
                                  function(res){
                                  debug("SIS HTML update success !!"+JSON.stringify(whereClauseObj2));
                              });
                       CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj3).then(
                                                          function(res){
                                                          debug("FHR HTML update success !!"+JSON.stringify(whereClauseObj3));
                                                      });
                      CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.POLICY_NO}, whereClauseObj4).then(
                                  function(res){
                                      debug("All LP_DOCUMENT_UPLOAD: update success !!"+JSON.stringify(whereClauseObj4));
                                  dfd.resolve(res);
                              });

                      });
      return dfd.promise;
  }
}]);

//COMBO POLICY NO service
initiatePSCModule.service('PSCComboPolicyNoService',['$q','$state','CommonService','LoginService','SisFormService', function($q, $state, CommonService, LoginService, SisFormService){

    var combo = this;
    var APP_ID;
    var FHR_ID;
    var SIS_ID;
    var PGL_ID;
    var AGENT_CD;
    var whereClauseObj = {};

    var policyBean = {};

    this.loadOppFlags = function(mainId, AGENT_CD, type, flag){

            var dfd = $q.defer();
            var OppData = {}
    		var whereClauseObj = {};
                    console.log("loadOppFlags:"+type);
    				if(type == 'opp')
            		    whereClauseObj.OPPORTUNITY_ID = mainId;
            		else if(type == 'combo')
            		    {
            		        whereClauseObj.COMBO_ID = mainId;
            		        if(!!flag)
            		            whereClauseObj.RECO_PRODUCT_DEVIATION_FLAG = flag;
            		    }
            		    else if(type == 'app')
            		             whereClauseObj.APPLICATION_ID = mainId;
            		        else
            		             whereClauseObj.FHR_ID = mainId;
            		whereClauseObj.AGENT_CD = AGENT_CD;

    		console.log(JSON.stringify(whereClauseObj));
    		CommonService.selectRecords(db,"LP_MYOPPORTUNITY",false,"*",whereClauseObj, "").then(
    			function(res){
    				console.log(JSON.stringify(whereClauseObj)+"LP_MYOPPORTUNITY FLAGS :" + res.rows.length);
    				if(!!res && res.rows.length>0){

                        OppData.oppFlags = CommonService.resultSetToObject(res);

                        dfd.resolve(OppData);
    				}
    				else
    					dfd.resolve(OppData);
    			}
    		);

    		return dfd.promise;
    }
    //Check policy no already generated for TERM
    this.loadComboDtls = function(COMBO_ID){
     var dfd = $q.defer();
        AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
        console.log("Here loadComboDtls");
        combo.loadOppFlags(COMBO_ID, AGENT_CD, 'combo','P').then(function(oppData){
            console.log("oppdata >>"+JSON.stringify(oppData));
            if(!oppData.oppFlags.REF_APPLICATION_ID || !oppData.oppFlags.REF_POLICY_NO)
            {
                console.log("loadPolicy IF")
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                SIS_ID = oppData.oppFlags.REF_SIS_ID;
                FHR_ID = oppData.oppFlags.REF_FHRID;
                if(!!oppData.oppFlags.REF_APPLICATION_ID)
                {
                    APP_ID = oppData.oppFlags.REF_APPLICATION_ID;
                    policyBean.REF_APPLICATION_ID = oppData.oppFlags.REF_APPLICATION_ID;
                }
                else
                {
                    APP_ID = CommonService.getRandomNumber();
                    policyBean.REF_APPLICATION_ID = APP_ID;
                }

                SisFormService.loadSavedSISData(AGENT_CD, SIS_ID).then(function(sisData){
                      PGL_ID = sisData.sisMainData.PGL_ID;
                      combo.checkPolicyNo('C').then(function(polBean){
                          if(polBean!=undefined && polBean.POLICY_NO!=undefined && polBean.PROD_TYPE!=undefined)
                          {
                              policyBean.REF_POLICY_NO = polBean.POLICY_NO;
                              policyBean.PROD_TYPE = polBean.PROD_TYPE;
                              policyBean.COMBO_ID = COMBO_ID;
                              combo.AssignPolicyNo(policyBean).then(function(){
                                  dfd.resolve(policyBean);
                              });
                          }
                      });
                });
            }
            else
            {
                AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                SIS_ID = oppData.oppFlags.REF_SIS_ID;
                FHR_ID = oppData.oppFlags.REF_FHRID;
                console.log(FHR_ID+"loadPolicy ELSE"+SIS_ID);
                policyBean.REF_APPLICATION_ID = oppData.oppFlags.REF_APPLICATION_ID;
                policyBean.REF_POLICY_NO = oppData.oppFlags.REF_POLICY_NO;
                policyBean.PROD_TYPE = 'C'; //hardcoded for For Term
                policyBean.COMBO_ID = COMBO_ID;
                console.log("policyBean:"+JSON.stringify(policyBean));
                psc.PSCModuleData.ComboFormBean = policyBean;
                 combo.AssignPolicyNo(policyBean).then(function(){
                      dfd.resolve(policyBean);
                  });
               // dfd.resolve(policyBean);
            }
        });
        return dfd.promise;
    }

    //Get one policty with status 'F'
    this.checkPolicyNo=function(prdType){
            var dfd = $q.defer();
            var policyBean = {};
            var whereClauseObj={};
                    whereClauseObj.POLICY_TYPE = prdType;
                    whereClauseObj.POLICY_FINAL_STATUS = 'F';
                    debug(JSON.stringify(whereClauseObj))
            CommonService.selectRecords(db,"LP_POLICY_ASSIGNMENT",false,'POLICY_NO',whereClauseObj).then(
                        function(res){
                            debug("LP_POLICY_ASSIGNMENT :"+res.rows.length);
                           if(!!res && res.rows.length>0){
                                debug(res.rows.item(0).POLICY_NO)
                                policyBean.POLICY_NO = res.rows.item(0).POLICY_NO;
                                policyBean.PROD_TYPE = prdType;
                                dfd.resolve(policyBean);
                           }
                           else
                            {
                                CommonService.hideLoading();
                                navigator.notification.alert(POLICYNO_MSG,null,"Application","OK");
                                dfd.resolve(null);
                            }
                        }
                    );
            return dfd.promise;

        }

        //Assign term policy no
    this.AssignPolicyNo=function(policyBean){
            var dfd = $q.defer();

            var whereClauseObj={};
                            whereClauseObj.POLICY_NO = policyBean.REF_POLICY_NO;
                            whereClauseObj.POLICY_TYPE = 'C';
                            whereClauseObj.POLICY_FINAL_STATUS = 'F';
            var whereClauseObj1 = {};
                            whereClauseObj1.COMBO_ID = policyBean.COMBO_ID;
                            whereClauseObj1.AGENT_CD = AGENT_CD;
                            whereClauseObj1.RECO_PRODUCT_DEVIATION_FLAG = 'P';
            var whereClauseObj4 = {};
                            whereClauseObj4.COMBO_ID = policyBean.COMBO_ID;
                            whereClauseObj4.AGENT_CD = AGENT_CD;
                            whereClauseObj4.RECO_PRODUCT_DEVIATION_FLAG = 'T';
            var whereClauseObj2 = {};
                             whereClauseObj2.DOC_CAT_ID = SIS_ID;
                             whereClauseObj2.AGENT_CD = AGENT_CD;
                             //whereClauseObj2.DOC_CAT = "SIS";
            var whereClauseObj3 = {};
                           whereClauseObj3.DOC_CAT_ID = FHR_ID;
                           whereClauseObj3.AGENT_CD = AGENT_CD;
                           whereClauseObj3.DOC_CAT = "FHR";

                    CommonService.updateRecords(db, 'LP_POLICY_ASSIGNMENT',{"POLICY_FINAL_STATUS" :"U"}, whereClauseObj).then(
                    function(res){
                        debug("TERM POLICY_FINAL_STATUS update success !!"+JSON.stringify(whereClauseObj));

                    CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"REF_POLICY_NO" :policyBean.REF_POLICY_NO, "REF_APPLICATION_ID":policyBean.REF_APPLICATION_ID}, whereClauseObj1).then(
                            function(res){
                                debug("TERM LP_MYOPPORTUNITY update success !!"+JSON.stringify(whereClauseObj1));
                            });
                    CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"POLICY_NO" :policyBean.REF_POLICY_NO, "APPLICATION_ID":policyBean.REF_APPLICATION_ID}, whereClauseObj4).then(
                            function(res){
                                debug("TERM LP_MYOPPORTUNITY update success - 2 !!"+JSON.stringify(whereClauseObj1));
                            });
                    CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.REF_POLICY_NO}, whereClauseObj2).then(
                                function(res){
                                debug("TERM SIS HTML update success !!"+JSON.stringify(whereClauseObj2));
                            });
                     CommonService.updateRecords(db, 'LP_DOCUMENT_UPLOAD',{"POLICY_NO" :policyBean.REF_POLICY_NO}, whereClauseObj3).then(
                                function(res){
                                debug("TERM FHR HTML update success !!"+JSON.stringify(whereClauseObj3));
                                dfd.resolve(true);
                            });
                });
            return dfd.promise;
        }
}])
