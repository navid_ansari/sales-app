pscPage3Module.controller('PSCPage3Ctrl', ['$q','$state','CommonService','InitiatePSCService','PSCService','LoginService','PSCExistingData', function($q, $state, CommonService, InitiatePSCService, PSCService, LoginService, PSCExistingData){
    var psc = this;

    debug("PSCModuleData::"+JSON.stringify(InitiatePSCService.PSCModuleData));

    psc.PGL_ID = InitiatePSCService.PSCModuleData.PGL_ID;
    psc.isTerm = null;
    if(!!PSCExistingData){
        psc.rdOnlinePsc = PSCExistingData.DIGITAL_PSC_FLAG;
        psc.ISDIGITAL_PSC_DONE = PSCExistingData.ISDIGITAL_PSC_DONE;
        psc.DIGITAL_PSC_FLAG = PSCExistingData.DIGITAL_PSC_FLAG;
        psc.PscTiming = PSCExistingData.PREFERRED_CALL_TIME
        psc.PscLanguage =  PSCExistingData.PREFERRED_LANGUAGE_CD;
    }
    if(!!InitiatePSCService.PSCModuleData && !!InitiatePSCService.PSCModuleData.ComboFormBean && !!InitiatePSCService.PSCModuleData.COMBO_ID)
      {
        psc.isTerm = true;
        psc.rdOnlinePsc = 'M';
      }
    if(psc.PGL_ID == '224' || psc.PGL_ID == '225')
        psc.rdOnlinePsc = 'M';

    CommonService.hideLoading();
    InitiatePSCService.loadTableData(null, InitiatePSCService.PSCModuleData.LEAD_ID, InitiatePSCService.PSCModuleData.AGENT_CD, "LP_MYLEAD").then(function (leadData) {
          InitiatePSCService.PSCModuleData.leadData = leadData;
    });


    this.openMenu = function(){
          CommonService.openMenu();
        };

    this.onBack = function(){
      debug("Here pscPage2")
      $state.go("pscPage2");

    }

    this.callPscAPP = function(){

     PSCService.getPSCData().then(
            function(reqData){
                if(reqData){
                    debug("<<<< REQ DATA IS >>>>>"+JSON.stringify(reqData));
                    psc.callPSCApplication(reqData, InitiatePSCService.PSCModuleData.APPLICATION_ID);
                }
            }
        );
    }

    //call PSC Application...
     this.callPSCApplication = function(finalReq, APP_ID){

     debug("Device Platform :"+device.platform);

     cordova.exec(
                  function(succ){

                    debug("Inside Android device");
                  console.log("RETURN from PSC:"+JSON.stringify(succ));
                  try{
                      if(!!succ && !!succ.REQ && !!succ.REQ.STATUS)
                      {
                          if(!!succ.REQ.POLICY_NO){
                              for(var arr=0;arr<succ.REQ.POLICY_NO.length;arr++){

                                  console.log("arr values" + succ.REQ.POLICY_NO[arr]);
                                  PSCService.updatePendingStatus(InitiatePSCService.PSCModuleData.AGENT_CD,succ.REQ.POLICY_NO[arr]);
                              }
                          }
                          console.log(APP_ID+" PSC Response ::"+JSON.stringify(succ));
                          if(succ.REQ.STATUS == '1'){
                          PSCService.updatePSCData(APP_ID,finalReq.REQ.USERCODE).then(
                                    function(updated){
                                      if(updated){
                                        psc.ISDIGITAL_PSC_DONE = 'Y';
                                        navigator.notification.alert('PSC completed successfully !!',null, 'Success','Ok');
                                        debug("UPDATE PSC DATA FLAG");
                                      }
                                });
                          }
                          else
                          {
                              console.log("HERE - PSC not completed!");
                              //FinalSubmissionService.setStatus("0");
                              try{
                                  console.log("RELOAD state ::");
                                  navigator.notification.alert('PSC Verification not completed. Please try again.',null, 'Error','Ok');
                                  $state.reload();
                              }
                              catch(ex){
                                debug("exception ::"+ex.message);
                              }
                              console.log("Open Pop Up");
                          }
                      }
                      else
                        navigator.notification.alert('PSC Data not found',null, 'Error','Ok');

                  }catch(e){
                  debug("Excepting in PSC return:"+e.message);
                  }

                  //}
                  },
                  function(err){
                  console.log("req error :"+err);
                  navigator.notification.alert("PSC Application not Installed\nPlease Install PSC Application");
                  },"PluginHandler","CallPSC",[JSON.stringify(finalReq)]
                  );
     }

    this.Exit = function (pscForm){
      if(pscForm.$invalid!=true){
          if(psc.rdOnlinePsc == 'M')
          {
            var query = "insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L(APPLICATION_ID,AGENT_CD,POLICY_NO,SIS_ID,FHR_ID,PREFERRED_LANGUAGE_CD,PREFERRED_CALL_TIME,DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE) values(?,?,?,?,?,?,?,?,?)";
            var params = [InitiatePSCService.PSCModuleData.APPLICATION_ID,InitiatePSCService.PSCModuleData.AGENT_CD,InitiatePSCService.PSCModuleData.POLICY_NO,InitiatePSCService.PSCModuleData.SIS_ID,InitiatePSCService.PSCModuleData.FHR_ID,psc.PscLanguage,psc.PscTiming,"M","Y"];
            if(psc.ISDIGITAL_PSC_DONE=='Y')
            {
              var p = InitiatePSCService.PSCModuleData;
              $state.go("leadDashboard", {"LEAD_ID": p.LEAD_ID, "FHR_ID": p.FHR_ID, "SIS_ID": p.SIS_ID, "APP_ID": p.APPLICATION_ID, "PGL_ID": p.PGL_ID, "OPP_ID": p.OPP_ID, 'POLICY_NO': p.POLICY_NO,'COMBO_ID':p.COMBO_ID});
            }
            else
              PSCService.insertPSCData(query, params).then(function(resp) {
                navigator.notification.alert("PSC details saved successfully!!");
                var p = InitiatePSCService.PSCModuleData;
                $state.go("leadDashboard", {"LEAD_ID": p.LEAD_ID, "FHR_ID": p.FHR_ID, "SIS_ID": p.SIS_ID, "APP_ID": p.APPLICATION_ID, "PGL_ID": p.PGL_ID, "OPP_ID": p.OPP_ID, 'POLICY_NO': p.POLICY_NO,'COMBO_ID':p.COMBO_ID});
              });
          }
          else{
            var p = InitiatePSCService.PSCModuleData;
            $state.go("leadDashboard", {"LEAD_ID": p.LEAD_ID, "FHR_ID": p.FHR_ID, "SIS_ID": p.SIS_ID, "APP_ID": p.APPLICATION_ID, "PGL_ID": p.PGL_ID, "OPP_ID": p.OPP_ID, 'POLICY_NO': p.POLICY_NO,'COMBO_ID':p.COMBO_ID});
        }
      }
      else
        navigator.notification.alert("Please complete all details.");
    }
  }]);

pscPage3Module.service('PSCService', ['$q','$state','CommonService','LoginService','InitiatePSCService', function($q, $state, CommonService, LoginService, InitiatePSCService){
  var callPSC = this;

  this.createRequestForPSC = function(sisId,prdData,sessionID,APPLICATION_ID,AGENT_CD){
     var data = InitiatePSCService.PSCModuleData;
     var sisData = InitiatePSCService.PSCModuleData.sisData;
     var leadData = InitiatePSCService.PSCModuleData.leadData;
      var pscReqData = {};
      pscReqData.REQ = {};
      pscReqData.REQ.APPNO = data.POLICY_NO;//APPLICATION_ID;
      pscReqData.REQ.PRONAME = sisData.sisFormAData.INSURED_FIRST_NAME + ' ' + sisData.sisFormAData.INSURED_LAST_NAME;
      if(!sisData.sisFormAData.INSURED_EMAIL || !sisData.sisFormAData.INSURED_MOBILE)
      {
          pscReqData.REQ.EMAILID = sisData.sisFormAData.PROPOSER_EMAIL;
          pscReqData.REQ.MONO = sisData.sisFormAData.PROPOSER_MOBILE;
      }
      else
      {
          pscReqData.REQ.EMAILID = sisData.sisFormAData.INSURED_EMAIL;
          pscReqData.REQ.MONO = sisData.sisFormAData.INSURED_MOBILE;
      }
      pscReqData.REQ.COMADD = ((!!leadData.CURNT_ADDRESS1) ? leadData.CURNT_ADDRESS1 : "");
      if(!!leadData.CURNT_ADDRESS2)
      {
          pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ leadData.CURNT_ADDRESS2;
          if(!!leadData.CURNT_ADDRESS3)
              pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ leadData.CURNT_ADDRESS3 +" "+ leadData.CURNT_ZIP_CODE;
          else
              pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ ((!!leadData.CURNT_DISTRICT_LANDMARK) ? leadData.CURNT_DISTRICT_LANDMARK : "" ) +" "+ leadData.CURNT_ZIP_CODE;
      }
      else
          pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ ((!!leadData.CURNT_DISTRICT_LANDMARK) ? leadData.CURNT_DISTRICT_LANDMARK : "" ) +" "+ leadData.CURNT_ZIP_CODE;

      pscReqData.REQ.PLANNAME = sisData.sisFormBData.PLAN_NAME;
      pscReqData.REQ.PLANTYPE = prdData.PRODUCT_SOLUTION;
      pscReqData.REQ.PTYPE = prdData.PRODUCT_TYPE;
      pscReqData.REQ.PREAMT = sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
      pscReqData.REQ.FREQ = sisData.sisFormBData.PREMIUM_PAY_MODE_DESC; //not
      pscReqData.REQ.POLITERM = sisData.sisFormBData.POLICY_TERM;
      pscReqData.REQ.PRETEAMPAY = sisData.sisFormBData.PREMIUM_PAY_TERM;
      pscReqData.REQ.SUMASSURED = sisData.sisFormBData.SUM_ASSURED;
      pscReqData.REQ.MATU4 = "";
      pscReqData.REQ.MATU8 = "";
      pscReqData.REQ.MATULIP = "";
      var db = sisData.sisFormAData.INSURED_DOB;
      pscReqData.REQ.DOB = db.substring(0,10);
      pscReqData.REQ.ANNUOPT = "";
      pscReqData.REQ.ANNUAMT = "";
      pscReqData.REQ.ANNUFREQ = "";
      pscReqData.REQ.BRONAME = "";
      pscReqData.REQ.BANKNAME = "";//r.NEFT_BANK_NAME;
      pscReqData.REQ.MODFRE = sisData.sisFormBData.PREMIUM_PAY_MODE_DESC;//r.PREMIUM_PAY_MODE_DESC; // Added in new string
      pscReqData.REQ.MODPRE = sisData.sisMainData.MODAL_PREMIUM; // Added in new string
      pscReqData.REQ.USERID = AGENT_CD;     // Not in new string
      pscReqData.REQ.PROID = sisData.sisFormBData.PLAN_CODE; //sisData.PRODUCT_UINNO;
      pscReqData.REQ.USERCODE = AGENT_CD;
      pscReqData.REQ.USERNAME = LoginService.lgnSrvObj.userinfo.AGENT_NAME;
      pscReqData.REQ.PASSWORD = LoginService.lgnSrvObj.password;
      pscReqData.REQ.USERCONNO = LoginService.lgnSrvObj.userinfo.MOBILENO;
      pscReqData.REQ.EMAIL = LoginService.lgnSrvObj.userinfo.EMAIL_ID;
      pscReqData.REQ.USERDEG = LoginService.lgnSrvObj.userinfo.DESIGNATION_CODE;
      pscReqData.REQ.USERSTAT = "success";
      pscReqData.REQ.USERTYPE = "";
      if(BUSINESS_TYPE == 'iWealth')
          pscReqData.REQ.CHANID = 'Agency-DSF';
      else if(BUSINESS_TYPE == 'IndusSolution')
              pscReqData.REQ.CHANID = 'BANCA';
           else
              pscReqData.REQ.CHANID = 'AGENCY';
      pscReqData.REQ.SESSION = sessionID;
      pscReqData.REQ.SAVEFLAG = "";
      pscReqData.REQ.IMAGEOFUSER = "";
      console.log("DATA TO PSC :"+JSON.stringify(pscReqData));
      return pscReqData;
  };
  this.insertPSCData=function(query,parameters){
    var dfd = $q.defer();
    db.transaction(function(tx){
      tx.executeSql(query,parameters,
      function(success){
        debug("success fully inserted PSC data");
        dfd.resolve(true);
      },
      function(tx, err){
        debug("fail to insert PSC data"+err.message);
        dfd.resolve(false);
      }
      )
    });
    return dfd.promise;
  }
      /*Psc offline popup data flag*/
      this.getPscExistingData=function(APPLICATION_ID,AGENT_CD){
          var dfd=$q.defer();
          var query="select PREFERRED_CALL_TIME,PREFERRED_LANGUAGE_CD,DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID=? and AGENT_CD=?";
          var parameter=[APPLICATION_ID,AGENT_CD];
          var pscObj={};
          db.transaction(function(tx){
                tx.executeSql(query,parameter,
                function(tx,success){
                if(success.rows.length!=0){
                debug("success fully selected Psc Offline Data" + JSON.stringify(success.rows.item(0)));
                      pscObj = CommonService.resultSetToObject(success);
                      dfd.resolve(pscObj);
                }
                else
                    dfd.resolve(null);
            },
            function(tx,err){
                debug("fail to insert PSC Offline Data");
                dfd.resolve(null);
            })
         })
         return dfd.promise;
      }
      this.generatePSCSessionId = function(){

           var currentdate = new Date();
           var sessionID = "A" + currentdate.getDate() +
                              + (currentdate.getMonth()+1)  +
                              + currentdate.getFullYear() +
                              + currentdate.getHours() +
                              + currentdate.getMinutes() +
                              + currentdate.getSeconds();
           return sessionID;
      }
      this.updateUserInfo = function(sessionID,AGENT_CD){
          var dfd = $q.defer();
          var query = "update LP_USERINFO set PSC_SESSION_ID = ? where AGENT_CD = ?";
          var parameters = [sessionID,AGENT_CD];
          db.transaction(function(tx){
              tx.executeSql(query,parameters,
              function(tx,success){
                  dfd.resolve(true);
              },
              function(tx,err){
                  debug("fail to insert PSC data");
                  dfd.resolve(false);
              }
              )
          })
          return dfd.promise;
      }

      this.updatePSCData = function(APPLICATION_ID,AGENT_CD){
          var dfd = $q.defer();
          var query = "update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE = ? where APPLICATION_ID = ? AND AGENT_CD = ?";
          var parameters = ["Y",APPLICATION_ID,AGENT_CD];

          db.transaction(function(tx){
              tx.executeSql(query,parameters,
                  function(tx,success){
                      dfd.resolve(true);
                  },
                  function(tx,err){
                      debug("fail to insert PSC data");
                      dfd.resolve(false);
                  }
              )
          })

          return dfd.promise;
      }
      /*Pending status*/
      this.updatePendingStatus=function(AGENT_CD,PolicyNo){
          var dfd=$q.defer();
          var query="update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE= ? where POLICY_NO= ?";
          var parameter=["Y",PolicyNo];
          db.transaction(function(tx){
              tx.executeSql(query,parameter,
              function(tx,res){
              console.log("updated Successfully! "+ JSON.stringify(res));
              console.log("Policy No" + PolicyNo);
              dfd.resolve("success");
              },
              function(tx,err){
              console.log("Updated"+ err.message);
              }
              );
          },
          function(err){
              console.log("error while updating policyNo");
          }
          );
          return dfd.promise;
      }
      this.getProductData = function(AGENT_CD,SIS_ID){
          var dfd = $q.defer();
          //PSC change
          var query = "select SIS.PGL_ID, PD.PRODUCT_UINNO, PD.PRODUCT_SOLUTION,PD.PRODUCT_TYPE from LP_SIS_MAIN AS SIS INNER JOIN LP_PRODUCT_MASTER AS PD ON SIS.PGL_ID==PD.PRODUCT_PGL_ID where SIS.SIS_ID = ? AND SIS.AGENT_CD = ?";
          var parameters = [SIS_ID,AGENT_CD];

          db.transaction(function(tx){
              tx.executeSql(query,parameters,
              function(tx,success){
                  debug("sis data success"+JSON.stringify(success.rows.item(0)));
                  var res = success;
                  if(!!res && res.rows.length > 0)
                      dfd.resolve(res.rows.item(0));
                  else
                      dfd.resolve(null);
              },
              function(tx,err){
                  debug("sis data failed");
                  dfd.resolve(null);
              }
              )
          });

          return dfd.promise;
      };
      this.getPSCData = function(){
          var dfd = $q.defer();
            var sessionID = callPSC.generatePSCSessionId();
            debug(InitiatePSCService.PSCModuleData.SIS_ID+"<<<<SESSSION ID>>>>"+sessionID);
            callPSC.getProductData(InitiatePSCService.PSCModuleData.AGENT_CD,InitiatePSCService.PSCModuleData.SIS_ID).then(
                function(prdData){
                    debug("<<<<<<<prdData DATA SUCCESS>>>>>>"+JSON.stringify(prdData));
                    var finalReq = callPSC.createRequestForPSC(InitiatePSCService.PSCModuleData.SIS_ID,prdData,sessionID,InitiatePSCService.PSCModuleData.APPLICATION_ID,InitiatePSCService.PSCModuleData.AGENT_CD);
                    var query = "insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L(APPLICATION_ID,AGENT_CD,POLICY_NO,SIS_ID,FHR_ID,PREFERRED_LANGUAGE_CD,PREFERRED_CALL_TIME,DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE) values(?,?,?,?,?,?,?,?,?)";
                    var params = [InitiatePSCService.PSCModuleData.APPLICATION_ID,InitiatePSCService.PSCModuleData.AGENT_CD,InitiatePSCService.PSCModuleData.POLICY_NO,InitiatePSCService.PSCModuleData.SIS_ID,InitiatePSCService.PSCModuleData.FHR_ID,null,null,"D","P"];
                    debug("<<<< PARAMETERS >>>"+params);
                    callPSC.insertPSCData(query, params).then(function(succ) {
                      callPSC.updateUserInfo(sessionID,InitiatePSCService.PSCModuleData.AGENT_CD).then(
                          function(updated){
                              if(updated){
                                  debug("<<<<<<< CALL PSC APPLICATION >>>>>>>");
                                  dfd.resolve(finalReq);
                              }
                          });
                    });
                }
            );
            return dfd.promise;
        }

}]);
