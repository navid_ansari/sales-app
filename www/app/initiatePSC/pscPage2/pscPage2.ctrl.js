pscPage2Module.controller('PSCPage2Ctrl', ['$q','$state','CommonService', 'PolicyNoData','TermPolicyNoData','InitiatePSCService','LoginService', function($q, $state, CommonService, PolicyNoData, TermPolicyNoData, InitiatePSCService, LoginService){
    var pol = this;

    debug("PSCModuleData::"+JSON.stringify(InitiatePSCService.PSCModuleData));
    pol.POLICY_NO = PolicyNoData.POLICY_NO;
    pol.REF_POLICY_NO = '';
    pol.COMBO_ID = (!!InitiatePSCService.PSCModuleData && InitiatePSCService.PSCModuleData.COMBO_ID) ?InitiatePSCService.PSCModuleData.COMBO_ID : '';

    console.log(JSON.stringify(TermPolicyNoData));
    debug("InitiatePSCService.PSCModuleData.ComboFormBean :"+JSON.stringify(InitiatePSCService.PSCModuleData.ComboFormBean));
    if(!!TermPolicyNoData && !!TermPolicyNoData.REF_POLICY_NO)
        {
          if(!InitiatePSCService.PSCModuleData.ComboFormBean)
            InitiatePSCService.PSCModuleData.ComboFormBean = {};
            pol.REF_POLICY_NO = TermPolicyNoData.REF_POLICY_NO;
            InitiatePSCService.PSCModuleData.ComboFormBean.REF_APPLICATION_ID = TermPolicyNoData.REF_APPLICATION_ID;
            InitiatePSCService.PSCModuleData.ComboFormBean.REF_POLICY_NO = TermPolicyNoData.REF_POLICY_NO;
        }
    CommonService.hideLoading();

    this.openMenu = function(){
          CommonService.openMenu();
        };

    this.onNext = function (){
      var p = InitiatePSCService.PSCModuleData;
      $state.go("pscPage3");
    }
    this.onBack = function(){
       var p = InitiatePSCService.PSCModuleData;
      var params = {"OPP_ID": p.OPP_ID,"APPLICATION_ID": p.APPLICATION_ID, "AGENT_CD": p.AGENT_CD, "SIS_ID": p.SIS_ID, "FHR_ID": p.FHR_ID, "POLICY_NO": p.POLICY_NO, "LEAD_ID": p.LEAD_ID, "COMBO_ID": p.COMBO_ID};
      $state.go("pscPage1", params);

    }
  }]);
