  pscPage1Module.controller('pscPage1Ctrl',['$state', 'CommonService', 'LoginService', 'LeadDashboardService', '$stateParams','pscPage1ModuleService','SISData','loadEKYCOpportunityData','$filter','LeadData','FhrPersonalDetailsService','InitiatePSCService',function($state, CommonService, LoginService, LeadDashboardService, $stateParams,pscPage1ModuleService,SISData,loadEKYCOpportunityData,$filter,LeadData,FhrPersonalDetailsService,InitiatePSCService){
      "use strict";

  	CommonService.hideLoading();
  	var pscCtrl = this;
    pscCtrl.click = false;
  	pscCtrl.disableFlag=false;
  	pscCtrl.srState=false;
    pscCtrl.stateList=[];
    pscCtrl.cityList=[];
  	this.pscPage1Data={};

    var addressDetails = {"insured": {},"proposer": {}};
    var ADRRESS_DATA=null;

    this.openMenu = function(){
      CommonService.openMenu();
    };
    InitiatePSCService.loadTableData(InitiatePSCService.PSCModuleData.OPP_ID, InitiatePSCService.PSCModuleData.LEAD_ID, InitiatePSCService.PSCModuleData.AGENT_CD, "LP_MYOPPORTUNITY").then(function (oppData){
              InitiatePSCService.PSCModuleData.oppData = oppData;
            });
    CommonService.showLoading("Loading...");
  	FhrPersonalDetailsService.getStateList().then(function(res){
    	console.log("array val :"+JSON.stringify(res));
    	pscCtrl.stateList=res;
      pscCtrl.stateOptionSelected = pscCtrl.stateList[0];
          FhrPersonalDetailsService.getCityList().then(function(res){
            console.log("array val :"+JSON.stringify(res));
            pscCtrl.cityList=res;
            pscCtrl.cityOptionSelected = pscCtrl.cityList[0];
            pscCtrl.loadFormData();
          });
  	});

    this.onStateChange = function(){
        this.pscPage1Data.CURNT_STATE = this.stateOptionSelected.STATE_CODE;
        debug("state code is "+this.stateOptionSelected.STATE_CODE)
        FhrPersonalDetailsService.getCityList(this.pscPage1Data.CURNT_STATE).then(
            function(res){
                pscCtrl.cityList = res;
            }
        );
    };

    this.onCityChange = function(){
        this.pscPage1Data.CURNT_CITY = this.cityOptionSelected.CITY_CODE;
    };

    this.setFormAddress  = function(addData){
      debug("<<<<<<<<<< SETTING FORM ADDRESS >>>>>>>>>>>");
      try{

        var STATE = '';
      var CITY = 'OT';
      if(!addData)
        {
          STATE = LeadData.CURNT_STATE;
          CITY = LeadData.CURNT_CITY;
          console.log("city for other"+JSON.stringify(LeadData));
        }
        else
        {
          console.log("city for else other"+JSON.stringify(addData));
          STATE = addData.STATE;
          CITY = addData.CITY;
          console.log("city for else other"+JSON.stringify(addData));
        }

      pscCtrl.pscPage1Data.CURNT_ADDRESS1 = (!!pscCtrl.ADRRESS_DATA && !!pscCtrl.ADRRESS_DATA.addressLine1) ? (pscCtrl.ADRRESS_DATA.addressLine1.trim()) : ( (!!LeadData.CURNT_ADDRESS1) ? LeadData.CURNT_ADDRESS1 : "");
      pscCtrl.pscPage1Data.CURNT_ADDRESS2 =  (!!pscCtrl.ADRRESS_DATA && !!pscCtrl.ADRRESS_DATA.addressLine2) ? (pscCtrl.ADRRESS_DATA.addressLine2.trim()) : ( (!!LeadData.CURNT_ADDRESS2) ? LeadData.CURNT_ADDRESS2 : "");
      pscCtrl.pscPage1Data.CURNT_ADDRESS3 = (!!pscCtrl.ADRRESS_DATA && !!pscCtrl.ADRRESS_DATA.addressLine3) ? (pscCtrl.ADRRESS_DATA.addressLine3.trim()) : ( (!!LeadData.CURNT_ADDRESS3) ? LeadData.CURNT_ADDRESS3 : "");
      pscCtrl.pscPage1Data.CURNT_DISTRICT_LANDMARK = (!!pscCtrl.ADRRESS_DATA && !!pscCtrl.ADRRESS_DATA.landmark) ? (pscCtrl.ADRRESS_DATA.landmark.trim()) : ( (!!LeadData.CURNT_DISTRICT_LANDMARK) ? LeadData.CURNT_DISTRICT_LANDMARK : "");
      pscCtrl.pscPage1Data.CURNT_ZIP_CODE = (!!pscCtrl.ADRRESS_DATA && !!pscCtrl.ADRRESS_DATA.pincode ) ? parseInt(pscCtrl.ADRRESS_DATA.pincode.trim()) : ((!!LeadData.CURNT_ZIP_CODE) ? parseInt(LeadData.CURNT_ZIP_CODE) : parseInt(0));
      console.log("data:"+ JSON.stringify(pscCtrl.pscPage1Data));
      if(!!pscCtrl.ADRRESS_DATA)
          pscCtrl.stateOptionSelected = $filter('filter')(pscCtrl.stateList, { "STATE_DESC": STATE.toUpperCase()}, true)[0];
      else
          pscCtrl.stateOptionSelected = $filter('filter')(pscCtrl.stateList, { "STATE_CODE": STATE}, true)[0];

      pscCtrl.pscPage1Data.CURNT_STATE = (!!pscCtrl.stateOptionSelected) ? pscCtrl.stateOptionSelected.STATE_CODE : "";
      pscCtrl.pscPage1Data.CURNT_STATE = (!!pscCtrl.stateOptionSelected) ? pscCtrl.stateOptionSelected.STATE_CODE : "";

      CommonService.hideLoading();
      if(!!pscCtrl.stateOptionSelected)
      FhrPersonalDetailsService.getCityList(pscCtrl.stateOptionSelected.STATE_CODE).then(
                           function(res){
                               pscCtrl.cityList = res;
                               if(!!pscCtrl.ADRRESS_DATA)
                               {
                                 var CITY_SELECT = {};
                                 var cityList = $filter('filter')(pscCtrl.cityList, {"CITY_DESC": CITY.toUpperCase()}, true)[0];
                                 pscCtrl.cityOptionSelected = (!!cityList && !!cityList.CITY_CODE)?cityList:$filter('filter')(pscCtrl.cityList, {"CITY_CODE": "OT"}, true)[0];
                                  pscCtrl.pscPage1Data.CURNT_CITY = pscCtrl.cityOptionSelected.CITY_CODE;
                                  if((pscCtrl.pscPage1Data.CURNT_CITY =='OT')||(!!LeadData.Other_CityName)){
                                    pscCtrl.Other_CityName = LeadData.Other_CityName;
                                  }else{
                                    pscCtrl.Other_CityName = null;
                                  }
                                }
                               else
                                  {
                                    pscCtrl.cityOptionSelected = $filter('filter')(pscCtrl.cityList, { "CITY_CODE":CITY }, true)[0];
                                    pscCtrl.pscPage1Data.CURNT_CITY = pscCtrl.cityOptionSelected.CITY_CODE;
                                    if((pscCtrl.pscPage1Data.CURNT_CITY =='OT')||(!!LeadData.Other_CityName)){
                                      pscCtrl.Other_CityName = LeadData.Other_CityName;
                                    }else{
                                      pscCtrl.Other_CityName = null;
                                    }
                                  }


                           }
                       );

      }catch(e){
        CommonService.hideLoading();
          debug("Exception in setFormAddress"+e.message);
      }

    }

    this.setSR_StateCity = function(){
      debug("<<<<<<<<<< SETTING SR/SR+ ADDRESS >>>>>>>>>>>");
      console.log("SISData.sisMainData.PGL_ID in else"+SISData.sisMainData.LEAD_ID);
                    pscPage1ModuleService.loadSRData(SISData.sisMainData.LEAD_ID).then(
                    function(res){
                              if(!!res){
                                console.log("data for sr and sr+ :"+ JSON.stringify(res)+res.CURR_STATE+res.CURR_CITY);
                                var add = {};
                                add.STATE = res.CURR_STATE_CODE;
                                add.CITY = res.CURR_CITY_CODE;

                                pscCtrl.setFormAddress(add);
                              }
                              else{

                                console.log("Error in SR data");
                                pscCtrl.setFormAddress();
                              }
                          }
                    );
    }

    this.setEKYCData = function(EKYC_ID){
      debug("<<<<<<<<<< SETTING EKYC ADDRESS >>>>>>>>>>>");
      pscPage1ModuleService.loadEKYCAddressData(EKYC_ID).then(
                     function(res){
                            if(!!res){
                              pscCtrl.ADRRESS_DATA = pscPage1ModuleService.ekycAddressLogic(res);
                              pscCtrl.setFormAddress(res);
                              console.log("data for address detail for spliting address:"+ JSON.stringify(pscCtrl.ADRRESS_DATA));

                            }
                            else{
                                console.log("Error in insured EKYC data");
                                pscCtrl.setFormAddress();
                            }
                    }
                );

    }

    this.loadFormData = function(){

    // for self Case
    if(loadEKYCOpportunityData.BUY_FOR == '200'){
      debug("<<<<<<<<<< SELF CASE >>>>>>>>>>>");
        if(loadEKYCOpportunityData.INS_EKYC_FLAG == 'Y'){
            addressDetails.insured.EKYC_ID = null;
            addressDetails.insured.EKYC_ID=loadEKYCOpportunityData.INS_EKYC_ID;

            if(!!addressDetails.insured.EKYC_ID){
                pscCtrl.setEKYCData(addressDetails.insured.EKYC_ID);
            }
            else if(SISData.sisMainData.PGL_ID=='225'|| SISData.sisMainData.PGL_ID=='224'){
                    pscCtrl.setSR_StateCity();
            }
            else{
              console.log("Ending if of EKYC_ID");
              pscCtrl.setFormAddress();
            }

        }
        // INS_EKYC_FLAG else portion.
        else{
             if(SISData.sisMainData.PGL_ID=='225'|| SISData.sisMainData.PGL_ID=='224'){
                    console.log("SISData.sisMainData.PGL_ID in else"+SISData.sisMainData.LEAD_ID);
                    pscCtrl.setSR_StateCity();
                }
                else
                  {
                    debug("Lead data population");
                    pscCtrl.setFormAddress();
                  }
        }
    }
    else{  // for non self case.
        debug("<<<<<<<<<< NON-SELF CASE >>>>>>>>>>>");
        if(loadEKYCOpportunityData.PR_EKYC_FLAG == 'Y'){
            addressDetails.proposer.EKYC_ID = null;
            addressDetails.proposer.EKYC_ID=loadEKYCOpportunityData.PR_EKYC_ID;
             if(!!addressDetails.proposer.EKYC_ID){
                  pscCtrl.setEKYCData(addressDetails.proposer.EKYC_ID);
             }
             else if(SISData.sisMainData.PGL_ID=='225'|| SISData.sisMainData.PGL_ID=='224'){
                        console.log("SISData.sisMainData.PGL_ID in else"+SISData.sisMainData.LEAD_ID);
                        pscCtrl.setSR_StateCity();
            }
            else{
              console.log("2: Lead data population");
              pscCtrl.setFormAddress();
            }

        }
        else{
            if(SISData.sisMainData.PGL_ID=='225'|| SISData.sisMainData.PGL_ID=='224'){
                console.log("SISData.sisMainData.PGL_ID in else"+SISData.sisMainData.LEAD_ID);
                  pscCtrl.setSR_StateCity();
            }
            else{
              console.log("ending SR and SR+");
              pscCtrl.setFormAddress();
            }
        }
    }


    if((loadEKYCOpportunityData.INS_EKYC_FLAG == 'Y' && (!!loadEKYCOpportunityData.INS_EKYC_ID) && loadEKYCOpportunityData.BUY_FOR == '200')||(loadEKYCOpportunityData.PR_EKYC_FLAG == 'Y' && (!!loadEKYCOpportunityData.PR_EKYC_ID))){
        pscCtrl.disableFlag=true;
    }
    else{
        pscCtrl.disableFlag=false;
    }
    if((SISData.sisMainData.PGL_ID=='225'|| SISData.sisMainData.PGL_ID=='224') && (loadEKYCOpportunityData.INS_EKYC_FLAG == 'N'|| loadEKYCOpportunityData.PR_EKYC_FLAG == 'N')){

       pscCtrl.srState=false;

    }
    else{

        pscCtrl.srState=false;
    }
    }
    //pscCtrl.pscPage1Data.Other_CityName=pscCtrl.pscPage1Data.Other_CityName;
    pscCtrl.pscPage1Data.Other_CityName=pscCtrl.Other_CityName;
    console.log("PscPageData"+JSON.stringify(pscCtrl.pscPage1Data));
    console.log("PscPageData"+JSON.stringify(pscCtrl.pscPage1Data.Other_CityName));
    this.onNext=function(pscPageForm){
      pscCtrl.click = true;
      if((!!pscCtrl.pscPage1Data.CURNT_CITY)&&(pscCtrl.pscPage1Data.CURNT_CITY =='OT'))
      pscCtrl.pscPage1Data.Other_CityName=pscCtrl.Other_CityName;
      console.log("othercity"+  pscCtrl.pscPage1Data.Other_CityName);
      if(pscPageForm.$invalid !=true){
         var whereClauseObj = {};
         whereClauseObj.IPAD_LEAD_ID = InitiatePSCService.PSCModuleData.LEAD_ID;
          CommonService.updateRecords(db, 'LP_MYLEAD',pscCtrl.pscPage1Data, whereClauseObj).then(
          function(res){
              navigator.notification.alert("Address Details saved successfully!!");
              console.log("LP_MYLEAD pscCtrl.pscPage1Data update success !!"+JSON.stringify(whereClauseObj));
                $state.go("pscPage2");
               }
           );
      }
      else
        navigator.notification.alert("Please complete Address Details.")
    }


  }]);
  pscPage1Module.service('pscPage1ModuleService',['$q', 'CommonService', 'LoginService', '$state', 'SisTimelineService', '$filter','$stateParams', function($q, CommonService, LoginService, $state, SisTimelineService, $filter,$stateParams){

      var psc=this;
      this.loadEKYCAddressData = function(EKYC_ID){
              var dfd = $q.defer();
              var oppData = {};
              var params =  {"EKYC_ID" :EKYC_ID};
              CommonService.selectRecords(db, "LP_LEAD_EKYC_DTLS", true, null,params).then(
              function(res){
                  debug("Opp :: res.rows.length: " + res);
                  if(!!res && res.rows.length>0){
                      oppData = CommonService.resultSetToObject(res);
                      console.log(oppData);
                      dfd.resolve(oppData);
                  }
                  else{
                      dfd.resolve(null);
                  }
              }
              );
              return dfd.promise;
          };
      this.loadSRData = function(Lead_ID){
          var dfd = $q.defer();
          var oppData = {};
          var params =  {"LEAD_ID" :Lead_ID};
          CommonService.selectRecords(db, "LP_TERM_VALIDATION", true, null,params).then(
          function(res){
              debug("Opp :: res.rows.length for term validation: " + res);
              if(!!res && res.rows.length>0){
                  oppData = CommonService.resultSetToObject(res);
                  console.log(oppData);
                  dfd.resolve(oppData);
              }
              else{
                  dfd.resolve(null);
              }
          }
          );
          return dfd.promise;
      };


      this.ekycAddressLogic = function(res) {
          var addressLine1 = "";
          var addressLine2 = "";
          var addressLine3 = "";
          var landmark = "";
          var carryForward = "";
          var areaDone = false;
          var distSubDistDone = false;
          var i = 0;
          addressLine1 += ((!!res.BUILDING)?(res.BUILDING + " "):"");
          addressLine1 += ((!!res.STREET)?(res.STREET + " "):"");

          if(!addressLine1 || addressLine1.length<45){
              addressLine1 += res.AREA || "";
              areaDone = true;
          }
          if(!addressLine1 || addressLine1.length < 45){
                addressLine1 += res.DISTRICT || "";
                addressLine1 += res.SUBDISTRICT || "";
                distSubDistDone = true;
          }
          if(!!addressLine1 && addressLine1.length>45){
              var addr1SplitArr = addressLine1.split(" ");
              var _addressLine1 = "";
              carryForward = "";
                  for(i=0;i<addr1SplitArr.length;i++){
                      if((!carryForward) && ((_addressLine1 + addr1SplitArr[i]).length < 45)) {
                          _addressLine1 += addr1SplitArr[i] + " ";
                      }
                      else {
                          carryForward += addr1SplitArr[i] + " ";
                      }
                  }
              addressLine1 = (!!_addressLine1)?(_addressLine1.trim()):"";
          }
          else{
             carryForward = "";
          }
          addressLine2 += carryForward + " ";
          if(!addressLine2 || addressLine2.length<45){
              if(!areaDone)
                  addressLine2 += res.AREA || "";
              if(!addressLine2){
                  addressLine2 += res.DISTRICT || "";
                  addressLine2 += res.SUBDISTRICT || "";
                  addressLine2 = addressLine2 || null;
                  distSubDistDone = true;
              }
          }
          if(!!addressLine2 && addressLine2.length>45){
              var addr2SplitArr = addressLine2.split(" ");
              var _addressLine2 = "";
              carryForward = "";
              for(i=0;i<addr2SplitArr.length;i++){
                  if((!carryForward) && ((_addressLine2 + addr2SplitArr[i]).length < 45)) {
                      _addressLine2 += addr2SplitArr[i] + " ";
                  }
                  else {
                      carryForward += addr2SplitArr[i] + " ";
                  }
              }
          addressLine2 = (!!_addressLine2)?(_addressLine2.trim()):null;
          }
          else{
          carryForward = "";
          }
          addressLine3 += carryForward + " ";
          if(!addressLine3 || addressLine3.length<45){
              if(!distSubDistDone){
                  addressLine3 += res.DISTRICT || "";
                  addressLine3 += res.SUBDISTRICT || "";
                  addressLine3 = addressLine3 || null;
              }
          }
          if(!!addressLine3 && addressLine3.length>45){
              var addr3SplitArr = addressLine3.split(" ");
              var _addressLine3 = "";
              carryForward = "";
              for(i=0;i<addr3SplitArr.length;i++){
                  if((!carryForward) && ((_addressLine3 + addr3SplitArr[i]).length < 45)) {
                      _addressLine3 += addr3SplitArr[i] + " ";
                  }
                  else {
                      carryForward += addr3SplitArr[i] + " ";
                  }
              }
          addressLine3 = (!!_addressLine3)?(_addressLine3.trim()):null;
          }
          else{
          carryForward = "";
          }
          landmark += carryForward + " ";
          if(!landmark || landmark.length<45){
              landmark += res.LANDMARK || "";
          }
          if(!!landmark && landmark.length>45){
              var landmarkSplitArr = landmark.split(" ");
              var _landmark = "";
              carryForward = "";
              for(i=0;i<landmarkSplitArr.length;i++){
                  if((!carryForward) && ((_landmark + landmarkSplitArr[i]).length < 45)) {
                      _landmark += landmarkSplitArr[i] + " ";
                  }
                  else {
                      carryForward += landmarkSplitArr[i] + " ";
                  }
              }
          landmark = (!!_landmark)?(_landmark.trim()):null;
          }

      return {"addressLine1": addressLine1, "addressLine2": addressLine2, "addressLine3": addressLine3, "landmark": landmark, "carryForward": carryForward, "pincode":res.PIN};
      };


  }]);
