showDatabaseModule.controller('showDatabaseCtrl', ['$state', '$q', 'CommonService', 'ShowDBService', 'allDbTableNames', function($state, $q, CommonService, ShowDBService, allDbTableNames) {
	"use strict";
	var thisObj = this;

	this.openMenu = function() {
		CommonService.openMenu();
	};

	setTimeout(function(){
		var addLeadCalcHeight = $(".sdb-height .fixed-bar").height();
		debug("calcHeight : " + addLeadCalcHeight);
		$(".sdb-height .custom-position").css({top:addLeadCalcHeight + "px"});

		var mytableHeight = ($(".sdb-height").height() - (($(".firstPart").height())+(addLeadCalcHeight)));
		debug("mytable height: " + mytableHeight + "px");
		$(".mytable").css({height: (mytableHeight + "px")});
	});

	this.tableArray = [];

	if (allDbTableNames.rows.length > 0) {
		for (var j=0; j<allDbTableNames.rows.length; j++) {
			this.tableArray.push({
				name: allDbTableNames.rows.item(j).name,
				table_name: allDbTableNames.rows.item(j).tbl_name,
				type: allDbTableNames.rows.item(j).type
			});
		}
	}


	debug("tableArray");
	debug(this.tableArray);

	this.showTableData = function() {
		this.emptyQuery = "";
		var selected_table = this.tableNameSelected.table_name;

		if(!!selected_table){
			CommonService.showLoading();
			ShowDBService.getTableData(selected_table).then(
				function(res) {
					debug("Response    " + JSON.stringify(res));
					thisObj.tableData = res;
					CommonService.hideLoading();
				}
			);
		}
	};

	/*********Sorting and filter starts*************/
	this.tableDataRowCount = 100;
	this.queryToExecute = "";
	this.sortColumn = "";
	this.searchAnyText = "";
	this.emptyQuery = "";
	this.reverseSort = false;
	this.hidecolumn = false;
	this.hideQueryPanel = false;

	//to sort table column
	this.sortData = function(column) {
		debug("column -> " + column);
		this.reverseSort = (this.sortColumn == column) ? !this.reverseSort : false;
		this.sortColumn = column;
	};


	//to get sort arrow class
	this.getSortClass = function(column) {
		if (this.sortColumn == column) {
			return this.reverseSort ? 'arrow-down' : 'arrow-up';
		}
		return '';
	};

	//to show selected table column
	this.tableMultiColSelected = ['all'];
	this.isColumnShown = function(columnName) {
		if (this.tableMultiColSelected.indexOf('all') !== -1) {
			return true;
		}

		var cloumnListLength = this.tableMultiColSelected.length;
		for (var i = 0; i <= cloumnListLength; i++) {
			if (this.tableMultiColSelected.indexOf(columnName) !== -1) {
				return true;
			} else {
				return false;
			}
		}
	};

	this.deleteRow = function(tableName, delRowObject) {
		debug("Inside deleteRow ");
		debug(delRowObject);
		var keys = Object.keys(delRowObject);
		var str = "";
		for (var i = 0; i < keys.length; i++) {
			if (!!delRowObject[keys[i]] && keys[i]!=="$$hashKey"){
				str += keys[i] + "='" + delRowObject[keys[i]] + "' AND ";
			}
		}
		debug(str.length);
		str = str.substring(0, str.length - 4);
		str = "DELETE FROM " + tableName.name + " where " + str;
		this.queryToExecute = str;
		this.executeQuery();

		this.tableNameSelected = {"table_name": tableName.name};

		this.showTableData();
		debug(str);
	};

	//To execute query
	this.executeQuery = function() {
		debug("Query to execute");
		debug(this.queryToExecute);
		this.emptyQuery = "";
		this.tableMultiColSelected = ['all'];
		this.tableNameSelected = '';
		if (this.queryToExecute.length > 0) {
			ShowDBService.executeQuery(this.queryToExecute).then(
				function(res) {
					debug("Res from query");
					debug(res);
					debug("Response from query " + JSON.stringify(res));
					thisObj.tableData = res;
				}
			);
		} else {
			this.emptyQuery = "Please enter query.";
		}
	};

	//To reset query
	this.resetQuery = function() {
		this.queryToExecute = "";
	};

	//to show/hide query box
	this.showHideQuery = function(showHideBoolean) {
		this.hideQueryPanel = showHideBoolean ? false : true;
	};
		/*********Sorting and filter ends*************/
	CommonService.hideLoading();
}]);


showDatabaseModule.service('ShowDBService', ['$state', '$q', 'CommonService', function($state, $q, CommonService) {
	var thisObj = this;
	this.getAllTableName = function() {
		var dfd = $q.defer();
		debug("Fetching database tables");
		CommonService.transaction(db,
			function(tx) {

				CommonService.executeSql(tx, "SELECT * FROM sqlite_master where type = 'table' order by name", [],
					function(tx, res) {
						dfd.resolve(res);
					},
					function(tx, err) {
						debug("Error in fetching table names: " + err.message);
						dfd.resolve(null);
					}
				);
			},
			function(err) {
				debug("Error in fetchErrDescMasterData(): " + err.message);
				dfd.resolve(null);
			}, null
		);
		return dfd.promise;
	};

	this.getTableData = function(tableName) {
		var dfd = $q.defer();
		var tableData = {};
		tableData.keys = [];
		tableData.data = [];
		debug("table name is " + tableName);
		try {
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, 'select * from ' + tableName, [],
						function(tx, res) {
							debug("table total rows :" + res.rows.length);
							if (!!res && res.rows.length > 0) {
								tableData.keys = Object.keys(res.rows.item(0));
								debug("table column name data is " + Object.keys(res.rows.item(0)));
								//below commented code is for ng repeat
								for (var i = 0; i < res.rows.length; i++) {
									tableData.data.push(res.rows.item(i));
								}
								dfd.resolve(tableData);
							} else {
								dfd.resolve(tableData);
							}
						},
						function(tx, err) {
							dfd.resolve(null);
						}
					);
				},
				function(err) {
					dfd.resolve(null);
				}
			);
		} catch (ex) {
			dfd.resolve(tableData);
		}
		return dfd.promise;
	};

	this.executeQuery = function(query) {
		var dfd = $q.defer();
		var tableData = {};
		tableData.keys = [];
		tableData.data = [];
		tableData.queryError = "";

		try {
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, query, [],
						function(tx, res) {
							debug("table total rows :" + res.rows.length);
							if (!!res && res.rows.length > 0) {
								tableData.keys = Object.keys(res.rows.item(0));
								debug("table column name data is " + Object.keys(res.rows.item(0)));
								for (var i = 0; i < res.rows.length; i++) {
									tableData.data.push(res.rows.item(i));
								}
								dfd.resolve(tableData);
							} else {
								dfd.resolve(tableData);
							}
						},
						function(tx, err) {
							tableData.queryError = err.message;
							dfd.resolve(tableData);
						}
					);
				},
				function(err) {
					dfd.resolve(null);
				}
			);
		} catch (ex) {
			dfd.resolve(tableData);
		}
		return dfd.promise;
	};
}]);
