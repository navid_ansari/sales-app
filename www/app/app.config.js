app.config(['panelsProvider', function (panelsProvider) {
	panelsProvider.add({
		id: 'overlayMenu',
		position: 'left',
		size: '350px',
		templateUrl: 'overlayMenu/overlayMenu.html',
		controller: 'OverlayMenuCtrl as oc'
	});
}]);

app.config(['ChartJsProvider', function (ChartJsProvider) {
	// Configure all charts
	ChartJsProvider.setOptions({
		responsive: true
	});
}]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	"use strict";
	if (IS_DEBUG)
		debug = console.log.bind(window.console);
	else
		debug = function(){};

    //Sneha 27-12-16
    if(ionic.Platform.isIOS()) {
        debug("iOS Platform");
        db = window.sqlitePlugin.openDatabase({name: "SA_DB_MAIN.db", key:'life', iosDatabaseLocation: 'Documents'});
        sisDB = window.sqlitePlugin.openDatabase({name: "SA_DB_SIS.db", key:'life', iosDatabaseLocation: 'Documents'});
    } else {
        debug("android Platform");
	db = window.sqlitePlugin.openDatabase({name: "SA_DB_MAIN.db", key:'life', location: 'default'});
	sisDB = window.sqlitePlugin.openDatabase({name: "SA_DB_SIS.db", key:'life', location: 'default'});
    }

	cordova.plugins.Keyboard.disableScroll(true);

	$stateProvider

	.state("login", {
		url: "/login",
		templateUrl: "login/login.html",
		controller: "LoginCtrl as login",
		cache: false,
		resolve: {
			UserData: ["CommonService", function(CommonService){
				debug("resolving UserData");
				return CommonService.getUserDataFromDb().then(
					function(userData){
						debug("userData: " + userData);
						return userData;
					}
				);
			}]
		}
	})

	.state("forgotPassword", {
		url: "/forgotPassword",
		templateUrl: "passwordManagement/forgotPassword/forgotPassword.html",
		controller: "ForgotPasswordCtrl as fp",
		cache: false,
		params: {SecQuesData: null},
		resolve: {
			SecQuesData: ['$stateParams',
				function($stateParams){
					return $stateParams.SecQuesData;
				}
			]
		}
	})

	.state("ftlSetPwd", {
		url: "/ftlSetPwd",
		templateUrl: "passwordManagement/ftlSetPwd/ftlSetPwd.html",
		controller: "FtlSetPwdCtrl as fspc",
		cache: false,
		resolve: {
			SecurityQuestions: ['FtlSetPwdService',
				function(FtlSetPwdService){
					debug("resolving SecurityQuestions");
					return FtlSetPwdService.getSecurityQuestions();
				}
			]
		}
	})

	.state("changePassword", {
		url: "/changePassword",
		cache: false,
		templateUrl: "passwordManagement/changePassword/changePassword.html",
		controller: "ChangePasswordCtrl as cp"
	})

	.state("chgSecQues", {
		url: "/chgSecQues",
		cache: false,
		templateUrl: "passwordManagement/chgSecQues/chgSecQues.html",
		controller: "ChangeSecQuesCtrl as csq",
		resolve:{
			QuestionData: ['ChangeSecQuesService',
				function(ChangeSecQuesService){
					return ChangeSecQuesService.getQuestionData();
				}
			]
		}
	})

	.state("dashboard.downloads",{
        url: "/downloads",
        cache: false,
        views:{
            'dashboard-content': {
                templateUrl: "optionalDownloads/optionalDownloads.html",
                controller: "OptionalDownloadsCtrl as op",
                resolve: {
                    ActiveTab: ['DashboardService',
                        function(DashboardService){
                            DashboardService.setActiveTab("downloads");
                            return "downloads";
                        }
                    ],
                    OptionalDownloadsList: ['OptionalDownloadsService', 'LoginService',
                        function(OptionalDownloadsService, LoginService){
                            debug("resolving getOptionalDocuments()");
                            return OptionalDownloadsService.getOptionalDocuments(LoginService.lgnSrvObj.userinfo.AGENT_CD, LoginService.lgnSrvObj.password, LoginService.lgnSrvObj.userinfo.DEVICE_ID, BUSINESS_TYPE).then(
                                function(res){
                                    debug("res: " + res);
                                    return res;
                                }
                            );
                        }
                    ]
                }
            }
        }
    })
    .state("toolkit",{
        url: "/toolkit",
        cache: false,
        templateUrl: "toolkit/toolkit.html",
        controller: "ToolkitCtrl as tc",
        params: {ToolkitList: null},
        resolve: {
            ToolkitList: ['$stateParams',
                function($stateParams){
                    debug("resolving ToolkitList");
                    return $stateParams.ToolkitList;
                }
            ]
        }
    })
	.state("disclamier",{
         url:"/disclamier",
         templateUrl: "disclaimer/disclaimer.html",
         controller:"DisclaimerCtrl as dc",
		 resolve: {
			 ProductList: ['DisclaimerService', 'LoginService',
			 	function(DisclaimerService, LoginService){
					return DisclaimerService.getProductList(LoginService.lgnSrvObj.username);
				}
			]
		 }
    })
	.state("dashboard", {
		url: "/dashboard",
		abstract: true,
		templateUrl: "dashboard/dashboard.html",
		controller: "DashboardCtrl as db"
	})
	.state("dashboard.home",{
		url: "/home",
		cache: false,
		views:{
			'dashboard-content': {
				templateUrl: "dashboard/home/home.html",
				controller: "HomeCtrl as hc",
				resolve: {
					ActiveTab: ['DashboardService',
						function(DashboardService){
							DashboardService.setActiveTab("home");
							return "home";
						}
					],
					TopBannerCounts: ['HomeService', 'LoginService',
						function(HomeService, LoginService){
							debug("resolving TopBannerCounts");
							return HomeService.getTopBannerCounts(LoginService.lgnSrvObj.userinfo.AGENT_CD);
						}
					],
					BottomBannerCounts: ['HomeService',
						function(HomeService){
							debug("resolving BottomBannerCounts");
							return HomeService.getBottomBannerCounts();
						}
					],
					ChartCounts: ['HomeService',
						function(HomeService){
							debug("resolving ChartCounts");
							return HomeService.getChartCounts('daily');
						}
					]
				}
			}
		}
	})

	.state("dashboard.appTracker",{
        url: "/appTracker",
        abstract :true,
        views:{
            'dashboard-content': {
                templateUrl: "dashboard/appTracker/appTracker.html",
                controller: "AppTrackerCtrl as atc",
                resolve:{
                    ActiveTab: ['DashboardService',
                        function(DashboardService){
                            DashboardService.setActiveTab("appTracker");
                            return "appTracker";
                        }
                    ],
                    AppTrackData : ['AppTrackerService',
                        function(AppTrackerService){
                            debug("resolving getAppTrackData");
                            return AppTrackerService.getAppTrackData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.appTracker.oppNotInitiated",{
        url: "/oppNotInitiated",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/oppNotInitiated/oppNotInitiated.html",
                controller: "OppNotInitiatedCtrl as oic",
                resolve:{
                    OppNotInitiatedArr : ['OppNotInitiatedService',
                        function(OppNotInitiatedService){
                            debug("resolving getOppNotInitiatedData");
                            return OppNotInitiatedService.getOppNotInitiatedData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.appTracker.fhrInProgress",{
        url: "/fhrInProgress",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/fhrInProgress/fhrInProgress.html",
                controller: "FHRInProgressCtrl as fpc",
                resolve:{
                    FHRInProgressArr : ['FHRInProgressService',
                        function(FHRInProgressService){
                            debug("resolving getFHRInProgressData");
                            return FHRInProgressService.getFHRInProgressData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.appTracker.fhrCompleted",{
        url: "/fhrCompleted",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/fhrCompleted/fhrCompleted.html",
                controller: "FHRCompletedCtrl as fc",
                resolve:{
                    FHRCompletedArr : ['FHRCompletedService',
                        function(FHRCompletedService){
							debug("resolving getFHRCompletedData");
							return FHRCompletedService.getFHRCompletedData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.appTracker.sisInProgress",{
        url: "/sisInProgress",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/sisInProgress/sisInProgress.html",
                controller: "SisInProgressCtrl as sc",
                resolve:{
                    SisInProgressArr : ['SisInProgressService',
                        function(SisInProgressService){
                            debug("resolving getSisInProgeressData");
                            return SisInProgressService.getSisInProgeressData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.appTracker.sisCompleted",{
        url: "/sisCompleted",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/sisCompleted/sisCompleted.html",
                controller: "SisCompletedCtrl as sc",
                resolve:{
                    SisCompletedArr : ['SisCompletedService',
                        function(SisCompletedService){
                            debug("resolving getSisCompletedData");
                            return SisCompletedService.getSisCompletedData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.appTracker.appInProgress",{
        url: "/appInProgress",
        views:{
            'app-trk-tab': {
                templateUrl: "dashboard/appTracker/appInProgress/appInProgress.html",
                controller: "AppInProgressCtrl as apc",
                resolve:{
                    AppInProgressArr: ['AppInProgressService',
                        function(AppInProgressService){
                            debug("resolving getAppInProgressData");
                            return AppInProgressService.getAppInProgressData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.policyTracker",{
        url: "/policyTracker",
        abstract :true,
        views:{
            'dashboard-content': {
                templateUrl: "dashboard/policyTracker/policyTracker.html",
                controller: "PolicyTrackerCtrl as ptC",
                resolve:{
                    ActiveTab: ['DashboardService',
                        function(DashboardService){
                            DashboardService.setActiveTab("policyTracker");
                            return "policyTracker";
                        }
                    ],
                    POLICYTRACKDATA : ['PolicyTrackerService',
                        function(PolicyTrackerService){
                            debug("resolving getPolicyTrackData");
                            return PolicyTrackerService.getPolicyTrackData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.policyTracker.appSubmitted",{
        cache: false,
        url: "/appSubmitted",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/appSubmitted/appSubmitted.html",
                controller: "AppSubmittedCtrl as asc",
                resolve:{
                    appSubmittedArr : ['AppSubmittedService',
                        function(AppSubmittedService){
                            debug("resolving getAppSubmittedData");
                            return AppSubmittedService.getAppSubmittedData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.policyTracker.preUW",{
        cache: false,
        url: "/preUW",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/preUW/preUW.html",
                controller: "PreUWCtrl as puwc",
                resolve:{
                    preUWDataArr : ['PreUWService',
                        function(PreUWService){
                            debug("resolving getPreUWData");
                            return PreUWService.getPreUWData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.policyTracker.uwPending",{
         cache: false,
        url: "/uwPending",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/uwPending/uwPending.html",
                controller: "UWPendingCtrl as uwpc",
                resolve:{
                    uwPendingArr : ['UWPendingService',
                        function(UWPendingService){
                            debug("resolving getUWPendingData");
                            return UWPendingService.getUWPendingData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.policyTracker.issuedPolicy",{
        url: "/issuedPolicy",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/issuedPolicy/issuedPolicy.html",
                controller: "IssuedPolCtrl as ipc",
                resolve:{
                    issuedPolicyArr : ['IssuedPolService',
                        function(IssuedPolService){
                            debug("resolving getIssuedPolicyData");
                            return IssuedPolService.getIssuedPolicyData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.policyTracker.declinedPolicy",{
        url: "/declinedPolicy",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/declinedPolicy/declinedPolicy.html",
                controller: "DeclinedPolCtrl as dpc",
                resolve:{
                    declinedPolicyArr : ['DeclinedPolService',
                        function(DeclinedPolService){
                            debug("resolving getDeclinedPolicyData");
                            return DeclinedPolService.getDeclinedPolicyData();
                        }
                    ]
                }
            }
        }
    })
    .state("dashboard.policyTracker.closedPolicy",{
        url: "/closedPolicy",
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/closedPolicy/closedPolicy.html",
                controller: "ClosedPolCtrl as cpc",
                resolve:{
                    closedPolicyArr : ['ClosedPolService',
                        function(ClosedPolService){
                            debug("resolving getClosedPolicyData");
                            return ClosedPolService.getClosedPolicyData();
                        }
                    ]
                }
            }
        }
    })

    .state("dashboard.policyTracker.viewPending",{
        url: "/viewPending",
        params: {"POLICY_NO": null,"POLICY_STATUS":null},
        views:{
            'policy-tracker-content': {
                templateUrl: "dashboard/policyTracker/viewPending/viewPending.html",
                controller: "ViewPendingCtrl as vpc",
                resolve:{
                POLICY_STATUS: ['$stateParams', function($stateParams) {
                           console.log("test   "+$stateParams.POLICY_STATUS);
                          return $stateParams.POLICY_STATUS;
                      }],
                    pendingList : ['$stateParams','ViewPendingService',
                        function($stateParams,ViewPendingService){
                            debug("resolving getPendingList");
                            return ViewPendingService.getPendingList($stateParams.POLICY_NO);
                        }
                    ],
                    pendingL1List : ['$stateParams','ViewPendingService',
                        function($stateParams,ViewPendingService){
                            debug("resolving getPendingList");
                            return ViewPendingService.getPendingL1List($stateParams.POLICY_NO);
                        }
                    ],
                    appSubmittedArr : ['AppSubmittedService',
                        function(AppSubmittedService){
                            debug("resolving getAppSubmittedData");
                            return AppSubmittedService.getAppSubmittedData();
                        }
                    ]
                }
            }
        }
    })
	.state("dashboard.solutions",{
		url: "/solutions",
		params: {"FHR_ID": null, "LEAD_ID": null, "OPP_ID": null},
		views:{
			'dashboard-content': {
				templateUrl: "sis/solutions/solutions.html",
				controller: "SolutionsCtrl as sc",
				resolve: {
					ActiveTab: ['DashboardService',
						function(DashboardService){
							DashboardService.setActiveTab("solutions");
							return "solutions";
						}
					],
					AllowedProdsList: ['SolutionsService', 'LoginService',
						function(SolutionsService, LoginService){
							debug('resolving getListOfProductsAllowed()');
							return SolutionsService.getListOfProductsAllowed(LoginService.lgnSrvObj.userinfo.AGENT_CD);
						}
					],
					ProductList:['SolutionsService', 'LoginService',
						function(SolutionsService, LoginService){
							return SolutionsService.getProductList(LoginService.lgnSrvObj.userinfo.AGENT_CD);
						}
					],
					FHR_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.FHR_ID;
						}
					],
					LEAD_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.LEAD_ID;
						}
					],
					OPP_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.OPP_ID;
						}
					]
				}
			}
		}
	})

	.state("dashboard.productLandingPage",{
		url: "/productLandingPage",
		params: {"PROD_NAME": null, "SOL_TYPE": null, "PGL_ID": null, "FHR_ID": null, "LEAD_ID": null, "OPP_ID": null},
		views:{
			'dashboard-content': {
				templateUrl: "sis/productLandingPage/productLandingPage.html",
				controller: "ProductLandingPageCtrl as pc",
				resolve: {
					ProductDetails: ['$stateParams',
						function($stateParams){
							return {"solutionType": $stateParams.SOL_TYPE, "productName": $stateParams.PROD_NAME, "PGL_ID": $stateParams.PGL_ID};
						}
					],
					FHR_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.FHR_ID;
						}
					],
					LEAD_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.LEAD_ID;
						}
					],
					OPP_ID: ['$stateParams',
						function($stateParams){
							return $stateParams.OPP_ID;
						}
					]
				}
			}
		}
	})

	.state("dashboard.savedSISListing",{
		url: "/savedSISListing",
		params: {"PGL_ID": null, "SIS_LISTING": null},
		views:{
			'dashboard-content': {
				templateUrl: "sis/productLandingPage/savedSISListing/savedSISListing.html",
				controller: "SavedSISListingCtrl as sl",
				resolve: {
					SavedSISList: ['$stateParams',
						function($stateParams){
							return $stateParams.SIS_LISTING;
						}
					]
				}
			}
		}
	})

	.state("viewReport",{
		url: "/viewReport",
		cache: false,
		templateUrl: "viewReport/viewReport.html",
		controller: "ViewReportCtrl as vr",
		params: {"REPORT_TYPE": null, "REPORT_ID": null, "PREVIOUS_STATE": null, "PREVIOUS_STATE_PARAMS": null, "REPORT_PARAMS": null, "SAVED_REPORT": null},
		resolve: {
			ReportType: ['$stateParams',
				function($stateParams){
					return $stateParams.REPORT_TYPE;
				}
			],
			ReportID: ['$stateParams',
				function($stateParams){
					return $stateParams.REPORT_ID;
				}
			],
			PreviousState: ['$stateParams',
				function($stateParams){
					return $stateParams.PREVIOUS_STATE;
				}
			],
			PreviousStateParams: ['$stateParams',
				function($stateParams){
					return $stateParams.PREVIOUS_STATE_PARAMS;
				}
			],
			ReportParams: ['$stateParams',
				function($stateParams){
				debug("CONFIG ReportParams::"+JSON.stringify($stateParams.REPORT_PARAMS));
					return $stateParams.REPORT_PARAMS;
				}
			],
			SavedReport:['$stateParams',
				function($stateParams){
					return $stateParams.SAVED_REPORT;
				}
			]
		}
	})

	.state("sisForm", {
		url: "/sisForm",
		abstract: true,
		templateUrl: "sis/sisForm/sisForm.html",
		controller: "SisFormCtrl as sc"
	})

	.state("sisForm.sisVernacular", {
		url: "/sisVernacular",
		cache: false,
		params: {"isTermAttached": null},
		views: {
			'sisform-content': {
				templateUrl: "sis/sisForm/sisVernacular/sisVernacular.html",
				controller: "SisVernacularCtrl as sc",
				resolve: {
					IsTermAttached: ['$stateParams', function($stateParams){
						return $stateParams.isTermAttached;
					}],
					IDProofList: ['LoadAppProofList', function(LoadAppProofList){
                        console.log("resolve getIDProofList");
                            return LoadAppProofList.getIDProofList();
                }]
				}
			}
		}
	})

	/*.state("sisForm.sisEKYCConfirm", {
		url: "/sisEKYCConfirm",
		cache: false,
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisForm0/sisEKYCConfirm/sisEKYCConfirm.html",
				controller: "SisEKycConfirmCtrl as sc"
			}
		}
	})*/

	.state("sisForm.sisEKYC", {
		url: "/sisEKYC",
		params: {"AGENT_CD": null, "LEAD_ID": null, "FHR_ID": null, "SIS_ID": null, "COMBO_ID": null, "PGL_ID": null, "INS_SEQ_NO": null, "SOL_TYPE": null, "PROD_NAME": null, "OPP_ID": null},
		cache: false,
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisForm0/sisEKYC/sisEKYC.html",
				controller: "SisEKycCtrl as sc",
				resolve: {
					ProductData: ['$stateParams', 'SisFormService',
						function($stateParams, SisFormService){
							if(!!$stateParams.PGL_ID)
								return SisFormService.loadProductData($stateParams.PGL_ID);
							else
								return null;
						}
					],
					ExistingData: ['SisFormService', 'SisEKycService', '$stateParams',
						function(SisFormService, SisEKycService, $stateParams){
							debug("resolving ExistingData $stateParams: " + JSON.stringify($stateParams));
							try{
								if(!!$stateParams.SOL_TYPE && !!$stateParams.PROD_NAME)
									SisFormService.setExtraData($stateParams.SOL_TYPE, $stateParams.PROD_NAME);
								return SisFormService.loadSISData($stateParams.AGENT_CD, $stateParams.PGL_ID, $stateParams.LEAD_ID, $stateParams.FHR_ID, $stateParams.SIS_ID, $stateParams.COMBO_ID, $stateParams.INS_SEQ_NO, $stateParams.OPP_ID);
							}catch(ex){
								debug("Exception: " + ex.message);
								return null;
							}
						}
					],
					SisFormAData_eKyc: ['SisFormService', 'SisEKycService','ExistingData',
						function(SisFormService, SisEKycService, ExistingData){
							debug("in here SisFormAData_eKyc " + JSON.stringify(ExistingData));
							var sisEKycData = SisEKycService.getSisFormAData_eKyc(SisFormService.sisData.sisFormAData);
							SisEKycService.sisEKycData = JSON.parse(JSON.stringify(SisFormService.sisData.sisEKycData));
							return (sisEKycData || null);
						}
					],
					PurchasingInsuranceForList: ['SisEKycService',
						function(SisEKycService){
							debug("resolving getPurchasingInsuranceForList");
							return SisEKycService.getPurchasingInsuranceForList();
						}
					],
					InsuranceBuyingForList: ['SisEKycService','PurchasingInsuranceForList','SisFormAData_eKyc',
						function(SisEKycService, PurchasingInsuranceForList, SisFormAData_eKyc){
							debug("resolving getInsuranceBuyingForList " + SisFormAData_eKyc.PURCHASE_FOR);
							return SisEKycService.getInsuranceBuyingForList(SisFormAData_eKyc.PURCHASE_FOR || PurchasingInsuranceForList[0]);
						}
					],
					loadEKYCOpportunityData:['SisFormService', 'SisEKycService', '$stateParams',
						function(SisFormService, SisEKycService, $stateParams){
							debug("resolving loadEKYCOpportunityData $stateParams: " + JSON.stringify($stateParams));
							try{
								if(!!SisFormService.sisData.OPP_ID)
									return SisEKycService.loadEKYCOpportunityData(SisFormService.sisData.OPP_ID, SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.AGENT_CD);
								else
										return null;
							}catch(ex){
								debug("loadEKYCOpportunityData Exception: " + ex.message);
								return null;
							}
						}
					],
					EKYCData: ['SisFormService', '$stateParams',
						function(SisFormService, $stateParams){
							return SisFormService.getEKYCData(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID);
						}
					]

				}
			}
		}
	})

	.state("sisForm.sisOutput", {
		url: "/sisOutput",
		cache: false,
		params: {"BASE_OR_TERM": null, "DONT_REGEN": null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisOutput/sisOutput.html",
				controller: "SisOutputCtrl as sc",
				resolve: {
					SisGenOutput: ['$stateParams', 'SisOutputService', 'SisFormService',
						function($stateParams, SisOutputService, SisFormService){

							/*		SINGLE SIS COMMENTED - Akhil - 22 Dec 2016

								var COMBO_ID =  SisFormService.sisData.oppData.COMBO_ID;
								debug("Combo id is: " + COMBO_ID);
								if(!!COMBO_ID){
									debug("$stateParams.BASE_OR_TERM= "+$stateParams.BASE_OR_TERM);
									if($stateParams.BASE_OR_TERM=="BASE"){
										var sisObject =  SisOutputService.getSISDataArray();
										var res = sisObject.Base;
										var test;
										SisOutputService.setSISDataForComboPlan(test,res);
										return res;
									}else if($stateParams.BASE_OR_TERM=="TERM"){
										var sisObject =  SisOutputService.getSISDataArray();
										var res = sisObject.Term;
										SisOutputService.setSISDataForComboPlan(true,res);
										return res;
									}else{
										SisOutputService.isTermOutput = null;
										return SisOutputService.generateSISComboPlan();
									}
								}

							*/

							if(!!$stateParams.BASE_OR_TERM && $stateParams.BASE_OR_TERM=="TERM"){
								// COMMENT THIS ENTIRE IF BLOCK WHEN ENABLING SINGLE SIS
								debug("term sis generation");
								var sisData = SisFormService.sisData.sisTermOutput.SIS;
								var sisOutput = SisFormService.sisData.sisTermOutput;
								if(!$stateParams.DONT_REGEN){
									debug("resolving generateSIS()...term");
									sisOutput.isTermOutput = true;
									return SisOutputService.generateSIS(true);
								}
								else{
									debug("resolving sisReport...term");
									sisOutput.isTermOutput = true;
									return sisOutput;
								}
							}
							else{
								// DO NOT COMMENT THIS BLOCK WHEN ENABLING SINGLE SIS - only comment the setCurrentPageIndex("BASE") below (entire if block)...
								var sisData = SisFormService.sisData.sisOutput.SIS;
								var sisOutput = SisFormService.sisData.sisOutput;
								SisOutputService.setSISDataArray(null);
								SisOutputService.setCurrentPageIndex(null);
								SisOutputService.setIsSingleSIS(null);
								if(!$stateParams.DONT_REGEN){
									debug("resolving generateSIS()");
									return SisOutputService.generateSIS();
								}
								else{
									debug("resolving sisReport");
									return sisOutput;
								}
							}
						}
					]
				}
			}
		}
	})
	.state("sisForm.sisFormF", {
		url: "/sisFormF",
		cache: false,
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormF/sisFormF.html",
				controller: "SisFormFCtrl as sc",
				resolve: {
					SisFormFData: ['SisFormService', 'SisFormFService',
						function(SisFormService, SisFormFService){
							debug("test : " + JSON.stringify(SisFormService.sisData));
							SisFormFService.sisFormFData = SisFormService.sisData.sisFormFData || {};
							return (SisFormFService.sisFormFData || null);
						}
					],
					ResidentCountryList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getResidentCountryList");
							return SisFormFService.getResidentCountryList();
						}
					],
					EduQualList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getEduQualList");
							return SisFormFService.getEduQualList();
						}
					],
					StateList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getStateList");
							return SisFormFService.getStateList();
						}
					],
					CityList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getCityList");
							return SisFormFService.getCityList(SisFormFService.sisFormFData.CURR_STATE_CODE || 0);
						}
					],
					/*CityList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getCityList");
							return SisFormFService.getCityList();
						}
					],*/
					OccClassList: ['SisFormFService',
						function(SisFormFService){
							debug("resolving getOccClassList");
							return SisFormFService.getOccClassList();
						}
					]
				}
			}
		}
	})
	.state("sisForm.sfePage3", {
		url: "/sisFormE_Page3",
		cache: false,
		params: {"PGL_ID": null, "IS_TERM_CHANGED": null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormE/sfePage3/sfePage3.html",
				controller: "SfePage3Ctrl as sc",
				resolve: {
					PGL_ID: ['$stateParams',
						function($stateParams){
						    debug("$stateParams.PGL_ID : " + $stateParams.PGL_ID);
							return $stateParams.PGL_ID;
						}
					],
					ProductData: ['$stateParams', 'SisFormService',
						function($stateParams, SisFormService){
							if(!!$stateParams.PGL_ID)
								return SisFormService.loadProductData($stateParams.PGL_ID, true).then(
									function(res){
										debug("RESULT: " + JSON.stringify(res));
										return res;
									}
								);
							else
								return null;
						}
					],
					SfePage3Data: ['SisFormService', 'SisFormEService', 'SfePage3Service','$stateParams',
						function(SisFormService, SisFormEService, SfePage3Service,$stateParams){
							var sfePage3Data = SisFormEService.getSfePage3Data(SisFormService.sisData.sisFormEData,$stateParams.IS_TERM_CHANGED);
							SfePage3Service.setSfePage3Data(sfePage3Data);
							return (sfePage3Data || null);
						}
					],
					PlanCodeList: ['$stateParams', 'SisFormService', 'ProductData',
						function($stateParams, SisFormService, ProductData){
							// ProductData is dependency injected as we cannot getPlanCodeList without loading product data
							debug("resolving PlanCodeList");
							return SisFormService.getPlanCodeList($stateParams.PGL_ID, true);
						}
					],
					PlanTermDetails: ['SisFormService', 'ProductData',
						function(SisFormService, ProductData){
							debug("resolving PlanTermDetails");
							return SisFormService.getProductDetails(true).LP_PNL_TERM_PPT_LK;
						}
					],
					PPTList: ['$stateParams', 'SisFormService', 'ProductData',
						function($stateParams, SisFormService, ProductData){
							debug("resolving PPTList");
							return SisFormService.getPPTList($stateParams.PGL_ID);
						}
					],
					PPMList: ['SisFormService', 'ProductData',
						function(SisFormService, ProductData){
							debug("resolving PPMList");
							return SisFormService.getPPMList(true);
						}
					],
					SPList: ['SisFormService', 'ProductData',
						function(SisFormService, ProductData){
							debug("resolving SPList");
							return SisFormService.getSPList(true);
						}
					],
					PolTermLimits: ['$stateParams', 'SisFormService', 'SfePage3Service', 'PlanTermDetails', 'PlanCodeList', 'ProductData',
						function($stateParams, SisFormService, SfePage3Service, PlanTermDetails, PlanCodeList, ProductData){
							debug("resolving PolTermLimits");
							var termLimits = SisFormService.getPolTermLimitDetails(PlanTermDetails, PlanCodeList, $stateParams.PGL_ID, true);
							if(!!termLimits){
								SfePage3Service.minPolTerm = termLimits.MIN || null;
								SfePage3Service.maxPolTerm = termLimits.MAX || null;
							}
							else{
								SfePage3Service.minPolTerm = null;
								SfePage3Service.maxPolTerm = null;
							}
							return (termLimits || null);
						}
					],
					PlanName: ['SisFormService', 'ProductData',
						function(SisFormService, ProductData){
							debug("resolving PlanName");
							return SisFormService.getPlanName(true);
						}
					],
					OptionList: ['$stateParams', 'SisFormService', 'ProductData',
						function($stateParams, SisFormService, ProductData){
							debug("resolving OptionList: ");
							return SisFormService.getOptionList($stateParams.PGL_ID, true);
						}
					],
					PremiumMultipleList: ['$stateParams', 'SisFormService', 'SfePage3Data','PlanCodeList',
						function($stateParams, SisFormService, SfePage3Data, PlanCodeList){
							debug("resolving getPremiumMultipleList");
							return SisFormService.getPremiumMultipleList($stateParams.PGL_ID, true, (SfePage3Data.POLICY_TERM || null), (PlanCodeList||null), (SfePage3Data.PLAN_CODE||null), (SfePage3Data.SUM_ASSURED || null));
						}
					],
					MinimumSumAssured: ['$stateParams', 'SisFormService', 'SfePage3Service', 'SfePage3Data', 'PlanCodeList',
						function($stateParams, SisFormService, SfePage3Service, SfePage3Data, PlanCodeList){
							debug("resolving getMinSumAssured");
							SfePage3Service.minSumAssured = SisFormService.getMinSumAssured($stateParams.PGL_ID, true, PlanCodeList, SfePage3Data.PLAN_CODE || null);
							return SfePage3Service.minSumAssured;
						}
					],
					MaximumSumAssured: ['$stateParams', 'SisFormService', 'SfePage3Service', 'SfePage3Data', 'PlanCodeList',
						function($stateParams, SisFormService, SfePage3Service, SfePage3Data, PlanCodeList){
							debug("resolving getMaxSumAssured");
							SfePage3Service.maxSumAssured = SisFormService.getMaxSumAssured($stateParams.PGL_ID, true, PlanCodeList, SfePage3Data.PLAN_CODE || null);
							return SfePage3Service.maxSumAssured;
						}
					],
					MinimumPremium: ['$stateParams', 'SisFormService', 'SfePage3Service', 'SfePage3Data', 'PlanCodeList',
						function($stateParams, SisFormService, SfePage3Service, SfePage3Data, PlanCodeList){
							debug("resolving: getMinPremium");
							var startingPoint = SisFormService.getProductDetails().LP_PNL_PLAN_LK[0].PNL_START_PT;
							SfePage3Service.minPremium = SisFormService.getMinPremium($stateParams.PGL_ID, true, PlanCodeList, SfePage3Data.PLAN_CODE || null, SfePage3Data.INVT_STRAT_FLAG || startingPoint);
							return SfePage3Service.minPremium;
						}
					],
					MaximumPremium: ['SfePage3Service', 'SisFormService', 'SfePage3Data', 'PlanCodeList',
						function(SfePage3Service, SisFormService, SfePage3Data, PlanCodeList){
							debug("resolving: getMaxPremium");
							SfePage3Service.maxPremium = SisFormService.getMaxPremium(true, PlanCodeList, SfePage3Data.PLAN_CODE || null);
							return SfePage3Service.maxPremium;
						}
					]
				}
			}
		}
	})

	.state("sisForm.sisFormD", {
		url: "/sisFormD",
		cache: false,
		params: {"RIDER_LIST": null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormD/sisFormD.html",
				controller: "SisFormDCtrl as sc",
				resolve: {
					RidersList: ['$stateParams', 'SisFormDService',
						function($stateParams, SisFormDService){
							return SisFormDService.getSavedData($stateParams.RIDER_LIST);
						}
					]
				}
			}
		}
	})
	.state("sisForm.sfePage1", {
		url: "/sisFormE_Page1",
		cache: false,
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormE/sfePage1/sfePage1.html",
				controller: "SfePage1Ctrl as sp1",
				resolve: {
					SfePage1Data: ['SisFormService', 'SisFormEService', 'SfePage1Service',
						function(SisFormService, SisFormEService, SfePage1Service){
							var sfePage1Data = SisFormEService.getSfePage1Data(SisFormService.sisData.sisFormEData);
							SfePage1Service.setSfePage1Data(sfePage1Data);
							return (sfePage1Data || null);
						}
					],
					ResidentCountryList: ['SfePage1Service',
						function(SfePage1Service){
							debug("resolving getResidentCountryList");
							return SfePage1Service.getResidentCountryList();
						}
					],
					NationalityList: ['SfePage1Service',
						function(SfePage1Service){
							debug("resolving getNationalityList");
							return SfePage1Service.getNationalityList();
						}
					],
					EduQualList: ['SfePage1Service',
						function(SfePage1Service){
							debug("resolving getEduQualList");
							return SfePage1Service.getEduQualList();
						}
					],
					StateList: ['SfePage1Service',
						function(SfePage1Service){
							debug("resolving getStateList");
							return SfePage1Service.getStateList();
						}
					],
					CityList: ['SfePage1Service', 'SisFormService',
						function(SfePage1Service, SisFormService){
							debug("resolving getCityList");
							if(SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=='Y'){
								return SfePage1Service.getCityList(SisFormService.sisData.sisEKycData.insured.STATE, true);
							}
							else{
								return SfePage1Service.getCityList(SfePage1Service.sfePage1Data.CURR_STATE_CODE || 0);
							}
						}
					],
					OccClassList: ['SfePage1Service',
						function(SfePage1Service){
							debug("resolving getOccClassList");
							return SfePage1Service.getOccClassList();
						}
					],
					FFNAData: ['SfePage1Service','SisFormService',
						function(SfePage1Service, SisFormService){
							debug("resolving getFFNATableData");

							return SfePage1Service.getFFNATableData(SisFormService.sisData.sisMainData.FHR_ID,SisFormService.sisData.sisMainData.AGENT_CD).then(
								function(ffData){
									SfePage1Service.FFNAData = ffData;
									return SfePage1Service.FFNAData;
								}
							);
						}
					]
				}
			}
		}
	})
	.state("sisForm.sfePage2", {
        url: "/sisFormE_Page2",
        cache: false,
        params: {"ML_PREM_AMT": null, "ML_COV_AMT":null},
        views:{
            'sisform-content': {
                templateUrl: "sis/sisForm/sisFormE/sfePage2/sfePage2.html",
                controller: "SfePage2Ctrl as sp2",
                resolve: {
                    SfePage2Data: ['SfePage2Service', 'SisFormService',
                        function(SfePage2Service, SisFormService){
                            debug("resolving getNMLAmount");
                            return SfePage2Service.getNMLAmount(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.sisMainData.SIS_ID);
                        }
                    ],
                    MLData: ['$stateParams','SfePage1Service',
                        function($stateParams, SfePage1Service){
                            debug("resolving MLData");
                            var mlData = {};
                            if(!!$stateParams.ML_PREM_AMT && !!$stateParams.ML_COV_AMT){
                                mlData.ML_PREM_AMT = $stateParams.ML_PREM_AMT;
                                mlData.ML_COV_AMT = $stateParams.ML_COV_AMT;
                                return mlData
                            }else{
                                return SfePage1Service.getMLCoverage().then(
                                    function(Data){
                                        mlData.ML_PREM_AMT = Data.premium;
                                        mlData.ML_COV_AMT = Data.coverage;
                                        return mlData;
                                    }
                                );
                            }
                        }
                    ]
                }
            }
        }
	})
	.state("sisForm.sfePage4", {
        url: "/sisFormE_Page4",
        cache: false,
        views:{
            'sisform-content': {
                templateUrl: "sis/sisForm/sisFormE/sfePage4/sfePage4.html",
                controller: "SfePage4Ctrl as sp4",
                resolve: {
                    SfePage4Data: ['SfePage4Service',
                        function(SfePage4Service){
                            debug("resolving getSfePage4Data");
                            return SfePage4Service.getSfePage4Data();
                        }
                    ]
                }
            }
        }
	})
	.state("sisForm.sisFormC", {
		url: "/sisFormC",
		cache: false,
		params: {"RIDER_ALLOWED": null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormC/sisFormC.html",
				controller: "SisFormCCtrl as sc",
				resolve: {
					SisFormCData: ['SisFormService', 'SisFormCService',
						function(SisFormService, SisFormCService){
							debug("in here SisFormCService " + SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE + " " + SisFormCService.sisFormCData.INVEST_OPT);
							SisFormCService.sisFormCData = SisFormService.sisData.sisFormCData || {};
							if(SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE!='A' && SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE!='O'){
								if(SisFormCService.sisFormCData.INVEST_OPT=="Smart"){
									SisFormService.sisData.sisFormCData = {};
									SisFormCService.sisFormCData = {};
								}
							}
							return (SisFormCService.sisFormCData || null);
						}
					],
					FundsList: ['SisFormCService','SisFormService',
						function(SisFormCService, SisFormService){
							debug("resolving FundsList");
							return SisFormCService.getFundsList(SisFormService.sisData.sisFormBData.PLAN_CODE).then(
								function(res){
									debug("in getFundsList then: " + JSON.stringify(res));
									return res;
								}
							);
						}
					],
					DebtFundList: ['SisFormCService', 'FundsList',
						function(SisFormCService, FundsList){
							return SisFormCService.getDebtFundList(FundsList);
						}
					],
					EquityFundList: ['SisFormCService', 'FundsList',
						function(SisFormCService, FundsList){
							return SisFormCService.getEquityFundList(FundsList);
						}
					],
					RiderAllowed: ['$stateParams',
						function($stateParams){
							debug("resolving RiderAllowed: " + $stateParams.RIDER_ALLOWED);
							return $stateParams.RIDER_ALLOWED;
						}
					]
				}
			}
		}
	})

	.state("sisForm.sisFormB", {
		url: "/sisFormB",
		cache: false,
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormB/sisFormB.html",
				controller: "SisFormBCtrl as sc",
				resolve: {
					SisFormBData: ['SisFormService', 'SisFormBService',
						function(SisFormService, SisFormBService){
							debug("in here SisFormService " + JSON.stringify(SisFormService.sisData.sisFormBData));
							SisFormBService.sisFormBData = SisFormService.sisData.sisFormBData || {};
							return (SisFormBService.sisFormBData || null);
						}
					],
					PlanCodeList: ['SisFormService', 'SisFormBService',
						function(SisFormService, SisFormBService){
							debug("resolving PlanCodeList");
							var pglId = SisFormService.sisData.sisMainData.PGL_ID;
							SisFormBService.setPglId(pglId);
							return SisFormService.getPlanCodeList(pglId);
						}
					],
					PlanTermDetails: ['SisFormService',
						function(SisFormService){
							debug("resolving PlanTermDetails");
							return SisFormService.getProductDetails().LP_PNL_TERM_PPT_LK;
						}
					],
					PPTList: ['SisFormBService', 'SisFormService',
						function(SisFormBService, SisFormService){
							debug("resolving PPTList");
							if(!!SisFormService.sisData.sisFormBData && !!SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE)
								return SisFormService.getPPTList(SisFormService.sisData.sisMainData.PGL_ID,SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE);
							else
								return SisFormService.getPPTList(SisFormService.sisData.sisMainData.PGL_ID);
						}
					],
					PPMList: ['SisFormService',
						function(SisFormService){
							debug("resolving PPMList");
							return SisFormService.getPPMList();
						}
					],
					SPList: ['SisFormService',
						function(SisFormService){
							debug("resolving SPList");
							return SisFormService.getSPList();
						}
					],
					PolTermLimits: ['SisFormService', 'SisFormBService', 'PlanTermDetails','PlanCodeList',
						function(SisFormService, SisFormBService, PlanTermDetails,PlanCodeList){
							debug("resolving PolTermLimits");
							var pglId = SisFormService.sisData.sisMainData.PGL_ID;
							var termLimits = SisFormService.getPolTermLimitDetails(PlanTermDetails, PlanCodeList, pglId);
							if(!!termLimits){
								if(!!SisFormBService.sisFormBData.PLAN_CODE){
									termLimits.MIN = SisFormService.getMinPolTerm(SisFormBService.sisFormBData.PLAN_CODE, SisFormService.sisData.sisMainData.PGL_ID);
									termLimits.MAX = SisFormService.getMaxPolTerm(SisFormBService.sisFormBData.PLAN_CODE, SisFormService.sisData.sisMainData.PGL_ID);
									SisFormBService.minPolTerm = termLimits.MIN;
									SisFormBService.maxPolTerm = termLimits.MAX;
								}
								else{
									SisFormBService.minPolTerm = termLimits.MIN || null;
									SisFormBService.maxPolTerm = termLimits.MAX || null;
								}
								debug("app config SisFormBService.minPolTerm: " + SisFormBService.minPolTerm);
								debug("app config SisFormBService.maxPolTerm: " + SisFormBService.maxPolTerm);
							}
							else{
								SisFormBService.minPolTerm = null;
								SisFormBService.maxPolTerm = null;
							}
							return (termLimits || null);
						}
					],
					PlanName: ['SisFormService',
						function(SisFormService){
							debug("resolving PlanName");
							return SisFormService.getPlanName();
						}
					],
					OptionList: ['SisFormService',
						function(SisFormService){
							debug("resolving OptionList: ");
							return SisFormService.getOptionList(SisFormService.sisData.sisMainData.PGL_ID);
						}
					],
					PremiumMultipleList: ['SisFormService', 'SisFormBService', 'PlanCodeList',
						function(SisFormService, SisFormBService, PlanCodeList){
							debug("resolving getPremiumMultipleList PGL_ID: " + SisFormService.sisData.sisMainData.PGL_ID);
							return SisFormService.getPremiumMultipleList(SisFormService.sisData.sisMainData.PGL_ID, null, (SisFormBService.sisFormBData.POLICY_TERM || null), (PlanCodeList||null), (SisFormBService.sisFormBData.PLAN_CODE||null), (SisFormBService.sisFormBData.SUM_ASSURED || null));
						}
					],
					MinimumSumAssured: ['SisFormService', 'SisFormBService', 'PlanCodeList',
						function(SisFormService, SisFormBService, PlanCodeList){
							debug("resolving getMinSumAssured");
							SisFormBService.minSumAssured = SisFormService.getMinSumAssured(SisFormService.sisData.sisMainData.PGL_ID, null, PlanCodeList, SisFormBService.sisFormBData.PLAN_CODE || null);
							return SisFormBService.minSumAssured;
						}
					],
					MaximumSumAssured: ['SisFormService', 'SisFormBService', 'PlanCodeList',
						function(SisFormService, SisFormBService, PlanCodeList){
							debug("resolving: getMaxSumAssured");
							SisFormBService.maxSumAssured = SisFormService.getMaxSumAssured(SisFormService.sisData.sisMainData.PGL_ID, null, PlanCodeList, SisFormBService.sisFormBData.PLAN_CODE || null);
							return SisFormBService.maxSumAssured;
						}
					],
					MinimumPremium:['SisFormService', 'SisFormBService', 'PlanCodeList',
						function(SisFormService, SisFormBService, PlanCodeList){
							debug("resolving: getMinPremium");
							var startingPoint = SisFormService.getProductDetails().LP_PNL_PLAN_LK[0].PNL_START_PT;
							SisFormBService.minPremium = SisFormService.getMinPremium(SisFormService.sisData.sisMainData.PGL_ID,null,PlanCodeList, SisFormBService.sisFormBData.PLAN_CODE || null, SisFormBService.sisFormBData.INVT_STRAT_FLAG || startingPoint);
							debug("SisFormBService.minPremium: " + SisFormBService.minPremium);
							return SisFormBService.minPremium;
						}
					],
					MaximumPremium:['SisFormService', 'SisFormBService', 'PlanCodeList',
						function(SisFormService, SisFormBService, PlanCodeList){
							debug("resolving: getMaxPremium");
							SisFormBService.maxPremium = SisFormService.getMaxPremium(null,PlanCodeList, SisFormBService.sisFormBData.PLAN_CODE || null);
							return SisFormBService.maxPremium;
						}
					]
				}
			}
		}
	})

	.state("sisForm.sfaPage1", {
		url: "/sisFormA_Page1",
		cache: false,
		params: {"AGENT_CD": null, "LEAD_ID": null, "FHR_ID": null, "SIS_ID": null, "COMBO_ID": null, "PGL_ID": null, "INS_SEQ_NO": null, "SOL_TYPE": null, "PROD_NAME": null,"OPP_ID":null, "EKYC_ID": null,"PR_EKYC_ID":null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormA/sfaPage1/sfaPage1.html",
				controller: "SfaPage1Ctrl as sp1",
				resolve: {
					SfaPage1Data: ['SisFormService', 'SisFormAService', 'SfaPage1Service','SisEKycService',
						function(SisFormService, SisFormAService, SfaPage1Service, SisEKycService){
							debug(SisFormService.sisData.sisMainData.LEAD_ID+"in here SisFormService " + JSON.stringify(SisFormService.sisData.sisFormAData));
							var sfaPage1Data = null;
							if((!SisFormService.sisData.sisFormAData.INSURED_FIRST_NAME || !SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME) && !!SisFormService.sisData.OPP_ID){
								return SisEKycService.loadEKYCOpportunityData(SisFormService.sisData.OPP_ID,SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.AGENT_CD).then(function(oppdata) {
									debug("oppdata :"+JSON.stringify(oppdata));
									if(!oppdata || (!oppdata.INS_EKYC_ID && !oppdata.PR_EKYC_ID))
									{
										sfaPage1Data = {};
										sfaPage1Data = SisFormAService.getSfaPage1Data(SisFormService.sisData.sisFormAData);
										SfaPage1Service.setSfaPage1Data(sfaPage1Data);
										return (sfaPage1Data || null);
									}
									else
										return SisFormAService.getEkycData(SisFormService.sisData.sisMainData.LEAD_ID ,oppdata.INS_EKYC_ID, oppdata.PR_EKYC_ID).then(
											function(sisEkycData){
												sfaPage1Data = {};
												sfaPage1Data = SisFormAService.getSfaPage1Data(SisFormService.sisData.sisFormAData);
												debug("sisEkycData: : " + JSON.stringify(sisEkycData));
												SisFormService.sisData.sisEKycData = sisEkycData;
												SfaPage1Service.setSfaPage1Data(sfaPage1Data);
												return (sfaPage1Data || null);
											}
										);
								});
							}
							else{
								sfaPage1Data = {};
								sfaPage1Data = SisFormAService.getSfaPage1Data(SisFormService.sisData.sisFormAData);
								SfaPage1Service.setSfaPage1Data(sfaPage1Data);
								return (sfaPage1Data || null);
							}
						}
					],
					IndustryList: ['SisFormAService',
						function(SisFormAService){
							debug("resolving getIndustryList");
							return SisFormAService.getIndustryList();
						}
					],
					OccupationList: ['SisFormAService','IndustryList', 'SfaPage1Data',
						function(SisFormAService, IndustryList, SfaPage1Data){
							debug("resolving getOccupationList " + SfaPage1Data.INSURED_OCCU_INDUSTRIES);
							return SisFormAService.getOccupationList(SfaPage1Data.INSURED_OCCU_INDUSTRIES || IndustryList[0].SIS_INDUSTRIES);
						}
					],
					NatureOfWorkList: ['SisFormAService','IndustryList','OccupationList','SfaPage1Data',
						function(SisFormAService, IndustryList, OccupationList,SfaPage1Data){
							debug("OccupationList: " + JSON.stringify(OccupationList));
							debug("resolving NatureOfWorkList");
							return SisFormAService.getNatureOfWorkList((SfaPage1Data.INSURED_OCCU_INDUSTRIES || IndustryList[0].SIS_INDUSTRIES), (SfaPage1Data.INSURED_OCCUPATION || OccupationList[0].SIS_OCCUPATION));
						}
					],
					AgeProofList: ['CommonService', 'SisFormService', 'SfaPage1Service',
						function(CommonService, SisFormService, SfaPage1Service){
							debug("resolving getAgeProofList");
							var noPanCard = null;
							if(!!SisFormService.sisData.sisEKycData.insured && (SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y")){
								if((SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-07")=="-1")){
									noPanCard = null;
								}
								else{
									noPanCard = true;
								}
							}
							debug("noPanCard: " + noPanCard);

							if(SisFormService.sisData.sisMainData.PGL_ID == IRS_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == ITRP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SR_PGL_ID ||
						 	SisFormService.sisData.sisMainData.PGL_ID == SRP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MRS_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == VCP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MIP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == FR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IPR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IMX_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == WMX_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == WPR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SM7_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MBP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IWP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SGP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SUA_PGL_ID){
								SfaPage1Service.compStdAgeProofList = true;
								return CommonService.getAgeProofList(INSURED_CODE, "Y", noPanCard);
							}
							else{
								SfaPage1Service.compStdAgeProofList = false;
								if(!!SisFormService.sisData.sisFormAData.INSURED_DOB){
									debug("SisFormService.sisData.sisFormAData.INSURED_DOB: " + SisFormService.sisData.sisFormAData.INSURED_DOB);
									var age = CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB);
									debug("age : " + age);
									if(age<18)
										return CommonService.getAgeProofList(INSURED_CODE, "Y", noPanCard);
									else
										return CommonService.getAgeProofList(INSURED_CODE, null, noPanCard);
								}
								else{
									return CommonService.getAgeProofList(INSURED_CODE, null, noPanCard);
								}
							}
						}
					],
					TataDiscountAvailable: ['SisFormAService', 'SisFormService',
						function(SisFormAService, SisFormService){
							debug("resolving isTataDiscountAvailable");
							return SisFormAService.isTataDiscountAvailable(SisFormService.sisData.sisMainData.PGL_ID);
						}
					],
					CompanyList: ['SisFormAService',
						function(SisFormAService){
							debug("resolving getCompanyNameList");
							return SisFormAService.getCompanyNameList("Y"); // tataFlag - "Y"
						}
					]
				}
			}
		}
	})

	.state("sisForm.sfaPage2", {
		url: "/sisFormA_Page2",
		cache: false,
		params: {"AGENT_CD": null, "LEAD_ID": null, "EKYC_ID": null,"PR_EKYC_ID":null},
		views:{
			'sisform-content': {
				templateUrl: "sis/sisForm/sisFormA/sfaPage2/sfaPage2.html",
				controller: "SfaPage2Ctrl as sp2",
				resolve: {
					SfaPage2Data: ['SisFormService', 'SisFormAService', 'SfaPage2Service',
						function(SisFormService, SisFormAService, SfaPage2Service){

							debug("in here SisFormService " + JSON.stringify(SisFormService.sisData.sisFormAData));
							var sfaPage2Data = SisFormAService.getSfaPage2Data(SisFormService.sisData.sisFormAData);
							debug("work here is almost done");
							SfaPage2Service.setSfaPage2Data(sfaPage2Data);
							debug("work here is done");
							return (sfaPage2Data || null);
						}
					],
					ConvertedAgeProof: ['SisFormService', 'SfaPage2Service',
						function(SisFormService, SfaPage2Service){
							if(SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=="Y"){
								debug("resolving getProposerAgeProof");
								return SfaPage2Service.getProposerAgeProof(SisFormService.sisData.sisFormAData.AGE_PROOF_DOC_ID);
							}
							else
								return null;
						}
					],
					IndustryList: ['SisFormAService',
						function(SisFormAService){
							debug("resolving getIndustryList");
							return SisFormAService.getIndustryList();
						}
					],
					OccupationList: ['SisFormAService','IndustryList','SfaPage2Data',
						function(SisFormAService, IndustryList, SfaPage2Data){
							debug("resolving getOccupationList");
							return SisFormAService.getOccupationList(SfaPage2Data.PROPOSER_OCCU_INDUSTRIES || IndustryList[0].SIS_INDUSTRIES);
						}
					],
					NatureOfWorkList: ['SisFormAService', 'IndustryList', 'OccupationList', 'SfaPage2Data',
						function(SisFormAService, IndustryList, OccupationList, SfaPage2Data){
							debug("resolving getNatureOfWorkList");
							return SisFormAService.getNatureOfWorkList((SfaPage2Data.PROPOSER_OCCU_INDUSTRIES || IndustryList[0].SIS_INDUSTRIES), (SfaPage2Data.PROPOSER_OCCUPATION || OccupationList[0].SIS_OCCUPATION));
						}
					],
					AgeProofList: ['CommonService', 'SisFormService', 'SfaPage2Service',
						function(CommonService, SisFormService, SfaPage2Service){
							debug("resolving getAgeProofList");
							var noPanCard = null;
							if(!!SisFormService.sisData.sisEKycData.proposer && (SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="Y")){
								if((SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-07")=="-1")){
									noPanCard = null;
								}
								else{
									noPanCard = true;
								}
							}
							debug("noPanCard: " + noPanCard);

							if(SisFormService.sisData.sisMainData.PGL_ID == IRS_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == ITRP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SR_PGL_ID ||
						 	SisFormService.sisData.sisMainData.PGL_ID == SRP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == VCP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MRS_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MIP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == FR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IPR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IMX_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == WMX_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == WPR_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SM7_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == MBP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == IWP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SGP_PGL_ID ||
							SisFormService.sisData.sisMainData.PGL_ID == SUA_PGL_ID){
								SfaPage2Service.compStdAgeProofList = true;
								return CommonService.getAgeProofList(PROPOSER_CODE, "Y", noPanCard);
							}
							else{
								if(!!SisFormService.sisData.sisFormAData.PROPOSER_DOB){
									SfaPage2Service.compStdAgeProofList = false;
									var age = CommonService.getAge(SisFormService.sisData.sisFormAData.PROPOSER_DOB);
									if(age<18)
										return CommonService.getAgeProofList(PROPOSER_CODE, "Y", noPanCard);
									else
										return CommonService.getAgeProofList(PROPOSER_CODE, null, noPanCard);
								}
								else{
									return CommonService.getAgeProofList(PROPOSER_CODE, null, noPanCard);
								}
							}
						}
					],
					TataDiscountAvailable: ['SisFormAService', 'SisFormService',
						function(SisFormAService, SisFormService){
							debug("resolving isTataDiscountAvailable");
							 return SisFormAService.isTataDiscountAvailable(SisFormService.sisData.sisMainData.PGL_ID);
						}
					],
					CompanyList: ['SisFormAService',
						function(SisFormAService){
							debug("resolving getCompanyNameList");
							return SisFormAService.getCompanyNameList("Y"); // tataFlag - "Y"
						}
					]
				}
			}
		}
	})

	.state("cpDone", {
		url: "/cpDone",
		templateUrl: "donePages/cpDone.html",
		controller: "DonePagesCtrl as dpc"
	})

   .state("ffPage1", {
		url: "/ffPage1",
		cache: false,
		templateUrl: "factFinder/ffPage1/ffPage1.html",
		controller: "ffPage1Ctrl as ff1Ctrl",
		params :{LEADID:null,FHRID :null, AGENTCD: null, OPP_ID: null},
		resolve: {
			page1Data: 	['SavePage1Data',
							function(SavePage1Data){
								return SavePage1Data.getMasterData();
							}
						],
            maxDate:['SavePage1Data',
                    function(SavePage1Data){
                        debug("app config max date: " + SavePage1Data.getCurrentDate());
                        return SavePage1Data.getCurrentDate();
                    }
                ],
            FhrMainData: ['SavePage1Data',
                            function(SavePage1Data){
                                return SavePage1Data.getFHRMainData();
                            }
                        ],
			leadData : 	['$stateParams','factFinderService',
							function($stateParams,factFinderService){
								var obj = {};
								obj.LEADID = $stateParams.LEADID;
								obj.FHRID = $stateParams.FHRID;
								obj.AGENTCD = $stateParams.AGENTCD;
								obj.OpportunityID = $stateParams.OPP_ID;
								if(!factFinderService.getFHRId()){
                                    factFinderService.setFHRData(obj.FHRID,"",obj.OpportunityID,obj.AGENTCD);
                                    factFinderService.setleadId(obj.LEADID);
                                }
								return obj;
							}
						],
                    EKYCData: ['SisFormService', '$stateParams','LoginService',
                        function(SisFormService, $stateParams, LoginService){
                            return SisFormService.getEKYCData(LoginService.lgnSrvObj.userinfo.AGENT_CD, $stateParams.LEADID);
                        }
                    ]
		}
	})

	.state("ffPage2", {
		url: "/ffPage2",
		cache: false,
		templateUrl: "factFinder/ffPage2/ffPage2.html",
		controller: "ffPage2Ctrl as ff2Ctrl"
	})



	.state("ffPage3", {
		url: "/ffPage3",
		cache: false,
		templateUrl: "factFinder/ffPage3/ffPage3.html",
		controller: "ffPage3Ctrl as ff3Ctrl"
	})


	.state("ffPage4", {
		url: "/ffPage4",
		cache: false,
		templateUrl: "factFinder/ffPage4/ffPage4.html",
		controller: "ffPage4Ctrl as ff4Ctrl",
		resolve: {
			page2Data: ['SavePage4Data',
				function(SavePage4Data){
					return SavePage4Data.LPProductMaster();
				}
			],
			page3Data: ['SavePage4Data',
				function(SavePage4Data){
					return SavePage4Data.LPFundMaster();
				}
			]
		}
	})

	.state("ffPage5", {
		url: "/ffPage5",
		cache: false,
		templateUrl: "factFinder/ffPage5/ffPage5.html",
		controller: "ffPage5Ctrl as ff5Ctrl"
	})

	.state("ffPageOutput", {
		url: "/ffPageOutput",
		cache: false,
		templateUrl: "factFinder/ffPageOutput/ffPageOutput.html",
		controller: "ffPageOutputCtrl as ffOutputCtrl",
		resolve: {
		 docId: ['SavePageOutputData',
                        function(SavePageOutputData){
                            return SavePageOutputData.getDocId();
                        }
                    ],
//         saveData: ['SavePageOutputData',
//                         function(SavePageOutputData){
//                             return SavePageOutputData.saveData();
//                         }
//                     ],
		 FFOutputFile:['SavePageOutputData','factFinderService',
				function(SavePageOutputData,factFinderService){
					if(!factFinderService.ffData)
						return SavePageOutputData.generateFFOutput();
					else
						return factFinderService.ffData;
				}
		]
		}
	})

	.state("leadDashboard", {
		url: "/leadDashboard",
		templateUrl: "lead/leadDashboard/leadDashboard.html",
		cache: false,
		controller: "LeadDashboardCtrl as ldc",
		params:{"LEAD_ID": null, "SIS_FHR_ID": null, "FHR_ID": null, "SIS_ID": null, "APP_ID": null, "PGL_ID": null, "OPP_ID": null, 'POLICY_NO': null,'COMBO_ID':null},
		resolve:{
			Params:['$stateParams',
				function($stateParams){
				    debug("inside first resolved"+JSON.stringify($stateParams));
					return {"LEAD_ID": $stateParams.LEAD_ID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID": $stateParams.SIS_ID, "APP_ID": $stateParams.APP_ID, "PGL_ID": $stateParams.PGL_ID,"OPP_ID":$stateParams.OPP_ID,"COMBO_ID":$stateParams.COMBO_ID};
				}
			],
			LeadDetails: ['$stateParams','LeadDashboardService', 'LoginService',
				function($stateParams, LeadDashboardService, LoginService){
					debug("resolving getLeadDetails");
					if(!!$stateParams.LEAD_ID){
						debug("LEAD_ID: " + $stateParams.LEAD_ID);
						return LeadDashboardService.getLeadDetails($stateParams.LEAD_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
							function(res){
								debug("result: " + JSON.stringify(res));
								return res;
							}
						);
					}
					else{
						debug("LEAD_ID is null");
						return null;
					}
				}
			],
			OpportunityDetails:['$stateParams', 'LeadDashboardService', 'LoginService',
				function($stateParams, LeadDashboardService, LoginService){
					debug('in OpportunityDetails');
					if(!!$stateParams.LEAD_ID && (!!$stateParams.FHR_ID)){
						debug("LEAD_ID: " + $stateParams.LEAD_ID);
						debug("FHR_ID: " + $stateParams.FHR_ID);
						return LeadDashboardService.getOpportunityDetails($stateParams.LEAD_ID, $stateParams.FHR_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
					}
					else{
						debug("LEAD_ID or FHR_ID is null");
						return null;
					}
				}
			],
			TermPlanDetails: ['$stateParams', 'OpportunityDetails', 'LeadDashboardService', 'LoginService', function($stateParams, OpportunityDetails, LeadDashboardService, LoginService){
				if(!!OpportunityDetails && !!OpportunityDetails.RECO_PRODUCT_DEVIATION_FLAG && OpportunityDetails.RECO_PRODUCT_DEVIATION_FLAG=="P"){
					debug("Term attached");
					return LeadDashboardService.getOpportunityDetails($stateParams.LEAD_ID, OpportunityDetails.REF_FHRID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
						function(res){
							return res || {};
						}
					);
				}
				else{
					debug("Not term attached");
					return null;
				}
			}],
			AppDetails: ['$stateParams', 'LeadDashboardService', 'LoginService',
				function($stateParams, LeadDashboardService, LoginService){
					if(!!$stateParams.APP_ID){
						debug("APP_ID: " + $stateParams.APP_ID);
						return LeadDashboardService.getAppDetails($stateParams.APP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
					}
					else{
						debug("APP_ID is null");
						return null;
					}
				}
			]
		}
	})

	.state("leadListing", {
		url: "/leadListing",
		templateUrl: "lead/leadListing/leadListing.html",
		controller: "LeadListingCtrl as lc",
		cache: false,
		resolve: {
			LeadsList: ['LeadListingService', 'LoginService', function(LeadListingService, LoginService){
				debug("resolving LeadsList");
				return LeadListingService.getLeadsList(LoginService.lgnSrvObj.username).then(
					function(res){
						debug("LeadListingService.getLeadsList(): " + JSON.stringify(res));
						return res;
					}
				);
			}]
		}
	})

	.state("leadPage1", {
		url: "/leadPage1",
		templateUrl: "lead/addLead/page1/page1.html",
		controller: "Page1Ctrl as pc",
		params: {"LEAD_ID": null},
		cache: false,
		resolve:{
			LeadData: ['AddLeadService', '$stateParams',
				function(AddLeadService, $stateParams){
					debug("resolving getLeadData");
					if(!!AddLeadService.leadData){
						var _leadData = JSON.parse(JSON.stringify(AddLeadService.leadData));
						var _leadNameSplitArr = _leadData.LEAD_NAME.split(" ");
						if(_leadNameSplitArr.length==1)
							_leadData.FIRST_NAME = _leadNameSplitArr[0];
						if(_leadNameSplitArr.length==2){
							_leadData.FIRST_NAME = _leadNameSplitArr[0];
							_leadData.LAST_NAME = _leadNameSplitArr[1];
						}
						else if(_leadNameSplitArr.length>2){
							_leadData.FIRST_NAME = _leadNameSplitArr[0];
							_leadData.LAST_NAME = _leadNameSplitArr[_leadNameSplitArr.length-1];
							var _middleName = "";
							for(var i=1;i<(_leadNameSplitArr.length-1); i++){
								_middleName += _leadNameSplitArr[i] + " ";
							}
							_leadData.MIDDLE_NAME = _middleName.trim();
						}

						return AddLeadService.getLeadPage1Data(_leadData);
					}
					else{
						return AddLeadService.getLeadData($stateParams.LEAD_ID).then(
							function(leadData){
								debug("resolving getLeadPage1Data");
								return AddLeadService.getLeadPage1Data(leadData);
							}
						);
					}
				}
			],
		 	PreferredContactList:['Page1Service',
				function(Page1Service){
					debug("resolving PreferredContactList");
					return Page1Service.getPreferredContactList();
				}
			],
			MaritalStatusList:['Page1Service',
				function(Page1Service){
					debug("resolving MaritalStatusList");
					return Page1Service.getMaritalStatusList();
				}
			],
			StateList: ['Page1Service',
				function(Page1Service){
					debug("resolving StateList");
					return Page1Service.getStateList();
				}
			],
			CityList:['Page1Service', 'LeadData',
				function(Page1Service, LeadData){
					debug("resolving CityList");
					var state = ((!!LeadData)?(LeadData.CURNT_STATE):null);
					return Page1Service.getCityList(state);
				}
			]
		}
	})

	.state("leadPage2", {
		url: "/leadPage2",
		templateUrl: "lead/addLead/page2/page2.html",
		controller: "Page2Ctrl as pc",
		params: {"LEAD_ID": null},
		cache: false,
		resolve:{
			LeadData: ['AddLeadService',
				function(AddLeadService){
					return AddLeadService.getLeadPage2Data(AddLeadService.leadData);
				}
			],
			LeadStatusList: ['Page2Service',
				function(Page2Service){
					debug("resolving LeadStatus");
					return Page2Service.getLeadStatusList();
				}
			],
			LeadSubStatusList: ['Page2Service', 'LeadStatusList',
				function(Page2Service, LeadStatusList){
					debug("resolving LeadSubStatus");
					if(!!LeadStatusList && !!LeadStatusList[0] && !!LeadStatusList[0].LOV_CD)
						return Page2Service.getLeadSubStatusList(LeadStatusList[0].LOV_CD);
					else
						return Page2Service.getLeadSubStatusList(null);
				}
			],
			LeadSource1List:['Page2Service',
				function(Page2Service){
					debug("resolving LeadSource1List");
					return Page2Service.getLeadSourceList("3");
				}
			],
			LeadSource2List: ['Page2Service',
				function(Page2Service){
					debug("resolving LeadSource2List");
					return Page2Service.getLeadSourceList("14");
				}
			]
		}
	})

	.state("leadPage3", {
		url: "/leadPage3",
		templateUrl: "lead/addLead/page3/page3.html",
		controller: "leadEKYCCtrl as sc",
		params: {"LEAD_ID": null},
		cache: false,
		resolve:{
			leadEKYCInsuredData: ['AddLeadService','$stateParams',
			function(AddLeadService, $stateParams){
				debug("resolving leadEKYCInsuredData");
				var leadEKYCData = AddLeadService.getLeadEKYCData($stateParams.LEAD_ID,'C01');
				debug("resolving leadEKYCData - 1"+JSON.stringify(leadEKYCData));
				AddLeadService.leadEKYCData = leadEKYCData;
				return (leadEKYCData || null);
			}
		],
			leadEKYCProposerData: ['AddLeadService','$stateParams',
			function(AddLeadService, $stateParams){
				debug("resolving leadEKYCData");
				var leadEKYCData = AddLeadService.getLeadEKYCData($stateParams.LEAD_ID,'C02');
				debug("resolving leadEKYCData - 1"+JSON.stringify(leadEKYCData));
				AddLeadService.leadEKYCData = leadEKYCData;
				return (leadEKYCData || null);
			}
		],
			PurchasingInsuranceForList: ['leadEKYCService',
				function(leadEKYCService){
					debug("resolving getPurchasingInsuranceForList");
					return leadEKYCService.getPurchasingInsuranceForList();
				}
			],
			InsuranceBuyingForList: ['leadEKYCService','PurchasingInsuranceForList'/*,'SisFormAData_eKyc'*/,
				function(leadEKYCService, PurchasingInsuranceForList/*, SisFormAData_eKyc*/){
					//debug("resolving getInsuranceBuyingForList " + leadFormAData_eKyc.PURCHASE_FOR);
					return leadEKYCService.getInsuranceBuyingForList(/*leadFormAData_eKyc.PURCHASE_FOR ||*/ PurchasingInsuranceForList[0]);
				}
			]
		}
	})

	.state("leadPage4", {
		url: "/leadPage4",
		cache: false,
		templateUrl: "lead/addLead/page4/page4.html",
		controller: "leadEKYCConfirmCtrl as ek",
		params: {"LEAD_ID": null}

	})

	.state("applicationForm",{
                    url:"/applicationForm",
            		cache: false,
                    params: {"OPP_ID":null,"APPLICATION_ID": null, "AGENT_CD": null, "SIS_ID":null, "FHR_ID":null, "LEAD_ID":null,"POLICY_NO":null,"COMBO_ID":null,"REF_SIS_ID":null,"REF_FHRID":null,"REF_OPP_ID":null,"REF_APPLICATION_ID":null},
                    abstract :true,
                   	templateUrl :"applicationForm/applicationForm.html",
                   	controller: "ApplicationCtrl as appC",
                   	resolve:{
                        flagSet :['ApplicationFormDataService','$stateParams',function(ApplicationFormDataService,$stateParams){
                                return ApplicationFormDataService.setAllHeaderFlags($stateParams.APPLICATION_ID, $stateParams.AGENT_CD).then(
                                    function(inserted){
                                        debug("<< FLAG SET HEADER COMPLETED >>>")
                                    }
                                );
                            }
                        ],
                        percent : ['ApplicationFormDataService','$stateParams',function(ApplicationFormDataService,$stateParams){
                                return ApplicationFormDataService.appCompletedPercent($stateParams.APPLICATION_ID, $stateParams.AGENT_CD).then(
                                   function(count){
                                        debug("RESOLVE COUNT "+count);
                                       //if(!!count)
                                       //{
                                            return count;
                                       //}
                                   }
                                );
                            }
                        ]
            		}

                    })

                 	.state("applicationForm.personalInfo",{
                     		url: "/personalInfo",
                     		cache: false,
                            params: {"OPP_ID":null,"APPLICATION_ID": null, "AGENT_CD": null, "SIS_ID":null, "FHR_ID":null, "LEAD_ID":null,"POLICY_NO":null,"COMBO_ID":null,"REF_SIS_ID":null,"REF_FHRID":null,"REF_OPP_ID":null,"REF_APPLICATION_ID":null},
                     		abstract: true,
                     		views:{
                     			'app-tab-content': {
                     				templateUrl: "applicationForm/personalInformation/personalInfo.html",
                     				controller: "PersonalInfoCtrl as personal"
                     			}
                     		}
                     	})
                     	.state("applicationForm.personalInfo.personalDetails",{
                                url: "/personalDetails",
                                cache: false,
                                params: {"OPP_ID": null, "APPLICATION_ID": null, "AGENT_CD": null, "SIS_ID": null, "FHR_ID": null, "LEAD_ID": null, "POLICY_NO": null, "COMBO_ID": null, "REF_SIS_ID": null, "REF_FHRID": null, "REF_OPP_ID": null, "REF_APPLICATION_ID": null},
                                views:{
                                    'app-main-content': {
                                        templateUrl: "applicationForm/personalInformation/personalDetails/personalDetails.html",
                                        controller: "PersonalDetailsCtrl as pDet",
                                        resolve:{
                                                    SISFormData: ['SisFormService', 'SfaPage1Service', '$stateParams',
                                                    function(SisFormService, SfaPage1Service, $stateParams){
                                                    console.log("SISFormData :"+$stateParams.AGENT_CD);
                                                        if(!!$stateParams.SIS_ID)
                                                            return SisFormService.loadSavedSISData($stateParams.AGENT_CD, $stateParams.SIS_ID, $stateParams.OPP_ID, $stateParams.FF_FHR_ID, $stateParams.LEAD_ID);
                                                        else
                                                            return null;
                                                    }],
                                                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                    console.log("inside ExistingAppData");
                                                        return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                    }
                                                    ],
                                                    Form60List: ['LoadAppProofList', function(LoadAppProofList){
                                                                console.log("resolve Form60List");
                                                                return LoadAppProofList.getForm60DocID();
                                                    }],
                                                    PANList: ['LoadAppProofList', function(LoadAppProofList){
                                                                console.log("resolve PANList");
                                                                return LoadAppProofList.getPANDocID();
                                                    }],
                                                    UANList: ['LoadAppProofList', function(LoadAppProofList){
                                                                console.log("resolve UANList");
                                                                return LoadAppProofList.getUANDocID();
                                                    }],
                                                    InsAgeProofList: ['CommonService', 'SISFormData', function(CommonService, SISFormData){
                                                            console.log("resolving getAgeProofList:IN::");
                                                                return CommonService.getAgeProofList(INSURED_CODE, SISFormData.sisFormAData.STD_AGEPROF_FLAG);
                                                    }],
													ProAgeProofList: ['CommonService', 'SISFormData', function(CommonService, SISFormData){
                                                            console.log("resolving getAgeProofList:PR");
                                                                return CommonService.getAgeProofList(PROPOSER_CODE, SISFormData.sisFormAData.STD_AGEPROF_FLAG_PR);
                                                    }],
                                                    IDProofList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve getIDProofList");
                                                                return LoadAppProofList.getIDProofList();
                                                    }],
                                                    RelDescList:['SisFormService', 'SfaPage1Service', '$stateParams','LoadAppProofList',
                                                            function(SisFormService, SfaPage1Service, $stateParams,LoadAppProofList){

                                                                   return SisFormService.loadSavedSISData($stateParams.AGENT_CD, $stateParams.SIS_ID).then(
                                                                            function(SISFormData){
                                                                            console.log("resolve getRelDesc"+SISFormData.sisFormAData.PROPOSER_IS_INSURED);
                                                                            if(SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y')
                                                                                    return LoadAppProofList.getRelDescList(true);
                                                                            else
                                                                                    return LoadAppProofList.getRelDescList(false);
                                                                            });

                                                    }],
                                                    Profile:['personalDetailsService', function(personalDetailsService){
                                                            console.log("resolve profilePicDocID");
                                                                return personalDetailsService.profilePicDocID();
                                                    }],
                                                    ProfileFlag: ['personalDetailsService', function(personalDetailsService){
                                                             console.log("resolve checkExistingPhoto");
                                                                 return personalDetailsService.checkExistingPhoto();
                                                     }],
                                                     LeadData: ['LoadApplicationScreenData','$stateParams', function(LoadApplicationScreenData, $stateParams){
                                                               console.log("resolve loadLeadTableData"+$stateParams.LEAD_ID);
                                                               return LoadApplicationScreenData.loadLeadTableData($stateParams.LEAD_ID, $stateParams.AGENT_CD);
                                                     }],
                                                     EKYCData: ['ApplicationFormDataService','LoadApplicationScreenData','$stateParams','SISFormData', function(ApplicationFormDataService, LoadApplicationScreenData, $stateParams, SISFormData){
                                                                console.log("resolve OPP ID "+$stateParams.OPP_ID);
                                                                return LoadApplicationScreenData.loadLeadEKYCData($stateParams.OPP_ID, $stateParams.AGENT_CD,null, $stateParams.LEAD_ID, null, SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE);
                                                     }],
																										 EKYCOldData: ['ApplicationFormDataService','LoadApplicationScreenData','$stateParams', function(ApplicationFormDataService, LoadApplicationScreenData, $stateParams){
                                                                console.log("resolve OPP ID "+$stateParams.OPP_ID);
                                                                return LoadApplicationScreenData.loadEKYCData($stateParams.OPP_ID/*'123456789'*/, $stateParams.AGENT_CD);
                                                     }],
																										TdDocId: ['CommonService', 'SISFormData', function(CommonService, SISFormData){
																											return CommonService.getEmpProofDocId(((SISFormData.sisFormAData.PROPOSER_IS_INSURED=='N')?"PR":"IN"));
																									 	}],
                                                    NOCparentsList:['LoadAppProofList',function(LoadAppProofList){
                                                        console.log("resolve getParentsNOC");
                                                        return LoadAppProofList.getParentsNOC();
                                                    }],
																										DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																											function(CommonService, LoginService, ApplicationFormDataService){
																													console.log("inside DeDupeData");
																													return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																											}
																										]
                                                }
                                    }
                                }
                            })
            .state("applicationForm.personalInfo.contactDetails",{
                                            url: "/contactDetails",
                                            views:{
                                                'app-main-content': {
                                                    templateUrl: "applicationForm/personalInformation/contactDetails/contactDetails.html",
                                                    controller: "ContactDetailsCtrl as contact",
                                                    resolve:{
                                                        ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                        console.log("inside contactDetails");
                                                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                        }
																											],
																											DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																												function(CommonService, LoginService, ApplicationFormDataService){
																														console.log("inside DeDupeData");
																														return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																												}
																											]
                                                    }
                                                }
                                            }
                                        })
                .state("applicationForm.personalInfo.addressDetails",{
                                                        url: "/addressDetails",
                                                        views:{
                                                            'app-main-content': {
                                                                templateUrl: "applicationForm/personalInformation/addressDetails/addressDetails.html",
                                                                controller: "AddressDetailsCtrl as addrDet",
                                                                resolve:{
                                                                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                            function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                            console.log("inside contactDetails");
                                                                                return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                            }
                                                                    ],
                                                                    NRIList: ['LoadAppProofList', function(LoadAppProofList){
                                                                                console.log("resolve NRIList");
                                                                                return LoadAppProofList.getNRIDocID();
                                                                    }],
                                                                    OCIList: ['LoadAppProofList', function(LoadAppProofList){
                                                                                console.log("resolve OCIList");
                                                                                return LoadAppProofList.getOCIDocID();
                                                                    }],
                                                                    NationalityList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolving getNationalityList");
                                                                        return LoadAppProofList.getNationalityList();

                                                                    }],
                                                                    ResidentCountryList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolving getResidentCountryList");
                                                                        return LoadAppProofList.getResidentCountryList();
                                                                    }],
                                                                    BirthCountryList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolving getBirthCountryList");
                                                                        return LoadAppProofList.getBirthCountryList();
                                                                    }],
                                                                    StateList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolving getAppState");
                                                                        return LoadAppProofList.getStateList();
                                                                    }],
                                                                    CityList: ['LoadAppProofList','StateList', function(LoadAppProofList,StateList){
                                                                    console.log("resolving getAppCity");
                                                                        return LoadAppProofList.getCityList(StateList[0].STATE_CODE);
                                                                    }],
                                                                    AddrProofList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getAddrProofList");
                                                                        return LoadAppProofList.getAddrProofList();
                                                                    }],
                                                                   LeadData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                             console.log("resolve loadLeadTableData"+ApplicationFormDataService.applicationFormBean.LEAD_ID);
                                                                             return LoadApplicationScreenData.loadLeadTableData(ApplicationFormDataService.applicationFormBean.LEAD_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                   }],
                                                                   EKYCData: ['ApplicationFormDataService','LoadApplicationScreenData','LoginService', function(ApplicationFormDataService, LoadApplicationScreenData, LoginService){
                                                                             console.log("ADDR: OPP ID :"+ApplicationFormDataService.applicationFormBean.OPP_ID);
																																						 return LoadApplicationScreenData.loadLeadEKYCData(ApplicationFormDataService.applicationFormBean.OPP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD,null, ApplicationFormDataService.applicationFormBean.LEAD_ID, null, ApplicationFormDataService.SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE);

                                                                   }],
																																	 EKYCOldData: ['ApplicationFormDataService','LoadApplicationScreenData','LoginService', function(ApplicationFormDataService, LoadApplicationScreenData, LoginService){
                                                                             console.log("ADDR: OPP ID :"+ApplicationFormDataService.applicationFormBean.OPP_ID);
                                                                             return LoadApplicationScreenData.loadEKYCData(ApplicationFormDataService.applicationFormBean.OPP_ID/*'123456789'*/, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                   }],
                                                                   SISTermData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                          console.log("resolve SISTermData: "+ApplicationFormDataService.applicationFormBean.OPP_ID);
                                                                          return LoadApplicationScreenData.loadSISTermData(ApplicationFormDataService.applicationFormBean.OPP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                   }],
																   FHRData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                             console.log("resolve loadFHRTableData"+ApplicationFormDataService.applicationFormBean.FHR_ID);
                                                                             return LoadApplicationScreenData.loadFHRTableData(ApplicationFormDataService.applicationFormBean.FHR_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                   }],
							 																										DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
							 																											function(CommonService, LoginService, ApplicationFormDataService){
							 																													console.log("inside DeDupeData");
							 																													return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
							 																											}
							 																										]

                                                                }

                                                            }
                                                        }
                                                    }) //
                .state("applicationForm.personalInfo.eduQualDetails",{
                                                        url: "/eduQualDetails",
                                                        views:{
                                                            'app-main-content': {
                                                                templateUrl: "applicationForm/personalInformation/eduQualDetails/eduQualDetails.html",
                                                                controller: "EduQualDetailsCtrl as eduQ",
                                                                resolve:{
                                                                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                        console.log("inside contactDetails");
                                                                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                        }
                                                                    ],
                                                                    EduQualList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolving getEduQual");
                                                                        return LoadAppProofList.getEduQualList();
                                                                    }],
                                                                    OccClassList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getOccpList");
                                                                        return LoadAppProofList.getOccClassList();
                                                                    }],
                                                                    CompanyNameList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getCompanyNameList");
                                                                        return LoadAppProofList.getCompanyNameList();
                                                                    }],
                                                                    OrgTypeList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve AppOrgType");
                                                                        return LoadAppProofList.getOrgTypeList();
                                                                    }],
                                                                    OccIndustryList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getOccIndustryList");
                                                                        return LoadAppProofList.getOccIndustryList();
                                                                    }],
                                                                    OccNatureList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getOccNatureList");
                                                                        return LoadAppProofList.getOccNatureList();
                                                                    }],
                                                                    DesignationList: ['LoadAppProofList','OccClassList', function(LoadAppProofList,OccClassList){
                                                                    console.log("resolve getDesignationList:::");
                                                                        return LoadAppProofList.getDesignationList();
                                                                    }],
                                                                    IncomeProofList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getIncomeProofList");
                                                                        return LoadAppProofList.getIncomeProofList();
                                                                    }],
                                                                    /*StudentIDList:['LoadAppProofList',function(LoadAppProofList){
                                                                     console.log("resolve StudentIDList");
                                                                        return LoadAppProofList.getStudentIDProof();
                                                                    }],*/
                                                                    FatcaList: ['LoadAppProofList', function(LoadAppProofList){
                                                                                console.log("resolve FatcaList");
                                                                                return LoadAppProofList.getFatcaDocID();
                                                                    }],
                                                                   FFNAData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                             console.log("resolve loadFFNATableData"+ApplicationFormDataService.applicationFormBean.FHR_ID);
                                                                             return LoadApplicationScreenData.loadFFNATableData(ApplicationFormDataService.applicationFormBean.FHR_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                   }],
                                                                    SISTermData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                           console.log("resolve SISTermData: "+ApplicationFormDataService.applicationFormBean.OPP_ID);
                                                                           return LoadApplicationScreenData.loadSISTermData(ApplicationFormDataService.applicationFormBean.OPP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                    }],
 																   FHRData: ['LoadApplicationScreenData','ApplicationFormDataService','LoginService', function(LoadApplicationScreenData, ApplicationFormDataService, LoginService){
                                                                              console.log("resolve loadFHRTableData"+ApplicationFormDataService.applicationFormBean.FHR_ID);
                                                                              return LoadApplicationScreenData.loadFHRTableData(ApplicationFormDataService.applicationFormBean.FHR_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                    }],
                                                                    OccupDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                                            console.log("resolve getOccupQuestDocId in educationCtrl");
                                                                            return LoadAppProofList.getOccupQuestDocId();
                                                                    }],
                                                                    OccuQuesList :['LoadAppProofList',function(LoadAppProofList){
                                                                            console.log("resolve getOccuQuesionnarie");
                                                                            return LoadAppProofList.getOccuQuesionnarie();
                                                                    }],
																																		DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																																			function(CommonService, LoginService, ApplicationFormDataService){
																																					console.log("inside DeDupeData");
																																					return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																																			}
																																		]
                                                                }
                                                            }
                                                        }
                                                    })
                .state("applicationForm.personalInfo.otherDetails",{
                                                                    url: "/otherDetails",
                                                                    views:{
                                                                        'app-main-content': {
                                                                            templateUrl: "applicationForm/personalInformation/otherDetails/otherDetails.html",
                                                                            controller: "OtherDetailsCtrl as otherDet",
                                                                            resolve:{
                                                                            ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                                    console.log("inside contactDetails");
                                                                                        return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                                    }
                                                                            ],
                                                                             PscData:['otherDetailService',function(otherDetailService){
                                                                                 return otherDetailService.getPscData().then(
                                                                                     function(pscData){
                                                                                         return pscData;
                                                                                     }
                                                                                 )
                                                                             }
                                                                             ],
		                                                                     IncomeSourceProofList: ['LoadAppProofList', function(LoadAppProofList){
		                                                                     console.log("resolve getIncomeSourceProofList");
		                                                                         return LoadAppProofList.getIncomeSourceProofList();
		                                                                     }],
		                                                                     amlData: ['otherDetailService','ExistingAppData','ApplicationFormDataService', function(otherDetailService,ExistingAppData,ApplicationFormDataService){
		                                                                     console.log("resolve getAmlData");
																																						 var pepFlag;
																																						 if(ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED == 'Y'){
																																							 pepFlag = ExistingAppData.applicationPersonalInfo.Insured.PEP_SELP_FLAG == 'Y' ? true : false;
																																						 }else {
																																							 pepFlag = ExistingAppData.applicationPersonalInfo.Proposer.PEP_SELP_FLAG == 'Y' ? true : false;
																																						 }
																																						 debug("pepFlag : " + pepFlag);
		                                                                         return otherDetailService.getAmlData(ExistingAppData, pepFlag);
		                                                                     }],
		 																																		DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
		 																																			function(CommonService, LoginService, ApplicationFormDataService){
		 																																					console.log("inside DeDupeData");
		 																																					return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
		 																																			}
		 																																		]
                                                                            }
                                                                        }
                                                                    }
                                                                })
            .state("applicationForm.questionnairels",{
                                    url: "/questionnairels",
                                    abstract: true,
                                    views:{
                                        'app-tab-content': {
                                            templateUrl: "applicationForm/questionnaire/questionnaireLS.html",
                                            controller: "QuestionnaireCtrl as quest"
                                        }
                                    }
                                })
            .state("applicationForm.questionnairels.lifeStyleIns",{
                                    url: "/lifeStyle",
                                    views:{
                                        'app-quest-content': {
                                            templateUrl: "applicationForm/questionnaire/lifeStyle/InsLifeStyle.html",
                                            controller: "LifeStyleCtrl as life",
                                            resolve: {
                                            ExistingAppMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                        console.log("inside LS Ins");
                                                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                    }],
                                            ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                    console.log("inside lifestyle");
                                                        return LoadApplicationScreenData.loadLifeStyleData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                    }],
                                            DefenceDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                    console.log("resolve getDefenceDocId");
                                                        return LoadAppProofList.getDefenceDocId();
                                                    }],
                                            OccupDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                    console.log("resolve getOccupQuestDocId");
                                                        return LoadAppProofList.getOccupQuestDocId();
                                                    }],
                                            SubOccDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                    console.log("resolve SubOccDocList");
                                                        return LoadAppProofList.getSubOccDocId();
                                                    }],
                                            TravelDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                    console.log("resolve TravelDocList");
                                                        return LoadAppProofList.getTravelDocId();
                                                    }],
                                            DrinksTypeList: ['LoadAppProofList', function(LoadAppProofList){
                                                    console.log("resolve getDrinkTypeList");
                                                        return LoadAppProofList.getDrinkTypeList();
                                                    }],
                                            SmokeTypeList: ['LoadAppProofList', function(LoadAppProofList){
                                                        console.log("resolve SmokeTypeList");
                                                            return LoadAppProofList.getSmokeTypeList();
                                                        }],
                                            FrequencyList: ['LoadAppProofList', function(LoadAppProofList){
                                                        console.log("resolve getFrequencyList");
                                                            return LoadAppProofList.getFrequencyList();
                                                        }]
                                            }
                                        }
                                    }
                                })
            .state("applicationForm.questionnairels.lifeStylePro",{
                                            url: "/lifeStyle",
                                            views:{
                                                'app-quest-content': {
                                                    templateUrl: "applicationForm/questionnaire/lifeStyle/ProLifeStyle.html",
                                                    controller: "LifeStyleProCtrl as plife",
                                                    resolve: {
                                                    ExistingAppMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                console.log("inside LS pro");
                                                                    return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                }],
                                                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                console.log("inside lifestyle");
                                                                    return LoadApplicationScreenData.loadLifeStyleData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                }],
                                                    DefenceDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve getDefenceDocId");
                                                                return LoadAppProofList.getDefenceDocId();
                                                            }],
                                                    OccupDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve getOccupQuestDocId");
                                                                return LoadAppProofList.getOccupQuestDocId();
                                                            }],
                                                    SubOccDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve SubOccDocList");
                                                                return LoadAppProofList.getSubOccDocId();
                                                            }],
                                                    TravelDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve TravelDocList");
                                                                return LoadAppProofList.getTravelDocId();
                                                            }],
                                                    DrinksTypeList: ['LoadAppProofList', function(LoadAppProofList){
                                                            console.log("resolve getDrinkTypeList");
                                                                return LoadAppProofList.getDrinkTypeList();
                                                            }],
                                                    SmokeTypeList: ['LoadAppProofList', function(LoadAppProofList){
                                                                console.log("resolve SmokeTypeList");
                                                                    return LoadAppProofList.getSmokeTypeList();
                                                                }],
                                                    FrequencyList: ['LoadAppProofList', function(LoadAppProofList){
                                                                console.log("resolve getFrequencyList");
                                                                    return LoadAppProofList.getFrequencyList();
                                                                }]
                                                    }
                                                }
                                            }
                                        })
            .state("applicationForm.questionnairehl",{
                                            url: "/questionnairehl",
                                            abstract: true,
                                            views:{
                                                'app-tab-content': {
                                                    templateUrl: "applicationForm/questionnaire/questionnaireHL.html",
                                                    controller: "QuestionnaireCtrl as quest"

                                                }
                                            }
                                        })
            .state("applicationForm.questionnairehl.healthIns",{
                                            url: "/health",
                                            views:{
                                                'app-questHL-content': {
                                                    templateUrl: "applicationForm/questionnaire/health/healthIns.html",
                                                    controller: "HealthInsCtrl as health",
                                                    resolve: {
                                                        ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                    console.log("inside health");
                                                                        return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                    }
                                                                ],
                                                        ExistingHealthMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                        console.log("inside healthMain");
                                                                            return LoadApplicationScreenData.loadHealthData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                        }],
                                                        ExistingHealthSubData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                        console.log("inside healthSub");
                                                                            return LoadApplicationScreenData.loadHealthSubData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                        }],
                                                        HealthDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                                        console.log("resolve getHealthDocIDList");
                                                                            return LoadAppProofList.getHealthDocIDList();
                                                                        }]

                                                    }
                                                }
                                            }
                                        })
            .state("applicationForm.questionnairehl.healthPro",{
                                                    url: "/health",
                                                    views:{
                                                        'app-questHL-content': {
                                                            templateUrl: "applicationForm/questionnaire/health/healthPro.html",
                                                            controller: "HealthProCtrl as phealth",
                                                            resolve: {
                                                                ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                                    console.log("inside health pro");
                                                                                        return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                                    }
                                                                                ],
                                                                ExistingHealthMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                                function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                                console.log("inside healthMain proposer");
                                                                                    return LoadApplicationScreenData.loadHealthData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                                }],
                                                                ExistingHealthSubData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                                function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                                console.log("inside healthSub proposer");
                                                                                    return LoadApplicationScreenData.loadHealthSubData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                                }],
                                                                HealthDocList: ['LoadAppProofList', function(LoadAppProofList){
                                                                                console.log("resolve getHealthDocIDList");
                                                                                    return LoadAppProofList.getHealthDocIDList();
                                                                                }]
                                                            }
                                                        }
                                                    }
                                                })
                                        /* ***************** Amruta ***************** */

            .state("applicationForm.existingIns",{
                            url: "/existingIns",
                            abstract: true,
                            views:{
                                'app-tab-content': {
                                    templateUrl: "applicationForm/existingIns/existingIns.html",
                                    controller: "ExistingInsCtrl as existIns"
                                }
                            }
                        })
             .state("applicationForm.existingIns.tataPolicy",{
                                                url: "/tataPolicy",
                                                cache: false,
                                                views:{
                                                    'app-exist-content': {
                                                        templateUrl: "applicationForm/existingIns/tataPolicy/tataPolicy.html",
                                                        controller: "tataPolicyCtrl as tata",
                                                        resolve:{
                                                                getExistInsRec:['appExistingInsService',function(appExistingInsService){
                                                                    console.log("Getting data");
                                                                    return appExistingInsService.selectTataInsRecords();
                                                                }],
                                                                getExistPropRec:['appExistingInsService',function(appExistingInsService){
                                                                    console.log("Getting proposer data");
                                                                    return appExistingInsService.selectTataPropRecords()
                                                                }],
																																ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                          function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                          console.log("inside health pro");
                                                                              return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                          }
                                                                      ],
																															 DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																																 function(CommonService, LoginService, ApplicationFormDataService){
																																		 console.log("inside DeDupeData");
																																		 return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																																 }
																															 ]
                                                         }
                                                    }
                                                }
                                            })
            .state("applicationForm.existingIns.nonTataPolicy",{
                                        url: "/nonTataPolicy",
                                        cache: false,
                                        views:{
                                            'app-exist-content': {
                                                templateUrl: "applicationForm/existingIns/nonTataPolicy/nonTataPolicy.html",
                                                controller: "nonTataPolicyCtrl as nonTata",
                                                resolve:{
                                                        getNonExistInsRec:['appNonTataExistingInsService',function(appNonTataExistingInsService){
                                                            console.log("NonTata Policy Data");
                                                            return appNonTataExistingInsService.selectNontataInsRec();
                                                        }],
                                                        getNonExistPropRec:['appNonTataExistingInsService',function(appNonTataExistingInsService){
                                                            console.log("Nontata Policy Proposer data");
                                                            return appNonTataExistingInsService.selectNontataPropRec();
                                                        }]
                                                }
                                            }
                                        }
                                    })
            .state("applicationForm.familyDetails",{
                                            url: "/familyDetails",
                                            abstract: true,
                                            views:{
                                                'app-tab-content': {
                                                    templateUrl: "applicationForm/familyDetails/familyDetails.html",
                                                    controller: "familyCtrl as family"
                                                }
                                            }
                                        })
                  .state("applicationForm.familyDetails.familySection1",{
                                                        url: "/familySection1",
                                                        cache: false,
                                                        views:{
                                                            'app-family-content': {
                                                                templateUrl: "applicationForm/familyDetails/familySection1/familySection1.html",
                                                                controller: "familySection1Ctrl as family1",
                                                                resolve:{
                                                                        ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                            function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                            console.log("inside family section1");
                                                                                return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                            }
                                                                        ],
                                                                        getFamilyMemList:['appFamilyDetService',function(appFamilyDetService){
                                                                            console.log("Getting member details");
                                                                            return appFamilyDetService.loadFamilyMemList();
                                                                        }],
                                                                        getFamilyRecord:['appFamilyDetService',function(appFamilyDetService){
                                                                            console.log("Selecting Records");
                                                                            return appFamilyDetService.selectFamilyMem();

                                                                        }],
                                                                        getProdFlagData:['appFamilyDetService',function(appFamilyDetService){
                                                                            console.log("Product data");
                                                                            return appFamilyDetService.chkBackDate();
                                                                        }],
                                                                        getBackDate:['appFamilyDetService',function(appFamilyDetService){
                                                                                        console.log("Backdate::");
                                                                                        return appFamilyDetService.selectBackdate();
                                                                        }]

                                                                }
                                                            }
                                                        }
                                                    })
                  .state("applicationForm.familyDetails.familySection2",{
                                                      url: "/familySection2",
                                                      cache: false,
                                                      views:{
                                                          'app-family-content': {
                                                              templateUrl: "applicationForm/familyDetails/familySection2/familySection2.html",
                                                              controller: "familySection2Ctrl as family2",
                                                              resolve:{
                                                                        ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                            function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                            console.log("inside family section2");
                                                                                return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                            }
                                                                        ],
                                                                        getMemberRec:['appFamilyChildDetService',function(appFamilyChildDetService){
                                                                            console.log("Family member2 Data");
                                                                            return appFamilyChildDetService.getFamilyMemRecords();

                                                                        }],
                                                                        getChildRec:['appFamilyChildDetService',function(appFamilyChildDetService){
                                                                            console.log("Getting child data");
                                                                            return appFamilyChildDetService.getChildMemRecords();
                                                                        }],
                                                                        getSecondChildRec:['appFamilyChildDetService',function(appFamilyChildDetService){
                                                                            console.log("Getting second child data");
                                                                            return appFamilyChildDetService.getSecondChildMemRecords();
                                                                        }]

                                                              }
                                                          }
                                                      }
                                                  })
            /* ***************** END ******************** */
            //nominee appointee start...
                 		  .state("applicationForm.nomAppDetails",{
                 					url: "/nomAppDetails",
                 					abstract: true,
                 					views:{
                 						'app-tab-content': {
                 							templateUrl: "applicationForm/nomAppDetails/nomApp.html",
                 							controller : "NomApptCtrl as nac"
                 						}
                 					}
                 				})

                 		  .state("applicationForm.nomAppDetails.nomineeDetails",{
                 						url :"/nomineeDetails",
                 						views :
                 						{
                 							'app-nomapp-content':{
                 								templateUrl : "applicationForm/nomAppDetails/nomineeDetails/nomineeDetails.html",
                 								controller : "NomCntrl as nc",
                 								resolve : {
                 										nomRelationData :
                 										['nomService',function(nomService){
                 												debug("resolove nom details");
                 												return nomService.getNomRelationData();
                 											}
                 										],
                 										NomExistingData :
                 										['nomService','ApplicationFormDataService','LoginService',function(nomService,ApplicationFormDataService,LoginService){
                 										        return nomService.existingData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                 										    }

                 										],
                 										TermExistingData :
                                                        ['nomService','ApplicationFormDataService','LoginService',function(nomService,ApplicationFormDataService,LoginService){
                                                                return nomService.existingDataTerm(ApplicationFormDataService.ComboFormBean.COMBO_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                            }

                                                        ],
                 										ExistingAppMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                             function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                 debug("existing object loaded");
                                                                 return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                             }
                                                        ],
                                                        sisData :
                                                        ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                                             debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                             debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                             return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                         }
                                                        ],
																								 DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																									 function(CommonService, LoginService, ApplicationFormDataService){
																											 console.log("inside DeDupeData");
																											 return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																									 }
																								 ]
                 								}
                 							}
                 						}
                 		  })

                 			.state("applicationForm.nomAppDetails.appointeeDetails",{
                 					 url :"/appointeeDetails",
                 					 views :{
                 						 'app-nomapp-content':{
                 							 templateUrl : "applicationForm/nomAppDetails/appointeeDetails/appointeeDetails.html",
                 							 controller : "ApptCntrl as apc",
                 							 resolve :{
                 								apptRelationData :
                 								['ApptService',function(ApptService){
                 										debug("resolove appt details");
                 										return ApptService.getApptRelationData();
                 									}
                 								],
                 								ApptExistingData :
                 								['ApplicationFormDataService','LoginService','ApptService',function(ApplicationFormDataService,LoginService,ApptService){
                 								        debug("appt existing data");
                 								        return ApptService.existingApptData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                 								    }
                 								],
                                  ApptTermExistingData :
                                  ['ApplicationFormDataService','LoginService','ApptService',function(ApplicationFormDataService,LoginService,ApptService){
                                          debug("appt existing data");
                                          return ApptService.existingTermApptData(ApplicationFormDataService.ComboFormBean.COMBO_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                      }
                                  ],
																	 DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																		 function(CommonService, LoginService, ApplicationFormDataService){
																				 console.log("inside DeDupeData");
																				 return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																		 }
																	 ]
                 							 }
                 						 }
                 					 }
                 			 })
                         //nominee appointee end...

                         //payment details...start
                                 .state("applicationForm.paymentDetails",{
                                         url:"/paymentDetails",
                                         abstract : true,
                                         views :{
                                             'app-tab-content':{
                                                 templateUrl : "applicationForm/paymentDetails/paymentdetails.html",
                                                 controller: "payDetCtrl as pdc"
                                             }
                                         }
                                 })

                                 .state("applicationForm.paymentDetails.bankdetails",{
                                     url:"/bankdetails",
                                     views : {
                                         'app-payDet-content' :{
                                             templateUrl : "applicationForm/paymentDetails/bankdetails/bankdetails.html",
                                             controller : "banDetCtrl as bdc",
                                             resolve : {
                                                 bankData :
                                                 ['bankDetService',function(bankDetService){
                                                         return bankDetService.getBankNames();
                                                     }
                                                 ],
                                                 NEFTData :
                                                 ['bankDetService',function(bankDetService){
                                                     return bankDetService.NEFTDocData();
                                                     }
                                                 ],
                                                 loadData :
                                                 ['bankDetService','ApplicationFormDataService','LoginService',function(bankDetService,ApplicationFormDataService,LoginService){
                                                         return bankDetService.loadBankDetails(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                     }
                                                 ],
                                                 ExistingAppMainData:
                                                 ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                       function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                           debug("existing object loaded");
                                                           return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                       }
                                                 ],
                                                 sisData :
                                                  ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                                      debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                      debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                      return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                  }
                                                  ],
                                                  bankDataFHR:['paymentDetService','LoginService','ApplicationFormDataService',function(paymentDetService,LoginService,ApplicationFormDataService){
                                                            var FHR_ID = ApplicationFormDataService.applicationFormBean.FHR_ID;
                                                             debug("FHR_ID BANK DETAILS1: "+FHR_ID);

                                                        return paymentDetService.getFHRBankData(LoginService.lgnSrvObj.userinfo.AGENT_CD,FHR_ID);
                                                      }
                                                  ],
																									 DeDupeData: ['CommonService','LoginService','ApplicationFormDataService',
																										 function(CommonService, LoginService, ApplicationFormDataService){
																												 console.log("inside DeDupeData");
																												 return CommonService.loadDeDupeData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
																										 }
																									 ]
                                             }
                                         }
                                     }
                                 })

                                 .state("applicationForm.paymentDetails.paymentoption",{
                                     url:"/paymentoptions",
                                     views : {
                                         'app-payDet-content' :{
                                             templateUrl : "applicationForm/paymentDetails/paymentoption/paymentoption.html",
                                             controller : "payOptionCtrl as poc",
                                             resolve :
                                             {
                                                 paymentData :
                                                 ['paymentDetService',function(paymentDetService){
                                                        return paymentDetService.getPaymentData(BUSINESS_TYPE);
                                                    }
                                                 ],
                                                 bankData :
                                                 ['bankDetService',function(bankDetService){
                                                         return bankDetService.getBankNames();
                                                     }
                                                 ]/*,
                                                 payData :
                                                 ['paymentDetService',function(paymentDetService){
                                                        return paymentDetService.getPayingModes();
                                                     }
                                                 ]*/,
                                                 sisData :
                                                 ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                                         debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                        debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                        return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                    }
                                                 ],
                                                 loadData :
                                                 ['paymentDetService','LoginService','ApplicationFormDataService',function(paymentDetService,LoginService,ApplicationFormDataService){
                                                      return paymentDetService.getPayModeFlagFromDB(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                    }
                                                 ],
                                                 ExistingAppMainData:
                                                 ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                     function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                         debug("existing object loaded");
                                                         return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                     }
                                                 ],
                                                 maxDate:
                                                 ['paymentDetService',
                                                     function(paymentDetService){
                                                        return paymentDetService.getMaxDate();
                                                     }
                                                 ],
                                                 minDate:
                                                 ['paymentDetService',
                                                       function(paymentDetService){
                                                          return paymentDetService.getMinDate();
                                                     }
                                                 ],
                                                 payData:
                                                 ['sisData','paymentDetService',
                                                    function(sisData,paymentDetService){
                                                       return paymentDetService.getModes(sisData.sisMainData.PGL_ID,sisData.sisFormBData.PLAN_CODE).then(
                                                          function(data){
                                                            debug("pay data is "+JSON.stringify(data));
                                                             return data;
                                                          }
                                                       );
                                                    }
                                                 ],
                                                 sisDataTermPlan :
                                                 ['SisFormService','LoginService','ApplicationFormDataService','paymentDetService',function(SisFormService,LoginService,ApplicationFormDataService,paymentDetService){
                                                        debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                        debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                        var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                                        console.log("sisDataTermPlan COMBO_ID"+COMBO_ID);
                                                        var termPlanData;
                                                        if(COMBO_ID!=undefined){
                                                            return paymentDetService.getDataFrom_LP_MYOPPORTUNITY(COMBO_ID);
                                                        }else{
                                                            return null;
                                                        }
                                                    }
                                                 ]

                                             }
                                         }
                                     }
                                 })

                                 .state("applicationForm.paymentDetails.renewalPayment",{
                                   url:"/renewalPayment",
                                   params :{amount : null,payMode:null},
                                   views : {
                                        'app-payDet-content' : {
                                            templateUrl : "applicationForm/paymentDetails/renewalPayment/renewalPayment.html",
                                            controller : "renewalPayCtrl as repc",
                                            resolve :
                                            {
                                                ExistingAppMainData:
                                                ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                     function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                         debug("existing object loaded");
                                                         return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                     }
                                                ]/*,
                                                amount :
                                                ['$stateParams',
                                                    function($stateParams){
                                                        debug("amt --->"+$stateParams.amount);
                                                        return $stateParams.amount;
                                                    }
                                                ]*/,
                                                payMode :
                                                ['$stateParams',
                                                    function($stateParams){
                                                        debug("amt --->"+$stateParams.payMode);
                                                        return $stateParams.payMode;
                                                    }
                                                ],
                                                sisData :
                                                ['SisFormService','LoginService','ApplicationFormDataService',
                                                    function(SisFormService,LoginService,ApplicationFormDataService){
                                                       debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                       return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                                    }
                                                ],
                                                existingData :
                                                ['renPayService','ApplicationFormDataService','LoginService',
                                                    function(renPayService,ApplicationFormDataService,LoginService){
                                                        return renPayService.getExistingRenewalData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                    }
                                                ],
                                                amount:
                                                ['renPayService','LoginService','ApplicationFormDataService',function(renPayService,LoginService,ApplicationFormDataService){
                                                        return renPayService.getAmount(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.applicationFormBean.APPLICATION_ID);/*.then(
                                                            function(amount){
                                                                debug("amount in resolve "+ amount);
                                                                return amount;
                                                            }
                                                        );*/
                                                    }
                                                ],
                                                 existingDataTermPlan :
                                                ['renPayService','ApplicationFormDataService','LoginService',
                                                    function(renPayService,ApplicationFormDataService,LoginService){
                                                      //return renPayService.getExistingRenewalDataTermPlan('COMBO_ID',LoginService.lgnSrvObj.userinfo.AGENT_CD);//1788 02
                                                    console.log(" COMBO_ID obj"+JSON.stringify(ApplicationFormDataService.ComboFormBean));

                                                      var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                                              console.log("existing Data Term Plan COMBO_ID "+COMBO_ID);
                                                              if(COMBO_ID!=undefined){
                                                                   return renPayService.getExistingRenewalDataTermPlan(ApplicationFormDataService.ComboFormBean.COMBO_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                              }
                                                              else
                                                                  return null;

                                                    }
                                                ],
                                                 sisDataTermPlan :
                                                 ['SisFormService','LoginService','ApplicationFormDataService','renPayService',function(SisFormService,LoginService,ApplicationFormDataService,renPayService){
                                                     var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                                    debug("sisDataTermPlan COMBO_ID"+COMBO_ID);
                                                    var termPlanData;
                                                    if(COMBO_ID!=undefined){
                                                         return renPayService.getDataFrom_LP_MYOPPORTUNITY(COMBO_ID);
                                                    }else{
                                                        return null;
                                                    }


                                                }],
                                                bankDataFHR:['paymentDetService','LoginService','ApplicationFormDataService',function(paymentDetService,LoginService,ApplicationFormDataService){
                                                          var FHR_ID = ApplicationFormDataService.applicationFormBean.FF_FHR_ID;
                                                           debug("FHR_ID BANK DETAILS: "+FHR_ID);

                                                      return paymentDetService.getFHRBankData(LoginService.lgnSrvObj.userinfo.AGENT_CD,FHR_ID);
                                                    }
                                                ]

                                            }

                                        }
                                   }
                                 })

                               .state("uploadDoc",{
                                   url:"/uploadDoc",
                                   templateUrl:"uploadDoc/uploadDoc.html",//7034384358
                                   controller:"uploadDocCtrl as updc",
									cache: false,
                                     params : {"appid": null, "agentCd": null, "sisId": null, "fhrId": null, "policyNo": null, "leadId": null, "pglId": null, "fromPT": false, "oppId": null, "comboId": null},
                                  // params : {"appid":null,"agentCd" : null,"sisId":null,"ffhrId":null,"policyNo":null,"leadId":null,"pglId":null,"fromPT":false, "oppId":null},

                                   resolve:{
                                       paramData :['$stateParams','ApplicationFormDataService',function($stateParams,ApplicationFormDataService){
                                            console.log("uploadDoc $stateParams :"+JSON.stringify($stateParams));
                                            var data = {};
                                            data.APP_ID = $stateParams.appid;
                                            data.AGENT_CD = $stateParams.agentCd;
                                            data.SIS_ID = $stateParams.sisId;
                                            data.LEAD_ID = $stateParams.leadId;
                                            data.FHR_ID = $stateParams.fhrId;
                                            data.POLICY_NO = $stateParams.policyNo;
                                            data.PGL_ID = $stateParams.pglId;
                                            data.fromPT = $stateParams.fromPT;
                                            data.OPP_ID = $stateParams.oppId;
                                            data.COMBO_ID = $stateParams.comboId;
                                           //data.COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                            return data;
                                       }],
                                       appData:['uploadDocService','ApplicationFormDataService','LoginService','$stateParams',function(uploadDocService,ApplicationFormDataService,LoginService,$stateParams){
                                               var COMBO_ID = $stateParams.comboId;
                                               console.log("sisDataTermPlan COMBO_ID"+COMBO_ID);
                                                 if(!!COMBO_ID){

                                                    return  uploadDocService.getAppIDFrom_LP_MYOPPORTUNITY(COMBO_ID);
                                                 }
                                                 else{
                                                     return null;
                                                 }
                                            }
                                        ],
                                        // getRefPolicyNo:['uploadDocService','ApplicationFormDataService','LoginService','$stateParams',function(uploadDocService,ApplicationFormDataService,LoginService,$stateParams){
                                        //        var COMBO_ID = $stateParams.comboId;
                                        //        console.log("sisDataTermPlan COMBO_ID"+COMBO_ID);
                                        //          if(!!COMBO_ID){
                                        //             return  uploadDocService.getREFPOLICYNOFrom_LP_MYOPPORTUNITY(COMBO_ID);
                                        //          }
                                        //          else{
                                        //              return null;
                                        //          }
                                        //     }
                                        // ],
                                         ExistingAppMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData','$stateParams',
                                             function(LoginService, ApplicationFormDataService, LoadApplicationScreenData,$stateParams){
                                                 return LoadApplicationScreenData.loadApplicationMain($stateParams.appid,$stateParams.agentCd);
                                             }
                                       ]

                                   }
                              })

                .state("applicationForm.policyNoGen",{
                                                    url: "/policyNoGeneration",
                                                    views:{
                                                        'app-tab-content': {
                                                            templateUrl: "applicationForm/policyNoGeneration/policyNoGen.html",
                                                            controller : "PolicyNoCtrl as pol",
                                                            resolve:{
                                                            PolicyNoData: ['PolicyNoService', function(PolicyNoService){
                                                                        console.log("resolve PolicyNoService");
                                                                            return PolicyNoService.isPolicyNoAssigned();
                                                                        }],
                                                            TermPolicyNoData: ['ComboPolicyNoService','ApplicationFormDataService', function(ComboPolicyNoService, ApplicationFormDataService){
                                                                   var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                                                                  console.log("resolve ComboPolicyNoService"+COMBO_ID);
                                                                  if(COMBO_ID!=undefined)
                                                                      return ComboPolicyNoService.loadComboDtls(COMBO_ID);
                                                                  else
                                                                      return null;
                                                                  }],
                                                            ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                                                                    function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                                                                    console.log("inside policyNoGeneration");
                                                                        return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                                    }
                                                                ],
                                                            MedicalFlags: ['LoadAppProofList','ApplicationFormDataService', function(LoadAppProofList, ApplicationFormDataService){
                                                                    console.log(ApplicationFormDataService.SISFormData.sisFormBData.PLAN_CODE+"resolve MedicalFlags"+JSON.stringify(ApplicationFormDataService.SISFormData));
                                                                try{

                                                                    if(!!ApplicationFormDataService.SISFormData && !!ApplicationFormDataService.SISFormData.sisFormDData && ApplicationFormDataService.SISFormData.sisFormDData[0]!=undefined && ApplicationFormDataService.SISFormData.sisFormBData.PLAN_CODE!=undefined)
                                                                        {
                                                                            debug("inside if");
                                                                            var planCode = ApplicationFormDataService.SISFormData.sisFormBData.PLAN_CODE;
                                                                            debug("inside if :: "+planCode);
                                                                            return LoadAppProofList.getPlanMasterDetails(planCode);
                                                                        }
                                                                        else
                                                                        {   debug("inside else");
                                                                            return LoadAppProofList.getPlanMasterDetails();
                                                                        }
                                                                    }catch(e){
                                                                        debug("inside exception ::"+e.message);
                                                                    }
                                                                    }],
                                                            FinancialQuest: ['LoadAppProofList', function(LoadAppProofList){
                                                                 console.log("resolve FinancialQuest");

                                                                     return LoadAppProofList.getFinancialQuest();
                                                                 }],
                                                            TermSisData :
                                                            ['SisFormService','LoginService','ApplicationFormDataService',
                                                                function(SisFormService,LoginService,ApplicationFormDataService){
                                                                   debug("SIS IS "+ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                                                   return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                                            }],
                                                            TotInsPremium :['ApplicationFormDataService','LoginService','PolicyNoService',function(ApplicationFormDataService, LoginService, PolicyNoService){
                                                                        debug("Resolving TotInsPremium");
                                                                    return PolicyNoService.addExistInsPremium(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                            }],
                                                            TotProPremium :['ApplicationFormDataService','LoginService','PolicyNoService',function(ApplicationFormDataService, LoginService, PolicyNoService){
                                                                    debug("Resolving TotProPremium");
                                                                    return PolicyNoService.addExistProPremium(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                            }],
                                                            EKYCData: ['ApplicationFormDataService','LoadApplicationScreenData', 'LoginService', function(ApplicationFormDataService, LoadApplicationScreenData, LoginService){
                                                                     console.log("EKYCData resolve OPP ID "+ApplicationFormDataService.applicationFormBean.OPP_ID);
																																		 return LoadApplicationScreenData.loadLeadEKYCData(ApplicationFormDataService.applicationFormBean.OPP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD,null, ApplicationFormDataService.applicationFormBean.LEAD_ID, null, ApplicationFormDataService.SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE);

                                                            }],
																														EKYCOldData: ['ApplicationFormDataService','LoadApplicationScreenData', 'LoginService', function(ApplicationFormDataService, LoadApplicationScreenData, LoginService){
                                                                     console.log("EKYCData resolve OPP ID "+ApplicationFormDataService.applicationFormBean.OPP_ID);
                                                                     return LoadApplicationScreenData.loadEKYCData(ApplicationFormDataService.applicationFormBean.OPP_ID/*'123456789'*/, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                            }],
                                                            ImmunizationList: ['LoadAppProofList',function(LoadAppProofList){
                                                                    console.log("resolve getImmunizationRecord");
                                                                    return LoadAppProofList.getImmunizationRecord();
                                                             }],
                                                            StudentIDList:['LoadAppProofList',function(LoadAppProofList){
                                                                console.log("resolve StudentIDList");
                                                                return LoadAppProofList.getStudentIDProof();
                                                            }]

                                                            }
                                                        }
                                                    }
                                                })

/********* Reflective ************/
	.state("applicationForm.reflectiveQuestionaries",{
        url: "/reflectiveQuestionaries",
        abstract: true,
        views:{
            'app-tab-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/reflectiveQuestionaries.html",
                controller: "reflectiveQuesCtrl as rqc"
                /*resolve: {
                    ReflectiveQuesList: ['ApplicationService',
                        function(ApplicationService){
                            debug("resolving getReflectiveQuesList");
                            return ApplicationService.getReflectiveQuesList('123456');
                        }
                    ]
                }*/
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.agricultureQues",{
        url: "/agricultureQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/agricultureQues/agricultureQues.html",
                controller: "agricultureQuesCtrl as agriC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.arthritisQues",{
        url: "/arthritisQues",
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/arthritisQues/arthritisQues.html",
                controller: "arthritisQuesCtrl as arthriC"
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.asthmaQues",{
        url: "/asthmaQues",
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/asthmaQues/asthmaQues.html",
                controller: "asthmaQuesCtrl as asthmaC"
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.diabetesQues",{
        url: "/diabetesQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/diabetesQues/diabetesQues.html",
                controller: "diabetesQuesCtrl as diabetC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.houseWifeQues",{
        url: "/houseWifeQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/houseWifeQues/houseWifeQues.html",
                controller: "houseWifeQuesCtrl as hwC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.hypertensionQues",{
        url: "/hypertensionQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/hypertensionQues/hypertensionQues.html",
                controller: "hypertensionQuesCtrl as htC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.juvenileQues",{
        url: "/juvenileQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/juvenileQues/juvenileQues.html",
                controller: "juvenileQuesCtrl as juvC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.mwpQues",{
        url: "/juvenileQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/mwpQues/mwpQues.html",
                controller: "mwpQuesCtrl as mwpC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.nriQues",{
        url: "/nriQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/nriQues/nriQues.html",
                controller: "nriQuesCtrl as nriC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.pioOciQues",{
        url: "/pioOciQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/pioOciQues/pioOciQues.html",
                controller: "pioOciQuesCtrl as poC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.smrIndsQues",{
        url: "/smrIndsQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/smrIndsQues/smrIndsQues.html",
                controller: "smrIndsQuesCtrl as smrIndsC"
            }
        }
    })

     // change on 1 Dec
    .state("applicationForm.reflectiveQuestionaries.fatcaQues",{
        url: "/fatcaQues",
        params: {"LoadData": null},
        views:{
            'app-main-content': {
                templateUrl: "applicationForm/reflectiveQuestionaries/fatcaQues/fatcaQues.html",
                controller: "fatcaQuesCtrl as fatC",
                resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                            function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside contactDetails");
                                return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                            }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    BirthCountryList: ['LoadAppProofList',
                             function(LoadAppProofList){
                               console.log("resolving getBirthCountryList");
                                 return LoadAppProofList.getBirthCountryList();
                                 }],
                    ExistingAppMainData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData','$stateParams',
                                 function(LoginService, ApplicationFormDataService, LoadApplicationScreenData,$stateParams){
                                     return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                 }
                           ]
                }
            }
        }
    })

    .state("applicationForm.reflectiveQuestionaries.occupationQues",{
        url:"/occupationQues",
        params: {"LoadData":null},
        views:{
        'app-main-content':{
            templateUrl: "applicationForm/reflectiveQuestionaries/occupationQues/occupationQues.html",
            controller: "occupationQuesCtrl as occQ",
            resolve:{
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside policyNoGeneration");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ],
                    LoadData : ['$stateParams','ReflectiveQuesService',
                        function($stateParams,ReflectiveQuesService){
                            debug("$stateParams.LoadData : " + JSON.stringify($stateParams.LoadData));
                            if(!!$stateParams.LoadData){
                                return ReflectiveQuesService.getDocPresentData($stateParams.LoadData);
                            }
                            else
                                return null;
                        }
                    ],
                    TermData : ['ApplicationFormDataService',
                        function(ApplicationFormDataService){
                            return ApplicationFormDataService.ComboFormBean;
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.medicalDiagnosis",{
        url: "/medicalDiagnosis",
        abstract: true,
        views:{
            'app-tab-content': {
                templateUrl: "applicationForm/medicalDiagnosis/medicalDiagnosis.html",
                controller: "MedicalCtrl as med",
                resolve :{
                    MedicalDCData :['LoginService', 'ApplicationFormDataService', 'LoadApplicationScreenData',function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                        console.log("Resolving MedicalDCData ");
                        return LoadApplicationScreenData.loadMedicalDCData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                    }]
                }
            }
        }
    })

    .state("applicationForm.medicalDiagnosis.insMedDiagnosis",{
        url: "/insMedDiagnosis",
        views:{
            'app-med-content': {
                templateUrl: "applicationForm/medicalDiagnosis/insMedDiagnosis/insMedDiagnosis.html",
                controller: "InsMedDiagnosisCtrl as imdc",
                resolve :{
                    MedicalDCData :['LoginService', 'ApplicationFormDataService', 'LoadApplicationScreenData',function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                        console.log("Resolving MedicalDCData ");
                        return LoadApplicationScreenData.loadMedicalDCData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                    }],
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside insMedDiagnosis");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                        }
                    ]
                }
            }
        }
    })

    .state("applicationForm.medicalDiagnosis.propMedDiagnosis",{
        url: "/propMedDiagnosis",
        params: {"isInsCompleted": false},
        views:{
            'app-med-content': {
                templateUrl: "applicationForm/medicalDiagnosis/propMedDiagnosis/propMedDiagnosis.html",
                controller: "PropMedDiagnosisCtrl as pmdc",
                resolve :{
                    MedicalDCData :['LoginService', 'ApplicationFormDataService', 'LoadApplicationScreenData',function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                        console.log("Resolving MedicalDCData ");
                        return LoadApplicationScreenData.loadMedicalDCData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                    }],
                    ExistingAppData: ['LoginService','ApplicationFormDataService','LoadApplicationScreenData',
                        function(LoginService, ApplicationFormDataService, LoadApplicationScreenData){
                            console.log("inside propMedDiagnosis");
                            return LoadApplicationScreenData.loadApplicationMain(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                    }
                    ],
                    isInsCompleted : ['$stateParams',
                        function($stateParams){
                            if(!!$stateParams.isInsCompleted){
                                return $stateParams.isInsCompleted;
                            }
                        }

                    ]
                }
            }
        }
    })
          .state("applicationForm.appOutput",{
                      url: "/appOutput",
                      views:{
                          'app-tab-content': {
                              templateUrl: "applicationForm/appOutput/appOutput.html",
                              controller: "GenerateOutputCtrl as gen",
                              resolve:{
                              ApplicationFlagsList :['LoadApplicationScreenData','LoginService', 'ApplicationFormDataService',function(LoadApplicationScreenData,LoginService, ApplicationFormDataService){
                                                console.log("Resolving ApplicationFlags :"+ApplicationFormDataService.applicationFormBean.APPLICATION_ID);
                                        return LoadApplicationScreenData.loadApplicationFlags(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                    }],
                              sisData :
                                 ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                     debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                     debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                     return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                                    }
                                 ],
                               sisTermData :
                                 ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                     debug("SIS IS "+ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                     debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                     return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                    }
                                 ],
                              PaymentData :  ['LoadApplicationScreenData','LoginService', 'ApplicationFormDataService',function(LoadApplicationScreenData,LoginService, ApplicationFormDataService){
                                             console.log("Resolving PaymentData :"+ApplicationFormDataService.applicationFormBean.APPLICATION_ID);
                                     return LoadApplicationScreenData.loadPaymentData(ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                 }],
                             FatcaDocId : ['LoadAppProofList', function(LoadAppProofList){
                                               debug("resolving FatcaDocId")
                                              return LoadAppProofList.getFatcaDocID().then(function(fatca){
                                                       return fatca;
                                                      });
                                                  }],
                             FatcaIns :['ApplicationFormDataService','LoginService','LoadAppProofList', function(ApplicationFormDataService,LoginService,LoadAppProofList){
                                                debug("resolving FatcaIns");
                                return LoadAppProofList.getFatcaDocID().then( function(fatca){
                                         return  LoadAppProofList.getFatcaFileData(fatca,ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD,'C01').then(function(fatcaFile){
                                                return fatcaFile;
                                           });
                                        });
                                    }],
                             FatcaPro :['ApplicationFormDataService','LoginService','LoadAppProofList', function(ApplicationFormDataService,LoginService,LoadAppProofList){
                               debug("resolving FatcaPro");
                                           return LoadAppProofList.getFatcaDocID().then( function(fatca){
                                                    return  LoadAppProofList.getFatcaFileData(fatca,ApplicationFormDataService.applicationFormBean.APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD,'C02').then(function(fatcaFile){
                                                           return fatcaFile;
                                                      });
                                                   });
                                               }]

                              }
                          }
                      }
                  })
                  .state("applicationForm.appOutputTerm",{
                                        url: "/appOutputTerm",
                                        views:{
                                            'app-tab-content': {
                                                templateUrl: "applicationForm/appOutput/appOutputTerm.html",
                                                controller: "GenerateTermOutputCtrl as genT",
                                                resolve:{
                                                ApplicationFlagsList :['LoadApplicationScreenData','LoginService', 'ApplicationFormDataService',function(LoadApplicationScreenData,LoginService, ApplicationFormDataService){
                                                            //changes home
                                                                  console.log("Resolving ApplicationFlags :"+ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID);
                                                          return LoadApplicationScreenData.loadApplicationFlags(ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                      }],
                                                sisData :
                                                   ['SisFormService','LoginService','ApplicationFormDataService',function(SisFormService,LoginService,ApplicationFormDataService){
                                                       debug("SIS IS "+ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                                       debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                       return SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,ApplicationFormDataService.ComboFormBean.REF_SIS_ID);
                                                      }
                                                   ],
                                                PaymentData :  ['LoadApplicationScreenData','LoginService', 'ApplicationFormDataService',function(LoadApplicationScreenData,LoginService, ApplicationFormDataService){
                                                               console.log("Resolving PaymentData :"+ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID);
                                                       return LoadApplicationScreenData.loadPaymentData(ApplicationFormDataService.ComboFormBean.REF_APPLICATION_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD);
                                                   }]
                                                }
                                            }
                                        }
                                    })
                  .state("applicationForm.appVernacular",{
                                        url: "/appVernacular",
                                        params: {"COMBO_ID": null},
                                        views:{
                                            'app-tab-content': {
                                                templateUrl: "applicationForm/appOutput/appVernacular/appVernacular.html",
                                                controller: "VernacularCtrl as vern",
                                                 resolve:{
                                                    IDProofList: ['LoadAppProofList', function(LoadAppProofList){
                                                                    console.log("resolve getIDProofList");
                                                                    return LoadAppProofList.getIDProofList();
                                                    }],
                                                     SISFormData: ['SisFormService', 'SfaPage1Service', '$stateParams',
                                                                    function(SisFormService, SfaPage1Service, $stateParams){
                                                                    console.log("SISFormData :"+$stateParams.AGENT_CD);
                                                                        if(!!$stateParams.SIS_ID)
                                                                            return SisFormService.loadSavedSISData($stateParams.AGENT_CD, $stateParams.SIS_ID, $stateParams.OPP_ID, $stateParams.FF_FHR_ID, $stateParams.LEAD_ID);
                                                                            else
                                                                            return null;
                                                    }]
                                                 }
                                            }

                                        }
                                    })
/************ Agent Details ****************/
     .state("agentDetails",{
        url:"/agentDetails",
        abstract :true,
        params: {"DOC_ID":"6033010334","AGENT_CD": null, "APPLICATION_ID": null, "FHR_ID": null, "SIS_ID":null, "POLICY_NO": null},
        templateUrl :"agentDetails/agentDetails.html",
        controller: "AgentDetailsCtrl as adc",
        resolve:{
           sisData :
           ['SisFormService','$stateParams',function(SisFormService,$stateParams){
                 return SisFormService.loadSavedSISData($stateParams.AGENT_CD,$stateParams.SIS_ID);
               }
           ],
           tickData:
           ['AgentDetailsService',function(AgentDetailsService){
                 debug("inside tick flags method resolved");
                 return AgentDetailsService.setTimeLineTicks();
               }
           ]
        }
     })

       .state("agentDetails.spDetails",{
           url:"/spDetails",
           params: {"DOC_ID":"6033010334","AGENT_CD": null, "APPLICATION_ID": null, "FHR_ID": null, "SIS_ID":null, "POLICY_NO": null,"REF_SIS_ID":null,"REF_FHRID":null,"REF_OPP_ID":null,"COMBO_ID":null,"REF_APPLICATION_ID":null},
           views: {
               'agent-tab-content': {
                   templateUrl :"agentDetails/spDetails/spDetails.html",
                   controller: "SpDetailsCtrl as spdc",
                   resolve: {
                       LoadData: ['$stateParams','SpDetailsService',
                           function($stateParams,SpDetailsService){
                               return SpDetailsService.getLoadData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD).then(
                                   function(resArr){
                                       var LoadData = {};
                                       LoadData.AGENT_CD = $stateParams.AGENT_CD;
                                       LoadData.FHR_ID = $stateParams.FHR_ID;
                                       LoadData.SIS_ID = $stateParams.SIS_ID;
                                       LoadData.APPLICATION_ID = $stateParams.APPLICATION_ID;
                                       LoadData.POLICY_NO = $stateParams.POLICY_NO;
                                       if(!!resArr){
                                           LoadData.resArr = resArr;
                                           debug("LoadData 1: " + JSON.stringify(LoadData));
                                           return LoadData;
                                       }
                                       else{
                                           LoadData.resArr = null;
                                           debug("LoadData 2: " + JSON.stringify(LoadData));
                                           return LoadData;
                                       }
                                   }
                               );
                           }
                       ],
                       sisData :
                       ['SisFormService','$stateParams',function(SisFormService,$stateParams){
                           return SisFormService.loadSavedSISData($stateParams.AGENT_CD,$stateParams.SIS_ID);
                         }
                       ],
                       OppData : ['$stateParams', 'SpDetailsService',function($stateParams, SpDetailsService){
                                return SpDetailsService.getTermData($stateParams.AGENT_CD, $stateParams.FHR_ID, $stateParams.SIS_ID, $stateParams.APPLICATION_ID, $stateParams.POLICY_NO);
                            }
                       ]
                   }
               }
           }

       })

        .state("agentDetails.ccrQues",{
           url:"/ccrQues",
           params: {"DOC_ID":"6033010334","AGENT_CD": null, "APPLICATION_ID": null, "FHR_ID": null, "SIS_ID":null, "POLICY_NO": null,"REF_SIS_ID":null,"REF_FHRID":null,"REF_OPP_ID":null,"COMBO_ID":null,"REF_APPLICATION_ID":null},
           views: {
               'agent-tab-content': {
                   templateUrl :"agentDetails/ccrQues/ccrQues.html",
                   controller: "CcrQuesCtrl as ccrC",
                   resolve: {
                       LoadData: ['$stateParams','CcrQuesService',
                           function($stateParams,CcrQuesService){
                               return CcrQuesService.getCcrData($stateParams.DOC_ID,$stateParams.APPLICATION_ID,$stateParams.AGENT_CD).then(
                                   function(resArr){
                                       var LoadData = {};
                                       LoadData.agentCd = $stateParams.AGENT_CD;
                                       LoadData.appId = $stateParams.APPLICATION_ID;
                                       LoadData.docId = $stateParams.DOC_ID;
                                       LoadData.polNo = $stateParams.POLICY_NO;
                                       if(!!resArr){
                                           LoadData.isOPGEN = true;
                                           debug("LoadData 1: " + JSON.stringify(LoadData));
                                           return LoadData;
                                       }
                                       else{
                                           LoadData.isOPGEN = false;
                                           debug("LoadData 2: " + JSON.stringify(LoadData));
                                           return LoadData;
                                       }
                                   }
                               );
                           }
                       ],
                        appData:['uploadDocService','ApplicationFormDataService','LoginService','$stateParams','CcrQuesService',function(uploadDocService,ApplicationFormDataService,LoginService,$stateParams,CcrQuesService){
                               var COMBO_ID = $stateParams.COMBO_ID;
                                debug("$stateParams "+JSON.stringify($stateParams));
                             //  var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID
                               console.log("sisDataTermPlan COMBO_ID"+COMBO_ID);
                                 if(!!COMBO_ID){
                                      CcrQuesService.setAppDataMYOPPORTUNITY(uploadDocService.getAppIDFrom_LP_MYOPPORTUNITY(COMBO_ID));
                                    return  uploadDocService.getAppIDFrom_LP_MYOPPORTUNITY(COMBO_ID);
                                 }
                                 else{
                                     return null;
                                 }
                            }
                        ],
                        DocumentDescription:['$stateParams','CcrQuesService',
                            function($stateParams,CcrQuesService){
                               return CcrQuesService.get_DOCUMENT_DESCRIPTION($stateParams.DOC_ID).then(
                                   function(res){
                                       debug(" DocumentDescription response: " + JSON.stringify(res));
                                       return res;
                                   }
                               );
                           }

                       ],
                       IsSelf: ['$stateParams','CcrQuesService',
                            function($stateParams,CcrQuesService){

                               return CcrQuesService.get_INSURANCE_BUYING_FOR_CODE($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,$stateParams.SIS_ID).then(
                                   function(res){
                                       debug("res NameData: " + JSON.stringify(res));
                                       return res;
                                   }
                               );
                           }
                       ],

                       NameData: ['$stateParams','CcrQuesService',
                            function($stateParams,CcrQuesService){
                                debug("resolving NameData");
                               return CcrQuesService.getCcrNameData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,$stateParams.SIS_ID).then(
                                   function(res){
                                       debug("res NameData: " + JSON.stringify(res));
                                       var NameData = {};
                                       if(!!res){
                                           NameData.INSURED_NAME = res.INSURED_NAME;
                                           NameData.PROPOSER_NAME = res.PROPOSER_NAME;
                                       }
                                       return NameData;
                                   }
                               );
                           }
                       ],
                        TermData : ['LoadApplicationScreenData','$stateParams',
                            function(LoadApplicationScreenData, $stateParams){
                                debug("resolve TermData");
                                return LoadApplicationScreenData.loadOppFlags($stateParams.APPLICATION_ID, $stateParams.AGENT_CD,'app').then(function(OppData){
                                    return OppData.oppFlags;
                                });
                            }
                        ],
                        IsAgentRelated: ['$stateParams','CcrQuesService',
                                                     function($stateParams,CcrQuesService){
                                                         debug("APPLICATION_ID is"+$stateParams.APPLICATION_ID);
                                                        // checkIsRelatedFlag
                                                        return CcrQuesService.getIsAgentRelatedInformation($stateParams.APPLICATION_ID).then(
                                                            function(res){
                                                                debug("res NameData: responce IsAgentRelated " + JSON.stringify(res));
                                                               // debug("responce in case default value :  " + CcrQuesService.getDefaultAgentRelatedData());
                                                                 if(!!res){
                                                                     return res;
                                                                 }else{
                                                                     return CcrQuesService.getDefaultAgentRelatedData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,$stateParams.POLICY_NO,"N");
                                                                 }
                                                            }
                                                        );
                                                    }
                        ],
                        IsDocSuccessfullyUploaded: ['$stateParams','CcrQuesService',
                             function($stateParams,CcrQuesService){
                                 debug("APPLICATION_ID is IsDocSuccessfullyUploaded"+$stateParams.APPLICATION_ID);
                                     var appDataMyOpportunity = CcrQuesService.getAppDataMYOPPORTUNITY();
                                     var refernceApplicationID = appDataMyOpportunity.REF_APPLICATION_ID;
                                     var isAgentRelated = CcrQuesService.getAgentRelFlagObject();
                                     debug("isAgentRelated "+JSON.stringify(isAgentRelated));
                                     debug("refernceApplicationID "+JSON.stringify(refernceApplicationID));
                                     debug("appDataMyOpportunity "+JSON.stringify(appDataMyOpportunity));
                                      return CcrQuesService.getIsAgentRelatedInformation($stateParams.APPLICATION_ID).then(
                                        function(res){
                                            debug("res NameData: responce IsAgentRelated " + JSON.stringify(res));
                                             if(!!res){
                                                  return CcrQuesService.checkIfAgentRelated(res,$stateParams.DOC_ID,$stateParams.AGENT_CD,$stateParams.APPLICATION_ID,$stateParams.COMBO_ID,refernceApplicationID);

                                             }else{
                                                 var defaultResponse = CcrQuesService.getDefaultAgentRelatedData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,$stateParams.POLICY_NO,"N");
                                                 return CcrQuesService.checkIfAgentRelated(defaultResponse,$stateParams.DOC_ID,$stateParams.AGENT_CD,$stateParams.APPLICATION_ID,$stateParams.COMBO_ID,refernceApplicationID);

                                             }
                                        }
                                    );
                             }
                        ]
                   }
               }
           }
       })
       .state("onlinePayment",{
            url:"/onlinePayment",
            params:{"appid":null},
            templateUrl: "onlinePayment/onlinePaymentDone.html",
            controller: "onlinePayCtrl as ol",
            resolve:{
               sisData :
               ['SisFormService','$stateParams',function(SisFormService,$stateParams){
                     return SisFormService.loadSavedSISData($stateParams.AGENT_CD,$stateParams.SIS_ID);
                   }
               ],
                sisDataTermPlan :
                ['SisFormService','LoginService','ApplicationFormDataService','paymentDetService',function(SisFormService,LoginService,ApplicationFormDataService,paymentDetService){
                    debug("SIS IS "+ApplicationFormDataService.SISFormData.sisMainData.SIS_ID);
                    debug("LOGIN ID "+LoginService.lgnSrvObj.userinfo.AGENT_CD);
                    var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID;
                    console.log("sisDataTermPlan COMBO_ID"+COMBO_ID);
                    var termPlanData;
                    if(COMBO_ID!=undefined){
                        return paymentDetService.getDataFrom_LP_MYOPPORTUNITY(COMBO_ID);
                    }else{
                        return null;
                    }
                }
                ]
            }
       })

       .state("agentDetails.finalSubmission",{
           url:"/finalSubmission",
           params: {"AGENT_CD": null, "APPLICATION_ID": null, "FHR_ID": null, "SIS_ID":null, "POLICY_NO": null,"REF_SIS_ID":null,"REF_FHRID":null,"REF_OPP_ID":null,"COMBO_ID":null,"REF_APPLICATION_ID":null},
           views: {
               'agent-tab-content': {
                   templateUrl :"agentDetails/finalSubmission/finalSubmission.html",
                   controller: "FinalSubmissionCtrl as fsc",
                   resolve:{
                       oppParams:['FinalSubmissionService','$stateParams', function(FinalSubmissionService, $stateParams){
                            return FinalSubmissionService.loadOppFlags($stateParams.APPLICATION_ID, $stateParams.AGENT_CD,'app').then(function(oppData){
                                       console.log(" oppData:"+JSON.stringify(oppData));
                                    if(!!oppData.oppFlags)
                                        return {"AGENT_CD": $stateParams.AGENT_CD, "APPLICATION_ID": $stateParams.APPLICATION_ID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID": $stateParams.SIS_ID, "POLICY_NO":  $stateParams.POLICY_NO,"REF_SIS_ID": oppData.oppFlags.REF_SIS_ID,"REF_FHRID": oppData.oppFlags.REF_FHRID,"REF_OPP_ID": $stateParams.REF_OPP_ID,"COMBO_ID":oppData.oppFlags.COMBO_ID,"REF_APPLICATION_ID":oppData.oppFlags.REF_APPLICATION_ID,"REF_POLICY_NO":oppData.oppFlags.REF_POLICY_NO};
                                    else
                                        return {"AGENT_CD": $stateParams.AGENT_CD, "APPLICATION_ID": $stateParams.APPLICATION_ID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID": $stateParams.SIS_ID, "POLICY_NO":  $stateParams.POLICY_NO,"REF_SIS_ID": null,"REF_FHRID": null,"REF_OPP_ID": $stateParams.REF_OPP_ID,"COMBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":null};
                            });
                        }],
                       AgentData : ['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                           return FinalSubmissionService.getAgentImpData(oppParams.APPLICATION_ID, oppParams.AGENT_CD);
                       }],
                       APPFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                           return FinalSubmissionService.viewFinalReport(oppParams);
                         }],
                       SISFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                           return FinalSubmissionService.loadSISReport(oppParams);
                         }],
                       FFFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                          return FinalSubmissionService.loadFFNReport(oppParams);
                        }],
                       CCRFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                          return FinalSubmissionService.loadCCRReport(oppParams);
                        }],
                       sisData :['SisFormService','oppParams',function(SisFormService,oppParams){
                         return SisFormService.loadSavedSISData(oppParams.AGENT_CD,oppParams.SIS_ID);
                        }],
                       TermAPPFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService,oppParams){
                           console.log("calling viewTermFinalReport"+JSON.stringify(oppParams));
                                return FinalSubmissionService.viewTermFinalReport(oppParams);

                         }],
                       TermSISFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                           console.log("calling loadTermSISReport");
                                return FinalSubmissionService.loadTermSISReport(oppParams);
                         }],
                       TermFFFileData :['FinalSubmissionService','oppParams', function(FinalSubmissionService, oppParams){
                          console.log("calling loadTermFFNReport");
                                return FinalSubmissionService.loadTermFFNReport(oppParams);
                        }],
                       PscFlag:['FinalSubmissionService','$stateParams',function(FinalSubmissionService,$stateParams){
                                  return FinalSubmissionService.getPscFlag($stateParams.APPLICATION_ID,$stateParams.AGENT_CD).then(
                                   function(pscFlag){
                                   debug("resloveFlag"+JSON.stringify(pscFlag));
                                      return pscFlag;
                                   })
                       }]
                   }
               }
           }

       })

       .state("hswSIP", {
        url: "/hswSIP",
        cache: false,
        templateUrl: "hsw/hswSIP/hswSIP.html",
        controller: "hswPagesCtrl as hswCtrl",
        params: {"PROD_NAME": null, "SOL_TYPE": null, "PGL_ID": null, "FHR_ID": null, "LEAD_ID": null, "OPP_ID": null},
        resolve: {
            HswPlanGroupData:  ['hswService','$stateParams',
                function(hswService,$stateParams){
                    debug("PlanGroupDetail Service called");
                    return hswService.setHSWPlanGroupDetails($stateParams.PGL_ID);
                }
            ],
            HswPlanGroupFeatureData:  ['hswService',
                function(hswService){
                    debug("FeatureDetail Service called");
                    return hswService.getHSWPlanGroupFeatureDetails();
                }
            ],
            HswPlanDetails:  ['hswService',
                function(hswService){
                    debug("PlanDetail Service called");
                    return hswService.getHSWPlanDetails();
                }
            ],
            HswPnlTermppt:  ['hswService',
                function(hswService){
                    debug("PlanTermPPT Service called");
                    return hswService.getHSWPnltermppt();
                }
            ],
            ProductDetails: ['$stateParams',
                function($stateParams){
                    return {"SOL_TYPE": $stateParams.SOL_TYPE, "PROD_NAME": $stateParams.PROD_NAME, "PGL_ID": $stateParams.PGL_ID, "FHR_ID": $stateParams.FHR_ID, "LEAD_ID": $stateParams.LEAD_ID, "OPP_ID": $stateParams.OPP_ID};
                }
            ]
        }
    })

    .state("hswULIP", {
        url: "/hswULIP",
        cache: false,
        templateUrl: "hsw/hswULIP/hswULIP.html",
        controller: "hswULIPagesCtrl as hswULIP",
        params: {"PROD_NAME": null, "SOL_TYPE": null, "PGL_ID": null, "FHR_ID": null, "LEAD_ID": null, "OPP_ID": null},
        resolve: {
            HswPlanLK:  ['hswULIPService','$stateParams',
                function(hswULIPService,$stateParams){
                    return hswULIPService.getHSWPlanLK($stateParams.PGL_ID);
                }
            ],
            ProductDetails: ['$stateParams',
                function($stateParams){
					return {"SOL_TYPE": $stateParams.SOL_TYPE, "PROD_NAME": $stateParams.PROD_NAME, "PGL_ID": $stateParams.PGL_ID, "FHR_ID": $stateParams.FHR_ID, "LEAD_ID": $stateParams.LEAD_ID, "OPP_ID": $stateParams.OPP_ID};
                }
            ]
        }
	})
    .state("fhrMain", {       // FHR Routing start here - Ameya
        url: "/fhrMain",
        cache: false,
        templateUrl: "fhr/fhrCommon.html",
        controller: "FhrCtrl as fhrm"
    })
    .state("fhrPersonalDetails", {
        url: "/fhrPersonalDetails",
        cache: false,
        templateUrl: "fhr/personalDetails/personalDetails.html",
        controller: "FhrPersonalDetailsCtrl as persd",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            LeadData: ['LoadApplicationScreenData','$stateParams', function(LoadApplicationScreenData, $stateParams){
                console.log("resolve loadLeadTableData"+$stateParams.LEADID);
                return LoadApplicationScreenData.loadLeadTableData($stateParams.LEADID, $stateParams.AGENTCD);
            }],
            PersonalDetailsExistingData: ['FhrService','$stateParams', function(FhrService, $stateParams){
                if($stateParams.FHRID){
                    console.log("resolve PersonalDetailsData"+$stateParams.FHRID);
                    return FhrService.setPersonalDetailsDataInFhrMain($stateParams.FHRID, $stateParams.AGENTCD);
                }else{
                    console.log("resolve PersonalDetailsData no FHR");
                    return null;
                }
            }],
            StateList: ['FhrPersonalDetailsService',
                function(FhrPersonalDetailsService){
                    debug("resolving getStateList");
                    return FhrPersonalDetailsService.getStateList();
                }
            ],
            CityList: ['LeadData', 'FhrPersonalDetailsService', 'PersonalDetailsExistingData',
                function(LeadData, FhrPersonalDetailsService,PersonalDetailsExistingData){
                    debug("resolving getCityList");
                    if(!!LeadData && PersonalDetailsExistingData === null){      // If Personal details are not yet saved
                        debug("No personal data");
                        return FhrPersonalDetailsService.getCityList(LeadData.CURNT_STATE);
                    }else if(!!LeadData && !!PersonalDetailsExistingData){        // If Personal details are already saved
                        debug("Personal data found");
                        return FhrPersonalDetailsService.getCityList(PersonalDetailsExistingData[0].STATE_CODE);
                    }else{
                        debug("Else of getCityList");
                        return FhrPersonalDetailsService.getCityList(0);
                    }
                }
            ],
            FhrRelationList: ['FhrService',
                function(FhrService){
                    debug("resolving fhrRelationList");
                    return FhrService.setFhrRelationList();
                }
            ],
            FHR_ID: ['$stateParams',
                function($stateParams){
                    debug("Stateparams are "+JSON.stringify($stateParams));
                    return $stateParams.FHRID;
                }
            ],
            LEAD_ID: ['$stateParams',
                function($stateParams){
                    return $stateParams.LEADID;
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.setGoalsWiseCount($stateParams);
                }
            ],
            ExistingChildGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllChildGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingProtectionGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllProtectionGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingRetirementGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllRetirementGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingWealthGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllWealthGoalsDataRelatedFHRId($stateParams);
                }
            ],
            SetOutputDataInMain : ['FhrService','GoalsOutputService',
                function(FhrService, GoalsOutputService){
                    var outputData = GoalsOutputService.getOutputData();
                    return FhrService.setOutputDetailsDataInFhrMain(outputData);
                }
            ],
            EKYCData: ['SisFormService', '$stateParams','LoginService',
                        function(SisFormService, $stateParams, LoginService){
                            return SisFormService.getEKYCData(LoginService.lgnSrvObj.userinfo.AGENT_CD, $stateParams.LEADID);
                        }
                    ]
        }
    })
    .state("fhrOthers", {
        url: "/fhrOthers",
        cache: false,
        templateUrl: "fhr/otherDetails/otherDetails.html",
        controller: "FhrOtherDetailsCtrl as fhrother",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            FHR_ID: ['$stateParams',
                function($stateParams){
                    return $stateParams.FHRID;
                }
            ],
            LEAD_ID: ['$stateParams',
                function($stateParams){
                    return $stateParams.LEADID;
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ]
        }
    })
    .state("fhrGoals", {
        url: "/fhrGoals",
        cache: false,
        templateUrl: "fhr/goals/goalsMain.html",
        controller: "FhrGoalsMainCtrl as fhrgoals",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            stateParams: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    FhrGoalsMainService.setStateParams($stateParams);
                    return $stateParams;
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ],
            PersonalDetailsExistingData: ['FhrService','$stateParams', function(FhrService, $stateParams){
                if($stateParams.FHRID){
                    console.log("resolve PersonalDetailsData"+$stateParams.FHRID);
                    return FhrService.setPersonalDetailsDataInFhrMain($stateParams.FHRID, $stateParams.AGENTCD);
                }else{
                    console.log("resolve PersonalDetailsData no FHR");
                    return null;
                }
            }]
        }
    })
    .state("childGoals", {
        url: "/fhrChildGoals",
        cache: false,
        templateUrl: "fhr/goals/childGoals/childGoals.html",
        controller: "FhrChildGoalsCtrl as fhrchlg",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ExistingChildGoalsData : ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getChildGoalsRelatedFHR($stateParams);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ]
        }
    })
    .state("retireFund", {
        url: "/fhrRetireFund",
        cache: false,
        templateUrl: "fhr/goals/retirementFund/retirementFund.html",
        controller: "FhrRetireFundCtrl as fhrretfund",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ExistingRetireFundGoalsData : ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getRetirementFundGoalsRelatedFHR($stateParams);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ]
        }
    })
    .state("protection", {
        url: "/fhrProtection",
        cache: false,
        templateUrl: "fhr/goals/protection/protection.html",
        controller: "FhrProtectionCtrl as fhrprot",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ExistingProtectionGoalsData : ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getProtectionGoalsRelatedFHR($stateParams);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ]
        }
    })
    .state("wealthCreation", {
        url: "/fhrWealthCreation",
        cache: false,
        templateUrl: "fhr/goals/wealthCreation/wealthCreation.html",
        controller: "FhrWealthCreationCtrl as fhrweacreat",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ExistingWealthGoalsData : ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getWealthGoalsRelatedFHR($stateParams);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ]
        }
    })
    .state("goalsSuccess", {
        url: "/goalsSuccess",
        cache: false,
        templateUrl: "fhr/goals/goalsSuccess.html",
        controller: "FhrGoalsSuccessCtrl as fhrsuccessgoals",
        params :{GOALTYPE:null,LEADID:null,FHRID:null, AGENTCD: null, OPPID: null},
        resolve : {
            stateParams: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    FhrGoalsMainService.setStateParams($stateParams);
                    return $stateParams;
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.setGoalsWiseCount($stateParams);
                }
            ]
        }
    })
    .state("goalsOutput", {
        url: "/goalsOutput",
        cache: false,
        templateUrl: "fhr/goals/goalsOutput/goalsOutput.html",
        controller: "GoalsOutputCtrl as goalsout",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ExistingChildGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllChildGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingProtectionGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllProtectionGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingRetirementGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllRetirementGoalsDataRelatedFHRId($stateParams);
                }
            ],
            ExistingWealthGoalsData : ['$stateParams','GoalsOutputService',
                function($stateParams, GoalsOutputService){
                    return GoalsOutputService.setAllWealthGoalsDataRelatedFHRId($stateParams);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount();
                }
            ],
            PersonalDetailsExistingData: ['FhrService','$stateParams', function(FhrService, $stateParams){
                if($stateParams.FHRID){
                    console.log("resolve PersonalDetailsData"+$stateParams.FHRID);
                    return FhrService.setPersonalDetailsDataInFhrMain($stateParams.FHRID, $stateParams.AGENTCD);
                }else{
                    console.log("resolve PersonalDetailsData no FHR");
                    return null;
                }
            }]
        }
    })
    .state("fhrRecommendations", {
        url: "/fhrRecommendations",
        cache: false,
        templateUrl: "fhr/recommendations/recommendations.html",
        controller: "FhrRecommendationsCtrl as fhrrecomm",
        params :{LEADID:null,FHRID :null, AGENTCD: null, OPPID: null},
        resolve : {
            ProdRecommData : ['$stateParams','FhrRecommendationsService',
                function($stateParams, FhrRecommendationsService){
                    var fhrDetails = { LEAD_ID: $stateParams.LEADID, OPP_ID: $stateParams.OPPID, FHR_ID: $stateParams.FHRID, AGENT_CD: $stateParams.AGENTCD };
                    return FhrRecommendationsService.getAllRecommendationsForFhr(fhrDetails);
                }
            ],
            CategoryWiseGoalsCount: ['$stateParams','FhrGoalsMainService',
                function($stateParams, FhrGoalsMainService){
                    return FhrGoalsMainService.getGoalsWiseCount($stateParams);
                }
            ],
            PersonalDetailsExistingData: ['FhrService','$stateParams', function(FhrService, $stateParams){
                if($stateParams.FHRID){
                    console.log("resolve PersonalDetailsData"+$stateParams.FHRID);
                    return FhrService.setPersonalDetailsDataInFhrMain($stateParams.FHRID, $stateParams.AGENTCD);
                }else{
                    console.log("resolve PersonalDetailsData no FHR");
                    return null;
                }
            }]
        }
    })
    .state("agentInfo", {
                url: "/agentInfo",
                templateUrl: "agentInfo/agentInfo.html",
                params: {"DOC_ID":"6033010334","AGENT_CD": null, "APPLICATION_ID": null, "FHR_ID": null, "SIS_ID":null, "POLICY_NO": null},
                controller: "agentInfoCtrl as aic"
//                resolve:{
//                   sisData :
//                   ['SisFormService','$stateParams',function(SisFormService,$stateParams){
//                         return SisFormService.loadSavedSISData($stateParams.AGENT_CD,$stateParams.SIS_ID);
//                       }
//                   ],
//                   tickData:
//                   ['AgentInfoService',function(AgentInfoService){
//                         debug("inside tick flags method resolved");
//                         return AgentInfoService.setTimeLineTicks();
//                       }
//                   ]
//                }
        })
    .state("showdatabase", {
            url: "/showdatabase",
            templateUrl: "showdatabase/showdatabase.html",
            controller: "showDatabaseCtrl as showDBCtrl",
            cache: false,
            resolve : {
                allDbTableNames :  ['ShowDBService',
                  function(ShowDBService){
                      debug("getAllTableName function in ShowDBService Service called");
                      return ShowDBService.getAllTableName();
                  }
              ],
        }
    })
    .state("pscPage1", {
            url: "/pscPage1",
            templateUrl: "initiatePSC/pscPage1/pscPage1.html",
            controller: "pscPage1Ctrl as pscCtrl",
            cache: false,
            params: {"AGENT_CD": null, "LEAD_ID": null,"SIS_ID": null,"PGL_ID": null, "INS_SEQ_NO": null, "SOL_TYPE": null, "PROD_NAME": null, "OPP_ID": null},
                resolve:{
                    SISData: ['$stateParams', 'SisFormService',
											function($stateParams, SisFormService){
												debug("stateParams ::"+JSON.stringify($stateParams))
												if(!!$stateParams.SIS_ID)
													return SisFormService.loadSavedSISData($stateParams.AGENT_CD, $stateParams.SIS_ID);
												else
													return null;
											}
										],
                    loadEKYCOpportunityData:['SisEKycService', '$stateParams',
                         function(SisEKycService, $stateParams){
                             try{
                                 if(!!$stateParams.OPP_ID)
                                    return SisEKycService.loadEKYCOpportunityData($stateParams.OPP_ID, $stateParams.LEAD_ID, $stateParams.AGENT_CD);

                                 else
                                    return null;
                             }catch(ex){
                                 debug("loadEKYCOpportunityData Exception: " + ex.message);
                                 return null;
                             }
                         }
                     ],
                    LeadData: ['LoadApplicationScreenData','$stateParams', function(LoadApplicationScreenData, $stateParams){
                        console.log("resolve loadLeadTableData"+$stateParams.LEAD_ID);
                        return LoadApplicationScreenData.loadLeadTableData($stateParams.LEAD_ID, $stateParams.AGENT_CD);
                    }]
                }
    })
		.state("pscPage2", {
            url: "/pscPage2",
            templateUrl: "initiatePSC/pscPage2/pscPage2.html",
            controller: "PSCPage2Ctrl as pol",
            cache: false,
           resolve : {
               PolicyNoData :  ['InitiatePSCService',
                 function(InitiatePSCService){
									 return InitiatePSCService.isPolicyNoAssigned();
                 }
             ],
						 TermPolicyNoData: ['InitiatePSCService','PSCComboPolicyNoService','ApplicationFormDataService','$stateParams', function(InitiatePSCService, PSCComboPolicyNoService, $stateParams){
								var COMBO_ID =  InitiatePSCService.PSCModuleData.COMBO_ID;
							 console.log("resolve PSCComboPolicyNoService"+COMBO_ID);
							 if(COMBO_ID!=undefined)
									 return PSCComboPolicyNoService.loadComboDtls(COMBO_ID);
							 else
									 return null;
						 }]
       }
    })
		.state("pscPage3", {
						url: "/pscPage3",
						templateUrl: "initiatePSC/pscPage3/pscPage3.html",
						controller: "PSCPage3Ctrl as psc",
						cache: false,
	            resolve : {
								PSCExistingData:['PSCService', 'InitiatePSCService', function (PSCService, InitiatePSCService) {
											return PSCService.getPscExistingData(InitiatePSCService.PSCModuleData.APPLICATION_ID, InitiatePSCService.PSCModuleData.AGENT_CD);
								}]
	        }
		});

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise("login");
}]);
