productLandingPageModule.controller('ProductLandingPageCtrl',['$state', 'ProductDetails', 'LoginService', 'SisFormService', 'CommonService', 'SavedSISListingService', 'FHR_ID', 'LEAD_ID', 'OPP_ID', 'ProductLandingPageService', function($state, ProductDetails, LoginService, SisFormService, CommonService, SavedSISListingService, FHR_ID, LEAD_ID, OPP_ID, ProductLandingPageService){
	"use strict";
	CommonService.hideLoading();
	this.solutionType = (!!ProductDetails)?ProductDetails.solutionType:"";
	this.productName = (!!ProductDetails)?ProductDetails.productName:"";
	this.pglId = (!!ProductDetails)?ProductDetails.PGL_ID:"";


	debug("this.solutionType: " + this.solutionType);

	if(this.solutionType=="Protection"){
		this.imgIcon = "icon-39";
		this.imgColor = "red-clr";
		this.solBg = "red-clr-bg";
		if(this.pglId=='224'){
			this.imgIcon = "sr-lg";
			this.imgColor = "red-clr blue-clr";
			this.solBg = "red-clr-bg blue-clr-bg";
		}
		if(this.pglId=='225'){
			this.imgIcon = "srp-lg";
			this.imgColor = "red-clr gold-clr";
			this.solBg = "gold-clr-bg";
		}
	}
	else if(this.solutionType=="Savings"){
		this.imgIcon = "icon-12";
		this.imgColor = "yellow-clr";
		this.solBg = "yellow-clr-bg";
	}
	else if(this.solutionType=="Wealth"){
		this.imgIcon = "icon-3";
		this.imgColor = "blue-clr";
		this.solBg = "blue-clr-bg";
	}
	else if(this.solutionType=="Child"){
		this.imgIcon = "icon-14";
		this.imgColor = "skyblue-clr";
		this.solBg = "skyblue-clr-bg";
	}
	else if(this.solutionType=="Retirement"){
		this.imgIcon = "icon-7";
		this.imgColor = "purple-clr";
		this.solBg = "purple-clr-bg";
	}
	else if(this.solutionType=="Health"){
		this.imgIcon = "health-lg";
		this.imgColor = "green-clr";
		this.solBg = "green-clr-bg";
	}

	debug("this.imgIcon: " + this.imgIcon);
	debug("this.imgColor: " + this.imgColor);
	debug("this.solBg: " + this.solBg);

	this.gotoSIS = function(){
		CommonService.showLoading("Loading...please wait...");
		debug("ProductDetails.PGL_ID: " + ProductDetails.PGL_ID);
		SisFormService.generateSIS(LoginService.lgnSrvObj.userinfo.AGENT_CD, ProductDetails.PGL_ID, LEAD_ID, FHR_ID, null, null, null, this.solutionType, this.productName, OPP_ID);
	};

	this.onBack = function(){
		CommonService.showLoading("Loading...");
		$state.go("dashboard.solutions");
	};

	this.viewFile = function(type){
		CommonService.showLoading("Loading...please wait...");
		ProductLandingPageService.viewFile(type, this.pglId);
	};


	this.openSISListing = function(pglId){
		CommonService.showLoading("Loading...please wait...");
		debug("pglId: " + pglId);
		SavedSISListingService.getSavedSISListing(LoginService.lgnSrvObj.userinfo.AGENT_CD, pglId).then(
			function(res){
				if(!!res){
					$state.go("dashboard.savedSISListing",{"PGL_ID": pglId, "SIS_LISTING": res});
				}
				else{
					CommonService.hideLoading();
					navigator.notification.alert("No SIS reports available.",null,"SIS Report","OK");
				}
			}
		);
	};

}]);

productLandingPageModule.service('ProductLandingPageService',['CommonService',function(CommonService){
	this.viewFile = function(type, pglId){
		CommonService.selectRecords(db,"lp_doc_fetch_user_master",true,"doc_name, doc_type, doc_path",{PGL_ID: pglId, DOC_TYPE: type}).then(
			function(res){
				if(!!res && res.rows.length>0){
					cordova.exec(
						function(fileName){
							debug("fileName: " + fileName);
							CommonService.openFile(fileName);
						},
						function(errMsg){
							CommonService.hideLoading();
							debug(errMsg);
						},
						"PluginHandler","getDecryptedFileName",[(res.rows.item(0).DOC_NAME + ((res.rows.item(0).DOC_TYPE==".pps")?".pdf":res.rows.item(0).DOC_TYPE)), res.rows.item(0).DOC_PATH]
					);
				}
				else{
					CommonService.hideLoading();
					navigator.notification.alert("No file available.",null,"SIS","OK");
				}
			}
		);
	}
}]);
