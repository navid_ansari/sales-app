savedSISListingModule.controller('SavedSISListingCtrl',['$state','SavedSISList', 'CommonService', 'ViewReportService', '$stateParams', function($state, SavedSISList, CommonService, ViewReportService, $stateParams){
	CommonService.hideLoading();
	this.sisListing = SavedSISList;

	this.viewSIS = function(sisId, custEmailId){
		debug("sisId: " + sisId);
		debug("custEmailId: " + custEmailId);
		//CommonService.showLoading("Loading report...please wait...");
		ViewReportService.fetchSavedReport("SIS",sisId,{CUST_EMAIL_ID: custEmailId}).then(
			function(res){
				CommonService.hideLoading();
				$state.go("viewReport", {'REPORT_TYPE': 'SIS', 'REPORT_ID': sisId, 'PREVIOUS_STATE': "dashboard.savedSISListing", "REPORT_PARAMS": {"CUST_EMAIL_ID": custEmailId}, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
			}
		);

	};

}]);

savedSISListingModule.service('SavedSISListingService', ['CommonService', '$q', function(CommonService, $q){

	this.getSavedSISListing = function(agentCd, pglId){
		debug("in getSavedSISListing " + agentCd + " " + pglId);
		var dfd = $q.defer();
		try{
			var savedSISList = [];
			var query = "select a.proposer_first_name as FN, a.proposer_middle_name as MN, a.proposer_last_name as LN, a.proposer_email, b.* from lp_sis_screen_a a, lp_sis_main b where (b.lead_id is null or b.lead_id=0) and pgl_id=? and a.sis_id = b.sis_id and b.agent_cd=? and a.agent_cd = b.agent_cd and (select 1 from lp_document_upload where doc_cat=? and doc_cat_id=b.sis_id) order by b.completion_timestamp desc";
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,query,[pglId, agentCd, 'SIS'],
						function(tx,res){
							if(!!res && res.rows.length>0){
								for(var i=0;i<res.rows.length;i++){
									var sis = {};
									var FN = res.rows.item(i).FN;
									var MN = res.rows.item(i).MN;
									var LN = res.rows.item(i).LN;
									sis.CUST_NAME = ((!!FN)?FN:"") + ((!!MN)?(" " + MN):"") + ((!!LN)?(" " + LN):"");
									sis.CREATED_DATE = (!!res.rows.item(i).COMPLETION_TIMESTAMP)?res.rows.item(i).COMPLETION_TIMESTAMP:"";
									sis.SIS_ID = res.rows.item(i).SIS_ID;
									sis.PGL_ID = res.rows.item(i).PGL_ID;
									sis.CUST_EMAIL_ID = res.rows.item(i).PROPOSER_EMAIL;
									sis.planColor = "plan-color" + ((i%6)+1);
									savedSISList.push(sis);
								}
								dfd.resolve(savedSISList);
							}
							else{
								dfd.resolve(null);
							}
						},
						function(tx,err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		catch(ex){
			debug("Exception: " + ex.message);
		}
		return dfd.promise;
	};

}]);
