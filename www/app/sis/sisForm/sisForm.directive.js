sisFormModule.controller('SisTimelineCtrl',['SisTimelineService', '$state', 'SisFormDService', 'SisFormService', function(SisTimelineService, $state, SisFormDService, SisFormService){
	"use strict";
	debug("in SisTimelineCtrl");
	this.timelineObj = [
		{'name': 'E-KYC', 'number': '1', 'type': 'ekyc', 'isTabComplete': SisTimelineService.isEKYCTabComplete},
		{'name': 'Insured', 'number': '2', 'type': 'insured', 'isTabComplete': SisTimelineService.isInsuredTabComplete},
		{'name': 'Proposer', 'number': '3', 'type': 'proposer', 'isTabComplete': SisTimelineService.isProposerTabComplete},
		{'name': 'Plan', 'number': '4', 'type': 'plan', 'isTabComplete': SisTimelineService.isPlanTabComplete},
	 	{'name': 'Fund', 'number': '5', 'type': 'fund', 'isTabComplete': SisTimelineService.isFundTabComplete},
	 	{'name': 'Rider', 'number': '6', 'type': 'rider', 'isTabComplete': SisTimelineService.isRiderTabComplete},
	 	{'name': 'Other', 'number': '6', 'type': 'otherDetails', 'isTabComplete': SisTimelineService.isOtherDetailsTabComplete},
		{'name': 'SIS', 'number': '7', 'type': 'output', 'isTabComplete': SisTimelineService.isOutputTabComplete}
	];
	debug("this.timelineObj: " + JSON.stringify(this.timelineObj));
	this.activeTab = SisTimelineService.getActiveTab();

	this.gotoEKYCDetailsTab = function(){
		if(!!SisTimelineService.isEKYCTabComplete)
			$state.go("sisForm.sisEKYC");
	};

	this.gotoInsuredDetailsTab = function(){
		if(!!SisTimelineService.isInsuredTabComplete)
			$state.go("sisForm.sfaPage1");
	};

	this.gotoProposerDetailsTab = function(){
		if(!!SisTimelineService.isProposerTabComplete && !!SisFormService.sisData && !!SisFormService.sisData.sisFormAData && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED!='Y'))
			$state.go("sisForm.sfaPage2");
	};

	this.gotoPlanDetailsTab = function(){
		if(!!SisTimelineService.isPlanTabComplete)
			$state.go("sisForm.sisFormB");
	};

	this.gotoFundDetailsTab = function(){
		if(!!SisTimelineService.isFundTabComplete)
			$state.go("sisForm.sisFormC");
	};

	this.gotoOtherDetailsTab = function(){
		if(!!SisTimelineService.isOtherDetailsTabComplete)
			$state.go("sisForm.sfePage1");
	};

	this.gotoRiderDetailsTab = function(){
		if(!!SisTimelineService.isRiderTabComplete){
			SisFormDService.getRidersList().then(
				function(ridersList){
					if(!!ridersList && ridersList.length>0){
						$state.go("sisForm.sisFormD",{"RIDER_LIST": ridersList});
					}
					else
						$state.go("sisForm.sisOutput");
				}
			);
		}
	};

	this.gotoSISOutputTab = function(){
		if(!!SisTimelineService.isOutputTabComplete)
			$state.go("sisForm.sisOutput");
	};

	this.onTabClick = function(type){
		debug("type: " + type);
		if(type=="ekyc")
			this.gotoEKYCDetailsTab();
		else if(type=="insured")
			this.gotoInsuredDetailsTab();
		else if(type=="proposer")
			this.gotoProposerDetailsTab();
		else if(type=="plan")
			this.gotoPlanDetailsTab();
		else if(type=="fund")
			this.gotoFundDetailsTab();
		else if(type=="rider")
			this.gotoRiderDetailsTab();
		else if(type=="otherDetails")
			this.gotoOtherDetailsTab();
		else if(type=="output")
			this.gotoSISOutputTab();
	};

}]);

sisFormModule.service('SisTimelineService', [function(){
	"use strict";
	debug("in SisTimelineService");

	this.activeTab = "insured";
	this.isEKYCTabComplete = false;
	this.isInsuredTabComplete = false;
	this.isProposerTabComplete = false;
	this.isPlanTabComplete = false;
	this.isFundTabComplete = false;
	this.isRiderTabComplete = false;
	this.isOtherDetailsTabComplete = false;
	this.isOutputTabComplete = false;

	this.getActiveTab = function(){
		return this.activeTab;
	};
	this.setActiveTab = function(activeTab){
		this.activeTab = activeTab;
	};
}]);

sisFormModule.directive('sisTimeline',[function() {
	"use strict";
	debug('in sisTimeline');
	return {
		restrict: 'E',
		controller: "SisTimelineCtrl",
		controllerAs: "st",
		bindToController: true,
		templateUrl: "sis/sisForm/sisTimeline.html"
	};
}]);


sisFormModule.directive('sisHeader',[function() {
	"use strict";
	debug('in sisHeader');
	return {
		restrict: 'E',
		controller: "SisFormCtrl",
		controllerAs: "shdr",
		bindToController: true,
		templateUrl: "sis/sisForm/sisHeader.html"
	};
}]);
