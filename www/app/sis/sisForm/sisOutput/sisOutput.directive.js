sisOutputModule.directive('viewSisOutput',['SisOutputService', function (SisOutputService) {
	"use strict";
	return {
		template:	function(tElement, tAttrs) {
						var sisData = SisOutputService.getSisData();

						debug("status of the is Term plan is "+SisOutputService.isTermOutput);
						debug("SisOutputService. getSisTAData() "+SisOutputService.getSisTAData());
						debug("SisOutputService. getSisData()"+SisOutputService.getSisData());
						if(BUSINESS_TYPE=="IndusSolution"){
							var CurrentPageIndex = SisOutputService.getCurrentPageIndex();
							debug("sisObject current page directive "+CurrentPageIndex);
							if(CurrentPageIndex == "COMBO"){
								var singleSISData = SisOutputService.getSingleSISData();
								return (!!singleSISData.SIS)?singleSISData.SIS:singleSISData;
							}
							else if(!!SisOutputService.isTermOutput){
								var sisTAData = SisOutputService.getSisTAData();
								return (!!sisTAData.SIS)?sisTAData.SIS:sisTAData;
							}
							else{
								return (!!sisData.SIS)?sisData.SIS:sisData;
							}
						}
						else{
							return (!!sisData.SIS)?sisData.SIS:sisData;
						}
					}
	};
}]);
