sisOutputModule.controller('SisOutputCtrl',['SisGenOutput', 'SisOutputService', 'CommonService', 'SisFormService', 'SisFormDService', '$state', 'SisTimelineService', function(SisGenOutput, SisOutputService, CommonService, SisFormService, SisFormDService, $state, SisTimelineService){
	"use strict";
	var sc = this;
	//this.init = function(){
		debug("sis output 1");
		CommonService.hideLoading();

    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/



		debug("sis output 2");
		SisTimelineService.setActiveTab("output");
		debug("sis output 3");

		sc.isTermOutput = SisOutputService.isTermOutput;
		sc.isTermAttached = (!!SisFormService.sisData.oppData.REF_SIS_ID);

		debug("this.isTermOutput: " + sc.isTermOutput);
		debug("this.isTermAttached: " + sc.isTermAttached);

		sc.sisOutput = SisFormService.sisData.sisOutput;
		sc.pglId = SisFormService.sisData.sisMainData.PGL_ID;
		sc.sisId = SisFormService.sisData.sisMainData.SIS_ID;
		sc.fhrId = SisFormService.sisData.sisMainData.FHR_ID;
		if(!!sc.isTermOutput){
			sc.sisOutput = SisFormService.sisData.sisTermOutput;
			sc.pglId = SisFormService.sisData.sisFormEData.PGL_ID;
			sc.sisId = SisFormService.sisData.oppData.REF_SIS_ID;
			sc.fhrId = SisFormService.sisData.oppData.REF_FHRID;
		}

		if(!!SisGenOutput){
			if(!!SisGenOutput.SIS){
				debug("SisOutput: " + SisGenOutput.SIS.length);
			}
			debug("AnnualPremiumST: " + SisGenOutput.AnnualPremiumST);
			if(!!sc.isTermOutput){
				var sisMainData = {};
				sisMainData.ANNUAL_PREMIUM_AMOUNT = SisGenOutput.AnnualPremiumST || null;
				sisMainData.MODAL_PREMIUM = SisGenOutput.ModalPremium || null;
				sisMainData.SERVICE_TAX = SisGenOutput.ServiceTax || "0";
				SisOutputService.updateTermSisMainTable(sisMainData, SisFormService.sisData.oppData.REF_SIS_ID, SisFormService.sisData.sisMainData.AGENT_CD);
			}
			else{
				SisFormService.sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT = SisGenOutput.AnnualPremiumST || null;
				SisFormService.sisData.sisMainData.MODAL_PREMIUM = SisGenOutput.ModalPremium || null;
				SisFormService.sisData.sisMainData.SERVICE_TAX = SisGenOutput.ServiceTax || "0";

				SisFormService.sisData.sisMainData.DISCOUNTED_MODAL_PREMIUM = SisGenOutput.DiscountedModalPremium || null;
				SisFormService.sisData.sisMainData.DISCOUNTED_SERVICE_TAX = SisGenOutput.DiscountedServiceTax || null;

				SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED = "Y";
				SisFormDService.updateSisScreenDTable(SisGenOutput.riderData);
				SisFormService.insertDataInSisMainTable();
			}
		}
		else{
			debug("SisOutput is null or undefined");
		}
		SisFormService.sisData.sisMainData.DISCOUNTED_MODAL_PREMIUM = SisGenOutput.DiscountedModalPremium || null;
		SisFormService.sisData.sisMainData.DISCOUNTED_SERVICE_TAX = SisGenOutput.DiscountedServiceTax || null;
		/*GIP SAR*/
		SisFormService.sisData.sisMainData.SAR = SisGenOutput.Sar || null;
		debug("sis signed: " + (!!sc.sisOutput.SIS_SIGNED));

		// DISABLE BELOW LINES TILL COMMENT START WHILE ENABLING SINGLE SIS

		this.disableSignature = !!this.sisOutput.SIS_SIGNED;

		this.disableProceed = !(!!this.sisOutput.SIS_SIGNED || ((this.pglId=="185") || (this.pglId=="191")));

		this.showSaveButton = ((!SisFormService.sisData.sisMainData.LEAD_ID) || (SisFormService.sisData.sisMainData.LEAD_ID==0));
		this.showSignatureButton = (!!SisFormService.sisData.sisMainData.LEAD_ID && ((this.pglId!="185") && (this.pglId!="191")) && ((!this.isTermAttached) || (this.isTermAttached && this.isTermOutput)));
		this.showNextButton = (this.isTermAttached && !this.isTermOutput);
		this.showExitButton = !SisFormService.sisData.sisMainData.LEAD_ID;

		this.showProceedButton = ((SisFormService.sisData.sisMainData.LEAD_ID) && ((!this.isTermAttached) || (this.isTermAttached && this.isTermOutput)));

		this.showNextButtonForBase = false;

		sc.sisData = sc.sisOutput.SIS;

/*
		COMMENTED FOR SINGLE SIS - Akhil - 22 DEC 2016

		sc.disableSignature = !!sc.sisOutput.SIS_SIGNED;

		sc.disableProceed = !(!!sc.sisOutput.SIS_SIGNED || ((sc.pglId=="185") || (sc.pglId=="191")));

		sc.showSaveButton = ((!SisFormService.sisData.sisMainData.LEAD_ID) || (SisFormService.sisData.sisMainData.LEAD_ID==0));
		sc.showSignatureButton = (!!SisFormService.sisData.sisMainData.LEAD_ID && ((sc.pglId!="185") && (sc.pglId!="191")) && ((!sc.isTermAttached) || (sc.isTermAttached && sc.isTermOutput)));

		sc.showExitButton = !SisFormService.sisData.sisMainData.LEAD_ID;

		sc.showProceedButton = ((SisFormService.sisData.sisMainData.LEAD_ID) && ((!sc.isTermAttached) || (sc.isTermAttached && sc.isTermOutput)));



	}


	this.init();
	var sisObject =  SisOutputService.getSISDataArray();
	 debug("get sis Object sisObject"+JSON.stringify(sisObject));
	if(!!sisObject){
		this.CurrentPageIndex = SisOutputService.getCurrentPageIndex();
		debug("sisObject current page "+ this.CurrentPageIndex);
		if(this.CurrentPageIndex == "COMBO"){
			SisOutputService.setIsSingleSIS(true)
			sc.showNextButtonForBase = true;
			sc.showNextButton = false;
		}
		else if(this.CurrentPageIndex == "BASE"){
			sc.showNextButtonForBase = false;
			sc.showNextButton = true;
		}
		else if(this.CurrentPageIndex == "TERM"){
			//need to check
		}
	}else{
		sc.showNextButtonForBase = false;
		sc.showNextButton = false;
	}

	// SINGLE SIS COMMENT ENDED

	*/

	/**
	*on click on the next button of the base plan
	*get the genrated output for the the genrateComboOutput
	**/
	this.onClickNextForTerm = function(){
		debug("...SIS on next fot Term ...");
		// sc.isTermOutput = true;
		CommonService.showLoading();
		SisOutputService.isTermOutput = true;
		SisOutputService.setCurrentPageIndex("TERM");
		SisOutputService.onNextCombo("TERM");
	};

	/**
	*on click on the next button of the base plan
	*get the genrated output for the the genrateComboOutput
	**/

	this.onClickNextForBase = function(){
		debug("...SIS on next fot base ...");
		var CurrentPageIndex = SisOutputService.getCurrentPageIndex();
		debug("sisObject current page "+CurrentPageIndex);
		if(CurrentPageIndex == "COMBO"){
			SisOutputService.saveSingleSIS().then(
				function(res){
					debug("save Single SIS final responce onClickNextForBase "+JSON.stringify(res));
					CommonService.showLoading();
					if(!!res && res=="S"){
						SisOutputService.setCurrentPageIndex("BASE");
						SisOutputService.onNextCombo("BASE");
					}
					else{
						CommonService.hideLoading();
						debug("Error in saving Single SIS ");
					}
				}
			);
		}
	};

	this.signSisReportFile = function(){
		if(!!SisFormService.sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT && parseInt(SisFormService.sisData.sisMainData.ANNUAL_PREMIUM_AMOUNT)>0){
			CommonService.doSign("sisPropSign");
		}
		else{
			navigator.notification.alert("PRM: Something went wrong. Kindly regenerate the SIS.",null,"SIS Report","OK");
		}
	};

	this.saveSisReportFile = function(){
		if((!!SisFormService.sisData.sisMainData.LEAD_ID && SisFormService.sisData.sisMainData.LEAD_ID!=0) && ((this.pglId=="185")||(this.pglId=="191"))){
			this.sisOutput.SIS_SIGNED = true;
			if(!!isTermOutput)
				this.sisOutput.SIS = SisOutputService.sisTermData.SIS;
			else
				this.sisOutput.SIS = SisOutputService.sisData.SIS;
		}
		SisOutputService.saveSisReportFile().then(
			function(res){
				if(!!res)
					navigator.notification.alert("SIS saved successfully.",null,"SIS Report","OK");
			}
		);
	};

	this.onBack = function(){
		$state.go("sisForm.sisFormD");
	};

	this.sisExit = function(){
		if(!!this.sisOutput.SIS_SAVED)
			$state.go("dashboard.productLandingPage", {"PGL_ID": SisFormService.sisData.sisMainData.PGL_ID, "SOL_TYPE": SisFormService.SOL_TYPE, "PROD_NAME": SisFormService.PROD_NAME, "OPP_ID": SisFormService.sisData.OPP_ID});
		else
			navigator.notification.alert("Please save the report before exiting.",null,"SIS Report","OK");
	};

	this.sisProceed = function(){
		if((this.pglId==IRS_PGL_ID)||(this.pglId == ITRP_PGL_ID)){
			if((!!SisFormService.sisData.sisMainData.LEAD_ID && SisFormService.sisData.sisMainData.LEAD_ID!=0) && ((this.pglId=="185")||(this.pglId=="191"))){
				this.sisOutput.SIS_SIGNED = true;
				if(!!isTermOutput)
					this.sisOutput.SIS = SisOutputService.sisTermData.SIS;
				else
					this.sisOutput.SIS = SisOutputService.sisData.SIS;
			}
			SisOutputService.saveSisReportFile().then(
				function(res){
					if(!!res){
						if(!!SisFormService.sisData.sisOutput.SIS_SAVED){
							$state.go('leadDashboard', {"LEAD_ID": SisFormService.sisData.sisMainData.LEAD_ID, "FHR_ID": SisFormService.sisData.sisMainData.FHR_ID, "SIS_ID": SisFormService.sisData.sisMainData.SIS_ID, "OPP_ID": SisFormService.sisData.oppData.OPPORTUNITY_ID, "APP_ID": null, "COMBO_ID": SisFormService.sisData.COMBO_ID});
						}
						else
							navigator.notification.alert("Please save the report before exiting.",null,"SIS Report","OK");
					}
					else{
						navigator.notification.alert("Could not save the report.",null,"SIS Report","OK");
					}
				}
			);
		}
		else{
			if(!!SisFormService.sisData.sisOutput.SIS_SAVED){
				$state.go('leadDashboard', {"LEAD_ID": SisFormService.sisData.sisMainData.LEAD_ID, "FHR_ID": SisFormService.sisData.sisMainData.FHR_ID, "SIS_ID": SisFormService.sisData.sisMainData.SIS_ID, "OPP_ID": SisFormService.sisData.oppData.OPPORTUNITY_ID, "APP_ID": null, "COMBO_ID": SisFormService.sisData.COMBO_ID});
			}
			else
				navigator.notification.alert("Please save the report before exiting.",null,"SIS Report","OK");
		}
	};

	this.shareSIS = function(){
		if(CommonService.checkConnection()!=="offline"){
			CommonService.showLoading("Sharing SIS...please wait...");
			var CurrentPageIndex = SisOutputService.getCurrentPageIndex();
			debug("sisObject current page "+CurrentPageIndex);
			SisOutputService.shareSIS(SisFormService.sisData.sisMainData.AGENT_CD, this.sisId, SisFormService.sisData.sisFormAData.PROPOSER_EMAIL, (!!SisFormService.sisData.COMBO_ID), sc.isTermOutput).then(
				function(res){
					CommonService.hideLoading();
					if(!!res && res=="S"){
						navigator.notification.alert("SIS shared successfully.",null,"SIS Report","OK");
					}
					else{
						navigator.notification.alert("Could not share SIS. Please try again later.",null,"SIS Report","OK");
					}
				}
			);
		}
		else{
			navigator.notification.alert("Please connect to the internet to share SIS.",null,"SIS Report","OK");
		}
	};

}]);

sisOutputModule.service('SisOutputService',['$q', '$filter', 'CommonService', 'SisFormService', 'LoginService', 'SisTimelineService', '$state', '$stateParams', function($q, $filter, CommonService, SisFormService, LoginService, SisTimelineService, $state, $stateParams){
	"use strict";

	var sc = this;
	this.sisComboArray = {};
	this.CurrentPageIndex;
	this.docCapID;
	this.generateSIS = function(isTermOutput){
		CommonService.showLoading("Generating SIS...please wait...");
		debug("gen sis");
		var dfd = $q.defer();
		this.isTermOutput = !!isTermOutput;
		if(!!isTermOutput){
			sc.setCurrentPageIndex("TERM");

		}
		else{
			if(!!SisFormService.sisData.oppData.REF_SIS_ID){
				sc.setCurrentPageIndex("BASE");
			}
		}
		this.getObjectBasedOnPlan(isTermOutput).then(
			function(sisReq){
				debug("response of the plan is " + JSON.stringify(sisReq));
				cordova.exec(
					function(res){
						debug("sis success : " + ((!!res)?(res.length):res));
						if(!!res && res.code==1){
							debug("sis report length : " + res.SIS.length);
							if(!!isTermOutput)
								sc.sisTermData = res;
							else
								sc.sisData = res;
						}
						else if(!!res && res.code==2){
							if(!!isTermOutput)
								sc.sisTermData = res.message;
							else
								sc.sisData = res.message;
							//navigator.notification.alert(res.message,null,"SIS","OK");
						}
						else{
							if(!!isTermOutput)
								sc.sisTermData = "res.SIS is null or undefined";
							else
								sc.sisData = "res.SIS is null or undefined";
						}
						if(!!isTermOutput){
							SisFormService.sisData.sisTermOutput = res;
						}
						else{
							SisFormService.sisData.sisOutput = res;
						}
						dfd.resolve(res);
					},
					function(err){
						debug("SIS Error: " + err);
						dfd.resolve(null);
					},"PluginHandler", "startSIS", [sisReq]
				);
			}
		);
		return dfd.promise;
	};


    this.generateSISComboPlan = function(isTermOutput){
    		CommonService.showLoading("Generating SIS...please wait...");
    		debug("gen sis");
    		var dfd = $q.defer();
    			  this.getArrayListForComboPlan().then(
    			    function(sisReq){
    			       /// debug("request for combo plan is  "+JSON.stringify(sisReq));
    			        cordova.exec(
                            function(resp){
                                sc.setSISDataArray(resp);
                                var res = resp.Combo;
                                sc.setCurrentPageIndex("COMBO");
                                sc.setSISDataForComboPlan(isTermOutput,res);
                                dfd.resolve(res);
                            },
                            function(err){
                                debug("SIS Error: " + err);
                                dfd.resolve(null);
                            },"PluginHandler", "startComboSIS", [sisReq]
                        );
    			});
    		return dfd.promise;
    	};

    this.setSISDataForComboPlan = function(isTermOutput,res){
        var CurrentPageIndex = this.getCurrentPageIndex();
        debug("sisObject current page setSISDataForComboPlan"+CurrentPageIndex);
          debug("isTermOutput : " + isTermOutput);
         if(!!res && res.code==1){
            debug("sis report length : " + res.SIS.length);
            if(CurrentPageIndex == "COMBO")
                sc.singleSISData = res;
            else if(!!isTermOutput)
                sc.sisTermData = res;
            else
                sc.sisData = res;
        }
        else if(!!res && res.code==2){
            if(CurrentPageIndex == "COMBO")
               sc.singleSISData = res.message;
            else if(!!isTermOutput)
                sc.sisTermData = res.message;
            else
            sc.sisData = res.message;
            //navigator.notification.alert(res.message,null,"SIS","OK");
        }
        else{
            if(CurrentPageIndex == "COMBO")
                 sc.singleSISData = "res.SIS is null or undefined";
            else if(!!isTermOutput)
                sc.sisTermData = "res.SIS is null or undefined";
            else
                sc.sisData = "res.SIS is null or undefined";
        }
        if(CurrentPageIndex == "COMBO"){
              SisFormService.sisData.singleSISData = res;
        }
        else if(!!isTermOutput){
            SisFormService.sisData.sisTermOutput = res;
        }
        else{
            SisFormService.sisData.sisOutput = res;
        }
    };


	this.getSisData = function(){
		return this.sisData;
	};

	this.getSisTAData = function(){
		return this.sisTermData;
	};
	this.getSingleSISData = function(){
		return this.singleSISData;
	};
	this.callCordovaToCallSIS = function(){

	};



	this.getProposerSignDocID = function(){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_doc_proof_master", true, "doc_id", {mpdoc_code: "SZ", mpproof_code: "SS", "customer_category": "PR"}).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug("getProposerSignDocID: docId: " + res.rows.item(0).DOC_ID);
					dfd.resolve(res.rows.item(0).DOC_ID);
				}
				else {
					debug("getProposerSignDocID: docId: null");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.saveProposerSignatureFile = function(sign, isTermOutput){
		var dfd = $q.defer();
		if(!!sign){
			var sisId = SisFormService.sisData.sisMainData.SIS_ID;
			if(!!isTermOutput)
				sisId = SisFormService.sisData.oppData.REF_SIS_ID;
			sign = sign.replace("data:image/jpeg;base64,","");
			CommonService.saveFile(LoginService.lgnSrvObj.userinfo.AGENT_CD + "_" + sisId + "_C02", ".jpg", "/FromClient/SIS/SIGNATURE/", sign, "N").then(
				function(){
					sc.getProposerSignDocID().then(
						function(docId){
							if(!!docId){
								var insertRecord = {"doc_id": docId, "agent_cd": LoginService.lgnSrvObj.userinfo.AGENT_CD, "doc_cat": "SIS", "doc_cat_id": sisId, "doc_name": (LoginService.lgnSrvObj.userinfo.AGENT_CD + "_" + sisId + "_C02.jpg"), "doc_timestamp": CommonService.getCurrDate(), "doc_page_no": "0"};
								CommonService.insertOrReplaceRecord(db, "lp_document_upload", insertRecord, true).then(
									function(){
										dfd.resolve("S");
									}
								);
							}
							else{
								dfd.resolve(null);
							}
						}
					);
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.getProposerSignDocID = function(){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_doc_proof_master", true, "doc_id", {mpdoc_code: "SZ", mpproof_code: "SS", "customer_category": "PR"}).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug("getProposerSignDocID: docId: " + res.rows.item(0).DOC_ID);
					dfd.resolve(res.rows.item(0).DOC_ID);
				}
				else {
					debug("getProposerSignDocID: docId: null");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.saveProposerSignatureFile = function(sign){
		var dfd = $q.defer();
		if(!!sign){
			sign = sign.replace("data:image/jpeg;base64,","");
			CommonService.saveFile(LoginService.lgnSrvObj.userinfo.AGENT_CD + "_" + SisFormService.sisData.sisMainData.SIS_ID + "_C02", ".jpg", "/FromClient/SIS/SIGNATURE/", sign, "N").then(
				function(){
					sc.getProposerSignDocID().then(
						function(docId){
							if(!!docId){
								var insertRecord = {"doc_id": docId, "agent_cd": LoginService.lgnSrvObj.userinfo.AGENT_CD, "doc_cat": "SIS", "doc_cat_id": SisFormService.sisData.sisMainData.SIS_ID, "doc_name": (LoginService.lgnSrvObj.userinfo.AGENT_CD + "_" + SisFormService.sisData.sisMainData.SIS_ID + "_C02.jpg"), "doc_timestamp": CommonService.getCurrDate(), "doc_page_no": "0"};
								CommonService.insertOrReplaceRecord(db, "lp_document_upload", insertRecord, true).then(
									function(){
										dfd.resolve("S");
									}
								);
							}
							else{
								dfd.resolve(null);
							}
						}
					);
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.saveProposerSignature = function(sign, vernac){
		debug("saveProposerSignature: " + ((!!sign)?(sign.length):null));
		var promises = [];
		var sisToWorkWith = [];
		sisToWorkWith[0] = {};
		sisToWorkWith[0].sisData = this.sisData;
		sisToWorkWith[0].sisOutput = SisFormService.sisData.sisOutput;
		sisToWorkWith[0].pglId = SisFormService.sisData.sisMainData.PGL_ID;
		sisToWorkWith[0].isTermOutput = false;
		if(!!this.isTermOutput){
			sisToWorkWith[1] = {};
			sisToWorkWith[1].sisData = this.sisTermData;
			sisToWorkWith[1].sisOutput = SisFormService.sisData.sisTermOutput;
			sisToWorkWith[1].pglId = SisFormService.sisData.sisFormEData.PGL_ID;
			sisToWorkWith[1].isTermOutput = true;
		}
		if(!!sisToWorkWith[0].sisData && !!sisToWorkWith[0].sisData.SIS){
			angular.forEach(sisToWorkWith, function(sisRec){
				sisRec.sisData.SIS = CommonService.replaceAll(sisRec.sisData.SIS,"@@PROPSIGN@@",sign);
				sisRec.sisData.SIS = sisRec.sisData.SIS.replace(/"hidePropSign"/g,"\"showPropSign\"");
				CommonService.showLoading();
				promises.push(sc.saveProposerSignatureFile(sign, sisRec.isTermOutput).then(
					function(saveSignRes){
						if(!!saveSignRes && saveSignRes=="S"){
							if((sisRec.pglId!=IRS_PGL_ID) && (sisRec.pglId!=ITRP_PGL_ID)){
								if(!!vernac){
									sisRec.sisData.SIS = CommonService.replaceAll(sisRec.sisData.SIS,"@@PROPSIGN_VERNAC@@",sign);
									sisRec.sisData.SIS = sisRec.sisData.SIS.replace(/"hidePropSignVernac"/g,"\"showPropSignVernac\"");
									sisRec.sisOutput.SIS_SIGNED = true;
									sisRec.sisOutput.SIS = sisRec.sisData.SIS;
									debug("going to sisVernacular");
									return "V";
								}
								else{
									sisRec.sisOutput.SIS_SIGNED = true;
									sisRec.sisOutput.SIS = sisRec.sisData.SIS;
									return sc.saveSisReportFile(sisRec.isTermOutput, true);
								}
							}
							else{
								CommonService.hideLoading();
								if(!!sc.isTermOutput){
									sisRec.sisOutput.SIS_SIGNED = true;
									sisRec.sisOutput.SIS = sisRec.sisData.SIS;
								}
								else{
									sisRec.sisOutput.SIS_SIGNED = true;
									sisRec.sisOutput.SIS = sisRec.sisData.SIS;
								}
								return sc.saveSisReportFile(sisRec.isTermOutput, true);
							}
						}
						else{
							navigator.notification.alert("Could not save the signature.",function(){
								CommonService.hideLoading();
							},"SIS Report","OK");
							return null;
						}
					}
				));
			});
		}
		else{
			promises.push(null);
		}
		return $q.all(promises).then(
			function(res){
				if(!!res && res.length>0){
					debug("final result: " + JSON.stringify(res));
					if(res.length==2){
						if(res[0]=="S" && res[1]=="S"){
							SisTimelineService.isOutputTabComplete = true;
							$stateParams.DONT_REGEN = true;
							$state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
						}
						else if(res[0]=="V" && res[1]=="V"){
							CommonService.showLoading();
							$state.go('sisForm.sisVernacular', {"isTermAttached": true});
						}
					}
					else if(res[0]=="S"){
						SisTimelineService.isOutputTabComplete = true;
						$stateParams.DONT_REGEN = true;
						$state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
					}
					else if(res[0]=="V"){
						CommonService.showLoading();
						$state.go('sisForm.sisVernacular');
					}
					else{
						debug("2. something went wrong...")
					}
				}
				else{
					debug("1. something went wrong...")
				}
				return res;
			}
		);
	};

	this.isSISSigned = function(){
		if(!!this.isTermOutput){
			return SisFormService.sisData.sisTermOutput.SIS_SIGNED||false;
		}
		else {
			return SisFormService.sisData.sisOutput.SIS_SIGNED||false;
		}
	};

	this.docUploadChangesForTerm = function(){
		var dfd = $q.defer();
		if(!!this.isTermOutput){
			SisFormService.docUploadChangesForTerm(SisFormService.sisData.sisMainData.SIS_ID, SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.oppData.REF_SIS_ID).then(
				function(res){
					debug("docUploadChangesForTerm: " + res);
					if(!!res && res=="S"){
						dfd.resolve("S");
					}
					else{
						dfd.resolve(null);
					}
				}
			);
		}
		else{
			debug("not a term sis...going to leadDashboard");
			dfd.resolve("S");
		}
		return dfd.promise;
	};

	this.saveSisReportFile = function(isTermOutput, resaveAfterSign){
		var dfd = $q.defer();
		var _sisOutput = SisFormService.sisData.sisOutput;
		var sisId = SisFormService.sisData.sisMainData.SIS_ID;
		var fhrId = SisFormService.sisData.sisMainData.FHR_ID;
		if(!!isTermOutput){
			_sisOutput = SisFormService.sisData.sisTermOutput;
			sisId = SisFormService.sisData.oppData.REF_SIS_ID;
			fhrId = SisFormService.sisData.oppData.REF_FHRID;
		}
		debug("SIS id is saveSisReportFile: " + isTermOutput + ": " +sisId)
		CommonService.saveFile(sisId, ".html", "/FromClient/SIS/HTML/", _sisOutput.SIS, "N").then(
			function(saveFileRes){
				if(!!saveFileRes && saveFileRes=="success"){
					var query = "INSERT OR IGNORE";
					if(!!resaveAfterSign){
						query = "INSERT OR REPLACE";
					}
					query += " INTO LP_DOCUMENT_UPLOAD (DOC_ID, AGENT_CD, DOC_CAT, DOC_CAT_ID, POLICY_NO, DOC_NAME, DOC_TIMESTAMP, DOC_PAGE_NO, IS_FILE_SYNCED, IS_DATA_SYNCED) VALUES((SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE CUSTOMER_CATEGORY=? AND MPDOC_CODE=?),?,?,?,?,?,?,?,?,?)";
					var parameterList = ["IN", "SA", SisFormService.sisData.sisMainData.AGENT_CD, "SIS", sisId, "0", (sisId+".html"), CommonService.getCurrDate(), "0", "N", "N"];

					if(!!SisFormService.sisData.COMBO_ID){
						query += " INTO LP_DOCUMENT_CAPTURE (DOC_CAP_ID,AGENT_CD,DOC_ID,DOC_CAT,DOC_CAT_ID,DOC_PAGE_NO,DOC_NAME,DOC_TIMESTAMP,IS_FILE_SYNCED,IS_DATA_SYNCED,COMBO_ID,INSURED_SEQ_NO,INSURED_UNIQUE_NO,MODIFIED_DATE) VALUES(?,?,(SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE CUSTOMER_CATEGORY=? AND MPDOC_CODE=?),?,?,?,?,?,?,?,?,?,(SELECT INSURED_UNIQUE_NO FROM LP_COMBO_DTLS WHERE COMBO_ID=? AND INSURED_SEQ_NO=? AND AGENT_CD=?),?)";
						parameterList = [CommonService.getRandomNumber(),SisFormService.sisData.sisMainData.AGENT_CD, "IN", "SA", "SIS", sisId, "0", (sisId + ".html"), CommonService.getCurrDate(), "N", "N", SisFormService.sisData.COMBO_ID, SisFormService.sisData.COMBO_SEQ_NO, SisFormService.sisData.COMBO_ID, SisFormService.sisData.sisMainData.INS_SEQ_NO, SisFormService.sisData.sisMainData.AGENT_CD, CommonService.getCurrDate()];
					}

					debug("query: " + query);
					debug("parameterList: " + JSON.stringify(parameterList));
					CommonService.transaction(db,
						function(tx){
							CommonService.executeSql(tx, query, parameterList,
								function(tx,res){
									if(!SisFormService.sisData.sisMainData.LEAD_ID || SisFormService.sisData.sisMainData.LEAD_ID==0){
										SisFormService.sisData.sisMainData.COMPLETION_TIMESTAMP = CommonService.getCurrDate();
										SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP = CommonService.getCurrDate();
										if(!!isTermOutput){
											var sisMainData = {};
											sisMainData.COMPLETION_TIMESTAMP = SisFormService.sisData.sisMainData.COMPLETION_TIMESTAMP || CommonService.getCurrDate();
											sisMainData.SIGNED_TIMESTAMP = SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP || CommonService.getCurrDate();
											sc.updateTermSisMainTable(sisMainData, sisId, SisFormService.sisData.sisMainData.AGENT_CD).then(
												function(res){
													if(!!res){
														SisTimelineService.isOutputTabComplete = true;
														_sisOutput.SIS_SAVED = true;
														sc.docUploadChangesForTerm().then(
															function(docRes){
																dfd.resolve(docRes);
															}
														);
														//dfd.resolve("S");
													}
													else{
														dfd.resolve(null);
													}
												}
											);
										}
										else{
											SisFormService.insertDataInSisMainTable().then(
												function(res){
													if(!!res){
														SisTimelineService.isOutputTabComplete = true;
														_sisOutput.SIS_SAVED = true;
														dfd.resolve("S");
													}
													else {
														dfd.resolve(null);
													}
												}
											);
										}
									}
									else if(!!_sisOutput.SIS_SIGNED){
										SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP = CommonService.getCurrDate();
										if(!!isTermOutput){
											var sisMainData = {};
											sisMainData.SIGNED_TIMESTAMP = SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP || CommonService.getCurrDate();
											sc.updateTermSisMainTable(sisMainData, sisId, SisFormService.sisData.sisMainData.AGENT_CD);
										}
										else{
											SisFormService.insertDataInSisMainTable();
										}
										/* to update the sis signed timestamp in opp table */
										SisFormService.createOrUpdateOpportunity(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID, fhrId, sisId, SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP);
										_sisOutput.SIS_SAVED = true;
										if(!!isTermOutput){
											sc.docUploadChangesForTerm().then(
												function(docRes){
													dfd.resolve(docRes);
												}
											);
										}
										else{
											dfd.resolve("S");
										}
									}
									else{
										if(!!isTermOutput){
											var sisMainData = {};
											sisMainData.COMPLETION_TIMESTAMP = SisFormService.sisData.sisMainData.COMPLETION_TIMESTAMP || CommonService.getCurrDate();
											sisMainData.SIGNED_TIMESTAMP = SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP || CommonService.getCurrDate();
											sc.updateTermSisMainTable(sisMainData, sisId, SisFormService.sisData.sisMainData.AGENT_CD).then(
												function(res){
													if(!!res){
														SisTimelineService.isOutputTabComplete = true;
														_sisOutput.SIS_SAVED = true;
														sc.docUploadChangesForTerm().then(
															function(docRes){
																dfd.resolve(docRes);
															}
														);
														//dfd.resolve("S");
													}
													else {
														dfd.resolve(null);
													}
												}
											);
										}
										else{
											SisFormService.insertDataInSisMainTable().then(
												function(res){
													if(!!res){
														SisTimelineService.isOutputTabComplete = true;
														_sisOutput.SIS_SAVED = true;
														dfd.resolve("S");
													}
													else {
														dfd.resolve(null);
													}
												}
											);
										}
									}
								},
								function(tx,err){
									dfd.resolve(null);
								}
							);
						},
						function(err){
							dfd.resolve(null);
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.saveSingleSisReportFile = function(DOC_CAT, sisId, DOC_CAP_ID, DOC_CAP_REF_ID, IsFlagSync){
		var dfd = $q.defer();
		var _sisOutput = SisFormService.sisData.singleSISData;
		var fhrId = SisFormService.sisData.sisMainData.FHR_ID;
		debug("SIS id is "+DOC_CAT +" : "+sisId)
		CommonService.saveFile(DOC_CAT+"_"+sisId,".html","/FromClient/SIS/HTML/",_sisOutput.SIS,"N").then(
			function(saveFileRes){
				if(!!saveFileRes && saveFileRes=="success"){
					var query = "INSERT OR REPLACE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID,DOC_CAP_REF_ID) VALUES((SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE MPPROOF_CODE=? AND MPDOC_CODE=?),?,?,?,?,?,?,?,?,?,?,?)";
					var parameterList = ["CS", "FD", SisFormService.sisData.sisMainData.AGENT_CD, "SIS", sisId, "0", (DOC_CAT+"_"+sisId+".html"), CommonService.getCurrDate(), "0", IsFlagSync, "N", DOC_CAP_ID, DOC_CAP_REF_ID];
					debug("query: " + query);
					debug("parameterList: " + JSON.stringify(parameterList));
					CommonService.transaction(db,
						function(tx){
							CommonService.executeSql(tx, query, parameterList,
								function(tx,res){
									debug("document uploaded on single sis "+JSON.stringify(res));
									if(!SisFormService.sisData.sisMainData.LEAD_ID || SisFormService.sisData.sisMainData.LEAD_ID==0){
										SisFormService.sisData.sisMainData.COMPLETION_TIMESTAMP = CommonService.getCurrDate();
										SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP = CommonService.getCurrDate();
										SisFormService.insertDataInSisMainTable().then(
											function(res){
												if(!!res){
													SisTimelineService.isOutputTabComplete = true;
													_sisOutput.SIS_SAVED = true;
													dfd.resolve("S");
												}
												else {
													dfd.resolve(null);
												}
											}
										);
									}
									else if(!!_sisOutput.SIS_SIGNED){
										SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP = CommonService.getCurrDate();
										SisFormService.insertDataInSisMainTable();
										/* to update the sis signed timestamp in opp table */
										SisFormService.createOrUpdateOpportunity(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID, fhrId, sisId, SisFormService.sisData.sisMainData.SIGNED_TIMESTAMP);
										_sisOutput.SIS_SAVED = true;
										dfd.resolve("S");
									}
									else{
										SisFormService.insertDataInSisMainTable().then(
											function(res){
												if(!!res){
													SisTimelineService.isOutputTabComplete = true;
													_sisOutput.SIS_SAVED = true;
													dfd.resolve("S");
												}
												else {
													dfd.resolve(null);
												}
											}
										);
									}
								},
								function(tx,err){
									dfd.resolve(null);
								}
							);
						},
						function(err){
							dfd.resolve(null);
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.shareBaseSisIfReq = function(agentCd, sisId, leadId, custEmailId, isTermOutput) {
		var dfd = $q.defer();
		if(!!isTermOutput){
			SisFormService.shareSIS(agentCd, sisId, custEmailId, null, leadId).then(
				function(baseRes){
					dfd.resolve(baseRes);
				}
			);
		}
		else{
			dfd.resolve("S");
		}
		return dfd.promise;
	};

	this.shareSIS = function(agentCd, sisId, custEmailId, isCombo, isTermOutput){
		var dfd = $q.defer();
		var _sisOutput = SisFormService.sisData.sisOutput;
		if(!!this.isTermOutput)
			_sisOutput = SisFormService.sisData.sisTermOutput;
		if((!!SisFormService.sisData.sisMainData.LEAD_ID && SisFormService.sisData.sisMainData.LEAD_ID!=0) && ((SisFormService.sisData.sisMainData.PGL_ID=="185")||(SisFormService.sisData.sisMainData.PGL_ID=="191"))){
			if(!!this.isTermOutput){
				_sisOutput.SIS_SIGNED = true;
				_sisOutput.SIS = this.sisTermData.SIS;
			}
			else{
				_sisOutput.SIS_SIGNED = true;
				_sisOutput.SIS = this.sisData.SIS;
			}
		}

		this.saveSisReportFile(isTermOutput).then(
			function(res){
				if(!!res && res=="S"){
					sc.shareBaseSisIfReq(agentCd, SisFormService.sisData.sisMainData.SIS_ID, SisFormService.sisData.sisMainData.LEAD_ID, custEmailId, isTermOutput).then(
						function(baseRes){
							if(!!baseRes && baseRes=="S"){
								SisFormService.shareSIS(agentCd, sisId, custEmailId, isCombo, SisFormService.sisData.sisMainData.LEAD_ID).then(
									function(shareSISRes){
										if(!!shareSISRes && shareSISRes=="S"){
											//changes made with respect to the base and term
											debug("share single sis Starts ");
											if(!!sc.getIsSingleSIS()){
												var BASE_SIS_ID = SisFormService.sisData.sisMainData.SIS_ID;
												var TERM_SIS_ID = SisFormService.sisData.oppData.REF_SIS_ID;
												SisFormService.shareSingleSIS(agentCd, BASE_SIS_ID, TERM_SIS_ID, custEmailId, isCombo, false).then(
													function(shareSISRes){
														debug("share single sis responce  "+JSON.stringify(shareSISRes) );
														if(!!shareSISRes && shareSISRes=="S"){
															dfd.resolve("S");
														}
														else{
															dfd.resolve(null);
														}
													}
												);
											}
											else{
												dfd.resolve("S");
											}
										}
										else{
											dfd.resolve(null);
										}
									}
								);
							}
							else{
								dfd.resolve(null);
							}
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.saveSingleSIS = function(){
		var dfd = $q.defer();
		var _sisOutput = SisFormService.sisData.singleSISData;

		if((!!SisFormService.sisData.sisMainData.LEAD_ID && SisFormService.sisData.sisMainData.LEAD_ID!=0) && ((SisFormService.sisData.sisMainData.PGL_ID=="185")||(SisFormService.sisData.sisMainData.PGL_ID=="191"))){
			_sisOutput.SIS_SIGNED = true;
			_sisOutput.SIS = this.singleSISData.SIS;
		}
		var SIS_ID  = SisFormService.sisData.sisMainData.SIS_ID;
		var DOC_CAT = "TRMB";
		var DOC_CAP_ID = CommonService.getRandomNumber();
		sc.setDocCapID(DOC_CAP_ID);
		var DOC_CAP_REF_ID = null;
		var IsFlagSync = "N";
		sc.saveSingleSisReportFile(DOC_CAT,SIS_ID,DOC_CAP_ID,DOC_CAP_REF_ID,IsFlagSync).then(
			function(res){
				debug("saving first record single SIS " + " DOC_CAP_ID " + DOC_CAP_ID + " " + JSON.stringify(res));
				if(!!res && res=="S"){
					var SIS_ID = SisFormService.sisData.oppData.REF_SIS_ID;
					var DOC_CAT = "TRMT";
					var DOC_CAP_REF_ID = sc.getDocCapID();
					var DOC_CAP_ID = CommonService.getRandomNumber();
					var IsFlagSync = "Y";
					sc.saveSingleSisReportFile(DOC_CAT,SIS_ID,DOC_CAP_ID,DOC_CAP_REF_ID,IsFlagSync).then(
						function(res){
							debug("saving first record single SIS " + JSON.stringify(res))
							if(!!res && res=="S"){
								dfd.resolve(res);
							}
							else{
								dfd.resolve(null);
							}
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.setDocCapID = function(docCapID){
		this.docCapID = docCapID;
	};

	this.getDocCapID = function(){
		return this.docCapID;
	};


	this.updateTermSisMainTable = function(sisMainData, sisId, agentCd){
		var dfd = $q.defer();
		if(!!sisMainData){
			CommonService.updateRecords(db, "lp_sis_main", sisMainData, {"sis_id": sisId, "agent_cd": agentCd}).then(
				function(res){
					if(!!res){
						dfd.resolve("S");
					}
					else{
						dfd.resolve(null);
					}
				}
			);
		}
		else {
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.onNext = function(){
		$stateParams.BASE_OR_TERM = "TERM";
		debug("$stateParams.BASE_OR_TERM: " + $stateParams.BASE_OR_TERM);
		$state.go($state.current, {"BASE_OR_TERM": "TERM"}, {reload: true});
	};

	this.onNextCombo = function(PLAN_TYPE){
    		$stateParams.BASE_OR_TERM = PLAN_TYPE;

    		debug("$stateParams.BASE_OR_TERM: " + $stateParams.BASE_OR_TERM);
    		$state.go($state.current, {"BASE_OR_TERM": PLAN_TYPE}, {reload: true});
    	};


	this.getObjectBasedOnPlan = function(isTermOutput){
    	var dfd = $q.defer();

            		try{
            			var sisReq = {};

            			if(!!SisFormService.sisData.sisFormAData.INSURED_MIDDLE)
            				sisReq.InsName = SisFormService.sisData.sisFormAData.INSURED_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.INSURED_MIDDLE + " " + SisFormService.sisData.sisFormAData.INSURED_LAST_NAME;
            			else
            				sisReq.InsName = SisFormService.sisData.sisFormAData.INSURED_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.INSURED_LAST_NAME;

            			sisReq.InsAge = CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB);
            			sisReq.InsSex = SisFormService.sisData.sisFormAData.INSURED_GENDER;
            			sisReq.InsOccDesc = "ANALYST";//SisFormService.sisData.sisFormAData.INSURED_OCCUPATION;
            			sisReq.InsOcc = "W10";//SisFormService.sisData.sisFormAData.INSURED_OCCU_CODE;
            			debug("sisReq.InsOccDesc::"+JSON.stringify(sisReq.InsOccDesc)+"sisReq.InsOcc:::"+JSON.stringify(sisReq.InsOcc));

            			if(!!SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME)
            				sisReq.OwnName = SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME;
            			else
            				sisReq.OwnName = SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME;

            			sisReq.OwnAge = CommonService.getAge(SisFormService.sisData.sisFormAData.PROPOSER_DOB);
            			sisReq.OwnOcc = SisFormService.sisData.sisFormAData.PROPOSER_OCCU_CODE;
            			sisReq.OwnSex = SisFormService.sisData.sisFormAData.PROPOSER_GENDER;
            			sisReq.OwnOccDesc = SisFormService.sisData.sisFormAData.PROPOSER_OCCUPATION;
            			//1 is non smoker and 0 is smoker
            			if(!!isTermOutput)
            				sisReq.InsSmoker = '1';
            			else
            				sisReq.InsSmoker = (SisFormService.sisData.sisFormAData.ISSMOKER=="Y")?'0':'1';
            			if(!!isTermOutput){
            				sisReq.PaymentMode = SisFormService.sisData.sisFormEData.PREMIUM_PAY_MODE;
            				sisReq.Sumassured = SisFormService.sisData.sisFormEData.SUM_ASSURED;
            			}
            			else{
            				sisReq.PaymentMode = SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE;
            				sisReq.Sumassured = SisFormService.sisData.sisFormBData.SUM_ASSURED;
            			}

            			if(!!isTermOutput)
            				sisReq.PropNo = SisFormService.sisData.oppData.REF_SIS_ID;
            			else
            				sisReq.PropNo = SisFormService.sisData.sisMainData.SIS_ID;

            			sisReq.SAgeProofFlg = SisFormService.sisData.sisFormAData.STD_AGEPROF_FLAG;
            			sisReq.PropDOB = SisFormService.sisData.sisFormAData.PROPOSER_DOB;
            			sisReq.InsDOB = SisFormService.sisData.sisFormAData.INSURED_DOB;
            			sisReq.ProdTye = SisFormService.getProductDetails(isTermOutput).LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE;
            			sisReq.commision = "Payable";

            			//plan code
            			if(!!isTermOutput){
            				sisReq.BasePlan = SisFormService.sisData.sisFormEData.PLAN_CODE;
            				sisReq.PolicyTerm = SisFormService.sisData.sisFormEData.POLICY_TERM;
            				sisReq.PPTerm = SisFormService.sisData.sisFormEData.PREMIUM_PAY_TERM;
            				sisReq.PremiumMul = SisFormService.sisData.sisFormEData.PREM_MULTIPLIER;
            				sisReq.Frequency = SisFormService.sisData.sisFormEData.PREMIUM_PAY_MODE;
            			}
            			else{
            				sisReq.BasePlan = SisFormService.sisData.sisFormBData.PLAN_CODE;
            				sisReq.PolicyTerm = SisFormService.sisData.sisFormBData.POLICY_TERM;
            				sisReq.PPTerm = SisFormService.sisData.sisFormBData.PREMIUM_PAY_TERM;
            				sisReq.PremiumMul = SisFormService.sisData.sisFormBData.PREM_MULTIPLIER;
            				sisReq.Frequency = SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE;
            			}
            			sisReq.TataEmployee = SisFormService.sisData.sisFormAData.TATA_FLAG || "N";

            			if(!!SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME)
            				sisReq.ProposerName = SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME;
            			else
            				sisReq.ProposerName = SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME + " " + SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME;
            			sisReq.ProposerGender = SisFormService.sisData.sisFormAData.PROPOSER_GENDER;
            			sisReq.ProposerOccupation = "ANALYST";//SisFormService.sisData.sisFormAData.PROPOSER_OCCU_CODE;
                        sisReq.ProposerDescOcc = "W10";//SisFormService.sisData.sisFormAData.PROPOSER_OCCUPATION;
                        //sisReq.ProposerAge = SisFormService.sisData.sisFormAData.PROPOSER_AGE;
            			sisReq.ProposerAge =  CommonService.getAge(SisFormService.sisData.sisFormAData.PROPOSER_DOB);

            			//*********************************ULIP*********************************

            			//new parameters..
            			if(!isTermOutput){
            				if((!!SisFormService.sisData.sisFormBData.INVT_STRAT_FLAG && SisFormService.sisData.sisFormBData.INVT_STRAT_FLAG === 'P') || (sisReq.ProdTye === "U")){
            					if(SisFormService.sisData.sisMainData.PGL_ID==MIP_PGL_ID){
            						//MIP
            						sisReq.BasePremiumAnnual = parseInt(SisFormService.sisData.sisFormBData.BASE_PREMIUM);
            					}
            					else{
            						sisReq.BasePremiumAnnual = Math.round(parseInt(SisFormService.sisData.sisFormBData.SUM_ASSURED) / parseFloat(SisFormService.sisData.sisFormBData.PREM_MULTIPLIER));
            					}
            				}
            			}

            			sisReq.SmartDebtFund = "";
            			sisReq.SmartEquFund = "";

            			sisReq.TLC = "0";
            			sisReq.WLA = "0";
            			sisReq.WLE = "0";
            			sisReq.WLF = "0";
            			sisReq.WLI = "0";
            			sisReq.WLS = "0";
            			debug("SisFormService.sisData.sisFormCData : " + JSON.stringify(SisFormService.sisData.sisFormCData));
            			if(sisReq.ProdTye === "U"){
            				sisReq.SmartDebtFund = SisFormService.sisData.sisFormCData.FUND_CODE_FROM || "";
            				sisReq.SmartEquFund = SisFormService.sisData.sisFormCData.FUND_CODE_TO || "";

            				if(SisFormService.sisData.sisFormCData.INVEST_OPT === "Manual"){

            					var fundRec = SisFormService.sisData.sisFormCData;

            					for(var j=1;j<11;j++){
            						if(!!fundRec['FUND'+j+'_CODE']){
            							sisReq[fundRec['FUND'+j+'_CODE']] = fundRec['FUND'+j+'_PERCENT'];
            						}
            					}

            				}
            				else{
            					sisReq[SisFormService.sisData.sisFormCData.FUND_CODE_FROM] = "100";
            				}
            			}

            			// Rider
            			sisReq.WOPULV1 = "";
            			sisReq.WOPPULV1 = "";
            			sisReq.ADDLULV2 = "";
            			sisReq.ADDLN1V1 = "";
            			sisReq.ADDLULV1 = "";
            			sisReq.WPPN1V1 = "";

            			if(!isTermOutput){
            				var riderList = SisFormService.sisData.sisFormDData;
                            console.log("riderList: " + JSON.stringify(riderList));
            				if(!!riderList && riderList.length>0){
            					angular.forEach(riderList, function(rider){
            						sisReq[rider.RIDER_CODE] = rider.RIDER_COVERAGE;
            					});
            				}
            			}

            			if(!!LoginService.lgnSrvObj && !!LoginService.lgnSrvObj.userinfo){
            				sisReq.AgentName = LoginService.lgnSrvObj.userinfo.AGENT_NAME;
            				sisReq.AgentNumber = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            				debug("agentMobileNumber:: " + LoginService.lgnSrvObj.userinfo.MOBILENO);
            				sisReq.AgentContactNumber = (LoginService.lgnSrvObj.userinfo.MOBILENO || "");
            			}

            			sisReq.PnlId = ($filter('filter')(SisFormService.getProductDetails(isTermOutput).LP_PNL_PLAN_LK, {"PNL_CODE": sisReq.BasePlan})[0]).PNL_ID;
            			sisReq.PglId = ($filter('filter')(SisFormService.getProductDetails(isTermOutput).LP_PNL_PLAN_LK, {"PNL_CODE": sisReq.BasePlan})[0]).PNL_PGL_ID;
                        dfd.resolve(sisReq);
						sisReq.businessType = BUSINESS_TYPE;
                    }
                    catch(ex){
                        debug("Exception: " + ex.message);
                    }
             return dfd.promise;
    	};
        this.getArrayListForComboPlan = function(){
            var dfd = $q.defer();
            var sisComboArray = [];
            var sisBaseArray = [];
            var sisTermArray = [];
            sc.getObjectBasedOnPlan().then(
                function(sisReqObjectBase){
                    debug("responce of the bases plan is in  getArrayListForComboPlan"+JSON.stringify(sisReqObjectBase));
                    sisBaseArray.push(sisReqObjectBase);


                    sc.getObjectBasedOnPlan(true).then(
                        function(sisReqObjectTerm){
                        	 sisTermArray.push(sisReqObjectTerm);

                             sisComboArray.push(sisBaseArray);
                             sisComboArray.push(sisTermArray);
                            debug("responce of the term plan is in getArrayListForComboPlan "+JSON.stringify(sisReqObjectTerm));
                            debug("responce of the term plan is in getArrayListForComboPlan "+JSON.stringify(sisComboArray));
                           dfd.resolve(sisComboArray);
                    });
            });
           return dfd.promise;
        };

        this.setSISDataArray = function(sisComboArray){
             this.sisComboArray = sisComboArray;
        };

         this.getSISDataArray = function(){
             return this.sisComboArray;

          };

           this.setCurrentPageIndex = function(sisComboArray){
                this.CurrentPageIndex = sisComboArray;
          };

           this.getCurrentPageIndex = function(){
               return this.CurrentPageIndex;

           };

            this.getIsSingleSIS = function(){
					return false;
                  //return this.IsSingleSIS;
            };

            this.setIsSingleSIS = function(isSingleSIS){
                  this.IsSingleSIS = isSingleSIS;
            };



}]);
