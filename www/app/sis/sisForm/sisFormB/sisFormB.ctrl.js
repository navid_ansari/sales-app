sisFormBModule.controller('SisFormBCtrl',['$state', '$filter', 'CommonService', 'SisFormService', 'SisFormBService', 'PlanName', 'PlanTermDetails', 'PlanCodeList', 'PPTList', 'PPMList', 'SPList', 'PolTermLimits', 'OptionList', 'PremiumMultipleList', 'MinimumSumAssured', 'MaximumSumAssured', 'MinimumPremium', 'MaximumPremium', 'SisTimelineService', function($state, $filter, CommonService, SisFormService, SisFormBService, PlanName, PlanTermDetails, PlanCodeList, PPTList, PPMList, SPList, PolTermLimits, OptionList, PremiumMultipleList, MinimumSumAssured, MaximumSumAssured, MinimumPremium, MaximumPremium, SisTimelineService){
	"use strict";
	CommonService.hideLoading();

    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	SisTimelineService.setActiveTab("plan");
	var sc = this;
	this.pglId = SisFormBService.pglId;
	this.productType = (SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
	this.isSelf = SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED;
	debug("in ctrl sis form b" + JSON.stringify(SPList));
	this.planCodeList = PlanCodeList;
	this.pptList = PPTList;
	this.ppmList = PPMList;
	this.spList = SPList;

	if(this.pglId==MRS_PGL_ID){
		this.pptLabel = "Option";
	}
	else{
		this.pptLabel = "Premium Paying Term";
	}

	if(!!OptionList)
		this.optionList = OptionList;

	this.premiumMultipleList = PremiumMultipleList;

	this.planTermDetails = PlanTermDetails;

	this.minPolTerm = PolTermLimits.MIN;
	this.maxPolTerm = PolTermLimits.MAX;

	SisFormBService.minPolTerm = this.minPolTerm;
	SisFormBService.maxPolTerm = this.maxPolTerm;

	this.minSumAssured = MinimumSumAssured;
	this.maxSumAssured = MaximumSumAssured;

	SisFormBService.minSumAssured = this.minSumAssured;
	SisFormBService.maxSumAssured = this.maxSumAssured;

	//added on 03-02-2017 - iOS
	SisFormService.minSumAssured = this.minSumAssured;
	SisFormService.maxSumAssured = this.maxSumAssured;

	debug("this.minPolTerm: " + this.minPolTerm);
	debug("this.maxPolTerm: " + this.maxPolTerm);

	debug("this.minSumAssured: " + this.minSumAssured);
	debug("this.maxSumAssured: " + this.maxSumAssured);

	debug("this.minPremium: " + this.minPremium);
	debug("this.maxPremium: " + this.maxPremium);

	if(this.pglId == MIP_PGL_ID)
		this.premiumDisable = 'ANNUAL';
	else
		this.premiumDisable = 'BASE';

	if(!!this.planCodeList && this.planCodeList.length>0)
		this.planCodeSelected = this.planCodeList[0];
	if(!!this.planCodeList && this.planCodeList.length==2)
		this.planCodeSelected = this.planCodeList[1];

	if(!!this.pptList && this.pptList.length>0)
		this.pptSelected = this.pptList[0];

	if(!!this.ppmList && this.ppmList.length>2)
		this.ppmSelected = this.ppmList[0];
	else if(!!this.ppmList && this.ppmList.length>1)
		this.ppmSelected = this.ppmList[1];

	if(this.pglId==MRS_PGL_ID){
		this.pptList[0] = {"PPT": null, "PPT_DISPLAY": "Select an option"};
		if(!!SisFormService.sisData.sisFormBData && !!SisFormService.sisData.sisFormBData.PLAN_CODE){
			debug("SisFormService.sisData.sisFormBData.PLAN_CODE: " + SisFormService.sisData.sisFormBData.PLAN_CODE);
			this.pptSelected = $filter('filter')(sc.pptList, {"PLAN_CODE": SisFormService.sisData.sisFormBData.PLAN_CODE})[0];
		}
		else{
			this.pptSelected = this.pptList[0];
		}
	}

	if(!!this.premiumMultipleList && (this.premiumMultipleList instanceof Array)){
		debug("this.premiumMultipleList: " + this.premiumMultipleList);
		this.premMultSelected = this.premiumMultipleList[0];
		this.showPremMultList = true;
	}
	else
		this.showPremMultList = false;

	this.policyTermReadOnly = !!PolTermLimits.READONLY;

	if(!!this.optionList && this.optionList.length>0)
		this.optionSelected = this.optionList[0];



	if(this.pglId == SIP_PGL_ID || this.pglId == FR_PGL_ID || this.pglId == SGP_PGL_ID || this.pglId == VCP_PGL_ID || this.pglId == SR_PGL_ID || this.pglId == SRP_PGL_ID || this.pglId == GIP_PGL_ID) // SIP, FR, SGP
		this.showOptionList = true;
	else
		this.showOptionList = false;

	this.spSelected = this.spList[0];

	if(!!this.pptList && this.pptList.length>0){
		this.showPPTList = true;
		SisFormBService.showPPTList = true;
	}
	else{
		this.showPPTList = false;
		SisFormBService.showPPTList = false;
	}

	debug("SisFormBService.sisFormBData: " + JSON.stringify(SisFormBService.sisFormBData));
	this.sisFormBData = SisFormBService.sisFormBData;


	this.sisFormBData.PLAN_NAME = this.sisFormBData.PLAN_NAME || PlanName;

	if(!!this.sisFormBData.PLAN_CODE && ((!!this.planCodeList && this.planCodeList.length>0))){
		var tempPlanCodeList = $filter('filter')(this.planCodeList, {"PNL_CODE": this.sisFormBData.PLAN_CODE});
		if(!!tempPlanCodeList && tempPlanCodeList.length>0)
			this.planCodeSelected = tempPlanCodeList[0];
		else{
			this.planCodeSelected = this.planCodeList[0];
		}
	}


	if(!!this.sisFormBData.PREMIUM_PAY_MODE && ((!!this.ppmList && this.ppmList.length>0))){
		this.ppmSelected = $filter('filter')(this.ppmList, {"PPM": this.sisFormBData.PREMIUM_PAY_MODE})[0];
	}

	if(!!this.sisFormBData.PLAN_CODE && ((!!this.optionList && this.optionList.length>0))){
		this.optionSelected = SisFormService.getOption(this.pglId, this.sisFormBData.PLAN_CODE, this.optionList);
	}

	if(!!this.pglId && this.pglId!=MRS_PGL_ID){
		if(!!this.sisFormBData.PREMIUM_PAY_TERM && ((!!this.pptList && this.pptList.length>0))){
			var tempPptList = $filter('filter')(this.pptList, {"PPT": this.sisFormBData.PREMIUM_PAY_TERM});
			if(!!tempPptList && tempPptList.length>0)
				this.pptSelected = tempPptList[0];
			else
				this.pptSelected = this.pptList[0];
			if((this.sisFormBData.PLAN_CODE=="IRSRP1N1V2")|| (this.sisFormBData.PLAN_CODE=="IRTRRP1N1") || (this.sisFormBData.PLAN_CODE.indexOf("SRRP") != -1) || (this.sisFormBData.PLAN_CODE.indexOf("SRPRP") != -1)){
				this.pptSelected = $filter('filter')(this.pptList, {"PPT": "0"})[0];
			}
			debug("this.pptSelected: " + JSON.stringify(this.pptSelected));
			debug("this.sisFormBData.PREMIUM_PAY_TERM: " + JSON.stringify(this.sisFormBData.PREMIUM_PAY_TERM));

		}
	}

	if(!!this.sisFormBData.INVT_STRAT_FLAG && ((!!this.spList && this.spList.length>0))){
		this.spSelected = $filter('filter')(this.spList, {"SP": this.sisFormBData.INVT_STRAT_FLAG})[0];
	}

	if(!!this.sisFormBData.PREM_MULTIPLIER && ((!!this.premiumMultipleList && this.premiumMultipleList.length>0))){
		this.premMultSelected = $filter('filter')(this.premiumMultipleList, {"PREM_MULT": this.sisFormBData.PREM_MULTIPLIER})[0];
		debug("this.pptSelected: " + JSON.stringify(this.premMultSelected));
		debug("this.sisFormBData.PREMIUM_PAY_TERM: " + JSON.stringify(this.sisFormBData.PREM_MULTIPLIER));
	}

	if(!!this.sisFormBData.INPUT_ANNUAL_PREMIUM)
		this.sisFormBData.INPUT_ANNUAL_PREMIUM = parseInt(this.sisFormBData.INPUT_ANNUAL_PREMIUM);

	if(!!this.sisFormBData.SUM_ASSURED)
		this.sisFormBData.SUM_ASSURED = parseInt(this.sisFormBData.SUM_ASSURED);

	if(!!this.sisFormBData.BASE_PREMIUM)
		this.sisFormBData.BASE_PREMIUM = parseInt(this.sisFormBData.BASE_PREMIUM);

	if(!!this.sisFormBData.POLICY_TERM)
		this.sisFormBData.POLICY_TERM = parseInt(this.sisFormBData.POLICY_TERM);

	if(!!this.sisFormBData.PREMIUM_PAY_TERM)
		this.sisFormBData.PREMIUM_PAY_TERM = parseInt(this.sisFormBData.PREMIUM_PAY_TERM);


	if(!!this.spList && this.spList.length>1)
		this.showSPList = true;
	else{
		this.sisFormBData.INVT_STRAT_FLAG = this.spList[0].SP;
		this.showSPList = false;
	}

	SisFormBService.startingPoint = this.sisFormBData.INVT_STRAT_FLAG;
	debug("SisFormBService.startingPoint : " + this.sisFormBData.INVT_STRAT_FLAG);

	if((this.sisFormBData.INVT_STRAT_FLAG == "P") || ((this.sisFormBData.INVT_STRAT_FLAG=="S") && ((this.pglId==SEC7_PGL_ID) || (this.pglId==MMX_PGL_ID) || (this.pglId==VCP_PGL_ID) || (this.pglId==SR_PGL_ID) || (this.pglId==SRP_PGL_ID) || (this.pglId==MRS_PGL_ID)) || (sc.productType=="U"))){
		this.minPremium = MinimumPremium;
		this.maxPremium = MaximumPremium;
		debug("this.minPremium : " + this.minPremium + "\tSisFormBService.maxPremium : " + SisFormBService.maxPremium)
		SisFormBService.minPremium = this.minPremium;
		SisFormBService.maxPremium = this.maxPremium;
		
		//added on 03-02-2017 - iOS
		SisFormService.minPremium = this.minPremium;
		SisFormService.maxPremium = this.maxPremium;
	}
	else{
		this.minPremium = null;
		this.maxPremium = null;

		SisFormBService.minPremium = null;
		SisFormBService.maxPremium = null;
		
		//added on 03-02-2017 - iOS
		SisFormService.minPremium = null;
		SisFormService.maxPremium = null;
	}

	//added/ changed on 03-02-2017 - iOS
	if((!!this.planCodeList) && (!!this.planCodeSelected.PNL_CODE)){
		this.premMultipleOf = SisFormBService.getPremMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
		this.sumAssuredMultipleOf = SisFormBService.getSumAssuredMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
	}
	else{
		this.premMultipleOf = SisFormBService.premiumMultipleOf || null;
		this.sumAssuredMultipleOf = SisFormBService.saMultipleOf || null;
	}


	this.previousPPM = null;

	this.onBack = function(){
		CommonService.showLoading("Loading...");
		if(SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y'){
			$state.go("sisForm.sfaPage1");
		}
		else{
			$state.go("sisForm.sfaPage2");
		}
	};

	this.onPPMChange = function(){
		this.sisFormBData.PREMIUM_PAY_MODE = this.ppmSelected.PPM;
		this.sisFormBData.PREMIUM_PAY_MODE_DESC = this.ppmSelected.PPM_DISPLAY;
		if((!this.previousPPM && !!this.ppmSelected) || (this.previousPPM=="O" && this.ppmSelected.PPM!="O") || (this.previousPPM!="O" && this.ppmSelected.PPM=="O")){
			this.pptList = SisFormService.getPPTList(this.pglId, this.ppmSelected.PPM);
			if(!!this.pptList){
				this.pptSelected = this.pptList[0];
				this.onPPTChange();
			}
			else if(this.pglId!=SGP_PGL_ID){
				this.sisFormBData.PLAN_CODE = this.planCodeList[1]; // first value is select
				this.onPlanCodeChange();
			}
		}

		if((!!this.sisFormBData.INPUT_ANNUAL_PREMIUM) && (!!this.sisFormBData.PREMIUM_PAY_MODE))
			this.sisFormBData.BASE_PREMIUM = SisFormService.calculateBasePremium(this.sisFormBData.INPUT_ANNUAL_PREMIUM, this.sisFormBData.PREMIUM_PAY_MODE);
		else
			this.sisFormBData.BASE_PREMIUM = null;

		this.previousPPM = this.ppmSelected.PPM;
	};

	this.onPPTChange = function(){
		debug("this.pptSelected.PPT: " + JSON.stringify(this.pptSelected) + " " + this.pptSelected.PPT);
		this.sisFormBData.PREMIUM_PAY_TERM = parseInt(this.pptSelected.PPT);
		debug("in ppt onchange " + this.sisFormBData.PREMIUM_PAY_TERM);
		debug("condition " + ((this.pglId!=FR_PGL_ID && this.pglId!=SIP_PGL_ID && this.pglId!=SR_PGL_ID && this.pglId!=SRP_PGL_ID) || ((this.pglId==FR_PGL_ID || this.pglId==SIP_PGL_ID || this.pglId==SR_PGL_ID || this.pglId==SRP_PGL_ID) && (!!this.optionSelected && !!this.optionSelected.OPTION))));
		debug("this.optionSelected1 : " + JSON.stringify(this.optionSelected));
		if(!!this.policyTermReadOnly) // Policy term is fixed
			this.sisFormBData.POLICY_TERM = (!!this.pptSelected.PPT)?parseInt(SisFormService.calculatePolicyTerm(this.pptSelected.PPT, this.pglId)):null;
		if(!this.pptSelected.PPT){
			//set plan code to null (default value) if ppt is null
			this.planCodeSelected = ($filter('filter')(this.planCodeList, {"PNL_CODE": null})[0]);
			this.onPlanCodeChange();
		}
		else if(this.pglId==MRS_PGL_ID){
			debug("this.pptSelected.PLAN_CODE: " + JSON.stringify(this.pptSelected.PLAN_CODE));
			this.planCodeSelected = ($filter('filter')(this.planCodeList, {"PNL_CODE": this.pptSelected.PLAN_CODE})[0]);
			this.onPlanCodeChange();
		}
		else if(this.pglId==VCP_PGL_ID){
			if(!!this.sisFormBData.SUM_ASSURED)
				this.onSumAssuredChange();
		}
		else if((this.pglId!=FR_PGL_ID && this.pglId!=SIP_PGL_ID && this.pglId!=SR_PGL_ID && this.pglId!=SRP_PGL_ID) || ((this.pglId==FR_PGL_ID || this.pglId==SIP_PGL_ID || this.pglId==SR_PGL_ID || this.pglId==SRP_PGL_ID) && (!!this.optionSelected && !!this.optionSelected.OPTION))){
			// here if plan not (SIP and Freedom) or plan is (SIP or Freedom and Option is selected(not null))
			debug("this.optionSelected : " + JSON.stringify(this.optionSelected));
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.pptSelected.PPT, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, null, this.pglId);
			this.onPlanCodeChange();
		}
	};

	this.onOptionChange = function(){
		debug("this.optionSelected : " + JSON.stringify(this.optionSelected));
		if((this.pglId == VCP_PGL_ID) && (!!this.sisFormBData.SUM_ASSURED && this.sisFormBData.SUM_ASSURED>0)){
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, (!!this.pptSelected)?this.pptSelected.PPT:null, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, this.sisFormBData.SUM_ASSURED, this.pglId);
			this.onPlanCodeChange();
			this.onSumAssuredChange();
		}
		else if(this.pglId == SGP_PGL_ID){
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.sisFormBData.PREMIUM_PAY_TERM, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, null, this.pglId);
			this.onPlanCodeChange();
		}
		else if(this.pglId == GIP_PGL_ID && (!!this.pptSelected.PPT && this.pptSelected.PPT!="0") && (!!this.optionSelected)){
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.sisFormBData.PREMIUM_PAY_TERM, (!!this.optionSelected)?this.optionSelected.OPTION:null, null, null, this.pglId);
			this.onPlanCodeChange();
		}
		else if((this.pglId == SR_PGL_ID) || (this.pglId == SRP_PGL_ID) || (!!this.pptSelected.PPT && this.pptSelected.PPT!="0")){
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.pptSelected.PPT, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, null, this.pglId);
			this.onPlanCodeChange();
		}
	};

	this.onPlanCodeChange = function(fromBPChange){
		if(!!this.planCodeSelected && !!this.planCodeSelected.PNL_CODE){

			this.sisFormBData.PLAN_CODE = this.planCodeSelected.PNL_CODE;

			this.minPolTerm = SisFormBService.getMinPolTerm(this.planCodeSelected.PNL_CODE);
			this.maxPolTerm = SisFormBService.getMaxPolTerm(this.planCodeSelected.PNL_CODE);

			this.minSumAssured = SisFormService.getMinSumAssured(this.pglId, null, this.planCodeList, this.planCodeSelected.PNL_CODE);
			SisFormService.minSumAssured = this.minSumAssured;
			this.maxSumAssured = SisFormService.getMaxSumAssured(this.pglId, null, this.planCodeList, this.planCodeSelected.PNL_CODE);
			SisFormService.maxSumAssured = this.maxSumAssured;

			if((this.sisFormBData.INVT_STRAT_FLAG == "P") || ((this.sisFormBData.INVT_STRAT_FLAG=="S") && ((this.pglId==SEC7_PGL_ID) || (this.pglId==MMX_PGL_ID) || (this.pglId==VCP_PGL_ID) || (this.pglId==SR_PGL_ID) || (this.pglId==SRP_PGL_ID) || (this.pglId==MRS_PGL_ID)) || (sc.productType=="U"))){
				this.minPremium = SisFormService.getMinPremium(this.pglId, null, this.planCodeList, this.planCodeSelected.PNL_CODE, this.sisFormBData.INVT_STRAT_FLAG);
				SisFormService.minPremium = this.minPremium;
				this.maxPremium = SisFormService.getMaxPremium(null, this.planCodeList, this.planCodeSelected.PNL_CODE);
				SisFormService.maxPremium = this.maxPremium;
			}
			else{
				this.minPremium = null;
				this.maxPremium = null;
			}

			if(this.sisFormBData.INVT_STRAT_FLAG=="P")
				this.premMultipleOf = SisFormBService.getPremMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
			else
				this.premMultipleOf = null;

			if(this.sisFormBData.INVT_STRAT_FLAG=="S")
				this.sumAssuredMultipleOf = SisFormBService.getSumAssuredMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
			else
				this.sumAssuredMultipleOf = null;

			debug("this.minPolTerm: " + this.minPolTerm);
			debug("this.maxPolTerm: " + this.maxPolTerm);

			if(!!this.pglId && this.pglId!=MIP_PGL_ID && this.pglId!=VCP_PGL_ID){
				if(parseInt(this.minPolTerm)==parseInt(this.maxPolTerm)){
					this.sisFormBData.POLICY_TERM = parseInt(this.minPolTerm);
					if(sc.productType!="U")
						this.sisFormBData.PREMIUM_PAY_TERM = parseInt(SisFormBService.getPremiumPayTermFixed(this.sisFormBData.PLAN_CODE));
				}
				else if((this.sisFormBData.POLICY_TERM<parseInt(this.minPolTerm)) || (this.sisFormBData.POLICY_TERM>parseInt(this.maxPolTerm)))
					this.sisFormBData.POLICY_TERM = null;
				this.onPolicyTermChange(true);

				if(this.sisFormBData.INVT_STRAT_FLAG=="S"){
					if((parseInt(this.sisFormBData.SUM_ASSURED)<parseInt(this.minSumAssured)) || (this.sisFormBData.SUM_ASSURED>parseInt(this.maxSumAssured)))
						this.sisFormBData.SUM_ASSURED = null;
					this.onSumAssuredChange();
				}
				else if (this.sisFormBData.INVT_STRAT_FLAG=="P"){
					if((parseInt(this.sisFormBData.INPUT_ANNUAL_PREMIUM)<parseInt(this.minPremium)) || (this.sisFormBData.INPUT_ANNUAL_PREMIUM>parseInt(this.maxPremium)))
						this.sisFormBData.INPUT_ANNUAL_PREMIUM = null;
					this.onAnnualPremiumChange();
				}
			}
			else{
				if(!fromBPChange){
					if((parseInt(this.sisFormBData.BASE_PREMIUM)<parseInt(this.minPremium)) || (this.sisFormBData.BASE_PREMIUM>parseInt(this.maxPremium)))
						this.sisFormBData.BASE_PREMIUM = null;
					this.onBasePremiumChange();
				}
			}

		}
	};

	this.onPolicyTermChange = function(fromCtrl){
		if(!!this.sisFormBData.POLICY_TERM){
			if((parseInt(this.sisFormBData.POLICY_TERM)>=parseInt(this.minPolTerm)) && (this.sisFormBData.POLICY_TERM<=parseInt(this.maxPolTerm))){
				if(!fromCtrl){
					if(this.pglId==GK_PGL_ID)
						this.sisFormBData.PREMIUM_PAY_TERM = parseInt(this.sisFormBData.POLICY_TERM - 5);
					if(!this.pptList || (this.pptList.length==0))
						this.sisFormBData.PREMIUM_PAY_TERM = parseInt(this.sisFormBData.POLICY_TERM);
				}
				if(this.pglId==MRS_PGL_ID){
					if(this.sisFormBData.PREMIUM_PAY_MODE=="O"){
						this.sisFormBData.PREMIUM_PAY_TERM = 1;
					}
					else{
						this.sisFormBData.PREMIUM_PAY_TERM = parseInt(this.sisFormBData.POLICY_TERM);
					}
				}
				if(!!this.planCodeSelected.PNL_CODE && this.pglId!=MIP_PGL_ID){ //not for MIP
					SisFormService.getPremiumMultipleList(this.pglId, null, this.sisFormBData.POLICY_TERM,this.planCodeList,this.planCodeSelected.PNL_CODE, this.sisFormBData.SUM_ASSURED).then(
						function(res){
							if(res instanceof Array){
								sc.premiumMultipleList = res;
								sc.premMultSelected = sc.premiumMultipleList[0];
							}
							else{
								sc.sisFormBData.PREM_MULTIPLIER = res;
								SisFormBService.premiumMultiple = res;
								debug("sc.sisFormBData.PREM_MULTIPLIER: " + sc.sisFormBData.PREM_MULTIPLIER);
								if(sc.sisFormBData.INVT_STRAT_FLAG=="P"){
									if(sc.pglId!=MIP_PGL_ID) // not MIP
										sc.onAnnualPremiumChange();
									else
										sc.onBasePremiumChange();
								}
								else
									sc.onSumAssuredChange();
							}
						}
					);
				}
			}
		}
	};

	this.onPremiumMultipleChange = function(){
		this.sisFormBData.PREM_MULTIPLIER = this.premMultSelected.PREM_MULT;
		SisFormBService.premiumMultiple = this.premMultSelected.PREM_MULT;
		debug("on prem mult change: " + this.sisFormBData.PREM_MULTIPLIER);
		if(!!this.premMultSelected.PREM_MULT){
			if(!!this.sisFormBData.INVT_STRAT_FLAG){
				if(this.sisFormBData.INVT_STRAT_FLAG=="P"){
					this.onAnnualPremiumChange();
				}
				else if(this.sisFormBData.INVT_STRAT_FLAG=="S"){
					this.onSumAssuredChange();
				}
			}
		}
	};

	this.onSPChange = function(){
		this.sisFormBData.INVT_STRAT_FLAG = this.spSelected.SP;

		if(!!this.sisFormBData.INVT_STRAT_FLAG && this.sisFormBData.INVT_STRAT_FLAG=="S"){
			this.sisFormBData.SUM_ASSURED = null;
			SisFormBService.startingPoint = "S";
			if(!!this.planCodeSelected){
				this.premMultipleOf = null;
				this.sumAssuredMultipleOf = SisFormBService.getSumAssuredMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
			}
			this.onSumAssuredChange();
		}
		else if(!!this.sisFormBData.INVT_STRAT_FLAG && this.sisFormBData.INVT_STRAT_FLAG=="P"){
			this.sisFormBData.INPUT_ANNUAL_PREMIUM = null;
			SisFormBService.startingPoint = "P";
			if(!!this.planCodeSelected){
				this.sumAssuredMultipleOf = null;
				this.premMultipleOf = SisFormBService.getPremMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE);
			}
			this.onAnnualPremiumChange();
		}
	};

	this.onAnnualPremiumChange = function(){

	//this.sisFormBData.INPUT_ANNUAL_PREMIUM =  $filter('myCurrency')(this.sisFormBData.INPUT_ANNUAL_PREMIUM);
		if((!!this.sisFormBData.INPUT_ANNUAL_PREMIUM) && (!!this.sisFormBData.PREM_MULTIPLIER))
			this.sisFormBData.SUM_ASSURED = SisFormService.calculateSumAssured(this.sisFormBData.INPUT_ANNUAL_PREMIUM, this.sisFormBData.PREM_MULTIPLIER);
		else
			this.sisFormBData.SUM_ASSURED = null;
		if((!!this.sisFormBData.INPUT_ANNUAL_PREMIUM) && (!!this.sisFormBData.PREMIUM_PAY_MODE)){
			this.sisFormBData.BASE_PREMIUM = SisFormService.calculateBasePremium(this.sisFormBData.INPUT_ANNUAL_PREMIUM, this.sisFormBData.PREMIUM_PAY_MODE);
		}
		else
			this.sisFormBData.BASE_PREMIUM = null;

	};

	this.onSumAssuredChange = function(){
		//this.sisFormBData.SUM_ASSURED =  $filter('myCurrency')(this.sisFormBData.SUM_ASSURED);
		if(this.pglId == VCP_PGL_ID){
			debug("sumAssured: " + this.sisFormBData.SUM_ASSURED);
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, (!!this.pptSelected)?this.pptSelected.PPT:null, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, this.sisFormBData.SUM_ASSURED, this.pglId);
			this.onPlanCodeChange();
		}
		if(!!this.productType && this.productType=="C"){
			SisFormService.getPremiumMultipleList(this.pglId, null, this.sisFormBData.POLICY_TERM,this.planCodeList,this.planCodeSelected.PNL_CODE, this.sisFormBData.SUM_ASSURED).then(
				function(res){
					sc.sisFormBData.PREM_MULTIPLIER = res;
					SisFormBService.premiumMultiple = res;
					if((!!sc.sisFormBData.SUM_ASSURED) && (!!sc.sisFormBData.PREM_MULTIPLIER)){
						sc.sisFormBData.INPUT_ANNUAL_PREMIUM = SisFormService.calculateAnnualPremium(sc.sisFormBData.SUM_ASSURED, sc.sisFormBData.PREM_MULTIPLIER);
						debug("sc.sisFormBData.INPUT_ANNUAL_PREMIUM:: " + sc.sisFormBData.INPUT_ANNUAL_PREMIUM);
					}
					else
						sc.sisFormBData.INPUT_ANNUAL_PREMIUM = null;

					if((!!sc.sisFormBData.INPUT_ANNUAL_PREMIUM) && (!!sc.sisFormBData.PREMIUM_PAY_MODE)){
						sc.sisFormBData.BASE_PREMIUM = SisFormService.calculateBasePremium(sc.sisFormBData.INPUT_ANNUAL_PREMIUM, sc.sisFormBData.PREMIUM_PAY_MODE);
					}
					else
						sc.sisFormBData.BASE_PREMIUM = null;
				}
			);
		}
		else{
			if((!!sc.sisFormBData.SUM_ASSURED) && (!!sc.sisFormBData.PREM_MULTIPLIER)){
				sc.sisFormBData.INPUT_ANNUAL_PREMIUM = SisFormService.calculateAnnualPremium(sc.sisFormBData.SUM_ASSURED, sc.sisFormBData.PREM_MULTIPLIER);
				debug("sc.sisFormBData.INPUT_ANNUAL_PREMIUM:: " + sc.sisFormBData.INPUT_ANNUAL_PREMIUM);
			}
			else
				sc.sisFormBData.INPUT_ANNUAL_PREMIUM = null;

			if((!!sc.sisFormBData.INPUT_ANNUAL_PREMIUM) && (!!sc.sisFormBData.PREMIUM_PAY_MODE)){
				sc.sisFormBData.BASE_PREMIUM = SisFormService.calculateBasePremium(sc.sisFormBData.INPUT_ANNUAL_PREMIUM, sc.sisFormBData.PREMIUM_PAY_MODE);
			}
			else
				sc.sisFormBData.BASE_PREMIUM = null;
		}
	};

	this.onBasePremiumChange = function(){
		debug("onBasePremiumChange: " + MIP_PGL_ID);
	//parseFloat(a.split(",").join(""))
		debug("onBasePremiumChange: " + MIP_PGL_ID);
		if(this.pglId==MIP_PGL_ID){
			debug("1");
			if((!!this.pptSelected.PPT) && (!!this.sisFormBData.BASE_PREMIUM)){
				this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.pptSelected.PPT, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sisFormBData.BASE_PREMIUM, null, this.pglId);
				debug("MIP this.planCodeSelected: " + (!!this.planCodeSelected)?JSON.stringify(this.planCodeSelected):null);
				this.onPlanCodeChange(true);
				if(!!this.planCodeSelected && !!this.planCodeSelected.PNL_CODE){
					SisFormService.getPremiumMultipleList(this.pglId, null, this.sisFormBData.POLICY_TERM,this.planCodeList,this.planCodeSelected.PNL_CODE).then(
						function(res){
							sc.sisFormBData.PREM_MULTIPLIER = res;
							SisFormBService.premiumMultiple = res;
							sc.sisFormBData.INPUT_ANNUAL_PREMIUM = SisFormBService.calculateAPFromBP(sc.sisFormBData.BASE_PREMIUM);
							sc.sisFormBData.SUM_ASSURED = SisFormBService.calculateSAFromBP(sc.sisFormBData.INPUT_ANNUAL_PREMIUM, sc.sisFormBData.BASE_PREMIUM, sc.sisFormBData.PREM_MULTIPLIER);
						}
					);
				}
			}
			else{
				debug("4");
				sc.sisFormBData.INPUT_ANNUAL_PREMIUM = null;
				sc.sisFormBData.SUM_ASSURED = null;
			}
		}
	};

	this.onNext = function(sisBForm, isPristine){
		try{
			CommonService.showLoading("Loading...");

			if(this.pglId == MIP_PGL_ID){
				this.sisFormBData.PREMIUM_PAY_MODE = this.ppmSelected.PPM;
				this.sisFormBData.PREMIUM_PAY_MODE_DESC = this.ppmSelected.PPM_DISPLAY;
			}

			if(!!this.premiumMultipleList && this.premiumMultipleList.length>0){
				if(this.sisFormBData.PREM_MULTIPLIER == this.premiumMultipleList[1].PREM_MULT)
					SisFormBService.riderAllowed = true;
				else
					SisFormBService.riderAllowed = false;
			}
			else
				SisFormBService.riderAllowed = true;

			debug("this.sisFormBData.PREMIUM_PAY_TERM:1: " + this.sisFormBData.PREMIUM_PAY_TERM);

			if((this.sisFormBData.PLAN_CODE=="IRSRP1N1V2")|| (this.sisFormBData.PLAN_CODE=="IRTRRP1N1") || (this.sisFormBData.PLAN_CODE.indexOf("SRRP") != -1) || (this.sisFormBData.PLAN_CODE.indexOf("SRPRP") != -1))
				this.sisFormBData.PREMIUM_PAY_TERM = this.sisFormBData.POLICY_TERM;

			debug("this.sisFormBData.PREMIUM_PAY_TERM:2: " + this.sisFormBData.PREMIUM_PAY_TERM);

			SisFormBService.setDataInSisFormBData(this.sisFormBData, this.sisFormBData.INPUT_ANNUAL_PREMIUM);
			SisFormBService.insertDataInSisScreenBTable().then(
				function(res){
					if(!!res){
						SisFormService.sisData.sisMainData.IS_SCREEN2_COMPLETED = 'Y';
						if(!isPristine || !SisTimelineService.isPlanTabComplete){
							if((!!SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED) && (SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED=="Y")){
								SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED = 'N';
								SisTimelineService.isFundTabComplete = false;
							}
							if((!!SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED) && (SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED=="Y")){
								SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED = 'N';
								SisTimelineService.isRiderTabComplete = false;
							}
							if(!!SisTimelineService.isOtherDetailsTabComplete && SisTimelineService.isOtherDetailsTabComplete==true){
								SisTimelineService.isOtherDetailsTabComplete = false;
							}
							if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
								SisTimelineService.isOutputTabComplete = false;
						}
						SisFormService.insertDataInSisMainTable().then(
							function(sisMainRes){
								if(!!sisMainRes){
									SisTimelineService.isPlanTabComplete = true;
									SisFormBService.gotoNextState(sc.planCodeSelected.PNL_ID || null);
								}
								else{
									CommonService.hideLoading();
								}
							}
						);
					}
					else{
						CommonService.hideLoading();
					}
				}
			);
		}catch(ex){
			debug("Exception: " + ex.message);
		}
	};
}]);

sisFormBModule.service('SisFormBService',['$q', '$state', '$filter', 'CommonService', 'SisFormService', 'SisFormDService', function($q, $state, $filter, CommonService, SisFormService, SisFormDService){
	"use strict";
	var sc = this;
	this.pglId = null;
	this.sisFormBData = this.sisFormBData || {};
	this.minPolTerm = null;
	this.maxPolTerm = null;
	this.premiumMultiple = null;

	this.calculateAPFromBP = function(annualPremium){
		return Math.round(12 * annualPremium);
	};

	this.calculateSAFromBP = function(annualPremium, basePremium, premiumMultiple){
		var LPD = ((annualPremium<=49000)?(0):(((annualPremium>=49100) && (annualPremium<=99000))?(3):(5)));
		var premRate = Math.floor(((premiumMultiple - LPD) * 0.0833) * 100) / 100;
		return Math.round((1000/premRate) * parseInt(basePremium));
	};

	this.setPglId = function(pglId){
		this.pglId = pglId;
	};

	this.getMinPolTerm = function(plancode){
		this.minPolTerm = SisFormService.getMinPolTerm(plancode, this.pglId);
		return this.minPolTerm;
	};

	this.getMaxPolTerm = function(plancode){
		this.maxPolTerm = SisFormService.getMaxPolTerm(plancode, this.pglId);
		return this.maxPolTerm;
	};

	this.getMinPolTermValue = function(){
		return this.minPolTerm;
	};
	this.getMaxPolTermValue = function(){
		return this.maxPolTerm;
	};

	this.getPremMultipleOf = function(planCodeList, plancode){
		debug("fetching premium multiple of");
		if(!!plancode){
			sc.premiumMultipleOf = $filter('filter')(SisFormService.getProductDetails().LP_PNL_PLAN_LK, {"PNL_CODE": plancode})[0].PNL_PREMIUM_MULTIPLE;
			debug("premium multiple of: " + sc.premiumMultipleOf);
			return sc.premiumMultipleOf;
		}
		else{
			debug("premium multiple of: null");
			sc.premiumMultipleOf = null;
			return sc.premiumMultipleOf;
		}
	};

	this.getSumAssuredMultipleOf = function(planCodeList, plancode){
		debug("fetching sa multiple of");
		sc.saMultipleOf = SisFormService.getSumAssuredMultipleOf(planCodeList, plancode);
			debug("SA multiple of: " + sc.saMultipleOf);
			debug("SA multiple of: null");
		return sc.saMultipleOf;
	};

	this.getInsuredAge = function(){
		return CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB);
	};

	this.getPremiumPayTermFixed = function(plancode){
		debug("plan code: " + plancode);
		debug("ppt: " + $filter('filter')(SisFormService.getProductDetails().LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].PPT);
		return $filter('filter')(SisFormService.getProductDetails().LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].PPT;
	};

	this.setDataInSisFormBData = function(sisFormBData, annualPremium){
		if(!SisFormService.sisData.sisFormBData)
			SisFormService.sisData.sisFormBData = {};
		Object.assign(SisFormService.sisData.sisFormBData, sisFormBData);
		SisFormService.sisData.sisMainData.IS_SCREEN2_COMPLETED = 'Y';
	};

	this.insertDataInSisScreenBTable = function(){
		var dfd = $q.defer();
		SisFormService.sisData.sisFormBData.MODIFIED_DATE = CommonService.getCurrDate();
		CommonService.insertOrReplaceRecord(db, "lp_sis_screen_b", SisFormService.sisData.sisFormBData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_SIS_SCREEN_B successfully: SIS_ID: " + SisFormService.sisData.sisMainData.SIS_ID);
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_SIS_SCREEN_B");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.gotoNextState = function(pnlId){
		var productType = (SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
		debug("productType: " + productType);
		if(productType=="U"){
			debug("going to fund page");
			$state.go("sisForm.sisFormC",{"RIDER_ALLOWED": this.riderAllowed});
		}
		else{
			CommonService.selectRecords(sisDB, 'lp_rpx_rider_plan_xref', true, null, {"rpx_pnl_id": pnlId},null).then(
				function(res){
					if(!!res && res.rows.length>0){
						debug("going to rider page");
						SisFormDService.getRidersList().then(
							function(ridersList){
								if(!!ridersList && ridersList.length>0){
								    SisFormService.gotoTAorOutput(false,true).then(
								        function(res){
								            $state.go("sisForm.sisFormD",{"RIDER_LIST": ridersList});
								        }
								    );
								}
								else{
									SisFormService.gotoTAorOutput(true);
								}
							}
						);
					}
					else{
						debug("generating sis output");
						SisFormService.gotoTAorOutput(true);
					}
				}
			);
		}
	};
}]);
