sisFormBModule.directive('validateSumAssured',['SisFormBService','SisFormService', function(SisFormBService,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!viewValue && !!SisFormBService.minSumAssured){
						if(parseInt(viewValue)<parseInt(SisFormBService.minSumAssured))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!viewValue && !!SisFormBService.maxSumAssured){
						if(parseInt(viewValue)>parseInt(SisFormBService.maxSumAssured))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					debug("viewValue: " + viewValue);
					debug("SisFormBService.saMultipleOf: " + SisFormBService.saMultipleOf);
					debug("SisFormBService.startingPoint: " + SisFormBService.startingPoint);
					if(!!viewValue && !!SisFormBService.saMultipleOf && SisFormBService.startingPoint=="S"){
						if((parseInt(viewValue)%parseInt(SisFormBService.saMultipleOf))>0)
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}

				ctrl.$validators.juvenileCheck = function(ngModelValue, viewValue){
					if(!!viewValue){
						if(SisFormBService.pglId!=IPR_PGL_ID && SisFormBService.pglId!=WPR_PGL_ID && SisFormBService.pglId!=IMX_PGL_ID && SisFormBService.pglId!=WMX_PGL_ID && SisFormBService.pglId!=MIP_PGL_ID && SisFormBService.pglId!=SGP_PGL_ID && SisFormBService.pglId!=MBP_PGL_ID && SisFormBService.pglId!=MMX_PGL_ID){
							if((SisFormBService.getInsuredAge()<18) && (parseInt(viewValue)>20000000))
								return false;
							else
								return true;
						}
						else{
							return true;
						}
					}
					else {
						return true;
					}
				}

				ctrl.$validators.juvenileCheckMMX = function(ngModelValue, viewValue){
					if(!!viewValue){
						if(SisFormBService.pglId==MMX_PGL_ID){
							if((SisFormBService.getInsuredAge()<18) && (parseInt(viewValue)>30000000))
								return false;
							else
								return true;
						}
						else{
							return true;
						}
					}
					else {
						return true;
					}
				}

				ctrl.$validators.stdAgeProofCheck = function(ngModelValue, viewValue){
					if(!!viewValue){
						if((SisFormService.sisData.sisFormAData.STD_AGEPROF_FLAG=='N') && (parseInt(viewValue)>1000000))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
			}
	};
}]);


sisFormBModule.directive('validateAnnualPremium',['SisFormBService','SisFormService', function(SisFormBService,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			validateAnnualPremium: "="
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium){
						debug("viewValue: " + viewValue + "\tSisFormService.minPremium : " + SisFormService.minPremium);
						if(!!viewValue && !!SisFormService.minPremium){
							if(parseInt(viewValue)<parseInt(SisFormService.minPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium){
						if(!!viewValue && !!SisFormService.maxPremium){
							if(parseInt(viewValue)>parseInt(SisFormService.maxPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium && SisFormBService.startingPoint=="P"){
						debug("viewValue: " + viewValue);
						debug("SisFormBService.premiumMultipleOf: " + SisFormBService.premiumMultipleOf);
						if(!!viewValue && !!SisFormBService.premiumMultipleOf){
							if((parseInt(viewValue)%parseInt(SisFormBService.premiumMultipleOf))>0)
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
			}
	};
}]);

sisFormBModule.directive('validateBasePremium',['SisFormBService','SisFormService', function(SisFormBService,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			validateBasePremium: "="
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium){
						if(!!viewValue && !!SisFormService.minPremium){
							if(parseInt(viewValue)<parseInt(SisFormService.minPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium){
						if(!!viewValue && !!SisFormService.maxPremium){
							if(parseInt(viewValue)>parseInt(SisFormService.maxPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else {
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium && SisFormBService.startingPoint=="P"){
						if(!!viewValue && !!SisFormBService.premiumMultipleOf){
							if((parseInt(viewValue)%parseInt(SisFormBService.premiumMultipleOf))>0)
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
			}
	};
}]);

sisFormBModule.directive('validatePolicyTerm',['SisFormBService', function(SisFormBService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			ppt: "=validatePolicyTerm"
		},
		link:
			function(scope, element, attr, ctrl){
				debug('in validatePolicyTerm for validation');
				ctrl.$validators.minTerm = function(ngModelValue, viewValue){
					debug("in validatePolicyTerm $validators in minTerm");
					if(!!viewValue && !!SisFormBService.minPolTerm){
						if(parseInt(viewValue)<parseInt(SisFormBService.minPolTerm))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxTerm = function(ngModelValue, viewValue){
					debug("in validatePolicyTerm $validators in maxTerm");
					if(!!viewValue && !!SisFormBService.maxPolTerm){
						if(parseInt(viewValue)>parseInt(SisFormBService.maxPolTerm))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.lessThanPPT = function(ngModelValue, viewValue){
					debug("in validatePolicyTerm $validators in lessThanPPT");
					debug("in lessThanPPT " + JSON.stringify(SisFormBService.showPPTList));
					debug("scope.ppt: " + scope.ppt);
					if(!!viewValue && !!scope.ppt){
						if(parseInt(viewValue)<parseInt(scope.ppt) && (!!SisFormBService.showPPTList))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
			}
	};
}]);
sisFormBModule.directive('validatePpt',['SisFormBService', 'SisFormService', function(SisFormBService, SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			pt: "=validatePpt"
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.greaterThanPT = function(ngModelValue, viewValue){
					debug("in greaterThanPT " + viewValue);
					debug("scope.pt: " + scope.pt);
					if(!!viewValue && !!viewValue.PPT && !!scope.pt){
						if((!SisFormService.sisData.sisMainData.PGL_ID==VCP_PGL_ID) && parseInt(viewValue.PPT)>parseInt(scope.pt))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}

				ctrl.$validators.selectRequired = function(ngModelValue, viewValue){
					if(!!viewValue && (viewValue instanceof Object) && viewValue!={}){
						var keys = Object.keys(viewValue);
						var selectRequired = true;
						angular.forEach(keys,function(key){
							if(!!viewValue[key] && viewValue[key].length>0 && viewValue[key].substring(0,7)=="Select "){
								if(!!selectRequired)
									selectRequired = false;
								debug("key: " + key + " " + "ngModelValue[key]: " + viewValue[key]);
								debug("setting validity: false");
							}
						});
						return selectRequired;
					}
					else
						return true;
				}
			}
	};
}]);
