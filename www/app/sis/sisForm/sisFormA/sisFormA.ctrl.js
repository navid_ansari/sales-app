sisFormAModule.controller('SisFormACtrl',['$state', 'CommonService',function($state, CommonService){
	"use strict";
}]);

sisFormAModule.service('SisFormAService',['$q', 'CommonService', 'SisFormService', '$filter', function($q, CommonService, SisFormService, $filter){
	"use strict";
	var sfa = this;

	this.getSfaPage1Data = function(sisFormAData){
		debug("in getSfaPage1Data");
		var sfaPage1Data = {};
		sfaPage1Data.INSURED_TITLE = sisFormAData.INSURED_TITLE || null;
		sfaPage1Data.INSURED_FIRST_NAME = sisFormAData.INSURED_FIRST_NAME || null;
		sfaPage1Data.INSURED_MIDDLE = sisFormAData.INSURED_MIDDLE || null;
		sfaPage1Data.INSURED_LAST_NAME = sisFormAData.INSURED_LAST_NAME || null;
		sfaPage1Data.INSURED_OCCU_INDUSTRIES = sisFormAData.INSURED_OCCU_INDUSTRIES || null;
		sfaPage1Data.INSURED_OCCUPATION = sisFormAData.INSURED_OCCUPATION || null;
		sfaPage1Data.INSURED_OCCU_CODE = sisFormAData.INSURED_OCCU_CODE || null;
		sfaPage1Data.INSURED_OCCU_WORK_NATURE = sisFormAData.INSURED_OCCU_WORK_NATURE || null;
		sfaPage1Data.AGEPROF_DOC_TYPE = sisFormAData.AGEPROF_DOC_TYPE || null;
		sfaPage1Data.STD_AGEPROF_FLAG = sisFormAData.STD_AGEPROF_FLAG || null;
		sfaPage1Data.AGE_PROOF_DOC_ID = sisFormAData.AGE_PROOF_DOC_ID || null;
		sfaPage1Data.INSURED_GENDER = sisFormAData.INSURED_GENDER || null;
		sfaPage1Data.INSURED_MOBILE = sisFormAData.INSURED_MOBILE || null;
		sfaPage1Data.INSURED_EMAIL = sisFormAData.INSURED_EMAIL || null;
		sfaPage1Data.INSURED_DOB = sisFormAData.INSURED_DOB || null;
		sfaPage1Data.INSURED_DOB = sisFormAData.INSURED_DOB || null;
		sfaPage1Data.ISSMOKER = sisFormAData.ISSMOKER || null;
		if(!!sisFormAData.PROPOSER_IS_INSURED && sisFormAData.PROPOSER_IS_INSURED=='Y'){
			sfaPage1Data.TATA_FLAG = sisFormAData.TATA_FLAG || null;
			sfaPage1Data.COMPANY_NAME = sisFormAData.COMPANY_NAME || null;
			sfaPage1Data.COMPANY_CODE = sisFormAData.COMPANY_CODE || null;
		}
		return sfaPage1Data;
	};

	this.getSfaPage2Data = function(sisFormAData){

		var sfaPage2Data = {};
		if(!!sisFormAData.PROPOSER_IS_INSURED && sisFormAData.PROPOSER_IS_INSURED=="Y"){
			sfaPage2Data.PROPOSER_TITLE = sisFormAData.INSURED_TITLE || null;
			sfaPage2Data.PROPOSER_FIRST_NAME = sisFormAData.INSURED_FIRST_NAME || null;
			sfaPage2Data.PROPOSER_MIDDLE_NAME = sisFormAData.INSURED_MIDDLE || null;
			sfaPage2Data.PROPOSER_LAST_NAME = sisFormAData.INSURED_LAST_NAME || null;
			sfaPage2Data.PROPOSER_OCCU_INDUSTRIES = sisFormAData.INSURED_OCCU_INDUSTRIES || null;
			sfaPage2Data.PROPOSER_OCCUPATION = sisFormAData.INSURED_OCCUPATION || null;
			sfaPage2Data.PROPOSER_OCCU_CODE = sisFormAData.INSURED_OCCU_CODE || null;
			sfaPage2Data.PROPOSER_OCCU_WORK_NATURE = sisFormAData.INSURED_OCCU_WORK_NATURE || null;
			debug("sisFormAData.INSURED_DOB: " + sisFormAData.INSURED_DOB);
			sfaPage2Data.PROPOSER_DOB = sisFormAData.INSURED_DOB || null;
			debug("sisFormAData.PROPOSER_DOB: " + sfaPage2Data.PROPOSER_DOB);
			sfaPage2Data.PROPOSER_GENDER = sisFormAData.INSURED_GENDER || null;
			debug("sfaPage2Data.INSURED_GENDER: " + sfaPage2Data.PROPOSER_GENDER);
			sfaPage2Data.PROPOSER_MOBILE = sisFormAData.INSURED_MOBILE || null;
			sfaPage2Data.PROPOSER_EMAIL = sisFormAData.INSURED_EMAIL || null;
		}
		else{
			sfaPage2Data.PROPOSER_TITLE = sisFormAData.PROPOSER_TITLE || null;
			sfaPage2Data.PROPOSER_FIRST_NAME = sisFormAData.PROPOSER_FIRST_NAME || null;
			sfaPage2Data.PROPOSER_MIDDLE_NAME = sisFormAData.PROPOSER_MIDDLE_NAME || null;
			sfaPage2Data.PROPOSER_LAST_NAME = sisFormAData.PROPOSER_LAST_NAME || null;
			sfaPage2Data.PROPOSER_OCCU_INDUSTRIES = sisFormAData.PROPOSER_OCCU_INDUSTRIES || null;
			sfaPage2Data.PROPOSER_OCCUPATION = sisFormAData.PROPOSER_OCCUPATION || null;
			sfaPage2Data.PROPOSER_OCCU_CODE = sisFormAData.PROPOSER_OCCU_CODE || null;
			sfaPage2Data.PROPOSER_OCCU_WORK_NATURE = sisFormAData.PROPOSER_OCCU_WORK_NATURE || null;
			sfaPage2Data.PROPOSER_DOB = sisFormAData.PROPOSER_DOB || null;
			sfaPage2Data.PROPOSER_GENDER = sisFormAData.PROPOSER_GENDER || null;
			sfaPage2Data.PROPOSER_MOBILE = sisFormAData.PROPOSER_MOBILE || null;
			sfaPage2Data.PROPOSER_EMAIL = sisFormAData.PROPOSER_EMAIL || null;
			sfaPage2Data.AGE_PROOF_DOC_ID_PR = sisFormAData.AGE_PROOF_DOC_ID_PR || null;
			sfaPage2Data.AGEPROF_DOC_TYPE_PR = sisFormAData.AGEPROF_DOC_TYPE_PR || null;
			sfaPage2Data.STD_AGEPROF_FLAG_PR = sisFormAData.STD_AGEPROF_FLAG_PR || null;
			sfaPage2Data.TATA_FLAG = sisFormAData.TATA_FLAG || null;
			sfaPage2Data.COMPANY_NAME = sisFormAData.COMPANY_NAME || null;
			sfaPage2Data.COMPANY_CODE = sisFormAData.COMPANY_CODE || null;
		}
		debug("in getSfaPage2Data: " + sfaPage2Data);
		return sfaPage2Data;
	};

	this.getIndustryList = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct sis_industries from lp_sis_occupation order by sis_industries asc",[],
					function(tx,res){
						debug("industryList res.rows.length: " + res.rows.length);
						var industryList = [];
						industryList.push({"SIS_INDUSTRIES_DISPLAY": "Select Industry", "SIS_INDUSTRIES": null});
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var industry = {};
								industry.SIS_INDUSTRIES_DISPLAY = CommonService.toTitleCase(res.rows.item(i).SIS_INDUSTRIES);
								industry.SIS_INDUSTRIES = res.rows.item(i).SIS_INDUSTRIES;
								industryList.push(industry);
							}
							dfd.resolve(industryList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getOccupationList = function(industry){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct sis_occupation from lp_sis_occupation where sis_industries=? order by sis_occupation asc",[industry],
					function(tx,res){
						debug("occupationList res.rows.length: " + res.rows.length);
						var occupationList = [];
						occupationList.push({"SIS_OCCUPATION_DISPLAY": "Select Occupation", "SIS_OCCUPATION": null});
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var occupation = {};
								occupation.SIS_OCCUPATION_DISPLAY = CommonService.toTitleCase(res.rows.item(i).SIS_OCCUPATION);
								occupation.SIS_OCCUPATION = res.rows.item(i).SIS_OCCUPATION;
								occupationList.push(occupation);
							}
							dfd.resolve(occupationList);
						}
						else
							dfd.resolve(occupationList);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};


	this.getNatureOfWorkList = function(industry, occupation){
		var dfd = $q.defer();
		debug("industry: " + industry);
		debug("occupation: " + occupation);
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct occupation_code, sis_nature_of_work from lp_sis_occupation where isactive=? and sis_industries=? and sis_occupation=? order by sis_nature_of_work asc",["Y", industry, occupation],
					function(tx,res){
						var natureOfWorkList = [];
						debug("natureOfWorkList res.rows.length: " + res.rows.length);
						natureOfWorkList.push({"SIS_NATURE_OF_WORK_DISPLAY": "Select Nature of Work", "OCCUPATION_CODE": null});
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var natureOfWork = {};
								natureOfWork.OCCUPATION_CODE = res.rows.item(i).OCCUPATION_CODE;
								natureOfWork.SIS_NATURE_OF_WORK = res.rows.item(i).SIS_NATURE_OF_WORK;
								natureOfWork.SIS_NATURE_OF_WORK_DISPLAY = CommonService.toTitleCase(res.rows.item(i).SIS_NATURE_OF_WORK);
								natureOfWorkList.push(natureOfWork);
							}
							dfd.resolve(natureOfWorkList);
						}
						else
							dfd.resolve(natureOfWorkList);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getGenderOptionSelected = function(gender){
		return ((!!gender && gender=="F")?"Female":"Male");
	};


	this.insertDataInSisScreenATable = function(){
		var dfd = $q.defer();
		SisFormService.sisData.sisFormAData.MODIFIED_DATE = CommonService.getCurrDate();
		CommonService.insertOrReplaceRecord(db, "lp_sis_screen_a", SisFormService.sisData.sisFormAData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_SIS_SCREEN_A successfully: SIS_ID: " + SisFormService.sisData.sisMainData.SIS_ID);
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_SIS_SCREEN_A");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.deriveGender = function(title, gender){
		if(title=="Mr.")
			return "M";
		else if((title=="Dr.") || (!title))
			return gender || null;
		else
			return "F";
	};

	this.getCompanyNameList = function(tataFlag){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_app_company_master", true, null, {"tata_flag": tataFlag}, "company_name asc").then(
			function(res){
				var companyNameList = [];
				debug("companyNameList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					companyNameList.push({"COMPANY_NAME": "Select Company Name", "COMPANY_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var companyName = {};
						companyName.COMPANY_NAME = res.rows.item(i).COMPANY_NAME;
						companyName.COMPANY_CODE = res.rows.item(i).COMPANY_CODE;
						companyNameList.push(companyName);
					}
					dfd.resolve(companyNameList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.isTataDiscountAvailable = function(pglId){
		var dfd = $q.defer();
		CommonService.selectRecords(sisDB, "lp_pgl_plan_group_lk", true, "pgl_staff_discount", {"pgl_id": pglId}).then(
			function(selRes){
				if(!!selRes && selRes.rows.length>0){
					dfd.resolve(selRes.rows.item(0).PGL_STAFF_DISCOUNT=="Y");
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getEkycData = function(leadId, ekyc_id, prEkycId){
		var dfd = $q.defer();
		var EKYC_DATA = {};
		CommonService.transaction(db,
			function(tx){
				var sql = "select * from LP_LEAD_EKYC_DTLS where EKYC_ID in (?,?)";
				if(!!leadId){
					sql = sql +" and LEAD_ID = '"+leadId+"'";
				}
				CommonService.executeSql(tx, sql, [ekyc_id, prEkycId], function(tx, res){
					debug("EKYC :: "+sql);
					if(!!res && res.rows.length>0){
						var retData = {};
						EKYC_DATA = CommonService.resultSetToObject(res);
						if(!!EKYC_DATA && !(EKYC_DATA instanceof Array))
							EKYC_DATA = [EKYC_DATA];
						if(EKYC_DATA.length>0){
							if(!!ekyc_id)
								retData.insured = $filter('filter')(EKYC_DATA, {"EKYC_ID": ekyc_id})[0] || null;
							else
								retData.insured = {};
							if(!!prEkycId){
								retData.proposer = $filter('filter')(EKYC_DATA, {"EKYC_ID": prEkycId})[0] || null;
							}
							else{
								retData.proposer = null;
							}
						}
						else{
							retData = {};
						}
						dfd.resolve(retData);
					}
					else{
						dfd.resolve(null);
					}
				}, function(tx, err){
						dfd.resolve(null);
				})
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

}]);
