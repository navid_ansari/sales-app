sfaPage1Module.controller('SfaPage1Ctrl',['$q','$state','$stateParams', '$filter', 'LoginService', 'CommonService', 'SisFormService', 'SisFormAService', 'SfaPage1Service', 'IndustryList', 'OccupationList', 'NatureOfWorkList', 'AgeProofList', 'SisTimelineService', 'CompanyList', 'TataDiscountAvailable', function($q, $state, $stateParams, $filter, LoginService, CommonService, SisFormService, SisFormAService, SfaPage1Service, IndustryList, OccupationList, NatureOfWorkList, AgeProofList, SisTimelineService, CompanyList, TataDiscountAvailable){
	"use strict";
	debug("in controller sfaPage1Ctrl: " + JSON.stringify(SisFormService.sisData.sisFormAData));
	CommonService.hideLoading();


    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	var sp = this;
	SisTimelineService.setActiveTab("insured");
	debug("PROPOSER_GENDER : " + SisFormService.sisData.sisFormAData.PROPOSER_GENDER);
	this.PROPOSER_GENDER = SisFormService.sisData.sisFormAData.PROPOSER_GENDER;
	this.nameRegex = NAME_REGEX;
	this.mNameRegex = FULLNAME_REGEX_SP;

	//if(BUSINESS_TYPE=="GoodSolution" || BUSINESS_TYPE=="iWealth"){
	if(BUSINESS_TYPE!="IndusSolution"){
		this.leadNonFhrDataDisabled = false;
	}
	else{
		this.leadNonFhrDataDisabled = true;
	}
	if(!SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED && !!SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE)
	{
		if(SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE == '200')
			SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED = 'Y';
	}
	if(!SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE && !!SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE)
		{
			SisFormService.sisData.sisFormAData.PURCHASING_INSURANCE_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE;
			SisFormService.sisData.sisFormAData.PURCHASING_INSURANCE_FOR = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR;
			SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR;
			SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE;
		}

	this.showEmployeeFlag = (!!TataDiscountAvailable) && (BUSINESS_TYPE=='iWealth') && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y');

	this.genderDisabled = (!!SisFormService.sisData.sisFormAData.INSURED_TITLE) && (SisFormService.sisData.sisFormAData.INSURED_TITLE !== "Dr.");

	this.insMobRequired = false;
	this.insEmailRequired = false;

	this.firstNameDisabled = false;
	this.middleNameDisabled = false;
	this.lastNameDisabled = false;

	this.mobileRegex = MOBILE_REGEX;
	this.emailRegex = EMAIL_REGEX;

	this.prodMinAge = SisFormService.getProdMinAge();
	this.prodMaxAge = SisFormService.getProdMaxAge();

	this.adultAge = ADULT_AGE;

	this.IRS_PGL_ID = IRS_PGL_ID;
	this.ITRP_PGL_ID = ITRP_PGL_ID;
	this.PGL_ID = SisFormService.sisData.sisMainData.PGL_ID;

	this.titleList = [
		{"TITLE_DISPLAY": "Select Title", "TITLE": null},
		{"TITLE_DISPLAY": "Mr.","TITLE": "Mr."},
		{"TITLE_DISPLAY": "Mrs.","TITLE": "Mrs."},
		{"TITLE_DISPLAY": "Ms.","TITLE": "Ms."},
		{"TITLE_DISPLAY": "Miss.","TITLE": "Miss."},
		{"TITLE_DISPLAY": "Dr.","TITLE": "Dr."}
	];
	this.industryList = IndustryList;
	this.occupationList = OccupationList;
	this.natureOfWorkList = NatureOfWorkList;
	this.ageProofList = AgeProofList;
	this.companyList = CompanyList;

	this.titleOptionSelected = this.titleList[0];
	this.industryOptionSelected = this.industryList[0];
	this.occupationOptionSelected = this.occupationList[0];
	this.natureOfWorkOptionSelected = this.natureOfWorkList[0];
	this.ageProofOptionSelected = this.ageProofList[0];
	this.companyOptionSelected = this.companyList[0];

	debug("natureOfWorkList LIST: " + JSON.stringify(this.natureOfWorkList));

	debug("AP LIST: " + JSON.stringify(this.ageProofList));

	debug("AP LIST selected: " + JSON.stringify(this.ageProofOptionSelected));

	this.sfaPage1Data = SfaPage1Service.sfaPage1Data;


	if(SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE == "200")
		this.sfaPage1Data.PROPOSER_IS_INSURED = "Y";
	else
		this.sfaPage1Data.PROPOSER_IS_INSURED = "N";

	if(!!this.sfaPage1Data.INSURED_TITLE){
		this.titleOptionSelected = $filter('filter')(this.titleList, {"TITLE": sp.sfaPage1Data.INSURED_TITLE})[0];
	}

	if(!!this.sfaPage1Data.INSURED_OCCU_INDUSTRIES){
		this.industryOptionSelected = $filter('filter')(this.industryList, {"SIS_INDUSTRIES": sp.sfaPage1Data.INSURED_OCCU_INDUSTRIES})[0];
	}

	if(!!sp.sfaPage1Data.INSURED_OCCUPATION){
		this.occupationOptionSelected = $filter('filter')(this.occupationList, {"SIS_OCCUPATION": sp.sfaPage1Data.INSURED_OCCUPATION})[0];
	}

	if(!!this.sfaPage1Data.INSURED_OCCU_CODE){
		this.natureOfWorkOptionSelected = $filter('filter')(this.natureOfWorkList, {"SIS_NATURE_OF_WORK": sp.sfaPage1Data.INSURED_OCCU_WORK_NATURE})[0];
	}

	if(!!this.sfaPage1Data.INSURED_DOB)
		this.sfaPage1Data.INSURED_DOB = CommonService.formatDobFromDb(this.sfaPage1Data.INSURED_DOB);

	this.sfaPage1Data.INSURED_GENDER = SisFormAService.deriveGender(this.sfaPage1Data.INSURED_TITLE, this.sfaPage1Data.INSURED_GENDER);

	if((SisFormService.sisData.sisMainData.PGL_ID==MRS_PGL_ID) || (SisFormService.sisData.sisMainData.PGL_ID==IRS_PGL_ID) || (SisFormService.sisData.sisMainData.PGL_ID==ITRP_PGL_ID) || (SisFormService.sisData.sisMainData.PGL_ID==SR_PGL_ID) || (SisFormService.sisData.sisMainData.PGL_ID==SRP_PGL_ID)){
		this.showSmoker = true;
	}
	else{
		if(!this.sfaPage1Data.ISSMOKER)
			this.sfaPage1Data.ISSMOKER = 'N'
		this.showSmoker = false;
	}


	if(!!this.sfaPage1Data.INSURED_MOBILE)
		this.sfaPage1Data.INSURED_MOBILE = parseInt(this.sfaPage1Data.INSURED_MOBILE);

	if(!!this.sfaPage1Data.AGE_PROOF_DOC_ID){
		this.ageProofOptionSelected = $filter('filter')(this.ageProofList, {"DOC_ID": sp.sfaPage1Data.AGE_PROOF_DOC_ID})[0];
	}
	debug("this.ageProofOptionSelected()::: " + JSON.stringify(this.ageProofOptionSelected));

	if(!!this.sfaPage1Data.COMPANY_CODE && !!this.companyList){
		this.companyOptionSelected = $filter('filter')(this.companyList, {"COMPANY_CODE": sp.sfaPage1Data.COMPANY_CODE})[0];
	}
	debug("this.companyOptionSelected()::: " + JSON.stringify(this.companyOptionSelected));

	if(!!SisFormService.sisData.sisMainData.LEAD_ID){
		// Only enter if proposer is not null
		if(!!this.sfaPage1Data.PROPOSER_IS_INSURED && this.sfaPage1Data.PROPOSER_IS_INSURED=="Y"){
			this.leadDataDisabled = true;
		}
		else {
			this.leadDataDisabled = false;
		}
	}
	else {
		this.leadDataDisabled = false;
	}

	this.ekycDobDisabled = true;
	if(!!SisFormService.sisData.sisEKycData.insured && !!SisFormService.sisData.sisEKycData.insured.EKYC_FLAG && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y"){
		this.firstNameDisabled = true;
		this.middleNameDisabled = true;
		this.lastNameDisabled = true;
	}
	else{
		this.firstNameDisabled = false;
		this.middleNameDisabled = false;
		this.lastNameDisabled = false;
		this.ekycDobDisabled = false;
	}

	this.onProposerIsInsuredChg = function(){
		if(!!SisFormService.sisData.sisMainData.LEAD_ID){

			// Only enter if proposer is not null

			if(!!this.sfaPage1Data.PROPOSER_IS_INSURED && this.sfaPage1Data.PROPOSER_IS_INSURED=="Y"){
				if(!!SfaPage1Service.sfaPage2Data){
					this.sfaPage1Data.INSURED_TITLE = SfaPage1Service.sfaPage2Data.PROPOSER_TITLE;
					this.sfaPage1Data.INSURED_FIRST_NAME = SfaPage1Service.sfaPage2Data.PROPOSER_FIRST_NAME;
					this.sfaPage1Data.INSURED_MIDDLE = SfaPage1Service.sfaPage2Data.PROPOSER_MIDDLE_NAME;
					this.sfaPage1Data.INSURED_LAST_NAME = SfaPage1Service.sfaPage2Data.PROPOSER_LAST_NAME;
					this.sfaPage1Data.INSURED_OCCU_INDUSTRIES = SfaPage1Service.sfaPage2Data.PROPOSER_OCCU_INDUSTRIES;
					this.sfaPage1Data.INSURED_OCCUPATION = SfaPage1Service.sfaPage2Data.PROPOSER_OCCUPATION;
					this.sfaPage1Data.INSURED_OCCU_WORK_NATURE = SfaPage1Service.sfaPage2Data.PROPOSER_OCCU_WORK_NATURE;
					this.sfaPage1Data.INSURED_OCCU_CODE = SfaPage1Service.sfaPage2Data.PROPOSER_OCCU_CODE;
					this.sfaPage1Data.INSURED_GENDER = SfaPage1Service.sfaPage2Data.PROPOSER_GENDER;
					this.sfaPage1Data.INSURED_DOB = CommonService.formatDobFromDb(SfaPage1Service.sfaPage2Data.PROPOSER_DOB);
					this.sfaPage1Data.INSURED_MOBILE = parseInt(SfaPage1Service.sfaPage2Data.PROPOSER_MOBILE);
					this.sfaPage1Data.INSURED_EMAIL = SfaPage1Service.sfaPage2Data.PROPOSER_EMAIL;
				}
				else{
					this.sfaPage1Data.INSURED_TITLE = SisFormService.sisData.sisFormAData.PROPOSER_TITLE;
					this.sfaPage1Data.INSURED_FIRST_NAME = SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME;
					this.sfaPage1Data.INSURED_MIDDLE = SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME;
					this.sfaPage1Data.INSURED_LAST_NAME = SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME;

					if(!!SisFormService.sisData.sisEKycData.insured.EKYC_FLAG && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y"){
						var nameArr = SisFormService.sisData.sisEKycData.insured.NAME.split(" ");
						this.sfaPage1Data.INSURED_FIRST_NAME = nameArr[0];
						SisFormService.sisData.sisFormAData.PROPOSER_FIRST_NAME = nameArr[0];
						if(nameArr.length>2){
							this.sfaPage1Data.INSURED_MIDDLE = "";
							for(var i=1; i<(nameArr.length-1); i++){
								this.sfaPage1Data.INSURED_MIDDLE += nameArr[i];
								if(i!=(nameArr.length-2))
									this.sfaPage1Data.INSURED_MIDDLE += " ";
							}
							this.sfaPage1Data.INSURED_LAST_NAME = nameArr[(nameArr.length - 1)];

							for(var i=1; i<(nameArr.length-1); i++){
								SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += nameArr[i];
								if(i!=(nameArr.length-2))
									SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += " ";
							}
							SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[(nameArr.length - 1)];
						}
						else{
							this.sfaPage1Data.INSURED_MIDDLE = null;
							SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME = null;
							if(nameArr.length>1){
								this.sfaPage1Data.INSURED_LAST_NAME = nameArr[1];
								SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[1];
							}
						}
						if(!!SisFormService.sisData.sisEKycData.insured.DOB){
							SisFormService.sisData.sisFormAData.PROPOSER_DOB = SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00";
							if((SisFormService.sisData.sisEKycData.insured.DOB.length>9) && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-07")=="-1")){
								sp.ekycDobDisabled = true;
							}
							else{
								sp.ekycDobDisabled = false;
							}
						}
						else{
							sp.ekycDobDisabled = true;
						}
					}
					else{
						sp.ekycDobDisabled = false; //earlier was true - changed on 17-02-2017
					}

					this.sfaPage1Data.INSURED_OCCU_INDUSTRIES = SisFormService.sisData.sisFormAData.PROPOSER_OCCU_INDUSTRIES;
					this.sfaPage1Data.INSURED_OCCUPATION = SisFormService.sisData.sisFormAData.PROPOSER_OCCUPATION;
					this.sfaPage1Data.INSURED_OCCU_WORK_NATURE = SisFormService.sisData.sisFormAData.PROPOSER_OCCU_WORK_NATURE;
					this.sfaPage1Data.INSURED_OCCU_CODE = SisFormService.sisData.sisFormAData.PROPOSER_OCCU_CODE;
					this.sfaPage1Data.INSURED_GENDER = SisFormService.sisData.sisFormAData.PROPOSER_GENDER;
					debug("SisFormService.sisData.sisFormAData.PROPOSER_DOB: " + SisFormService.sisData.sisFormAData.PROPOSER_DOB);
					if(!!SisFormService.sisData.sisFormAData.PROPOSER_DOB)
						this.sfaPage1Data.INSURED_DOB = CommonService.formatDobFromDb(SisFormService.sisData.sisFormAData.PROPOSER_DOB);
					this.sfaPage1Data.INSURED_MOBILE = parseInt(SisFormService.sisData.sisFormAData.PROPOSER_MOBILE);
					this.sfaPage1Data.INSURED_EMAIL = SisFormService.sisData.sisFormAData.PROPOSER_EMAIL;
				}
				this.leadDataDisabled = true;
			}
			else{
				if(!this.sfaPage1Data.INSURED_FIRST_NAME){
					this.sfaPage1Data.INSURED_TITLE = null;
					this.sfaPage1Data.INSURED_FIRST_NAME = null;
					this.sfaPage1Data.INSURED_MIDDLE = null;
					this.sfaPage1Data.INSURED_LAST_NAME = null;
					this.sfaPage1Data.INSURED_OCCU_INDUSTRIES = null;
					this.sfaPage1Data.INSURED_OCCUPATION = null;
					this.sfaPage1Data.INSURED_OCCU_WORK_NATURE = null;
					this.sfaPage1Data.INSURED_OCCU_CODE = null;
					this.sfaPage1Data.INSURED_GENDER = null;
					this.sfaPage1Data.INSURED_DOB = null;
					this.sfaPage1Data.INSURED_MOBILE = null;
					this.sfaPage1Data.INSURED_EMAIL = null;

					if(!!SisFormService.sisData.sisEKycData.insured && !!SisFormService.sisData.sisEKycData.insured.EKYC_FLAG && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y"){
						var nameArr = SisFormService.sisData.sisEKycData.insured.NAME.split(" ");
						this.sfaPage1Data.INSURED_FIRST_NAME = nameArr[0];
						this.firstNameDisabled = true;
						if(nameArr.length>2){
							this.sfaPage1Data.INSURED_MIDDLE = "";
							for(var i=1; i<(nameArr.length-1); i++){
								this.sfaPage1Data.INSURED_MIDDLE += nameArr[i];
								if(i!=(nameArr.length-2))
									this.sfaPage1Data.INSURED_MIDDLE += " ";
							}
							this.sfaPage1Data.INSURED_LAST_NAME = nameArr[(nameArr.length - 1)];
							this.middleNameDisabled = true;
							this.lastNameDisabled = true;
						}
						else{
							this.sfaPage1Data.INSURED_MIDDLE = null;
							this.sfaPage1Data.INSURED_LAST_NAME = nameArr[1];
							this.middleNameDisabled = false;
							this.lastNameDisabled = true;
						}
						if(!!SisFormService.sisData.sisEKycData.insured.DOB){
							sp.sfaPage1Data.INSURED_DOB = CommonService.formatDobFromDb(SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00");
							if((SisFormService.sisData.sisEKycData.insured.DOB.length>9) && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-07")=="-1")){
								sp.ekycDobDisabled = true;
							}
							else{
								sp.ekycDobDisabled = false;
							}
						}
						else{
							sp.ekycDobDisabled = false;
						}
						debug("First Name: " + this.sfaPage1Data.INSURED_FIRST_NAME);
						debug("Middle Name: " + this.sfaPage1Data.INSURED_MIDDLE);
						debug("Last Name: " + this.sfaPage1Data.INSURED_LAST_NAME);
					}
					else{
						sp.ekycDobDisabled = false;
					}

					if(!!SisFormService.sisData.sisEKycData.proposer && !!SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG && SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="Y"){
						var nameArr = SisFormService.sisData.sisEKycData.proposer.NAME.split(" ");
						this.sfaPage1Data.PROPOSER_FIRST_NAME = nameArr[0];
						if(nameArr.length>2){
							SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME = "";
							for(i=1; i<(nameArr.length-1); i++){
								SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += nameArr[i];
								if(i!=(nameArr.length-2))
									SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += " ";
							}
							SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[(nameArr.length - 1)];
						}
						else{
							SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME = null;
							if(nameArr.length==2){
								SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[1];
							}
						}
						if(!!SisFormService.sisData.sisEKycData.proposer.DOB){
							SisFormService.sisData.sisFormAData.PROPOSER_DOB = SisFormService.sisData.sisEKycData.proposer.DOB + " 00:00:00";
							if((SisFormService.sisData.sisEKycData.proposer.DOB.length>9) && (SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-07")=="-1")){
								SisFormService.sisData.propEkycDobDisabled = true;
							}
							else{
								SisFormService.sisData.propEkycDobDisabled = false;
							}
						}
						else{
							SisFormService.sisData.propEkycDobDisabled = true;
						}
					}
					else{
						SisFormService.sisData.propEkycDobDisabled = true;
					}

					this.leadDataDisabled = false;
				}
			}

			debug("this.sfaPage1Data.INSURED_TITLE: " + this.sfaPage1Data.INSURED_TITLE + " " + SisFormService.sisData.sisFormAData.PROPOSER_TITLE)

			this.titleOptionSelected = $filter('filter')(this.titleList, {"TITLE": (this.sfaPage1Data.INSURED_TITLE || null)})[0];
			this.onTitleChg();

			this.industryOptionSelected = $filter('filter')(this.industryList, {"SIS_INDUSTRIES": (this.sfaPage1Data.INSURED_OCCU_INDUSTRIES || null)})[0];

			this.onIndustryChg().then(
				function(occupationList){
					sp.occupationOptionSelected = $filter('filter')(sp.occupationList, {"SIS_OCCUPATION": (sp.sfaPage1Data.INSURED_OCCUPATION || null)})[0];
				}
			);

			this.ageProofOptionSelected = $filter('filter')(this.ageProofList, {"DOC_ID": (this.sfaPage1Data.AGE_PROOF_DOC_ID || null)})[0];
			this.onAgeProofChg();
		}
		else{
			if(!!SisFormService.sisData.sisEKycData.insured && !!SisFormService.sisData.sisEKycData.insured.EKYC_FLAG && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y"){
				var nameArr = SisFormService.sisData.sisEKycData.insured.NAME.split(" ");
				this.sfaPage1Data.INSURED_FIRST_NAME = nameArr[0];
				this.firstNameDisabled = true;
				if(nameArr.length>2){
					this.sfaPage1Data.INSURED_MIDDLE = "";
					for(var i=1; i<(nameArr.length-1); i++){
						this.sfaPage1Data.INSURED_MIDDLE += nameArr[i];
						if(i!=(nameArr.length-2))
							this.sfaPage1Data.INSURED_MIDDLE += " ";
					}
					this.sfaPage1Data.INSURED_LAST_NAME = nameArr[(nameArr.length - 1)];
					this.middleNameDisabled = true;
					this.lastNameDisabled = true;
				}
				else{
					this.sfaPage1Data.INSURED_MIDDLE = null;
					this.sfaPage1Data.INSURED_LAST_NAME = nameArr[1];
					this.middleNameDisabled = false;
					this.lastNameDisabled = true;
				}
				if(!!SisFormService.sisData.sisEKycData.insured.DOB){
					sp.sfaPage1Data.INSURED_DOB = CommonService.formatDobFromDb(SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00");
					if((SisFormService.sisData.sisEKycData.insured.DOB.length>9) && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-07")=="-1")){
						sp.ekycDobDisabled = true;
					}
					else{
						sp.ekycDobDisabled = false;
					}
				}
				else{
					sp.ekycDobDisabled = false;
				}
				debug("First Name: " + this.sfaPage1Data.INSURED_FIRST_NAME);
				debug("Middle Name: " + this.sfaPage1Data.INSURED_MIDDLE);
				debug("Last Name: " + this.sfaPage1Data.INSURED_LAST_NAME);
			}
			else{
				sp.ekycDobDisabled = false;
			}
			if(!this.sfaPage1Data.PROPOSER_IS_INSURED || this.sfaPage1Data.PROPOSER_IS_INSURED!="Y"){
				if(!!SisFormService.sisData.sisEKycData.proposer && !!SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG && SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="Y"){
					var nameArr = SisFormService.sisData.sisEKycData.proposer.NAME.split(" ");
					this.sfaPage1Data.PROPOSER_FIRST_NAME = nameArr[0];
					if(nameArr.length>2){
						SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME = "";
						for(i=1; i<(nameArr.length-1); i++){
							SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += nameArr[i];
							if(i!=(nameArr.length-2))
								SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE_NAME += " ";
						}
						SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[(nameArr.length - 1)];
					}
					else{
						SisFormService.sisData.sisFormAData.PROPOSER_MIDDLE = null;
						if(nameArr.length==2){
							SisFormService.sisData.sisFormAData.PROPOSER_LAST_NAME = nameArr[1];
						}
					}
					if(!!SisFormService.sisData.sisEKycData.proposer.DOB){
						SisFormService.sisData.sisFormAData.PROPOSER_DOB = SisFormService.sisData.sisEKycData.proposer.DOB + " 00:00:00";
						if((SisFormService.sisData.sisEKycData.proposer.DOB.length>9) && (SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.proposer.DOB.indexOf("01-07")=="-1")){
							SisFormService.sisData.propEkycDobDisabled = true;
						}
						else{
							SisFormService.sisData.propEkycDobDisabled = false;
						}
					}
					else{
						SisFormService.sisData.propEkycDobDisabled = false; //earlier was true - changed on 14-02-2017
					}
				}
				else{
					SisFormService.sisData.propEkycDobDisabled = false;
				}
			}
			this.leadDataDisabled = false;
		}
	};

	this.onTitleChg = function(){
		debug("this.titleOptionSelected: " + JSON.stringify(this.titleOptionSelected));
		this.sfaPage1Data.INSURED_TITLE = this.titleOptionSelected.TITLE;
		if(this.PGL_ID==VCP_PGL_ID && this.sfaPage1Data.PROPOSER_IS_INSURED != 'Y'){
			this.sfaPage1Data.INSURED_GENDER = "M";
			this.genderDisabled = true;
		}
		else{
			this.sfaPage1Data.INSURED_GENDER = SisFormAService.deriveGender(this.sfaPage1Data.INSURED_TITLE, this.sfaPage1Data.INSURED_GENDER);
			this.genderDisabled = (!!this.sfaPage1Data.INSURED_TITLE) && (this.sfaPage1Data.INSURED_TITLE !== "Dr.");
		}
	};

	this.onIndustryChg = function(){
		var dfd = $q.defer();
		debug("this.industryOptionSelected: " + JSON.stringify(this.industryOptionSelected));
		this.sfaPage1Data.INSURED_OCCU_INDUSTRIES = this.industryOptionSelected.SIS_INDUSTRIES;
		SisFormAService.getOccupationList(this.industryOptionSelected.SIS_INDUSTRIES).then(
			function(occupationList){
				sp.occupationList = occupationList;
				sp.occupationOptionSelected = occupationList[0];
				dfd.resolve(sp.occupationList);
			}
		);
		return dfd.promise;
	};

	this.onOccupationChg = function(){
		var dfd = $q.defer();
		debug("this.occupationOptionSelected: " + JSON.stringify(this.occupationOptionSelected));
		this.sfaPage1Data.INSURED_OCCUPATION = this.occupationOptionSelected.SIS_OCCUPATION;
		SisFormAService.getNatureOfWorkList(this.industryOptionSelected.SIS_INDUSTRIES, this.occupationOptionSelected.SIS_OCCUPATION).then(
			function(natureOfWorkList){
				sp.natureOfWorkList = natureOfWorkList;
				sp.natureOfWorkOptionSelected = natureOfWorkList[0];
				dfd.resolve(sp.natureOfWorkList);
			}
		);
		return dfd.promise;
	};

	this.onInsuredDobChg = function(){
		debug("this.sfaPage1Data.INSURED_DOB: " + this.sfaPage1Data.INSURED_DOB);
		this.insDob = CommonService.formatDobToDb(new Date(this.sfaPage1Data.INSURED_DOB));
		debug("insDob: " + this.insDob);
		var age = CommonService.getAge(this.insDob);
		var prevAge = this.prevAge || ((age<18)?18:17);
		this.prevAge = age;
		debug("age: " + age + " " + prevAge);
		if(age >= ADULT_AGE){ // age>18
			this.insMobRequired = true;
			this.insEmailRequired = true;
		}
		else{
			this.insMobRequired = false;
			this.insEmailRequired = false;
		}
		if(!SfaPage1Service.compStdAgeProofList){
			if((prevAge>=18 && age<18) || (prevAge<18 && age>=18)){
				//CommonService.showLoading("Loading age proof list...please wait...");
				CommonService.getAgeProofList(INSURED_CODE, ((age<18)?"Y":null)).then(
					function(ageProofList){
						if(!!ageProofList){
							sp.ageProofList = ageProofList;
							var ageProofOptionSelected = $filter('filter')(sp.ageProofList, {"DOC_ID": sp.sfaPage1Data.AGE_PROOF_DOC_ID});
							ageProofOptionSelected = (!!ageProofOptionSelected)?ageProofOptionSelected[0]:null;
							sp.ageProofOptionSelected = (!!ageProofOptionSelected)?ageProofOptionSelected:sp.ageProofList[0];
							sp.onAgeProofChg(true);
						}
						//CommonService.hideLoading();
					}
				);
			}
		}
	};

	this.onNatureOfWorkChg = function(){
		debug("mobReq: " + this.mobRequired);
		debug("this.natureOfWorkOptionSelected: " + JSON.stringify(this.natureOfWorkOptionSelected));
		this.sfaPage1Data.INSURED_OCCU_WORK_NATURE = this.natureOfWorkOptionSelected.SIS_NATURE_OF_WORK;
		this.sfaPage1Data.INSURED_OCCU_CODE = this.natureOfWorkOptionSelected.OCCUPATION_CODE;
	};

	this.onAgeProofChg = function(fromCtrl){
		debug("this.ageProofOptionSelected: " + JSON.stringify(this.ageProofOptionSelected));
		this.sfaPage1Data.AGE_PROOF_DOC_ID = this.ageProofOptionSelected.DOC_ID;
		this.sfaPage1Data.AGEPROF_DOC_TYPE = this.ageProofOptionSelected.MPPROOF_DESC;
		this.sfaPage1Data.STD_AGEPROF_FLAG = this.ageProofOptionSelected.STANDARD_FLAG;
		if(!!this.sfaPage1Data.STD_AGEPROF_FLAG && this.sfaPage1Data.STD_AGEPROF_FLAG=="N" && !fromCtrl){
			navigator.notification.alert("Extra premium will be applicable in case of non standard age proof.",null,"SIS","OK");
		}
	};

	this.onCompanyChg = function(){
		this.sfaPage1Data.COMPANY_CODE = this.companyOptionSelected.COMPANY_CODE;
		this.sfaPage1Data.COMPANY_NAME = this.companyOptionSelected.COMPANY_NAME;
	};

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("sisForm.sisEKYC");
	};

	this.onNext = function(isPristine){

		/* Purchasing insurance for is set here as the option is selected by default and onchange doesn't get called hence the data remains null*/

		debug("isPristine: " + isPristine);

		this.sfaPage1Data.PURCHASING_INSURANCE_FOR = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR;
		this.sfaPage1Data.PURCHASING_INSURANCE_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE;

		this.sfaPage1Data.INSURANCE_BUYING_FOR = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR;
		this.sfaPage1Data.INSURANCE_BUYING_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE;

		debug("this.sfaPage1Data: " + JSON.stringify(this.sfaPage1Data));
		SfaPage1Service.setDataInSisFormAData(this.sfaPage1Data);
		CommonService.showLoading("Loading...");
		if(!isPristine || !SisTimelineService.isInsuredTabComplete){
			SisTimelineService.isProposerTabComplete = false;
			SisTimelineService.isPlanTabComplete = false;
			if(!!SisTimelineService.isFundTabComplete && SisTimelineService.isFundTabComplete==true)
				SisTimelineService.isFundTabComplete = false;
			if(!!SisTimelineService.isRiderTabComplete && SisTimelineService.isRiderTabComplete==true)
				SisTimelineService.isRiderTabComplete = false;
			if(!!SisTimelineService.isOtherDetailsTabComplete && SisTimelineService.isOtherDetailsTabComplete==true)
				SisTimelineService.isOtherDetailsTabComplete = false;
			if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
				SisTimelineService.isOutputTabComplete = false;
		}
		SisTimelineService.isInsuredTabComplete = true;
		if(SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y'){
			if((!!SisFormService.sisData.sisEKycData.insured && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y") && ((SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00") != (SisFormService.sisData.sisFormAData.INSURED_DOB))){
				SisFormService.resaveDOBInEKYCOutputIfRequired(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.SIS_ID, "insured",  SisFormService.sisData.sisFormAData.INSURED_DOB).then(
					function(saveRes){
						if(!!saveRes && saveRes=="S"){
							SfaPage1Service.saveProposerData(sp.sfaPage1Data, isPristine).then(
								function(res){
									if(!!res){
										$state.go("sisForm.sisFormB");
									}
									else{
										navigator.notification.alert("Unnable to save proposer's data.",
											function(){
												CommonService.hideLoading();
											}
										,"SIS","OK");
									}
								}
							);
						}
						else{
							navigator.notification.alert("Unnable to save insured's data.",
								function(){
									CommonService.hideLoading();
								}
							,"SIS","OK");
						}
					}
				);
			}
			else{
				SfaPage1Service.saveProposerData(this.sfaPage1Data, isPristine).then(
					function(res){
						if(!!res){
							$state.go("sisForm.sisFormB");
						}
						else{
							navigator.notification.alert("Unnable to save proposer's data.",
								function(){
									CommonService.hideLoading();
								}
							,"SIS","OK");
						}
					}
				);
			}
		}
		else{
			if((!!SisFormService.sisData.sisEKycData.insured && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y") && ((SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00") != (SisFormService.sisData.sisFormAData.INSURED_DOB))){
				SisFormService.resaveDOBInEKYCOutputIfRequired(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.SIS_ID, "insured",  SisFormService.sisData.sisFormAData.INSURED_DOB).then(
					function(saveRes){
						if(!!saveRes && saveRes=="S"){
							$state.go("sisForm.sfaPage2");
						}
						else{
							navigator.notification.alert("Unnable to save insured's data.",
								function(){
									CommonService.hideLoading();
								}
							,"SIS","OK");
						}
					}
				);
			}
			else{
				$state.go("sisForm.sfaPage2");
			}
		}
	};

	if(!SisFormService.sisData.sisMainData.IS_SCREEN1_COMPLETED){
		this.onProposerIsInsuredChg();
		if(!!this.sfaPage1Data.INSURED_DOB){
			this.insDob = CommonService.formatDobToDb(new Date(sp.sfaPage1Data.INSURED_DOB));
			debug("insDob: " + sp.insDob);
			var age = CommonService.getAge(sp.insDob);
			var prevAge = this.prevAge || ((age<18)?18:17);
			sp.prevAge = age;
			debug("age: " + age + " " + prevAge);
			if(age >= ADULT_AGE){ // age>18
				sp.insMobRequired = true;
				sp.insEmailRequired = true;
			}
			else{
				sp.insMobRequired = false;
				sp.insEmailRequired = false;
			}
		}
	}
	else{
		if((!!SisFormService.sisData.sisEKycData.insured && SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=="Y")){ // && ((SisFormService.sisData.sisEKycData.insured.DOB + " 00:00:00") == (SisFormService.sisData.sisFormAData.INSURED_DOB))
			if((SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-01")=="-1") && (SisFormService.sisData.sisEKycData.insured.DOB.indexOf("01-07")=="-1")){
				sp.ekycDobDisabled = true;
			}
			else{
				sp.ekycDobDisabled = false;
			}
		}
		else{
			sp.ekycDobDisabled = false;
		}
	}

}]);

sfaPage1Module.service('SfaPage1Service',['$q', '$filter', 'CommonService', 'SisFormService', 'SisTimelineService', 'SisFormAService', 'SfaPage2Service', function($q, $filter, CommonService, SisFormService, SisTimelineService, SisFormAService, SfaPage2Service){
	"use strict";

	var sc = this;

	this.sfaPage1Data = null;

	this.setSfaPage1Data = function(sfaPage1Data){
		this.sfaPage1Data = sfaPage1Data;
	};

	this.getSfaPage1Data = function(){
		return this.sfaPage1Data;
	};

	this.saveProposerData = function(sfaPage1Data, isPristine){
		var dfd = $q.defer();
		var sfaPage2Data = SisFormAService.getSfaPage2Data(SisFormService.sisData.sisFormAData);
		var apDocId = null;
		if(!!SisFormService.sisData.sisFormAData.AGE_PROOF_DOC_ID){
			SfaPage2Service.getProposerAgeProof(SisFormService.sisData.sisFormAData.AGE_PROOF_DOC_ID).then(
				function(_apDocId){
					if(!!_apDocId){
						apDocId = _apDocId;
						return CommonService.getAgeProofList(PROPOSER_CODE);
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(ageProofList){
					if(!!ageProofList){
						var propAgeProof = $filter('filter')(ageProofList, {"DOC_ID": apDocId})[0];
						if(!!propAgeProof){
							sfaPage2Data.AGE_PROOF_DOC_ID_PR = propAgeProof.DOC_ID;
							sfaPage2Data.AGEPROF_DOC_TYPE_PR = propAgeProof.MPPROOF_DESC;
							sfaPage2Data.STD_AGEPROF_FLAG_PR = propAgeProof.STANDARD_FLAG;
							return SfaPage2Service.onNext(sfaPage2Data, isPristine, true);
						}
						else{
							sfaPage2Data.AGE_PROOF_DOC_ID_PR = null;
							sfaPage2Data.AGEPROF_DOC_TYPE_PR = null;
							sfaPage2Data.STD_AGEPROF_FLAG_PR = "Y";
							return SfaPage2Service.onNext(sfaPage2Data, isPristine, true);
						}
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(finalRes){
					dfd.resolve(finalRes);
				}
			);
		}
		else{
			sfaPage2Data.AGE_PROOF_DOC_ID_PR = null;
			sfaPage2Data.AGEPROF_DOC_TYPE_PR = null;
			sfaPage2Data.STD_AGEPROF_FLAG_PR = "Y";
			dfd.resolve(SfaPage2Service.onNext(sfaPage2Data, isPristine, true));
		}
		return dfd.promise;
	};

	this.setDataInSisFormAData = function(sfaPage1Data){
		if(!SisFormService.sisData.sisFormAData){
			SisFormService.sisData.sisFormAData = {};
		}
		if(SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED!='Y'){
			delete sfaPage1Data.TATA_FLAG;
			delete sfaPage1Data.COMPANY_CODE;
			delete sfaPage1Data.COMPANY_NAME;
		}
		if(!sfaPage1Data.AGE_PROOF_DOC_ID){
			sfaPage1Data.STD_AGEPROF_FLAG = 'Y';
		}
		Object.assign(SisFormService.sisData.sisFormAData, sfaPage1Data);
		SisFormService.sisData.sisFormAData.INSURED_DOB = CommonService.formatDobToDb(SisFormService.sisData.sisFormAData.INSURED_DOB);
	};
}]);
