sfaPage2Module.controller('SfaPage2Ctrl', ['$q','$state','$stateParams', '$filter', 'LoginService', 'CommonService', 'SisFormService', 'SisFormAService', 'SfaPage2Service', 'IndustryList', 'OccupationList', 'NatureOfWorkList', "AgeProofList", "ConvertedAgeProof", 'SisTimelineService', 'CompanyList', 'TataDiscountAvailable', function($q, $state, $stateParams, $filter, LoginService, CommonService, SisFormService, SisFormAService, SfaPage2Service, IndustryList, OccupationList, NatureOfWorkList, AgeProofList, ConvertedAgeProof, SisTimelineService, CompanyList, TataDiscountAvailable){
	"use strict";
	CommonService.hideLoading();

    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	var sp = this;

	SisTimelineService.setActiveTab("proposer");
	debug("in sfaPage2Ctrl " + JSON.stringify(SisFormService.sisData.sisFormAData));
	this.nameRegex = NAME_REGEX;
	this.mNameRegex = FULLNAME_REGEX_SP;
	this.mobileRegex = MOBILE_REGEX;
	this.emailRegex = EMAIL_REGEX;
	this.mNameRegex = FULLNAME_REGEX_SP;

	this.propMinAge = ADULT_AGE;
	this.PGL_ID = SisFormService.sisData.sisMainData.PGL_ID;
	if(this.PGL_ID == VCP_PGL_ID){
		this.propMaxAge = VCP_PROPOSER_AGE;
		SfaPage2Service.CURR_PGL_ID = VCP_PGL_ID;
	}
	this.VCP_PGL_ID = VCP_PGL_ID;


	this.isSelfInsured = (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y');

	this.leadDataDisabled = !!SisFormService.sisData.sisMainData.LEAD_ID;

	this.ekycDataDisabled = false;

	if(!!SisFormService.sisData.sisEKycData.proposer && !!SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG && SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="Y"){
		this.ekycDobDisabled = SisFormService.sisData.propEkycDobDisabled;
		this.ageProofHidden = SisFormService.sisData.propEkycDobDisabled;
		this.ekycDataDisabled = true;
	}
	else if(!SisFormService.sisData.sisEKycData.proposer || !SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG || SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="N"){
		if(!!this.leadDataDisabled)
			this.ekycDobDisabled = true;
		this.ageProofHidden = false;
	}

	if((BUSINESS_TYPE=="GoodSolution" || BUSINESS_TYPE=="iWealth" || BUSINESS_TYPE=="LifePlaner") && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='N')){
		this.leadNonFhrDataDisabled = false;
	}
	else{
		this.leadNonFhrDataDisabled = true;
	}
	this.showEmployeeFlag = (!!TataDiscountAvailable) && (BUSINESS_TYPE=='iWealth') && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED!='Y');

	this.titleList = [
		{"TITLE_DISPLAY": "Select Title", "TITLE": null},
		{"TITLE_DISPLAY": "Mr.","TITLE": "Mr."},
		{"TITLE_DISPLAY": "Mrs.","TITLE": "Mrs."},
		{"TITLE_DISPLAY": "Ms.","TITLE": "Ms."},
		{"TITLE_DISPLAY": "Miss.","TITLE": "Miss."},
		{"TITLE_DISPLAY": "Dr.","TITLE": "Dr."}
	];
	this.industryList = IndustryList;
	this.occupationList = OccupationList;
	this.natureOfWorkList = NatureOfWorkList;
	this.ageProofList = AgeProofList;
	this.companyList = CompanyList;

	this.titleOptionSelected = this.titleList[0];
	this.industryOptionSelected = this.industryList[0];
	this.occupationOptionSelected = this.occupationList[0];
	this.natureOfWorkOptionSelected = this.natureOfWorkList[0];
	this.ageProofOptionSelected = this.ageProofList[0];
	this.companyOptionSelected = this.companyList[0];

	this.sfaPage2Data = SfaPage2Service.sfaPage2Data;

	/*if(SisFormService.sisData.sisMainData.PROPOSER_IS_INSURED!='Y')
		this.TATA_FLAG = SisFormService.sisData.sisMainData.TATA_FLAG;*/

	if(!!this.sfaPage2Data.PROPOSER_TITLE){
		this.titleOptionSelected = $filter('filter')(this.titleList, {"TITLE": this.sfaPage2Data.PROPOSER_TITLE})[0];
	}

	if(!!this.sfaPage2Data.PROPOSER_OCCU_INDUSTRIES){
		this.industryOptionSelected = $filter('filter')(this.industryList, {"SIS_INDUSTRIES": this.sfaPage2Data.PROPOSER_OCCU_INDUSTRIES})[0];
	}

	if(!!sp.sfaPage2Data.PROPOSER_OCCUPATION){
		this.occupationOptionSelected = $filter('filter')(this.occupationList, {"SIS_OCCUPATION": this.sfaPage2Data.PROPOSER_OCCUPATION})[0];
	}

	if(!!sp.sfaPage2Data.PROPOSER_OCCU_WORK_NATURE){
		this.natureOfWorkOptionSelected = $filter('filter')(this.natureOfWorkList, {"OCCUPATION_CODE": this.sfaPage2Data.PROPOSER_OCCU_CODE})[0];
	}

	if(!!this.sfaPage2Data.PROPOSER_DOB)
		this.sfaPage2Data.PROPOSER_DOB = CommonService.formatDobFromDb(this.sfaPage2Data.PROPOSER_DOB);

	this.sfaPage2Data.PROPOSER_GENDER = SisFormAService.deriveGender(this.sfaPage2Data.PROPOSER_TITLE, this.sfaPage2Data.PROPOSER_GENDER);

	if(!!this.sfaPage2Data.PROPOSER_MOBILE)
		this.sfaPage2Data.PROPOSER_MOBILE = parseInt(this.sfaPage2Data.PROPOSER_MOBILE);

	if(!!this.sfaPage2Data.AGE_PROOF_DOC_ID_PR){
		this.ageProofOptionSelected = $filter('filter')(this.ageProofList, {"DOC_ID": this.sfaPage2Data.AGE_PROOF_DOC_ID_PR})[0];
		debug("123: " + this.sfaPage2Data.AGE_PROOF_DOC_ID_PR);
	}
	else if(!!ConvertedAgeProof){
		this.ageProofOptionSelected = $filter('filter')(this.ageProofList, {"DOC_ID": ConvertedAgeProof})[0];
		debug("123 lets see");
	}

	debug("this.ageProofOptionSelected_PR: " + JSON.stringify(this.ageProofOptionSelected));

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("sisForm.sfaPage1");
	};

	this.onTitleChg = function(){
		debug("this.titleOptionSelected: " + JSON.stringify(this.titleOptionSelected));
		this.sfaPage2Data.PROPOSER_TITLE = this.titleOptionSelected.TITLE;
		if(this.PGL_ID==VCP_PGL_ID && !this.isSelfInsured){
			this.sfaPage2Data.PROPOSER_GENDER = "F";
			this.genderDisabled = true;
		}
		else{
			this.sfaPage2Data.PROPOSER_GENDER = SisFormAService.deriveGender(this.sfaPage2Data.PROPOSER_TITLE, this.sfaPage2Data.PROPOSER_GENDER);
			this.genderDisabled = (!!this.sfaPage2Data.PROPOSER_TITLE) && (this.sfaPage2Data.PROPOSER_TITLE !== "Dr.");
		}
	};
	this.onTitleChg();

	this.onIndustryChg = function(){
		var dfd = $q.defer();
		debug("this.industryOptionSelected: " + JSON.stringify(this.industryOptionSelected));
		this.sfaPage2Data.PROPOSER_OCCU_INDUSTRIES = this.industryOptionSelected.SIS_INDUSTRIES;
		SisFormAService.getOccupationList(this.industryOptionSelected.SIS_INDUSTRIES).then(
			function(occupationList){
				sp.occupationList = occupationList;
				sp.occupationOptionSelected =  occupationList[0];
				dfd.resolve(sp.occupationList);
			}
		);
		return dfd.promise;
	};

	this.onOccupationChg = function(){
		var dfd = $q.defer();
		debug("this.occupationOptionSelected: " + JSON.stringify(this.occupationOptionSelected));
		this.sfaPage2Data.PROPOSER_OCCUPATION = this.occupationOptionSelected.SIS_OCCUPATION;
		SisFormAService.getNatureOfWorkList(this.industryOptionSelected.SIS_INDUSTRIES, this.occupationOptionSelected.SIS_OCCUPATION).then(
			function(natureOfWorkList){
				sp.natureOfWorkList = natureOfWorkList;
				sp.natureOfWorkOptionSelected = natureOfWorkList[0];
				dfd.resolve(sp.natureOfWorkList);
			}
		);
		return dfd.promise;
	};

	this.onNatureOfWorkChg = function(){
		debug("this.natureOfWorkOptionSelected: " + JSON.stringify(this.natureOfWorkOptionSelected));
		this.sfaPage2Data.PROPOSER_OCCU_WORK_NATURE = this.natureOfWorkOptionSelected.SIS_NATURE_OF_WORK;
		this.sfaPage2Data.PROPOSER_OCCU_CODE = this.natureOfWorkOptionSelected.OCCUPATION_CODE;
	};

	this.onNext = function(isPristine){
		CommonService.showLoading("Loading...");
		//SisFormService.sisData.sisMainData.TATA_FLAG = this.TATA_FLAG;
		debug("this.sfaPage2Data: " + JSON.stringify(this.sfaPage2Data));
		this.onAgeProofChg();
		var proposerDOB = CommonService.formatDobToDb(sp.sfaPage2Data.PROPOSER_DOB);
		if((!!SisFormService.sisData.sisEKycData.proposer && SisFormService.sisData.sisEKycData.proposer.EKYC_FLAG=="Y") && ((SisFormService.sisData.sisEKycData.proposer.DOB + " 00:00:00") != (proposerDOB))){
			SisFormService.resaveDOBInEKYCOutputIfRequired(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.SIS_ID, "proposer", proposerDOB).then(
				function(saveRes){
					if(!!saveRes && saveRes=="S"){
						SfaPage2Service.onNext(sp.sfaPage2Data, isPristine).then(
							function(res){
								if(!!res){
									$state.go("sisForm.sisFormB");
								}
								else{
									CommonService.hideLoading();
								}
							}
						);
					}
					else{
						navigator.notification.alert("Unnable to save proposer's data.",
							function(){
								CommonService.hideLoading();
							}
						,"SIS","OK");
					}
				}
			);
		}
		else{
			SfaPage2Service.onNext(this.sfaPage2Data, isPristine).then(
				function(res){
					if(!!res){
						$state.go("sisForm.sisFormB");
					}
					else{
						CommonService.hideLoading();
					}
				}
			);
		}
	};

	this.onAgeProofChg = function(){
		debug("this.ageProofOptionSelected: " + JSON.stringify(this.ageProofOptionSelected));
		this.sfaPage2Data.AGE_PROOF_DOC_ID_PR = this.ageProofOptionSelected.DOC_ID;
		this.sfaPage2Data.AGEPROF_DOC_TYPE_PR = this.ageProofOptionSelected.MPPROOF_DESC;
		this.sfaPage2Data.STD_AGEPROF_FLAG_PR = this.ageProofOptionSelected.STANDARD_FLAG;
	};

	this.onCompanyChg = function(){
		this.sfaPage2Data.COMPANY_CODE = this.companyOptionSelected.COMPANY_CODE;
		this.sfaPage2Data.COMPANY_NAME = this.companyOptionSelected.COMPANY_NAME;
	};

	if(!!this.sfaPage2Data.COMPANY_CODE){
		this.companyOptionSelected = $filter('filter')(this.companyList, {"COMPANY_CODE": sp.sfaPage2Data.COMPANY_CODE})[0];
	}
	debug("this.companyOptionSelected()::: " + JSON.stringify(this.companyOptionSelected));

	this.onBack = function(){
		CommonService.showLoading("Loading...");
		$state.go("sisForm.sfaPage1");
	};
}]);

sfaPage2Module.service('SfaPage2Service', ['$q', 'CommonService', 'SisTimelineService', 'SisFormService', 'SisFormAService', function($q, CommonService, SisTimelineService, SisFormService, SisFormAService){
	"use strict";
	this.sfaPage2Data = null;

	this.setSfaPage2Data = function(sfaPage2Data){
		this.sfaPage2Data = sfaPage2Data;
	};

	this.getSfaPage2Data = function(){
		return this.sfaPage2Data;
	};

	this.setDataInSisFormAData = function(sfaPage2Data, isSelf){
		if(SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y'){
			delete sfaPage2Data.TATA_FLAG;
			delete sfaPage2Data.COMPANY_CODE;
			delete sfaPage2Data.COMPANY_NAME;
		}
		if(!sfaPage2Data.AGE_PROOF_DOC_ID_PR){
			sfaPage2Data.STD_AGEPROF_FLAG_PR = 'Y';
		}
		Object.assign(SisFormService.sisData.sisFormAData, sfaPage2Data);
		if(!isSelf){
			SisFormService.sisData.sisFormAData.PROPOSER_DOB = CommonService.formatDobToDb(SisFormService.sisData.sisFormAData.PROPOSER_DOB);
		}
		SisFormService.sisData.sisMainData.IS_SCREEN1_COMPLETED = 'Y';
	};

	this.getProposerAgeProof = function(insAgeProofDocId){
		var dfd = $q.defer();
		try{
			debug("getProposerAgeProof before : " + insAgeProofDocId);
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx, "select doc_id from lp_doc_proof_master where customer_category=? and mpproof_code=(select mpproof_code from lp_doc_proof_master where doc_id=?) and mpdoc_code=(select mpdoc_code from lp_doc_proof_master where doc_id=?)",
						[PROPOSER_CODE, insAgeProofDocId, insAgeProofDocId],
						function(tx,res){
							debug("getProposerAgeProof: " + res.rows.length);
							if(!!res && res.rows.length>0)
								dfd.resolve(res.rows.item(0).DOC_ID);
							else
								dfd.resolve(null);
						},
						function(tx,err){
							dfd.resolve(null);
						}

					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		catch(ex){
			debug("Exception: " + ex.message);
		}
		return dfd.promise;
	};

	this.onNext = function(sfaPage2Data, isPristine, isSelf) {
		var dfd = $q.defer();
		this.setDataInSisFormAData(sfaPage2Data, isSelf);
		SisFormAService.insertDataInSisScreenATable().then(
			function(res){
				if(!!res){
					SisFormService.sisData.sisMainData.IS_SCREEN1_COMPLETED = 'Y';
					if(!isPristine || !SisTimelineService.isProposerTabComplete){
						SisFormService.sisData.sisMainData.IS_SCREEN2_COMPLETED = 'N';
						SisTimelineService.isPlanTabComplete = false;
						if((!!SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED) && (SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED=="Y")){
							SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED = 'N';
							SisTimelineService.isFundTabComplete = false;
						}
						if((!!SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED) && (SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED=="Y")){
							SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED = 'N';
							SisTimelineService.isFundTabComplete = false;
						}
						if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
							SisTimelineService.isOutputTabComplete = false;
					}
					SisFormService.insertDataInSisMainTable().then(
						function(sisMainRes){
							if(!!sisMainRes){
								SisTimelineService.isProposerTabComplete = true;
								SisFormService.sisData.ekycDisabled = true;

								/*update opportunity to update insured and proposer names in opportunity table in case of non-fhr & non-ff cases.*/
								SisFormService.createOrUpdateOpportunity(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.sisMainData.SIS_ID, null, null, true).then(
									function(oppRes){
										dfd.resolve(oppRes);
									}
								);
							}
							else{
								dfd.resolve(null);
							}
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

}]);
