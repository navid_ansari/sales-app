sisFormAModule.directive('validateInsuredDob',['CommonService', 'SisFormService', function(CommonService,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				debug("PNL_MIN_AGE: " + SisFormService.getProdMinAge());
				debug("PNL_MAX_AGE: " + SisFormService.getProdMaxAge());
				ctrl.$validators.insMinAge = function(ngModelValue, viewValue){
					var newDateFormat = viewValue.substring(8,10) + "-" + viewValue.substring(5,7) + "-" + viewValue.substring(0,4);
					var date = newDateFormat + " 00:00:00";
					debug("date: " + date);
					var minDate = null;
					if(SisFormService.getProdMinAge()==0){
						minDate = new Date();
						minDate.setDate(minDate.getDate()-30);
					}
					if(CommonService.getAge(date)<SisFormService.getProdMinAge())
						return false;
					else if(!!minDate && (minDate < (new Date(ngModelValue))))
						return false;
					else
						return true;
				}
				ctrl.$validators.insMaxAge = function(ngModelValue, viewValue){
					var newDateFormat = viewValue.substring(8,10) + "-" + viewValue.substring(5,7) + "-" + viewValue.substring(0,4);
					var date = newDateFormat + " 00:00:00";
					debug("date: " + date);
					var minDate = null;
					if(SisFormService.getProdMinAge()==0){
						minDate = new Date();
						minDate.setDate(minDate.getDate()-30);
					}
					if(CommonService.getAge(date)>SisFormService.getProdMaxAge())
						return false;
					else
						return true;
				}
				ctrl.$validators.insMinAge_Adult = function(ngModelValue, viewValue){
					var newDateFormat = viewValue.substring(8,10) + "-" + viewValue.substring(5,7) + "-" + viewValue.substring(0,4);
					var date = newDateFormat + " 00:00:00";
					debug("date: " + date + " " + SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED);
					if((!!SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED) && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED=="Y") && (CommonService.getAge(date)<ADULT_AGE))
						return false;
					else
						return true;
				}
				ctrl.$validators.insSpouseMinAge_Adult = function(ngModelValue, viewValue){
					var newDateFormat = viewValue.substring(8,10) + "-" + viewValue.substring(5,7) + "-" + viewValue.substring(0,4);
					var date = newDateFormat + " 00:00:00";
					debug("date: " + date + " " + SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED);
					debug("INSURANCE_BUYING_FOR_CODE: " + SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE);
					if((!!SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED) && (SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED!="Y") && (!!SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE) && (SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE=="75") && (CommonService.getAge(date)<ADULT_AGE))
						return false;
					else
						return true;
				}
			}
	};
}]);


sisFormAModule.directive('validateProposerDob',['CommonService','SfaPage2Service', function(CommonService,SfaPage2Service){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					debug("ngModelValue: " + JSON.stringify(ngModelValue));
					if(!!ngModelValue){
						var newDateFormat = ngModelValue.substring(8,10) + "-" + ngModelValue.substring(5,7) + "-" + ngModelValue.substring(0,4);
						var date = newDateFormat + " 00:00:00";
						if(CommonService.getAge(date)<ADULT_AGE)
							ctrl.$setValidity('propMinAge', false);
						else
							ctrl.$setValidity('propMinAge', true);
						if(!!SfaPage2Service.CURR_PGL_ID && SfaPage2Service.CURR_PGL_ID == VCP_PGL_ID){
							if(CommonService.getAge(date)>VCP_PROPOSER_AGE)
								ctrl.$setValidity('propMaxAge', false);
							else
								ctrl.$setValidity('propMaxAge', true);
						}
					}
					else if(!!ngModelValue && ngModelValue===""){
						ctrl.$setValidity('propMinAge', true);
						ctrl.$setValidity('propMaxAge', true);
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
				ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
}]);
