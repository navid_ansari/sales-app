sisFormDModule.controller('SisFormDCtrl',['$state', 'CommonService', 'SisFormService', 'SisFormDService', 'RidersList', 'SisTimelineService',function($state, CommonService, SisFormService, SisFormDService, RidersList, SisTimelineService){
	debug("in SisFormDCtrl");
	CommonService.hideLoading();

    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	SisTimelineService.setActiveTab("rider");
	var sc = this;

	debug("RidersList: " + JSON.stringify(RidersList));

	this.onRiderSelectChange = function(riderSelected, riderCode, loadingSavedData, riderName){
		debug("riderSelected" + riderSelected);
		debug("riderCode" + riderCode);
		if(riderSelected=='Y'){
			angular.forEach(this.ridersList, function(rider){
				if(riderCode!=rider.RDL_CODE){
					rider.RIDER_SELECTED = 'N';
					rider.DISABLED = true;
					rider.INPUT_DISABLED = true;
					rider.SUM_ASSURED = null;
					rider.MIN_SA = null;
					rider.MAX_SA = null;
					rider.MULTIPLE_OF = null;
				}
				else{
					rider.RIDER_SELECTED = 'Y';
					rider.INPUT_DISABLED = false;
					rider.DISABLED = false;
					if(!loadingSavedData)
						rider.SUM_ASSURED = SisFormDService.getSumAssured(riderCode, rider.RPX_MIN_SUM_ASSURED);
					rider.MIN_SA = rider.RPX_MIN_SUM_ASSURED;
					if(rider.MIN_SA==0 && riderCode.charAt(0)=='W')
						rider.INPUT_DISABLED = true;
					rider.MAX_SA = SisFormDService.getMaxSumAssured(riderCode, rider.SUM_ASSURED, rider.RPX_MAX_SUM_ASSURED);
					debug("min sa: " + rider.MIN_SA);
					debug("max sa: " + rider.MAX_SA);
					rider.MULTIPLE_OF = rider.RPX_PREMIUM_MULTIPLE;
					SisFormDService.MIN_SA = rider.MIN_SA;
					SisFormDService.MAX_SA = rider.MAX_SA;
					debug("riderName: " + riderName);
					if(!!riderName)
						sc.sisFormD[riderName].$validate();
				}
			});
		}
		else{
			angular.forEach(this.ridersList, function(rider){
				rider.RIDER_SELECTED = 'N';
				rider.DISABLED = false;
				rider.INPUT_DISABLED = true;
				rider.SUM_ASSURED = null;
				rider.MIN_SA = null;
				rider.MAX_SA = null;
				rider.MULTIPLE_OF = null;
			});
		}
	};

	if(!!RidersList && RidersList.length>0){
		this.ridersList = RidersList;
		angular.forEach(this.ridersList, function(rider){
			if(rider.RIDER_SELECTED=="Y")
				sc.onRiderSelectChange(rider.RIDER_SELECTED, rider.RDL_CODE, true);
		});
	}
	else
		$state.go("sisForm.sisOutput");

	this.onNext = function(isPristine){
		var baseAnnPrem = SisFormService.sisData.sisFormBData.INPUT_ANNUAL_PREMIUM;
		debug("on next : " + baseAnnPrem);
		SisFormDService.getRiderPrem(this.ridersList[0]).then(
			function(riderPrem){
				debug("riderPrem : " + riderPrem);
		        sc.ridersList[0].RIDER_ANNUAL_PREMIUM = riderPrem;
				if(!!riderPrem && (riderPrem > (baseAnnPrem * 0.30))){
					navigator.notification.alert("Total Rider Premium should not be greater than 30% of base premium",null,"SIS","OK");
				}else{
		        	if(!!SisTimelineService.isOtherDetailsTabComplete && SisTimelineService.isOtherDetailsTabComplete==true){
						SisTimelineService.isOtherDetailsTabComplete = false;
					}
					SisFormDService.saveRiderData(sc.ridersList);
					SisFormDService.insertDataInSisScreenDTable();
					SisTimelineService.isRiderTabComplete = true;
					if(!isPristine || !SisTimelineService.isRiderTabComplete){
						SisTimelineService.isOutputTabComplete = false;
					}
					debug("going to sisOutput");
					SisFormService.gotoTAorOutput();
                    //$state.go("sisForm.sisOutput");
				}
			}
		);
	};

	this.onBack = function(){
		var productType = (SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
		if(!!productType && productType=="C"){
			CommonService.showLoading("Loading...");
			$state.go("sisForm.sisFormB");
		}
		else{
			CommonService.showLoading("Loading...");
			$state.go("sisForm.sisFormC");
		}
	};

}]);

sisFormDModule.service('SisFormDService', ['$q', 'CommonService', 'SisFormService', function($q, CommonService, SisFormService){

	var sc = this;
	this.MIN_SA = null;
	this.MAX_SA = null;
	this.getSumAssured = function(riderCode, riderMinSA){
		var planCode = SisFormService.sisData.sisFormBData.PLAN_CODE;
		var planSA = SisFormService.sisData.sisFormBData.SUM_ASSURED;
		if(!!riderCode && (riderCode=="ADDLULV1" || riderCode=="ADDLULV2" || riderCode=="ADDLN1V1")){
			if(!!planCode && (planCode=="IPR7ULN1" || planCode=="IPR7ULN2" || planCode=="IPR7ULN3" || planCode=="IPR7ULN4" || planCode=="IMX8ULN1" || planCode=="WPR5ULN1" || planCode=="WPR5ULN2" || planCode=="WPR5ULN3" || planCode=="WPR5ULN4" || planCode=="WPR7ULN1" || planCode=="WPR7ULN2" || planCode=="WPR7ULN3" || planCode=="WPR7ULN4" || planCode=="WMX7ULN1")){
					return (parseInt(planSA) / 2);
			}
			else
				return parseInt(planSA);
		}
		else if(!!planSA){
			return parseInt(riderMinSA);
		}
	};

	this.getMaxSumAssured = function(riderCode, riderSA, riderMaxSA){
		var planCode = SisFormService.sisData.sisFormBData.PLAN_CODE;
		var planSA = SisFormService.sisData.sisFormBData.SUM_ASSURED;
		if(!!riderCode && ((parseInt(riderSA))<=(parseInt(planSA)))){
			if(!!planCode && (planCode=="IPR7ULN1" || planCode=="IPR7ULN2" || planCode=="IPR7ULN3" || planCode=="IPR7ULN4" || planCode=="IMX8ULN1" || planCode=="WPR5ULN1" || planCode=="WPR5ULN2" || planCode=="WPR5ULN3" || planCode=="WPR5ULN4" || planCode=="WPR7ULN1" || planCode=="WPR7ULN2" || planCode=="WPR7ULN3" || planCode=="WPR7ULN4" || planCode=="WMX7ULN1")){
				return (parseInt(planSA) / 2);
			}
			else if(!!riderMaxSA){
				return ((parseInt(riderMaxSA)<parseInt(planSA))?(parseInt(riderMaxSA)):(parseInt(planSA)));
			}
			else{
				return (parseInt(planSA)<10000000)?parseInt(planSA):10000000;
			}
		}
		else
			return parseInt(planSA);
	};

	this.saveRiderData = function(ridersList){
		SisFormService.sisData.sisFormDData = [];
		var riderSeqNo = 2;
		angular.forEach(ridersList,function(rider){
			if(rider.RIDER_SELECTED=="Y"){
				var sisFormDData = {};
				sisFormDData.SIS_ID = SisFormService.sisData.sisMainData.SIS_ID;
				sisFormDData.AGENT_CD = SisFormService.sisData.sisMainData.AGENT_CD;
				sisFormDData.RIDER_SEQ_NO = riderSeqNo;
				sisFormDData.RIDER_CODE = rider.RDL_CODE;
				sisFormDData.RIDER_NAME = rider.RDL_DESCRIPTION;
				sisFormDData.RIDER_PAY_MODE = SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE;
				sisFormDData.RIDER_PAY_MODE_DESC = SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE_DESC;
				sisFormDData.RIDER_SERVICE_TAX = rider.RDL_SERVICE_TAX;
				sisFormDData.RIDER_PREM_MULTIPLIER = "0";
				sisFormDData.RIDER_COVERAGE = rider.SUM_ASSURED;
				sisFormDData.RIDER_INVT_STRAT_FLAG = 'S';
				sisFormDData.RIDER_EFFECTIVE_DATE = null;
				sisFormDData.RIDER_UNIT = "0";//null
				sisFormDData.RIDER_BASE_PREMIUM = '0';
				sisFormDData.RIDER_ANNUAL_PREMIUM = rider.RIDER_ANNUAL_PREMIUM || '0';
				sisFormDData.MODIFIED_DATE = CommonService.getCurrDate();
				SisFormService.sisData.sisFormDData.push(sisFormDData);
				riderSeqNo++;
			}
		});
	};

	this.insertDataInSisScreenDTable = function(){
		CommonService.deleteRecords(db, 'lp_sis_screen_d', {"SIS_ID": SisFormService.sisData.sisMainData.SIS_ID, "AGENT_CD": SisFormService.sisData.sisMainData.AGENT_CD}).then(
			function(delRes){
				debug("SisFormService.sisData.sisFormDData: " + JSON.stringify(SisFormService.sisData.sisFormDData));
				if(!!delRes){
					angular.forEach(SisFormService.sisData.sisFormDData,function(riderData){
						CommonService.insertOrReplaceRecord(db, "lp_sis_screen_d", riderData, true).then(
							function(res){
								if(!!res){
									debug("Inserted record in LP_SIS_SCREEN_D successfully: SIS_ID: " + SisFormService.sisData.sisMainData.SIS_ID + " " + riderData.RIDER_SEQ_NO);
								}
								else{
									debug("Couldn't insert record in LP_SIS_SCREEN_D");
								}
							}
						);
					});
				}
			}
		);
	};


	this.updateSisScreenDTable = function(riderDataArray){
		angular.forEach(riderDataArray,function(riderData){
			var updateClause = {"RIDER_TERM": riderData.PolicyTerm, "RIDER_PAY_TERM": riderData.PremiumPayTerm, "RIDER_MODAL_PREMIUM": riderData.ModalPremium, "RIDER_SERVICE_TAX": riderData.RiderServiceTax};
			var whereClause = {"SIS_ID": SisFormService.sisData.sisMainData.SIS_ID, "RIDER_CODE": riderData.RiderCode, "AGENT_CD": SisFormService.sisData.sisMainData.AGENT_CD};
			CommonService.updateRecords(db, 'lp_sis_screen_d', updateClause, whereClause).then(
				function(res){
					debug("udpated rider: " + whereClause.RIDER_CODE);
				}
			);
		});
	};

	this.getSavedData = function(ridersList){
		var savedRidersList = SisFormService.sisData.sisFormDData;
		angular.forEach(ridersList,function(rider){
			angular.forEach(savedRidersList,function(savedRider){
				if(rider.RDL_CODE==savedRider.RIDER_CODE){
					rider.RIDER_SELECTED = "Y";
					if(!!savedRider.RIDER_COVERAGE)
						rider.SUM_ASSURED = parseInt(savedRider.RIDER_COVERAGE);
				}
			});
		});
		return ridersList;
	};

	this.getRidersList = function(){
		var dfd = $q.defer();
		var planCode = SisFormService.sisData.sisFormBData.PLAN_CODE;
		var premiumPayTerm = SisFormService.sisData.sisFormBData.PREMIUM_PAY_TERM;
		var insAge = CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB);
		var propAge = CommonService.getAge(SisFormService.sisData.sisFormAData.PROPOSER_DOB);
		var insOccuCode = "W10";//SisFormService.sisData.sisFormAData.INSURED_OCCU_CODE;
		var propOccuCode = "W10";//SisFormService.sisData.sisFormAData.PROPOSER_OCCU_CODE;
		var insStdAgeProofFlag = SisFormService.sisData.sisFormAData.STD_AGEPROF_FLAG;
		var propStdAgeProofFlag = SisFormService.sisData.sisFormAData.STD_AGEPROF_FLAG_PR;

		var selfInsured = SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED;

		debug("[planCode, insAge, propAge, propAge, insOccuCode]: " + [planCode, insAge, propAge, propAge, insOccuCode]);

		CommonService.transaction(sisDB,
			function(tx){
				CommonService.executeSql(tx, "SELECT RDL.RDL_ID, RDL.RDL_CODE, RDL.RDL_DESCRIPTION, RDL.RDL_SERVICE_TAX, RPX.RPX_PNL_ID, RPX.RPX_RDL_ID, RPX.RPX_MIN_SUM_ASSURED, RPX.RPX_MAX_SUM_ASSURED, RPX.RPX_MIN_AGE, RPX.RPX_MAX_AGE, RPX.RPX_PREMIUM_MULTIPLE, RPX.RPX_OWNER_MIN_AGE, RPX.RPX_OWNER_MAX_AGE, RPX.RPX_AGENTTYPE FROM LP_RDL_RIDER_LK RDL, LP_RPX_RIDER_PLAN_XREF RPX WHERE RDL.RDL_ID = RPX.RPX_RDL_ID AND RPX.RPX_PNL_ID IN (SELECT PNL_ID FROM LP_PNL_PLAN_LK WHERE PNL_CODE = ?) AND (? BETWEEN CAST(RPX.RPX_MIN_AGE as int) AND CAST(RPX.RPX_MAX_AGE as int)) AND (((RPX.RPX_OWNER_MIN_AGE IS NULL) OR (RPX.RPX_OWNER_MIN_AGE IS NOT NULL) and (? >= CAST(RPX.RPX_OWNER_MIN_AGE as int))) AND ((RPX.RPX_OWNER_MAX_AGE IS NULL) OR ((RPX.RPX_OWNER_MAX_AGE IS NOT NULL) AND (? <= CAST(RPX.RPX_OWNER_MAX_AGE as int))))) AND ((SELECT OCL_CLASS FROM LP_OCL_OCCUPATION_LK WHERE OCL_CODE=?) BETWEEN CAST(RDL.MINOCC AS INT) AND CAST(RDL.MAXOCC AS INT))",
					[planCode, insAge, propAge, propAge, insOccuCode],
					function(tx,res){
						if(!!res && res.rows.length>0){
							res = CommonService.resultSetToObject(res);
							if(!(res instanceof Array))
								res = [res];
							debug("res rider: " + JSON.stringify(res));

							var ageCondition = ((parseInt(propAge)) + (parseInt(premiumPayTerm))>65);
							var propOccuCodeCondition = ((propOccuCode=="Z64") || (propOccuCode=="O21") || (propOccuCode=="Z87") || (propOccuCode=="Z69") || (propOccuCode=="ZA0"));
							var wppn1v1SkipCondition = ((selfInsured=="Y") || (propStdAgeProofFlag=="N") || (ageCondition) || (propOccuCodeCondition));
							var ridersList = [];
							angular.forEach(res, function(rider){
								if(((rider.RDL_CODE=="WPPN1V1") && (wppn1v1SkipCondition))){

								}
								else if(((rider.RDL_CODE=="WOPPULV1") && (selfInsured=="Y"))){

								}
								else if(((rider.RDL_CODE=="WOPULV1") && (selfInsured!="Y"))){

								}
								else{
									rider.DISABLED=false;
									rider.INPUT_DISABLED = true;
									rider.RIDER_SELECTED='N';
									ridersList.push(rider);
								}
							});

							dfd.resolve(ridersList);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},null,
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getRiderPrem = function(riderData){
		var dfd = $q.defer();
		var insOccuCode = "W10";//SisFormService.sisData.sisFormAData.INSURED_OCCU_CODE;
		var riderPremium = 0;
		debug("insOccuCode : " + insOccuCode + "\rriderData : " + JSON.stringify(riderData));
		CommonService.transaction(sisDB,
			function(tx){
				CommonService.executeSql(tx, "select RML_PREM_MULT from LP_RML_RIDER_PREM_MULTIPLE_LK where (RML_BAND = (select OCL_CLASS from LP_OCL_OCCUPATION_LK where OCL_CODE = ?)) and (RML_RIDER_CD = ?)", [insOccuCode,riderData.RDL_CODE],
					function(tx,res){
						if(!!res && res.rows.length>0){
							rpm = res.rows.item(0).RML_PREM_MULT;
							debug("rpm : " + rpm);
							riderPremium = Math.round((rpm) * (riderData.SUM_ASSURED) / 1000);
							debug("riderPremium : " + riderPremium);
							dfd.resolve(riderPremium);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},null,
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
