sisFormDModule.directive('rsaMultipleOf',[function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			'rsaMultipleOf': '='
		},
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					if(!!scope.rsaMultipleOf && scope.rsaMultipleOf>0){
						if(!!ngModelValue){
							if((parseInt(ngModelValue) % parseInt(scope.rsaMultipleOf)) === 0)
								ctrl.$setValidity('rsaMultipleOf', true);
							else
								ctrl.$setValidity('rsaMultipleOf', false);
						}
						else if(!!ngModelValue && ngModelValue==="")
							ctrl.$setValidity('rsaMultipleOf', false);
					}
					else
						ctrl.$setValidity('rsaMultipleOf', true);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
}]);

sisFormDModule.directive('riderMinMax',['SisFormDService', function(SisFormDService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.riderMin = function(ngModelValue, viewValue){
					var riderMin = SisFormDService.MIN_SA;
					if(!!riderMin && !!viewValue){
						if(parseInt(viewValue)<parseInt(riderMin))
							return false;
						else
							return true;
					}
					else
						return null;
				};
				ctrl.$validators.riderMax = function(ngModelValue, viewValue){
					var riderMax = SisFormDService.MAX_SA;
					if(!!riderMax && !!viewValue){
						if(parseInt(viewValue)>parseInt(riderMax))
							return false;
						else
							return true;
					}
					else
						return null;
				};
			}
	};
}]);
