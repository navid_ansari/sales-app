sisFormFModule.controller('SisFormFCtrl',['$state', '$filter', 'CommonService', 'SisFormService', 'SisFormFService', 'SisTimelineService', 'ResidentCountryList', 'StateList', 'CityList','EduQualList','OccClassList','SfePage1Service',function($state, $filter, CommonService, SisFormService, SisFormFService, SisTimelineService, ResidentCountryList, StateList, CityList, EduQualList, OccClassList, SfePage1Service){
	debug("in SisFormFCtrl");
	CommonService.hideLoading();
	//SisTimelineService.setActiveTab("rider");
	var sc = this;
	sc.freeTxtRegex = FREE_TEXT_REGEX;
	this.onNextClick = false;
	this.nationalityList = [
		{"NATIONALITY_DISPLAY": "Select ", "NATIONALITY_VALUE":null},
		{"NATIONALITY_DISPLAY": "Indian", "NATIONALITY_VALUE":"Indian"},
		{"NATIONALITY_DISPLAY": "Other", "NATIONALITY_VALUE":"Other"}
	];

	this.residentStatusList = [
		{"RESIDENT_STATUS": "Select ", "RESIDENT_STATUS_CODE":null},
		{"RESIDENT_STATUS": "Resident Indian", "RESIDENT_STATUS_CODE":"RI"},
		{"RESIDENT_STATUS": "Non Resident Indian", "RESIDENT_STATUS_CODE":"NRI"},
		{"RESIDENT_STATUS": "Foreign National", "RESIDENT_STATUS_CODE":"FN"},
		{"RESIDENT_STATUS": "Overseas Citizen of India", "RESIDENT_STATUS_CODE":"OCI"}
	];

	this.residentCountryList = ResidentCountryList;
	this.stateList = StateList;
	this.cityList = CityList;
	this.eduQualList = EduQualList;
	this.occClassList = OccClassList;

	this.residentStatusOptionSelected = this.residentStatusList[0];
	this.nationalityOptionSelected = this.nationalityList[0];
	this.residentCountryOptionSelected = this.residentCountryList[0];
	this.stateOptionSelected = this.stateList[0];
	this.cityOptionSelected = this.cityList[0];
	this.eduQualOptionSelected = this.eduQualList[0];
	this.occClassOptionSelected = this.occClassList[0];

	this.sisFormFData = SisFormFService.sisFormFData;
	debug("this.sisFormFData : " + JSON.stringify(this.sisFormFData));
	/*if(!!this.sisFormFData.GROUP_EMPLOYEE){
		this.stateOptionSelected = $filter('filter')(this.stateList, {"STATE_CODE": this.sisFormFData.CURR_STATE_CODE}, true)[0];
	}*/

	if(!!this.sisFormFData.NATIONALITY && ((!!this.nationalityList && this.nationalityList.length>0))){
		this.nationalityOptionSelected = $filter('filter')(this.nationalityList, {"NATIONALITY_VALUE": this.sisFormFData.NATIONALITY}, true)[0];
	}

	if(!!this.sisFormFData.RESIDENT_STATUS_CODE && ((!!this.residentStatusList && this.residentStatusList.length>0))){
		this.residentStatusOptionSelected = $filter('filter')(this.residentStatusList, {"RESIDENT_STATUS_CODE": this.sisFormFData.RESIDENT_STATUS_CODE}, true)[0];
	}
	if(!!this.sisFormFData.CURR_RESIDENT_COUNTRY_CODE && ((!!this.residentCountryList && this.residentCountryList.length>0))){
		this.residentCountryOptionSelected = $filter('filter')(this.residentCountryList, {"COUNTRY_CODE": this.sisFormFData.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
	}

	if(!!this.sisFormFData.CURR_STATE_CODE && ((!!this.stateList && this.stateList.length>0))){
		this.stateOptionSelected = $filter('filter')(this.stateList, {"STATE_CODE": this.sisFormFData.CURR_STATE_CODE}, true)[0];
	}

	if(!!this.sisFormFData.CURR_CITY_CODE && ((!!this.cityList && this.cityList.length>0))){
		this.cityOptionSelected = $filter('filter')(this.cityList, {"CITY_CODE": this.sisFormFData.CURR_CITY_CODE}, true)[0];
	}

	if(!!this.sisFormFData.OCCUPATION_CLASS_NBFE_CODE && ((!!this.occClassList && this.occClassList.length>0))){
		this.occClassOptionSelected = $filter('filter')(this.occClassList, {"NBFE_CODE": this.sisFormFData.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
	}
	if(!!this.sisFormFData.IBL_JOINING_DATE){
		this.sisFormFData.IBL_JOINING_DATE = CommonService.formatDobFromDb(sc.sisFormFData.IBL_JOINING_DATE);
	}

	this.onGrpEmpChange = function(){
		sc.residentStatusOptionSelected = this.residentStatusList[0];
		sc.onResidenceChg();
		if(sc.sisFormFData.GROUP_EMPLOYEE=='IBL Employee'){

		}else {
			sc.sisFormFData.IBL_JOINING_DATE = null;
		}
	};

	this.onNationalityChg = function(){
		sc.sisFormFData.NATIONALITY = sc.nationalityOptionSelected.NATIONALITY_VALUE;
		sc.residentStatusOptionSelected = sc.residentStatusList[0];
		sc.onResidenceChg();
	};

	this.onResidenceChg = function(){
		debug("sc.residentStatusOptionSelected : " + JSON.stringify(sc.residentStatusOptionSelected));
		sc.sisFormFData.RESIDENT_STATUS = sc.residentStatusOptionSelected.RESIDENT_STATUS;
		sc.sisFormFData.RESIDENT_STATUS_CODE = sc.residentStatusOptionSelected.RESIDENT_STATUS_CODE;
		sc.residentCountryOptionSelected = this.residentCountryList[0];
		sc.stateOptionSelected = this.stateList[0];
		sc.cityOptionSelected = this.cityList[0];
		sc.onCountryChg();
		sc.onStateChange();
		sc.onCityChg();
		if(!!sc.sisFormFData.RESIDENT_STATUS_CODE && sc.sisFormFData.RESIDENT_STATUS_CODE!='RI'){
			sc.sisFormF.residentCountry.$setValidity('selectRequired', false);
			sc.sisFormF.state.$setValidity('selectRequired', true);
			sc.sisFormF.city.$setValidity('selectRequired', true);
		}else if(!!sc.sisFormFData.RESIDENT_STATUS_CODE && sc.sisFormFData.RESIDENT_STATUS_CODE=='RI'){
			sc.sisFormF.residentCountry.$setValidity('selectRequired', true);
			sc.sisFormF.state.$setValidity('selectRequired', false);
			sc.sisFormF.city.$setValidity('selectRequired', false);
		}
	};

	this.onCountryChg = function(){
		debug("residentCountryOptionSelected : " + JSON.stringify(sc.residentCountryOptionSelected));
		sc.sisFormFData.CURR_RESIDENT_COUNTRY = sc.residentCountryOptionSelected.COUNTRY_NAME;
		sc.sisFormFData.CURR_RESIDENT_COUNTRY_CODE = sc.residentCountryOptionSelected.COUNTRY_CODE;
		sc.sisFormFData.COUNTRY_TERM_FLAG = sc.residentCountryOptionSelected.TERM_FLAG;
	};

	this.onStateChange = function(){
		debug("stateOptionSelected : " + JSON.stringify(sc.stateOptionSelected));
		sc.sisFormFData.CURR_STATE = sc.stateOptionSelected.STATE_DESC;
		sc.sisFormFData.CURR_STATE_CODE = sc.stateOptionSelected.STATE_CODE;
		SisFormFService.getCityList(sc.stateOptionSelected.STATE_CODE || 0).then(
			function(CityList){
				sc.cityList = CityList;
				debug("CityList refresh:: " + CityList.length);
				sc.cityOptionSelected = CityList[0];
			}
		);
	};

	this.onCityChg = function(){
		debug("cityOptionSelected : " + JSON.stringify(sc.cityOptionSelected));
		sc.sisFormFData.CURR_CITY = sc.cityOptionSelected.CITY_DESC;
		sc.sisFormFData.CURR_CITY_CODE = sc.cityOptionSelected.CITY_CODE;
		sc.sisFormFData.CITY_TERM_FLAG = sc.cityOptionSelected.TERM_FLAG;
		sc.sisFormFData.OTHER_CITY = null;
	};

	this.onEducationChg = function(){
		debug("eduQualOptionSelected : " + JSON.stringify(sc.eduQualOptionSelected));
		sc.sisFormFData.EDUCATION_QUALIFICATION = sc.eduQualOptionSelected.LOOKUP_TYPE_DESC;
		sc.sisFormFData.EDUCATION_QUALIFICATION_CODE = sc.eduQualOptionSelected.NBFE_CODE;
	};

	this.onOccClassChg = function(){
		debug("occClassOptionSelected : " + JSON.stringify(sc.occClassOptionSelected));
		sc.sisFormFData.OCCUPATION_CLASS = sc.occClassOptionSelected.OCCU_CLASS;
		sc.sisFormFData.OCCUPATION_CLASS_NBFE_CODE = sc.occClassOptionSelected.NBFE_CODE;
	};

	this.onNext = function(sisFormF){
	CommonService.showLoading("Loading...");
		this.onNextClick = true;
		debug("sisFormFData : " + JSON.stringify(sc.sisFormFData));
		debug("sisFormFData : " + JSON.stringify(SisFormService.sisData.sisFormFData));
		if(sisFormF.$invalid === false){
			if(sc.sisFormFData.RESIDENT_STATUS_CODE=='RI' && sc.sisFormFData.CITY_TERM_FLAG != 'Y'){
				/*if(sc.sisFormFData.GROUP_EMPLOYEE == 'IBL Employee' && !!sc.sisFormFData.IBL_JOINING_DATE){
					var monthCnt = parseInt(CommonService.getNoOfMonths(CommonService.formatDobToDb(sc.sisFormFData.IBL_JOINING_DATE)));
					debug("monthCnt : " + monthCnt);
					if(monthCnt>6)
						SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_CITY = 'Y';
					else
						SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_CITY = 'N';
				}
				else*/
					SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_CITY = 'N';
			}else {
				SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_CITY = 'Y';
			}
			if(sc.sisFormFData.RESIDENT_STATUS_CODE!='RI' && sc.sisFormFData.COUNTRY_TERM_FLAG != 'Y'){
				/*if(sc.sisFormFData.GROUP_EMPLOYEE == 'IBL Employee' && !!sc.sisFormFData.IBL_JOINING_DATE){
					var monthCnt = parseInt(CommonService.getNoOfMonths(CommonService.formatDobToDb(sc.sisFormFData.IBL_JOINING_DATE)));
					debug("monthCnt country : " + monthCnt);
					if(monthCnt>6)
						SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_COUNTRY = 'Y';
					else
						SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_COUNTRY = 'N';
				}
				else*/
					SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_COUNTRY = 'N';
			}else {
				SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_COUNTRY = 'Y';
			}
			if(sc.sisFormFData.OCCUPATION_CLASS_NBFE_CODE=='7' || sc.sisFormFData.OCCUPATION_CLASS_NBFE_CODE=='8'){
				SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_OCCCLASS = 'N';
			}else {
				SisFormService.sisData.sisFormFData.ELIGIBILITY_FLAG_OCCCLASS = 'Y';
			}
			SisFormFService.setDataInSisFormFData(sc.sisFormFData);
			SisFormFService.insertOrUpdateDataInSisScreenFTable(sc.sisFormFData).then(
				function (res) {
					debug("res : " + res);
					if(!!res)
						$state.go("sisForm.sisOutput");
				}
			);
		}else {
			navigator.notification.alert('Please fill all mandatory fields',null,"SIS","OK");
		}
	};

	this.onBack = function(){
		SfePage1Service.goBack();
	};

}]);

sisFormFModule.service('SisFormFService', ['$q', 'CommonService', 'SisFormService', function($q, CommonService, SisFormService){

	var sc = this;
	this.sisFormFData = null;

	this.getResidentCountryList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		//whereClauseObj.TERM_FLAG = 'Y';
		CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
			function(res){
				var ResidentCountryList = [];
				debug("resiCountry :" + res.rows.length);
				if(!!res && res.rows.length>0){
					ResidentCountryList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null});
					for(var i=0;i<res.rows.length;i++){
						var resident = {};
						resident.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						resident.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
						resident.TERM_FLAG = res.rows.item(i).TERM_FLAG;
						ResidentCountryList.push(resident);
					}
					//ResidentCountryList.push({"COUNTRY_NAME": "Others ", "COUNTRY_CODE": "OT", "TERM_FLAG":"N"});
					dfd.resolve(ResidentCountryList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getEduQualList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.LOOKUP_TYPE = 'Education';
		whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var EduQualList = [];
				debug("EduQualiftn :" + res.rows.length);
				if(!!res && res.rows.length>0){
						EduQualList.push({"LOOKUP_TYPE_DESC": "Select ", "LOOKUP_TYPE": null,"NBFE_CODE":null, "ISACTIVE": null});
						for(var i=0;i<res.rows.length;i++){
						var eduQual = {};
						eduQual.LOOKUP_TYPE = res.rows.item(i).LOOKUP_TYPE;
						eduQual.NBFE_CODE = res.rows.item(i).NBFE_CODE;
						eduQual.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
						eduQual.ISACTIVE = res.rows.item(i).ISACTIVE;
						EduQualList.push(eduQual);
					}
					dfd.resolve(EduQualList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getStateList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		CommonService.selectRecords(db,"LP_STATE_MASTER",false,"*",whereClauseObj, "STATE_DESC ASC").then(
			function(res){
				var StateList = [];
				debug("StateList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					StateList.push({"STATE_DESC": "Select ", "STATE_CODE":null, "COUNTRY_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var stateList = {};
						stateList.STATE_DESC = res.rows.item(i).STATE_DESC;
						stateList.STATE_CODE = res.rows.item(i).STATE_CODE;
						stateList.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						StateList.push(stateList);
					}
					dfd.resolve(StateList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getCityList = function(stateCode, termFlag){
		var dfd = $q.defer();
		debug("getCityList : " + stateCode);
		var termCondition = "";
		if(!!termFlag)
			termCondition = " AND TERM_FLAG = 'Y' ";
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"SELECT * FROM LP_CITY_MASTER WHERE STATE_CODE IN (?,?) AND ISACTIVE = ?" + termCondition + "ORDER BY CITY_DESC ASC",[stateCode,'0','Y'],
					function(tx,res){
						var CityList = [];
						//debug("CityList :" + res.rows.item(0).CITY_DESC);
						CityList.push({"CITY_DESC": "Select ", "CITY_CODE":null, "STATE_CODE":null});
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var cityList = {};
								cityList.CITY_DESC = res.rows.item(i).CITY_DESC;
								cityList.CITY_CODE = res.rows.item(i).CITY_CODE;
								cityList.STATE_CODE = res.rows.item(i).STATE_CODE;
								cityList.TERM_FLAG = res.rows.item(i).TERM_FLAG;
								CityList.push(cityList);
							}
							//CityList.push({"CITY_DESC": "Others ", "CITY_CODE":"OT", "STATE_CODE":null, "TERM_FLAG":"N"});
							dfd.resolve(CityList);
						}
						else
							dfd.resolve(CityList);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
			dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getOccClassList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		CommonService.selectRecords(db, "LP_APP_OCCUPATION_CLASS", false, "*", whereClauseObj, "OCCU_CLASS ASC").then(
			function(res){
				var OccClassList = [];
				console.log("OccList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					OccClassList.push({"OCCU_CLASS": "Select ", "NBFE_CODE":null, "MPDOC_CODE":null,"OCCU_CLASS_PRINT":null,"MPPROOF_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var occClassList = {};
						occClassList.OCCU_CLASS = res.rows.item(i).OCCU_CLASS;
						occClassList.OCCU_CLASS_PRINT = res.rows.item(i).OCCU_CLASS_PRINT;
						occClassList.NBFE_CODE = res.rows.item(i).NBFE_CODE;
						occClassList.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						occClassList.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						OccClassList.push(occClassList);
					}
					dfd.resolve(OccClassList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.setDataInSisFormFData = function(sisFormFData){
		if(!SisFormService.sisData.sisFormFData){
			SisFormService.sisData.sisFormFData = {};
		}
		SisFormService.sisData.sisFormFData.FHR_ID = SisFormService.sisData.sisMainData.FHR_ID;
		SisFormService.sisData.sisFormFData.LEAD_ID = SisFormService.sisData.sisMainData.LEAD_ID;
		SisFormService.sisData.sisFormFData.GROUP_EMPLOYEE = sisFormFData.GROUP_EMPLOYEE || null;
		SisFormService.sisData.sisFormFData.RESIDENT_STATUS = sisFormFData.RESIDENT_STATUS != "Select " ? sisFormFData.RESIDENT_STATUS : null;
		SisFormService.sisData.sisFormFData.NATIONALITY = sisFormFData.NATIONALITY != "Select " ? sisFormFData.NATIONALITY : null;
		SisFormService.sisData.sisFormFData.CURR_RESIDENT_COUNTRY = sisFormFData.CURR_RESIDENT_COUNTRY != "Select " ? sisFormFData.CURR_RESIDENT_COUNTRY : null;
		SisFormService.sisData.sisFormFData.CURR_RESIDENT_COUNTRY_CODE = sisFormFData.CURR_RESIDENT_COUNTRY_CODE || null;
		SisFormService.sisData.sisFormFData.CURR_STATE = sisFormFData.CURR_STATE != "Select " ? sisFormFData.CURR_STATE : null;
		SisFormService.sisData.sisFormFData.CURR_STATE_CODE = sisFormFData.CURR_STATE_CODE || null;
		SisFormService.sisData.sisFormFData.CURR_CITY = sisFormFData.CURR_CITY != "Select " ? sisFormFData.CURR_CITY : null;
		SisFormService.sisData.sisFormFData.CURR_CITY_CODE = sisFormFData.CURR_CITY_CODE || null;
		SisFormService.sisData.sisFormFData.OCCUPATION_CLASS = sisFormFData.OCCUPATION_CLASS != "Select " ? sisFormFData.OCCUPATION_CLASS : null;
		SisFormService.sisData.sisFormFData.OCCUPATION_CLASS_NBFE_CODE = sisFormFData.OCCUPATION_CLASS_NBFE_CODE || null;
		SisFormService.sisData.sisFormFData.OTHER_CITY = sisFormFData.OTHER_CITY || null;
		debug("before" + sisFormFData.IBL_JOINING_DATE);
		var iblDate = sisFormFData.IBL_JOINING_DATE;
		SisFormService.sisData.sisFormFData.IBL_JOINING_DATE = !!iblDate ? CommonService.formatDobToDb(iblDate) : null;
		debug(SisFormService.sisData.sisFormFData.IBL_JOINING_DATE + " after " + sisFormFData.IBL_JOINING_DATE);
	};

	this.insertOrUpdateDataInSisScreenFTable = function(){
		var dfd = $q.defer();
		CommonService.insertOrReplaceRecord(db, "LP_TERM_VALIDATION", SisFormService.sisData.sisFormFData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_TERM_VALIDATION successfully");
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_TERM_VALIDATION");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

}]);
