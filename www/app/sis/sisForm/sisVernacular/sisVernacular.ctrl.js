sisVernacularModule.controller('SisVernacularCtrl',['IsTermAttached', 'CommonService', 'SisFormService', 'SisVernacularService','ApplicationFormDataService','LoadAppProofList','IDProofList','$filter', function(IsTermAttached, CommonService, SisFormService, SisVernacularService,ApplicationFormDataService,LoadAppProofList,IDProofList, $filter){
	"use strict";
	var sc = this;
	CommonService.hideLoading();
    console.log("data of list:" + JSON.stringify(IDProofList));
    console.log("Data of  "+JSON.stringify(IDProofList[0].CUSTOMER_CATEGORY));
                                                    
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/

     console.log("sisVernaculardata :" + JSON.stringify(SisFormService));
     console.log("sisVernaculardata for custType :" + JSON.stringify(SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR));
     if(SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR =='Self'){
            console.log("In Insured");
           sc.customertype='IN';
     }else{
          console.log("In Proposer");
          sc.customertype='PR';
     }
	this.vernacDetails = {};
	this.nameRegex = VERNAC_WITNESS_NAME_REGEX;
	this.insIDProofList = IDProofList;
	this.vernacDetails.PROOF_TYPE=this.insIDProofList[0];
	console.log("setting data"+ this.vernacDetails.PROOF_TYPE);
	this.idProofNameRegex = FULLNAME_REGEX;
	this.idNumRegex = USERNAME_REGEX;
	this.sisOutput = SisFormService.sisData.sisOutput.SIS;
	//console.log("data of loadidentityproof:" + this.insIDProofList );
	this.saveDetailsAndSign = function(){
		this.vernacDetails.isTermAttached = IsTermAttached || null;
		debug("this.vernacDetails: " + JSON.stringify(this.vernacDetails));
		SisVernacularService.vernacDetails = this.vernacDetails;
		CommonService.doSign("sisDeclarantSign");
	};

	this.onIDNumChange = function(){
		this.vernacDetails.PROOF_NO = $filter('uppercase')(this.vernacDetails.PROOF_NO);
	};

this.ExpLanguageList = [
		{"EXP_LANGUAGE": "Select ", "LANGUAGE": null},
		{"EXP_LANGUAGE": "Adi","LANGUAGE": "Adi"},
		{"EXP_LANGUAGE": "Angami","LANGUAGE": "Angami"},
		{"EXP_LANGUAGE": "Ao","LANGUAGE": "Ao"},
		{"EXP_LANGUAGE": "Assamese","LANGUAGE": "Assamese"},
		{"EXP_LANGUAGE": "Bengali","LANGUAGE": "Bengali"},
		{"EXP_LANGUAGE": "Bhili/Bhilodi","LANGUAGE": "Bhili/Bhilodi"},
		{"EXP_LANGUAGE": "Bodo","LANGUAGE": "Bodo"},
		{"EXP_LANGUAGE": "Coorgi/Kodagu","LANGUAGE": "Coorgi/Kodagu"},
		{"EXP_LANGUAGE": "Dimasa","LANGUAGE": "Dimasa"},
		{"EXP_LANGUAGE": "Dogri","LANGUAGE": "Dogri"},
		{"EXP_LANGUAGE": "Garo","LANGUAGE": "Garo"},
		{"EXP_LANGUAGE": "Gondi","LANGUAGE": "Gondi"},
		{"EXP_LANGUAGE": "Gujarati","LANGUAGE": "Gujarati"},
		{"EXP_LANGUAGE": "Halabi","LANGUAGE": "Halabi"},
		{"EXP_LANGUAGE": "Hindi","LANGUAGE": "Hindi"},
		{"EXP_LANGUAGE": "Ho","LANGUAGE": "Ho"},
		{"EXP_LANGUAGE": "Kannada","LANGUAGE": "Kannada"},
		{"EXP_LANGUAGE": "Karbi/Mikir","LANGUAGE": "Karbi/Mikir"},
		{"EXP_LANGUAGE": "Kashmiri","LANGUAGE": "Kashmiri"},
		{"EXP_LANGUAGE": "Khandeshi","LANGUAGE": "Khandeshi"},
		{"EXP_LANGUAGE": "Kharia","LANGUAGE": "Kharia"},
		{"EXP_LANGUAGE": "Khasi","LANGUAGE": "Khasi"},
		{"EXP_LANGUAGE": "Khond/Kondh","LANGUAGE": "Khond/Kondh"},
		{"EXP_LANGUAGE": "Kisan","LANGUAGE": "Kisan"},
		{"EXP_LANGUAGE": "Kolami","LANGUAGE": "Kolami"},
		{"EXP_LANGUAGE": "Konkani","LANGUAGE": "Konkani"},
		{"EXP_LANGUAGE": "Konyak","LANGUAGE": "Konyak"},
		{"EXP_LANGUAGE": "Korku","LANGUAGE": "Korku"},
		{"EXP_LANGUAGE": "Koya","LANGUAGE": "Koya"},
		{"EXP_LANGUAGE": "Kui","LANGUAGE": "Kui"},
		{"EXP_LANGUAGE": "Kurukh","LANGUAGE": "Kurukh"},
		{"EXP_LANGUAGE": "Ladakhi","LANGUAGE": "Ladakhi"},
		{"EXP_LANGUAGE": "Lotha","LANGUAGE": "Lotha"},
		{"EXP_LANGUAGE": "Lushai/Mizo","LANGUAGE": "Lushai/Mizo"},
		{"EXP_LANGUAGE": "Maithili","LANGUAGE": "Maithili"},
		{"EXP_LANGUAGE": "Malayalam","LANGUAGE": "Malayalam"},
		{"EXP_LANGUAGE": "Malto","LANGUAGE": "Malto"},
		{"EXP_LANGUAGE": "Marathi","LANGUAGE": "Marathi"},
		{"EXP_LANGUAGE": "Meitei/Manipuri","LANGUAGE": "Meitei/Manipuri"},
		{"EXP_LANGUAGE": "Miri/Mishing","LANGUAGE": "Miri/Mishing"},
		{"EXP_LANGUAGE": "Munda","LANGUAGE": "Munda"},
		{"EXP_LANGUAGE": "Mundari","LANGUAGE": "Mundari"},
		{"EXP_LANGUAGE": "Nepali","LANGUAGE": "Nepali"},
		{"EXP_LANGUAGE": "Nissi/Dafla","LANGUAGE": "Nissi/Dafla"},
		{"EXP_LANGUAGE": "Odia","LANGUAGE": "Odia"},
		{"EXP_LANGUAGE": "Phom","LANGUAGE": "Phom"},
		{"EXP_LANGUAGE": "Punjabi","LANGUAGE": "Punjabi"},
		{"EXP_LANGUAGE": "Rabha","LANGUAGE": "Rabha"},
		{"EXP_LANGUAGE": "Santali","LANGUAGE": "Santali"},
		{"EXP_LANGUAGE": "Savara","LANGUAGE": "Savara"},
		{"EXP_LANGUAGE": "Sema","LANGUAGE": "Sema"},
		{"EXP_LANGUAGE": "Sindhi","LANGUAGE": "Sindhi"},
		{"EXP_LANGUAGE": "Tamil","LANGUAGE": "Tamil"},
		{"EXP_LANGUAGE": "Tangkhul","LANGUAGE": "Tangkhul"},
		{"EXP_LANGUAGE": "Telugu","LANGUAGE": "Telugu"},
		{"EXP_LANGUAGE": "Thado","LANGUAGE": "Thado"},
		{"EXP_LANGUAGE": "Tripuri","LANGUAGE": "Tripuri"},
		{"EXP_LANGUAGE": "Tulu","LANGUAGE": "Tulu"},
		{"EXP_LANGUAGE": "Urdu","LANGUAGE": "Urdu"}
	];
	    console.log("Value : " + JSON.stringify(this.ExpLanguageList[0]));
        this.vernacDetails.EXP_LANG = this.ExpLanguageList[0];

}]);
sisVernacularModule.service('SisVernacularService',['$state', 'CommonService', 'SisFormService', 'SisOutputService', 'SisTimelineService', function($state, CommonService, SisFormService, SisOutputService, SisTimelineService){
	"use strict";
	var sc = this;
	this.vernacDetails = {};
	this.saveDeclarantSignature = function(sign){
		if(!!this.vernacDetails.isTermAttached){
			SisFormService.sisData.sisTermOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisTermOutput.SIS,"@@DEC_NAME@@",sc.vernacDetails.DEC_NAME||"");
			if(sc.vernacDetails.PROOF_TYPE.MPPROOF_DESC== 'Others'){
			console.log("in sis other");
            SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@PROOF_TYPE@@",sc.vernacDetails.Other_PROOF_TYPE||"");
            }else{
            console.log("in sis non other");
			SisFormService.sisData.sisTermOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisTermOutput.SIS,"@@PROOF_TYPE@@",sc.vernacDetails.PROOF_TYPE.MPPROOF_DESC||"");
			}
			SisFormService.sisData.sisTermOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisTermOutput.SIS,"@@PROOF_NO@@",sc.vernacDetails.PROOF_NO||"");
			SisFormService.sisData.sisTermOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisTermOutput.SIS,"@@EXP_LANG@@",sc.vernacDetails.EXP_LANG.LANGUAGE||"");
			SisFormService.sisData.sisTermOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisTermOutput.SIS,"@@DEC_SIGN@@",sign);

			SisFormService.sisData.sisTermOutput.SIS = SisFormService.sisData.sisTermOutput.SIS.replace(/"hideDeclarantSign"/g,"\"showDeclarantSign\"");
		}
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@DEC_NAME@@",sc.vernacDetails.DEC_NAME||"");
		if(sc.vernacDetails.PROOF_TYPE.MPPROOF_DESC== 'Others'){
		console.log("in other");
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@PROOF_TYPE@@",sc.vernacDetails.Other_PROOF_TYPE||"");
		}else{
		console.log("in non other");
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@PROOF_TYPE@@",sc.vernacDetails.PROOF_TYPE.MPPROOF_DESC||"");
		}
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@PROOF_NO@@",sc.vernacDetails.PROOF_NO||"");
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@EXP_LANG@@",sc.vernacDetails.EXP_LANG.LANGUAGE||"");
		SisFormService.sisData.sisOutput.SIS = CommonService.replaceAll(SisFormService.sisData.sisOutput.SIS,"@@DEC_SIGN@@",sign);

		SisFormService.sisData.sisOutput.SIS = SisFormService.sisData.sisOutput.SIS.replace(/"hideDeclarantSign"/g,"\"showDeclarantSign\"");

		SisOutputService.saveSisReportFile().then(
			function(res){
				if(!!res && res=="S"){
					if(!!sc.vernacDetails.isTermAttached){
						SisOutputService.saveSisReportFile(true).then(
							function(termRes){
								if(!!termRes && termRes=="S"){
									SisTimelineService.isOutputTabComplete = true;
									$state.go("sisForm.sisOutput", {"BASE_OR_TERM": "TERM", "DONT_REGEN": true});
								}
							}
						);
					}
					else{
						SisTimelineService.isOutputTabComplete = true;
						$state.go("sisForm.sisOutput", {"DONT_REGEN": true});
					}
				}
			}
		);
	};

}]);
