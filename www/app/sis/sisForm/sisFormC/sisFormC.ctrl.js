sisFormCModule.controller('SisFormCCtrl',['$state', '$filter', 'CommonService', 'SisFormCService', 'SisFormService', 'FundsList', 'DebtFundList', 'EquityFundList', 'SisFormDService', 'SisTimelineService', 'RiderAllowed', function($state, $filter, CommonService, SisFormCService, SisFormService, FundsList, DebtFundList, EquityFundList, SisFormDService, SisTimelineService, RiderAllowed){
	debug("in SisFormCCtrl");
	CommonService.hideLoading();
                                          
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/
                                          
                                          
	SisTimelineService.setActiveTab("fund");
	var sc = this;

	this.fundsList = FundsList;
	this.debtFundList = DebtFundList;
	this.equityFundList = EquityFundList;

	debug("FundsList: " + JSON.stringify(this.fundsList));

	this.INVEST_OPT = SisFormService.sisData.sisFormCData.INVEST_OPT;

	if(!this.INVEST_OPT)
		this.INVEST_OPT = "Manual";

	if(!!this.INVEST_OPT && this.INVEST_OPT=="Manual")
		this.minTotal = 100;
	else
		this.minTotal = 0;

	this.debtOptionSelected = this.debtFundList[0];
	this.equityOptionSelected = this.equityFundList[0];

	SisFormCService.riderAllowed = RiderAllowed;

	if(!!SisFormService.sisData.sisFormCData.FUND_CODE_FROM)
		this.debtOptionSelected = ($filter('filter')(this.debtFundList, {"FUND_CODE": SisFormService.sisData.sisFormCData.FUND_CODE_FROM})[0]);

	if(!!SisFormService.sisData.sisFormCData.FUND_CODE_TO)
		this.equityOptionSelected = ($filter('filter')(this.equityFundList, {"FUND_CODE": SisFormService.sisData.sisFormCData.FUND_CODE_TO})[0]);

	this.onAllocChg = function(){
		var total = 0;
		angular.forEach(this.fundsList,function(fund){
			if(!!fund.FUND_ALLOCATION && fund.FUND_ALLOCATION>0)
				total += fund.FUND_ALLOCATION;
		});
		this.totalFundPercentage = total;
		debug("total: " + this.totalFundPercentage);
	};

	this.onAllocChg();

	this.onFundTypeChange = function(){
		debug("onFundTypeChange");
		if(!!this.INVEST_OPT && this.INVEST_OPT=="Manual")
			this.minTotal = 100;
		else
			this.minTotal = 0;
	};

	this.selectingEnhancedSmart = function(event){
		if(SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE!="A" && SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE!="O"){
			navigator.notification.alert("Enhanced Smart cannot be opted only if the premium payment mode is Annual or Single-Pay.",function(){
				this.minTotal = 100;
				sc.INVEST_OPT = "Manual";
			},"SIS","OK");
		}
		else {
			sc.INVEST_OPT = "Smart";
			this.minTotal = 0;
		}
	};

	this.onNext = function(isPristine){
		debug("on next : " + JSON.stringify(this.debtOptionSelected) + "\t " + JSON.stringify(this.equityOptionSelected));
		CommonService.showLoading("Loading...");
		SisFormCService.setDataInSisFormCData(this.INVEST_OPT,this.fundsList, this.debtOptionSelected, this.equityOptionSelected);
		SisFormCService.insertDataInSisScreenCTable().then(
			function(res){
				if(!!res){
					SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED = 'Y';
					if(!isPristine || !SisTimelineService.isFundTabComplete){
						if((!!SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED) && (SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED=="Y")){
							SisFormService.sisData.sisMainData.IS_SCREEN4_COMPLETED = 'N';
							SisTimelineService.isRiderTabComplete = false;
						}
						if(!!SisTimelineService.isOtherDetailsTabComplete && SisTimelineService.isOtherDetailsTabComplete==true){
							SisTimelineService.isOtherDetailsTabComplete = false;
						}
						if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
							SisTimelineService.isOutputTabComplete = false;
					}
					SisFormService.insertDataInSisMainTable().then(
						function(sisMainRes){
							if(!!sisMainRes){
								SisTimelineService.isFundTabComplete = true;
								SisFormCService.gotoNextState();
							}
							else{
								CommonService.hideLoading();
							}
						}
					);
				}
				else{
					CommonService.hideLoading();
				}
			}
		);
	};

	this.onBack = function(){
		CommonService.showLoading("Loading...");
		$state.go("sisForm.sisFormB");
	};

}]);

sisFormCModule.service('SisFormCService',['$state', '$filter', '$q', 'CommonService', 'SisFormService', 'SisFormDService', 'SisTimelineService', function($state, $filter, $q, CommonService, SisFormService, SisFormDService, SisTimelineService){
	var sc = this;

	this.sisFormCData = this.sisFormCData || {};

	this.getFundsList = function(planCode){
		debug("planCode: " + planCode);
		var dfd = $q.defer();
		try{
			CommonService.transaction(sisDB,
				function(txSIS){
					CommonService.executeSql(txSIS,"SELECT PFX_FDL_ID FROM LP_PFX_PLAN_FUND_XREF WHERE PFX_PNL_ID=(SELECT PNL_ID FROM LP_PNL_PLAN_LK WHERE PNL_CODE=?)",[planCode],
						function(txSIS, pnlRes){
							if(!!pnlRes && pnlRes.rows.length>0){
								debug("LP_PFX_PLAN_FUND_XREF length: " + pnlRes.rows.length);
								var quesString = "?";
								var parameterList = [pnlRes.rows.item(0).PFX_FDL_ID];
								for(var i=1; i<pnlRes.rows.length; i++){
									quesString += ", ?";
									parameterList.push(pnlRes.rows.item(i).PFX_FDL_ID);
								}
								parameterList.push("Y");
								debug("parameterList: " + JSON.stringify(parameterList));
								CommonService.transaction(db,
									function(tx){
										CommonService.executeSql(tx,"SELECT FUND_DESCRIPTION, FUND_SFIN_NUMBER, FUND_CODE, FUND_FROM, FUND_TO, FUND_TYPE FROM LP_FUND_MASTER WHERE FUND_FDL_ID IN (" + quesString + ") AND ISACTIVE=? ORDER BY FUND_ORDER ASC",parameterList,
											function(tx, res){
												debug("LP_FUND_MASTER length: " + res.rows.length);
												sc.fundsList = [];
												if(!!res && res.rows.length>0){
													for(i=0;i<res.rows.length;i++){
														var sisFund = {};
														sisFund.FUND_DESCRIPTION = res.rows.item(i).FUND_DESCRIPTION;
														sisFund.FUND_SFIN_NUMBER = res.rows.item(i).FUND_SFIN_NUMBER;
														sisFund.FUND_CODE = res.rows.item(i).FUND_CODE;
														sisFund.FUND_FROM = res.rows.item(i).FUND_FROM;
														sisFund.FUND_TO = res.rows.item(i).FUND_TO;
														sisFund.FUND_TYPE = res.rows.item(i).FUND_TYPE;
														var fundRec = SisFormService.sisData.sisFormCData;
														for(var j=1;j<11;j++){
															if((!!fundRec['FUND'+j+'_CODE']) && (fundRec['FUND'+j+'_CODE'] == sisFund.FUND_CODE)){
																sisFund.FUND_ALLOCATION = parseInt(fundRec['FUND'+j+'_PERCENT']);
																break;
															}
														}
														sc.fundsList.push(sisFund);
													}
												}
												if(!!sc.fundsList && sc.fundsList.length>0)
													dfd.resolve(sc.fundsList);
												else
													dfd.resolve(null);
											},
											function(tx, err){
												dfd.resolve(null);
											}
										);
									},
									function(err){
										dfd.resolve(null);
									}
								);
							}
							else{
								dfd.resolve(null);
							}
						},
						function(txSIS,err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}catch(ex){
			debug("Exception : " + ex.message);
		}
		return dfd.promise;
	};

	this.getDebtFundList = function(fundsList){
		if(!!fundsList){
			var debtList = [];
			debtList.push({"FUND_DESCRIPTION": "Select Debt Oriented Fund", "FUND_CODE": null});
			debtList = debtList.concat($filter('filter')(fundsList, {"FUND_FROM": 'Y', 'FUND_TYPE': 'Dept'}));
			return debtList;
		}
		else
			return null;
	};

	this.getEquityFundList = function(fundsList){
		if(!!fundsList){
			var equityList = [];
			equityList.push({"FUND_DESCRIPTION": "Select Equity Oriented Fund", "FUND_CODE": null});
			equityList = equityList.concat($filter('filter')(fundsList, {"FUND_TO": 'Y', 'FUND_TYPE': 'Equity'}));
			return equityList;
		}
		else
			return null;
	};

	this.setDataInSisFormCData = function(investOpt, fundsList, debtOptionSelected, equityOptionSelected){
		var sisFormCData = {"SIS_ID": SisFormService.sisData.sisFormCData.SIS_ID, "AGENT_CD": SisFormService.sisData.sisFormCData.AGENT_CD};
		var i=1;
		sisFormCData.INVEST_OPT = investOpt;
		if(!!investOpt && investOpt=="Manual"){
			angular.forEach(fundsList,function(fund){
				if(!!fund.FUND_ALLOCATION && fund.FUND_ALLOCATION>0){
					sisFormCData['FUND'+i+'_CODE'] = fund.FUND_CODE;
					sisFormCData['FUND'+i+'_PERCENT'] = fund.FUND_ALLOCATION;
					i++;
				}
			});
			sisFormCData.FUND_CODE_FROM = null;
			sisFormCData.FUND_CODE_TO = null;
		}
		else{
			sisFormCData.FUND_CODE_FROM = debtOptionSelected.FUND_CODE || null;
			sisFormCData.FUND_CODE_TO = equityOptionSelected.FUND_CODE || null;
			angular.forEach(fundsList,function(fund){
				if(!!fund.FUND_ALLOCATION && fund.FUND_ALLOCATION>0){
					sisFormCData['FUND'+i+'_CODE'] = null;
					sisFormCData['FUND'+i+'_PERCENT'] = null;
					i++;
				}
			});
		}
		SisFormService.sisData.sisFormCData = sisFormCData;
		SisFormService.sisData.sisMainData.IS_SCREEN3_COMPLETED = 'Y';
	};

	this.insertDataInSisScreenCTable = function(){
		var dfd = $q.defer();
		SisFormService.sisData.sisFormCData.MODIFIED_DATE = CommonService.getCurrDate();
		CommonService.insertOrReplaceRecord(db, "lp_sis_screen_c", SisFormService.sisData.sisFormCData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_SIS_SCREEN_C successfully: SIS_ID: " + SisFormService.sisData.sisMainData.SIS_ID);
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_SIS_SCREEN_C");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.gotoNextState = function(){
		var pnlId = ($filter('filter')(SisFormService.getProductDetails().LP_PNL_PLAN_LK, {"PNL_CODE": SisFormService.sisData.sisFormBData.PLAN_CODE})[0]).PNL_ID;
		debug("pnlId: " + pnlId);
		CommonService.selectRecords(sisDB, 'lp_rpx_rider_plan_xref', true, null, {"rpx_pnl_id": pnlId},null).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug("going to rider page");
					SisFormDService.getRidersList().then(
						function(ridersList){
							if(!!ridersList && ridersList.length>0){
								if(!!sc.riderAllowed || !!SisTimelineService.isRiderTabComplete) // only if minimum premium multiple is selected
									$state.go("sisForm.sisFormD",{"RIDER_LIST": ridersList});
								else {
									SisFormService.gotoTAorOutput();
									//$state.go("sisForm.sisOutput");
								}
							}
							else{
								SisFormService.gotoTAorOutput();
								//$state.go("sisForm.sisOutput");
							}
						}
					);
				}
				else{
					debug("generating sis output");
					SisFormService.gotoTAorOutput();
					//$state.go("sisForm.sisOutput");
				}
			}
		);
	};

}]);
