sfePage1Module.controller('SfePage1Ctrl',['$state', '$filter', 'CommonService', 'SfePage1Service', 'ResidentCountryList', 'NationalityList', 'EduQualList', 'StateList', 'CityList','OccClassList','SisFormService', 'SisFormEService', 'SisTimelineService', function($state, $filter, CommonService, SfePage1Service, ResidentCountryList, NationalityList, EduQualList, StateList, CityList, OccClassList, SisFormService, SisFormEService, SisTimelineService){
	"use strict";
	debug("SfePage1Ctrl : " + ResidentCountryList.length);
	CommonService.hideLoading();
                                          
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/
                                          
                                          
	SisTimelineService.setActiveTab("otherDetails");
	var sc = this;
	this.onNextClick = false;
	this.residentStatusList = [
		{"RESIDENT_STATUS": "Select ", "RESIDENT_STATUS_CODE":null},
		{"RESIDENT_STATUS": "Resident Indian", "RESIDENT_STATUS_CODE":"RI"},
		{"RESIDENT_STATUS": "Non Resident Indian", "RESIDENT_STATUS_CODE":"NRI"},
		{"RESIDENT_STATUS": "Foreign National", "RESIDENT_STATUS_CODE":"FN"},
		{"RESIDENT_STATUS": "Overseas Citizen of India", "RESIDENT_STATUS_CODE":"OCI"}
	];
	this.nationalityList = NationalityList;
	this.residentCountryList = ResidentCountryList;
	this.eduQualList = EduQualList;
	this.stateList = StateList;
	this.cityList = CityList;
	this.occClassList = OccClassList;
	this.ekycFlag = false;
	this.onCountryChg = function(){
		debug("residentCountryOptionSelected : " + JSON.stringify(sc.residentCountryOptionSelected));
		sc.sfePage1Data.CURR_RESIDENT_COUNTRY = sc.residentCountryOptionSelected.COUNTRY_NAME;
		sc.sfePage1Data.CURR_RESIDENT_COUNTRY_CODE = sc.residentCountryOptionSelected.COUNTRY_CODE;
		sc.sfePage1Data.COUNTRY_TERM_FLAG = sc.residentCountryOptionSelected.TERM_FLAG;
		sc.sfePage1Data.COUNTRY_TERM_NML_A = sc.residentCountryOptionSelected.TERM_NML_A;
		sc.sfePage1Data.COUNTRY_TERM_NML_B = sc.residentCountryOptionSelected.TERM_NML_B;
	};

	this.onNationalityChg = function(){
		debug("nationalityOptionSelected : " + JSON.stringify(sc.nationalityOptionSelected));
		sc.sfePage1Data.NATIONALITY = sc.nationalityOptionSelected.COUNTRY_NAME;
		sc.sfePage1Data.NATIONALITY_CODE = sc.nationalityOptionSelected.COUNTRY_CODE;
	};
	this.residentStatusOptionSelected = this.residentStatusList[0];
	this.nationalityOptionSelected = this.nationalityList[0];
	this.residentCountryOptionSelected = this.residentCountryList[0];
	this.eduQualOptionSelected = this.eduQualList[0];
	this.occClassOptionSelected = this.occClassList[0];
	this.sfePage1Data = SfePage1Service.sfePage1Data;
	debug("this.sfePage1Data : " + JSON.stringify(this.sfePage1Data));
	debug("this.stateList : " + JSON.stringify(this.stateList));
	if(SisFormService.sisData.sisEKycData.insured.EKYC_FLAG=='Y'){
		this.ekycFlag = true;
		if(!this.sfePage1Data.RESIDENT_STATUS_CODE){
			this.residentStatusOptionSelected = $filter('filter')(this.residentStatusList, {"RESIDENT_STATUS_CODE": "RI"})[0];
			sc.sfePage1Data.RESIDENT_STATUS = sc.residentStatusOptionSelected.RESIDENT_STATUS;
			sc.sfePage1Data.RESIDENT_STATUS_CODE = sc.residentStatusOptionSelected.RESIDENT_STATUS_CODE;
			this.residentCountryOptionSelected = this.residentCountryList[0];
			sc.nationalityOptionSelected = this.nationalityList[0];
			sc.onCountryChg();
			sc.onNationalityChg();
		}
		if(!this.sfePage1Data.CURR_STATE_CODE){
			this.stateOptionSelected = $filter('filter')(this.stateList, {"STATE_DESC": SisFormService.sisData.sisEKycData.insured.STATE.toUpperCase()}, true)[0];
			sc.sfePage1Data.CURR_STATE = sc.stateOptionSelected.STATE_DESC;
			sc.sfePage1Data.CURR_STATE_CODE = sc.stateOptionSelected.STATE_CODE;
		}
		if(!this.sfePage1Data.CURR_CITY_CODE){
			var cityList = $filter('filter')(this.cityList, {"CITY_DESC": SisFormService.sisData.sisEKycData.insured.CITY.toUpperCase()}, true);
			if(!!cityList && cityList.length>0){
				debug("cityList: " + JSON.stringify(cityList));
				sc.cityOptionSelected = cityList[0];
			}
			else {
				sc.cityOptionSelected = $filter('filter')(this.cityList, {"CITY_CODE":"OT"}, true)[0];
			}
			sc.sfePage1Data.CURR_CITY = sc.cityOptionSelected.CITY_DESC;
			sc.sfePage1Data.CURR_CITY_CODE = sc.cityOptionSelected.CITY_CODE;
			sc.sfePage1Data.CITY_TERM_FLAG = sc.cityOptionSelected.TERM_FLAG;
			sc.sfePage1Data.CITY_TERM_NML_A = sc.cityOptionSelected.TERM_NML_A;
			sc.sfePage1Data.CITY_TERM_NML_B = sc.cityOptionSelected.TERM_NML_B;
			debug("this.cityOptionSelected: " + JSON.stringify(this.cityOptionSelected));
		}
	}
	else{
		this.stateOptionSelected = this.stateList[0];
		this.cityOptionSelected = this.cityList[0];
	}

	if(!!this.sfePage1Data.RESIDENT_STATUS_CODE && ((!!this.residentStatusList && this.residentStatusList.length>0))){
		this.residentStatusOptionSelected = $filter('filter')(this.residentStatusList, {"RESIDENT_STATUS_CODE": this.sfePage1Data.RESIDENT_STATUS_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.CURR_RESIDENT_COUNTRY_CODE && ((!!this.residentCountryList && this.residentCountryList.length>0))){
		this.residentCountryOptionSelected = $filter('filter')(this.residentCountryList, {"COUNTRY_CODE": this.sfePage1Data.CURR_RESIDENT_COUNTRY_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.NATIONALITY_CODE && ((!!this.nationalityList && this.nationalityList.length>0))){
		this.nationalityOptionSelected = $filter('filter')(this.nationalityList, {"COUNTRY_CODE": this.sfePage1Data.NATIONALITY_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.CURR_STATE_CODE && ((!!this.stateList && this.stateList.length>0))){
		this.stateOptionSelected = $filter('filter')(this.stateList, {"STATE_CODE": this.sfePage1Data.CURR_STATE_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.CURR_CITY_CODE && ((!!this.cityList && this.cityList.length>0))){
		this.cityOptionSelected = $filter('filter')(this.cityList, {"CITY_CODE": this.sfePage1Data.CURR_CITY_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.EDUCATION_QUALIFICATION_CODE && ((!!this.eduQualList && this.eduQualList.length>0))){
		this.eduQualOptionSelected = $filter('filter')(this.eduQualList, {"NBFE_CODE": this.sfePage1Data.EDUCATION_QUALIFICATION_CODE}, true)[0];
	}
	if(!!this.sfePage1Data.OCCUPATION_CLASS_NBFE_CODE && ((!!this.occClassList && this.occClassList.length>0))){
		this.occClassOptionSelected = $filter('filter')(this.occClassList, {"NBFE_CODE": this.sfePage1Data.OCCUPATION_CLASS_NBFE_CODE}, true)[0];
	}


	this.onResidenceChg = function(sfePage1Form){
		debug("residentStatusOptionSelected : " + JSON.stringify(sc.residentStatusOptionSelected));
		sc.sfePage1Data.RESIDENT_STATUS = sc.residentStatusOptionSelected.RESIDENT_STATUS;
		sc.sfePage1Data.RESIDENT_STATUS_CODE = sc.residentStatusOptionSelected.RESIDENT_STATUS_CODE;

		if(sc.sfePage1Data.RESIDENT_STATUS_CODE == "RI"){
			sfePage1Form.state.$setValidity('selectRequired', false);
			sfePage1Form.city.$setValidity('selectRequired', false);
			sfePage1Form.residentCountry.$setValidity('selectRequired', true);
			sfePage1Form.nationality.$setValidity('selectRequired', true);
			this.residentCountryOptionSelected = this.residentCountryList[0];
			this.nationalityOptionSelected = this.nationalityList[0];
			this.onCountryChg();
			this.onNationalityChg();
		}
		else if(sc.sfePage1Data.RESIDENT_STATUS_CODE == "NRI"){
			sfePage1Form.residentCountry.$setValidity('selectRequired', false);
			sfePage1Form.state.$setValidity('selectRequired', true);
			sfePage1Form.city.$setValidity('selectRequired', true);
			sfePage1Form.nationality.$setValidity('selectRequired', true);
			this.stateOptionSelected = this.stateList[0];
			this.cityOptionSelected = this.cityList[0];
			this.nationalityOptionSelected = this.nationalityList[0];
			this.onStateChange();
			this.onCityChg();
			this.onNationalityChg();
		}
		else if(sc.sfePage1Data.RESIDENT_STATUS_CODE == "OCI" || sc.sfePage1Data.RESIDENT_STATUS_CODE == "FN"){
			sfePage1Form.residentCountry.$setValidity('selectRequired', false);
			sfePage1Form.nationality.$setValidity('selectRequired', false);
			sfePage1Form.state.$setValidity('selectRequired', true);
			sfePage1Form.city.$setValidity('selectRequired', true);
			this.stateOptionSelected = this.stateList[0];
			this.cityOptionSelected = this.cityList[0];
			this.residentCountryOptionSelected = this.residentCountryList[0];
			this.nationalityOptionSelected = this.nationalityList[0];
			this.onStateChange();
			this.onCityChg();
			this.onCountryChg();
			this.onNationalityChg();
		}
	};



	this.onEducationChg = function(){
		debug("eduQualOptionSelected : " + JSON.stringify(sc.eduQualOptionSelected));
		sc.sfePage1Data.EDUCATION_QUALIFICATION = sc.eduQualOptionSelected.LOOKUP_TYPE_DESC;
		sc.sfePage1Data.EDUCATION_QUALIFICATION_CODE = sc.eduQualOptionSelected.NBFE_CODE;
	};


	this.onStateChange = function(){
		debug("stateOptionSelected : " + JSON.stringify(sc.stateOptionSelected));
		sc.sfePage1Data.CURR_STATE = sc.stateOptionSelected.STATE_DESC;
		sc.sfePage1Data.CURR_STATE_CODE = sc.stateOptionSelected.STATE_CODE;
		SfePage1Service.getCityList(sc.stateOptionSelected.STATE_CODE).then(
			function(CityList){
				sc.cityList = CityList;
				debug("CityList refresh::"+CityList.length);
				sc.cityOptionSelected = CityList[0];
			}
		);
	};

	this.onCityChg = function(){
		debug("cityOptionSelected : " + JSON.stringify(sc.cityOptionSelected));
		sc.sfePage1Data.CURR_CITY = sc.cityOptionSelected.CITY_DESC;
		sc.sfePage1Data.CURR_CITY_CODE = sc.cityOptionSelected.CITY_CODE;
		sc.sfePage1Data.CITY_TERM_FLAG = sc.cityOptionSelected.TERM_FLAG;
		sc.sfePage1Data.CITY_TERM_NML_A = sc.cityOptionSelected.TERM_NML_A;
		sc.sfePage1Data.CITY_TERM_NML_B = sc.cityOptionSelected.TERM_NML_B;
	};

	this.onOccClassChg = function(){
		debug("occClassOptionSelected : " + JSON.stringify(sc.occClassOptionSelected));
		sc.sfePage1Data.OCCUPATION_CLASS = sc.occClassOptionSelected.OCCU_CLASS;
		sc.sfePage1Data.OCCUPATION_CLASS_NBFE_CODE = sc.occClassOptionSelected.NBFE_CODE;
	};

	this.onNext = function(sfePage1Form){
		this.onNextClick = true;
		if(sfePage1Form.$invalid == false){
			debug("sfePage1Data : " + JSON.stringify(sc.sfePage1Data));
			SfePage1Service.setDataInSisFormEData(this.sfePage1Data);
			SisFormService.sisData.oppData.REF_FHRID = SisFormService.sisData.sisFormEData.FHR_ID;
			SisFormService.updateMyOpportunity('REF_FHRID',SisFormService.sisData.oppData.REF_FHRID);
			SisFormEService.insertDataInSisScreenETable();
			SfePage1Service.checkTermAllowed().then(
				function(res){
					debug("res : " + res);
					if(res){
						SisFormService.sisData.oppData.REF_TERM_FLAG2 = 'Y';
						SisFormService.updateMyOpportunity('REF_TERM_FLAG2','Y');
						debug("Term Allowed");
						SfePage1Service.checkNMLAllowed().then(
							function(nmlRes){
								debug("nmlRes : " + nmlRes);
								if(nmlRes){
									SisFormService.sisData.oppData.REF_NML_FLAG = 'Y';
									SisFormService.updateMyOpportunity('REF_NML_FLAG','Y');
									SfePage1Service.getNMLCoverage().then(
										function(nmlCoverage){
											debug("nmlCoverage : " + nmlCoverage);
											if(!!nmlCoverage && nmlCoverage>0){
												SisFormService.sisData.oppData.REF_NML_AMT = nmlCoverage;
												SisFormService.updateMyOpportunity('REF_NML_AMT',nmlCoverage);
												CommonService.showLoading("Loading...");
												$state.go("sisForm.sfePage2");
											}else{
												SfePage1Service.getMLCoverage().then(
													function(mlData){
														debug("mlData : " + JSON.stringify(mlData));
														CommonService.showLoading("Loading...");
														$state.go("sisForm.sfePage2",{"ML_PREM_AMT" : mlData.premium, "ML_COV_AMT" : mlData.coverage});
													}
												);
											}
										}
									);
								}else{
									debug("skip term nml");
									SisFormService.sisData.oppData.REF_NML_FLAG = 'N';
									SisFormService.updateMyOpportunity('REF_NML_FLAG','N');
									SfePage1Service.getMLCoverage().then(
										function(mlData){
											debug("mlData : " + JSON.stringify(mlData));
											CommonService.showLoading("Loading...");
											$state.go("sisForm.sfePage2",{"ML_PREM_AMT" : null, "ML_COV_AMT" : null});
										}
									);
								}
							}
						);
					}else{
						debug("skip term");
						SisFormService.sisData.oppData.REF_NML_FLAG = 'N';
						SisFormService.updateMyOpportunity('REF_TERM_FLAG2','N');
						$state.go("sisForm.sisOutput");
					}
				}
			);
		}
		else{
			navigator.notification.alert('Please fill all mandatory fields',null,"SIS","OK");
		}
	};

	this.onBack = function(){
		SfePage1Service.goBack();
	};
}]);

sfePage1Module.service('SfePage1Service', ['$q', '$state', 'CommonService','SisFormService', 'SisFormDService', function($q, $state, CommonService,SisFormService, SisFormDService){

	this.sfePage1Data = null;
	this.FFNAData = {};
	var sc = this;

	this.setSfePage1Data = function(sfePage1Data){
		this.sfePage1Data = sfePage1Data;
	};

	this.getSfePage1Data = function(){
		return this.sfePage1Data;
	};

	this.setDataInSisFormEData = function(sfePage1Data){
		if(!SisFormService.sisData.sisFormEData){
			SisFormService.sisData.sisFormEData = {};
		}
		SisFormService.sisData.sisFormEData.OPPORTUNITY_ID = SisFormService.sisData.sisFormEData.OPPORTUNITY_ID || SisFormService.sisData.oppData.OPPORTUNITY_ID;
		SisFormService.sisData.sisFormEData.FHR_ID = SisFormService.sisData.sisFormEData.FHR_ID || CommonService.getRandomNumber();
		SisFormService.sisData.sisFormEData.RESIDENT_STATUS = sfePage1Data.RESIDENT_STATUS || null;
		SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE = sfePage1Data.RESIDENT_STATUS_CODE || null;
		SisFormService.sisData.sisFormEData.NATIONALITY = sfePage1Data.NATIONALITY != "Select " ? sfePage1Data.NATIONALITY : null;
		SisFormService.sisData.sisFormEData.NATIONALITY_CODE = sfePage1Data.NATIONALITY_CODE || null;
		SisFormService.sisData.sisFormEData.CURR_RESIDENT_COUNTRY = sfePage1Data.CURR_RESIDENT_COUNTRY != "Select " ? sfePage1Data.CURR_RESIDENT_COUNTRY : null;
		SisFormService.sisData.sisFormEData.CURR_RESIDENT_COUNTRY_CODE = sfePage1Data.CURR_RESIDENT_COUNTRY_CODE || null;
		SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION = sfePage1Data.EDUCATION_QUALIFICATION || null;
		SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE = sfePage1Data.EDUCATION_QUALIFICATION_CODE || null;
		SisFormService.sisData.sisFormEData.CURR_CITY = sfePage1Data.CURR_CITY != "Select " ? sfePage1Data.CURR_CITY : null;
		SisFormService.sisData.sisFormEData.CURR_CITY_CODE = sfePage1Data.CURR_CITY_CODE || null;
		SisFormService.sisData.sisFormEData.CURR_STATE = sfePage1Data.CURR_STATE != "Select " ? sfePage1Data.CURR_STATE : null;
		SisFormService.sisData.sisFormEData.CURR_STATE_CODE = sfePage1Data.CURR_STATE_CODE || null;
		SisFormService.sisData.sisFormEData.OCCUPATION_CLASS = sfePage1Data.OCCUPATION_CLASS || null;
		SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE = sfePage1Data.OCCUPATION_CLASS_NBFE_CODE || null;
		SisFormService.sisData.sisFormEData.CITY_TERM_FLAG = sfePage1Data.CITY_TERM_FLAG || null;
		SisFormService.sisData.sisFormEData.COUNTRY_TERM_FLAG = sfePage1Data.COUNTRY_TERM_FLAG || null;
		SisFormService.sisData.sisFormEData.CITY_TERM_NML_A = sfePage1Data.CITY_TERM_NML_A || null;
		SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_A = sfePage1Data.COUNTRY_TERM_NML_A || null;
		SisFormService.sisData.sisFormEData.CITY_TERM_NML_B = sfePage1Data.CITY_TERM_NML_B || null;
		SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_B = sfePage1Data.COUNTRY_TERM_NML_B || null;
	};

	this.getResidentCountryList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
			function(res){
				var ResidentCountryList = [];
				debug("resiCountry :" + res.rows.length);
				if(!!res && res.rows.length>0){
					ResidentCountryList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null,"NATIONALITY_ALLOWED": null, "FATF_FLAG": null, "MPDOC_CODE": null, "MPPROOF_CODE": ""});
					for(var i=0;i<res.rows.length;i++){
						var resident = {};
						resident.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						resident.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
						resident.NATIONALITY_ALLOWED = res.rows.item(i).NATIONALITY_ALLOWED;
						resident.FATF_FLAG = res.rows.item(i).FATF_FLAG;
						resident.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						resident.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						resident.TERM_FLAG = res.rows.item(i).TERM_FLAG;
						resident.TERM_NML_A = res.rows.item(i).TERM_NML_A;
						resident.TERM_NML_B = res.rows.item(i).TERM_NML_B;
						ResidentCountryList.push(resident);
					}
					dfd.resolve(ResidentCountryList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getEduQualList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.LOOKUP_TYPE = 'Education';
		whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_APP_SUB_QUESTION_LOOKUP",false,"*",whereClauseObj, "NBFE_CODE ASC").then(
			function(res){
				var EduQualList = [];
				debug("EduQualiftn :" + res.rows.length);
				if(!!res && res.rows.length>0){
						EduQualList.push({"LOOKUP_TYPE_DESC": "Select ", "LOOKUP_TYPE": null,"NBFE_CODE":null, "ISACTIVE": null});
						for(var i=0;i<res.rows.length;i++){
						var eduQual = {};
						eduQual.LOOKUP_TYPE = res.rows.item(i).LOOKUP_TYPE;
						eduQual.NBFE_CODE = res.rows.item(i).NBFE_CODE;
						eduQual.LOOKUP_TYPE_DESC = res.rows.item(i).LOOKUP_TYPE_DESC;
						eduQual.ISACTIVE = res.rows.item(i).ISACTIVE;
						EduQualList.push(eduQual);
					}
					dfd.resolve(EduQualList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getStateList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		CommonService.selectRecords(db,"LP_STATE_MASTER",false,"*",whereClauseObj, "STATE_DESC ASC").then(
			function(res){
				var StateList = [];
				debug("StateList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					StateList.push({"STATE_DESC": "Select ", "STATE_CODE":null, "COUNTRY_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var stateList = {};
						stateList.STATE_DESC = res.rows.item(i).STATE_DESC;
						stateList.STATE_CODE = res.rows.item(i).STATE_CODE;
						stateList.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						StateList.push(stateList);
					}
					dfd.resolve(StateList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getCityList = function(stateCode, isEKyc){
		var dfd = $q.defer();
		debug("getCityList : " + stateCode);
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT * FROM LP_CITY_MASTER WHERE STATE_CODE IN (?,?) AND ISACTIVE=? ORDER BY CITY_DESC ASC";
				var parameterList = [stateCode,'0','Y'];
				if(!!isEKyc){
					query = "SELECT * FROM LP_CITY_MASTER WHERE STATE_CODE in ((select state_code from lp_state_master where state_desc like '" + stateCode + "'), ?) AND ISACTIVE=? ORDER BY CITY_DESC ASC";
					parameterList = ['0', 'Y'];
				}
				CommonService.executeSql(tx,query,parameterList,
					function(tx,res){
						var CityList = [];
						debug("CityList :" + res.rows.item(0).CITY_DESC);
						if(!!res && res.rows.length>0){
							CityList.push({"CITY_DESC": "Select ", "CITY_CODE":null, "STATE_CODE":null, "TOP_CITY_FLAG":null});
							for(var i=0;i<res.rows.length;i++){
								var cityList = {};
								cityList.CITY_DESC = res.rows.item(i).CITY_DESC;
								cityList.CITY_CODE = res.rows.item(i).CITY_CODE;
								cityList.STATE_CODE = res.rows.item(i).STATE_CODE;
								cityList.TOP_CITY_FLAG = res.rows.item(i).TOP_CITY_FLAG;
								cityList.TERM_FLAG = res.rows.item(i).TERM_FLAG;
								cityList.TERM_NML_A = res.rows.item(i).TERM_NML_A;
								cityList.TERM_NML_B = res.rows.item(i).TERM_NML_B;
								CityList.push(cityList);
							}
							dfd.resolve(CityList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
			dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getNationalityList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.NATIONALITY_ALLOWED = 'Y';
		whereClauseObj.ISACTIVE = 'Y';

		CommonService.selectRecords(db,"LP_COUNTRY_MASTER",false,"*",whereClauseObj, "COUNTRY_NAME ASC").then(
			function(res){
				var nationalityList = [];
				debug("nationalityList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					nationalityList.push({"COUNTRY_NAME": "Select ", "COUNTRY_CODE": null, "FATF_FLAG": null, "MPDOC_CODE": null, "MPPROOF_CODE": null});
					for(var i=0;i<res.rows.length;i++){
						var nationality = {};
						nationality.COUNTRY_CODE = res.rows.item(i).COUNTRY_CODE;
						nationality.COUNTRY_NAME = res.rows.item(i).COUNTRY_NAME;
						nationality.FATF_FLAG = res.rows.item(i).FATF_FLAG;
						nationality.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						nationality.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						nationalityList.push(nationality);
					}
					dfd.resolve(nationalityList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getOccClassList = function(){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.ISACTIVE = 'Y';
		CommonService.selectRecords(db, "LP_APP_OCCUPATION_CLASS", false, "*", whereClauseObj, "OCCU_CLASS ASC").then(
			function(res){
				var OccClassList = [];
				console.log("OccList :" + res.rows.length);
				if(!!res && res.rows.length>0){
					OccClassList.push({"OCCU_CLASS": "Select ", "NBFE_CODE":null, "MPDOC_CODE":null,"OCCU_CLASS_PRINT":null,"MPPROOF_CODE":null});
					for(var i=0;i<res.rows.length;i++){
						var occClassList = {};
						occClassList.OCCU_CLASS = res.rows.item(i).OCCU_CLASS;
						occClassList.OCCU_CLASS_PRINT = res.rows.item(i).OCCU_CLASS_PRINT;
						occClassList.NBFE_CODE = res.rows.item(i).NBFE_CODE;
						occClassList.MPDOC_CODE = res.rows.item(i).MPDOC_CODE;
						occClassList.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						OccClassList.push(occClassList);
					}
					dfd.resolve(OccClassList);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.checkTermAllowed = function(){
		var dfd = $q.defer();
		debug("FFNAData : " + JSON.stringify(sc.FFNAData));
		var annIncome = parseInt(sc.FFNAData.FFNA.ANNUAL_INCOME);
		debug("RESIDENT_STATUS_CODE : " + SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE);
		debug("CITY_TERM_FLAG : " + SisFormService.sisData.sisFormEData.CITY_TERM_FLAG);
		debug("COUNTRY_TERM_FLAG : " + SisFormService.sisData.sisFormEData.COUNTRY_TERM_FLAG);
		debug("EDUCATION_QUALIFICATION_CODE : " + SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE);
		debug("OCCUPATION_CLASS_NBFE_CODE : " + SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE);
		debug("annIncome : " + SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE);

		var ec1 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && SisFormService.sisData.sisFormEData.CITY_TERM_FLAG=='Y' && [3,4,5].includes(parseInt(SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE));
		var ec2 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && SisFormService.sisData.sisFormEData.COUNTRY_TERM_FLAG=='Y' && [4,5].includes(parseInt(SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE));
		var ec3 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 300000;
		var ec4 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 1200000;
		var ec5 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 500000;
		var ec6 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 2000000;
		var checkTermAllowedArr = [ec1,ec2,ec3,ec4,ec5,ec6];
		debug("checkTermAllowedArr : " + JSON.stringify(checkTermAllowedArr));
		if((ec1 && (ec3 || ec5)) || (ec2 && (ec4 || ec6))){
			debug("Eligibility Criteria 2 Fullfilled");
			dfd.resolve(true);
		}
		else{
			debug("Eligibility Criteria 2 not Fullfilled");
			dfd.resolve(false);
		}
		return dfd.promise;
	};

	this.checkNMLAllowed = function(){
		var dfd = $q.defer();
		var annIncome = parseInt(sc.FFNAData.FFNA.ANNUAL_INCOME);
		var segment = sc.FFNAData.FFNA.CONSUMER_SEGMENT;
		debug("segment : " + segment);
		debug("RESIDENT_STATUS_CODE : " + SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE);
		debug("CITY_TERM_NML_A : " + SisFormService.sisData.sisFormEData.CITY_TERM_FLAG);
		debug("CITY_TERM_NML_B : " + SisFormService.sisData.sisFormEData.CITY_TERM_NML_B);
		debug("COUNTRY_TERM_NML_A : " + SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_A);
		debug("COUNTRY_TERM_NML_B : " + SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_B);
		debug("EDUCATION_QUALIFICATION_CODE : " + SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE);
		debug("OCCUPATION_CLASS_NBFE_CODE : " + SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE);
		debug("annIncome : " + annIncome);
		var ec1 = false;
		var ec2 = false;
		var ec3 = false;
		var ec4 = false;
		var ec5 = false;
		var ec6 = false;
		var ec7 = false;
		var ec8 = false;
		var checkNMLAllowedArr = [];

		if(segment=="EXCLUSIVE" || segment=="SELECT"){
			ec1 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && SisFormService.sisData.sisFormEData.CITY_TERM_NML_A=='Y';
			ec2 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_A=='Y';
			ec3 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 500000;
			ec4 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 1200000;
			ec5 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 750000;
			ec6 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 2000000;
			ec7 = [4,5].includes(parseInt(SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE));
			checkNMLAllowedArr = [ec1,ec2,ec3,ec4,ec5,ec6,ec7];
			debug("checkNMLAllowedArr : " + JSON.stringify(checkNMLAllowedArr));
			if((ec1 && (ec3 || ec5) && ec7) || (ec2 && (ec4 || ec6) && ec7)){
				debug("NML Exclusive || SELECT Criteria Fullfilled");
				dfd.resolve(true);
			}else{
				debug("NML Exclusive || SELECT Criteria not Fullfilled");
				dfd.resolve(false);
			}
		}
		else{
			ec1 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && SisFormService.sisData.sisFormEData.CITY_TERM_NML_B=='Y';
			ec2 = SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && SisFormService.sisData.sisFormEData.COUNTRY_TERM_NML_B=='Y';
			ec3 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 500000;
			ec4 = (SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==1 || SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==2) && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 1200000;
			ec5 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE=='RI' && annIncome >= 750000;
			ec6 = SisFormService.sisData.sisFormEData.OCCUPATION_CLASS_NBFE_CODE==3 && SisFormService.sisData.sisFormEData.RESIDENT_STATUS_CODE!='RI' && annIncome >= 2000000;
			ec7 = [4,5].includes(parseInt(SisFormService.sisData.sisFormEData.EDUCATION_QUALIFICATION_CODE));
			if(!!sc.FFNAData.FFNA.EXIST_BANK_AC_OPEN_DATE){
				ec8 = CommonService.getAge(sc.FFNAData.FFNA.EXIST_BANK_AC_OPEN_DATE) >= 1 ? true : false;
			}
			checkNMLAllowedArr = [ec1,ec2,ec3,ec4,ec5,ec6,ec7,ec8];
			debug("checkNMLAllowedArr : " + JSON.stringify(checkNMLAllowedArr));
			if((ec1 && (ec3 || ec5) && ec7 && ec8) || (ec2 && (ec4 || ec6) && ec7 && ec8)){
				debug("NML OTHER Criteria Fullfilled");
				dfd.resolve(true);
			}
			else{
				debug("NML OTHER Criteria not Fullfilled");
				dfd.resolve(false);
			}
		}
		return dfd.promise;
	};

	this.getFFNATableData = function(FHR_ID, AGENT_CD){
		var dfd = $q.defer();
		var FFNAData = {};
		var whereClauseObj = {};
		whereClauseObj.FHR_ID = FHR_ID;
		whereClauseObj.AGENT_CD = AGENT_CD;
		CommonService.selectRecords(db,"LP_FHR_FFNA",false,"*",whereClauseObj, "").then(
			function(res){
				debug("LP_FHR_FFNA :" + res.rows.length);
				if(!!res && res.rows.length>0){
					FFNAData.FFNA = CommonService.resultSetToObject(res);
					dfd.resolve(FFNAData);
				}
				else{
					dfd.resolve(FFNAData);
				}
			}
		);
		return dfd.promise;
	};

	this.getNMLCoverage = function(){
		var dfd = $q.defer();
		//segment check needs to be put after discusion
		var segment = sc.FFNAData.FFNA.CONSUMER_SEGMENT;
		debug("segment1 : " + segment);
		if(segment == 'SELECT'){}
		else if(segment == 'EXCLUSIVE'){}
		else
			segment = 'OTHERS';

		debug("segment2 :" + segment);
		var query = "select TSAR from LP_TERM_ATTACHMENT_NML where ? between cast(MIN_AGE as int) and cast(MAX_AGE as int) and SEGMENT = ?";
		var parameterList = [parseInt(CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB)),segment];
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,query,parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							debug("TSAR: " + JSON.stringify(res.rows.item(0).TSAR));
							dfd.resolve(res.rows.item(0).TSAR);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getMLCoverage = function(){
		var dfd = $q.defer();
		var gender = SisFormService.sisData.sisFormAData.INSURED_GENDER;
		var age = CommonService.getAge(SisFormService.sisData.sisFormAData.INSURED_DOB);
		var mlData = {};
		mlData.coverage = 5000000;
		mlData.premium = 0;
		sc.getPremiumMultiple(gender,age).then(
			function(pm){
				debug("pm : " + pm);
				mlData.premium = Math.round(pm * mlData.coverage/1000);
				debug("premium : "+ mlData.premium);
				dfd.resolve(mlData);
			}
		);
		return dfd.promise;
	};

	this.getPremiumMultiple = function(gender,age){
		var dfd = $q.defer();
		debug("gender : " + gender + "\tage : " + age);
		/*
			assumptions as per the solution document:
				1. PlanCode = SR Regular Plan
				2. NonSmoker
				3. PolicyTerm = 35
		*/

		var query = "select * from LP_PML_PREMIUM_MULTIPLE_LK where PML_PNL_ID='762' and PML_SEX = ? and PML_TERM = '10' and SMOKER_FLAGAG = 'N' and PML_AGE = ?";
		var parameterList = [gender,age];
		CommonService.transaction(sisDB,
			function(tx){
				CommonService.executeSql(tx,query,parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							debug("PML_PREM_MULT: " + JSON.stringify(res.rows.item(0).PML_PREM_MULT));
							dfd.resolve(res.rows.item(0).PML_PREM_MULT);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.goBack = function(){
		var dfd = $q.defer();
		var productType = (SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
		var query = "select * from lp_rpx_rider_plan_xref where rpx_pnl_id = (select pnl_id from lp_pnl_plan_lk where pnl_code = ?)";
		var parameterList = [SisFormService.sisData.sisFormBData.PLAN_CODE];
		CommonService.transaction(sisDB,
			function(tx){
				CommonService.executeSql(tx,query,parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							debug("going to rider page");
							SisFormDService.getRidersList().then(
								function(ridersList){
									if(!!ridersList && ridersList.length>0){
										$state.go("sisForm.sisFormD",{"RIDER_LIST": ridersList});
									}
									else{
										if(!!productType && productType=="C"){
											CommonService.showLoading("Loading...");
											$state.go("sisForm.sisFormB");
										}
										else{
											CommonService.showLoading("Loading...");
											$state.go("sisForm.sisFormC");
										}
									}
								}
							);
						}
						else{
							if(!!productType && productType=="C"){
								CommonService.showLoading("Loading...");
								$state.go("sisForm.sisFormB");
							}
							else{
								CommonService.showLoading("Loading...");
								$state.go("sisForm.sisFormC");
							}
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
