sisFormEModule.controller('SisFormECtrl',['$state', 'CommonService', 'SisFormService',function($state, CommonService, SisFormService){
	"use strict";
}]);

sisFormEModule.service('SisFormEService',['$q', 'CommonService', 'SisFormService', function($q, CommonService, SisFormService){
	"use strict";
	var sc = this;

	this.getSfePage1Data = function(sisFormEData){
		debug("in getSfePage1Data");
		var sfePage1Data = {};
		sfePage1Data.RESIDENT_STATUS = sisFormEData.RESIDENT_STATUS || null;
		sfePage1Data.RESIDENT_STATUS_CODE = sisFormEData.RESIDENT_STATUS_CODE || null;
		sfePage1Data.NATIONALITY = sisFormEData.NATIONALITY || null;
		sfePage1Data.NATIONALITY_CODE = sisFormEData.NATIONALITY_CODE || null;
		sfePage1Data.CURR_RESIDENT_COUNTRY = sisFormEData.CURR_RESIDENT_COUNTRY || null;
		sfePage1Data.CURR_RESIDENT_COUNTRY_CODE = sisFormEData.CURR_RESIDENT_COUNTRY_CODE || null;
		sfePage1Data.EDUCATION_QUALIFICATION = sisFormEData.EDUCATION_QUALIFICATION || null;
		sfePage1Data.EDUCATION_QUALIFICATION_CODE = sisFormEData.EDUCATION_QUALIFICATION_CODE || null;
		sfePage1Data.CURR_CITY = sisFormEData.CURR_CITY || null;
		sfePage1Data.CURR_CITY_CODE = sisFormEData.CURR_CITY_CODE || null;
		sfePage1Data.CURR_STATE = sisFormEData.CURR_STATE || null;
		sfePage1Data.CURR_STATE_CODE = sisFormEData.CURR_STATE_CODE || null;
		sfePage1Data.OCCUPATION_CLASS = sisFormEData.OCCUPATION_CLASS || null;
		sfePage1Data.OCCUPATION_CLASS_NBFE_CODE = sisFormEData.OCCUPATION_CLASS_NBFE_CODE || null;
		sfePage1Data.CITY_TERM_FLAG = sisFormEData.CITY_TERM_FLAG || null;
		sfePage1Data.COUNTRY_TERM_FLAG = sisFormEData.COUNTRY_TERM_FLAG || null;
		sfePage1Data.CITY_TERM_NML_A = sisFormEData.CITY_TERM_NML_A || null;
		sfePage1Data.COUNTRY_TERM_NML_A = sisFormEData.COUNTRY_TERM_NML_A || null;
		sfePage1Data.CITY_TERM_NML_B = sisFormEData.CITY_TERM_NML_B || null;
		sfePage1Data.COUNTRY_TERM_NML_B = sisFormEData.COUNTRY_TERM_NML_B || null;
		return sfePage1Data;
	};

	this.getSfePage3Data = function(sisFormEData, isTermChanged){
		debug("in getsfePage3Data");
		var sfePage3Data = {};
		if(!!isTermChanged){
		    sfePage3Data.PGL_ID = sisFormEData.PGL_ID || null;
            sfePage3Data.PLAN_NAME = null;
            sfePage3Data.PLAN_CODE = null;
            sfePage3Data.PREMIUM_PAY_MODE = null;
            sfePage3Data.PREMIUM_PAY_MODE_DESC = null;
            sfePage3Data.POLICY_TERM = null;
            sfePage3Data.PREMIUM_PAY_TERM = null;
            sfePage3Data.SUM_ASSURED = null;
            sfePage3Data.BASE_PREMIUM = null;
            sfePage3Data.INVT_STRAT_FLAG = null;
            sfePage3Data.PREM_MULTIPLIER = null;
            sfePage3Data.INPUT_ANNUAL_PREMIUM = null;
		}else{
            sfePage3Data.PGL_ID = sisFormEData.PGL_ID || null;
            sfePage3Data.PLAN_NAME = sisFormEData.PLAN_NAME || null;
            sfePage3Data.PLAN_CODE = sisFormEData.PLAN_CODE || null;
            sfePage3Data.PREMIUM_PAY_MODE = sisFormEData.PREMIUM_PAY_MODE || null;
            sfePage3Data.PREMIUM_PAY_MODE_DESC = sisFormEData.PREMIUM_PAY_MODE_DESC || null;
            sfePage3Data.POLICY_TERM = sisFormEData.POLICY_TERM || null;
            sfePage3Data.PREMIUM_PAY_TERM = sisFormEData.PREMIUM_PAY_TERM || null;
            sfePage3Data.SUM_ASSURED = sisFormEData.SUM_ASSURED || null;
            sfePage3Data.BASE_PREMIUM = sisFormEData.BASE_PREMIUM || null;
            sfePage3Data.INVT_STRAT_FLAG = sisFormEData.INVT_STRAT_FLAG || null;
            sfePage3Data.PREM_MULTIPLIER = sisFormEData.PREM_MULTIPLIER || null;
            sfePage3Data.INPUT_ANNUAL_PREMIUM = sisFormEData.INPUT_ANNUAL_PREMIUM || null;
        }
		debug("returning sfePage3Data:: " + JSON.stringify(sfePage3Data));
		return sfePage3Data;
	};

	this.insertDataInSisScreenETable = function(){
		var dfd = $q.defer();
		SisFormService.sisData.sisFormEData.MODIFIED_DATE = CommonService.getCurrDate();
		CommonService.insertOrReplaceRecord(db, "LP_COMBO_ADD_DETAILS", SisFormService.sisData.sisFormEData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_COMBO_ADD_DETAILS successfully: BASE_FHR_ID: " + SisFormService.sisData.sisMainData.FHR_ID + " TERM_FHR_ID: " + SisFormService.sisData.sisFormEData.FHR_ID);
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_COMBO_ADD_DETAILS");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};
}]);
