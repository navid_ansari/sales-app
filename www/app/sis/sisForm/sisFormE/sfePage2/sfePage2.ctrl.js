sfePage2Module.controller('SfePage2Ctrl',['$state', '$filter', 'CommonService', 'SfePage2Service', 'SfePage2Data','MLData', 'SisFormService', 'SisFormEService', 'SisTimelineService', function($state, $filter, CommonService, SfePage2Service, SfePage2Data, MLData, SisFormService, SisFormEService, SisTimelineService){
	"use strict";
	CommonService.hideLoading();
                                          
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/
                                          
                                          
	SisTimelineService.setActiveTab("otherDetails");
	debug("SfePage2Data : " + JSON.stringify(SfePage2Data));
	debug("MLData : " + JSON.stringify(MLData));
	var sc = this;
	this.showDisclaimer = false;
	this.msg = "";
	this.productList = [
		{"PRODUCT_NAME": "Select a product", "PGL_ID": null},
		{"PRODUCT_NAME": "Tata AIA Life Insurance Sampoorna Raksha", "PGL_ID": "224"},
		{"PRODUCT_NAME": "Tata AIA Life Insurance Sampoorna Raksha+", "PGL_ID": "225"}
	];
	this.selectedPGL = SisFormService.sisData.sisFormEData.PGL_ID;
	this.productSelected = ($filter('filter')(this.productList, {"PGL_ID": sc.selectedPGL}, true)[0]) || this.productList[0];
	this.comboIdExists = !!SisFormService.sisData.oppData.COMBO_ID;
	if(!!SfePage2Data && SfePage2Data.REF_NML_FLAG == 'Y' && SfePage2Data.REF_NML_AMT>0){
		sc.showDisclaimer = true;
		var NML_AMOUNT= $filter('myCurrency')(SfePage2Data.REF_NML_AMT);
        sc.msg = "Congratulations! You have a pre-approved Non-Medical life cover of Rs. " + NML_AMOUNT;
		//sc.msg = "Congratulations! You have a pre-approved Non-Medical life cover of Rs. " + SfePage2Data.REF_NML_AMT;
	}
	else if(!!MLData){
		var ML_COVAMOUNT= $filter('myCurrency')(MLData.ML_COV_AMT);
		var ML_PREMAMOUNT= $filter('myCurrency')( MLData.ML_PREM_AMT);
		sc.msg = "You can protect your family with a life cover of Rs. " + ML_COVAMOUNT + " at Rs. " + ML_PREMAMOUNT + " only";

	}


	this.onSkip = function(){
		debug("skip by user going to normal flow");
		$state.go("sisForm.sisOutput", {"BASE_OR_TERM": "BASE"});
		//$state.go("sisForm.sisOutput");
	};

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("sisForm.sfePage1");
	};

	this.onNext = function(){
		CommonService.showLoading();
		var isTermChanged = false;
		if(this.productSelected.PGL_ID != SisFormService.sisData.sisFormEData.PGL_ID)
		    isTermChanged = true;
		SisFormService.sisData.sisFormEData.PGL_ID = this.productSelected.PGL_ID;
		SisFormEService.insertDataInSisScreenETable().then(
			function(res){
				if(!!res){
					debug("PGL_ID : " + sc.productSelected.PGL_ID);
					$state.go("sisForm.sfePage3", {"PGL_ID": sc.productSelected.PGL_ID, "IS_TERM_CHANGED":isTermChanged});
				}
				else{
					CommonService.hideLoading();
					navigator.notification.alert("Cannot navigate to the next page.",null,"SIS","OK");
				}
			}
		);
	};

}]);

sfePage2Module.service('SfePage2Service', ['$q', 'CommonService','SisFormService', function($q, CommonService,SisFormService){
	this.getNMLAmount = function(agentCd, leadId, fhrId, sisId){
		var dfd = $q.defer();
		var FFNAData = {};
		var whereClauseObj = {};
		whereClauseObj.AGENT_CD = agentCd;
		whereClauseObj.LEAD_ID = leadId;
		whereClauseObj.FHR_ID = fhrId;
		whereClauseObj.SIS_ID = sisId;
		CommonService.selectRecords(db,"LP_MYOPPORTUNITY",false,"*",whereClauseObj, "").then(
			function(res){
				debug("LP_MYOPPORTUNITY :" + res.rows.length);
				if(!!res && res.rows.length>0){
					dfd.resolve(CommonService.resultSetToObject(res));
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
