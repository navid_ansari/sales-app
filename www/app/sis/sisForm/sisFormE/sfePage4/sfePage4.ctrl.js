sfePage4Module.controller('SfePage4Ctrl',['$state', 'CommonService', 'SfePage4Service', 'SfePage4Data', 'SisFormService', 'SisTimelineService', function($state, CommonService, SfePage4Service, SfePage4Data, SisFormService, SisTimelineService){
	"use strict";
	SisTimelineService.setActiveTab("otherDetails");
	CommonService.hideLoading();
                                          
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/
                                          
                                          
	debug("SfePage4Data : " + JSON.stringify(SfePage4Data));
	this.sfePage4Data = SfePage4Data;

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("sisForm.sfePage3", {"PGL_ID": SisFormService.sisData.sisFormEData.PGL_ID});
	};

	this.onNext = function(){
        CommonService.showLoading();
		SfePage4Service.onNext();
	};

}]);

sfePage4Module.service('SfePage4Service', ['$q', 'CommonService','SisFormService', '$state', 'SisTimelineService', function($q, CommonService, SisFormService, $state, SisTimelineService){
	var sc = this;

	this.getSfePage4Data = function(){
		var dfd = $q.defer();
		var sfePage4Data = {};
		debug("sis data is "+SisFormService.sisData.sisFormEData);
		sfePage4Data.BASE_PRODUCT_NAME = SisFormService.sisData.sisFormBData.PLAN_NAME || "";
		sfePage4Data.TERM_PRODUCT_NAME = SisFormService.sisData.sisFormEData.PLAN_NAME || "";
		sfePage4Data.BASE_PT = SisFormService.sisData.sisFormBData.POLICY_TERM || 0;
		sfePage4Data.TERM_PT = SisFormService.sisData.sisFormEData.POLICY_TERM || 0;
		sfePage4Data.BASE_PPT = SisFormService.sisData.sisFormBData.PREMIUM_PAY_TERM || 0;
		sfePage4Data.TERM_PPT = SisFormService.sisData.sisFormEData.PREMIUM_PAY_TERM || 0;
		sfePage4Data.BASE_PREMIUM = parseInt(SisFormService.sisData.sisFormBData.INPUT_ANNUAL_PREMIUM) || 0;
		sfePage4Data.TERM_PREMIUM = parseInt(SisFormService.sisData.sisFormEData.INPUT_ANNUAL_PREMIUM) || 0;
		sfePage4Data.BASE_SA = parseInt(SisFormService.sisData.sisFormBData.SUM_ASSURED) || 0;
		sfePage4Data.TERM_SA = parseInt(SisFormService.sisData.sisFormEData.SUM_ASSURED) || 0;
		if(!!SisFormService.sisData.sisFormDData){
			debug("SisFormService.sisData.sisFormDData : " + JSON.stringify(SisFormService.sisData.sisFormDData));
			if(SisFormService.sisData.sisFormDData instanceof Array && SisFormService.sisData.sisFormDData.length>0){
				var prodType = SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE;
				if(prodType != "U")
					sfePage4Data.ISRIDER = true;
				sfePage4Data.RIDER_NAME = SisFormService.sisData.sisFormDData[0].RIDER_NAME || "";
				//sfePage4Data.RIDER_PT = SisFormService.sisData.sisFormDData[0].RIDER_TERM || 0;
				//sfePage4Data.RIDER_PPT = SisFormService.sisData.sisFormData[0].RIDER_PAY_TERM || 0;
				sfePage4Data.RIDER_PREMIUM = parseInt(SisFormService.sisData.sisFormDData[0].RIDER_ANNUAL_PREMIUM) || 0;
				sfePage4Data.RIDER_SA = parseInt(SisFormService.sisData.sisFormDData[0].RIDER_COVERAGE) || 0;
			}else if((Object.keys(SisFormService.sisData.sisFormDData).length>0) && (!!SisFormService.sisData.sisFormDData[0])){
				var prodType = SisFormService.getProductDetails().LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE;
				if(prodType != "U")
					sfePage4Data.ISRIDER = true;
				sfePage4Data.RIDER_NAME = SisFormService.sisData.sisFormDData[0].RIDER_NAME || "";
				//sfePage4Data.RIDER_PT = SisFormService.sisData.sisFormDData[0].RIDER_TERM || 0;
				//sfePage4Data.RIDER_PPT = SisFormService.sisData.sisFormData[0].RIDER_PAY_TERM || 0;
				sfePage4Data.RIDER_PREMIUM = parseInt(SisFormService.sisData.sisFormDData[0].RIDER_ANNUAL_PREMIUM) || 0;
				sfePage4Data.RIDER_SA = parseInt(SisFormService.sisData.sisFormDData[0].RIDER_COVERAGE) || 0;
			}
		}
		dfd.resolve(sfePage4Data);
		return dfd.promise;
	};

	this.onNext = function(){
		var dfd = $q.defer();
		SisFormService.sisData.oppData.REF_FHRID = SisFormService.sisData.oppData.REF_FHRID || SisFormService.sisData.sisFormEData.FHR_ID || CommonService.getRandomNumber();
		SisFormService.sisData.oppData.REF_SIS_ID = SisFormService.sisData.oppData.REF_SIS_ID || CommonService.getRandomNumber();
		SisFormService.sisData.oppData.COMBO_ID = SisFormService.sisData.oppData.COMBO_ID || CommonService.getRandomNumber();
		this.updateOpportunity(SisFormService.sisData.oppData.REF_FHRID, SisFormService.sisData.oppData.REF_SIS_ID, SisFormService.sisData.oppData.COMBO_ID).then(
			function(res){
				debug("res : " + res.rowsAffected);
				if(!!res){
					return sc.replicateOpportunity(SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.oppData.REF_FHRID,SisFormService.sisData.sisMainData.SIS_ID, SisFormService.sisData.oppData.REF_SIS_ID, SisFormService.sisData.sisMainData.AGENT_CD,SisFormService.sisData.oppData.COMBO_ID).then(
						function(repOppRes){
							debug("repOppRes : " + repOppRes);
							if(repOppRes){
								return repOppRes;
							}
							else{
								return null;
							}
						}
					)
					.then(
						function(repOppRes){
							if(!!repOppRes && repOppRes=="S"){
								return sc.replicateFHRRelatedData(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.oppData.REF_FHRID).then(
									function(repFhrRes){
										debug("replicateFHRRelatedData: " + repFhrRes);
										return repFhrRes;
									}
								);
							}
							else{
								return null;
							}
						}
					)
					.then(
						function(repFhrRes){
							debug("repFhrRes: " + repFhrRes);
							if(!!repFhrRes && repFhrRes=="S"){
								return sc.replicateSISRelatedData(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.oppData.REF_FHRID, SisFormService.sisData.sisMainData.SIS_ID, SisFormService.sisData.oppData.REF_SIS_ID).then(
									function(repSisRes){
										debug("replicateSISRelatedData: " + repSisRes);
										return repSisRes;
									}
								);
							}
							else{
								return null;
							}
						}
					)
					.then(
						function(repSisRes){
							debug("repSisRes: " + repSisRes);
							return repSisRes;
						}
					);
				}
				else{
					debug("combo id generation error");
					return null;
				}
			}
		)
		.then(
			function(repSisRes){
				if(!!repSisRes && repSisRes=="S"){
				    SisTimelineService.isOtherDetailsTabComplete = true;
					$state.go("sisForm.sisOutput");
				}
				else{
					navigator.notification.alert("Could not save the data.", null, "SIS", "OK");
				}
			}
		);
		return dfd.promise;
	};

	this.updateOpportunity = function(REF_FHRID,REF_SIS_ID,COMBO_ID){
		var dfd = $q.defer();
		CommonService.updateRecords(db, 'lp_myopportunity', {'RECO_PRODUCT_DEVIATION_FLAG': 'P', 'COMBO_FLAG': 'Y', 'REF_FHRID': REF_FHRID, 'REF_SIS_ID': REF_SIS_ID, 'COMBO_ID': COMBO_ID}, {'agent_cd': SisFormService.sisData.sisMainData.AGENT_CD, 'lead_id': SisFormService.sisData.sisMainData.LEAD_ID, 'fhr_id': SisFormService.sisData.sisMainData.FHR_ID, 'sis_id':SisFormService.sisData.sisMainData.SIS_ID}).then(
			function(res){
				dfd.resolve(res || null);
			}
		);
		return dfd.promise;
	};

	this.replicateOpportunity = function(fhrId, refFhrId, sisId, refSisId, agentCd, comboId){
		var dfd = $q.defer();
		var whereClauseObj = {};
		whereClauseObj.FHR_ID = fhrId;
		whereClauseObj.AGENT_CD = SisFormService.sisData.sisMainData.AGENT_CD;
		whereClauseObj.LEAD_ID = SisFormService.sisData.sisMainData.LEAD_ID;
		whereClauseObj.OPPORTUNITY_ID = SisFormService.sisData.oppData.OPPORTUNITY_ID;
		CommonService.selectRecords(db, "lp_myopportunity", true, null, whereClauseObj).then(
			function(selectRes){
				debug("selectRes : " + selectRes.rows.length);
				if(!!selectRes && selectRes.rows.length>0){
					selectRes = selectRes.rows.item(0);
					CommonService.selectRecords(db, "lp_myopportunity", true, null, {"fhr_id": refFhrId, "sis_id": refSisId, "agent_cd": agentCd}).then(
						function(dataObj){
							debug("dataObj : " + dataObj.rows.length);
							if(!!dataObj && dataObj.rows.length>0){
								dfd.resolve("S");
							}
							else{
								selectRes.REF_TERM_FLAG1 = null;
								selectRes.REF_TERM_FLAG2 = null;
								selectRes.REF_NML_FLAG = null;
								selectRes.REF_NML_AMT = null;
								selectRes.COMBO_FLAG = null;
								selectRes.REF_FHRID = null;
								selectRes.REF_SIS_ID = null;
								selectRes.COMBO_ID = comboId;
								selectRes.RECO_PRODUCT_DEVIATION_FLAG = 'T';
								selectRes.FHR_ID = refFhrId;
								selectRes.SIS_ID = refSisId;
								selectRes.OPPORTUNITY_ID = CommonService.getRandomNumber();
								SisFormService.sisData.taOppData = JSON.parse(JSON.stringify(selectRes));
								CommonService.insertOrReplaceRecord(db, "lp_myopportunity", selectRes, true).then(
									function(insertRes){
										if(!!insertRes){
											dfd.resolve("S");
										}
										else {
											dfd.resolve(null);
										}
									}
								);
							}
						}
					);
				}
				else{
					debug("Something went wrong...");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};


	this.replicateSISRelatedData = function(agentCd, fhrId, refFhrId, sisId, refSisId){
		var dfd = $q.defer();
		this.fetchSISRelatedData(agentCd, sisId).then(
			function(sisRes){
				debug("fetchSISRelatedData: " + sisRes);
				if(!!sisRes){
					sc.insertSISRelatedData(agentCd, refFhrId, refSisId, sisRes).then(
						function(insRes){
							debug("insertSISRelatedData: " + insRes);
							dfd.resolve(insRes);
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.insertSISRelatedData = function(agentCd, refFhrId, refSisId, sisData){
		var dfd = $q.defer();
		if(!!sisData){
			sisData.LP_SIS_MAIN.FHR_ID = refFhrId;
			sisData.LP_SIS_MAIN.SIS_ID = refSisId;
			sisData.LP_SIS_MAIN.PGL_ID = SisFormService.sisData.sisFormEData.PGL_ID;
			sisData.LP_SIS_MAIN.IS_SCREEN3_COMPLETED = null;
			sisData.LP_SIS_MAIN.IS_SCREEN4_COMPLETED = null;
			sisData.LP_SIS_SCREEN_A.SIS_ID = refSisId;
			sisData.LP_SIS_SCREEN_A.ISSMOKER = "N";
			sisData.LP_SIS_SCREEN_B.SIS_ID = refSisId;
			sisData.LP_SIS_SCREEN_B.PLAN_NAME = SisFormService.sisData.sisFormEData.PLAN_NAME;
			sisData.LP_SIS_SCREEN_B.PLAN_CODE = SisFormService.sisData.sisFormEData.PLAN_CODE;
			sisData.LP_SIS_SCREEN_B.PREMIUM_PAY_MODE = SisFormService.sisData.sisFormEData.PREMIUM_PAY_MODE;
			sisData.LP_SIS_SCREEN_B.PREMIUM_PAY_MODE_DESC = SisFormService.sisData.sisFormEData.PREMIUM_PAY_MODE_DESC;
			sisData.LP_SIS_SCREEN_B.POLICY_TERM = SisFormService.sisData.sisFormEData.POLICY_TERM;
			sisData.LP_SIS_SCREEN_B.PREMIUM_PAY_TERM = SisFormService.sisData.sisFormEData.PREMIUM_PAY_TERM;
			sisData.LP_SIS_SCREEN_B.SUM_ASSURED = SisFormService.sisData.sisFormEData.SUM_ASSURED;
			sisData.LP_SIS_SCREEN_B.BASE_PREMIUM = SisFormService.sisData.sisFormEData.BASE_PREMIUM;
			sisData.LP_SIS_SCREEN_B.INVT_STRAT_FLAG = SisFormService.sisData.sisFormEData.INVT_STRAT_FLAG;
			sisData.LP_SIS_SCREEN_B.PREM_MULTIPLIER = SisFormService.sisData.sisFormEData.PREM_MULTIPLIER;
			sisData.LP_SIS_SCREEN_B.INPUT_ANNUAL_PREMIUM = SisFormService.sisData.sisFormEData.INPUT_ANNUAL_PREMIUM;
			CommonService.insertOrReplaceRecord(db, "lp_sis_main", sisData.LP_SIS_MAIN, true).then(
				function(sisMainRes){
					return sisMainRes;
				}
			).then(
				function(sisMainRes){
					if(!!sisMainRes){
						return CommonService.insertOrReplaceRecord(db, "lp_sis_screen_a", sisData.LP_SIS_SCREEN_A, true).then(
							function(sisScreenARes){
								return sisScreenARes;
							}
						);
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(sisScreenARes){
					if(!!sisScreenARes){
						return CommonService.insertOrReplaceRecord(db, "lp_sis_screen_b", sisData.LP_SIS_SCREEN_B, true).then(
							function(sisScreenBRes){
								return sisScreenBRes;
							}
						);
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(sisScreenBRes){
					if(!!sisScreenBRes){
						dfd.resolve("S");
					}
					else {
						dfd.resolve(null);
					}
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	}

	this.fetchSISRelatedData = function(agentCd, sisId){
		var dfd = $q.defer();
		var sisData = {};
		CommonService.selectRecords(db, "lp_sis_main", true, null, {agent_cd: agentCd, sis_id: sisId}).then(
			function(sisMainRes){
				debug("fetchSISRelatedData: sisMainRes: " + sisMainRes);
				if(!!sisMainRes && sisMainRes.rows.length>0){
					sisData.LP_SIS_MAIN = sisMainRes.rows.item(0);
					return sisMainRes;
				}
				else {
					return null;
				}
			}
		)
		.then(
			function(sisMainRes){
				debug("sisMainRes: " + sisMainRes);
				if(!!sisMainRes && sisMainRes.rows.length>0){
					return CommonService.selectRecords(db, "lp_sis_screen_a", true, null, {agent_cd: agentCd, sis_id: sisId}).then(
						function(sisScreenARes){
							debug("fetchSISRelatedData: sisScreenARes: " + sisScreenARes);
							if(!!sisScreenARes && sisScreenARes.rows.length>0){
								sisData.LP_SIS_SCREEN_A = sisScreenARes.rows.item(0);
								return sisScreenARes;
							}
							else{
								return null;
							}
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(sisScreenARes){
				debug("sisScreenARes: " + sisScreenARes);
				if(!!sisScreenARes && sisScreenARes.rows.length>0){
					return CommonService.selectRecords(db, "lp_sis_screen_b", true, null, {agent_cd: agentCd, sis_id: sisId}).then(
						function(sisScreenBRes){
							debug("fetchSISRelatedData: sisScreenBRes: " + sisScreenBRes);
							if(!!sisScreenBRes && sisScreenBRes.rows.length>0){
								sisData.LP_SIS_SCREEN_B = sisScreenBRes.rows.item(0);
								return sisScreenBRes;
							}
							else{
								return null;
							}
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(sisScreenBRes){
				debug("sisScreenBRes: " + sisScreenBRes);
				if(!!sisScreenBRes && sisScreenBRes.rows.length>0){
					dfd.resolve(sisData);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.replicateFHRRelatedData = function(agentCd, fhrId, refFhrId){
		var dfd = $q.defer();
		this.fetchFHRRelatedData(agentCd, fhrId).then(
			function(fhrRes){
				debug("fetchFHRRelatedData: " + JSON.stringify(fhrRes));
				if(!!fhrRes){
					sc.insertFHRRelatedData(agentCd, refFhrId, fhrRes).then(
						function(insRes){
							debug("insertFHRRelatedData: " + insRes);
							if(!!insRes){
                                sc.replicateFHRReport(agentCd, fhrId, refFhrId).then(
                                    function(fhrRepRes){
                                        debug("fhrRepRes : " + fhrRepRes);
                                        dfd.resolve(fhrRepRes);
                                    }
                                );
							}else
							    dfd.resolve(null);
						}
					);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.insertFHRRelatedData = function (agentCd, refFhrId, fhrData) {
		var dfd = $q.defer();
		if(!!fhrData){
			fhrData.LP_FHR_MAIN.FHR_ID = refFhrId;
			fhrData.LP_FHR_FFNA.FHR_ID = refFhrId;
			fhrData.LP_FHR_PRODUCT_RCMND.FHR_ID = refFhrId;
			fhrData.LP_FHR_PRODUCT_RCMND.PRODUCT_PGL_ID = SisFormService.sisData.sisFormEData.PGL_ID;
			CommonService.selectRecords(db, "lp_product_master", true, "PRODUCT_UINNO", {"PRODUCT_PGL_ID": fhrData.LP_FHR_PRODUCT_RCMND.PRODUCT_PGL_ID}).then(
				function(selRes){
					debug("selRes : " + selRes.rows.length);
					if(!!selRes && selRes.rows.length>0){
						fhrData.LP_FHR_PRODUCT_RCMND.RCMND_PRODUCT_URNNO = selRes.rows.item(0).PRODUCT_UINNO;
						return CommonService.insertOrReplaceRecord(db, "lp_fhr_main", fhrData.LP_FHR_MAIN, true).then(
							function(fhrMainRes){
								debug("insertFHRRelatedData fhrMainRes : " + fhrMainRes);
								return fhrMainRes;
							}
						);
					}
					else{
						return null;
					}
				}
			).then(
				function(fhrMainRes){
					if(!!fhrMainRes){
						return CommonService.insertOrReplaceRecord(db, "lp_fhr_ffna", fhrData.LP_FHR_FFNA, true).then(
							function(fhrFfnaRes){
								debug("insertFHRRelatedData fhrFfnaRes : " + fhrFfnaRes);
								return fhrFfnaRes;
							}
						);
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(fhrFfnaRes){
					if(!!fhrFfnaRes){
						return CommonService.insertOrReplaceRecord(db, "lp_fhr_product_rcmnd", fhrData.LP_FHR_PRODUCT_RCMND, true).then(
							function(fhrProdRecRes){
								debug("insertFHRRelatedData fhrProdRecRes : " + fhrProdRecRes);
								return fhrProdRecRes;
							}
						);
					}
					else{
						return null;
					}
				}
			)
			.then(
				function(fhrProdRecRes){
					if(!!fhrProdRecRes){
						dfd.resolve("S");
					}
					else {
						dfd.resolve(null);
					}
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.fetchFHRRelatedData = function(agentCd, fhrId){
		var dfd = $q.defer();
		var fhrData = {};
		CommonService.selectRecords(db, "lp_fhr_main", true, null, {agent_cd: agentCd, fhr_id: fhrId}).then(
			function(fhrMainRes){
				debug("fetchFHRRelatedData: fhrMainRes: " + fhrMainRes.rows.length);
				if(!!fhrMainRes && fhrMainRes.rows.length>0){
					fhrData.LP_FHR_MAIN = fhrMainRes.rows.item(0);
					return fhrMainRes;
				}
			}
		)
		.then(
			function(fhrMainRes){
				if(!!fhrMainRes && fhrMainRes.rows.length>0){
					return CommonService.selectRecords(db, "lp_fhr_ffna", true, null, {agent_cd: agentCd, fhr_id: fhrId}).then(
						function(fhrFfnaRes){
							debug("fetchFHRRelatedData fhrFfnaRes : " + fhrFfnaRes.rows.length);
							if(!!fhrFfnaRes && fhrFfnaRes.rows.length>0){
								fhrData.LP_FHR_FFNA = fhrFfnaRes.rows.item(0);
								return fhrFfnaRes;
							}
							else{
								return null;
							}
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(fhrFfnaRes){
				if(!!fhrFfnaRes && fhrFfnaRes.rows.length>0){
					return CommonService.selectRecords(db, "lp_fhr_product_rcmnd", true, null, {agent_cd: agentCd, fhr_id: fhrId}).then(
						function(fhrProdRecRes){
							debug("fetchFHRRelatedData fhrProdRecRes : " + fhrProdRecRes.rows.length);
							if(!!fhrProdRecRes && fhrProdRecRes.rows.length>0){
								fhrData.LP_FHR_PRODUCT_RCMND = fhrProdRecRes.rows.item(0);
								return fhrProdRecRes;
							}
							else{
								return null;
							}
						}
					);
				}
				else{
					return null;
				}
			}
		).
		then(
			function(fhrProdRecRes){
				if(!!fhrProdRecRes && fhrProdRecRes.rows.length>0){
					dfd.resolve(fhrData);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

    this.replicateFHRReport = function(agentCd, fhrId, refFhrId){
        var dfd = $q.defer();
        CommonService.readFile(fhrId + ".html", "/FromClient/FF/HTML/").then(
            function(res){
                if(!!res){
                    debug("res: " + res.length);
                    return res;
                }
                else{
                    return null;
                }
            }
        ).then(
            function(ffFileData){
                debug("ffFileData : " + ffFileData.length);
                if(!!ffFileData){
                    return CommonService.saveFile(refFhrId, ".html","/FromClient/FF/HTML/",ffFileData,"N").then(
                        function(saveFileResp){
                            debug("saveFileResp1 : " + saveFileResp);
                            if(!!saveFileResp && saveFileResp=="success"){
                                return saveFileResp;
                            }
                            else
                                return null;
                        }
                    );
                }else
                    return null;
            }
        ).then(
            function(saveFileResp){
                debug("saveFileResp2 : " + saveFileResp);
                if(!!saveFileResp){
                    CommonService.transaction(db,
                        function(tx){
                            tx.executeSql("Select DOC_ID from LP_DOC_PROOF_MASTER where CUSTOMER_CATEGORY = 'PR' AND MPDOC_CODE= 'FD' AND MPPROOF_CODE='FF'",[],
                                function(tx,results){
                                    if(results.rows.length > 0){
                                        debug("FF Doc Id : " + results.rows.item(0).DOC_ID);
                                        CommonService.executeSql(tx,"insert or replace into LP_DOCUMENT_UPLOAD (DOC_ID, AGENT_CD, DOC_CAT, DOC_CAT_ID , POLICY_NO , DOC_NAME, DOC_TIMESTAMP, DOC_PAGE_NO, IS_FILE_SYNCED, IS_DATA_SYNCED) VALUES (?,?,?,?,?,?,?,?,?,?) ",[results.rows.item(0).DOC_ID, agentCd ,"FHR", refFhrId,null, refFhrId+".html", CommonService.getCurrDate(), "0", "N", "N"],
                                            function(tx,res){
                                                debug("successfully updated LP_DOCUMENT_UPLOAD for term ff");
                                                dfd.resolve("S");
                                            },
                                            function(tx,err){
                                                debug("Error in LP_DOCUMENT_UPLOAD().transaction(): " + err.message);
                                                dfd.resolve(null);
                                            }
                                        );
                                   }
                                   else
                                       dfd.resolve(null);
                                },
                                function(tx,err){
                                    debug("Error: " + err.message);
                                    dfd.resolve(null);
                                }
                            );
                        },
                        function(err){
                            debug("Error in LP_DOCUMENT_UPLOAD(): " + err.message);
                            dfd.resolve(null);
                        },null
                    );
                }
                else
                    dfd.resolve(null);
            }
        );
        return dfd.promise;
    };
}]);
