sfePage3Module.directive('validateSumAssuredTa',['SfePage3Service','SisFormService', function(SfePage3Service,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!viewValue && !!SfePage3Service.minSumAssured){
						if(parseInt(viewValue)<parseInt(SfePage3Service.minSumAssured))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!viewValue && !!SfePage3Service.maxSumAssured){
						if(parseInt(viewValue)>parseInt(SfePage3Service.maxSumAssured))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					debug("viewValue: " + viewValue);
					debug("SfePage3Service.saMultipleOf: " + SfePage3Service.saMultipleOf);
					debug("SfePage3Service.startingPoint: " + SfePage3Service.startingPoint);
					if(!!viewValue && !!SfePage3Service.saMultipleOf && SfePage3Service.startingPoint=="S"){
						if((parseInt(viewValue)%parseInt(SfePage3Service.saMultipleOf))>0)
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}

				ctrl.$validators.juvenileCheck = function(ngModelValue, viewValue){
					if(!!viewValue){
						if((SisFormService.getInsuredAge()<18) && (parseInt(viewValue)>20000000))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}

				ctrl.$validators.stdAgeProofCheck = function(ngModelValue, viewValue){
					if(!!viewValue){
						if((SisFormService.sisData.sisFormAData.STD_AGEPROF_FLAG=='N') && (parseInt(viewValue)>1000000))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
			}
	};
}]);

sfePage3Module.directive('validatePolicyTermTa',['SfePage3Service', function(SfePage3Service){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			ppt: "=validatePolicyTerm"
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minTerm = function(ngModelValue, viewValue){
					debug("in minTerm");
					if(!!viewValue && !!SfePage3Service.minPolTerm){
						if(parseInt(viewValue)<parseInt(SfePage3Service.minPolTerm))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxTerm = function(ngModelValue, viewValue){
					debug("in maxTerm");
					if(!!viewValue && !!SfePage3Service.maxPolTerm){
						if(parseInt(viewValue)>parseInt(SfePage3Service.maxPolTerm))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
				ctrl.$validators.lessThanPPT = function(ngModelValue, viewValue){
					debug("in lessThanPPT");
					debug("scope.ppt: " + scope.ppt);
					if(!!viewValue && !!scope.ppt){
						if(parseInt(viewValue)<parseInt(scope.ppt))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}
			}
	};
}]);

sfePage3Module.directive('validateAnnualPremiumTa',['SfePage3Service','SisFormService', function(SfePage3Service,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			validateAnnualPremium: "="
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium){
						debug("viewValue: " + viewValue + "\tSfePage3Service.minPremium : " + SfePage3Service.minPremium);
						if(!!viewValue && !!SfePage3Service.minPremium){
							if(parseInt(viewValue)<parseInt(SfePage3Service.minPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else {
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium){
						if(!!viewValue && !!SfePage3Service.maxPremium){
							if(parseInt(viewValue)>parseInt(SfePage3Service.maxPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					if(!!scope.validateAnnualPremium && SfePage3Service.startingPoint=="P"){
						debug("viewValue: " + viewValue);
						debug("SfePage3Service.premiumMultipleOf: " + SfePage3Service.premiumMultipleOf);
						if(!!viewValue && !!SfePage3Service.premiumMultipleOf){
							if((parseInt(viewValue)%parseInt(SfePage3Service.premiumMultipleOf))>0)
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
			}
	};
}]);

sfePage3Module.directive('validateBasePremiumTa',['SfePage3Service','SisFormService', function(SfePage3Service,SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			validateBasePremium: "="
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.minVal = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium){
						if(!!viewValue && !!SfePage3Service.minPremium){
							if(parseInt(viewValue)<parseInt(SfePage3Service.minPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
				ctrl.$validators.maxVal = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium){
						if(!!viewValue && !!SfePage3Service.maxPremium){
							if(parseInt(viewValue)>parseInt(SfePage3Service.maxPremium))
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else {
						return true;
					}
				}
				ctrl.$validators.multipleOf = function(ngModelValue, viewValue){
					if(!!scope.validateBasePremium && SfePage3Service.startingPoint=="P"){
						if(!!viewValue && !!SfePage3Service.premiumMultipleOf){
							if((parseInt(viewValue)%parseInt(SfePage3Service.premiumMultipleOf))>0)
								return false;
							else
								return true;
						}
						else {
							return true;
						}
					}
					else{
						return true;
					}
				}
			}
	};
}]);

sfePage3Module.directive('validatePptTa',['SfePage3Service', 'SisFormService', function(SfePage3Service, SisFormService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			pt: "=validatePpt"
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.greaterThanPT = function(ngModelValue, viewValue){
					debug("in greaterThanPT " + viewValue);
					debug("scope.pt: " + scope.pt);
					if(!!viewValue && !!viewValue.PPT && !!scope.pt){
						if((!SisFormService.sisData.sisMainData.PGL_ID==VCP_PGL_ID) && parseInt(viewValue.PPT)>parseInt(scope.pt))
							return false;
						else
							return true;
					}
					else {
						return true;
					}
				}

				ctrl.$validators.selectRequired = function(ngModelValue, viewValue){
					if(!!viewValue && (viewValue instanceof Object) && viewValue!={}){
						var keys = Object.keys(viewValue);
						var selectRequired = true;
						angular.forEach(keys,function(key){
							if(!!viewValue[key] && viewValue[key].length>0 && viewValue[key].substring(0,7)=="Select "){
								if(!!selectRequired)
									selectRequired = false;
								debug("key: " + key + " " + "ngModelValue[key]: " + viewValue[key]);
								debug("setting validity: false");
							}
						});
						return selectRequired;
					}
					else
						return true;
				}
			}
	};
}]);
