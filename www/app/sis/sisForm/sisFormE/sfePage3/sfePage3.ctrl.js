sfePage3Module.controller('SfePage3Ctrl',['$q','$state', '$filter', 'CommonService', 'SisFormService', 'SfePage3Service', 'SfePage3Data', 'PlanName', 'PlanTermDetails', 'PlanCodeList', 'PPTList', 'PPMList', 'SPList', 'PolTermLimits', 'OptionList', 'PremiumMultipleList', 'MinimumSumAssured', 'MaximumSumAssured', 'MinimumPremium', 'MaximumPremium', 'PGL_ID', 'SisTimelineService', function($q, $state, $filter, CommonService, SisFormService, SfePage3Service, SfePage3Data, PlanName, PlanTermDetails, PlanCodeList, PPTList, PPMList, SPList, PolTermLimits, OptionList, PremiumMultipleList, MinimumSumAssured, MaximumSumAssured, MinimumPremium, MaximumPremium, PGL_ID, SisTimelineService){
	"use strict";
	debug("in SfePage3Ctrl");
	SisTimelineService.setActiveTab("otherDetails");
	CommonService.hideLoading();
                                          
                                          
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/
                                          
	var sc = this;

	this.pglId = PGL_ID;
	this.productType = (SisFormService.getProductDetails(true).LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
	this.isSelf = SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED;

	this.sfePage3Data = SfePage3Data;
	this.spList = SPList;
	this.planCodeList = PlanCodeList;
	this.pptList = PPTList;
	this.ppmList = PPMList;
	if(!!OptionList)
		this.optionList = OptionList;

	this.planTermDetails = PlanTermDetails;

	this.minPolTerm = PolTermLimits.MIN;
	this.maxPolTerm = PolTermLimits.MAX;

	SfePage3Service.minPolTerm = this.minPolTerm;
	SfePage3Service.maxPolTerm = this.maxPolTerm;

	this.minSumAssured = MinimumSumAssured;
	this.maxSumAssured = MaximumSumAssured;

	SfePage3Service.minSumAssured = this.minSumAssured;
	SfePage3Service.maxSumAssured = this.maxSumAssured;

	debug("this.minPolTerm: " + this.minPolTerm);
	debug("this.maxPolTerm: " + this.maxPolTerm);

	debug("this.minSumAssured: " + this.minSumAssured);
	debug("this.maxSumAssured: " + this.maxSumAssured);

	debug("this.minPremium: " + this.minPremium);
	debug("this.maxPremium: " + this.maxPremium);

	this.premMultipleOf = null;
	this.sumAssuredMultipleOf = null;

	if(!!this.planCodeList && this.planCodeList.length>0)
		this.planCodeSelected = this.planCodeList[0];
	if(!!this.planCodeList && this.planCodeList.length==2)
		this.planCodeSelected = this.planCodeList[1];

	if(!!this.pptList && this.pptList.length>0)
		this.pptSelected = this.pptList[0];

	if(!!this.ppmList && this.ppmList.length>2)
		this.ppmSelected = this.ppmList[0];
	else if(!!this.ppmList && this.ppmList.length>1)
		this.ppmSelected = this.ppmList[1];

	if(this.pglId=="224" || this.pglId=="225"){
	    this.ppmSelected = $filter('filter')(this.ppmList, {"PPM": SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE=="O" ? "A" : SisFormService.sisData.sisFormBData.PREMIUM_PAY_MODE})[0];
	    this.sfePage3Data.PREMIUM_PAY_MODE = this.ppmSelected.PPM;
	    this.sfePage3Data.PREMIUM_PAY_MODE_DESC = this.ppmSelected.PPM_DISPLAY;
	}

	this.policyTermReadOnly = !!PolTermLimits.READONLY;

	if(!!this.spList && this.spList.length>1){
		this.sfePage3Data.INVT_STRAT_FLAG = this.spList[1].SP || "S";
	}
	else {
		this.sfePage3Data.INVT_STRAT_FLAG = "S";
	}

	this.optionSelected = this.optionList[1];

	this.sfePage3Data.PLAN_NAME = this.sfePage3Data.PLAN_NAME || PlanName;

	if(!!this.sfePage3Data.PLAN_CODE && ((!!this.planCodeList && this.planCodeList.length>0))){
		this.planCodeSelected = $filter('filter')(this.planCodeList, {"PNL_CODE": this.sfePage3Data.PLAN_CODE})[0];
	}

	if(!!this.sfePage3Data.PREMIUM_PAY_MODE && ((!!this.ppmList && this.ppmList.length>0))){
		this.ppmSelected = $filter('filter')(this.ppmList, {"PPM": this.sfePage3Data.PREMIUM_PAY_MODE})[0];
	}

	if(!!this.sfePage3Data.PREMIUM_PAY_TERM && ((!!this.pptList && this.pptList.length>0))){
		this.pptSelected = $filter('filter')(this.pptList, {"PPT": this.sfePage3Data.PREMIUM_PAY_TERM})[0];
		if((this.sfePage3Data.PLAN_CODE=="IRSRP1N1V2")|| (this.sfePage3Data.PLAN_CODE=="IRTRRP1N1") || (this.sfePage3Data.PLAN_CODE.indexOf("SRRP") != -1) || (this.sfePage3Data.PLAN_CODE.indexOf("SRPRP") != -1))
            this.pptSelected = $filter('filter')(this.pptList, {"PPT": "0"})[0];;
		debug("this.pptSelected: " + JSON.stringify(this.pptSelected));
		debug("this.sfePage3Data.PREMIUM_PAY_TERM: " + JSON.stringify(this.sfePage3Data.PREMIUM_PAY_TERM));

	}

	if(!!this.sfePage3Data.PREM_MULTIPLIER && ((!!this.premiumMultipleList && this.premiumMultipleList.length>0))){
		this.premMultSelected = $filter('filter')(this.premiumMultipleList, {"PREM_MULT": this.sfePage3Data.PREM_MULTIPLIER})[0];
		debug("this.pptSelected: " + JSON.stringify(this.premMultSelected));
		debug("this.sfePage3Data.PREMIUM_PAY_TERM: " + JSON.stringify(this.sfePage3Data.PREM_MULTIPLIER));
	}

	if(!!this.sfePage3Data.INPUT_ANNUAL_PREMIUM)
		this.sfePage3Data.INPUT_ANNUAL_PREMIUM = parseInt(this.sfePage3Data.INPUT_ANNUAL_PREMIUM);

	if(!!this.sfePage3Data.SUM_ASSURED)
		this.sfePage3Data.SUM_ASSURED = parseInt(this.sfePage3Data.SUM_ASSURED);
	else if(SisFormService.sisData.oppData.REF_NML_FLAG == 'Y' && !!SisFormService.sisData.oppData.REF_NML_AMT && SisFormService.sisData.oppData.REF_NML_AMT>0)
		this.sfePage3Data.SUM_ASSURED = parseInt(SisFormService.sisData.oppData.REF_NML_AMT);

	if(!!this.sfePage3Data.BASE_PREMIUM)
		this.sfePage3Data.BASE_PREMIUM = parseInt(this.sfePage3Data.BASE_PREMIUM);

	if(!!this.sfePage3Data.POLICY_TERM)
		this.sfePage3Data.POLICY_TERM = parseInt(this.sfePage3Data.POLICY_TERM);
	else
		this.sfePage3Data.POLICY_TERM = parseInt(this.maxPolTerm);

	if(!!this.sfePage3Data.PREMIUM_PAY_TERM)
		this.sfePage3Data.PREMIUM_PAY_TERM = parseInt(this.sfePage3Data.PREMIUM_PAY_TERM);

	SfePage3Service.startingPoint = this.sfePage3Data.INVT_STRAT_FLAG;

	this.minPremium = null;
	this.maxPremium = null;

	SfePage3Service.minPremium = null;
	SfePage3Service.maxPremium = null;

	this.previousPPM = null;

	this.onPlanCodeChange = function(){
		if(!!this.planCodeSelected && !!this.planCodeSelected.PNL_CODE){

			this.sfePage3Data.PLAN_CODE = this.planCodeSelected.PNL_CODE;

			this.minPolTerm = SfePage3Service.getMinPolTerm(this.planCodeSelected.PNL_CODE, this.pglId);
			this.maxPolTerm = SfePage3Service.getMaxPolTerm(this.planCodeSelected.PNL_CODE, this.pglId);

			this.minSumAssured = SisFormService.getMinSumAssured(this.pglId, true, this.planCodeList, this.planCodeSelected.PNL_CODE);
			SisFormService.minSumAssured = this.minSumAssured;
			this.maxSumAssured = SisFormService.getMaxSumAssured(this.pglId, true, this.planCodeList, this.planCodeSelected.PNL_CODE);
			SisFormService.maxSumAssured = this.maxSumAssured;

			this.minPremium = null;
			this.maxPremium = null;
			this.premMultipleOf = null;
			this.sumAssuredMultipleOf = SfePage3Service.getSumAssuredMultipleOf(this.planCodeList, this.planCodeSelected.PNL_CODE, true);

			debug("this.minPolTerm: " + this.minPolTerm);
			debug("this.maxPolTerm: " + this.maxPolTerm);

			if(!!this.pglId){
				if(parseInt(this.minPolTerm)==parseInt(this.maxPolTerm)){
					this.sfePage3Data.POLICY_TERM = parseInt(this.minPolTerm);
					if(sc.productType!="U"){
						this.sfePage3Data.PREMIUM_PAY_TERM = parseInt(SfePage3Service.getPremiumPayTermFixed(this.sfePage3Data.PLAN_CODE));
					}
				}
				else if((this.sfePage3Data.POLICY_TERM<parseInt(this.minPolTerm)) || (this.sfePage3Data.POLICY_TERM>parseInt(this.maxPolTerm))){
					this.sfePage3Data.POLICY_TERM = null;
				}

				this.onPolicyTermChange(true);

				if(this.sfePage3Data.INVT_STRAT_FLAG=="S"){
					if((parseInt(this.sfePage3Data.SUM_ASSURED)<parseInt(this.minSumAssured)) || (this.sfePage3Data.SUM_ASSURED>parseInt(this.maxSumAssured)))
						this.sfePage3Data.SUM_ASSURED = null;
					this.onSumAssuredChange();
				}
			}
		}
	};

	this.onPolicyTermChange = function(fromCtrl){
		debug("onPolicyTermChange");
		if(!!this.sfePage3Data.POLICY_TERM){
			if((parseInt(this.sfePage3Data.POLICY_TERM)>=parseInt(this.minPolTerm)) && (this.sfePage3Data.POLICY_TERM<=parseInt(this.maxPolTerm))){
				if(!fromCtrl){
					if(!this.pptList || (this.pptList.length==0))
						this.sfePage3Data.PREMIUM_PAY_TERM = parseInt(this.sfePage3Data.POLICY_TERM);
				}
				if(!!this.planCodeSelected.PNL_CODE && this.pglId!=MIP_PGL_ID){ //not for MIP
					SisFormService.getPremiumMultipleList(this.pglId, true, this.sfePage3Data.POLICY_TERM,this.planCodeList,this.planCodeSelected.PNL_CODE, this.sfePage3Data.SUM_ASSURED).then(
						function(res){
							if(res instanceof Array){
								sc.premiumMultipleList = res;
								sc.premMultSelected = sc.premiumMultipleList[0];
							}
							else{
								sc.sfePage3Data.PREM_MULTIPLIER = res;
								SfePage3Service.premiumMultiple = res;
								debug("sc.sfePage3Data.PREM_MULTIPLIER: " + sc.sfePage3Data.PREM_MULTIPLIER);
								sc.onSumAssuredChange();
							}
						}
					);
				}
			}
		}
	};

	this.onSumAssuredChange = function(onInput){
	    debug("REF_NML_AMT : " + parseInt(SisFormService.sisData.oppData.REF_NML_AMT));
	    if(!!onInput && SisFormService.sisData.oppData.REF_NML_FLAG == 'Y' && !!SisFormService.sisData.oppData.REF_NML_AMT && parseInt(SisFormService.sisData.oppData.REF_NML_AMT)>0 && this.sfePage3Data.SUM_ASSURED > parseInt(SisFormService.sisData.oppData.REF_NML_AMT))
            navigator.notification.alert('Selected cover is greater than Non Medical Limit hence medical requirements will be applicable',null,"SIS","OK");
		SisFormService.getPremiumMultipleList(this.pglId, true, this.sfePage3Data.POLICY_TERM,this.planCodeList,this.planCodeSelected.PNL_CODE, this.sfePage3Data.SUM_ASSURED).then(
			function(res){
				sc.sfePage3Data.PREM_MULTIPLIER = res;
				SfePage3Service.premiumMultiple = res;
				if((!!sc.sfePage3Data.SUM_ASSURED) && (!!sc.sfePage3Data.PREM_MULTIPLIER)){
					sc.sfePage3Data.INPUT_ANNUAL_PREMIUM = SisFormService.calculateAnnualPremium(sc.sfePage3Data.SUM_ASSURED, sc.sfePage3Data.PREM_MULTIPLIER, true);
					debug("sc.sfePage3Data.INPUT_ANNUAL_PREMIUM:: " + sc.sfePage3Data.INPUT_ANNUAL_PREMIUM);
				}
				else
					sc.sfePage3Data.INPUT_ANNUAL_PREMIUM = null;

				if((!!sc.sfePage3Data.INPUT_ANNUAL_PREMIUM) && (!!sc.sfePage3Data.PREMIUM_PAY_MODE)){
					sc.sfePage3Data.BASE_PREMIUM = SisFormService.calculateBasePremium(sc.sfePage3Data.INPUT_ANNUAL_PREMIUM, sc.sfePage3Data.PREMIUM_PAY_MODE);
				}
				else
					sc.sfePage3Data.BASE_PREMIUM = null;
			}
		);
	};

	this.onPPMChange = function(){
		this.sfePage3Data.PREMIUM_PAY_MODE = this.ppmSelected.PPM;
		this.sfePage3Data.PREMIUM_PAY_MODE_DESC = this.ppmSelected.PPM_DISPLAY;
		if((!this.previousPPM && !!this.ppmSelected) || (this.previousPPM=="O" && this.ppmSelected.PPM!="O") || (this.previousPPM!="O" && this.ppmSelected.PPM=="O")){
			this.pptList = SisFormService.getPPTList(this.pglId, this.ppmSelected.PPM);
			if(!!this.pptList){
				this.pptSelected = this.pptList[0];
				this.onPPTChange();
			}
			else if(this.pglId!=SGP_PGL_ID){
				this.sfePage3Data.PLAN_CODE = this.planCodeList[1]; // first value is select
				this.onPlanCodeChange();
			}
		}
		if((!!this.sfePage3Data.INPUT_ANNUAL_PREMIUM) && (!!this.sfePage3Data.PREMIUM_PAY_MODE)){
			this.sfePage3Data.BASE_PREMIUM = SisFormService.calculateBasePremium(this.sfePage3Data.INPUT_ANNUAL_PREMIUM, this.sfePage3Data.PREMIUM_PAY_MODE);
		}
		else{
			this.sfePage3Data.BASE_PREMIUM = null;
		}
		this.previousPPM = this.ppmSelected.PPM;
	};

	this.onPPTChange = function(){
		debug("this.pptSelected.PPT: " + JSON.stringify(this.pptSelected) + " " + this.pptSelected.PPT);
		this.sfePage3Data.PREMIUM_PAY_TERM = parseInt(this.pptSelected.PPT);
		debug("in ppt onchange " + this.sfePage3Data.PREMIUM_PAY_TERM);
		debug("condition " + ((this.pglId==SR_PGL_ID || this.pglId==SRP_PGL_ID) && (!!this.optionSelected && !!this.optionSelected.OPTION)));
		debug("this.optionSelected1 : " + JSON.stringify(this.optionSelected));
		if(!!this.policyTermReadOnly){ // Policy term is fixed
			this.sisFormBData.POLICY_TERM = (!!this.pptSelected.PPT)?parseInt(SisFormService.calculatePolicyTerm(this.pptSelected.PPT, this.pglId, true)):null;
		}
		if(!this.pptSelected.PPT){
			//set plan code to null (default value) if ppt is null
			this.planCodeSelected = ($filter('filter')(this.planCodeList, {"PNL_CODE": null})[0]);
			this.onPlanCodeChange();
		}
		else if((this.pglId==SR_PGL_ID || this.pglId==SRP_PGL_ID) && (!!this.optionSelected && !!this.optionSelected.OPTION)){
			// if plan is (SR or SRP and Option is selected(not null))
			debug("this.optionSelected : " + JSON.stringify(this.optionSelected));
			this.planCodeSelected = SisFormService.getPlanCode(this.planCodeList, this.pptSelected.PPT, (!!this.optionSelected)?this.optionSelected.OPTION:null, this.sfePage3Data.BASE_PREMIUM, null, this.pglId, true);
			this.onPlanCodeChange();
		}
	};

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("sisForm.sfePage2");
	};

	this.onNext = function(isPristine){
	    if((this.sfePage3Data.PLAN_CODE=="IRSRP1N1V2")|| (this.sfePage3Data.PLAN_CODE=="IRTRRP1N1") || (this.sfePage3Data.PLAN_CODE.indexOf("SRRP") != -1) || (this.sfePage3Data.PLAN_CODE.indexOf("SRPRP") != -1))
    		this.sfePage3Data.PREMIUM_PAY_TERM = this.sfePage3Data.POLICY_TERM;
		SfePage3Service.onNext(this.sfePage3Data);
	};

}]);
sfePage3Module.service('SfePage3Service',['$q', '$filter', 'CommonService', 'SisFormService', 'SisFormEService', '$state', 'SisTimelineService', function($q, $filter, CommonService, SisFormService, SisFormEService, $state, SisTimelineService){
	"use strict";
	debug("in SfePage3Service");
	var sc = this;

	this.pglId = null;
	this.sfePage3Data = this.sfePage3Data || {};
	this.minPolTerm = null;
	this.maxPolTerm = null;
	this.premiumMultiple = null;

	this.setSfePage3Data = function(sfePage3Data){
		debug("Entering setSfePage3Data");
		this.sfePage3Data = sfePage3Data;
	};

	this.getMinPolTerm = function(plancode, pglId){
		this.minPolTerm = SisFormService.getMinPolTerm(plancode, pglId, true);
		return this.minPolTerm;
	};

	this.getMaxPolTerm = function(plancode, pglId){
		this.maxPolTerm = SisFormService.getMaxPolTerm(plancode, pglId, true);
		return this.maxPolTerm;
	};

	this.getMinPolTermValue = function(){
		return this.minPolTerm;
	};
	this.getMaxPolTermValue = function(){
		return this.maxPolTerm;
	};

	this.getSumAssuredMultipleOf = function(planCodeList, plancode){
		debug("fetching sa multiple of");
		sc.saMultipleOf = SisFormService.getSumAssuredMultipleOf(planCodeList, plancode, true);
		return sc.saMultipleOf;
	};

	this.getPremiumPayTermFixed = function(plancode){
		return $filter('filter')(SisFormService.getProductDetails(true).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].PPT;
	};

	this.onNext = function(sfePage3Data){
		debug(".....on next.....");
		if(!SisFormService.sisData.sisFormEData)
			SisFormService.sisData.sisFormEData = {};
		Object.assign(SisFormService.sisData.sisFormEData, sfePage3Data);
		SisFormEService.insertDataInSisScreenETable().then(
			function(res){
				if(!!res){
					SisTimelineService.isOtherDetailsTabComplete = true;
					$state.go("sisForm.sfePage4");
				}
				else{
					CommonService.hideLoading();
					navigator.notification.alert("Cannot navigate to the next page.",null,"SIS","OK");
				}
			}
		);
	};

}]);
