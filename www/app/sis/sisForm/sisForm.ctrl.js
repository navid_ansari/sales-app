sisFormModule.controller('SisFormCtrl',['$state', 'CommonService', 'panels', function($state, CommonService, panels){
	"use strict";

	debug("in sis form controller");

	this.openMenu = function(){
		CommonService.openMenu();
	};
}]);
sisFormModule.service('SisFormService',['$q', 'CommonService', 'LoginService', '$state', 'SisTimelineService', '$filter', function($q, CommonService, LoginService, $state, SisTimelineService, $filter){
	"use strict";
	var sf = this;

	this.productData = {};
	this.taProductData = {};

	this.getProductDetails = function(isTermAttached){
		var objKey = ((!!isTermAttached)?("taProductData"):("productData"));
		return this[objKey];
	};

	this.setExtraData = function(solType, prodName){
		this.SOL_TYPE = solType;
		this.PROD_NAME = prodName;
	};

	this.loadSISData = function(agentCd, pglId, leadId, fhrId, sisId, comboId, insSeqNo, oppId){
		debug("in load sis data");
		var dfd = $q.defer();
		if(!this.sisData){
			this.sisData = {};

			this.sisData.COMBO_ID = comboId || null;
			this.sisData.INS_SEQ_NO = insSeqNo || null;
			this.sisData.OPP_ID = oppId || null;

			this.sisData.sisMainData = {};
			this.sisData.sisFormAData_eKyc = {};
			this.sisData.sisFormAData = {};
			this.sisData.sisFormBData = {};
			this.sisData.sisFormCData = {};
			this.sisData.sisFormDData = {};
			this.sisData.sisEKycData = {};
			this.sisData.sisFormEData = {};
			this.sisData.sisFormFData = {};
			this.sisData.oppData = {};

			this.sisData.sisOutput = {};
			this.sisData.sisTermOutput = {};

			this.sisData.sisMainData.PGL_ID = pglId || null;
			this.sisData.sisMainData.SIS_ID = sisId || CommonService.getRandomNumber();
			this.sisData.sisMainData.AGENT_CD = agentCd || null;
			this.sisData.sisMainData.FHR_ID = fhrId || null;
			this.sisData.sisMainData.LEAD_ID = leadId || null;

			this.sisData.sisFormEData.LEAD_ID = leadId || null;

			this.sisData.oppData.OPPORTUNITY_ID = oppId || null;

			this.sisData.sisMainData.STANDARD_OR_ALTERNATE_SIS = "S";

			this.sisData.sisOutput.SIS_SAVED = false;
			this.sisData.sisOutput.SIS_SIGNED = false;

			this.sisData.sisEKycData.AGENT_CD = agentCd || null;
			this.sisData.sisFormAData.AGENT_CD = agentCd || null;
			this.sisData.sisFormBData.AGENT_CD = agentCd || null;
			this.sisData.sisFormCData.AGENT_CD = agentCd || null;
			this.sisData.sisFormDData.AGENT_CD = agentCd || null;
			this.sisData.sisFormEData.AGENT_CD = agentCd || null;
			this.sisData.sisFormFData.AGENT_CD = agentCd || null;

			this.sisData.sisEKycData.SIS_ID = this.sisData.sisMainData.SIS_ID;
			this.sisData.sisFormAData.SIS_ID = this.sisData.sisMainData.SIS_ID;
			this.sisData.sisFormBData.SIS_ID = this.sisData.sisMainData.SIS_ID;
			this.sisData.sisFormCData.SIS_ID = this.sisData.sisMainData.SIS_ID;
			this.sisData.sisFormDData.SIS_ID = this.sisData.sisMainData.SIS_ID;
			this.sisData.sisFormFData.LEAD_ID = this.sisData.sisMainData.LEAD_ID;
			this.sisData.sisFormFData.FHR_ID = this.sisData.sisMainData.FHR_ID;
			this.sisData.sisFormFData.SIS_ID = this.sisData.sisMainData.SIS_ID;

			this.sisData.ekycDisabled = false;
			this.sisData.disableInsDtlsView = null;
			this.sisData.disablePropDtlsView = null;
			this.sisData.insConfirmed = null;
			this.sisData.propConfirmed = null;

		}
		if(!!sf.sisData.sisEKycData.insured)
			sf.sisData.sisEKycData = sf.sisData.sisEKycData;
		else {
			sf.sisData.sisEKycData = {"insured": {CUST_TYPE: INSURED_CUST_TYPE}, "proposer": {CUST_TYPE: PROPOSER_CUST_TYPE}};
		}

		this.loadOpportunityData(oppId, leadId, fhrId, agentCd).then(
			function(savedOpportunityData){
				if(!!savedOpportunityData)
					Object.assign(sf.sisData.oppData, savedOpportunityData.oppData);
				if(!!sisId){
					/*sf.getEKYCData(agentCd, leadId).then(
						function(sisEKycDataResp){
							Object.assign(sf.sisData.sisEKycData, sisEKycDataResp || null);
							debug("sf.sisData.sisEKycData: " + JSON.stringify(sf.sisData.sisEKycData));
							return sisEKycDataResp;
						}
					).then(
						function(sisEKycDataResp){*/
							var refFhrId = sf.sisData.oppData.REF_FHRID;
							//if(!!sisEKycDataResp){
								SisTimelineService.isEKYCTabComplete = true;
							//}
							sf.loadSavedSISData(agentCd, sisId, oppId, fhrId, leadId, refFhrId).then(
								function(savedSISDataResp) {
									Object.assign(sf.sisData.sisMainData, savedSISDataResp.sisMainData);
									Object.assign(sf.sisData.sisFormAData_eKyc, savedSISDataResp.sisFormAData_eKyc);
									Object.assign(sf.sisData.sisFormAData, savedSISDataResp.sisFormAData);
									Object.assign(sf.sisData.sisFormBData, savedSISDataResp.sisFormBData);
									Object.assign(sf.sisData.sisFormCData, savedSISDataResp.sisFormCData);
									Object.assign(sf.sisData.sisFormDData, savedSISDataResp.sisFormDData);
									Object.assign(sf.sisData.sisFormEData, savedSISDataResp.sisFormEData);
									Object.assign(sf.sisData.sisFormFData, savedSISDataResp.sisFormFData);
									debug("sf.sisData: " + JSON.stringify(sf.sisData));

									if(!sf.sisData.sisFormAData || !sf.sisData.sisFormAData.PROPOSER_IS_INSURED){
										if(!!fhrId && BUSINESS_TYPE!='IndusSolution'){
											debug("in fhr");
											sf.loadLeadSISData(agentCd, leadId).then(
												function(leadSISDataResp){
													Object.assign(sf.sisData.sisMainData, leadSISDataResp.sisMainData);
													Object.assign(sf.sisData.sisFormAData, leadSISDataResp.sisFormAData);
													Object.assign(sf.sisData.sisFormBData, leadSISDataResp.sisFormBData);
													Object.assign(sf.sisData.sisFormCData, leadSISDataResp.sisFormCData);
													Object.assign(sf.sisData.sisFormDData, leadSISDataResp.sisFormDData);
													sf.loadFHRSISData(agentCd, fhrId, leadSISDataResp).then(
														function(fhrSISDataResp) {
															if(!!fhrSISDataResp){
																Object.assign(sf.sisData.sisMainData, fhrSISDataResp.sisMainData);
																Object.assign(sf.sisData.sisFormAData, fhrSISDataResp.sisFormAData);
																Object.assign(sf.sisData.sisFormBData, fhrSISDataResp.sisFormBData);
																Object.assign(sf.sisData.sisFormCData, fhrSISDataResp.sisFormCData);
																Object.assign(sf.sisData.sisFormDData, fhrSISDataResp.sisFormDData);
															}
															dfd.resolve(sf.sisData);
														}
													);
												}
											);
										}
										else if(!!fhrId && BUSINESS_TYPE=='IndusSolution'){
											debug("in ff");
											sf.loadLeadSISData(agentCd, leadId).then(
												function(leadSISDataResp){
													Object.assign(sf.sisData.sisMainData, leadSISDataResp.sisMainData);
													Object.assign(sf.sisData.sisFormAData, leadSISDataResp.sisFormAData);
													Object.assign(sf.sisData.sisFormBData, leadSISDataResp.sisFormBData);
													Object.assign(sf.sisData.sisFormCData, leadSISDataResp.sisFormCData);
													Object.assign(sf.sisData.sisFormDData, leadSISDataResp.sisFormDData);
													sf.loadFactFinderSISData(agentCd, fhrId, leadSISDataResp).then(
														function(ffSISDataResp) {
															if(!!ffSISDataResp){
																Object.assign(sf.sisData.sisMainData, ffSISDataResp.sisMainData);
																Object.assign(sf.sisData.sisFormAData, ffSISDataResp.sisFormAData);
																Object.assign(sf.sisData.sisFormBData, ffSISDataResp.sisFormBData);
																Object.assign(sf.sisData.sisFormCData, ffSISDataResp.sisFormCData);
																Object.assign(sf.sisData.sisFormDData, ffSISDataResp.sisFormDData);
															}
															debug("sf.sisData: " + JSON.stringify(sf.sisData));
															dfd.resolve(sf.sisData);
														}
													);
												}
											);
										}
										else{
											sf.loadLeadSISData(agentCd, leadId).then(
												function(leadSISDataResp){
													Object.assign(sf.sisData.sisMainData, leadSISDataResp.sisMainData);
													Object.assign(sf.sisData.sisFormAData, leadSISDataResp.sisFormAData);
													Object.assign(sf.sisData.sisFormBData, leadSISDataResp.sisFormBData);
													Object.assign(sf.sisData.sisFormCData, leadSISDataResp.sisFormCData);
													Object.assign(sf.sisData.sisFormDData, leadSISDataResp.sisFormDData);
													if(!!leadSISDataResp){
														Object.assign(sf.sisData.sisMainData, leadSISDataResp.sisMainData);
														Object.assign(sf.sisData.sisFormAData, leadSISDataResp.sisFormAData);
														Object.assign(sf.sisData.sisFormBData, leadSISDataResp.sisFormBData);
														Object.assign(sf.sisData.sisFormCData, leadSISDataResp.sisFormCData);
														Object.assign(sf.sisData.sisFormDData, leadSISDataResp.sisFormDData);
													}
													dfd.resolve(sf.sisData);
												}
											);
											dfd.resolve(sf.sisData);
										}
									}
									else{
										sf.sisData.ekycDisabled = true;
										dfd.resolve(sf.sisData);
									}
								}
							);
						//}
					//);
				}
				else if(!!comboId){
					sf.loadComboSISData(agentCd, comboId, insSeqNo).then(
						function(comboSISDataResp) {
							if(!!comboSISDataResp){
								Object.assign(sf.sisData.sisMainData, comboSISDataResp.sisMainData);
								Object.assign(sf.sisData.sisFormAData, comboSISDataResp.sisFormAData);
								Object.assign(sf.sisData.sisFormBData, comboSISDataResp.sisFormBData);
								Object.assign(sf.sisData.sisFormCData, comboSISDataResp.sisFormCData);
								Object.assign(sf.sisData.sisFormDData, comboSISDataResp.sisFormDData);
							}
							dfd.resolve(sf.sisData);
						}
					);
				}
				else if(!!fhrId && BUSINESS_TYPE=='IndusSolution'){
					debug("in ff");
					sf.loadLeadSISData(agentCd, leadId).then(
						function(leadSISDataResp){
					debug("loadFactFinderSISData: " + fhrId);
							sf.loadFactFinderSISData(agentCd, fhrId, leadSISDataResp).then(
								function(ffSISDataResp) {
									if(!!ffSISDataResp){
										Object.assign(sf.sisData.sisMainData, ffSISDataResp.sisMainData);
										Object.assign(sf.sisData.sisFormAData, ffSISDataResp.sisFormAData);
										Object.assign(sf.sisData.sisFormBData, ffSISDataResp.sisFormBData);
										Object.assign(sf.sisData.sisFormCData, ffSISDataResp.sisFormCData);
										Object.assign(sf.sisData.sisFormDData, ffSISDataResp.sisFormDData);
									}
									debug("sf.sisData: " + JSON.stringify(sf.sisData));
									dfd.resolve(sf.sisData);
								}
							);
						}
					);
				}
				else if(!!fhrId && BUSINESS_TYPE!='IndusSolution'){
					sf.loadFHRSISData(agentCd, fhrId).then(
						function(fhrSISDataResp) {
							if(!!fhrSISDataResp){
								Object.assign(sf.sisData.sisMainData, fhrSISDataResp.sisMainData);
								Object.assign(sf.sisData.sisFormAData, fhrSISDataResp.sisFormAData);
								Object.assign(sf.sisData.sisFormBData, fhrSISDataResp.sisFormBData);
								Object.assign(sf.sisData.sisFormCData, fhrSISDataResp.sisFormCData);
								Object.assign(sf.sisData.sisFormDData, fhrSISDataResp.sisFormDData);
							}
							dfd.resolve(sf.sisData);
						}
					);
				}
				else if(!!leadId){
					sf.loadLeadSISData(agentCd, leadId).then(
						function(leadSISDataResp) {
							if(!!leadSISDataResp){
								Object.assign(sf.sisData.sisMainData, leadSISDataResp.sisMainData);
								Object.assign(sf.sisData.sisFormAData, leadSISDataResp.sisFormAData);
								Object.assign(sf.sisData.sisFormBData, leadSISDataResp.sisFormBData);
								Object.assign(sf.sisData.sisFormCData, leadSISDataResp.sisFormCData);
								Object.assign(sf.sisData.sisFormDData, leadSISDataResp.sisFormDData);
							}
							dfd.resolve(sf.sisData);
						}
					);
				}
				else{
					debug("resolving: " + JSON.stringify(sf.sisData));
							dfd.resolve(sf.sisData);
				}
			}
		);
		return dfd.promise;
	};

	this.loadLeadSISData = function(agentCd, leadId){
		var dfd = $q.defer();
		var sisData = {};
		CommonService.selectRecords(db, "lp_mylead", true, null, {"agent_cd": agentCd, "ipad_lead_id": leadId}).then(
			function(res){
				if(!!res && res.rows.length>0){
					sisData.sisFormAData = {};
					var nameSplit = (res.rows.item(0).LEAD_NAME).split(" ");
					debug("nameSplit: " + nameSplit);
					sisData.sisFormAData.PROPOSER_FIRST_NAME = nameSplit[0];
					if(nameSplit.length>2){
						sisData.sisFormAData.PROPOSER_MIDDLE_NAME = "";
						for(var i=1; i<(nameSplit.length-1); i++){
							sisData.sisFormAData.PROPOSER_MIDDLE_NAME += nameSplit[i];
							if(i<(nameSplit.length-2))
								sisData.sisFormAData.PROPOSER_MIDDLE_NAME += " ";
						}
						sisData.sisFormAData.PROPOSER_LAST_NAME = nameSplit[(nameSplit.length - 1)];
					}
					else{
						sisData.sisFormAData.PROPOSER_MIDDLE_NAME = null;
						sisData.sisFormAData.PROPOSER_LAST_NAME = nameSplit[1];
					}
					sisData.sisFormAData.PROPOSER_GENDER = (res.rows.item(0).GENDER_CD=="1")?"M":"F";
					sisData.sisFormAData.PROPOSER_EMAIL = res.rows.item(0).EMAIL_ID;
					debug("sisData.sisFormAData.PROPOSER_EMAIL: " + sisData.sisFormAData.PROPOSER_EMAIL);
					sisData.sisFormAData.PROPOSER_MOBILE = res.rows.item(0).MOBILE_NO;
					sisData.sisFormAData.PROPOSER_DOB = res.rows.item(0).BIRTH_DATE;
					dfd.resolve(sisData);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.loadFactFinderSISData = function(agentCd, fhrId, sisData_lead){
		debug("in loadFactFinderSISData");
		var dfd = $q.defer();
		var sisData = sisData_lead || {};
		CommonService.selectRecords(db, "lp_fhr_ffna", true, null, {"agent_cd": agentCd, "fhr_id": fhrId}).then(
			function(res){
				if(!!res && res.rows.length>0){
					debug('sisFhrData selectRecords :: ' + res.rows.length);
					sisData.sisFormAData = sisData.sisFormAData || {};
					sisData.sisFormAData.PROPOSER_TITLE = res.rows.item(0).CONSUMER_TITLE;
					sisData.sisFormAData.PROPOSER_FIRST_NAME = res.rows.item(0).CONSUMER_FIRST_NAME;
					sisData.sisFormAData.PROPOSER_MIDDLE_NAME = res.rows.item(0).CONSUMER_MIDDLE_NAME;
					sisData.sisFormAData.PROPOSER_LAST_NAME = res.rows.item(0).CONSUMER_LAST_NAME;
					sisData.sisFormAData.PROPOSER_MOBILE = res.rows.item(0).MOBILE_NO;
					sisData.sisFormAData.PROPOSER_DOB = res.rows.item(0).CONSUMER_BIRTH_DATE;
					CommonService.selectRecords(db, "lp_fhr_product_rcmnd", true, null, {"agent_cd": agentCd, "fhr_id": fhrId}).then(
						function(fhrRecRes){
							if(!!fhrRecRes && fhrRecRes.rows.length>0){
								sisData.sisFormCData = sisData.sisFormCData || {};
								sisData.sisFormCData.INVEST_OPT = fhrRecRes.rows.item(0).RCMND_INVEST_OPT;
								sisData.sisFormCData.FUND_CODE_FROM = fhrRecRes.rows.item(0).RCMND_FUND_CODE_FROM;
								sisData.sisFormCData.FUND_CODE_TO = fhrRecRes.rows.item(0).RCMND_FUND_CODE_TO;
								sisData.sisFormCData.FUND1_CODE = fhrRecRes.rows.item(0).RCMND_FUND1_CODE;
								sisData.sisFormCData.FUND1_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND1_PERCENT;
								sisData.sisFormCData.FUND2_CODE = fhrRecRes.rows.item(0).RCMND_FUND2_CODE;
								sisData.sisFormCData.FUND2_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND2_PERCENT;
								sisData.sisFormCData.FUND3_CODE = fhrRecRes.rows.item(0).RCMND_FUND3_CODE;
								sisData.sisFormCData.FUND3_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND3_PERCENT;
								sisData.sisFormCData.FUND4_CODE = fhrRecRes.rows.item(0).RCMND_FUND4_CODE;
								sisData.sisFormCData.FUND4_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND4_PERCENT;
								sisData.sisFormCData.FUND5_CODE = fhrRecRes.rows.item(0).RCMND_FUND5_CODE;
								sisData.sisFormCData.FUND5_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND5_PERCENT;
								sisData.sisFormCData.FUND6_CODE = fhrRecRes.rows.item(0).RCMND_FUND6_CODE;
								sisData.sisFormCData.FUND6_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND6_PERCENT;
								sisData.sisFormCData.FUND7_CODE = fhrRecRes.rows.item(0).RCMND_FUND7_CODE;
								sisData.sisFormCData.FUND7_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND7_PERCENT;
								sisData.sisFormCData.FUND8_CODE = fhrRecRes.rows.item(0).RCMND_FUND8_CODE;
								sisData.sisFormCData.FUND8_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND8_PERCENT;
								sisData.sisFormCData.FUND9_CODE = fhrRecRes.rows.item(0).RCMND_FUND9_CODE;
								sisData.sisFormCData.FUND9_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND9_PERCENT;
								sisData.sisFormCData.FUND10_CODE = fhrRecRes.rows.item(0).RCMND_FUND10_CODE;
								sisData.sisFormCData.FUND10_PERCENT = fhrRecRes.rows.item(0).RCMND_FUND10_PERCENT;
							}
							debug('sisFhrData: ' + JSON.stringify(sisData));
							dfd.resolve(sisData);
						}
					);
				}
				else{
					debug('sisFhrData selectRecords ::: ' + res.rows.length);
					dfd.resolve(sisData);
				}
			}
		);
		return dfd.promise;
	};

	this.loadFHRSISData = function(agentCd, fhrId, sisData_lead){
		var dfd = $q.defer();
		var sisData = sisData_lead || {};
		CommonService.selectRecords(db, "lp_fhr_personal_inf_scrn_a", true, null, {"agent_cd": agentCd, "fhr_id": fhrId}).then(
			function(res){
				if(!!res && res.rows.length>0){
					sisData.sisFormAData = sisData.sisFormAData || {};
					//sisData.sisFormAData.PROPOSER_TITLE = res.rows.item(0).TITLE;
					sisData.sisFormAData.PROPOSER_FIRST_NAME = res.rows.item(0).FIRST_NAME;
					sisData.sisFormAData.PROPOSER_MIDDLE_NAME = res.rows.item(0).MIDDLE_NAME;
					sisData.sisFormAData.PROPOSER_LAST_NAME = res.rows.item(0).LAST_NAME;
					sisData.sisFormAData.PROPOSER_EMAIL = res.rows.item(0).EMAIL_ID;
					sisData.sisFormAData.PROPOSER_MOBILE = res.rows.item(0).MOBILE_NO;
					sisData.sisFormAData.PROPOSER_DOB = res.rows.item(0).BIRTH_DATE;
					dfd.resolve(sisData);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.loadComboSISData = function(agentCd, comboId, insSeqNo){
		var dfd = $q.defer();
		var sisData = {};
		CommonService.selectRecords(db, "lp_combo_dtls", true, null, {"agent_cd": agentCd, "combo_id": comboId, "ins_seq_no": insSeqNo}).then(
			function(comboDtlsRes){
				if(!!comboDtlsRes && comboDtlsRes.rows.length>0){

					sisData.sisMainData = {};
					sisData.sisFormAData = {};

					sisData.sisMainData.ANNUAL_PREMIUM = comboDtlsRes.ANNUAL_PREMIUM;

					sisData.sisFormAData.PURCHASING_INSURANCE_FOR_CODE = comboDtlsRes.PURCHASING_INSURANCE_FOR_CODE;
					sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE = comboDtlsRes.INSURANCE_BUYING_FOR_CODE;
					sisData.sisFormAData.INSURED_TITLE = comboDtlsRes.INSURED_TITLE;
					sisData.sisFormAData.INSURED_FIRST_NAME = comboDtlsRes.INSURED_FIRST_NAME;
					sisData.sisFormAData.INSURED_MIDDLE = comboDtlsRes.INSURED_MIDDLE_NAME;
					sisData.sisFormAData.INSURED_OCCU_CODE = comboDtlsRes.INSURED_OCCU_CODE;
					sisData.sisFormAData.INSURED_DOB = comboDtlsRes.INSURED_DOB;
					sisData.sisFormAData.INSURED_GENDER = comboDtlsRes.INSURED_GENDER;
					sisData.sisFormAData.INSURED_MOBILE = comboDtlsRes.INSURED_MOBILE;
					sisData.sisFormAData.INSURED_EMAIL = comboDtlsRes.INSURED_EMAIL;
					sisData.sisFormAData.ISSMOKER = comboDtlsRes.IS_SMOKER;
					sisData.sisFormAData.AGEPROF_DOC_ID = comboDtlsRes.AGE_PROOF_DOC_ID;
					sisData.sisFormAData.PROPOSER_TITLE = comboDtlsRes.PROPOSER_TITLE;
					sisData.sisFormAData.PROPOSER_FIRST_NAME = comboDtlsRes.PROPOSER_FIRST_NAME;
					sisData.sisFormAData.PROPOSER_MIDDLE_NAME = comboDtlsRes.PROPOSER_MIDDLE_NAME;
					sisData.sisFormAData.PROPOSER_OCCU_CODE = comboDtlsRes.PROPOSER_OCCU_CODE;
					sisData.sisFormAData.PROPOSER_DOB = comboDtlsRes.PROPOSER_DOB;
					sisData.sisFormAData.PROPOSER_GENDER = comboDtlsRes.PROPOSER_GENDER;
					sisData.sisFormAData.PROPOSER_MOBILE = comboDtlsRes.PROPOSER_MOBILE;
					sisData.sisFormAData.PROPOSER_EMAIL = comboDtlsRes.PROPOSER_EMAIL;

					sisData.sisFormBData.PLAN_CODE = comboDtlsRes.PLAN_CODE;
					sisData.sisFormBData.PREMIUM_PAY_MODE = comboDtlsRes.PREMIUM_PAY_MODE;
					sisData.sisFormBData.POLICY_TERM = comboDtlsRes.POLICY_TERM;
					sisData.sisFormBData.PREMIUM_PAY_TERM = comboDtlsRes.PREMIUM_PAY_TERM;
					sisData.sisFormBData.SUM_ASSURED = comboDtlsRes.SUM_ASSURED;
					sisData.sisFormBData.PREM_MULTIPLIER = comboDtlsRes.PREM_MULTIPLIER;
					sisData.sisFormBData.INVT_STRAT_FLAG = comboDtlsRes.INVT_STRAT_FLAG;

					sisData.sisFormCData.INVEST_OPT = comboDtlsRes.INVEST_OPT;
					sisData.sisFormCData.FUND_CODE_FROM = comboDtlsRes.FUND_CODE_FROM;
					sisData.sisFormCData.FUND_CODE_TO = comboDtlsRes.FUND_CODE_TO;
					sisData.sisFormCData.FUND1_CODE = comboDtlsRes.FUND1_CODE;
					sisData.sisFormCData.FUND1_PERCENT = comboDtlsRes.FUND1_PERCENT;
					sisData.sisFormCData.FUND2_CODE = comboDtlsRes.FUND2_CODE;
					sisData.sisFormCData.FUND2_PERCENT = comboDtlsRes.FUND2_PERCENT;
					sisData.sisFormCData.FUND3_CODE = comboDtlsRes.FUND3_CODE;
					sisData.sisFormCData.FUND3_PERCENT = comboDtlsRes.FUND3_PERCENT;
					sisData.sisFormCData.FUND4_CODE = comboDtlsRes.FUND4_CODE;
					sisData.sisFormCData.FUND4_PERCENT = comboDtlsRes.FUND4_PERCENT;
					sisData.sisFormCData.FUND5_CODE = comboDtlsRes.FUND5_CODE;
					sisData.sisFormCData.FUND5_PERCENT = comboDtlsRes.FUND5_PERCENT;
					sisData.sisFormCData.FUND6_CODE = comboDtlsRes.FUND6_CODE;
					sisData.sisFormCData.FUND6_PERCENT = comboDtlsRes.FUND6_PERCENT;
					sisData.sisFormCData.FUND7_CODE = comboDtlsRes.FUND7_CODE;
					sisData.sisFormCData.FUND7_PERCENT = comboDtlsRes.FUND7_PERCENT;
					sisData.sisFormCData.FUND8_CODE = comboDtlsRes.FUND8_CODE;
					sisData.sisFormCData.FUND8_PERCENT = comboDtlsRes.FUND8_PERCENT;
					sisData.sisFormCData.FUND9_CODE = comboDtlsRes.FUND9_CODE;
					sisData.sisFormCData.FUND9_PERCENT = comboDtlsRes.FUND9_PERCENT;
					sisData.sisFormCData.FUND10_CODE = comboDtlsRes.FUND10_CODE;
					sisData.sisFormCData.FUND10_PERCENT = comboDtlsRes.FUND10_PERCENT;

					CommonService.selectRecords(db, "lp_combo_rider_dtls", true, null, {"agent_cd": agentCd, "combo_id": comboId, "ins_seq_no": insSeqNo}).then(
						function(comboRiderResp){
							if(!!comboRiderResp && comboRiderResp.rows.length>0){
								sisData.sisFormDData.RIDER_SEQ_NO = comboRiderResp.RIDER_SEQ_NO;
								sisData.sisFormDData.RIDER_CODE = comboRiderResp.RIDER_CODE;
								sisData.sisFormDData.RIDER_NAME = comboRiderResp.RIDER_NAME;
								sisData.sisFormDData.RIDER_TERM = comboRiderResp.RIDER_TERM;
								sisData.sisFormDData.RIDER_PAY_TERM = comboRiderResp.RIDER_PAY_TERM;
								sisData.sisFormDData.RIDER_COVERAGE = comboRiderResp.RIDER_COVERAGE;
								sisData.sisFormDData.RIDER_ANNUAL_PREMIUM = comboRiderResp.RIDER_ANNUAL_PREMIUM;
								sisData.sisFormDData.RIDER_UNIT = comboRiderResp.RIDER_UNIT;
								sisData.sisFormDData.RIDER_PREM_MULTIPLIER = comboRiderResp.RIDER_PREM_MULTIPLIER;
								sisData.sisFormDData.RIDER_INVT_STRAT_FLAG = comboRiderResp.RIDER_INVT_STRAT_FLAG;
								sisData.sisFormDData.RIDER_EFFECTIVE_DATE = comboRiderResp.RIDER_EFFECTIVE_DATE;
								dfd.resolve(sisData);
							}
						}
					);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.loadOpportunityData = function(oppId, leadId, fhrId, agentCd){
		var dfd = $q.defer();
		var sisData = {};
		CommonService.selectRecords(db, "lp_myopportunity", true, null, {"agent_cd": agentCd, "lead_id": leadId, "fhr_id": fhrId, "opportunity_id": oppId}).then(
			function(res){
				debug("Opp :: res.rows.length: " + res.rows.length);
				if(!!res && res.rows.length>0){
					sisData.oppData = CommonService.resultSetToObject(res);
					dfd.resolve(sisData);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.loadSavedSISData = function(agentCd, sisId, oppId, fhrId, leadId, refFhrId){
		var dfd = $q.defer();
		var sisData = {};
		CommonService.selectRecords(db, "lp_sis_main", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
			function(res){
				if(!!res && res.rows.length>0){
					sisData.sisMainData = CommonService.resultSetToObject(res);
					CommonService.selectRecords(db, "lp_sis_screen_a", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
						function(res_a){
							if(!!res_a && res_a.rows.length>0){
								SisTimelineService.isInsuredTabComplete = true;
								SisTimelineService.isProposerTabComplete = true;
								sisData.sisFormAData = CommonService.resultSetToObject(res_a);
								sisData.sisFormAData_eKyc = {};
								sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR = sisData.sisFormAData.INSURANCE_BUYING_FOR;
								sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE;
								sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR = sisData.sisFormAData.PURCHASING_INSURANCE_FOR;
								sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = sisData.sisFormAData.PURCHASING_INSURANCE_FOR_CODE;
								sisData.sisFormAData_eKyc.PROPOSER_IS_INSURED = sisData.sisFormAData.PROPOSER_IS_INSURED;
								CommonService.selectRecords(db, "lp_sis_screen_b", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
									function(res_b){
										if(!!res_b && res_b.rows.length>0){
											SisTimelineService.isPlanTabComplete = true;
											sisData.sisFormBData = CommonService.resultSetToObject(res_b);
											CommonService.selectRecords(db, "lp_sis_screen_c", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
												function(res_c){
													if(!!res_c && res_c.rows.length>0){
														SisTimelineService.isFundTabComplete = true;
														sisData.sisFormCData = CommonService.resultSetToObject(res_c);
														CommonService.selectRecords(db, "lp_sis_screen_d", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
															function(res_d){
																if(!!res_d && res_d.rows.length>0){
																	SisTimelineService.isRiderTabComplete = true;
																	sisData.sisFormDData = CommonService.resultSetToObject(res_d);
																	if(!!sisData.sisFormDData && !(sisData.sisFormDData instanceof Array)){
																		sisData.sisFormDData = [sisData.sisFormDData];
																		CommonService.selectRecords(db, "lp_combo_add_details", true, null, {"agent_cd": agentCd, "fhr_id":refFhrId, "lead_id":leadId}).then(
																			function(res_e){
																				debug("res_e : " + JSON.stringify(res_e));
																				if(!!res_e && res_e.rows.length>0){
																					sisData.sisFormEData = CommonService.resultSetToObject(res_e);
																					SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																					dfd.resolve(sisData);
																				}
																				else
																				dfd.resolve(sisData);
																			}
																		);
																	}
																}
																else{
																	CommonService.selectRecords(db, "lp_combo_add_details", true, null, {"agent_cd": agentCd, "fhr_id":refFhrId, "lead_id":leadId}).then(
																		function(res_e){
																			debug("res_e : " + JSON.stringify(res_e));
																			if(!!res_e && res_e.rows.length>0){
																				sisData.sisFormEData = CommonService.resultSetToObject(res_e);
																				SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																				dfd.resolve(sisData);
																			}
																			else
																			dfd.resolve(sisData);
																		}
																	);
																}
															}
														);
													}
													else {
														//changes due to rider premium issue 26/10/2016
														CommonService.selectRecords(db, "lp_sis_screen_d", true, null, {"agent_cd": agentCd, "sis_id": sisId}).then(
															function(res_d){
																if(!!res_d && res_d.rows.length>0){
																	SisTimelineService.isRiderTabComplete = true;
																	sisData.sisFormDData = CommonService.resultSetToObject(res_d);
																	if(!!sisData.sisFormDData && !(sisData.sisFormDData instanceof Array)){
																		sisData.sisFormDData = [sisData.sisFormDData];
																	}
																	CommonService.selectRecords(db, "lp_combo_add_details", true, null, {"agent_cd": agentCd, "fhr_id":refFhrId, "lead_id":leadId}).then(
																		function(res_e){
																			debug("res_e2 : " + JSON.stringify(res_e));
																			if(!!res_e && res_e.rows.length>0){
																				sisData.sisFormEData = CommonService.resultSetToObject(res_e);
																				SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																				dfd.resolve(sisData);
																			}
																			else{
																				CommonService.selectRecords(db, "LP_TERM_VALIDATION", true, null, {"agent_cd": agentCd,  "sis_id":sisId}).then(
																					function(res_f){
																						debug("res_f1 : " + JSON.stringify(res_f));
																						if(!!res_f && res_f.rows.length>0){
																							sisData.sisFormFData = CommonService.resultSetToObject(res_f);
																							// SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																							dfd.resolve(sisData);
																						}
																						else
																							dfd.resolve(sisData);
																					}
																				);
																			}
																		}
																	);
																}
																else{
																	CommonService.selectRecords(db, "lp_combo_add_details", true, null, {"agent_cd": agentCd, "fhr_id":refFhrId, "lead_id":leadId}).then(
																		function(res_e){
																			debug("res_e2 : " + JSON.stringify(res_e));
																			if(!!res_e && res_e.rows.length>0){
																				sisData.sisFormEData = CommonService.resultSetToObject(res_e);
																				SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																				dfd.resolve(sisData);
																			}
																			else{
																				CommonService.selectRecords(db, "LP_TERM_VALIDATION", true, null, {"agent_cd": agentCd,  "sis_id":sisId}).then(
																					function(res_f){
																						debug("res_f2 : " + JSON.stringify(res_f));
																						if(!!res_f && res_f.rows.length>0){
																							sisData.sisFormFData = CommonService.resultSetToObject(res_f);
																							// SisTimelineService.isOtherDetailsTabComplete = (!!sf.sisData.oppData.COMBO_ID);
																							dfd.resolve(sisData);
																						}
																						else
																							dfd.resolve(sisData);
																					}
																				);
																			}
																		}
																	);
																}
															}
														);
													}
												}
											);
										}
										else
											dfd.resolve(sisData);
									}
								);
							}
							else
								dfd.resolve(sisData);
						}
					);
				}
				else{
					dfd.resolve(sisData);
				}
			}
		);
		return dfd.promise;
	};

	this.loadMdlModalFactorLk = function(pglId){
		var dfd = $q.defer();
		var whereClause = {"mdl_pgl_id": pglId};
		CommonService.selectRecords(sisDB, "lp_mdl_modal_factor_lk", true, null, whereClause, null).then(
			function(res){
				debug("res.rows.length: " + res.rows.length);
				debug("mdlPglId: " + pglId);
				res = CommonService.resultSetToObject(res);
				if(!(res instanceof Array))
					res = [res];
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.loadPnlPlanLk = function(pglId){
		var dfd = $q.defer();
		var whereClause = {"pnl_pgl_id": pglId};
		CommonService.selectRecords(sisDB, "lp_pnl_plan_lk", true, null, whereClause, "pnl_code").then(
			function(res){
				debug("res.rows.length: " + res.rows.length);
				debug("pnlPglId: " + pglId);
				res = CommonService.resultSetToObject(res);
				if(!(res instanceof Array))
					res = [res];
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.loadPgfPlanGrpFeature = function(pglId){
		var dfd = $q.defer();
		var whereClause = {"pgf_pgl_id": pglId};
		CommonService.selectRecords(sisDB, "lp_pgf_plan_group_feature", true, null, whereClause, "pgf_id").then(
			function(res){
				debug("res.rows.length: " + res.rows.length);
				debug("pgf_pgl_id: " + pglId);
				res = CommonService.resultSetToObject(res);
				if(!(res instanceof Array))
					res = [res];
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.loadPglPlanGroupLk = function(pglId){
		var dfd = $q.defer();
		var whereClause = {"pgl_id": pglId};
		CommonService.selectRecords(sisDB, "lp_pgl_plan_group_lk", true, null, whereClause).then(
			function(res){
				debug("res.rows.length: " + res.rows.length);
				debug("pglId: " + pglId);
				res = CommonService.resultSetToObject(res);
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.loadPglGrpMinPremLk = function(pglId){
		var dfd = $q.defer();
		var whereClause = {"pgl_grp_id": pglId};
		CommonService.selectRecords(sisDB, "lp_pgl_grp_min_prem_lk", true, null, whereClause).then(
			function(res){
				debug("res.rows.length: " + res.rows.length);
				debug("pglId: " + pglId);
				res = CommonService.resultSetToObject(res);
				if(!(res instanceof Array))
					res = [res];
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.loadPnlTermPPTLk = function(pglId){
		var dfd = $q.defer();
		CommonService.transaction(sisDB,
			function(tx){
				CommonService.executeSql(tx,"select * from lp_pnl_term_ppt_lk where pnl_code in (select pnl_code from lp_pnl_plan_lk where pnl_pgl_id=?)", [pglId],
					function(tx,res){
						//var length = res.rows.length;
						res = CommonService.resultSetToObject(res);
						if(!(res instanceof Array))
							res = [res];
						dfd.resolve(res);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.loadProductData = function(pglId, isTermAttached){
		var dfd = $q.defer();
		debug("pglId: " + pglId);
		var objKey = ((!!isTermAttached)?("taProductData"):("productData"));
		sf.loadPnlPlanLk(pglId).then(
			function(pnlPlanLkRes){
				if(!!pnlPlanLkRes){
					sf[objKey].LP_PNL_PLAN_LK = pnlPlanLkRes;
					sf.loadPglPlanGroupLk(pglId).then(
						function(pglPlanGroupLkRes){
							if(!!pglPlanGroupLkRes){
								sf[objKey].LP_PGL_PLAN_GROUP_LK = pglPlanGroupLkRes;
								sf.loadPnlTermPPTLk(pglId).then(
									function(pnlTermPPTLkRes){
										sf[objKey].LP_PNL_TERM_PPT_LK = pnlTermPPTLkRes;
										sf.loadMdlModalFactorLk(pglId).then(
											function(mdlModalFactorLkRes){
												sf[objKey].LP_MDL_MODAL_FACTOR_LK = mdlModalFactorLkRes;
												sf.loadPglGrpMinPremLk(pglId).then(
													function(pglGrpMinPremLkRes){
														sf[objKey].LP_PGL_GRP_MIN_PREM_LK = pglGrpMinPremLkRes;
														sf.loadPgfPlanGrpFeature(pglId).then(
															function(pgfPlanGrpFeatureRes){
																sf[objKey].LP_PGF_PLAN_GROUP_FEATURE = pgfPlanGrpFeatureRes;
																dfd.resolve(sf[objKey]);
															}
														);
													}
												);
											}
										);
									}
								);
							}
							else
								dfd.resolve(null);
						}
					);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getProdMinAge = function(isTermAttached){
		var objKey = ((!!isTermAttached)?("taProductData"):("productData"));
		var minAge = 0;
		if(!!sf[objKey].LP_PNL_PLAN_LK && sf[objKey].LP_PNL_PLAN_LK.length>0){
			minAge = sf[objKey].LP_PNL_PLAN_LK[0].PNL_MIN_AGE;
			angular.forEach(sf[objKey].LP_PNL_PLAN_LK,function(planData){
				if((!!planData.PNL_MIN_AGE) && (planData.PNL_MIN_AGE < minAge))
					minAge = planData.PNL_MIN_AGE;
			});
		}
		return minAge;
	};

	this.getProdMaxAge = function(isTermAttached){
		var objKey = ((!!isTermAttached)?("taProductData"):("productData"));
		var maxAge = 100;
		if(!!sf[objKey].LP_PNL_PLAN_LK && sf[objKey].LP_PNL_PLAN_LK.length>0){
			maxAge = sf[objKey].LP_PNL_PLAN_LK[0].PNL_MAX_AGE;
			angular.forEach(sf[objKey].LP_PNL_PLAN_LK,function(planData){
				if((!!planData.PNL_MAX_AGE) && (planData.PNL_MAX_AGE > maxAge))
					maxAge = planData.PNL_MAX_AGE;
			});
		}
		return maxAge;
	};

	this.insertDataInSisMainTable = function(){
		var dfd = $q.defer();
		CommonService.insertOrReplaceRecord(db, "lp_sis_main", this.sisData.sisMainData, true).then(
			function(res){
				if(!!res){
					debug("Inserted record in LP_SIS_MAIN successfully: SIS_ID: " + sf.sisData.sisMainData.SIS_ID);
					dfd.resolve("S");
				}
				else{
					debug("Couldn't insert record in LP_SIS_MAIN");
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.shareSIS = function(agentCd, sisId, custEmailId, isCombo, leadId){
		var dfd = $q.defer();
		debug("Agent Cd: " + agentCd);
		debug("sisId: " + sisId);
		debug("custEmailId: " + custEmailId);
		debug("isCombo: " + isCombo);
		sf.createEmailEntry(agentCd, sisId, custEmailId).then(
			function(createEmailEntryRes){
				debug("in createEmailEntryRes");
				if(!!createEmailEntryRes && createEmailEntryRes=="S"){
					return sf.syncSISData(agentCd, sisId, leadId).then(
						function(sisDataSyncRes){
							debug("sisDataSyncRes: " + sisDataSyncRes);
							if(!!sisDataSyncRes && sisDataSyncRes=="S"){
								debug("updating lp_sis_main");
								return CommonService.updateRecords(db, 'lp_sis_main', {'issynced': 'Y'}, {'SIS_ID': sisId}).then(
									function(res){
										return sisDataSyncRes;
									}
								);
							}
							else
								return null;
						}
					);
				}
				else
					return null;
			}
		).then(
			function(sisDataSyncRes){
				debug("in then sisDataSyncRes");
				if(!!sisDataSyncRes){
					return sf.syncDocumentUpload(agentCd, sisId, isCombo).then(
						function(syncDocUploadRes){
							debug("in syncDocUploadRes " + JSON.stringify(syncDocUploadRes));
							if(!!syncDocUploadRes && syncDocUploadRes=="S"){
								if(!!isCombo){
									return CommonService.updateRecords(db, 'lp_document_capture', {'is_data_synced': 'Y'}, {'doc_cat_id': sisId, 'doc_cat': 'SIS', 'agent_cd': agentCd}).then(
										function(res){
											return syncDocUploadRes;
										}
									);
								}
								else{
									return sf.updateSISDataShareFlag(sisId, agentCd).then(
										function(res){
											return syncDocUploadRes;
										}
									);
								}
							}
							else
								return syncDocUploadRes;
						}
					);
				}
				else
					return null;
			}
		)
		.then(
			function(sisDocUploadRes){
				if(!!sisDocUploadRes){
					if(!!leadId && leadId!="0"){
						return sf.syncSISSignature(agentCd, sisId, isCombo).then(
							function(syncSisSignRes){
								debug("in syncSisSignRes " + JSON.stringify(syncSisSignRes));
								if(!!syncSisSignRes && syncSisSignRes=="S"){
									if(!!isCombo){
										return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sisId, 'doc_cat': 'SIS', 'agent_cd': agentCd, 'doc_name': agentCd + "_"  + sisId + "_C02.jpg"}).then(
											function(res){
												return syncSisSignRes;
											}
										);
									}
									else{
										return CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'doc_cat_id': sisId, 'doc_cat': 'SIS', 'agent_cd': agentCd, 'doc_name': agentCd + "_"  + sisId + "_C02.jpg"}).then(
											function(res){
												return syncSisSignRes;
											}
										);
									}
								}
								else{
									return syncSisSignRes;
								}
							}
						);
					}
					else{
						return "S";
					}
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(syncSisSignRes){
				if(!!syncSisSignRes && syncSisSignRes!='SKIP'){
					return sf.syncSISReport(agentCd, sisId, isCombo).then(
						function(syncSisReportRes){
							debug("in syncSisReportRes " + JSON.stringify(syncSisReportRes));
							if(!!syncSisReportRes && syncSisReportRes=="S"){
								if(!!isCombo){
									return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sisId, 'doc_cat': 'SIS', 'agent_cd': agentCd, 'doc_name': sisId + ".html"}).then(
										function(res){
											return syncSisReportRes;
										}
									);


								}
								else{
									return sf.updateSISShareFlag(sisId, agentCd).then(
										function(res){
											return syncSisReportRes;
										}
									);

								}
							}
							else
								return syncSisReportRes;
						}
					);
				}
				else if(!!syncSisSignRes && syncSisSignRes=="SKIP"){
					return "S";
				}
				else
					return null;
			}
		).then(
			function(syncSisReportRes){
				if(!!syncSisReportRes){
					debug("inside syncSisReportRes");
					return sf.syncSISEmail(agentCd, sisId, sf.EMAIL_ID_KEY, isCombo).then(
						function(syncEmailRes){
							debug("inside syncEmailRes " + syncEmailRes);
							if(!!syncEmailRes && syncEmailRes=="S"){
								return CommonService.updateRecords(db, 'lp_email_user_dtls', {'issynced': 'Y'}, {'email_cat_id': sisId, 'email_cat': 'SIS', 'agent_cd': agentCd}).then(
									function(res){
										return syncEmailRes;
									}
								);
							}
							else
								return null;
						}
					);
				}
				else
					return null;
			}
		).then(
			function(syncEmailRes){
				dfd.resolve(syncEmailRes);
			}
		);
		return dfd.promise;
	};

	this.updateSISShareFlag =  function(sisId,agentCd){
		var dfd = $q.defer();
		var query = "update lp_document_upload set IS_FILE_SYNCED = ? WHERE DOC_CAT_ID = ? AND DOC_CAT =? AND AGENT_CD = ? AND DOC_NAME NOT LIKE 'TRM%'";//1714
		var parameters = ["Y",sisId,"SIS",agentCd];
		debug(query);
		debug(parameters);
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameters,
					function(tx,res){
						debug("update SIS Share Flag");
						debug(res);
						dfd.resolve(res);

					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	}

	this.updateSISDataShareFlag =  function(sisId,agentCd){
		var dfd = $q.defer();
		var query = "update lp_document_upload set IS_DATA_SYNCED = ? WHERE DOC_CAT_ID = ? AND DOC_CAT =? AND AGENT_CD = ? AND DOC_NAME NOT LIKE 'TRM%'";//1714
		var parameters = ["Y",sisId,"SIS",agentCd];
		debug(query);
		debug(parameters);
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameters,
					function(tx,res){
						debug("update SIS Share Flag");
						debug(res);
						dfd.resolve(res);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	}

	this.updateRecordForSingleSIS = function(SIS_ID, REF_SIS_ID, agent_cd, columnName){
		var dfd = $q.defer();
		debug("in updateRecordForSingleSIS");
		var bool = false;
		var query = "update lp_document_upload set " + columnName + "=? where doc_cat_id in (?,?) and doc_cat=? and agent_cd=? and doc_name like 'TRM%'";
		var params = ['Y', SIS_ID, REF_SIS_ID, 'SIS', agent_cd];

		if(columnName=='is_file_synced'){
			query += " and is_data_synced=?";
			params.push("Y");
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, params,
					function(tx, result){
						debug("update sis id for term");
						bool = true;
						dfd.resolve(bool);
					},
					function(tx, err){
						debug("error in term updation db");
						dfd.resolve(bool);
					}
				);
			},
			function(err){
				dfd.resolve(bool);
				debug("error in term updation transaction");
			}, null
		);

		return dfd.promise;
	};

	this.shareSingleSIS = function(agentCd, SIS_Id, REF_SIS_ID, custEmailId, isCombo, finalSubmission){

		var dfd = $q.defer();
		debug("Agent Cd: " + agentCd);
		debug("sisId: " + SIS_Id);
		debug("REF_SIS_ID: " + REF_SIS_ID);
		debug("custEmailId: " + custEmailId);
		debug("isCombo: " + isCombo);

		sf.syncDocumentUploadSingleSIS(agentCd, SIS_Id, REF_SIS_ID, isCombo, finalSubmission).then(
			function(syncDocUploadRes){
				debug("in syncDocUploadRes: " + JSON.stringify(syncDocUploadRes));
				if(!!syncDocUploadRes && syncDocUploadRes=="S"){
					return sf.updateRecordForSingleSIS(SIS_Id, REF_SIS_ID, agentCd, 'is_data_synced').then(
						function(res){
							return res;
						}
					);
				}
				else{
					return syncDocUploadRes;
				}
			}
		)
		.then(
			function(sisDocUploadRes){
				if(!!sisDocUploadRes){
					return sf.syncSingleSISReport(agentCd, SIS_Id, REF_SIS_ID, isCombo).then(
						function(syncSisReportRes){
							debug("in syncSingleSISReport: " + JSON.stringify(syncSisReportRes));
							if(!!syncSisReportRes && syncSisReportRes=="S"){
								return sf.updateRecordForSingleSIS(SIS_Id, REF_SIS_ID, agentCd, 'is_file_synced').then(
									function(res){
										return res;
									}
								);
							}
							else{
								return syncSisReportRes;
							}
						}
					);
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(syncSisReportRes){
				if(!!syncSisReportRes){
					debug("inside syncSisReportRes");
					if(!!sf.EMAIL_ID_KEY){
						return sf.syncSingleSISEmail(agentCd, SIS_Id, sf.EMAIL_ID_KEY, isCombo).then(
							function(syncEmailRes){
								debug("inside syncEmailRes: " + syncEmailRes);
								if(!!syncEmailRes && syncEmailRes=="S"){
									return CommonService.updateRecords(db, 'lp_email_user_dtls', {'issynced': 'Y'}, {'email_cat_id': SIS_Id, 'email_cat': 'SIS', 'agent_cd': agentCd, "email_id": sf.EMAIL_ID_KEY}).then(
										function(res){
											return syncEmailRes;
										}
									);
								}
								else{
									return null;
								}
							}
						);
					}
					else{
						return "S";
					}
				}
				else{
					return null;
				}
			}
		)
		.then(
			function(syncEmailRes){
				dfd.resolve(syncEmailRes);
			}
		);
		return dfd.promise;
	};

	this.syncSISEmail = function(agentCd, sisId, emailIdKey, isCombo){
		debug("inside sync sis email");
		var dfd = $q.defer();

		var query = "SELECT EMAIL_ID AS EML, EMAIL_CAT AS TYP, EMAIL_TO, EMAIL_CC AS CC, EMAIL_CAT_ID AS ID, EMAIL_SUBJECT AS SUB, EMAIL_BODY AS BDY, EMAIL_TIMESTAMP AS TMP FROM LP_EMAIL_USER_DTLS WHERE AGENT_CD=? AND EMAIL_CAT=? AND EMAIL_ID=?";
		var parameterList = [agentCd, "SIS", emailIdKey];

		if(!!isCombo){
			query += " AND EMAIL_CAT_ID=?";
			parameterList.push(sisId);
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(!!sisId){
								var seDet = CommonService.resultSetToObject(res);
								seDet.TO = seDet.EMAIL_TO;
								delete seDet.EMAIL_TO;

								var sisEmailRequest = {"REQ": {"ACN": "SE", "AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID, "SEDET": []}};

								sisEmailRequest.REQ.SEDET.push(seDet);

								CommonService.ajaxCall(EMAIL_SYNC_URL, AJAX_TYPE, TYPE_JSON, sisEmailRequest, AJAX_ASYNC, AJAX_TIMEOUT,
									function(ajaxResp){
										debug("email sync resp: " + JSON.stringify(ajaxResp));
										if(!!ajaxResp && !!ajaxResp.RS){
											if(!!ajaxResp.RS.SEDET && !!ajaxResp.RS.SEDET[0] && !!ajaxResp.RS.SEDET[0].RESP && ajaxResp.RS.SEDET[0].RESP=="S"){
												dfd.resolve("S");
											}
											else{
												dfd.resolve(null);
											}
										}
										else{
											dfd.resolve(null);
										}
									},
									function(data, status, headers, config, statusText){
										dfd.resolve(null);
									}
								);
							}
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	/**
	* Function name syncSingleSISEmail
	* Function to get the email information from the local db for selected document and upload to the server for single SIS
	* Input parameters are agentCd, sisId, emailIdKey, isCombo
	* Created on 7 th december
	**/
	this.syncSingleSISEmail = function(agentCd, sisId, emailIdKey, isCombo){
		//need to verify
		debug("inside sync sis email");
		var dfd = $q.defer();

		var query = "SELECT EMAIL_ID AS EML, EMAIL_CAT AS TYP, EMAIL_TO, EMAIL_CC AS CC, EMAIL_CAT_ID AS ID, EMAIL_SUBJECT AS SUB, EMAIL_BODY AS BDY, EMAIL_TIMESTAMP AS TMP FROM LP_EMAIL_USER_DTLS WHERE AGENT_CD=? AND EMAIL_CAT=? AND EMAIL_ID=?";
		var parameterList = [agentCd, "SIS", emailIdKey];

		debug("syncSingleSISEmail Query: " + query);
		debug("syncSingleSISEmail Parameter List: " + JSON.stringify(parameterList));

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(!!sisId){
								var seDet = CommonService.resultSetToObject(res);
								seDet.TO = seDet.EMAIL_TO;
								delete seDet.EMAIL_TO;

								var sisEmailRequest = {"REQ": {"ACN": "SE", "AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID, "SEDET": []}};

								sisEmailRequest.REQ.SEDET.push(seDet);

								CommonService.ajaxCall(EMAIL_SYNC_URL, AJAX_TYPE, TYPE_JSON, sisEmailRequest, AJAX_ASYNC, AJAX_TIMEOUT,
									function(ajaxResp){
										debug("email sync resp: syncSingleSISEmail" + JSON.stringify(ajaxResp));
										if(!!ajaxResp && !!ajaxResp.RS){
											if(!!ajaxResp.RS.SEDET && !!ajaxResp.RS.SEDET[0] && !!ajaxResp.RS.SEDET[0].RESP && ajaxResp.RS.SEDET[0].RESP=="S"){
												dfd.resolve("S");
											}
											else{
												dfd.resolve(null);
											}
										}
										else{
											dfd.resolve(null);
										}
									},
									function(data, status, headers, config, statusText){
										dfd.resolve(null);
									}
								);
							}
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.createEmailEntry = function(agentCd, sisId, custEmailId){
		var dfd = $q.defer();
		function createNewEntry(){
			var dfd_new = $q.defer();
			sf.EMAIL_ID_KEY = CommonService.getRandomNumber();
			var insertRecord = {"email_id": sf.EMAIL_ID_KEY, "agent_cd": agentCd, "email_to": custEmailId, "email_cc":"", "email_subject": "", "email_body": "", "email_cat": "SIS", "email_cat_id": sisId, "email_timestamp": CommonService.getCurrDate(), "issynced": "N"};
			CommonService.insertOrReplaceRecord(db, "lp_email_user_dtls", insertRecord, true).then(
				function(res){
					dfd_new.resolve("S");
				}
			);
			return dfd_new.promise;
		}

		CommonService.selectRecords(db, "lp_email_user_dtls", null, "issynced, email_id", {email_cat: "SIS", email_cat_id: sisId, agent_cd: agentCd}).then(
			function(res){
				if(!!res && res.rows.length>0){
					if(res.rows.item(0).ISSYNCED!="Y"){
						sf.EMAIL_ID_KEY = res.rows.item(0).EMAIL_ID;
						dfd.resolve("S");
					}
					else{
						createNewEntry().then(
							function(newEntryRes){
								if(!!newEntryRes)
									dfd.resolve("S");
								else
									dfd.resolve(null);
							}
						);
					}
				}
				else{
					createNewEntry().then(
						function(newEntryRes){
							if(!!newEntryRes)
								dfd.resolve("S");
							else
								dfd.resolve(null);
						}
					);
				}
			}
		);
		return dfd.promise;
	};

	this.syncSISSignature = function(agentCd, sisId, isCombo, finalSubmission){
		var dfd = $q.defer();

		var query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND IS_DATA_SYNCED=?";
		var parameterList = ["SIS", agentCd, "%.jpg", 'Y'];
		if(!!finalSubmission){
			query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?";
			parameterList = ["SIS", agentCd, "%.jpg"];
		}


		if(!!isCombo){
			query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND IS_DATA_SYNCED=?";
			if(!!finalSubmission){
				query = "SELECT IS_FILE_SYNCED, DOC_NAME, DOC_CAT_ID AS FRM, POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST, DOC_CAP_ID, DOC_CAP_REF_ID FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?";
				parameterList = ["SIS", agentCd, "%.jpg"];
			}
		}

		if(!!sisId){
			query += " AND DOC_CAT_ID=?";
			parameterList.push(sisId);
		}

		function sendFile(file,options){
			var innerDfd = $q.defer();
			var ft = new FileTransfer();
			debug("file: " + file);
			ft.upload(file, DOCUMENT_FILE_UPLOAD_URL, function(succ){
				debug("Code = " + succ.responseCode);
				debug("Response = " + succ.response);
				debug("Sent = " + succ.bytesSent);
				if(!!succ && !!succ.response && JSON.parse(succ.response).RS.RESP==="S"){
					innerDfd.resolve("S");
				}
				else{
					innerDfd.resolve(null);
				}
			},
			function(err){
				debug("FT upload error: " + JSON.stringify(err));
				innerDfd.resolve(null);
			},options);
			return innerDfd.promise;
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(res.rows.item(0).IS_FILE_SYNCED=='Y' && !finalSubmission){
								dfd.resolve("S");
							}
							else{
								var options = new FileUploadOptions();
								options.fileKey = "file";
								options.fileName = res.rows.item(0).DOC_NAME;
								options.mimeType = "image/jpeg";
								options.headers={'contentType':'multipart/form-data'};
								options.httpMethod = "POST";
								var params = {};
								params.AC = agentCd+"";
								params.DVID = localStorage.DVID;
								params.PWD = LoginService.lgnSrvObj.password;
								params.ACN = "SID";
								if(!!isCombo){
									params.COMBO = "C";
								}
								else{
									params.COMBO = "A";
								}
								params.FRM = res.rows.item(0).FRM;
								params.PAG = res.rows.item(0).PAG;
								params.TST = res.rows.item(0).TST;
								params.POL = res.rows.item(0).POL || "0";
								params.DOC = res.rows.item(0).DOC;
								params.ISASUB = null;
								params.DOC_NAME = res.rows.item(0).DOC_NAME;
								params.DOCCID = res.rows.item(0).DOC_CAP_ID;
								params.DOCRID = res.rows.item(0).DOC_CAP_REF_ID;

								debug("paramList: " + JSON.stringify(params));
								debug("file details: " + JSON.stringify(params));
								options.params = params;

								CommonService.getTempFileURI("/FromClient/SIS/SIGNATURE/", res.rows.item(0).DOC_NAME).then(
									function(file){
										sendFile(file,options).then(
											function(resp){
												dfd.resolve(resp);
											}
										);
									}
								);
							}
						}
						else
							dfd.resolve("S");
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);

		return dfd.promise;
	};


	/**
	* Function name createEmailEntrySingleSIS
	* Function create  New Entry of email table
	* Input parameters are agentCd, sisId, custEmailId
	* Created on 7 th december
	**/

	this.createEmailEntrySingleSIS = function(agentCd, sisId, custEmailId){
		var dfd = $q.defer();
		function createNewEntry(){
			var dfd_new = $q.defer();
			sf.EMAIL_ID_KEY = CommonService.getRandomNumber();
			var insertRecord = {"email_id": sf.EMAIL_ID_KEY, "agent_cd": agentCd, "email_to": custEmailId, "email_cc":"", "email_subject": "", "email_body": "", "email_cat": "SIS", "email_cat_id": sisId, "email_timestamp": CommonService.getCurrDate(), "issynced": "N"};
			CommonService.insertOrReplaceRecord(db, "lp_email_user_dtls", insertRecord, true).then(
				function(res){
					dfd_new.resolve("S");
				}
			);
			return dfd_new.promise;
		}

		CommonService.selectRecords(db, "lp_email_user_dtls", null, "issynced, email_id", {email_cat: "SIS", email_cat_id: sisId, agent_cd: agentCd}).then(
			function(res){
				if(!!res && res.rows.length>0){
					if(res.rows.item(0).ISSYNCED!="Y"){
						sf.EMAIL_ID_KEY = res.rows.item(0).EMAIL_ID;
						dfd.resolve("S");
					}
					else{
						createNewEntry().then(
							function(newEntryRes){
								if(!!newEntryRes)
									dfd.resolve("S");
								else
									dfd.resolve(null);
							}
						);
					}
				}
				else{
					createNewEntry().then(
						function(newEntryRes){
							if(!!newEntryRes)
								dfd.resolve("S");
							else
								dfd.resolve(null);
						}
					);
				}
			}
		);
		return dfd.promise;
	};


	this.syncSISReport = function(agentCd, sisId, isCombo, finalSubmission){
		var dfd = $q.defer();

		var query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?  AND DOC_NAME NOT LIKE ? AND IS_DATA_SYNCED=?";
		var parameterList = ["SIS", agentCd, "%.html", "TRM%",'Y'];
		if(!!finalSubmission){
			query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ?  AND DOC_NAME NOT LIKE ?";
			parameterList = ["SIS", agentCd, "%.html", "TRM%"];
		}


		if(!!isCombo){
			query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND DOC_NAME NOT LIKE ? AND IS_DATA_SYNCED=?";
			if(!!finalSubmission){
				query = "SELECT IS_FILE_SYNCED, DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_CAPTURE WHERE DOC_CAT=? AND AGENT_CD=? AND DOC_NAME LIKE ? AND DOC_NAME NOT LIKE ?";
				parameterList = ["SIS", agentCd, "%.html", "TRM%"];
			}
		}

		if(!!sisId){
			query += " AND DOC_CAT_ID=?";
			parameterList.push(sisId);
		}

		function sendFile(file,options){
			var innerDfd = $q.defer();
			var ft = new FileTransfer();
			debug("file: " + file);
			ft.upload(file, HTML_SYNC_URL, function(succ){
				debug("Code = " + succ.responseCode);
				debug("Response = " + succ.response);
				debug("Sent = " + succ.bytesSent);
				if(!!succ && !!succ.response && JSON.parse(succ.response).RS.RESP==="S"){
					innerDfd.resolve("S");
				}
				else{
					innerDfd.resolve(null);
				}
			},
			function(err){
				debug("FT upload error: " + err);
				innerDfd.resolve(null);
			},options);
			return innerDfd.promise;
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("share report ");
						debug(res);
						if(!!res && res.rows.length>0){
							if(res.rows.item(0).IS_FILE_SYNCED=='Y' && !finalSubmission){
								dfd.resolve("S");
							}
							else{
								delete res.rows.item(0).IS_FILE_SYNCED;
								var options = new FileUploadOptions();
								options.fileKey = "file";
								options.fileName = res.rows.item(0).DOC_NAME;
								options.mimeType = "application/octet-stream";
								options.headers={'contentType':'multipart/form-data'};
								options.httpMethod = "POST";
								var params = {};
								params.AC = agentCd+"";
								params.DVID = localStorage.DVID;
								params.PWD = LoginService.lgnSrvObj.password;
								params.ACN = "SCFD";
								if(!!isCombo){
									params.COMBO = "C";
								}
								else{
									params.COMBO = "A";
								}
								params.CATID = res.rows.item(0).FRM;
								params.CAT = "SIS";
								params.DOCID = res.rows.item(0).DOC;
								params.ISASUB = null;
								params.FILETYPE = "HTML";
								debug("paramList: " + JSON.stringify(params));
								debug("file details: " + JSON.stringify(params));
								options.params = params;

								CommonService.getTempFileURI("/FromClient/SIS/HTML/", res.rows.item(0).DOC_NAME).then(
									function(file){
										sendFile(file,options).then(
											function(resp){
												dfd.resolve(resp);
											}
										);
									}
								);
							}
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.sendSingleSISFile = function(agentCd, sis){
		var dfd = $q.defer();
		delete sis.IS_FILE_SYNCED;
		var options = new FileUploadOptions();
		options.fileKey = "file";
		options.fileName = sis.DOC_NAME;
		options.mimeType = "application/octet-stream";
		options.headers={'contentType':'multipart/form-data'};
		options.httpMethod = "POST";
		var params = {};
		params.AC = agentCd+"";
		params.DVID = localStorage.DVID;
		params.PWD = LoginService.lgnSrvObj.password;
		params.ACN = "SCFD";
		params.COMBO = "A";
		params.CATID = sis.FRM;
		params.CAT = "SIS";
		params.DOCID = sis.DOC;
		params.ISASUB = null;
		params.FILETYPE = "HTML";

		params.DOCCID = sis.DOCCID;
		params.DOCRID = sis.DOCRID;;
		debug("paramList: " + JSON.stringify(params));
		debug("file details: " + JSON.stringify(params));

		options.params = params;

		CommonService.getTempFileURI("/FromClient/SIS/HTML/", sis.DOC_NAME).then(
			function(file){
				CommonService.sendFile(file, options).then(
					function(resp){
						dfd.resolve(resp);
					}
				);
			}
		);
		return dfd.promise;
	};

	this.sendSingleSISReports = function(agentCd, sisList, finalSubmission){
		var promises = [];
		if(!!sisList && sisList.length>0){
			for(var i=0; i<sisList.length; i++){
				var sis = sisList[i];
				debug("Sis trm sync file request: " + JSON.stringify(sis));
				if(sis.IS_FILE_SYNCED=='Y' && !finalSubmission){
					promises.push("S");
				}
				else{
					promises.push(sf.sendSingleSISFile(agentCd, sis));
				}
			}
		}
		else{
			promises.push(null);
		}
		return $q.all(promises).then(
			function(res){
				debug("$q.all: " + JSON.stringify(res));
				if(!!res && res.length>0){
					for(var i=0; i<res.length; i++){
						var ret = "S";
						if(!res[i] || res[i]!="S"){
							ret = null;
							break;
						}
						return ret;
					}
				}
				else{
					return null;
				}
			}
		);
	};

	/**
	* Function name syncSingleSISReport
	* Function to uploade the html file to server in case of single sis we checked if the the data is doc name should not starts with TRM
	* Input parameters are agentCd, sisId, isCombo, finalSubmission
	* Created on 7 th december
	**/

	this.syncSingleSISReport = function(agentCd, SIS_Id, REF_SIS_ID, isCombo, finalSubmission){
		var dfd = $q.defer();
		var BASE_SIS_ID = SIS_Id;
		var TERM_SIS_ID = REF_SIS_ID;
		var query = "SELECT IS_FILE_SYNCED,DOC_CAP_ID AS DOCCID, DOC_CAP_REF_ID AS DOCRID,DOC_NAME,DOC_CAT_ID AS FRM,POLICY_NO AS POL, DOC_ID AS DOC, DOC_PAGE_NO AS PAG, DOC_TIMESTAMP AS TST FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT_ID in (?, ?) and AGENT_CD=? and DOC_CAT=? and DOC_NAME like 'TRM%'";
		var parameterList = [BASE_SIS_ID, TERM_SIS_ID, agentCd, 'SIS'];


		debug("syncSingleSISReport query: " + query);
		debug("syncSingleSISReport parameters: " + JSON.stringify(parameterList));

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx, res){
						debug("syncSingleSISReport response: " + JSON.stringify(res.rows.item(0)));
						debug(res);
						if(!!res && res.rows.length>0){
							var res = CommonService.resultSetToObject(res);
							if(!!res && (!(res instanceof Array)))
								res = [res];
							dfd.resolve(sf.sendSingleSISReports(agentCd, res, finalSubmission));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx, err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.syncDocumentUpload = function(agentCd, sisId, isCombo, finalSubmission, polNo){
		var dfd = $q.defer();

		var acn = "FDS";
		var query = "SELECT IS_DATA_SYNCED, DOC_ID AS DOCID, DOC_CAT AS CAT,DOC_CAT_ID AS CATID,POLICY_NO AS POLNO,DOC_NAME AS DOCNM,DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG FROM LP_DOCUMENT_UPLOAD WHERE AGENT_CD=? AND DOC_CAT=? AND DOC_NAME NOT LIKE 'TRM%'";
		var parameterList = [agentCd, "SIS"];

		var URL = DOC_UPLOAD_SYNC_URL;

		if(!!isCombo){
			query = "SELECT IS_DATA_SYNCED, DOC_CAP_ID AS DOCCAPID, DOC_CAP_REF_ID AS DOCCAPREFID, DOC_ID AS DOCID, DOC_CAT AS CAT,DOC_CAT_ID AS CATID,POLICY_NO AS POLNO,DOC_NAME AS DOCNM,DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG FROM LP_DOCUMENT_CAPTURE WHERE AGENT_CD=? AND DOC_CAT=? AND DOC_NAME NOT LIKE 'TRM%'";
			acn = "CFDS";
			URL = COMBO_DOC_UPLOAD_SYNC_URL;
		}

		if(!!sisId){
			query += " AND DOC_CAT_ID=?";
			parameterList.push(sisId);
		}

		debug("syncDocumentUpload query: " + query);
		debug("syncDocumentUpload Parameter list: " + parameterList);
		debug("syncDocumentUpload URL: " + URL);

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("syncDocumentUpload response");
						if(!!res && res.rows.length>0){
							if(res.rows.item(0).IS_DATA_SYNCED!="Y" || (!!finalSubmission)){
								debug("res.rows.length: " + res.rows.length);
								delete res.rows.item(0).IS_DATA_SYNCED;
								if(res.rows.length>1)
									delete res.rows.item(1).IS_DATA_SYNCED;
								if(!!sisId){
									var sisDet = CommonService.resultSetToObject(res);
									if(!(sisDet instanceof Array))
										sisDet = [sisDet];
									if(!!sisDet){
										sisDet[0].POLNO = sisDet[0].POLNO || polNo || "0";
										if(sisDet.length>1){
											// Signature...
											sisDet[1].POLNO = sisDet[1].POLNO || polNo || "0";
										}
										var sisDocUploadRequest = {"REQ": {"AC":(agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID}};
										if(!!isCombo){
											sisDocUploadRequest.REQ.ComDocUpload = [];
											sisDocUploadRequest.REQ.ComDocUpload = sisDet;
										}
										else{
											sisDocUploadRequest.REQ.DOCUPLOAD = [];
											sisDocUploadRequest.REQ.DOCUPLOAD = sisDet;
										}
										sisDocUploadRequest.REQ.ACN = acn;
										debug("URL: " + URL);
										debug("sisDocUploadRequest: " + JSON.stringify(sisDocUploadRequest));
										CommonService.ajaxCall(URL, AJAX_TYPE, TYPE_JSON, sisDocUploadRequest, AJAX_ASYNC, AJAX_TIMEOUT,
											function(ajaxResp){
												debug("sisDocUploadRes: " + JSON.stringify(ajaxResp));
												if(!!ajaxResp && !!ajaxResp.RS){
													if((!!isCombo) && (!!ajaxResp.RS.ComDocUpload && !!ajaxResp.RS.ComDocUpload[0] && !!ajaxResp.RS.ComDocUpload[0].RESP && ajaxResp.RS.ComDocUpload[0].RESP=="S")){
														dfd.resolve("S");
													}
													else if((!isCombo) && (!!ajaxResp.RS.DOCUPLOAD && !!ajaxResp.RS.DOCUPLOAD[0] && !!ajaxResp.RS.DOCUPLOAD[0].RESP && ajaxResp.RS.DOCUPLOAD[0].RESP=="S")){
														dfd.resolve("S");
													}
													else{
														dfd.resolve(null);
													}
												}
												else{
													dfd.resolve(null);
												}
											},
											function(data, status, headers, config, statusText){
												dfd.resolve(null);
											}
										);
									}
									else
										dfd.resolve(sisDet || null);
								}
							}
							else{
								dfd.resolve("S");
							}
						}
						else
							dfd.resolve(null);
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};


	/**
	* Function name syncDocumentUploadSingleSIS
	* Sync app data to the server related to server
	* Input parameters are agentCd, sisId, isCombo, finalSubmission, polNo
	* Created on 7 th december
	**/

	this.syncDocumentUploadSingleSIS = function(agentCd, SIS_Id, REF_SIS_ID, isCombo, finalSubmission, polNo){
		var dfd = $q.defer();
		var BASE_SIS_ID = SIS_Id;
		var TERM_SIS_ID = REF_SIS_ID;

		var acn = "FDS";
		var query = "SELECT IS_DATA_SYNCED, DOC_CAP_ID AS DOCCID, DOC_CAP_REF_ID AS DOCRID,DOC_ID AS DOCID, DOC_CAT AS CAT,DOC_CAT_ID AS CATID,POLICY_NO AS POLNO,DOC_NAME AS DOCNM,DOC_TIMESTAMP AS DOCTM,DOC_PAGE_NO AS PAG FROM LP_DOCUMENT_UPLOAD WHERE DOC_CAT_ID in (?,?) and DOC_NAME like 'TRM%'";

		var parameterList = [BASE_SIS_ID, TERM_SIS_ID];
		var URL = DOC_UPLOAD_SYNC_URL;

		debug("query: " + query);
		debug("Parameter list: " + parameterList);
		debug("URL: " + URL);

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("RECORD FOUND >>" + res.rows.length);
						debug(res);
						if(!!res && res.rows.length>1){
							if(res.rows.item(0).IS_DATA_SYNCED!="Y" || (!!finalSubmission)){
								delete res.rows.item(0).IS_DATA_SYNCED;
								if(!!BASE_SIS_ID){
									debug(JSON.stringify(res));
									debug(res);
									var sisDet = res.rows.item(0);
									var sisDetTerm = res.rows.item(1);
									if(!!sisDet){
										sisDet.POLNO = sisDet.POLNO || polNo || "0";
										sisDetTerm.POLNO = sisDetTerm.POLNO || polNo || "0";
										var sisDocUploadRequest = {"REQ": {"AC": (agentCd + ""), "PWD": LoginService.lgnSrvObj.password, "DVID": localStorage.DVID}};
										if(!!isCombo){
											sisDocUploadRequest.REQ.ComDocUpload = [];
											sisDocUploadRequest.REQ.ComDocUpload = sisDet;
											sisDocUploadRequest.REQ.ComDocUpload.push(sisDetTerm);
										}
										else{
											sisDocUploadRequest.REQ.DOCUPLOAD = [];
											sisDocUploadRequest.REQ.DOCUPLOAD = sisDet;
											sisDocUploadRequest.REQ.DOCUPLOAD.push(sisDetTerm);
										}
										sisDocUploadRequest.REQ.ACN = acn;
										debug("sisDocUploadRequest : "+JSON.stringify(sisDocUploadRequest));
										debug("URL : "+URL);

										CommonService.ajaxCall(URL, AJAX_TYPE, TYPE_JSON, sisDocUploadRequest, AJAX_ASYNC, AJAX_TIMEOUT,
											function(ajaxResp){
												debug("sisDocUploadRes: " + JSON.stringify(ajaxResp));
												if(!!ajaxResp && !!ajaxResp.RS){
													if((!!isCombo) && (!!ajaxResp.RS.ComDocUpload && !!ajaxResp.RS.ComDocUpload[0] && !!ajaxResp.RS.ComDocUpload[0].RESP && ajaxResp.RS.ComDocUpload[0].RESP=="S")){
														dfd.resolve("S");
													}
													else if((!isCombo) && (!!ajaxResp.RS.DOCUPLOAD && !!ajaxResp.RS.DOCUPLOAD[0] && !!ajaxResp.RS.DOCUPLOAD[0].RESP && ajaxResp.RS.DOCUPLOAD[0].RESP=="S")){
														dfd.resolve("S");
													}
													else{
														dfd.resolve(null);
													}
												}
												else{
													dfd.resolve(null);
												}
											},
											function(data, status, headers, config, statusText){
												dfd.resolve(null);
											}
										);
									}
									else{
										dfd.resolve(sisDet || null);
									}
								}
							}
							else{
								dfd.resolve("S");
							}
						}
						else
							dfd.resolve("S");
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getSISPlanAndPersonalData = function(agentCd, sisId){
		debug("in getSISPlanAndPersonalData");
		var dfd = $q.defer();
		var query = "SELECT SISMAIN.ISSYNCED AS ISSYNCED, SISMAIN.SIS_ID AS SISID, SISMAIN.FHR_ID AS FHID, SISMAIN.LEAD_ID AS LEAD, SISMAIN.GOAL_TYPE AS GTYP, SISMAIN.ANNUAL_PREMIUM_AMOUNT AS PRAM, SISMAIN.MODAL_PREMIUM AS MDPR, SISMAIN.SERVICE_TAX AS SRTX, SISMAIN.COMPLETION_TIMESTAMP AS CTM, SISMAIN.SIGNED_TIMESTAMP AS STM, SISMAIN.STANDARD_OR_ALTERNATE_SIS AS STAL, SISMAIN.DISCOUNTED_MODAL_PREMIUM AS DMP, SISMAIN.DISCOUNTED_SERVICE_TAX AS DST,SISMAIN.SAR AS SAR, SCRB.PREM_MULTIPLIER AS PRMLT, SCRA.PURCHASING_INSURANCE_FOR AS PF, SCRA.PURCHASING_INSURANCE_FOR_CODE AS PFCD, SCRA.PROPOSER_IS_INSURED AS PISI, SCRA.INSURANCE_BUYING_FOR AS BF, SCRA.INSURANCE_BUYING_FOR_CODE AS BFCD, SCRA.INSURED_TITLE AS ITITL, SCRA.INSURED_FIRST_NAME AS IFNM, SCRA.INSURED_MIDDLE AS IMNM, SCRA.INSURED_LAST_NAME AS ILNM, SCRA.INSURED_OCCU_CODE AS IOCCD, SCRA.INSURED_DOB AS IDOB, SCRA.INSURED_GENDER AS IGND, SCRA.INSURED_MOBILE AS IMOB, SCRA.INSURED_EMAIL AS IEML, SCRA.ISSMOKER AS ISMOK, SCRA.STD_AGEPROF_FLAG AS STDAGE, SCRA.PROPOSER_TITLE AS PTITL, SCRA.PROPOSER_FIRST_NAME AS PFNM, SCRA.PROPOSER_MIDDLE_NAME AS PMNM, SCRA.PROPOSER_LAST_NAME AS PRLNM, SCRA.PROPOSER_OCCU_CODE AS POCCD, SCRA.PROPOSER_DOB AS PDOB, SCRA.PROPOSER_GENDER AS PGND, SCRA.PROPOSER_MOBILE AS PMOB, SCRA.PROPOSER_EMAIL AS PEML, SCRA.TATA_FLAG AS TATAFLG, SCRA.COMPANY_CODE AS CMPCODE, SCRB.PLAN_NAME AS PLNM, SCRB.PLAN_CODE AS PLCD, SCRB.PREMIUM_PAY_MODE AS PYMD, SCRB.POLICY_TERM AS POLTRM, SCRB.PREMIUM_PAY_TERM AS PAYTRM, SCRB.SUM_ASSURED AS SUMAS, SCRB.BASE_PREMIUM AS BSPRM, SCRB.INVT_STRAT_FLAG AS INVSTR FROM LP_SIS_MAIN AS SISMAIN JOIN LP_SIS_SCREEN_A AS SCRA ON (SCRA.SIS_ID=SISMAIN.SIS_ID) JOIN LP_SIS_SCREEN_B AS SCRB ON ( SCRB.SIS_ID=SISMAIN.SIS_ID) WHERE SISMAIN.AGENT_CD=?";
		var parameterList = [agentCd];
		if(!!sisId){
			query += " AND SISMAIN.SIS_ID=?";
			parameterList.push(sisId);
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(res.rows.item(0).ISSYNCED=='Y'){
								dfd.resolve("S");
							}
							else{
								delete res.rows.item(0).ISSYNCED;
								if(!!sisId){
									var sisDet = CommonService.resultSetToObject(res);
									if(!!sisDet){
										sisDet.FHID = sisDet.FHID || "0";
										sisDet.LEAD = sisDet.LEAD || "0";
										sisDet.GTYP = sisDet.GTYP || "0";
									}
									dfd.resolve(sisDet || null);
								}
							}
						}
						else
							dfd.resolve(null);
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getSISFundData = function(agentCd, sisId){
		var dfd = $q.defer();
		debug("in getSisFundData");

		var query = "SELECT INVEST_OPT AS INVOPT,FUND_CODE_FROM AS FFRM,FUND_CODE_TO AS FTO,FUND1_CODE AS F1CD,FUND1_PERCENT AS F1PT, FUND2_CODE AS F2CD,FUND2_PERCENT AS F2PT, FUND3_CODE AS F3CD,FUND3_PERCENT AS F3PT, FUND4_CODE AS F4CD, FUND4_PERCENT AS F4PT, FUND5_CODE AS F5CD, FUND5_PERCENT AS F5PT,FUND6_CODE AS F6CD, FUND6_PERCENT AS F6PT, FUND7_CODE AS F7CD, FUND7_PERCENT AS F7PT, FUND8_CODE AS F8CD, FUND8_PERCENT F8PT, FUND9_CODE AS F9CD, FUND9_PERCENT AS F9PT, FUND10_CODE AS F10CD, FUND10_PERCENT AS F10PT from LP_SIS_SCREEN_C where AGENT_CD=?";
		var parameterList = [agentCd];

		if(!!sisId){
			query += " AND SIS_ID=?";
			parameterList.push(sisId);
		}

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(!!sisId){
								var fundsList = CommonService.resultSetToObject(res); // Object
								if(!!fundsList){
									for(var i=1;i<=10;i++){
										if(!fundsList['F' + i +'PT']){
											fundsList['F' + i +'PT'] = "0";
										}
									}
								}
								dfd.resolve(fundsList || null);
							}
						}
						else
							dfd.resolve(null);
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);

		return dfd.promise;
	};

	this.getSISRiderData = function(agentCd, sisId){
		var dfd = $q.defer();
		debug("in getSISRiderData");
		var query = "SELECT SIS_ID AS RISIS, AGENT_CD AS RIAGCD, RIDER_SEQ_NO AS RISENO, RIDER_CODE AS RICO, RIDER_NAME AS RINA, RIDER_TERM AS RITE, RIDER_PAY_TERM AS RIPATE,RIDER_PAY_MODE AS RIPAMO, RIDER_PAY_MODE_DESC AS RIPAMODE, RIDER_ANNUAL_PREMIUM AS RIANPR, RIDER_MODAL_PREMIUM AS RIMOPR, RIDER_BASE_PREMIUM RIBAPR, RIDER_SERVICE_TAX AS RISETA, RIDER_PREM_MULTIPLIER AS RIPRMU, RIDER_COVERAGE AS RICOVR, RIDER_UNIT AS RIUN, RIDER_INVT_STRAT_FLAG AS RIINSTF FROM LP_SIS_SCREEN_D WHERE AGENT_CD=?";
		var parameterList = [agentCd];
		if(!!sisId){
			query += " AND SIS_ID=?";
			parameterList.push(sisId);
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(!!sisId){
								var riderList = CommonService.resultSetToObject(res);
								if(!!riderList){
									if(!(riderList instanceof Array))
										riderList = [riderList];
									angular.forEach(riderList, function(rider){
										if(!!rider){
											rider.RIUN = rider.RIUN || "0";
											rider.RIANPR = rider.RIANPR || "0";
											rider.RIPRMU = rider.RIPRMU || "0";
											rider.RISETA = rider.RISETA || "0";
										}
									});
								}
								dfd.resolve(riderList || null);
							}
						}
						else
							dfd.resolve(null);
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.syncSISData = function(agentCd, sisId, leadId){
		debug("in syncSISData : " + agentCd + "\tsisId : " + sisId + "\tleadId : " + leadId);
		var dfd = $q.defer();
		var sisDataShareRequest = {"REQ":{"ACN":"SSD","AC":agentCd,"PWD":LoginService.lgnSrvObj.password,"DVID":localStorage.DVID,"SISDET":[],"SISFUND":[],"SISRIDER":[]}};
		this.getSISPlanAndPersonalData(agentCd, sisId).then(
			function(mainRes){
				debug("in mainRes");
				if(!!sisId && !!mainRes){
					sisDataShareRequest.REQ.SISDET.push(mainRes);
				}
				return mainRes;
			}
		).then(
			function(mainRes){
				if(!!mainRes){
					debug("in then mainRes");
					return sf.getSISFundData(agentCd, sisId).then(
						function(fundRes){
							debug("in fundRes");
							if(!!fundRes)
								sisDataShareRequest.REQ.SISFUND.push(fundRes);
							return mainRes;
						}
					);
				}
				else
					return mainRes;
			}
		).then(
			function(mainRes){
				debug("in then fundRes");
				if(!!mainRes){
					return sf.getSISRiderData(agentCd, sisId).then(
						function(riderRes){
							sisDataShareRequest.REQ.SISRIDER = riderRes;
							return mainRes;
						}
					);
				}
				else
					return mainRes;
			}
		).then(
			function(mainRes){
				if(!!mainRes && mainRes!="S"){
					return CommonService.ajaxCall(SIS_DATA_SYNC_URL, AJAX_TYPE, TYPE_JSON, sisDataShareRequest, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
							return ajaxResp;
						},
						function(data, status, headers, config, statusText){
							return null;
						}
					);
				}
				else
					return mainRes;
			}
		).then(
			function(ajaxResp){
				debug("resolving syncRes " + JSON.stringify(ajaxResp));
				if(!!ajaxResp && ((ajaxResp=="S")  ||  (!!ajaxResp.data && !!ajaxResp.data.RS && !!ajaxResp.data.RS.SISDET && !!ajaxResp.data.RS.SISDET[0] && !!ajaxResp.data.RS.SISDET[0].RESP && ajaxResp.data.RS.SISDET[0].RESP=="S"))){
					return "S";
				}
				else
					return null;
			}
		).then(
			function(sisDataRes){
				if(!!sisDataRes){
					return sf.syncTermValidationData(agentCd, leadId, sisId).then(
						function(termValSyncRes){
							if(!!termValSyncRes && termValSyncRes=="S"){
								return CommonService.updateRecords(db, 'LP_TERM_VALIDATION', {'issynced': 'Y'}, {'agent_cd':agentCd, 'sis_id': sisId}).then(
									function(res){
										dfd.resolve(termValSyncRes);
									}
								);
							}else if(!!termValSyncRes && termValSyncRes=="NA"){
								dfd.resolve("S");
							}else {
								dfd.resolve(null);
							}
						}
					);
				}else {
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.createOpportunity = function(agentCd, leadId, fhrId, sisId, fhrSignedTimestamp, ekycDetails){
		var dfd = $q.defer();

		var _oppId = CommonService.getRandomNumber();
		var oppData = {"AGENT_CD": agentCd, "LEAD_ID": leadId, "FHR_ID": fhrId, "FHR_SIGNED_TIMESTAMP": fhrSignedTimestamp, "SIS_ID": sisId, "OPPORTUNITY_ID": _oppId, "RECO_PRODUCT_DEVIATION_FLAG": "S", "LAST_UPDATED_DATE": CommonService.getCurrDate()};
		if(!!ekycDetails && !!ekycDetails.insured)
			oppData = {"AGENT_CD": agentCd, "LEAD_ID": leadId, "FHR_ID": fhrId, "FHR_SIGNED_TIMESTAMP": fhrSignedTimestamp, "SIS_ID": sisId, "OPPORTUNITY_ID": _oppId, "RECO_PRODUCT_DEVIATION_FLAG": "S", "LAST_UPDATED_DATE": CommonService.getCurrDate(),"BUY_FOR":(!!ekycDetails) ? ekycDetails.BUY_FOR : '', "INS_EKYC_FLAG": (!!ekycDetails && !!ekycDetails.insured)? ekycDetails.insured.EKYC_FLAG:null, "PR_EKYC_FLAG":(!!ekycDetails && !!ekycDetails.proposer)? ekycDetails.proposer.EKYC_FLAG: '', "INS_EKYC_ID":(!!ekycDetails && !!ekycDetails.insured)?ekycDetails.insured.EKYC_ID :'',"PR_EKYC_ID":(!!ekycDetails && !!ekycDetails.proposer)?ekycDetails.proposer.EKYC_ID :''};
		CommonService.insertOrReplaceRecord(db, 'lp_myopportunity', oppData, true).then(
			function(res){
				debug("OPP DATA INSERTED :"+JSON.stringify(oppData));
				sf.sisData.OPP_ID = _oppId;
				dfd.resolve(res || null);
			}
		);

		return dfd.promise;
	};

	this.createOrUpdateOpportunity = function(agentCd, leadId, fhrId, sisId, signedTimestamp /*sis or fhr*/,ekycDetails, flag){
		var dfd = $q.defer();
		debug("this.sisData.OPP_ID: " + this.sisData.OPP_ID);
		if(flag)
		{
			/*do not updated sis signed timestamp*/
			debug("sisSignedTimestamp set to null")
			signedTimestamp = null;
		}
		if(!!this.sisData.OPP_ID && this.sisData.OPP_ID!="0"){
			this.updateOpportunity(agentCd, leadId, fhrId, sisId, signedTimestamp /*sis signed timestamp*/,ekycDetails).then(
				function(res){
					dfd.resolve(res);
				}
			);
		}
		else{
			this.createOpportunity(agentCd, leadId, fhrId, sisId, signedTimestamp /*fhr signed timestamp*/, ekycDetails).then(
				function(res){
					dfd.resolve(res);
				}
			);
		}
		return dfd.promise;
	};

	this.updateOpportunity = function(agentCd, leadId, fhrId, sisId, sisSignedTimestamp, ekycDetails){
		var dfd = $q.defer();
		if(!!leadId && leadId!="0"){
			var oppData = {'sis_id': sisId, 'sis_signed_timestamp': sisSignedTimestamp, 'last_updated_date': CommonService.getCurrDate()};
			if(!!ekycDetails && !!ekycDetails.insured)
				oppData = {'sis_id': sisId, 'sis_signed_timestamp': sisSignedTimestamp, 'last_updated_date': CommonService.getCurrDate(),"BUY_FOR":(!!ekycDetails) ? ekycDetails.BUY_FOR : '', 'ins_ekyc_flag': (!!ekycDetails && !!ekycDetails.insured)? ekycDetails.insured.EKYC_FLAG:'', 'pr_ekyc_flag':(!!ekycDetails && !!ekycDetails.proposer)? ekycDetails.proposer.EKYC_FLAG: '', 'ins_ekyc_id':(!!ekycDetails && !!ekycDetails.insured)?ekycDetails.insured.EKYC_ID :'','pr_ekyc_id':(!!ekycDetails && !!ekycDetails.proposer)?ekycDetails.proposer.EKYC_ID :''};
			sf.sisData.oppData.sis_id = sisId;
			sf.sisData.oppData.sis_signed_timestamp = sisSignedTimestamp;

			if(BUSINESS_TYPE!="LifePlaner" && BUSINESS_TYPE!='IndusSolution'){
				oppData['insured_name'] = sf.sisData.sisFormAData.INSURED_FIRST_NAME || null;
				oppData['proposer_name'] = sf.sisData.sisFormAData.PROPOSER_FIRST_NAME || null;
			}

			CommonService.updateRecords(db, 'lp_myopportunity', oppData, {'agent_cd': agentCd, 'lead_id': leadId, 'fhr_id': fhrId}).then(
				function(res){
					debug("OPP DATA UPDATED :"+JSON.stringify(oppData));
					dfd.resolve(res || null);
				}
			);
		}
		else {
			dfd.resolve("S");
		}
		return dfd.promise;
	};

	this.generateSIS = function(agentCd, pglId, leadId, fhrId, sisId, comboId, insSeqNo, solType, prodName, oppId){
		debug("in generateSIS " + fhrId);
		if(!!this.sisData){
			this.sisData.sisFormAData = null;
			this.sisData.sisFormBData = null;
			this.sisData.sisFormCData = null;
			this.sisData.sisFormDData = null;
			this.sisData.sisFormEData = null;
			this.sisData.sisOutput = null;
			this.sisData.sisTermOutput = null;
		}
		this.sisData = null;
		this.SOL_TYPE = null;
		this.PROD_NAME = null;

		SisTimelineService.activeTab = "insured";
		SisTimelineService.isEKYCTabComplete = false;
		SisTimelineService.isInsuredTabComplete = false;
		SisTimelineService.isProposerTabComplete = false;
		SisTimelineService.isPlanTabComplete = false;
		SisTimelineService.isFundTabComplete = false;
		SisTimelineService.isRiderTabComplete = false;
		SisTimelineService.isOtherDetailsTabComplete = false;
		SisTimelineService.isOutputTabComplete = false;

		$state.go('sisForm.sisEKYC',{"SIS_ID": sisId, "AGENT_CD": agentCd, "PGL_ID": pglId, "LEAD_ID": leadId, "FHR_ID": fhrId, "COMBO_ID": comboId, "INS_SEQ_NO": insSeqNo, "SOL_TYPE": solType, "PROD_NAME": prodName, "OPP_ID": oppId});
	};

	this.getEKYCData = function(agentCd, /*sisId*/leadId){
		var dfd = $q.defer();
		var sql = "select * from LP_LEAD_EKYC_DTLS where LEAD_ID = '"+leadId+"' and AGENT_CD = '"+agentCd+"' and EKYC_FLAG='Y' and EKYC_ID is not null and CONSUMER_AADHAR_NO is not null";
		CommonService.transaction(db, function(tx){
					CommonService.executeSql(tx,sql,[],
								function(tx,res){
									var ins = [];
									ins.push({"CONSUMER_AADHAR_NO": "Select Aadhar Number", "CUST_TYPE":INSURED_CUST_TYPE});
									if(!!res && res.rows.length>0){
										var _res = CommonService.resultSetToObject(res);
										if(!(_res instanceof Array))
											_res = [_res];
										var obj = {};
										obj.insured = ins;
										var _Instemp = $filter('filter')(_res, {"CUST_TYPE": INSURED_CUST_TYPE});
										for(var i = 0; i<_Instemp.length;i++)
										{
											ins.push(_Instemp[i]);
											if(i == _Instemp.length-1)
												obj.insured = ins;//$filter('filter')(_res, {"CUST_TYPE": INSURED_CUST_TYPE});//[0];
									  }
										var propArr = $filter('filter')(_res, {"CUST_TYPE": PROPOSER_CUST_TYPE});
										obj.proposer = (!!propArr && propArr instanceof Array && propArr.length>0)?(propArr[0]):{};
										dfd.resolve(obj);
									}
									else{
										var obj = {"proposer": {"CUST_TYPE": PROPOSER_CUST_TYPE}};
										obj.insured = ins;
										dfd.resolve(obj);
									}
								},
								function(tx, error){
									debug("LP_LEAD_EKYC_DTLS error :"+error.message);
								}
							);
							},
						function(err){
							dfd.resolve(obj);
						}
					);
		/*CommonService.selectRecords(db, "LP_LEAD_EKYC_DTLS", true, null, {"LEAD_ID": leadId, "AGENT_CD": agentCd}).then(
			function(res){
				if(!!res && res.rows.length>0){
					var _res = CommonService.resultSetToObject(res);
					if(!(_res instanceof Array))
						_res = [_res];
					var obj = {};
					obj.insured = $filter('filter')(_res, {"CUST_TYPE": INSURED_CUST_TYPE});//[0];
					var propArr = $filter('filter')(_res, {"CUST_TYPE": PROPOSER_CUST_TYPE});
					obj.proposer = (!!propArr && propArr instanceof Array && propArr.length>0)?(propArr[0]):{};
					dfd.resolve(obj);
				}
				else{
					var obj = {"insured": {"CUST_TYPE": INSURED_CUST_TYPE}, "proposer": {"CUST_TYPE": PROPOSER_CUST_TYPE}};
					dfd.resolve(obj);
				}
			}
		);*/
		return dfd.promise;
	};

	this.getSelectedEKYCData = function(agentCd, leadId, EKYC_ID){
		var dfd = $q.defer();
		var whereObj = {"LEAD_ID": leadId, "AGENT_CD": agentCd};
		if(!!EKYC_ID)
			whereObj.EKYC_ID = EKYC_ID;
		CommonService.selectRecords(db, "LP_LEAD_EKYC_DTLS", true, null, whereObj).then(
			function(res){
				if(!!res && res.rows.length>0){
					var _res = CommonService.resultSetToObject(res);
					if(!(_res instanceof Array))
						_res = [_res];
					var obj = {};
					obj.insured = $filter('filter')(_res, {"CUST_TYPE": INSURED_CUST_TYPE});//[0];
					var propArr = $filter('filter')(_res, {"CUST_TYPE": PROPOSER_CUST_TYPE});
					obj.proposer = (!!propArr && propArr instanceof Array && propArr.length>0)?(propArr[0]):{};
					dfd.resolve(obj);
				}
				else{
					var obj = {"insured": {"CUST_TYPE": INSURED_CUST_TYPE}, "proposer": {"CUST_TYPE": PROPOSER_CUST_TYPE}};
					dfd.resolve(obj);
				}
			}
		);
		return dfd.promise;
	}

	this.insertDataInFhrMainTable = function(){
		var dfd = $q.defer();
		if(!!this.sisData.sisMainData.LEAD_ID && this.sisData.sisMainData.LEAD_ID!="0" && !this.sisData.sisMainData.FHR_ID){
			var _fhrId = CommonService.getRandomNumber();
			var _fhrSignedTimestamp = CommonService.getCurrDate();
			var fhrData = {"FHR_ID": _fhrId, "AGENT_CD": this.sisData.sisMainData.AGENT_CD, "LEAD_ID": this.sisData.sisMainData.LEAD_ID,"COMPLETION_TIMESTAMP": _fhrSignedTimestamp, "SIGNED_TIMESTAMP": _fhrSignedTimestamp};
			CommonService.insertOrReplaceRecord(db, "lp_fhr_main", fhrData, true).then(
				function(res){
					if(!!res){
						debug("Inserted record in LP_FHR_MAIN successfully: FHR_ID: " + fhrData.FHR_ID);
						sf.sisData.sisMainData.FHR_ID = _fhrId;
						sf.sisData.FHR_SIGNED_TIMESTAMP = _fhrSignedTimestamp;
						dfd.resolve("S");
					}
					else{
						debug("Couldn't insert record in LP_FHR_MAIN");
						dfd.resolve(null);
					}
				}
			);
		}
		else {
			dfd.resolve("S");
		}
		return dfd.promise;
	};

	this.gotoTAorOutput = function(scrnB, isRider){
		var dfd = $q.defer();
		debug("scrnB : " + scrnB);
		debug("isRider : " + isRider);
		debug("PGL_ID : " + this.sisData.sisMainData.PGL_ID);
		CommonService.showLoading("Loading...");
		if(this.sisData.sisMainData.PGL_ID == '224' || this.sisData.sisMainData.PGL_ID == '225'){
			if(isRider){
				dfd.resolve(true);
			}else{
				$state.go("sisForm.sisFormF");
			}
		}else {
			CommonService.selectRecords(db,'LP_PLAN_MASTER',true,'PRODUCT_PGL_ID',{'PRODUCT_PGL_ID' : sf.sisData.sisMainData.PGL_ID,'TERM_ALLOWED_FLAG': 'Y'}, "").then(
				function(res){
					debug("LP_PLAN_MASTER :" + res.rows.length);
					debug("INPUT_ANNUAL_PREMIUM :" + sf.sisData.sisFormBData.INPUT_ANNUAL_PREMIUM);
					debug("STD_AGEPROF_FLAG :" + sf.sisData.sisFormAData.STD_AGEPROF_FLAG);
					if(!!res && res.rows.length>0){
						var propAge = CommonService.getAge(sf.sisData.sisFormAData.PROPOSER_DOB);
						if(sf.sisData.sisFormAData.PROPOSER_IS_INSURED=='Y' && propAge>=18 && sf.sisData.sisFormAData.STD_AGEPROF_FLAG=='Y' && (BUSINESS_TYPE=="IndusSolution") && (!!sf.sisData.sisMainData.LEAD_ID && sf.sisData.sisMainData.LEAD_ID!=0)){
							if(sf.sisData.sisFormBData.INPUT_ANNUAL_PREMIUM>=50000){
								if(isRider){
									dfd.resolve(true);
								}else{
									sf.sisData.oppData.REF_TERM_FLAG1 = 'Y';
									sf.updateMyOpportunity('REF_TERM_FLAG1','Y');
									$state.go("sisForm.sfePage1");
								}
							}else if(!!scrnB){
								navigator.notification.confirm("Policies with Annual premium Rs.50000 and above are eligible for Term Plan. Would you like to increase your Premium ?",
									function(buttonIndex){
										if(buttonIndex=="1"){
											CommonService.hideLoading();
											debug("on hold to increase premium");
											//SisOutputService.saveProposerSignature(sign, true);
										}
										else{
											sf.sisData.oppData.REF_TERM_FLAG1 = 'N';
											sf.updateMyOpportunity('REF_TERM_FLAG1','N');
											$state.go("sisForm.sisOutput");
										}
									},
									'SIS','Yes, No'
								);
							}
							else if(!!isRider){
								navigator.notification.confirm("Policies with Annual premium Rs.50000 and above are eligible for Term Plan. Would you like to increase your Premium ?",
									function(buttonIndex){
										if(buttonIndex=="1"){
											CommonService.hideLoading();
											debug("on hold to increase premium");
											//SisOutputService.saveProposerSignature(sign, true);
										}
										else{
											dfd.resolve(true);
										}
									},
									'SIS','Yes, No'
								);
							}else{
								sf.sisData.oppData.REF_TERM_FLAG1 = 'N';
								sf.updateMyOpportunity('REF_TERM_FLAG1','N');
								$state.go("sisForm.sisOutput");
							}
						}
						else {
							if(!!isRider){
								dfd.resolve(true);
							}else{
								sf.sisData.oppData.REF_TERM_FLAG1 = 'N';
								sf.updateMyOpportunity('REF_TERM_FLAG1','N');
								$state.go("sisForm.sisOutput");
							}
						}
					}
					else {
						if(!!isRider){
							dfd.resolve(true);
						}else{
							sf.sisData.oppData.REF_TERM_FLAG1 = 'N';
							sf.updateMyOpportunity('REF_TERM_FLAG1','N');
							$state.go("sisForm.sisOutput");
						}
					}
				}
			);
		}

		return dfd.promise;
	};

	this.updateMyOpportunity = function(columnName, value){
		debug("columnName : " + columnName + "\tvalue : " + value);
		var dfd = $q.defer();
		var updateRec = {};
		updateRec[columnName] = value;
		CommonService.updateRecords(db, 'lp_myopportunity', updateRec, {'agent_cd': sf.sisData.sisMainData.AGENT_CD, 'lead_id': sf.sisData.sisMainData.LEAD_ID, 'fhr_id': sf.sisData.sisMainData.FHR_ID, 'sis_id': sf.sisData.sisMainData.SIS_ID}).then(
			function(res){
				debug("lp_myopportunity update : " + JSON.stringify(res));
				dfd.resolve(res || null);
			}
		);
		return dfd.promise;
	};

	this.getInsuredAge = function(){
		if(!!this.sisData.sisFormAData.INSURED_DOB){
			return CommonService.getAge(this.sisData.sisFormAData.INSURED_DOB);
		}
		else {
			return null;
		}
	};

	this.getTAPlanCodeList = function(){
		var insuredAge = this.getInsuredAge() + "";
		var insuredGender = this.sisData.sisFormAData.INSURED_GENDER;
		var productDetails = this.getProductDetails(isTermAttached);
		var planCodeList = [{"PNL_CODE_DISPLAY": "Select Plan Code", "PNL_CODE": null}];
		if(!!productDetails && !!productDetails.LP_PNL_PLAN_LK && productDetails.LP_PNL_PLAN_LK.length>0){
			angular.forEach(productDetails.LP_PNL_PLAN_LK, function(planDetails){
				var insuredAgeCondition = (!!insuredAge && ((parseInt(insuredAge)>=parseInt(planDetails.PNL_MIN_AGE)) && (parseInt(insuredAge)<=planDetails.PNL_MAX_AGE)));
				var taOptionCheck = (planDetails.PNL_CODE.indexOf('V1N1')!='-1');
				if(insuredAgeCondition && (!planDetails.PNL_GENDER || insuredGender===planDetails.PNL_GENDER)){
					var obj = {};

					obj.PNL_CODE_DISPLAY = planDetails.PNL_CODE;
					obj.PNL_CODE = planDetails.PNL_CODE;

					obj.PNL_ID = planDetails.PNL_ID;
					obj.PNL_DESCRIPTION = planDetails.PNL_DESCRIPTION;
					obj.PNL_PGL_ID = planDetails.PNL_PGL_ID;
					obj.PNL_MIN_AGE = planDetails.PNL_MIN_AGE;
					obj.PNL_MAX_AGE = planDetails.PNL_MAX_AGE;
					obj.PNL_MIN_SUM_ASSURED = planDetails.PNL_MIN_SUM_ASSURED;
					obj.PNL_MAX_SUM_ASSURED = planDetails.PNL_MAX_SUM_ASSURED;
					obj.PNL_MIN_PREMIUM = planDetails.PNL_MIN_PREMIUM;
					obj.PNL_MAX_PREMIUM = planDetails.PNL_MAX_PREMIUM;
					obj.PNL_MIN_OCCUPATION_CLASS = planDetails.PNL_MIN_OCCUPATION_CLASS;
					obj.PNL_MAX_OCCUPATION_CLASS = planDetails.PNL_MAX_OCCUPATION_CLASS;
					obj.PNL_PREMIUM_MULTIPLE = planDetails.PNL_PREMIUM_MULTIPLE; // premium should be multiple of...
					obj.PNL_MAX_PREMIUM_PAYING_TERM = planDetails.PNL_MAX_PREMIUM_PAYING_TERM;
					obj.PNL_FREQUENCY = planDetails.PNL_FREQUENCY;
					obj.PNL_PRREM_MULT_FORMULA = planDetails.PNL_PRREM_MULT_FORMULA;
					obj.PNL_SA_MULTIPLEOF = planDetails.PNL_SA_MULTIPLEOF;
					obj.PNL_GENDER = planDetails.PNL_GENDER;
					obj.PNL_MIN_PREMIUM_BAND = planDetails.PNL_MIN_PREMIUM_BAND;
					obj.PNL_MAX_PREMIUM_BAND = planDetails.PNL_MAX_PREMIUM_BAND;
					planCodeList.push(obj);
				}
			});
			return planCodeList;
		}
		else{
			debug("Plan details are not available");
			return planCodeList;
		}
	};

	this.getPlanCodeList = function(pglId, isTermAttached){
		debug("in getPlanCodeList");
		var insuredAge = this.getInsuredAge() + "";
		var insuredGender = this.sisData.sisFormAData.INSURED_GENDER;
		var productDetails = this.getProductDetails(isTermAttached);
		var planCodeList = [{"PNL_CODE_DISPLAY": "Select Plan Code", "PNL_CODE": null}];
		if(!!productDetails && !!productDetails.LP_PNL_PLAN_LK && productDetails.LP_PNL_PLAN_LK.length>0){
			angular.forEach(productDetails.LP_PNL_PLAN_LK, function(planDetails){
				var insuredAgeCondition = (!!insuredAge && ((parseInt(insuredAge)>=parseInt(planDetails.PNL_MIN_AGE)) && (parseInt(insuredAge)<=planDetails.PNL_MAX_AGE)));
				var taOptionCheck = true;
				if(isTermAttached){
					// Only Option 1 (SR/ SR+) to be allowed for Term Attachment
					taOptionCheck = (planDetails.PNL_CODE.indexOf('V1N1')!='-1');
				}
				if(insuredAgeCondition && (!planDetails.PNL_GENDER || insuredGender===planDetails.PNL_GENDER) && taOptionCheck){
					var obj = {};

					obj.PNL_CODE_DISPLAY = planDetails.PNL_CODE;
					obj.PNL_CODE = planDetails.PNL_CODE;

					obj.PNL_ID = planDetails.PNL_ID;
					obj.PNL_DESCRIPTION = planDetails.PNL_DESCRIPTION;
					obj.PNL_PGL_ID = planDetails.PNL_PGL_ID;
					obj.PNL_MIN_AGE = planDetails.PNL_MIN_AGE;
					obj.PNL_MAX_AGE = planDetails.PNL_MAX_AGE;
					obj.PNL_MIN_SUM_ASSURED = planDetails.PNL_MIN_SUM_ASSURED;
					obj.PNL_MAX_SUM_ASSURED = planDetails.PNL_MAX_SUM_ASSURED;
					obj.PNL_MIN_PREMIUM = planDetails.PNL_MIN_PREMIUM;
					obj.PNL_MAX_PREMIUM = planDetails.PNL_MAX_PREMIUM;
					obj.PNL_MIN_OCCUPATION_CLASS = planDetails.PNL_MIN_OCCUPATION_CLASS;
					obj.PNL_MAX_OCCUPATION_CLASS = planDetails.PNL_MAX_OCCUPATION_CLASS;
					obj.PNL_PREMIUM_MULTIPLE = planDetails.PNL_PREMIUM_MULTIPLE; // premium should be multiple of...
					obj.PNL_MAX_PREMIUM_PAYING_TERM = planDetails.PNL_MAX_PREMIUM_PAYING_TERM;
					obj.PNL_FREQUENCY = planDetails.PNL_FREQUENCY;
					obj.PNL_PRREM_MULT_FORMULA = planDetails.PNL_PRREM_MULT_FORMULA;
					obj.PNL_SA_MULTIPLEOF = planDetails.PNL_SA_MULTIPLEOF;
					obj.PNL_GENDER = planDetails.PNL_GENDER;
					obj.PNL_MIN_PREMIUM_BAND = planDetails.PNL_MIN_PREMIUM_BAND;
					obj.PNL_MAX_PREMIUM_BAND = planDetails.PNL_MAX_PREMIUM_BAND;
					planCodeList.push(obj);
				}
			});
			return planCodeList;
		}
		else{
			debug("Plan details are not available");
			return planCodeList;
		}
	};


	this.getPPTList = function(pglId, ppm){
		debug("in getPPTList" + pglId);
		var pptList = [{"PPT_DISPLAY": "Select Premium Paying Term", "PPT": null}];
		switch(pglId){
			case IMX_PGL_ID:
			case WMX_PGL_ID: // Fortune Maxima, Wealth Maxima
							if((!ppm) || (!!ppm && ppm=="O"))
								pptList.push({"PPT_DISPLAY": "1", "PPT": "1"});
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "7", "PPT": "7"});
								pptList.push({"PPT_DISPLAY": "8", "PPT": "8"});
								pptList.push({"PPT_DISPLAY": "9", "PPT": "9"});
								pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
								pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
								pptList.push({"PPT_DISPLAY": "20", "PPT": "20"});
							}
							break;

			case IPR_PGL_ID:
			case WPR_PGL_ID: // Fortune Pro, Wealth Pro
							if((!ppm) || (!!ppm && ppm=="O"))
								pptList.push({"PPT_DISPLAY": "1", "PPT": "1"});
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "5", "PPT": "5"});
								pptList.push({"PPT_DISPLAY": "7", "PPT": "7"});
								pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
								pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
								pptList.push({"PPT_DISPLAY": "20", "PPT": "20"});
							}
							break;

			case MIP_PGL_ID: //MIP
							pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});

							// In case of MIP PPT 7 is allowed only in insured is at least 3 years old

							if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))>=3)
								pptList.push({"PPT_DISPLAY": "7", "PPT": "7"});
							break;

			case SIP_PGL_ID: //SIP
							pptList.push({"PPT_DISPLAY": "7", "PPT": "7"}); // commented for IBL
							pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
							pptList.push({"PPT_DISPLAY": "12", "PPT": "12"});
							break;

			case MLS_PGL_ID: //MLS
							pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
							pptList.push({"PPT_DISPLAY": "12", "PPT": "12"});
							break;

			case MBP_PGL_ID: //MBP
							pptList.push({"PPT_DISPLAY": "8", "PPT": "8"});
							pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
							pptList.push({"PPT_DISPLAY": "12", "PPT": "12"});
							break;

			case ITRP_PGL_ID: //IR TROP
							if((!ppm) || (!!ppm && ppm!="O"))
								pptList.push({"PPT_DISPLAY": "Regular", "PPT": "0"});
							if((!ppm) || (!!ppm && ppm=="O"))
								pptList.push({"PPT_DISPLAY": "Single", "PPT": "1"});
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "Limited - 5 years", "PPT": "5"});
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=60)
									pptList.push({"PPT_DISPLAY": "Limited - 10 years", "PPT": "10"});
							}
							break;

			case IRS_PGL_ID: //IRS
							if((!ppm) || (!!ppm && ppm!="O"))
								pptList.push({"PPT_DISPLAY": "Regular", "PPT": "0"});
							if((!ppm) || (!!ppm && ppm=="O"))
								pptList.push({"PPT_DISPLAY": "Single", "PPT": "1"});
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "Limited - 5 years", "PPT": "5"});
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=65)
									pptList.push({"PPT_DISPLAY": "Limited - 10 years", "PPT": "10"});
							}
							break;

			case MRS_PGL_ID: //MRS
							if((!ppm) || (!!ppm && ppm!="O")){
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=49)
									pptList.push({"PPT_DISPLAY": "Pure Protection Cover with Life Stage Plus", "PPT": "0", "PLAN_CODE": "MRSP1N1V2"});
								pptList.push({"PPT_DISPLAY": "Pure Protection Cover", "PPT": "0", "PLAN_CODE": "MRSP1N2V2"});
							}
							if((!ppm) || (!!ppm && ppm=="O")){
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=49)
									pptList.push({"PPT_DISPLAY": "Pure Protection Cover with Life Stage Plus - Single Pay", "PPT": "1", "PLAN_CODE": "MRSP2N1V2"});
								pptList.push({"PPT_DISPLAY": "Pure Protection Cover - Single Pay", "PPT": "1", "PLAN_CODE": "MRSP2N2V2"});
							}
							break;

			case IWP_PGL_ID: // IWA
							if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))>=8)
								pptList.push({"PPT_DISPLAY": "7", "PPT": "7"});
							if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))>=3)
								pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
							pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
							break;

			case FR_PGL_ID: // Freedom
							pptList.push({"PPT_DISPLAY": "7", "PPT": "7"});
							pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
							pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
							break;
			case VCP_PGL_ID: // VCP
							var insAge = parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB));
							var propAge = parseInt(CommonService.getAge(sf.sisData.sisFormAData.PROPOSER_DOB));
							if((insAge + 10 <= 85) && (propAge + 10 <= 85)){
								pptList.push({"PPT_DISPLAY": "10", "PPT": "10"});
							}
							if((insAge + 15 <= 85) && (propAge + 15 <= 85)){
								pptList.push({"PPT_DISPLAY": "15", "PPT": "15"});
							}
							if((insAge + 20 <= 85) && (propAge + 20 <= 85)){
								pptList.push({"PPT_DISPLAY": "20", "PPT": "20"});
							}
							if((insAge + 25 <= 85) && (propAge + 25 <= 85)){
								pptList.push({"PPT_DISPLAY": "25", "PPT": "25"});
							}
							if((insAge + 30 <= 85) && (propAge + 30 <= 85)){
								pptList.push({"PPT_DISPLAY": "30", "PPT": "30"});
							}
							break;
			case SR_PGL_ID: //SR
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "Regular", "PPT": "0"});
							}
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "Limited - 5 years", "PPT": "5"});
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=65){
									pptList.push({"PPT_DISPLAY": "Limited - 10 years", "PPT": "10"});
								}
							}
							break;

			case SRP_PGL_ID: //SRP
							if((!ppm) || (!!ppm && ppm!="O")){
								pptList.push({"PPT_DISPLAY": "Regular", "PPT": "0"});
							}
							if((!ppm) || (!!ppm && ppm!="O")){
									pptList.push({"PPT_DISPLAY": "Limited - 5 years", "PPT": "5"});
								if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=65){
									pptList.push({"PPT_DISPLAY": "Limited - 10 years", "PPT": "10"});
								}
							}
							break;
			case GIP_PGL_ID: //GIP
							var insAge = parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB));
							debug("insAge : " + insAge);
							if(insAge>=6 && insAge<=65)
								pptList.push({"PPT_DISPLAY": "5", "PPT": "5"});
							if(insAge>=3 && insAge<=55)
							pptList.push({"PPT_DISPLAY": "12", "PPT": "12"});
							break;
			default:
							debug("PPT list is not available");
							pptList = null;
		}
		return pptList;
	};

	this.getPPMList = function(isTermAttached){
		try{
			var pnlList = sf.getProductDetails(isTermAttached).LP_PNL_PLAN_LK;
			debug("pnlList: " + JSON.stringify(pnlList));
			var ppmList = [{"PPM_DISPLAY": "Select Premium Payment Mode", "PPM": null}];
			var pnlFreqMaster = {"A": {"NAME": "Annual", "AVAIL": false}, "S": {"NAME": "Semi-Annual", "AVAIL": false}, "Q": {"NAME": "Quarterly", "AVAIL": false}, "M": {"NAME": "Monthly", "AVAIL": false}, "O": {"NAME": "Single-Pay", "AVAIL": false}};
			angular.forEach(pnlList,function(plan){
				var pnlFreqList = plan.PNL_FREQUENCY.split("");
				angular.forEach(pnlFreqList,function(freq){
					pnlFreqMaster[freq].AVAIL = true;
				});
			});
			var pnlFreqList = [];
			var keys = Object.keys(pnlFreqMaster);
			angular.forEach(keys,function(key){
				if(pnlFreqMaster[key].AVAIL)
					ppmList.push({"PPM_DISPLAY": pnlFreqMaster[key].NAME, "PPM": key});
			});
			return ppmList;
		}catch(ex){
			debug("Exception getPPMList: " + ex.message);
			return null;
		}
	};

	this.getSPList = function(isTermAttached){
		try{
			var spList = [];
			var startingPoint = sf.getProductDetails(isTermAttached).LP_PNL_PLAN_LK[0].PNL_START_PT;
			if(!startingPoint){
				spList.push({"SP_DISPLAY": "Select Starting Point", "SP": null});
				spList.push({"SP_DISPLAY": "Premium", "SP": "P"});
				spList.push({"SP_DISPLAY": "Sum Assured", "SP": "S"});
			}
			else if(startingPoint === "S")
				spList.push({"SP_DISPLAY": "Sum Assured", "SP": "S"});
			else if(startingPoint === "P")
				spList.push({"SP_DISPLAY": "Premium", "SP": "P"});
			return spList;
		}catch(ex){
			debug("Exception getSPList: " + ex.message);
			return null;
		}
	};

	this.getPolTermFixedPGLs = function(planCodeList, pglId, isTermAttached){
		try{
			var termFlag = null;
			angular.forEach(planCodeList,function(plan){
				var planTermDet = ($filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plan.PNL_CODE})[0]);
				if(!!planTermDet)
					termFlag = planTermDet.TERM_FLAG;
			});

			if((!!termFlag) && (termFlag == "F"))
				return [pglId];
			else
				return null;
		}
		catch(ex){
			debug("Exception in getPolTermFixedPGLs(): " + ex.message);
			return null;
		}
	};

	this.getPolTermLimitDetails = function(planTermList, planCodeList, pglId, isTermAttached){
		try{
			var termLimits = {"MIN": null, "MAX": null, "READONLY": true};
			//debug("planTermlist: " + planTermList);
			angular.forEach(planTermList, function(planTermDet){
				if(!termLimits.MIN)
					termLimits.MIN = planTermDet.MIN_TERM;
				else if((!!planTermDet.MIN_TERM) && (termLimits.MIN > planTermDet.MIN_TERM))
					termLimits.MIN = planTermDet.MIN_TERM;

				if(!termLimits.MAX)
					termLimits.MAX = planTermDet.MAX_TERM;
				else if((!!planTermDet.MAX_TERM) && (termLimits.MAX < planTermDet.MAX_TERM))
					termLimits.MAX = planTermDet.MAX_TERM;
			});
			debug("termLimits: " + JSON.stringify(termLimits));
			//var readOnlyPGLs = ['194','195','200','201','215','219','198','206','205','212','217','189','188','193']; // MLG, MLGP, IMX, WMX, MIP, SIP, Smart 7, IW, MBP, SA, Freedom, Sec7, MLS, MLM

			var readOnlyPGLs = this.getPolTermFixedPGLs(planCodeList, pglId, isTermAttached); // products having term flag F

			// below products (derPolTermList) have some pol term that is fixed but derived hence don't have term flag F
			var derPolTermList = [MLGP_PGL_ID, MLG_PGL_ID, IMX_PGL_ID, WMX_PGL_ID, SUA_PGL_ID, FR_PGL_ID, VCP_PGL_ID, GIP_PGL_ID];

			readOnlyPGLs = (!!readOnlyPGLs)?readOnlyPGLs.concat(derPolTermList):derPolTermList;

			debug("readOnlyPGLs: " + readOnlyPGLs);

			if((!readOnlyPGLs) || (readOnlyPGLs.indexOf(pglId) === -1))
				termLimits.READONLY = false;
			else
				termLimits.READONLY = true;

			return termLimits;
		} catch(ex){
			debug("Exception: " + ex.message);
		}
	};

	this.getPlanName = function(isTermAttached){
		return (("Tata AIA Life Insurance " + (sf.getProductDetails(isTermAttached).LP_PGL_PLAN_GROUP_LK.PGL_DESCRIPTION).replace("Insurance ","")).replace("V2","")).trim();
	};

	this.getOptionList = function(pglId, isTermAttached){
		var optionList = [];
		if(pglId == SIP_PGL_ID){ // SIP
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			optionList.push({"OPTION_DISPLAY": "Regular Income Benefit", "OPTION": "IV1N1"});
			optionList.push({"OPTION_DISPLAY": "Endowment Benefit", "OPTION": "EV1N1"});
			return optionList;
		}
		else if(pglId == SGP_PGL_ID){ // SGP
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			optionList.push({"OPTION_DISPLAY": "Endowment", "OPTION": "SGPV1N1"});
			if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))>=18)
				optionList.push({"OPTION_DISPLAY": "Endowment with inbuilt Accidental Death Benefit", "OPTION": "SGPV1N2"});
			return optionList;
		}
		else if(pglId == FR_PGL_ID){ // Freedom
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			if(parseInt(CommonService.getAge(sf.sisData.sisFormAData.INSURED_DOB))<=45)
				optionList.push({"OPTION_DISPLAY": "Option A", "OPTION": "V1P1"});
			optionList.push({"OPTION_DISPLAY": "Option B", "OPTION": "V1P2"});
			return optionList;
		}else if(pglId == VCP_PGL_ID){ // VCP
			return this.getPlanFeatureList(isTermAttached);
		}
		else if(pglId == SR_PGL_ID){ // SGP
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			optionList.push({"OPTION_DISPLAY": 'Option 1: "Sum Assured on death" payable on death', "OPTION": "V1N1"});
			optionList.push({"OPTION_DISPLAY": 'Option 2: "Sum Assured on Death" payable on Death & Monthly Income thereafter for 10 years ', "OPTION": "V1N2"});
			optionList.push({"OPTION_DISPLAY": 'Option 3: "Enhanced Sum Assured on Death" payable on Death', "OPTION": "V1N3"});
			optionList.push({"OPTION_DISPLAY": 'Option 4: "Enhanced Sum Assured on Death" payable on Death & Monthly Income thereafter for 10 years', "OPTION": "V1N4"});
			return optionList;
		}
		else if(pglId == SRP_PGL_ID){ // SGP
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			optionList.push({"OPTION_DISPLAY": 'Option 1: "Sum Assured on death" payable on death', "OPTION": "V1N1"});
			optionList.push({"OPTION_DISPLAY": 'Option 2: "Sum Assured on Death" payable on Death & Monthly Income thereafter for 10 years ', "OPTION": "V1N2"});
			return optionList;
		}
		else if(pglId == GIP_PGL_ID){ // GIP
			optionList.push({"OPTION_DISPLAY": "Select an option", "OPTION": null});
			optionList.push({"OPTION_DISPLAY": "Income Term 10 Years", "OPTION": "10N1"});
			optionList.push({"OPTION_DISPLAY": "Income Term 15 Years", "OPTION": "15N1"});
			return optionList;
		}
		else
			return null;
	};

	this.getPlanFeatureList = function(isTermAttached){
		debug("in getPlanFeatureList");
		try{
			var productDetails = sf.getProductDetails(isTermAttached);
			var planFeatureList = [{"OPTION_DISPLAY": "Select Plan Option", "OPTION": null}];
			if(!!productDetails && !!productDetails.LP_PGF_PLAN_GROUP_FEATURE && productDetails.LP_PGF_PLAN_GROUP_FEATURE.length>0){
				angular.forEach(productDetails.LP_PGF_PLAN_GROUP_FEATURE, function(planOptionDetails){
					var obj = {};
					obj.OPTION_DISPLAY = planOptionDetails.PGF_FEATURE;
					obj.OPTION = planOptionDetails.PGF_FEATURE;
					obj.PGF_ATTACHED_PLANS = planOptionDetails.PGF_ATTACHED_PLANS.split(',');
					planFeatureList.push(obj);
				});
				return planFeatureList;
			}
			else{
				debug("Plan options are not available");
				return planFeatureList;
			}
		}catch(ex){
			debug("Exception in getPlanFeatureList : " + ex.message);
		}
	};

	this.getPremiumMultipleList = function(pglId, isTermAttached, policyTerm, planCodeList, plancode, sumAssured){
		var dfd = $q.defer();
		debug("plancode: " + plancode);
		debug("pglId: " + pglId);
		debug("isTermAttached: " + isTermAttached);
		debug("policyTerm: " + policyTerm);
		debug("sumAssured: " + sumAssured);
		debug("planCodeList: " + JSON.stringify(planCodeList));
		var prodType = this.getProductDetails(isTermAttached).LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE;
		var age = this.getInsuredAge();
		var gender = this.sisData.sisFormAData.INSURED_GENDER;
		var smokerFlag = this.sisData.sisFormAData.ISSMOKER;
		var query = null;
		var parameterList = [];
		var premMultList = [];
		premMultList.push({"PREM_MULT_DISPLAY": "Select Premium Multiple", "PREM_MULT": null});
			if(!!prodType && prodType=="U"){
				if(!!plancode){
					query = "select distinct pml_prem_mult from lp_pml_premium_multiple_lk,lp_pnl_plan_lk where pnl_pgl_id=? and pml_pnl_id = pnl_id and ? between cast(pnl_min_age as int) and cast(pnl_max_age as int) and (pnl_end_date is null or pnl_end_date >=date('now')) and pml_age = (case when pml_age = '999' then '999' else ? end) and ifnull(pml_term,0) = (case when pnl_prrem_mult_formula = ? then ? else ifnull(pml_term,0) end)";
					parameterList = [pglId+"",age+"",age+"",'3',policyTerm+""];

					if (pglId === IPR_PGL_ID) {
						if(plancode=="IPR1ULN1")
							query += " and pnl_code = ?";
						else
							query += " and pnl_code != ?";
						parameterList.push("IPR1ULN1");
					}
					if (pglId === '182' || pglId === WMX_PGL_ID || pglId === IMX_PGL_ID || pglId === WPR_PGL_ID || pglId === IPR_PGL_ID){
						query += " and pnl_code = ? order by pml_prem_mult";
						parameterList.push(plancode);
					}
					else
						query += " order by pml_prem_mult";
					debug("query: " + query);
					debug("parameterList: " + parameterList);
					CommonService.transaction(sisDB,
						function(tx){
							CommonService.executeSql(tx,query,parameterList,
								function(tx,res){
									if(!!res && res.rows.length>0){
										debug("result: " + JSON.stringify(res.rows.item(0)));
										for(var i=0;i<res.rows.length;i++){
											premMultList.push({"PREM_MULT_DISPLAY": res.rows.item(i).PML_PREM_MULT, "PREM_MULT": res.rows.item(i).PML_PREM_MULT});
										}
										debug("premMultList: " + JSON.stringify(premMultList));
										dfd.resolve(premMultList);
									}
									else
										dfd.resolve(premMultList);
								},
								function(tx,err){
									dfd.resolve(premMultList);
								}
							);
						},
						function(err){
							dfd.resolve(premMultList);
						}
					);
				}
				else
					dfd.resolve(premMultList);
			}
			else{
				if(!!plancode){
				    var pnlId = null;
				    var tempPlanCodeList = $filter('filter')(planCodeList, {"PNL_CODE": plancode});
				    debug("tempPlanCodeList : " + JSON.stringify(tempPlanCodeList));
				    if(!!tempPlanCodeList && tempPlanCodeList.length>0)
					    pnlId = tempPlanCodeList[0].PNL_ID;
					query = "select pml_prem_mult from lp_pml_premium_multiple_lk where pml_pnl_id=? and pml_sex in (?,?) and pml_age in (?,?)";
					parameterList = [pnlId,gender,'U',age,'999'];
					if(!!policyTerm){
						query += " and pml_term in (?,?)";
						parameterList.push(policyTerm);
						parameterList.push("0");
					}
					if(!!smokerFlag){
						query += " and smoker_flagag=?";
						parameterList.push(smokerFlag);
					}
					if(pglId==MLG_PGL_ID){ // MLG
						query += " and pml_band in (select min(pba_band) from lp_pba_product_band where pba_plan_code=? and pba_type=? and pba_sum_assured>=?)";
						parameterList = parameterList.concat([plancode,'B',sumAssured||null]);
					}
					debug("query: " + query);
					debug("parameterList: " + parameterList);

					CommonService.transaction(sisDB,
						function(tx){
							CommonService.executeSql(tx,"select pmd_disc_rate from lp_pmd_plan_premium_mult_disc where pmd_plan_code=? and cast(pmd_min_sa as int)<=? and cast(pmd_max_sa as int)>=?",[plancode, sumAssured, sumAssured],
								function(tx,selRes){
									CommonService.transaction(sisDB,
										function(tx1){
											CommonService.executeSql(tx1,query,parameterList,
												function(tx1,res){
													if(!!res && res.rows.length>0){
														var discount = 0;
														if(!!selRes && selRes.rows.length>0)
															discount = parseFloat(selRes.rows.item(0).PMD_DISC_RATE);
														var finalPM = 1;
														if(pglId == "224" || pglId == "225")
															finalPM = parseFloat(Math.round((res.rows.item(0).PML_PREM_MULT * (1 - discount)) * 100) / 100).toFixed(2)
														else
															finalPM = res.rows.item(0).PML_PREM_MULT - discount;
														debug("result: " + finalPM);
														dfd.resolve(finalPM);
													}
													else
														dfd.resolve(null);
												},
												function(err){
													dfd.resolve(null);
												}
											);
										},
										function(err){
											dfd.resolve(null);
										}
									);
								}
							);
						}
					);
				}
				else
					dfd.resolve(null);
			}
		return dfd.promise;
	};

	this.getMinSumAssured = function(pglId, isTermAttached, planCodeList, plancode){
		var minSA = null;
		var retMinSumAssured = null;
		var tempPlanCodeList = null;
		debug("getMinSumAssured: " + pglId + " " + plancode);
		if(!!plancode && pglId!=VCP_PGL_ID){
		    tempPlanCodeList = $filter('filter')(planCodeList, {"PNL_CODE": plancode});
		    if(!!tempPlanCodeList && tempPlanCodeList.length>0)
			    minSA = tempPlanCodeList[0].PNL_MIN_SUM_ASSURED;
			retMinSumAssured = (!!minSA && minSA=="0")?null:minSA;
			return retMinSumAssured;
		}
		else{
			if(!!planCodeList){
				debug("min SA planCodeList: " + JSON.stringify(planCodeList));
				angular.forEach(planCodeList, function(planDet){
					if(!minSA)
						minSA = planDet.PNL_MIN_SUM_ASSURED;
					else if((!!minSA) && (!!planDet.PNL_MIN_SUM_ASSURED) && (parseInt(minSA) > parseInt(planDet.PNL_MIN_SUM_ASSURED)))
						minSA = planDet.PNL_MIN_SUM_ASSURED;
					/*VCP Min Prem & Min SA CR*/
					if(pglId == VCP_PGL_ID)
					    minSA = 2001000;
				});
			}
			else{
				debug("getMinSumAssured - planCodeList is null");
			}
			retMinSumAssured = (!!minSA && minSA!="0")?minSA:null;
			debug("this.getMinSumAssured(): " + retMinSumAssured);
			return retMinSumAssured;
		}
	};

	this.getMaxSumAssured = function(pglId, isTermAttached, planCodeList, plancode){
		var maxSA = null;
		var age = this.getInsuredAge();
		var retMaxSumAssured = null;
		if(!!plancode && pglId!=VCP_PGL_ID){
			maxSA = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_PLAN_LK, {"PNL_CODE": plancode})[0].PNL_MAX_SUM_ASSURED;
			if(pglId==MLM_PGL_ID){
				if(age>=12 && age<=17){
					// if prod is mahalife magic and age is 12-17 yrs. the max sum assured limit is 2 crores (20000000)
					maxSA = "20000000";
				}
			}
			if(age<18 && (pglId!=IPR_PGL_ID && pglId!=WPR_PGL_ID && pglId!=IMX_PGL_ID && pglId!=WMX_PGL_ID && pglId!=MIP_PGL_ID && pglId!=SGP_PGL_ID && pglId!=MBP_PGL_ID)){
				//Umesh Zajam asked to remove on 16-11-2016 for all ULIP products
				if(maxSA>20000000)
					maxSA = "20000000";
			}
			if(pglId==MMX_PGL_ID){
                if(age<=17){
                	//Umesh Zajam asked to add on 27-01-2017 for MONEY MAXIMA products
                    // If prod is MONEY MAXIMA age is LESS THAN EQUAL TO 17 yrs. the max sum assured limit is 3 crores (30000000)
                    maxSA = "30000000";
                }
            }
			retMaxSumAssured = (!!maxSA && maxSA=="0")?null:maxSA;
			return retMaxSumAssured;
		}
		else{
			if(!!planCodeList){
				angular.forEach(planCodeList, function(planDet){
					if(!maxSA)
						maxSA = planDet.PNL_MAX_SUM_ASSURED;
					else if((!!maxSA) && (!!planDet.PNL_MAX_SUM_ASSURED) && (parseInt(maxSA) < parseInt(planDet.PNL_MAX_SUM_ASSURED)))
						maxSA = planDet.PNL_MAX_SUM_ASSURED;
				});
			}
			else{
				debug("getMaxSumAssured - planCodeList is null");
			}
			if(age<18 && (pglId!=IPR_PGL_ID && pglId!=WPR_PGL_ID && pglId!=IMX_PGL_ID && pglId!=WMX_PGL_ID && pglId!=MIP_PGL_ID && pglId!=SGP_PGL_ID && pglId!=MBP_PGL_ID)){
				//Umesh Zajam asked to remove on 16-11-2016 for all ULIP products
				if(maxSA>20000000)
					maxSA = "20000000";
			}
			if(pglId==MMX_PGL_ID){
                if(age<=17){
                	//Umesh Zajam asked to add on 27-01-2017 for MONEY MAXIMA products
                    // if prod is MONEY MAXIMA age is LESS THAN EQUAL TO 17 yrs. the max sum assured limit is 3 crores (30000000)
                    maxSA = "30000000";
                }
            }
			retMaxSumAssured = (!!maxSA && maxSA!="0")?maxSA:null;
			debug("this.getMaxSumAssured(): " + retMaxSumAssured);
			return retMaxSumAssured;
		}
	};


	this.getMinPremium = function(pglId, isTermAttached, planCodeList, plancode, startingPoint){
		try{
			var minPrem = null;
			var age = this.getInsuredAge();
			var retMinPremium = null;
			if(!!plancode && pglId!=MIP_PGL_ID){
			debug("getMinPremium 1");
				minPrem = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_PLAN_LK, {"PNL_CODE": plancode})[0].PNL_MIN_PREMIUM;
			}
			else{
				debug("getMinPremium 2");
				if(!!planCodeList){
					debug("getMinPremium 3");
					angular.forEach(planCodeList, function(planDet){
						if(!minPrem)
							minPrem = planDet.PNL_MIN_PREMIUM;
						else if((!!minPrem) && (parseInt(minPrem) > parseInt(planDet.PNL_MIN_PREMIUM)))
							minPrem = planDet.PNL_MIN_PREMIUM;
					});
				}
				else{
					debug("getMinPremium - planCodeList is null");
				}
			}
			if(!!startingPoint && startingPoint=="S"){
				debug("getMinPremium 4");
				var minBaseAnnPrem = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PGL_GRP_MIN_PREM_LK, {"PGL_PREMIUM_MODE": "A"})[0].PGL_MIN_PREM_AMT;
				if(((!!minBaseAnnPrem && parseInt(minBaseAnnPrem)>parseInt(minPrem))||(!minPrem || minPrem=="0")) && (pglId!=SR_PGL_ID && pglId!=SRP_PGL_ID)){
					debug("getMinPremium 5");
					retMinPremium = minBaseAnnPrem || null;
					//VCP min Prem CR
					if(pglId==VCP_PGL_ID){
						debug("getMinPremium 6");
						/*if(age < 30)
							retMinPremium = 10000;
						else
							retMinPremium = 5000;*/
						//CR by Umesh
						retMinPremium = 5000;
					}
					/* Below code for ULIP IBL CR
					if(pglId==IPR_PGL_ID || pglId==IMX_PGL_ID){
						if(parseInt(retMinPremium)<100000)
							retMinPremium = "100000";
					}
					 Above code for ULIP IBL CR - END */
					return retMinPremium;
				}
				else{
					debug("getMinPremium 7");
					retMinPremium = (!!minPrem && minPrem=="0")?null:minPrem;
					//VCP min Prem CR
					if(pglId==VCP_PGL_ID){
						/*if(age < 30)
							retMinPremium = 10000;
						else
							retMinPremium = 5000;*/
						//CR by Umesh
						retMinPremium = 5000;
					}
					/* Below code for ULIP IBL CR
					if(pglId==IPR_PGL_ID || pglId==IMX_PGL_ID){
						if(parseInt(retMinPremium)<100000)
							retMinPremium = "100000";
					}
					 Above code for ULIP IBL CR - END */
					return retMinPremium;
				}
			}
			else{
				debug("getMinPremium 8");
				retMinPremium = (!!minPrem && minPrem=="0")?null:minPrem;
				// Below code for SIP 7 IBL CR
				if((BUSINESS_TYPE=="IndusSolution") && pglId==SIP_PGL_ID && !!plancode && plancode.indexOf("SIP7")!=-1){
					debug("getMinPremium 9");
					retMinPremium = "50000";
				}
				if((BUSINESS_TYPE=="iWealth") && pglId==SIP_PGL_ID){
					debug("getMinPremium 10");
					retMinPremium = "50000";
				}
				// Above code for SIP 7 IBL CR - END
				return retMinPremium;
			}
		}
		catch(ex){
			debug("Exception in getMinPremium: " + ex.message);
		}
	};

	this.getMaxPremium = function(isTermAttached, planCodeList, plancode){
		var maxPrem = null;
		var retMaxPremium = null;
		if(!!plancode){
			var pglRec = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PGL_GRP_MIN_PREM_LK, {"PGL_PREMIUM_MODE": "A"})[0];
			maxPrem = (!!pglRec)?(pglRec.PGL_MAX_PREM_AMT):null;
			retMaxPremium = (!!maxPrem && maxPrem=="0")?null:maxPrem;
		}
		return retMaxPremium;
	};

	this.getOption = function(pglId, planCode, optionList){
		if(!!optionList && !!planCode){
			var opt = null;

			if(planCode.indexOf("IV1N1")!=-1)
				opt = "IV1N1";
			else if(planCode.indexOf("EV1N1")!=-1)
				opt = "EV1N1";
			else if(planCode.indexOf("SGPV1N1")!=-1)
				opt = "SGPV1N1";
			else if(planCode.indexOf("SGPV1N2")!=-1)
				opt = "SGPV1N2";
			else if(planCode.indexOf("V1P1")!=-1)
				opt = "V1P1";
			else if(planCode.indexOf("V1P2")!=-1)
				opt = "V1P2";
			else
				opt = null;
			if(pglId==VCP_PGL_ID){
				if(planCode.indexOf("VCPSPV")!=-1 || planCode.indexOf("VCPFPV")!=-1)
					opt = "Pro Care";
				else if(planCode.indexOf("VCPSPPV")!=-1 || planCode.indexOf("VCPFPPV")!=-1)
					opt = "Pro Care Plus";
				else if(planCode.indexOf("VCPSDV")!=-1 || planCode.indexOf("VCPFDV")!=-1)
					opt = "Duo Care";
				else if(planCode.indexOf("VCPSDPV")!=-1 || planCode.indexOf("VCPFDPV")!=-1)
					opt = "Duo Care Plus";
			}
			if(pglId==SR_PGL_ID || pglId==SRP_PGL_ID){
				if(planCode.indexOf("V1N1")!=-1)
					opt = "V1N1";
				else if(planCode.indexOf("V1N2")!=-1)
					opt = "V1N2";
				else if(planCode.indexOf("V1N3")!=-1)
					opt = "V1N3";
				else if(planCode.indexOf("V1N4")!=-1)
					opt = "V1N4";
			}
			if(pglId==GIP_PGL_ID){
				if(planCode.indexOf("10N1")!=-1)
					opt = "10N1";
				else if(planCode.indexOf("15N1")!=-1)
					opt = "15N1";
			}

			return ($filter('filter')(optionList, {"OPTION": opt})[0]);
		}
		else
			return null;
	};

	this.calculateAnnualPremium = function(sumAssured, premiumMultiple, isTermAttached){
		debug("sumAssured: " + sumAssured + "\tpremiumMultiple: " + premiumMultiple);
		var productType = (sf.getProductDetails(isTermAttached).LP_PGL_PLAN_GROUP_LK.PGL_PRODUCT_TYPE);
		if(productType == "C")
			return Math.round((sumAssured * premiumMultiple)/1000);
		else
			return Math.round(sumAssured / premiumMultiple);
	};

	this.calculateSumAssured = function(annualPremium, premiumMultiple){
		return Math.round(annualPremium * premiumMultiple);
	};

	this.calculateBasePremium = function(annualPremium, premiumPayMode, isTermAttached){
		var mdlList = sf.getProductDetails(isTermAttached).LP_MDL_MODAL_FACTOR_LK;
		debug("annualPremium: " + annualPremium);
		debug("premiumPayMode: " + premiumPayMode);
		debug("isTermAttached: " + isTermAttached);
		debug("mdlList: " + JSON.stringify(mdlList));
		var modalFactor = ($filter('filter')(mdlList, {"MDL_PAY_FREQ": premiumPayMode})[0]).MDL_MODAL_FACTOR;
		return Math.round(annualPremium * modalFactor);
	};

	this.getMinPolTerm = function(plancode, pglId, isTermAttached){
		var age = this.getInsuredAge();
		var minPolTerm = null;
		var min_term =  $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].MIN_TERM;
		var vesting_age = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].MIN_VESTING_AGE;
		var matPglIds = [MLGP_PGL_ID, MLG_PGL_ID, IMX_PGL_ID, WMX_PGL_ID, SUA_PGL_ID, FR_PGL_ID];
		if(matPglIds.indexOf(pglId)!=-1){
			var maturity_age = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].MATURITY_AGE;
			minPolTerm = (maturity_age - age);
		}
		else{
			if(!!vesting_age && ((vesting_age-age)>min_term))
				minPolTerm = vesting_age;
			else
				minPolTerm = min_term;
		}
		return minPolTerm;
	};

	this.getMaxPolTerm = function(plancode, pglId, isTermAttached){
		var age = this.getInsuredAge();
		var maxPolTerm = null;
		var max_term = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].MAX_TERM;
		var maturity_age = $filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PNL_CODE": plancode})[0].MATURITY_AGE;
		var matPglIds = [MLGP_PGL_ID, MLG_PGL_ID, IMX_PGL_ID, WMX_PGL_ID, SUA_PGL_ID, FR_PGL_ID];
		if(matPglIds.indexOf(pglId)!=-1){
			maxPolTerm = (maturity_age - age);
		}
		else{
			if(!!maturity_age && ((maturity_age-age)<max_term))
				maxPolTerm = (maturity_age-age);
			else
				maxPolTerm = max_term;
		}
		return maxPolTerm;
	};


	this.getPlanCode = function(planCodeList, ppt, option, premium, sumAssured, pglId, isTermAttached){
		debug("ppt: " + ppt);
		debug("option: " + option);
		debug("sumassured: " + sumAssured);
		debug("planCodeList: " + JSON.stringify(planCodeList));
		var _pcList = [];
		var plancode = null;
		var _pcMaster = [];
		angular.forEach(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK,function(termDet){
			angular.forEach(planCodeList,function(plan){
				if(termDet.PNL_CODE == plan.PNL_CODE)
					_pcMaster.push(termDet);
			});
		});

		if(!!option){
			if(pglId==SIP_PGL_ID || pglId==FR_PGL_ID || pglId==GIP_PGL_ID){ //SIP or Freedom or GIP
				_pcList = $filter('filter')(_pcMaster, {"PPT": ppt});
				debug("_pcList: " + JSON.stringify(_pcList));
				angular.forEach(_pcList,function(plan){
					if((plan.PNL_CODE.indexOf(option))!=-1){
						plancode = plan.PNL_CODE;
						debug("plancode: " + plancode);
					}
				});
			}
			else if(pglId==SGP_PGL_ID){
				plancode = option;
			}
			else if(pglId == VCP_PGL_ID){
				angular.forEach(planCodeList, function(plan){
					if((!!plan.PNL_MIN_SUM_ASSURED && (parseInt(plan.PNL_MIN_SUM_ASSURED)<=parseInt(sumAssured))) && (!!plan.PNL_MAX_SUM_ASSURED && (parseInt(plan.PNL_MAX_SUM_ASSURED)>=parseInt(sumAssured)))){
						if(plan.PNL_CODE.indexOf("D")!=-1){
							if(plan.PNL_CODE.indexOf("DP")!=-1){
								if(option == "Duo Care Plus")
									plancode = plan.PNL_CODE;
							}
							else{
								if(option == "Duo Care")
									plancode = plan.PNL_CODE;
							}
						}
						else{
							if(plan.PNL_CODE.indexOf("PP")!=-1){
								if(option == "Pro Care Plus")
									plancode = plan.PNL_CODE;
							}
							else{
								if(option == "Pro Care")
									plancode = plan.PNL_CODE;
							}
						}
					}
				});
			}
			else if(pglId == SR_PGL_ID || pglId == SRP_PGL_ID){
				debug("_pcMaster : " + JSON.stringify(_pcMaster))
				var pptFilter = ((ppt!="0")?ppt:null);
				debug("pptFilter : " + pptFilter)
				var _planCodeList = $filter('filter')(_pcMaster, {"PPT": pptFilter});
				debug("_planCodeList : " + JSON.stringify(_planCodeList));
				if(!!_planCodeList && _planCodeList.length>1){
					angular.forEach(_planCodeList,function(_plan){
						debug("_plan.PLAN_CODE[_plan.PNL_CODE.length] : " +_plan.PNL_CODE)
						if(!!_plan.PPT && (parseInt(_plan.PPT) == parseInt(pptFilter)) && (_plan.PNL_CODE.indexOf(option)!=-1))
							plancode = _plan.PNL_CODE;
						else if(!_plan.PPT && (_plan.PPT_FLAG=='C') && (_plan.PNL_CODE.indexOf(option)!=-1))
							plancode = _plan.PNL_CODE;
					});
				}
				else {
					if(!!_planCodeList && _planCodeList.length>0)
						plancode = _planCodeList[0].PNL_CODE;
				}
			}
			debug("plancode : " + plancode);
		}
		_pcList = [];

		if(pglId==MIP_PGL_ID && !!premium){
			_pcList = $filter('filter')(_pcMaster, {"PPT": ppt});
			angular.forEach(_pcList,function(plan){
				var planDet = $filter('filter')(planCodeList, {"PNL_CODE": plan.PNL_CODE})[0];
				if(premium>=planDet.PNL_MIN_PREMIUM_BAND && premium<=planDet.PNL_MAX_PREMIUM_BAND)
					plancode = planDet.PNL_CODE;
			});
		}
		_pcList = [];

		if(pglId!=MIP_PGL_ID && pglId!=MRS_PGL_ID && pglId!=SIP_PGL_ID && pglId!=FR_PGL_ID && pglId!=SGP_PGL_ID && pglId!=VCP_PGL_ID && pglId!=SR_PGL_ID && pglId!=SRP_PGL_ID && pglId!=GIP_PGL_ID){
			var pptFilter = ((ppt!="0")?ppt:null);
			if((pglId==IPR_PGL_ID || pglId==IMX_PGL_ID || pglId==WMX_PGL_ID || pglId==WPR_PGL_ID) && (ppt=="10" || ppt=="15" || ppt=="20"))
				pptFilter = null;
			debug("pptFilter : " + pptFilter)
			var _planCodeList = $filter('filter')(_pcMaster, {"PPT": pptFilter});
			debug("_planCodeList : " + JSON.stringify(_planCodeList));
			if(!!_planCodeList && _planCodeList.length>1){
				angular.forEach(_planCodeList,function(_plan){
					if(!!_plan.PPT && (parseInt(_plan.PPT) == parseInt(pptFilter)))
						plancode = _plan.PNL_CODE;
				});
			}
			else {
				if(_planCodeList.length>0)
					plancode = _planCodeList[0].PNL_CODE;
			}
			debug("plancode : " + plancode);
		}
		debug("plancode final: " + plancode);
		debug("returning final: " + JSON.stringify($filter('filter')(planCodeList, {"PNL_CODE": plancode})[0]));
		return $filter('filter')(planCodeList, {"PNL_CODE": plancode})[0];
	};

	this.getSumAssuredMultipleOf = function(planCodeList, plancode, isTermAttached){
		debug("SisFormService fetching sa multiple of");
		return ((!!plancode)?($filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_PLAN_LK, {"PNL_CODE": plancode})[0].PNL_SA_MULTIPLEOF):null);
	};

	this.calculatePolicyTerm = function(ppt, pglId, isTermAttached){
		var pglList = [MIP_PGL_ID, SIP_PGL_ID, SM7_PGL_ID, IWP_PGL_ID, MBP_PGL_ID, GIP_PGL_ID];
		debug("sf.getProductDetails().LP_PNL_TERM_PPT_LK : " + JSON.stringify(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK));
		if(pglList.indexOf(pglId) !== -1){
			return ($filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PPT": ppt})[0]).TERM;
		}
		else if(pglId==VCP_PGL_ID){
			return ppt;
		}
		else{
			if((pglId==IMX_PGL_ID || pglId==WMX_PGL_ID) && (ppt=="10" || ppt=="15" || ppt=="20"))
				ppt = null;
			var maturityAge = ($filter('filter')(sf.getProductDetails(isTermAttached).LP_PNL_TERM_PPT_LK, {"PPT": ppt})[0]).MATURITY_AGE;
			var insuredAge = this.getInsuredAge();
			return (maturityAge - insuredAge);
		}
	};


	this.syncRemainingSIS = function(){
		CommonService.showLoading();
		var promises = [];
		var isCombo = null;
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, "select b.* from lp_sis_main a, lp_document_upload b where a.issynced=? and b.doc_cat_id=a.sis_id and b.doc_cat=? and b.is_data_synced=?", ['Y', 'SIS', 'N'],
					function(tx, res){
						if(!!res && res.rows.length>0){
							var sisList = CommonService.resultSetToObject(res);
							debug("sisList: " + JSON.stringify(sisList));
							if(!!sisList && (!(sisList instanceof Array)))
								sisList = [sisList];
							if(!!sisList && sisList.length>0){
								angular.forEach(sisList, function(sis){
									promises.push(sf.syncDocumentUpload(sis.AGENT_CD, sis.DOC_CAT_ID).then(
										function(syncDocUploadRes){
											if(!!syncDocUploadRes){
												if(!!syncDocUploadRes && syncDocUploadRes=="S"){
													if(!!isCombo){
														return CommonService.updateRecords(db, 'lp_document_capture', {'is_data_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD}).then(
															function(res){
																return syncDocUploadRes;
															}
														);
													}
													else{
														return CommonService.updateRecords(db, 'lp_document_upload', {'is_data_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD}).then(
															function(res){
																return syncDocUploadRes;
															}
														);
													}
												}
												else
													return syncDocUploadRes;
											}
										}
									)
									.then(
										function(syncDocUploadRes){
											return sf.syncSISSignature(sis.AGENT_CD, sis.DOC_CAT_ID).then(
												function(syncSisSignRes){
													debug("in syncSisSignRes " + JSON.stringify(syncSisSignRes));
													if(!!syncSisSignRes && syncSisSignRes=="S"){
														if(!!isCombo){
															return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.AGENT_CD + "_"  + sis.DOC_CAT_ID + "_C02.jpg"}).then(
																function(res){
																	return syncSisSignRes;
																}
															);
														}
														else{
															return CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.AGENT_CD + "_"  + sis.DOC_CAT_ID + "_C02.jpg"}).then(
																function(res){
																	return syncSisSignRes;
																}
															);
														}
													}
													else{
														return syncSisSignRes;
													}
												}
											);
										}
									)
									.then(
										function(syncSisSignRes){
											if(!!syncSisSignRes && syncSisSignRes!='SKIP'){
												return sf.syncSISReport(sis.AGENT_CD, sis.DOC_CAT_ID, isCombo).then(
													function(syncSisReportRes){
														debug("in syncSisReportRes " + JSON.stringify(syncSisReportRes));
														if(!!syncSisReportRes && syncSisReportRes=="S"){
															if(!!isCombo){
																return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.DOC_CAT_ID + ".html"}).then(
																	function(res){
																		return syncSisReportRes;
																	}
																);
															}
															else{
																return CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.DOC_CAT_ID + ".html"}).then(
																	function(res){
																		return syncSisReportRes;
																	}
																);
															}
														}
														else
															return syncSisReportRes;
													}
												);
											}
											else if(!!syncSisSignRes && syncSisSignRes=="SKIP"){
												return "S";
											}
											else
												return null;
										}
									));
								});
							}
							else{
								promises.push(null);
							}
						}
						else{
							CommonService.executeSql(tx, "select b.* from lp_sis_main a, lp_document_upload b where a.issynced=? and b.doc_cat_id=a.sis_id and b.doc_cat=? and b.is_data_synced=? and b.is_file_synced=?", ['Y', 'SIS', 'Y', 'N'],
								function(tx, res1){
									if(!!res1 && res1.rows.length>0){
										var sisList = CommonService.resultSetToObject(res1);
										angular.forEach(sisList, function(sis){
											promises.push(sf.syncSISSignature(sis.AGENT_CD, sis.DOC_CAT_ID).then(
												function(syncSisSignRes){
													debug("in syncSisSignRes " + JSON.stringify(syncSisSignRes));
													if(!!syncSisSignRes && syncSisSignRes=="S"){
														if(!!isCombo){
															return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.AGENT_CD + "_"  + sis.DOC_CAT_ID + "_C02.jpg"}).then(
																function(res){
																	return syncSisSignRes;
																}
															);
														}
														else{
															return CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.AGENT_CD + "_"  + sis.DOC_CAT_ID + "_C02.jpg"}).then(
																function(res){
																	return syncSisSignRes;
																}
															);
														}
													}
													else{
														return syncSisSignRes;
													}
												}
											)
											.then(
												function(syncSisSignRes){
													if(!!syncSisSignRes && syncSisSignRes!='SKIP'){
														return sf.syncSISReport(sis.AGENT_CD, sis.DOC_CAT_ID, isCombo).then(
															function(syncSisReportRes){
																debug("in syncSisReportRes " + JSON.stringify(syncSisReportRes));
																if(!!syncSisReportRes && syncSisReportRes=="S"){
																	if(!!isCombo){
																		return CommonService.updateRecords(db, 'lp_document_capture', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.DOC_CAT_ID + ".html"}).then(
																			function(res){
																				return syncSisReportRes;
																			}
																		);
																	}
																	else{
																		return CommonService.updateRecords(db, 'lp_document_upload', {'is_file_synced': 'Y'}, {'doc_cat_id': sis.DOC_CAT_ID, 'doc_cat': 'SIS', 'agent_cd': sis.AGENT_CD, 'doc_name': sis.DOC_CAT_ID + ".html"}).then(
																			function(res){
																				return syncSisReportRes;
																			}
																		);
																	}
																}
																else
																	return syncSisReportRes;
															}
														);
													}
													else if(!!syncSisSignRes && syncSisSignRes=="SKIP"){
														return "S";
													}
													else
														return null;
												}
											));
										});
									}
									else{
										promises.push("S");
									}
								},
								function(tx, err1){
									promises.push(null);
								}
							)
						}
					},
					function(tx, err) {
						promises.push(null);
					}
				);
			}
		);
		return $q.all(promises).then(
			function(res){
				debug("sync res: " + res);
				CommonService.hideLoading();
			}
		);
	};

	this.fetchDocUploadChanges = function(sisId, agentCd, trmSISId, ekycDocId){
		var dfd = $q.defer();
		debug("sisId: " + sisId);
		debug("agentCd: " + agentCd);
		debug("trmSISId: " + trmSISId);
		CommonService.transaction(db,
			function(tx){
				var query = "select * from lp_document_upload where agent_cd=? and doc_cat=? and doc_cat_id=? and doc_name!=?";
				var parameterList = [agentCd, 'FORM', sisId, sisId+".html"];
				if(!!trmSISId){
					query = "select * from lp_document_upload where agent_cd=? and doc_cat=? and (doc_cat_id=? or doc_cat_id=?) and doc_name not in (?, ?, ?, ?)";
					parameterList = [agentCd, 'FORM', sisId, trmSISId, sisId+".html", trmSISId+".html", "TRMB_"+sisId+".html", "TRMT_"+trmSISId+".html"];
				}
				debug("query: " + query);
				debug("parameterList: " + parameterList);
				CommonService.executeSql(tx, query, parameterList,
					function(tx, res){
						if(!!res && res.rows.length>0){
							var documentUploadList = CommonService.resultSetToObject(res);
							if(!!documentUploadList && (!(documentUploadList instanceof Array))){
								documentUploadList = [documentUploadList];
							}
							debug("res: " + JSON.stringify(documentUploadList));
							if(documentUploadList.length>0){
								var _docUploadList = [];
								for(var i=0; i<documentUploadList.length; i++){
									var currObj = JSON.parse(JSON.stringify(documentUploadList[i]));
									if(!!currObj){
										currObj.DOC_CAT_ID = trmSISId;
										documentUploadList[i].DOC_CAP_ID = CommonService.getRandomNumber();
										currObj.DOC_CAP_ID = CommonService.getRandomNumber();
										currObj.DOC_CAP_REF_ID = documentUploadList[i].DOC_CAP_ID;
										currObj.IS_DATA_SYNCED = null;
										currObj.IS_FILE_SYNCED = null;
										/*if(documentUploadList[i].DOC_NAME.indexOf(".jpg")!=-1){
											// sign
											currObj.DOC_NAME = agentCd + "_" + trmSISId + "_C02.jpg";
										}
										else{*/
											// ekyc
										currObj.DOC_NAME = agentCd + "_" + trmSISId + "_" + ekycDocId.insured.DOC_ID + ".html";
										//}
										_docUploadList.push(documentUploadList[i]);
										_docUploadList.push(currObj);
									}
									else{
										_docUploadList.push(null);
									}
								}
								debug("resolving documentUploadList: " + JSON.stringify(_docUploadList));
								dfd.resolve(_docUploadList);
							}
							else {
								debug("documentUploadList.length: " + documentUploadList.length);
								dfd.resolve(null);
							}
						}
						else{
							dfd.resolve("S");
						}
					},
					function(tx, err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.updateDocUploadList = function(docUploadList){
		var promises = [];
		if(!!docUploadList && docUploadList.length>0){
			for(var i=0;i<docUploadList.length; i++){
				promises.push(
					CommonService.insertOrReplaceRecord(db, "lp_document_upload", docUploadList[i], true).then(
						function(res){
							return "S";
						}
					)
				);
			}
		}
		else{
			promises.push(null);
		}
		return $q.all(promises);
	};

	this.docUploadChangesForTerm = function(sisId, agentCd, trmSISId){
		var dfd = $q.defer();
		CommonService.getEKYCDocumentID().then(
			function(ekycDocId){
				return sf.fetchDocUploadChanges(sisId, agentCd, trmSISId, ekycDocId);
			}
		)
		.then(
			function(docUploadList){
				debug("fetchDocUploadChanges: " + ((!!docUploadList)?(JSON.stringify(docUploadList)):null));
				if(!!docUploadList && docUploadList=="S")
					return [docUploadList];
				return sf.updateDocUploadList(docUploadList);
			}
		)
		.then(
			function(updateRes){
				debug("updateRes: " + JSON.stringify(updateRes));
				if(!!updateRes && updateRes.length>0){
					var ret = "S";
					for(var i=0; i<updateRes.length; i++){
						if(!updateRes[i] || updateRes[i]!="S"){
							ret = null;
							break;
						}
					}
					dfd.resolve(ret);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};


	this.resaveDOBInEKYCOutputIfRequired = function(agentCd, sisId, type, newDob){
		var dfd = $q.defer();
		try{
			if(!!agentCd && !!sisId && !!type && !!newDob){
				CommonService.getEKYCDocumentID().then(function(eKycData){
					if(!!eKycData){
						var fileName = agentCd + "_" + sisId + "_" + eKycData[type].DOC_ID;
						CommonService.readFile(fileName + ".html", "/FromClient/APP/HTML/").then(
							function(res){
								if(!!res){
									var ekycFileData = res;
									ekycFileData = CommonService.replaceAll(ekycFileData, "||USER_ENT_DOB||", newDob.substring(0,10));
									ekycFileData = CommonService.replaceAll(ekycFileData, "<div style='display: none'>", "<div>");
									CommonService.saveFile(fileName, ".html", "/FromClient/APP/HTML/", ekycFileData, "N").then(
										function(saveFileRes){
											if(!!saveFileRes && saveFileRes=="success"){
												dfd.resolve("S");
											}
											else{
												dfd.resolve(null);
											}
										}
									);
								}
								else{
									debug("E-KYC file not found");
									dfd.resolve(null);
								}
							}
						);
					}
					else{
						dfd.resolve(null);
					}
				});
			}
			else{
				dfd.resolve(null);
			}
		}
		catch(ex){
			debug("Exception in resaveDOBInEKYCOutputIfRequired(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.syncTermValidationData = function(agentCd, leadId, sisId){
		var dfd = $q.defer();
		var termValidateSyncRequest = {"REQ":{"ACN":"TVD","AC":agentCd,"PWD":LoginService.lgnSrvObj.password,"DVID":localStorage.DVID,"TRMVAD":[]}};
		sf.getTermValidationData(agentCd, leadId, sisId).then(
			function(termValRes){
				debug("in termValRes");
				if(!!termValRes){
					debug("in termValRes : " + JSON.stringify(termValRes));
					termValidateSyncRequest.REQ.TRMVAD.push(termValRes);
				}
				return termValRes;
			}
		).then(
			function(termValRes){
				if(!!termValRes && (termValRes!="S" && termValRes!="NA")){
					return CommonService.ajaxCall(TERM_VALIDATION_URL, AJAX_TYPE, TYPE_JSON, termValidateSyncRequest, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
							return ajaxResp;
						},
						function(data, status, headers, config, statusText){
							return null;
						}
					);
				}
				else
					return termValRes;
			}
		).then(
			function(ajaxResp){
				debug("resolving syncResTermVal " + JSON.stringify(ajaxResp));
				if(!!ajaxResp && ((ajaxResp=="S")  ||  (!!ajaxResp.data && !!ajaxResp.data.RS && !!ajaxResp.data.RS.SISDET && !!ajaxResp.data.RS.SISDET[0] && !!ajaxResp.data.RS.SISDET[0].RESP && ajaxResp.data.RS.SISDET[0].RESP=="S"))){
					dfd.resolve("S");
				}
				else if(ajaxResp=="NA"){
					dfd.resolve("NA");
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getTermValidationData = function(agentCd, leadId, sisId){
		var dfd = $q.defer();
		var query = "SELECT ISSYNCED AS ISSYNCED, SIS_ID AS SISID, FHR_ID AS FHID, LEAD_ID AS LEAD, POLICY_NO AS PNO, GROUP_EMPLOYEE AS GEMP, NATIONALITY AS NTLTY, RESIDENT_STATUS AS RESI, RESIDENT_STATUS_CODE AS RESICD, CURR_RESIDENT_COUNTRY AS RCNT, CURR_RESIDENT_COUNTRY_CODE AS RCNTCD, CURR_STATE AS CST, CURR_STATE_CODE AS CSTCD, CURR_CITY AS CCTY, CURR_CITY_CODE AS CCTYCD, EDUCATION_QUALIFICATION AS EDU, EDUCATION_QUALIFICATION_CODE AS EDUCD, OCCUPATION_CLASS AS OCCL, OCCUPATION_CLASS_NBFE_CODE AS OCCLCD, INCOME_PROOF AS INP, INCOME_PROOF_DOC_ID AS INPDCID, S_FRM16_YEAR1 AS SFY1, S_FRM16_YEAR2 AS SFY2, S_FRM16_YEAR3 AS SFY3, S_FRM16_SAL1 AS SFS1, S_FRM16_SAL2 AS SFS2, S_FRM16_SAL3 AS SFS3, S_EMP_ISSDT AS SEID, S_EMP_SAL AS SES, S_BNK_MONTH_YEAR1 AS SBMY1, S_BNK_MONTH_YEAR2 AS SBMY2, S_BNK_MONTH_YEAR3 AS SBMY3, S_BNK_MONTH_YEAR4 AS SBMY4, S_BNK_MONTH_YEAR5 AS SBMY5, S_BNK_MONTH_YEAR6 AS SBMY6, S_BNK_SAL1 AS SBS1, S_BNK_SAL2 AS SBS2, S_BNK_SAL3 AS SBS3, S_BNK_SAL4 AS SBS4, S_BNK_SAL5 AS SBS5, S_BNK_SAL6 AS SBS6, S_SAL_MONTH_YEAR1 AS SSMY1, S_SAL_MONTH_YEAR2 AS SSMY2, S_SAL_MONTH_YEAR3 AS SSMY3, S_SAL_SAL1 AS SSS1, S_SAL_SAL2 AS SSS2, S_SAL_SAL3 AS SSS3, S_COI_YEAR1 AS SCY1, S_COI_YEAR2 AS SCY2, S_COI_YEAR3 AS SCY3, S_COI_SAL1 AS SCS1, S_COI_SAL2 AS SCS2, S_COI_SAL3 AS SCS3, SE_FRM16_YEAR1 AS SEFY1, SE_FRM16_YEAR2 AS SEFY2, SE_FRM16_YEAR3 AS SEFY3, SE_FRM16_SAL1 AS SEFS1, SE_FRM16_SAL2 AS SEFS2, SE_FRM16_SAL3 AS SEFS3, SE_COI_YEAR1 AS ECY1, SE_COI_YEAR2 AS ECY2, SE_COI_YEAR3 AS ECY3, SE_COI_PROFIT1 AS ECP1, SE_COI_PROFIT2 AS ECP2, SE_COI_PROFIT3 AS ECP3, SE_COI_SAL1 AS ECS1, SE_COI_SAL2 AS ECS2, SE_COI_SAL3 AS ECS3, SE_OTH_DOC AS SOD, SE_OTH_PROFIT AS SOP, UE_HOUSE_PROP AS UHP, UE_RENT AS URT, UE_GAIN_DIV AS UGD, UE_AGRI AS UAGR, UE_IOTH AS UITH, UE_GIFT AS UGFT, UE_OTH AS UTH, SAR AS SAR, ELIGIBILITY_FLAG AS EF, ELIGIBILITY_CATEGORY AS EC, ELIGIBILITY_REASON AS ER, ELIGIBILITY_JUSTIFICATION AS EJ, IBL_JOINING_DATE AS IJD, OTHER_CITY as OCITY, CITY_TERM_FLAG AS CITYTF, COUNTRY_TERM_FLAG AS CNTRTF, ELIGIBILITY_FLAG_CITY AS EFCITY, ELIGIBILITY_FLAG_COUNTRY AS EFCNTRY, ELIGIBILITY_FLAG_OCCCLASS AS EFOCC, ELIGIBILITY_FLAG_EDUCATION AS EFEDU, ELIGIBILITY_FLAG_INCOME AS EFINC, AVG_INCOME AS AVGINC, ELIGIBILITY_VALUE AS ELGVAL, ELIGIBILITY_FLAG_OCC AS EFOC FROM LP_TERM_VALIDATION WHERE AGENT_CD=? AND SIS_ID=?";
		var parameterList = [agentCd, sisId];
		if(!!leadId){
			query +=" AND LEAD_ID=?";
			 parameterList.push(leadId);
		}
		debug("query : " + query);
		debug("parameterList : " + JSON.stringify(parameterList));
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						if(!!res && res.rows.length>0){
							if(res.rows.item(0).ISSYNCED=='Y'){
								dfd.resolve("S");
							}
							else{
								delete res.rows.item(0).ISSYNCED;
								var termValDet = CommonService.resultSetToObject(res);
								//Added by Akhil for standalone SIS 30 Jan 2017
								termValDet.LEAD = termValDet.LEAD || "0";
								termValDet.FHID = termValDet.FHID || "0";
								termValDet.AVGINC = Math.round(termValDet.AVGINC);
								termValDet.ELGVAL = Math.round(termValDet.ELGVAL);
								dfd.resolve(termValDet || null);
							}
						}
						else
							dfd.resolve("NA");
					},
					function(tx,res){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

}]);
