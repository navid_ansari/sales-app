sisEKYCModule.controller('SisEKycCtrl',['CommonService', 'SisEKycService', 'SisFormService','loadEKYCOpportunityData', 'PurchasingInsuranceForList', 'InsuranceBuyingForList', 'ProductData', 'ExistingData', 'SisFormAData_eKyc', '$filter', '$q', 'SisTimelineService','EKYCData', function(CommonService, SisEKycService, SisFormService, loadEKYCOpportunityData, PurchasingInsuranceForList, InsuranceBuyingForList, ProductData, ExistingData, SisFormAData_eKyc, $filter, $q, SisTimelineService,EKYCData){
	"use strict";
	debug("SisEKycCtrl");
	CommonService.hideLoading();
    debug("loadData" +JSON.stringify(loadEKYCOpportunityData));
    /*SIS Header Calculator*/
    setTimeout(function(){
        var sistimelineHeight = $(".sis-header-wrap").height();
        var sisHeaderHeight = $(".SIStimeline-wrapper").height();
        var sisGetHeight = sistimelineHeight + sisHeaderHeight;
        $(".sis-height .custom-position").css({top:sisGetHeight + "px"});
    });
    /*End SIS Header Calculator*/


	var sc = this;
	this.prodMinAge = SisFormService.getProdMinAge();
	this.prodMaxAge = SisFormService.getProdMaxAge();

	SisTimelineService.setActiveTab("ekyc");

	this.aadharRegex = ADHAAR_REG;

	this.purchasingInsuranceForList = PurchasingInsuranceForList;
	this.insuranceBuyingForList = InsuranceBuyingForList;

	this.showConsent = false;
	this.showInsConsent = false;

	//this.forcedDisableEKYC = FORCED_DISABLE_EKYC;
	this.forcedDisableProEKYC = true;
	this.ekycDisabled = SisFormService.sisData.ekycDisabled;

	this.PGL_ID = SisFormService.sisData.sisMainData.PGL_ID;

	this.ITRP_PGL_ID = ITRP_PGL_ID;
	this.IRS_PGL_ID = IRS_PGL_ID;

	this.VCP_PGL_ID = VCP_PGL_ID;

	debug("SisEKycCtrl PGL_ID: " + this.PGL_ID);

	this.sisEKycData = SisEKycService.sisEKycData || {"insured": {"CUST_TYPE": INSURED_CUST_TYPE}, "proposer": {"CUST_TYPE": PROPOSER_CUST_TYPE}};
	this.sisFormAData_eKyc = SisFormAData_eKyc || {};
	this.insAadharList = EKYCData.insured;
	sc.sisEKycData.insured.CONSUMER_AADHAR_NO = this.insAadharList[0];
	debug("sisEkycCtrl this.sisEKycData: " + JSON.stringify(this.sisEKycData));
	debug("sisEkycCtrl this.sisFormAData_eKyc: " + JSON.stringify(this.sisFormAData_eKyc));

	/*if(!!this.sisEKycData && !!this.sisEKycData.insured && !!this.sisEKycData.insured.CONSUMER_AADHAR_NO){
		this.sisEKycData.insured.CONSUMER_AADHAR_NO = parseInt(this.sisEKycData.insured.CONSUMER_AADHAR_NO);
	}*/
	if(!!EKYCData.insured.CONSUMER_AADHAR_NO && EKYCData.insured.EKYC_FLAG == 'Y' && !!this.sisFormAData_eKyc.EKYC_FLAG && this.sisFormAData_eKyc.EKYC_FLAG == 'Y')
		sc.sisEKycData.insured.CONSUMER_AADHAR_NO = $filter('filter')(EKYCData.insured, {"EKYC_ID": this.sisFormAData_eKyc.EKYC_ID}, true)[0];

	if(!!EKYCData.proposer.CONSUMER_AADHAR_NO && EKYCData.proposer.EKYC_FLAG == 'Y' && !!this.sisFormAData_eKyc.PR_EKYC_FLAG && this.sisFormAData_eKyc.PR_EKYC_FLAG == 'Y'){
		this.sisEKycData.proposer.CONSUMER_AADHAR_NO = parseInt(EKYCData.proposer.CONSUMER_AADHAR_NO);
	}


			if(!!SisFormService.sisData.sisFormAData_eKyc && !!SisFormService.sisData.sisFormAData_eKyc)
				{
					sc.INSURED_EKYC_CONSENT_FLAG_PERSIST = SisFormService.sisData.sisFormAData_eKyc.INSURED_EKYC_CONSENT_FLAG_PERSIST || null;
					sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = SisFormService.sisData.sisFormAData_eKyc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST || null;
					sc.insBuyForDisabled = SisFormService.sisData.sisFormAData_eKyc.insBuyForDisabled;
					debug("insBuyForDisabled "+sc.insBuyForDisabled);
				}
				else
				{
					sc.INSURED_EKYC_CONSENT_FLAG_PERSIST = null;
					sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = null;
					sc.insBuyForDisabled = null;
				}


//	this.INSURED_EKYC_CONSENT_FLAG = this.sisEKycData.insured.EKYC_FLAG || "N";
//	this.PROPOSER_EKYC_CONSENT_FLAG = this.sisEKycData.proposer.EKYC_FLAG || "N";
	debug("loadData" +JSON.stringify(loadEKYCOpportunityData));

	this.INSURED_EKYC_FLAG = "N";
	this.PROPOSER_EKYC_FLAG = "N";

	this.purchaseForOptionSelected = $filter('filter')(this.purchasingInsuranceForList, {"PUR_NBFE_CODE": this.sisEKycData.insured.PUR_FOR_CD})[0] || this.purchasingInsuranceForList[0];
	this.buyingForOptionSelected = $filter('filter')(this.insuranceBuyingForList, {"BUY_NBFE_CODE": this.sisEKycData.insured.BUY_FOR_CD})[0] || this.insuranceBuyingForList[0];
	if(!this.sisEKycData.insured.BUY_FOR_CD && !!loadEKYCOpportunityData && !!loadEKYCOpportunityData.BUY_FOR)
		{
			this.buyingForOptionSelected = $filter('filter')(this.insuranceBuyingForList, {"BUY_NBFE_CODE": loadEKYCOpportunityData.BUY_FOR})[0];
			this.INSURED_EKYC_FLAG = (!!loadEKYCOpportunityData.INS_EKYC_FLAG ? loadEKYCOpportunityData.INS_EKYC_FLAG:"N") || "N";
			this.PROPOSER_EKYC_FLAG = (!!loadEKYCOpportunityData.PR_EKYC_FLAG ? loadEKYCOpportunityData.PR_EKYC_FLAG:"N") || "N";
			if(loadEKYCOpportunityData.BUY_FOR == '200')
			{
				var _temp = EKYCData.proposer;
				_temp.CUST_TYPE = 'C01';
				_temp = [_temp];
				sc.insAadharList = _temp;
		  }
			if(loadEKYCOpportunityData.INS_EKYC_FLAG == 'Y')
				{
					if(loadEKYCOpportunityData.BUY_FOR == '200')
						this.sisEKycData.insured.CONSUMER_AADHAR_NO = $filter('filter')(sc.insAadharList, {"EKYC_ID": loadEKYCOpportunityData.INS_EKYC_ID}, true)[0];
					else
						this.sisEKycData.insured.CONSUMER_AADHAR_NO = $filter('filter')(sc.insAadharList, {"EKYC_ID": loadEKYCOpportunityData.INS_EKYC_ID}, true)[0];
				}
			if(loadEKYCOpportunityData.PR_EKYC_FLAG == 'Y')
				this.sisEKycData.proposer.CONSUMER_AADHAR_NO = parseInt(EKYCData.proposer.CONSUMER_AADHAR_NO);
		}

	if(!!sc.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR){
		this.purchaseForOptionSelected = $filter('filter')(this.purchasingInsuranceForList, {"PUR_NBFE_CODE": this.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE})[0];
	}
	else{
		this.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR = this.purchaseForOptionSelected.PURCHASE_FOR;
		this.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = this.purchaseForOptionSelected.PUR_NBFE_CODE;
		this.sisEKycData.insured.PUR_FOR_CD = this.purchaseForOptionSelected.PUR_NBFE_CODE;
	}

	if(!!sc.sisFormAData_eKyc.INSURANCE_BUYING_FOR){
		this.buyingForOptionSelected = $filter('filter')(this.insuranceBuyingForList, {"BUY_NBFE_CODE": this.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE})[0];
	}
	else{
		this.sisFormAData_eKyc.INSURANCE_BUYING_FOR = this.buyingForOptionSelected.BUYING_FOR;
		this.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = this.buyingForOptionSelected.BUY_NBFE_CODE;
		this.sisEKycData.insured.BUY_FOR_CD = this.buyingForOptionSelected.BUY_NBFE_CODE;
	}

	if(!!SisFormService.sisData.sisMainData && !!this.sisEKycData){
		this.insuredEKYCAlreadyDone = (SisFormService.sisData.sisMainData.IS_SCREEN_A_COMPLETED=="Y") && this.sisEKycData.insured.EKYC_DONE_FLAG;
		this.proposerEKYCAlreadyDone = (SisFormService.sisData.sisMainData.IS_SCREEN_A_COMPLETED=="Y") && this.sisEKycData.proposer.EKYC_DONE_FLAG;
	}
	else{
		this.insuredEKYCAlreadyDone = false;
		this.proposerEKYCAlreadyDone = false;
	}


	if(!!SisFormService.sisData.sisMainData.LEAD_ID){
		// Only enter if proposer is not null
		if(!!this.sisEKycData.BUY_FOR_CD && this.sisEKycData.BUY_FOR_CD=="200"){
			this.leadDataDisabled = true;
		}
		else {
			this.leadDataDisabled = false;
		}
	}
	else {
		this.leadDataDisabled = false;
	}

	this.onAddAadharFlagChange = function(type){
		if(!!type && type=="insured"){
			if(sc.buyingForOptionSelected.BUY_NBFE_CODE == '200')
				{
					var _temp = EKYCData.proposer;
					_temp.CUST_TYPE = 'C01';
					_temp = [_temp];
					sc.insAadharList = _temp;
				}
			if(this.INSURED_EKYC_FLAG == 'Y' && !!sc.insAadharList && sc.insAadharList.length>0 && (sc.insAadharList[0].EKYC_FLAG=='Y' || (!!sc.insAadharList[1] && sc.insAadharList[1].EKYC_FLAG == 'Y'))){
				sc.sisFormAData_eKyc.EKYC_FLAG = 'Y'
				if(sc.buyingForOptionSelected.BUY_NBFE_CODE == '200')
				{
					this.sisEKycData.insured.CONSUMER_AADHAR_NO = sc.insAadharList[0];
				}
				/*this.INSURED_EKYC_CONSENT_FLAG_PERSIST = null;
				this.INSURED_EKYC_CONSENT_FLAG = null;
				this.sisEKycForm.insuredAadharNum.$validate();*/
			}
			else {

				sc.sisFormAData_eKyc.EKYC_FLAG = 'N'
				this.sisEKycForm.insuredAadharNum.$validate();
				if(this.INSURED_EKYC_FLAG == 'Y')
					{
							sc.INSURED_EKYC_FLAG = 'N';
							navigator.notification.confirm("Insured Aadhar details not added in Lead or Invalid Aadhar details.");
					}
					else
					{
						sc.insAadharList = EKYCData.insured;
						this.sisEKycData.insured.CONSUMER_AADHAR_NO = sc.insAadharList[0];
					}
			}
		}
		else{

			if(sc.PROPOSER_EKYC_FLAG == 'Y' && !!EKYCData.proposer && EKYCData.proposer.EKYC_FLAG=="Y" && !!EKYCData.proposer.CONSUMER_AADHAR_NO && !!parseInt(EKYCData.proposer.CONSUMER_AADHAR_NO)){
				this.sisEKycData.proposer.CONSUMER_AADHAR_NO = parseInt(EKYCData.proposer.CONSUMER_AADHAR_NO);
			}
			else {

				this.sisEKycForm.proposerAadharNum.$validate();
				this.sisEKycData.proposer.CONSUMER_AADHAR_NO = null;
				if(this.PROPOSER_EKYC_FLAG == 'Y')
					{
						this.PROPOSER_EKYC_FLAG = 'N';
						navigator.notification.confirm("Proposer Aadhar details not added in Lead or Invalid Aadhar details.");
					}
					else
						this.sisEKycData.proposer.CONSUMER_AADHAR_NO = null

			}
		}
	};

	this.onInsAadharNumberChange = function(){
	/*	this.INSURED_EKYC_CONSENT_FLAG_PERSIST = null;
		this.INSURED_EKYC_CONSENT_FLAG = null;*/
		debug(JSON.stringify(this.sisEKycData.insured.CONSUMER_AADHAR_NO));
		sc.sisFormAData_eKyc.EKYC_ID = this.sisEKycData.insured.CONSUMER_AADHAR_NO.CONSUMER_AADHAR_NO;
	};

	this.onPropAadharNumberChange = function(){
	/*	this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = null;
		this.PROPOSER_EKYC_CONSENT_FLAG = null;*/
		debug(JSON.stringify(this.sisEKycData.proposer.CONSUMER_AADHAR_NO));
		sc.sisFormAData_eKyc.PR_EKYC_ID = this.sisEKycData.proposer.CONSUMER_AADHAR_NO;
	};

	this.onPurInsForChg = function(){
		debug("this.purchaseForOptionSelected: " + JSON.stringify(this.purchaseForOptionSelected));
		this.sisEKycData.insured.PUR_FOR_CD = this.purchaseForOptionSelected.PUR_NBFE_CODE;
		this.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR = this.purchaseForOptionSelected.PURCHASE_FOR;
		this.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = this.purchaseForOptionSelected.PUR_NBFE_CODE;
		SisEKycService.getInsuranceBuyingForList(this.purchaseForOptionSelected).then(
			function(insBuyForList){
				sc.insuranceBuyingForList = insBuyForList;
				sc.buyingForOptionSelected = insBuyForList[0];
			}
		);
	};

	this.onInsBuyForChg = function(){
		debug("this.buyingForOptionSelected: " + JSON.stringify(this.buyingForOptionSelected));
		if(!!this.sisEKycData.insured.BUY_FOR_CD && this.sisEKycData.insured.BUY_FOR_CD=="200"){
			if(!this.buyingForOptionSelected.BUY_NBFE_CODE || this.buyingForOptionSelected.BUY_NBFE_CODE!="200"){
				SisEKycService.resetProposerSelfData(SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.FHR_ID);
			}
		}
		this.sisEKycData.insured.BUY_FOR_CD = this.buyingForOptionSelected.BUY_NBFE_CODE;
		this.sisFormAData_eKyc.INSURANCE_BUYING_FOR = this.buyingForOptionSelected.BUYING_FOR;
		this.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = this.buyingForOptionSelected.BUY_NBFE_CODE;

		if(this.buyingForOptionSelected.BUY_NBFE_CODE == '200')
		{
			this.sisFormAData_eKyc.PROPOSER_IS_INSURED = 'Y';
			if(!!EKYCData.proposer && !!EKYCData.proposer.EKYC_FLAG){
				var _temp = EKYCData.proposer;
				_temp.CUST_TYPE = 'C01';
				_temp = [_temp];
				sc.insAadharList = _temp;
		  }
			/*if(!!this.sisEKycData.proposer.CONSUMER_AADHAR_NO && this.sisEKycData.proposer.EKYC_FLAG == 'Y' && this.INSURED_EKYC_FLAG == 'Y'){
				this.sisEKycData.insured.CONSUMER_AADHAR_NO = this.sisEKycData.proposer.CONSUMER_AADHAR_NO;
		  }*/
			this.INSURED_EKYC_FLAG = 'N';
			this.PROPOSER_EKYC_FLAG = 'N';
			this.sisEKycData.proposer.CONSUMER_AADHAR_NO = null;
			this.sisEKycForm.insuredAadharNum.$validate();
			//this.sisEKycData.insured.CONSUMER_AADHAR_NO = $filter('filter')(sc.insAadharList, {"CONSUMER_AADHAR_NO": EKYCData.proposer.CONSUMER_AADHAR_NO}, true) || sc.insAadharList[0];
		}
		else {
			this.sisFormAData_eKyc.PROPOSER_IS_INSURED = 'N';
			this.INSURED_EKYC_FLAG = 'N';
			this.PROPOSER_EKYC_FLAG = 'N';
			sc.insAadharList = EKYCData.insured;
			if(!!EKYCData.proposer.CONSUMER_AADHAR_NO && (sc.sisFormAData_eKyc.PR_EKYC_FLAG == 'Y' || sc.PROPOSER_EKYC_FLAG == 'Y'))
				this.sisEKycData.proposer.CONSUMER_AADHAR_NO = parseInt(EKYCData.proposer.CONSUMER_AADHAR_NO);
			this.sisEKycForm.insuredAadharNum.$validate();
			this.sisEKycForm.proposerAadharNum.$validate();
		}

	};

	this.openConsent = function(type){
		if(type=="insured") {
			this.showInsConsent = true;
			this.showConsent = true;
		}
		else if(type=="proposer") {
			this.showInsConsent = false;
			this.showConsent = true;
		}
	};

	this.onNext = function(isPristine){
		if(sc.INSURED_EKYC_FLAG == 'Y' && (sc.sisEKycData.insured.CONSUMER_AADHAR_NO.CONSUMER_AADHAR_NO == "Select Aadhar Number" || !sc.sisEKycData.insured.CONSUMER_AADHAR_NO.CONSUMER_AADHAR_NO))
		{
			navigator.notification.alert("Please select Insured Aadhar Number");
		}
		else{
			var ekycDetails = {"insured": {}, "proposer": {}};
			ekycDetails.BUY_FOR = sc.buyingForOptionSelected.BUY_NBFE_CODE;
			ekycDetails.insured.EKYC_FLAG = sc.INSURED_EKYC_FLAG || 'N';
			ekycDetails.proposer.EKYC_FLAG = sc.PROPOSER_EKYC_FLAG || 'N';
			ekycDetails.insured.EKYC_ID = null;
			ekycDetails.proposer.EKYC_ID = null;
			if(sc.INSURED_EKYC_FLAG == 'Y')
				{
					ekycDetails.insured.EKYC_ID = sc.sisEKycData.insured.CONSUMER_AADHAR_NO.EKYC_ID;
					sc.INSURED_EKYC_CONSENT_FLAG_PERSIST = 'Y';
					sc.insBuyForDisabled = true;
				}
			if(sc.PROPOSER_EKYC_FLAG == 'Y')
				{
					ekycDetails.proposer.EKYC_ID = EKYCData.proposer.EKYC_ID;
					sc.INSURED_EKYC_CONSENT_FLAG_PERSIST = 'Y';
					sc.insBuyForDisabled = true;
				}
			SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR = sc.purchaseForOptionSelected.PURCHASE_FOR;
			SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = sc.purchaseForOptionSelected.PUR_NBFE_CODE;
			SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR = sc.buyingForOptionSelected.BUYING_FOR;
			SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = sc.buyingForOptionSelected.BUY_NBFE_CODE;
			if(sc.buyingForOptionSelected.BUY_NBFE_CODE == '200')
				{
					SisFormService.sisData.sisFormAData_eKyc.PROPOSER_IS_INSURED = 'Y';
					SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED = 'Y';
				}
			else
				{
					SisFormService.sisData.sisFormAData_eKyc.PROPOSER_IS_INSURED = 'N';
					SisFormService.sisData.sisFormAData.PROPOSER_IS_INSURED = 'N';
				}
			SisFormService.sisData.sisFormAData.PURCHASING_INSURANCE_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE;
			SisFormService.sisData.sisFormAData.PURCHASING_INSURANCE_FOR = SisFormService.sisData.sisFormAData_eKyc.PURCHASING_INSURANCE_FOR;
			SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR;
			SisFormService.sisData.sisFormAData.INSURANCE_BUYING_FOR_CODE = SisFormService.sisData.sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE;
			SisFormService.sisData.sisFormAData_eKyc.INSURED_EKYC_CONSENT_FLAG_PERSIST = sc.INSURED_EKYC_CONSENT_FLAG_PERSIST;
			SisFormService.sisData.sisFormAData_eKyc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST;
			SisFormService.sisData.sisFormAData_eKyc.insBuyForDisabled = sc.insBuyForDisabled;
			if((sc.INSURED_EKYC_FLAG !='Y' && !!sc.sisEKycData.insured && !!sc.sisEKycData.insured.CONSUMER_AADHAR_NO && !!sc.sisEKycData.insured.CONSUMER_AADHAR_NO.EKYC_ID) || (sc.PROPOSER_EKYC_FLAG !='Y' && !!EKYCData.proposer && !!EKYCData.proposer.EKYC_ID && sc.buyingForOptionSelected.BUY_NBFE_CODE!='200'))
			{
				navigator.notification.confirm("Do you want to continue without using e-kyc?",function(buttonIndex) {
					if(buttonIndex=="1"){
									SisEKycService.onNext(ekycDetails).then(function(){
								SisEKycService.saveDataAndMoveOn(isPristine, ekycDetails);
						//	$state.go("sisForm.sfaPage1");
						});
					}
				},"E-KYC",["Yes", "No"]);
			}
			else
			SisEKycService.onNext(ekycDetails).then(function(){
					SisEKycService.saveDataAndMoveOn(isPristine, ekycDetails);
			//	$state.go("sisForm.sfaPage1");
			});
			//$state.go("sisForm.sfaPage1");
		}

	}
}]);

sisEKYCModule.service('SisEKycService',['CommonService', '$q', '$http', 'SisFormService', '$state', 'SisTimelineService', function(CommonService, $q, $http, SisFormService, $state, SisTimelineService){
	"use strict";

	var sc = this;

	this.sisEKycData = null;
	this.sisFormAData_eKyc = null;

	this.loadEKYCOpportunityData = function(oppId, leadId, agentCd){
		var dfd = $q.defer();
		var oppData = {};
		var params =  {"agent_cd": agentCd, "lead_id": leadId};
		if(!!oppId)
			params = {"agent_cd": agentCd, "lead_id": leadId, "opportunity_id": oppId};

		CommonService.selectRecords(db, "lp_myopportunity", true, null,params).then(
			function(res){
				debug("Opp :: res.rows.length: " + res.rows.length);
				if(!!res && res.rows.length>0){
					oppData = CommonService.resultSetToObject(res);
					dfd.resolve(oppData);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};
		this.saveDataAndMoveOn = function(isPristine, ekycDetails){
			CommonService.showLoading("Loading ...");
			debug("saveDataAndMoveOn");
			if(!isPristine || !SisTimelineService.isEKYCTabComplete){
				SisTimelineService.isInsuredTabComplete = false;
				SisTimelineService.isProposerTabComplete = false;
				SisTimelineService.isPlanTabComplete = false;
				if(!!SisTimelineService.isFundTabComplete && SisTimelineService.isFundTabComplete==true)
					SisTimelineService.isFundTabComplete = false;
				if(!!SisTimelineService.isRiderTabComplete && SisTimelineService.isRiderTabComplete==true)
					SisTimelineService.isRiderTabComplete = false;
				if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
					SisTimelineService.isOutputTabComplete = false;
			}
			SisTimelineService.isEKYCTabComplete = true;
			SisFormService.insertDataInFhrMainTable().then(
				function(insFHRMainRes){
					if(!!insFHRMainRes && insFHRMainRes=="S"){
					/*	sc.saveEKYCData().then(
							function(res){*/
							//	debug("saveEKYCData res: " + res);
								//if(!!res){
									SisFormService.insertDataInSisMainTable().then(
										function(insSISMainRes){
											debug("insSISMainRes: " + insSISMainRes);
											if(!!insSISMainRes){
												SisFormService.createOrUpdateOpportunity(SisFormService.sisData.sisMainData.AGENT_CD, SisFormService.sisData.sisMainData.LEAD_ID, SisFormService.sisData.sisMainData.FHR_ID, SisFormService.sisData.sisMainData.SIS_ID, SisFormService.sisData.FHR_SIGNED_TIMESTAMP || null, ekycDetails, true).then(
													function(oppRes){
														if(!!oppRes){
															debug("oppRes: " + oppRes);
																$state.go("sisForm.sfaPage1");


															/*CommonService.updateRecords(db, "lp_ekyc_dtls", {"OPP_ID": SisFormService.sisData.OPP_ID}, {"SIS_ID": SisFormService.sisData.sisMainData.SIS_ID, "AGENT_CD": SisFormService.sisData.sisMainData.AGENT_CD}).then(
																function(updateRes){
																	debug("before going to sfaPage1 in if");
																	$state.go("sisForm.sfaPage1");
																}
															)*/
														}
														else{
															navigator.notification.alert("Cannot proceed.",function(){
																CommonService.hideLoading();
															},"E-KYC","OK");
														}
													}
												);
											}
											else{
												navigator.notification.alert("Cannot proceed.",function(){
													CommonService.hideLoading();
												},"E-KYC","OK");
											}
										}
									);
							/*	}
								else{
									navigator.notification.alert("Could not save e-kyc details.",function(){
										CommonService.hideLoading();
									},"E-KYC","OK");
								}
						}
					);*/
					}
					else{
						navigator.notification.alert("Cannot proceed.",function(){
							CommonService.hideLoading();
						},"E-KYC","OK");
					}
				}
			);
		};

	this.getSisFormAData_eKyc = function(sisFormAData){
		debug("in getSisFormAData_eKyc");
		var sisFormAData_eKyc = {};
		sisFormAData_eKyc.PURCHASING_INSURANCE_FOR = sisFormAData.PURCHASING_INSURANCE_FOR || null;
		sisFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = sisFormAData.PURCHASING_INSURANCE_FOR_CODE || null;
		sisFormAData_eKyc.PROPOSER_IS_INSURED = sisFormAData.PROPOSER_IS_INSURED || null;
		sisFormAData_eKyc.INSURANCE_BUYING_FOR = sisFormAData.INSURANCE_BUYING_FOR || null;
		sisFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = sisFormAData.INSURANCE_BUYING_FOR_CODE || null;
		return sisFormAData_eKyc;
	};

	this.saveEKYCFile = function (type, ekycDetails) {
		var dfd = $q.defer();
		CommonService.getEKYCDocumentID().then(function(eKycData){
			debug("eKycData: "+JSON.stringify(eKycData));
			var eKycDocId = eKycData[type].DOC_ID || null;
			CommonService.insertOrReplaceRecord(db, "lp_document_upload", {"DOC_ID": eKycDocId, "AGENT_CD": SisFormService.sisData.sisMainData.AGENT_CD, "DOC_CAT": "FORM", "DOC_CAT_ID": SisFormService.sisData.sisMainData.SIS_ID, "DOC_NAME": ekycDetails[type].EKYC_ID + ".html", "DOC_TIMESTAMP": CommonService.getCurrDate(), "DOC_PAGE_NO": "0", "IS_FILE_SYNCED": "N", "IS_DATA_SYNCED": "N"}).then(
				function(res){
					if(!!res)
						{
							  dfd.resolve(res);
								debug(type+"EKYC file save success");
						}
					else {
						dfd.resolve(null);
						navigator.notification.confirm("Cannot save EKYC Details!!");
					}

				}
			);
		});
		return dfd.promise;
	}

	this.onNext = function(ekycDetails) {
		var dfd = $q.defer();
		if(ekycDetails.insured.EKYC_FLAG == 'Y')
			sc.saveEKYCFile('insured', ekycDetails).then(function(resp) {
				if(resp!=null && ekycDetails.proposer.PR_EKYC_FLAG == 'Y'){
					sc.saveEKYCFile('proposer', ekycDetails).then(function(pr_resp) {
						if(pr_resp!=null)
							dfd.resolve(true);//
					});
				}
				else
					dfd.resolve(true);//$state.go("sisForm.sfaPage1");
			});
		else if(ekycDetails.proposer.EKYC_FLAG == 'Y'){
			sc.saveEKYCFile('proposer', ekycDetails).then(function() {
					dfd.resolve(true);	//$state.go("sisForm.sfaPage1");
			});
		}
		else
				dfd.resolve(true);//$state.go("sisForm.sfaPage1");

		return dfd.promise;
	}

	this.getPurchasingInsuranceForList = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct pur_nbfe_code,purchase_for from lp_insured_relation where isactive=? order by purchase_for asc",["Y"],
					function(tx,res){
						var purchasingInsuranceForList = [];
						debug("purchasingInsuranceForList res.rows.length: " + res.rows.length);
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var purchasingInsuranceFor = {};
								purchasingInsuranceFor.PURCHASE_FOR = res.rows.item(i).PURCHASE_FOR;
								purchasingInsuranceFor.PUR_NBFE_CODE = res.rows.item(i).PUR_NBFE_CODE;
								purchasingInsuranceForList.push(purchasingInsuranceFor);
							}
							dfd.resolve(purchasingInsuranceForList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getInsuranceBuyingForList = function(purchasingInsuranceFor){
		var dfd = $q.defer();
		var nbfeCodeStr = "";
		var parameterList = ["Y"];
		if(!!purchasingInsuranceFor && !!purchasingInsuranceFor.PUR_NBFE_CODE){
			nbfeCodeStr = "and pur_nbfe_code=? ";
			parameterList.push(purchasingInsuranceFor.PUR_NBFE_CODE || "");
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct buy_nbfe_code,buying_for from lp_insured_relation where isactive=? " + nbfeCodeStr + "order by buying_for asc",parameterList,
					function(tx,res){
						var insuranceBuyingForList = [];
						debug("insuranceBuyingForList res.rows.length: " + res.rows.length);
						if(!!res && res.rows.length>0){
							insuranceBuyingForList.push({"BUYING_FOR": "Select Insurance Buying For", "BUY_NBFE_CODE": null});
							for(var i=0;i<res.rows.length;i++){
								var insuranceBuyingFor = {};
								insuranceBuyingFor.BUYING_FOR = res.rows.item(i).BUYING_FOR;
								insuranceBuyingFor.BUY_NBFE_CODE = res.rows.item(i).BUY_NBFE_CODE;
								insuranceBuyingForList.push(insuranceBuyingFor);
							}
							dfd.resolve(insuranceBuyingForList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.gotoEKYCConfirmationPage = function(isPristine){
		if(!isPristine || !SisTimelineService.isEKYCTabComplete){
			SisTimelineService.isInsuredTabComplete = false;
			SisTimelineService.isProposerTabComplete = false;
			SisTimelineService.isPlanTabComplete = false;
			if(!!SisTimelineService.isFundTabComplete && SisTimelineService.isFundTabComplete==true)
				SisTimelineService.isFundTabComplete = false;
			if(!!SisTimelineService.isRiderTabComplete && SisTimelineService.isRiderTabComplete==true)
				SisTimelineService.isRiderTabComplete = false;
			if(!!SisTimelineService.isOutputTabComplete && SisTimelineService.isOutputTabComplete==true)
				SisTimelineService.isOutputTabComplete = false;
		}
		SisTimelineService.isEKYCTabComplete = true;
		$state.go("sisForm.sisEKYCConfirm");
	};

	this.resetProposerSelfData = function(leadId, fhrId){
		var sisFormAData = SisFormService.sisData.sisFormAData;
		if(!fhrId){
			if(!leadId){
				sisFormAData.PROPOSER_FIRST_NAME = null;
				sisFormAData.PROPOSER_MIDDLE_NAME = null;
				sisFormAData.PROPOSER_LAST_NAME = null;
				sisFormAData.PROPOSER_DOB = null;
				sisFormAData.PROPOSER_GENDER = null;
				sisFormAData.PROPOSER_MOBILE = null;
				sisFormAData.PROPOSER_EMAIL = null;
			}
			sisFormAData.PROPOSER_TITLE = null;
		}
		sisFormAData.PROPOSER_OCCU_INDUSTRIES = null;
		sisFormAData.PROPOSER_OCCUPATION = null;
		sisFormAData.PROPOSER_OCCU_CODE = null;
		sisFormAData.PROPOSER_OCCU_WORK_NATURE = null;
		sisFormAData.AGE_PROOF_DOC_ID_PR = null;
		sisFormAData.AGEPROF_DOC_TYPE_PR = null;
		sisFormAData.STD_AGEPROF_FLAG_PR = null;
		sisFormAData.TATA_FLAG = null;
		sisFormAData.COMPANY_NAME = null;
		sisFormAData.COMPANY_CODE = null;
	};

}]);
