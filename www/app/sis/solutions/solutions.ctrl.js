solutionsModule.controller('SolutionsCtrl',['$state', '$filter', 'SolutionsService', 'CommonService', 'LoginService', 'AllowedProdsList', 'ProductList', 'FHR_ID', 'LEAD_ID', 'OPP_ID', 'hswService', function($state, $filter, SolutionsService, CommonService, LoginService, AllowedProdsList, ProductList, FHR_ID, LEAD_ID, OPP_ID, hswService){
	"use strict";
	CommonService.hideLoading();
	var sc = this;

	this.activeTab = "Savings";
	debug('ProductList: ' + ProductList.length);
	this.productList = ProductList;
	this.gotoTab = function(tab){
		this.activeTab = tab;
	};

	this.isAllowed = function(pglId){
		return SolutionsService.isProductAllowed(pglId, this.productList);
	};

	this.solutions = [{"name": "Protection", "isActive": false}, {"name": "Wealth", "isActive": false}, {"name": "Savings", "isActive": false}, {"name": "Child", "isActive": false}, {"name": "Retirement", "isActive": false}, {"name": "Health", "isActive": false}];
	debug("productList : " + JSON.stringify(this.productList));
	angular.forEach(this.productList, function(product){
		var solName = (!!product.PRODUCT_SOLUTION)?(product.PRODUCT_SOLUTION.replace(" Solutions","")):"";
		solName = (!!solName)?(solName.replace(" Solution","")):"";
		var obj = $filter('filter')(sc.solutions, {"name": solName})[0];
		debug("Sol:: " + solName + ":: " + JSON.stringify(obj));
		if(product.PRODUCT_SOLUTION_CD=="PT") {
			obj.colorClass = "color_1";
			obj.headClass = "head-protection";
			obj.imageClass = "icon icon-39 red-clr";
			obj.isActive = true;
		}
		else if (product.PRODUCT_SOLUTION_CD=="WL") {
			obj.colorClass = "color_2";
			obj.headClass = "head-wealth";
			obj.imageClass = "icon icon-3 blue-clr";
			obj.isActive = true;
		}
		else if (product.PRODUCT_SOLUTION_CD=="SA") {
			obj.colorClass = "color_3";
			obj.headClass = "head-savings";
			obj.imageClass = "icon icon-12 yellow-clr";
			obj.isActive = true;
		}
		else if (product.PRODUCT_SOLUTION_CD=="CH") {
			obj.colorClass = "color_4";
			obj.headClass = "head-child";
			obj.imageClass = "icon icon-14 skyblue-clr";
			obj.isActive = true;
		}
		else if (product.PRODUCT_SOLUTION_CD=="RT") {
			obj.colorClass = "color_5";
			obj.headClass = "head-retirement";
			obj.imageClass = "icon icon-7 purple-clr";
			obj.isActive = true;
		}
		else if (product.PRODUCT_SOLUTION_CD=="HS") {
			debug("product : " + JSON.stringify(product))
			obj.colorClass = "color_6";
			obj.headClass = "head-health";
			obj.imageClass = "health-sm green-clr";
			obj.isActive = true;
		}

		obj.products = obj.products || [];
		var newObj = {};
		newObj.name = product.PRODUCT_NAME;
		newObj.pgl_id = product.PRODUCT_PGL_ID;
		newObj.isActive =  true;
		obj.products.push(newObj);
	});

	debug("solutions: " + JSON.stringify(this.solutions));

	this.gotoProductLandingPage = function(pglId, solType, prodName){
		CommonService.showLoading("Loading...");
		debug("gotoProductLandingPage pglId: " + pglId);
		if(pglId == SIP_PGL_ID){
			hswService.resetHswServiceVals();
			$state.go('hswSIP',{"PGL_ID": pglId, "SOL_TYPE": solType, "PROD_NAME": prodName, "FHR_ID": FHR_ID, "LEAD_ID": LEAD_ID, "OPP_ID": OPP_ID});
		}
		else if((pglId == IMX_PGL_ID) || (pglId == WMX_PGL_ID)){
			$state.go('hswULIP',{"PGL_ID": pglId, "SOL_TYPE": solType, "PROD_NAME": prodName, "FHR_ID": FHR_ID, "LEAD_ID": LEAD_ID, "OPP_ID": OPP_ID});
		}
		else
			$state.go('dashboard.productLandingPage',{"PGL_ID": pglId, "SOL_TYPE": solType, "PROD_NAME": prodName, "FHR_ID": FHR_ID, "LEAD_ID": LEAD_ID, "OPP_ID": OPP_ID});
	};

}]);
solutionsModule.service('SolutionsService',['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){

	var sc = this;

	this.productList = null;

	this.isProductAllowed = function(pglId, allowedProdsList){

		// returns true if pglId is available in the allowedProdsList

		return ((!!allowedProdsList) && (allowedProdsList.indexOf(pglId) !== -1))?true:false;
	};

	this.getListOfProductsAllowed = function(username){

		/*
			This function returns a list(array) of pglIds of products that the agent is allowed to sell based on policytypeallowed (C / U / C,U) column in userinfo table.
		*/

		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_userinfo', true, ['policytypeallowed'], {'agent_cd': username}, null).then(
			function(res){
				if(!!res && res.rows.length>0){
					if(!!res.rows.item(0) && !!res.rows.item(0).POLICYTYPEALLOWED){
						CommonService.transaction(sisDB,
							function(tx){
								var parameterList = res.rows.item(0).POLICYTYPEALLOWED.toUpperCase().split(',');
								debug("Policy Type Allowed: " + parameterList);
								var quesString = "?";
								for(var i=1;i<parameterList.length;i++){
									quesString += ", ?";
								}
								CommonService.executeSql(tx,"select pgl_id from lp_pgl_plan_group_lk where pgl_product_type in (" + quesString + ")",parameterList,
									function(tx,resList){
										if(!!resList && resList.rows.length>0){
											var polTypeAllList = [];
											for(var i=0;i<resList.rows.length;i++){
												polTypeAllList.push(resList.rows.item(i).PGL_ID);
											}
											dfd.resolve(polTypeAllList);
										}
										else
											dfd.resolve(null);
									},
									function(tx,err){
										dfd.resolve(null);
									}
								);
							},
							function(err){
								dfd.resolve(null);
							}
						);
					}
					else
						dfd.resolve(null);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getAllowedPolicyTypes = function(agentCd){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select policytypeallowed from lp_userinfo where agent_cd=?",[agentCd],
					function(tx,res){
						var polTypeAllowed = [];
						if(!!res && res.rows.length>0){
							if(!!res.rows.item(0).POLICYTYPEALLOWED){
								polTypeAllowed = res.rows.item(0).POLICYTYPEALLOWED.split(",");
								dfd.resolve(polTypeAllowed);
							}
							else {
								dfd.resolve(null);
							}
						}
					},
					function(err){
						dfd.resolve(null);
					}
				);
			},
			function(){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getListOfAllwdPGLs = function(polTypeAllowed){
		var dfd = $q.defer();
		CommonService.transaction(sisDB,
			function(tx){
				var quesString = "?";
				for(var i=1;i<polTypeAllowed.length;i++){
					quesString += ", ?";
				}
				CommonService.executeSql(tx,"select pgl_id from lp_pgl_plan_group_lk where pgl_product_type in (" + quesString + ")",polTypeAllowed,
					function(tx,res){
						if(!!res && res.rows.length>0){
							var pglList = [];
							for(var i=0;i<res.rows.length;i++){
								pglList.push(res.rows.item(i).PGL_ID);
							}
							debug("pglList: " + pglList);
							dfd.resolve(pglList);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getAllowedProdListFromMaster = function(pglList){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var quesString = "?";
				for(var i=1;i<pglList.length;i++){
					quesString += ", ?";
				}
				CommonService.executeSql(tx,"select distinct * from lp_product_master where business_type_list like '%" + BUSINESS_TYPE + "%' and product_pgl_id in (" + quesString + ") order by product_pgl_id desc", pglList,
					function(tx,res){
						var polTypeAllowed = [];
						if(!!res && res.rows.length>0){
							var productList = CommonService.resultSetToObject(res);
							if(!(productList instanceof Array))
								productList = [productList];
							dfd.resolve(productList);
						}
						else {
							dfd.resolve(null);
						}
					},
					function(err){
						dfd.resolve(null);
					}
				);
			},
			function(){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getProductList = function(agentCd){
		var dfd = $q.defer();
		this.getAllowedPolicyTypes(agentCd).then(
			function(allwdPolTypesRes){
				if(!!allwdPolTypesRes){
					sc.getListOfAllwdPGLs(allwdPolTypesRes).then(
						function(allowedPglList){
							if(!!allowedPglList){
								sc.getAllowedProdListFromMaster(allowedPglList).then(
									function(res){
										dfd.resolve(res);
									}
								);
							}
							else {
								dfd.resolve(null);
							}
						}
					);
				}
				else
					dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

}]);
