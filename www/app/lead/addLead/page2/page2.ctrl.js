page2Module.controller('Page2Ctrl', ['$state', '$filter', 'Page2Service', 'CommonService', 'LeadData', 'LeadStatusList', 'LeadSubStatusList', 'LeadSource1List', 'LeadSource2List', 'AddLeadService', function($state, $filter, Page2Service, CommonService, LeadData, LeadStatusList, LeadSubStatusList, LeadSource1List, LeadSource2List, AddLeadService){
	"use strict";

	var pc = this;

	this.emailRegex = EMAIL_REGEX;
	this.numberRegex = NUM_REG;

	this.page2Data = LeadData || {};

	debug("this.page2Data: " + JSON.stringify(this.page2Data));

	this.leadStatusList = LeadStatusList;
	this.leadSubStatusList = LeadSubStatusList;
	this.leadSource1List = LeadSource1List;
	this.leadSource2List = LeadSource2List;

	if(!!LeadStatusList)
		this.leadStatusOptionSelected = LeadStatusList[0];
	if(!!LeadSubStatusList)
		this.leadSubStatusOptionSelected = LeadSubStatusList[0];
	if(!!LeadSource1List)
		this.leadSource1OptionSelected = LeadSource1List[0];
	if(!!LeadSource2List)
		this.leadSource2OptionSelected = LeadSource2List[0];

	if(!!this.page2Data){
		if(!!this.page2Data.LEAD_STATUS_CD){
			var leadStatus = $filter('filter')(this.leadStatusList, {"STATUS_CD": pc.page2Data.LEAD_STATUS_CD})[0];
			this.leadStatusOptionSelected = leadStatus || this.leadStatusOptionSelected;
		}
		if(!!this.page2Data.LEAD_SUB_STATUS_CD){
			var leadSubStatus = $filter('filter')(this.leadSubStatusList, {"STATUS_SUB_CD": pc.page2Data.LEAD_SUB_STATUS_CD})[0];
			this.leadSubStatusOptionSelected = leadSubStatus || this.leadSubStatusOptionSelected;
		}
		if(!!this.page2Data.LEAD_SOURCE1_CD){
			var leadSource1 = $filter('filter')(this.leadSource1List, {"LOV_CD": pc.page2Data.LEAD_SOURCE1_CD})[0];
			this.leadSource1OptionSelected = leadSource1 || this.leadSource1OptionSelected;
		}
		if(!!this.page2Data.LEAD_SOURCE2_CD){
			var leadSource2 = $filter('filter')(this.leadSource2List, {"LOV_CD": pc.page2Data.LEAD_SOURCE2_CD})[0];
			this.leadSource2OptionSelected = leadSource2 || this.leadSource2OptionSelected;
		}
	}

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("leadPage1");
	};

	this.onNext = function(){
		CommonService.showLoading();
		Page2Service.onSubmit(JSON.parse(JSON.stringify(this.page2Data))).then(
			function(res){
				if(!!res){
					try{
						debug("calling EKYC"+AddLeadService.leadData.IPAD_LEAD_ID);
						$state.go("leadPage3", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
					}catch(e){
						debug("EXCEPTION in EKYC:"+e.message);
						CommonService.hideLoading();
					}
				}
				else{
					navigator.notification.alert("Could not add lead.",function(){
						CommonService.hideLoading();
					},"Lead","OK");
				}
			}
		);
	};
	CommonService.hideLoading();
}]);


page2Module.service('Page2Service', ['$q', '$state', 'CommonService', 'LoginService', 'AddLeadService', function($q, $state, CommonService, LoginService, AddLeadService){
	"use strict";

	this.getLeadSourceList = function(lovTypeCd){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_lead_lov_master", true, null, {"lov_type_cd": lovTypeCd}).then(
			function(res){
				var leadSourceList = [];
				var leadSourceLabel = "Lead Source 1";
				if(lovTypeCd=='14')
					leadSourceLabel = "Lead Source 2";
				leadSourceList.push({"LOV_TYPE_CD": null, "LOV_TYPE": null, "LOV_VALUE": "Select " + leadSourceLabel,"LOV_CD": null});
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						var leadObj = {};
						leadObj.LOV_TYPE_CD = res.rows.item(i).LOV_TYPE_CD;
						leadObj.LOV_TYPE = res.rows.item(i).LOV_TYPE;
						leadObj.LOV_VALUE = res.rows.item(i).LOV_VALUE;
						leadObj.LOV_CD = res.rows.item(i).LOV_CD;
						leadSourceList.push(leadObj);
					}
				}
				dfd.resolve(leadSourceList);
			}
		);
		return dfd.promise;
	};

	this.getLeadStatusList = function(){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_lead_status_master", true, "status_cd, status_value").then(
			function(res){
				var leadStatusList = [];
				leadStatusList.push({"STATUS_CD": null, "STATUS_VALUE": "Select Lead Status"});
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						var leadObj = {};
						leadObj.STATUS_CD = res.rows.item(i).STATUS_CD;
						leadObj.STATUS_VALUE = res.rows.item(i).STATUS_VALUE;
						leadStatusList.push(leadObj);
					}
				}
				dfd.resolve(leadStatusList);
			}
		);
		return dfd.promise;
	};

	this.getLeadSubStatusList = function(statusCd){
		var dfd = $q.defer();
		CommonService.selectRecords(db, "lp_lead_status_master", true, "status_cd, status_value", {"status_cd": statusCd}).then(
			function(res){
				var leadStatusList = [];
				leadStatusList.push({"STATUS_SUB_CD": null, "STATUS_SUB_VALUE": "Select Lead Sub Status"});
				if(!!res && res.rows.length>0){
					for(var i=0;i<res.rows.length;i++){
						var leadObj = {};
						leadObj.STATUS_SUB_CD = res.rows.item(i).STATUS_SUB_CD;
						leadObj.STATUS_SUB_VALUE = res.rows.item(i).STATUS_SUB_VALUE;
						leadStatusList.push(leadObj);
					}
				}
				dfd.resolve(leadStatusList);
			}
		);
		return dfd.promise;
	};

	this.onSubmit = function(leadData){
		var dfd = $q.defer();
		if(leadData.LEAD_SOURCE1_CD=='14' && !AddLeadService.editLead){
			leadData.LEAD_CREATED_DTTM = leadData.LEAD_CREATED_DTTM || CommonService.getCurrDate();
		}
		leadData.IPAD_MODIFIED_DTTM = CommonService.getCurrDate();
		debug("AddLeadService.leadData ::before:: " + JSON.stringify(AddLeadService.leadData));
		Object.assign(AddLeadService.leadData, leadData);
		debug("AddLeadService.leadData ::after:: " + JSON.stringify(AddLeadService.leadData));
		CommonService.insertOrReplaceRecord(db, 'lp_mylead', AddLeadService.leadData, true).then(
			function(res){
				if(!!res){
					dfd.resolve(res);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

}]);
