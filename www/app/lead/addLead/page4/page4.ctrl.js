leadEKYCConfirmModule.controller('leadEKYCConfirmCtrl', ['CommonService', 'LoginService', 'leadEKYCConfirmService', 'AddLeadService', '$stateParams', 'ViewReportService', '$state', function(CommonService, LoginService, leadEKYCConfirmService, AddLeadService, $stateParams, ViewReportService, $state){
	"use strict";

	var sc = this;
	this.leadEKYCConfirmData = {"insured": {}, "proposer": {}};

	debug("AddLeadService.leadEKYCData: " + JSON.stringify(AddLeadService.leadEKYCData));
	debug("AddLeadService.leadFormAData_eKyc: " + JSON.stringify(AddLeadService.leadFormAData_eKyc));

	this.ekycDisabled = AddLeadService.ekycDisabled;

	leadEKYCConfirmService.disableInsDtlsView = AddLeadService.disableInsDtlsView;
	leadEKYCConfirmService.disablePropDtlsView = AddLeadService.disablePropDtlsView;
	leadEKYCConfirmService.insConfirmed = AddLeadService.insConfirmed;
	leadEKYCConfirmService.propConfirmed = AddLeadService.propConfirmed;

	this.insuredOTPRequired = (!!AddLeadService.leadEKYCData.insured.EKYC_FLAG && AddLeadService.leadEKYCData.insured.EKYC_FLAG=="Y") || false;
	this.proposerOTPRequired = (!!AddLeadService.leadEKYCData.proposer.EKYC_FLAG && AddLeadService.leadEKYCData.proposer.EKYC_FLAG=="Y") || false;

	this.INS_EKYC_ID = null;
 	this.PRO_EKYC_ID = null;
	if(!!AddLeadService.leadEKYCData.insured && (AddLeadService.leadEKYCData.insured.EKYC_FLAG == 'Y' || !!AddLeadService.leadEKYCData.insured.CONSUMER_AADHAR_NO)){
		this.INS_EKYC_ID = (!!AddLeadService.leadEKYCData.insured && !!AddLeadService.leadEKYCData.insured.EKYC_ID ? AddLeadService.leadEKYCData.insured.EKYC_ID : CommonService.getRandomNumber());
		if(!AddLeadService.leadEKYCData.insured || !AddLeadService.leadEKYCData.insured.EKYC_ID){
			AddLeadService.leadEKYCData.insured.EKYC_ID = this.INS_EKYC_ID;
		}
	}
	if(!!AddLeadService.leadEKYCData.proposer && (AddLeadService.leadEKYCData.proposer.EKYC_FLAG == 'Y' || !!AddLeadService.leadEKYCData.proposer.CONSUMER_AADHAR_NO)){
		this.PRO_EKYC_ID = (!!AddLeadService.leadEKYCData.proposer && !!AddLeadService.leadEKYCData.proposer.EKYC_ID ? AddLeadService.leadEKYCData.proposer.EKYC_ID : CommonService.getRandomNumber());
		if(!AddLeadService.leadEKYCData.proposer || !AddLeadService.leadEKYCData.proposer.EKYC_ID){
			AddLeadService.leadEKYCData.proposer.EKYC_ID = this.PRO_EKYC_ID;
		}
	}

	this.disableInsDtlsView = (leadEKYCConfirmService.disableInsDtlsView);
	this.disablePropDtlsView = (leadEKYCConfirmService.disablePropDtlsView);

	if(!leadEKYCConfirmService.disableInsDtlsView){
		this.disableInsDtlsView = (!AddLeadService.leadEKYCData.insured.EKYC_DONE_DATE);
	}

	if(!leadEKYCConfirmService.disablePropDtlsView){
		this.disablePropDtlsView = (!AddLeadService.leadEKYCData.proposer.EKYC_DONE_DATE);
	}

	this.disableInsuredOTP = (!this.insuredOTPRequired) || (!this.disableInsDtlsView);
	this.disableProposerOTP = (!this.proposerOTPRequired) || (!this.disablePropDtlsView);

	this.otpRegex = NUM_REG;

	debug("lead ekyc confirm controller: " + leadEKYCConfirmService.disableInsDtlsView + " " + leadEKYCConfirmService.disablePropDtlsView);

	debug("lead otp controller: " + this.insuredOTPRequired + " " + this.insuredOTPRequired);
	CommonService.hideLoading();
	this.openMenu = function(){
		CommonService.openMenu();
	};
	this.resendOTP = function(type){
		debug("inside onOTPSubmit: " + type);
		CommonService.showLoading();
		leadEKYCConfirmService.resendOTP(type).then(
			function(response){
				navigator.notification.alert("OTP Sent.",function(){
					CommonService.hideLoading();
				},"OTP","OK");
				debug("resendOTP response: " + type + ": " + response);
			}
		);
	};

	this.onOTPSubmit = function(type){
		CommonService.showLoading();
		debug("inside onOTPSubmit: " + type + " :: OTP: " + sc.leadEKYCConfirmData[type].OTP + " : " + CommonService.checkConnection());
		leadEKYCConfirmService.INS_EKYC_ID = sc.INS_EKYC_ID;
		leadEKYCConfirmService.PRO_EKYC_ID = sc.PRO_EKYC_ID;
		if(type === 'insured'){
			AddLeadService.insConfirmed = null;
			leadEKYCConfirmService.insConfirmed = AddLeadService.insConfirmed;
		}
		if(type === 'proposer'){
			AddLeadService.propConfirmed = null;
			leadEKYCConfirmService.propConfirmed = AddLeadService.propConfirmed;
		}
		if(CommonService.checkConnection()=="online"){
			leadEKYCConfirmService.onOTPSubmit(type, sc.leadEKYCConfirmData[type].OTP).then(
				function(response){
					if(!!response && !!response.responseCode && response.responseCode=='400'){
						navigator.notification.alert("OTP Mismatch.",function(){
							if(type==="insured"){
								sc.disableInsDtlsView=true;
							}
							else{
								sc.disablePropDtlsView=true;
							}
						},"OTP","OK");
						AddLeadService.leadEKYCData[type].RESPONSE_CODE = response.responseCode;
						AddLeadService.leadEKYCData[type].RESPONSE_MSG = response.responseMessage;
						AddLeadService.leadEKYCData[type].TIMESTAMP = response.timeStamp;
					}
					else if((!response) || (!!response.responseCode && (response.responseCode=='103' || response.responseCode=='114'))){
						navigator.notification.alert("Could not verify the OTP. Please try again later.",function(){
							if(type==="insured"){
								sc.disableInsDtlsView=true;
							}
							else{
								sc.disablePropDtlsView=true;
							}
							CommonService.hideLoading();
						},"OTP","OK");
					}
					else{
						if(type==="insured"){
							sc.disableInsDtlsView=false;
						}
						else{
							sc.disablePropDtlsView=false;
						}
					}
					AddLeadService.leadEKYCData[type].AUTH_CODE_UIDAI = response.authenticationCode;
					debug("onOTPSubmit response: " + type + ": " + response);
					CommonService.hideLoading();
				}
			);
		}
		else{
			navigator.notification.alert("Please connect to the internet to submit OTP.",function(){
				CommonService.hideLoading();
			},"OTP","OK");
		}
	};

	this.viewAndConfirmDetails = function(type){
		debug("inside viewEKYCDetails: " + type + " :: "  + JSON.stringify(AddLeadService.leadEKYCData));
		var leadEKYCConfirmHtml = leadEKYCConfirmService.viewAndConfirmDetails(type);
		CommonService.getEKYCDocumentID().then(function(eKycData){
			debug("eKycData: "+JSON.stringify(eKycData));
			var eKycDocId = eKycData[type].DOC_ID || null;
			var isConfirmed = false;
			var reportId = null;
			if(type=="insured")
				isConfirmed = leadEKYCConfirmService.insConfirmed;
			else if(type=="proposer")
				isConfirmed = leadEKYCConfirmService.propConfirmed;
			if(!AddLeadService.leadEKYCData[type].EKYC_DONE_DATE && !isConfirmed){
				ViewReportService.savedReport = leadEKYCConfirmHtml;
				reportId = (type === "insured")?sc.INS_EKYC_ID:sc.PRO_EKYC_ID;
				$state.go("viewReport", {'REPORT_TYPE': 'EKYC', 'REPORT_ID': reportId, 'PREVIOUS_STATE': "leadPage4", "REPORT_PARAMS": {"TYPE": type, "LEAD_ID": AddLeadService.leadData.IPAD_LEAD_ID, "AGENT_CD": AddLeadService.leadData.AGENT_CD, "EKYC_DOC_ID":eKycDocId}, "SAVED_REPORT": leadEKYCConfirmHtml, 'PREVIOUS_STATE_PARAMS': $stateParams});
			}
			else{
				reportId = (type === "insured")?sc.INS_EKYC_ID:sc.PRO_EKYC_ID;
				ViewReportService.fetchSavedReport("EKYC", reportId, {"TYPE": type, "LEAD_ID": AddLeadService.leadData.IPAD_LEAD_ID, "AGENT_CD": AddLeadService.leadData.AGENT_CD}).then(
					function(res){
						if(!res){
							navigator.notification.alert("EKYC data not found!!",function(){
								CommonService.hideLoading();
							},"Error","OK");
						}
						else{
							$state.go("viewReport", {"REPORT_TYPE": "EKYC", "REPORT_ID": reportId, 'PREVIOUS_STATE': "leadPage4", "REPORT_PARAMS": {"TYPE": type, "LEAD_ID": AddLeadService.leadData.IPAD_LEAD_ID, "AGENT_CD": AddLeadService.leadData.AGENT_CD, "EKYC_DOC_ID":eKycDocId, "HIDE_CONFIRM_BUTTON": true}, "SAVED_REPORT": res, "PREVIOUS_STATE_PARAMS": $stateParams});
						}
					}
				);
			}
		});
	};

	this.onNext = function(isPristine){
		CommonService.showLoading();
		leadEKYCConfirmService.INS_EKYC_ID = sc.INS_EKYC_ID;
		leadEKYCConfirmService.PRO_EKYC_ID = sc.PRO_EKYC_ID;
		leadEKYCConfirmService.onNext(isPristine);
	};

	this.onBack = function(){
		CommonService.showLoading();
		$state.go("leadPage3", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
	};
	CommonService.hideLoading();
}]);
leadEKYCConfirmModule.service('leadEKYCConfirmService',['CommonService', 'AddLeadService', '$q', '$state', function(CommonService, AddLeadService, $q, $state){
	"use strict";
	var sc = this;

	this.resendOTP = function(type){
		var dfd = $q.defer();
		if(!!type){
			CommonService.ekycServletCall(AddLeadService.leadEKYCData[type].CONSUMER_AADHAR_NO).then(
				function(response){
					AddLeadService.leadEKYCData[type + "OTPResp"] = response;
					dfd.resolve(response || null);
				}
			);
		}
		else{
			navigator.notification.alert("Could not resend the OTP",null,"Resend OTP","OK");
			CommonService.hideLoading();
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.onOTPSubmit = function(type, otp){
		var dfd = $q.defer();
		if(!!type){
			var aadharNumber = AddLeadService.leadEKYCData[type].CONSUMER_AADHAR_NO;
			var timestamp = AddLeadService.leadEKYCData[type + "OTPResp"].timeStamp;
			CommonService.ekycServletCall(aadharNumber, timestamp, otp).then(
				function(response){
					AddLeadService.leadEKYCData[type].AGENT_CD = AddLeadService.leadData.AGENT_CD || LoginService.lgnSrvObj.userinfo.AGENT_CD;
					AddLeadService.leadEKYCData[type].EKYC_ID = (type == "insured")?sc.INS_EKYC_ID : sc.PRO_EKYC_ID;
					AddLeadService.leadEKYCData[type].CUST_TYPE = (type=="insured")?INSURED_CUST_TYPE:PROPOSER_CUST_TYPE;
					AddLeadService.leadEKYCData[type].LEAD_ID = AddLeadService.leadData.IPAD_LEAD_ID;
					if(!!response){
						AddLeadService.leadEKYCData[type].NAME = response.name || null;
						AddLeadService.leadEKYCData[type].GENDER = response.gender || null;
						AddLeadService.leadEKYCData[type].DOB = response.dob || null;
						AddLeadService.leadEKYCData[type].EMAIL_ID = response.emailId || null;
						AddLeadService.leadEKYCData[type].BUILDING = response.houseNo || null;
						AddLeadService.leadEKYCData[type].STREET = response.street || null;
						AddLeadService.leadEKYCData[type].AREA = response.locality || null;
						AddLeadService.leadEKYCData[type].CITY = response.city || null;
						AddLeadService.leadEKYCData[type].DISTRICT = response.disrict || null;
						AddLeadService.leadEKYCData[type].SUBDISTRICT = response.subdistrict || null;
						AddLeadService.leadEKYCData[type].LANDMARK = response.landmark || null;
						AddLeadService.leadEKYCData[type].STATE = response.state || null;
						AddLeadService.leadEKYCData[type].PIN = response.pin || null;
						AddLeadService.leadEKYCData[type].POSTOFFICE = response.postOffice || null;
						AddLeadService.leadEKYCData[type].CARE_OF = response.careOf || null;
						AddLeadService.leadEKYCData[type].PHONE_NO = response.phoneNo || null;
						sc.TRANSID = response.rrn || null;
						AddLeadService.leadEKYCData[type + "_photo"] = response.photo || null;
					}
					dfd.resolve(response || null);
				}
			);
		}
		else{
			navigator.notification.alert("Could not resend the OTP",null,"Resend OTP","OK");
			CommonService.hideLoading();
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.viewAndConfirmDetails = function(type){
		var leadEKYCConfirmHtml_top = "<html><body><div style='margin: 30px auto; width: 50%;'>";
		var leadEKYCConfirmHtml_mid = "<div style='display: none;'>Application Number: ||APP_NUM||</div>";
		var leadEKYCConfirmHtml_bot = "<div>Photo: <img width='150px' height='150px' style='display: none;' src='data:image/jpg;base64,||PHOTO||' /></div><div>Name: ||NAME||</div><div>EmailId: ||EMAIL_ID||</div><div>DOB: ||DOB||</div><div style='display: none'>User entered DOB: ||USER_ENT_DOB||</div><div>Gender: ||GENDER||</div><div>Mobile Number: ||PHONE_NO||</div><div>careOf: ||CARE_OF||</div><div>HouseNo: ||BUILDING||</div><div>Street: ||STREET||</div><div>Locality: ||AREA||</div><div>Landmark: ||LANDMARK||</div><div>District: ||DISTRICT||</div><div>Subdistrict: ||SUBDISTRICT||</div><div>City: ||CITY||</div><div>State: ||STATE||</div><div>Pin: ||PIN||</div><div>PostOffice: ||POSTOFFICE||</div><div>Transaction ID: ||TRANSID||</div></div></body></html>";

		var keys = Object.keys(AddLeadService.leadEKYCData[type]);
		for(var i=0; i<keys.length; i++){
			leadEKYCConfirmHtml_bot = CommonService.replaceAll(leadEKYCConfirmHtml_bot, "||" + keys[i] + "||", ((AddLeadService.leadEKYCData[type])[keys[i]] || ""));
		}
		leadEKYCConfirmHtml_bot = CommonService.replaceAll(leadEKYCConfirmHtml_bot, "style='display: none;'", "");
		leadEKYCConfirmHtml_bot = CommonService.replaceAll(leadEKYCConfirmHtml_bot, "||PHOTO||", AddLeadService.leadEKYCData[type + "_photo"]);
		leadEKYCConfirmHtml_bot = CommonService.replaceAll(leadEKYCConfirmHtml_bot, "||TRANSID||", sc.TRANSID);
		var leadEKYCConfirmHtml = leadEKYCConfirmHtml_top + leadEKYCConfirmHtml_mid + leadEKYCConfirmHtml_bot;
		debug("leadEKYCConfirmHtml: " + leadEKYCConfirmHtml);
		return leadEKYCConfirmHtml;
	};

	this.postSaveReport = function(res, type){
		if(!!res){
			if(type=="insured"){
				sc.disableInsDtlsView = false;
			}
			else{
				sc.disablePropDtlsView = false;
			}
		}
		else{
			if(type=="insured"){
				sc.disableInsDtlsView = true;
			}
			else{
				sc.disablePropDtlsView = true;
			}
		}
		debug("postSaveReport: " + sc.disableInsDtlsView + " " + sc.disablePropDtlsView);
	};

	this.validateState = function(state){
		var dfd = $q.defer();

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select * from lp_state_master where state_desc like '" + state + "' and isactive=?", ['Y'],
					function(tx, res){
						if(!!res && res.rows.length>0)
							dfd.resolve(true);
						else
							dfd.resolve(false);
					},
					function(tx, err){
						dfd.resolve(false);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);

		return dfd.promise;
	};

	this.validateCity = function(city){
		var dfd = $q.defer();

		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select * from lp_city_master where city_desc like '" + city + "' and isactive=?", ['Y'],
					function(tx, res){
						if(!!res && res.rows.length>0)
							dfd.resolve(true);
						else
							dfd.resolve(false);
					},
					function(tx, err){
						dfd.resolve(false);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);

		return dfd.promise;
	};

	this.validateData = function(_testData, confirmFlag){
		debug("in validateData :",JSON.stringify(_testData));
		var dfd = $q.defer();
		var validateRes = {};
		validateRes.flag = true;
		try{
			if(!!confirmFlag){
				validateRes.reason = "Reason(s): ";
				var index = 1;
				var _nameArr = (_testData.NAME.split(" "));
				var nameArr = [];
				var middleName = "";

				if(!_testData.NAME || _nameArr.length===0) {
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Name not available.";
					index++;
				}
				else {
					nameArr[0] = _nameArr[0];
					if(_nameArr.length == 2){
						nameArr[1] = null;
						nameArr[2] = _nameArr[1];
					}
					else{
						for(var i=1;i<_nameArr.length;i++){
							nameArr[i] = _nameArr[i];
							if(i<(_nameArr.length-1))
								middleName += nameArr[i];
							if(i!=(_nameArr.length-2))
								middleName += " ";
						}
					}

					if(nameArr[0].length > 20)
						validateRes.flag = false;
					else if(!!middleName && middleName.length>20)
						validateRes.flag = false;
					else if(nameArr[nameArr.length-1]>20)
						validateRes.flag = false;

					if(!validateRes.flag){
						validateRes.reason += "\n" + index + ". Name too long.";
						index++;
					}
				}
				debug("nameArr: " + nameArr);
				debug("middleName: " + middleName);
				if(!!validateRes.flag && ((!!_testData.BUILDING) && (!!(_testData.BUILDING.trim())) && (!BLOCKHASH.test(_testData.BUILDING)))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Building name is invalid.";
					index++;
				}
				if(!!validateRes.flag && ((!!_testData.STREET) && (!!(_testData.STREET.trim())) && (!BLOCKHASH.test(_testData.STREET)))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Street name is invalid.";
					index++;
				}
				if(!!validateRes.flag && ((!!_testData.AREA) && (!!(_testData.AREA.trim())) && (!BLOCKHASH.test(_testData.AREA)))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Area name is invalid.";
					index++;
				}
				if(!!validateRes.flag && ((!!_testData.LANDMARK) && (!!(_testData.LANDMARK.trim())) && ((!BLOCKHASH.test(_testData.LANDMARK))/* || ((_testData.LANDMARK).length>45)*/))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Landmark is invalid.";
					index++;
				}
				if(!!validateRes.flag && ((!!_testData.PIN) && (!!(_testData.PIN.trim())) && ((!PINCODE_REG.test(_testData.PIN)) || ((_testData.PIN).length>6)))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Pincode invalid or too long.";
					index++;
				}
				else if(!!validateRes.flag && (!_testData.PIN || (!(_testData.PIN.trim())))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Pincode cannot be blank.";
					index++;
				}
				/* EKYC address validation*/
				var ADDRESS_LINES = "";
				if(!!_testData.BUILDING)
			 		ADDRESS_LINES = _testData.BUILDING;
				if(!!_testData.STREET)
					ADDRESS_LINES = ADDRESS_LINES + " " +_testData.STREET;
				if(!!_testData.AREA)
					ADDRESS_LINES = ADDRESS_LINES + " "+ _testData.AREA /*+ " " +EKYCData.Insured.SUBDISTRICT;*/;
				if(!!_testData.DISTRICT)
					ADDRESS_LINES = ADDRESS_LINES + " "+_testData.DISTRICT;
				if(!!_testData.SUBDISTRICT)
					ADDRESS_LINES = ADDRESS_LINES + " "+_testData.SUBDISTRICT;

				debug("ADDRESS_LINES: " + ADDRESS_LINES);
				if(!!validateRes.flag && (!ADDRESS_LINES || (!(ADDRESS_LINES.trim())))){
					validateRes.flag = false;
					validateRes.reason += "\n" + index + ". Address cannot be empty.";
					index++;
				}

				if(!!validateRes.flag){
					debug("_testData.STATE: " + _testData.STATE);
					if(!_testData.STATE){
						debug("_testData.STATE if: " + _testData.STATE);
						validateRes.flag = false;
						validateRes.reason += "\n" + index + ". State not available.";
						index++;
						dfd.resolve(validateRes.flag);
					}
					else{
						debug("_testData.STATE else : " + _testData.STATE);
						sc.validateState(_testData.STATE).then(
							function(stateRes){
								validateRes.flag = stateRes;
								if(!!validateRes.flag){
									dfd.resolve(validateRes || false);
								}
								else{
									debug("state not found");
									validateRes.reason += "\n" + index + ". Cannot use E-KYC State data.";
									index++;
									dfd.resolve(validateRes || false);
								}
							}
						);
					}
				}
				else{
					dfd.resolve(validateRes || false);
				}
			}
			else {
				dfd.resolve(validateRes);
			}
		} catch(ex){
			debug("Exception: " + ex.message);
			validateRes.reason = "Something went wrong.";
			dfd.resolve(validateRes);
		}
		return dfd.promise;
	};

	this.insertEKYCData = function(type){
		var dfd = $q.defer();
		if(!AddLeadService.leadEKYCData[type].EKYC_ID){
			CommonService.hideLoading();
			dfd.resolve("S");
		}
		else{
			CommonService.insertOrReplaceRecord(db, "LP_LEAD_EKYC_DTLS", AddLeadService.leadEKYCData[type], true).then(
				function(response){
					dfd.resolve(response);
				}
			);
		}
		return dfd.promise;
	};

	this.postValidationProcess = function(valIns, valProp){
		var valFlagIns = valIns.flag;
		var valFlagProp = valProp.flag;
		debug("in postValidationProcess valFlagIns: " + valFlagIns + " valFlagProp: " + valFlagProp);
		var dfd = $q.defer();
		if(!AddLeadService.leadEKYCData.insured.EKYC_DONE_DATE && !this.insConfirmed) {
			AddLeadService.leadEKYCData.insured.EKYC_FLAG = "N";
		}
		if(!AddLeadService.leadEKYCData.proposer.EKYC_DONE_DATE && !this.propConfirmed){
			AddLeadService.leadEKYCData.proposer.EKYC_FLAG = "N";
		}
		AddLeadService.leadEKYCData.insured.EKYC_DONE_DATE = CommonService.getCurrDate();
		AddLeadService.leadEKYCData.proposer.EKYC_DONE_DATE = CommonService.getCurrDate();

		AddLeadService.leadEKYCData.insured.AGENT_CD = AddLeadService.leadData.AGENT_CD;
		AddLeadService.leadEKYCData.insured.EKYC_ID = sc.INS_EKYC_ID;
		AddLeadService.leadEKYCData.insured.LEAD_ID = AddLeadService.leadData.IPAD_LEAD_ID;

		if(!valFlagIns && AddLeadService.leadEKYCData.insured.EKYC_FLAG=="Y"){
			AddLeadService.leadEKYCData.insured.EKYC_FLAG = "N";
			CommonService.hideLoading();
			navigator.notification.alert("Cannot use e-kyc details for the insured.\n" + valIns.reason, null, "E-KYC", "OK");
		}
		sc.insertEKYCData('insured').then(
			function(insSaveRes){
				if(!valFlagProp && AddLeadService.leadEKYCData.proposer.EKYC_FLAG=="Y"){
					AddLeadService.leadEKYCData.proposer.EKYC_FLAG = "N";
					CommonService.hideLoading();
					navigator.notification.alert("Cannot use e-kyc details for the proposer.\n" + valProp.reason, null, "E-KYC", "OK");
				}
				sc.insertEKYCData('proposer').then(
					function(propSaveRes){
						if(!!valFlagIns && !insSaveRes){
							navigator.notification.alert("Could not save e-kyc details for the insured.",function(){
								CommonService.hideLoading();
								dfd.resolve(null);
							} ,"E-KYC","OK");
						}
						else if(!!valFlagProp && !propSaveRes){
							navigator.notification.alert("Could not save e-kyc details for the proposer.",function(){
								CommonService.hideLoading();
								dfd.resolve(null);
							} ,"E-KYC","OK");
						}
						else if((!!valFlagIns && !!insSaveRes) || (!!valFlagProp && !!propSaveRes)){
							navigator.notification.alert("E-KYC details saved successfully.",
								function(){
									CommonService.hideLoading();
									dfd.resolve("success");
								}, "E-KYC","OK"
							);
						}
						else{
							dfd.resolve("success");
						}
					}
				);
			}
		);
		return dfd.promise;
	};

	this.saveEKYCData = function(){
		debug("in saveEKYCData " + JSON.stringify(AddLeadService.leadEKYCData.insured));
		var dfd = $q.defer();

		var valFlagIns = false;
		var valFlagProp = false;

		if(!!AddLeadService.leadEKYCData){
			if(AddLeadService.leadEKYCData.insured.EKYC_FLAG == "Y") {
				debug("insured ekyc flag Y");
				try{
					sc.validateData(AddLeadService.leadEKYCData.insured, AddLeadService.insConfirmed).then(
						function(insRes){
							valFlagIns = insRes || {"flag": false, "reason": "Reason: Something went wrong."};
							if(AddLeadService.leadEKYCData.proposer.EKYC_FLAG == "Y"){
								sc.validateData(AddLeadService.leadEKYCData.proposer, AddLeadService.propConfirmed).then(
									function(propRes){
										valFlagProp = propRes || {"flag": false, "reason": "Reason: Something went wrong."};
										sc.postValidationProcess(valFlagIns, valFlagProp).then(
											function(postValRes){
												dfd.resolve(postValRes);
											}
										);
									}
								);
							}
							else{
								sc.postValidationProcess(valFlagIns, valFlagProp).then(
									function(postValRes){
										dfd.resolve(postValRes);
									}
								);
							}
						}
					);
				} catch(ex) {
					debug("Exception in saveEKYCData: " + ex.message);
				}
			}
			else {
				AddLeadService.leadEKYCData.insured.AGENT_CD = AddLeadService.leadData.AGENT_CD;
				AddLeadService.leadEKYCData.insured.EKYC_ID = sc.INS_EKYC_ID;
				AddLeadService.leadEKYCData.insured.CUST_TYPE = INSURED_CUST_TYPE;
				AddLeadService.leadEKYCData.insured.LEAD_ID = AddLeadService.leadData.IPAD_LEAD_ID;
				sc.insertEKYCData('insured').then(
					function(insRes){
						if(!!insRes){
							if(AddLeadService.leadEKYCData.proposer.EKYC_FLAG == "Y"){
								sc.validateData(AddLeadService.leadEKYCData.proposer, AddLeadService.propConfirmed).then(
									function(propRes){
										valFlagProp = propRes || {"flag": false, "reason": "Reason: Something went wrong."};
										sc.postValidationProcess(valFlagIns, valFlagProp).then(
											function(postValRes){
												dfd.resolve(postValRes);
											}
										);
									}
								);
							}
							else{
								AddLeadService.leadEKYCData.proposer.AGENT_CD = AddLeadService.leadData.AGENT_CD;
								AddLeadService.leadEKYCData.proposer.EKYC_ID = sc.PRO_EKYC_ID;
								AddLeadService.leadEKYCData.proposer.CUST_TYPE = PROPOSER_CUST_TYPE;
								AddLeadService.leadEKYCData.proposer.LEAD_ID = AddLeadService.leadData.IPAD_LEAD_ID;
								sc.insertEKYCData('proposer').then(
									function(res){
										if(!!res){
											dfd.resolve(true);
										}
										else{
											navigator.notification.alert("Could not save e-kyc details for the proposer.",function(){
												CommonService.hideLoading();
												dfd.resolve(false);
											} ,"E-KYC","OK");
										}
									}
								);
							}
						}
						else{
							navigator.notification.alert("Could not save e-kyc details for the proposer.",function(){
								CommonService.hideLoading();
								dfd.resolve(false);
							} ,"E-KYC","OK");
						}
					}
				);
			}
		}
		return dfd.promise;
	};

	this.onNext = function(isPristine){

		var insCanProceed = (!AddLeadService.leadEKYCData.insured.EKYC_DONE_DATE && !this.insConfirmed);
		var propCanProceed = (!AddLeadService.leadEKYCData.proposer.EKYC_DONE_DATE && !this.propConfirmed);

		debug("on next: insCanProceed: " + insCanProceed + " propCanProceed: " + propCanProceed);
		debug("AddLeadService.ekycDisabled: " + AddLeadService.ekycDisabled);

		if(insCanProceed && propCanProceed){
			debug("Done EKYC in else");
			if(AddLeadService.ekycDisabled){
				navigator.notification.confirm("Do you want to proceed without adding new E-KYC details?",
					function(buttonIndex){
						if(buttonIndex=="1"){
							$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
						}
						else{
							CommonService.hideLoading();
						}
					}
				);
			}
			else{
				if((insCanProceed && AddLeadService.leadEKYCData.insured.EKYC_FLAG == 'Y') || (propCanProceed && AddLeadService.leadEKYCData.proposer.EKYC_FLAG == 'Y')){
					navigator.notification.confirm("Do you want to proceed without adding new E-KYC details?",
					function(buttonIndex){
						if(buttonIndex=="1"){
							sc.saveEKYCData().then(function(res){
								$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
							});
						}
						else{
							CommonService.hideLoading();
						}
					});
				}
				else{
					sc.saveEKYCData().then(function(res){
						$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
					});
				}
			}
		}
		else {
			if(AddLeadService.leadEKYCData.insured.EKYC_FLAG == "Y"){
				if(insCanProceed){
					navigator.notification.confirm("Insured's details have not been confirmed, do you want to proceed without E-KYC for the insured?",
						function(buttonIndex){
							if(buttonIndex=="1"){
								if(AddLeadService.leadEKYCData.proposer.EKYC_FLAG == "Y"){
									if(propCanProceed){
										navigator.notification.confirm("Proposer's details have not been confirmed, do you want to proceed without E-KYC for the proposer?.",
											function(buttonIndex){
												if(buttonIndex=="1"){
													debug("Here IF  !!");
													sc.saveEKYCData().then(function(res){
														$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
													});
												}
												else {
													CommonService.hideLoading();
												}
											},
											'E-KYC Confirmation',
											'Yes, No'
										);
									}
									else {
										sc.saveEKYCData().then(function(res){
											$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
										});
									}
								}
								else {
									sc.saveEKYCData().then(function(res){
										$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
									});
								}
							}
							else{
								CommonService.hideLoading();
							}
						},
						'E-KYC Confirmation',
						'Yes, No'
					);
				}
				else {
					if(AddLeadService.leadEKYCData.proposer.EKYC_FLAG == "Y"){
						if(propCanProceed){
							navigator.notification.confirm("Proposer's details have not been confirmed, do you want to proceed without E-KYC for the proposer?.",
								function(buttonIndex){
									if(buttonIndex=="1"){
										debug("Here IF  !!");
										sc.saveEKYCData().then(function(res){
											$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
										});
									}
									else {
										CommonService.hideLoading();
									}
								},
								'E-KYC Confirmation',
								'Yes, No'
							);
						}
						else {
							sc.saveEKYCData().then(function(res){
								$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
							});
						}
					}
					else {
						sc.saveEKYCData().then(function(res){
							$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
						});
					}
				}
			}
			else if(AddLeadService.leadEKYCData.proposer.EKYC_FLAG == "Y"){
				if(propCanProceed){
					navigator.notification.confirm("Proposer's details have not been confirmed, do you want to proceed without E-KYC for the proposer?",
						function(buttonIndex){
							if(buttonIndex=="1"){
								debug("Here IF - 1  !!");
								sc.saveEKYCData().then(function(res){
									$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
								});
							}
							else {
								CommonService.hideLoading();
							}
						},
						'E-KYC Confirmation',
						'Yes, No'
					);
				}
				else{
					sc.saveEKYCData().then(function(res){
						$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
					});
				}
			}
			else{
				debug("Here elsee - 1 !!");
				sc.saveEKYCData().then(function(res){
					$state.go("leadDashboard", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
				});
			}
		}
	};
}]);
