addLeadModule.service('AddLeadService', ['CommonService', '$q', function(CommonService, $q){
	"use strict";

	var al = this;

	this.getLeadPage1Data = function(leadData){
		if(!!leadData){
			var leadPage1Data = {};
			leadPage1Data.LEAD_SOURCE1_CD = leadData.LEAD_SOURCE1_CD;
			leadPage1Data.FIRST_NAME = leadData.FIRST_NAME;
			leadPage1Data.MIDDLE_NAME = leadData.MIDDLE_NAME;
			leadPage1Data.LAST_NAME = leadData.LAST_NAME;
			leadPage1Data.CURNT_ADDRESS1 = leadData.CURNT_ADDRESS1;
			leadPage1Data.CURNT_ADDRESS2 = leadData.CURNT_ADDRESS2;
			leadPage1Data.CURNT_ADDRESS3 = leadData.CURNT_ADDRESS3;
			leadPage1Data.CURNT_DISTRICT_LANDMARK = leadData.CURNT_DISTRICT_LANDMARK;
			leadPage1Data.CURNT_ZIP_CODE = leadData.CURNT_ZIP_CODE;
			leadPage1Data.BIRTH_DATE = leadData.BIRTH_DATE;
			leadPage1Data.CURRENT_AGE = leadData.CURRENT_AGE;
			leadPage1Data.EMAIL_ID = leadData.EMAIL_ID;
			leadPage1Data.MOBILE_NO = leadData.MOBILE_NO;
			leadPage1Data.LANDLINE_NO = leadData.LANDLINE_NO;
			leadPage1Data.ALTERNATE_NO = leadData.ALTERNATE_NO;
			leadPage1Data.PRIMARY_CONTACT_FLAG = leadData.PRIMARY_CONTACT_FLAG;
			leadPage1Data.GENDER_CD = leadData.GENDER_CD;
			leadPage1Data.MARITAL_STATUS_CD = leadData.MARITAL_STATUS_CD;
			leadPage1Data.MARITAL_STATUS_VAL = leadData.MARITAL_STATUS_VAL;
			leadPage1Data.CURNT_STATE = leadData.CURNT_STATE;
			leadPage1Data.CURNT_CITY = leadData.CURNT_CITY;
			leadPage1Data.IPAD_LEAD_ID = leadData.IPAD_LEAD_ID;
			leadPage1Data.CRM_LEAD_ID = leadData.CRM_LEAD_ID;
			this.ekycDisabled = false;
			this.disableInsDtlsView = null;
			this.disablePropDtlsView = null;
			this.insConfirmed = null;
			this.propConfirmed = null;
			return leadPage1Data;
		}
		else{
			return {};
		}
	};

	this.getLeadPage2Data = function(leadData){
		if(!!leadData){
			var leadPage2Data = {};
			leadPage2Data.LEAD_SOURCE1_CD = leadData.LEAD_SOURCE1_CD;
			leadPage2Data.LEAD_SUB_SOURCE1_CD = leadData.LEAD_SUB_SOURCE1_CD;
			leadPage2Data.LEAD_SOURCE2_CD = leadData.LEAD_SOURCE2_CD;
			leadPage2Data.LEAD_SOURCE3_CD = leadData.LEAD_SOURCE3_CD;
			leadPage2Data.NO_OF_CHILDREN_CD = leadData.NO_OF_CHILDREN_CD;
			leadPage2Data.SPECIAL_REMARK = leadData.SPECIAL_REMARK;
			leadPage2Data.LEAD_STATUS_CD = leadData.LEAD_STATUS_CD;
			leadPage2Data.LEAD_SUB_STATUS_CD = leadData.LEAD_SUB_STATUS_CD;
			leadPage2Data.CAMPAIGN_NAME = leadData.CAMPAIGN_NAME;
			leadPage2Data.ANALYTIC_RECO_PRODUCT = leadData.ANALYTIC_RECO_PRODUCT;
			leadPage2Data.PROPOSED_SERVICE_TYPE_ID = leadData.PROPOSED_SERVICE_TYPE_ID;
			leadPage2Data.REFERENCE_LEAD_ID = leadData.REFERENCE_LEAD_ID;
			leadPage2Data.REFERRED_BY = leadData.REFERRED_BY;
			leadPage2Data.RELATIONSHIP_CD = leadData.RELATIONSHIP_CD;
			leadPage2Data.INCOME_GROUP_CD = leadData.INCOME_GROUP_CD;
			leadPage2Data.OCCUPATION_CD = leadData.OCCUPATION_CD;
			leadPage2Data.PRODUCT_PITCHED = leadData.PRODUCT_PITCHED;
			leadPage2Data.LEAD_CREATED_DTTM = leadData.LEAD_CREATED_DTTM;
			leadPage2Data.IPAD_MODIFIED_DTTM = leadData.IPAD_MODIFIED_DTTM;
			if(!!leadData.IPAD_ALTERNATE_NO)
				leadPage2Data.IPAD_ALTERNATE_NO = parseInt(leadData.IPAD_ALTERNATE_NO);
			leadPage2Data.IPAD_SPECIAL_REMARK = leadData.IPAD_SPECIAL_REMARK;
			leadPage2Data.IPAD_EMAIL_ID = leadData.IPAD_EMAIL_ID;
			leadPage2Data.DESIRE_SERVICE_TYPE_ID = leadData.DESIRE_SERVICE_TYPE_ID;
			return leadPage2Data;
		}
		else{
			return {};
		}
	};

	this.getLeadData = function(leadId){
		var dfd = $q.defer();
		if(!!leadId){
			CommonService.selectRecords(db, "lp_mylead", true, null, {"ipad_lead_id": leadId}).then(
				function(res){
					if(!!res && res.rows.length>0){
						var _leadData = res.rows.item(0);
						al.leadData = JSON.parse(JSON.stringify(res.rows.item(0)));
						_leadData.LEAD_NAME = _leadData.LEAD_NAME.trim();
						if(!!_leadData.LEAD_NAME){
							var _leadNameSplitArr = _leadData.LEAD_NAME.split(" ");
							if(_leadNameSplitArr.length==1)
								_leadData.FIRST_NAME = _leadNameSplitArr[0];
							if(_leadNameSplitArr.length==2){
								_leadData.FIRST_NAME = _leadNameSplitArr[0];
								_leadData.LAST_NAME = _leadNameSplitArr[1];
							}
							else if(_leadNameSplitArr.length>2){
								_leadData.FIRST_NAME = _leadNameSplitArr[0];
								_leadData.LAST_NAME = _leadNameSplitArr[_leadNameSplitArr.length-1];
								var _middleName = "";
								for(var i=1;i<(_leadNameSplitArr.length-1); i++){
									_middleName += _leadNameSplitArr[i] + " ";
								}
								_leadData.MIDDLE_NAME = _middleName.trim();
							}
							if(!!_leadData.MOBILE_NO)
								_leadData.MOBILE_NO = parseInt(_leadData.MOBILE_NO);
							if(!!_leadData.LANDLINE_NO)
								_leadData.LANDLINE_NO = parseInt(_leadData.LANDLINE_NO);
							if(!!_leadData.ALTERNATE_NO)
								_leadData.ALTERNATE_NO = parseInt(_leadData.ALTERNATE_NO);
							if(!!_leadData.CURNT_ZIP_CODE)
								_leadData.CURNT_ZIP_CODE = parseInt(_leadData.CURNT_ZIP_CODE);
							if(!!_leadData.IPAD_ALTERNATE_NO)
								_leadData.IPAD_ALTERNATE_NO = parseInt(_leadData.IPAD_ALTERNATE_NO);
						}
						debug("_leadData: " + JSON.stringify(_leadData));
						dfd.resolve(_leadData);
					}
					else{
						dfd.resolve(null);
					}
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.getLeadEKYCData = function(leadId,cust_type,buy_for,aadhar_no) {
		var dfd = $q.defer();
		var _leadEKYCData = null;
		if(!!leadId){
			var whereClauseObj = {};
			whereClauseObj.LEAD_ID = leadId;
			if(!!cust_type)
				whereClauseObj.CUST_TYPE = cust_type;
			if(!!buy_for)
				whereClauseObj.BUY_FOR_CD = buy_for;
		  if(!!aadhar_no)
				whereClauseObj.CONSUMER_AADHAR_NO = aadhar_no;

			CommonService.selectRecords(db, "LP_LEAD_EKYC_DTLS", true, null, whereClauseObj).then(
				function(res){
					if(!!res && res.rows.length>0){
						if(!!cust_type && cust_type == 'C01')
						{
							_leadEKYCData = CommonService.resultSetToObject(res);
							if(!(_leadEKYCData instanceof Array))
								_leadEKYCData = [_leadEKYCData];
							debug("_leadEKYCData: " + JSON.stringify(_leadEKYCData));
							dfd.resolve(_leadEKYCData);
						}
						else{
							_leadEKYCData = res.rows.item(0);
							var ekycNo = _leadEKYCData.CONSUMER_AADHAR_NO;
							_leadEKYCData.CONSUMER_AADHAR_NO = parseInt(ekycNo);
							al.leadEKYCDetails = JSON.parse(JSON.stringify(_leadEKYCData));
							debug("_leadEKYCData: " + JSON.stringify(_leadEKYCData));
							dfd.resolve(_leadEKYCData);
						}
					}
					else{
						dfd.resolve(_leadEKYCData);
					}
				}
			);
		}
		else{
			dfd.resolve(_leadEKYCData);
		}
		return dfd.promise;
	};
}]);
