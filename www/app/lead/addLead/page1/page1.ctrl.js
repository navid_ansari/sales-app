page1Module.controller('Page1Ctrl', ['$state', 'MaritalStatusList', 'StateList', 'Page1Service', 'PreferredContactList', 'CommonService', 'CityList', 'LeadData', '$filter', 'AddLeadService', function($state, MaritalStatusList, StateList, Page1Service, PreferredContactList, CommonService, CityList, LeadData, $filter, AddLeadService){
	"use strict";

	var pc = this;

	this.submitButtonLabel = "Next";

	if(BUSINESS_TYPE==="IndusSolution"){
		this.submitButtonLabel = "Submit";
	}

	this.page1Data = LeadData || {};

	if(!!LeadData){
		AddLeadService.editLead = true;
		if(!!AddLeadService.leadData){
			if(!!AddLeadService.leadData.Other_CityName){
				this.page1Data.Other_CityName = AddLeadService.leadData.Other_CityName;
			}
		}
	}

	this.page1Data.IPAD_LEAD_ID = this.page1Data.IPAD_LEAD_ID || CommonService.getRandomNumber();
	this.page1Data.LEAD_SOURCE1_CD = this.page1Data.LEAD_SOURCE1_CD || "14";

	if(!!this.page1Data.BIRTH_DATE){
		this.dob = CommonService.formatDobFromDb(this.page1Data.BIRTH_DATE);
	}

	this.nameRegex = NAME_REGEX;
	this.middleNameRegex = FULLNAME_REGEX;
	this.mobileRegex = MOBILE_REGEX;
	this.emailRegex = EMAIL_REGEX;
	this.zipcodeRegex = PINCODE_REG;
	this.numberRegex = NUM_REG;
	this.freeTxtRegex = FREE_TEXT_REGEX;

	this.preferredContactList = PreferredContactList;
	this.maritalStatusList = MaritalStatusList;
	this.stateList = StateList;
	this.cityList = CityList;

	this.preferredContactOptionSelected = this.preferredContactList[0];
	this.maritalStatusOptionSelected = this.maritalStatusList[0];
	this.stateOptionSelected = this.stateList[0];
	this.cityOptionSelected = this.cityList[0];

	if(!!this.page1Data.PRIMARY_CONTACT_FLAG){
		var prefContact = $filter("filter")(this.preferredContactList, {"LOV_CD": pc.page1Data.PRIMARY_CONTACT_FLAG})[0];
		this.preferredContactOptionSelected = prefContact || this.preferredContactOptionSelected;
	}

	if(!!this.page1Data.MARITAL_STATUS_CD){
		this.maritalStatusOptionSelected = $filter("filter")(this.maritalStatusList, {"LOV_CD": pc.page1Data.MARITAL_STATUS_CD})[0];
	}

	if(!!this.page1Data.CURNT_STATE){
		var state = $filter("filter")(this.stateList, {"STATE_CODE": pc.page1Data.CURNT_STATE})[0];
		this.stateOptionSelected = state || this.stateOptionSelected;
	}

	if(!!this.page1Data.CURNT_CITY){
		var city = $filter("filter")(this.cityList, {"CITY_CODE": pc.page1Data.CURNT_CITY})[0];
		this.cityOptionSelected = city || this.cityOptionSelected;
	}

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.onMaritalStatusChange = function(){
		this.page1Data.MARITAL_STATUS_CD = this.maritalStatusOptionSelected.LOV_CD;
		this.page1Data.MARITAL_STATUS_VAL = this.maritalStatusOptionSelected.LOV_MAP_VALUE;
	};

	this.onPreferredContactChange = function(){
		this.page1Data.PRIMARY_CONTACT_FLAG = this.preferredContactOptionSelected.LOV_CD;
	};

	this.onStateChange = function(){
		this.page1Data.CURNT_STATE = this.stateOptionSelected.STATE_CODE;
		debug("state code is "+this.stateOptionSelected);
		debug("state code is "+this.stateOptionSelected.STATE_CODE);
		Page1Service.getCityList(this.page1Data.CURNT_STATE).then(
			function(res){
				pc.cityList = res;
			}
		);
	};

	this.onCityChange = function(){
		this.page1Data.CURNT_CITY = this.cityOptionSelected.CITY_CODE;
	};

	this.onDOBChange = function(){
		this.page1Data.BIRTH_DATE = CommonService.formatDobToDb(this.dob);
	};

	this.onNext = function(){
		CommonService.showLoading();
		Page1Service.onSubmit(JSON.parse(JSON.stringify(this.page1Data))).then(
			function(res){
				if(!!res){
					if(BUSINESS_TYPE==="IndusSolution"){
						try{
							debug("calling EKYC" + pc.page1Data.IPAD_LEAD_ID);
							$state.go("leadPage3", {LEAD_ID: pc.page1Data.IPAD_LEAD_ID});
						}catch(e){
							CommonService.hideLoading();
							debug("EXCEPTION in EKYC:"+e.message);
						}
					}
					else{
						$state.go("leadPage2");
					}
				}
				else{
					navigator.notification.alert("Could not add lead.",function(){
						CommonService.hideLoading();
					},"Lead","OK");
				}
			}
		);
	};
	CommonService.hideLoading();
}]);


page1Module.service('Page1Service', ['$q', '$state', 'CommonService', 'LoginService', 'AddLeadService', function($q, $state, CommonService, LoginService, AddLeadService){
	"use strict";

	this.getPreferredContactList = function(){
		var dfd = $q.defer();
		var preferredContactList = [];
		preferredContactList.push({"LOV_VALUE": "Select Preferred Contact", "LOV_CD": null});
		try{
			CommonService.selectRecords(db, 'lp_lead_lov_master', true, 'lov_value, lov_cd', {"lov_type_cd": "11"}).then(
				function(res){
					if(!!res && res.rows.length>0){
						for(var i=0;i<res.rows.length;i++){
							preferredContactList.push(res.rows.item(i));
						}
						dfd.resolve(preferredContactList);
					}
					else{
						dfd.resolve(preferredContactList);
					}
				}
			);
		}
		catch(ex){
			debug("Exception in getPreferredContactList(): " + ex.message);
			dfd.resolve(preferredContactList);
		}
		return dfd.promise;
	};

	this.getMaritalStatusList = function(){
		var dfd = $q.defer();
		var maritalStatusList = [];
		maritalStatusList.push({"LOV_VALUE": "Select Marital Status", "LOV_CD": null, "LOV_MAP_VALUE": null});
		try{
			CommonService.selectRecords(db, 'lp_lead_lov_master', true, 'lov_value, lov_cd, lov_map_value', {"lov_type_cd": "5"}).then(
				function(res){
					if(!!res && res.rows.length>0){
						for(var i=0;i<res.rows.length;i++){
							maritalStatusList.push(res.rows.item(i));
						}
						dfd.resolve(maritalStatusList);
					}
					else{
						dfd.resolve(maritalStatusList);
					}
				}
			);
		}
		catch(ex){
			debug("Exception in getMaritalStatusList(): " + ex.message);
			dfd.resolve(maritalStatusList);
		}
		return dfd.promise;
	};

	this.getStateList = function(){
		var dfd = $q.defer();
		var stateList = [];
		stateList.push({"STATE_DESC": "Select State", "STATE_CODE": null});
		try{
			CommonService.selectRecords(db, 'lp_state_master', true, 'state_desc, state_code', null, "state_desc").then(
				function(res){
					if(!!res && res.rows.length>0){
						for(var i=0;i<res.rows.length;i++){
							stateList.push(res.rows.item(i));
						}
						dfd.resolve(stateList);
					}
					else{
						dfd.resolve(stateList);
					}
				}
			);
		}
		catch(ex){
			dfd.resolve(stateList);
		}
		return dfd.promise;
	};

	this.getCityList = function(stateCode){
		var dfd = $q.defer();
		var cityList = [];
		cityList.push({"CITY_DESC": "Select City", "CITY_CODE": null});
		try{
			CommonService.transaction(db,
				function(tx) {
					CommonService.executeSql(tx, 'select city_desc, city_code from lp_city_master where state_code in (?,?) and isactive=? order by city_desc', [stateCode, "0", "Y"],
						function(tx, res){
							if(!!res && res.rows.length>0){
								for(var i=0;i<res.rows.length;i++){
									cityList.push(res.rows.item(i));
								}
								dfd.resolve(cityList);
							}
							else{
								dfd.resolve(cityList);
							}
						},
						function(tx, err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		catch(ex){
			dfd.resolve(cityList);
		}
		return dfd.promise;
	};

	this.onSubmit = function(leadData){
		var dfd = $q.defer();
		leadData.LEAD_CREATED_DTTM = leadData.LEAD_CREATED_DTTM || CommonService.getCurrDate();
		leadData.IPAD_MODIFIED_DTTM = CommonService.getCurrDate();
		leadData.AGENT_CD = leadData.AGENT_CD || LoginService.lgnSrvObj.userinfo.AGENT_CD;
		leadData.LEAD_NAME = leadData.FIRST_NAME + ((!!leadData.MIDDLE_NAME)?(" " + leadData.MIDDLE_NAME):"") + (" " + leadData.LAST_NAME);

		delete leadData.FIRST_NAME;
		delete leadData.MIDDLE_NAME;
		delete leadData.LAST_NAME;

		if(!AddLeadService.leadData){
			AddLeadService.leadData = {};
		}
		Object.assign(AddLeadService.leadData, leadData);

		CommonService.insertOrReplaceRecord(db, 'lp_mylead', AddLeadService.leadData, true).then(
			function(res){
				if(!!res){
					dfd.resolve(res);
				}
				else{
					dfd.resolve(null);
				}
			}
		);

		debug("leadData: " + JSON.stringify(leadData));
		return dfd.promise;
	};
}]);
