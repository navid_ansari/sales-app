addLeadModule.directive("addLeadFooter", ["$window", function($window){
	return{
		restrict: "C",
		link: function(scope, element, attrs){
			var screenSize = window.innerHeight;
			$window.addEventListener('native.showkeyboard', showkeyboard);
			$window.addEventListener('native.hidekeyboard', hidekeyboard);
			function showkeyboard(){
				debug("screenSize: " + screenSize);
				if(screenSize<700){
					document.querySelector('.add-lead .has-header').classList.add('add-lead-content-footer-hide');
					document.querySelector('.add-lead-footer').classList.add('add-lead-footer-hide');
				}
				else{
					document.querySelector('.add-lead .has-header').classList.remove('add-lead-content-footer-hide');
					document.querySelector('.add-lead-footer').classList.remove('add-lead-footer-hide');
				}
			}
			function hidekeyboard(){
				document.querySelector('.add-lead .has-header').classList.remove('add-lead-content-footer-hide');
				document.querySelector('.add-lead-footer').classList.remove('add-lead-footer-hide');
			}
		}
	};
}]);
