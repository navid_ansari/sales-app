leadEKYCModule.controller('leadEKYCCtrl',['$state', 'CommonService', 'leadEKYCService', 'AddLeadService', 'PurchasingInsuranceForList', 'InsuranceBuyingForList', 'leadEKYCInsuredData', 'leadEKYCProposerData', '$q', function($state, CommonService, leadEKYCService, AddLeadService, PurchasingInsuranceForList, InsuranceBuyingForList, leadEKYCInsuredData, leadEKYCProposerData, $q){
	"use strict";
	debug("leadEKycCtrl");

	var sc = this;

	this.aadharRegex = ADHAAR_REG;

	this.purchasingInsuranceForList = PurchasingInsuranceForList;
	this.insuranceBuyingForList = InsuranceBuyingForList;

	this.showConsent = false;
	this.showInsConsent = false;
	this.mandateHide = false;

	this.leadEKYCData = {};
	this.leadFormAData_eKyc = {};
	this.leadEKYCData.insured = {"CUST_TYPE": INSURED_CUST_TYPE};
	this.insAadharList = leadEKYCInsuredData;
	this.leadEKYCData.proposer = (!!leadEKYCProposerData ? leadEKYCProposerData : {"CUST_TYPE": PROPOSER_CUST_TYPE});
	this.purchaseForOptionSelected = this.purchasingInsuranceForList[0];
	this.buyingForOptionSelected = this.insuranceBuyingForList[0];
	debug("Lead data: "+JSON.stringify(AddLeadService.leadData));
	debug("leadEKYCProposerData: "+JSON.stringify(leadEKYCProposerData));

	this.LEAD_NAME = AddLeadService.leadData.LEAD_NAME;
	if((!!leadEKYCProposerData) && (leadEKYCProposerData.EKYC_FLAG == 'Y') && (!!leadEKYCProposerData.CONSUMER_AADHAR_NO)){
		this.ekycDisabled = true;
		this.insBuyForDisabled = true;
		this.leadEKYCData.proposer.CONSUMER_AADHAR_NO = parseInt(leadEKYCProposerData.CONSUMER_AADHAR_NO);
	}

	this.INSURED_EKYC_CONSENT_FLAG = (!!this.leadEKYCData.insured ? (this.leadEKYCData.insured.EKYC_FLAG || "N") : "N");
	this.PROPOSER_EKYC_CONSENT_FLAG = (!!this.leadEKYCData.proposer ? (this.leadEKYCData.proposer.EKYC_FLAG || "N") : "N");

	this.INSURED_EKYC_CONSENT_FLAG_PERSIST = (!!this.leadEKYCData.insured ? (this.leadEKYCData.insured.EKYC_FLAG || "N") : "N");
	this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = (!!this.leadEKYCData.proposer ? (this.leadEKYCData.proposer.EKYC_FLAG || "N") : "N");

	this.INSURED_EKYC_FLAG = "N";
	this.PROPOSER_EKYC_FLAG = (!!this.leadEKYCData.proposer.CONSUMER_AADHAR_NO?"Y":"N") || "N";

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.onBack = function(){
		CommonService.showLoading();
		if(BUSINESS_TYPE==="IndusSolution"){
			$state.go("leadPage1");
		}
		else{
			$state.go("leadPage2");
		}
	};

	this.onAadharFlagChange = function(type){
		if(!!type && type=="insured"){
			if(!this.INSURED_EKYC_FLAG || this.INSURED_EKYC_FLAG=="N"){
				this.leadEKYCData.insured.CONSUMER_AADHAR_NO = null;
				this.INSURED_EKYC_CONSENT_FLAG_PERSIST = null;
				this.INSURED_EKYC_CONSENT_FLAG = null;
				this.leadEKYCForm.insuredAadharNum.$validate();
			}
		}
		else{
			if(!this.PROPOSER_EKYC_FLAG || this.PROPOSER_EKYC_FLAG=="N"){
				this.leadEKYCData.proposer.CONSUMER_AADHAR_NO = null;
				this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = null;
				this.PROPOSER_EKYC_CONSENT_FLAG = null;
				this.leadEKYCForm.proposerAadharNum.$validate();
			}
		}
	};

	this.onInsAadharNumberChange = function(){
		this.INSURED_EKYC_CONSENT_FLAG_PERSIST = null;
		this.INSURED_EKYC_CONSENT_FLAG = null;
	};

	this.onPropAadharNumberChange = function(){
		this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = null;
		this.PROPOSER_EKYC_CONSENT_FLAG = null;
	};

	this.onPurInsForChg = function(){
		debug("this.purchaseForOptionSelected: " + JSON.stringify(this.purchaseForOptionSelected));
		this.leadEKYCData.insured.PUR_FOR_CD = this.purchaseForOptionSelected.PUR_NBFE_CODE;
		this.leadFormAData_eKyc.PURCHASING_INSURANCE_FOR = this.purchaseForOptionSelected.PURCHASE_FOR;
		this.leadFormAData_eKyc.PURCHASING_INSURANCE_FOR_CODE = this.purchaseForOptionSelected.PUR_NBFE_CODE;
		leadEKYCService.getInsuranceBuyingForList(this.purchaseForOptionSelected).then(
			function(insBuyForList){
				sc.insuranceBuyingForList = insBuyForList;
				sc.buyingForOptionSelected = insBuyForList[0];
			}
		);
	};

	this.onInsBuyForChg = function(){
		debug("this.buyingForOptionSelected: " + JSON.stringify(this.buyingForOptionSelected));

		this.leadEKYCData.insured.BUY_FOR_CD = this.buyingForOptionSelected.BUY_NBFE_CODE;
		this.leadFormAData_eKyc.INSURANCE_BUYING_FOR = this.buyingForOptionSelected.BUYING_FOR;
		this.leadFormAData_eKyc.INSURANCE_BUYING_FOR_CODE = this.buyingForOptionSelected.BUY_NBFE_CODE;

		if(!this.leadEKYCData.insured.BUY_FOR_CD){
			this.INSURED_EKYC_FLAG = "N";
			this.PROPOSER_EKYC_FLAG = "N";
			this.leadEKYCData.insured.EKYC_DONE_DATE = null;
			this.leadEKYCData.proposer.EKYC_DONE_DATE = null;
			this.onAadharFlagChange('insured');
			this.onAadharFlagChange('proposer');
		}

		if(this.leadFormAData_eKyc.INSURANCE_BUYING_FOR_CODE == "200"){
			this.leadFormAData_eKyc.PROPOSER_IS_INSURED="Y";{
				this.PROPOSER_EKYC_FLAG = "N";
				this.leadEKYCData.proposer.EKYC_DONE_DATE = null;
				this.onAadharFlagChange('proposer');
			}
		}
		else{
			this.leadFormAData_eKyc.PROPOSER_IS_INSURED="N";
			this.onAadharFlagChange('proposer');
		}
	};

	this.searchInsuredEKYC = function() {
		var dfd = $q.defer();
		if(!!sc.leadEKYCData.insured.CONSUMER_AADHAR_NO){
			AddLeadService.getLeadEKYCData(AddLeadService.leadData.IPAD_LEAD_ID, 'C01', null, sc.leadEKYCData.insured.CONSUMER_AADHAR_NO+"").then(function(insEKYC) {
				debug("serch leng: " + JSON.stringify(insEKYC));
				if(!!insEKYC && !!insEKYC[0] && !!insEKYC[0].CONSUMER_AADHAR_NO && !!insEKYC[0].EKYC_FLAG){
					navigator.notification.alert("Aadhar Number - "+insEKYC[0].CONSUMER_AADHAR_NO +" already exists.");
				}
				else if(!!leadEKYCInsuredData && parseInt(leadEKYCInsuredData.length) == 5){
					navigator.notification.alert("Only 5 Aadhar Numbers can be added per Lead.");
				}
				else{
					dfd.resolve(insEKYC);
				}
			});
		}
		else{
			navigator.notification.alert("Please enter Insured Aadhar Number");
		}
		return dfd.promise;
	};

	this.openConsent = function(type){
		if(type=="insured") {
			sc.searchInsuredEKYC().then(function(insEKYC){
				debug("Show ins consent");
					sc.showInsConsent = true;
					sc.showConsent = true;
			});
		}
		else if(type=="proposer") {
			this.showInsConsent = false;
			this.showConsent = true;
		}
	};

	this.onConsentSubmit = function(){
		function showConfirmation(){
			var dfd = $q.defer();
			navigator.notification.confirm("If the consent for e-kyc is not provided the e-kyc cannot be opted, do you want to continue without e-kyc?",function(buttonIndex) {
				if(buttonIndex=="1"){
					dfd.resolve(true);
				}
			},"E-KYC",["Yes", "No"]);
			return dfd.promise;
		}
		if((this.showInsConsent && this.INSURED_EKYC_CONSENT_FLAG!="Y") || (!this.showInsConsent && this.PROPOSER_EKYC_CONSENT_FLAG!="Y")){
			showConfirmation().then(
				function(res){
					if(!!res){
						sc.showConsent = false;
						sc.INSURED_EKYC_CONSENT_FLAG_PERSIST = sc.INSURED_EKYC_CONSENT_FLAG;
						sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = sc.PROPOSER_EKYC_CONSENT_FLAG;
						debug("values set: ins_con_pers: " + sc.INSURED_EKYC_CONSENT_FLAG_PERSIST);
						debug("values set: ins_con: " + sc.INSURED_EKYC_CONSENT_FLAG);
						debug("values set: pro_con_pers: " + sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST);
						debug("values set: pro_con: " + sc.PROPOSER_EKYC_CONSENT_FLAG);
					}
				}
			);
		}
		else{
			this.showConsent = false;
			this.INSURED_EKYC_CONSENT_FLAG_PERSIST = this.INSURED_EKYC_CONSENT_FLAG;
			this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST = this.PROPOSER_EKYC_CONSENT_FLAG;
		}
	};

	this.closeConsent = function(){
		this.showConsent = false;
		this.INSURED_EKYC_CONSENT_FLAG = this.INSURED_EKYC_CONSENT_FLAG_PERSIST;
		this.PROPOSER_EKYC_CONSENT_FLAG = this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST;
	};

	this.duplicateAadharNo = function(){
		var dfd = $q.defer();
		if(!!sc.leadEKYCData.proposer.CONSUMER_AADHAR_NO && sc.leadEKYCData.insured.CONSUMER_AADHAR_NO){
			if(sc.leadEKYCData.proposer.CONSUMER_AADHAR_NO == sc.leadEKYCData.insured.CONSUMER_AADHAR_NO){
				navigator.notification.alert("Insured and Proposer Aadhar No. cannot be same.");
			}
			else{
				dfd.resolve("S");
			}
		}
		else{
			dfd.resolve("S");
		}
		return dfd.promise;
	};

	this.onNext = function(isPristine){
		CommonService.showLoading();
		debug("this.INSURED_EKYC_CONSENT_FLAG_PERSIST: " + this.INSURED_EKYC_CONSENT_FLAG_PERSIST);
		debug("this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST: " + this.PROPOSER_EKYC_CONSENT_FLAG_PERSIST);
		debug("this.leadEKYCData.proposer : "+JSON.stringify(this.leadEKYCData.proposer));
		sc.duplicateAadharNo().then(function(dupResp){
			if(!!dupResp){
				if((!sc.leadEKYCData.insured.EKYC_FLAG && !sc.leadEKYCData.insured.EKYC_DONE_DATE) || (!sc.leadEKYCData.proposer.EKYC_FLAG && !sc.leadEKYCData.proposer.EKYC_DONE_DATE)){
					if((!!sc.INSURED_EKYC_FLAG && sc.INSURED_EKYC_FLAG=='Y') && (!sc.INSURED_EKYC_CONSENT_FLAG_PERSIST || sc.INSURED_EKYC_CONSENT_FLAG_PERSIST=='N')) {
						navigator.notification.confirm("Insured's consent is not provided, would you like to continue without E-KYC for the insured?",function(buttonIndex) {
							if(buttonIndex=="1"){
								sc.leadEKYCData.insured.EKYC_FLAG = 'N';
								if(!!sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST && sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST=='Y'){
									sc.leadEKYCData.proposer.EKYC_FLAG = 'Y';
								}
								else{
									sc.leadEKYCData.proposer.EKYC_FLAG = 'N';
									if(sc.PROPOSER_EKYC_FLAG!="Y"){
										sc.leadEKYCData.proposer.CONSUMER_AADHAR_NO = null;
									}
								}
								if(((!!sc.PROPOSER_EKYC_FLAG && sc.PROPOSER_EKYC_FLAG=='Y')) && (!sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST || sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST=='N')) {
									navigator.notification.confirm("Proposer's consent is not provided, would you like to continue without E-KYC for the proposer?",function(buttonIndex) {
										if(buttonIndex=="1"){
											sc.leadEKYCData.proposer.EKYC_FLAG = 'N';
											leadEKYCService.onNext(sc.leadEKYCData, sc.leadFormAData_eKyc, isPristine);
										}
										else{
											CommonService.hideLoading();
										}
									},"E-KYC","Yes, No");
								}
								else{
									leadEKYCService.onNext(sc.leadEKYCData, sc.leadFormAData_eKyc, isPristine);
								}
							}
							else{
								CommonService.hideLoading();
							}
						},"E-KYC","Yes, No");
					}
					else if(((!!sc.PROPOSER_EKYC_FLAG && sc.PROPOSER_EKYC_FLAG=='Y')) && (!sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST || sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST=='N')) {
						navigator.notification.confirm("Proposer's consent is not provided, would you like to continue without E-KYC for the proposer?",function(buttonIndex) {
							if(buttonIndex=="1"){
								sc.leadEKYCData.proposer.EKYC_FLAG = 'N';
								if(!!sc.INSURED_EKYC_CONSENT_FLAG_PERSIST && sc.INSURED_EKYC_CONSENT_FLAG_PERSIST=='Y'){
									sc.leadEKYCData.insured.EKYC_FLAG = 'Y';
								}
								else{
									sc.leadEKYCData.insured.CONSUMER_AADHAR_NO = null;
									if(sc.INSURED_EKYC_FLAG!="Y"){
										sc.leadEKYCData.insured.EKYC_FLAG = 'N';
									}
								}
								leadEKYCService.onNext(sc.leadEKYCData, sc.leadFormAData_eKyc, isPristine);
							}
							else{
								CommonService.hideLoading();
							}
						},"E-KYC","Yes, No");
					}
					else {
						debug("in else");
						debug("sc.INSURED_EKYC_CONSENT_FLAG_PERSIST: " + sc.INSURED_EKYC_CONSENT_FLAG_PERSIST);
						debug("sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST: " + sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST);

						if(!!sc.INSURED_EKYC_CONSENT_FLAG_PERSIST && sc.INSURED_EKYC_CONSENT_FLAG_PERSIST=='Y'){
							sc.leadEKYCData.insured.EKYC_FLAG = 'Y';
						}
						else{
							sc.leadEKYCData.insured.EKYC_FLAG = 'N';
							if(sc.INSURED_EKYC_FLAG!="Y"){
								sc.leadEKYCData.insured.CONSUMER_AADHAR_NO = null;
							}
						}
						debug("sc.leadEKYCData.insured.EKYC_FLAG: " + sc.leadEKYCData.insured.EKYC_FLAG);

						if(!!sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST && sc.PROPOSER_EKYC_CONSENT_FLAG_PERSIST=='Y'){
							sc.leadEKYCData.proposer.EKYC_FLAG = 'Y';
						}
						else{
							sc.leadEKYCData.proposer.EKYC_FLAG = 'N';
							if(sc.PROPOSER_EKYC_FLAG!="Y"){
								sc.leadEKYCData.proposer.CONSUMER_AADHAR_NO = null;
							}
						}
						debug("sc.leadEKYCData.proposer.EKYC_FLAG: " + sc.leadEKYCData.proposer.EKYC_FLAG);
						leadEKYCService.onNext(sc.leadEKYCData, sc.leadFormAData_eKyc, isPristine);
					}
				}
				else {
					leadEKYCService.onNext(sc.leadEKYCData, sc.leadFormAData_eKyc, isPristine);
				}
			}
			else{
				CommonService.hideLoading();
			}
		});
	};
	CommonService.hideLoading();
}]);

leadEKYCModule.service('leadEKYCService',['CommonService','LoginService', '$q', '$http', 'AddLeadService', '$state', function(CommonService, LoginService, $q, $http, AddLeadService, $state){
	"use strict";

	var sc = this;

	this.leadEKYCData = null;
	this.leadFormAData_eKyc = null;

	this.getPurchasingInsuranceForList = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct pur_nbfe_code,purchase_for from lp_insured_relation where isactive=? order by purchase_for asc",["Y"],
					function(tx,res){
						var purchasingInsuranceForList = [];
						debug("purchasingInsuranceForList res.rows.length: " + res.rows.length);
						if(!!res && res.rows.length>0){
							for(var i=0;i<res.rows.length;i++){
								var purchasingInsuranceFor = {};
								purchasingInsuranceFor.PURCHASE_FOR = res.rows.item(i).PURCHASE_FOR;
								purchasingInsuranceFor.PUR_NBFE_CODE = res.rows.item(i).PUR_NBFE_CODE;
								purchasingInsuranceForList.push(purchasingInsuranceFor);
							}
							dfd.resolve(purchasingInsuranceForList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.getInsuranceBuyingForList = function(purchasingInsuranceFor){
		var dfd = $q.defer();
		var nbfeCodeStr = "";
		var parameterList = ["Y"];
		if(!!purchasingInsuranceFor && !!purchasingInsuranceFor.PUR_NBFE_CODE){
			nbfeCodeStr = "and pur_nbfe_code=? ";
			parameterList.push(purchasingInsuranceFor.PUR_NBFE_CODE || "");
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct buy_nbfe_code,buying_for from lp_insured_relation where isactive=? " + nbfeCodeStr + "order by buying_for asc",parameterList,
					function(tx,res){
						var insuranceBuyingForList = [];
						debug("insuranceBuyingForList res.rows.length: " + res.rows.length);
						if(!!res && res.rows.length>0){
							insuranceBuyingForList.push({"BUYING_FOR": "Select Insurance Buying For", "BUY_NBFE_CODE": null});
							for(var i=0;i<res.rows.length;i++){
								var insuranceBuyingFor = {};
								insuranceBuyingFor.BUYING_FOR = res.rows.item(i).BUYING_FOR;
								insuranceBuyingFor.BUY_NBFE_CODE = res.rows.item(i).BUY_NBFE_CODE;
								insuranceBuyingForList.push(insuranceBuyingFor);
							}
							dfd.resolve(insuranceBuyingForList);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};


	this.onNext = function(leadEKYCData, leadFormAData_eKyc, isPristine){
		debug("leadEKYCData: " + JSON.stringify(leadEKYCData));
		debug("leadFormAData_eKyc: " + JSON.stringify(leadFormAData_eKyc));
		AddLeadService.disableInsDtlsView = null;
		AddLeadService.disablePropDtlsView = null;
		if(!!leadEKYCData.insured.CONSUMER_AADHAR_NO && !leadEKYCData.insured.EKYC_DONE_DATE && leadEKYCData.insured.EKYC_FLAG=="Y"){
			AddLeadService.ekycDisabled = false;
			if(CommonService.checkConnection()==="online"){
				CommonService.ekycServletCall(leadEKYCData.insured.CONSUMER_AADHAR_NO).then(
					function(insEKycResp){
						debug("insEKycResp: " + JSON.stringify(insEKycResp));
						if(!!insEKycResp && insEKycResp.responseCode!="001"){
							navigator.notification.alert("Could not generate OTP for the insured.", function(){
								sc.leadEKYCData.insured.EKYC_FLAG = null;
								sc.leadEKYCData.proposer.EKYC_FLAG = null;
								CommonService.hideLoading();
							}, "E-KYC OTP", "OK");
						}
						else{
							if(!!leadEKYCData.proposer.CONSUMER_AADHAR_NO && !leadEKYCData.proposer.EKYC_DONE_DATE) {
								CommonService.ekycServletCall(leadEKYCData.proposer.CONSUMER_AADHAR_NO).then(
									function(propEkycResp){
										debug("propEkycResp: " + JSON.stringify(propEkycResp));
										if(!!propEkycResp && propEkycResp.responseCode!="001"){
											navigator.notification.alert("Could not generate OTP for the proposer.", function(){
												sc.leadEKYCData.insured.EKYC_FLAG = null;
												sc.leadEKYCData.proposer.EKYC_FLAG = null;
												CommonService.hideLoading();
											}, "E-KYC OTP", "OK");
										}
										else{
											AddLeadService.leadEKYCData = JSON.parse(JSON.stringify(leadEKYCData));
											if(!!propEkycResp){
												AddLeadService.leadEKYCData.proposer.RESPONSE_CODE = propEkycResp.responseCode;
												AddLeadService.leadEKYCData.proposer.RESPONSE_MSG = propEkycResp.responseMessage;
												AddLeadService.leadEKYCData.proposer.EKYC_TRAN_ID = propEkycResp.transactionId;
												AddLeadService.leadEKYCData.proposer.TIMESTAMP = propEkycResp.timeStamp;
												AddLeadService.leadEKYCData.proposerOTPResp = propEkycResp;
												AddLeadService.leadEKYCData.insuredOTPResp = insEKycResp;
												AddLeadService.leadFormAData_eKyc = JSON.parse(JSON.stringify(leadFormAData_eKyc));
												sc.gotoEKYCConfirmationPage();
											}
											else{
												navigator.notification.alert("No data found for proposer.",
													function(){
														AddLeadService.leadEKYCData.proposer.RESPONSE_CODE = null;
														AddLeadService.leadEKYCData.proposer.RESPONSE_MSG = null;
														AddLeadService.leadEKYCData.proposer.EKYC_TRAN_ID = null;
														AddLeadService.leadEKYCData.proposer.TIMESTAMP = null;
														CommonService.hideLoading();
													}, "E-KYC OTP","OK"
												);
											}
										}
									}
								);
							}
							else {
								AddLeadService.leadEKYCData = JSON.parse(JSON.stringify(leadEKYCData));
								if(!!insEKycResp){
									AddLeadService.leadEKYCData.insured.RESPONSE_CODE = insEKycResp.responseCode;
									AddLeadService.leadEKYCData.insured.RESPONSE_MSG = insEKycResp.responseMessage;
									AddLeadService.leadEKYCData.insured.EKYC_TRAN_ID = insEKycResp.transactionId;
									AddLeadService.leadEKYCData.insured.TIMESTAMP = insEKycResp.timeStamp;
									AddLeadService.leadEKYCData.proposerOTPResp = null;
									AddLeadService.leadEKYCData.insuredOTPResp = insEKycResp;
									AddLeadService.leadFormAData_eKyc = JSON.parse(JSON.stringify(leadFormAData_eKyc));
									sc.gotoEKYCConfirmationPage();
								}
								else{
									navigator.notification.alert("No data found.",
										function(){
											AddLeadService.leadEKYCData.insured.RESPONSE_CODE = null;
											AddLeadService.leadEKYCData.insured.RESPONSE_MSG = null;
											AddLeadService.leadEKYCData.insured.EKYC_TRAN_ID = null;
											AddLeadService.leadEKYCData.insured.TIMESTAMP = null;
											CommonService.hideLoading();
										}, "E-KYC OTP","OK"
									);
								}
							}
						}
					}
				);
			}
			else{
				navigator.notification.alert("Please connect to the internet to continue with E-KYC.",
					function(){
						CommonService.hideLoading();
					}, "OTP", "OK"
				);
			}
		}
		else{
			if(!!leadEKYCData.proposer.CONSUMER_AADHAR_NO && !leadEKYCData.proposer.EKYC_DONE_DATE && leadEKYCData.proposer.EKYC_FLAG=="Y") {
				AddLeadService.ekycDisabled = false;
				if(CommonService.checkConnection()=="online"){
					CommonService.ekycServletCall(leadEKYCData.proposer.CONSUMER_AADHAR_NO).then(
						function(propEkycResp){
							debug("propEkycResp: " + JSON.stringify(propEkycResp));
							if(!!propEkycResp && propEkycResp.responseCode!="001"){
								navigator.notification.alert("Could not generate OTP for the proposer.",
									function(){
										sc.leadEKYCData.insured.EKYC_FLAG = null;
										sc.leadEKYCData.proposer.EKYC_FLAG = null;
										CommonService.hideLoading();
									}, "E-KYC OTP", "OK"
								);
							}
							else {
								AddLeadService.leadEKYCData = JSON.parse(JSON.stringify(leadEKYCData));
								if(!!propEkycResp){
									AddLeadService.leadEKYCData.proposer.RESPONSE_CODE = propEkycResp.responseCode;
									AddLeadService.leadEKYCData.proposer.RESPONSE_MSG = propEkycResp.responseMessage;
									AddLeadService.leadEKYCData.proposer.EKYC_TRAN_ID = propEkycResp.transactionId;
									AddLeadService.leadEKYCData.proposer.TIMESTAMP = propEkycResp.timeStamp;
								}
								else{
									AddLeadService.leadEKYCData.proposer.RESPONSE_CODE = null;
									AddLeadService.leadEKYCData.proposer.RESPONSE_MSG = null;
									AddLeadService.leadEKYCData.proposer.EKYC_TRAN_ID = null;
									AddLeadService.leadEKYCData.proposer.TIMESTAMP = null;
								}
								AddLeadService.leadEKYCData.proposerOTPResp = propEkycResp;
								AddLeadService.leadEKYCData.insuredOTPResp = null;
								AddLeadService.leadFormAData_eKyc = JSON.parse(JSON.stringify(leadFormAData_eKyc));
								sc.gotoEKYCConfirmationPage();
							}
						}
					);
				}
				else{
					navigator.notification.alert("Please connect to the internet to continue with E-KYC.",function(){
						CommonService.hideLoading();
					},"OTP","OK");
				}
			}
			else {
				AddLeadService.leadEKYCData = JSON.parse(JSON.stringify(leadEKYCData));
				AddLeadService.leadEKYCData.proposerOTPResp = null;
				AddLeadService.leadEKYCData.insuredOTPResp = null;
				AddLeadService.leadFormAData_eKyc = JSON.parse(JSON.stringify(leadFormAData_eKyc));
				AddLeadService.ekycDisabled = true;
				sc.gotoEKYCConfirmationPage();
			}
		}
	};

	this.gotoEKYCConfirmationPage = function() {
		var dfd = $q.defer();
		$state.go("leadPage4", {LEAD_ID: AddLeadService.leadData.IPAD_LEAD_ID});
		dfd.resolve('s');
		return dfd.promise;
	};
}]);
