leadModule.service('LeadService',['$q','$state','LoginService','CommonService',function($q,$state,LoginService,CommonService){
	var self=this;
	var leadTimeSync=null;
	var policyTimeSync=null;
	var quicker_Obj=new Object();
    quicker_Obj.Lead_appnt_length=0;
    quicker_Obj.Link_Policy_Data_length=0;
    quicker_Obj.GBL_LeadArr=new Object();
    quicker_Obj.GBL_PolicyDataArr=new Object();
    var appSubmit ="No";
    self.leadAppointSync = function(){
   			var dfd = $q.defer();
			var request = {};
			var assignedLeadData = new Object();
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN = "FAL";
			assignedLeadData.REQ = request;
			debug("fetchLeadAppointment Request: " + JSON.stringify(assignedLeadData));
			CommonService.ajaxCall(FETCH_LEAD_APPOINTMENT_URL, AJAX_TYPE, TYPE_JSON, assignedLeadData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
					dfd.resolve(ajaxResp);
			});
			return dfd.promise;
    };
	self.linkPolicyData = function(){
			var dfd = $q.defer();
			var assignedLinkData = new Object();
			var request = {};
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN = "FLP";
			assignedLinkData.REQ = request;
			debug("fetchLinkPolicy Request: " + JSON.stringify(assignedLinkData));
			CommonService.ajaxCall(LINK_POLICY_DATA_URL, AJAX_TYPE, TYPE_JSON, assignedLinkData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
					dfd.resolve(ajaxResp);
			});
			return dfd.promise;
	};
	self.linkPolicyDataConfirmation = function(allLinkDataArr){

			var dfd = $q.defer();
			var linkPolicyDataSync = new Object();
			var request = {};
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN="CFLP";
			request.LRD=policyTimeSync;
			request.CnfPolicyResp=allLinkDataArr;
			linkPolicyDataSync.REQ = request;
			debug("fetchLinkPolicyConfirmation Request: " + JSON.stringify(linkPolicyDataSync));
			CommonService.ajaxCall(LINK_POLICY_DATA_URL, AJAX_TYPE, TYPE_JSON, linkPolicyDataSync, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
					dfd.resolve(ajaxResp);
			});
			return dfd.promise;
    };
    self.leadAppointSyncConfirmation = function(alleadArr){
			var leadRespTimeStamp=leadTimeSync;
			var leadAndAppntData = new Object();
			var dfd = $q.defer();
			var request = {};
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN = "CFAL";
			request.LRD=leadRespTimeStamp;
			request.CnfLeadResp=alleadArr;
			leadAndAppntData.REQ = request;
			debug("fetchLeadAppointmentConfirmation Request: " + JSON.stringify(leadAndAppntData));
			CommonService.ajaxCall(FETCH_LEAD_APPOINTMENT_URL, AJAX_TYPE, TYPE_JSON, leadAndAppntData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
					dfd.resolve(ajaxResp);
			});
			return dfd.promise;
    };
    self.ContinueSyncAppntDataIpadToServer=function(appntArr){
        var ContinueSyncAppntData = new Object();
        var dfd = $q.defer();
        var request = new Object();
        request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
		request.PWD = LoginService.lgnSrvObj.password;
		request.DVID = LoginService.lgnSrvObj.dvid;
        request.ACN="SCD";
        request.SLDDET=appntArr;
        ContinueSyncAppntData.REQ = request;
        console.log(JSON.stringify(ContinueSyncAppntData))
        console.log("ContinueSyncAppntDataIpadToServer data is :::"+JSON.stringify(appntArr));
        CommonService.ajaxCall(APPOINTMENT_DATA_URL, AJAX_TYPE, TYPE_JSON, ContinueSyncAppntData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
        					dfd.resolve(ajaxResp);
		});
		return dfd.promise;
    }
    self.fetchOpportunityData = function(){
    		var dfd = $q.defer();
			var request = new Object();
			var fetchOppoData=new Object();
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN="MOD";
			var localArr=[];
			CommonService.transaction(db,
				  function(tx){
					  CommonService.executeSql(tx,"select * from LP_MYOPPORTUNITY_DEVIATION where ISSYNCED=?", ["N"],
					  function(tx,resp){
					  		console.log("fetchOpportunityData data length is"+resp.rows.length);
							if(resp.rows.length>0){
                                 for(var j=0;j<resp.rows.length;j++){
                                 localArr.push({
										   OPPID: resp.rows.item(j).OPPORTUNITY_ID,
										   FHRID: resp.rows.item(j).FHR_ID,
										   RPDR:resp.rows.item(j).RECO_PRODUCT_DEVIATION_REASON,
										   DSTM:resp.rows.item(j).DEVIATION_SIGNED_TIMESTAMP,
									   });
							 	 }
                                 request.MODDET=localArr;
                                 fetchOppoData.REQ = request;
                                 console.log("fetchOppoData"+JSON.stringify( fetchOppoData));
                                 CommonService.ajaxCall(FETCH_OPPORTUNITY_DATA_URL, AJAX_TYPE, TYPE_JSON, fetchOppoData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
                                 		dfd.resolve(ajaxResp);
                                	});
                                 }
                                 else{
                                 console.log("NO RECORD FOR DEVIATION")

                                 }					  },
					  function(tx,err){
						  console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
						  dfd.resolve(null);
					  }
					  );
				  },
				  function(err){
					  	console.log("Error in fetchErrDescMasterData(): " + err.message);
					 	 dfd.resolve(null);
					  },null
			);
			debug("linkPolicyDataSync Request: " + JSON.stringify(fetchOppoData));

			return dfd.promise;
    	};
    self.LeadIpadToServerSync = function(leadArr){
			var leadASyncData = new Object();
			var dfd = $q.defer();
			var request = {};
			request.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
			request.PWD = LoginService.lgnSrvObj.password;
			request.DVID = LoginService.lgnSrvObj.dvid;
			request.ACN = "SLD";
			request.SLDDET=leadArr;
			leadASyncData.REQ = request;
			debug("sendLeadSync Request: " + JSON.stringify(leadASyncData));
			CommonService.ajaxCall(SYNC_LEAD_DATA_URL, AJAX_TYPE, TYPE_JSON, leadASyncData, AJAX_ASYNC, AJAX_TIMEOUT, function(ajaxResp){
					dfd.resolve(ajaxResp);
			});
			return dfd.promise;
    };


    self.checkIfLeadIdExist=function(leadData,pos){
		  var dfd = $q.defer();
		  debug("POS LEAD "+pos);
		  CommonService.transaction(db,
		  function(tx){
		  	  console.log("Select Query Check If Lead Exist " +"SELECT * from LP_MYLEAD where CRM_LEAD_ID = '"+leadData.RS.FALDET[pos].CLI+"' AND AGENT_CD = '"+LoginService.lgnSrvObj.userinfo.AGENT_CD+"'");
			  CommonService.executeSql(tx,"SELECT * from LP_MYLEAD where CRM_LEAD_ID = '"+leadData.RS.FALDET[pos].CLI+"' AND AGENT_CD = '"+LoginService.lgnSrvObj.userinfo.AGENT_CD+"'",[],
			  function(tx,res){
					  dfd.resolve(res);
			  },
			  function(tx,err){
				  console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
				  dfd.resolve(null);
			  }
			  );
		  },
		  function(err){
			  console.log("Error in fetchErrDescMasterData(): " + err.message);
			  dfd.resolve(null);
			  },null
		  );
		  return dfd.promise;
    };
	self.insertLeadData=function(leadData,pos){
	    var dfd = $q.defer();
		var leadAge='';
		var leadLand='';
		var leadAlter='';
		var leadMo='';
		var leadZip='';
		var ldt=null;
		console.log("Lead_data_Result is Position :::"+pos+" ::  "+JSON.stringify(leadData));
		var leaddte=leadData.RS.FALDET[pos].BDT;
		if(leadData.RS.FALDET[pos].ALT !=0){leadAlter=leadData.RS.FALDET[pos].ALT}else{leadAlter=null}
		if(leadData.RS.FALDET[pos].MOB !=0){leadMo=leadData.RS.FALDET[pos].MOB}else{leadMo=null}
		if(leadData.RS.FALDET[pos].LND !=0){leadLand=leadData.RS.FALDET[pos].LND}else{leadLand=null}
		if(leadData.RS.FALDET[pos].AGE !=0){leadAge=leadData.RS.FALDET[pos].AGE}else{leadAge=null}
		if(leadData.RS.FALDET[pos].CZC !=0){leadZip=leadData.RS.FALDET[pos].CZC}else{leadZip=null}
		var IPAD_LEAD_ID = CommonService.getRandomNumber();
		CommonService.transaction(db,
		function(tx){
			CommonService.executeSql(tx,"insert into LP_MYLEAD (CRM_LEAD_ID,IPAD_LEAD_ID,LEAD_SOURCE1_CD,LEAD_SUB_SOURCE1_CD,LEAD_SOURCE2_CD,LEAD_SOURCE3_CD,PROPOSED_SERVICE_TYPE_ID,DESIRE_SERVICE_TYPE_ID,REFERENCE_LEAD_ID,REFERRED_BY,RELATIONSHIP_CD,LEAD_NAME ,CURNT_ADDRESS1,CURNT_ADDRESS2,CURNT_ADDRESS3 ,CURNT_DISTRICT_LANDMARK,CURNT_CITY ,CURNT_STATE,CURNT_ZIP_CODE,MOBILE_NO,LANDLINE_NO, ALTERNATE_NO, PRIMARY_CONTACT_FLAG,EMAIL_ID,INCOME_GROUP_CD,OCCUPATION_CD, BIRTH_DATE, CURRENT_AGE, PRODUCT_PITCHED,GENDER_CD,MARITAL_STATUS_CD, NO_OF_CHILDREN_CD,SPECIAL_REMARK ,LEAD_STATUS_CD,LEAD_SUB_STATUS_CD,ASSIGNED_DTTM ,LEAD_CREATED_DTTM ,CAMPAIGN_NAME ,ANALYTIC_RECO_PRODUCT, AGENT_CD ,ISSYNCED) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [leadData.RS.FALDET[pos].CLI,IPAD_LEAD_ID,leadData.RS.FALDET[pos].LS1,leadData.RS.FALDET[pos].LSS1,leadData.RS.FALDET[pos].LS2,leadData.RS.FALDET[pos].LS3,leadData.RS.FALDET[pos].PLST,leadData.RS.FALDET[pos].DLST,leadData.RS.FALDET[pos].RFL,leadData.RS.FALDET[pos].RFB,leadData.RS.FALDET[pos].RFR,leadData.RS.FALDET[pos].LM,leadData.RS.FALDET[pos].CD1,leadData.RS.FALDET[pos].CD2,leadData.RS.FALDET[pos].CD3,leadData.RS.FALDET[pos].CDL,leadData.RS.FALDET[pos].CCT,leadData.RS.FALDET[pos].CST,leadZip,leadMo,leadLand,leadAlter,leadData.RS.FALDET[pos].PCF,leadData.RS.FALDET[pos].EML,leadData.RS.FALDET[pos].IGC,leadData.RS.FALDET[pos].OCC,leadData.RS.FALDET[pos].BDT,leadAge,leadData.RS.FALDET[pos].PRDP,leadData.RS.FALDET[pos].GDC,leadData.RS.FALDET[pos].MSC,leadData.RS.FALDET[pos].NOC,leadData.RS.FALDET[pos].SPR,leadData.RS.FALDET[pos].LSC,leadData.RS.FALDET[pos].LSSC,leadData.RS.FALDET[pos].AST,leadData.RS.FALDET[pos].LDT,leadData.RS.FALDET[pos].CMP,leadData.RS.FALDET[pos].ARP,LoginService.lgnSrvObj.userinfo.AGENT_CD,"Y"],
					function(tx,res){
					console.log("inserted into LP_MYLEAD");
			},
			function(tx,err){
				console.log("Error in insertintoLead.transaction(): " + err.message);
			}
			);
		},
		function(err){
			console.log("Error in fetchErrDescMasterData(): " + err.message);
			},null
		);

		CommonService.transaction(db,
		function(tx){
			CommonService.executeSql(tx,"select * from LP_MYCALENDAR where AGENT_CD=? and  IPAD_LEAD_ID=? and APPOINTMENT_DATE=? and APPOINTMENT_START_TIME=? and APPOINTMENT_END_TIME=?", [LoginService.lgnSrvObj.userinfo.AGENT_CD,IPAD_LEAD_ID,leadData.RS.APDET[pos].FLDT,leadData.RS.APDET[pos].FLSTTM,leadData.RS.APDET[pos].FLEDTM],
					function(tx,calRes){
					console.log("Select  LP_MYCALENDAR Count :"+calRes.rows.length);
					 dfd.resolve(calRes);
					 if(calRes.rows.length>0){
                    	if(quicker_Obj.Lead_appnt_length==(pos+1)){
								 self.selAllLeadDataFromDb();
						 }
					 }else{
							 self.insertCalendarData(leadData,pos,IPAD_LEAD_ID);
					 }
			},
			function(tx,err){
				console.log("Error in insertintoLead.transaction(): " + err.message);
				 dfd.resolve(null);
			}
			);
		},
		function(err){
			dfd.resolve(null);
			console.log("Error in fetchErrDescMasterData(): " + err.message);
			},null

		);
		return dfd.promise;
	}
 	self.updateLeadData=function(leadData,pos){
		var resAge='';
		var resLand='';
		var resAlter='';
		var resMo='';
		var resZip='';
		var ldt=null;
		if(leadData.RS.FALDET[pos].ALT !=0){resAlter=leadData.RS.FALDET[pos].ALT}else{resAlter=null}
		if(leadData.RS.FALDET[pos].MOB !=0){resMo=leadData.RS.FALDET[pos].MOB}else{resMo=null}
		if(leadData.RS.FALDET[pos].LND !=0){resLand=leadData.RS.FALDET[pos].LND}else{resLand=null}
		if(leadData.RS.FALDET[pos].AGE !=0){resAge=leadData.RS.FALDET[pos].AGE}else{resAge=null}
		if(leadData.RS.FALDET[pos].CZC !=0){resZip=leadData.RS.FALDET[pos].CZC}else{resZip=null}
		CommonService.transaction(db,
        		function(tx){
        			CommonService.executeSql(tx,"update LP_MYLEAD set LEAD_SOURCE1_CD=?,LEAD_SUB_SOURCE1_CD=?,LEAD_SOURCE2_CD=?,LEAD_SOURCE3_CD=?,PROPOSED_SERVICE_TYPE_ID=?,DESIRE_SERVICE_TYPE_ID=?,REFERENCE_LEAD_ID=?,REFERRED_BY=?,RELATIONSHIP_CD=?,LEAD_NAME=? ,CURNT_ADDRESS1=?,CURNT_ADDRESS2=?,CURNT_ADDRESS3=? ,CURNT_DISTRICT_LANDMARK=?,CURNT_CITY=? ,CURNT_STATE=?,CURNT_ZIP_CODE=?,MOBILE_NO=?,LANDLINE_NO=?, ALTERNATE_NO=?, PRIMARY_CONTACT_FLAG=?,EMAIL_ID=?,INCOME_GROUP_CD=?,OCCUPATION_CD=?, BIRTH_DATE=?, CURRENT_AGE=?, PRODUCT_PITCHED=?,GENDER_CD=?,MARITAL_STATUS_CD=?, NO_OF_CHILDREN_CD=?,SPECIAL_REMARK =?,LEAD_STATUS_CD=?,LEAD_SUB_STATUS_CD=?,ASSIGNED_DTTM=? ,LEAD_CREATED_DTTM =?,CAMPAIGN_NAME=? ,ANALYTIC_RECO_PRODUCT=? where AGENT_CD=? and CRM_LEAD_ID=?", [leadData.RS.FALDET[pos].LS1,leadData.RS.FALDET[pos].LSS1,leadData.RS.FALDET[pos].LS2,leadData.RS.FALDET[pos].LS3,leadData.RS.FALDET[pos].PLST,leadData.RS.FALDET[pos].DLST,leadData.RS.FALDET[pos].RFL,leadData.RS.FALDET[pos].RFB,leadData.RS.FALDET[pos].RFR,leadData.RS.FALDET[pos].LM,leadData.RS.FALDET[pos].CD1,leadData.RS.FALDET[pos].CD2,leadData.RS.FALDET[pos].CD3,leadData.RS.FALDET[pos].CDL,leadData.RS.FALDET[pos].CCT,leadData.RS.FALDET[pos].CST,resZip,resMo,resLand,resAlter,leadData.RS.FALDET[pos].PCF,leadData.RS.FALDET[pos].EML,leadData.RS.FALDET[pos].IGC,leadData.RS.FALDET[pos].OCC,leadData.RS.FALDET[pos].BDT,resAge,leadData.RS.FALDET[pos].PRDP,leadData.RS.FALDET[pos].GDC,leadData.RS.FALDET[pos].MSC,leadData.RS.FALDET[pos].NOC,leadData.RS.FALDET[pos].SPR,leadData.RS.FALDET[pos].LSC,leadData.RS.FALDET[pos].LSSC,leadData.RS.FALDET[pos].AST,leadData.RS.FALDET[pos].LDT,leadData.RS.FALDET[pos].CMP,leadData.RS.FALDET[pos].ARP,LoginService.lgnSrvObj.userinfo.AGENT_CD,leadData.RS.FALDET[pos].CLI],
        			function(tx, res) {
					console.log("updateLeadData sucess");
				},
				 function(tx,err){
								console.log("Error in UpdateLead.transaction(): " + err.message);
								 dfd.resolve(null);
				}
			);
			(function(leadData,pos){
					CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"select * from LP_MYCALENDAR where AGENT_CD=? and  IPAD_LEAD_ID=? and APPOINTMENT_DATE=? and APPOINTMENT_START_TIME=? and APPOINTMENT_END_TIME=?", [LoginService.lgnSrvObj.userinfo.AGENT_CD,leadData.RS.FALDET[pos].IPLID,leadData.RS.APDET[pos].FLDT,leadData.RS.APDET[pos].FLSTTM,leadData.RS.APDET[pos].FLEDTM],
								function(tx,calRes){
								 console.log("Select  LP_MYCALENDAR Count :"+calRes.rows.length);
								 if(calRes.rows.length>0){
									if(quicker_Obj.Lead_appnt_length==(pos+1)){
											 self.selAllLeadDataFromDb();
									 }
								 }else{
										 self.insertCalendarData(leadData,pos,leadData.RS.FALDET[pos].IPLID);
								 }
						},
						function(tx,err){
							console.log("Error in insertintoLead.transaction(): " + err.message);
						}
			);
			})}(leadData,pos))
			},

		   function(error){
		 	console.log("updateLeadData transaction err"+ JSON.stringify(error));
		   })
	}



	self.insertCalendarData=function(leadData,pos,ipadLeadID){
		if((leadData.RS.APDET[pos].FLTCD !=0 && leadData.RS.APDET[pos].FLTCD.length !=0) && (leadData.RS.APDET[pos].FLDT !=null && leadData.RS.APDET[pos].FLDT.length !=0)){
		(function(leadData,pos,ipadLeadID){
		 var flsttm=null;
		 var fledtm=null;
		 if(leadData.RS.APDET[pos].FLSTTM !=null &leadData.RS.APDET[pos].FLSTTM !='null' && leadData.RS.APDET[pos].FLSTTM !=''){
		 	flsttm=leadData.RS.APDET[pos].FLSTTM;
		 }
		 if(leadData.RS.APDET[pos].FLEDTM !=null && leadData.RS.APDET[pos].FLEDTM !='null' && leadData.RS.APDET[pos].FLEDTM !=''){
		 	fledtm=leadData.RS.APDET[pos].FLEDTM;
		 }
		 CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"insert into LP_MYCALENDAR (AGENT_CD,IPAD_LEAD_ID,LEAD_NAME,CALL_TYPE_ID,APPOINT_CREATE_DTTM,APPOINTMENT_DATE,APPOINTMENT_START_TIME,APPOINTMENT_END_TIME,ISSYNCED) values (?,?,?,?,?,?,?,?,?)", [LoginService.lgnSrvObj.userinfo.AGENT_CD,ipadLeadID,leadData.RS.FALDET[pos].LM,leadData.RS.APDET[pos].FLTCD,CommonService.getCurrDate(),leadData.RS.APDET[pos].FLDT,flsttm,fledtm,"Y"],
						function(tx,res){
						console.log("inserted into LP_MYCALENDAR");
						if(quicker_Obj.Lead_appnt_length==(pos+1)){
							 self.selAllLeadDataFromDb();
						}
				},
				function(tx,err){
					console.log("Error in insertintoLead.transaction(): " + err.message);
				}
				);
			},
			function(err){
				console.log("Error in fetchErrDescMasterData(): " + err.message);
				},null
		);
	 }(leadData,pos,ipadLeadID))

	}else{
		if(quicker_Obj.Lead_appnt_length==(pos+1)){
			self.selAllLeadDataFromDb();

		}
	}
}
	self.selAllLeadDataFromDb=function(){
	 	var dfd = $q.defer();
		var localArr=[];
		for(var j=0;j<quicker_Obj.GBL_LeadArr.RS.FALDET.length;j++){
        	(function(j){
					CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"select CRM_LEAD_ID,IPAD_LEAD_ID from LP_MYLEAD where AGENT_CD=? and CRM_LEAD_ID=?", [LoginService.lgnSrvObj.userinfo.AGENT_CD,quicker_Obj.GBL_LeadArr.RS.FALDET[j].CLI],
								function(tx,leadRes){
								 console.log("Select  LP_MYLEAD  :"+leadRes.rows.length);
								 dfd.resolve(leadRes);
								 if(leadRes.rows.length>0){
									 localArr.push({
									   CLI:leadRes.rows.item(0).CRM_LEAD_ID,
									   ILI:leadRes.rows.item(0).IPAD_LEAD_ID
									  });
								 }
								 if(quicker_Obj.GBL_LeadArr.RS.FALDET.length==j+1){
									 console.log("sendtoserver arr is ::::"+JSON.stringify( localArr))
									 self.leadAppointSyncConfirmation(localArr).then(
										 function(leadAppointAckRes){
												 debug("fetchLeadAkownldment Response: " + JSON.stringify(leadAppointAckRes));
												 self.linkPolicyData().then(
													function(linkpolicyDataRes){
														if(linkpolicyDataRes.RS.RESP=="S"){
															quicker_Obj.GBL_PolicyDataArr=linkpolicyDataRes;
															quicker_Obj.Link_Policy_Data_length=linkpolicyDataRes.RS.FLPDET.length;
															debug("Length Response: " + linkpolicyDataRes.RS.FLPDET.length);
															   if(linkpolicyDataRes.RS.FLPDET.length>0){
																   policyTimeSync=CommonService.getCurrDate();
																   for(var i=0;i<linkpolicyDataRes.RS.FLPDET.length;i++){
																		self.checkLinkPolicyData(linkpolicyDataRes,i);
																   }
															   }else{
															   self.SyncLeadIpadToServer();
															   }
															   }else{
													   self.SyncLeadIpadToServer();
													   }
														debug("fetchLinkPolicy Response: " + JSON.stringify(linkpolicyDataRes));
												 });
										 }
									);
								 }
						},
						function(tx,err){
							console.log("Error in insertintoLead.transaction(): " + err.message);
							 dfd.resolve(null);
						}
						);
					},
					function(err){
						dfd.resolve(null);
						console.log("Error in fetchErrDescMasterData(): " + err.message);
						},null

					);
					return dfd.promise;
        	}(j))
        }
	}
	self.checkLinkPolicyData=function(linkDataJson,dataAt){
		debug("Position: " + dataAt);
		  var dfd = $q.defer();
		  CommonService.transaction(db,
		  function(tx){
			  console.log("checkLinkPolicyData POLICY_NUMBER"+linkDataJson.RS.FLPDET[dataAt].POL);
			  CommonService.executeSql(tx,"select * from LP_LINKED_POLICY where POLICY_NUMBER=?", [linkDataJson.RS.FLPDET[dataAt].POL],
			  function(tx,res){
					  dfd.resolve(res);
						 if(res.rows.length>0){
							 console.log('updateLinkPolicyData')
							 self.updateLinkPolicyData(linkDataJson,dataAt);
						 }
						 else{
							 console.log('insertLinkPolicyData');
							 self.insertLinkPolicyData(linkDataJson,dataAt);
						 }
			  },
			  function(tx,err){
				  console.log("Error in fetchErrDescSelectData().transaction(): " + err.message);
				  dfd.resolve(null);
			  }
			  );
		  },
		  function(err){
			  console.log("Error in fetchErrDescMasterData(): " + err.message);
			  dfd.resolve(null);
			  },null
		  );
		  return dfd.promise;
	}

	self.updateLinkPolicyData=function(linkDataJson,dataAt){
		CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"update LP_LINKED_POLICY set CRM_LEAD_ID=?,PRIMARY_POLICY_FLAG=?,PLAN_NAME=?,ISSUE_DATE=?,SUM_ASSURED=?,POLICY_TERM=?,ANNUAL_PREMIUM=?,MODAL_PREMIUM=?,MODE =?,ISSYNCED=?  where POLICY_NUMBER=?", [linkDataJson.RS.FLPDET[dataAt].CLI,linkDataJson.RS.FLPDET[dataAt].PPF,linkDataJson.RS.FLPDET[dataAt].PLN,linkDataJson.RS.FLPDET[dataAt].ISD,linkDataJson.RS.FLPDET[dataAt].SAS,linkDataJson.RS.FLPDET[dataAt].POT,linkDataJson.RS.FLPDET[dataAt].ALP,linkDataJson.RS.FLPDET[dataAt].MPR,linkDataJson.RS.FLPDET[dataAt].MOD,"Y",linkDataJson.RS.FLPDET[dataAt].POL],
							function(tx,res){
							console.log("Updated into LP_LINKED_POLICY");
							if(quicker_Obj.Link_Policy_Data_length==(dataAt+1)){
								 console.log("complete link policy data")
								 self.selAllLinkPolicyDataFromDb();
							 }
					},
					function(tx,err){
						console.log("Error in insertintoLead.transaction(): " + err.message);
					}
					);
				},
				function(err){
					console.log("Error in fetchErrDescMasterData(): " + err.message);
				},null
		);
    }
    self.insertLinkPolicyData=function(linkDataJson,dataAt){
		CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"insert into LP_LINKED_POLICY (CRM_LEAD_ID,POLICY_NUMBER,PRIMARY_POLICY_FLAG,PLAN_NAME,ISSUE_DATE,SUM_ASSURED,POLICY_TERM,ANNUAL_PREMIUM,MODAL_PREMIUM,MODE,ISSYNCED) values(?,?,?,?,?,?,?,?,?,?,?)", [linkDataJson.RS.FLPDET[dataAt].CLI,linkDataJson.RS.FLPDET[dataAt].POL,linkDataJson.RS.FLPDET[dataAt].PPF,linkDataJson.RS.FLPDET[dataAt].PLN,linkDataJson.RS.FLPDET[dataAt].ISD,linkDataJson.RS.FLPDET[dataAt].SAS,linkDataJson.RS.FLPDET[dataAt].POT,linkDataJson.RS.FLPDET[dataAt].ALP,linkDataJson.RS.FLPDET[dataAt].MPR,linkDataJson.RS.FLPDET[dataAt].MOD,"Y"],
							function(tx,res){
							console.log("inserted into LP_LINKED_POLICY");
							if(quicker_Obj.Link_Policy_Data_length==(dataAt+1)){
								 console.log("complete link policy data")
								 self.selAllLinkPolicyDataFromDb();
							 }
					},
					function(tx,err){
						console.log("Error in insertintoLead.transaction(): " + err.message);
					}
					);
				},
				function(err){
					console.log("Error in fetchErrDescMasterData(): " + err.message);
				},null
		);
    }
    self.selAllLinkPolicyDataFromDb=function(){
        var localArr=[];
    	for(var k=0;k<quicker_Obj.GBL_PolicyDataArr.RS.FLPDET.length;k++){
        	localArr.push({
                          CLI:quicker_Obj.GBL_PolicyDataArr.RS.FLPDET[k].CLI.toString(),
                          POL:quicker_Obj.GBL_PolicyDataArr.RS.FLPDET[k].POL
                          });
            if(k==(quicker_Obj.GBL_PolicyDataArr.RS.FLPDET.length-1)){
                 self.linkPolicyDataConfirmation(localArr).then(
					function(linkpolicyConfrmRes)
					{
						debug("linkpolicyConfrm Response: " + JSON.stringify(linkpolicyConfrmRes));
						self.SyncLeadIpadToServer();
					}
                 );
            }
        }
    }
	self.leadSync_N_Appoint=function(){
		var loginType = CommonService.checkConnection();
		CommonService.showLoading("Loading...");
		if(loginType=='online'){
			self.leadAppointSync().then(
				function(leadAppointRes){
					 debug("fetchLeadAppointment Response: " + JSON.stringify(leadAppointRes));
					 if(leadAppointRes.RS.RESP=="S"){
						   leadTimeSync=CommonService.getCurrDate();
					 	   quicker_Obj.GBL_LeadArr=leadAppointRes;
						   quicker_Obj.Lead_appnt_length=(leadAppointRes.RS.FALDET.length);
						   console.log("Lead_appnt_length is :::"+quicker_Obj.Lead_appnt_length);
						   debug("leadRespTimeStamp : : "+CommonService.getCurrDate());
						   if(leadAppointRes.RS.FALDET.length>0){
							   for(var i=0;i<leadAppointRes.RS.FALDET.length;i++){
							   		(function(i){
										self.checkIfLeadIdExist(leadAppointRes,i).then(
										function(leadData){
											console.log("Lead_data_length is :::"+leadData.rows.length)
											if(leadData.rows.length>0){
	                                            	 self.updateLeadData(leadAppointRes,i);
											}else{
												 console.log("Lead_data_Result is :::"+JSON.stringify(leadAppointRes));
												 self.insertLeadData(leadAppointRes,i);
											 }
										});
							   		}(i))

							   }
						   }else{
						    	self.linkPolicyData().then(
									function(linkpolicyDataRes){
										if(linkpolicyDataRes.RS.RESP=="S"){
											quicker_Obj.GBL_PolicyDataArr=linkpolicyDataRes;
											quicker_Obj.Link_Policy_Data_length=linkpolicyDataRes.RS.FLPDET.length;
											debug("Length Response: " + linkpolicyDataRes.RS.FLPDET.length);
											   if(linkpolicyDataRes.RS.FLPDET.length>0){
												   policyTimeSync=CommonService.getCurrDate();
												   for(var i=0;i<linkpolicyDataRes.RS.FLPDET.length;i++){
														self.checkLinkPolicyData(linkpolicyDataRes,i);
												   }
											   }else{
															   self.SyncLeadIpadToServer();
											   }
											   }else{
													   self.SyncLeadIpadToServer();
									   }
										debug("fetchLinkPolicy Response: " + JSON.stringify(linkpolicyDataRes));
								 });
						   }
					 }
					 else
					 {
							self.linkPolicyData().then(
								function(linkpolicyDataRes){
									if(linkpolicyDataRes.RS.RESP=="S"){
										quicker_Obj.GBL_PolicyDataArr=linkpolicyDataRes;
										quicker_Obj.Link_Policy_Data_length=linkpolicyDataRes.RS.FLPDET.length;
										debug("Length Response: " + linkpolicyDataRes.RS.FLPDET.length);
										   if(linkpolicyDataRes.RS.FLPDET.length>0){
											   policyTimeSync=CommonService.getCurrDate();
											   for(var i=0;i<linkpolicyDataRes.RS.FLPDET.length;i++){
													self.checkLinkPolicyData(linkpolicyDataRes,i);
											   }
										   }else{
															   self.SyncLeadIpadToServer();
										   }
										   }else{
													   self.SyncLeadIpadToServer();
								   }
									debug("fetchLinkPolicy Response: " + JSON.stringify(linkpolicyDataRes));
							 });
					 }

			});
		}
		else{
				navigator.notification.alert('Please connect to the internet to sync lead.',function(){
					CommonService.hideLoading();
				},"Lead Sync","OK");
		}
	};


	self.SyncLeadIpadToServer=function(leadID){
        var query='';
        if(BUSINESS_TYPE=="LifePlaner" || BUSINESS_TYPE=="GoodSolution"){
            if(leadID==undefined)
           		query="select * from LP_MYLEAD where AGENT_CD=? and ISSYNCED=? and CRM_LEAD_ID IS NOT NULL";
            else
            	query="select * from LP_MYLEAD where IPAD_LEAD_ID='"+leadID+"' AND AGENT_CD=? and (ISSYNCED=? or ISSYNCED is null) and CRM_LEAD_ID IS NOT NULL";
        }else{
            if(leadID==undefined)
            	query="select * from LP_MYLEAD where AGENT_CD=? and (ISSYNCED=? or ISSYNCED is null)";
            else
              	query="select * from LP_MYLEAD where IPAD_LEAD_ID='"+leadID+"' AND AGENT_CD=? and (ISSYNCED=? or ISSYNCED is null)";
        }
        var localArr=[];
        console.log("query:-"+query);
        CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,query, [LoginService.lgnSrvObj.userinfo.AGENT_CD,"N"],
						function(tx,calRes){
						 console.log("Select  SyncLeadIpadToServer Count :"+calRes.rows.length);
						 if(calRes.rows.length>0){
							for(var j=0;j<calRes.rows.length;j++){
								(function(j){
									 localArr.push({
												   LID: calRes.rows.item(j).IPAD_LEAD_ID,
												   LS1: calRes.rows.item(j).LEAD_SOURCE1_CD,
												   LSS1: calRes.rows.item(j).LEAD_SUB_SOURCE1_CD,
												   LS2: calRes.rows.item(j).LEAD_SOURCE2_CD,
												   LS3: calRes.rows.item(j).LEAD_SOURCE3_CD,
												   PLST: calRes.rows.item(j).PROPOSED_SERVICE_TYPE_ID,
												   DLST: calRes.rows.item(j).DESIRE_SERVICE_TYPE_ID,
												   RFL: calRes.rows.item(j).REFERENCE_LEAD_ID,
												   RFB: calRes.rows.item(j).REFERRED_BY,
												   RFR: calRes.rows.item(j).RELATIONSHIP_CD,
												   LM: calRes.rows.item(j).LEAD_NAME,
												   CD1: calRes.rows.item(j).CURNT_ADDRESS1,
												   CD2: calRes.rows.item(j).CURNT_ADDRESS2,
												   CD3: calRes.rows.item(j).CURNT_ADDRESS3,
												   CDL: calRes.rows.item(j).CURNT_DISTRICT_LANDMARK,
												   CCT: calRes.rows.item(j).CURNT_CITY,
												   CST: calRes.rows.item(j).CURNT_STATE,
												   CZC: calRes.rows.item(j).CURNT_ZIP_CODE,
												   MOB: calRes.rows.item(j).MOBILE_NO,
												   LND: calRes.rows.item(j).LANDLINE_NO,
												   ALT: calRes.rows.item(j).ALTERNATE_NO,
												   PCF: calRes.rows.item(j).PRIMARY_CONTACT_FLAG,
												   EML: calRes.rows.item(j).EMAIL_ID,
												   IGC: calRes.rows.item(j).INCOME_GROUP_CD,
												   OCC: calRes.rows.item(j).OCCUPATION_CD,
												   BDT: calRes.rows.item(j).BIRTH_DATE,
												   AGE: calRes.rows.item(j).CURRENT_AGE,
												   PRDP:calRes.rows.item(j).PRODUCT_PITCHED,
												   GDC: calRes.rows.item(j).GENDER_CD,
												   MSC: calRes.rows.item(j).MARITAL_STATUS_CD,
												   NOC: calRes.rows.item(j).NO_OF_CHILDREN_CD,
												   SPR: calRes.rows.item(j).SPECIAL_REMARK,
												   LSC: calRes.rows.item(j).LEAD_STATUS_CD,
												   LSSC: calRes.rows.item(j).LEAD_SUB_STATUS_CD,
												   AST: calRes.rows.item(j).ASSIGNED_DTTM,
												   LDT: calRes.rows.item(j).LEAD_CREATED_DTTM,
												   CMP: calRes.rows.item(j).CAMPAIGN_NAME,
												   ARP: calRes.rows.item(j).ANALYTIC_RECO_PRODUCT,
												   IMD: calRes.rows.item(j).IPAD_MODIFIED_DTTM
												   });
								}(j))
							}
								self.LeadIpadToServerSync(localArr).then(
								function(leadSyncRes){
								 	console.log("  SyncLeadIpadToServer Response :"+JSON.stringify(leadSyncRes));
								 	if((!!leadSyncRes) && (!!leadSyncRes.RS) && (leadSyncRes.RS.hasOwnProperty('RESP') != true)){
                                           if(leadSyncRes.RS.SLDDET.length>0){
												self.SyncLeadIpadToServerUpdateDB(leadSyncRes);
										   }else{
												console.log("with out update lead data");
												self.SyncAppntDataIpadToServer();
										   }
									   }else{
										   self.leadsErrHandle(((!!leadSyncRes && !!leadSyncRes.RS)?leadSyncRes.RS.EC:(99)))
									   }
								 }
								);
						 }
						 else{
							   self.SyncAppntDataIpadToServer();
						}
				},
				function(tx,err){
					console.log("Error in insertintoLead.transaction(): " + err.message);
				}
		);
		})
    }
    self.leadsErrHandle=function (errCode){
        CommonService.hideLoading();
        switch(parseInt(errCode)){
            case 1:
                console.log("Either DVID EXISTS in the table (in Server)\nOR\nIMEI EXISTS for other device.",null,"Login Failed","OK");
                break;
            case 2:
                console.log("Agent is LOCKED or INACTIVE!!",null,"Login Failed","OK");
                break;
            case 3:
                console.log("Agent information not available!!",null,"Login Failed","OK");
                break;
            case 4: console.log("INVALID Credentials!!",null,"Login Failed","OK");

                break;
            case 5: console.log("this device ID is associated with other username !!",null,"Login Failed","OK");

                break;
            case 18: console.log("No Data Found!",null,"Sync failed","OK");

                break;
            case 19:console.log("No Data Found!",null,"Sync failed","OK");

                break;
            case 8:

                break;
            default:console.log("Lead Sync",null,"Could not process the request. Please try again later.","OK");
        }
    }
	self.SyncLeadIpadToServerUpdateDB=function(leadArr){
		for(var z=0;z<leadArr.RS.SLDDET.length;z++){
		(function(z){
		 if(leadArr.RS.SLDDET[z].RESP=="S"){
			CommonService.transaction(db,
    				function(tx){
    					CommonService.executeSql(tx,"update LP_MYLEAD set ISSYNCED=? where AGENT_CD=? and IPAD_LEAD_ID=?", ["Y",LoginService.lgnSrvObj.userinfo.AGENT_CD,leadArr.RS.SLDDET[z].LID],
    							function(tx,res){
    							console.log("SyncLeadIpadToServerUpdateDB");
    							if(quicker_Obj.Link_Policy_Data_length==(dataAt+1)){
    								 console.log("complete link policy data")
    								 self.selAllLinkPolicyDataFromDb();
    							 }
    					},
    					function(tx,err){
    						console.log("Error in insertintoLead.transaction(): " + err.message);
    					}
    					);
    				},
    				function(err){
    					console.log("Error in fetchErrDescMasterData(): " + err.message);
    				},null
    		);
    		if(leadArr.RS.SLDDET.length==z+1){
				self.SyncAppntDataIpadToServer();
			}
		   }
		 }(z));
    	}
	}

	self.SyncAppntDataIpadToServer=function(){
		var localArr=[];
		CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"select * from LP_MYCALENDAR where AGENT_CD=? and ISSYNCED=?", [LoginService.lgnSrvObj.userinfo.AGENT_CD,"N"],
							function(tx,resp){
							console.log("SyncAppntDataIpadToServer data length is"+resp.rows.length);
							if(resp.rows.length>0){
                            	 for(var j=0;j<resp.rows.length;j++){
									 localArr.push({
										   CTI: resp.rows.item(j).CALL_TYPE_ID,
										   APD: resp.rows.item(j).APPOINTMENT_DATE,
										   AST:resp.rows.item(j).APPOINTMENT_START_TIME,
										   AET:resp.rows.item(j).APPOINTMENT_END_TIME,
										   ILI: resp.rows.item(j).IPAD_LEAD_ID,
										   LM: resp.rows.item(j).LEAD_NAME,
										   ADS: resp.rows.item(j).APPOINTMENT_DESCRIPTION,
										   DPI: resp.rows.item(j).DISPOSITON_ID,
										   CLR: resp.rows.item(j).CALL_REMARKS,
										   ACD: resp.rows.item(j).APPOINT_CREATE_DTTM,
										   AUD: resp.rows.item(j).APPOINT_UPDATE_DTTM
										   });
								 }
								 ContinueSyncAppntDataIpadToServer(localArr).then(
								 	function(succArr)
								 	{
								 		console.log("app data to server  LEADSERVLET 6"+JSON.stringify(succArr))
                                        if(succArr.RS.hasOwnProperty('RESP') != true){
										   if(succArr.RS.SLDDET.length>0){
										 	  self.SyncSyncAppntIpadToServerUpdateDB(succArr);
										   }else{
											   if(appSubmit!=undefined && appSubmit.toLowerCase()==="yes".toLowerCase())
											   {
											   }
											   else
											   {
													CommonService.hideLoading();
										   	   }
										   }
										  }else{
										   		self.leadsErrHandle(succArr.RS.EC);
										  }
								 	}
								 );
							}
							else{
								   console.log("DOne with record null");
								   if(appSubmit!=undefined && appSubmit.toLowerCase()==="yes".toLowerCase())
								   {
								   }
								   else
								   {
								  		CommonService.hideLoading();
								   }
								}
					},
					function(tx,err){
						console.log("Error in insertintoLead.transaction(): " + err.message);
					}
					);
				},
				function(err){
					console.log("Error in fetchErrDescMasterData(): " + err.message);
				},null
		);
	}

	self.opportunityUpdateFlag=function (opprArr){
        console.log("opportunityUpdateFlag"+JSON.stringify( opprArr))
        for(var z=0;z<opprArr.RS.MODDET.length;z++){
			if(opprArr.RS.MODDET[z].RESP=="S"){
	   			(function(z){
	   					CommonService.transaction(db,
						function(tx){
							CommonService.executeSql(tx,"update LP_MYOPPORTUNITY_DEVIATION set ISSYNCED=? where OPPORTUNITY_ID=?", ["Y",opprArr.RS.MODDET[z].OPPID],
									function(tx,res){
									console.log(JSON.stringify(resp))
                                    console.log("opportunityUpdateFlagUpdateDB+++++++++++");
							},
							function(tx,err){
								console.log("Error in insertintoLead.transaction(): " + err.message);
							}
						);
	  				  })}(z));
	   			}
	   if(opprArr.RS.MODDET.length==z+1){
	   }
	}
	self.SyncSyncAppntIpadToServerUpdateDB=function(appntArr){
		   for(var z=0;z<appntArr.RS.SLDDET.length;z++){
		   	if(appntArr.RS.SLDDET[z].RESP=="S"){
		   		(function(z){
					CommonService.transaction(db,
					function(tx){
						CommonService.executeSql(tx,"update LP_MYCALENDAR set ISSYNCED=? where AGENT_CD=? and IPAD_LEAD_ID=? and APPOINT_CREATE_DTTM=?", ["Y",LoginService.lgnSrvObj.userinfo.AGENT_CD,appntArr.RS.SLDDET[z].ILI,appntArr.RS.SLDDET[z].ACD.toString()],
								function(tx,resp){
								console.log(JSON.stringify(resp))
							    console.log("SyncLeadIpadToServerUpdateDB+++++++++++");
						},
						function(tx,err){
							console.log("Error in insertintoLead.transaction(): " + err.message);
						}
					);
				  })}(z));
			if(appntArr.RS.SLDDET.length==z+1){
			   console.log("DOne");
			   if(appSubmit!=undefined && appSubmit.toLowerCase()==="yes".toLowerCase())
			   {
			   }
			   else
			   {
					CommonService.hideLoading();
			   }
		   	}
		 }
		}
	}
  }

}]);
