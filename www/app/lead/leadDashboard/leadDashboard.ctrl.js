leadDashboardModule.controller('LeadDashboardCtrl',['$state', 'CommonService', 'LoginService', 'LeadDashboardService','Params', 'LeadDetails', 'OpportunityDetails', 'AppDetails', 'ViewReportService', '$stateParams', 'SisFormService','factFinderService', 'TermPlanDetails','PSCService', function($state, CommonService, LoginService, LeadDashboardService, Params, LeadDetails, OpportunityDetails, AppDetails, ViewReportService, $stateParams, SisFormService, factFinderService, TermPlanDetails,PSCService){
	"use strict";

	CommonService.hideLoading();

    /* Header Hight Calculator */
    setTimeout(function(){

        var leadDashboardCalcHeight = $(".lead-details .fixed-bar").height();
        console.log("Testing : " + leadDashboardCalcHeight);
        $(".lead-details .custom-position").css({top:leadDashboardCalcHeight + "px"});
    });
    /* End Header Hight Calculator */


	debug("in LeadDashboardCtrl");
	//debug(JSON.stringify(Params));
	var lc = this;

	this.isFhrSel = (!!OpportunityDetails && OpportunityDetails.RECO_PRODUCT_DEVIATION_FLAG!='S');

	if(BUSINESS_TYPE == "IndusSolution")
		this.oppStarter = "FF";
	else// if(BUSINESS_TYPE == "GoodSolution" || BUSINESS_TYPE == "iWealth")
		this.oppStarter = "FHR_SIS";
	/*else if(BUSINESS_TYPE == "LifePlaner")
		this.oppStarter = "FHR";*/

	if(!!Params){
		this.leadId = Params.LEAD_ID || null;
		this.fhrId = Params.FHR_ID || null;
		this.appId = Params.APP_ID || null;
		this.pglId = Params.PGL_ID || null;
		this.sisId = Params.SIS_ID || null;
		this.oppId = Params.OPP_ID || null;
		this.policyNo = Params.POLICY_NO || null;
		if(!!Params.APP_ID)
		{
			PSCService.getPscExistingData(Params.APP_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(function(pscData){
				if(!!pscData && !!pscData.DIGITAL_PSC_FLAG){
					lc.ISDIGITAL_PSC_DONE = pscData.ISDIGITAL_PSC_DONE;
					lc.DIGITAL_PSC_FLAG = pscData.DIGITAL_PSC_FLAG;
				}
				else
					debug("NO PSC DATA");
			});
		}
	}

	if(!!LeadDetails){
		this.leadName = LeadDetails.LEAD_NAME;
		this.leadMobileNo = LeadDetails.MOBILE_NO;
		this.leadModifiedTimestamp = LeadDetails.LEAD_CREATED_DTTM || null;
	}

	this.agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
	this.empFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
	debug("OpportunityDetails values are : "+JSON.stringify(OpportunityDetails));
	if(!!OpportunityDetails){
		this.fhrSignedTimestamp = OpportunityDetails.FHR_SIGNED_TIMESTAMP || null;
		this.sisSignedTimestamp = OpportunityDetails.SIS_SIGNED_TIMESTAMP || null;
		this.appSignedTimestamp = OpportunityDetails.APP_SIGNED_TIMESTAMP || null;
		this.oppId = OpportunityDetails.OPPORTUNITY_ID || null;//Drastty
		this.policyNo = OpportunityDetails.POLICY_NO || null;
		this.isCustDet = OpportunityDetails.IS_CUST_DET || null;//Drastty
		this.isDocDet = OpportunityDetails.IS_DOC_DET || null;
		this.isAgntDet = OpportunityDetails.IS_AGNT_DET || null;
		this.COMBO_ID = OpportunityDetails.COMBO_ID || null;
		this.refAppID = OpportunityDetails.REF_APPLICATION_ID || null;
		debug("Inside oppDet::"+JSON.stringify(OpportunityDetails));
	}

	if(!!TermPlanDetails){
		debug("Inside TermPlanDetails::"+JSON.stringify(TermPlanDetails));
		this.isTermAttached = true;
		this.termSisId = TermPlanDetails.SIS_ID;
		this.termFhrId = TermPlanDetails.FHR_ID;
		this.sisTermSignedTimestamp = TermPlanDetails.SIS_SIGNED_TIMESTAMP || null;
		this.termOppId = TermPlanDetails.OPPORTUNITY_ID || null;
	}
	else{
		this.isTermAttached = false;
		this.sisTermSignedTimestamp = null;
		this.termSisId = null;
		this.termFhrId = null;
		this.termOppId = null;
	}

	this.lastUpdatedDate = LeadDashboardService.getLastUpdatedDate(((!!OpportunityDetails)?(OpportunityDetails.LAST_UPDATED_DATE):(null)) || this.appSignedTimestamp || this.sisSignedTimestamp || this.fhrSignedTimestamp || this.leadModifiedTimestamp);

	debug("Name: " + this.leadName);
	debug("Agent Code: " + this.agentCd);
	debug("FHR ID: " + this.fhrId);
	debug("SIS ID: " + this.sisId);
	debug("OPP ID: " + this.oppId);
	debug("APP ID: " + this.appId);
	debug("PGL ID: " + this.pglId);
	debug("sisSignedTimestamp :"+this.sisSignedTimestamp);

	this.initNewSISDisabled = (
		((this.oppStarter=="FF" || this.oppStarter=="FHR" || this.oppStarter=="FHR_SIS")&&((!this.fhrId)||((!!this.fhrId)&&(!this.fhrSignedTimestamp)))) ||
		((this.oppStarter=="SIS")&&(!!this.sisId))
	);

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.editLead = function(){
		$state.go("addLead", {LEAD_ID: this.leadId});
	};

	this.goToFHR = function(){
		CommonService.showLoading();
		$state.go(FHR_PERSONAL_DETAILS, {LEADID: lc.leadId, FHRID: null, AGENTCD: lc.agentCd});
		// navigator.notification.alert("FHR is not yet available.",function(){CommonService.hideLoading();},"Lead Dashboard","OK");
	};

	this.goToFactFinder = function() {
		CommonService.showLoading();
		LeadDashboardService.checkIfLeadIsComplete().then(
			function(leadRes){
				if(!!leadRes){
					lc.fhrId = CommonService.getRandomNumber();
					lc.oppId = CommonService.getRandomNumber();
					LeadDashboardService.updtOppModDate(lc.oppId);
					factFinderService.clearFHRData();
					factFinderService.ffData = null;
					$state.go(FACT_FINDER_STATE, {LEADID : lc.leadId, FHRID : lc.fhrId, AGENTCD : lc.agentCd, OPP_ID: lc.oppId});
				}
				else{
					navigator.notification.alert("Lead data is incomplete please complete it from leads page in side menu.",function(){CommonService.hideLoading();},"Lead Dashboard","OK");
				}
			}
		);
	};

	this.continueFHR = function() {
		//Not yet implemented...
		$state.go("fhrPersonalDetails", {LEADID: lc.leadId, FHRID: lc.fhrId, AGENTCD: lc.agentCd});
	};

	this.continueFactFinder = function() {
		LeadDashboardService.updtOppModDate(this.oppId);
		$state.go(FACT_FINDER_STATE, {"LEADID": this.leadId, "FHRID": this.fhrId, "AGENTCD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "OPP_ID": this.oppId});
	};

	this.initiateNewSIS = function(noFHR){
		CommonService.showLoading();
		LeadDashboardService.checkIfLeadIsComplete().then(
			function(leadRes){
				if(!!leadRes){
					debug("in initiateNewSIS");
					if((lc.oppStarter=="SIS") || (lc.oppStarter=="FHR_SIS") || (!!lc.fhrId)) {
						var oppId = null;
						if(lc.oppStarter!="SIS"){
							oppId = lc.oppId;
							if(!!lc.oppId)
								LeadDashboardService.updtOppModDate(lc.oppId);
						}
						var fhrId = (!!noFHR)?null:lc.fhrId;
						oppId = (!!noFHR)?null:oppId;
						LeadDashboardService.initiateNewSIS(lc.agentCd, fhrId, lc.leadId, oppId, lc.oppStarter, lc.sisId);
					}
					else{
						navigator.notification.alert("Please create a fact finder.",null,"Lead Dashboard","OK");
					}
				}
				else{
					navigator.notification.alert("Lead data is incomplete please complete it from leads page in side menu.",function(){CommonService.hideLoading();},"Lead Dashboard","OK");
				}
			}
		);
	};

	this.continueSIS = function(){
		debug("in continueSIS");
		if(!!this.sisId) {
			LeadDashboardService.updtOppModDate(this.oppId);
			SisFormService.generateSIS(this.agentCd, this.pglId, this.leadId, this.fhrId, this.sisId, null, null, null, null, this.oppId);
		}
	};
	/*this.continuePsc = function(){
    		debug("in continuePsc");
    		//$state.go("pscPage1");
    		if(!!this.sisId) {
    			LeadDashboardService.updtOppModDate(this.oppId);
    			console.log("lead psc"+this.leadId);
    			console.log("lead"+this.pglId);
    			console.log("lead psc2"+this.oppId);
    			console.log("lead psc3"+this.oppId);
    			console.log("lead psc4"+this.agentCd);
    			$state.go("pscPage1",{"LEAD_ID":this.leadId,"SIS_ID":this.sisId,"OPP_ID":this.oppId,"AGENT_CD":this.agentCd,"PGL_ID":this.pglId});
    			//initiatePSCModuleService.geAddressDetail(this.leadId,this.sisId,this.oppId,this.agentCd,this.pglId);
    		}
    	};*/

	this.viewSISReport = function(){
		if(!!this.sisId){
			CommonService.showLoading();
			LeadDashboardService.getCustEmailId(this.sisId).then(
				function(custEmailId){
					if(!!custEmailId){
						ViewReportService.fetchSavedReport("SIS", lc.sisId, {CUST_EMAIL_ID: custEmailId}).then(
							function(res){
								CommonService.hideLoading();
								$state.go("viewReport", {'REPORT_TYPE': 'SIS', 'REPORT_ID': lc.sisId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": {"CUST_EMAIL_ID": custEmailId, "LEAD_ID": lc.leadId}, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
							}
						);
					}
				}
			);
		}
	};

	this.viewTermSISReport = function(){
		if(!!this.sisId){
			CommonService.showLoading();
			LeadDashboardService.getCustEmailId(this.termSisId).then(
				function(custEmailId){
					if(!!custEmailId){
						ViewReportService.fetchSavedReport("SIS", lc.termSisId, {CUST_EMAIL_ID: custEmailId}).then(
							function(res){
								CommonService.hideLoading();
								$state.go("viewReport", {'REPORT_TYPE': 'SIS', 'REPORT_ID': lc.termSisId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": {"CUST_EMAIL_ID": custEmailId, "LEAD_ID": lc.leadId}, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
							}
						);
					}
				}
			);
		}
	};

	this.viewFHRReport = function(){
		if(!!this.fhrId){
			CommonService.showLoading();
			LeadDashboardService.getFhrCustEmailId(this.fhrId).then(
				function(custEmailId){
					ViewReportService.fetchSavedReport("FHR", lc.fhrId, null).then(
						function(res){
							CommonService.hideLoading();
							$state.go("viewReport", {'REPORT_TYPE': 'FHR', 'REPORT_ID': lc.fhrId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": {"CUST_EMAIL_ID": custEmailId}, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams});
						}
					);
				}
			);
		}
	};

	this.viewFFReport = function(){
		if(!!this.fhrId){
			CommonService.showLoading();
			ViewReportService.fetchSavedReport("FF", lc.fhrId, null).then(
				function(res){
					CommonService.hideLoading();
					$state.go("viewReport", {'REPORT_TYPE': 'FF', 'REPORT_ID': lc.fhrId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": null, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams});
				}
			);
		}
	};

	this.viewTermFFReport = function(){
		if(!!this.termFhrId){
			CommonService.showLoading();
			ViewReportService.fetchSavedReport("FF", lc.termFhrId, null).then(
				function(res){
					CommonService.hideLoading();
					$state.go("viewReport", {'REPORT_TYPE': 'FF', 'REPORT_ID': lc.termFhrId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": null, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams});
				}
			);
		}
	};

	this.viewAppReport = function(){
		if(!!this.appId){
			LeadDashboardService.getCustEmailId(this.sisId).then(
				function(custEmailId){
					var params = {"AGENT_CD": lc.agentCd, "FHR_ID": lc.fhrId, "OPP_ID": lc.oppId, "SIS_ID": lc.sisId, "APP_ID": lc.appId, "CUST_EMAIL_ID": custEmailId, "PGL_ID": lc.pglId, "COMBO_ID": lc.COMBO_ID, "COMBO_FLAG": 'P',"EMPLOYEE_FLAG":lc.empFlag};
					if(!!custEmailId){
						ViewReportService.fetchSavedReport("APP", lc.appId, params).then(
							function(res){
								CommonService.hideLoading();
								$state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': lc.appId, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": params, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams});
							}
						);
					}
				}
			);
		}
	};

	this.viewAppTermReport = function(){
		debug(this.refAppID+"lc.pglId: " + lc.pglId);
		if(!!this.refAppID){
			LeadDashboardService.getCustEmailId(this.termSisId).then(
				function(custEmailId){
					var params = {"AGENT_CD": lc.agentCd, "FHR_ID": lc.termFhrId, "OPP_ID": lc.termOppId, "SIS_ID": lc.termSisId, "APP_ID": lc.refAppID, "CUST_EMAIL_ID": custEmailId, "PGL_ID": lc.pglId, "COMBO_ID": lc.COMBO_ID, "COMBO_FLAG": 'T'};
					if(!!custEmailId){
						ViewReportService.fetchSavedReport("APP", lc.refAppID, params).then(
							function(res){
								CommonService.hideLoading();
								$state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': lc.refAppID, 'PREVIOUS_STATE': "leadDashboard", "REPORT_PARAMS": params, "SAVED_REPORT": null, 'PREVIOUS_STATE_PARAMS': $stateParams});
							}
						);
					}
				}
			);
		}
	};
	this.loadInitiatePSC = function(){//Drastty
		debug("in continuePsc");
		//$state.go("pscPage1");
		if(!!this.sisId) {
			LeadDashboardService.updtOppModDate(this.oppId);
			LeadDashboardService.loadInitiatePSC(this.appId, this.agentCd, this.sisId, this.oppId, this.fhrId, this.policyNo, this.leadId, this.termSisId, this.termFhrId, this.termOppId, this.COMBO_ID, this.refAppID);
		}
	}
	//changes home
	this.loadApplicationCustDet = function(){
		debug("1 - this.COMBO_ID :"+this.COMBO_ID);
		LeadDashboardService.updtOppModDate(this.oppId);
		if(!!this.oppId){
			LeadDashboardService.loadApplicationCustDet(this.appId, this.agentCd, this.sisId, this.oppId, this.fhrId, this.policyNo, this.leadId, this.termSisId, this.termFhrId, this.termOppId, this.COMBO_ID, this.refAppID);
		}
		else{
			CommonService.selectRecords(db, 'lp_myopportunity', true, 'opportunity_id', {fhr_id: this.fhrId, agent_cd: this.agentCd}).then(
				function(res){
					if(!!res && res.rows.length>0){
						lc.oppId = res.rows.item(0).OPPORTUNITY_ID;
					}
					LeadDashboardService.loadApplicationCustDet(lc.appId, lc.agentCd, lc.sisId, lc.oppId, lc.fhrId, lc.policyNo, lc.leadId, this.termSisId, this.termFhrId, this.termOppId, this.COMBO_ID, this.refAppID);
				}
			);
		}
	};

	this.loadAgentDet = function(){
		LeadDashboardService.updtOppModDate(this.oppId);
		LeadDashboardService.loadAgentDet(this.appId, this.agentCd, this.sisId, this.oppId, this.fhrId, this.policyNo, this.termSisId, this.termFhrId, this.termOppId, this.COMBO_ID, this.refAppID);
	};

	this.uploadDocument = function(){
		LeadDashboardService.updtOppModDate(this.oppId);
		$state.go("uploadDoc",{"appid":this.appId,"agentCd": this.agentCd, "sisId": this.sisId, "fhrId": this.fhrId, "policyNo": this.policyNo, "leadId": this.leadId, "pglId": this.pglId, "oppId": this.oppId, "comboId": this.COMBO_ID});
	};

}]);

leadDashboardModule.service('LeadDashboardService',['$state', '$q', 'CommonService', 'SisFormService', 'ApplicationFormDataService', 'hswService','LoadApplicationScreenData','AgentDetailsService','InitiatePSCService', function($state, $q, CommonService, SisFormService, ApplicationFormDataService, hswService,LoadApplicationScreenData,AgentDetailsService, InitiatePSCService){
	"use strict";
	var ls = this;

	this.getOpportunityDetails = function(leadId, fhrId, agentCd){
		var dfd = $q.defer();
		try{
			//policy_no,opportunity_id, lead_id, fhr_id, sis_id, application_id, fhr_signed_timestamp, sis_signed_timestamp, app_submitted_timestamp, last_updated_date,is_cust_det, is_doc_det,is_agnt_det
			CommonService.selectRecords(db, 'lp_myopportunity', true, "*", {"lead_id": leadId, "fhr_id": fhrId, "agent_cd": agentCd}).then(
				function(res) {
					if(!!res && res.rows.length>0) {
						dfd.resolve(CommonService.resultSetToObject(res));
					}
					else {
						dfd.resolve(null);
					}
				}
			);
		}catch(ex){
			debug("Exception in getOpportunityDetails(): " + ex.message);
		}
		return dfd.promise;
	};

	this.getLeadDetails = function(leadId, agentCd){
		var dfd = $q.defer();
		try{
			CommonService.selectRecords(db, 'lp_mylead', true, "lead_created_dttm, lead_name, mobile_no", {"ipad_lead_id": leadId, "agent_cd": agentCd}).then(
				function(res){
					if(!!res && res.rows.length>0) {
						debug("res.rows.length: " + res.rows.length);
						res = CommonService.resultSetToObject(res);
						debug("res: " + JSON.stringify(res));
						dfd.resolve(res);
					}
					else {
						dfd.resolve(null);
					}
				}
			);
		}catch(ex){
			debug("Exception in getLeadDetails(): " + ex.message);
		}
		return dfd.promise;
	};

	this.initiateNewSIS = function(agentCd, fhrId, leadId, oppId, oppStarter, sisId) {
		debug("agentCd: " + agentCd);
		debug("fhrId: " + fhrId);
		debug("leadId: " + leadId);
		debug("oppStarter: " + oppStarter);
		debug("sisId: " + sisId);

		if(oppStarter=="SIS"){
			$state.go("dashboard.solutions",{"FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
		}
		else if(oppStarter=="FF"){
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,'select a.cust_cnt_not_agree as CNT, b.product_pgl_id as PGL from lp_fhr_ffna a, lp_fhr_product_rcmnd b where b.fhr_id=? and b.agent_cd=? and a.agent_cd = b.agent_cd and a.fhr_id=b.fhr_id',[fhrId, agentCd],
						function(tx,res){
							debug("res:" + JSON.stringify(res));
							if(!!res && res.rows.length>0){
								if(!!res.rows.item(0).CNT && res.rows.item(0).CNT=="N"){
									//ls.gotoProductLandingPage(res.rows.item(0).PGL, leadId, fhrId, oppId);
									CommonService.showLoading("Loading...");
									debug("gotoProductLandingPage pglId: " + res.rows.item(0).PGL);
									if(res.rows.item(0).PGL == SIP_PGL_ID){
										hswService.resetHswServiceVals();
										$state.go('hswSIP',{"PGL_ID": res.rows.item(0).PGL, "SOL_TYPE": "Savings", "PROD_NAME": "Smart Income Plus", "FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
									}
									else if(res.rows.item(0).PGL == IMX_PGL_ID){
										$state.go('hswULIP',{"PGL_ID": res.rows.item(0).PGL, "SOL_TYPE": "Wealth", "PROD_NAME": "Fortune Maxima", "FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
									}
									else if(res.rows.item(0).PGL == WMX_PGL_ID){
										$state.go('hswULIP',{"PGL_ID": res.rows.item(0).PGL, "SOL_TYPE": "Wealth", "PROD_NAME": "Wealth Maxima", "FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
									}
									else{
										ls.gotoProductLandingPage(res.rows.item(0).PGL, leadId, fhrId, oppId);
									}
								}
								else
									$state.go("dashboard.solutions",{"FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
							}
						}
					);
				}
			);
		}
		else if(oppStarter=="FHR" || oppStarter=="FHR_SIS"){
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,'select reco_product_deviation_flag, product_pgl_id from lp_myopportunity a, lp_fhr_product_rcmnd b where a.fhr_id=? and a.agent_cd=? and a.opportunity_id=? and b.agent_cd=a.agent_cd and b.fhr_id=a.fhr_id',[fhrId, agentCd, oppId],
						function(tx,res){
							debug("res:" + JSON.stringify(res));
							if(!!res && res.rows.length>0){
								if(!!res.rows.item(0).RECO_PRODUCT_DEVIATION_FLAG && res.rows.item(0).RECO_PRODUCT_DEVIATION_FLAG=="N")
									ls.gotoProductLandingPage(res.rows.item(0).PRODUCT_PGL_ID, leadId, fhrId, oppId);
								else
									$state.go("dashboard.solutions",{"FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
							}
							else
								$state.go("dashboard.solutions",{"FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
						}
					);
				}
			);
		}
	};

	this.getAppDetails = function(appId){
		var dfd = $q.defer();
		CommonService.selectRecords(db,'lp_application_main',true,null,{'application_id': appId},null).then(
			function(res){
				if(!!res && res.rows.length>0){
					dfd.resolve(CommonService.resultSetToObject(res));
				}
				else {
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getLastUpdatedDate = function(date){
		if(!!date){
			var months = {"01": "Jan", "02": "Feb", "03": "Mar", "04": "Apr", "05": "May", "06": "Jun", "07": "Jul", "08": "Aug", "09": "Sep", "10": "Oct", "11": "Nov", "12": "Dec"};
			var dd = date.substring(0,2);
			var mm = date.substring(3,5);
			var yyyy = date.substring(6,10);
			return dd + " " + months[mm] + " " + yyyy;
		}
		else {
			return null;
		}
	};

	this.gotoProductLandingPage = function(pglId, leadId, fhrId, oppId){
		var dfd = $q.defer();
		CommonService.selectRecords(db, 'lp_product_master', true, "product_solution, product_name", {"product_pgl_id": pglId}).then(
			function(res){
				if(!!res && res.rows.length>0){
					if(!!res.rows.item(0).PRODUCT_SOLUTION){
						res.rows.item(0).PRODUCT_SOLUTION = res.rows.item(0).PRODUCT_SOLUTION.replace(" Solutions","");
					}
					$state.go('dashboard.productLandingPage',{"PGL_ID": pglId, "SOL_TYPE": res.rows.item(0).PRODUCT_SOLUTION, "PROD_NAME": res.rows.item(0).PRODUCT_NAME, "FHR_ID": fhrId, "LEAD_ID": leadId, "OPP_ID": oppId});
					dfd.resolve("S");
				}
				else{
					navigator.notification.alert("Product unavailable.",null,"SIS","OK");
				}
			}
		);
		return dfd.promise;
	};
	//PSC CR
	this.loadInitiatePSC = function(appId, agentCd, sisId, oppId, fhrId, policyNo, leadId, refSisId, refFhrID, refOppID, comboId,refAppID){//Drastty
		CommonService.showLoading("Loading...");
		debug("inside loadInitiatePSC" + comboId);
		var params = {"OPP_ID": oppId,"APPLICATION_ID": appId, "AGENT_CD": agentCd, "SIS_ID": sisId, "FHR_ID": fhrId, "POLICY_NO": policyNo, "LEAD_ID": leadId, "REF_SIS_ID": refSisId, "REF_FHRID": refFhrID, "REF_OPP_ID": refOppID, "COMBO_ID": comboId, "REF_APPLICATION_ID": refAppID};
		InitiatePSCService.loadPSCModuleData(params);
	};
	//changes home
	this.loadApplicationCustDet = function(appId, agentCd, sisId, oppId, fhrId, policyNo, leadId, refSisId, refFhrID, refOppID, comboId,refAppID){//Drastty
		CommonService.showLoading("Loading...");
		debug("inside loadApplicationCustDet" + comboId);
		var params = {"OPP_ID": oppId,"APPLICATION_ID": appId, "AGENT_CD": agentCd, "SIS_ID": sisId, "FHR_ID": fhrId, "POLICY_NO": policyNo, "LEAD_ID": leadId, "REF_SIS_ID": refSisId, "REF_FHRID": refFhrID, "REF_OPP_ID": refOppID, "COMBO_ID": comboId, "REF_APPLICATION_ID": refAppID};
		ApplicationFormDataService.continueToApplication(params);
	};

	this.loadAgentDet = function(appId, agentCd, sisId, oppId, fhrId, policyNo, refSisId, refFhrID, refOppID, comboId,refAppID){
		CommonService.showLoading("Loading...");
		debug("inside loadAgentDet" + agentCd);
		if(BUSINESS_TYPE=='IndusSolution' || BUSINESS_TYPE=='GoodSolution'){
		    //1788
		    LoadApplicationScreenData.loadApplicationFlags(appId,agentCd).then(function(response){
                debug("response for the loadApplicationFlags "+JSON.stringify(response));
                debug("response.applicationFlags.ISAGENT_SIGNED"+response.applicationFlags.ISAGENT_SIGNED);
                 if((!!response) && response.applicationFlags.ISAGENT_SIGNED == "Y"){
                        debug("response.applicationFlags.ISAGENT_SIGNED YES");
                        AgentDetailsService.gotoPage('finalSubmission');
                 }else{
			$state.go('agentDetails.spDetails', {"AGENT_CD": agentCd, "APPLICATION_ID": appId, "FHR_ID": fhrId, "SIS_ID": sisId, "POLICY_NO": policyNo, "REF_SIS_ID": refSisId, "REF_FHRID": refFhrID, "REF_OPP_ID": refOppID, "COMBO_ID": comboId, "REF_APPLICATION_ID": refAppID});

                 }
             });
		}
		else{
             	LoadApplicationScreenData.loadApplicationFlags(appId,agentCd).then(function(response){
                debug("response for the loadApplicationFlags "+JSON.stringify(response));
                debug("response.applicationFlags.ISAGENT_SIGNED"+response.applicationFlags.ISAGENT_SIGNED);
                 if((!!response) && response.applicationFlags.ISAGENT_SIGNED == "Y"){
                        debug("response.applicationFlags.ISAGENT_SIGNED YES");
                        AgentDetailsService.gotoPage('finalSubmission');
                 }else{
			$state.go('agentDetails.ccrQues', {"AGENT_CD": agentCd, "APPLICATION_ID": appId, "FHR_ID": fhrId, "SIS_ID":sisId, "POLICY_NO": policyNo});

                 }
             });
		}
	};

	this.getCustEmailId = function(sisId){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select proposer_email from lp_sis_screen_a where sis_id=?",[sisId],
					function(tx,res){
						if(!!res && res.rows.length>0){
							dfd.resolve(res.rows.item(0).PROPOSER_EMAIL);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};


	/**
	 * Function retrive Email id related to FHR Id
	 *
	 * @param {string} fhrId
	 * @returns	Email id or null
	 */
	this.getFhrCustEmailId = function(fhrId){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select EMAIL_ID from LP_FHR_PERSONAL_INF_SCRN_A where FHR_ID=?",[fhrId],
					function(tx,res){
						if(!!res && res.rows.length>0){
							dfd.resolve(res.rows.item(0).EMAIL_ID);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.updtOppModDate = function(oppId){
		debug("oppId : " + oppId);
		var whereClauseObj = {};
		whereClauseObj.OPPORTUNITY_ID = oppId;
		CommonService.updateRecords(db, 'LP_MYOPPORTUNITY',{"LAST_UPDATED_DATE" : CommonService.getCurrDate()}, whereClauseObj).then(
			function(res){
				debug("LP_MYOPPORTUNITY updated : " + JSON.stringify(whereClauseObj));
			}
		);
	};

	this.checkIfLeadIsComplete = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, "select 1 from lp_mylead where birth_date is not null and birth_date!='' and mobile_no is not null and mobile_no!='' and primary_contact_flag is not null and primary_contact_flag!='' and email_id is not null and email_id!='' and gender_cd is not null and gender_cd!=''", [],
					function(tx, res){
						dfd.resolve((!!res) && (res.rows.length>0));
					},
					function(tx, err){
						dfd.resolve(null);
					}
				)
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
