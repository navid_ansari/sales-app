leadListingModule.controller('LeadListingCtrl', ['$state', '$filter', 'LeadsList', 'CommonService', 'AddLeadService', function($state, $filter, LeadsList, CommonService, AddLeadService){
	"use strict";
	debug("in LeadListingCtrl");

	var lc = this;
	if((!LeadsList || !LeadsList.PUSHED || ((LeadsList.PUSHED instanceof Array) && LeadsList.PUSHED.length===0)) || (!!LeadsList && !!LeadsList.SELF && ((LeadsList.SELF instanceof Array) && LeadsList.SELF.length>0))){
		this.activeTab = "S";
	}
	else{
		this.activeTab = "P";
	}

	this.leadsList = LeadsList || {"SELF": [], "PUSHED": []};

	var lead = null;

	for(var i=0; i<this.leadsList.SELF.length; i++){
		lead = this.leadsList.SELF[i];
		lead.leadLeftBorderClass = "lead-list-block-bdr-" + ((i%6)+1);
		lead.leadCircleClass = "lead-list-circle-clr-" + ((i%6)+1);

		lead.INVALID = false;

		lead.LEAD_INITIAL = lead.LEAD_NAME.trim().charAt(0);

		if(!lead.BIRTH_DATE){
			lead.INVALID = true;
		}
		else if(!lead.EMAIL_ID){
			lead.INVALID = true;
		}
		else if(!lead.PRIMARY_CONTACT_FLAG){
			lead.INVALID = true;
		}
		else if(!lead.GENDER_CD || (lead.GENDER_CD!='1' && lead.GENDER_CD!='2')){
			lead.INVALID = true;
		}
		else if(!!lead.PRIMARY_CONTACT_FLAG){
			if(lead.PRIMARY_CONTACT_FLAG=="2" && (!lead.LANDLINE_NO)){
				lead.INVALID = true;
			}
			else if(lead.PRIMARY_CONTACT_FLAG=="3" && (!lead.ALTERNATE_NO)){
				lead.INVALID = true;
			}
		}
	}

	for(i=0; i<this.leadsList.PUSHED.length; i++){
		lead = this.leadsList.PUSHED[i];
		lead.leadLeftBorderClass = "lead-list-block-bdr-" + ((i%6)+1);
		lead.leadCircleClass = "lead-list-circle-clr-" + ((i%6)+1);
		lead.INVALID = false;
		lead.LEAD_INITIAL = lead.LEAD_NAME.trim().charAt(0);
		if(!lead.BIRTH_DATE){
			lead.INVALID = true;
		}
		else if(!lead.EMAIL_ID){
			lead.INVALID = true;
		}
		else if(!lead.PRIMARY_CONTACT_FLAG){
			lead.INVALID = true;
		}
		else if(!lead.GENDER_CD || (lead.GENDER_CD!='1' && lead.GENDER_CD!='2')){
			lead.INVALID = true;
		}
		else if(!!lead.PRIMARY_CONTACT_FLAG){
			if(lead.PRIMARY_CONTACT_FLAG=="2" && (!lead.LANDLINE_NO)){
				lead.INVALID = true;
			}
			else if(lead.PRIMARY_CONTACT_FLAG=="3" && (!lead.ALTERNATE_NO)){
				lead.INVALID = true;
			}
		}
	}

	lc.openMenu = function(){
		CommonService.openMenu();
	};

	this.onTabClick = function(tab){
		this.activeTab = tab;
	};

	this.onEdit = function(tab, leadId){
		CommonService.showLoading();
		AddLeadService.leadData = null;
		$state.go("leadPage1", {"LEAD_ID": leadId});
	};

	this.onContinue = function(tab, leadId){
		CommonService.showLoading();
		$state.go("leadDashboard", {"LEAD_ID": leadId});
	};

	CommonService.hideLoading();
}]);

leadListingModule.service('LeadListingService', ['$state', '$filter', 'CommonService', '$q', function($state, $filter, CommonService, $q){
	"use strict";
	var ls = this;

	this.getLeadsList = function(agentCd){
		var dfd = $q.defer();
		debug("agentCd: " + agentCd);
		var selfLeadQuery = "select mylead.*, lov.lov_value from lp_mylead mylead inner join lp_lead_lov_master lov where mylead.ipad_lead_id is not null and (mylead.crm_lead_id is null or mylead.crm_lead_id=?) and mylead.agent_cd=? and mylead.lead_source1_cd=? and lov.lov_cd=mylead.lead_source1_cd and lov.lov_type=? ORDER BY ipad_modified_dttm DESC";
		var pushedLeadQuery = "select mylead.*, lov.lov_value from lp_mylead mylead inner join lp_lead_lov_master lov where mylead.ipad_lead_id is not null and (mylead.crm_lead_id is not null and mylead.crm_lead_id!=?) and mylead.agent_cd=? and mylead.lead_source1_cd!=? and lov.lov_cd=mylead.lead_source1_cd and lov.lov_type=?";
		var parameterList = ['', agentCd, '14', 'Source 1'];
		var leadsList = {"SELF": [], "PUSHED": []};
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx, selfLeadQuery, parameterList,
					function(tx,slRes){
						if(!!slRes){
							debug("slRes: " + slRes.rows.length);
							if(!!slRes && slRes.rows.length>0){
								slRes = CommonService.resultSetToObject(slRes);

								if(!!slRes && !(slRes instanceof Array))
									slRes = [slRes];
								leadsList.SELF = slRes;
							}
							CommonService.executeSql(tx, pushedLeadQuery, parameterList,
								function(tx,plRes){
									if(!!plRes && plRes.rows.length>0){
										debug("plRes: " + plRes.rows.length);
										plRes = CommonService.resultSetToObject(plRes);
										if(!!plRes && !(plRes instanceof Array))
											plRes = [plRes];
										leadsList.PUSHED = plRes;
									}
									dfd.resolve(leadsList);
								},
								function(tx,plErr){
									dfd.resolve(leadsList);
								}
							);
						}
						else{
							dfd.resolve(leadsList);
						}
					},
					function(tx,err){
						dfd.resolve(leadsList);
					}
				);
			},
			function(err){
				dfd.resolve(leadsList);
			}
		);
		return dfd.promise;
	};

}]);
