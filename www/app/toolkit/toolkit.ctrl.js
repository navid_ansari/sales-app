toolkitModule.controller('ToolkitCtrl',['$state','$q','ToolkitService','ToolkitList','CommonService',function($state,$q,ToolkitService,ToolkitList,CommonService){
	"use strict";
	var tc = this;
	debug("toolkit controller");
	this.toolkitList = ToolkitList;
	debug("toolkitList is::"+JSON.stringify(ToolkitList));
	this.viewDocument = function(toolkitObj){
		ToolkitService.viewDocument(toolkitObj);
	};
	this.openMenu = function(){
		CommonService.openMenu();
	};
	CommonService.hideLoading();
}]);

toolkitModule.service('ToolkitService',['$q','$state','CommonService', function($q,$state,CommonService){
	"use strict";
	var ts = this;
	this.viewDocument = function(toolkitObj){
		if(!!toolkitObj){
			debug("Doc Name Selected: " + toolkitObj.DOC_NAME);
			cordova.exec(
				function(fileName){
					debug("fileName: " + fileName);
					CommonService.openFile(fileName);
				},
				function(errMsg){
					debug(errMsg);
				},
				"PluginHandler", "getDecryptedFileName", [(toolkitObj.DOC_NAME + toolkitObj.DOC_TYPE), toolkitObj.DOC_PATH]
			);
		}
	};

	this.getToolkitList = function(username){
		debug("Inside function ToolkitList: " + username);
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				try{
					CommonService.executeSql(tx,"select * from lp_doc_fetch_user_master where agent_cd=? and doc_path=?", [username, REL_PATH_RESOURCES],
						function(tx,res){
							var toolkitList = [];
							debug("res.rows.length: " + res.rows.length + "Username::: " + username + "REL_PATH_RESOURCES:: " + REL_PATH_RESOURCES);
							for(var i=0; i<res.rows.length; i++){
								var toolkitObj = {};
								toolkitObj.DOC_NAME = res.rows.item(i).DOC_NAME;
								toolkitObj.DOC_TYPE = res.rows.item(i).DOC_TYPE;
								toolkitObj.DOC_PATH = res.rows.item(i).DOC_PATH;
								toolkitObj.FETCH_DATE = res.rows.item(i).FETCH_DATE;
								toolkitList.push(toolkitObj);
							}
							debug("toolkitList.length: " + toolkitList.length);
							dfd.resolve(toolkitList);
						},
						function(tx,err){
							debug("Error in getToolkitList().transaction(): " + err.message);
							dfd.resolve(toolkitList);
						}
					);
				}
				catch(error){
					debug("Error::"+error.message);
				}
			},
			function(err){
				debug("Error in getToolkitList(): " + err.message);
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
