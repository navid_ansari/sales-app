// Ionic Starter App
//strict();
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var db = null;
var sisDB = null;
var app = angular.module('app', ['ionic', 'ngMessages', 'angular.panels', 'ui.bootstrap','angular-carousel','chart.js', 'countTo','rzModule','ngStorage',
	'app.common',
	'app.login',
	'app.passwordManagement',
	'app.donePages',
	'app.dashboard',
	'app.overlayMenu',
	'app.sis',
	'app.signature',
	'app.viewReport',
	'app.lead',
	'app.factFinder',
	'app.applicationModule',
	'app.syncApp', // Drastty
	'app.optionalDownloads',
	'app.toolkit',
	'app.uploadDoc',
	'app.agentDetails',
	'app.disclaimer',
	'app.hsw',
	'app.onlinePayModule',
	'app.fhr',
	'app.showDatabase',
	'app.initiatePSC',
	'app.agentInfo'
])
.run(['$ionicPlatform', '$state','$rootScope', function($ionicPlatform, $state,$rootScope) {
	"use strict";

	$ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			// Don't remove this line unless you know what you are doing. It stops the viewport
			// from snapping when text inputs are focused. Ionic handles this internally for
			// a much nicer keyboard experience.
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if(window.StatusBar) {
			StatusBar.styleDefault();
		}
		if (navigator.splashscreen) {
			console.warn('Hiding splash screen');
			// We're done initializing, remove the splash screen
			setTimeout(function() {
				navigator.splashscreen.hide();
			}, 100);
		}
	});
	$rootScope.setTabVal = false;
	$ionicPlatform.registerBackButtonAction(function(e){
		debug("going back now y'all " + $state.current.name);
		if($state.current.name==LOGIN_STATE){
			navigator.notification.confirm("Are you sure you want to exit?",
				function(buttonIndex){
					if(buttonIndex=="1"){
						navigator.app.exitApp();
					}
				},
				'Exit',
				['Yes',' No']
			);
		}
		else{
			e.preventDefault();
			e.stopPropagation();
		}
		return false;
	}, 101);
}]);
app.filter('myCurrency', ['$filter', function($filter) {
	return function(input) {
		if (!!input) {
			var result = input.toString().split('.');
			result[0] = result[0].split(",").join("");
			var lastThree = result[0].substring(result[0].length - 3);
			var otherNumbers = result[0].substring(0, result[0].length - 3);
			if (otherNumbers !== ''){
				lastThree = ',' + lastThree;
			}
			var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if (result.length > 1) {
				output += "." + result[1];
			}
			return output;
		}
	};
}]);
