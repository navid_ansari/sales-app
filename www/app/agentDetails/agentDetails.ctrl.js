agentDetailsModule.controller('AgentDetailsCtrl', ['$state','$stateParams', 'CommonService','AgentDetailsService','FinalSubmissionService','sisData', function($state, $stateParams, CommonService, AgentDetailsService,FinalSubmissionService,sisData){
    var adc = this;
    debug("params inside controller >>"+JSON.stringify($stateParams));
     var PGL_ID = sisData.sisMainData.PGL_ID;
     debug("data is "+PGL_ID);
     this.showCCR = true;
     if(PGL_ID == "185" || PGL_ID == "191"){
        this.showCCR = false;
     }
}]);

agentDetailsModule.service('AgentDetailsService',['$q', 'CommonService','$stateParams','FinalSubmissionService','$state','agentTimeService','SisFormService','LoginService', function($q, CommonService,$stateParams,FinalSubmissionService,$state,agentTimeService,SisFormService,LoginService){
    debug("$stateParams params inside controller >>"+JSON.stringify($stateParams));
    this.APPLICATION_ID = $stateParams.APP_ID;
    this.AGENT_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;

    this.setTimeLineTicks = function(){
        var dfd = $q.defer();
        debug("params inside servic >>");//+JSON.stringify($stateParams)
        FinalSubmissionService.getAgentImpData($stateParams.APP_ID,LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
            function(data){
                var sisID = "";
                if(!!$stateParams.SIS_ID){
                    sisID = $stateParams.SIS_ID;
                }else{
                    sisID = $stateParams.PREVIOUS_STATE_PARAMS.SIS_ID;
                }
                debug("ImpData :"+JSON.stringify(data));
                SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,sisID).then(
                    function(sisdata){
                        var showCCR = true;
                        var pglID = sisdata.sisMainData.PGL_ID;
                        debug("<<< VALUE TO GET IN TICK CLASS >>"+pglID);
                        if(pglID == "185" || pglID == "191"){
                            showCCR = false;
                        }
                        debug("the data is "+JSON.stringify(data));
                        if(!!data){
                            if(!!data.ImpData){
                                debug("tick sp details");
                                agentTimeService.isSpDetailCompleted = true;
                            }
                            if(showCCR){
                                debug("NON IRAKSHA ");
                                if(!!data.DocUploadData){
                                    agentTimeService.isCCRCompleted = true;
                                }
                            }else{
                                debug("IRAKSHA CASE SO CCR NOT COMPLETED");
                            }
                        }
                        dfd.resolve(null);
                    }
                );
            }
        );
        return dfd.promise;
    }

    this.gotoPage = function(pageName){
        console.log(JSON.stringify($stateParams)+"gotoPage :pageName:"+pageName);
        debug("STATE PARAMS >>>>"+JSON.stringify($stateParams));

        var pglID = "";
        var appID = "";
        var sisID = "";

        if(!!$stateParams.APPLICATION_ID){
            appID = $stateParams.APPLICATION_ID;
            debug("APPLICATION ID >>"+appID);
        }else{
            appID = $stateParams.APP_ID;
            debug("APP ID >>"+appID);
        }

        if(!!$stateParams.SIS_ID){
            sisID = $stateParams.SIS_ID;
        }else{
            sisID = $stateParams.PREVIOUS_STATE_PARAMS.SIS_ID;
        }

        SisFormService.loadSavedSISData(LoginService.lgnSrvObj.userinfo.AGENT_CD,sisID).then(
            function(data){
                pglID = data.sisMainData.PGL_ID;
                debug("<<< VALUE TO GET IN FINAL SUBMISSION >>"+pglID);
                if(!!$stateParams.APPLICATION_ID){
                    appID = $stateParams.APPLICATION_ID;
                    debug("APPLICATION SWITCH ID >>"+appID);
                }else{
                    appID = $stateParams.APP_ID;
                    debug("APP SWITCH ID >>"+appID);
                }
                if(!!$stateParams.SIS_ID){
                    sisID = $stateParams.SIS_ID;
                }else{
                    sisID = $stateParams.PREVIOUS_STATE_PARAMS.SIS_ID;
                }
                switch(pageName){
                    case 'spDetails':
                    		agentTimeService.setActiveTab(pageName);
                    		$state.go('agentDetails.spDetails',{"AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "APPLICATION_ID": appID,"FHR_ID": $stateParams.FHR_ID, "SIS_ID":sisID, "POLICY_NO": $stateParams.POLICY_NO,"REF_SIS_ID":$stateParams.REF_SIS_ID,"REF_FHRID":$stateParams.REF_FHRID,"REF_OPP_ID":$stateParams.REF_OPP_ID,"COMBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":$stateParams.REF_APPLICATION_ID});
                        break;
                    case 'ccrQues':
						if(pglID == "185" || pglID == "191"){
                            navigator.notification.alert("Not Allowed For IRaksha Product",null,"Agent Details","OK");
						}else{
							CommonService.showLoading();
							FinalSubmissionService.getAgentImpData(appID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
								function(AgentData){
									debug("ImpData :"+JSON.stringify(AgentData));
									//changes 27th jan
									CommonService.hideLoading();
									var showCCR = true;
									if(pglID == "185" || pglID == "191"){
										showCCR = false;
									}
									if(AgentData.ImpData == undefined || (AgentData.DocUploadData == undefined && !!showCCR)){
                                        navigator.notification.alert("Please Complete All Agent details !!",null,"Agent Details","OK");
										
									}
									else{
										agentTimeService.setActiveTab(pageName);
										$state.go('agentDetails.ccrQues',{"AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "APPLICATION_ID": appID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID":sisID, "POLICY_NO": $stateParams.POLICY_NO,"REF_SIS_ID":$stateParams.REF_SIS_ID,"REF_FHRID":$stateParams.REF_FHRID,"REF_OPP_ID":$stateParams.REF_OPP_ID,"COMBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":$stateParams.REF_APPLICATION_ID});
									}
								}
							);
						}
                        break;
                    case 'finalSubmission':
                            FinalSubmissionService.getAgentImpData(appID, LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
                                function(AgentData){
                                    debug("ImpData :"+JSON.stringify(AgentData));
                                    CommonService.showLoading("Loading...");
                                    var showCCR = true;
                                    if(pglID == "185" || pglID == "191"){
                                       showCCR = false;
                                    }
                                    CommonService.hideLoading();
                                    if(AgentData.ImpData == undefined || (AgentData.DocUploadData == undefined && !!showCCR)){
                                        navigator.notification.alert("Please Complete All Agent details !!",null,"Agent Details","OK");
										CommonService.hideLoading();
									}
                                    else{
										agentTimeService.setActiveTab(pageName);
										$state.go('agentDetails.finalSubmission',{"AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "APPLICATION_ID": appID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID":sisID, "POLICY_NO": $stateParams.POLICY_NO,"REF_SIS_ID":$stateParams.REF_SIS_ID,"REF_FHRID":$stateParams.REF_FHRID,"REF_OPP_ID":$stateParams.REF_OPP_ID,"CO`MBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":$stateParams.REF_APPLICATION_ID});
									}
                                }
                            );
                        break;
                    default:{
						agentTimeService.setActiveTab(pageName);
						$state.go('agentDetails.spDetails',{"AGENT_CD": LoginService.lgnSrvObj.userinfo.AGENT_CD, "APPLICATION_ID": appID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID":sisID, "POLICY_NO": $stateParams.POLICY_NO,"REF_SIS_ID":$stateParams.REF_SIS_ID,"REF_FHRID":$stateParams.REF_FHRID,"REF_OPP_ID":$stateParams.REF_OPP_ID,"COMBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":$stateParams.REF_APPLICATION_ID});
					}
                }

            }
        );


    }
}]);
