spDetailsModule.controller('SpDetailsCtrl', ['$q','$state','SpDetailsService','LoginService','LoadData','CommonService','sisData','agentTimeService','OppData','$timeout','$scope', function($q,$state, SpDetailsService,LoginService,LoadData,CommonService,sisData,agentTimeService, OppData, $timeout,$scope){
    CommonService.hideLoading();
    debug("OppData : " + JSON.stringify(OppData));
    agentTimeService.setActiveTab("spDetails");
    var spdc = this;
    this.codeRegex = USERNAME_REGEX;
	this.codeSourcerRegex = SOURCER_REGEX;
	this.agentCodeRegex = AGENT_CD_REGEX;
    this.mobnoRegex = MOBILE_REGEX;
    this.nameRegex = FULLNAME_REGEX_SP;
    this.spValidated = false;
    this.genOpClick = false;
    this.SPData = {};
    this.SPData.OppData = OppData;

	this.businessType = BUSINESS_TYPE;
    this.updateSourcerValue=function(x){
            console.log("value of x: " +spdc.SPData.SOURCER_CODE);
                 /*For IBL User */
            if(BUSINESS_TYPE=="IndusSolution"){
                if(spdc.SPData.SOURCER_CODE.indexOf("C") == 0 ||  spdc.SPData.SOURCER_CODE.indexOf("R") ==0){
                            spdc.len = 6;
                        }
                        else{
                            spdc.len = 5;
                        }
                    if(spdc.SPData.SOURCER_CODE.length ==spdc.len ){
                        if(SOURCER_REGEX.test(spdc.SPData.SOURCER_CODE)){
                            spdc.isSourceCode = false;
                        }
                    else{
                        spdc.isSCodepattern =true;
                        spdc.isSCodemax =false;
						spdc.isSourceCode = true;
                        }
                    }
                    else{
                        spdc.isSourceCode = true;
                        spdc.isSCodemax =true;
                        spdc.isSCodepattern =false;
                        }
                     }
                      /*  non IBL Case */
               else{
              spdc.len = 20;
             if(spdc.SPData.SOURCER_CODE.length<=spdc.len ){
                    debug("spdc.len" + spdc.len);
                   if(USERNAME_REGEX.test(spdc.SPData.SOURCER_CODE)){
                       spdc.isSourceCode = false;
                     }
                 else{
                     spdc.isSourceCode = true;
                     spdc.isSCodepattern =true;
                     spdc.isSCodemax =false;
                 }
                }
                else{
                      spdc.isSourceCode = true;
                      spdc.isSCodepattern =false;
                      spdc.isSCodemax =true;
                 }
                }
              }



	this.calHeight = function(){
											 var agentGetHeight = $(".agent-details .fixed-bar").height();
											 console.log("agentGetHeight : " + agentGetHeight);
											 $(".agent-details .custom-position").css({top:agentGetHeight + "px"});

											 }

											 /* Header Hight Calculator */
											 $timeout(this.calHeight,1);
											 /*
											 $timeout(function(){
													  this.calHeight
													  },1);
													  */


	this.otherAgentList = [
		{"OTHER_AGENT_DISPLAY": "Select", "OTHER_AGENT": null},
		{"OTHER_AGENT_DISPLAY": "Yes", "OTHER_AGENT": "Y"},
		{"OTHER_AGENT_DISPLAY": "No", "OTHER_AGENT": "N"},
	];

    this.EmpFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
	if(this.businessType=='IndusSolution'){
		this.nameLabel = "SP Name";
	    if(this.EmpFlag=='R'){
	        this.SPData.IMP_RM_CAMS_CD = LoginService.lgnSrvObj.userinfo.AGENT_CD;
	    }
	}
	else{
		this.nameLabel = "Agent Name";
	}
	if(this.businessType=='GoodSolution'){
		if(this.EmpFlag!='L'){
			if(this.EmpFlag=='R' || this.EmpFlag=='S')
				this.otherAgentFlag = 'Y';
			else
				this.otherAgentFlag = 'N';
		}
	}
    /*this.SPData.IMP_RM_CAMS_CD = '4028886';
    this.SPData.IMP_AGENT_CAMS_CD = '4588531';
    this.SPData.SUBOFFICECODE = 'IBL0215';*/
	debug("LoadData: " + JSON.stringify(LoadData));
    if(!!LoadData){
        this.SPData.AGENT_CD = LoadData.AGENT_CD;
        this.SPData.FHR_ID = LoadData.FHR_ID;
        this.SPData.SIS_ID = LoadData.SIS_ID;
        this.SPData.APPLICATION_ID = LoadData.APPLICATION_ID;
        this.SPData.POLICY_NO = LoadData.POLICY_NO;
        this.SPData.FHR_ID = LoadData.FHR_ID;
        debug("!!LoadData.resArr : " + JSON.stringify(LoadData.resArr));
        if(!!LoadData.resArr && !!LoadData.resArr.IMP_RM_CAMS_CD){
			if(!!LoadData.resArr.IMP_RM_CAMS_CD)
				spdc.SPData.spValidated = true;
			else {
				spdc.SPData.spValidated = false;
			}
            spdc.SPData.IMP_RM_CAMS_CD = LoadData.resArr.IMP_RM_CAMS_CD;
            spdc.SPData.IMP_AGENT_CAMS_CD = LoadData.resArr.IMP_AGENT_CAMS_CD;
            spdc.SPData.SUBOFFICECODE = LoadData.resArr.SUBOFFICECODE;
            spdc.SPData.SOURCER_CODE = LoadData.resArr.SOURCER_CODE;
            spdc.SPData.SOURCER_NAME = LoadData.resArr.SOURCER_NAME;
            spdc.SPData.SOURCER_CONTACT_NO = LoadData.resArr.SOURCER_CONTACT_NO ? parseInt(LoadData.resArr.SOURCER_CONTACT_NO) : null;
            spdc.SPData.IMP_RM_NAME = LoadData.resArr.IMP_RM_NAME;
            spdc.SPData.IMP_AGENT_NAME = LoadData.resArr.IMP_AGENT_NAME;
            spdc.SPData.IRDA_LIC_NO = (LoadData.resArr.IRDA_LIC_NO);
            spdc.SPData.IRDA_LIC_EXP_DT = (!!LoadData.resArr.IRDA_LIC_EXP_DT) ? (LoadData.resArr.IRDA_LIC_EXP_DT.substring(0,10)) : null;
            spdc.SPData.MOBILENO = LoadData.resArr.MOBILENO;
            spdc.SPData.CHANNEL_NAME = LoadData.resArr.CHANNEL_NAME;
            spdc.SPData.OFFICE_CODE = LoadData.resArr.OFFICE_CODE;
            spdc.SPData.SUBOFFICECODE = LoadData.resArr.SUBOFFICECODE;
            spdc.SPData.OFFICE_NAME = LoadData.resArr.OFFICE_NAME;
            spdc.SPData.OFFICE_ADDRESS1 = LoadData.resArr.OFFICE_ADDRESS1;
            spdc.SPData.OFFICE_ADDRESS2 = LoadData.resArr.OFFICE_ADDRESS2;
            spdc.SPData.OFFICE_STATE = LoadData.resArr.OFFICE_STATE;
            spdc.SPData.OFFICE_ZIP_CODE = LoadData.resArr.OFFICE_ZIP_CODE;
            spdc.SPData.OFFICE_OFFICE_TEL = LoadData.resArr.OFFICE_OFFICE_TEL;
        }
    }

	this.onNext = function(){
		this.SPData.IMP_AGENT_CAMS_CD = this.SPData.AGENT_CD;
		var SPData = this.SPData;
		var saveData = {"AGENT_CD": SPData.AGENT_CD, "APPLICATION_ID": SPData.APPLICATION_ID, "SIS_ID": SPData.SIS_ID, "POLICY_NO": SPData.POLICY_NO, "FHR_ID": SPData.FHR_ID};

		if(this.businessType=='IndusSolution')
			saveData.FHR_ID = SPData.FHR_ID;

		SpDetailsService.saveNoOtherAgentData(saveData).then(
			function(res){
				var PGL_ID = sisData.sisMainData.PGL_ID;
				debug("PGL ID IN SP DETAILS >>>"+PGL_ID);
				if(PGL_ID =="185" || PGL_ID == "191"){
					//agent timeline changes..
					agentTimeService.setActiveTab("finalSubmission");
					agentTimeService.isSpDetailCompleted = true;
					$state.go('agentDetails.finalSubmission',{"AGENT_CD": SPData.AGENT_CD, "APPLICATION_ID": SPData.APPLICATION_ID, "FHR_ID": SPData.FHR_ID, "SIS_ID":SPData.SIS_ID, "POLICY_NO": SPData.POLICY_NO})
				}else{
					//agent timeline changes..
					agentTimeService.setActiveTab("ccrQues");
					agentTimeService.isSpDetailCompleted = true;
					$state.go('agentDetails.ccrQues',{"AGENT_CD": SPData.AGENT_CD, "APPLICATION_ID": SPData.APPLICATION_ID, "DOC_ID": "6033010334"});
				}
			}
		);
	};

    this.validateSP = function(){
        var dfd = $q.defer();
        CommonService.showLoading("Loading...");
        debug("this.SPData : " + JSON.stringify(this.SPData));
        SpDetailsService.validateSP(spdc.SPData).then(
            function(resp){
                if(!!resp){
                    spdc.SPData.spValidated = true;
                    spdc.SPData.IMP_RM_NAME = resp.RS.RMN;
                    spdc.SPData.IMP_AGENT_NAME = resp.RS.SPN;
                    spdc.SPData.IRDA_LIC_NO = resp.RS.LICN;
                    spdc.SPData.IRDA_LIC_EXP_DT = resp.RS.LICE;
                    spdc.SPData.MOBILENO = resp.RS.MOB;
                    spdc.SPData.CHANNEL_NAME = resp.RS.CHN;
                    spdc.SPData.OFFICE_CODE = resp.RS.OFFC;
                    spdc.SPData.SUBOFFICECODE = resp.RS.SOC;
                    spdc.SPData.OFFICE_NAME = resp.RS.OFFN;
                    spdc.SPData.OFFICE_ADDRESS1 = resp.RS.OFFA;
                    spdc.SPData.OFFICE_ADDRESS2 = resp.RS.OFFAA;
                    spdc.SPData.OFFICE_STATE = resp.RS.OFFST;
                    spdc.SPData.OFFICE_ZIP_CODE = resp.RS.OFFPN;
                    spdc.SPData.OFFICE_OFFICE_TEL = resp.RS.OFFTEL;
                    CommonService.hideLoading();
                }
                else{
                    CommonService.hideLoading();
                    debug("resp null");
                }
            }
        );
        return dfd.promise;
    };

    this.updateSpData = function(spDetailsForm,SPData){
        var dfd = $q.defer();
        this.genOpClick = true;
        debug("SPData : " + JSON.stringify(SPData));
        if(spDetailsForm.$invalid == false){
            SpDetailsService.updateSpData(SPData).then(
                function(res){
                    if(!!res){
                        //7th July
                        var PGL_ID = sisData.sisMainData.PGL_ID;
                        debug("PGL ID IN SP DETAILS >>>"+PGL_ID);
                        if(PGL_ID =="185" || PGL_ID == "191"){
                            //agent timeline changes..
                            agentTimeService.setActiveTab("finalSubmission");
                            agentTimeService.isSpDetailCompleted = true;
                            $state.go('agentDetails.finalSubmission',{"AGENT_CD": SPData.AGENT_CD, "APPLICATION_ID": SPData.APPLICATION_ID, "FHR_ID": SPData.FHR_ID, "SIS_ID":SPData.SIS_ID, "POLICY_NO": SPData.POLICY_NO,"REF_SIS_ID":SPData.REF_SIS_ID,"REF_FHRID":SPData.REF_FHRID,"REF_OPP_ID":SPData.REF_OPP_ID,"COMBO_ID":SPData.COMBO_ID,"REF_APPLICATION_ID":SPData.REF_APPLICATION_ID})
                        }else{
                            //agent timeline changes..
                            agentTimeService.setActiveTab("ccrQues");
                            agentTimeService.isSpDetailCompleted = true;
                            $state.go('agentDetails.ccrQues',{"AGENT_CD": SPData.AGENT_CD, "APPLICATION_ID": SPData.APPLICATION_ID, "DOC_ID": "6033010334","REF_SIS_ID":SPData.REF_SIS_ID,"REF_FHRID":SPData.REF_FHRID,"REF_OPP_ID":SPData.REF_OPP_ID,"COMBO_ID":SPData.COMBO_ID,"REF_APPLICATION_ID":SPData.REF_APPLICATION_ID});
                        }
                    }
                }
            );
        }else{
            navigator.notification.alert('Please fill all the fields with valid data.',null,"Agent Details","OK");
        }
        return dfd.promise;
    };

}]);

spDetailsModule.service('SpDetailsService',['$q','$state', 'CommonService','LoginService','LoadApplicationScreenData', function($q, $state, CommonService,LoginService, LoadApplicationScreenData){
    var spds = this;
    this.SPData = {};
    this.validateSP = function(SPData){
        var dfd = $q.defer();
        debug("SPData : " + JSON.stringify(SPData));
        var loginType = CommonService.checkConnection();
        if(loginType=='online'){
            debug("You are " + loginType);
            spds.fetchSpDetails(SPData).then(
                function(resp){
                    if(!!resp && resp.RS.RESP=='S'){
                        debug("fetchSpDetails : " + JSON.stringify(resp));
                        spds.saveSpData(resp).then(
                            function(saveResp){
                                if(!!saveResp){
                                    dfd.resolve(saveResp);
                                }else{
                                    CommonService.hideLoading();
                                    dfd.resolve(null)
                                }
                            }
                        );
                    }else{
                        CommonService.hideLoading();
                        navigator.notification.alert('Error: ' + resp.RS.ECM,null,"Agent Details","OK");
                    }
                }
            );
        }else{
            CommonService.hideLoading();
            navigator.notification.alert('Please connect to the internet to validate the agent details.',null,"Agent Details","OK");
        }
        return dfd.promise;
    };

    this.fetchSpDetails = function(SPData){
        var dfd = $q.defer();
        if(!!SPData && !!this.SPData)
            this.SPData.IMP_AGENT_CAMS_CD = SPData.IMP_AGENT_CAMS_CD;
        function fsdpSucc(resp){
            debug("fetchSpDetails Resp : " + JSON.stringify(resp));
            if(resp.RS.RESP=='S'){
                resp.RS.AGENT_CD = SPData.AGENT_CD;
                resp.RS.FHR_ID = SPData.FHR_ID;
                resp.RS.SIS_ID = SPData.SIS_ID;
                resp.RS.APPLICATION_ID = SPData.APPLICATION_ID;
                resp.RS.POLICY_NO = SPData.POLICY_NO;
                resp.SPData = SPData;
                dfd.resolve(resp);
            }else{
                dfd.resolve(resp)
            }
        }

        function fsdpErr(data, status, headers, config, statusText) {
            debug("error in fetchSpDetails");
            dfd.resolve(null);
        }
        try{
            var request = {};
            request.REQ = {};
            request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.PWD = LoginService.lgnSrvObj.password;
            request.REQ.DVID = LoginService.lgnSrvObj.dvid;
            request.REQ.RMC = SPData.IMP_RM_CAMS_CD;
            request.REQ.SPC = SPData.IMP_AGENT_CAMS_CD;
            request.REQ.SOC = SPData.SUBOFFICECODE;
            request.REQ.POT = 'C,U';
            //debug("BTI : " + LoginService.lgnSrvObj.userinfo.BUSSINESS_TYPE_ID)
            request.REQ.BTI = BUSINESS_TYPE;
            request.REQ.ACN = 'FSDP';
            debug("fetchSpDetails Request: " + JSON.stringify(request));
            CommonService.ajaxCall(VALIDATE_SP_SERVLET_URL, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, fsdpSucc, fsdpErr);
            //dfd.resolve(fetchSpDetails);
        }catch(ex){
            debug("exception in getSpData : " + ex.message);
            dfd.resolve(null);
        }
        return dfd.promise;
    };

    this.saveSpData = function(resp){
        var dfd = $q.defer();
        LoadApplicationScreenData.loadAgentImpData(resp.RS.APPLICATION_ID,resp.RS.AGENT_CD).then(function(AgentImpData){

        		    if(!!AgentImpData && !!AgentImpData.APPLICATION_ID)
        		    {
                        CommonService.transaction(db,
                                    function(tx){
                                        CommonService.executeSql(tx,"UPDATE LP_APP_SOURCER_IMPSNT_SCRN_L SET POLICY_NO = ?, IMP_RM_CAMS_CD = ?, IMP_AGENT_CAMS_CD = ?, SOURCER_CODE = ?, SOURCER_NAME = ?, SOURCER_CONTACT_NO = ?, IMP_RM_NAME = ?, IMP_AGENT_NAME = ?, IRDA_LIC_NO = ?, IRDA_LIC_EXP_DT = ?, MOBILENO = ?, CHANNEL_NAME = ?, OFFICE_CODE = ?, SUBOFFICECODE = ?, OFFICE_NAME = ?, OFFICE_ADDRESS1 = ?, OFFICE_ADDRESS2 = ?, OFFICE_STATE = ?, OFFICE_ZIP_CODE = ?, OFFICE_OFFICE_TEL = ?, MODIFIED_DATE = ?, ISSYNCED = ?,AGENT_REL_FLAG = ? WHERE APPLICATION_ID = ? AND AGENT_CD = ?",[resp.RS.POLICY_NO,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N",'N',resp.RS.APPLICATION_ID,resp.RS.AGENT_CD],
                                            function(tx,res){
                                                debug("LP_APP_SOURCER_IMPSNT_SCRN_L : updated record successfully!");
                        //Ashwin changes 29-12-2016 removed 29th parameter related to CCR
                                                if(!!resp.SPData && !!resp.SPData.OppData && !!resp.SPData.OppData.COMBO_ID && resp.SPData.OppData.COMBO_FLAG == 'Y'){
                                                    //CommonService.executeSql(tx,"UPDATE LP_APP_SOURCER_IMPSNT_SCRN_L SET POLICY_NO = ?, IMP_RM_CAMS_CD = ?, IMP_AGENT_CAMS_CD = ?, SOURCER_CODE = ?, SOURCER_NAME = ?, SOURCER_CONTACT_NO = ?, IMP_RM_NAME = ?, IMP_AGENT_NAME = ?, IRDA_LIC_NO = ?, IRDA_LIC_EXP_DT = ?, MOBILENO = ?, CHANNEL_NAME = ?, OFFICE_CODE = ?, SUBOFFICECODE = ?, OFFICE_NAME = ?, OFFICE_ADDRESS1 = ?, OFFICE_ADDRESS2 = ?, OFFICE_STATE = ?, OFFICE_ZIP_CODE = ?, OFFICE_OFFICE_TEL = ?, MODIFIED_DATE = ?, ISSYNCED = ?,AGENT_REL_FLAG = ? WHERE APPLICATION_ID = ? AND AGENT_CD = ?",[resp.RS.POLICY_NO,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N",'N',resp.RS.APPLICATION_ID,resp.RS.AGENT_CD],
                                                    CommonService.executeSql(tx,"insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[resp.SPData.OppData.REF_APPLICATION_ID,resp.SPData.OppData.AGENT_CD,resp.SPData.OppData.REF_POLICY_NO,resp.SPData.OppData.REF_SIS_ID,resp.SPData.OppData.REF_FHRID,AgentImpData.PREFERRED_LANGUAGE_CD,AgentImpData.PREFERRED_CALL_TIME,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N","N",AgentImpData.DIGITAL_PSC_FLAG, AgentImpData.ISDIGITAL_PSC_DONE],
                                                        function(tx,res){
                                                            debug("LP_APP_SOURCER_IMPSNT_SCRN_L term : inserted record successfully!");
                                                            dfd.resolve(resp);
                                                        },
                                                        function(tx,err){
                                                            dfd.resolve(null);
                                                        }
                                                    );
                                                }else
                                                    dfd.resolve(resp);
                                            },
                                            function(tx,err){
                                                dfd.resolve(null);
                                            }
                                        );
                                    },
                                    function(err){
                                        dfd.resolve(null);
                                    },null
                                );
                    }
                    else
        		    {
                        CommonService.transaction(db,
                                    function(tx){
                                        //CommonService.executeSql(tx,"UPDATE LP_APP_SOURCER_IMPSNT_SCRN_L SET POLICY_NO = ?, IMP_RM_CAMS_CD = ?, IMP_AGENT_CAMS_CD = ?, SOURCER_CODE = ?, SOURCER_NAME = ?, SOURCER_CONTACT_NO = ?, IMP_RM_NAME = ?, IMP_AGENT_NAME = ?, IRDA_LIC_NO = ?, IRDA_LIC_EXP_DT = ?, MOBILENO = ?, CHANNEL_NAME = ?, OFFICE_CODE = ?, SUBOFFICECODE = ?, OFFICE_NAME = ?, OFFICE_ADDRESS1 = ?, OFFICE_ADDRESS2 = ?, OFFICE_STATE = ?, OFFICE_ZIP_CODE = ?, OFFICE_OFFICE_TEL = ?, MODIFIED_DATE = ?, ISSYNCED = ?,AGENT_REL_FLAG = ? WHERE APPLICATION_ID = ? AND AGENT_CD = ?",[resp.RS.POLICY_NO,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N",'N',resp.RS.APPLICATION_ID,resp.RS.AGENT_CD],
                                          CommonService.executeSql(tx,"insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[resp.RS.APPLICATION_ID,resp.RS.AGENT_CD,resp.RS.POLICY_NO,resp.RS.SIS_ID,resp.RS.FHR_ID,null,null,resp.RS.RMC,/*resp.RS.SPC*/spds.SPData.IMP_AGENT_CAMS_CD,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N","N",null,null],
                                            function(tx,res){
                                                debug("LP_APP_SOURCER_IMPSNT_SCRN_L : updated record successfully!");
                                                if(!!resp.SPData && !!resp.SPData.OppData && !!resp.SPData.OppData.COMBO_ID && resp.SPData.OppData.COMBO_FLAG == 'Y'){
                                                    //CommonService.executeSql(tx,"UPDATE LP_APP_SOURCER_IMPSNT_SCRN_L SET POLICY_NO = ?, IMP_RM_CAMS_CD = ?, IMP_AGENT_CAMS_CD = ?, SOURCER_CODE = ?, SOURCER_NAME = ?, SOURCER_CONTACT_NO = ?, IMP_RM_NAME = ?, IMP_AGENT_NAME = ?, IRDA_LIC_NO = ?, IRDA_LIC_EXP_DT = ?, MOBILENO = ?, CHANNEL_NAME = ?, OFFICE_CODE = ?, SUBOFFICECODE = ?, OFFICE_NAME = ?, OFFICE_ADDRESS1 = ?, OFFICE_ADDRESS2 = ?, OFFICE_STATE = ?, OFFICE_ZIP_CODE = ?, OFFICE_OFFICE_TEL = ?, MODIFIED_DATE = ?, ISSYNCED = ?,AGENT_REL_FLAG = ? WHERE APPLICATION_ID = ? AND AGENT_CD = ?",[resp.RS.POLICY_NO,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N",'N',resp.RS.APPLICATION_ID,resp.RS.AGENT_CD],
                                                    CommonService.executeSql(tx,"insert or replace into LP_APP_SOURCER_IMPSNT_SCRN_L values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[resp.SPData.OppData.REF_APPLICATION_ID,resp.SPData.OppData.AGENT_CD,resp.SPData.OppData.REF_POLICY_NO,resp.SPData.OppData.REF_SIS_ID,resp.SPData.OppData.REF_FHRID,null,null,resp.RS.RMC,resp.RS.SPC,null,null,null,resp.RS.RMN,resp.RS.SPN,resp.RS.LICN,resp.RS.LICE + " 00:00:00",resp.RS.MOB,resp.RS.CHN,resp.RS.OFFC,resp.RS.SOC,resp.RS.OFFN,resp.RS.OFFA,resp.RS.OFFAA,resp.RS.OFFST,resp.RS.OFFPN,resp.RS.OFFTEL,CommonService.getCurrDate(),"N","N",null,null],
                                                        function(tx,res){
                                                            debug("LP_APP_SOURCER_IMPSNT_SCRN_L term : inserted record successfully!");
                                                            dfd.resolve(resp);
                                                        },
                                                        function(tx,err){
                                                            dfd.resolve(null);
                                                        }
                                                    );
                                                }else
                                                    dfd.resolve(resp);
                                            },
                                            function(tx,err){
                                                dfd.resolve(null);
                                            }
                                        );
                                    },
                                    function(err){
                                        dfd.resolve(null);
                                    },null
                                );
        		    }

        });

        return dfd.promise;
    };

	this.saveNoOtherAgentData = function(data){
		var dfd = $q.defer();
		debug("data.POLICY_NO :"+data.POLICY_NO);
		LoadApplicationScreenData.loadAgentImpData(data.APPLICATION_ID, data.AGENT_CD).then(function(AgentImpData){

		    if(!!AgentImpData && !!AgentImpData.APPLICATION_ID)
		    {
		        CommonService.transaction(db,
                        function(tx){
                             CommonService.executeSql(tx,"update LP_APP_SOURCER_IMPSNT_SCRN_L set SIS_ID = ?, FHR_ID = ?, POLICY_NO = ?, MODIFIED_DATE = ?, ISSYNCED = ?  where APPLICATION_ID = ? and AGENT_CD = ?",[data.SIS_ID, data.FHR_ID, data.POLICY_NO, CommonService.getCurrDate(), "N"],
                                                                            function(tx,res){
                                                                                debug("LP_APP_SOURCER_IMPSNT_SCRN_L :saveNoOtherAgentData");
                                                                                dfd.resolve(res);
                                                                            },
                                                                            function(tx,err){
                                                                                dfd.resolve(res);
                                                                            }
                                                                        );
                                    },function(err){
                                    dfd.resolve(null);
                                   },null);
		    }
		    else
		        CommonService.insertOrReplaceRecord(db, "lp_app_sourcer_impsnt_scrn_l", {"agent_cd": data.AGENT_CD, "application_id": data.APPLICATION_ID, "sis_id": data.SIS_ID, "fhr_id": data.FHR_ID, "policy_no": data.POLICY_NO, "modified_date": CommonService.getCurrDate(), "issynced": "N"}, true).then(
                			function(res){
                				dfd.resolve(!!res);
                			}
                		);

		});

		return dfd.promise;
	};

    this.getLoadData = function(APPLICATION_ID,AGENT_CD){
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select * from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID = ? and AGENT_CD = ?",[APPLICATION_ID,AGENT_CD],
                    function(tx,res){
                        debug("res.rows.length : " + res.rows.length);
                        if(!!res && res.rows.length>0)
                            dfd.resolve(res.rows.item(0));
                        else
                            dfd.resolve(null);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.updateSpData = function(SPData){
        var dfd = $q.defer();
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"update LP_APP_SOURCER_IMPSNT_SCRN_L set SOURCER_CODE = ? ,SOURCER_NAME = ?,SOURCER_CONTACT_NO = ? where (APPLICATION_ID = ?) and (AGENT_CD = ?)",[SPData.SOURCER_CODE,SPData.SOURCER_NAME,SPData.SOURCER_CONTACT_NO,SPData.APPLICATION_ID,SPData.AGENT_CD],
                    function(tx,res){
                        debug("LP_APP_SOURCER_IMPSNT_SCRN_L : " + 'Updated');
                        if(!!SPData.OppData && !!SPData.OppData.COMBO_ID && SPData.OppData.COMBO_FLAG == 'Y'){
                            CommonService.executeSql(tx,"update LP_APP_SOURCER_IMPSNT_SCRN_L set SOURCER_CODE = ? ,SOURCER_NAME = ?,SOURCER_CONTACT_NO = ? where (APPLICATION_ID = ?) and (AGENT_CD = ?)",[SPData.SOURCER_CODE,SPData.SOURCER_NAME,SPData.SOURCER_CONTACT_NO,SPData.OppData.REF_APPLICATION_ID,SPData.AGENT_CD],
                                function(tx,res){
                                    debug("LP_APP_SOURCER_IMPSNT_SCRN_L term : " + 'Updated');
                                    dfd.resolve(res);
                                },
                                function(tx,err){
                                    dfd.resolve(null);
                                }
                            );
                        }else
                            dfd.resolve(res);
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.getTermData = function(agentCd, fhrId, sisId, appId, polNo){
        var dfd = $q.defer();
        var oppData = {};
        var query = "select * from LP_MYOPPORTUNITY where AGENT_CD = ? and FHR_ID =? and SIS_ID = ? and APPLICATION_ID = ? and POLICY_NO = ? and RECO_PRODUCT_DEVIATION_FLAG = 'P'";
        var parameterList = [agentCd,fhrId, sisId, appId, polNo];
        debug("query : " + query);
        debug("parameterList : " + JSON.stringify(parameterList));
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,query,parameterList,
                    function(tx,res){
                        if(!!res && res.rows.length>0){
                            debug("LP_MYOPPORTUNITY: " + res.rows.length);
                            oppData = CommonService.resultSetToObject(res);
                            dfd.resolve(oppData);
                        }
                        else{
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            }
        );
        return dfd.promise;
    };
}]);
