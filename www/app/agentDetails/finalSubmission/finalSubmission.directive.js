finalSubmissionModule.directive('viewFinalOutput',['FinalSubmissionService', function (FinalSubmissionService) {
	"use strict";
    return {
		template: function(tElement, tAttrs) {
			console.log("inside directive")
			try{
					var data = FinalSubmissionService.getFileData();
					return data;
			}catch(e){debug(e.message);}
		}
	};
}]);