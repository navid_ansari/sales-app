finalSubmissionModule.controller('FinalSubmissionCtrl', ['$state','$stateParams','CommonService', 'FinalSubmissionService','AgentData','APPFileData','SISFileData','FFFileData','TermAPPFileData','TermSISFileData','TermFFFileData','CCRFileData','sisData','LoginService','oppParams','PscFlag', function($state, $stateParams, CommonService, FinalSubmissionService, AgentData,APPFileData,SISFileData,FFFileData,TermAPPFileData,TermSISFileData,TermFFFileData, CCRFileData, sisData,LoginService,oppParams,PscFlag){

var fsc = this;
debug(JSON.stringify(oppParams)+" :AgentData :"+JSON.stringify(AgentData));
debug("FinalSubmissionService.offlinePscPopup :"+FinalSubmissionService.offlinePscPopup);
/*if(FinalSubmissionService.offlinePscPopup == undefined && BUSINESS_TYPE != 'iWealth')
    FinalSubmissionService.offlinePscPopup = false;*/

fsc.click = false;
var params = {"FHR_ID":oppParams.FHR_ID, "OPP_ID":oppParams.OPP_ID,"SIS_ID":oppParams.SIS_ID, "APP_ID": oppParams.APPLICATION_ID,"FINAL_SUB":'Y',"REF_SIS_ID":oppParams.REF_SIS_ID,"REF_FHRID":oppParams.REF_FHRID,"REF_OPP_ID":oppParams.REF_OPP_ID,"COMBO_ID":oppParams.COMBO_ID,"REF_APPLICATION_ID":oppParams.REF_APPLICATION_ID};

FinalSubmissionService.Params = oppParams;
fsc.isAgentSigned = '';

this.isPscEnable = 'N';
this.isPscDone = false;
var PGL_ID = sisData.sisMainData.PGL_ID;
if(PscFlag.ISDIGITAL_PSC_DONE == 'Y')
{    this.disableOnSucc = true;
    FinalSubmissionService.offlinePscPopup = false;
}
else
    this.disableOnSucc = false;
if(PGL_ID == "185" || PGL_ID == "191"){
debug("isPscEnable - No - IRS");
		this.isPscEnable='N';
}
else if(!!PscFlag.DIGITAL_PSC_FLAG){
	if(PscFlag.DIGITAL_PSC_FLAG == 'M'){
	    this.offlinePscPopup=false;
	    FinalSubmissionService.offlinePscPopup = false;
	    debug("isPscEnable N - 1");
		this.isPscEnable='N';
	}else if(PscFlag.DIGITAL_PSC_FLAG == 'D'){
	    debug("isPscEnable D - 2");
		this.isPscEnable='Y';
		if(PscFlag.ISDIGITAL_PSC_DONE != 'Y')
		    {
				FinalSubmissionService.offlinePscPopup = true;
				fsc.disableBackground = 'Y'; // disable background button on popup
			}
	}
	else
	    debug("isPscEnable N - 3");

	   // this.isPscEnable="Y";
}
else{
	FinalSubmissionService.offlinePscPopup = true;
	fsc.disableBackground = 'Y'; // disable background button on popup
}



if(PGL_ID == "185" || PGL_ID == "191" /*|| BUSINESS_TYPE == 'iWealth'*/){
    this.isPscDone = true; debug("isPscDone - IRS/iWealth ");
}
else{
    if(!!PscFlag.ISDIGITAL_PSC_DONE && PscFlag.ISDIGITAL_PSC_DONE == 'Y')
        {this.isPscDone = true; debug("isPscDone True 1");}
    else if(PscFlag.DIGITAL_PSC_FLAG == 'M')
        {this.isPscDone = true; debug("isPscDone True 2");}
    else
        {this.isPscDone = false; debug("isPscDone False");}
}

this.callPscAPP = function(){

FinalSubmissionService.getProposerDetails($stateParams.APPLICATION_ID,$stateParams.AGENT_CD).then(
    function(propDet){
    console.log("propDet :"+JSON.stringify(propDet));
         FinalSubmissionService.getPSCData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,"C01",propDet).then(
                function(reqData){
                    if(reqData){
                        debug("<<<< REQ DATA IS >>>>>"+JSON.stringify(reqData));
                        fsc.callPSCApplication(reqData, $stateParams.APPLICATION_ID);
                    }
                }
            );

    }
);
}

//OfflinepscSubmit
this.offlinePscPopup=false;
this.pscSubmit=function(finalSub){
    fsc.click=true;
        console.log("PSC disableBackground:"+fsc.disableBackground+" Psc Call Time:" +this.PscTiming);
        console.log("Psc Pre Language"+this.PscLanguage);
        if(finalSub.$invalid != true){
            if(fsc.offlinePsc=='Y')
            FinalSubmissionService.updatePscOfflineData($stateParams.APPLICATION_ID,$stateParams.AGENT_CD,this.PscTiming,this.PscLanguage,fsc.offlinePsc).then(function(){
                fsc.isPscDone = true;
                console.log("Enabling Submit");

            });
			else
				fsc.disableBackground = 'N'; // disable background button on popup
            console.log("hide popup");
            this.offlinePscPopup=false;
            console.log("popup hidden");
      }
}

this.offlinePscPopup = FinalSubmissionService.offlinePscPopup;

//call PSC Application...
 this.callPSCApplication = function(finalReq, APP_ID){

 debug("Device Platform :"+device.platform);

 cordova.exec(
              function(succ){

              /*if( device.platform === 'iOS' || ionic.Platform.isIOS()){

                debug("Inside iOS device");

              }
              else{*/
                debug("Inside Android device");
              console.log(APP_ID+"RETURN from PSC:"+JSON.stringify(succ));
              try{
                  if(!!succ && !!succ.REQ && !!succ.REQ.STATUS)
                  {
                      if(!!succ.REQ.POLICY_NO){
                          for(var arr=0;arr<succ.REQ.POLICY_NO.length;arr++){

                              console.log("arr values" + succ.REQ.POLICY_NO[arr]);
                              FinalSubmissionService.updatePendingStatus($stateParams.AGENT_CD,succ.REQ.POLICY_NO[arr]);
                          }
                      }
                      console.log(APP_ID+" PSC Response ::"+JSON.stringify(succ));
                      if(succ.REQ.STATUS == '1'){
                      FinalSubmissionService.updatePSCData(APP_ID,finalReq.REQ.USERCODE).then(
                                                                                              function(updated){
                                                                                              if(updated){
                                                                                              fsc.isPscDone = true;
                                                                                              fsc.disableOnSucc = true;
                                                                                              navigator.notification.alert('PSC completed successfully !!',null, 'Success','Ok');
                                                                                              debug("UPDATE PSC DATA FLAG");
                                                                                              }
                                                                                              });
                      }
                      else
                      {
                          console.log("HERE - PSC not completed!");
                          //FinalSubmissionService.setStatus("0");
                          try{
                              //fsc.offlinePscPopup = true;
                              FinalSubmissionService.offlinePscPopup = true;
                              console.log("FSC ::"+JSON.stringify(fsc));
                              $state.reload();
                          }
                          catch(ex){
                            debug("exception ::"+ex.message);
                          }
                          console.log("Open Pop Up");
                      }
                  }
                  else
                    navigator.notification.alert('PSC Data not found',null, 'Error','Ok');

              }catch(e){
              debug("Excepting in PSC return:"+e.message);
              }

              //}
              },
              function(err){
              console.log("req error :"+err);
              navigator.notification.alert("PSC Application not Installed\nPlease Install PSC Application");
              },"PluginHandler","CallPSC",[JSON.stringify(finalReq)]
              );
 }


debug("Flag is"+JSON.stringify(PscFlag));

FinalSubmissionService.getAgentImpData(oppParams.APPLICATION_ID, oppParams.AGENT_CD).then(function(AgentData){
    debug("New Agent values ::"+JSON.stringify(AgentData));
    fsc.isAgentSigned = AgentData.AppMain.ISAGENT_SIGNED;
    FinalSubmissionService.AgentData = AgentData;

});
CommonService.hideLoading();

														 /* Header Hight Calculator */
														 setTimeout(function(){
																	var agentGetHeight = $(".agent-details .fixed-bar").height();
																	console.log("agentGetHeight : " + agentGetHeight);
																	$(".agent-details .custom-position").css({top:agentGetHeight + "px"});
																	});


//7th july 2016
var PGL_ID = '';
if(!!sisData)
	PGL_ID = sisData.sisMainData.PGL_ID;
if(BUSINESS_TYPE=="IndusSolution"){
	//PGL_ID = sisData.sisMainData.PGL_ID;

	this.text = "SP Sign";
}
else {
	this.text = "Agent Sign";
}
if(PGL_ID == "185" || PGL_ID == "191"){
    this.text = "Consent";
}
//7th july 2016

this.setAppFileData = function(){
    debug("inside setAppFileData");
    FinalSubmissionService.FINAL_OUTPUT = APPFileData;

}
this.setAppFileData();

this.setTermAppFileData = function(){
    debug("inside setTermAppFileData");
    FinalSubmissionService.TERM_FINAL_OUTPUT = TermAPPFileData;

}
if(!!oppParams.COMBO_ID)
    this.setTermAppFileData();

this.setSISFileData = function(){
    debug("inside setSISFileData");
    FinalSubmissionService.SIS_OUTPUT = SISFileData;
}
this.setSISFileData();

this.setTermSISFileData = function(){
    debug("inside setTermSISFileData");
    FinalSubmissionService.TERM_SIS_OUTPUT = TermSISFileData;
}
if(!!oppParams.COMBO_ID)
    this.setTermSISFileData();

this.setFFFileData = function(){
    debug("inside setFFFileData");
    FinalSubmissionService.FFNA_OUTPUT = FFFileData;
}
this.setFFFileData();

this.setTermFFFileData = function(){
    debug("inside setTermFFFileData");
    FinalSubmissionService.TERM_FFNA_OUTPUT = TermFFFileData;
}
if(!!oppParams.COMBO_ID)
    this.setTermFFFileData();

this.setCCRFileData = function(){
    debug("inside setCCRFileData"+CCRFileData);
    FinalSubmissionService.CCR_OUTPUT = CCRFileData;
}
this.setCCRFileData();

this.viewFinalReport = function(){

		if(!!oppParams.APPLICATION_ID){
			FinalSubmissionService.viewFinalReport(params, oppParams);

		}
	};

//7th july 2016
this.consentPopup = true;
this.saveAgentSignature = function(){

    if(PGL_ID == "191" || PGL_ID == "185"){
        this.consentPopup = !this.consentPopup;
    }else{
        CommonService.doSign("agentSign");
    }

}

//on click of submitt on Consent Popup
//7th july 2016
this.accept = function(finalSub){
	fsc.click = true;
	debug("Form invalid:"+finalSub.$invalid+" :: "+this.promoOff+"value selected is "+this.custConsent);
    if(finalSub.$invalid==false){
		this.consentPopup = true;
		FinalSubmissionService.replaceConsentData(this.custConsent, this.promoOff);
    }
}
//7th july 2016

this.startPolicySubmission = function(){
var EMPFlag = LoginService.lgnSrvObj.userinfo.EMPLOYEE_FLAG;
debug(EMPFlag+"AgentData.AppMain :"+JSON.stringify(AgentData.AppMain))
// IRS block
if(PGL_ID != "185" && PGL_ID != "191" && PGL_ID!=FG_PGL_ID && (((BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner') && PGL_ID != SIP_PGL_ID) || (BUSINESS_TYPE!='iWealth' && BUSINESS_TYPE!='GoodSolution' && BUSINESS_TYPE!='LifePlaner'))){
if(EMPFlag == 'E')
{
	navigator.notification.confirm("Your are not allowed to submit policy.",function(){CommonService.hideLoading();},"Trainer","Ok");
}
else if(AgentData.AppMain.ISSYNCED == 'Y' && !!AgentData.NEFTData && !!AgentData.NEFTData.NEFT_BANK_NAME)
{	if(AgentData.AppMain.ISAGENT_SIGNED == 'Y')
	{
		navigator.notification.confirm("Do you want to submit your policy ?",
					function(buttonIndex){
						if(buttonIndex=="1"){
							FinalSubmissionService.loadOppFlags(oppParams.APPLICATION_ID,oppParams.AGENT_CD,'app').then(function(oppData){
									//FHR DEV FLAG
									oppParams.RECO_DEV_FLAG = null;
									if(!!oppData)
										oppParams.RECO_DEV_FLAG = oppData.oppFlags.RECO_PRODUCT_DEVIATION_FLAG;
                        			var Params;
                        			console.log("COMBO POLICY ::"+JSON.stringify(oppData));
                        			if(oppData.oppFlags && !!oppData.oppFlags.COMBO_ID)
                        				Params = {"AGENT_CD":oppParams.AGENT_CD, "FHR_ID": oppParams.REF_FHRID, "FHR_ID":oppParams.REF_FHRID, "SIS_ID":oppParams.REF_SIS_ID, "APPLICATION_ID": oppParams.REF_APPLICATION_ID,"BASE_APP_ID":oppParams.APPLICATION_ID,"FINAL_SUB":'Y',"COMBO_FLAG":'T'};
                        			else
                        				Params = null;
                        			FinalSubmissionService.startPolicySubmission(oppParams, Params);
                        	});
						}
					},
					'Confirm Policy Submission',
					'Yes, No'
				);
	}
	else{
		if(BUSINESS_TYPE=="IndusSolution")
			navigator.notification.alert("Please complete SP signature",function(){CommonService.hideLoading();} ,"Application","OK");
		else
			navigator.notification.alert("Please complete the agent signature",function(){CommonService.hideLoading();} ,"Application","OK");
	}

}
else
	{
        if(AgentData.AppMain.ISSYNCED == 'E')
          navigator.notification.alert("You cannot submit this policy !!\nApplication Data entry was done during Training Role.\n Payment Data not available.",function(){CommonService.hideLoading();} ,"Trainee","OK");
        else
          navigator.notification.alert("Please complete Application Data Sync",function(){CommonService.hideLoading();} ,"Application","OK");
    }
}
else{
	if(PGL_ID==FG_PGL_ID || (PGL_ID==SIP_PGL_ID && (BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='GoodSolution' || BUSINESS_TYPE=='LifePlaner')))
		navigator.notification.alert("The sale of this product via tabsales has been discontinued.",null,"App Form","OK");
	else
		navigator.notification.alert("Please buy this product online - through WebSales.",null,"App Form","OK");
}
}

}]);


function updateiOSPSCData(succ){
    if(!!succ && !!succ.REQ && !!succ.REQ.STATUS)
    {
        if(!!succ.REQ.POLICY_NO){
            PSC_POL_ARRAY = succ.REQ.POLICY_NO;
            for(var arr=0;arr<succ.REQ.POLICY_NO.length;arr++){

                console.log("arr values" + succ.REQ.POLICY_NO[arr]);
                updateiOSPendingStatus(PSC_AGENT_CD,succ.REQ.POLICY_NO[arr]);
            }
        }
        if(succ.REQ.STATUS == '1')
            updateiOSPSCFlag();

    }
    else
        navigator.notification.alert('PSC Data not found',null, 'Error','Ok');
}

function updateiOSPendingStatus(AGENT_CD, POLICY_NO){
    debug("iOS Platform");
    iDB = window.sqlitePlugin.openDatabase({name: "SA_DB_MAIN.db", iosDatabaseLocation: 'Documents'});
    console.log("updateiOSPendingStatus started!!");

    db.transaction(function(tx){
                   tx.executeSql("update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE= ? where POLICY_NO= ?",['Y', POLICY_NO],function(tx, results){
                                 console.log("updateiOSPendingStatus done!!");

                                 },function(error){
                                    console.log("DB ERROR1"+error.message);
                                 });
                   },function(error){
                        console.log("DB ERROR"+error.message);
                   });

}

function updateiOSPSCFlag(){
    debug("iOS Platform");
    iDB = window.sqlitePlugin.openDatabase({name: "SA_DB_MAIN.db", iosDatabaseLocation: 'Documents'});
    db.transaction(function(tx){
                   tx.executeSql("update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE = ? where APPLICATION_ID = ? AND AGENT_CD = ?",['Y',PSC_APP_ID,PSC_AGENT_CD],function(tx, results){
                                 console.log("updateiOSPSCFlag started!!")

                                 },function(error){
                                 console.log("DB ERROR1"+error.message);
                                 });
                   },function(error){
                   console.log("DB ERROR"+error.message);
                   });

}

finalSubmissionModule.service('FinalSubmissionService',['$q', '$state', 'CommonService','ViewReportService','AppSyncService', 'LoginService', function($q, $state, CommonService, ViewReportService, AppSyncService, LoginService){

var finalSubSrv = this;
//finalSubSrv.FINAL_OUTPUT = "";
//finalSubSrv.SIS_OUTPUT = "";
//finalSubSrv.FFNA_OUTPUT = "";
//finalSubSrv.Params = "";

/* PSC RELATED */
this.getProposerDetails = function(APPLICATION_ID, AGENT_CD){
     var dfd = $q.defer();
        var query = "SELECT * from LP_APP_CONTACT_SCRN_A WHERE APPLICATION_ID=? AND AGENT_CD=? AND CUST_TYPE=?"
        var parameters = [APPLICATION_ID,AGENT_CD,'C02'];

        db.transaction(
            function(tx){
                tx.executeSql(query,parameters,
                              function(tx, res){
                              console.log("getProposerDetails :leng:"+res.rows.length);
                                if(res.rows.length!=0)
                                {
                                    console.log("res.rows.item(0) :"+res.rows.item(0).CUST_TYPE);
                                    dfd.resolve(res.rows.item(0));
                                }
                                else
                                    dfd.resolve(null);
                              },function(err){
                                    debug("error "+error.message);
                                    dfd.resolve(null);
                              });

                       },function(err){
                            debug("error "+error.message);
                            dfd.resolve(null);
                       });
    return dfd.promise;
}
this.getPSCData = function(APPLICATION_ID,AGENT_CD,custType,propDet){
    var dfd = $q.defer();
    var query = "SELECT DISTINCT SB.SIS_ID, SB.PLAN_NAME, SB.PLAN_CODE,SB.SUM_ASSURED,SB.POLICY_TERM,SB.PREMIUM_PAY_TERM,SB.PREMIUM_PAY_MODE_DESC,AM.APPLICATION_ID, AM.POLICY_NO, CNT.FIRST_NAME, CNT.LAST_NAME,CNT.EMAIL_ID AS EMAILID,CNT.MOBILE_NO,CNT.BIRTH_DATE,CNT.COMMUNICATE_ADD_FLAG, CNT.CURR_ADD_LINE1, CNT.CURR_ADD_LINE2, CNT.CURR_ADD_LINE3, CNT.CURR_DISTRICT_LANDMARK,CNT.CURR_PIN,AM.ANNUAL_PREMIUM_AMOUNT, AM.MODEL_PREMIUM_AMOUNT,NEFT.NEFT_BANK_NAME,AGENT.AGENT_NAME,AGENT.MOBILENO AS USERCONNO,AGENT.EMAIL_ID AS EMAIL, AGENT.CHANNEL_NAME FROM LP_SIS_SCREEN_B AS SB INNER JOIN LP_APPLICATION_MAIN AS AM ON SB.SIS_ID==AM.SIS_ID, LP_APPLICATION_MAIN INNER JOIN LP_APP_CONTACT_SCRN_A AS CNT ON AM.APPLICATION_ID==CNT.APPLICATION_ID, LP_APPLICATION_MAIN INNER JOIN LP_APP_PAY_NEFT_SCRN_H AS NEFT ON AM.APPLICATION_ID==NEFT.APPLICATION_ID,LP_APPLICATION_MAIN AS INNER JOIN LP_USERINFO AS AGENT ON AM.AGENT_CD==AGENT.AGENT_CD WHERE AM.APPLICATION_ID=? AND AM.AGENT_CD=? AND CNT.CUST_TYPE=?"
    var parameters = [APPLICATION_ID,AGENT_CD,custType];

    db.transaction(
        function(tx){
            tx.executeSql(query,parameters,
                          function(tx,success){
                                if(!!success && success.rows.length > 0){
                                    var res = success.rows.item(0);
                                    debug("<<<<<<<PSC DATA SUCCESS>>>>>>"+JSON.stringify(res));
                                    var sessionID = finalSubSrv.generatePSCSessionId();
                                    debug("<<<<SESSSION ID>>>>"+sessionID);
                                    finalSubSrv.getSisRelatedData(AGENT_CD,res.SIS_ID).then(
                                        function(sisData){
                                            debug("<<<<<<<SIS DATA SUCCESS>>>>>>"+JSON.stringify(sisData));
                                            var finalReq = finalSubSrv.createRequestForPSC(res,sisData,sessionID,APPLICATION_ID,AGENT_CD,propDet);
                                            finalSubSrv.updateUserInfo(sessionID,AGENT_CD).then(
                                                function(updated){
                                                    if(updated){
                                                        debug("<<<<<<< CALL PSC APPLICATION >>>>>>>");
                                                        dfd.resolve(finalReq);
                                                    }
                                                }
                                            )
                                        }
                                    );
                                }
                          },
                          function(tx,error){
                                debug("error "+error.message);
                                dfd.resolve(null);
                          }
            )
        }
    )

    return dfd.promise;
};

this.updateUserInfo = function(sessionID,AGENT_CD){
    var dfd = $q.defer();
    var query = "update LP_USERINFO set PSC_SESSION_ID = ? where AGENT_CD = ?";
    var parameters = [sessionID,AGENT_CD];
    db.transaction(function(tx){
        tx.executeSql(query,parameters,
        function(tx,success){
            dfd.resolve(true);
        },
        function(tx,err){
            debug("fail to insert PSC data");
            dfd.resolve(false);
        }
        )
    })
    return dfd.promise;
}

this.generatePSCSessionId = function(){

     var currentdate = new Date();
     var sessionID = "A" + currentdate.getDate() +
                        + (currentdate.getMonth()+1)  +
                        + currentdate.getFullYear() +
                        + currentdate.getHours() +
                        + currentdate.getMinutes() +
                        + currentdate.getSeconds();
     return sessionID;
}

this.getSisRelatedData = function(AGENT_CD,SIS_ID){
    var dfd = $q.defer();
    //PSC change
    var query = "select SIS.PGL_ID, PD.PRODUCT_UINNO, PD.PRODUCT_SOLUTION,PD.PRODUCT_TYPE from LP_SIS_MAIN AS SIS INNER JOIN LP_PRODUCT_MASTER AS PD ON SIS.PGL_ID==PD.PRODUCT_PGL_ID where SIS.SIS_ID = ? AND SIS.AGENT_CD = ?";
    var parameters = [SIS_ID,AGENT_CD];

    db.transaction(function(tx){
        tx.executeSql(query,parameters,
        function(tx,success){
            debug("sis data success"+JSON.stringify(success.rows.item(0)));
            var res = success;
            if(!!res && res.rows.length > 0)
                dfd.resolve(res.rows.item(0));
            else
                dfd.resolve(null);
        },
        function(tx,err){
            debug("sis data failed");
            dfd.resolve(null);
        }
        )
    });

    return dfd.promise;
};

this.createRequestForPSC = function(r,sisData,sessionID,APPLICATION_ID,AGENT_CD,propDet){
    var pscReqData = {};
    pscReqData.REQ = {};
    pscReqData.REQ.APPNO = r.POLICY_NO;//APPLICATION_ID;
    pscReqData.REQ.PRONAME = r.FIRST_NAME + ' ' + r.LAST_NAME;
    if(r.EMAILID == undefined || r.MOBILE_NO == undefined || r.EMAILID == "" || r.MOBILE_NO == "")
    {
        pscReqData.REQ.EMAILID = propDet.EMAIL_ID;
        pscReqData.REQ.MONO = propDet.MOBILE_NO;
    }
    else
    {
        pscReqData.REQ.EMAILID = r.EMAILID;
        pscReqData.REQ.MONO = r.MOBILE_NO;
    }
    pscReqData.REQ.COMADD = r.CURR_ADD_LINE1;
    if(r.CURR_ADD_LINE2!=undefined)
    {
        pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ r.CURR_ADD_LINE2;
        if(r.CURR_ADD_LINE3!=undefined)
            pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ r.CURR_ADD_LINE3;
        else
            pscReqData.REQ.COMADD = ((!!r.CURR_DISTRICT_LANDMARK) ? r.CURR_DISTRICT_LANDMARK : "" ) +" "+ r.CURR_PIN;
    }
    else
        pscReqData.REQ.COMADD = pscReqData.REQ.COMADD +" "+ ((!!r.CURR_DISTRICT_LANDMARK) ? r.CURR_DISTRICT_LANDMARK : "" ) +" "+ r.CURR_PIN;

    pscReqData.REQ.PLANNAME = r.PLAN_NAME;
    pscReqData.REQ.PLANTYPE = sisData.PRODUCT_SOLUTION;
    pscReqData.REQ.PTYPE = sisData.PRODUCT_TYPE;
    pscReqData.REQ.PREAMT = r.ANNUAL_PREMIUM_AMOUNT;
    pscReqData.REQ.FREQ = r.PREMIUM_PAY_MODE_DESC; //not
    pscReqData.REQ.POLITERM = r.POLICY_TERM;
    pscReqData.REQ.PRETEAMPAY = r.PREMIUM_PAY_TERM;
    pscReqData.REQ.SUMASSURED = r.SUM_ASSURED;
    pscReqData.REQ.MATU4 = "";
    pscReqData.REQ.MATU8 = "";
    pscReqData.REQ.MATULIP = "";
    var db = r.BIRTH_DATE;
    pscReqData.REQ.DOB = db.substring(0,10);
    pscReqData.REQ.ANNUOPT = "";
    pscReqData.REQ.ANNUAMT = "";
    pscReqData.REQ.ANNUFREQ = "";
    pscReqData.REQ.BRONAME = "";
    pscReqData.REQ.BANKNAME = r.NEFT_BANK_NAME;
    pscReqData.REQ.MODFRE = r.PREMIUM_PAY_MODE_DESC; // Added in new string
    pscReqData.REQ.MODPRE = r.MODEL_PREMIUM_AMOUNT; // Added in new string
    pscReqData.REQ.USERID = AGENT_CD;     // Not in new string
    pscReqData.REQ.PROID = r.PLAN_CODE; //sisData.PRODUCT_UINNO;
    pscReqData.REQ.USERCODE = AGENT_CD;
    pscReqData.REQ.USERNAME = r.AGENT_NAME;
    pscReqData.REQ.PASSWORD = LoginService.lgnSrvObj.password;
    pscReqData.REQ.USERCONNO = r.USERCONNO;
    pscReqData.REQ.EMAIL = r.EMAIL;
    pscReqData.REQ.USERDEG = r.USER_DESIGNATION;
    pscReqData.REQ.USERSTAT = "success";
    pscReqData.REQ.USERTYPE = "";
    if(BUSINESS_TYPE == 'iWealth')
        pscReqData.REQ.CHANID = 'Agency-DSF';
    else if(BUSINESS_TYPE == 'IndusSolution')
            pscReqData.REQ.CHANID = 'BANCA';
         else
            pscReqData.REQ.CHANID = 'AGENCY';
    pscReqData.REQ.SESSION = sessionID;
    pscReqData.REQ.SAVEFLAG = "";
    pscReqData.REQ.IMAGEOFUSER = "";
    console.log("DATA TO PSC :"+JSON.stringify(pscReqData));
    return pscReqData;
};

this.updatePSCData = function(APPLICATION_ID,AGENT_CD){
    var dfd = $q.defer();
    var query = "update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE = ? where APPLICATION_ID = ? AND AGENT_CD = ?";
    var parameters = ["Y",APPLICATION_ID,AGENT_CD];

    db.transaction(function(tx){
        tx.executeSql(query,parameters,
            function(tx,success){
                dfd.resolve(true);
            },
            function(tx,err){
                debug("fail to insert PSC data");
                dfd.resolve(false);
            }
        )
    })

    return dfd.promise;
}

this.updatePscOfflineData=function(APPLICATION_ID,AGENT_CD,PscTiming,PscLanguage,offlinePsc){
    var dfd=$q.defer();
    var query="update LP_APP_SOURCER_IMPSNT_SCRN_L set PREFERRED_CALL_TIME= ?,PREFERRED_LANGUAGE_CD= ?, DIGITAL_PSC_FLAG= ?,ISDIGITAL_PSC_DONE= ? where APPLICATION_ID=? and AGENT_CD=?";
    var params="";
    if(offlinePsc=="Y"){
    params=[PscTiming,PscLanguage,"M","Y",APPLICATION_ID,AGENT_CD];
    }
    db.transaction(function(tx){
        tx.executeSql(query,params,
        function(tx,res){
            console.log("updated Successfully! "+ res);
            console.log("pscTiming:"+ PscTiming);
            console.log("pscLanguage:"+ PscLanguage);
            dfd.resolve("success");
        },
        function(tx,err){
            console.log("Update Unsuccessfull"+ err.message);
         }
         );

    },
    function(err){
        console.log("error while updating");
    }
    );
    return dfd.promise;
}
/*Pending status*/
this.updatePendingStatus=function(AGENT_CD,PolicyNo){
    var dfd=$q.defer();
    var query="update LP_APP_SOURCER_IMPSNT_SCRN_L set ISDIGITAL_PSC_DONE= ? where POLICY_NO= ?";
    var parameter=["Y",PolicyNo];
    db.transaction(function(tx){
        tx.executeSql(query,parameter,
        function(tx,res){
        console.log("updated Successfully! "+ JSON.stringify(res));
        console.log("Policy No" + PolicyNo);
        dfd.resolve("success");
        },
        function(tx,err){
        console.log("Updated"+ err.message);
        }
        );
    },
    function(err){
        console.log("error while updating policyNo");
    }
    );
    return dfd.promise;
}

/*Psc offline popup data flag*/
this.getPscOfflineData=function(APPLICATION_ID,AGENT_CD){
    var dfd=$q.defer();
    var query="select PREFERRED_CALL_TIME,PREFERRED_LANGUAGE_CD,DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID=? and AGENT_CD=?";
    var parameter=[APPLICATION_ID,AGENT_CD];
    var pscObj={};
    db.transaction(function(tx){
        tx.executeSql(query,parameter,
        function(tx,success){
        if(success.rows.length!=0){
        debug("success fully selected Psc Offline Data" + JSON.stringify(success.rows.item(0)));
        pscObj.PREFERRED_CALL_TIME=success.rows.item(0).PREFERRED_CALL_TIME;
        pscObj.PREFERRED_LANGUAGE_CD=success.rows.item(0).PREFERRED_LANGUAGE_CD;
        pscObj.DIGITAL_PSC_FLAG=success.rows.item(0).DIGITAL_PSC_FLAG;
        pscObj.ISDIGITAL_PSC_DONE=success.rows.item(0).ISDIGITAL_PSC_DONE;
        dfd.resolve(pscObj);
        }
            else
                dfd.resolve(null);
    },
    function(tx,err){
        debug("fail to insert PSC Offline Data");
        dfd.resolve(null);
    }
    )
   })
   return dfd.promise;
}

/* PSC RELATED*/
this.getPscFlag=function(APPLICATION_ID,AGENT_CD){
    var dfd = $q.defer();
    var PscFlag = {};
    var query="select DIGITAL_PSC_FLAG,ISDIGITAL_PSC_DONE from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID=? and AGENT_CD=?";
    var parameters=[APPLICATION_ID,AGENT_CD];

    db.transaction(function(tx){
        tx.executeSql(query,parameters,
        function(tx,success){
           PscFlag = CommonService.resultSetToObject(success);
           dfd.resolve(PscFlag);
        },
        function(tx,err){
            debug("fail to insert PSC data");
            dfd.resolve(null);
        }
        )
    })
    return dfd.promise;
};
//replace consent data into html..
//7th july 2016
this.loadOppFlags = function(mainId, AGENT_CD, type, flag){

        var dfd = $q.defer();
        var OppData = {}
		var whereClauseObj = {};

				if(type == 'opp')
        		    whereClauseObj.OPPORTUNITY_ID = mainId;
        		else if(type == 'combo')
        		    {
        		        whereClauseObj.COMBO_ID = mainId;
        		        if(!!flag)
        		            whereClauseObj.RECO_PRODUCT_DEVIATION_FLAG = flag;
        		    }
        		else if(type == 'app')
						 whereClauseObj.APPLICATION_ID = mainId;
					else
						 whereClauseObj.FHR_ID = mainId;
        		whereClauseObj.AGENT_CD = AGENT_CD;

		console.log(JSON.stringify(whereClauseObj));
		CommonService.selectRecords(db,"LP_MYOPPORTUNITY",false,"*",whereClauseObj, "").then(
			function(res){
				console.log("LP_MYOPPORTUNITY FLAGS :" + res.rows.length);
				if(!!res && res.rows.length>0){

                    OppData.oppFlags = CommonService.resultSetToObject(res);

                    dfd.resolve(OppData);
				}
				else
					dfd.resolve(OppData);
			}
		);

		return dfd.promise;
}


this.replaceConsentData = function(flag, promoFlag){
    finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,'||APP_TNC||',flag);
    finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,'||PROMO_OFFER||',promoFlag);

    debug("Data is >>"+finalSubSrv.FINAL_OUTPUT);
    debug("replaced consent data ....");
    var ffOutput = finalSubSrv.FFNA_OUTPUT;
    ffOutput = CommonService.replaceAll(ffOutput,"||NAME_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
    ffOutput = CommonService.replaceAll(ffOutput,"||CODE_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
    ffOutput = CommonService.replaceAll(ffOutput,"||PROPOSAL_NO||",finalSubSrv.Params.POLICY_NO);
    finalSubSrv.FFNA_OUTPUT = ffOutput;
    debug("replaced FFNA data ....");

    CommonService.saveFile(finalSubSrv.Params.APPLICATION_ID,".html","/FromClient/APP/HTML/",finalSubSrv.FINAL_OUTPUT,"N").then(function(){
                                                     console.log("APP with Consent Data saved");
                                                     });
    CommonService.saveFile(finalSubSrv.Params.FHR_ID,".html","/FromClient/FF/HTML/",finalSubSrv.FFNA_OUTPUT,"N").then(function(){
                                              console.log("FHR with Consent Data saved");
                                             });

    var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = finalSubSrv.Params.APPLICATION_ID;
    whereClauseObj.AGENT_CD = finalSubSrv.Params.AGENT_CD;

    CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISAGENT_SIGNED" : 'Y'}, whereClauseObj).then(
                                        function(res){
                                            console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                                        });
    $state.reload();
    debug("Consent Data DONE");

}
//7th july 2016

this.getAgentImpData = function(APP_ID, AGENT_CD){
var dfd = $q.defer();
var AgentData = {};
var whereClauseObj = {};
    whereClauseObj.APPLICATION_ID = APP_ID;
    whereClauseObj.AGENT_CD = AGENT_CD;

var query = "SELECT * from LP_DOCUMENT_UPLOAD WHERE DOC_ID = (SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE CUSTOMER_CATEGORY=? AND MPDOC_CODE=? AND MPPROOF_CODE=?) AND DOC_CAT_ID = ? AND AGENT_CD = ?";
var parameterList = ["AG","AR","CC", APP_ID, AGENT_CD];


CommonService.transaction(db,
							function(tx){
								CommonService.executeSql(tx, query, parameterList,
									function(tx,res){
									debug(JSON.stringify(parameterList)+" :: "+res.rows.length);
									if(!!res && res.rows.length>0)
									    AgentData.DocUploadData = CommonService.resultSetToObject(res);
									else
									    AgentData.DocUploadData = null;
									CommonService.selectRecords(db,"LP_APP_SOURCER_IMPSNT_SCRN_L",false,"*",whereClauseObj, "").then(
                                    			function(res){
                                    				if(!!res && res.rows.length>0){
                                    				 AgentData.ImpData = CommonService.resultSetToObject(res);
                                    				}
                                    				else
                                    				    {
                                    				        AgentData.ImpData = null;
                                    				    }
                                    CommonService.selectRecords(db,"LP_APPLICATION_MAIN",false,"*",whereClauseObj, "").then(
                                                function(res){
                                                    if(!!res && res.rows.length>0){
                                                    AgentData.AppMain = CommonService.resultSetToObject(res);
                                                      //  dfd.resolve(AgentData)
                                                    }
                                                    else
                                                        {
                                                            AgentData.AppMain = null;
                                                          //  dfd.resolve(AgentData);
                                                        }
                                                }
                                            );
                                            CommonService.selectRecords(db,"LP_APP_PAY_NEFT_SCRN_H",false,"*",whereClauseObj, "").then(
                                                             function(res){
                                                                  if(!!res && res.rows.length>0){
                                                                    AgentData.NEFTData = CommonService.resultSetToObject(res);
                                                                    dfd.resolve(AgentData)
                                                                  }
                                                                  else
                                                                  {
                                                                    AgentData.NEFTData = null;
                                                                    dfd.resolve(AgentData);
                                                                  }
                                                                });
                                                              }
                                                            );
									},
									function(tx,err){
										dfd.resolve(null);
									}
								);
							},
							function(err){
								dfd.resolve(null);
							}
						);

return dfd.promise;

}


this.loadCCRReport = function(params){
var dfd = $q.defer();
debug("inside loadCCRReport");

var query = "SELECT * from LP_DOCUMENT_UPLOAD WHERE DOC_ID = (SELECT DOC_ID FROM LP_DOC_PROOF_MASTER WHERE CUSTOMER_CATEGORY=? AND MPDOC_CODE=? AND MPPROOF_CODE=?) AND DOC_CAT_ID = ? AND AGENT_CD = ?";
var parameterList = ["AG","AR","CC", params.APPLICATION_ID, params.AGENT_CD];
var fileName = params.AGENT_CD+"_"+params.APPLICATION_ID;

debug(fileName+": CCRReport :1:"+JSON.stringify(params));

CommonService.transaction(db,function(tx){
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){

						if(!!res && res.rows.length>0)
						{
							fileName = fileName + "_" + res.rows.item(0).DOC_ID;
							finalSubSrv.CCR_DOC_ID = res.rows.item(0).DOC_ID;
							debug(fileName+"inside CCRReport"+JSON.stringify(params))
							CommonService.readFile(fileName + ".html", "/FromClient/APP/HTML/").then(
										function(res){
											if(!!res){
													finalSubSrv.CCR_OUTPUT = res;
													debug("CCR HTML :"+res);
													dfd.resolve(res);
											}
											else
												dfd.resolve(null);

										}
									);
						}
						else
							dfd.resolve(null);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
return dfd.promise;
}

this.loadFFNReport = function(params){
var dfd = $q.defer();
debug("inside FFReport"+JSON.stringify(params))
	CommonService.readFile(params.FHR_ID + ".html", "/FromClient/FF/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.FFNA_OUTPUT = res;
							//debug("FF HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;

}

this.loadTermFFNReport = function(params){
var dfd = $q.defer();
debug("inside Term FFReport"+JSON.stringify(params))
	CommonService.readFile(params.REF_FHRID + ".html", "/FromClient/FF/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.TERM_FFNA_OUTPUT = res;
							//debug("FF HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;

}

this.loadSISReport = function(params){
var dfd = $q.defer();
debug("inside SISReport"+JSON.stringify(params))
	CommonService.readFile(params.SIS_ID + ".html", "/FromClient/SIS/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.SIS_OUTPUT = res;
							//debug("SIS HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					debug("SIS file not found");
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;

}

this.loadTermSISReport = function(params){
var dfd = $q.defer();
debug("inside Term SISReport"+JSON.stringify(params))
	CommonService.readFile(params.REF_SIS_ID + ".html", "/FromClient/SIS/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.TERM_SIS_OUTPUT = res;
							//debug("SIS HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					debug("SIS file not found");
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;

}

this.viewFinalReport = function(params){
var dfd = $q.defer();
debug("inside viewFinalReport"+JSON.stringify(params))
	CommonService.readFile(params.APPLICATION_ID + ".html", "/FromClient/APP/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.FINAL_OUTPUT = res;
							debug("HTML :");//+res
							dfd.resolve(res);
    				}
    				else{
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;
}


this.viewTermFinalReport = function(params){
var dfd = $q.defer();
debug("params.APPLICATION_ID:"+params.APPLICATION_ID+" inside viewTermFinalReport:"+JSON.stringify(params))
	CommonService.readFile(params.REF_APPLICATION_ID + ".html", "/FromClient/APP/HTML/").then(
    			function(res){
    				if(!!res){
							finalSubSrv.TERM_FINAL_OUTPUT = res;
							debug("TERM HTML :"+res);
							dfd.resolve(res);
    				}
    				else{
    					dfd.resolve(null);
    				}
    			}
    		);
return dfd.promise;
}

this.getFileData = function(){

return finalSubSrv.FINAL_OUTPUT;

}

this.saveAgentSignature = function(sign){
	CommonService.showLoading();
	console.log(finalSubSrv.AgentData.DocUploadData.DOC_ID+" :: "+JSON.stringify(finalSubSrv.Params)+"Agent Sign :"+sign);

	var fileName = finalSubSrv.Params.AGENT_CD+"_"+finalSubSrv.Params.APPLICATION_ID+"_"+finalSubSrv.AgentData.DocUploadData.DOC_ID;


	var signFileName = finalSubSrv.Params.AGENT_CD+"_"+finalSubSrv.Params.APPLICATION_ID;

	var whereClauseObj = {};
		whereClauseObj.APPLICATION_ID = finalSubSrv.Params.APPLICATION_ID;
		whereClauseObj.AGENT_CD = finalSubSrv.Params.AGENT_CD;

	    var whereClauseObj2 = {};
    		whereClauseObj2.SIS_ID = finalSubSrv.Params.SIS_ID;
    		whereClauseObj2.AGENT_CD = finalSubSrv.Params.AGENT_CD;
	var doc_cap = CommonService.getRandomNumber();
    //Save Sign File
	var saveSign = sign.replace("data:image/jpeg;base64,","");
	CommonService.saveFile(signFileName,".jpg","/FromClient/APP/SIGNATURE/",saveSign,"N").then(function(){
        console.log("Agent Sign Success");
        finalSubSrv.getSignDocID().then(function(SignDocID){
                var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                var parameterList = [SignDocID,finalSubSrv.Params.AGENT_CD,"APP",finalSubSrv.Params.APPLICATION_ID,finalSubSrv.Params.POLICY_NO,signFileName+".jpg",CommonService.getCurrDate(),"0","N","N",doc_cap];
                CommonService.transaction(db, function(tx){
                    CommonService.executeSql(tx, query, parameterList, function(tx,res){
                                finalSubSrv.insertTermData(SignDocID,doc_cap);
                               debug("Success AGENT Doc")

                    }, function(tx, err){console.log("Error 1");});
                    },function(err){});
            });

            //Replace Sign and agent Data
            if(BUSINESS_TYPE=="IndusSolution")
                CommonService.showLoading("Saving SP Signature...");
            else {
                CommonService.showLoading("Saving Agent Signature...");
            }
            debug("REPLACING SIGN ");
            var FINAL_OUTPUT = finalSubSrv.replaceForAppHTML(sign);
            var TERM_FINAL_OUTPUT = '';
            if(!!finalSubSrv.Params.COMBO_ID)
                TERM_FINAL_OUTPUT = finalSubSrv.replaceForTermAppHTML(sign);
            debug("FINAL_OUTPUT COMPLTE");


            CommonService.saveFile(finalSubSrv.Params.APPLICATION_ID,".html","/FromClient/APP/HTML/",finalSubSrv.FINAL_OUTPUT,"N").then(function(){
                      console.log("APP with Agent Sign Success"+finalSubSrv.CCR_OUTPUT);

                        if(!!finalSubSrv.Params.COMBO_ID)
                            finalSubSrv.saveTermAppFile();
                    var CCR_OUTPUT =  finalSubSrv.replaceForCCRHTML(sign);
                    debug("CCR COMPLTE");
                    debug("CCR OUTput is : "+finalSubSrv.CCR_OUTPUT);
                    if(!!finalSubSrv.CCR_OUTPUT){
                            CommonService.saveFile(fileName,".html","/FromClient/APP/HTML/",finalSubSrv.CCR_OUTPUT,"N").then(function(){
                                      console.log("CCR with Agent Sign Success");
                                      var SIS_OUTPUT = finalSubSrv.replaceForSISHTML(sign);
                                      var TERM_SIS_OUTPUT = '';
                                      if(!!finalSubSrv.Params.COMBO_ID)
                                          TERM_SIS_OUTPUT = finalSubSrv.replaceForTermSISHTML(sign);
                                          debug("SIS COMPLTE");
                                          CommonService.saveFile(finalSubSrv.Params.SIS_ID,".html","/FromClient/SIS/HTML/",finalSubSrv.SIS_OUTPUT,"N").then(function(){
                                                  console.log("SIS with Agent Sign Success");
                                                  if(!!finalSubSrv.Params.COMBO_ID)
                                                         finalSubSrv.saveTermSISFile();
                                              if(BUSINESS_TYPE=='IndusSolution'){
                                                  var FFNA_OUTPUT = finalSubSrv.replaceForFFNHTML(sign);

                                                  var TERM_FFNA_OUTPUT = '';

                                              if(!!finalSubSrv.Params.COMBO_ID)
                                                  finalSubSrv.replaceForTermFFNHTML(sign).then(function(TermFFData){
                                                    debug("replaceForTermFFNHTML :"+TermFFData);
                                                    TERM_FFNA_OUTPUT = TermFFData;
                                                    finalSubSrv.saveTermFHRFile();
                                                  });

                                              debug("FFN COMPLTE");

							CommonService.saveFile(finalSubSrv.Params.FHR_ID,".html","/FromClient/FF/HTML/",finalSubSrv.FFNA_OUTPUT,"N").then(function(){

                                              console.log("FHR with Agent Sign Success");

                                              /*if(!!finalSubSrv.Params.COMBO_ID)
                                                  finalSubSrv.saveTermFHRFile();*/

                                                  CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISAGENT_SIGNED" : 'Y'}, whereClauseObj).then(
                                                      function(res){
                                                          console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                                                          CommonService.hideLoading();
                                                          $state.reload();
                                                      });
                                              });

                                              }
                                              else{
                                                  CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISAGENT_SIGNED" : 'Y'}, whereClauseObj).then(
                                                  function(res){
                                                      console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                                                      CommonService.hideLoading();
                                                      $state.reload();
                                                  }
                                              );
                                              }
                                          });
                            });
                    }else{
                    	  var SIS_OUTPUT = finalSubSrv.replaceForSISHTML(sign);
						  var TERM_SIS_OUTPUT = '';
						  if(!!finalSubSrv.Params.COMBO_ID)
							  TERM_SIS_OUTPUT = finalSubSrv.replaceForTermSISHTML(sign);
							  debug("SIS COMPLTE");
                         CommonService.saveFile(finalSubSrv.Params.SIS_ID,".html","/FromClient/SIS/HTML/",finalSubSrv.SIS_OUTPUT,"N").then(function(){
                                                                      console.log("SIS with Agent Sign Success");
                                                                      if(!!finalSubSrv.Params.COMBO_ID)
                                                                        finalSubSrv.saveTermSISFile();
                                                                  if(BUSINESS_TYPE=='IndusSolution'){
                                                                      var FFNA_OUTPUT = finalSubSrv.replaceForFFNHTML(sign);

                                                                      var TERM_FFNA_OUTPUT = '';

                                                                  if(!!finalSubSrv.Params.COMBO_ID)
                                                                      finalSubSrv.replaceForTermFFNHTML(sign).then(function(TermFFData){
                                                                        debug("replaceForTermFFNHTML :2:"+TermFFData);
                                                                        TERM_FFNA_OUTPUT = TermFFData;
                                                                        finalSubSrv.saveTermFHRFile();
                                                                      });

                                                                  debug("FFN COMPLTE");

                                                                  CommonService.saveFile(finalSubSrv.Params.FHR_ID,".html","/FromClient/FF/HTML/",finalSubSrv.FFNA_OUTPUT,"N").then(function(){

                                                                  console.log("FHR with Agent Sign Success");

                                                                  /*if(!!finalSubSrv.Params.COMBO_ID)
                                                                      finalSubSrv.saveTermFHRFile();*/

                                                                      CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISAGENT_SIGNED" : 'Y'}, whereClauseObj).then(
                                                                          function(res){
                                                                              console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                                                                              CommonService.hideLoading();
                                                                              $state.reload();
                                                                          });
                                                                  });

                                                                  }
                                                                  else{
                                                                      CommonService.updateRecords(db, 'LP_APPLICATION_MAIN',{"ISAGENT_SIGNED" : 'Y'}, whereClauseObj).then(
                                                                      function(res){
                                                                          console.log("ISAGENT_SIGNED update success !!"+JSON.stringify(whereClauseObj));
                                                                          CommonService.hideLoading();
                                                                          $state.reload();
                                                                      }
                                                                  );
                                                                  }
                                                              });

                    }
			});

});



}


this.insertTermData = function(SignDocID, ref_doc_cap){

	finalSubSrv.loadOppFlags(finalSubSrv.Params.COMBO_ID,finalSubSrv.Params.AGENT_CD,'combo','P').then(function(oppData){

	var signFileName = finalSubSrv.Params.AGENT_CD+"_"+finalSubSrv.Params.REF_APPLICATION_ID;
	var doc_cap = CommonService.getRandomNumber();
	var query = "INSERT OR IGNORE INTO LP_DOCUMENT_UPLOAD (DOC_ID,AGENT_CD,DOC_CAT,DOC_CAT_ID,POLICY_NO,DOC_NAME,DOC_TIMESTAMP,DOC_PAGE_NO,IS_FILE_SYNCED,IS_DATA_SYNCED,DOC_CAP_ID,DOC_CAP_REF_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
	var parameterList = [SignDocID,finalSubSrv.Params.AGENT_CD,"APP",finalSubSrv.Params.REF_APPLICATION_ID,finalSubSrv.Params.REF_POLICY_NO,signFileName+".jpg",CommonService.getCurrDate(),"0","Y","N",doc_cap,ref_doc_cap];
	CommonService.transaction(db, function(tx){
		CommonService.executeSql(tx, query, parameterList, function(tx,res){
				   debug("Success Term AGENT Doc");

		}, function(tx, err){console.log("Error 1");});
	},function(err){});

	});

}

this.saveTermAppFile = function(){
if(!!finalSubSrv.TERM_FINAL_OUTPUT){
console.log("finalSubSrv.Params.APP_ID:"+finalSubSrv.Params.APPLICATION_ID+"finalSubSrv.Params.REF_APPLICATION_ID :"+finalSubSrv.Params.REF_APPLICATION_ID);
CommonService.saveFile(finalSubSrv.Params.REF_APPLICATION_ID,".html","/FromClient/APP/HTML/",finalSubSrv.TERM_FINAL_OUTPUT,"N").then(function(){
              console.log("TERM APP with Agent Sign Success");


			 });
}


}

this.saveTermSISFile = function(){
console.log("finalSubSrv.Params.REF_SIS_ID"+finalSubSrv.Params.REF_SIS_ID)
   CommonService.saveFile(finalSubSrv.Params.REF_SIS_ID,".html","/FromClient/SIS/HTML/",finalSubSrv.TERM_SIS_OUTPUT,"N").then(function(){
				  console.log("TERM SIS with Agent Sign Success");

				  });

}

this.saveTermFHRFile = function(){
console.log("finalSubSrv.Params.REF_FHRID"+finalSubSrv.Params.REF_FHRID);
	  CommonService.saveFile(finalSubSrv.Params.REF_FHRID,".html","/FromClient/FF/HTML/",finalSubSrv.TERM_FFNA_OUTPUT,"N").then(function(){
				   console.log("TERM FHR with Agent Sign Success");

				   });

}


	this.replaceForAppHTML = function(sign){
		finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,'||AGENT_SIGN||',sign);
		if(!!finalSubSrv.AgentData.ImpData && !!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||OFFCODE||",finalSubSrv.AgentData.ImpData.OFFICE_CODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||SUBOFFCODE||",finalSubSrv.AgentData.ImpData.SUBOFFCODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||CHANNEL||",finalSubSrv.AgentData.ImpData.CHANNEL_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||SERVICEBRANCHNAME||",finalSubSrv.AgentData.ImpData.OFFICE_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||RMCAMSCODE||",finalSubSrv.AgentData.ImpData.IMP_RM_CAMS_CD);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||BRANCHCODE||",finalSubSrv.AgentData.ImpData.OFFICE_CODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||OFFICENAME||",finalSubSrv.AgentData.ImpData.OFFICE_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||ADDRESSLINE1||",finalSubSrv.AgentData.ImpData.OFFICE_ADDRESS1);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||ADDRESSLINE2||",finalSubSrv.AgentData.ImpData.OFFICE_ADDRESS2);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||AGENTCODE||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||AGENTNAME||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||CONTACTDETAILS||",finalSubSrv.AgentData.ImpData.MOBILENO);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||LICENSEDETAILS||",finalSubSrv.AgentData.ImpData.IRDA_LIC_NO);
		}
		else{
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||OFFCODE||",LoginService.lgnSrvObj.userinfo.OFFICE_CODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||SUBOFFCODE||",LoginService.lgnSrvObj.userinfo.SUBOFFICECODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||CHANNEL||",LoginService.lgnSrvObj.userinfo.CHANNEL_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||SERVICEBRANCHNAME||",LoginService.lgnSrvObj.userinfo.OFFICE_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||RMCAMSCODE||","");
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||BRANCHCODE||",LoginService.lgnSrvObj.userinfo.OFFICE_CODE);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||OFFICENAME||",LoginService.lgnSrvObj.userinfo.OFFICE_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||ADDRESSLINE1||",LoginService.lgnSrvObj.userinfo.OFFICE_ADDRESS1);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||ADDRESSLINE2||",LoginService.lgnSrvObj.userinfo.OFFICE_ADDRESS2);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||AGENTCODE||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||AGENTNAME||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||CONTACTDETAILS||",LoginService.lgnSrvObj.userinfo.MOBILENO);
			finalSubSrv.FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.FINAL_OUTPUT,"||LICENSEDETAILS||",LoginService.lgnSrvObj.userinfo.IRDA_LIC_NO);
		}
		return finalSubSrv.FINAL_OUTPUT;
	};


	this.replaceForTermAppHTML = function(sign){
		debug("inside replaceForTermAppHTML");
		finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,'||AGENT_SIGN||',sign);
		if(!!finalSubSrv.AgentData.ImpData && !!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||OFFCODE||",finalSubSrv.AgentData.ImpData.OFFICE_CODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||SUBOFFCODE||",finalSubSrv.AgentData.ImpData.SUBOFFCODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||CHANNEL||",finalSubSrv.AgentData.ImpData.CHANNEL_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||SERVICEBRANCHNAME||",finalSubSrv.AgentData.ImpData.OFFICE_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||RMCAMSCODE||",finalSubSrv.AgentData.ImpData.IMP_RM_CAMS_CD);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||BRANCHCODE||",finalSubSrv.AgentData.ImpData.OFFICE_CODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||OFFICENAME||",finalSubSrv.AgentData.ImpData.OFFICE_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||ADDRESSLINE1||",finalSubSrv.AgentData.ImpData.OFFICE_ADDRESS1);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||ADDRESSLINE2||",finalSubSrv.AgentData.ImpData.OFFICE_ADDRESS2);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||AGENTCODE||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||AGENTNAME||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||CONTACTDETAILS||",finalSubSrv.AgentData.ImpData.MOBILENO);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||LICENSEDETAILS||",finalSubSrv.AgentData.ImpData.IRDA_LIC_NO);
		}
		else{
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||OFFCODE||",LoginService.lgnSrvObj.userinfo.OFFICE_CODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||SUBOFFCODE||",LoginService.lgnSrvObj.userinfo.SUBOFFICECODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||CHANNEL||",LoginService.lgnSrvObj.userinfo.CHANNEL_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||SERVICEBRANCHNAME||",LoginService.lgnSrvObj.userinfo.OFFICE_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||RMCAMSCODE||","");
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||BRANCHCODE||",LoginService.lgnSrvObj.userinfo.OFFICE_CODE);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||OFFICENAME||",LoginService.lgnSrvObj.userinfo.OFFICE_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||ADDRESSLINE1||",LoginService.lgnSrvObj.userinfo.OFFICE_ADDRESS1);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||ADDRESSLINE2||",LoginService.lgnSrvObj.userinfo.OFFICE_ADDRESS2);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||AGENTCODE||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||AGENTNAME||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||CONTACTDETAILS||",LoginService.lgnSrvObj.userinfo.MOBILENO);
			finalSubSrv.TERM_FINAL_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_FINAL_OUTPUT,"||LICENSEDETAILS||",LoginService.lgnSrvObj.userinfo.IRDA_LIC_NO);
		}
		debug("return replaceForTermAppHTML");
		return finalSubSrv.TERM_FINAL_OUTPUT;
	};

	this.replaceForCCRHTML = function(sign){
		debug("befor ccr update");
		if(!!finalSubSrv.CCR_OUTPUT){
		    finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||AGENT_SIGN||",'<img src="' + sign + '" width="150px" height="50px"/>');
            if(!!finalSubSrv.AgentData.ImpData && !!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
                finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||AGENT_NAME||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
                finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||AGENT_CD||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
                if(!!finalSubSrv.AgentData.ImpData.MOBILENO)
                    finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||MOB_NO||",finalSubSrv.AgentData.ImpData.MOBILENO);
                else
                    finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||MOB_NO||","");
            }
            else{
                finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||AGENT_NAME||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
                finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||AGENT_CD||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
                if(!!LoginService.lgnSrvObj.userinfo.MOBILENO)
                    finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||MOB_NO||",LoginService.lgnSrvObj.userinfo.MOBILENO);
                else
                    finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||MOB_NO||","");
            }
            finalSubSrv.CCR_OUTPUT = finalSubSrv.CCR_OUTPUT.replace("||PLACE||","");
            debug("after ccr update"+finalSubSrv.CCR_OUTPUT);
		}

		return finalSubSrv.CCR_OUTPUT;
	};

	this.replaceForSISHTML = function(sign){
		if(!!finalSubSrv.AgentData.ImpData && !!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
			finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTNAME||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
			finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTNUMBER||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
			if(!!finalSubSrv.AgentData.ImpData.MOBILENO){
				finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTCONTACTNUMBER||",finalSubSrv.AgentData.ImpData.MOBILENO);
			}
			else{
				finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTCONTACTNUMBER||","");
			}
		}
		else{
			finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTNAME||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
			finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTNUMBER||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
			if(!!LoginService.lgnSrvObj.userinfo.MOBILENO){
				finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTCONTACTNUMBER||",LoginService.lgnSrvObj.userinfo.MOBILENO);
			}
			else{
				finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"||AGENTCONTACTNUMBER||","");
			}
		}
		finalSubSrv.SIS_OUTPUT = finalSubSrv.SIS_OUTPUT.replace(/"hideAgentSign"/g,"\"showAgentSign\"");
		finalSubSrv.SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.SIS_OUTPUT,"@@AGENTSIGN@@", sign);
		return finalSubSrv.SIS_OUTPUT;
	};


	this.replaceForTermSISHTML = function(sign){
		debug("inside replaceForTermSISHTML");
		if(!!finalSubSrv.AgentData.ImpData && !!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTNAME||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTNUMBER||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
			if(!!finalSubSrv.AgentData.ImpData.MOBILENO){
				finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTCONTACTNUMBER||",finalSubSrv.AgentData.ImpData.MOBILENO);
			}
			else{
				finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTCONTACTNUMBER||","");
			}
			finalSubSrv.TERM_SIS_OUTPUT = finalSubSrv.TERM_SIS_OUTPUT.replace(/"hideAgentSign"/g,"\"showAgentSign\"");
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"@@AGENTSIGN@@", sign);
			debug("return replaceForTermSISHTML");
		}
		else{
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTNAME||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTNUMBER||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
			if(!!finalSubSrv.AgentData.ImpData.MOBILENO){
				finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTCONTACTNUMBER||",LoginService.lgnSrvObj.userinfo.MOBILENO);
			}
			else{
				finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"||AGENTCONTACTNUMBER||","");
			}
			finalSubSrv.TERM_SIS_OUTPUT = finalSubSrv.TERM_SIS_OUTPUT.replace(/"hideAgentSign"/g,"\"showAgentSign\"");
			finalSubSrv.TERM_SIS_OUTPUT = CommonService.replaceAll(finalSubSrv.TERM_SIS_OUTPUT,"@@AGENTSIGN@@", sign);
			debug("return replaceForTermSISHTML");
		}
		return finalSubSrv.TERM_SIS_OUTPUT;
	};

	this.replaceForFFNHTML = function(sign){
		var ffOutput = finalSubSrv.FFNA_OUTPUT;
		ffOutput = CommonService.replaceAll(ffOutput,"||SIGNATURE_SEPECIFIED_PERSON||",'<img src="' + sign + '" width="150px" height="50px"/>');
		if(!!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
			ffOutput = CommonService.replaceAll(ffOutput,"||NAME_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
			ffOutput = CommonService.replaceAll(ffOutput,"||CODE_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
		}
		else{
			ffOutput = CommonService.replaceAll(ffOutput,"||NAME_SPECIFIED_PERSON||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
			ffOutput = CommonService.replaceAll(ffOutput,"||CODE_SPECIFIED_PERSON||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
		}
		ffOutput = CommonService.replaceAll(ffOutput,"||PROPOSAL_NO||",finalSubSrv.Params.POLICY_NO);
		finalSubSrv.FFNA_OUTPUT = ffOutput;
		return finalSubSrv.FFNA_OUTPUT;
	};

	this.replaceForTermFFNHTML = function(sign){
		debug("inside replaceForTermFFNHTML");
		return finalSubSrv.loadOppFlags(finalSubSrv.Params.COMBO_ID,finalSubSrv.Params.AGENT_CD,'combo','P').then(function(oppData){
  			var ffOutput = finalSubSrv.TERM_FFNA_OUTPUT;
  			ffOutput = CommonService.replaceAll(ffOutput,"||SIGNATURE_SEPECIFIED_PERSON||",'<img src="' + sign + '" width="150px" height="50px"/>');
  			if(!!finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD){
  				ffOutput = CommonService.replaceAll(ffOutput,"||NAME_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_NAME);
  				ffOutput = CommonService.replaceAll(ffOutput,"||CODE_SPECIFIED_PERSON||",finalSubSrv.AgentData.ImpData.IMP_AGENT_CAMS_CD);
  			}
  			else{
  				ffOutput = CommonService.replaceAll(ffOutput,"||NAME_SPECIFIED_PERSON||",LoginService.lgnSrvObj.userinfo.AGENT_NAME);
  				ffOutput = CommonService.replaceAll(ffOutput,"||CODE_SPECIFIED_PERSON||",LoginService.lgnSrvObj.userinfo.AGENT_CD);
  			}
  			ffOutput = CommonService.replaceAll(ffOutput,"||PROPOSAL_NO||",oppData.oppFlags.REF_POLICY_NO);
  			finalSubSrv.TERM_FFNA_OUTPUT = ffOutput;
  			debug("return replaceForTermFFNHTML");
  			return finalSubSrv.TERM_FFNA_OUTPUT;
		});
	};

this.getSignDocID = function(){
    var dfd = $q.defer();
    var SignDocID;
	var sql = "SELECT * FROM LP_DOC_PROOF_MASTER WHERE MPDOC_CODE=? AND MPPROOF_CODE=? AND CUSTOMER_CATEGORY=?";
    var params = ["SZ","AS","AG"];
    CommonService.transaction(db, function(tx){
                                    CommonService.executeSql(tx, sql, params, function(tx,res){
                                                if(res.rows.length!=0)
                                                {
                                                	SignDocID = res.rows.item(0).DOC_ID
                                                    dfd.resolve(SignDocID);
                                                }

                					}, function(tx, err){console.log("Error 1");});
                					},function(err){});
       return dfd.promise;
	}

this.startPolicySubmission = function(Params, ComboParams){

AppSyncService.startFinalSubmission(Params, ComboParams);
}

}]);
