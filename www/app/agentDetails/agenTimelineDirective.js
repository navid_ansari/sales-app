agentDetailsModule.controller('agentTimeCtrl',['$state','agentTimeService','AgentDetailsService', 'CommonService','LoadApplicationScreenData','ApplicationFormDataService','$stateParams',function($state,agentTimeService,AgentDetailsService, CommonService,LoadApplicationScreenData,ApplicationFormDataService,$stateParams){
	debug("agent timeline loaded");

	this.firstLabel = 'Capture SP Details';
	if(BUSINESS_TYPE=="GoodSolution"){
		this.firstLabel = 'Capture Agent Details';
	}

	this.agentTimeArr = [
		{'name': 'spDetails', 'timeLineName': this.firstLabel, 'type': 'spDetails', 'isCompleted': agentTimeService.isSpDetailCompleted},
		{'name': 'ccrQues', 'timeLineName': 'CCR Questionnaire', 'type': 'ccrQues', 'isCompleted': agentTimeService.isCCRCompleted},
		{'name':'finalSubmission', 'timeLineName': 'Final Submission', 'type': 'finalSubmission', 'isCompleted': agentTimeService.isFinalSubCompleted}
	];

	if(BUSINESS_TYPE!='IndusSolution' && BUSINESS_TYPE!='GoodSolution'){
		this.agentTimeArr.shift();
	}

	this.activeTab = agentTimeService.getActiveTab();
	debug("active tab set");
    debug("APPLICATION_ID details is : "+JSON.stringify($stateParams));
    debug("AGENT_CD is : "+AgentDetailsService.AGENT_CD);

	this.gotoPage = function(pageName){
        debug("inputs for the APPLICATION_ID "+$stateParams.APPLICATION_ID);
        debug("inputs for the AGENT_CD "+AgentDetailsService.AGENT_CD);

         LoadApplicationScreenData.loadApplicationFlags($stateParams.APPLICATION_ID,AgentDetailsService.AGENT_CD).then(function(response){
            debug("response for the loadApplicationFlags "+ JSON.stringify(response));
             if((!!response) && (response.applicationFlags.ISAGENT_SIGNED != "Y")){
				AgentDetailsService.gotoPage(pageName);
             }else{
                navigator.notification.alert("No changes allowed after signature.",null,"Alert","OK");
               // navigator.notification.alert("No changes allowed after signature..",null,"Alert","OK");
             }
         });
	};

	this.openMenu = function(){
		CommonService.openMenu();
	};

	debug("this.appTimeArr: " + JSON.stringify(this.agentTimeArr));
}]);

agentDetailsModule.service('agentTimeService',['$state',function($state){
	debug("in agent time service");

	if(BUSINESS_TYPE!="iWealth")
		this.activeTab = "spDetails";
	else
		this.activeTab = "ccrQues";

	this.isSpDetailCompleted = false;
	this.isCCRCompleted = false;
	this.isFinalSubCompleted = false;

	this.getActiveTab = function(){
		return this.activeTab;
	};

	this.setActiveTab = function(activeTab){
		this.activeTab = activeTab;
	};
}]);

agentDetailsModule.directive('agentTimeline',[function() {
	"use strict";
	debug('in agentTimeline');
	return {
		restrict: 'E',
		controller: "agentTimeCtrl",
		controllerAs: "agtc",
		bindToController: true,
		templateUrl: "agentDetails/agentDetailsHeader.html"
	};
}]);
