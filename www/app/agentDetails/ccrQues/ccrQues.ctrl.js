ccrQuesModule.controller('CcrQuesCtrl', ['$state','CcrQuesService','LoadData','NameData','$stateParams','ViewReportService','CommonService','ReflectiveQuesService','agentTimeService','IsSelf','uploadDocService','DocumentDescription','appData','$q','IsAgentRelated','IsDocSuccessfullyUploaded','TermData', function($state, CcrQuesService,LoadData,NameData,$stateParams,ViewReportService,CommonService,ReflectiveQuesService,agentTimeService,IsSelf,uploadDocService,DocumentDescription,appData,$q,IsAgentRelated,IsDocSuccessfullyUploaded,TermData){
    CommonService.hideLoading();

    /* Header Hight Calculator */
    setTimeout(function(){
        var agentGetHeight = $(".agent-details .fixed-bar").height();
		console.log("agentGetHeight : " + agentGetHeight);
        $(".agent-details .custom-position").css({top:agentGetHeight + "px"});
    });
    /* End Header Hight Calculator */


    var ccrc = this;
    this.genOpClick = false;
    debug("LoadData : " + JSON.stringify(LoadData));
    debug("NameData : " + JSON.stringify(NameData));
    this.ccrQuesData = {};
    this.LoadData = LoadData;
    this.LoadData.NameData = NameData;
    this.isOPGEN = LoadData.isOPGEN;
    this.LoadData.refFlag = true;
    this.LoadData.TermData = TermData;
    //this.IsProposer = true;
    this.IsInsured = true;
    this.DocumentDescription = DocumentDescription;
   // ccrc.IsAgentRelated = {};


    ccrc.IsAgentRelated = IsAgentRelated;

    this.isSelf = IsSelf; //1788 remove

     this.IsProposer = (this.isSelf == "200")?false:true;
	 debug("IsProposer :::"+this.IsProposer);
    this.DOC_ID = this.LoadData.docId;
    this.inDatabase = false;

    var imagePath = "/FromClient/APP/IMAGES/";
    var pdfExtension = ".pdf";
    var jpegExtension = ".jpg";

    var refernceApplicationID = null;
    var RefPolicyNo = null;
    if(!!appData){
        refernceApplicationID = appData.REF_APPLICATION_ID || null;
        RefPolicyNo = appData.REF_POLICY_NO || null;
    }
    ccrc.inDatabase = IsDocSuccessfullyUploaded;

    if(IsDocSuccessfullyUploaded){
        ccrc.isOPGEN = true;
    }
     debug("IsDocSuccessfullyUploaded "+IsDocSuccessfullyUploaded);
    debug("refernce Application ID "+refernceApplicationID);
    debug("refernce RefPolicyNo  "+RefPolicyNo);

    Object.assign(CcrQuesService.LoadData,this.LoadData);
    var filteredArray = [];


ccrc.changeIsRelatedFlag = function(isAgentRelated){
        debug("change Is Related Flag"+JSON.stringify(ccrc.IsAgentRelated));
        ccrc.IsAgentRelated.AGENT_REL_FLAG = isAgentRelated;
        var DOC_ID =   LoadData.docId;
        var AGENT_CD =  LoadData.agentCd;
        var APPLICATION_ID =  LoadData.appId;
        var POLICY_NO = $stateParams.POLICY_NO;
        debug("refernceApplicationID "+refernceApplicationID);
        debug("APPLICATION_ID "+APPLICATION_ID);
        var COMBO_ID = $stateParams.COMBO_ID;
       // ccrc.inDatabase =CcrQuesService.checkIfAgentRelated(ccrc.IsAgentRelated,DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID,refernceApplicationID);
         CcrQuesService.checkIfAgentRelated(ccrc.IsAgentRelated,DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID,refernceApplicationID).then(
            function(responcCheckIfAgentRelated){
                if(isAgentRelated == "Y"){
                     ccrc.inDatabase = responcCheckIfAgentRelated;
                      CcrQuesService.getUploadedDocumentExtentions(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID).then(
                         function(responce){
                              var resp = (!!responce)? responce.inDatabase :false;
                              if(resp){
                                 var docName = (responce.docName);
                                 var isHtml = (docName.indexOf(".html") !== -1);
                                 debug("extention type : "+isHtml);
                                 if(isHtml){
                                      uploadDocService.deleteAllImagesOfDOCID(APPLICATION_ID,AGENT_CD,DOC_ID,POLICY_NO).then(
                                         function(deleted){
                                             if(deleted){
                                                 debug("<<<< DELETED ALL DOCIDS >>>>");
                                                 debug("<<<< INSERTION STARTED >>>>");
                                                  ccrc.inDatabase = false;
                                                 //checkTermPlanConditions();
                                             }
                                         }
                                     );
                                  }
                              }

                         });
                }else{
                    uploadDocService.deleteAllImagesOfDOCID(APPLICATION_ID,AGENT_CD,DOC_ID,POLICY_NO).then(
                        function(deleted){
                            if(deleted){
                                debug("<<<< DELETED ALL DOCIDS >>>>");
                                debug("<<<< INSERTION STARTED >>>>");
                                 ccrc.inDatabase = false;
                                //checkTermPlanConditions();
                            }
                        }
                    );
                }
        });

    };
    //ccrc.changeIsRelatedFlag(ccrc.IsAgentRelated.AGENT_REL_FLAG);

    //this.IsAgentRelated

    this.onGenerateOutput = function(ccrQuesForm){
        this.genOpClick = true;
        if(ccrQuesForm.$invalid == false){
            console.log("ccrQuesData : " + JSON.stringify(this.ccrQuesData));
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            cordova.exec(
                function(fileData){
                    console.log("fileData: " + fileData.length);
                    Object.assign(CcrQuesService.ccrQuesData,ccrc.ccrQuesData)
                    if(!!fileData)
                        CcrQuesService.generateOutput(fileData,this.isSelf).then(
                            function(HtmlFormOutput){
                                ReflectiveQuesService.saveQuesReportFile(fileName,HtmlFormOutput).then(
                                    function(resp){
                                        if(!!resp){
                                            ViewReportService.fetchSavedReport("APP",fileName).then(
                                                function(res){
                                                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "agentDetails.ccrQues", "REPORT_PARAMS": ccrc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        );
                    else
                        navigator.notification.alert("Template file not found",null,"Agent Details","OK");
                },
                function(errMsg){
                    debug(errMsg);
                },
                "PluginHandler","getDataFromFileInAssets",["www/templates/APP/Customer_Confidential_Report.html"]
            );
        }else{
            navigator.notification.alert('Please fill all mandatory fields',null,"Agent Details","OK");
        }
    };

    this.reGenerate = function(){
        this.isOPGEN = false;
    };

    this.goNext = function(){
        agentTimeService.setActiveTab("finalSubmission");
        agentTimeService.isCCRCompleted = true;
        debug("PARAMS IN CCR QUEST >>"+JSON.stringify($stateParams));
        CommonService.showLoading("Loading...");
		CcrQuesService.onNext($stateParams.APPLICATION_ID, $stateParams.POLICY_NO, $stateParams.SIS_ID, $stateParams.FHR_ID, $stateParams.AGENT_CD).then(
			function(res){
				$state.go('agentDetails.finalSubmission',{"AGENT_CD": $stateParams.AGENT_CD, "APPLICATION_ID": $stateParams.APPLICATION_ID, "FHR_ID": $stateParams.FHR_ID, "SIS_ID":$stateParams.SIS_ID, "POLICY_NO": $stateParams.POLICY_NO,"REF_SIS_ID":$stateParams.REF_SIS_ID,"REF_FHRID":$stateParams.REF_FHRID,"REF_OPP_ID":$stateParams.REF_OPP_ID,"COMBO_ID":$stateParams.COMBO_ID,"REF_APPLICATION_ID":$stateParams.REF_APPLICATION_ID});
			}
		);
    }
    this.showPreview = function(){
        if(ccrc.inDatabase){
                var DOC_ID =   LoadData.docId;
                var AGENT_CD =  LoadData.agentCd;
                var APPLICATION_ID =  LoadData.appId;
                debug("refernceApplicationID "+refernceApplicationID);
                debug("APPLICATION_ID "+APPLICATION_ID);
                var COMBO_ID = $stateParams.COMBO_ID;
                CcrQuesService.getUploadedDocumentExtentions(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID).then(
                function(responce){
                     var resp = (!!responce)? responce.inDatabase :false;
                     if(resp){
                        var docName = (responce.docName);
                        //var isImage = docName.includes(jpegExtension);
                        var isImage = (docName.indexOf(jpegExtension) !== -1);
                        debug("extention type : "+isImage);
                        if(isImage){
                             ccrc.callNativeViewImage("viewimage",0,DOC_ID,jpegExtension);
                             ccrc.isOPGEN = true;
                         }else{
                              navigator.notification.alert('Sorry you ,cannot view pdf file',null,"Agent Details","OK");
                         }
                     }
                    // dfd.resolve(resp);
                     //debug(" CCR Is Related Change"+JSON.stringify(resp));
                });
        }else{
            var fileName = this.LoadData.agentCd + "_" + this.LoadData.appId + "_" + this.LoadData.docId;
            ViewReportService.fetchSavedReport("APP",fileName).then(
                function(res){
                    $state.go("viewReport", {'REPORT_TYPE': 'APP', 'REPORT_ID': fileName, 'PREVIOUS_STATE': "agentDetails.ccrQues", "REPORT_PARAMS": ccrc.LoadData, "SAVED_REPORT": res, 'PREVIOUS_STATE_PARAMS': $stateParams});
                }
            );
        }
    };



    /**
    **
    **
    **/

     //Document click..
    this.nativeDocClicked = function(index){
        debug("index  clicked is "+index);
        debug("selected docid >>>"+this.DOC_ID);
        if(!!this.DOC_ID){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
            this.callNative("Document",index,this.DOC_ID,pdfExtension);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };

    //Camera click..
    this.nativeCamClicked = function(index){
        debug("index  clicked is "+index);
        debug("selected docid >>>"+this.DOC_ID);
        if(!!this.DOC_ID){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
            this.callNative("Camera",index,this.DOC_ID,jpegExtension);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };



    //Gallery click..
    this.nativeGallClicked = function(index){
        debug("index  clicked is "+index);
        debug("selected docid >>>"+this.DOC_ID);
        if(!!this.DOC_ID){
            //debug("selected doc data >>"+JSON.stringify(filteredArray[index]));
            this.callNative("Gallery",index,this.DOC_ID,jpegExtension);
        }
        else
            navigator.notification.alert("Please select the document",null,"Document Upload","OK");
    };
    this.callNativeViewImage = function(source,index,DOC_ID,extension){
        //var documentID = filteredArray[index].DOC_ID;
        var AGENT_CD =  LoadData.agentCd;
        var APP_ID =  LoadData.appId;
        var fileName = AGENT_CD + "_" + APP_ID + "_" + DOC_ID;
        debug("File name "+fileName);

        //call native class...
        cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
            },//function error..
            "PluginHandler",
            'camera',
            [imagePath,fileName,source]
        );
    }
    //call native code depending on the source..
     this.callNative = function(source,index,DOC_ID,extension){
        //var documentID = filteredArray[index].DOC_ID;
      //  var fileName = AGENT_CD + "_" + APP_ID + "_" + DOC_ID;
        debug(JSON.stringify(LoadData));
        var DOC_ID = DOC_ID;
        var AGENT_CD =  LoadData.agentCd;
        var APPLICATION_ID =  LoadData.appId;
        var POLICY_NO = $stateParams.POLICY_NO;
        var DOC_ID_NEW = DOC_ID;
        var isSelfDisable = false;
        var REF_POLICY_NO = RefPolicyNo;
        var isTermClick = "";
        var REF_APPLICATION_ID = refernceApplicationID;
        var COMBO_ID = $stateParams.COMBO_ID;
        var fileName = AGENT_CD + "_" + APPLICATION_ID+ "_" + DOC_ID;
        debug("File name "+fileName);
        debug("source : "+source+" index "+index+" DOC_ID "+DOC_ID+" extension "+extension);
        //call native class...
        cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
                if(success.code == 1){
                    if(!!success.count){
                        debug("result from the camera class "+JSON.stringify(success));//1788

                       uploadDocService.insertCapturedDocs(DOC_ID,fileName,success.count,AGENT_CD,POLICY_NO,APPLICATION_ID,extension,COMBO_ID,REF_APPLICATION_ID,DOC_ID_NEW,isSelfDisable,RefPolicyNo,isTermClick).then(

                       // uploadDocService.insertCapturedDocs(DOC_ID,fileName,success.count,this.LoadData.agentCd ,$stateParams.POLICY_NO,this.LoadData.appId,extension).then(
                            function(ifInsertedCnt){
                                debug("inserted Flag "+JSON.stringify(ifInsertedCnt));
                                if(ifInsertedCnt){
                                    debug("inserted Flag  inside"+JSON.stringify(ifInsertedCnt));
                                  //ccrC.isOPGEN
                                  ccrc.inDatabase = true;

                                }//if loop
                            }//function
                        );//service function
                    }
                }else{
                    debug("code from camera  "+success.code);
                }
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
            },//function error..
            "PluginHandler",
            'camera',
            [imagePath,fileName,source]
        );
    }


    this.callNativeViewImage = function(source,index,DOC_ID,extension,DOC_ID_NEW){
        //var documentID = filteredArray[index].DOC_ID;
         var AGENT_CD =  LoadData.agentCd;
         var APP_ID =  LoadData.appId;
        var fileName = AGENT_CD + "_" + APP_ID + "_" + DOC_ID;
        debug("File name "+fileName);

        //call native class...
        cordova.exec(
            function(success){
                debug("success -->"+JSON.stringify(success));
            },//function success
            function(error){
                debug("error -->"+JSON.stringify(error));
            },//function error..
            "PluginHandler",
            'camera',
            [imagePath,fileName,source]
        );
    }

    this.submitt = function(){
            if(ccrc.inDatabase){
                //need t ovaideated

                var DOC_ID =   LoadData.docId;
                var AGENT_CD =  LoadData.agentCd;
                var APPLICATION_ID =  LoadData.appId;
                debug("refernceApplicationID "+refernceApplicationID);
                debug("APPLICATION_ID "+APPLICATION_ID);
                var COMBO_ID = $stateParams.COMBO_ID;
                CcrQuesService.getUploadedDocumentExtentions(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID).then(
                function(responce){
                     var resp = (!!responce)? responce.inDatabase :false;
                     if(resp){
                        var docName = (responce.docName);
                        //var isImage = docName.includes(jpegExtension);
                        var isImage = (docName.indexOf(jpegExtension) !== -1);
                        debug("extention type : "+isImage);
                        if(isImage){
                             ccrc.callNativeViewImage("viewimage",0,DOC_ID,jpegExtension);
                             ccrc.isOPGEN = true;
                         }else{
                              navigator.notification.alert('Sorry you ,cannot view pdf file',null,"Agent Details","OK");
                         }
                     }
                    // dfd.resolve(resp);
                     //debug(" CCR Is Related Change"+JSON.stringify(resp));
                });

            }else{
                navigator.notification.alert('Please upload all the documents',null,"Agent Details","OK");
            }
    };

}]);

ccrQuesModule.service('CcrQuesService',['$q', '$state', '$stateParams', 'CommonService','SisFormService','$filter','uploadDocService','LoadApplicationScreenData', function($q, $state, $stateParams, CommonService,SisFormService,$filter,uploadDocService,LoadApplicationScreenData){
    var ccrs = this;
    this.HtmlFormOutput = "";
    this.ccrQuesData = {};
    this.LoadData = {};

	this.onNext = function(appId, polNo, sisId, fhrId, agentCd){
		var dfd = $q.defer();
		if(BUSINESS_TYPE=='iWealth' || BUSINESS_TYPE=='LifePlaner'){
			var insObj = {};
             debug("inside impsnt"+BUSINESS_TYPE);
			insObj.MODIFIED_DATE = CommonService.getCurrDate();
			insObj.ISSYNCED = 'N';
			//insObj.APPLICATION_ID = appId;   /// commented for update query
			insObj.POLICY_NO = polNo;
			//insObj.AGENT_CD = agentCd;      /// commented for update query
			insObj.SIS_ID = sisId;
			insObj.FHR_ID = fhrId;
            LoadApplicationScreenData.loadAgentImpData(appId,agentCd).then(function(AgentImpData){

               if(!!AgentImpData && !!AgentImpData.APPLICATION_ID)
               {
                   CommonService.updateRecords(db, 'lp_app_sourcer_impsnt_scrn_l', insObj, {"APPLICATION_ID":appId,"AGENT_CD":agentCd}).then(
                      function(res){
                        debug("update impsnt");
                      dfd.resolve(res);
                   });
               }
               else{

                   insObj.APPLICATION_ID = appId;
                   insObj.AGENT_CD = agentCd;
                   CommonService.insertOrReplaceRecord(db, 'lp_app_sourcer_impsnt_scrn_l', insObj, true).then(
                      function(res){
                      debug("insert impsnt");
                      dfd.resolve(res);
                   });
              }
            });
		}
		else{
			dfd.resolve("S");
		}
		return dfd.promise;
	};

  //  this.generateOutput = function(fileData){
    this.agentRelFlagObject = {};
    this.AppDataMYOPPORTUNITY = {};
    /**
    *Function name get_INSURANCE_BUYING_FOR_CODE stats here
    *This function to get policy type to check weather it is the self or proposed
    *The get the table values from the  table name LP_SIS_SCREEN_A
    *In this class the input parameter is SIS_ID
    */
     this.get_INSURANCE_BUYING_FOR_CODE = function(APPLICATION_ID,AGENT_CD,SIS_ID){
         debug("input parameter  get_INSURANCE_BUYING_FOR_CODE: " + APPLICATION_ID + "AGENT_CD : " + AGENT_CD + "SIS_ID : " + SIS_ID);
            var dfd = $q.defer();
            var NameData = {};
            SisFormService.loadSavedSISData(AGENT_CD, SIS_ID).then(
                function(SISFormData){
                    debug("SISFormData");

                    debug(JSON.stringify(SISFormData));
                    if(!!SISFormData){
                        var INSURANCE_BUYING_FOR_CODE = SISFormData.sisFormAData.INSURANCE_BUYING_FOR_CODE;
                        debug("INSURANCE_BUYING_FOR_CODE : "+INSURANCE_BUYING_FOR_CODE);
                        dfd.resolve(INSURANCE_BUYING_FOR_CODE);
                    }else{
                        dfd.resolve(null);
                    }
                },
                function(err){
                    dfd.resolve(null);
                }
            );
             return dfd.promise;
     };

    /* get_INSURANCE_BUYING_FOR_CODE  ends here */

    /**
    *Function name get_INSURANCE_BUYING_FOR_CODE stats here
    *This function to get policy type to check weather it is the self or proposed
    *The get the table values from the  table name LP_SIS_SCREEN_A
    *In this class the input parameter is SIS_ID
    */
     this.get_DOCUMENT_DESCRIPTION = function(DOC_ID){
            var dfd = $q.defer();
            var sql = "SELECT * FROM LP_DOC_PROOF_MASTER where DOC_ID = '"+DOC_ID+"'";
            CommonService.transaction(db,
                function(tx){
                    CommonService.executeSql(tx,sql,[],
                        function(tx,res){
                            debug(sql+"res.rows.length: " + res.rows.length);
                            if(!!res && res.rows.length>0){
                                debug("LP_DOC_PROOF_MASTER record found");
                                dfd.resolve(res.rows.item(0));
                            }
                            else{
                                debug("LP_DOC_PROOF_MASTER record not found");
                                dfd.resolve(null);
                            }
                        },
                        function(tx,err){
                            dfd.resolve(null);
                        }
                    );
                },
                function(err){
                    dfd.resolve(null);
                },null
            );
            return dfd.promise;
     };

    /* get_INSURANCE_BUYING_FOR_CODE  ends here */




    this.generateOutput = function(fileData,isSelf){
        var dfd = $q.defer();
        debug(JSON.stringify(this.LoadData)+"this.ccrQuesData : " + JSON.stringify(this.ccrQuesData));
        this.HtmlFormOutput = fileData;
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||POLNUMBER||",ccrs.LoadData.polNo);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSNAME||",ccrs.LoadData.NameData.INSURED_NAME);
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPNAME||",ccrs.LoadData.NameData.PROPOSER_NAME);


        if(isSelf){

            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES1||",this.ccrQuesData.ANS_1 ? (this.ccrQuesData.ANS_1=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES2||",this.ccrQuesData.ANS_2 ? (this.ccrQuesData.ANS_2=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES3||",this.ccrQuesData.ANS_3 ? (this.ccrQuesData.ANS_3=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4||",this.ccrQuesData.ANS_4 ? (this.ccrQuesData.ANS_4=='Y' ? 'Yes' : 'No') : "");

            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES1||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES2||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES3||","");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4||","");
        }else{

            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES1||",this.ccrQuesData.ANS_1 ? (this.ccrQuesData.ANS_1=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES2||",this.ccrQuesData.ANS_2 ? (this.ccrQuesData.ANS_2=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES3||",this.ccrQuesData.ANS_3 ? (this.ccrQuesData.ANS_3=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||INSQUES4||",this.ccrQuesData.ANS_4 ? (this.ccrQuesData.ANS_4=='Y' ? 'Yes' : 'No') : "");



            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES1||",this.ccrQuesData.ANS_1Proposer ? (this.ccrQuesData.ANS_1Proposer=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES2||",this.ccrQuesData.ANS_2Proposer ? (this.ccrQuesData.ANS_2Proposer=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES3||",this.ccrQuesData.ANS_3Proposer ? (this.ccrQuesData.ANS_3Proposer=='Y' ? 'Yes' : 'No') : "");
            this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||PROPQUES4||",this.ccrQuesData.ANS_4Proposer ? (this.ccrQuesData.ANS_4Proposer=='Y' ? 'Yes' : 'No') : "");


        }
        this.HtmlFormOutput = CommonService.replaceAll(this.HtmlFormOutput,"||DATE||",$filter('date')(CommonService.getCurrDate(), "dd-MM-yyyy"));

        debug("this.HtmlFormOutput : " + this.HtmlFormOutput.length);
        dfd.resolve(this.HtmlFormOutput);
        return dfd.promise;
    };

    this.getCcrData = function(DOC_ID,APPLICATION_ID,AGENT_CD){
        var dfd = $q.defer();
        var sql = "SELECT * FROM LP_DOCUMENT_UPLOAD where DOC_ID = '"+DOC_ID+"' and DOC_CAT_ID = '"+APPLICATION_ID+"' and AGENT_CD = '"+AGENT_CD+"'";
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,sql,[],
                    function(tx,res){
                        debug(sql+"res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            debug("LP_DOCUMENT_UPLOAD record found");
                            dfd.resolve(res);
                        }
                        else{
                            debug("LP_DOCUMENT_UPLOAD record not found");
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.getCcrNameData = function(APPLICATION_ID,AGENT_CD,SIS_ID){
        debug("getCcrNameData : " + APPLICATION_ID + "AGENT_CD : " + AGENT_CD + "SIS_ID : " + SIS_ID);
        var dfd = $q.defer();
        var NameData = {};
        SisFormService.loadSavedSISData(AGENT_CD, SIS_ID).then(
            function(SISFormData){
                debug("SISFormData Result starts " );
                debug(JSON.stringify(SISFormData));
                debug("SISFormData Result starts ");
                debug("resolve getRelDesc : " + SISFormData.sisFormAData.PROPOSER_IS_INSURED);
                var isSelf = SISFormData.sisFormAData.PROPOSER_IS_INSURED;
                CommonService.transaction(db,
                    function(tx){
                        CommonService.executeSql(tx,"select FIRST_NAME,MIDDLE_NAME,LAST_NAME from LP_APP_CONTACT_SCRN_A where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[APPLICATION_ID,AGENT_CD,'C01'],
                            function(tx,res){
                                debug("res.rows.length: " + res.rows.length);
                                if(!!res && res.rows.length>0){
                                    NameData.INSURED_NAME = res.rows.item(0).FIRST_NAME + " " + (!!res.rows.item(0).MIDDLE_NAME ? (res.rows.item(0).MIDDLE_NAME + " ") : "") + res.rows.item(0).LAST_NAME;
                                    if(isSelf=='N'){
                                        CommonService.transaction(db,
                                            function(tx){
                                                CommonService.executeSql(tx,"select FIRST_NAME,MIDDLE_NAME,LAST_NAME from LP_APP_CONTACT_SCRN_A where APPLICATION_ID = ? and AGENT_CD = ? and CUST_TYPE = ?",[APPLICATION_ID,AGENT_CD,'C02'],
                                                    function(tx,res){
                                                        debug("res.rows.length: " + res.rows.length);
                                                        if(!!res && res.rows.length>0){
                                                            NameData.PROPOSER_NAME = res.rows.item(0).FIRST_NAME + " " + (!!res.rows.item(0).MIDDLE_NAME ? (res.rows.item(0).MIDDLE_NAME + " ") : "") + res.rows.item(0).LAST_NAME;
                                                            dfd.resolve(NameData);
                                                        }else{
                                                            NameData.PROPOSER_NAME = "";
                                                            dfd.resolve(NameData);
                                                        }
                                                    },
                                                    function(tx,err){
                                                        dfd.resolve(null);
                                                    }
                                                );
                                            },
                                            function(err){
                                                dfd.resolve(null);
                                            },null
                                        );
                                    }else{
                                        NameData.PROPOSER_NAME = NameData.INSURED_NAME;
                                        dfd.resolve(NameData);
                                    }
                                }else{
                                    NameData.INSURED_NAME = "";
                                    NameData.PROPOSER_NAME = "";
                                    dfd.resolve(NameData);
                                }
                            },
                            function(tx,err){
                                dfd.resolve(null);
                            }
                        );
                    },
                    function(err){
                        dfd.resolve(null);
                    }
                );
            }
        );
        return dfd.promise;
    };

    this.getIsAgentRelatedInformation = function(APPLICATION_ID){
    var dfd = $q.defer();
    var query = "select * from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID = ?";
    debug("query "+query+" "+APPLICATION_ID);
     CommonService.transaction(db,
                function(tx){
                CommonService.executeSql(tx,query,[APPLICATION_ID],
                    function(tx,res){
                         debug("query "+res);
                         if(!!res && res.rows.length>0){
                                 debug("get Is Agent Related Information success");
                                var isAgentRelated = res.rows.item(0);
                                 debug(JSON.stringify(isAgentRelated));
                                dfd.resolve(isAgentRelated);
                          }else{
                            dfd.resolve(null);
                         }
                    },
                    function(tx,err){
                        debug("get Is Agent Related Information failed");
                        dfd.resolve(null);
                    }
                );
                },
                function(err){
                      debug("get Is Agent Related Information transaction failed");

                    dfd.resolve(null);
                }
    );

     return dfd.promise;
    };

    this.updateIsRelatedFlag = function(IsAgentRelated){

       debug("updateIsRelatedFlag input "+JSON.stringify(IsAgentRelated));
        var dfd = $q.defer();
      //  var query = "select AGENT_REL_FLAG from LP_APP_SOURCER_IMPSNT_SCRN_L where APPLICATION_ID = ?";

        CommonService.insertOrReplaceRecord(db, "LP_APP_SOURCER_IMPSNT_SCRN_L", IsAgentRelated, true).then(
            function(res){
                if(!!res){
                    debug(res);
                    debug("Inserted record in updateIsRelatedFlag successfully: SIS_ID: "+res );
                    dfd.resolve(true);
                }
                else{
                    debug("Couldn't insert record in LP_APP_SOURCER_IMPSNT_SCRN_L");
                    dfd.resolve(false);
                }
            }
        );
        return dfd.promise;
    };

   this.getDefaultAgentRelatedData = function(APPLICATION_ID,AGENT_CD,POLICY_NO,AGENT_REL_FLAG){
            var IsAgentRelated ={};
            IsAgentRelated.APPLICATION_ID =  APPLICATION_ID;
            IsAgentRelated.AGENT_CD = AGENT_CD;
            IsAgentRelated.POLICY_NO = POLICY_NO;
            IsAgentRelated.SIS_ID = null;
            IsAgentRelated.FHR_ID = null;
            IsAgentRelated.PREFERRED_LANGUAGE_CD = null;
            IsAgentRelated.PREFERRED_CALL_TIME = null;
            IsAgentRelated.IMP_RM_CAMS_CD = null;
            IsAgentRelated.IMP_AGENT_CAMS_CD = null;
            IsAgentRelated.SOURCER_CODE = null;
            IsAgentRelated.SOURCER_NAME = null;
            IsAgentRelated.SOURCER_CONTACT_NO = null;
            IsAgentRelated.IMP_RM_NAME = null;
            IsAgentRelated.IMP_AGENT_NAME = null;
            IsAgentRelated.IRDA_LIC_NO = null;
            IsAgentRelated.IRDA_LIC_EXP_DT = null;
            IsAgentRelated.MOBILENO = null;
            IsAgentRelated.CHANNEL_NAME = null;
            IsAgentRelated.OFFICE_CODE = null;
            IsAgentRelated.SUBOFFICECODE = null;
            IsAgentRelated.OFFICE_NAME = null;
            IsAgentRelated.OFFICE_ADDRESS1 = null;
            IsAgentRelated.OFFICE_ADDRESS2 = null;
            IsAgentRelated.OFFICE_STATE =  null;
            IsAgentRelated.OFFICE_ZIP_CODE = null;
            IsAgentRelated.OFFICE_OFFICE_TEL = null;
            IsAgentRelated.MODIFIED_DATE = null;
            IsAgentRelated.ISSYNCED = null;
            IsAgentRelated.AGENT_REL_FLAG = AGENT_REL_FLAG;

            debug("is agent related data when the no data available : "+JSON.stringify(IsAgentRelated));

            return IsAgentRelated;
    };

    this.setAgentRelFlagObject = function(agentRelFlagObject){
         debug("ser data "+JSON.stringify(agentRelFlagObject));
         this.agentRelFlagObject = agentRelFlagObject;
    };

    this.getAgentRelFlagObject = function(){
         debug("ger data "+JSON.stringify(this.agentRelFlagObject));
         return this.agentRelFlagObject;
    };
    this.checkIfAgentRelated = function(IsAgentRelated,DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID,refernceApplicationID){
        var dfd = $q.defer();

         this.updateIsRelatedFlag(IsAgentRelated).then(
            function(responce){
                debug("responce checkIfAgentRelated"+JSON.stringify(responce));
                if(!!responce){
                    if(responce){
                        debug("record updated successfully + checkIfAgentRelated"+IsAgentRelated.AGENT_REL_FLAG);

                        if(IsAgentRelated.AGENT_REL_FLAG == "Y"){

                             ccrs.getDocDataBasedOnUser(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID).then(
                                function(responce){
                                     var resp = (!!responce)? responce.inDatabase :false;
                                     dfd.resolve(resp);
                                     debug(" CCR Is Related Change"+JSON.stringify(resp));
                            });

                         }else{
                            dfd.resolve(false);
                         }
                    }else{
                        dfd.resolve(false);
                    }
                }else{
                    dfd.resolve(false);
                }

        });
         return dfd.promise;
    };

    ccrs.getDocDataBasedOnUser = function(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID){
           var dfd = $q.defer();
            var DOC_ID = DOC_ID;
            var AGENT_CD =  AGENT_CD;
            var APPLICATION_ID = APPLICATION_ID;
             var COMBO_ID = COMBO_ID;
            var index = 0;


            var docObj = {};
            docObj.DOC_ID=DOC_ID;
            docObj.FLAG=true;
            debug("DATA object : "+JSON.stringify(docObj));
            debug(">> DOC_ID >> "+DOC_ID+" >> AGENT_CD >> : "+AGENT_CD+" >> APPLICATION_ID >> "+APPLICATION_ID+"  >> COMBO_ID >>"+COMBO_ID+" >> refernceApplicationID >> ");
            var docObj = {};
             uploadDocService.checkIfDocPresentInDB(DOC_ID,AGENT_CD,APPLICATION_ID).then(
                function(ifPresent){
                    if(ifPresent){
                        debug("doc id present in database >>>"+DOC_ID);
                        docObj.selected = DOC_ID;
                        docObj.inDatabase = true;
                        dfd.resolve(docObj);
                    }else{
                        docObj.selected = DOC_ID;
                        docObj.inDatabase = false;
                        dfd.resolve(docObj);
                    }
                }
            );


            return dfd.promise;
    };

     ccrs.getUploadedDocumentExtentions = function(DOC_ID,AGENT_CD,APPLICATION_ID,COMBO_ID){
           var dfd = $q.defer();
            var DOC_ID = DOC_ID;
            var AGENT_CD =  AGENT_CD;
            var APPLICATION_ID = APPLICATION_ID;
            var COMBO_ID = COMBO_ID;
            var index = 0;


            var docObj = {};
            docObj.DOC_ID=DOC_ID;
            docObj.FLAG=true;
            debug("DATA object : "+JSON.stringify(docObj));
            debug(" getUploadedDocumentExtentions >> DOC_ID >> "+DOC_ID+" >> AGENT_CD >> : "+AGENT_CD+" >> APPLICATION_ID >> "+APPLICATION_ID+"  >> COMBO_ID >>"+COMBO_ID+" >> refernceApplicationID >> ");
            var docObj = {};
             uploadDocService.getDocInfoFormDb(DOC_ID,AGENT_CD,APPLICATION_ID).then(
                function(ifPresent){
                    if(ifPresent.STATUS){
                        debug("doc id present in database >>>"+DOC_ID);
                        docObj.selected = DOC_ID;
                        docObj.inDatabase = true;
                        docObj.docName = ifPresent.DOC_NAME;
                        dfd.resolve(docObj);
                    }else{
                        docObj.selected = DOC_ID;
                        docObj.inDatabase = false;
                       // docObj.docName = ifPresent.DOC_NAME;
                        dfd.resolve(docObj);
                    }
                }
            );


            return dfd.promise;
    };


      this.setAppDataMYOPPORTUNITY = function(lpAppData){
        debug("set App Data MY OPPORTUNITY"+ JSON.stringify(lpAppData));
        this.AppDataMYOPPORTUNITY = lpAppData;
      };
      this.getAppDataMYOPPORTUNITY = function(){
        debug("get App Data MY OPPORTUNITY"+ JSON.stringify(this.AppDataMYOPPORTUNITY));
        return this.AppDataMYOPPORTUNITY ;
      }



}]);
