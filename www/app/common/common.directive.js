commonModule.directive('touchTimeout',['$timeout', '$document', 'CommonService', '$state', 'OverlayMenuService', function($timeout, $document, CommonService, $state, OverlayMenuService){
	return {
		restrict: 'A',
		link: function(scope, elem, attr, ctrl) {
			var elemClickHandler = function(e) {
				debug("touched " + $state.current.name);
				//e.stopPropagation();
				$timeout.cancel(CommonService.timeout);
				if($state.current.name != LOGIN_STATE){
					debug("setting timeout");
					CommonService.timeout = $timeout(
						function(){
							debug("Log out");
							OverlayMenuService.logout();
						},(1000 * 60 * SESSION_TIMEOUT) // 15 minutes
					);
				}
			};
			debug("elem: " + elem);
			elem.on('touchend', elemClickHandler);
			if(attr.touchTimeout == "OnLoad"){elemClickHandler();}
		}
	}
}]);
/*commonModule.directive('expireLoginCookie',['$timeout', '$document', 'CommonService', '$state', 'OverlayMenuService', function($timeout, $document, CommonService, $state, OverlayMenuService){
    	return {
    		restrict: 'A',
    		link: function(scope, elem, attr, ctrl) {
    			var elemClickHandler = function(e) {
    				debug("touched " + $state.current.name);
    				//e.stopPropagation();
    				$timeout.cancel(CommonService.timeout);
    				if($state.current.name != LOGIN_STATE){
    					debug("setting timeout");
    					CommonService.timeout = $timeout(
    						function(){
    							debug("Log out");
    							OverlayMenuService.logout();
    						},(1000 * 60 * SESSION_TIMEOUT) // 15 minutes
    					);
    				}
    			};

    			elem.on('touchend', elemClickHandler);
    			if(attr.touchTimeout == "OnLoad"){elemClickHandler();}
    		}
    	}
    }]);*/



commonModule.directive('selectRequired',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			'selectRequired': '='
		},
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					if(scope.selectRequired!=false){
						if(!!ngModelValue && (ngModelValue instanceof Object) && ngModelValue!={}){
							var keys = Object.keys(ngModelValue);
							ctrl.$setValidity('selectRequired', true);
							angular.forEach(keys,function(key){
								if(!!ngModelValue[key] && !(ngModelValue[key] instanceof Array) && ngModelValue[key].length>0 && ngModelValue[key].substring(0,7)=="Select "){
									ctrl.$setValidity('selectRequired', false);
									debug("key: " + key + " " + "ngModelValue[key]: " + ngModelValue[key]);
									debug("setting validity: false");
								}
							});
						}
						else if(!!ngModelValue && ngModelValue==="")
							ctrl.$setValidity('selectRequired', false);
					}
					else
						ctrl.$setValidity('selectRequired', true);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
});

commonModule.directive('inputUppercase',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function convertToUppercase(ngModelValue){
					if(!!ngModelValue){
						debug("ngModelValue: " + ngModelValue);
						return ngModelValue.toUpperCase();
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(convertToUppercase); // when user manually changes the value
                ctrl.$formatters.unshift(convertToUppercase); // when model is updated in the controller
			}
	};
});


commonModule.directive('autoCapitalize', ['$parse', function($parse) {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
        if(!!inputValue){
        debug("inputValue:"+inputValue);
           var capitalized = inputValue.toUpperCase();
           //if(capitalized !== inputValue) {
           		debug("capitalized :"+capitalized);
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
         //   }
            return capitalized;
          }
         }
         modelCtrl.$parsers.unshift(capitalize); // when user manually changes the value
         modelCtrl.$formatters.unshift(capitalize); // when model is updated in the controller
     }
   };
}]);

commonModule.directive('validateDob',['CommonService', function(CommonService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					debug("ngModelValue: " + JSON.stringify(ngModelValue));
					if(!!ngModelValue){
						var newDateFormat = ngModelValue.substring(8,10) + "-" + ngModelValue.substring(5,7) + "-" + ngModelValue.substring(0,4);
						var date = newDateFormat + " 00:00:00";
						if(CommonService.getAge(date)<ADULT_AGE)
							ctrl.$setValidity('minAge', false);
						else
							ctrl.$setValidity('minAge', true);
					}
					else if(!!ngModelValue && ngModelValue===""){
						ctrl.$setValidity('minAge', true);
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('validatePreferedDate',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					debug("ngModelValue: " + ngModelValue);
					if(!!ngModelValue){
					    ctrl.$setValidity('validatePreferedDate', true);
					    var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
					    var days2allowed = ["Sunday","Monday","Tuesday","Wednesday","Thursday"];
					    var currDay = days[new Date().getDay()];
					    debug("currDay : " + currDay);
                        var maxDate = new Date();
                        debug("new Date(ngModelValue) : " + new Date(ngModelValue));
                        var maxDate1 = new Date(maxDate.getFullYear() + '-' + (maxDate.getMonth() + 1) + '-' + maxDate.getDate());


                        debug("maxDate : " + maxDate);
                        debug("maxDate1 : " + maxDate1);
                        if(!!maxDate1 && (maxDate1>(new Date(ngModelValue)))){
                            ctrl.$setValidity('validatePreferedDate', false);
                        }
					    else if(days2allowed.indexOf(currDay)>0){
					        debug("1")
                            maxDate1.setDate(maxDate1.getDate()+2);
                            debug("maxDate1 : " + maxDate1);//Condition changed - Drastty
					        if(!!maxDate1 && (maxDate1 >= (new Date(ngModelValue))))
					            ctrl.$setValidity('validatePreferedDate', false);
					    }
					    else if(currDay === "Friday"){
					        debug("2")
					        maxDate1.setDate(maxDate1.getDate()+4);
					        debug("maxDate1 : " + maxDate1);
                            if(!!maxDate1 && (maxDate1 >= (new Date(ngModelValue))))
                                ctrl.$setValidity('validatePreferedDate', false);
					    }else if(currDay === "Saturday"){
					        debug("3")
					        maxDate1.setDate(maxDate1.getDate()+3);
					        debug("maxDate1 : " + maxDate1);
                            if(!!maxDate1 && (maxDate1 >= (new Date(ngModelValue))))
                                ctrl.$setValidity('validatePreferedDate', false);
					    }
					}
					else if(!!ngModelValue && ngModelValue===""){
						ctrl.$setValidity('validatePreferedDate', true);
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
});

commonModule.directive('compareDate',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			'compareDate': '='
		},
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
					ctrl.$setValidity('compareDate', true);
					var dateToCompare = new Date(scope.compareDate);
					if(!!dateToCompare)
					    dateToCompare.setDate(dateToCompare.getDate()+1);
					debug("after add 1 : " + dateToCompare);
				    if((new Date(ngModelValue)) < dateToCompare){
				        ctrl.$setValidity('compareDate', false);
				    }
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
});

commonModule.directive('incmproofDate',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			'incmproofDate': '='
		},
		link:
			function(scope, element, attr, ctrl){
				function customValidator(ngModelValue){
				    debug((new Date(ngModelValue)) + " :: incmproofDate :: ");
					ctrl.$setValidity('incmproofDate', true);
					var currDate = new Date();
					debug("Curr Date : " + currDate);
				    if((new Date(ngModelValue)) > currDate){
				        ctrl.$setValidity('incmproofDate', false);
				    }
					var before3Months = new Date(currDate.setMonth(currDate.getMonth() - 3));
					debug("before3Months : " + before3Months);
					if((new Date(ngModelValue)) < before3Months){
						ctrl.$setValidity('incmproofDate', false);
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(customValidator); // when user manually changes the value
                ctrl.$formatters.unshift(customValidator); // when model is updated in the controller
			}
	};
});

commonModule.directive('blockHash',function(){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function blockHash(ngModelValue){
						ctrl.$setValidity('blockHash', true);
					 if(!!ngModelValue && !BLOCKHASH.test(ngModelValue)){
						ctrl.$setValidity('blockHash', false);
					}

					return ngModelValue;
				}
				ctrl.$parsers.unshift(blockHash); // when user manually changes the value
                ctrl.$formatters.unshift(blockHash); // when model is updated in the controller
			}
	};
});

commonModule.directive('contactRequired',['CommonService', function(CommonService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
        	'dob' : '=',
        	'self' : '='
        },
		link:
			function(scope, element, attr, ctrl){
				function contactRequired(ngModelValue){
				var dob = scope.dob;
				var age = CommonService.getAge(scope.dob);
				ctrl.$setValidity('contactRequired', true);
					if(((ngModelValue==undefined || ngModelValue=="" || ngModelValue==" ") && parseInt(age)>18) || ((ngModelValue==undefined || ngModelValue=="" || ngModelValue==" ") && scope.self == 'Y'))
						{
						ctrl.$setValidity('contactRequired', false);
						}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(contactRequired); // when user manually changes the value
                ctrl.$formatters.unshift(contactRequired); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('addrProofRequired',['CommonService','ApplicationFormDataService', function(CommonService,ApplicationFormDataService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
        	'fatf' : '=',
        	'addrProofRequired' : '='
        },
		link:
			function(scope, element, attr, ctrl){
				function addrProofRequired(ngModelValue){
				if(scope.addrProofRequired == undefined || scope.addrProofRequired!=false){
					ctrl.$setValidity('addrProofRequired', true);
					if(!!ngModelValue)
					{
						var keys = Object.keys(ngModelValue);
						angular.forEach(keys,function(key){
								if((ngModelValue["MPPROOF_DESC"]==undefined || ngModelValue["MPPROOF_DESC"] == "Select " || ngModelValue["DOC_ID"] ==undefined) && (parseInt(ApplicationFormDataService.SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT)>10000 || scope.fatf == 'Y'))
									ctrl.$setValidity('addrProofRequired', false);

						});
					}
					}
					else
						ctrl.$setValidity('addrProofRequired', true);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(addrProofRequired); // when user manually changes the value
                ctrl.$formatters.unshift(addrProofRequired); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('orgTypeRequired',['CommonService','ApplicationFormDataService', function(CommonService,ApplicationFormDataService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
        	'occClass' : '='
        },
		link:
			function(scope, element, attr, ctrl){
				function addrProofRequired(ngModelValue){
					ctrl.$setValidity('orgTypeRequired', true);
					var dob = ApplicationFormDataService.SISFormData.sisFormAData.INSURED_DOB;
					var age = CommonService.getAge(dob);
					if(!!ngModelValue)
					{
						var keys = Object.keys(ngModelValue);
						angular.forEach(keys,function(key){
						if(!!ngModelValue[key] && ngModelValue[key].length>0 && ngModelValue[key].substring(0,7)=="Select "){
								if(parseInt(age)>18 && scope.occClass.NBFE_CODE!="4" && scope.occClass.NBFE_CODE!="6")
									ctrl.$setValidity('orgTypeRequired', false);

								debug("scope.occClass :"+scope.occClass.NBFE_CODE+"key: " + key + " " + "ngModelValue[key]: " + ngModelValue[key]);
								debug("setting validity: false");
							}

						});
					}
					return ngModelValue;
				}
				ctrl.$parsers.unshift(addrProofRequired); // when user manually changes the value
                ctrl.$formatters.unshift(addrProofRequired); // when model is updated in the controller
			}
	};
}]);
commonModule.directive('selfMandate',['CommonService','ApplicationFormDataService', function(CommonService,ApplicationFormDataService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function selfMandate(ngModelValue){
					ctrl.$setValidity('selfMandate', true);
					var isSelf = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED;
						if(isSelf == 'Y' && (ngModelValue==undefined || ngModelValue==""))
							ctrl.$setValidity('selfMandate', false);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(selfMandate); // when user manually changes the value
                ctrl.$formatters.unshift(selfMandate); // when model is updated in the controller
			}
	};
}]);
commonModule.directive('incProofRequired',['CommonService','ApplicationFormDataService', function(CommonService,ApplicationFormDataService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
        	'cust' : '=',
        	'occClass' : '=',
        	'incProofRequired' : '='
        },
		link:
			function(scope, element, attr, ctrl){
				function incProofRequired(ngModelValue){
				if(scope.incProofRequired == undefined || scope.incProofRequired !=false){
				var isSelf = ApplicationFormDataService.SISFormData.sisFormAData.PROPOSER_IS_INSURED;
				var AnnualPrem = ApplicationFormDataService.SISFormData.sisMainData.ANNUAL_PREMIUM_AMOUNT;
				var pgl_id = ApplicationFormDataService.SISFormData.sisMainData.PGL_ID;
				var fatf;
				if(ApplicationFormDataService.applicationFormBean.addressDetBean !=undefined){
				if(scope.cust == 'IN')
					fatf = ApplicationFormDataService.applicationFormBean.addressDetBean.InsFATFFlag || 'N';
				else
					fatf = ApplicationFormDataService.applicationFormBean.addressDetBean.ProFATFFlag || 'N';
				}
				else
				fatf = 'N';

				var incomeCond = ((scope.cust == 'PR' || isSelf == 'Y') && (parseInt(AnnualPrem)>99999 || fatf == 'Y' || scope.occClass.NBFE_CODE == '5'));
				var irsCond = (pgl_id == '191' || pgl_id == '185' || pgl_id == SR_PGL_ID || pgl_id == SRP_PGL_ID);
				ctrl.$setValidity('incProofRequired', true);

				if(!!ngModelValue)
				{
					var keys = Object.keys(ngModelValue);
					angular.forEach(keys,function(key){
					if(!!ngModelValue[key] && ngModelValue[key].length>0 && ngModelValue[key].substring(0,7)=="Select "){
							if(incomeCond || irsCond)
								ctrl.$setValidity('incProofRequired', false);
					}

					});
				}
				}
				else
					ctrl.$setValidity('incProofRequired', true);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(incProofRequired); // when user manually changes the value
                ctrl.$formatters.unshift(incProofRequired); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('insObjRequired',['CommonService', function(CommonService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		link:
			function(scope, element, attr, ctrl){
				function insObjRequired(ngModelValue){
				ctrl.$setValidity('insObjRequired', true);
					if(ngModelValue)
						{
							ctrl.$setValidity('insObjRequired', true);
						}
						else
							ctrl.$setValidity('insObjRequired', false);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(insObjRequired); // when user manually changes the value
                ctrl.$formatters.unshift(insObjRequired); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('eiaRequired',['CommonService', function(CommonService){
	"use strict";
	return {
		restrict: 'A',
		require: 'ngModel',
		scope:{
			'eiaFlag' : '='
		},
		link:
			function(scope, element, attr, ctrl){
				function eiaRequired(ngModelValue){
				var eiaFlag = scope.eiaFlag;
				ctrl.$setValidity('eiaRequired', true);
					if(eiaFlag == true && (ngModelValue == undefined || ngModelValue == ""))
						{
							ctrl.$setValidity('eiaRequired', false);
						}
						else
							ctrl.$setValidity('eiaRequired', true);
					return ngModelValue;
				}
				ctrl.$parsers.unshift(eiaRequired); // when user manually changes the value
                ctrl.$formatters.unshift(eiaRequired); // when model is updated in the controller
			}
	};
}]);

commonModule.directive('validNumber', ['CommonService',function(CommonService) {
	"use strict";
    return {
    	// limit usage to argument only
        restrict: 'A',
        // require NgModelController, i.e. require a controller of ngModel directive
        require: 'ngModel',
        // create linking function and pass in our NgModelController as a 4th argument
        link: function(scope, element, attr, ctrl) {
            function validNumber(ngModelValue) {
                var moduloBy = 1000;
                if (ngModelValue%moduloBy != 0) {
                    ctrl.$setValidity('modValidator', false);
                } else {
                    ctrl.$setValidity('modValidator', true);
                }
                return ngModelValue;
            }
            ctrl.$parsers.unshift(validNumber); // when user manually changes the value
            ctrl.$formatters.unshift(validNumber); // when model is updated in the controller
        }
    };
}]);

/* Do not use cut copy paste */

commonModule.directive('stopccp', function(){

		   return {

				restrict : 'A',

				scope: {},

				link:function(scope,element){

					element.on('cut copy paste', function (event) {

					event.preventDefault();

					});

				}

			};

});

commonModule.directive('dblClick2Zoom',['CommonService', function (CommonService) {
	"use strict";
	var DblClickInterval = 300; //milliseconds
	var firstClickTime;
        var waitingSecondClick = false;
		var lastZoom = true;
    return {
		restrict: 'A',
		link: function (scope, element, attrs) {
                element.bind('click', function (e) {

                    if (!waitingSecondClick) {
                        firstClickTime = (new Date()).getTime();
                        waitingSecondClick = true;

                        setTimeout(function () {
                            waitingSecondClick = false;
                        }, DblClickInterval);
                    }
                    else {
                        waitingSecondClick = false;

                        var time = (new Date()).getTime();
                        if (time - firstClickTime < DblClickInterval) {
							if(!!lastZoom) {
								element.css("zoom", 1.5);
								lastZoom = false;
							}
							else{
								element.css("zoom", 1);
								lastZoom = true;
							}
                        }
                    }
                });
            },
	};
}]);

commonModule.directive('validateIssuance', ['CommonService', 'validationService', function(CommonService, validationService){
	"use strict";
	debug("inside validateIssuance");
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			vi: "=validateIssuance",
			'issucond' : '='
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.issuanceDate = function(ngModelValue, viewValue){
					debug("validateIssuance: " + viewValue);
					var valid = validationService.validateIssuanceDate(viewValue) || null;
					debug("scope.issuCond: " + scope.issucond);
					debug("valid::: " + JSON.stringify(valid));
					if(scope.issucond==undefined || scope.issucond!=false){
						if(!!valid){
							scope.vi = valid.msg;
							return valid.flag;
						}
						else{
							scope.vi = "Something went wrong";
							return false;
						}
					}
					else
						{
							debug("inside validateIssuance else condition");
							scope.vi = '';
							return true;
						}
				};
			}
	}
}]);

commonModule.directive('dateExpiry',['validationService',function(validationService){
	"use strict";
	debug("inside dateExpiry");
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			de: "=dateExpiry",

		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.expiryDate = function(ngModelValue, viewValue){
					debug("expiryDate: " + viewValue);
					if(scope.de == undefined || scope.de !=false)
						return validationService.validateExpiryDate(viewValue) || false;
					else
						return true;
				};
			}
	}

}])

commonModule.directive('dateBill',['validationService',function(validationService){
	"use strict";
	debug("inside dateBill");
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			dbi: "=dateBill"
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.billDate = function(ngModelValue, viewValue){
					debug("billDate: " + viewValue);
					if(scope.dbi == undefined || scope.dbi != false) {
						return validationService.validateBillDate(viewValue) || false;
					} else
						return true;
				};
			}
	}
}
])

commonModule.directive('dateValidations', ['CommonService', 'validationService', function(CommonService, validationService){
	"use strict";
	debug("inside dateValidations");
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			dv: "=dateValidations",
			'datecond' : '='
		},
		link:
			function(scope, element, attr, ctrl){
				ctrl.$validators.dobMismatch = function(ngModelValue, viewValue){
					if(scope.datecond==undefined || scope.datecond!=false){

						var newDateFormat = viewValue.substring(8,10) + "-" + viewValue.substring(5,7) + "-" + viewValue.substring(0,4);
						var ageProofDate = newDateFormat + " 00:00:00";
						debug("ageProofDate: " + ageProofDate);
						var birthDate = ((!!scope.dv)?(CommonService.formatDobToDb(scope.dv)):null);
						debug("birthDate: " + birthDate);
						if(ageProofDate != birthDate) {
							return false;
						}
						else {
							return true;
						}
					}
					else
						return true;
				};
			}
	}
}]);
