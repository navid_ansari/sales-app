
	var BUSINESS_TYPE = null;

	var IS_DEBUG = true; // toggle this to turn on / off for global control
	var debug = function(){};

	var VERSION_NO = "2.0.5";

	var APP_NAME = "GoodConnect";

	var FORCED_DISABLE_EKYC = false;

	var IMG_TYPE_JPG = ".jpg";
	var IMG_TYPE_PNG = ".png";


	var MIME_PLAIN = "text/plain";
	var MIME_IMAGE = "text/image";

	var LANGUAGE = "EN";

	var SEC_QUES_LIMIT = 3;

	var ADULT_AGE = 18;
	var VCP_PROPOSER_AGE = 65;

	var SESSION_TIMEOUT = 15; // 15 minutes


/***** AJAX CONSTANTS START *****/

	var AJAX_TYPE = "POST";
	var TYPE_JSON = "application/json";
	var AJAX_ASYNC = true;
	var AJAX_TIMEOUT = 2;

/***** AJAX CONSTANTS END *****/


/************ Cookie timeout   Start  *********************/

var LOGINSTORAGEEXPIRETIME = 24*60*60*1000;

/************ Cookie timeout   End  *********************/



/***** RELATIVE PATHS START *****/

	var REL_PATH_PROFILE = "/ToClient/Profile/";
	var REL_PATH_PRODUCTS = "/ToClient/Products/";
	var REL_PATH_RESOURCES = "/ToClient/MyResources/";

/***** RELATIVE PATHS END *****/


/***** APP STATES START *****/

	var LOGIN_STATE = "login";
	var DASHBOARD_HOME_STATE = "dashboard.home";


	var PROFILE_STATE = "app.profile";
	var FTL_CP_STATE = "ftlSetPwd";
	var CP_DONE_STATE = "cpDone";
	var OPT_DOWNLOADS_STATE = "app.optionalDownloads";
	var PR_OPT_DOWNLOADS_STATE = "app.optionalDownloads.products";
	var TOOLKIT_STATE = "app.toolkit";
	var CHANGE_PASSWORD_STATE = "changePassword";
	var CHANGE_SEC_QUES_STATE = "chgSecQues";
	var FORGOT_PASSWORD_STATE = "forgotPassword";

	/** LEAD - SUMIT START **/
	var LEAD = "addLead";
    var LEAD_DETAILS = "LeadDetails";
	var LEAD_LISTING_STATE = "leadListing";
    /** LEAD - SUMIT END **/

	/** FF - SUMIT START **/
	var FACT_FINDER_STATE  = "ffPage1";
    var FACT_FINDER_STATE1 = "ffPage2";
    var FACT_FINDER_STATE2 = "ffPage3";
    var FACT_FINDER_STATE3 = "ffPage4";
    var FACT_FINDER_STATE4 = "ffPage5";
	/** FF - SUMIT END **/

	/** FHR - AMEYA START **/
	var FHRM = "fhrMain";
    var FHR_PERSONAL_DETAILS = "fhrPersonalDetails";
    var FHR_OTHERS = "fhrOthers";
    var FHR_GOALS = "fhrGoals";
    var FHR_OUTPUT = "fhrOutput";
    var FHR_RECOMMENDATIONS = "fhrRecommendations";

    var FHR_CHILD_GOALS = "childGoals";
    var FHR_RETIRE_FUND = "retireFund";
    var FHR_PROTECTION = "protection";
    var FHR_WEALTH_CREATION = "wealthCreation";
	var FHR_GOALS_SUCCESS = "goalsSuccess";
	var FHR_GOALS_OUTPUT = "goalsOutput";

    /** FHR - AMEYA END **/

/***** APP STATES END *****/


/***** SERVLET URLS START *****/

	//var DOMAIN_URL = "https://lpu.tataaia.com/LifePlaner/"; // UAT
	var DOMAIN_URL = "https://lp.tataaia.com/LifePlaner/"; // production


	var FINGERPRINT = "31 0e a1 c7 51 a0 42 b7 44 d6 b4 a7 44 d9 8d 0e 16 ac 6f 6b";

	var LOGIN_SERVLET_URL = DOMAIN_URL + "LoginServlet";
	var USER_INFO_SERVLET_URL = DOMAIN_URL + "UserInfoServlet";
	var CATALOG_SERVLET_URL = DOMAIN_URL + "CatalogServlet";
	var DOCUMENT_SERVLET_URL = DOMAIN_URL + "DocumentServlet";
	var POLICY_SERVLET_URL = DOMAIN_URL + "PolicyServlet";
	var PHOTO_SERVLET_URL = DOMAIN_URL + "PhotoServlet";
	var DOC_UPLOAD_SYNC_URL = DOMAIN_URL + "UploadDataServlet";

	var EMAIL_SYNC_URL = DOMAIN_URL + "EmailDataSevlet";

	var SIS_DATA_SYNC_URL = DOMAIN_URL + "SISServlet";
	var SIS_FILE_SYNC_URL = DOMAIN_URL + "AndroidSISHtmlServlet";

	var COMBO_DATA_SYNC_URL = DOMAIN_URL + "ComboDataSyncServlet";
	var COMBO_DOC_UPLOAD_SYNC_URL = DOMAIN_URL + "ComboFileDataUploadServlet";
	var COMBO_IMAGE_SYNC_URL = DOMAIN_URL + "ComboSyncImageDocServlet";
	var VALIDATE_SP_SERVLET_URL = DOMAIN_URL + "RMSPDetailsServlet";
	var TERM_VALIDATION_URL = DOMAIN_URL + "SRValidationServlet";
	//Drastty
	var FACTFINDER_SYNC_URL = DOMAIN_URL + "FactFinderServlert";
	var APP_DATA_SYNC_URL = DOMAIN_URL + "ApplicationFormDataServlet";
	var APP_AGNT_IMP_URL = DOMAIN_URL + "SoucerIMPSNTServlet";
	var MEDICAL_GRID_ENQUIRY_SERVLET_URL = DOMAIN_URL + "MedGridEnquiryServlet";
	var MTRF_DETAILS_SERVLET_URL = DOMAIN_URL + "MTRFDetailsServlet";

	//camera DOC
	var DOCUMENT_UPLOAD_URL = DOMAIN_URL + "UploadDataServlet";
    var DOCUMENT_FILE_UPLOAD_URL = DOMAIN_URL + "SyncImageDocServletTest";
    var HTML_SYNC_URL = DOMAIN_URL + "SyncCommonFileDataServlet";

	// FHR URL
	var FHR_DATA_SYNC_URL = DOMAIN_URL + "FHRServlet";
	var FHR_FILE_SYNC_URL = DOMAIN_URL + "FHRPDFFormServlet";
	var FHR_DEVIATION_DATA_URL = DOMAIN_URL + "MyOpportunityServlet";
	var FHRDevURL = DOMAIN_URL + "DeviationPDFServlet";

	var EKYC_SERVLET = DOMAIN_URL + "BioMetricAuthenticationServlet";

	var BUSINESS_TYPE_SERVLET = DOMAIN_URL + "BTServlet";
	var DATA_RESTORE_SERVLET = DOMAIN_URL + "DataRestoreServlet";
	var FILE_RESTORE_SERVLET = DOMAIN_URL + "FileRestoreServlet";
	var SYNC_COMPLETE_SERVLET = DOMAIN_URL + "DataSyncCompleteServlet";


	var FETCH_LEAD_APPOINTMENT_URL = DOMAIN_URL + "LeadServlet";
	var LINK_POLICY_DATA_URL = DOMAIN_URL + "LeadPolicyServlet";
	var SYNC_LEAD_DATA_URL = DOMAIN_URL + "SyncLeadDataServlet";
	var FETCH_OPPORTUNITY_DATA_URL = DOMAIN_URL + "MyOpportunityServlet";
	var APPOINTMENT_DATA_URL = DOMAIN_URL + "SyncAppointmentDataServlet";

	var DUPLICATE_DATA_URL = DOMAIN_URL + "DuplicationServlet";
	var DUPLICATE_APP_URL = DOMAIN_URL + "DeDupAppDataServlet";
	//refresh button url
	var REFRESH_DASHBOARD_DATA = DOMAIN_URL + "FetchPolicyServlet";

	//Agent Info servlet
	var AGENT_DATA = DOMAIN_URL + "AgentInfoServlet";
	var AGENT_DETAILS = DOMAIN_URL + "AgentDetailsServlet";

/***** SERVLET URLS END *****/


/***** REGEX START *****/

	var USERNAME_REGEX = /^[a-zA-Z0-9]*$/;
	var SOURCER_REGEX=  /^([CR]|[0-9]){1}(0*[1-9][0-9]*([0-9]+)?|0+[0-9]*[1-9][0-9]*)$/;

	//var SOURCER_REGEX = /^([Cc|Rr]{1}(?!0{5})[0-9]{5})|((?!0{5})([0-9]{5}))$/;

	var NAME_REGEX = /^([a-zA-Z]){1}([a-zA-Z'-]*)$/;
	var MOBILE_REGEX = /^[7-9][0-9]{9}$/;
	var EMAIL_REGEX = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/;

	var ZIPCODE_REGEX=/^(\d{5}-\d{4}|\d{6})$/; // SUMIT
	var FULLNAME_REGEX=/^([a-zA-Z]){1}([a-zA-Z ]*)$/;
	var FULLNAME_REGEX_SP=/^([a-zA-Z]){1}([a-zA-Z '-]*)$/;
	var MAND_REG = /\S/;
	var ALPHA_REG = /^[a-zA-Z]{1,150}$/;
	var NUM_REG = /^[0-9]*$/;
	var ADHAAR_REG = /^[0-9]{12}$/;
	var eIANo_REG = /^[0-9]{1,13}$/;
	var PAN_REG = /^([a-zA-Z]){3}([PCHFATBLJGpchfatbljg]){1}([a-zA-Z]){1}([0-9]){4}([a-zA-Z]){1}?$/;
	var VERNAC_WITNESS_NAME_REGEX = /^([a-zA-Z]){1}([a-zA-Z'-.\s]*)$/;
	var ALPHA_WITHSPACE_REG = /^[a-zA-Z\s]*$/;
	var ALPHA_NUM_WITHSPACE_REG = /^[a-zA-Z\s0-9]*$/;
	var DATE_REG = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
	var PINCODE_REG = /^[1-9][0-9]{5}$/;
	var BLOCKHASH = /^[^#|]*$/;
	var IFSC_REG = /^([A-Za-z]){4}([A-Za-z0-9]){7}$/;
	var AGENT_CD_REGEX = /^[1-9][0-9]*$/;

	var FATCA_ADD_REGEX = /^[a-zA-Z0-9 ,/().'-@]*$/;
	var FATCA_ADD_PIN = /^[a-zA-Z0-9 -]*$/;

	var FREE_TEXT_REGEX = /^[a-zA-Z0-9 ,/().'-]*$/;
/***** REGEX END *****/

/* Sync Messages */
	var SIS_SUBMIT_SUCC = 'Email request has been synced to server successfully !!';
	var SIS_SAVE_SUCC = 'SIS saved successfully!!';
	var SIS_SAVE_FAIL = 'SIS not saved.\nCheck your data & Try again.';
	var APP_SAVE_SUCC = 'Application saved successfully !!';
	var APP_SAVE_FAIL = 'Application form not saved.\nCheck your data & Try again.';
	var APP_SUBMIT_SUCC = 'Application submitted successfully!!';
	var APP_DATA_SYN_MSG = "Application Data Synced Successfully";
	var APP_EXIT_CONFIRM = 'Are you sure you want to exit \nwithout saving Application output?';
	var APP_SAVE_CONFIRM = 'Please verify your entire data\n before saving.Do you want to continue?';
	var SIS_REQ_FAIL_MSG = 'Email request has not been synced.\nTry again later';
	var REQ_FAIL_MSG = 'Request has not been synced.\nTry again later.';
	var SERVER_FAIL_MSG = 'Server not responding...\nContact Helpdesk.';
	var NO_NETWORK_MSG = 'Check your internet connection\nand Try again later.';
	var OFFLINE_SYNC_MSG = 'Your are in offline mode.\nPlease sync your data online.';
	var REQ_TIMEOUT_MSG = 'Request Timed out...\nTry again later.';
	var DOC_PHOTOERR_MSG = 'Document not saved.\nPlease click photo again.';
	var PAYMENT_NOTALL_MSG = 'You are not allowed to do payment.';
	var POLICYNO_MSG = 'No Policy Number.\nPlease proceed with online login once.';
	var NO_DATA_TO_SAVE_MSG = 'Data not saved.\n Kindly re-enter the data and try again.';
	var OFFLINE_PAY_MSG = 'Please be online for\n online payment option';

	var INCOMPLETE_DATA_MSG = "Please complete your form";
    var VALID_AGNT="Agent is active and eligible to sell policy.";
    var LIC_EXP="License has expired. Please renew your License for logging policies online.";
    var AGNT_INACTIVE="Advisor Code is Inactive in the system. Please get in touch with your Branch Ops to Login policies online.";
    var LIC_NOT_COLLECTED="License has not been collected by Advisor. Please collect the same for logging policies online.";
    var MAND_TRAIN="Advisor has not completed mandatory training. Please complete your training for logging policies online.";
    var ULIP_TRAIN="Advisor has not completed ULIP training. Please complete your training for logging policies online.";
    var AGNT_NOT_FOUND="Agent code not found";

	var INSURED_CODE = "IN";
	var PROPOSER_CODE = "PR";

	var INSURED_CUST_TYPE = "C01";
	var PROPOSER_CUST_TYPE = "C02";

/***** PGL IDs *****/

	var ITRP_PGL_ID = "185";
	var MRS_PGL_ID = "187";
	var MLS_PGL_ID = "188";
	var SEC7_PGL_ID = "189";
	var MMX_PGL_ID = "190";
	var IRS_PGL_ID = "191";
	var MLM_PGL_ID = "193";
	var MLGP_PGL_ID = "194";
	var MLG_PGL_ID = "195";
	var SM7_PGL_ID = "198";
	var IPR_PGL_ID = "199";
	var IMX_PGL_ID = "200";
	var WMX_PGL_ID = "201";
	var WPR_PGL_ID = "202";
	var IONE_PGL_ID = "203";
	var SGP_PGL_ID = "204";
	var MBP_PGL_ID = "205";
	var IWP_PGL_ID = "206";
	var FG_PGL_ID = "211";
	var SUA_PGL_ID = "212";
	var MIP_PGL_ID = "215";
	var FR_PGL_ID = "217";
	var SIP_PGL_ID = "219";
	var GK_PGL_ID = "221";
	var VCP_PGL_ID = "222";
	var SR_PGL_ID = "224";
	var SRP_PGL_ID = "225";
	var GIP_PGL_ID = "226";

/***** PGL IDs *****/


var MAIN_TAB = 'lifeStage';
var SUB_TAB = 'personalDetails';
var APP_HTML_PATH = "FromClient/APP/HTML";
var SIS_HTML_PATH = "";
    /* camera */
    var RES_ADD_PROOF = "Residential Address Proof";
    var MED_DOCS = "Medical Documents";
    var ID_PROOF = "Identity Proof";
    var PERM_ADD_PROOF = "Permanent Address Proof";
    var INC_PROOF = "Income Proof";
    var PHOTOGRAPH = "Photograph";
    var OTHER = "other";
    var RES_PROOF_PAYOR = "Residence Proof for Payor";
    var CUST_DEC_FORM = "Customer Declaration Form";
    var SIS_ATTACH = "SIS Attachment";
    var APP_FORM = "Application Form";
    var ATTENDANCE = "Attendance";
    var FIN_DOC = "Financial Document";
    var STAND_INST = "Standing Instruction";
    var REL_PROOF = "Relationship Proof";
    var ADD_PROOF = "Address Proof";
    var TRAVEL_QUEST = "Travel Questionnaire";
    var OCC_QUEST = "Occupation Questionnaire";
    var HOBB_QUEST = "Hobbies Questionnaire";
    var HEALTH_QUEST = "Health  Questionnaire";
    var NAT_QUEST = "Nationality Questionnaire";
    var FIN_QUEST = "Financial Questionnaire";
    var AGE_PROOF = "Age Proof";
    var NEFT_REG = "NEFT Registration";
    var AGENT_DEC_FORM = "Agent Declaration Form";
    var SIGN = "Signature";


    var ageproof = "ageproof";
    var idproof = "idproof";
    var addrproof = "addrproof";
    var incproof = "incproof";
    var form60 = "form60";
    var nri = "nri";
    var oci = "oci";
    var LSQuest1 = "LSQuest1";
    var HDQuest4 = "HDQuest4";
    var HDQuest5 = "HDQuest5";
    var HDQuest9C = "HDQuest9C";
    var bankDetail = "bankDetail";
    var paymentOption = "paymentOption";
    var TALIC_MERCHANT_ID = "BillDesk";

var PSC_AGENT_CD = '';
var PSC_APP_ID = '';
var PSC_POL_ARRAY = [];
