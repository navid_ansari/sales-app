commonModule.service('CommonService',['$q','$http', 'panels', '$ionicLoading', '$uibModal', '$state', function($q, $http, panels, $ionicLoading, $uibModal, $state, $window, $localStorage, $sessionStorage){
	"use strict";
	var cs = this;

	(function(){
		//ES6 Object assign polyfill
		if (typeof Object.assign != 'function') {
			Object.assign = function(target) {
				if (!target) {
					throw new TypeError('Cannot convert undefined or null to object');
				}
				target = Object(target);
				for (var index = 1; index < arguments.length; index++) {
					var source = arguments[index];
					if (!!source) {
						for (var key in source) {
							if (Object.prototype.hasOwnProperty.call(source, key)) {
								target[key] = source[key];
							}
						}
					}
				}
				return target;
			};
		}

		if (!Array.prototype.includes) {
			Array.prototype.includes = function(searchElement /*, fromIndex*/) {
				if (!this) {
					throw new TypeError('Array.prototype.includes called on null or undefined');
				}

				var O = Object(this);
				var len = parseInt(O.length, 10) || 0;
				if (len === 0) {
					return false;
				}
				var n = parseInt(arguments[1], 10) || 0;
				var k;
				if (n >= 0) {
					k = n;
				}
				else {
					k = len + n;
					if (k < 0) {k = 0;}
				}
				var currentElement;
				while (k < len) {
					currentElement = O[k];
					if (searchElement === currentElement ||
					(searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
						return true;
					}
					k++;
				}
				return false;
			};
		}
	}());

	this.checkConnection = function(){
		return navigator.onLine?"online":"offline";
	};

	this.createBlob = function(docData,docType){
		var contentType = MIME_PLAIN;
		if((docType == IMG_TYPE_JPG) || (docType == IMG_TYPE_PNG))
			contentType = MIME_IMAGE;
		var byteCharacters = atob(docData);
		var byteNumbers = new Array(byteCharacters.length);
		for(var z = 0; z < byteCharacters.length; z++){
			byteNumbers[z] = byteCharacters.charCodeAt(z);
		}
		var byteArray = new Uint8Array(byteNumbers);
		var blob = null;
		try{
			blob = new Blob([byteArray], {type: contentType});
		}
		catch(ex){
			// write the ArrayBuffer to a blob, and you're done
			try{
				debug("Blob is not supported...So, using BlobBuilder: " + ex.message);
				var bb = new WebKitBlobBuilder();
				bb.append(byteArray);
				return bb.getBlob(contentType);
			}catch(exn){
				debug("Exception in blob: " + exn.message);
			}
		}
		return blob;
	};

	this.openFile = function(filePath){
		cs.hideLoading();
		cordova.exec(function(){cs.hideLoading();}, function(){cs.hideLoading();}, "PluginHandler", "openFile", [filePath]);
	};

	this.saveFile = function(fileName,fileType,filePath,fileContent,isBase64){
		var dfd = $q.defer();
		debug("fileContent.length: " + fileContent.length);
		try{
			cordova.exec(
				function(succMsg){
					debug(succMsg);
					dfd.resolve("success");
				},
				function(errMsg){
					debug(errMsg);
					dfd.resolve(null);
				},
				"PluginHandler",((fileType==IMG_TYPE_JPG)?"saveEncryptedFile":"saveToClientEncryptedFile"),[fileName,fileType,filePath,fileContent,isBase64||null]
			);
		}catch(ex){
			debug("Exception in saveFile(): " + ex.message);
		}
		return dfd.promise;
	};

	this.saveNonEncryptedFile = function(fileNameWithExt,filePath,fileContent,isBase64){
		var dfd = $q.defer();
		debug("fileContent.length: " + fileContent.length);
		try{
			cordova.exec(
				function(succMsg){
					debug(succMsg);
					dfd.resolve("success");
				},
				function(errMsg){
					debug(errMsg);
					dfd.resolve(null);
				},
				"PluginHandler","saveNonEncryptedFile",[fileNameWithExt,filePath,fileContent,isBase64]
			);
		}catch(ex){
			debug("Exception in saveNonEncryptedFile(): " + ex.message);
		}
		return dfd.promise;
	};

	this.readFile = function(fileName, filePath){
		var dfd = $q.defer();
		cordova.exec(
			function(fileData){
				debug("readFile Codrdova Success");
				dfd.resolve(fileData);
			},
			function(errMsg){
				debug(errMsg);
				dfd.resolve(null);
			},
			"PluginHandler","readDecryptedFile",[fileName,filePath]
		);
		return dfd.promise;
	};

	this.getDataUrl = function(data){
		debug("returning data url");
		return "data:image/jpeg;base64," + data;
	};

	/*
	31-12-2014 11:11:20
	 0123456789012345678*/
	 this.getDateObj = function(dte){
		 try{
			debug("***** Entering getDateObj() *****");
			var dteObj = {}; // index: 0: dd, 1: mm, 2: yyyy, 3: hh, 4: mm, 5: ss
			dteObj.getDD = parseInt(dte.substring(0,2));
			dteObj.getMM = parseInt(dte.substring(3,5));
			dteObj.getYYYY = parseInt(dte.substring(6,10));
			dteObj.getHR = parseInt(dte.substring(11,13));
			dteObj.getMIN = parseInt(dte.substring(14,16));
			dteObj.getSS = parseInt(dte.substring(17,19));

			dteObj.getSDD = dte.substring(0,2);
			dteObj.getSMM = dte.substring(3,5);
			dteObj.getSYYYY = dte.substring(6,10);
			dteObj.getSHR = dte.substring(11,13);
			dteObj.getSMIN = dte.substring(14,16);
			dteObj.getSSS = dte.substring(17,19);
			debug("***** Exiting getDateObj *****");
			return dteObj;
		}catch(ex){
			debug("Exception in getDateObj(): " + ex.message);
			debug("***** Exiting getDateObj() *****");
		}
	};

	this.compareDates = function(src,trgt){
		try{
			debug("***** Entering compareDates() *****");
			src = getDateObj(src);
			trgt = getDateObj(trgt);
			if(src.getYYYY>trgt.getYYYY){
				return "more";
			}
			else{
				if(src.getYYYY===trgt.getYYYY){
					if(src.getMM>trgt.getMM){
						return "more";
					}
					else{
						if(src.getMM===trgt.getMM){
							if(src.getDD>trgt.getDD){
								return "more";
							}
							else{
								return "less";
							}
						}
						else{
							return "less";
						}
					}
				}
				else{
					return "less";
				}
			}
			debug("***** Exiting compareDates() *****");
		}catch(ex){
			debug("Exception in compareDates(): " + ex.message);
			debug("***** Exiting compareDates() *****");
		}
	};

	Number.prototype.padLeft = function(base,chr){
		try{
			var  len = (String(base || 10).length - String(this).length)+1;
			return len > 0? new Array(len).join(chr || '0')+this : this;
		}catch(ex){
			debug("Exception in Number.prototype.padLeft(): " + ex.message);
			return null;
		}
	};

	this.getCurrDate = function(){
		try{
			var d = new Date();
			var dformat = [d.getDate().padLeft(),(d.getMonth()+1).padLeft(),d.getFullYear()].join('-') +' ' + [d.getHours().padLeft(),d.getMinutes().padLeft(), d.getSeconds().padLeft()].join(':');
			return dformat;
		}catch(ex){
			debug("Exception in getCurrDate(): " + ex.message);
			return null;
		}
	};

	this.compairCurrentDateWithUTC = function(){
		var dfd = $q.defer();
		if(cs.checkConnection()== "online"){
			var serverURL = "https://google.com";
			var geturl;
			geturl = $.ajax({
				type: "GET",
				url: serverURL,
				success: function (res, status, xhr) {
					var dateTime = xhr.getResponseHeader("date");
					debug("dateTime : "+dateTime);
					// dateTime = dateTime.replace("GMT","");
					var compairResponse = {};

					var utcTime =  cs.convertUTCToLocal(dateTime);
					var convertedTime  =  cs.getDifferenceLocalToUTC(dateTime);
					compairResponse.utcTime = utcTime;
					compairResponse.convertedTime = convertedTime;
					compairResponse.CurrDate = cs.getCurrDate();
					dfd.resolve(compairResponse);
				},
				error:function(xlt,ajaxResponse,errorThrown){
					debug("Error thrown is "+errorThrown);
					dfd.resolve(null);
				}
			});
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.getDifferenceLocalToUTC = function(utcDateTime){
		var localTime  = moment.utc(utcDateTime).toDate();
		localTime = moment(utcDateTime);
		debug("convertUTCToLocal dateTime : "+localTime);
		var deviceTime = moment(new Date());
		debug("deviceTime is : "+deviceTime);
		var duration = moment.duration(deviceTime.diff(localTime));
		debug("duration is : "+duration);
		var hours = duration.asHours();
		debug("hours are "+hours);
		var checkIfValidate = (hours > 2 || hours < -2 )?false:true;
		debug("round of the float "+Math.round(hours));
		return checkIfValidate;
	};

	this.getCurrDateUTC = function(){
		var dfd = $q.defer();
		if(cs.checkConnection()== "online"){
			var serverURL = "https://google.com";
			var geturl;
			geturl = $.ajax({
				type: "GET",
				url: serverURL,
				success: function (res, status, xhr) {
					var dateTime = xhr.getResponseHeader("date");
					debug("dateTime : "+dateTime);
					// dateTime = dateTime.replace("GMT","");
					var convertedTime  =  cs.convertUTCToLocal(dateTime);
					dfd.resolve(convertedTime);
				}
			});
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.convertUTCToLocal = function(utcDateTime){
		var localTime  = moment.utc(utcDateTime).toDate();
		localTime = moment(utcDateTime).format("DD-MM-YYYY HH:mm:ss");
		debug("convertUTCToLocal dateTime : "+localTime);
		return localTime;
	};

	this.getFiscalYearData = function(){
		try{
			//get current date
			var today = new Date();
			//get current month
			var curMonth = today.getMonth();
			var fiscalYr1 = "";
			var fiscalYr2 = "";
			var fiscalYr3 = "";
			var last3YearArr = [];
			var currYr = null;
			var nextYr1 = null;
			var prevYr1 = null;
			var prevYr2 = null;
			var prevYr3 = null;
			if (curMonth > 3) {
				currYr = (today.getFullYear()).toString();
				nextYr1 = (today.getFullYear() + 1).toString();
				prevYr1 = (today.getFullYear() - 1).toString();
				prevYr2 = (today.getFullYear() - 2).toString();
				fiscalYr1 = today.getFullYear().toString() + "-" + nextYr1.charAt(2) + nextYr1.charAt(3);
				fiscalYr2 = prevYr1 + "-" + currYr.charAt(2) + currYr.charAt(3);
				fiscalYr3 = prevYr2 + "-" + prevYr1.charAt(2) + prevYr1.charAt(3);
				last3YearArr=[fiscalYr1,fiscalYr2,fiscalYr3];
				return last3YearArr;
			}
			else {
				currYr = (today.getFullYear()).toString();
				prevYr1 = (today.getFullYear() - 1).toString();
				prevYr2 = (today.getFullYear() - 2).toString();
				prevYr3 = (today.getFullYear() - 3).toString();
				fiscalYr1 = prevYr1 + "-" + currYr.charAt(2) + currYr.charAt(3);
				fiscalYr2 = prevYr2 + "-" + prevYr1.charAt(2) + prevYr1.charAt(3);
				fiscalYr3 = prevYr3 + "-" + prevYr2.charAt(2) + prevYr2.charAt(3);
				last3YearArr=[fiscalYr1,fiscalYr2,fiscalYr3];
				return last3YearArr;
			}
		}catch(ex){
			debug("Exception in getFiscalYearData(): " + ex.message);
			return null;
		}
	};

	this.get6MonthYearData = function(){
		try {
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var today = new Date();
			//today.setMonth(today.getMonth() + 2);
			var d;
			var month;
			var initMonth = today.getMonth()+1;
			var monthYearData = [];

			for(var i = 1; i <= 6; i++) {
				var obj = {};
				d = new Date(today.getFullYear(), today.getMonth() - i, 1);
				month = monthNames[d.getMonth()];
				obj.MONTH = month;
				if(initMonth < 6 && d.getMonth()+1>6){
					obj.YEAR = today.getFullYear()-1;
				}
				else{
					obj.YEAR = today.getFullYear();
				}
				monthYearData.push(obj);
			}
			return monthYearData;
		} catch (e) {
			debug("Exception in get6MonthYearData(): " + ex.message);
			return null;
		}
	};

	this.ajaxCall = function(pUrl,pType,pDataType,pData,pAsync,pTimeout,pSuccess,pError){
		/*
			To make ajax calls for communication with server.
		*/
		try{
			return $http({
				url: pUrl,
				method: pType,
				headers: {'Content-Type': pDataType},
				params: {"Key":JSON.stringify(pData)},
				async: pAsync || true,
				timeout: 1000*60*(pTimeout || 5)
			}).success(function(response){
				pSuccess(response);
			}).error(function(data, status, headers, config, statusText){
				debug("Error in ajax request: " + data);
				debug("Error status: " + status + " - " + statusText);
				if(!!pError){
					pError(data, status, headers, config, statusText);
				}
				else{
					navigator.notification.alert("Could not process the request right now. Please try again later.",null,"Server Request","OK");
				}
			});
		}catch(ex){
			printLog(d,"Exception in ajaxCall: " + ex.message);
			return null;
		}
	};

	this.APP_AJAX_CALL = function(pUrl,pType,pDataType,pData,pAsync,pTimeout,pSuccess,pError){
		$.ajax({
			url: pUrl,
			type: pType,
			dataType: 'json',
			data: {"Key":JSON.stringify(pData)},
			async: pAsync || true,
			timeout: 1000*60*(pTimeout || 5),
			success:
					function(json){
						console.log( "JSON AppData Response: " + JSON.stringify(json));
						pSuccess(json);
					},
			error:
					function(jqXHR,textStatus,errorThrown){
						console.log("Error: " + errorThrown + "\n" + jqXHR.status);
						pError(null, errorThrown, null, null, textStatus);
					}
		});
	};

	this.transaction = function(dbObj, trxCallback, errCallback, succCallback){
		dbObj.transaction(
			function (tx){
				if(!!trxCallback)
					trxCallback(tx);
			},
			function(err){
				debug("CommonService.transaction(): error");
				debug("Error: " + err.message);
				if(!!errCallback)
					errCallback(err);
			},
			function(){
				debug("CommonService.transaction(): success");
				if(!!succCallback)
					succCallback();
			}
		);
	};

	this.executeSql = function(tx, query, parametersList, succCallback, errCallback){
		tx.executeSql(
			query,parametersList,
			function (tx,res){
				debug("CommonService.executeSql(): success");
				if(!!succCallback)
					succCallback(tx,res);
			},
			function (tx,err){
				debug("CommonService.executeSql(): error");
				debug("Error: " + err.message);
				if(!!errCallback)
					errCallback(tx,err);
			}
		);
	};

	this.getIndexOfObject = function(array, value){
		for (var i = 0; i < array.length ; i++) {
			if(!!array[i] && !!value){
				if (JSON.stringify(array[i]) == JSON.stringify(value)) {
					return i;
				}
			}
		}
		debug("Returning 0 as index as object not found");
		return 0;
	};

	this.validatePassword = function(password){
		if(!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*_\-]).{8,}$/.test(password)){
			navigator.notification.alert("The password must have: \n1. Atleast 8 digits\n2. An uppercase alphabet\n3. An lowercase alphabet\n4. At least one digit\n5. At least one special character",null,"Password","OK");
			return false;
		}
		else{
			return true;
		}
	};

	this.getAgeProofList = function(custCat, stdAgeProofFlag, noPanCard){
		var dfd = $q.defer();
		var whereClauseObj = {};
		//Application Form - Drastty
		if(!!custCat)
			whereClauseObj.customer_category = custCat;
		whereClauseObj.mpdoc_code = "AP";
		whereClauseObj.isactive = "1";
		if(!!stdAgeProofFlag/* && stdAgeProofFlag=='Y'*/){
			whereClauseObj.standard_flag = stdAgeProofFlag;
		}

		cs.selectRecords(db, "lp_doc_proof_master", true, "doc_id, standard_flag, mpproof_code, mpproof_desc, customer_category", whereClauseObj, "mpproof_desc asc").then(
			function(res){
				var ageProofList = [];
				debug("getAgeProofList res.rows.length: " + res.rows.length);
				if(!!res && res.rows.length>0){
					ageProofList.push({"MPPROOF_DESC": "Select Age Proof", "DOC_ID": null, "STANDARD_FLAG": null, "CUSTOMER_CATEGORY": "INPR"});
					for(var i=0;i<res.rows.length;i++){
						var ageProof = {};
						//Application Form - Drastty
						ageProof.CUSTOMER_CATEGORY = res.rows.item(i).CUSTOMER_CATEGORY;
						ageProof.MPPROOF_DESC = res.rows.item(i).MPPROOF_DESC;
						ageProof.DOC_ID = res.rows.item(i).DOC_ID;
						ageProof.STANDARD_FLAG = res.rows.item(i).STANDARD_FLAG;
						ageProof.MPPROOF_CODE = res.rows.item(i).MPPROOF_CODE;
						if((!noPanCard) || (!!noPanCard && ageProof.MPPROOF_CODE!="PC")){
							ageProofList.push(ageProof);
						}
					}
					dfd.resolve(ageProofList);
				}
				else
					dfd.resolve(null);
			}
		);

		return dfd.promise;
	};


	this.insertOrReplaceRecord = function(database, tableName, record, replace){
		var dfd = $q.defer();
		if((!!database)){
			if(!!tableName){
				if(!!record){
				//Application main - Drastty
					var insertQuery = "insert or ignore into " + tableName + " ( ";
					if(!!replace){
						insertQuery = "insert or replace into " + tableName + " ( ";
					}
					var columnNames = Object.keys(record);
					insertQuery += columnNames[0];
					var parameterList = [];
					parameterList.push(record[columnNames[0]]);
					var questionList = "?";
					for(var i=1;i<columnNames.length;i++){
						insertQuery += ", " + columnNames[i];
						questionList += ", ?";
						parameterList.push(record[columnNames[i]]);
					}
					insertQuery += " ) values ( " + questionList + " )";

					debug("insertQuery: " + insertQuery);
					debug("parameterList: " + parameterList);

					cs.transaction(db,
						function(tx){
							cs.executeSql(tx,insertQuery,parameterList,
								function(tx,res){
									debug("inserted record successfully!");
									dfd.resolve(res);
								},
								function(tx,err){
								debug("error1::" + err);
									dfd.resolve(null);
								}
							);
						},
						function(err){
						debug("error2::" + err);
							dfd.resolve(null);
						},null
					);
				}
				else{
					debug("Please provide the record to be inserted.");
					dfd.resolve(null);
				}
			}
			else{
				debug("Please provide the name of the table.");
				dfd.resolve(null);
			}
		}
		else{
			debug("Please provide the name of the database.");
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.selectRecords = function(database, tableName, isDistinct, columnNames, whereClauseObj, orderByClause){
		var dfd = $q.defer();
		if((!!database)){
			if(!!tableName){
				var selectQuery = "select " + ((isDistinct)?"distinct ":"") + "* from " + tableName;
				var columns = null;
				var parameterList = [];
				var i=1;
				if(!!columnNames){
					if(columnNames instanceof Array){
						if(columnNames.length>0){
							columns = columnNames[0];
							for(i=1;i<columnNames.length;i++){
								columns = ", " + columnNames[i];
							}
						}
					}
					else{
						columns = columnNames;
					}
					selectQuery = "select " + ((isDistinct)?"distinct ":"") + columns + " from " + tableName;
				}

				if(!!whereClauseObj){
					var whereColumns = Object.keys(whereClauseObj);
					var whereClause = " where " + whereColumns[0] + "=?";
					parameterList.push(whereClauseObj[whereColumns[0]]);
					for(i=1;i<whereColumns.length;i++){
						whereClause += " and " + whereColumns[i] + "=?";
						parameterList.push(whereClauseObj[whereColumns[i]]);
					}
					selectQuery += whereClause;
				}

				if(!!orderByClause){
					selectQuery += " order by " + orderByClause;
				}

				debug("select query: " + selectQuery);
				debug("parameter list: " + JSON.stringify(parameterList));

				cs.transaction(database,
					function(tx){
						cs.executeSql(tx,selectQuery,parameterList,
							function(tx,res){
								debug("selected record successfully!");
								dfd.resolve(res);
							},
							function(tx,err){
								dfd.resolve(null);
							}
						);
					},
					function(err){
						dfd.resolve(null);
					},null
				);
			}
			else{
				debug("Please provide the name of the table.");
				dfd.resolve(null);
			}
		}
		else{
			debug("Please provide the name of the database.");
			dfd.resolve(null);
		}
		return dfd.promise;
	};
	this.updateRecords = function(database, tableName, updateClauseObj, whereClauseObj){
		var dfd = $q.defer();
		if((!!database)){
			if(!!tableName){
				var updateQuery = "update " + tableName + " set ";
				var parameterList = [];
				var i=1;
				if(!!updateClauseObj){
					var updateColumns = Object.keys(updateClauseObj);
					var updateClause = updateColumns[0] + "=?";
					parameterList.push(updateClauseObj[updateColumns[0]]);
					for(i=1;i<updateColumns.length;i++){
						updateClause += ", " + updateColumns[i] + "=?";
						parameterList.push(updateClauseObj[updateColumns[i]]);
					}
					updateQuery += updateClause;
				}

				if(!!whereClauseObj){
					var whereColumns = Object.keys(whereClauseObj);
					var whereClause = " where " + whereColumns[0] + "=?";
					parameterList.push(whereClauseObj[whereColumns[0]]);
					for(i=1;i<whereColumns.length;i++){
						whereClause += " and " + whereColumns[i] + "=?";
						parameterList.push(whereClauseObj[whereColumns[i]]);
					}
					updateQuery += whereClause;
				}

				debug("update query: " + updateQuery);
				debug("parameter list: " + JSON.stringify(parameterList));

				cs.transaction(database,
					function(tx){
						cs.executeSql(tx,updateQuery,parameterList,
							function(tx,res){
								debug("updated record successfully!");
								dfd.resolve(res);
							},
							function(tx,err){
								dfd.resolve(null);
							}
						);
					},
					function(err){
						dfd.resolve(null);
					},null
				);
			}
			else{
				debug("Please provide the name of the table.");
				dfd.resolve(null);
			}
		}
		else{
			debug("Please provide the name of the database.");
			dfd.resolve(null);
		}
		return dfd.promise;
	};



	this.deleteRecords = function(database, tableName, whereClauseObj){
		var dfd = $q.defer();
		if((!!database)){
			if(!!tableName){
				var deleteQuery = "delete from " + tableName;
				var parameterList = [];

				if(!!whereClauseObj){
					var whereColumns = Object.keys(whereClauseObj);
					var whereClause = " where " + whereColumns[0] + "=?";
					parameterList.push(whereClauseObj[whereColumns[0]]);
					for(var i=1;i<whereColumns.length;i++){
						whereClause += " and " + whereColumns[i] + "=?";
						parameterList.push(whereClauseObj[whereColumns[i]]);
					}
					deleteQuery += whereClause;
				}

				debug("delete query: " + deleteQuery);
				debug("parameter list: " + JSON.stringify(parameterList));

				cs.transaction(database,
					function(tx){
						cs.executeSql(tx,deleteQuery,parameterList,
							function(tx,res){
								debug("deleted record(s) successfully!");
								dfd.resolve(res);
							},
							function(tx,err){
								dfd.resolve(null);
							}
						);
					},
					function(err){
						dfd.resolve(null);
					},null
				);
			}
			else{
				debug("Please provide the name of the table.");
				dfd.resolve(null);
			}
		}
		else{
			debug("Please provide the name of the database.");
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.resultSetToObject = function(res){
		if(!!res && res.rows.length>0){
			var objectArray = [];
			for(var i=0;i<res.rows.length;i++){
				var colNames = Object.keys(res.rows.item(i));
				var obj = {};
				for(var j=0;j<colNames.length;j++){
					obj[colNames[j]] = (res.rows.item(i))[colNames[j]];
				}
				objectArray.push(obj);
			}
			if(objectArray.length==1)
				return objectArray[0];
			else
				return objectArray;
		}
		else {
			return null;
		}
	};

	this.termResultSetToObject = function(res, REF_APPLICATION_ID, REF_POL_NO,flag){
		if(!!res && res.rows.length>0){
			var objectArray = [];
			for(var i=0;i<res.rows.length;i++){
				var colNames = Object.keys(res.rows.item(i));
				var obj = {};
				for(var j=0;j<colNames.length;j++){
					obj[colNames[j]] = (res.rows.item(i))[colNames[j]];
					if(colNames[j]=='APPLICATION_ID')
						obj[colNames[j]] = REF_APPLICATION_ID;
					if(colNames[j]=='POLICY_NO' && (flag == undefined || flag!=false))
						obj[colNames[j]] = REF_POL_NO;
				}
				objectArray.push(obj);
			}
			if(objectArray.length==1)
				return objectArray[0];
			else
				return objectArray;
		}
	};

	this.formatDobToDb = function (ogDate){
		try{
			var date = new Date(ogDate); // so that below manipulation(s) doesn't affect parameter ogDate
			//date.setMonth(date.getMonth()+1);
			var _month = date.getMonth()+1;
			return ((date.getDate()>9)?date.getDate():("0"+date.getDate())) + "-" + ((_month>9)?(_month):("0"+_month)) + "-" + date.getFullYear() + " 00:00:00";
		}catch(ex){
			debug("Exception in formatDobToDb(): " + ex.message);
			return null;
		}
	};

	this.formatDobFromDb = function (dob){
		return new Date(dob.substring(6,10)/* YYYY */ + "-" + dob.substring(3,5)/* MM */ + "-" + dob.substring(0,2)/* DD */);
	};

	//camera..
	this.readOriginalFile = function(fileName, filePath){
		var dfd = $q.defer();
		cordova.exec(
			function(fileData){
				debug("readFile Codrdova Success");
				dfd.resolve(fileData);
			},
			function(errMsg){
				debug(errMsg);
				dfd.resolve(null);
			},
			"PluginHandler","readOriginalFile",[fileName,filePath]
		);
		return dfd.promise;
	};

	this.getAge = function (birth){
		var age;
		var today = new Date();
		var nowyear = today.getFullYear();
		var nowmonth = today.getMonth()+1;
		var nowday = today.getDate();
		birth = birth.replace("-", "");
		birth = birth.replace("-", "");
		var birthday = birth.substring(0,2); //6, 8
		var birthmonth = birth.substring(2,4); //4, 6
		var birthyear = birth.substring(4,8); //0, 4
		age = nowyear - birthyear;
		var age_month = nowmonth - birthmonth;
		var age_day = nowday - birthday;
		if(age_month < 0 || (age_month === 0 && age_day <0)) {
			age = parseInt(age) -1;
		}
		return age;
	};

	this.getNoOfMonths = function (dateToCopmare){
		var Nomonths;
		var date2 = new Date();
		dateToCopmare = dateToCopmare.replace("-", "");
		dateToCopmare = dateToCopmare.replace("-", "");
		var day = dateToCopmare.substring(0,2); //6, 8
		var month = dateToCopmare.substring(2,4); //4, 6
		var year = dateToCopmare.substring(4,8); //0, 4
		var date1 = new Date(year, month, day);
		Nomonths = (date2.getFullYear() - date1.getFullYear()) * 12;
		Nomonths -= date1.getMonth() + 1;
		Nomonths += date2.getMonth() +1; // we should add + 1 to get correct month number
		return Nomonths <= 0 ? 0 : Nomonths;
	};

	this.openMenu = function(){
		try{
			panels.open("overlayMenu");
		}
		catch(ex){
			debug(ex.message);
		}
	};

	this.closeMenu = function(){
		try{
			panels.close("overlayMenu");
		}
		catch(ex){
			debug(ex.message);
		}
	};

	this.getRandomNumber = function() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getMilliseconds();
		var random = Math.floor((Math.random() * 100000) + 1000);
		random=''+random+h+m+s;
		return random;
	};

	this.escapeRegExp = function(string) {
		return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
	};

	this.replaceAll = function(string, find, replace) {
		if(!replace)
			return string.replace(new RegExp(this.escapeRegExp(find), 'g'), "");
		else
			return string.replace(new RegExp(this.escapeRegExp(find), 'g'), replace);
	};

	this.showLoading = function(text){
		$ionicLoading.show({
			template: text || 'Loading...'
		}).then(function(){
			debug("The loading indicator is now displayed");
		});
	};

	this.hideLoading = function(){
		$ionicLoading.hide().then(function(){
			debug("The loading indicator is now hidden");
		});
	};

	this.getTempFileURI = function(docPath, docName){ //for sharing
		var dfd = $q.defer();
		cordova.exec(
			function(fileName){
				debug("fileName: " + fileName);
				cordova.exec(
					function(uri){
						debug("uri: " + uri);
						dfd.resolve(uri);
					},
					function(erMsg){
						debug(erMsg);
					},
					"PluginHandler","getTempFileURI",[fileName]
				);
			},
			function(errMsg){
				debug(errMsg);
			},
			"PluginHandler","getDecryptedFileName",[docName, docPath]
		);
		return dfd.promise;
	};

	//june 30th
	this.getTempNormalFileURI = function(docPath, docName){ //for sharing
		var dfd = $q.defer();
		cordova.exec(
			function(uri){
				debug("uri: " + uri);
				dfd.resolve(uri);
			},
			function(erMsg){
				debug(erMsg);
				dfd.resolve(null);
			},
			"PluginHandler","getTempNormalFileURI",[docPath,docName]
		);
		return dfd.promise;
	};

	this.getFileData = function(){ //to view
		var dfd = $q.defer();
		cordova.exec(
			function(fileData){
				debug("fileData: " + ((!!fileData)?fileData.length:null));
				dfd.resolve(dfd);
			},
			function(errMsg){
				debug(errMsg);
				dfd.resolve(null);
			},
			"PluginHandler","getDecryptedFileData",[docName, docPath]
		);
		return dfd.promise;
	};

	this.doSign = function(action){
		var opts = {
			animation: true,
			templateUrl: 'signature/signature.html',
			controller: "SignatureCtrl as sc",
			resolve: {
				Action: [
					function(){
						debug("resolving action: " + action);
						return action;
					}
				]
			}
		};
		this.modalInstance = $uibModal.open(opts);
	};

	this.copyDBToExternal = function(){
		cordova.exec(
			function(res){
				debug("copyDBToExternal res: " + res);
			},
			function(erMsg){
				debug(erMsg);
			},
			"PluginHandler","getSADatabase",[]
		);
	};

	this.rebuildSlide = function(slideThreshold, values) {
		var tempArr = [];
		var slide = [];
		if(!!values){
			for(var i=0; i<values.length; i++) {
				if(slide.length === slideThreshold) {
					tempArr.push(slide);
					slide = [];
				}
				slide.push(values[i]);
			}
			if(!!slide && slide.length>0)
				tempArr.push(slide);
		}
		return tempArr;
	};

	this.toTitleCase = function(str){
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	};


	this.ekycServletCall = function(aadharNumber, timestamp, otp){
		var dfd = $q.defer();

		var data = {"Type": "o", "AadhaarNo": aadharNumber};

		if(!!timestamp){
			data.Type = "b";
			data.TimeStamp = timestamp.substring(6,10)/* YYYY */ + "-" + timestamp.substring(3,5)/* MM */ + "-" + timestamp.substring(0,2)/* DD */ + " " + timestamp.substring(11,20);
			data.OTP = otp;
		}

		debug("data:: " + JSON.stringify(data));

		$http({
			url: EKYC_SERVLET,
			method: AJAX_TYPE,
			headers: {'Content-Type': TYPE_JSON},
			params: data,
			async: AJAX_ASYNC || true,
			timeout: 1000*60*(AJAX_TIMEOUT || 1)
		}).success(function(response){
			debug(response);
			dfd.resolve(response);
		}).error(function(data, status, headers, config, statusText){
			navigator.notification.alert("Could not process the request right now. Please try again later.",function(){
				cs.hideLoading();
			},"Server Request","OK");
			debug("Error in ajax request: " + data);
			debug("Error status: " + status + " - " + statusText);
			dfd.resolve(null);
		});

		return dfd.promise;
	};

	this.isNTIDLogin = function(){
		return ( (((localStorage.EMPFLG=='Y')||(localStorage.EMPFLG=='E')||(localStorage.EMPFLG=='R')||(localStorage.EMPFLG=='S')) || ((BUSINESS_TYPE=="iWealth") || (BUSINESS_TYPE=="IndusSolution")))?true:false );
	};

	this.getBusinessType=function(){
		return ((BUSINESS_TYPE=="GoodSolution")?true:false);
	};

	this.getAppName = function(){
		if(BUSINESS_TYPE == "IndusSolution")
			return "Glide";
		else if(BUSINESS_TYPE == "GoodSolution")
			return "Good Solutions";
		else if(BUSINESS_TYPE == "iWealth")
			return "iWealth";
		else if(BUSINESS_TYPE == "LifePlaner")
			return "Life Planner";
		else
			return null;
	};

	this.getEmpProofDocId = function(custType){
		var dfd = $q.defer();
		this.selectRecords(db, "lp_doc_proof_master", true, "DOC_ID", {"customer_category": custType, "mpdoc_code": "CD", "mpproof_code": "EI"}).then(
			function(res){
				if(!!res && res.rows.length>0){
					dfd.resolve(res.rows.item(0).DOC_ID);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	this.getEKYCDocumentID = function(){
		var dfd = $q.defer();
		var eKycData = {insured : {}, proposer : {}};
		var whereClauseObj = {};
		whereClauseObj.MPDOC_CODE = 'KY';
		whereClauseObj.MPPROOF_CODE = 'EA';
		whereClauseObj.CUSTOMER_CATEGORY = 'IN';
		cs.selectRecords(db, "lp_doc_proof_master", false, null, whereClauseObj).then(
			function(res){
				debug(JSON.stringify(whereClauseObj) + " EKYC doc ID - INS: " + res.rows.length);
				if(!!res && res.rows.length>0){
					eKycData.insured = cs.resultSetToObject(res);
					whereClauseObj.CUSTOMER_CATEGORY = 'PR';
					cs.selectRecords(db, "lp_doc_proof_master", false, null, whereClauseObj).then(
						function(res){
							debug("LP_EKYC_DTLS PRO: " + res.rows.length);
							if(!!res && res.rows.length>0)
								eKycData.proposer = cs.resultSetToObject(res);
							dfd.resolve(eKycData);
						}
					);
				}
				else
					dfd.resolve(eKycData);
			}
		);
		return dfd.promise;
	};

	this.sendFile = function(file, options){
		var dfd = $q.defer();
		var ft = new FileTransfer();
		debug("file: " + file);
		ft.upload(file, HTML_SYNC_URL,
			function(succ){
				debug("Code = " + succ.responseCode);
				debug("Response = " + succ.response);
				debug("Sent = " + succ.bytesSent);
				if(!!succ && !!succ.response && JSON.parse(succ.response).RS.RESP==="S"){
					dfd.resolve("S");
				}
				else{
					dfd.resolve(null);
				}
			},
			function(err){
				debug("FT upload error: " + JSON.stringify(err));
				dfd.resolve(null);
			},options
		);
		return dfd.promise;
	};

	/**
	 * Function to get Master Residental status list
	 */
	this.getResidentalStatusList = function(){
		var residentStatusList = [
			{"RESIDENT_STATUS": "Select Residental Status ", "RESIDENT_CD":null},
			{"RESIDENT_STATUS": "Resident Indian", "RESIDENT_CD":"RI"},
			{"RESIDENT_STATUS": "Non Resident Indian", "RESIDENT_CD":"NRI"},
			{"RESIDENT_STATUS": "Foreign National", "RESIDENT_CD":"FN"},
			{"RESIDENT_STATUS": "Overseas Citizen of India", "RESIDENT_CD":"OCI"}
		];
		return residentStatusList;
	};

	this.getOrdinalSuffixOf = function(i) {
		var j = i % 10,
			k = i % 100;
		if (j == 1 && k != 11) {
			return i + "st";
		}
		if (j == 2 && k != 12) {
			return i + "nd";
		}
		if (j == 3 && k != 13) {
			return i + "rd";
		}
		return i + "th";
	};

	this.getUserExtraDetails = function(loginType){
		return { "APP_VER_NO": VERSION_NO, "CORDOVA_VER_NO": device.cordova, "DEV_MODEL": device.model, "PLATFORM_NAME": device.platform, "UUID": device.uuid, "PLATFORM_VER_NO": device.version, "DEV_MANUFACTURER": device.manufacturer, "IS_VIRTUAL": ((!!device.isVirtual)?"Y":"N"), "DEV_SERIAL_NO": device.serial, "LAST_OL_LOGIN_TYPE": loginType, "APP_ID": APP_NAME };
	};

/*Start ssl certificate*/
	this.checkSslPinning = function(){
		var dfd = $q.defer();
		var server = DOMAIN_URL;
		var fingerprint = FINGERPRINT;
		window.plugins.sslCertificateChecker.check(successCallback, errorCallback, server, fingerprint);
		function successCallback(message) {
			dfd.resolve(message);
		}
		function errorCallback(message) {
			dfd.resolve(message);
		}
		return dfd.promise;
	};
/*END ssl certificate*/


/*Start expireLoginCookie*/
	this.checkLoginLocalStorage = function(){
		var DATE = new Date();
		var CURRENTDATETIME = new Date(DATE.getFullYear(), DATE.getMonth(), DATE.getDate(), DATE.getHours(), DATE.getMinutes(), DATE.getSeconds());
		CURRENTDATETIME = CURRENTDATETIME.getTime();
		var loginLocalStoragedata = '';
		var chackDtatype = typeof window.localStorage.getItem('loginLocalStorage');
		var loginLocalStorage =   JSON.parse(window.localStorage.getItem('loginLocalStorage'));
		debug("chackDtatype:"+chackDtatype);
		if(chackDtatype == 'object' || chackDtatype == 'undefined' || JSON.stringify(loginLocalStorage)=='null'){
			loginLocalStorage = false;
			debug("login after 24 hour:"+JSON.stringify(loginLocalStorage));
			return loginLocalStorage;
		}
		else {
			debug("Login before 24 hours:"+JSON.stringify(loginLocalStorage));
			var diffDate = loginLocalStorage[0].endLoginDateTime-loginLocalStorage[0].loginDateTime;
			debug("loginLocalStorage[0].endLoginDateTime-loginLocalStorage[0].loginDateTime: " + diffDate);
			debug("LOGINSTORAGEEXPIRETIME:"+LOGINSTORAGEEXPIRETIME);
			if((CURRENTDATETIME-loginLocalStorage[0].loginDateTime)<=LOGINSTORAGEEXPIRETIME){
				debug("Login storage success");
				return loginLocalStorage;
			}
			else{
				loginLocalStorage = false;
				debug("remove the login storage");
				return loginLocalStorage;
			}
		}
	};

	this.setLocalStorageForLogin = function(username, password){
		var DATE = new Date();
		var CURRENTDATETIME = new Date(DATE.getFullYear(), DATE.getMonth(), DATE.getDate(), DATE.getHours(), DATE.getMinutes(), DATE.getSeconds());
		var loginLocalStorageTime = CURRENTDATETIME.getTime();
		var loginExpireTime = loginLocalStorageTime + LOGINSTORAGEEXPIRETIME;
		var loginLocalStoragedata = '';
		loginLocalStoragedata = [{"loginDateTime": loginLocalStorageTime, "endLoginDateTime": loginExpireTime, "username": username } ];
		debug("loginLocalStoragedata"+loginLocalStoragedata);
		window.localStorage.setItem("loginLocalStorage", JSON.stringify(loginLocalStoragedata));
		debug("setLocalStorageForLogin:"+JSON.stringify(loginLocalStoragedata));
	};

	this.removeLocalStorageForLogin = function(username, password){
		window.localStorage.removeItem('loginLocalStorage');
		debug("Come in if condition data:"+this.loginLocalStoragedata);
	};
/*END expireLoginCookie*/

	this.getUserDataFromDb = function(){
		debug("inside getBusinessTypeFromDb");
		return cs.selectRecords(db, "lp_userinfo", true).then(
			function(res){
				debug("getBusinessTypeFromDb res.rows.length: " + res.rows.length);
				if(!!res && res.rows.length>0){
					var obj = cs.resultSetToObject(res);
					debug("getBusinessTypeFromDb res: " + JSON.stringify(obj));
					if(obj instanceof Array){
						obj = obj[0];
					}
					return obj;
				}
				else{
					debug("getBusinessTypeFromDb res: null");
					return null;
				}
			}
		);
	};

	this.splitInsertList = function(insertList){
		if(!!insertList && insertList.length>0){
			var insListLength = insertList.length;
			// at the max only 999 ?(hosted parameters) are allowed so, have to factor it in deciding chunk size...
			var chunkSize = Math.floor(999/(Object.keys(insertList[0]).length));

			// Number of chunks the insertList will be devided into...
			var chunks = Math.ceil(insertList.length/chunkSize);
			var splitArray = [];
			var start = 0;
			var end = chunkSize;
			for(var i=0; i<chunks; i++){
				//creating chunks...
				splitArray[i] = insertList.slice(start, end);
				start = end;
				end = start + chunkSize;
			}
			return splitArray;
		}
		return null;
	};

	this.insertBulkChunk = function(tableName, insertChunk){
		var dfd = $q.defer();
		if(!!tableName && !!insertChunk && insertChunk.length>0){
			var columnNames = Object.keys(insertChunk[0]);
			var columnNamesString = columnNames.join(", ");
			var insertQuery = "insert or replace into " + tableName + " (" + columnNames + ") values ";
			var insertParams = [];
			var k = 0;
			for(var i=0; i<insertChunk.length; i++){
				// creating insert query (appending parameters) for the chunk
				insertQuery += "(";
				for(var j=0; j<columnNames.length; j++){
					insertQuery += "?";
					if(j<(columnNames.length-1)){
						insertQuery += ", ";
					}
					insertParams[k] = insertChunk[i][columnNames[j]];
					k++;
				}
				insertQuery += ")";
				if(i<(insertChunk.length-1)){
					insertQuery += ", ";
				}
				else{
					insertQuery += ";";
				}
			}
			cs.transaction(db,
				function(tx){
					//actual insertion of the chunk into the table...
					/* insert query syntax --> insert or replace into tableName (column1, column2...) values (column1Value, column2Value...), (column1Value, column2Value...), ..., (column1Value, column2Value...); */
					cs.executeSql(tx, insertQuery, insertParams,
						function(tx, res){
							dfd.resolve("S");
						},
						function(tx, err){
							dfd.resolve(null);
						}
					);
				},
				function(err){
					dfd.resolve(null);
				}
			);
		}
		else{
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.bulkInsertProcess = function(tableName, insertList){
		var promises = [];
		if(!!tableName && !!insertList && insertList.length>0){
			var insertListChunks = this.splitInsertList(insertList); // [[], [], [], ...]  <-- insertList split into chunks
			if(!!tableName && !!insertListChunks){
				for(var i=0; i<insertListChunks.length; i++){
					// insert each chunk into table...
					promises.push(this.insertBulkChunk(tableName, insertListChunks[i]));
				}
			}
			else{
				promises.push(null);
			}
		}
		else{
			promises.push(null);
		}
		return $q.all(promises);
	};

	this.bulkInsert = function(tableName, insertList){
		var dfd = $q.defer();
		this.bulkInsertProcess(tableName, insertList).then(
			function(insRes){
				if(!!insRes && insRes.length>0){
					var flag="S";
					for(var i=0; i<insRes.length; i++){
						if(!insRes[i]){
							flag = null;
							break;
						}
					}
					dfd.resolve(flag);
				}
				else{
					dfd.resolve(null);
				}
			}
		);
		return dfd.promise;
	};

	//DEDUPE PLAN FORMAT to 10 charaters
	 this.formatText	= function(n, width, z) {
		 z = z || ' ';
		 n = n + '';
		 return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	 };

 this.loadDeDupeData = function(APPLICATION_ID, AGENT_CD){
		 var dfd = $q.defer();
		 var dupeData = {}
		 var whereClauseObj = {};
		 whereClauseObj.APPLICATION_ID = APPLICATION_ID;
		 whereClauseObj.AGENT_CD = AGENT_CD;
		 console.log(JSON.stringify(whereClauseObj));
		 cs.selectRecords(db,"LP_DEDUPE_DATA",false,"*",whereClauseObj, "").then(
				 function(res){
						 debug("LP_DEDUPE_DATA:" + res.rows.length);
						 if(!!res && res.rows.length>0){
								 dupeData = cs.resultSetToObject(res);
								 try{
								 if(!!dupeData.RESULT_APPDATA)
								 	dupeData.RESULT_APPDATA = JSON.parse(dupeData.RESULT_APPDATA);
									dfd.resolve(dupeData);
								}catch(e){
									debug("Exception in parsing DeDupe AppData:"+e.message);
									dfd.resolve(dupeData);
								}
						 }
						 else
								 dfd.resolve(dupeData);
				 }
		 );

	 return dfd.promise;
 };
}]);
