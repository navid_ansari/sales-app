agentInfoModule.controller('agentInfoCtrl',['$state','$stateParams', 'CommonService','LoginService','SpDetailsService',function($state,$stateParams,CommonService,LoginService,SpDetailsService){

    var ac=this;
    ac.agentdata=false;
    this.businessType = BUSINESS_TYPE;
    this.agentCodeRegex = AGENT_CD_REGEX;
    ac.SPData={};

    this.openMenu = function(){
      CommonService.openMenu();
   };
    this.getAgentData=function(){
    var internetType=CommonService.checkConnection();
   if(internetType==='online'){
       CommonService.showLoading("Validating agent....");
        try{
            var agentdata = {};
            agentdata.REQ = {};
            agentdata.REQ.AC = this.AGENT_CD;
            agentdata.REQ.PWD = LoginService.lgnSrvObj.password;
            agentdata.REQ.ACN = "AGNI";
            agentdata.REQ.DVID = LoginService.lgnSrvObj.userinfo.DEVICE_ID;
            debug("JSONObject: https://lpu.tataaia.com/LifePlaner/AgentInfoServlet?Key=" + JSON.stringify(agentdata));
            CommonService.ajaxCall(AGENT_DATA, AJAX_TYPE, TYPE_JSON, agentdata, AJAX_ASYNC, AJAX_TIMEOUT, refSucc, refError);
        }catch(ex){
            debug("Exception in agent validation : " + ex.message);
        }
        function refSucc(response){
                console.log("response :"+JSON.stringify(response));
                if( response.RS.ECM == LIC_EXP || response.RS.ECM == AGNT_INACTIVE || response.RS.ECM == LIC_NOT_COLLECTED || response.RS.ECM == MAND_TRAIN || response.RS.ECM == ULIP_TRAIN){
                     console.log("In success");
                     navigator.notification.alert(response.RS.ECM,null,"Agent Validation","OK");
                }else if(response.RS.ECM == VALID_AGNT){
                     console.log(VALID_AGNT);
                }else if(response.RS.ECM == AGNT_NOT_FOUND){
                     navigator.notification.alert(response.RS.ECM,null,"Agent Validation","OK");
                }else{
                     console.log(response.RS.ECM);
                }
                console.log("Agent cd :"+ac.AGENT_CD);
           function fsdpSucc(resp){
            debug("fetchSpDetails Resp : " + JSON.stringify(resp));
                        CommonService.hideLoading();

           if(resp.RS.RESP=='S'){
                   ac.agentdata=true;
                   ac.SPData.IMP_RM_NAME = resp.RS.RMN;
                   ac.SPData.IMP_AGENT_NAME = resp.RS.SPN;
                   ac.SPData.IRDA_LIC_NO = resp.RS.LICN;
                   ac.SPData.IRDA_LIC_EXP_DT = resp.RS.LICE;
                   ac.SPData.MOBILENO = resp.RS.MOB;
                   ac.SPData.CHANNEL_NAME = resp.RS.CHN;
                   ac.SPData.OFFICE_CODE = resp.RS.OFFC;
                   ac.SPData.SUBOFFICECODE = resp.RS.SOC;
                   ac.SPData.OFFICE_NAME = resp.RS.OFFN;
                   ac.SPData.OFFICE_ADDRESS1 = resp.RS.OFFA;
                   ac.SPData.OFFICE_ADDRESS2 = resp.RS.OFFAA;
                   ac.SPData.OFFICE_STATE = resp.RS.OFFST;
                   ac.SPData.OFFICE_ZIP_CODE = resp.RS.OFFPN;
                   ac.SPData.OFFICE_OFFICE_TEL = resp.RS.OFFTEL
           }else{
                   ac.agentdata=false;
           }
        }

        function fsdpErr(data, status, headers, config, statusText) {
            debug("error in fetchSpDetails");
//            dfd.resolve(null);
        }
        try{
            var request = {};
            request.REQ = {};
            request.REQ.AC = LoginService.lgnSrvObj.userinfo.AGENT_CD;
            request.REQ.PWD = LoginService.lgnSrvObj.password;
            request.REQ.DVID = LoginService.lgnSrvObj.dvid;
            request.REQ.RMC = null;
            request.REQ.SPC = ac.AGENT_CD;
            request.REQ.SOC = null;
            request.REQ.POT = 'C,U';
            //debug("BTI : " + LoginService.lgnSrvObj.userinfo.BUSSINESS_TYPE_ID)
            request.REQ.BTI = BUSINESS_TYPE;
            request.REQ.ACN = 'FSDP';
            debug("agentdetails Request: " + JSON.stringify(request));
            CommonService.ajaxCall(AGENT_DETAILS, AJAX_TYPE, TYPE_JSON, request, AJAX_ASYNC, AJAX_TIMEOUT, fsdpSucc, fsdpErr);
            //dfd.resolve(fetchSpDetails);
        }catch(ex){
            debug("exception in getSpData : " + ex.message);
        }

            }

        function refError(data, status, headers, config, statusText) {
            CommonService.hideLoading();
            navigator.notification.alert("Please check your network connection or try again after some time",null,"Agent Validation","OK");
        }
    }else{
       navigator.notification.alert("Please check your internet connection.",null,"Agent Validation","OK");
    }
    }
	CommonService.hideLoading();
}]);
