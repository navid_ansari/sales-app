hswULIPModule.service('hswULIPService', ['$q', '$http', '$state', 'CommonService','LoginService', function ($q, $http, $state, CommonService, LoginService) {
    "use strict";
    var self = this;
    this.productDetails = {};

    this.minAge=0;
    this.maxAge=0;
    this.minSumAssured=0;
    this.maxSumAssured=0;
    this.minAnnualPremium=0;
    this.maxAnnualPremium=0;
    this.startPl="";
    this.PGL_ID="";
    this.pnlId="";
    this.minTerm=0;
    this.maxTerm=0;
    this.ppt=0;
    this.multiPlier=0;
    this.pnlCode="";
    self.pptArrList = [];
    var hsms = this;
    this.sisData = {};
    this.totalFundValueArr = "";
    this.loyaltyChargeDetailsArr = "";
    this.totalFundValue ="";
    this.loyaltyChargeValue ="" ;
    this.policyTerm="";
    this.getHSWPlanLK = function (pgl_ID) {
        self.PGL_ID = pgl_ID;
        debug("PGL ID"+self.PGL_ID);
//        debug("Querry :  " + "select * from (SELECT  pnlPlanLk.PNL_CODE, pnlPlanLk.PNL_MIN_AGE, pnlPlanLk.PNL_MAX_AGE ,pnlPlanLk.PNL_MIN_SUM_ASSURED ,pnlPlanLk.PNL_MAX_SUM_ASSURED ,pnlPlanLk.PNL_MIN_PREMIUM ,pnlPlanLk.PNL_ID, pnlPlanLk.PNL_START_PT, pnlTerm.MIN_TERM, pnlTerm.MAX_TERM,IFNULL(pnlTerm.PPT,'PPT') PPT from LP_PNL_PLAN_LK  AS pnlPlanLk INNER JOIN LP_PNL_TERM_PPT_LK as pnlTerm ON pnlTerm.PNL_CODE=pnlPlanLk.PNL_CODE where pnlPlanLk.PNL_PGL_ID  = '" + self.PGL_ID + "'  ) a order by PPT asc");
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                CommonService.executeSql(tx, "select * from (SELECT  pnlPlanLk.PNL_CODE, pnlPlanLk.PNL_MIN_AGE, pnlPlanLk.PNL_MAX_AGE ,pnlPlanLk.PNL_MIN_SUM_ASSURED ,pnlPlanLk.PNL_MAX_SUM_ASSURED ,pnlPlanLk.PNL_MIN_PREMIUM ,pnlPlanLk.PNL_ID, pnlPlanLk.PNL_START_PT, pnlTerm.MIN_TERM, pnlTerm.MAX_TERM,IFNULL(pnlTerm.PPT,'PPT') PPT, pglTerm.PGL_MIN_PREM_AMT , pglTerm.PGL_MAX_PREM_AMT from LP_PNL_PLAN_LK  AS pnlPlanLk  INNER JOIN LP_PNL_TERM_PPT_LK as pnlTerm ON pnlTerm.PNL_CODE=pnlPlanLk.PNL_CODE INNER JOIN LP_PGL_GRP_MIN_PREM_LK as pglTerm ON pglTerm.PGL_GRP_ID=pnlPlanLk.PNL_PGL_ID AND pglTerm.PGL_PREMIUM_MODE ='A' where pnlPlanLk.PNL_PGL_ID  = ?  ) a order by PPT asc", [self.PGL_ID],
                    function (tx, res) {
                        debug(JSON.stringify(res.rows.item(0)));
                        debug("Length"+JSON.stringify(res.rows.length));
                        if (res.rows.length > 0) {
                            self.minAge = parseInt(res.rows.item(0).PNL_MIN_AGE);
                            self.maxAge = parseInt(res.rows.item(0).PNL_MAX_AGE);
                            self.minSumAssured = parseInt(res.rows.item(0).PNL_MIN_SUM_ASSURED);
                            self.maxSumAssured = parseInt(res.rows.item(0).PNL_MAX_SUM_ASSURED);
                            self.minAnnualPremium = parseInt(res.rows.item(0).PNL_MIN_PREMIUM);
                            self.maxAnnualPremium = parseInt(res.rows.item(0).PGL_MAX_PREM_AMT);
                            self.pnlId  = res.rows.item(0).PNL_ID;
                            self.startPl= res.rows.item(0).PNL_START_PT;
                            self.minTerm= res.rows.item(0).MIN_TERM;
                            self.maxTerm= res.rows.item(0).MAX_TERM;
                            self.ppt= res.rows.item(0).PPT;
                            self.pnlCode=res.rows.item(0).PNL_CODE;
                            self.pptArrList = [];

                            for (var i = 0; i < res.rows.length; i++) {
                                if(res.rows.item(i).PPT=="PPT")
                                {
                                    self.pptObj = {};
                                    self.pptObj.PNL_CODE = res.rows.item(i).PNL_CODE;
                                    self.pptObj.MIN_AGE = parseInt(res.rows.item(i).PNL_MIN_AGE);
                                    self.pptObj.MAX_AGE = parseInt(res.rows.item(i).PNL_MAX_AGE);
                                    self.pptObj.PNL_MIN_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MIN_SUM_ASSURED);
                                    self.pptObj.PNL_MAX_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MAX_SUM_ASSURED);
                                    self.pptObj.PNL_MIN_PREMIUM = parseInt(res.rows.item(i).PNL_MIN_PREMIUM);
                                    self.pptObj.PNL_MAX_PREMIUM = parseInt(res.rows.item(i).PGL_MAX_PREM_AMT);
                                    self.pptObj.PNL_ID = res.rows.item(i).PNL_ID;
                                    self.pptObj.START_PL = res.rows.item(i).START_PL;
                                    self.pptObj.MIN_TERM = res.rows.item(i).MIN_TERM;
                                    self.pptObj.MAX_TERM = res.rows.item(i).MAX_TERM;
                                    self.pptObj.PPT = "10";
                                    self.pptArrList.push(self.pptObj);

                                    self.pptObj = {};
                                    self.pptObj.PNL_CODE = res.rows.item(i).PNL_CODE;
                                    self.pptObj.MIN_AGE = parseInt(res.rows.item(i).PNL_MIN_AGE);
                                    self.pptObj.MAX_AGE = parseInt(res.rows.item(i).PNL_MAX_AGE);
                                    self.pptObj.PNL_MIN_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MIN_SUM_ASSURED);
                                    self.pptObj.PNL_MAX_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MAX_SUM_ASSURED);
                                    self.pptObj.PNL_MIN_PREMIUM = parseInt(res.rows.item(i).PNL_MIN_PREMIUM);
                                    self.pptObj.PNL_MAX_PREMIUM = parseInt(res.rows.item(i).PGL_MAX_PREM_AMT);
                                    self.pptObj.PNL_ID = res.rows.item(i).PNL_ID;
                                    self.pptObj.START_PL = res.rows.item(i).START_PL;
                                    self.pptObj.MIN_TERM = res.rows.item(i).MIN_TERM;
                                    self.pptObj.MAX_TERM = res.rows.item(i).MAX_TERM;
                                    self.pptObj.PPT = "15";
                                    self.pptArrList.push(self.pptObj);

                                    self.pptObj = {};
                                    self.pptObj.PNL_CODE = res.rows.item(i).PNL_CODE;
                                    self.pptObj.MIN_AGE = parseInt(res.rows.item(i).PNL_MIN_AGE);
                                    self.pptObj.MAX_AGE = parseInt(res.rows.item(i).PNL_MAX_AGE);
                                    self.pptObj.PNL_MIN_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MIN_SUM_ASSURED);
                                    self.pptObj.PNL_MAX_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MAX_SUM_ASSURED);
                                    self.pptObj.PNL_MIN_PREMIUM = parseInt(res.rows.item(i).PNL_MIN_PREMIUM);
                                    self.pptObj.PNL_MAX_PREMIUM = parseInt(res.rows.item(i).PGL_MAX_PREM_AMT);
                                    self.pptObj.PNL_ID = res.rows.item(i).PNL_ID;
                                    self.pptObj.START_PL = res.rows.item(i).START_PL;
                                    self.pptObj.MIN_TERM = res.rows.item(i).MIN_TERM;
                                    self.pptObj.MAX_TERM = res.rows.item(i).MAX_TERM;
                                    self.pptObj.PPT =  "20";
                                    self.pptArrList.push(self.pptObj);
                                }
                                else{
                                    self.pptObj = {};
                                    self.pptObj.PNL_CODE = res.rows.item(i).PNL_CODE;
                                    self.pptObj.MIN_AGE = parseInt(res.rows.item(i).PNL_MIN_AGE);
                                    self.pptObj.MAX_AGE = parseInt(res.rows.item(i).PNL_MAX_AGE);
                                    self.pptObj.PNL_MIN_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MIN_SUM_ASSURED);
                                    self.pptObj.PNL_MAX_SUM_ASSURED = parseInt(res.rows.item(i).PNL_MAX_SUM_ASSURED);
                                    self.pptObj.PNL_MIN_PREMIUM = parseInt(res.rows.item(i).PNL_MIN_PREMIUM);
                                    self.pptObj.PNL_MAX_PREMIUM = parseInt(res.rows.item(i).PGL_MAX_PREM_AMT);
                                    self.pptObj.PNL_ID = res.rows.item(i).PNL_ID;
                                    self.pptObj.START_PL = res.rows.item(i).START_PL;
                                    self.pptObj.MIN_TERM = res.rows.item(i).MIN_TERM;
                                    self.pptObj.MAX_TERM = res.rows.item(i).MAX_TERM;
                                    self.pptObj.PPT = res.rows.item(i).PPT;
                                    self.pptArrList.push(self.pptObj);
                                }
                            }
                            debug("Resolved pptArrList  " +JSON.stringify(self.pptArrList));
                            dfd.resolve(res);
                        }
                        else
                        {
                            dfd.resolve(null);
                        }
                    },
                    function (tx, err) {
                        console.log("Error in LP_PNL_PLAN_LK ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_PNL_PLAN_LK(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        CommonService.hideLoading();
        debug("Resolving HSW Plank");
        return dfd.promise;
    };

    this.getPremiumMultiplier = function(pnlId, pmlAge, pmlSex, pmlTerm){
        var dfd = $q.defer();
        console.log("Inside getPremiumMultiplier" +pnlId +"--"+ pmlAge +"--"+ pmlSex +"--"+ pmlTerm);
        CommonService.transaction(sisDB,
            function (tx) {
                CommonService.executeSql(tx, "select * from LP_PML_PREMIUM_MULTIPLE_LK where PML_PNL_ID = ? and PML_AGE = ? and PML_SEX = ?", [pnlId, pmlAge, pmlSex],
                    function (tx, res) {
                        debug("Length "+JSON.stringify(res.rows.length));
                        if (res.rows.length > 0) {
                            self.multiPlier = res.rows.item(0).PML_PREM_MULT;
                            debug("Resolved getPremiumMultiplier  " +JSON.stringify(self.multiPlier));
                            dfd.resolve(self.multiPlier);
                        }
                        else
                        {
                            dfd.resolve(self.multiPlier);
                        }
                    },
                    function (tx, err) {
                        console.log("Error in getPremiumMultiplier ().transaction(): " + err.message);
                        dfd.resolve(self.multiPlier);
                    }
                );
            },
            function (err) {
                console.log("Error in getPremiumMultiplier(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        debug("Exiting getPremiumMultiplier");
        return dfd.promise;
    };

    this.returnRequireVal = function(arr, key){
       var retVal = "";
       arr.forEach(function(item, index) {
         var tempArr = item.split(":");

         if(key == tempArr[0]){
            retVal = tempArr[1];
         }
         if(retVal!="") return;

       });

       return retVal;
    };

    this.generateSIS = function(inputObj){
        CommonService.showLoading("Generating SIS...please wait...");
        debug("gen sis");
        var dfd = $q.defer();
        try{
            var sisReq = {};

            sisReq.InsName = "";

            sisReq.InsAge = inputObj.age;
            sisReq.InsSex = "M";
            sisReq.InsOccDesc = "ACTUARY";
            sisReq.InsOcc = "W02";

            sisReq.OwnName = ""
            sisReq.OwnAge = inputObj.age;
            sisReq.OwnOcc = "W02";
            sisReq.OwnSex = "M";
            sisReq.OwnOccDesc = "ACTUARY";
            //1 is non smoker and 0 is smoker
            sisReq.InsSmoker = "N";
            sisReq.PaymentMode = "A";
            sisReq.Sumassured = inputObj.sumAssured;

            sisReq.PropNo = "";
            sisReq.SAgeProofFlg = "Y";
            sisReq.PropDOB = "";
            sisReq.InsDOB = "";
            sisReq.ProdTye = "U";           // For ULIP : U
            sisReq.commision = "Payable";

            //plan code
            sisReq.BasePlan = inputObj.pnlCode;
            sisReq.PolicyTerm = inputObj.policyTerm;
            sisReq.PPTerm = inputObj.ppt;
            sisReq.PremiumMul = inputObj.premMultiplier;
            sisReq.Frequency = "A";
            sisReq.TataEmployee = "N";
            sisReq.Option = inputObj.option;

            sisReq.ProposerName = "";
            sisReq.ProposerGender = "";
            sisReq.ProposerOccupation = "W02";
            sisReq.ProposerDescOcc = "ACTUARY";
            sisReq.ProposerAge = "18";

            //*********************************ULIP*********************************

            sisReq.BasePremiumAnnual = inputObj.premiumAmt;
            sisReq.SmartDebtFund = "";
            sisReq.SmartEquFund = "";

            sisReq.TLC = "0";
            sisReq.WLA = "0";
            sisReq.WLE = "100";
            sisReq.WLF = "0";
            sisReq.WLI = "0";
            sisReq.WLS = "0";

            sisReq.SmartDebtFund = "";
            sisReq.SmartEquFund = "";

            // Rider
            sisReq.WOPULV1 = "";
            sisReq.WOPPULV1 = "";
            sisReq.ADDLULV2 = "";
            sisReq.ADDLN1V1 = "";
            sisReq.ADDLULV1 = "";
            sisReq.WPPN1V1 = "";

            if(!!LoginService.lgnSrvObj && !!LoginService.lgnSrvObj.userinfo){
                sisReq.AgentName = LoginService.lgnSrvObj.userinfo.AGENT_NAME;
                sisReq.AgentNumber = LoginService.lgnSrvObj.userinfo.AGENT_CD;
                debug("agentMobileNumber:: " + LoginService.lgnSrvObj.userinfo.MOBILENO);
                sisReq.AgentContactNumber = (LoginService.lgnSrvObj.userinfo.MOBILENO || "");
            }

            sisReq.PnlId = inputObj.pnlId;
            sisReq.PglId = inputObj.pglId;

            sisReq.NoHtmlFlag = "Y";

            debug("sisReq: " + JSON.stringify(sisReq));
            cordova.exec(
                function(res){
                    debug("sis success : " + JSON.stringify(res));
                    if(!!res && res.code=='1'){
                        hsms.sisData = res;
                    }
                    else if(!!res && res.code==2){
                        hsms.sisData = res.message;
                        //navigator.notification.alert(res.message,null,"SIS","OK");
                    }
                    else
                        hsms.sisData = "res.SIS is null or undefined";
//                  SisFormService.sisData.sisOutput = res;
                    dfd.resolve(res);
                },
                function(err){
                    debug("SIS Error: " + err);
                    dfd.resolve(null);
                },"PluginHandler", "startSIS", [sisReq]
            );
        }
        catch(ex){
            debug("Exception: " + ex.message);
        }
        return dfd.promise;
    };
    this.getPglID=function(){
        return this.PGL_ID;
    };
    this.getPolicyTerm=function(){
        return this.policyTerm;
    };
    this.setPolicyTerm=function(policyterm){
        this.policyTerm=policyterm;
    };
    this.getSisData = function(){
        return this.sisData;
    };
    this.getMinAge=function(){
    	return this.minAge;
    };
    this.getPPT=function(){
        return this.ppt;
    };
    this.setPPT=function(PPT){
        this.ppt=PPT;
    };
    this.getPnlId=function(){
        return this.pnlId;
    }
    this.getPnlCode=function(){
        return this.pnlCode;
    };
    this.getMaxAge=function(){
    	return this.maxAge;
    };
    this.getMinSumAssuged=function(){
    	return this.minSumAssured;
    };
    this.getMaxSumAssuged=function(){
    	return this.maxSumAssured;
    };
    this.getstartPl=function(){
    	return this.startPl;
    };
    this.getMinPremium=function(){
    	return this.minAnnualPremium;
    };
    this.setMinPremium=function(AnnualPremium){
        this.minAnnualPremium=AnnualPremium;
    };
    this.getMaxPremium=function(){
        	return this.maxAnnualPremium;
    };
    this.setMaxPremium=function(AnnualPremium){
        this.maxAnnualPremium=AnnualPremium;
    };
    this.getPptArray=function(){
        return this.pptArrList;
    }
    this.setLoyaltyCharge=function(loyaltyCharge){
        this.loyaltyChargeValue=loyaltyCharge;
    }
    this.getLoyaltyCharge=function(){
        return this.loyaltyChargeValue;
    };
    this.setLoyaltyChargeDetailArr=function(loyaltyChargeDetails){
        this.loyaltyChargeDetailsArr=loyaltyChargeDetails;
    }
    this.getLoyaltyChargeDetailArr=function(){
        return this.loyaltyChargeDetailsArr;
    };
    this.setTotalFundValue=function(totalFund){
        this.totalFundValue=totalFund;
    }
    this.getTotalFundValue=function(){
        return this.totalFundValue;
    };
    this.setTotalFundValueArray=function(totalFundValue){
         this.totalFundValueArr=totalFundValue;
    };
    this.getTotalFundValueArray=function(){
        return this.totalFundValueArr;
    };
    this.setProductDetails = function(productDetails){
        this.productDetails=productDetails;
    };
    this.getProductDetails = function(){
        return this.productDetails;
    };

}]); 	
