hswFortuneModule.controller('fortuneMaximaPageCtrl',['$state','$scope','hswULIPService',function($state,$scope,hswULIPService){
	"use strict";
	this.htmlPathSIP = '../app/hsw/hswULIP/fortuneMaxima/fortuneMaxima.html';
	var self=this;
	self.annualPerminum="100000";
	self.totalMaturityBenifit="100000";
	self.loyaltyAddition="3798";
	self.premiumFundValue="1088956";
	self.policyTerm="100";
	self.ppt="1";
	self.sumAssured=1.25*parseInt(self.annualPerminum);
    $scope.$emit('passSumAssured',{message:self.sumAssured});
	self.percent=(parseInt(self.ppt)/parseInt(self.policyTerm))*100 || 0;

	$scope.$on('ULIPonClickOnGoButton',function(event){

		self.annualPerminum=hswULIPService.getMinPremium();
		self.totalMaturityBenifit=hswULIPService.getMinPremium() || 0;
		self.loyaltyAddition=hswULIPService.getLoyaltyCharge() || 0;
		self.premiumFundValue=hswULIPService.getTotalFundValue() || 0;
		self.policyTerm=hswULIPService.getPolicyTerm() || 0;
		self.ppt=hswULIPService.getPPT() || 0;
		self.percent=(parseInt(self.ppt)/parseInt(self.policyTerm))*100;
		debug("self.totalMaturityBenifit"+self.totalMaturityBenifit);
		debug("self.loyaltyAddition"+self.loyaltyAddition);
		debug("self.premiumFundValue"+self.premiumFundValue);
		debug("self.policyTerm"+self.policyTerm);
		debug("self.ppt"+self.ppt);
		debug("self.percent"+self.percent);
		if(self.ppt=="1"){
            self.sumAssured=1.25*parseInt(self.annualPerminum);
            $scope.$emit('passSumAssured',{message:self.sumAssured});
        }
/*        else{
            self.option1SumAssured=10*parseInt(self.annualPerminum);
            self.option2SumAssured=0.5*parseInt(self.annualPerminum)*parseInt(self.policyTerm);
            if(self.option1SumAssured>self.option2SumAssured)
                self.sumAssured=self.option1SumAssured;
            else
                self.sumAssured=self.option2SumAssured;
             $scope.$emit('passSumAssured',{message:self.sumAssured});
        }*/
	});

	self.processToSiS = function(){
	    var productDetails = hswULIPService.getProductDetails();
        debug("Retrieving product details"+JSON.stringify(productDetails));
		$state.go("dashboard.productLandingPage",{"PGL_ID": productDetails.PGL_ID, "SOL_TYPE": productDetails.SOL_TYPE, "PROD_NAME": productDetails.PROD_NAME, "FHR_ID": productDetails.FHR_ID, "LEAD_ID": productDetails.LEAD_ID, "OPP_ID": productDetails.OPP_ID});

    };

    var stateParameters = null;
    $scope.$on('StateParameters',function(event,stateParameters){
        stateParameters = stateParameters;
    });

	this.setValues=function(){
		self.annualPerminum=hswULIPService.getMinPremium();
		self.totalMaturityBenifit=hswULIPService.getMinPremium();
		self.loyaltyAddition=hswULIPService.getLoyaltyCharge();
		self.premiumFundValue=hswULIPService.getTotalFundValue();
		self.policyTerm=hswULIPService.getPolicyTerm();
		self.ppt=hswULIPService.getPPT();
		self.percent=(parseInt(self.ppt)/parseInt(self.policyTerm))*100;

	}
}]);
