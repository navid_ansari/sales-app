hswULIPModule.controller('hswULIPagesCtrl', ['$q', '$http', '$state', 'CommonService', 'hswULIPService', '$scope','HswPlanLK','$stateParams','ProductDetails', function($q, $http, $state, CommonService, hswULIPService, scope,HswPlanLK,$stateParams,ProductDetails) {
    "use strict";
    debug("PGL_ID"+$stateParams.PGL_ID);
    CommonService.hideLoading();

    if($stateParams.PGL_ID=="201")
    	scope.ctrlName = "wealthMaximaPageCtrl" + " as HSWMain";
    else
    	scope.ctrlName = "fortuneMaximaPageCtrl" + " as HSWMain";
    var self = this;

    self.openMenu = function(){
        CommonService.openMenu();
    };
    scope.$on('passSumAssured',function(event,data){
        console.log('on working');
         self.sumAssuredVal=data.message;
      });
    self.pptArrList=hswULIPService.getPptArray();
//    scope.$broadcast('StateParameters',$stateParams);

    hswULIPService.setProductDetails(ProductDetails);

    self.selectedTerm = self.pptArrList[0];
    self.click = false;
    self.optionVal = true;
    self.minAge = hswULIPService.getMinAge();
    self.maxAge = hswULIPService.getMaxAge();
    self.mAP=hswULIPService.getMinPremium();
    self.minAnnualPremium=hswULIPService.getMinPremium();
    self.maxAnnualPremium=hswULIPService.getMaxPremium();
    self.policyTerm=100-self.minAge;
    self.ppt=hswULIPService.getPPT();
    self.pnlCode=hswULIPService.getPnlCode();
    self.pnl_id=hswULIPService.getPnlId();
    self.PglId=hswULIPService.getPglID();
    self.multiplier=0;
    self.sumAssuredVal = 0;
    // this.option=document.getElementById("toggleval").innerHTML;
    self.updatePaymentTerm=function(){
    	self.policyTerm=100-self.minAge;
    };

    self.setPremMult=function(){
        hswULIPService.getPremiumMultiplier(self.pnl_id, self.minAge, "U", 1).then(function(res){
            self.multiplier = res;
            self.sumAssuredVal = self.minAnnualPremium * self.multiplier;
            debug("Multiplier is " + JSON.stringify(self.multiplier));
            debug("Sum assured is " + JSON.stringify(self.sumAssuredVal));
        });
    };

//    debug("pptArrList  " +JSON.stringify(self.pptArrList));
    debug("minAge  " + hswULIPService.getMinAge());
    debug("maxAge " + hswULIPService.getMaxAge());
    debug("minAnnualPremium " + hswULIPService.getMinPremium());
    self.updateBasedOnPPT=function(selectedTerm){
    	debug(JSON.stringify(selectedTerm));
    	self.minAge = selectedTerm.MIN_AGE;
	    self.maxAge = selectedTerm.MAX_AGE;
	    self.mAP=selectedTerm.PNL_MIN_PREMIUM;
	    self.minAnnualPremium=selectedTerm.PNL_MIN_PREMIUM;
	    self.maxAnnualPremium=selectedTerm.PNL_MAX_PREMIUM;
	    self.policyTerm=100-self.minAge;
	    self.ppt=self.selectedTerm.PPT;
    	self.pnlCode=selectedTerm.PNL_CODE;
    	self.pnl_id=selectedTerm.PNL_ID;
    	debug("maxAnnualPremium  " + self.maxAnnualPremium);
	    debug("maxAge " +self.maxAge);
	    debug("policyTerm " + self.policyTerm);
	    debug("ppt " + self.ppt);
	    debug("pnlCode " + self.pnlCode);
	    // debug("option " + self.option);

    };
    self.calculateData = function(HSWULIPForm) {
        CommonService.showLoading();
        debug("Go Clicked");
        self.click = true;
       // self.setPremMult();
        debug("PNLCODE SELECT "+self.selectedTerm.PPT);
	    debug("minAge  " + self.minAge);
	    debug("maxAge " +self.maxAge);
	    debug("policyTerm " + self.policyTerm);
	    debug("ppt " + self.ppt);
	    debug("pnlCode " + self.pnlCode);
	    if(HSWULIPForm.$invalid!=true){
	    // debug("option " + self.customText);
        hswULIPService.getPremiumMultiplier(self.pnl_id, self.minAge, "U", 1).then(function(res){
             self.multiplier = res;
             self.sumAssuredVal = self.minAnnualPremium * self.multiplier;
             debug("Multiplier is " + JSON.stringify(self.multiplier));
             debug("Sum assured is " + JSON.stringify(self.sumAssuredVal));
             var inputObj = {};
             inputObj.option = (self.optionVal == true?"4":"8");
             inputObj.age = self.minAge;
             inputObj.ppt = parseInt(self.ppt);
             inputObj.policyTerm = parseInt(self.policyTerm);
             inputObj.pnlCode = self.pnlCode;
             inputObj.pnlId = self.pnl_id;
             inputObj.premiumAmt = self.minAnnualPremium;
             inputObj.pglId = self.PglId;
             inputObj.sumAssured = self.sumAssuredVal;
             inputObj.premMultiplier = self.multiplier;
             debug("Input Obj is " + JSON.stringify(inputObj));
                 hswULIPService.generateSIS(inputObj).then(
                     function(resp){
                         debug("Response of SIS Is");
                         debug(JSON.stringify(resp));
                         /*
                         var sisData = resp;
                         var totalFundValueArr = sisData.TotalFundValueEndPolicyYear.replace(/=/g , ":").replace(/\s/g,'').replace("}","").replace("{","").split(",");
                         console.log(totalFundValueArr);

                         var loyaltyChargeDetailsArr = sisData.LoyaltyChargeDetails.replace(/=/g , ":").replace(/\s/g,'').replace("}","").replace("{","").split(",");
                         console.log(loyaltyChargeDetailsArr);

                         var totalFundValue = hswULIPService.returnRequireVal(totalFundValueArr,inputObj.policyTerm);
                         console.log(totalFundValue);

                         var loyaltyChargeValue = hswULIPService.returnRequireVal(loyaltyChargeDetailsArr,inputObj.policyTerm);
                         console.log(loyaltyChargeValue);
                         */

                         console.log("Total fund value is "+ JSON.stringify(resp.TotalFundValueEndPolicyYear));
                         console.log("Total charge value is "+ JSON.stringify(resp.LoyaltyChargeDetails));
                         
                         var totalFundValue = resp.TotalFundValueEndPolicyYear;
                         var loyaltyChargeValue = resp.LoyaltyChargeDetails;
                         
                        hswULIPService.setLoyaltyCharge(loyaltyChargeValue);
//                        hswULIPService.setLoyaltyChargeDetailArr(loyaltyChargeDetailsArr);
                        hswULIPService.setTotalFundValue(totalFundValue);
//                        hswULIPService.setTotalFundValueArray(totalFundValueArr);
                        hswULIPService.setMinPremium(self.minAnnualPremium);
                        hswULIPService.setPolicyTerm(self.policyTerm);
                        hswULIPService.setPPT(self.ppt);
                         scope.$broadcast('ULIPonClickOnGoButton');
                         CommonService.hideLoading();
                     }
                 );
         });
        }
        else{
            navigator.notification.alert("Please fill all the mandatory data !!",null,"HSW","OK");
        }


    };


}]);
