hswHeaderModule.controller('hswPagesCtrl', ['$q', '$http', '$state', 'CommonService', 'hswService', '$scope','$stateParams','ProductDetails', function($q, $http, $state, CommonService, hswService, scope,$stateParams,ProductDetails) {
    "use strict";
    CommonService.hideLoading();
    scope.ctrlName = "smartIncomePlusPageCtrl " + " as HSWMain";
    var self = this;

    self.openMenu = function(){
        CommonService.openMenu();
    };

    var pglIdsObj = {sip:$stateParams.PGL_ID};
    scope.$broadcast('StateParameters',$stateParams);

    self.selectedOption="Option 1";
    self.click = false;
    self.minAge = parseInt(hswService.getHSW_MIN_AGE());
    self.maxAge = parseInt(hswService.getHSW_MAX_AGE());
    self.ppt = hswService.getHSW_PPT();
    self.minTerm = hswService.getHSW_MIN_TERM();
    self.maxTerm = hswService.getHSW_MAX_TERM();
    self.planCodeArrList = hswService.getPlanCodeArrList();
    self.planGroupArrList = hswService.getPlanGroupArrList();
    self.planGroupDetailsMasterList = [];
    self.planGroupDetailsList = [];

    self.planGroupDetailsMasterList = hswService.getHSW_PLAN_GROUP_OBJ();

    self.selectedPlanCode = self.planCodeArrList[0];
    self.selectedGroupCode = self.planGroupArrList[0];
    self.policyNameCount = self.planCodeArrList.length;
    self.planTermArrayList = [];
    hswService.setHSW_SELECTED_TERM(self.minTerm);
    hswService.setProductDetails(ProductDetails);

    for (var i = parseInt(self.minTerm); i <= parseInt(self.maxTerm); i++) {
        debug("I" + i);
        var planTermObj = {};
        planTermObj.TERM = i;
        self.planTermArrayList.push(planTermObj);
    }

    self.minSumAssured = parseInt(hswService.getHSW_MIN_SUM_ASSURED());
    self.maxSumAssured = parseInt(hswService.getHSW_MAX_SUM_ASSURED());
    self.minAnnualPremium = parseInt(hswService.getHSW_MIN_ANNUAL_PREMIUM());
    self.startPl = hswService.getHSW_START_PL();
    self.annualPremium =(self.startPl=="P")?self.minAnnualPremium:self.minSumAssured ;
    self.age = self.minAge;

    debug("Self is on Controller launch");
    debug(self);

    scope.$on('passSumAssured',function(event,data){
        console.log('on working');
        self.sumAssuredVal=data.message;
    });

    /**
     *Function  get Policy Group 
     * 
     **/

    self.getPNLPolicyBasedOnOption = function() {
        debug("Selected POSITION" + self.selectedGroupCode.POS);

        self.selectedPlanCode = self.planCodeArrList[0];
        if(parseInt(self.selectedGroupCode.POS)==0){
            self.selectedOption="Option 1";
        }
        else{
             self.selectedOption="Option 2";
        }

        self.setPlanRelatedPolicyTerms(self.selectedGroupCode.PGF_PNL_CODE);
        self.getPNLTermPpt();

/*        hswService.getPolicyArray(self.selectedGroupCode.POS).then(
            function(res) {
                self.planCodeArrList = res;
                self.selectedPlanCode = self.planCodeArrList[0];
                debug("Plan Group ");
                debug(res);
                self.selectedGroupCode = self.planGroupArrList[parseInt(self.selectedGroupCode.POS)];
                if(parseInt(self.selectedGroupCode.POS)==0){
                    self.selectedOption="Option 1";
                }
                else{
                     self.selectedOption="Option 2";
                }
                self.getPNLTermPpt();
            });*/
    };

    self.setPlanRelatedPolicyTerms = function(PGF_PNL_CODE){
        self.planGroupDetailsList = [];
        debug("Length of self.planGroupDetailsMasterList : "+self.planGroupDetailsMasterList.length);
        for(var i=0; i<self.planGroupDetailsMasterList.length;i++){
            if(PGF_PNL_CODE == self.planGroupDetailsMasterList[i].PGF_PNL_CODE){
                self.planGroupDetailsList.push(self.planGroupDetailsMasterList[i]);
            }
        }

        self.selectedPlanTerm = self.planGroupDetailsList[2];
        self.getPNLTermPpt();
        debug("Plan group details list "+ JSON.stringify(self.planGroupDetailsList));
    };


    /**
     *Function  get PNL Term 
     * 
     **/

    self.getPNLTermPpt = function() {

        debug(JSON.stringify(self.selectedPlanTerm));
        debug("Selected Option" + self.selectedOption);
        debug("Selected POSITION" + self.selectedPlanCode.POS);

        self.ppt = self.selectedPlanTerm.PPT;
        self.minTerm = self.selectedPlanTerm.MIN_TERM;
        self.maxTerm = self.selectedPlanTerm.MAX_TERM;
        self.minAge = parseInt(self.selectedPlanTerm.PNL_MIN_AGE);
        self.maxAge = parseInt(self.selectedPlanTerm.PNL_MAX_AGE);
        self.minSumAssured = parseInt(self.selectedPlanTerm.PNL_MIN_SUM_ASSURED);
        self.maxSumAssured = parseInt(self.selectedPlanTerm.PNL_MAX_SUM_ASSURED);
        self.minAnnualPremium = parseInt(self.selectedPlanTerm.PNL_MIN_PREMIUM);
        self.startPl = self.selectedPlanTerm.PNL_START_PT;
        self.annualPremium =(self.startPl=="P")?self.minAnnualPremium:self.minSumAssured ;
        self.age = self.minAge;

        hswService.setHSW_PLAN_OBJ(self.selectedPlanTerm);
        hswService.setHSW_OPTION(self.selectedOption);
        hswService.setHSW_PPT(self.ppt);
        hswService.setHSW_MIN_TERM(self.minTerm);
        hswService.setHSW_MAX_TERM(self.maxTerm);
        hswService.setPNL_CODE(self.selectedPlanTerm.PNL_CODE);
        hswService.setHSW_SELECTED_AGE(self.minAge);

        var annualPremium =(self.startPl=="P")?self.minAnnualPremium:self.minSumAssured ;
        debug("annualPremium  " +annualPremium);
        hswService.setHSW_SELECTED_PREMIUM(annualPremium);

        if (self.minTerm == self.maxTerm) {
            debug("Broadcasting");
            hswService.setHSW_SELECTED_TERM(self.minTerm);
            scope.$broadcast('onDropDownChange', self.minTerm);
        } else {
            hswService.setHSW_SELECTED_TERM(self.selectedPlanTerm.TERM);
            scope.$broadcast('onDropDownChange', self.selectedPlanTerm.TERM);
        }

/*        hswService.getHSWPnltermppt().then(
            function(res) {
                self.ppt = hswService.getHSW_PPT();
                self.minTerm = hswService.getHSW_MIN_TERM();
                self.maxTerm = hswService.getHSW_MAX_TERM();
               // self.data = hswService.getHSW_PLAN_OBJ();
                //debug("DATA"+JSON.stringify(self.data.rows.item));
               // console.log(self.data.rows.item(1));
                hswService.getHSWPlanDetailsViaPNLCODE(self.selectedPlanTerm.PNL_CODE).then(
                function(res) {
                    debug(JSON.stringify(res.rows.item(0)));
                    self.minAge = parseInt(res.rows.item(0).PNL_MIN_AGE);
                    self.maxAge = parseInt(res.rows.item(0).PNL_MAX_AGE);
                    self.minSumAssured = parseInt(res.rows.item(0).PNL_MIN_SUM_ASSURED);
                    self.maxSumAssured = parseInt(res.rows.item(0).PNL_MAX_SUM_ASSURED);
                    self.minAnnualPremium = parseInt(res.rows.item(0).PNL_MIN_PREMIUM);
                    self.startPl = res.rows.item(0).PNL_START_PT;
                    self.annualPremium =(self.startPl=="P")?self.minAnnualPremium:self.minSumAssured ;
                    self.age = self.minAge;
                    scope.$broadcast('onDropDownChange', self.minTerm);
                });
            });*/
    };

    /**
     *Function to reder UI in the child controller progress bar  
     *This function will get trigger once select the dropdown
     **/

    self.getSelectedPlanTerm = function() {
        debug("Selected Term" + self.selectedPlanTerm.TERM);
        if (self.minTerm == self.maxTerm) {
            hswService.setHSW_SELECTED_TERM(self.minTerm);
            scope.$broadcast('onDropDownChange', self.minTerm);
        } else {
            hswService.setHSW_SELECTED_TERM(self.selectedPlanTerm.TERM);
            scope.$broadcast('onDropDownChange', self.selectedPlanTerm.TERM);
        }

    };

    /**
     *Function to calculate and reder UI in the child controller 
     *This function will get trigger once click on the GO button 
     */
    self.calculateData = function(HSWForm) {
        debug("Go Clicked");
        debug("Self in calculateData");
        //CommonService.showLoading("Calculatiog..");
        debug(self);
        self.click = true;
        if (self.minTerm == self.maxTerm) {
            hswService.setHSW_SELECTED_TERM(self.minTerm);
        } else {
            hswService.setHSW_SELECTED_TERM(self.selectedPlanTerm.TERM);
        }
//        alert(self.age);
        hswService.setHSW_SELECTED_AGE(self.age);
        hswService.setHSW_SELECTED_PREMIUM(self.annualPremium);

        hswService.setHSW_OPTION(self.selectedOption);
        scope.$broadcast('onClickOnGoButton');
        //CommonService.hideLoading();
    };

    self.setPlanRelatedPolicyTerms(self.selectedGroupCode.PGF_PNL_CODE);

    debug("Plan Term Array" + JSON.stringify(self.planGroupDetailsList));
    self.selectedPlanTerm = self.planGroupDetailsList[2];
    self.ppt = self.planGroupDetailsList[2].PPT;
    self.minAge = parseInt(self.planGroupDetailsList[2].PNL_MIN_AGE);
    self.maxAge = parseInt(self.planGroupDetailsList[2].PNL_MAX_AGE);
    self.minTerm = self.planGroupDetailsList[2].MIN_TERM;
    self.maxTerm = self.planGroupDetailsList[2].MAX_TERM;

}]);



/**
 *This directive used for the setting controller names dynamically to the HTMl 
 *created om 18 OCT 2016 
 */
