hswHeaderModule.service('hswService', ['$q', '$http', '$state', 'CommonService','LoginService', function ($q, $http, $state, CommonService,LoginService) {
    "use strict";
    var self = this;
    var PGL_ID = "";
    var SOL_TYPE = "";
    var PROD_NAME = "";
    var FHR_ID = "";
    var SIS_FHR_ID = "";
    var LEAD_ID = "";
    var OPP_ID = "";
    var HSW_PLAN_GROUP_OBJ = [];
    var HSW_PLAN_OBJ = {};
    var HSW_PLAN_GROUP_FEATURE_OBJ = {};
    var PNL_CODE = "";
    var minAge = "";
    var maxAge = "";
    var minTerm = "";
    var maxTerm = "";
    var minSumAssured = "";
    var maxSumAssured = "";
    var minAnnualPremium = "";
    var startPl = "";
    var ppt = "";
    var selectedTerm = "";
    var option = "Option 1";
    var selectedAge = "";
    var selectedPremium = "";
    var planCodeArrList = [];
    var planGroupArrList = [];
    var counter = 1;
    var hsms = this;
    this.sisData = {};
    this.productDetails = {};

    this.resetHswServiceVals = function(){
        PGL_ID = "";
        SOL_TYPE = "";
        PROD_NAME = "";
        FHR_ID = "";
        SIS_FHR_ID = "";
        LEAD_ID = "";
        OPP_ID = "";
        HSW_PLAN_GROUP_OBJ = [];
        HSW_PLAN_OBJ = {};
        HSW_PLAN_GROUP_FEATURE_OBJ = {};
        PNL_CODE = "";
        minAge = "";
        maxAge = "";
        minTerm = "";
        maxTerm = "";
        minSumAssured = "";
        maxSumAssured = "";
        minAnnualPremium = "";
        startPl = "";
        ppt = "";
        selectedTerm = "";
        option = "Option 1";
        selectedAge = "";
        selectedPremium = "";
        planCodeArrList = [];
        planGroupArrList = [];
        counter = 1;
        this.sisData = {};
        this.productDetails = {};
    };

    this.setHSWPlanGroupDetails = function (pgl_ID) {
        PGL_ID = pgl_ID;
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                var query = "select a.*,LPTPL.MIN_TERM as MIN_TERM,LPTPL.MAX_TERM as MAX_TERM,LPTPL.PPT as PPT from ( select LPPGF.PGF_ID,LPPGF.PGF_PNL_CODE,LPPGF.PGF_FEATURE ,LPPL.* "+
                    " FROM LP_PNL_PLAN_LK LPPL,LP_PGF_PLAN_GROUP_FEATURE LPPGF "+
                    " WHERE LPPGF.PGF_ATTACHED_PLANS LIKE '%' || LPPL.PNL_CODE || '%' "+
                    " AND LPPL.PNL_PGL_ID=? "+
                    " ORDER BY PGF_FEATURE ) a "+
                    " INNER JOIN LP_PNL_TERM_PPT_LK LPTPL ON LPTPL.PNL_CODE=a.PNL_CODE";

                debug("Querry :  " + query);
                debug("PGL_ID :  " + PGL_ID);
                CommonService.executeSql(tx, query, [PGL_ID],
                    function (tx, res) {
                        debug("Result of getHSWPlanGroupDetails : " +JSON.stringify(res));
                        self.setHSW_PLAN_GROUP_OBJ(res);
                        dfd.resolve(null);
                    },
                    function (tx, err) {
                        console.log("Error in getHSWPlanGroupDetails().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in getHSWPlanGroupDetails(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    this.getPolicyArray = function (pos) {
        var dfd = $q.defer();
        var Position = parseInt(pos);
        planCodeArrList = [];
        var planGroupFeatuureObj = self.getHSW_PLAN_GROUP_FEATURE_OBJ();

        var plancode = planGroupFeatuureObj.rows.item(Position).PGF_ATTACHED_PLANS;
        var planarr = plancode.split(",");
        debug("Plan Code" + plancode);
        for (var i = 0; i < planarr.length; i++) {
            var planCodeObj = {};
            planCodeObj.PNL_CODE = planarr[i];
            planCodeObj.POS = i;
            planCodeArrList.push(planCodeObj);
        }
        dfd.resolve(planCodeArrList);
        console.log(planCodeArrList);
        return dfd.promise;
    };

    this.getHSWPlanDetails = function () {
        debug("Counter is "+ counter++);
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                debug("Querry :  " + "SELECT * from LP_PNL_PLAN_LK  where PNL_PGL_ID = '" + PGL_ID + "'");
                CommonService.executeSql(tx, "SELECT * from LP_PNL_PLAN_LK  where PNL_PGL_ID = '" + PGL_ID + "'", [],
                    function (tx, res) {
                        self.setHSW_PLAN_OBJ(res);
                        self.setPNL_CODE(res.rows.item(0).PNL_CODE);
                        minAge = res.rows.item(0).PNL_MIN_AGE;
                        maxAge = res.rows.item(0).PNL_MAX_AGE;
                        minSumAssured = res.rows.item(0).PNL_MIN_SUM_ASSURED;
                        maxSumAssured = res.rows.item(0).PNL_MAX_SUM_ASSURED;
                        minAnnualPremium = res.rows.item(0).PNL_MIN_PREMIUM;
                        startPl = res.rows.item(0).PNL_START_PT;
                        self.setHSW_SELECTED_AGE(minAge);
                        debug("minAnnualPremium  " + minAnnualPremium);
                        debug("minSumAssured " + minSumAssured);
                        var annualPremium =(startPl=="P")?minAnnualPremium:minSumAssured ;
                        debug("annualPremium  " +annualPremium);
                        self.setHSW_SELECTED_PREMIUM(annualPremium);
                        dfd.resolve(res);
                        debug("Data from LP_PNL_PLAN_LK  " + JSON.stringify(res));
                    },
                    function (tx, err) {
                        console.log("Error in LP_PNL_PLAN_LK ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_PNL_PLAN_LK(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    this.getHSWPlanDetailsViaPNLCODE = function (pnlCode) {
        debug("Counter is "+ counter++);
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                debug("Querry :  " + "SELECT * from LP_PNL_PLAN_LK  where PNL_CODE = '" + pnlCode + "'");
                CommonService.executeSql(tx, "SELECT * from LP_PNL_PLAN_LK  where PNL_CODE = '" + pnlCode + "'", [],
                    function (tx, res) {
                        self.setHSW_PLAN_OBJ(res);
                        self.setPNL_CODE(res.rows.item(0).PNL_CODE);
                        minAge = res.rows.item(0).PNL_MIN_AGE;
                        maxAge = res.rows.item(0).PNL_MAX_AGE;
                        minSumAssured = res.rows.item(0).PNL_MIN_SUM_ASSURED;
                        maxSumAssured = res.rows.item(0).PNL_MAX_SUM_ASSURED;
                        minAnnualPremium = res.rows.item(0).PNL_MIN_PREMIUM;
                        startPl = res.rows.item(0).PNL_START_PT;
                        self.setHSW_SELECTED_AGE(minAge);
                        debug("minAnnualPremium  " + minAnnualPremium);
                        debug("minSumAssured " + minSumAssured);
                        var annualPremium =(startPl=="P")?minAnnualPremium:minSumAssured ;
                        debug("annualPremium  " +annualPremium);
                        self.setHSW_SELECTED_PREMIUM(annualPremium);
                        dfd.resolve(res);
                        debug("Data from LP_PNL_PLAN_LK  " + JSON.stringify(res));
                        debug(JSON.stringify(res.rows.item(0)));
                    },
                    function (tx, err) {
                        console.log("Error in LP_PNL_PLAN_LK ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_PNL_PLAN_LK(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };

    this.getHSWPlanGroupFeatureDetails = function () {
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                debug("Querry :  " + "SELECT * from LP_PGF_PLAN_GROUP_FEATURE  where PGF_PGL_ID = '" + PGL_ID + "'");
                CommonService.executeSql(tx, "SELECT * from LP_PGF_PLAN_GROUP_FEATURE  where PGF_PGL_ID = '" + PGL_ID + "'", [],
                    function (tx, res) {
                        planGroupArrList = [];
                        if (res.rows.length > 0) {
                            self.setHSW_PLAN_GROUP_FEATURE_OBJ(res);
                            for (var i = 0; i < res.rows.length; i++) {
                                var planGroupObj = {};
                                planGroupObj.PLAN_NAME = res.rows.item(i).PGF_FEATURE;
                                planGroupObj.PGF_PNL_CODE = res.rows.item(i).PGF_PNL_CODE;
                                planGroupObj.POS = i;
                                planGroupArrList.push(planGroupObj);
                            }
                            planCodeArrList = [];
                            var plancode = res.rows.item(0).PGF_ATTACHED_PLANS;
                            var planarr = plancode.split(",");
                            debug("Plan Code" + plancode);
                            for (var j = 0; j < planarr.length; j++) {
                                var planCodeObj = {};
                                planCodeObj.PNL_CODE = planarr[i];
                                planCodeObj.POS = j;
                                planCodeArrList.push(planCodeObj);
                            }
                            console.log(planCodeArrList);
                        }
                        else {
                            navigator.notification.alert("No Record found in LP_PGF_PLAN_GROUP_FEATURE for PGL_ID",null,"HSW","OK");                        
                        }
                        dfd.resolve(res);
                        debug("Data from LP_PGF_PLAN_GROUP_FEATURE  " + JSON.stringify(res));
                    },
                    function (tx, err) {
                        console.log("Error in LP_PGF_PLAN_GROUP_FEATURE ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_PGF_PLAN_GROUP_FEATURE(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };
    
    this.getHSWPnltermppt = function () {
        var dfd = $q.defer();
        CommonService.transaction(sisDB,
            function (tx) {
                debug("Querry :  " + "SELECT * from LP_PNL_TERM_PPT_LK  where PNL_CODE  = '" + PNL_CODE + "'");
                CommonService.executeSql(tx, "SELECT * from LP_PNL_TERM_PPT_LK  where PNL_CODE  = '" + PNL_CODE + "'", [],
                    function (tx, res) {
                        minTerm = res.rows.item(0).MIN_TERM;
                        maxTerm = res.rows.item(0).MAX_TERM;
                        ppt = res.rows.item(0).PPT;
                        debug("MIN TERM : " + minTerm);
                        debug("MAX TERM : " + maxTerm);
                        debug("MIN AGE : " + minAge);
                        debug("MAX AGE : " + maxAge);
                        debug("PPT : " + ppt);    
                        

                        debug("Data from LP_PNL_TERM_PPT_LK  " + JSON.stringify(res));
                        dfd.resolve(res);
                    },
                    function (tx, err) {
                        console.log("Error in LP_PNL_TERM_PPT_LK ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_PNL_TERM_PPT_LK(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        CommonService.hideLoading();
        return dfd.promise;
    };

    this.getRequiredData = function () {
        var obj = {};
        obj.PGL_ID = this.PGL_ID;
        obj.SOL_TYPE = this.SOL_TYPE;
        obj.PROD_NAME = this.PROD_NAME;
        obj.FHR_ID = this.FHR_ID;
        obj.SIS_FHR_ID = this.SIS_FHR_ID;
        obj.LEAD_ID = this.LEAD_ID;
        obj.OPP_ID = this.OPP_ID;
        return obj;
    };
    this.setRequiredData = function (pgl_ID, sol_TYPE, prod_NAME, fhr_ID, sis_FHR_ID, lead_ID, opp_ID) {

        PGL_ID = pgl_ID;
        SOL_TYPE = sol_TYPE;
        PROD_NAME = prod_NAME;
        FHR_ID = fhr_ID;
        SIS_FHR_ID = sis_FHR_ID;
        LEAD_ID = lead_ID;
        OPP_ID = opp_ID;
    };
   
    this.setPNL_CODE = function (pnl_CODE) {
        PNL_CODE = pnl_CODE;
    };
    this.getPNL_CODE = function () {
        return PNL_CODE;
    };
    this.setHSW_PLAN_GROUP_OBJ = function (res) {
        HSW_PLAN_GROUP_OBJ = [];
        for (var i = 0; i < res.rows.length; i++) {
            HSW_PLAN_GROUP_OBJ.push(res.rows.item(i));
        }
        debug("HSW_PLAN_GROUP_OBJ after set : "+ JSON.stringify(HSW_PLAN_GROUP_OBJ));
    };
    this.getHSW_PLAN_GROUP_OBJ = function () {
        debug("HSW_PLAN_GROUP_OBJ after get : "+ JSON.stringify(HSW_PLAN_GROUP_OBJ));
        return HSW_PLAN_GROUP_OBJ;
    };
    this.setHSW_PLAN_OBJ = function (hsw_PLAN_OBJ) {
        HSW_PLAN_OBJ = hsw_PLAN_OBJ;
    };
    this.getHSW_PLAN_OBJ = function () {
        return HSW_PLAN_OBJ;
    };
    this.setHSW_PLAN_GROUP_FEATURE_OBJ = function (hsw_PLAN_GROUP_FEATURE_OBJ) {
        HSW_PLAN_GROUP_FEATURE_OBJ = hsw_PLAN_GROUP_FEATURE_OBJ;
    };
    this.getHSW_PLAN_GROUP_FEATURE_OBJ = function () {
        return HSW_PLAN_GROUP_FEATURE_OBJ;
    };
    this.getPlanCodeArrList = function () {
        return planCodeArrList;
    };
    this.getPlanGroupArrList = function () {
        return planGroupArrList;
    };
    this.getHSW_MIN_TERM = function () {
        return minTerm;
    };
    this.setHSW_MIN_TERM = function (minTermVal) {
        minTerm = minTermVal;
    };
    this.getHSW_MAX_TERM = function () {
        return maxTerm;
    };
    this.setHSW_MAX_TERM = function (maxTermVal) {
        maxTerm = maxTermVal;
    };
    this.getHSW_MIN_AGE = function () {
        return minAge;
    };
    this.getHSW_MAX_AGE = function () {
        return maxAge;
    };
    this.getHSW_PPT = function () {
        return ppt;
    };
    this.setHSW_PPT = function (pptVal) {
        ppt = pptVal;
    };
    this.getHSW_MIN_SUM_ASSURED = function () {
        return minSumAssured;
    };
    this.getHSW_MAX_SUM_ASSURED = function () {
        return maxSumAssured;
    };
    this.getHSW_MIN_ANNUAL_PREMIUM = function () {
        return minAnnualPremium;
    };
    this.getHSW_START_PL = function () {
        return startPl;
    };
    this.getHSW_SELECTED_TERM = function () {
        return selectedTerm;
    };
    this.setHSW_SELECTED_TERM = function (termSelected) {
        selectedTerm = termSelected;
    };
    this.setHSW_OPTION = function (optionval) {
        option = optionval;
    };
    this.getHSW_OPTION = function () {
        return option;
    };
    this.setHSW_SELECTED_AGE = function (age) {
        selectedAge = age;
    };
    this.getHSW_SELECTED_AGE = function () {
        return selectedAge;
    };
    this.setHSW_SELECTED_PREMIUM = function (premium) {
        selectedPremium = premium;
    };
    this.getHSW_SELECTED_PREMIUM = function () {
        return selectedPremium;
    };
    this.setProductDetails = function(productDetails){
        this.productDetails=productDetails;
    };
    this.getProductDetails = function(){
        return this.productDetails;
    };
}]); 	
