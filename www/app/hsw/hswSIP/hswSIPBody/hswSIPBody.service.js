hswBodyModule.service('smartIncomePlusSevice',['$q', '$http', '$state','CommonService', function($q,$http,$state,CommonService){
    this.getGP_FOR_SIP=function(optionNo, insuredAge,ppt){
    	var dfd = $q.defer();
        CommonService.transaction(sisDB,
        function(tx){
           debug("Querry :  "+"SELECT * from LP_SIS_RATES  where PPT = '"+ppt+"' AND Gender = 'M' AND Key = 'GP' AND Option= '"+optionNo+"' AND Lo_Age > '"+insuredAge+"' AND Hi_Age <'"+insuredAge+"' ");
            CommonService.executeSql(tx,"SELECT * from LP_SIS_RATES  where PPT = '"+ppt+"' AND Gender = 'M' AND Key = 'GP' AND Option= '"+optionNo+"' AND Lo_Age > '"+insuredAge+"' AND Hi_Age <'"+insuredAge+"' ",[],
            function(tx,res){
                    if(res.rows.length > 0){
                            // self.setHSW_PLAN_GROUP_FEATURE_OBJ(res);
                            navigator.notification.alert("Record found ",null,"HSW","OK");
                            }
                          else{
                             navigator.notification.alert("No Record found in LP_SIS_RATES for PGL_ID",null,"Application","OK");                             
                          }
                    dfd.resolve(res);
                    debug("Data from LP_SIS_RATES  "+JSON.stringify(res));
            },
            function(tx,err){
                console.log("Error in LP_SIS_RATES ().transaction(): " + err.message);
                dfd.resolve(null);
            }
            );
        },
        function(err){
            console.log("Error in LP_SIS_RATES(): " + err.message);
            dfd.resolve(null);
            },null
        );
        return dfd.promise;
    };

    this.getSIPRateDetails = function (inputObj) {
        var dfd = $q.defer();

        var qryStr = "SELECT " +
            " (SELECT Rate FROM LP_SIS_RATES where Product_PGL_ID='{{pglId}}' AND Key='LPB' AND Option='{{option}}' AND (Lo_AnnualizedPremium <= '{{insuredAmt}}' AND Hi_AnnualizedPremium >= '{{insuredAmt}}')) LPBRATE, " +
            " (SELECT Rate FROM LP_SIS_RATES where Product_PGL_ID='{{pglId}}' AND Key='GP' AND Option='Option 1' AND PPT={{ppt}}) GPRATEOPT1, " +
            " (SELECT Rate FROM LP_SIS_RATES where Product_PGL_ID='{{pglId}}' AND Key='GP' AND Option='Option 2' AND PPT={{ppt}} AND (Lo_Age<={{age}} AND Hi_Age>={{age}})) GPRATEOPT2," +
            " (SELECT Rate FROM LP_SIS_RATES where Product_PGL_ID='{{pglId}}' AND Key='GMP' AND Gender='M' AND Option='{{option}}' AND PPT={{ppt}} AND (Lo_Age<={{age}} AND Hi_Age>={{age}})) GMPRATE " +
            " FROM LP_SIS_RATES LIMIT 1";

        qryStr = CommonService.replaceAll(qryStr, "{{option}}", inputObj.option || null);
        qryStr = CommonService.replaceAll(qryStr, "{{age}}", inputObj.age || null);
        qryStr = CommonService.replaceAll(qryStr, "{{ppt}}", inputObj.ppt || null);
        qryStr = CommonService.replaceAll(qryStr, "{{insuredAmt}}", inputObj.insuredAmt || null);
        qryStr = CommonService.replaceAll(qryStr, "{{pglId}}", inputObj.pglId || null);

        CommonService.transaction(sisDB,
            function (tx) {
                debug("Querry :  " + qryStr);
                CommonService.executeSql(tx, qryStr, [],
                    function (tx, res) {
                        /*                 minTerm=res.rows.item(0).LPBRATE;*/
                        dfd.resolve(res.rows.item(0));
                        debug("Data from LP_SIS_RATES  " + JSON.stringify(res.rows.item(0)));
                    },
                    function (tx, err) {
                        console.log("Error in LP_SIS_RATES ().transaction(): " + err.message);
                        dfd.resolve(null);
                    }
                );
            },
            function (err) {
                console.log("Error in LP_SIS_RATES(): " + err.message);
                dfd.resolve(null);
            }, null
        );
        return dfd.promise;
    };


}]);
