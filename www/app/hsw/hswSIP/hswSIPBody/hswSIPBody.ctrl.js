hswBodyModule.controller('smartIncomePlusPageCtrl',['$state','hswService','$scope','smartIncomePlusSevice',function($state,hswService,$scope,smartIncomePlusSevice){
	"use strict";
	//this.htmlPath1 = '../app/hsw/smartIncomePlus/smartIncomePlus.html'"';
	this.htmlPathSIP = '../app/hsw/hswSIP/hswSIPBody/hswSIPBody.html';
	var self=this;
	var rateDetails = {};
    self.gpRate=0;
    self.firstValue=0;
    self.secondValue=0;
    self.secondPercent=0;
    self.diffSecondVal=0;
    self.diffThirdVal=0;
    self.thridValue=0;
    self.thridPercent=0;
    self.diffForthVal=0;
    self.forthValue=0;
    self.forthPercent=0;
    self.ppt=0;
    self.insuredAmt = 0;
    self.displayYr="";
    self.display1Yr="";
    self.ordinalSuffixOf = function(i) {

            var j = i % 10;
            var k = i % 100;
            if (j == 1 && k != 11) {
                return i + "st";
            }
            if (j == 2 && k != 12) {
                return i + "nd";
            }
            if (j == 3 && k != 13) {
                return i + "rd";
            }
            return i + "th";
    };
    self.setValues=function(){
    	self.minTerm=hswService.getHSW_MIN_TERM();
	    self.maxTerm=hswService.getHSW_MAX_TERM();
	    self.percent=0;
    	if(self.maxTerm!=self.minTerm){
	    	self.diffrenceInMinMaxterm= parseInt(self.maxTerm) - parseInt(self.minTerm);
	    	debug("diffrenceInMinMaxterm: "+self.diffrenceInMinMaxterm);
	    	self.selectedTerm=hswService.getHSW_SELECTED_TERM()- parseInt(self.minTerm);
	    	debug("selectedTerm: "+self.selectedTerm);
	    	self.percent=(self.selectedTerm/self.diffrenceInMinMaxterm)*100;
	    	debug("Percent: "+self.percent);
	    }
	    else
	    {
	    	self.percent=100;
	    	debug("Percent: "+self.percent);
	    }
        var inputObj = {};
        inputObj.option = hswService.getHSW_OPTION();
        inputObj.age = hswService.getHSW_SELECTED_AGE();
        inputObj.ppt = hswService.getHSW_PPT();
        inputObj.insuredAmt = hswService.getHSW_SELECTED_PREMIUM();
        inputObj.pglId = 219;

        self.insuredAmt = inputObj.insuredAmt;
        self.option=inputObj.option;
        console.log("Option Value"+inputObj.option);
        console.log("Input obj is "+JSON.stringify(inputObj));
        self.firstValue=(parseFloat(inputObj.ppt)/parseFloat(self.maxTerm))*100;
        self.diffSecondVal=(inputObj.option =="Option 1")? 1 : parseFloat(self.maxTerm) - parseFloat(inputObj.ppt)-2;
        self.secondValue=(inputObj.option =="Option 1")? (1/parseFloat(self.maxTerm))*100:(parseFloat(self.maxTerm - inputObj.ppt - 2)/parseFloat(self.maxTerm))*100;
        self.secondPercent=100/(parseFloat(self.diffSecondVal));

//        if(self.option == "Option 2")
//            self.firstValue= parseFloat(self.firstValue) + parseFloat(self.secondValue);

        self.diffThirdVal=(inputObj.option =="Option 1")? self.maxTerm -(parseFloat(inputObj.ppt) + parseFloat(self.diffSecondVal)):(self.maxTerm -(parseFloat(inputObj.ppt) + parseFloat(self.diffSecondVal)))-1;
        self.thridValue=(inputObj.option =="Option 1")? 100-(self.secondValue + self.firstValue):1/parseFloat(self.maxTerm)*100;
        self.thridPercent=100/(parseFloat(self.diffThirdVal));
        self.diffForthVal=(inputObj.option =="Option 1")? 0:(self.maxTerm -(parseFloat(inputObj.ppt) + parseFloat(self.diffSecondVal)+ parseFloat(self.diffThirdVal)));
        self.forthValue=(inputObj.option =="Option 1")? 0:1/parseFloat(self.maxTerm)*100;
        self.forthPercent=(inputObj.option =="Option 1")? 0 : 100/(parseFloat(self.diffForthVal));
        console.log("firstValue"+self.firstValue);
        console.log("secondValue"+self.secondValue);
        console.log("thridValue"+self.thridValue);
        console.log("forthValue"+self.forthValue);
        console.log("secondPercent"+self.secondPercent);
        console.log("secondDiffValue"+self.diffSecondVal);
        console.log("thridDiffValue"+self.diffThirdVal);
        console.log("thridPercentValue"+self.thridPercent);
        console.log("forthDiffValue"+self.diffForthVal);
        console.log("forthPercentValue"+self.forthPercent);
        // self.secondValue=(parseFloat(inputObj.ppt)/parseFloat(self.maxTerm))*100;
        //self.thirdValue=(parseFloat(inputObj.ppt)/parseFloat(self.maxTerm))*100;
        self.displayYr=parseFloat(inputObj.ppt)+ parseFloat(self.diffSecondVal)+ parseFloat(self.diffThirdVal);
        self.displayYr=self.ordinalSuffixOf(self.displayYr);
        self.display1Yr=parseFloat(inputObj.ppt)+parseFloat(self.diffSecondVal)+1;
        self.display1Yr=self.ordinalSuffixOf(self.display1Yr);

        self.ppt=inputObj.ppt;
        var returnData = "";
        smartIncomePlusSevice.getSIPRateDetails(inputObj).then(
            function(res){
                debug("Returned Data is");
                debug(res.GPRATEOPT1);
                self.setRateDetails(res);

                self.gp=parseFloat(self.calculateGP(res,inputObj)).toFixed(0);
                debug("GP===>"+self.gp);
                self.gmp=parseFloat(self.calculateGMP(res,inputObj,self.gp)).toFixed(0);
                debug("GMP===>"+self.gmp);
                self.tmp=parseFloat(self.calculateTMP(inputObj,self.gp,self.gmp)).toFixed(0);
                debug("TMP===>"+self.tmp);
                self.ap=parseFloat(self.calculateAP(inputObj)).toFixed(0);
                $scope.$emit('passSumAssured',{message:self.ap});
                debug("AP===>"+self.ap);
            }
        );
    };
    self.setValues();

    $scope.$on('onDropDownChange',function(event,args){
    	self.setValues();
    });

    self.processToSiS = function(){
	    var productDetails = hswService.getProductDetails();
        debug("Retrieving product details"+JSON.stringify(productDetails));
        $state.go("dashboard.productLandingPage",{"PGL_ID": productDetails.PGL_ID, "SOL_TYPE": productDetails.SOL_TYPE, "PROD_NAME": productDetails.PROD_NAME, "FHR_ID": productDetails.FHR_ID, "LEAD_ID": productDetails.LEAD_ID, "OPP_ID": productDetails.OPP_ID});
    };

    $scope.$on('onClickOnGoButton',function(event){
//        alert("Click"+hswService.getHSW_SELECTED_AGE());
//        smartIncomePlusSevice.getGP_FOR_SIP(hswService.getHSW_OPTION(),hswService.getHSW_SELECTED_AGE(),hswService.getHSW_PPT());

        var inputObj = {};
        inputObj.option = hswService.getHSW_OPTION();
        inputObj.age = hswService.getHSW_SELECTED_AGE();
        inputObj.ppt = hswService.getHSW_PPT();
        inputObj.insuredAmt = hswService.getHSW_SELECTED_PREMIUM();
        inputObj.pglId = 219;

        self.insuredAmt = inputObj.insuredAmt;
        var returnData = "";
        smartIncomePlusSevice.getSIPRateDetails(inputObj).then(
            function(res){
                debug("Returned Data is" +JSON.stringify(res));
                debug(res.GPRATEOPT1);
                self.setRateDetails(res);

                self.gp=parseFloat(self.calculateGP(res,inputObj)).toFixed(0);
                debug("GP===>"+self.gp);
                self.gmp=parseFloat(self.calculateGMP(res,inputObj,self.gp)).toFixed(0);
                debug("GMP===>"+self.gmp);
                self.tmp=parseFloat(self.calculateTMP(inputObj,self.gp,self.gmp)).toFixed(0);
                debug("TMP===>"+self.tmp);
                self.ap=parseFloat(self.calculateAP(inputObj)).toFixed(0);
                $scope.$emit('passSumAssured',{message:self.ap});
                debug("AP===>"+self.ap);
            }
        );
    });

    var stateParameters = null;
    $scope.$on('StateParameters',function(event,stateParameters){
        stateParameters = stateParameters;
    });


	$scope.getNumber = function(num) {
		var data = [];
	    for(var i = 1; i <= num; i++) {
	      data.push(i);
	  	}
	    return data;
    };

    self.calculateGP=function(res,inputObj){

        debug("Res is ");
        debug(res);
        debug(inputObj);

        if(inputObj.option=="Option 1"){
            self.gpRate=(parseFloat(res.GPRATEOPT1)*100).toFixed(0);
            return (parseFloat(inputObj.insuredAmt)*parseFloat(res.GPRATEOPT1));
        }else{
             self.gpRate=(parseFloat(res.GPRATEOPT2)*100).toFixed(0);
            return (parseFloat(inputObj.insuredAmt)*parseFloat(res.GPRATEOPT2))*(parseFloat(res.LPBRATE));
        }
    };
    self.calculateGMP=function(res,inputObj,gp){
        debug("insuredAmt : "+parseFloat(inputObj.insuredAmt)+" GMRate : "+parseFloat(res.GMPRATE)+" LB Rate : "+parseFloat(res.LPBRATE));
        var gmprate=parseFloat(res.GMPRATE) || 0;
        var lprate=parseFloat(res.LPBRATE) || 0;
        var gprate2=parseFloat(res.GPRATEOPT2);
        debug("gmprate"+gmprate);
        debug("lprate"+lprate);
        debug("gprate2"+gprate2);
        if(inputObj.option=="Option 1"){
           //    debug("insuredAmt : "+parseInt(inputObj.insuredAmt)+" GMRate : "+parseInt(res.GMPRATE)+" LB Rate : "+parseInt(res.LPBRATE));
            return (parseFloat(inputObj.insuredAmt)*(gmprate)*(lprate));
        }else{
            return (parseFloat(inputObj.insuredAmt)*(gprate2)*(lprate));
        }
    };
    self.calculateTMP=function(inputObj,gp,gmp){
        debug("calculate TMP"+JSON.stringify(inputObj));
        if(inputObj.option=="Option 1"){
            return parseFloat(gp)+parseFloat(gmp);
        }else{
            return gmp;
        }
    };
    self.calculateAP=function(inputObj){
       return parseFloat(inputObj.insuredAmt) *11;
    };


    self.setRateDetails = function (resp) {
        rateDetails = resp;
        console.log(rateDetails);
    };

    self.getRateDetails = function () {
        return rateDetails;
    };


    debug("Rate details are ");
	debug(self.getRateDetails());

}]);
