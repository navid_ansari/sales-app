fhrCompletedModule.controller('FHRCompletedCtrl', ['$state', 'FHRCompletedService', 'CommonService', 'FHRCompletedArr', function($state, FHRCompletedService, CommonService, FHRCompletedArr){
	"use strict";
	debug("FHRCompletedArr : " + JSON.stringify(FHRCompletedArr));
	var fc = this;
	this.fhrCompletedArr = [];
	this.carouselIndex = 0;
	this.fhrCompletedArr = CommonService.rebuildSlide(6, FHRCompletedArr);

	this.goToLeadDashboard = function(fhrCompletedObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : fhrCompletedObj : " + JSON.stringify(fhrCompletedObj));
		$state.go('leadDashboard', {"LEAD_ID": fhrCompletedObj.LEAD_ID, "FHR_ID": fhrCompletedObj.FHR_ID, "SIS_ID": null, "OPP_ID":fhrCompletedObj.OPP_ID, "APP_ID": null, "PGL_ID": fhrCompletedObj.PGL_ID});
	};
	CommonService.hideLoading();
}]);

fhrCompletedModule.service('FHRCompletedService', ['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var fs = this;
	this.getFHRCompletedData = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT MLD.LEAD_SOURCE1_CD AS LDSRC1, MYOPP.SIS_ID AS SIS_ID, MYOPP.APPLICATION_ID AS APPLICATION_ID, MYOPP.LEAD_ID AS LEAD_ID, MYOPP.FHR_ID AS FHR_ID, MYOPP.LAST_UPDATED_DATE  AS FHR_TMSTMP, MYOPP.OPPORTUNITY_ID AS OPP_ID, MLD.LEAD_NAME AS LNAME, MLD.MOBILE_NO AS MOBNO, FR.PRODUCT_PGL_ID AS PGL_ID FROM LP_MYOPPORTUNITY MYOPP, LP_MYLEAD MLD, LP_FHR_PRODUCT_RCMND FR WHERE MYOPP.AGENT_CD=? AND (MYOPP.FHR_SIGNED_TIMESTAMP IS NOT NULL AND MYOPP.FHR_SIGNED_TIMESTAMP!=?) AND (MYOPP.SIS_ID IS NULL) AND (MYOPP.FHR_ID IS NOT NULL AND MYOPP.FHR_ID!=?) AND MLD.IPAD_LEAD_ID=MYOPP.LEAD_ID AND (MYOPP.OPPORTUNITY_STATUS IS NULL OR MYOPP.OPPORTUNITY_STATUS!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG !='T') AND FR.FHR_ID = MYOPP.FHR_ID ORDER BY FHR_TMSTMP DESC";
				var parameterList = [LoginService.lgnSrvObj.userinfo.AGENT_CD, '', '', 'D', 'S'];
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("res.rows.length: " + res.rows.length);
						var fhrCompletedArr = CommonService.resultSetToObject(res);
						if(!!fhrCompletedArr){
							if(!(fhrCompletedArr instanceof Array))
								fhrCompletedArr = [fhrCompletedArr];
							for(var i=0;i<fhrCompletedArr.length;i++){
								fhrCompletedArr[i].LDSRC1 = fhrCompletedArr[i].LDSRC1 == '14' ? 'Self' : '';
								fhrCompletedArr[i].FHR_TMSTMP = ((!!fhrCompletedArr[i].FHR_TMSTMP) ? fhrCompletedArr[i].FHR_TMSTMP.substring(0,10) : "");
							}
							dfd.resolve(fhrCompletedArr);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
