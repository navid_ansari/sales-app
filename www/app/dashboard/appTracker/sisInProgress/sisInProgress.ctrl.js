sisInProgressModule.controller('SisInProgressCtrl', ['$state', 'SisInProgressService', 'CommonService', 'SisInProgressArr', function($state, SisInProgressService, CommonService, SisInProgressArr){
	"use strict";
	debug("SisInProgressArr : " + JSON.stringify(SisInProgressArr));
	var sc = this;
	this.sisInProgressArr = [];
	this.carouselIndex = 0;
	this.sisInProgressArr = CommonService.rebuildSlide(6, SisInProgressArr);

	this.goToLeadDashboard = function(sisInProgressObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : sisInProgressObj : " + JSON.stringify(sisInProgressObj));
		$state.go('leadDashboard', {"LEAD_ID": sisInProgressObj.LEAD_ID, "FHR_ID": sisInProgressObj.FHR_ID, "SIS_ID": sisInProgressObj.SIS_ID, "OPP_ID":sisInProgressObj.OPP_ID, "APP_ID": null, "PGL_ID": sisInProgressObj.PGL_ID});
	};
	CommonService.hideLoading();
}]);
sisInProgressModule.service('SisInProgressService', ['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){
	var ss = this;

	this.getSisInProgeressData = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT MLD.LEAD_SOURCE1_CD AS LDSRC1, MYOPP.APPLICATION_ID AS APPLICATION_ID, MYOPP.SIS_ID AS SIS_ID, MYOPP.LEAD_ID AS LEAD_ID, MYOPP.FHR_ID AS FHR_ID, MYOPP.OPPORTUNITY_ID AS OPP_ID, MYOPP.LAST_UPDATED_DATE AS LSTUPDT_TMSTMP, MLD.LEAD_NAME AS LNAME, MLD.MOBILE_NO AS MOBNO, SISMAIN.PGL_ID AS PGL_ID FROM LP_MYOPPORTUNITY MYOPP, LP_MYLEAD MLD, LP_SIS_MAIN AS SISMAIN WHERE MYOPP.AGENT_CD=? AND (MYOPP.SIS_ID IS NOT NULL OR MYOPP.SIS_ID!=?) AND (MYOPP.SIS_ID = SISMAIN.SIS_ID) AND (MYOPP.FHR_ID IS NOT NULL AND MYOPP.FHR_ID!=?) AND MLD.IPAD_LEAD_ID=MYOPP.LEAD_ID AND (MYOPP.OPPORTUNITY_STATUS IS NULL OR MYOPP.OPPORTUNITY_STATUS!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG !='T') AND (SISMAIN.SIGNED_TIMESTAMP IS NULL OR SISMAIN.SIGNED_TIMESTAMP=?) ORDER BY LSTUPDT_TMSTMP DESC";
				var parameterList = [LoginService.lgnSrvObj.userinfo.AGENT_CD, '', '', 'D', ''];
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("res.rows.length: " + res.rows.length);
						var sisInProgressArr = CommonService.resultSetToObject(res);
						if(!!sisInProgressArr){
							if(!(sisInProgressArr instanceof Array))
								sisInProgressArr = [sisInProgressArr];
							for(var i=0;i<sisInProgressArr.length;i++){
								sisInProgressArr[i].LDSRC1 = (sisInProgressArr[i].LDSRC1 == '14' ? 'Self' : '');
								sisInProgressArr[i].LSTUPDT_TMSTMP = ((!!sisInProgressArr[i].LSTUPDT_TMSTMP) ? sisInProgressArr[i].LSTUPDT_TMSTMP.substring(0,10) : "");
							}
							dfd.resolve(sisInProgressArr);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
