fhrInProgressModule.controller('FHRInProgressCtrl', ['$state', 'FHRInProgressService', 'CommonService', 'FHRInProgressArr', function($state, FHRInProgressService, CommonService, FHRInProgressArr){
	"use strict";
	debug("FHRInProgressArr : " + JSON.stringify(FHRInProgressArr));
	var fpc = this;
	this.fhrInProgressArr = [];
	this.carouselIndex = 0;

	this.fhrInProgressArr = CommonService.rebuildSlide(6, FHRInProgressArr);

	this.goToLeadDashboard = function(fhrInProgressObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : fhrInProgressArr : " + JSON.stringify(fhrInProgressObj));
		$state.go('leadDashboard', {"LEAD_ID": fhrInProgressObj.LEAD_ID, "FHR_ID": fhrInProgressObj.FHR_ID, "OPP_ID": fhrInProgressObj.OPP_ID});
	};
	CommonService.hideLoading();
}]);

fhrInProgressModule.service('FHRInProgressService', ['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var fps = this;

	this.getFHRInProgressData = function(){
		var dfd = $q.defer();
		var fhrInProgressArr = [];
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT MLD.LEAD_SOURCE1_CD, MYOPP.SIS_ID AS SIS_ID, MYOPP.APPLICATION_ID AS APPLICATION_ID, MYOPP.LEAD_ID AS LEAD_ID, MYOPP.SIS_ID AS SIS_ID, MYOPP.FHR_ID AS FHR_ID, MLD.LEAD_NAME AS LNAME, MLD.MOBILE_NO AS MOBNO, MYOPP.OPPORTUNITY_ID AS OPP_ID, MYOPP.LAST_UPDATED_DATE AS FMMD FROM LP_MYOPPORTUNITY MYOPP, LP_MYLEAD MLD,LP_FHR_MAIN FM WHERE MYOPP.AGENT_CD=? AND (MYOPP.FHR_SIGNED_TIMESTAMP IS NULL OR MYOPP.FHR_SIGNED_TIMESTAMP=?) AND (MYOPP.FHR_ID IS NOT NULL AND MYOPP.FHR_ID!=?) AND MLD.IPAD_LEAD_ID=MYOPP.LEAD_ID AND FM.FHR_ID=MYOPP.FHR_ID AND (MYOPP.OPPORTUNITY_STATUS IS NULL OR MYOPP.OPPORTUNITY_STATUS!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG !='T') ORDER BY FMMD DESC";
				var parameterList = [LoginService.lgnSrvObj.userinfo.AGENT_CD, '', '', 'D', 'S'];
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("res.rows.length: " + res.rows.length);
						var fhrInProgressArr = CommonService.resultSetToObject(res);
						if(!!fhrInProgressArr){
							if(!(fhrInProgressArr instanceof Array))
								fhrInProgressArr = [fhrInProgressArr];
							for(var i=0;i<fhrInProgressArr.length;i++){
								fhrInProgressArr[i].LDSRC1 = ((fhrInProgressArr[i].LDSRC1 == '14') ? 'Self' : '');
								fhrInProgressArr[i].FMMD = ((!!fhrInProgressArr[i].FMMD) ? fhrInProgressArr[i].FMMD.substring(0,10) : "");
							}
							dfd.resolve(fhrInProgressArr);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

}]);
