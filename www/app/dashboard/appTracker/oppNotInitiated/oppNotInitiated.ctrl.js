oppNotInitiatedModule.controller('OppNotInitiatedCtrl', ['$state', 'OppNotInitiatedService', 'CommonService', 'OppNotInitiatedArr', function($state, OppNotInitiatedService, CommonService, OppNotInitiatedArr){
	"use strict";
	debug("OppNotInitiatedArr : ");
	var oic = this;
	this.oppNotInitiatedArr = [];
	this.carouselIndex = 0;
	this.oppNotInitiatedArr = CommonService.rebuildSlide(6, OppNotInitiatedArr);

	this.goToLeadDashboard = function(oppNotInitiatedObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : oppNotInitiatedObj : " + JSON.stringify(oppNotInitiatedObj));
		$state.go('leadDashboard', {"LEAD_ID": oppNotInitiatedObj.LEAD_ID});
	};
	CommonService.hideLoading();
}]);

oppNotInitiatedModule.service('OppNotInitiatedService', ['$q','$state', 'CommonService','LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var ois = this;
	this.getOppNotInitiatedData = function(){
		debug("getOppNotInitiatedData");
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT MLD.LEAD_SOURCE1_CD AS LDSRC1,MLD.IPAD_LEAD_ID AS LEAD_ID,MLD.LEAD_NAME AS LNAME,MLD.MOBILE_NO AS MOBNO,MLD.LEAD_CREATED_DTTM AS LAST_UPDT_DT FROM LP_MYLEAD MLD WHERE MLD.IPAD_LEAD_ID NOT IN (SELECT DISTINCT LEAD_ID FROM LP_MYOPPORTUNITY WHERE AGENT_CD=? AND FHR_ID IS NOT NULL AND FHR_ID!='') AND MLD.BIRTH_DATE IS NOT NULL AND MLD.BIRTH_DATE!='' AND MLD.EMAIL_ID IS NOT NULL AND MLD.EMAIL_ID!='' AND MLD.MOBILE_NO IS NOT NULL AND MLD.MOBILE_NO!='' AND MLD.PRIMARY_CONTACT_FLAG IS NOT NULL AND MLD.PRIMARY_CONTACT_FLAG!='' AND MLD.GENDER_CD IS NOT NULL AND MLD.GENDER_CD!=''  GROUP BY LEAD_ID ORDER BY LAST_UPDT_DT DESC";
				var parameterList = [LoginService.lgnSrvObj.userinfo.AGENT_CD];
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug(LoginService.lgnSrvObj.userinfo.AGENT_CD+"ois res.rows.length: " + res.rows.length);
						var oppNotInitiatedArr = CommonService.resultSetToObject(res);
						if(!!oppNotInitiatedArr){
							debug("oppNotInitiatedArr 1");
							if(!(oppNotInitiatedArr instanceof Array))
								oppNotInitiatedArr = [oppNotInitiatedArr];
							for(var i=0;i<oppNotInitiatedArr.length;i++) {
								debug("oppNotInitiatedArr 2");
								oppNotInitiatedArr[i].LDSRC1 = (oppNotInitiatedArr[i].LDSRC1 == '14' ? 'Self' : '');
								oppNotInitiatedArr[i].LAST_UPDT_DT = ((!!oppNotInitiatedArr[i].LAST_UPDT_DT) ? oppNotInitiatedArr[i].LAST_UPDT_DT.substring(0,10) : "");
							}
							dfd.resolve(oppNotInitiatedArr);
						}
						else{
							debug("oppNotInitiatedArr 3");
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
