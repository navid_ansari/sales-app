appTrackerModule.controller('AppTrackerCtrl',['$state','AppTrackerService','CommonService', 'AppTrackData',function($state, AppTrackerService, CommonService, AppTrackData){
	var atc = this;
	this.activeTab = 'oppNotInitiated';
	debug("AppTrackData : " + JSON.stringify(AppTrackData));
	if(BUSINESS_TYPE!="IndusSolution"){
		this.oppStarterModuleName = "FHR";
		this.hideFF = false;
	}
	else{
		this.oppStarterModuleName = "Fact Finder";
		this.hideFF = false;
	}

	if(!!AppTrackData) {
		this.oppNotInitiatedCnt = ((!!AppTrackData.oppNotInitiatedArr) ? (AppTrackData.oppNotInitiatedArr.length) : 0);
		this.fhrInProgressCnt = ((!!AppTrackData.fhrInProgressArr) ? (AppTrackData.fhrInProgressArr.length) : 0);
		this.fhrCompletedCnt = ((!!AppTrackData.fhrCompletedArr) ? (AppTrackData.fhrCompletedArr.length) : 0);
		this.sisInProgressCnt = ((!!AppTrackData.sisInProgressArr) ? (AppTrackData.sisInProgressArr.length) : 0);
		this.sisCompletedCnt = ((!!AppTrackData.sisCompletedArr) ? (AppTrackData.sisCompletedArr.length) : 0);
		this.appInProgressCnt = ((!!AppTrackData.appInProgressArr) ? (AppTrackData.appInProgressArr.length) : 0);
	}
	else{
		this.oppNotInitiatedCnt = 0;
		this.fhrInProgressCnt = 0;
		this.fhrCompletedCnt = 0;
		this.sisInProgressCnt = 0;
		this.sisCompletedCnt = 0;
		this.appInProgressCnt = 0;
	}

	this.gotoPage = function(pageName) {
		debug("gotoPage :pageName : " + pageName);
		debug("atc.activeTab : " + atc.activeTab);
		switch(pageName){
			case "oppNotInitiated":
									if(atc.activeTab != pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "oppNotInitiated";
									$state.go('dashboard.appTracker.oppNotInitiated');
									break;
			case "fhrInProgress":
									if(atc.activeTab != pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "fhrInProgress";
									$state.go('dashboard.appTracker.fhrInProgress');
									break;
			case "fhrCompleted":
									if(atc.activeTab !== pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "fhrCompleted";
									$state.go('dashboard.appTracker.fhrCompleted');
									break;
			case "sisInProgress":
									if(atc.activeTab !== pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "sisInProgress";
									$state.go('dashboard.appTracker.sisInProgress');
									break;
			case "sisCompleted":
									if(atc.activeTab !== pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "sisCompleted";
									$state.go('dashboard.appTracker.sisCompleted');
									break;
			case "appInProgress":
									if(atc.activeTab !== pageName)
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "appInProgress";
									$state.go('dashboard.appTracker.appInProgress');
									break;
			default:
									if(atc.activeTab !== pageName);
										CommonService.showLoading("Please Wait...");
									atc.activeTab = "oppNotInitiated";
									$state.go('dashboard.appTracker.oppNotInitiated');
									break;
		}
	};
}]);


appTrackerModule.service('AppTrackerService', ['$q', '$state', 'CommonService', 'OppNotInitiatedService', 'FHRInProgressService', 'FHRCompletedService', 'SisInProgressService', 'SisCompletedService', 'AppInProgressService', function($q, $state, CommonService, OppNotInitiatedService, FHRInProgressService, FHRCompletedService, SisInProgressService, SisCompletedService, AppInProgressService) {
	var ats = this;
	this.appTrackData = {};

	this.getAppTrackData = function(){
		debug("getAppTrackData");
		var dfd = $q.defer();
		OppNotInitiatedService.getOppNotInitiatedData().then(
			function(oppNotInitiatedArr){
				ats.appTrackData.oppNotInitiatedArr = oppNotInitiatedArr;
				return oppNotInitiatedArr;
			}
		).then(
			function(oppNotInitiatedArr){
				return FHRInProgressService.getFHRInProgressData().then(
					function(fhrInProgressArr){
						return ats.appTrackData.fhrInProgressArr = fhrInProgressArr;
					}
				);
			}
		).then(
			function(fhrInProgressArr){
				return FHRCompletedService.getFHRCompletedData().then(
					function(fhrCompletedArr){
						return ats.appTrackData.fhrCompletedArr = fhrCompletedArr;
					}
				);
			}
		).then(
			function(fhrCompletedArr){
				return SisInProgressService.getSisInProgeressData().then(
					function(sisInProgressArr){
						return ats.appTrackData.sisInProgressArr = sisInProgressArr;
					}
				);
			}
		).then(
			function(sisInProgressArr){
				return SisCompletedService.getSisCompletedData().then(
					function(sisCompletedArr){
						return ats.appTrackData.sisCompletedArr = sisCompletedArr;
					}
				);
			}
		).then(
			function(sisCompletedArr){
				return AppInProgressService.getAppInProgressData().then(
					function(appInProgressArr){
						return ats.appTrackData.appInProgressArr = appInProgressArr;
					}
				);
			}
		).then(
			function(appInProgressArr){
				dfd.resolve(ats.appTrackData);
			}
		);
		return dfd.promise;
	};
}]);
