ffNotInitiatedModule.controller('FFNotInitiatedCtrl', ['$state', 'FFNotInitiatedService', 'CommonService', 'FFNotInitiatedArr', function($state, FFNotInitiatedService, CommonService, FFNotInitiatedArr){
	"use strict";
	debug("FFNotInitiatedArr : " + JSON.stringify(FFNotInitiatedArr));
	var fic = this;
	this.ffNotInitiatedArr = [];
	this.carouselIndex = 0;
	this.ffNotInitiatedArr = CommonService.rebuildSlide(6, FFNotInitiatedArr);

	this.goToLeadDashboard = function(ffNotInitiatedObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : ffNotInitiatedArr : " + JSON.stringify(ffNotInitiatedObj));
		$state.go('leadDashboard', {"LEAD_ID": ffNotInitiatedObj.LEAD_ID});
	};
	CommonService.hideLoading();
}]);

ffNotInitiatedModule.service('FFNotInitiatedService', ['$q','$state', 'CommonService','LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var fis = this;
	this.getFfNotInitiatedData = function(){
		debug("getFfNotInitiatedData");
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"SELECT MLD.LEAD_SOURCE1_CD AS LDSRC1,MLD.IPAD_LEAD_ID AS LEAD_ID,MLD.LEAD_NAME AS LNAME,MLD.MOBILE_NO AS MOBNO,MLD.LEAD_CREATED_DTTM AS LAST_UPDT_DT FROM LP_MYLEAD MLD WHERE MLD.IPAD_LEAD_ID NOT IN (SELECT DISTINCT LEAD_ID FROM LP_MYOPPORTUNITY WHERE AGENT_CD=? AND FHR_ID IS NOT NULL AND FHR_ID!='') GROUP BY LEAD_ID ORDER BY LAST_UPDT_DT DESC",[LoginService.lgnSrvObj.userinfo.AGENT_CD],
					function(tx,res){
						debug("ffni res.rows.length: " + res.rows.length);
						var ffNotInitiatedArr = CommonService.resultSetToObject(res);
						if(!!ffNotInitiatedArr){
							if(!(ffNotInitiatedArr instanceof Array))
								ffNotInitiatedArr = [ffNotInitiatedArr];
							for(var i=0;i<ffNotInitiatedArr.length;i++) {
								ffNotInitiatedArr[i].LDSRC1 = (ffNotInitiatedArr[i].LDSRC1 == '14' ? 'Self' : '');
								ffNotInitiatedArr[i].LAST_UPDT_DT = ((!!ffNotInitiatedArr[i].LAST_UPDT_DT) ? ffNotInitiatedArr[i].LAST_UPDT_DT.substring(0,10) : "");
							}
							dfd.resolve(ffNotInitiatedArr);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
