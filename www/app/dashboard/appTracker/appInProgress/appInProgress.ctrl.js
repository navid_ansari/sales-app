appInProgressModule.controller('AppInProgressCtrl', ['$state', 'AppInProgressService', 'CommonService', 'AppInProgressArr', function($state, AppInProgressService, CommonService, AppInProgressArr){
	"use strict";
	debug("AppInProgressArr : " + JSON.stringify(AppInProgressArr));
	var apc = this;
	this.appInProgressArr = [];
	this.carouselIndex = 0;

	this.appInProgressArr = CommonService.rebuildSlide(6, AppInProgressArr);

	this.goToLeadDashboard = function(appInProgressArr){
		CommonService.showLoading();
		debug("gotoLeadDashboard : appInProgressArr : " + JSON.stringify(appInProgressArr));
		$state.go('leadDashboard', {"LEAD_ID": appInProgressArr.LEAD_ID, "FHR_ID": appInProgressArr.FHR_ID, "SIS_ID": appInProgressArr.SIS_ID, "APP_ID": appInProgressArr.APPLICATION_ID, "OPP_ID":appInProgressArr.OPP_ID, "PGL_ID": appInProgressArr.PGL_ID, "COMBO_ID": appInProgressArr.COMBO_ID});
	};
	CommonService.hideLoading();
}]);

appInProgressModule.service('AppInProgressService', ['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var aps = this;

	this.getAppInProgressData = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
					CommonService.executeSql(tx, "SELECT MYOPP.POLICY_NO AS POLICY_NO,MYOPP.OPPORTUNITY_ID AS OPP_ID, MYOPP.LAST_UPDATED_DATE AS LSTUPDT_TMSTMP, LD.LEAD_SOURCE1_CD AS LDSRC1, LD.LEAD_NAME AS LNAME, LD.MOBILE_NO AS MOBNO, LD.IPAD_LEAD_ID AS LEAD_ID, MYOPP.APPLICATION_ID, APPM.IS_OUTPUT_GENERATED AS OPGEN, MYOPP.SIS_ID, MYOPP.FHR_ID, MYOPP.POLICY_NO, MYOPP.OPPORTUNITY_STATUS, APPM.PLAN_NAME, APPM.ISAGENT_SIGNED, APPM.ISINSURED_SIGNED, APPM.ISPROPOSER_SIGNED, SISM.PGL_ID AS PGL_ID FROM LP_SIS_MAIN SISM, LP_MYOPPORTUNITY MYOPP, LP_MYLEAD LD, LP_APPLICATION_MAIN APPM WHERE (SISM.SIS_ID=APPM.SIS_ID) AND MYOPP.APPLICATION_ID IN (SELECT APPLICATION_ID FROM LP_APPLICATION_MAIN WHERE (APP_SUBMITTED_FLAG IS NULL OR APP_SUBMITTED_FLAG=?) AND (APP_STATUS IS NULL OR APP_STATUS!=?)) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG !='T') AND APPM.APPLICATION_ID = MYOPP.APPLICATION_ID AND LD.IPAD_LEAD_ID = MYOPP.LEAD_ID AND MYOPP.AGENT_CD=? ORDER BY LSTUPDT_TMSTMP DESC", ['', 'D', LoginService.lgnSrvObj.userinfo.AGENT_CD],
                //SELECT MYOPP.POLICY_NO AS POLICY_NO,MYOPP.OPPORTUNITY_ID AS OPP_ID, MYOPP.LAST_UPDATED_DATE AS LSTUPDT_TMSTMP,LD.LEAD_SOURCE1_CD AS LDSRC1,LD.LEAD_NAME AS LNAME,LD.MOBILE_NO AS MOBNO,LD.IPAD_LEAD_ID AS LEAD_ID, MYOPP.APPLICATION_ID,APPM.IS_OUTPUT_GENERATED AS OPGEN, MYOPP.SIS_ID, MYOPP.FHR_ID, MYOPP.POLICY_NO, MYOPP.OPPORTUNITY_STATUS, APPM.PLAN_NAME, APPM.ISAGENT_SIGNED, APPM.ISINSURED_SIGNED, APPM.ISPROPOSER_SIGNED, SISM.PGL_ID FROM LP_SIS_MAIN SISM, LP_MYOPPORTUNITY MYOPP, LP_MYLEAD LD,LP_APPLICATION_MAIN APPM WHERE (SISM.SIS_ID=APPM.SIS_ID) AND MYOPP.APPLICATION_ID IN (SELECT APPLICATION_ID FROM LP_APPLICATION_MAIN WHERE (APP_SUBMITTED_FLAG IS NULL OR APP_SUBMITTED_FLAG ='') AND (APP_STATUS IS NULL OR APP_STATUS !='D')) AND APPM.APPLICATION_ID = MYOPP.APPLICATION_ID AND LD.IPAD_LEAD_ID = MYOPP.LEAD_ID AND MYOPP.AGENT_CD=? ORDER BY LSTUPDT_TMSTMP DESC
					function(tx,res){
						debug("res.rows.length: " + res.rows.length);
						var appInProgressArr = CommonService.resultSetToObject(res);
						if(!!appInProgressArr){
							if(!(appInProgressArr instanceof Array))
								appInProgressArr = [appInProgressArr];
							for(var i=0;i<res.rows.length;i++){
								appInProgressArr[i].LSTUPDT_TMSTMP = (!!appInProgressArr[i].LSTUPDT_TMSTMP) ? appInProgressArr[i].LSTUPDT_TMSTMP.substring(0,10) : "";
								appInProgressArr[i].LDSRC1 = ((appInProgressArr[i] == '14') ? 'Self' : '');
								appInProgressArr[i].POLICY_NO = (!!appInProgressArr[i].POLICY_NO) ? appInProgressArr[i].POLICY_NO : null;
								appInProgressArr[i].APP_PROP_SIGNED = (((!!appInProgressArr[i].ISPROPOSER_SIGNED) && appInProgressArr[i].ISPROPOSER_SIGNED.toLowerCase()==='Y'.toLowerCase()) ? 'Y' : 'N');
								appInProgressArr[i].APP_AGENT_SIGNED = (((!!appInProgressArr[i].ISAGENT_SIGNED) && appInProgressArr[i].ISAGENT_SIGNED.toLowerCase()==='Y'.toLowerCase()) ? 'Y' : 'N');
                                appInProgressArr[i].COMBO_ID = res.rows.item(i).COMBO_ID;
							}
						}
						else{
							dfd.resolve(null);
						}
						dfd.resolve(appInProgressArr);
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
