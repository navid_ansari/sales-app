sisCompletedModule.controller('SisCompletedCtrl', ['$state', 'SisCompletedService', 'CommonService', 'SisCompletedArr', function($state, SisCompletedService, CommonService, SisCompletedArr){
	"use strict";
	debug("SisCompletedArr : " + JSON.stringify(SisCompletedArr));
	var sc = this;
	this.sisCompletedArr = [];
	this.carouselIndex = 0;
	this.sisCompletedArr = CommonService.rebuildSlide(6, SisCompletedArr);

	this.goToLeadDashboard = function(sisCompletedObj){
		CommonService.showLoading();
		debug("gotoLeadDashboard : sisCompletedArr : " + JSON.stringify(sisCompletedObj));
		$state.go('leadDashboard', {"LEAD_ID": sisCompletedObj.LEAD_ID, "FHR_ID": sisCompletedObj.FHR_ID, "SIS_ID": sisCompletedObj.SIS_ID, "OPP_ID":sisCompletedObj.OPP_ID, "APP_ID": sisCompletedObj.APPLICATION_ID, "POLICY_NO":sisCompletedObj.POLICY_NO});
	};
	CommonService.hideLoading();
}]);
sisCompletedModule.service('SisCompletedService', ['$q', '$state', 'CommonService', 'LoginService', function($q, $state, CommonService, LoginService){
	"use strict";
	var ss = this;

	this.getSisCompletedData = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				var query = "SELECT LD.LEAD_SOURCE1_CD AS LDSRC1, MYOPP.APPLICATION_ID AS APPLICATION_ID, MYOPP.POLICY_NO AS POLICY_NO, LD.LEAD_NAME AS LNAME, LD.MOBILE_NO AS MOBNO, LD.IPAD_LEAD_ID AS LEAD_ID, MYOPP.FHR_ID, MYOPP.LEAD_ID, MYOPP.SIS_ID, MYOPP.FHR_ID, MYOPP.OPPORTUNITY_ID AS OPP_ID, SISB.PLAN_NAME, MYOPP.LAST_UPDATED_DATE AS LSTUPDT_TMSTMP FROM LP_MYLEAD LD, LP_MYOPPORTUNITY MYOPP, LP_SIS_SCREEN_B SISB, LP_SIS_MAIN SISMAIN WHERE (SISMAIN.SIS_ID=SISB.SIS_ID) AND (SISMAIN.SIS_STATUS IS NULL OR SISMAIN.SIS_STATUS!=?) AND LD.IPAD_LEAD_ID=MYOPP.LEAD_ID AND (MYOPP.SIS_SIGNED_TIMESTAMP IS NOT NULL AND MYOPP.SIS_SIGNED_TIMESTAMP!=?) AND (MYOPP.RECO_PRODUCT_DEVIATION_FLAG IS NULL OR MYOPP.RECO_PRODUCT_DEVIATION_FLAG !='T') AND (MYOPP.APPLICATION_ID IS NULL OR MYOPP.APPLICATION_ID!=?) AND SISB.SIS_ID = MYOPP.SIS_ID AND MYOPP.AGENT_CD=? AND (MYOPP.APPLICATION_ID IS NULL OR MYOPP.APPLICATION_ID NOT IN(SELECT APPLICATION_ID FROM LP_APPLICATION_MAIN)) ORDER BY LSTUPDT_TMSTMP DESC";
				var parameterList = ['D', '', '', LoginService.lgnSrvObj.userinfo.AGENT_CD];
				CommonService.executeSql(tx, query, parameterList,
					function(tx,res){
						debug("res.rows.length: " + res.rows.length);
						var sisCompletedArr = CommonService.resultSetToObject(res);
						if(!!sisCompletedArr){
							if(!(sisCompletedArr instanceof Array))
								sisCompletedArr = [sisCompletedArr];
							for(var i=0;i<sisCompletedArr.length;i++){
								sisCompletedArr[i].LSTUPDT_TMSTMP = ((!!sisCompletedArr[i].LSTUPDT_TMSTMP) ? sisCompletedArr[i].LSTUPDT_TMSTMP.substring(0,10) : "");
								sisCompletedArr[i].LDSRC1 = (sisCompletedArr[i].LDSRC1 == '14' ? 'Self' : '');
								sisCompletedArr[i].APPLICATION_ID = (!!sisCompletedArr[i].APPLICATION_ID) ? sisCompletedArr[i].APPLICATION_ID : null;
								sisCompletedArr[i].POLICY_NO = (!!sisCompletedArr[i].POLICY_NO) ? sisCompletedArr[i].POLICY_NO : null;
							}
							dfd.resolve(sisCompletedArr);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};
}]);
