closedPolicyModule.controller('ClosedPolCtrl',['$state','ClosedPolService','CommonService','closedPolicyArr', function($state, ClosedPolService,CommonService, closedPolicyArr){
    debug("closedPolicyArr : " + JSON.stringify(closedPolicyArr));
    var cpc = this;
    this.closedPolicyArr = [];
    this.carouselIndex = 0;
//    localStorage.setItem("AS"," ");
//    localStorage.setItem("UW"," ");
//    localStorage.setItem("IP"," ");
//    localStorage.setItem("DP"," ");
//    localStorage.setItem("PUW"," ");
//    localStorage.setItem("CP","CP");

    localStorage.setItem("TabStateValue","closedPolicy");

	this.closedPolicyArr = CommonService.rebuildSlide(6, closedPolicyArr);

    this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
	CommonService.hideLoading();
}]);
closedPolicyModule.service('ClosedPolService',['$q','$state','CommonService', function($q,$state,CommonService){
    var cps = this;

    this.getClosedPolicyData = function(){
        var dfd = $q.defer();
        var closedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select mopp.LEAD_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS IN('W','L')) and (mopp.POLICY_NO = pa.POLICY_NO) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _closedPolicy = {};
                                _closedPolicy.TITLE = res.rows.item(j).PROPOSER_TITLE;
                                _closedPolicy.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _closedPolicy.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _closedPolicy.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _closedPolicy.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _closedPolicy.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _closedPolicy.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _closedPolicy.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _closedPolicy.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                var status = res.rows.item(j).POLICY_FINAL_STATUS;
                                _closedPolicy.POLICY_PREUW_STATUS = status == 'W' ? "Withdrawal" : (status == 'L' ? 'Lapsed' : '');
                                closedPolicyArr[j] = _closedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(closedPolicyArr);
            }
        );
        return dfd.promise;
    };
}]);
