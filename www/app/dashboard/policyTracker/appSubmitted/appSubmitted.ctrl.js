appSubmittedModule.controller('AppSubmittedCtrl',['$state','AppSubmittedService','CommonService','appSubmittedArr','onlinePaymentService', function($state, PolicyTrackerService,CommonService,appSubmittedArr,onlinePaymentService){
    debug("appSubmittedArr : " + JSON.stringify(appSubmittedArr));
    var asc = this;
    this.appSubmittedArr = [];
    this.carouselIndex = 0;
    this.reportPopup = true;
    this.paymentAppId = null;
    localStorage.setItem("TabStateValue","appSubmitted");
    this.appSubmittedArr = CommonService.rebuildSlide(6, appSubmittedArr);

    this.viewPending = function(appSubmittedObj){
//        if((appSubmittedObj.POLICY_PREUW_STATUS=='Submitted' || appSubmittedObj.POLICY_PREUW_STATUS=='L2 Approved' ||  appSubmittedObj.POLICY_PREUW_STATUS=='L1 Approved')){
//             console.log("status submitted or approved");
             $state.go('dashboard.policyTracker.viewPending',{'POLICY_NO' : appSubmittedObj.POLICY_NO,'POLICY_STATUS' : appSubmittedObj.POLICY_PREUW_STATUS});
//        }else{
//            debug("appSubmittedObj : " + JSON.stringify(appSubmittedObj));
//            $state.go('dashboard.policyTracker.viewPending',{'POLICY_NO' : appSubmittedObj.POLICY_NO,'POLICY_STATUS' : appSubmittedObj.POLICY_PREUW_STATUS})
//        }
    };

    this.docUpload = function(appSubmittedObj){
        debug("appSubmittedObj : " + JSON.stringify(appSubmittedObj));
        //$state.go("docUpload",{"appid":appSubmittedObj.APPLICATION_ID,"agentCd" : appSubmittedObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":appSubmittedObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true});
        $state.go("uploadDoc",{"appid":appSubmittedObj.APPLICATION_ID,"agentCd" : appSubmittedObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":appSubmittedObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true,"oppId":appSubmittedObj.OPP_ID,"comboId":appSubmittedObj.COMBO_ID})
    }

    this.payOnline = function(APPLICATION_ID, isDisabled){
		if(!isDisabled){
	        debug("APPLICATION_ID : " + APPLICATION_ID);
	        asc.paymentAppId = APPLICATION_ID;
	        onlinePaymentService.getProposerData(APPLICATION_ID).then(function(CustData){
	            debug("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
	            asc.CustName = CustData[0].FNAME;
	            asc.MobNo = CustData[0].MOBILENO;
	            asc.Email = CustData[0].EMAIL;
	            asc.TransAmt = (!!CustData[0].TERM_AMOUNT?parseInt(CustData[0].AMOUNT)+parseInt(CustData[0].TERM_AMOUNT):CustData[0].AMOUNT);
				//asc.TransAmt = CustData[0].AMOUNT;
	        });
	        this.reportPopup = !this.reportPopup;
		}
    };

    this.olPaymentData = function(){
        debug("inside callpopup");
        if(!!asc.paymentAppId){
            debug("App id::"+asc.paymentAppId);
            onlinePaymentService.checkPayDtls(asc.paymentAppId); //, true
        }
    }

    this.closePopUpOverlay = function(){
        this.reportPopup = !this.reportPopup;
    }

	this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
	CommonService.hideLoading();
}]);
appSubmittedModule.service('AppSubmittedService',['$q','$state', 'CommonService', function($q,$state, CommonService){
    var as = this;

    this.getAppSubmittedData = function(){
        var dfd = $q.defer();
        var appSubmittedArr = [];
        //query change by ganesh for online payment
        //select mopp.LEAD_ID, am.AGENT_CD, am.APPLICATION_ID, sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_PREUW_STATUS from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or (pa.POLICY_PREUW_STATUS IN('L1I','L1R','L1A','L2R','L2A','N') or (pa.POLICY_PREUW_STATUS = 'NA'))) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select apd.PAY_METHOD_ONLINE_FLAG as PAY_METHOD_ONLINE_FLAG,mopp.OPPORTUNITY_ID,mopp.COMBO_ID,mopp.LEAD_ID, am.AGENT_CD, am.APPLICATION_ID, sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_PREUW_STATUS, mopp.RECO_PRODUCT_DEVIATION_FLAG from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis, LP_APP_PAY_DTLS_SCRN_G apd where (apd.APPLICATION_ID = am.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or (pa.POLICY_PREUW_STATUS IN('L1I','L1R','L1A','L2R','L2A','N','BHI','BHA','BHR') or (pa.POLICY_PREUW_STATUS = 'NA'))) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        if(!!res && res.rows.length>0){
							debug("res.rows.length: " + res.rows.length);
                            for(var j=0;j<res.rows.length;j++){
                                var _appSubmitted = {};
								_appSubmitted.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _appSubmitted.TITLE = res.rows.item(j).PROPOSER_TITLE;
                                _appSubmitted.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _appSubmitted.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _appSubmitted.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _appSubmitted.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _appSubmitted.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _appSubmitted.APPLICATION_ID = res.rows.item(j).APPLICATION_ID;
                                _appSubmitted.AGENT_CD = res.rows.item(j).AGENT_CD;
                                _appSubmitted.PAY_METHOD_ONLINE_FLAG = res.rows.item(j).PAY_METHOD_ONLINE_FLAG;
								_appSubmitted.RECO_PRODUCT_DEVIATION_FLAG = res.rows.item(j).RECO_PRODUCT_DEVIATION_FLAG;
                                _appSubmitted.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _appSubmitted.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                _appSubmitted.OPP_ID = res.rows.item(j).OPPORTUNITY_ID;
                                _appSubmitted.COMBO_ID = res.rows.item(j).COMBO_ID;
                                var status = res.rows.item(j).POLICY_PREUW_STATUS;
                                var term_status = (status == 'BHI') ? 'BH Approval Pending' : (status == 'BHA' ? 'Business Head Approved' : (status == 'BHR' ? 'Business Head Rejected' : "Submitted"));
                                if(BUSINESS_TYPE=='IndusSolution'){
                                    _appSubmitted.POLICY_PREUW_STATUS = status ? (status=='L1I' ? 'L1 Pending' : (status=='L1R' ? 'L1 Reject' : (status=='L1A' ? 'L2 Pending' : (status=='L2R' ? 'L2 Reject' : (status=='L2A' ? 'L2 Approved' : term_status))))) : "Submitted";
                                }else{
                                    _appSubmitted.POLICY_PREUW_STATUS = status ? (status=='L1I' ? 'L1 Initiated' : (status=='L1R' ? 'L1 Reject' : (status=='L1A' ? 'L1 Approved' : (status=='L2R' ? 'L2 Reject' : (status=='L2A' ? 'L2 Approved' : term_status))))) : "Submitted";
                                }
                                //OLD
                                /*status ? (status=='L1I' ? 'L1 Initiated' : (status=='L1R' ? 'L1 Reject' : (status=='L1A' ? 'L1 Approved' : (status=='L2R' ? 'L2 Reject' : (status=='L2A' ? 'L2 Approved' : "Submitted"))))) : "Submitted";*/

                                appSubmittedArr[j] = _appSubmitted;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                //debug("return : " + appSubmittedArr.length)
                dfd.resolve(appSubmittedArr);
            }
        );
        return dfd.promise;
    };
}]);
