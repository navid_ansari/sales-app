issuedPolicyModule.controller('IssuedPolCtrl',['$state','IssuedPolService','CommonService','issuedPolicyArr', function($state, IssuedPolService,CommonService,issuedPolicyArr){
    debug("issuedPolicyArr : " + JSON.stringify(issuedPolicyArr));
    var ipc = this;
    this.issuedPolicyArr = [];
    this.carouselIndex = 0;

    localStorage.setItem("TabStateValue","issuedPolicy");

	this.issuedPolicyArr = CommonService.rebuildSlide(6, issuedPolicyArr);

    this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
	CommonService.hideLoading();
}]);
issuedPolicyModule.service('IssuedPolService',['$q','$state','CommonService', function($q,$state,CommonService){
    var ips = this;

    this.getIssuedPolicyData = function(){
        var dfd = $q.defer();
        var issuedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select mopp.LEAD_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'I') and (mopp.POLICY_NO = pa.POLICY_NO) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _issuedPolicy = {};
                                _issuedPolicy.TITLE = res.rows.item(j).PROPOSER_TITLE;
                                _issuedPolicy.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _issuedPolicy.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _issuedPolicy.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _issuedPolicy.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _issuedPolicy.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _issuedPolicy.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _issuedPolicy.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _issuedPolicy.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                var status = res.rows.item(j).POLICY_FINAL_STATUS;
                                _issuedPolicy.POLICY_PREUW_STATUS = status == 'I' ? "Issued" : "";
                                 issuedPolicyArr[j] = _issuedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(issuedPolicyArr);
            }
        );
        return dfd.promise;
    };
}]);
