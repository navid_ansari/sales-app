uwPendingModule.controller('UWPendingCtrl',['$state','UWPendingService','CommonService','uwPendingArr','onlinePaymentService', function($state, UWPendingService,CommonService, uwPendingArr,onlinePaymentService){
    debug("uwPendingArr : " + JSON.stringify(uwPendingArr));
    var uwpc = this;
    this.uwPendingArr = [];
    this.carouselIndex = 0;
    this.reportPopup = true;
    this.paymentAppId = null;
//    localStorage.setItem("AS"," ");
//    localStorage.setItem("PUW"," ");
//    localStorage.setItem("IP"," ");
//    localStorage.setItem("DP"," ");
//    localStorage.setItem("CP"," ");
//    localStorage.setItem("UW","UW");
    localStorage.setItem("TabStateValue","uwPending");

	this.uwPendingArr = CommonService.rebuildSlide(6, uwPendingArr);

    this.viewPending = function(uwPendObj){
        debug("uwPendObj : " + JSON.stringify(uwPendObj));
        $state.go('dashboard.policyTracker.viewPending',{'POLICY_NO' : uwPendObj.POLICY_NO,'POLICY_STATUS' : uwPendObj.POLICY_PREUW_STATUS});
    };

    this.docUpload = function(uwPendObj){
        debug("uwPendObj : " + JSON.stringify(uwPendObj));
        //$state.go("docUpload",{"appid":uwPendObj.APPLICATION_ID,"agentCd" : uwPendObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":uwPendObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true});
        $state.go("uploadDoc",{"appid":uwPendObj.APPLICATION_ID,"agentCd" : uwPendObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":uwPendObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true,"oppId":uwPendObj.OPP_ID,"comboId":uwPendObj.COMBO_ID});
    };

    this.payOnline = function(APPLICATION_ID, isDisabled){
		if(!isDisabled){
	        debug("APPLICATION_ID : " + APPLICATION_ID);
	        uwpc.paymentAppId = APPLICATION_ID;
	        onlinePaymentService.getProposerData(APPLICATION_ID).then(function(CustData){
	            debug("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
	            uwpc.CustName = CustData[0].FNAME;
	            uwpc.MobNo = CustData[0].MOBILENO;
	            uwpc.Email = CustData[0].EMAIL;
	            uwpc.TransAmt = (!!CustData[0].TERM_AMOUNT?parseInt(CustData[0].AMOUNT)+parseInt(CustData[0].TERM_AMOUNT):CustData[0].AMOUNT);
				//uwpc.TransAmt = CustData[0].AMOUNT;
	        });
	        this.reportPopup = !this.reportPopup;
		}
    };

    this.olPaymentData = function(){
        debug("inside callpopup");
        if(!!uwpc.paymentAppId){
            debug("App id::"+uwpc.paymentAppId);
            onlinePaymentService.checkPayDtls(uwpc.paymentAppId); //, true
        }

    };

    this.closePopUpOverlay = function(){
        this.reportPopup = !this.reportPopup;
    };

    this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
    CommonService.hideLoading();
}]);
uwPendingModule.service('UWPendingService',['$q','$state','CommonService', function($q,$state,CommonService){
    var uwps = this;

    this.getUWPendingData = function(){
        var dfd = $q.defer();
        var uwPendingArr = [];
        //select mopp.LEAD_ID,am.AGENT_CD,am.APPLICATION_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS from LP_APP_CONTACT_SCRN_A aca, LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (am.APPLICATION_ID = aca.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'P') and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc

        /* UW status logic Changes
        select apd.PAY_METHOD_ONLINE_FLAG as PAY_METHOD_ONLINE_FLAG,mopp.LEAD_ID,mopp.OPPORTUNITY_ID,mopp.COMBO_ID,am.AGENT_CD,am.APPLICATION_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS, mopp.RECO_PRODUCT_DEVIATION_FLAG from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis, LP_APP_PAY_DTLS_SCRN_G apd  where (apd.APPLICATION_ID = am.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'P') and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc*/
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select apd.PAY_METHOD_ONLINE_FLAG as PAY_METHOD_ONLINE_FLAG,mopp.LEAD_ID,mopp.OPPORTUNITY_ID,mopp.COMBO_ID,am.AGENT_CD,am.APPLICATION_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS, pa.POLICY_STATUS_CD, mopp.RECO_PRODUCT_DEVIATION_FLAG from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis, LP_APP_PAY_DTLS_SCRN_G apd  where (apd.APPLICATION_ID = am.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS IN( 'P','N')) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _uwPending = {};
                                _uwPending.TITLE = res.rows.item(j).PROPOSER_TITLE;
								_uwPending.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _uwPending.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _uwPending.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _uwPending.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _uwPending.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _uwPending.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _uwPending.AGENT_CD = res.rows.item(j).AGENT_CD;
                                _uwPending.APPLICATION_ID = res.rows.item(j).APPLICATION_ID;
                                _uwPending.PAY_METHOD_ONLINE_FLAG = res.rows.item(j).PAY_METHOD_ONLINE_FLAG;
								_uwPending.RECO_PRODUCT_DEVIATION_FLAG = res.rows.item(j).RECO_PRODUCT_DEVIATION_FLAG;
                                _uwPending.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _uwPending.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                _uwPending.OPP_ID = res.rows.item(j).OPPORTUNITY_ID;
                                _uwPending.COMBO_ID = res.rows.item(j).COMBO_ID;
                                var status = res.rows.item(j).POLICY_FINAL_STATUS;
                                var statusCd = parseInt(res.rows.item(j).POLICY_STATUS_CD);
                                /*status == 'P' ? "UW Pending" : ""*/
                                _uwPending.POLICY_PREUW_STATUS = (status == 'N' && statusCd == 19 ? 'UW Approved' : (status == 'P' ? 'Pending' : 'Others'));
                                uwPendingArr[j] = _uwPending;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(uwPendingArr);
            }
        );
        return dfd.promise;
    };
}]);
