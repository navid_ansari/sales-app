policyTrackerModule.controller('PolicyTrackerCtrl',['$state','PolicyTrackerService','CommonService','POLICYTRACKDATA','$rootScope', function($state, PolicyTrackerService,CommonService,POLICYTRACKDATA,$rootScope){
    var ptc = this;
    if(!!localStorage.getItem("TabStateValue")){
    console.log("set local for ip",localStorage.getItem("TabStateValue"));
    this.activeTab =localStorage.getItem("TabStateValue");
    }else{
        this.activeTab = 'appSubmitted';
    }
    debug("POLICYTRACKDATA : " + JSON.stringify(POLICYTRACKDATA));
    if(!!POLICYTRACKDATA.appSubmittedArr)
    this.appSubmittedCnt = POLICYTRACKDATA.appSubmittedArr.length;
    if(!!POLICYTRACKDATA.preUWDataArr)
    this.preUWDataCnt = POLICYTRACKDATA.preUWDataArr.length;
    if(!!POLICYTRACKDATA.uwPendingArr)
    this.uwPendingCnt = POLICYTRACKDATA.uwPendingArr.length;
    if(!!POLICYTRACKDATA.declinedPolicyArr)
    this.declinedPolicyCnt = POLICYTRACKDATA.declinedPolicyArr.length;
    if(!!POLICYTRACKDATA.issuedPolicyArr)
    this.issuedPolicyCnt = POLICYTRACKDATA.issuedPolicyArr.length;
    if(!!POLICYTRACKDATA.closedPolicyArr)
    this.closedPolicyCnt = POLICYTRACKDATA.closedPolicyArr.length;
    localStorage.setItem("AS","AS");

    this.gotoPage = function(pageName){
        $rootScope.setTabVal=false;
        console.log("gotoPage :pageName:"+pageName);
        switch(pageName){
            case 'appSubmitted':
                if(ptc.activeTab !== pageName)
                    //CommonService.showLoading("Please Wait...");
                ptc.activeTab = "appSubmitted";
                $state.go('dashboard.policyTracker.appSubmitted');
                break;
            case 'preUW':
                if(ptc.activeTab !== pageName)
//                    CommonService.showLoading("Please Wait...");
                ptc.activeTab = "preUW";
                $state.go('dashboard.policyTracker.preUW');

                break;
            case 'uwPending':
                if(ptc.activeTab !== pageName)
                    //CommonService.showLoading("Please Wait...");
                ptc.activeTab = "uwPending";
                $state.go('dashboard.policyTracker.uwPending');
                break;
            case 'issuedPolicy':
                if(ptc.activeTab !== pageName)
                   // CommonService.showLoading("Please Wait...");
                ptc.activeTab = "issuedPolicy";
                $state.go('dashboard.policyTracker.issuedPolicy');
                break;
            case 'declinedPolicy':
                if(ptc.activeTab !== pageName)
                   // CommonService.showLoading("Please Wait...");
                ptc.activeTab = "declinedPolicy";
                $state.go('dashboard.policyTracker.declinedPolicy');
                break;
            case 'closedPolicy':
                if(ptc.activeTab !== pageName)
                   // CommonService.showLoading("Please Wait...");
                ptc.activeTab = "closedPolicy";
                $state.go('dashboard.policyTracker.closedPolicy');
                break;
            default:
                if(ptc.activeTab !== pageName)
                    //CommonService.showLoading("Please Wait...");
                ptc.activeTab = "appSubmitted";
                $state.go('dashboard.policyTracker.appSubmitted');
        }

    };
}]);
policyTrackerModule.service('PolicyTrackerService',['$q','$state','CommonService','AppSubmittedService','PreUWService','UWPendingService','DeclinedPolService','IssuedPolService','ClosedPolService', function($q,$state,CommonService,AppSubmittedService,PreUWService,UWPendingService,DeclinedPolService,IssuedPolService,ClosedPolService){
    var pts = this;
    this.policyTrackData = {};
    this.getPolicyTrackData = function(){
        var dfd = $q.defer();
        AppSubmittedService.getAppSubmittedData().then(
            function(appSubmittedArr){
                pts.policyTrackData.appSubmittedArr = appSubmittedArr;
                PreUWService.getPreUWData().then(
                    function(preUWDataArr){
                        pts.policyTrackData.preUWDataArr = preUWDataArr;
                        UWPendingService.getUWPendingData().then(
                            function(uwPendingArr){
                                pts.policyTrackData.uwPendingArr = uwPendingArr;
                                DeclinedPolService.getDeclinedPolicyData().then(
                                    function(declinedPolicyArr){
                                        pts.policyTrackData.declinedPolicyArr = declinedPolicyArr;
                                        IssuedPolService.getIssuedPolicyData().then(
                                            function(issuedPolicyArr){
                                                pts.policyTrackData.issuedPolicyArr = issuedPolicyArr;
                                                ClosedPolService.getClosedPolicyData().then(
                                                    function(closedPolicyArr){
                                                        pts.policyTrackData.closedPolicyArr = closedPolicyArr;
                                                        dfd.resolve(pts.policyTrackData);
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
            }
        );
        return dfd.promise;
    };


    /*this.getAppSubmittedData = function(){
        var dfd = $q.defer();
        var appSubmittedArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _appSubmitted = {};
                                _appSubmitted.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _appSubmitted.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _appSubmitted.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _appSubmitted.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _appSubmitted.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _appSubmitted.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _appSubmitted.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _appSubmitted.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _appSubmitted.POLICY_PREUW_STATUS = "L1 Initaited";
                                appSubmittedArr[j] = _appSubmitted;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(appSubmittedArr);
            }
        );
        return dfd.promise;
    };

    this.getPreUWData = function(){
        var dfd = $q.defer();
        var preUWDataArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _preUW = {};
                                _preUW.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _preUW.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _preUW.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _preUW.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _preUW.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _preUW.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _preUW.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _preUW.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _preUW.POLICY_PREUW_STATUS = "L1 Initaited";
                                preUWDataArr[j] = _preUW;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(preUWDataArr);
            }
        );
        return dfd.promise;
    };

    this.getUWPendingData = function(){
        var dfd = $q.defer();
        var uwPendingArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _uwPending = {};
                                _uwPending.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _uwPending.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _uwPending.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _uwPending.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _uwPending.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _uwPending.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _uwPending.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _uwPending.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _uwPending.POLICY_PREUW_STATUS = "L1 Initaited";
                                uwPendingArr[j] = _uwPending;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(uwPendingArr);
            }
        );
        return dfd.promise;
    };

    this.getDeclinedPolicyData = function(){
        var dfd = $q.defer();
        var declinedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _declinedPolicy = {};
                                _declinedPolicy.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _declinedPolicy.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _declinedPolicy.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _declinedPolicy.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _declinedPolicy.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _declinedPolicy.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _declinedPolicy.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _declinedPolicy.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _declinedPolicy.POLICY_PREUW_STATUS = "L1 Initaited";
                                 declinedPolicyArr[j] = _declinedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(declinedPolicyArr);
            }
        );
        return dfd.promise;
    };

    this.getIssuedPolicyData = function(){
        var dfd = $q.defer();
        var issuedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _issuedPolicy = {};
                                _issuedPolicy.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _issuedPolicy.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _issuedPolicy.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _issuedPolicy.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _issuedPolicy.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _issuedPolicy.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _issuedPolicy.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _issuedPolicy.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _issuedPolicy.POLICY_PREUW_STATUS = "L1 Initaited";
                                 issuedPolicyArr[j] = _issuedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(issuedPolicyArr);
            }
        );
        return dfd.promise;
    };

    this.getClosedPolicyData = function(){
        var dfd = $q.defer();
        var closedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select aca.TITLE, aca.FIRST_NAME, aca.MIDDLE_NAME, aca.LAST_NAME, aca.MOBILE_NO, am.POLICY_NO, am.COMPLETION_TIMESTAMP from LP_APP_CONTACT_SCRN_A aca, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.APPLICATION_ID = aca.APPLICATION_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS is null or pa.POLICY_PREUW_STATUS = '') order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length<=0){
                            for(var j=0;j<3;j++){
                                var _closedPolicy = {};
                                _closedPolicy.TITLE = "Mr.";//res.rows.item(j).TITLE;
                                _closedPolicy.FIRST_NAME = "Ganesh";//res.rows.item(j).FIRST_NAME;
                                _closedPolicy.MIDDLE_NAME = null;//res.rows.item(j).MIDDLE_NAME;
                                _closedPolicy.LAST_NAME = "Shitole";//res.rows.item(j).LAST_NAME;
                                _closedPolicy.MOBILE_NO = "9960534152";//res.rows.item(j).MOBILE_NO;
                                _closedPolicy.POLICY_NO = "C789456123";//res.rows.item(j).POLICY_NO;
                                _closedPolicy.COMPLETION_TIMESTAMP = "12-06-2016 00:00:00";//res.rows.item(j).COMPLETION_TIMESTAMP || "";
                                _closedPolicy.FULL_NAME = "Mr Ganesh Shitole"; //res.rows.item(j).MIDDLE_NAME ? (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " res.rows.item(j).MIDDLE_NAME + " " + res.rows.item(j).LAST_NAME) : (res.rows.item(j).TITLE + " " + res.rows.item(j).FIRST_NAME + " " + res.rows.item(j).LAST_NAME);
                                _closedPolicy.POLICY_PREUW_STATUS = "L1 Initaited";
                                closedPolicyArr[j] = _closedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(closedPolicyArr);
            }
        );
        return dfd.promise;
    };*/

}]);
