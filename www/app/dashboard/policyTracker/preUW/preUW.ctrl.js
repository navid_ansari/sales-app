preUWModule.controller('PreUWCtrl',['$state','PreUWService','CommonService','preUWDataArr','onlinePaymentService', function($state, PreUWService,CommonService,preUWDataArr,onlinePaymentService){
    debug("preUWDataArr : " + JSON.stringify(preUWDataArr));
    var preC = this;
    this.preUWDataArr = [];
    this.carouselIndex = 0;
    this.reportPopup = true;
    this.paymentAppId = null;

	this.preUWDataArr = CommonService.rebuildSlide(6, preUWDataArr);
//    localStorage.setItem("AS"," ");
//    localStorage.setItem("UW"," ");
//    localStorage.setItem("IP"," ");
//    localStorage.setItem("DP"," ");
//    localStorage.setItem("CP"," ");
//    localStorage.setItem("PUW","PUW");
    localStorage.setItem("TabStateValue","preUW");



    this.viewPending = function(preUWObj){
        //if(preC.disableViewBtn==false){
        debug("preUWObj : " + JSON.stringify(preUWObj));
        $state.go('dashboard.policyTracker.viewPending',{'POLICY_NO' : preUWObj.POLICY_NO,'POLICY_STATUS' : preUWObj.POLICY_PREUW_STATUS})
       // }
    };

    this.docUpload = function(preUWObj){
        debug("preUWObj : " + JSON.stringify(preUWObj));
        //$state.go("docUpload",{"appid":preUWObj.APPLICATION_ID,"agentCd" : preUWObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":preUWObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true});
        $state.go("uploadDoc",{"appid":preUWObj.APPLICATION_ID,"agentCd" : preUWObj.AGENT_CD,"sisId":null,"ffhrId":null,"policyNo":preUWObj.POLICY_NO,"leadId":null,"pglId":null,"fromPT":true,"oppId":preUWObj.OPP_ID,"comboId":preUWObj.COMBO_ID});
    };

    this.payOnline = function(APPLICATION_ID, isDisabled){
		if(!isDisabled){
	        debug("APPLICATION_ID : " + APPLICATION_ID);
	        preC.paymentAppId = APPLICATION_ID;
	        onlinePaymentService.getProposerData(APPLICATION_ID).then(function(CustData){
	            debug("inside 1st function"+JSON.stringify(CustData)+"Value"+CustData[0].FNAME);
	            preC.CustName = CustData[0].FNAME;
	            preC.MobNo = CustData[0].MOBILENO;
	            preC.Email = CustData[0].EMAIL;
	            preC.TransAmt = (!!CustData[0].TERM_AMOUNT?parseInt(CustData[0].AMOUNT)+parseInt(CustData[0].TERM_AMOUNT):CustData[0].AMOUNT);
				//preC.TransAmt = CustData[0].AMOUNT;
	        });
	        this.reportPopup = !this.reportPopup;
		}
    };

    this.olPaymentData = function(){
        debug("inside callpopup");
        if(!!preC.paymentAppId){
            debug("App id::"+preC.paymentAppId);
            onlinePaymentService.checkPayDtls(preC.paymentAppId); //, true
        }

    }

    this.closePopUpOverlay = function(){
        this.reportPopup = !this.reportPopup;
    }

    this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
    CommonService.hideLoading();
}]);
preUWModule.service('PreUWService',['$q','$state','CommonService', function($q,$state,CommonService){
    var preS = this;

    this.getPreUWData = function(){
        var dfd = $q.defer();
        var preUWDataArr = [];
        //select mopp.LEAD_ID,am.AGENT_CD,am.APPLICATION_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_PREUW_STATUS from LP_APP_CONTACT_SCRN_A aca, LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (am.APPLICATION_ID = aca.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS IN('SMI','SMR','SMA','DSP','DSA')) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select apd.PAY_METHOD_ONLINE_FLAG as PAY_METHOD_ONLINE_FLAG,mopp.OPPORTUNITY_ID,mopp.COMBO_ID, mopp.LEAD_ID,am.AGENT_CD,am.APPLICATION_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_PREUW_STATUS, mopp.RECO_PRODUCT_DEVIATION_FLAG from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis, LP_APP_PAY_DTLS_SCRN_G apd where (apd.APPLICATION_ID = am.APPLICATION_ID) and (mopp.POLICY_NO = pa.POLICY_NO) and (am.POLICY_NO = pa.POLICY_NO) and (sis.SIS_ID = am.SIS_ID) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS IN('SMI','SMR','SMA','DSP','DSA')) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _preUW = {};
                                _preUW.TITLE = res.rows.item(j).PROPOSER_TITLE;
								_preUW.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _preUW.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _preUW.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _preUW.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _preUW.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _preUW.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _preUW.AGENT_CD = res.rows.item(j).AGENT_CD;
                                _preUW.APPLICATION_ID = res.rows.item(j).APPLICATION_ID;
                                _preUW.PAY_METHOD_ONLINE_FLAG = res.rows.item(j).PAY_METHOD_ONLINE_FLAG;
								_preUW.RECO_PRODUCT_DEVIATION_FLAG = res.rows.item(j).RECO_PRODUCT_DEVIATION_FLAG;
                                _preUW.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _preUW.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                _preUW.OPP_ID = res.rows.item(j).OPPORTUNITY_ID;
                                _preUW.COMBO_ID = res.rows.item(j).COMBO_ID;
                                var status = res.rows.item(j).POLICY_PREUW_STATUS;
                                _preUW.POLICY_PREUW_STATUS = status ? (status=='SMI' ? 'SMAR Initiated' : (status=='SMR' ? 'SMAR Reject' : (status=='SMA' ? 'SMAR Approved' : (status=='DSP' ? 'DST Pending' : (status=='DSA' ? 'DST Approved' : ""))))) : "";
                                preUWDataArr[j] = _preUW;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(preUWDataArr);
            }
        );
        return dfd.promise;
    };
}]);
