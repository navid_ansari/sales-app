viewPendingModule.controller('ViewPendingCtrl',['$state','ViewPendingService','CommonService','pendingList','appSubmittedArr','pendingL1List','POLICY_STATUS', function($state, ViewPendingService,CommonService, pendingList,appSubmittedArr,pendingL1List,POLICY_STATUS){
    debug("pendingList : " + JSON.stringify(pendingList));
    debug("pendingL1List : " + JSON.stringify(pendingL1List));
    debug("POLICY_STATUS : " + POLICY_STATUS);
    var vpc = this;
    this.visibleflag=true;
    this.rejectreason=pendingList;
    this.status = POLICY_STATUS;
    this.business_type=BUSINESS_TYPE;
    if(this.business_type=='iWealth' && (POLICY_STATUS == 'L1 Approved' || POLICY_STATUS == 'L2 Approved')){
        this.visibleflag=false;
    }

    if(POLICY_STATUS == 'L1 Pending' || POLICY_STATUS == 'L1 Approved' || POLICY_STATUS == 'L2 Pending' || POLICY_STATUS == 'L1 Reject' || POLICY_STATUS == 'L2 Approved' || POLICY_STATUS == 'L2 Reject'){
        console.log("in if");
      this.pendingL1List = pendingL1List;
    }else{
        console.log("in else");
      this.pendingList = pendingList;
    }
	CommonService.hideLoading();
}]);

viewPendingModule.service('ViewPendingService',['$q','$state','CommonService', function($q,$state,CommonService){
    var vps = this;

    this.getPendingList = function(POLICY_NO){
        var dfd = $q.defer();
        var pendingList = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT PENDING_CODE, PENDING_DETAILS FROM LP_POLICY_PENDING WHERE POLICY_NO=? ORDER BY PENDING_SEQ_NO DESC",[POLICY_NO],
                    function(tx,res){
                        debug("res.rows.length: " + res.rows.length);
                        if(res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _viewPending = {};
                                _viewPending.PENDING_CODE = res.rows.item(j).PENDING_CODE;
                                _viewPending.PENDING_DETAILS = res.rows.item(j).PENDING_DETAILS;
                                pendingList[j] = _viewPending;
                             }
                        }else{
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(pendingList);
            }
        );
        return dfd.promise;
    }

   this.getPendingL1List = function(POLICY_NO){
        var dfd = $q.defer();
        var pendingL1List = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"SELECT ASSIGN_TO FROM LP_POLICY_ASSIGNMENT WHERE POLICY_NO=? ",[POLICY_NO],
                    function(tx,res){
                        debug("res.rows.length L1 : " + res.rows.length);
                        if(res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                            debug("Result :"+JSON.stringify(res.rows.item(j)));
                                var _viewPending = {};
                                _viewPending.BRANCH_NAME = res.rows.item(j).ASSIGN_TO;
                                pendingL1List[j] = _viewPending;
                             }
                        }else{
                            dfd.resolve(null);
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(pendingL1List);
            }
        );
        return dfd.promise;
    }
}]);
