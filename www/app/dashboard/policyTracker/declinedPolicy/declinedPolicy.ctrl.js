declinedPolicyModule.controller('DeclinedPolCtrl',['$state','DeclinedPolService','CommonService','declinedPolicyArr', function($state, DeclinedPolService,CommonService, declinedPolicyArr){
    debug("declinedPolicyArr : " + JSON.stringify(declinedPolicyArr));
    var dpc = this;
    this.declinedPolicyArr = [];
    this.carouselIndex = 0;
//    localStorage.setItem("AS"," ");
//    localStorage.setItem("UW"," ");
//    localStorage.setItem("IP"," ");
//    localStorage.setItem("PUW"," ");
//    localStorage.setItem("CP"," ");
//    localStorage.setItem("DP","DP");

    localStorage.setItem("TabStateValue","declinedPolicy");
	this.declinedPolicyArr = CommonService.rebuildSlide(6, declinedPolicyArr);

    this.startNewOpp = function(LEAD_ID){
        debug("LEAD_ID : " + LEAD_ID);
        $state.go('leadDashboard',{"LEAD_ID": LEAD_ID, "FHR_ID": null, "SIS_ID": null, "APP_ID": null,"OPP_ID":null});
    };
	CommonService.hideLoading();
}]);
declinedPolicyModule.service('DeclinedPolService',['$q','$state','CommonService', function($q,$state,CommonService){
    var dps = this;

    this.getDeclinedPolicyData = function(){
        var dfd = $q.defer();
        var declinedPolicyArr = [];
        CommonService.transaction(db,
            function(tx){
                CommonService.executeSql(tx,"select mopp.LEAD_ID,sis.PROPOSER_TITLE, sis.PROPOSER_FIRST_NAME, sis.PROPOSER_MIDDLE_NAME, sis.PROPOSER_LAST_NAME, sis.PROPOSER_MOBILE, am.POLICY_NO, am.COMPLETION_TIMESTAMP, pa.POLICY_FINAL_STATUS from LP_MYOPPORTUNITY mopp,LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa, LP_SIS_SCREEN_A sis where (sis.SIS_ID = am.SIS_ID) and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'D') and (mopp.POLICY_NO = pa.POLICY_NO) and am.COMPLETION_TIMESTAMP is not null order by am.COMPLETION_TIMESTAMP desc",[],
                    function(tx,res){
                        debug("declinedPolicyArr res.rows.length: " + res.rows.length);
                        if(!!res && res.rows.length>0){
                            for(var j=0;j<res.rows.length;j++){
                                var _declinedPolicy = {};
                                _declinedPolicy.TITLE = res.rows.item(j).PROPOSER_TITLE;
                                _declinedPolicy.FIRST_NAME = res.rows.item(j).PROPOSER_FIRST_NAME;
                                _declinedPolicy.MIDDLE_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME;
                                _declinedPolicy.LAST_NAME = res.rows.item(j).PROPOSER_LAST_NAME;
                                _declinedPolicy.MOBILE_NO = res.rows.item(j).PROPOSER_MOBILE;
                                _declinedPolicy.POLICY_NO = res.rows.item(j).POLICY_NO;
                                _declinedPolicy.LEAD_ID = res.rows.item(j).LEAD_ID;
                                _declinedPolicy.COMPLETION_TIMESTAMP = res.rows.item(j).COMPLETION_TIMESTAMP.substring(0,10) || "";
                                _declinedPolicy.FULL_NAME = res.rows.item(j).PROPOSER_MIDDLE_NAME ? (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_MIDDLE_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME) : (res.rows.item(j).PROPOSER_TITLE + " " + res.rows.item(j).PROPOSER_FIRST_NAME + " " + res.rows.item(j).PROPOSER_LAST_NAME);
                                var status = res.rows.item(j).POLICY_FINAL_STATUS;
                                _declinedPolicy.POLICY_PREUW_STATUS = status == 'D' ? "Declined" : "";
                                 declinedPolicyArr[j] = _declinedPolicy;
                             }
                        }
                    },
                    function(tx,err){
                        dfd.resolve(null);
                    }
                );
            },
            function(err){
                dfd.resolve(null);
            },
            function(){
                dfd.resolve(declinedPolicyArr);
            }
        );
        return dfd.promise;
    };
}]);
