dashboardModule.controller('DashboardCtrl',['$state','DashboardService', 'CommonService','LoginService','$scope', function($state, DashboardService, CommonService,LoginService,$scope){
	var dc = this;
	dc.policyRefreshBtn = DashboardService.getRefreshFlag();
	$scope.service = DashboardService;
	$scope.$watch('service.getRefreshFlag()',function(flag){
		dc.policyRefreshBtn = flag;
	});

	this.isTabActive = function(tab){
		return ((!!tab) && (!!DashboardService.getActiveTab()) && (tab == DashboardService.getActiveTab()))?true:false;
	};

	this.gotoPage = function(pageName){
		DashboardService.gotoPage(pageName);
	};

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.refreshbtn = function(){
		var internetType=CommonService.checkConnection();
		if(internetType==='online'){
			navigator.notification.confirm("Do you want to refresh?",
				function(buttonIndex){
					if(buttonIndex=="1"){
						CommonService.showLoading("Refreshing Data...");
						LoginService.doOnlineLogin("RPL", LoginService.lgnSrvObj.userinfo.LOGIN_ID, LoginService.lgnSrvObj.userinfo.AGENT_CD, LoginService.lgnSrvObj.password, LoginService.lgnSrvObj.userinfo.DEVICE_ID, device.uuid, LoginService.ntidLogin, BUSINESS_TYPE, VERSION_NO);
					}
					else{
						debug("You have clicked no refresh");
					}
				},
				'Refresh',
				['Yes',' No']
			);
		}
		else{
			navigator.notification.alert("Please check your internet connection.",null,"Refresh","OK");
		}
	};
}]);


dashboardModule.service('DashboardService', ['$state', 'CommonService', function($state, CommonService){
	this.activeTab = "home";
	this.flagRefresh = false;
	var self=this;
	this.getRefreshFlag = function(){
		return this.flagRefresh;
	};

	this.setRefreshFlag = function(flag){
		self.flagRefresh = flag;
	};

	this.getActiveTab = function(){
		return this.activeTab;
	};

	this.setActiveTab = function(activeTab){
		this.activeTab = activeTab;
	};

	this.gotoPage = function(pageName){
		self.setRefreshFlag(false);
		if (!pageName || pageName==="solutions"){
			if(this.getActiveTab() !== pageName){
				CommonService.showLoading("Loading...");
			}
			$state.go("dashboard.solutions");
		}
		else if (pageName==="appTracker"){
			self.setRefreshFlag(false);
			if(this.getActiveTab() !== pageName){
				CommonService.showLoading("Loading...");
			}
			$state.go("dashboard.appTracker.oppNotInitiated");
		}
		else if (pageName==="home"){
			self.setRefreshFlag(false);
			if(this.getActiveTab() !== pageName){
				CommonService.showLoading("Loading...");
			}
			$state.go("dashboard.home");
		}
		else if (pageName==="policyTracker"){
			localStorage.setItem("TabStateValue","appSubmitted");
			self.setRefreshFlag(true);
			if(this.getActiveTab() !== pageName){
				CommonService.showLoading("Loading...");
			}
			$state.go('dashboard.policyTracker.appSubmitted');
		}
		else if (pageName==="downloads"){
			self.setRefreshFlag(false);
			if(CommonService.checkConnection()==="online"){
				if(this.getActiveTab() !== pageName){
					CommonService.showLoading("Loading...");
				}
				$state.go('dashboard.downloads');
			}
			else{
				navigator.notification.alert("Please connect to the internet to download the optional documents.",null,"Optional Documents","OK");
			}
		}
	};

}]);
