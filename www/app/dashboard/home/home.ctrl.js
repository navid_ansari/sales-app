homeModule.controller('HomeCtrl',['$state','HomeService', 'TopBannerCounts', 'ChartCounts', 'BottomBannerCounts', 'CommonService', 'AddLeadService', function($state, HomeService, TopBannerCounts, ChartCounts, BottomBannerCounts, CommonService, AddLeadService){
	"use strict";
	var hc = this;
	this.hideFF = (BUSINESS_TYPE!=="IndusSolution");

	this.dashboardClass = "dashboard-block-1";

	this.topBannerCounts = TopBannerCounts;
	this.bottomBannerCounts = BottomBannerCounts;
	this.chartCounts = ChartCounts;
	this.chartLabels = ['Pre-UW Pending', 'UW Pending', 'Issued', 'Declined', 'Closed','No Data Available'];
	this.chartData = HomeService.getChartData(this.chartCounts);
	this.activeTab = 'daily';

	this.imageName =  "../img/" + BUSINESS_TYPE + "_home.jpg";

	this.addLead = function(){
		CommonService.showLoading();
		AddLeadService.leadData = null;
		$state.go("leadPage1");
	};
	this.leadPopup = true;
	this.toggleLeadPopup = function () {
		this.leadPopup = !this.leadPopup;
	};

	this.refreshChart = function(frequency){
		CommonService.showLoading("Loading...");
		HomeService.getChartCounts(frequency||"daily").then(
			function(chartCounts){
				hc.activeTab = frequency;
				hc.chartCounts = chartCounts;
				if(!!chartCounts)
					hc.chartData = HomeService.getChartData(chartCounts);
				else
					hc.chartData = [0,0,0,0,0,100];
				CommonService.hideLoading();
			}
		);
	};
	CommonService.hideLoading();
}]);
homeModule.service('HomeService', ['$q', '$state', 'CommonService', function($q, $state, CommonService){
	var hc = this;
	this.getTopBannerCounts = function(agentCd){
		var dfd = $q.defer();
		var topBannerObj = {};
		this.getTotalLeadsCount(agentCd).then(
			function(leadsCount){
				debug("getTotalLeadsCount " + leadsCount);
				topBannerObj.LEADS = leadsCount;
				return leadsCount;
			}
		).then(
			function(leadsCount){
				if(BUSINESS_TYPE=="IndusSolution"){
					return hc.getTotalFactFinderCount(agentCd).then(
						function(ffCount){
							debug("getTotalFactFinderCount " + ffCount);
							topBannerObj.FF = ffCount;
							return ffCount;
						}
					);
				}
				else{
					return hc.getTotalFHRCount(agentCd).then(
						function(fhrCount){
							debug("getTotalFHRCount " + fhrCount);
							topBannerObj.FHR = fhrCount;
							return fhrCount;
						}
					);
				}
			}
		).then(
			function(ffCount){
				return hc.getTotalSISCount(agentCd).then(
					function(sisCount){
						debug("getTotalSISCount " + sisCount);
						topBannerObj.SIS = sisCount;
						return sisCount;
					}
				);
			}
		).then(
			function(sisCount){
				return hc.getInProgressAPPCount(agentCd).then(
					function(appCount){
						debug("getInProgressAPPCount " + appCount);
						topBannerObj.APP_IN_PROGRESS = appCount;
						return appCount;
					}
				);
			}
		).then(
			function(appCount){
				return hc.getCompletedAPPCount(agentCd).then(
					function(appCompletedCount){
						debug("getCompletedAPPCount " + appCompletedCount);
						topBannerObj.APP_COMPLETED = appCompletedCount;
						return appCompletedCount;
					}
				);
			}
		).then(
			function(appCompletedCount){
				debug("TottomBannerObj " + JSON.stringify(topBannerObj));
				dfd.resolve(topBannerObj);
			}
		);
		return dfd.promise;
	};

	this.getTotalLeadsCount = function(agentCd){
		var dfd = $q.defer();
		debug("in function getTotalLeadsCount");
		this.getTotalCount("LEAD", agentCd).then(
			function(res){
				if(!!res){
					dfd.resolve(res + "");
				}
				else {
					dfd.resolve("0");
				}
			}
		);
		return dfd.promise;
	};

	this.getTotalFHRCount = function(agentCd){
		var dfd = $q.defer();
		this.getTotalCount("FHR", agentCd).then(
			function(res){
				if(!!res){
					dfd.resolve(res + "");
				}
				else {
					dfd.resolve("0");
				}
			}
		);
		return dfd.promise;
	};

	this.getTotalFactFinderCount = function(agentCd){
		var dfd = $q.defer();
		this.getTotalCount("FF", agentCd).then(
			function(res){
				if(!!res){
					dfd.resolve(res + "");
				}
				else {
					dfd.resolve("0");
				}
			}
		);
		return dfd.promise;
	};

	this.getTotalSISCount = function(agentCd){
		var dfd = $q.defer();
		this.getTotalCount("SIS", agentCd).then(
			function(res){
				if(!!res){
					dfd.resolve(res + "");
				}
				else {
					dfd.resolve("0");
				}
			}
		);
		return dfd.promise;
	};

	this.getInProgressAPPCount = function(agentCd){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from lp_application_main where app_submitted_flag is null or app_submitted_flag!=? and agent_cd=?",['Y', agentCd],
					function(tx, res){
						if(!!res && res.rows.length>0){
							dfd.resolve(res.rows.item(0).total_count);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getCompletedAPPCount = function(agentCd){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from lp_application_main where app_submitted_flag=? and agent_cd=?",['Y', agentCd],
					function(tx, res){
						if(!!res && res.rows.length>0){
							dfd.resolve(res.rows.item(0).total_count);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getTotalCount = function(type, agentCd){
		var dfd = $q.defer();
		debug("in function getTotalCount");
		CommonService.transaction(db,
			function(tx){
				debug("transaction");
				var tableName = "lp_mylead a";
				var primaryKey = "ipad_lead_id";
				var optString = "";
				var parameterList = [agentCd];
				if(type=="FF"){
					tableName = "lp_fhr_ffna a";
					primaryKey = "a.fhr_id";
				}
				else if(type=="FHR"){
					tableName = "lp_fhr_main a, lp_myopportunity b";
					primaryKey = "a.fhr_id";
					optString = " and a.fhr_id=b.fhr_id and (b.reco_product_deviation_flag is null OR b.reco_product_deviation_flag!=?) " ;
					parameterList.push("S");
				}
				else if(type=="SIS"){
					tableName = "lp_sis_main a";
					primaryKey = "a.sis_id";
				}
				else if(type=="APP"){
					tableName = "lp_app_main a";
					primaryKey = "a.app_id";
				}
				CommonService.executeSql(tx,"select count(*) as total_count from " + tableName + " where " + primaryKey + " is not null and a.agent_cd=?" + optString, parameterList,
					function(tx, res){
						if(!!res && res.rows.length>0){
							debug("executeSql count: " + res.rows.item(0).total_count + " " +primaryKey);
							dfd.resolve(res.rows.item(0).total_count);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getBottomBannerCounts = function(){
		var dfd = $q.defer();
		this.getChartCounts().then(
			function(res){
				dfd.resolve(res);
			}
		);
		return dfd.promise;
	};

	this.getChartCounts = function(frequency){
		var dfd = $q.defer();
		var chartCountObj = {};
		var totalCount = 0;
		this.getPreUWPendingCount(frequency).then(
			function(preUWPendingCount){
				debug("PreUWPendingCount " + preUWPendingCount);
				chartCountObj.PreUWPendingCount = preUWPendingCount;
				totalCount += parseInt(preUWPendingCount);
				return totalCount;
			}
		).then(
			function(totalCount){
				return hc.getUWPendingCount(frequency).then(
					function(uwPendingCount){
						debug("UWPendingCount " + uwPendingCount);
						chartCountObj.UWPendingCount = uwPendingCount;
						totalCount += parseInt(uwPendingCount);
						return totalCount;
					}
				);
			}
		).then(
			function(totalCount){
				return hc.getIssuedCount(frequency).then(
					function(issuedCount){
						debug("IssuedCount " + issuedCount);
						chartCountObj.IssuedCount = issuedCount;
						totalCount += parseInt(issuedCount);
						return totalCount;
					}
				);
			}
		).then(
			function(totalCount){
				return hc.getDeclinedCount(frequency).then(
					function(declinedCount){
						debug("DeclinedCount " + declinedCount);
						chartCountObj.DeclinedCount = declinedCount;
						totalCount += parseInt(declinedCount);
						return totalCount;
					}
				);
			}
		).then(
			function(totalCount){
				return hc.getClosedCount(frequency).then(
					function(closedCount){
						debug("ClosedCount " + closedCount);
						chartCountObj.ClosedCount = closedCount;
						totalCount += parseInt(closedCount);
						return totalCount;
					}
				);
			}
		).then(
			function(totalCount){
				chartCountObj.TotalCount = totalCount;
				debug("chartCountObj " + JSON.stringify(chartCountObj));
				dfd.resolve(chartCountObj);
			}
		);
		return dfd.promise;
	};

	this.getPreUWPendingCount = function(frequency){
		var dfd = $q.defer();
		var dateQuery = "";
		var addnlStatus = "";
		if(!!frequency && (frequency=="daily")){
			var _date = new Date();
			dateQuery = " and am.COMPLETION_TIMESTAMP like '%" + (_date.getDate() + "-" + (((_date.getMonth()+1)<10)?("0"+(_date.getMonth()+1)):(_date.getMonth()+1)) + "-" + _date.getFullYear()) + "%'";
			addnlStatus = ",'L1I','L1R','L1A','L2R','L2A'";
		}
		else if(!!frequency && frequency=="weekly"){
			var weekLimits = this.returnFirstAndLastDayOfCurrentWeek();
			var _first = weekLimits.FirstDay;
			var _last = weekLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
			addnlStatus = ",'L1I','L1R','L1A','L2R','L2A'";
		}
		else if(!!frequency && frequency=="monthly"){
			var monthLimits = this.returnFirstAndLastDayOfCurrentMonth();
			var _first = monthLimits.FirstDay;
			var _last = monthLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
			addnlStatus = ",'L1I','L1R','L1A','L2R','L2A'";
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS IN('SMI','SMR','SMA','DSP','DSA'" + addnlStatus + "))" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc",[],
					function(tx, res){
						debug("query: " + "select count(*) as total_count from LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'U') and (pa.POLICY_PREUW_STATUS IN('SMI','SMR','SMA','DSP','DSA'" + addnlStatus + "))" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc");
						if(!!res && res.rows.length>0){
							debug("pre uw: " + res.rows.length);
							dfd.resolve((res.rows.item(0).total_count<10)?("0"+(res.rows.item(0).total_count)):(res.rows.item(0).total_count<10));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getUWPendingCount = function(frequency){
		var dfd = $q.defer();
		var dateQuery = "";
		if(!!frequency && (frequency=="daily")){
			var _date = new Date();
			dateQuery = " and am.COMPLETION_TIMESTAMP like '%" + (_date.getDate() + "-" + (((_date.getMonth()+1)<10)?("0"+(_date.getMonth()+1)):(_date.getMonth()+1)) + "-" + _date.getFullYear()) + "%'";
		}
		else if(!!frequency && (frequency=="weekly")){
			var weekLimits = this.returnFirstAndLastDayOfCurrentWeek();
			var _first = weekLimits.FirstDay;
			var _last = weekLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		else if(!!frequency && (frequency=="monthly")){
			var monthLimits = this.returnFirstAndLastDayOfCurrentMonth();
			var _first = monthLimits.FirstDay;
			var _last = monthLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'P')" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc",[],
					function(tx, res){
						debug("select count(*) as total_count from LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'P')" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc");
						if(!!res && res.rows.length>0){
							dfd.resolve((res.rows.item(0).total_count<10)?("0"+(res.rows.item(0).total_count)):(res.rows.item(0).total_count<10));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getIssuedCount = function(frequency){
		var dfd = $q.defer();
		var dateQuery = "";
		if(!!frequency &&  (frequency=="daily")){
			var _date = new Date();
			dateQuery = " and am.COMPLETION_TIMESTAMP like '%" + (_date.getDate() + "-" + (((_date.getMonth()+1)<10)?("0"+(_date.getMonth()+1)):(_date.getMonth()+1)) + "-" + _date.getFullYear()) + "%'";
		}
		else if(!!frequency && (frequency=="weekly")){
			var weekLimits = this.returnFirstAndLastDayOfCurrentWeek();
			var _first = weekLimits.FirstDay;
			var _last = weekLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		else if(!!frequency && (frequency=="monthly")){
			var monthLimits = this.returnFirstAndLastDayOfCurrentMonth();
			var _first = monthLimits.FirstDay;
			var _last = monthLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'I') and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc",[],
					function(tx, res){
						debug("select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'I') and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc");
						if(!!res && res.rows.length>0){
							dfd.resolve((res.rows.item(0).total_count<10)?("0"+(res.rows.item(0).total_count)):(res.rows.item(0).total_count<10));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getDeclinedCount = function(frequency){
		var dfd = $q.defer();
		var dateQuery = "";
		if(!!frequency && (frequency=="daily")){
			var _date = new Date();
			dateQuery = " and am.COMPLETION_TIMESTAMP like '%" + (_date.getDate() + "-" + (((_date.getMonth()+1)<10)?("0"+(_date.getMonth()+1)):(_date.getMonth()+1)) + "-" + _date.getFullYear()) + "%'";
		}
		else if(!!frequency && (frequency=="weekly")){
			var weekLimits = this.returnFirstAndLastDayOfCurrentWeek();
			var _first = weekLimits.FirstDay;
			var _last = weekLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		else if(!!frequency && (frequency=="monthly")){
			var monthLimits = this.returnFirstAndLastDayOfCurrentMonth();
			var _first = monthLimits.FirstDay;
			var _last = monthLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'D') and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc",[],
					function(tx, res){
						debug("select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS = 'D') and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc");
						if(!!res && res.rows.length>0){
							dfd.resolve((res.rows.item(0).total_count<10)?("0"+(res.rows.item(0).total_count)):(res.rows.item(0).total_count<10));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getClosedCount = function(frequency){
		var dfd = $q.defer();
		var dateQuery = "";
		if(!!frequency && (frequency=="daily")){
			var _date = new Date();
			dateQuery = " and am.COMPLETION_TIMESTAMP like '%" + (_date.getDate() + "-" + (((_date.getMonth()+1)<10)?("0"+(_date.getMonth()+1)):(_date.getMonth()+1)) + "-" + _date.getFullYear()) + "%'";
		}
		else if(!!frequency && (frequency=="weekly")){
			var weekLimits = this.returnFirstAndLastDayOfCurrentWeek();
			var _first = weekLimits.FirstDay;
			var _last = weekLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		else if(!!frequency && (frequency=="monthly")){
			var monthLimits = this.returnFirstAndLastDayOfCurrentMonth();
			var _first = monthLimits.FirstDay;
			var _last = monthLimits.LastDay;
			var firstDay = _first.getFullYear() + "-" + (((_first.getMonth()+1)<10)?("0"+(_first.getMonth()+1)):(_first.getMonth()+1)) + "-" + ((_first.getDate()<10)?("0"+_first.getDate()):(_first.getDate()));
			var lastDay = _last.getFullYear() + "-" + (((_last.getMonth()+1)<10)?("0"+(_last.getMonth()+1)):(_last.getMonth()+1)) + "-" + ((_last.getDate()<10)?("0"+_last.getDate()):(_last.getDate()));
			dateQuery = " and date(substr(am.COMPLETION_TIMESTAMP,7,4) || '-' || substr(am.COMPLETION_TIMESTAMP,4,2) || '-' || substr(am.COMPLETION_TIMESTAMP,1,2)) between '" + firstDay + "' and '" + lastDay + "'";
		}
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS IN('W','L')) and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc",[],
					function(tx, res){
						debug("select count(*) as total_count from LP_MYOPPORTUNITY mopp, LP_APPLICATION_MAIN am, LP_POLICY_ASSIGNMENT pa where and (am.POLICY_NO = pa.POLICY_NO) and (am.APP_SUBMITTED_FLAG = 'Y') and (pa.POLICY_FINAL_STATUS IN('W','L')) and (mopp.POLICY_NO = pa.POLICY_NO)" + dateQuery + " order by am.COMPLETION_TIMESTAMP desc");
						if(!!res && res.rows.length>0){
							dfd.resolve((res.rows.item(0).total_count<10)?("0"+(res.rows.item(0).total_count)):(res.rows.item(0).total_count<10));
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			}
		);
		return dfd.promise;
	};

	this.getChartData = function(chartCounts) {
		var chartData = [];
		debug("chartCounts: " + JSON.stringify(chartCounts));
		var totalCount = parseInt(chartCounts.TotalCount);
		if(chartCounts.TotalCount == "0"){
			return [0,0,0,0,0,100];
		}
		else{
			if(parseInt(chartCounts.PreUWPendingCount)>=0){
				var preUWPendingCount = parseInt(chartCounts.PreUWPendingCount);
				chartData.push(Math.round((preUWPendingCount/ totalCount) * 100));
			}
			if(parseInt(chartCounts.UWPendingCount)>=0){
				var uwPendingCount = parseInt(chartCounts.UWPendingCount);
				chartData.push(Math.round((uwPendingCount/ totalCount) * 100));
			}
			if(parseInt(chartCounts.IssuedCount)>=0){
				var issuedCount = parseInt(chartCounts.IssuedCount);
				chartData.push(Math.round((issuedCount/ totalCount) * 100));
			}
			if(parseInt(chartCounts.DeclinedCount)>=0){
				var declinedCount = parseInt(chartCounts.DeclinedCount);
				chartData.push(Math.round((declinedCount/ totalCount) * 100));
			}
			if(parseInt(chartCounts.ClosedCount)>=0){
				var closedCount = parseInt(chartCounts.ClosedCount);
				chartData.push(Math.round((closedCount/ totalCount) * 100));
			}
			chartData.push(0);
			debug("chartData: " + chartData);
			return chartData;
		}
	};

	this.returnFirstAndLastDayOfCurrentWeek = function(){
		var now = new Date();
		var startDay = 1; //0=sunday, 1=monday etc.
		var d = now.getDay(); //get the current day
		var weekStart = new Date(now.valueOf() - (d<=0 ? 7-startDay:d-startDay)*86400000); //rewind to start day
		var weekEnd = new Date(weekStart.valueOf() + 6*86400000);
		return {"FirstDay": weekStart, "LastDay": weekEnd};
	};

	this.returnFirstAndLastDayOfCurrentMonth = function(){
		var date = new Date();
		var firstDay = new Date(date.getFullYear()+"-"+(date.getMonth()+1)+"-01");
		var lastDay = new Date(date.getFullYear(), date.getMonth() + 1);
		return {"FirstDay": firstDay, "LastDay": lastDay};
	};

}]);
