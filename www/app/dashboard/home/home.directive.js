homeModule.directive("setDbImgHeight", function(){
	return{
		restrict: "A",
		link: function(scope, element, attrs){
			try{
				var dashboardRootHeight = (window.getComputedStyle(document.querySelector('.dashboard-root')).height);
				dashboardRootHeight = parseInt(dashboardRootHeight.split("px")[0]);
				debug("dashboardRootHeight: " + dashboardRootHeight + "px");

				var dashboardHeaderHeight = (window.getComputedStyle(document.querySelector('.dashboard-header')).height);
				dashboardHeaderHeight = parseInt(dashboardHeaderHeight.split("px")[0]);
				debug("dashboardHeaderHeight: " + dashboardHeaderHeight + "px");

				var db1Height = (window.getComputedStyle(document.querySelector('.dashboard-wrap ul li')).height);
				db1Height = parseInt(db1Height.split("px")[0]);
				debug("db1Height: " + db1Height + "px");

				var dashboardNavHeight = (window.getComputedStyle(document.querySelector('.dashboard-nav')).height);
				dashboardNavHeight = parseInt(dashboardNavHeight.split("px")[0]);
				debug("dashboardNavHeight: " + dashboardNavHeight + "px");

				var height = dashboardRootHeight - (dashboardHeaderHeight + db1Height + dashboardNavHeight);
				debug("setDB2ImgHeight: " + height + "px");

				if(device.platform==="iOS"){
					height += 2;
				}
				debug("window.innerHeight: " + window.innerHeight);
				if(window.innerHeight < 700){
					debug("window.innerHeight: " + height + "px");
					if(device.platform==="iOS"){
						height = 513;
					}
					else{
						height = 488;
						debug("device.platform.height: " + height + "px");
					}
				}
				element.css("height", height + "px");
			}
			catch(ex){
				debug("Exception in setDbImgHeight: " + ex.message);
				if(device.platform==="android"){
					element.css("height", "488px");
				}
				else if(device.platform==="iOS"){
					element.css("height", "513px");
				}
			}
		}
	};
});
