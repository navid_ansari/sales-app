ftlSetPwdModule.controller('FtlSetPwdCtrl',['SecurityQuestions','FtlSetPwdService','LoginService', 'CommonService',function(SecurityQuestions,FtlSetPwdService,LoginService, CommonService){
	"use strict";
	var fspc = this;
	this.appName = CommonService.getAppName();
	this.password = null;
	this.rePassword = null;
	this.securityQuestions = SecurityQuestions;
	this.submitCPReq = function(){
		FtlSetPwdService.submitCPReq(LoginService.lgnSrvObj.username, this.password, this.rePassword, LoginService.lgnSrvObj.dvid, this.securityQuestions);
	};

	/*get Height*/
	this.getHeight = (window.innerHeight - 200) + "px";
	CommonService.hideLoading();
}]);

ftlSetPwdModule.service('FtlSetPwdService',['$q','$state','LoginService', 'PwdMgtService', 'ChangeSecQuesService', 'CommonService', function($q, $state, LoginService, PwdMgtService, ChangeSecQuesService, CommonService){
	"use strict";
	var fsps = this;
	this.submitCPAction = function(username, password, rePassword, dvid, securityQuestions){
		var dfd = $q.defer();
		debug("Answers: " + JSON.stringify(securityQuestions));
		debug("LoginService.lgnSrvObj.userinfo.AGENT_CD: " + username + "\tLoginService.lgnSrvObj.dvid: " + dvid);
		if(!!password && password == rePassword){
			PwdMgtService.changePassword(username,password,dvid).then(
				function(changePasswordResp){
					debug("ChangePassword resolved: " + JSON.stringify(changePasswordResp));
					if(!!changePasswordResp && changePasswordResp.RS.RESP == "S"){
						LoginService.lgnSrvObj.password = password;
						ChangeSecQuesService.setSecQuesAns(username,securityQuestions).then(
							function(setSecQuesAnsResp){
								debug("setSecQuesAnsResp:: " + JSON.stringify(setSecQuesAnsResp));
								dfd.resolve(setSecQuesAnsResp);
							}
						);
					}
					else{
						window.alert("Could not process the request. Please try again after some time.");
						dfd.resolve(null);
					}
				}
			);
		}
		else{
			window.alert("Both the passwords do not match.");
			dfd.resolve(null);
		}
		return dfd.promise;
	};

	this.submitCPReq = function(username, password, rePassword, dvid, securityQuestions){
		CommonService.showLoading();
		this.submitCPAction(username, password, rePassword, dvid, securityQuestions).then(
			function(submitResult){
				if(!!submitResult){
					LoginService.continueFTL();
				}
				else {
					CommonService.hideLoading();
				}
			}
		);
	};

	this.changePassword = function(username,password,dvid){
		var dfd = $q.defer();
		function cpSucc(resp){
			dfd.resolve(resp);
		}
		function cpErr(data, status, headers, config, statusText) {
			debug("Error in changePassword");
			window.alert("Could not process the request. Please try again after some time.");
			dfd.reject(null);
		}
		try{
			var inputCPReq = {};
			inputCPReq.REQ = {};
			inputCPReq.REQ.AC = username;
			inputCPReq.REQ.DVID = dvid;
			inputCPReq.REQ.PWD = password;
			inputCPReq.REQ.ACN="CP";
			CommonService.ajaxCall(LOGIN_SERVLET_URL, AJAX_TYPE, TYPE_JSON, inputCPReq, AJAX_ASYNC, AJAX_TIMEOUT, cpSucc, cpErr);
		} catch(ex){
			debug("Exception in changePassword(): " + ex.message);
			dfd.reject(null);
		}
		return dfd.promise;
	};

	this.getSecurityQuestions = function(){
		var dfd = $q.defer();
		try{
			CommonService.transaction(db,
				function(tx){
					CommonService.executeSql(tx,"select distinct question_no, question_desc from lp_security_question_master where isactive=? order by random() limit ?",['Y',SEC_QUES_LIMIT],
						function(tx,res){
							try{
								var securityQuestions = [];
								if(!!res && res.rows.length > 0){
									for(var i=0;i<res.rows.length;i++){
										securityQuestions[i] = res.rows.item(i);
										securityQuestions[i].ANSWER = null;
										securityQuestions[i].FORM_NAME = "secQues" + i;
									}
								}
								dfd.resolve(securityQuestions);
							}catch(ex){
								dfd.reject("Exception");
								debug("ex: " + ex.message);
							}
						},
						function(tx,err){
							dfd.reject("Error");
						}
					);
				}
			);
		}catch(ex){
			dfd.reject("Exception");
			debug("Exception in getSecurityQuestions(): " + ex.message);
		}
		return dfd.promise;
	};
}]);
