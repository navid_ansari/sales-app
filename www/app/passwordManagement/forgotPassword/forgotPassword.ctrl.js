forgotPasswordModule.controller('ForgotPasswordCtrl',['ForgotPasswordService','SecQuesData','LoginService','$state', 'CommonService',function(ForgotPasswordService,SecQuesData,LoginService,$state, CommonService){
	"use strict";
	this.appName = CommonService.getAppName();
	this.secQues = SecQuesData.QUESTION_DESC;
	this.secQuesAnsOrg = SecQuesData.USER_ANSWER;
	this.secQuesAns = null;
	this.newPassword = null;
	this.confirmPassword = null;

	this.onSubmit = function(){
		ForgotPasswordService.submitCPReq(this.secQuesAns, this.secQuesAnsOrg, this.newPassword, this.confirmPassword, LoginService.lgnSrvObj.username, LoginService.lgnSrvObj.dvid).then(
			function(submitCPReqResp){
				if(!!submitCPReqResp){
					navigator.notification.alert("Password changed.",null,"Forgot Password","OK");
					$state.go(LOGIN_STATE);
				}
			}
		);
	};

	/*get Height*/
	//this.getHeight = (window.innerHeight - 200) + "px";
	CommonService.hideLoading();
}]);

forgotPasswordModule.service('ForgotPasswordService',['$q','CommonService', 'PwdMgtService', function($q,CommonService,PwdMgtService){
	"use strict";
	var fp = this;
	this.getSecurityQuestionData = function(username){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select distinct sqm.question_desc, sqa.user_answer from lp_security_question_answer sqa, lp_security_question_master sqm where sqa.agent_cd=? and sqm.question_no = sqa.question_no order by random() limit ?",[username,'1'],
					function(tx,res){
						if(!!res && res.rows.length>0)
							dfd.resolve(res.rows.item(0));
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.submitCPReq = function(secQuesAns, secQuesAnsOrg, newPassword, confirmPassword, username, dvid){
		var dfd = $q.defer();
		if(secQuesAns.trim() == secQuesAnsOrg.trim()){
			if(!!newPassword && !!confirmPassword && !!newPassword.trim() && !!confirmPassword.trim()){
				if(newPassword == confirmPassword){
					if(CommonService.validatePassword(newPassword)){
						PwdMgtService.changePassword(username, newPassword, dvid).then(
							function(chPassResp){
								dfd.resolve(chPassResp);
							}
						);
					}
					else{
						dfd.resolve(null);
					}
				}
				else{
					navigator.notification.alert("Both the passwords do not match.",null,"Forgot Password","OK");
					dfd.resolve(null);
				}
			}
			else{
				navigator.notification.alert("Please enter the password.",null,"Forgot Password","OK");
				dfd.resolve(null);
			}
		}
		else{
			navigator.notification.alert("The answer to the security question is incorrect.",null,"Forgot Password","OK");
			dfd.resolve(null);
		}
		return dfd.promise;
	};
}]);
