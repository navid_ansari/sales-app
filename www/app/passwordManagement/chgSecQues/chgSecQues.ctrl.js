chgSecQuesModule.controller('ChangeSecQuesCtrl',['ChangeSecQuesService','QuestionData','LoginService','$state','CommonService',function(ChangeSecQuesService,QuestionData,LoginService,$state,CommonService){
	"use strict";
	var csq = this;
	debug("Question List: " + JSON.stringify(QuestionData));
	this.questionListMaster = QuestionData.questionsListMaster;
	this.quesData = QuestionData.quesData;

	this.openMenu = function(){
		CommonService.openMenu();
	};

	this.onChange = function(){
		var questionsLists = ChangeSecQuesService.changeQuestionsList(this.quesData, this.questionListMaster);
		for(var i=0;i<questionsLists.length;i++){
			this.quesData[i].QUESTION_LIST = questionsLists[i];
		}
	};

	this.onSubmit = function(){
		var securityQuestions = [];
		angular.forEach(this.quesData,function(question){
			debug("Selected Question: " + JSON.stringify(question.SELECTED_QUESTION));
			debug("Answer: " + JSON.stringify(question.ANSWER));
			debug("\n");

			var secQuesObj = {};
			secQuesObj.QUESTION_NO = question.SELECTED_QUESTION.QUESTION_NO;
			secQuesObj.ANSWER = question.ANSWER;
			securityQuestions.push(secQuesObj);
		});
		ChangeSecQuesService.setSecQuesAns(LoginService.username, securityQuestions).then(
			function(setSecQuesAnsResp){
				debug("setSecQuesAnsResp:: " + JSON.stringify(setSecQuesAnsResp));
				$state.go(PROFILE_STATE);
			}
		);
	};

	CommonService.hideLoading();
}]);
chgSecQuesModule.service('ChangeSecQuesService',['$q','CommonService', function($q,CommonService){
	"use strict";
	var csqs = this;
	this.changeQuestionsList = function(quesData,questionListMaster){
		var questionsList = [];
		angular.forEach(quesData,function(ques){
			var tempQuesList = JSON.parse(JSON.stringify(questionListMaster));
			angular.forEach(quesData,function(innerQues){
				if(ques.SELECTED_QUESTION != innerQues.SELECTED_QUESTION){
					var index = CommonService.getIndexOfObject(tempQuesList,innerQues.SELECTED_QUESTION);
					tempQuesList.splice(index, 1);
				}
			});
			questionsList.push(tempQuesList);
		});
		return questionsList;
	};

	this.getQuestionData = function(){
		var dfd = $q.defer();
		var questionData = [];
		this.getQuestionList().then(
			function(questionsListMaster){
				var i=0;
				for(i=0;i<SEC_QUES_LIMIT;i++){
					var _question = {};
					_question.Q_NO = (i+1) + "";
					_question.QUESTION_LIST = JSON.parse(JSON.stringify(questionsListMaster));
					_question.SELECTED_QUESTION = _question.QUESTION_LIST[i];
					_question.ANSWER = null;
					questionData.push(_question);
				}
				var _quesLists = csqs.changeQuestionsList(questionData, questionsListMaster);
				for(i=0;i<_quesLists.length;i++){
					questionData[i].QUESTION_LIST = _quesLists[i];
				}

				var QuestionData = {};
				QuestionData.quesData = questionData;
				QuestionData.questionsListMaster = questionsListMaster;

				dfd.resolve(QuestionData);
			}
		);
		return dfd.promise;
	};

	this.getQuestionList = function(){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"select question_no, question_desc from lp_security_question_master where isactive=?",
					['Y'],
					function(tx,res){
						if(!!res && res.rows.length>0){
							var quesList = [];
							for(var i=0;i<res.rows.length;i++){
								quesList[i] = res.rows.item(i);
							}
							dfd.resolve(quesList);
						}
						else{
							dfd.resolve(null);
						}
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.setSecQuesAns = function(username,securityQuestions){
		var promises = [];
		try{
			debug("securityQuestions.length: " + securityQuestions.length);
			CommonService.transaction(db,
				function(tx){
					function insertFunction(securityQuestion){
						var dfd = $q.defer();
						CommonService.executeSql(tx,"insert or replace into lp_security_question_answer (agent_cd, question_no, user_answer, modified_date) values (?,?,?,?)",
							[username, securityQuestion.QUESTION_NO, securityQuestion.ANSWER,CommonService.getCurrDate()],
							function(tx,res){
								debug(securityQuestion.QUESTION_NO + " answer set");
								dfd.resolve({"QUES_NO":securityQuestion.QUESTION_NO,"ANS": securityQuestion.ANSWER});
							},
							function(tx,err){
								dfd.resolve(null);
								debug("Error in setSecQuesAns(): " + err.message);
							}
						);
						return dfd.promise;
					}
					CommonService.executeSql(tx,"delete from lp_security_question_answer where agent_cd=?",[username],
						function(tx,res){
							angular.forEach(securityQuestions,function(securityQuestion){
								promises.push(insertFunction(securityQuestion));
							});
						},null
					);
				},
				function(err){
					promises = [];
					debug("Error in setSecQuesAns(): " + err.message);
				},
				function(){
					debug("Successfully setSecQuesAns()");
				}
			);
		}catch(ex){
			promises = [];
			debug("Exception in setSecQuesAns(): " + ex.message);
		}
		return $q.all(promises);
	};
}]);
