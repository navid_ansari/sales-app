var pwdMgtModule = angular.module('app.passwordManagement', [
	'app.passwordManagement.ftlSetPwd',
	'app.passwordManagement.changePassword',
	'app.passwordManagement.chgSecQues',
	'app.passwordManagement.forgotPassword'
]);
