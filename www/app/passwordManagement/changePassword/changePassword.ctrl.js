chgPassModule.controller('ChangePasswordCtrl',['LoginService', 'ChangePasswordService', 'CommonService', function(LoginService, ChangePasswordService, CommonService){
	"use strict";
	var cp = this;
	this.password = null;
	this.rePassword = null;
	this.submitCPReq = function(){
		var agentCd = LoginService.lgnSrvObj.userinfo.AGENT_CD;
		ChangePasswordService.submitCPReq(agentCd, this.password, this.rePassword, LoginService.lgnSrvObj.dvid);
	};
	this.openMenu = function(){
		CommonService.openMenu();
	};
	CommonService.hideLoading();
}]);

chgPassModule.service('ChangePasswordService',['$q','$state','LoginService', 'PwdMgtService', function($q,$state,LoginService,PwdMgtService){
	"use strict";
	var cp = this;
	this.submitCPReq = function(username, password, rePassword, dvid){
		var dfd = $q.defer();
		if(!!password && password == rePassword){
			PwdMgtService.changePassword(username,password,dvid).then(
				function(changePasswordResp){
					debug("ChangePassword resolved: " + JSON.stringify(changePasswordResp));
					if(!!changePasswordResp && changePasswordResp.RS.RESP == "S"){
						LoginService.lgnSrvObj.password = password;
						$state.go(LOGIN_STATE);
					}
					else{
						if(!!LoginService.lgnSrvObj.errorDescObj[changePasswordResp.RS.EC])
							navigator.notification.alert("CP_EC_" + changePasswordResp.RS.EC + ": " + LoginService.lgnSrvObj.errorDescObj[changePasswordResp.RS.EC],null,"Change Password","OK");
						else
							navigator.notification.alert("Could not process the request. Please try again after some time.",null,"Change Password","OK");
						dfd.resolve(null);
					}
				}
			);
		}
		else
			navigator.notification.alert("Both the passwords do not match.",null,"Change Password","OK");
		return dfd.promise;
	};
}]);
