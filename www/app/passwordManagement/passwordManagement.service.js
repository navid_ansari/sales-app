pwdMgtModule.service('PwdMgtService',['$q','$state','CommonService', function($q,$state,CommonService){
	"use strict";
	var pm = this;
	this.updatePassInUserInfo = function(username, password){
		var dfd = $q.defer();
		CommonService.transaction(db,
			function(tx){
				CommonService.executeSql(tx,"update lp_userinfo set password=? where agent_cd=?",[null, username],
					function(tx,res){
						dfd.resolve("success");
					},
					function(tx,err){
						dfd.resolve(null);
					}
				);
			},
			function(err){
				dfd.resolve(null);
			},null
		);
		return dfd.promise;
	};

	this.changePassword = function(username,password,dvid){
		var dfd = $q.defer();
		function cpSucc(chPassResp){
			debug("changePassword resp: " + JSON.stringify(chPassResp));
			if(!!chPassResp && chPassResp.RS.RESP=="S"){
				pm.updatePassInUserInfo(username, password).then(
					function(updatePassResp){
						dfd.resolve(chPassResp);
					}
				);
			}

		}
		function cpErr(data, status, headers, config, statusText) {
			debug("Error in changePassword");
			dfd.resolve(null);
		}		
		try{
			var inputCPReq = {};
			inputCPReq.REQ = {};
			inputCPReq.REQ.AC = username;
			inputCPReq.REQ.DVID = dvid;
			inputCPReq.REQ.PWD = password;
			inputCPReq.REQ.ACN="CP";
			CommonService.ajaxCall(LOGIN_SERVLET_URL, AJAX_TYPE, TYPE_JSON, inputCPReq, AJAX_ASYNC, AJAX_TIMEOUT, cpSucc, cpErr);
		} catch(ex){
			debug("Exception in changePassword(): " + ex.message);
			dfd.resolve(null);
		}
		return dfd.promise;
	};

}]);
