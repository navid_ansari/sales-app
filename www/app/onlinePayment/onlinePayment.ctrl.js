onlinePayModule.controller('onlinePayCtrl',['$state','onlinePaymentService','$q','CommonService',function($state,onlinePaymentService,$q,CommonService){
var ol = this;
    debug("Online pay controller loaded");

//op.TransactionID = op.OTPayArr[2];
onlinePaymentService.returnPaymentDtls().then(function(OTPayArr){
    console.log("Controller OTPARR is::"+JSON.stringify(OTPayArr));
    ol.TransactionID = OTPayArr[2];
    ol.PolicyNo = OTPayArr[1];
    ol.Amount = OTPayArr[4];
    ol.TransactionDate = OTPayArr[13];
});

this.goToHomePg = function(){
    $state.go("dashboard.home");
}


}]);


onlinePayModule.service('onlinePaymentService',['$state','$q','CommonService','ApplicationFormDataService','LoginService','LoadApplicationScreenData',function($state,$q,CommonService,ApplicationFormDataService,LoginService,LoadApplicationScreenData){
    debug("Online pay service loaded");
    var ol = this;
    ol.abc=[];

    this.pay = function(paymentReq,APP_ID){
        var dfd = $q.defer();
        console.log("PaymentReqObj is::"+JSON.stringify(paymentReq)+"Connnection is::");

/*            var insertStr = "TALIC|C998840330|HCMP4885222465|014078-661794|1.00|CMP|485446|03|INR|VDDIRECT|NA|NA|NA|09-11-2016 19:15:40|0300|NA|Initial|Term_Case|4477641|NA|9874563210|ddd@g.com|TB|NA|PGS10001-Success|2692642333";
            var APP_ID = "801051130332";
            var OTPayArr = insertStr.split("|");
            console.log(APP_ID);
            console.log(insertStr);
            ol.insertOTPayDtls(OTPayArr,APP_ID);
            dfd.resolve(OTPayArr);
            return dfd.promise;*/

            if(CommonService.checkConnection()!="online"){
                navigator.notification.alert("Please be online",null,"Online Payment","OK");               
            }
            else{
            cordova.exec(
                function(succ){
                    debug("pay now success --->"+succ);
                    var OTPayArr = succ.split("|");
                    console.log("OTPayArr is::"+JSON.stringify(OTPayArr[14])+"Array value::"+JSON.stringify(OTPayArr)+"APP_ID::"+APP_ID);
                    switch(OTPayArr[14]){
                         case '0300':
                            console.log("Successful Transaction::"+JSON.stringify(OTPayArr));
                            //$.mobile.changePage("OTdone.html");
                            $state.go("onlinePayment");
							navigator.notification.alert("Payment success",null, "Online Payment","OK");
                            ol.insertOTPayDtls(OTPayArr,APP_ID);
                            dfd.resolve(OTPayArr);
                            break;
                         case '0399':
                            console.log("Invalid authentication in bank"+OTPayArr[14]);
                            navigator.notification.confirm(OTPayArr[24],
                                function(buttonIndex){
                                    if(buttonIndex=="1")
                                    sendOTData();
                                },
                                'Error',
                                'Try Again, Cancel'
                                );
                            dfd.resolve(OTPayArr);
                            break;
                         case 'NA':
                            console.log("Invalid Input in the request message"+OTPayArr[14]);
                            navigator.notification.confirm(OTPayArr[24],
                                function(buttonIndex){
                                    if(buttonIndex=="1")
                                    sendOTData();
                                },
                                'Error',
                                'Try Again, Cancel'
                                );
                            dfd.resolve(OTPayArr);
                        break;
                         case '0002':
                            console.log("Unable to ascertain the status of transaction"+OTPayArr[14]);
                            navigator.notification.confirm(OTPayArr[24],
                                function(buttonIndex){
                                    if(buttonIndex=="1")
                                    sendOTData();
                                },
                                'Error',
                                'Try Again, Cancel'
                                );
                            dfd.resolve(OTPayArr);
                        break;
                         case '0001':
                            console.log("Error at BillDesk");
                            navigator.notification.confirm(OTPayArr[24],
                                function(buttonIndex){
                                    if(buttonIndex=="1")
                                    sendOTData();
                                },
                                'Error',
                                'Try Again, Cancel'
                                );
                            dfd.resolve(OTPayArr);
                        break;
                         default:
                            console.log("nthng");
                            dfd.resolve(null);
                         }
                },
                function(errMsg){
                    debug("Error:::"+JSON.stringify(errMsg));
                },
                //"MainPlugin","",[paymentReq]
                "PluginHandler","paymentGateway",[paymentReq]
            );
        }
        return dfd.promise;
    };


    this.sendOTData = function(APP_ID, fromPolicyTracker){
        var dfd = $q.defer();
        //var COMBO_ID = ApplicationFormDataService.ComboFormBean.COMBO_ID || "";

        if(APP_ID==undefined)
            APP_ID = ApplicationFormDataService.applicationFormBean.APPLICATION_ID;

//        var sql = "SELECT SISA.PROPOSER_FIRST_NAME AS FNAME,SISA.PROPOSER_MIDDLE_NAME AS MNAME,SISA.PROPOSER_LAST_NAME AS LNAME,SISA.PROPOSER_MOBILE AS MOBILENO,SISA.PROPOSER_EMAIL AS EMAIL,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS AMOUNT,APPMAIN.PLAN_CODE AS PLAN_CODE,APPMAIN.POLICY_NO AS POLICYNO FROM LP_SIS_SCREEN_A AS SISA INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) INNER JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = APPMAIN.APPLICATION_ID) WHERE APPMAIN.APPLICATION_ID=? AND SISA.AGENT_CD=?";

        var sql = "SELECT a.*,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS TERM_AMOUNT from (SELECT SISA.SIS_ID,SISA.PROPOSER_FIRST_NAME AS FNAME,SISA.PROPOSER_MIDDLE_NAME AS MNAME,SISA.PROPOSER_LAST_NAME AS LNAME,SISA.PROPOSER_MOBILE AS MOBILENO,SISA.PROPOSER_EMAIL AS EMAIL,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS AMOUNT,APPMAIN.PLAN_CODE AS PLAN_CODE,APPMAIN.POLICY_NO AS POLICYNO,LOP.REF_APPLICATION_ID,LOP.COMBO_ID AS COMBO_ID FROM LP_SIS_SCREEN_A AS SISA "
                +" INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) "
                +" INNER JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = APPMAIN.APPLICATION_ID) "
                +" LEFT JOIN LP_MYOPPORTUNITY as LOP on LOP.application_id=APPMAIN.APPLICATION_ID "
                +" WHERE APPMAIN.APPLICATION_ID=? AND SISA.AGENT_CD=?) "
             +" a  "
             +" INNER JOIN LP_SIS_SCREEN_A AS SISA on (SISA.SIS_ID = a.SIS_ID) "
             +" INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) "
             +" LEFT JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = a.REF_APPLICATION_ID) ";

        CommonService.transaction(db,function(tx){
            CommonService.executeSql(tx,sql,[APP_ID,LoginService.lgnSrvObj.username],function(tx,res){
            console.log("Result len is::"+res.rows.length+"APP_ID"+APP_ID+"Username::"+LoginService.lgnSrvObj.username);
                if(!!res && res.rows.length>0){
                    var paymentReqObj = {};
                    var CUSTNAME = "";
                    CUSTNAME = res.rows.item(0).FNAME.trim() + " " + (((!!res.rows.item(0).MNAME)&&(!!res.rows.item(0).MNAME.trim()))?(res.rows.item(0).MNAME.trim() + " "):"") + res.rows.item(0).LNAME.trim();
                    console.log("OwnerName : " + CUSTNAME);
                    CUSTNAME = CUSTNAME.trim();
                    CUSTNAME = CUSTNAME.replace(/\s/g,"_");
                    CUSTNAME = CUSTNAME.replace("'","");
                    CUSTNAME = CUSTNAME.replace("-","");
                    console.log("OwnerName : " + CUSTNAME);

                    if(!!res.rows.item(0).COMBO_ID)
                        paymentReqObj.PolicyNo = res.rows.item(0).COMBO_ID;
                    else
                        paymentReqObj.PolicyNo = res.rows.item(0).POLICYNO;

					if(!!fromPolicyTracker){
						paymentReqObj.Amount = res.rows.item(0).AMOUNT;
					}
					else{
						paymentReqObj.Amount = (!!res.rows.item(0).TERM_AMOUNT?((parseInt(res.rows.item(0).AMOUNT)+parseInt(res.rows.item(0).TERM_AMOUNT)) + ""):res.rows.item(0).AMOUNT);
					}

                    console.log("Amount is "+paymentReqObj.Amount);
                    //paymentReqObj.Amount = "1";

                    paymentReqObj.PremiumType = "Initial";
                    paymentReqObj.OwnerName = CUSTNAME;
                    paymentReqObj.PremiumDueDate = "NA";
                    paymentReqObj.Mobile = res.rows.item(0).MOBILENO;
                    paymentReqObj.EmailId = res.rows.item(0).EMAIL;
                    paymentReqObj.TrSeries = "TB";

                    var plancode = res.rows.item(0).PLAN_CODE;
                    console.log("Plan_CODE123"+plancode);
                    if (plancode=='IRSSP1N1V2' || plancode=='IRSRP1N1V2' || plancode=='IRSL51N1V2' || plancode=='IRSL10N1V2' || plancode=='IRTRSP1N1' || plancode=='IRTRRP1N1' || plancode=='IRTRL51N1' || plancode=='IRTRL10N1')
                    paymentReqObj.AgentCode = '004587542';
                    else
                    paymentReqObj.AgentCode = LoginService.lgnSrvObj.username;
                    console.log("Payment req object is"+JSON.stringify(paymentReqObj));
                    dfd.resolve(paymentReqObj);
                }

            },function(tx,err)
                {
                    console.log("Database error"+err.message);
                    dfd.resolve(paymentReqObj);
                });
        },function(error)
        {
            console.log("Error::"+error.message);
            dfd.resolve(null);
        });

      return dfd.promise;
    }


    this.insertOTPayDtls = function(payRespArr,APP_ID){
        console.log("inside insertOTPayDtls");
        var COMBO_ID = null;
        var REFPOLICYNO = null;
        var POLICYNO = null;
		var REFAPPID = null;

        var newBasePAmt = 0;
        var newBaseTermAmt = 0;

        this.getProposerData(APP_ID).then(function(CustData){
            console.log("inside insertOTPayDtls fetching proposar details"+JSON.stringify(CustData));
            if(!!CustData){
                newBasePAmt = CustData[0].AMOUNT;
                newBaseTermAmt = CustData[0].TERM_AMOUNT;
                COMBO_ID = CustData[0].COMBO_ID;
                REFPOLICYNO = CustData[0].REF_POLICY_NO;
                POLICY_NO = CustData[0].POLICY_NO;
            	REFAPPID = CustData[0].REF_APPLICATION_ID;

        CommonService.transaction(db,function(tx){
            CommonService.executeSql(tx,"insert into LP_ONLINE_PAYMENT_DTLS values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                                    [CommonService.getCurrDate(),payRespArr[0],POLICY_NO,payRespArr[2],payRespArr[3],newBasePAmt,payRespArr[5],payRespArr[6],payRespArr[7],payRespArr[9],payRespArr[13],payRespArr[14],payRespArr[16],payRespArr[17],payRespArr[18],payRespArr[19],payRespArr[20],payRespArr[21],payRespArr[22],payRespArr[23],payRespArr[24],TALIC_MERCHANT_ID,"N",null,null],
                function(res){
					CommonService.executeSql(tx,"update LP_APP_PAY_DTLS_SCRN_G set TRANSACTION_DATE = ?,ONLINE_AMOUNT = ?,ONLINE_TRANS_REF_NO = ? where APPLICATION_ID = ? AND AGENT_CD = ?",
                                        [payRespArr[13],payRespArr[4],payRespArr[2],APP_ID,LoginService.lgnSrvObj.username],
                        function(res){
                            console.log("Table upadted successfully");
                        },function(tx,err){
                            console.log("Error::"+err.message);
                        });
                    console.log("data of base plan inserted successfully."+payRespArr[1]);

                    // Inserting if Combo Id is Present
                    if(!!COMBO_ID){
                        debug("Inside Term data insertion");
                        var newRefId = payRespArr[2]+"01";  //  New reference id for term plan transaction
                        CommonService.executeSql(tx,"insert into LP_ONLINE_PAYMENT_DTLS values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                                    [CommonService.getCurrDate(),payRespArr[0],REFPOLICYNO,newRefId,payRespArr[3],newBaseTermAmt,payRespArr[5],payRespArr[6],payRespArr[7],payRespArr[9],payRespArr[13],payRespArr[14],payRespArr[16],payRespArr[17],payRespArr[18],payRespArr[19],payRespArr[20],payRespArr[21],payRespArr[22],payRespArr[23],payRespArr[24],TALIC_MERCHANT_ID,"N",null,null],
                        function(res){
                            console.log("data of term plan inserted successfully."+payRespArr[1]);
							CommonService.executeSql(tx,"update LP_APP_PAY_DTLS_SCRN_G set TRANSACTION_DATE = ?,ONLINE_AMOUNT = ?,ONLINE_TRANS_REF_NO = ? where APPLICATION_ID = ? AND AGENT_CD = ?",
		                                        [payRespArr[13],payRespArr[4],payRespArr[2],REFAPPID,LoginService.lgnSrvObj.username],
		                        function(res){
		                            console.log("Table upadted successfully");
		                        },function(tx,err){
		                            console.log("Error::"+err.message);
		                        });
                        },
                        function(tx,error){
                            console.log("Database Error"+error.message);
                        });

                    }
            },
            function(tx,error){
                console.log("Database Error"+error.message);
            });
        });
		}
	});
    }

    /*this.appProceedtoPay = function(APP_ID){
        CommonService.transaction(db,function(tx){
            CommonService.executeSql(tx,"Select APPMAIN.APP_SUBMITTED_FLAG AS SUBMITFLAG,PAYDTLS. PAY_METHOD_ONLINE_FLAG from LP_APPLICATION_MAIN as APPMAIN INNER JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON APPMAIN.APPLICATION_ID = PAYDTLS.APPLICATION_ID WHERE PAYDTLS.APPLICATION_ID=?",
                                    [APP_ID]).then(
                function(tx,res){
                console.log("Length:::"+res.rows.length);
                    if(!!res && res.rows.length>0){
                        if(res.rows.item(0).SUBMITFLAG!=undefined && res.rows.item(0).SUBMITFLAG!="" && res.rows.item(0).SUBMITFLAG=='Y' && res.rows.item(0).PAY_METHOD_ONLINE_FLAG!=undefined && res.rows.item(0).PAY_METHOD_ONLINE_FLAG=='Y'){
                            if(CommonService.checkConnection()!= "online")
                            alert("Please be online");
                            else{}

                        }
                        else{}

                    }
                    else{}
                },
                function(tx,err){
                console.log("Error Occured::"+err.message);
                }
            );
        })

    }*/

    this.getProposerData = function(APP_ID){
    var dfd = $q.defer();
        CommonService.transaction(db,function(tx){
            /*SELECT SISA.PROPOSER_FIRST_NAME AS FNAME,SISA.PROPOSER_MIDDLE_NAME AS MNAME,SISA.PROPOSER_LAST_NAME AS LNAME,SISA.PROPOSER_MOBILE AS MOBILENO,SISA.PROPOSER_EMAIL AS EMAIL,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS AMOUNT FROM LP_SIS_SCREEN_A AS SISA INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) INNER JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = APPMAIN.APPLICATION_ID) WHERE APPMAIN.APPLICATION_ID=? AND SISA.AGENT_CD=?*/

            var query = "SELECT a.*,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS TERM_AMOUNT from (SELECT APPMAIN.APPLICATION_ID,SISA.SIS_ID,SISA.PROPOSER_FIRST_NAME AS FNAME,SISA.PROPOSER_MIDDLE_NAME AS MNAME,SISA.PROPOSER_LAST_NAME AS LNAME,SISA.PROPOSER_MOBILE AS MOBILENO,SISA.PROPOSER_EMAIL AS EMAIL,PAYDTLS.TOTAL_PAYMENT_AMOUNT AS AMOUNT,LOP.REF_APPLICATION_ID,LOP.COMBO_ID AS COMBO_ID,LOP.REF_POLICY_NO AS REF_POLICY_NO,LOP.POLICY_NO FROM LP_SIS_SCREEN_A AS SISA "
                +" INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) "
                +" INNER JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = APPMAIN.APPLICATION_ID) "
                +" LEFT JOIN LP_MYOPPORTUNITY as LOP on LOP.application_id=APPMAIN.APPLICATION_ID "
                +" WHERE APPMAIN.APPLICATION_ID=? AND SISA.AGENT_CD=?) "
             +" a  "
             +" INNER JOIN LP_SIS_SCREEN_A AS SISA on (SISA.SIS_ID = a.SIS_ID) "
             +" INNER JOIN LP_APPLICATION_MAIN AS APPMAIN ON (SISA.SIS_ID = APPMAIN.SIS_ID) "
             +" LEFT JOIN LP_APP_PAY_DTLS_SCRN_G AS PAYDTLS ON (PAYDTLS.APPLICATION_ID = a.REF_APPLICATION_ID) ";

            console.log(query);
            console.log(APP_ID+"----"+LoginService.lgnSrvObj.username);

            CommonService.executeSql(tx,query,[APP_ID,LoginService.lgnSrvObj.username],
                function(tx,res){
                var custData = [];
                console.log("Selected record result::"+res.rows.length);
                console.log(JSON.stringify(res.rows.item(0)));
                    if(!!res && res.rows.length>0){
                        var CUSTNAME = res.rows.item(0).FNAME.trim() + " " + (((!!res.rows.item(0).MNAME)&&(!!res.rows.item(0).MNAME.trim()))?(res.rows.item(0).MNAME.trim() + " "):"") + res.rows.item(0).LNAME.trim();
                        var custDataObj = {};
                        custDataObj.FNAME = CUSTNAME;
                        custDataObj.MOBILENO = res.rows.item(0).MOBILENO;
                        custDataObj.EMAIL = res.rows.item(0).EMAIL;
                        custDataObj.AMOUNT = res.rows.item(0).AMOUNT;
                        custDataObj.TERM_AMOUNT = res.rows.item(0).TERM_AMOUNT || 0;
                        custDataObj.COMBO_ID = res.rows.item(0).COMBO_ID || null;
                        custDataObj.REF_POLICY_NO = res.rows.item(0).REF_POLICY_NO || null;
                        custDataObj.POLICY_NO = res.rows.item(0).POLICY_NO || null;
						custDataObj.REF_APPLICATION_ID = res.rows.item(0).REF_APPLICATION_ID || null;
                        console.log("Customer data is::"+JSON.stringify(custDataObj));
                        custData.push(custDataObj);
                        dfd.resolve(custData);
                    }
                else
                        dfd.resolve(custData);
                }
            );

        });

        return dfd.promise;
    }

    /*this.checkPayDtls = function(APP_ID){

        CommonService.transaction(db,function(tx){
            CommonService.executeSql(tx,"Select TRANS_REF_ID from LP_ONLINE_PAYMENT_DTLS AS OLPAYDTLS inner join LP_APP_PAY_DTLS_SCRN_G AS APPPAYDTLS on (OLPAYDTLS.TRANS_REF_ID =  APPPAYDTLS.ONLINE_TRANS_REF_NO) where APPPAYDTLS.APPLICATION_ID=? AND APPPAYDTLS.AGENT_CD=?",
                                     [APP_ID,LoginService.lgnSrvObj.username],
             function(tx,res){
             console.log("Result len::"+res.rows.length);
                if(!!res && res.rows.length>0){
                    alert("Payment has already done");
                    $state.go("applicationForm.appOutput");
                }
                else if(CommonService.checkConnection()!="online")
                    alert("Please be online for online payment");
                else{
                    ol.sendOTData(APP_ID).then(function(res){
                    console.log("result is::"+JSON.stringify(res));
                    ol.pay(res,APP_ID).then(function(OTPayArr){
                    ol.abc = OTPayArr
                    });

                    });
                }
             },function(tx,err){
                console.log("Message::"+err.message);
            });

        });

    }*/


    this.checkPayDtls = function(APP_ID, fromPolicyTracker){
        try{
            CommonService.transaction(db,function(tx){
                    CommonService.executeSql(tx,"select APP_SUBMITTED_FLAG,PAY_METHOD_ONLINE_FLAG,TRANSACTION_DATE,ONLINE_TRANS_REF_NO from LP_APPLICATION_MAIN AM,LP_APP_PAY_DTLS_SCRN_G APPG where (AM.APPLICATION_ID = APPG.APPLICATION_ID) AND (AM.APPLICATION_ID=?) AND (AM.AGENT_CD=?)",
                                             [APP_ID,LoginService.lgnSrvObj.username],
                function(tx,results){
                console.log("length::::"+results.rows.length+"connection is::"+CommonService.checkConnection());
                    if(results.rows.length > 0){
                        if(results.rows.item(0).PAY_METHOD_ONLINE_FLAG != undefined && results.rows.item(0).PAY_METHOD_ONLINE_FLAG.toLowerCase() == "Y".toLowerCase()){
                            if(CommonService.checkConnection()=="online"){
                                if((results.rows.item(0).TRANSACTION_DATE == undefined || results.rows.item(0).TRANSACTION_DATE == "") && (results.rows.item(0).ONLINE_TRANS_REF_NO == undefined || results.rows.item(0).ONLINE_TRANS_REF_NO =="")){
                                    ol.sendOTData(APP_ID, fromPolicyTracker).then(function(res){
                                    console.log("result is::"+JSON.stringify(res));
                                    ol.pay(res,APP_ID).then(function(OTPayArr){
                                    ol.abc = OTPayArr
                                         });
                                    });
                                }else{
                                    navigator.notification.alert("Payment has already done",null ,"OnlinePayment","OK");
                                }
                            }else{
                                navigator.notification.alert('Please be onlilne for Online Payment',null ,"OnlinePayment","OK");
                            }
                        }else{
                            navigator.notification.alert('You have selected different payment method',null ,"OnlinePayment","OK");
                        }
                    }
                },
                function(tx,error){
                    console.log("Database error 1 : " + error.message);
                });
            },function(tx,error){
                console.log("Database error 2 : " + error.message);
            });
        }catch(ex){
            console.log("Exception in ProceedToPayment : " + ex.message);
        }
    }

    this.returnPaymentDtls = function(){
        var dfd = $q.defer();
        dfd.resolve(ol.abc);
        return dfd.promise;
    }

    this.getPaymentData = function(APP_ID, AGENT_CD){
        var dfd = $q.defer();

        LoadApplicationScreenData.loadPaymentData(APP_ID, AGENT_CD).then(
            function(payData){
            debug("getPaymentData :payData :"+JSON.stringify(payData));
                if(!!payData && !!payData.PAY_METHOD_ONLINE_FLAG && payData.PAY_METHOD_ONLINE_FLAG == 'Y')
                    dfd.resolve(true);
                else
                    dfd.resolve(false);

            }
        );
        return dfd.promise;
    }
}]);
