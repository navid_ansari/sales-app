overlayMenuModule.controller('OverlayMenuCtrl',['$state', 'CommonService', 'ToolkitService', 'LoginService', 'DashboardService', 'OverlayMenuService', 'LeadService', 'SisFormService', function($state, CommonService, ToolkitService, LoginService, DashboardService, OverlayMenuService, LeadService, SisFormService){
	var os = this;
	this.ntidLogin = CommonService.isNTIDLogin();

	debug("Menu ntidLogin: " + this.ntidLogin);
	debug("Menu CommonService.ntidLogin: " + CommonService.ntidLogin);

	this.restoreAvailable = (BUSINESS_TYPE!=="IndusSolution");

	this.isNTIDLogin = function(){
		return CommonService.isNTIDLogin();
	};

	this.getBusinessType=function(){
		return CommonService.getBusinessType();
	};

	this.copyDBToExternal = function(){
		CommonService.closeMenu();
		CommonService.copyDBToExternal();
		SisFormService.syncRemainingSIS();
	};

	this.goHome = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		if(DashboardService.getActiveTab()!=="home"){
			CommonService.showLoading();
		}
		$state.go("dashboard.home");
	};

	this.gotoKnowledgeBank = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		ToolkitService.getToolkitList(LoginService.lgnSrvObj.userinfo.AGENT_CD).then(
			function(res){
				if(!!res && res.length>0){
					$state.go("toolkit", {"ToolkitList": res});
				}
				else{
					navigator.notification.alert("No content available.",function(){
						CommonService.hideLoading();
					},"Knowledge Bank","OK");
				}
			}
		);
	};

	this.goToAgentInfo = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go("agentInfo");
	};

	this.goToDisclaimer = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go("disclamier");
	};

	this.gotoAppTracker = function(){
		CommonService.closeMenu();
		if(DashboardService.getActiveTab()!=="appTracker"){
			CommonService.showLoading();
		}
		DashboardService.gotoPage("appTracker");
	};

	this.gotoPolicyTracker = function(){
		CommonService.closeMenu();
		if(DashboardService.getActiveTab()!=="policyTracker"){
			CommonService.showLoading();
		}
		DashboardService.gotoPage("policyTracker");
	};

	this.goToChangePassword = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go(CHANGE_PASSWORD_STATE);
	};

	this.goToChangeSecQues = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go(CHANGE_SEC_QUES_STATE);
	};

	this.goToLeadListing = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go(LEAD_LISTING_STATE);
	};

	this.leadSync = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		LeadService.leadSync_N_Appoint();
	};

	this.startRestoration = function(){
		DashboardService.setRefreshFlag(false);
		CommonService.closeMenu();
		CommonService.showLoading();
		OverlayMenuService.restoreBackup();
	};

	this.logout = function(){
		OverlayMenuService.logout();
		DashboardService.setRefreshFlag(false);
	};

	this.showDB = function(){
		debug("db calling");
		CommonService.closeMenu();
		CommonService.showLoading();
		$state.go("showdatabase");
	};

	//Call PSC
	this.callPSC = function(){
		debug("Device Platform :"+device.platform);
		var pscReq ="{\"REQ\":{\"APPNO\":\"U987923336\",\"PRONAME\":\"Drastty Gusani\",\"EMAILID\":\"d@g.com\",\"MONO\":\"9004223289\",\"COMADD\":\"Vgvhx Zxf 400080\",\"PLANNAME\":\"Tata AIA Life Insurance Fortune Pro\",\"PLANTYPE\":\"Wealth Solutions\",\"PTYPE\":\"Non Linked Participating Endowment Assurance Plan\",\"PREAMT\":\"50000\",\"FREQ\":\"Annual\",\"POLITERM\":\"40\",\"PRETEAMPAY\":\"10\",\"SUMASSURED\":\"1000000\",\"MATU4\":\"\",\"MATU8\":\"\",\"MATULIP\":\"\",\"DOB\":\"10-01-1996\",\"ANNUOPT\":\"\",\"ANNUAMT\":\"\",\"ANNUFREQ\":\"\",\"BRONAME\":\"\",\"BANKNAME\":\"\",\"MODFRE\":\"Annual\",\"MODPRE\":\"50000\",\"USERID\":\"4574537\",\"PROID\":\"IPR2ULN1\",\"USERCODE\":\"4574537\",\"USERNAME\":\"FLMLBIR\",\"PASSWORD\":\"Tata_001\",\"USERCONNO\":\"9466856971\",\"EMAIL\":\"qaxabie.yeavad850@gqaid.srq\",\"USERSTAT\":\"success\",\"USERTYPE\":\"\",\"CHANID\":\"Agency-DSF\",\"SESSION\":\"A1212017132923\",\"SAVEFLAG\":\"\",\"IMAGEOFUSER\":\"\"}}";
		debug("pscReq:- " + pscReq);
		cordova.exec(
			function(succ){
				console.log("RETURN from PSC:"+JSON.stringify(succ));
				if(!!succ && !!succ.REQ && !!succ.REQ.STATUS){
					console.log(" PSC Response ::"+JSON.stringify(succ));
					if(succ.REQ.STATUS == '1'){
						navigator.notification.alert('PSC completed successfully !!',null, 'Success','Ok');
					}
					else{
						navigator.notification.alert("Not a proper response :-\n" + JSON.stringify(succ), null, "Application", "OK");
					}
				}
				else{
					navigator.notification.alert('PSC Data not found', null, 'Error', 'OK');
				}
			},
			function(err){
				console.log("req error :"+err);
				navigator.notification.alert("PSC Application not Installed\nPlease Install PSC Application", null, "Application", "OK");
			}, "PluginHandler", "CallPSC", [pscReq]
		);
	};
}]);

overlayMenuModule.service('OverlayMenuService',['CommonService', 'LoginService', 'SisFormService', '$state', function(CommonService, LoginService, SisFormService, $state){
	this.logout = function(){
		CommonService.closeMenu();
		CommonService.showLoading();
		this.clearLoginData();
		this.clearSISData();
		$state.go(LOGIN_STATE);
	};

	this.clearLoginData = function(){
		if(!!LoginService && !!LoginService.lgnSrvObj){
			delete LoginService.lgnSrvObj.username;
			delete LoginService.lgnSrvObj.password;
			delete LoginService.lgnSrvObj.userinfo;
			delete LoginService.lgnSrvObj.dvid;
			delete LoginService.lgnSrvObj.loginType;
			delete LoginService.lgnSrvObj.errorDescObj;
		}
		if(!!LoginService)
			LoginService.lgnSrvObj = {};
	};

	this.clearSISData = function(){
		if(!!SisFormService && !!SisFormService.sisData){
			delete SisFormService.sisData.sisFormAData;
			delete SisFormService.sisData.sisFormBData;
			delete SisFormService.sisData.sisFormCData;
			delete SisFormService.sisData.sisFormDData;
		}
		if(!!SisFormService)
			SisFormService.sisData = {};
	};

	this.restoreBackup = function(){
		var username = LoginService.lgnSrvObj.username;
		var password = LoginService.lgnSrvObj.password;
		var dvid = localStorage.DVID;
		CommonService.showLoading("Downloading data...please wait...");
		LoginService.startRestoration(username, password, dvid).then(
			function(res){
				CommonService.hideLoading();
				if(!!res && res=="S"){
					navigator.notification.alert("Restoration Successful", null, "Backup Restoration", "OK");
				}
			}
		);
	};
}]);
