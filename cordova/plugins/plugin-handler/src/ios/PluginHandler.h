//
//  PluginHandler.h
//  
//
//  Created by admin on 02/09/16.
//
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>
#import "ProcessDataForSIS.h"
#import "ProfileCamera.h"
#import "HTMLToPDF.h"

@interface PluginHandler : CDVPlugin

@property (nonatomic, retain) CDVInvokedUrlCommand *latestcommand;
@property (nonatomic, assign) BOOL hasPendingOperation;
@property (nonatomic ,retain) ProfileCamera *overlay;
@property (nonatomic, retain) NSArray *arrayOfProfDetails;

-(void) startSIS:(CDVInvokedUrlCommand *)command;
-(void)saveToClientEncryptedFile:(CDVInvokedUrlCommand *)command;
-(void)getTempFileURI:(CDVInvokedUrlCommand *)command;
-(void)getDecryptedFileName:(CDVInvokedUrlCommand*)command;
-(void)camera:(CDVInvokedUrlCommand *)command;
-(void)getDataFromFileInAssets:(CDVInvokedUrlCommand *)command;
-(void)readOriginalFile:(CDVInvokedUrlCommand *)command;

-(void) capturedImageWithPath:(NSDictionary *)dict;



@end
