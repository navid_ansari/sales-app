//
//  PluginHandler.m
//  
//
//  Created by admin on 02/09/16.
//
//

#import "PluginHandler.h"

@implementation PluginHandler
@synthesize latestcommand,hasPendingOperation,arrayOfProfDetails;
//generating sis..
-(void) startSIS:(CDVInvokedUrlCommand *)command{
    self.latestcommand = command;
	NSDictionary *reqSis = [command.arguments objectAtIndex:0];
	//NSLog(@"the request is %@",[reqSis objectForKey:@"REQ"]);
	NSLog(@"NATIVE FUNCTION CALLED SIS");
	
	ProcessDataForSIS *sisStart = [[ProcessDataForSIS alloc] init];
	NSMutableDictionary *sisResp =  [sisStart processDataForGeneratingSIS:reqSis];
	int respCode = [[sisResp objectForKey:@"code"] intValue];
	CDVPluginResult *result;
	if(respCode == 0){
		//error
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:sisResp];
	}else{
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:sisResp];
	}
	
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)saveEncryptedFile:(CDVInvokedUrlCommand *)command{
	NSArray *fileContent = [command.arguments objectAtIndex:0];
}

//save  the html file to give location..
-(void)saveToClientEncryptedFile:(CDVInvokedUrlCommand *)command{
	NSArray *fileContent = command.arguments;

	//first : sisid, second : type of file, third : filepath, fourth : filecontent, fifth : isbase64
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
    CDVPluginResult *result;
    BOOL flag = [htmlToPdf generatePDF:fileContent];
    if(flag){
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
        NSLog(@"ok");
    } else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail"];
        NSLog(@"error");
    }
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//in android it is used to decrypt the file.
//in ios no encryption..so directly return the file..
- (void)getDecryptedFileName:(CDVInvokedUrlCommand*)command {
    
    NSString *docName = [command.arguments objectAtIndex:0];
    NSString *docPath = [command.arguments objectAtIndex:1];
	
	CDVPluginResult *result;
	if(docName){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:docName];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//convert the file to nsurl..
-(void)getTempFileURI:(CDVInvokedUrlCommand *)command{
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSURL *url = [htmlToPdf getFileURI:command.arguments];
	
	CDVPluginResult *result;
	if(url){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[url absoluteString]];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//read the contents of file into string..
-(void)readDecryptedFile:(CDVInvokedUrlCommand *)command{
	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	CDVPluginResult *result;
	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)camera:(CDVInvokedUrlCommand *)command{
	self.hasPendingOperation = YES;
	self.latestcommand = command;
	
	arrayOfProfDetails = command.arguments;
	
	[self orientationFaceUp];
	
	self.overlay = [[ProfileCamera alloc] initWithProfileData:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
	//self.overlay.camera = self;
	// Display the view.  This will "slide up" a modal view from the bottom of the screen.
	
	//Sneha
	if ([arrayOfProfDetails[2] isEqualToString:@"Gallery"]) {
		self.overlay.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	} else if ([arrayOfProfDetails[2] isEqualToString:@"Camera"]) {
	self.overlay.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		}
	//Sneha
	
	NSLog(@"Moving to Camera View Controller from html");
	NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
		
		[userDefaults setInteger:1  forKey:@"orient"];
		
		
	}else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
		
		[userDefaults setInteger:0  forKey:@"orient"];
		
	}
	[userDefaults synchronize];
	NSLog(@"the camera view opened from profile plugin");
	[self.viewController presentViewController:self.overlay.picker animated:YES completion:nil];
}

-(void)gallery:(CDVInvokedUrlCommand *)command {
	
	NSLog(@"inside gallery plugin handler");
	
	
}
-(void)getDataFromFileInAssets:(CDVInvokedUrlCommand *)command{
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSString *fileDataVal = [htmlToPdf getContentFromFile:command.arguments];
	CDVPluginResult *result;
	if(fileDataVal){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileDataVal];
	}else{
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)readOriginalFile:(CDVInvokedUrlCommand *)command{
	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	
	CDVPluginResult *result;
	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}


#pragma mark - Public Methods
-(void)orientationFaceUp{
	
	UIDeviceOrientation orientatio =[[UIDevice currentDevice]orientation];
	//UIDeviceOrientation orientapp =
	//NSLog(@"the device orientation %d",orientatio);
	if( orientatio != UIDeviceOrientationLandscapeRight ||
	   orientatio != UIDeviceOrientationLandscapeLeft)
	{
		if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeRight] forKey:@"orientation"];
		}
		else
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
		}
	}
}

-(void) capturedImageWithPath:(NSDictionary *)dict{
	[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	
	// Unset the self.hasPendingOperation property
	self.hasPendingOperation = NO;
	
	// Hide the picker view
	[self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

@end
