package com.talic.plugins;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.billdesk.sdk.LibraryPaymentStatusProtocol;
import com.talic.payment.MainPaymentActivity;
import com.talic.plugins.CameraPlugin.CameraActivity;
import com.talic.plugins.CameraPlugin.CameraEditingActivity;
import com.talic.plugins.CameraPlugin.DisplayImageActivity;
import com.talic.plugins.CameraPlugin.DocumentActivity;
import com.talic.plugins.CameraPlugin.GalleryActivity;
import com.talic.plugins.CameraPlugin.ProfileCameraActivity;
import com.talic.plugins.sisEngine.control.ActionController;
import com.talic.salesapp.DBHelper;
import com.talic.salesapp.SalesApp;
import com.talic.plugins.CameraPlugin.GridViewActivity;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class PluginHandler extends CordovaPlugin implements LibraryPaymentStatusProtocol, Parcelable {
    private static final String TAG = "SA";
    String imgname;
    String values;
    public static CallbackContext currentContext;
	public PluginHandler() { }
    @Override
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) {
        Log.d(TAG, "***** Entering execute {PluginHandler} *****");
        if (action.equals("startSIS")) {
            Log.d(TAG, "***** Starting SIS *****");
            final PluginHandler pluginHandler = this;
            cordova.getThreadPool().execute(
                new Runnable() {
                    @Override
                    public void run() {
                    pluginHandler.startSIS(args, callbackContext);
                    }
                }
            );
            Log.d(TAG, "***** Exiting execute {PluginHandler} *****");
            return true;
        }
        if (action.equals("moveDatabase")) {
            Log.d(TAG, "***** Moving DB *****");
            DBHelper dbh = new DBHelper(AppConstants.SA_APP_CONTEXT);
            dbh.copyDatabase();
            callbackContext.success("true");
            Log.d(TAG, "***** Exiting execute {PluginHandler} *****");
            return true;
        }
        if (action.equals("showToast")) {
            Log.d(TAG, "***** showing Toast *****");
            this.showToast(args, callbackContext);
            Log.d(TAG, "***** Exiting execute {PluginHandler} *****");
            return true;
        }
        if(action.equals("getSADatabase")){
            Log.d(TAG, "***** Entering getDatabase *****");
            copyDatabaseToExternal();
            callbackContext.success("Successfully copied db");
            return true;
        }
        if(action.equals("updateCheck")){
            AppConstants.SA_CLASS.updateCheck();
            callbackContext.success("true");
        }
        if(action.equals("readDecryptedFile")){
            readDecryptedFile(args, callbackContext);
        }
        if(action.equals("saveEncryptedFile")){
            saveEncryptedFile(args, callbackContext);
        }
        if(action.equals("getDecryptedFileName")){
            getDecryptedFileName(args, callbackContext);
        }
        if(action.equals("saveToClientEncryptedFile")){
            saveToClientEncryptedFile(args, callbackContext);
        }
        if(action.equals("deleteTmp")){
            deleteTmp(callbackContext);
        }
        if(action.equals("openFile")){
            openFile(args);
        }
		if(action.equals("getTempFileURI")){
			getTempFileURI(args, callbackContext);
		}
		if(action.equals("getDataFromFileInAssets")){
			getDataFromFileInAssets(args, callbackContext);
		}
        if(action.equals("saveNonEncryptedFile")){
            saveNonEncryptedFile(args, callbackContext);
        }
        //camera
        if(action.equals("readOriginalFile")){
            readOriginalFile(args, callbackContext);
        }
        if(action.equals("camera")){
            callCamera(args, callbackContext);
            return true;
        }
        if(action.equals("viewimage")){
            callCamera(args, callbackContext);
            return true;
        }
//        //Added by Amruta
        if (action.equals("paymentGateway")) {
            paymentGateway(args, callbackContext);
			return true;
        }
        //june 30th
        if(action.equals("getTempNormalFileURI")){
            getTempNormalFileURI(args, callbackContext);
        }
        Log.d(TAG,"***** Exiting execute {PluginHandler} *****");
        return false;
    }

    public void copyDatabaseToExternal(){
        Log.d(TAG,"***** Entering copyDatabaseToExternal *****");
        Log.d(TAG,"source: " + AppConstants.SA_MAIN_DB_PATH);
        Log.d(TAG,"destination: " + AppConstants.SA_APP_CONTEXT.getExternalFilesDir(null) + "/" + AppConstants.SA_MAIN_DB_NAME);
        Log.d(TAG,"***** Writing file database *****");
        DBHelper.decrypt(AppConstants.SA_MAIN_DB_NAME, "life");
    }

    //read image
    //camera
    public void readOriginalFile(JSONArray args,CallbackContext callbackContext) {
        try{
            AppUtils nf = new AppUtils();
            String fileData = nf.readOriginalFile(args.getString(0),args.getString(1));

            if(fileData!=null && !("".equals(fileData))) {
                Log.d(TAG,"PluginHandler().readDecryptedFile() fileData.length(): if: " + fileData.length());
                callbackContext.success(fileData);
            }
            else {
                Log.d(TAG,"PluginHandler().readDecryptedFile() fileData.length(): else: " + fileData);
                callbackContext.error("File data returned blank");
            }
        }catch (Exception ex){
            Log.d(TAG, "Exception in readDecryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void getTempNormalFileURI(JSONArray args,CallbackContext callbackContext){
        try{
            File file = new File(AppConstants.FILE_ABS_PATH +args.getString(0)+ "/" + args.getString(1));
            callbackContext.success(file.toURI().toString());
        }catch(Exception ex){
            Log.d(TAG, "Exception in getTempNormalFileURI: " + ex.getMessage());
            callbackContext.error("Exception in getTempNormalFileURI(): " + ex.getMessage());
        }
    }

    public void startSIS(JSONArray args, CallbackContext callbackContext) {
        try {
            Log.d(TAG, "***** Entering startSIS() *****");
            Log.d(TAG, " JSONArray args: " + args.toString());
            ActionController ac = new ActionController();
            if (args.getJSONObject(0) != null && args.getJSONObject(0).length() > 0) {
                JSONObject json = ac.handleSisCall(args);
                Log.d(TAG,"doneHandlingSISCall");
                try{callbackContext.success(json);}catch (Exception ex){
                    Log.d(TAG,"Exception json: " + ex.getMessage());
                }
                Log.d(TAG, "doneHandlingSISCall json done");
            } else {
                callbackContext.error("Expected one non-empty string argument.");
            }
            Log.d(TAG, "***** Exiting startSIS() *****");
        }catch (Exception e) {
            Log.e(TAG, "Exception in startingPoint() {PluginHandler}: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void readDecryptedFile(JSONArray args,CallbackContext callbackContext) {
        try{
            AppUtils nf = new AppUtils();
            String fileData = nf.readDecryptedFile(args.getString(0),args.getString(1));

            if(fileData!=null && !("".equals(fileData))) {
                Log.d(TAG,"PluginHandler().readDecryptedFile() fileData.length(): if: " + fileData.length());
                callbackContext.success(fileData);
            }
            else {
                Log.d(TAG,"PluginHandler().readDecryptedFile() fileData.length(): else: " + fileData);
                callbackContext.error("File data returned blank");
            }
        }catch (Exception ex) {
            Log.d(TAG, "Exception in readDecryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

	public void saveNonEncryptedFile(JSONArray args,CallbackContext callbackContext){
		try{
			AppUtils appUtils = new AppUtils();
			appUtils.saveNonEncryptedFile(args.getString(0), args.getString(1), args.getString(2), args.getString(3));
			Log.d(TAG,"PluginHandler().saveEncryptedFile(): Success");
			callbackContext.success("success");
		}catch (JSONException ex){
			Log.d(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
			ex.printStackTrace();
			callbackContext.error("JSONException in saveEncryptedFile(): " + ex.getMessage());
		}
	}

    public void saveEncryptedFile(JSONArray args,CallbackContext callbackContext) {
        try{
            AppUtils appUtils = new AppUtils();
            appUtils.saveEncryptedFile(args.getString(0), args.getString(1), args.getString(2),args.getString(3));
            Log.d(TAG,"PluginHandler().saveEncryptedFile(): Success");
            callbackContext.success("Success");
        }catch (JSONException ex){
            Log.d(TAG, "Exception in saveEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
            callbackContext.error("JSONException in saveEncryptedFile(): " + ex.getMessage());
        }
    }

    public void getDecryptedFileName(JSONArray args,CallbackContext callbackContext) {
        try{
            AppUtils nf = new AppUtils();
            String name = nf.getDecryptedFileName(args.getString(0),args.getString(1));

            if(name!=null && (!"".equals(name))){
                callbackContext.success(name);
            }
            else{
                callbackContext.error("File path is blank or null");
            }
        }catch (JSONException ex) {
            Log.d(TAG, "Exception in getDecryptedFileName(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void saveToClientEncryptedFile(JSONArray args,CallbackContext callbackContext){
        try{
            AppUtils appUtils = new AppUtils();
            if(args.getString(4)!=null)
                appUtils.saveToClientEncryptedFile(args.getString(0), args.getString(1), args.getString(2),args.getString(3),args.getString(4));
            else
                appUtils.saveToClientEncryptedFile(args.getString(0), args.getString(1), args.getString(2), args.getString(3), null);
            Log.d(TAG,"PluginHandler().saveToClientEncryptedFile() fileData.length(): if: Success");
            callbackContext.success("Success");
        }catch (JSONException ex) {
            Log.d(TAG,"Exception in saveToClientEncryptedFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void getTempFileURI(JSONArray args,CallbackContext callbackContext){
        try{
            File file = new File(AppConstants.FILE_ABS_PATH + AppConstants.TMP_DIR_NAME + "/" + args.getString(0));
            callbackContext.success(file.toURI().toString());
        }catch(Exception ex){
            Log.d(TAG, "Exception in getTempFileName: " + ex.getMessage());
            callbackContext.error("Exception in getTempFileName(): " + ex.getMessage());
        }
    }

    public void deleteTmp(CallbackContext callbackContext){
        AppUtils nf = new AppUtils();
        String tmpDelStatus = nf.deleteTmp();
        if(tmpDelStatus!=null && (!"".equals(tmpDelStatus))){
            callbackContext.success(tmpDelStatus);
        }
        else{
            callbackContext.error(tmpDelStatus);
        }
    }

    public void showToast(JSONArray args, CallbackContext callbackContext) {
        try{
            int align;
            int duration;
            if(args.get(1).toString().equalsIgnoreCase("short")){
                duration = Toast.LENGTH_SHORT;
            }
            else{
                duration = Toast.LENGTH_LONG;
            }
            if(args.get(2).toString().equalsIgnoreCase("middle")){
                align = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;
            }
            else if(args.get(2).toString().equalsIgnoreCase("top")){
                align = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
            }
            else{
                align = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
            }
            Toast toast = Toast.makeText(AppConstants.SA_APP_CONTEXT, args.get(0).toString(), duration);
            toast.setGravity(align, 0, 0);
            toast.show();
            callbackContext.success("Toast shown");
        } catch (JSONException e) {
            Log.e(TAG, "Exception in startingPoint() {PluginHandler}: " + e.getMessage());
            e.printStackTrace();
            callbackContext.error("Error showing Toast");
        }
    }

    public void getDataFromFileInAssets(JSONArray args,CallbackContext callbackContext) {
		try {
			String assetFileData = new AppUtils().getDataFromFileInAssets(args.getString(0));
			if (assetFileData != null)
				callbackContext.success(assetFileData);
			else
				callbackContext.error("File data is null or File is not available");
		}catch (IOException ex){
			Log.e(TAG, "IOException in getDataFromFileInAssets(): " + ex.getMessage());
			ex.printStackTrace();
        } catch (JSONException ex) {
            Log.e(TAG, "JSONException in getDataFromFileInAssets(): " + ex.getMessage());
			ex.printStackTrace();
		}
    }

    private void openFile(JSONArray args) {
        try {
            String url = args.getString(0);
            // Create URI
            Uri uri = Uri.parse("content://" + AppConstants.SA_APP_CONTEXT.getPackageName() + "/" + AppConstants.ROOT_FOLDER + AppConstants.TMP_DIR_NAME + "/" + url);
			Log.d(TAG,"uri:: " + "content://" + AppConstants.SA_APP_CONTEXT.getPackageName() + "/" + AppConstants.ROOT_FOLDER + AppConstants.TMP_DIR_NAME + "/" + url);
            new DataProvider().openFile(uri,null);

            Intent intent;
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file

            if (url.contains(".doc") || url.contains(".docx")) {
                // Word document
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/msword");
            } else if (url.contains(".pdf")) {
                // PDF file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.contains(".ppt") || url.contains(".pptx")) {
                // Powerpoint file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.contains(".xls") || url.contains(".xlsx")) {
                // Excel file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.contains(".rtf")) {
                // RTF file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.contains(".wav")) {
                // WAV audio file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.contains(".gif")) {
                // GIF file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "image/gif");
            } else if (url.contains(".jpg") || url.contains(".jpeg")) {
                // JPG file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.contains(".png")) {
                // PNG file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "image/png");
            } else if (url.contains(".txt")) {
                // Text file
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "text/plain");
            } else if (url.contains(".mpg") || url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
                // Video files
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "video/*");
            }

            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use

            else {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            this.cordova.getActivity().startActivity(intent);
        } catch (JSONException ex) {
            Log.d(TAG, "Exception in openFile(): " + ex.getMessage());
            ex.printStackTrace();
        }catch (FileNotFoundException ex) {
            Log.d(TAG, "Exception in openFile(): " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void callCamera(JSONArray args,CallbackContext callbackContext){
        this.currentContext = callbackContext;
		String fullPath = null;
		String imageName = null;
		String flag = null;
		try{
			fullPath = args.getString(0);
			imageName = args.getString(1);
            flag = args.getString(2);
		}
		catch(JSONException ex){
			Log.d("TAG", "Exception: " + ex.getMessage());
		}
        Intent intent = null;

        Context context=this.cordova.getActivity().getApplicationContext();
        if(flag.equals("Edit")){
            intent=new Intent(context,CameraEditingActivity.class);
        }
        else if(flag.equals("Camera")){
            intent=new Intent(context,CameraActivity.class);
        }
        else if(flag.equals("Profile")){
            intent=new Intent(context,ProfileCameraActivity.class);
        }
        else if(flag.equals("Gallery")){
            intent = new Intent(context, GalleryActivity.class);
        }
        else if(flag.equals("Document")){
            intent = new Intent(context, DocumentActivity.class);
        }
        else if(flag.equals("viewimage")){
            intent = new Intent(context, GridViewActivity.class);
        }
        intent.putExtra("name", imageName);
        intent.putExtra("path", fullPath);

        Log.d("full path",fullPath);
        Log.d("img name",imageName);

        this.cordova.startActivityForResult(this, intent, 0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 0) {

                values=data.getStringExtra("values");
				Log.d(TAG, "onActivityResult PluginHandler");
                JSONObject path=new JSONObject(values);
                this.currentContext.success(path);
                System.out.println(path.toString());
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void paymentGateway(JSONArray args, CallbackContext callbackContext){
        try {
            Context context = this.cordova.getActivity();
            Intent intent = new Intent(this.cordova.getActivity(), MainPaymentActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            JSONObject reqObj = new JSONObject(args.getString(0));
            intent.putExtra("Amount", (String) reqObj.get("Amount"));
            intent.putExtra("PolicyNo", (String) reqObj.get("PolicyNo"));
            intent.putExtra("PremiumType", (String) reqObj.get("PremiumType"));
            intent.putExtra("OwnerName", (String) reqObj.get("OwnerName"));
            intent.putExtra("AgentCode", (String) reqObj.get("AgentCode"));
            intent.putExtra("PremiumDueDate", (String) reqObj.get("PremiumDueDate"));
            intent.putExtra("Mobile", (String) reqObj.get("Mobile"));
            intent.putExtra("EmailId", (String) reqObj.get("EmailId"));
            intent.putExtra("TrSeries", (String) reqObj.get("TrSeries"));
            intent.putExtra("plug", this);
            currentContext = callbackContext;
            context.startActivity(intent);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void paymentStatus(String status, Activity activity) {
		PluginResult result = new PluginResult(PluginResult.Status.OK, status);
		result.setKeepCallback(true);
        this.currentContext.sendPluginResult(result);
        // code for  the activity
        Intent mIntent = new Intent(activity, SalesApp.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(mIntent);
        activity.finish();
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

	public static final Creator CREATOR = new Creator() {
		String TAG="Callback --- Parcelable.Creator ::: > ";
		@Override
		public PluginHandler createFromParcel(Parcel in) {
			Log.v(TAG, "CallBackActivity createFromParcel(Parcel in)....");
			return new PluginHandler(in);
//            return null;
		}

		@Override
		public Object[] newArray(int size) {
			Log.v(TAG, "Object[] newArray(int size)....");
			return new PluginHandler[size];
		}
	};

	public PluginHandler(Parcel in) {
		Log.v(TAG, "CallBack(Parcel in)....");
	}
}
