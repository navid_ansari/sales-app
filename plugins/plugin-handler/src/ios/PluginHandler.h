//
//  PluginHandler.h
//
//
//  Created by admin on 02/09/16.
//
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>
#import "ProcessDataForSIS.h"
#import "ProfileCamera.h"
#import "HTMLToPDF.h"
#import "CustomCameraController.h"
#import "MultiImgSelectVC.h"
#import "CustomPickerViewController.h"

@interface PluginHandler : CDVPlugin <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, retain) CDVInvokedUrlCommand *latestcommand;
@property (nonatomic, assign) BOOL hasPendingOperation;
@property (nonatomic ,retain) ProfileCamera *overlay;
@property (nonatomic ,retain) CustomCameraController *custCamera;
@property (nonatomic, retain) NSArray *arrayOfProfDetails;
@property (nonatomic ,retain) MultiImgSelectVC *imgSelector;
@property (nonatomic, retain) CustomPickerViewController *custPicker;


-(void) startSIS:(CDVInvokedUrlCommand *)command;
-(void)saveToClientEncryptedFile:(CDVInvokedUrlCommand *)command;
-(void)getTempFileURI:(CDVInvokedUrlCommand *)command;
-(void)getDecryptedFileName:(CDVInvokedUrlCommand*)command;
-(void)camera:(CDVInvokedUrlCommand *)command;
-(void)getDataFromFileInAssets:(CDVInvokedUrlCommand *)command;
-(void)readOriginalFile:(CDVInvokedUrlCommand *)command;
-(void)getTempNormalFileURI:(CDVInvokedUrlCommand *)command;
-(void)paymentGateway:(CDVInvokedUrlCommand *)command;
-(void)openFile:(CDVInvokedUrlCommand *)command;

-(void) capturedImageWithPath:(NSDictionary *)dict;
-(void)closeCameraWithDict:(NSDictionary *)dict;



@end
