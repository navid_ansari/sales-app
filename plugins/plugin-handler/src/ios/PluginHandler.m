//
//  PluginHandler.m
//
//
//  Created by admin on 02/09/16.
//
//

#import "PluginHandler.h"
#import "GalleryVC.h"
#import "BillDeskPG.h"

@implementation PluginHandler
@synthesize latestcommand,hasPendingOperation,arrayOfProfDetails;
//generating sis..
-(void) startSIS:(CDVInvokedUrlCommand *)command{
	self.latestcommand = command;
	NSDictionary *reqSis = [command.arguments objectAtIndex:0];
	NSLog(@"the request in GoodConnect is %@",reqSis);
	NSLog(@"NATIVE FUNCTION CALLED SIS");
	
	ProcessDataForSIS *sisStart = [[ProcessDataForSIS alloc] init];
	NSMutableDictionary *sisResp =  [sisStart processDataForGeneratingSIS:reqSis];
	int respCode = [[sisResp objectForKey:@"code"] intValue];
	CDVPluginResult *result;
	if(respCode == 0){
		//error
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:sisResp];
	}else{
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:sisResp];
	}
	
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)saveEncryptedFile:(CDVInvokedUrlCommand *)command{
	//	NSArray *fileContent = [command.arguments objectAtIndex:0];
	NSArray *fileContent = command.arguments;
	
	//first : sisid, second : type of file, third : filepath, fourth : filecontent, fifth : isbase64
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	CDVPluginResult *result;
	BOOL flag = [htmlToPdf generatePDF:fileContent];
	if(flag){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail"];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//save  the html file to give location..
-(void)saveToClientEncryptedFile:(CDVInvokedUrlCommand *)command{
	NSArray *fileContent = command.arguments;
	
	//first : sisid, second : type of file, third : filepath, fourth : filecontent, fifth : isbase64
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	CDVPluginResult *result;
	BOOL flag = [htmlToPdf generatePDF:fileContent];
	if(flag){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail"];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//in android it is used to decrypt the file.
//in ios no encryption..so directly return the file..
- (void)getDecryptedFileName:(CDVInvokedUrlCommand*)command {
	
	NSString *docName = [command.arguments objectAtIndex:0];
	NSString *docPath = [command.arguments objectAtIndex:1];
	NSArray *fileWithPath = [NSArray arrayWithObjects:docPath, docName, nil]; // [docPath, docName];
	
	CDVPluginResult *result;
	if(docName){
		//		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:docName];
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:fileWithPath]; // Sneha
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//convert the file to nsurl..
-(void)getTempFileURI:(CDVInvokedUrlCommand *)command{
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSURL *url = [htmlToPdf getFileURI:command.arguments];
	
	CDVPluginResult *result;
	if(url){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[url absoluteString]];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//read the contents of file into string..
-(void)readDecryptedFile:(CDVInvokedUrlCommand *)command{
	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	CDVPluginResult *result;
	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)camera:(CDVInvokedUrlCommand *)command{
	
	self.hasPendingOperation = YES;
	self.latestcommand = command;
	arrayOfProfDetails = command.arguments;
	[self orientationFaceUp];
	
	NSLog(@"Moving to Camera View Controller from html");
	NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
		
		[userDefaults setInteger:1  forKey:@"orient"];
		
		
	}else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
		
		[userDefaults setInteger:0  forKey:@"orient"];
		
	}
	[userDefaults synchronize];
	
	//Sneha
	if ([arrayOfProfDetails[2] isEqualToString:@"Gallery"]) {
		
		self.imgSelector = [[MultiImgSelectVC alloc] initWithProfileData:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		[self.viewController presentViewController:self.imgSelector.elcPicker animated:YES completion:nil];
		
	} else if ([arrayOfProfDetails[2] isEqualToString:@"Camera"]) {
		
		self.custCamera = [[CustomCameraController alloc] initWithDataFromPhoneGap:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		NSLog(@"Moving to Camera View Controller from html");
		[self.viewController presentViewController:self.custCamera.picker animated:YES completion:nil];
		
	} else if ([arrayOfProfDetails[2] isEqualToString:@"Profile"]) {
		
		self.overlay = [[ProfileCamera alloc] initWithProfileData:(NSMutableArray *)arrayOfProfDetails andWithDelegate:self];
		self.overlay.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		NSLog(@"the camera view opened from profile plugin");
		[self.viewController presentViewController:self.overlay.picker animated:YES completion:nil];
	}
	else if ([arrayOfProfDetails[2] isEqualToString:@"viewimage"]  || [arrayOfProfDetails[2] isEqualToString:@"viewimageProfile"]) {
		[self gallery:command];
	}else if([arrayOfProfDetails[2] isEqualToString:@"GalleryProfile"]){
		
		//Added by Sneha for profile img selection 03-01-16
		self.custPicker = [[CustomPickerViewController alloc]init];
		self.custPicker.delegate = self;
		self.custPicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
		[self.viewController presentViewController:self.custPicker animated:YES completion:nil];
		
	}
}

-(void)gallery:(CDVInvokedUrlCommand *)command {
	
	NSLog(@"inside gallery plugin handler");
	NSLog(@"array: %@", arrayOfProfDetails);
	NSString *fileURI = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",command.arguments[0]]];
	
	NSError *error = nil;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fileURI error:&error];
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF contains[dc] %@",command.arguments[1]];
	NSLog(@"files: %@\ncount: %lu", [dirContents filteredArrayUsingPredicate:pred], (unsigned long)[dirContents filteredArrayUsingPredicate:pred].count);
	//Added by Sneha 30-12-16
	NSInteger imgCount;
	if ([[arrayOfProfDetails objectAtIndex:2] isEqualToString:@"viewimageProfile"]) {
		imgCount = 1;
	} else {
		imgCount = [dirContents filteredArrayUsingPredicate:pred].count;
	}
	
	GalleryVC *galleryVC = [[GalleryVC alloc] initWithArray:arrayOfProfDetails andWithImageCount:(int)imgCount];
	galleryVC.plugin = self;
	[self.viewController presentViewController:galleryVC animated:NO completion:^{}];
}

-(void)getTempNormalFileURI:(CDVInvokedUrlCommand *)command {
	
	NSLog(@"PluginHandler -> getTempNormalFileURI: %@", command.arguments);
	NSString *fileURI = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent: [NSString stringWithFormat:@"%@%@",command.arguments[0], command.arguments[1]]];
	CDVPluginResult *result;
	
	if(fileURI){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileURI];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)getDataFromFileInAssets:(CDVInvokedUrlCommand *)command {
	
	HTMLToPDF *htmlToPdf = [[HTMLToPDF alloc] init];
	NSString *fileDataVal = [htmlToPdf getContentFromFile:command.arguments];
	CDVPluginResult *result;
	
	if(fileDataVal){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileDataVal];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

-(void)readOriginalFile:(CDVInvokedUrlCommand *)command {
	
	NSString *fileData = [[[HTMLToPDF alloc] init] readFileData:command.arguments];
	CDVPluginResult *result;
	
	if(fileData.length != 0){
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:fileData];
		NSLog(@"ok");
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:NULL];
		NSLog(@"error");
	}
	[self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

//Added by Sneha 30-12-16
-(void)paymentGateway:(CDVInvokedUrlCommand *)command {
	
	NSLog(@"PluginHandler -> paymentGateway: %@", command.arguments);
	
	NSDictionary *paymentReqObj = [NSDictionary dictionaryWithObjectsAndKeys:
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"Amount"]], @"Amount",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PolicyNo"]], @"PolicyNo",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PremiumType"]], @"PremiumType",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"OwnerName"]], @"OwnerName",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"AgentCode"]], @"AgentCode",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"PremiumDueDate"]], @"PremiumDueDate",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"Mobile"]], @"Mobile",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"EmailId"]], @"EmailId",
								   [NSString stringWithFormat:@"%@", command.arguments[0][@"TrSeries"]], @"TrSeries", nil];
	
	NSLog(@"paymentReqObj: %@", paymentReqObj);
	
	[[BillDeskPG new] createMessageForBillDesk:paymentReqObj];
	
}

-(void)openFile:(CDVInvokedUrlCommand *)command {
	
	NSLog(@"PluginHandler -> openFile: %@", command.arguments);
	
	
}

#pragma mark - Public Methods
-(void)orientationFaceUp{
	
	UIDeviceOrientation orientatio =[[UIDevice currentDevice]orientation];
	//UIDeviceOrientation orientapp =
	//NSLog(@"the device orientation %d",orientatio);
	if( orientatio != UIDeviceOrientationLandscapeRight ||
	   orientatio != UIDeviceOrientationLandscapeLeft)
	{
		if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeRight] forKey:@"orientation"];
		}
		else
		{
			[[UIDevice currentDevice]setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
		}
	}
}

-(void) capturedImageWithPath:(NSDictionary *)dict{
	[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	
	// Unset the self.hasPendingOperation property
	self.hasPendingOperation = NO;
	
	// Hide the picker view
	[self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

// Sneha
-(void)closeCameraWithDict:(NSDictionary *)dict {
	
	[self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict] callbackId:self.latestcommand.callbackId];
	self.hasPendingOperation = NO;
	[self.viewController dismissViewControllerAnimated:YES completion:NULL];
}


//Added by Sneha for profile img selection 03-01-16
#pragma mark - UIImagePickerControllerDelegate method
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
	// Dismiss the image selection, hide the picker and
	
	//show the image view with the picked image
	
	[self.custPicker dismissViewControllerAnimated:YES completion:nil];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *imageFilePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetails objectAtIndex:0],[NSString stringWithFormat:@"%@.jpg",[arrayOfProfDetails objectAtIndex:1]]];
	NSLog(@"first profile picture path created :::%@",imageFilePath);
	NSData *JpegIamge = UIImageJPEGRepresentation(image, 0.4);
	[JpegIamge writeToFile:imageFilePath atomically:YES];
	
	//profile pic i.e thumbnail image
	NSString *thumbImagePath =[NSString stringWithFormat:@"%@%@%@",documentsDirectory,[arrayOfProfDetails objectAtIndex:0],[NSString stringWithFormat:@"%@_thumb.jpg",[arrayOfProfDetails objectAtIndex:1]]];
	NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
	[returnData setValue:thumbImagePath forKey:@"path"];
	NSLog(@"thumb picture path created :::%@",thumbImagePath);
	NSData *JpegThumbImage = UIImageJPEGRepresentation([self imageResize:image andResizeTo:CGSizeMake(110, 110)], 1.0);
	[JpegThumbImage writeToFile:thumbImagePath atomically:YES];
	
	
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	[dict setObject:[NSNumber numberWithInt:0] forKey:@"code"];
	[dict setObject:@"error" forKey:@"message"];
	[dict setObject:@"" forKey:@"path"];
	[self capturedImageWithPath:dict];
}

#pragma mark ImageResizing Method
// ImageResizing Method (image size is less than 500 kb)
-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize{
	
	CGFloat scale = 1.0;
	UIGraphicsBeginImageContextWithOptions(newSize, YES, scale);
	[img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

@end
